+++
Title = "iOS screen recording of bug test #1"
Date = "2023-09-11T02:00:00-0800"
Author = "Paige"
Description = "See other MobileSafari bug tests posts for more info"
cover = "img/og.png"
keywords = ["bug"]
tags = ["bug", "iOS"]
+++

Tests:
- [/posts/videotest1/](/posts/videotest1/)
- [/posts/videotest2/](/posts/videotest2/)
- [/posts/videotest3/](/posts/videotest3/)

Update: 
- This behavior is reproducible always in Safari, as well as Firefox, Chrome, and Edge.

Just in case I'm the only person in the world who is experiencing this, here's a screen capture of the behavior. The video starts by showing that the keyboard is working correctly, the sounds play correctly etc. 

After playing the video the sounds are lagged, and it's obvious something about the volume has changed as well. 

Also even playing this video in MobileSafari will cause the keyboard feedback clicks to lag and results in the same faulted state. So as near as I can tell: 

```
<video controls autoplay style="height: 480px; width: 360px;">
  <source src="/img/fault_example1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
```

HTML video embed tags/playback on MobileSafari just break the keyboard feedback / click sounds no matter what you do. 

<video controls autoplay style="height: 480px; width: 360px;">
  <source src="/img/fault_example1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

```
Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'static/img/fault_example1.mp4':
  Metadata:
    major_brand     : mp42
    minor_version   : 1
    compatible_brands: isommp41mp42
    creation_time   : 2023-09-13T01:06:08.000000Z
  Duration: 00:00:41.02, start: 0.000000, bitrate: 7126 kb/s
  Stream #0:0[0x1](und): Audio: aac (LC) (mp4a / 0x6134706D), 44100 Hz, stereo, fltp, 17 kb/s (default)
    Metadata:
      creation_time   : 2023-09-13T01:06:08.000000Z
      handler_name    : Core Media Audio
      vendor_id       : [0][0][0][0]
  Stream #0:1[0x2](und): Video: h264 (High) (avc1 / 0x31637661), yuvj420p(pc, bt709/bt709/iec61966-2-1, progressive), 886x1920, 7098 kb/s, 48.20 fps, 60 tbr, 600 tbn (default)
    Metadata:
      creation_time   : 2023-09-13T01:06:08.000000Z
      handler_name    : Core Media Video
      vendor_id       : [0][0][0][0]
```

