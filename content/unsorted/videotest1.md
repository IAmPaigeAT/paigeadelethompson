+++
Title = "Video bug test for iOS (controls, unmuted, autoplay)"
Date = "2023-09-11T00:00:00-0800"
Author = "Paige"
Description = "trying to isolate a bug in MobileSafari"
cover = "img/og.png"
keywords = ["bug"]
tags = ["bug", "iOS"]
+++

- Muted or unmuted doesn't make any difference
- autoplay doesn't work or is ignored on MobileSafari but does on desktop? 

After playing this video, the keyboard click sounds are lagged because the sound hw is in a faulted state. The video doesn't have an audio track: 

```
Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'test.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Google
  Duration: 00:00:05.70, start: 0.000000, bitrate: 113 kb/s
  Stream #0:0[0x1](und): Video: h264 (High) (avc1 / 0x31637661), yuv420p(progressive), 256x144, 111 kb/s, 10 fps, 10 tbr, 10240 tbn (default)
    Metadata:
      handler_name    : VideoHandler
      vendor_id       : [0][0][0][0]
```
just a video stream, so the fact that AppleAVD is setting up an audio client at all doesn't even make any sense, but it does. 

<video controls autoplay>
  <source src="/img/test.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
