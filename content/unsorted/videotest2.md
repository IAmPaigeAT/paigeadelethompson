+++
Title = "Video bug test #2 for iOS (controls, unmuted, autoplay)"
Date = "2023-09-11T00:30:00-0800"
Author = "Paige"
Description = "trying to isolate a bug in MobileSafari"
cover = "img/og.png"
keywords = ["bug"]
tags = ["bug", "iOS"]
+++

- I attached an audio track to the video with ffmpeg, but the sound hw is still in a faulted state after playing it (keyboard feedback clicks are still lagged) 

```
Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'paigeadelethompson/static/img/test2.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf60.3.100
  Duration: 00:00:05.70, start: 0.000000, bitrate: 142 kb/s
  Stream #0:0[0x1](und): Video: h264 (High) (avc1 / 0x31637661), yuv420p(progressive), 256x144, 111 kb/s, 10 fps, 10 tbr, 10240 tbn (default)
    Metadata:
      handler_name    : VideoHandler
      vendor_id       : [0][0][0][0]
  Stream #0:1[0x2](und): Audio: aac (LC) (mp4a / 0x6134706D), 44100 Hz, mono, fltp, 70 kb/s (default)
    Metadata:
      handler_name    : Core Media Audio
      vendor_id       : [0][0][0][0]
  14.29 A-V: -0.087 fd=   0 aq=    0KB vq=    0KB sq=    0B f=0/
```

The audio track is sampled at 44.1khz which is what AppleAVD tries to initialize the driver to even without a an audio track (iOS system logs reveals this.) 

<video controls autoplay>
  <source src="/img/test2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
