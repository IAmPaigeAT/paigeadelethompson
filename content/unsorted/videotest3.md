+++
Title = "Video bug test #3 for iOS (controls, unmuted, autoplay)"
Date = "2023-09-11T01:00:00-0800"
Author = "Paige"
Description = "trying to isolate a bug in MobileSafari"
cover = "img/og.png"
keywords = ["bug"]
tags = ["bug", "iOS"]
+++

- This test adds a sound track sampled at 48000khz, but the sound hw is still faulted after playing it, so it makes no difference. 

<video controls autoplay>
  <source src="/img/test3.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
