+++
Title = "Bookmarks"
Date = "2023-01-13T09:58:20-0800"
Author = "Paige"
Description = "Links"
cover = "img/og.png"
tags = ["Bookmarks", "Links"]
+++

    
## Programming
[The Rust programming language Lise Henry](https://lise-henry.github.io/books/trpl2.pdf)  
[B Flat; A golang inspired C# compiler](https://flattened.net)  
[Runtime reflection for Rust](https://www.osohq.com/post/rust-reflection-pt-1)  
[Short, friendly base32 slugs from timestamps](https://brandur.org/fragments/base32-slugs)  
[ZMachine Standards Documentation](https://inform-fiction.org/zmachine/standards/z1point1/index.html)  
[What is a UUID/GUID?](https://en.wikipedia.org/wiki/Universally_unique_identifier#History)  
[Asynchronous programming with Rust](https://rust-lang.github.io/async-book/)

### Web Development
[traviso.js; An isometric 3D game engine written in javascript](https://www.travisojs.com)  
[babylon.js; A 3D engine with WebGPU and WebGL2 support](https://doc.babylonjs.com/journey)  
[ClojureScript; Clojure LISP dialect for Javascript](https://clojurescript.org)  
[cube-test; Babylon CLJ VR game example](https://github.com/vt5491/cube-test)

### Game Development 
[Isometric Projection in Game Development](https://pikuma.com/blog/isometric-projection-in-games)  
[Diablo - Original game pitch](http://www.graybeardgames.com/download/diablo_pitch.pdf)  
[Blender .OBJ files on WebGPU](https://carmencincotti.com/2022-06-06/load-obj-files-into-webgpu/)

## Art
[Billy's quest for love](https://ily888.itch.io/billys-quest-for-love)  
[Sex with a chicken](https://everything2.com/title/Sex+with+a+chicken)  
[Da Wey, Путь, Der Weg, 办法, VEIEN](https://shithole.neocities.org/way)  
[Shavian; An Alternative alphabet for English](https://www.shavian.info)  
[ASCII Blaster](https://asdf.us/asciiblaster/)  
[vx-underground](https://www.vx-underground.org)  
[ACiD Productions](https://www.acid.org/whatsnew/whatsnew.html)  
[LHOHQ](http://lhohq.info)  
[yyyyyyy (y7)](http://yyyyyyy.info)  
[Yvette's Bridal Formal](https://yvettesbridalformal.p1r8.net)  
[Textfiles Art Scene](http://artscene.textfiles.com/ansi/)  
[ASCII Art Generator](https://www.ascii-art-generator.org)  
[ASCII Bird](https://github.com/birdneststream/asciibird)  
[A Strange collection of fake, uncanny interpol warrants](https://archive.org/details/@interpol_warrants)  
[140MP pictures of the sun](https://www.reddit.com/r/space/comments/122475u/i_teamed_up_with_a_fellow_redditor_to_try_and/)  
[PETSCII Editor](https://petscii.krissz.hu)

### Fonts 
[Bad Apple Font](https://github.com/hsfzxjy/Bad-Apple-Font)  
[Ultimate old-school PC font pack](https://int10h.org/oldschool-pc-fonts/readme/)  
[Iosevka Font](https://typeof.net/Iosevka/)  
[ShareTech Mono Font](https://fonts.google.com/specimen/Share+Tech+Mono)  
[Inter Font Family SF Pro Analogue](https://rsms.me/inter/)  
[Berkeley Mono](https://berkeleygraphics.com/typefaces/berkeley-mono/)  
[Apple Fonts (Including the infamous SF Pro Display/Text series)](https://developer.apple.com/fonts/)  
[Nerd Fonts](https://github.com/ryanoasis/nerd-fonts)  
[Font Awesome](https://fontawesome.com/versions)  
[Google Fonts](https://fonts.google.com)

## Podcasts
[OffTheHook](https://www.2600.com/offthehook/)  
[Hacker and the Fed](https://podcasts.apple.com/us/podcast/hacker-and-the-fed/id1649541362)  
[Darknet Diaries](https://podcasts.apple.com/us/podcast/darknet-diaries/id1296350485)  
[Malicious Life](https://podcasts.apple.com/us/podcast/malicious-life/id1252417787?i=1000590618127)  
[Cybrary Intruder Alert](https://podcasts.apple.com/us/podcast/intruder-alert/id1682558681)

## Networking
[Should I Enable ICMP](http://shouldiblockicmp.com)  
[Javascript CIDR calculator](http://pratik-github.github.io/subnet-cidr-adviser/)  
[Another Javascript subnet calculator](https://salieri.github.io/IPSubnetCalculator/)  
[My GitHub networking-related stars](https://github.com/stars/paigeadelethompson/lists/networking)  
[Using IPv6 Unique Local Addresses for private connectivity](https://cloud.google.com/blog/products/networking/using-ipv6-unique-local-addresses-or-ula-in-google-cloud)  
[How to drop 10 million packets per second](https://blog.cloudflare.com/how-to-drop-10-million-packets/)  
[IPv6 suffix-based addressing using tokens](https://wiki.gentoo.org/wiki/IPv6_Static_Addresses_using_Tokens)  
[Locator/Identifier Separation Protocol](https://en.wikipedia.org/wiki/Locator/Identifier_Separation_Protocol)  
[RFC 9229 - IPv4 Routes with an IPv6 Next Hop in the Babel Routing Protocol](https://datatracker.ietf.org/doc/html/rfc9229)  
[BCP38/RFC2827: Network Ingress Filtering: Defeating Denial of Service Attacks which employ IP Source Address Spoofing](http://www.bcp38.info/index.php/Main_Page)

## Linux
[Seccomp BPF (SECure COMPuting with filters)](https://www.kernel.org/doc/html/v4.18/userspace-api/seccomp_filter.html)  
[Containerized Android](https://waydro.id)  
[NFTables Wiki](https://wiki.nftables.org/wiki-nftables/index.php/Main_Page)  
[RedHat intro to IMA/EVM](https://www.redhat.com/en/blog/how-use-linux-kernels-integrity-measurement-architecture)  
[Redhat intro to UEFI / Secureboot](https://access.redhat.com/articles/5254641)  
[Linux system keyring; trusted and encrypted keys](https://www.kernel.org/doc/html/v5.0/security/keys/trusted-encrypted.html)  
[systemd-boot - alternative to GRUB](https://blog.bofh.it/debian/id_465)

## Windows
[WDAC Windows Defender Application Control / AppLocker](https://learn.microsoft.com/en-us/hololens/windows-defender-application-control-wdac)

## Cloud 
[Asecurecloud - Build secure cloud environments in minutes with dynamically generated infrastructure-as-code](https://asecure.cloud)  
[aiven.io - Data management platform](https://aiven.io)

### AWS
[Nitro Enclaves](https://aws.amazon.com/ec2/nitro/nitro-enclaves/)  
[Cloudformation Designer](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/working-with-templates-cfn-designer.html)  
[My GitHub starred AWS-related repositories](https://github.com/stars/paigeadelethompson/lists/aws)

## Security 
[NSA, CISA release Kubernetes Hardening Guidance](https://www.nsa.gov/Press-Room/News-Highlights/Article/Article/2716980/nsa-cisa-release-kubernetes-hardening-guidance/)

## Radio
[Groove Salad SomaFM](https://somafm.com/groovesalad/)

## Audio Production 
[Adlib Tracker II](http://www.adlibtracker.net)  
[Mixxx](https://mixxx.org)

## Psychology 
[You don't have to engage with people on the Internet](https://xeiaso.net/blog/lesson-online-feedback)

## Employment
[Interview Prep - Google Tech Dev Guide](https://techdevguide.withgoogle.com/paths/interview/)  
[Paige Thompson's Resume - Hugo/TOML based resume with a format based loosely on Awesome-CV (PDF & HTML, also works with Github Pages)](https://github.com/paigeadelethompson/resume/tree/main)

## Legal

### CFAA
[United States v. Paige Thompson (22-30168)](https://www.courtlistener.com/docket/66664696/united-states-v-paige-thompson/)  
[United States v. Thompson (2:19-cr-00159)](https://www.courtlistener.com/docket/16134895/united-states-v-thompson/)  
[United States v. Thompson (2:19-mj-00344)](https://www.courtlistener.com/docket/15983291/united-states-v-thompson/)  
[How to Get Sued Under the Computer Fraud and Abuse Act (CFAA): Spotting Trends in 30 Years of CFAA Data](https://mcca.com/wp-content/uploads/2017/06/How-to-Get-Sued-Under-the-CFAA-Presentation.pdf)  
[Department of Justice Announces New Policy for Charging Cases under the Computer Fraud and Abuse Act](https://www.justice.gov/opa/pr/department-justice-announces-new-policy-charging-cases-under-computer-fraud-and-abuse-act)  
[SCOTUS Limits Reach of Computer Fraud and Abuse Act: Nefarious Reasons Are Not Enough for Criminal Liability](https://www.dwt.com/blogs/media-law-monitor/2021/10/scotus-cfaa-decision)

### CFAA (Other cases)
[SUPREME COURT OF THE UNITED STATES Syllabus VAN BUREN v. UNITED STATES](https://www.supremecourt.gov/opinions/20pdf/19-783_k53l.pdf)  
[United States v. Kottmann (2:21-cr-00048)](https://www.courtlistener.com/docket/59745630/united-states-v-kottmann)  
[Suspension of US v. Till Kottmann case (pending extradition)](https://en.wikipedia.org/wiki/Maia_arson_crimew#Possibility_of_extradition_or_trial_in_Switzerland)  
[United States v. Auernheimer (5:11-mj-05003)](https://www.courtlistener.com/docket/4128845/united-states-v-auernheimer/)  
[United States v. AUERNHEIMER (2:11-cr-00470)](https://www.courtlistener.com/docket/4684967/united-states-v-auernheimer/)  
[Opinion - United States v. Auernheimer](https://casetext.com/case/united-states-v-auernheimer-3)  
[United States v. Swartz (1:11-cr-10260)](https://www.courtlistener.com/docket/4274338/united-states-v-swartz)  
[Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz)  
[United States v. Monsegur (1:11-cr-00666)](https://www.courtlistener.com/docket/4349399/united-states-v-monsegur)  
[United States v. Monsegur (2:11-cr-00766)](https://www.courtlistener.com/docket/4491788/united-states-v-monsegur/)  
[United States v. Mitnick (2:96-cr-00506)](https://www.courtlistener.com/docket/4143330/united-states-v-mitnick)

### CFAA (Less often heard of)
[Apple Inc. v. NSO Group Technologies Limited (3:21-cv-09078)](https://www.courtlistener.com/docket/61570971/apple-inc-v-nso-group-technologies-limited/)  
[Apple Resists Dismissal in CFAA Suit Against NSO Group](https://lawstreetmedia.com/news/tech/apple-resists-dismissal-in-cfaa-suit-against-nso-group/)  
[Massive data leak reveals Israeli NSO Group's spyware used to target activists, journalists, and political leaders globally](https://www.amnesty.org/en/latest/press-release/2021/07/the-pegasus-project/)  
[United States v. Nikulin (3:16-cr-00440)](https://www.courtlistener.com/docket/4572945/united-states-v-nikulin/?page=1)  
[Moscow 'Outraged' Over Prague's Extradition Of Alleged Russian Hacker (Nikulin) To US](https://www.rferl.org/a/nikulin-extradition-russia-outraged-czech-zeman-pelikan/29139957.html)  
[US govt offers $10 million bounty for info on Clop ransomware](https://www.bleepingcomputer.com/news/security/us-govt-offers-10-million-bounty-for-info-on-clop-ransomware/)  
[FBI.gov Cyber Most Wanted](https://www.fbi.gov/wanted/cyber)

## Retro
[LUNA-88K2](http://www.nk-home.net/~aoyama/luna88k/)

## Unsorted
[insecam live public camera streams](http://www.insecam.org/)  
[nexpo internet detective](https://nexpo.page)

## Crypto
[Trilema](http://trilema.com)
