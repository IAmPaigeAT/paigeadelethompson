+++
description = "A ttk::progressbar widget shows the status of a long-running operation.  They can operate in two modes: determinate mode shows the amount completed relative to the total amount of work to be done, and indeterminate mode provides an animated display..."
detected_package_version = "1.9"
title = "ttk::progressbar(n)"
operating_system_version = "15.3"
author = "None Specified"
manpage_name = "ttk::progressbar"
manpage_section = "n"
date = "Sun Feb 16 04:48:27 2025"
operating_system = "macos"
manpage_format = "troff"
keywords = ["ttk", "widget", "local", "variables", "mode", "nroff", "end"]
+++

ttk::progressbar n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::progressbar - Provide progress feedback

## SYNOPSIS

**ttk::progressbar** *pathName *?*options*?

---


## DESCRIPTION


A **ttk::progressbar** widget shows the status of a long-running
operation.  They can operate in two modes: *determinate* mode shows the
amount completed relative to the total amount of work to be done, and
*indeterminate* mode provides an animated display to let the user know
that something is happening.

### Standard ttk_widget

-class	-cursor	-takefocus
-style

See the standard options manual entry for details.


## WIDGET-SPECIFIC OPTIONS

One of **horizontal** or **vertical**.
Specifies the orientation of the progress bar.
Specifies the length of the long axis of the progress bar
(width if horizontal, height if vertical).
One of **determinate** or **indeterminate**.
A floating point number specifying the maximum **-value**.
Defaults to 100.
The current value of the progress bar.
In *determinate* mode, this represents the amount of work completed.
In *indeterminate* mode, it is interpreted modulo **-maximum**;
that is, the progress bar completes one
"cycle"when the **-value** increases by **-maximum**.
The name of a Tcl variable which is linked to the **-value**.
If specified, the **-value** of the progress bar is
automatically set to the value of the variable whenever
the latter is modified.
Read-only option.
The widget periodically increments the value of this option
whenever the **-value** is greater than 0 and,
in *determinate* mode, less than **-maximum**.
This option may be used by the current theme
to provide additional animation effects.

## WIDGET COMMAND



*pathName **cget** \fIoption*
Returns the current value of the specified *option*; see *ttk::widget(n)*.

*pathName **configure** ?*option*? ?\fIvalue option value ...*?
Modify or query widget options; see *ttk::widget(n)*.

*pathName **identify** \fIx y*
Returns the name of the element at position *x*, *y*.
See *ttk::widget(n)*.

*pathName **instate **statespec* ?*script*?
Test the widget state; see *ttk::widget(n)*.

*pathName **start** ?\fIinterval*?
Begin autoincrement mode:
schedules a recurring timer event that calls **step**
every *interval* milliseconds.
If omitted, *interval* defaults to 50 milliseconds (20 steps/second).

*pathName **state** ?\fIstateSpec*?
Modify or query the widget state; see *ttk::widget(n)*.

*pathName **step** ?\fIamount*?
Increments the **-value** by *amount*.
*amount* defaults to 1.0 if omitted.

\fIpathName **stop**
Stop autoincrement mode:
cancels any recurring timer event initiated by \fIpathName **start**.

## SEE ALSO

ttk::widget(n)
'\" Local Variables:
'\" mode: nroff
'\" End:
