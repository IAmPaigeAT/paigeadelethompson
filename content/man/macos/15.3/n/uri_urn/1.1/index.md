+++
date = "Sun Feb 16 04:48:27 2025"
manpage_name = "uri_urn"
manpage_format = "troff"
description = "This package provides two commands to quote and unquote the disallowed characters for url using the urn scheme, registers the scheme with the package uri, and provides internal helpers which will be automatically used by the commands uri::split and..."
title = "uri_urn(n)"
detected_package_version = "1.1"
operating_system_version = "15.3"
manpage_section = "n"
operating_system = "macos"
author = "None Specified"
+++

"uri_urn" n 1.1.2 uri "Tcl Uniform Resource Identifier Management"

---

## NAME

uri_urn - URI utilities, URN scheme

## SYNOPSIS

package require **Tcl  8.2**

package require **uri::urn  ?1.1.2?**

**uri::urn::quote** *url*

**uri::urn::unquote** *url*


---


## DESCRIPTION

This package provides two commands to quote and unquote the disallowed
characters for url using the *urn* scheme, registers the scheme
with the package **uri**, and provides internal helpers which
will be automatically used by the commands **uri::split** and
**uri::join** of package **uri** to handle urls using the
*urn* scheme.

## COMMANDS


**uri::urn::quote** *url*
This command quotes the characters disallowed by the *urn* scheme
(per RFC 2141 sec2.2) in the *url* and returns the modified url as
its result.

**uri::urn::unquote** *url*
This commands performs the reverse of **::uri::urn::quote**. It
takes an *urn* url, removes the quoting from all disallowed
characters, and returns the modified urls as its result.


## BUGS, IDEAS, FEEDBACK

This document, and the package it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *uri* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## KEYWORDS

rfc 2141, uri, url, urn

## CATEGORY

Networking
