+++
author = "None Specified"
date = "Sun Feb 16 04:48:30 2025"
detected_package_version = "1.9"
operating_system = "macos"
manpage_section = "n"
operating_system_version = "15.3"
manpage_name = "tk_setpalette"
title = "tk_setpalette(n)"
manpage_format = "troff"
description = "The tk_setPalette procedure changes the color scheme for Tk. It does this by modifying the colors of existing widgets and by changing the option database so that future widgets will use the new color scheme. If tk_setPalette is invoked with a singl..."
+++

tk_setPalette n 4.0 Tk "Tk Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

tk_setPalette, tk_bisque - Modify the Tk color palette

## SYNOPSIS

**tk_setPalette **background

**tk_setPalette **name value ?*name value ...*?

**tk_bisque**

---



## DESCRIPTION


The **tk_setPalette** procedure changes the color scheme for Tk.
It does this by modifying the colors of existing widgets and by changing
the option database so that future widgets will use the new color scheme.
If **tk_setPalette** is invoked with a single argument, the
argument is the name of a color to use as the normal background
color;  **tk_setPalette** will compute a complete color palette
from this background color.
Alternatively, the arguments to **tk_setPalette** may consist of any number
of *name*-*value* pairs, where the first argument of the pair
is the name of an option in the Tk option database and the second
argument is the new value to use for that option.  The following
database names are currently supported:

> .ta 4c 8c
**activeBackground**	**foreground**	**selectColor**
**activeForeground**	**highlightBackground**	**selectBackground**
**background**	**highlightColor**	**selectForeground**
**disabledForeground**	**insertBackground**	**troughColor**


**tk_setPalette** tries to compute reasonable defaults for any
options that you do not specify.  You can specify options other
than the above ones and Tk will change those options on widgets as
well.  This feature may be useful if you are using custom widgets with
additional color options.

Once it has computed the new value to use for each of the color options,
**tk_setPalette** scans the widget hierarchy to modify the options
of all existing widgets.  For each widget, it checks to see if any
of the above options is defined for the widget.  If so, and if the
option's current value is the default, then the value is changed;  if
the option has a value other than the default, **tk_setPalette**
will not change it.  The default for an option is the one provided by
the widget (**[lindex [$w configure $option] 3]**) unless
**tk_setPalette** has been run previously, in which case it is the
value specified in the previous invocation of **tk_setPalette**.

After modifying all the widgets in the application, **tk_setPalette**
adds options to the option database to change the defaults for
widgets created in the future.  The new options are added at
priority **widgetDefault**, so they will be overridden by options
from the .Xdefaults file or options specified on the command-line
that creates a widget.

The procedure **tk_bisque** is provided for backward compatibility:
it restores the application's colors to the light brown
("bisque")color scheme used in Tk 3.6 and earlier versions.


## KEYWORDS

bisque, color, palette
