+++
date = "Sun Feb 16 04:48:29 2025"
operating_system = "macos"
manpage_name = "source"
manpage_section = "n"
manpage_format = "troff"
operating_system_version = "15.3"
author = "None Specified"
detected_package_version = "1.18"
keywords = ["file", "cd", "encoding", "info", "keywords", "script"]
title = "source(n)"
description = "This command takes the contents of the specified file or resource and passes it to the Tcl interpreter as a text script.  The return value from source is the return value of the last command executed in the script.  If an error occurs in evaluating..."
+++

source n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

source - Evaluate a file or resource as a Tcl script

## SYNOPSIS

**source **fileName


>  (Added in version 8.5)
**source** **-encoding **encodingName fileName



---


## DESCRIPTION


This command takes the contents of the specified file or resource
and passes it to the Tcl interpreter as a text script.  The return
value from **source** is the return value of the last command
executed in the script.  If an error occurs in evaluating the contents
of the script then the **source** command will return that error.
If a **return** command is invoked from within the script then the
remainder of the file will be skipped and the **source** command
will return normally with the result from the **return** command.

The end-of-file character for files is
"\e32"(^Z) for all platforms.
The source command will read files up to this character.  This
restriction does not exist for the **read** or **gets** commands,
allowing for files containing code and data segments (scripted documents).
If you require a
"^Z"in code for string comparison, you can use
"\e032"or
"\eu001a" ,which will be safely substituted by the Tcl interpreter into
"^Z" .

>  (Added in version 8.5)
The **-encoding** option is used to specify the encoding of
the data stored in *fileName*.  When the **-encoding** option
is omitted, the system encoding is assumed.



## EXAMPLE

Run the script in the file **foo.tcl** and then the script in the
file **bar.tcl**:

```
**source** foo.tcl
**source** bar.tcl
```

Alternatively:

```
foreach scriptFile \{foo.tcl bar.tcl\} \{
   **source** $scriptFile
\}
```


## SEE ALSO

file(n), cd(n), encoding(n), info(n)

## KEYWORDS

file, script
