+++
description = "The math package provides utility math functions. Besides a set of basic commands, available via the package math, there are more specialised packages: math::bigfloat - Arbitrary-precision floating-point arithmetic math::bignum - Arbitrary-precis..."
author = "None Specified"
operating_system = "macos"
manpage_name = "math"
detected_package_version = "1.1"
manpage_section = "n"
title = "math(n)"
manpage_format = "troff"
date = "Sun Feb 16 04:48:30 2025"
operating_system_version = "15.3"
+++

"math" n 1.2.5 math "Tcl Math Library"

---

## NAME

math - Tcl Math Library

## SYNOPSIS

package require **Tcl  8.2**

package require **math  ?1.2.5?**

**::math::cov** *value* *value* ?*value ...*?

**::math::integrate** *list of xy value pairs*

**::math::fibonacci** *n*

**::math::max** *value* ?*value ...*?

**::math::mean** *value* ?*value ...*?

**::math::min** *value* ?*value ...*?

**::math::product** *value* ?*value ...*?

**::math::random** ?*value1*? ?*value2*?

**::math::sigma** *value* *value* ?*value ...*?

**::math::stats** *value* *value* ?*value ...*?

**::math::sum** *value* ?*value ...*?


---


## DESCRIPTION


The **math** package provides utility math functions.

Besides a set of basic commands, available via the package *math*,
there are more specialised packages:
\(bu
**math::bigfloat** - Arbitrary-precision floating-point
arithmetic
\(bu
**math::bignum** - Arbitrary-precision integer arithmetic
\(bu
**math::calculus::romberg** - Robust integration methods for
functions of one variable, using Romberg integration
\(bu
**math::calculus** - Integration of functions, solving ordinary
differential equations
\(bu
**math::combinatorics** - Procedures for various combinatorial
functions (for instance the Gamma function and "k out of n")
\(bu
**math::complexnumbers** - Complex number arithmetic
\(bu
**math::constants** - A set of well-known mathematical
constants, such as Pi, E, and the golden ratio
\(bu
**math::fourier** - Discrete Fourier transforms
\(bu
**math::fuzzy** - Fuzzy comparisons of floating-point numbers
\(bu
**math::geometry** - 2D geometrical computations
\(bu
**math::interpolate** - Various interpolation methods
\(bu
**math::linearalgebra** - Linear algebra package
\(bu
**math::optimize** - Optimization methods
\(bu
**math::polynomials** - Polynomial arithmetic (includes families
of classical polynomials)
\(bu
**math::rationalfunctions** - Arithmetic of rational functions
\(bu
**math::roman** - Manipulation (including arithmetic) of Roman
numerals
\(bu
**math::special** - Approximations of special functions from
mathematical physics
\(bu
**math::statistics** - Statistical operations and tests


## BASIC COMMANDS


**::math::cov** *value* *value* ?*value ...*?
Return the coefficient of variation expressed as percent of two or
more numeric values.

**::math::integrate** *list of xy value pairs*
Return the area under a "curve" defined by a set of x,y pairs and the
error bound as a list.

**::math::fibonacci** *n*
Return the *n*'th Fibonacci number.

**::math::max** *value* ?*value ...*?
Return the maximum of one or more numeric values.

**::math::mean** *value* ?*value ...*?
Return the mean, or "average" of one or more numeric values.

**::math::min** *value* ?*value ...*?
Return the minimum of one or more numeric values.

**::math::product** *value* ?*value ...*?
Return the product of one or more numeric values.

**::math::random** ?*value1*? ?*value2*?
Return a random number.  If no arguments are given, the number is a
floating point value between 0 and 1.  If one argument is given, the
number is an integer value between 0 and *value1*.  If two
arguments are given, the number is an integer value between
*value1* and *value2*.

**::math::sigma** *value* *value* ?*value ...*?
Return the population standard deviation of two or more numeric
values.

**::math::stats** *value* *value* ?*value ...*?
Return the mean, standard deviation, and coefficient of variation (as
percent) as a list.

**::math::sum** *value* ?*value ...*?
Return the sum of one or more numeric values.


## BUGS, IDEAS, FEEDBACK

This document, and the package it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *math* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## KEYWORDS

math, statistics

## CATEGORY

Mathematics
