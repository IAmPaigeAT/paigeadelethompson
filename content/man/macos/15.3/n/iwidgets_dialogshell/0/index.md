+++
detected_package_version = "0"
description = "The iwidgets::dialogshell command creates a dialog shell which is a top  level widget composed of a button box, separator, and child site area. The class also has methods to control button construction."
manpage_section = "n"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_name = "iwidgets_dialogshell"
date = "Sun Feb 16 04:48:28 2025"
title = "iwidgets_dialogshell(n)"
operating_system = "macos"
author = "None Specified"
+++

iwidgets::dialogshell iwid

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

iwidgets::dialogshell - Create and manipulate a dialog shell widget

## SYNOPSIS

**iwidgets::dialogshell** *pathName *?*options*?

## INHERITANCE

itk::Toplevel <- iwidgets::Shell <- iwidgets::Dialogshell

## STANDARD OPTIONS



```
.ta 4c 8c 12c
background	cursor	foreground
```


See the "options" manual entry for details on the standard options.

## INHERITED OPTIONS



```
.ta 4c 8c 12c
height	master	modality	width
```


See the "shell" manual entry for details on the above inherited options.


```
.ta 4c 8c 12c
title 
```


See the "Toplevel" manual entry for details on the above inherited options.


## WIDGET-SPECIFIC OPTIONS



```
Name:	buttonBoxPadX
Class:	Pad
Command-Line Switch:	-buttonboxpadx
```

.IP
Specifies a non-negative padding distance to leave between the button group and
the outer edge of the button box in the x direction.  The value may be
given in any of the forms accpetable to **Tk_GetPixels**.  The default
is 5 pixels.


```
Name:	buttonBoxPadY
Class:	Pad
Command-Line Switch:	-buttonboxpady
```

.IP
Specifies a non-negative padding distance to leave between the button group and
the outer edge of the button box in the y direction.  The value may be
given in any of the forms accpetable to **Tk_GetPixels**.  The default
is 5 pixels.


```
Name:	buttonBoxPos
Class:	Position
Command-Line Switch:	-buttonboxpos
```

.IP
Attaches buttons to the given side of the dialog: **n**, **s**,
**e** or **w**.  The default is s.


```
Name:	padX
Class:	Pad
Command-Line Switch:	-padx
```

.IP
Specifies a padding distance for the childsite in the X-direction in
any of the forms acceptable to **Tk_GetPixels**.  The default is 10.


```
Name:	padY
Class:	Pad
Command-Line Switch:	-pady
```

.IP
Specifies a padding distance for the childsite in the Y-direction in
any of the forms acceptable to **Tk_GetPixels**.  The default is 10.


```
Name:	separator
Class:	Separator
Command-Line Switch:	-separator
```

.IP
Specifies whether a line is drawn to separate the
buttons from the dialog box contents in any of the forms
acceptable to **Tcl_GetBoolean**.  The default is true.


```
Name:	thickness
Class:	Thickness
Command-Line Switch:	-thickness
```

.IP
Specifies the thickness of the separator in any of the forms acceptable
to **Tk_GetPixels**.  The default is 3 pixels.

---



## DESCRIPTION


The **iwidgets::dialogshell** command creates a dialog shell which is a top
level widget composed of a button box, separator, and child site area.
The class also has methods to control button construction.


## METHODS


The **iwidgets::dialogshell** command create a new Tcl command whose
name is *pathName*.  This command may be used to invoke various
operations on the widget.  It has the following general form:

> *pathName option *?*arg arg ...*?


*Option* and the *arg*s
determine the exact behavior of the command.  The following
commands are possible for dialogshell widgets:

## INHERITED METHODS



```
.ta 4c 8c 12c
activate	center	deactivate
```


See the "shell" manual entry for details on the above inherited methods.

## ASSOCIATED METHODS



```
.ta 4c 8c 12c
add	buttonconfigure	default	delete
hide	index	insert	invoke
show
```


See the "buttonbox" manual entry for details on the associated methods.

## WIDGET-SPECIFIC METHODS


*pathName **cget** \fIoption*
Returns the current value of the configuration option given
by *option*.
*Option* may have any of the values accepted by the **iwidgets::dialogshell**
command.

\fIpathName **childsite**
Returns the pathname of the child site widget.

*pathName* **configure** ?*option*? ?*value option value ...*?
Query or modify the configuration options of the widget.
If no *option* is specified, returns a list describing all of
the available options for *pathName* (see **Tk_ConfigureInfo** for
information on the format of this list).  If *option* is specified
with no *value*, then the command returns a list describing the
one named option (this list will be identical to the corresponding
sublist of the value returned if no *option* is specified).  If
one or more *option-value* pairs are specified, then the command
modifies the given widget option(s) to have the given value(s);  in
this case the command returns an empty string.
*Option* may have any of the values accepted by the **iwidgets::dialogshell**
command.


## COMPONENTS



```
Name:	dschildsite
Class:	frame
```

.IP
The dschildsite component is the user child site for the dialog shell.  See
the "frame" widget manual entry for details on the dschildsite component item.


```
Name:	separator
Class:	frame
```

.IP
The separator component devides the area between the user child site and
the button box.  See the "frame" widget manual entry for details on the
separator component item.


```
Name:	bbox
Class:	ButtonBox
```

.IP
The bbox component is the button box containing the buttons for the dialog
shell.  See the "ButtonBox" widget manual entry for details on the
bbox component item.



## EXAMPLE


>  package require Iwidgets 4.0
 iwidgets::dialogshell .ds -modality none

 .ds add OK -text "OK"
 .ds add Cancel -text "Cancel"
 .ds default OK

 .ds activate



## AUTHOR

Mark L. Ulferts

## KEYWORDS

dialogshell, dialog, shell, widget
