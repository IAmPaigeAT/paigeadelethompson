+++
date = "Sun Feb 16 04:48:29 2025"
author = "None Specified"
operating_system = "macos"
description = "This package provides a calendar megawidget (snidget). widget::calendar pathname ?options? -command A script to evaluate when a date was selected. -dateformat The format of the date that is returned. Default: %m/%d/%Y. -firstday Set first day t..."
operating_system_version = "15.3"
title = "widget_calendar(n)"
manpage_name = "widget_calendar"
manpage_format = "troff"
manpage_section = "n"
detected_package_version = "1.1"
+++

"widget_calendar" n 0.93 widget "widget::calendar Megawidget"

---

## NAME

widget_calendar - widget::calendar Megawidget

## SYNOPSIS

package require **Tcl  8.4**

package require **Tk  8.4**

package require **widget  ?3.0?**

**widget::calendar** *pathname* ?options?


---


## DESCRIPTION

This package provides a calendar megawidget (snidget).


**widget::calendar** *pathname* ?options?


## WIDGET OPTIONS



**-command**
A script to evaluate when a date was selected.

**-dateformat**
The format of the date that is returned. Default: %m/%d/%Y.

**-firstday**
Set first day the week, Either sunday or monday. It defaults to monday.

**-font**
Select the font used in the widget. It defaults to Helvetica 9.

**-highlightcolor**
Selects the background color for the day that has been selected. Default: #FFCC00

**-language**
Specify language of the calendar contents. The language is specified
by abbreviations of the languge, for example: en - english, de -
german ...
It defaults to en.

Supported languages: en, de, fr, it, es, pt, ru, sv, zh, fi

**-shadecolor**
Selects the color of the parts that have a shaded background. Default: #888888

**-showpast**
Specify if the past shall be shown. It is a boolean value and defaults
to 1.

**-textvariable**
Specifies the name of a variable whose value is linked to the entry widget's contents.
Whenever the variable changes value, the widget's contents are updated, and
vice versa.


## WIDGET COMMAND

*pathname* **get** ?*what*?

Returns a part of the selected date or 'all'. The argument *what* selects
the part. Valid values for *what* are: day, month, year and all.
\'all' is the default and returns the complete date in the format given
with -dateformat.

## DEFAULT BINDINGS

On creation of the calendar widget the following bindings are installed.
\(bu
Up - Move to week before current date
\(bu
Down - Move to week after current date
\(bu
Left - Move to day before current date
\(bu
Right - Move to day after current date
\(bu
Control-Left - Move to month before current date
\(bu
Control-Right - Move to month after current date
\(bu
Control-Up - Move to year before current date
\(bu
Control-Down - Move to year after current date


## EXAMPLE


```

    package require widget::calendar ; # or widget::all
    set t [widget::calendar .t]
    pack $t -fill x -expand 1

```


## KEYWORDS

megawidget, snit, widget
