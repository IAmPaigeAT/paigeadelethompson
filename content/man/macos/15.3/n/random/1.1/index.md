+++
description = "random creates a random channel which absorbs everything written into it and uses it as a seed for a random number generator. This generator is used to create a random sequence of bytes when reading from the channel. It is not possible to seek the..."
title = "random(n)"
author = "None Specified"
manpage_name = "random"
manpage_section = "n"
manpage_format = "troff"
detected_package_version = "1.1"
date = "Sun Feb 16 04:48:31 2025"
operating_system = "macos"
keywords = ["fifo", "fifo2", "memchan", "null", "zero", "keywords", "channel", "i", "o", "in-memory", "random", "copyright", "c", "2004", "pat", "thoyts", "patthoyts", "users", "sourceforge", "net"]
operating_system_version = "15.3"
+++

"random" n 2.2  "Memory channels"

---

## NAME

random - Create and manipulate randomizer channels

## SYNOPSIS

package require **Tcl **

package require **memchan **

**random**


---


## DESCRIPTION


**random**
creates a random channel which absorbs everything written into it and
uses it as a seed for a random number generator. This generator is
used to create a random sequence of bytes when reading from the
channel. It is not possible to seek the channel.


## OPTIONS

Memory channels created by **random** provide one additional option to
set or query.

*-delay ?milliseconds?*
A **random** channel is always writable and readable. This means
that all **fileevent**-handlers will fire continuously.  To
avoid starvation of other event sources the events raised by this
channel type have a configurable delay. This option is set in
milliseconds and defaults to 5.


## SEE ALSO

fifo, fifo2, memchan, null, zero

## KEYWORDS

channel, i/o, in-memory channel, random

## COPYRIGHT


```
Copyright (c) 2004 Pat Thoyts <patthoyts@users.sourceforge.net>

```

