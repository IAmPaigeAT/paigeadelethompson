+++
operating_system = "macos"
operating_system_version = "15.3"
manpage_format = "troff"
author = "None Specified"
description = "A ttk::label widget displays a textual label and/or image. The label may be linked to a Tcl variable  to automatically change the displayed text. -class-compound-cursor -image-style-takefocus -text-textvariable-underline -width Specifies ..."
keywords = ["ttk", "widget", "label", "local", "variables", "mode", "nroff", "end"]
detected_package_version = "1.9"
title = "ttk::label(n)"
manpage_name = "ttk::label"
manpage_section = "n"
date = "Sun Feb 16 04:48:30 2025"
+++

ttk::label n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::label - Display a text string and/or image

## SYNOPSIS

**ttk::label** *pathName *?*options*?

---


## DESCRIPTION


A **ttk::label** widget displays a textual label and/or image.
The label may be linked to a Tcl variable
to automatically change the displayed text.

### Standard ttk_widget

-class	-compound	-cursor
-image	-style	-takefocus
-text	-textvariable	-underline
-width

See the standard options manual entry for details.


## WIDGET-SPECIFIC OPTIONS

Specifies how the information in the widget is positioned
relative to the inner margins.  Legal values are
**n**, **ne**, **e**, **se**,
**s**, **sw**, **w**, **nw**, and **center**.
See also **-justify**.
The widget's background color.
If unspecified, the theme default is used.
Font to use for label text.
The widget's foreground color.
If unspecified, the theme default is used.
If there are multiple lines of text, specifies how
the lines are laid out relative to one another.
One of **left**, **center**, or **right**.
See also **-anchor**.
Specifies the amount of extra space to allocate for the widget.
The padding is a list of up to four length specifications
*left top right bottom*.
If fewer than four elements are specified,
*bottom* defaults to *top*,
*right* defaults to *left*, and
*top* defaults to *left*.
Specifies the 3-D effect desired for the widget border.
Valid values are
**flat**, **groove**, **raised**, **ridge**, **solid**,
and **sunken**.
Specifies a text string to be displayed inside the widget
(unless overridden by **-textvariable**).
Specifies the maximum line length (in pixels).
If this option is less than or equal to zero,
then automatic wrapping is not performed; otherwise
the text is split into lines such that no line is longer
than the specified value.

## WIDGET COMMAND


Supports the standard widget commands
**configure**, **cget**, **identify**, **instate**, and **state**;
see *ttk::widget(n)*.

## SEE ALSO

ttk::widget(n), label(n)
'\" Local Variables:
'\" mode: nroff
'\" End:
