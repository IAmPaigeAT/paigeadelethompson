+++
manpage_name = "tie"
description = "The packages listed as requirements for this document are internal packages providing the standard data sources of package tie, as described in section STANDARD DATA SOURCE TYPES of ties documentation. They are automatically loaded and registered..."
author = "None Specified"
date = "Sun Feb 16 04:48:28 2025"
operating_system_version = "15.3"
operating_system = "macos"
detected_package_version = "1.1"
manpage_format = "troff"
title = "tie(n)"
manpage_section = "n"
+++

"tie" n 1.1 tie "Tcl Data Structures"

---

## NAME

tie - Array persistence, standard data sources

## SYNOPSIS

package require **Tcl  8.4**

package require **tie::std::log  ?1.1?**

package require **tie::std::array  ?1.1?**

package require **tie::std::rarray  ?1.1?**

package require **tie::std::file  ?1.1?**

package require **tie::std::growfile  ?1.1?**

package require **tie::std::dsource  ?1.1?**


---


## DESCRIPTION

The packages listed as requirements for this document are internal
packages providing the standard data sources of package **tie**,
as described in section *STANDARD DATA SOURCE TYPES* of
**tie**'s documentation.

They are automatically loaded and registered by **tie** when it
itself is requested, and as such there is no need to request them on
their own, although it is possible to do so.

## BUGS, IDEAS, FEEDBACK

This document, and the packages it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *tie* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## KEYWORDS

array, database, file, metakit, persistence, tie, untie

## CATEGORY

Programming tools

## COPYRIGHT


```
Copyright (c) 2008 Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

