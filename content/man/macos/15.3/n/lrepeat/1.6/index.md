+++
manpage_section = "n"
author = "None Specified"
title = "lrepeat(n)"
manpage_name = "lrepeat"
operating_system = "macos"
date = "Sun Feb 16 04:48:29 2025"
description = "The lrepeat command creates a list of size  elements by repeating number times the sequence of elements element1 element2 ....  number must be a positive integer, elementn can be any Tcl value.  Note that lrepeat 1 arg ... is identical to list arg..."
manpage_format = "troff"
detected_package_version = "1.6"
keywords = ["list", "lappend", "linsert", "llength", "lset", "keywords", "element", "index"]
operating_system_version = "15.3"
+++

lrepeat n 8.5 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

lrepeat - Build a list by repeating elements

## SYNOPSIS

**lrepeat **number element1 ?*element2 element3 ...*?

---


## DESCRIPTION


The **lrepeat** command creates a list of size \fInumber * number of
elements by repeating *number* times the sequence of elements
*element1 element2 ...*.  *number* must be a positive integer,
*elementn* can be any Tcl value.  Note that **lrepeat 1 arg ...**
is identical to **list arg ...**, though the *arg* is required
with **lrepeat**.

## EXAMPLES


```
**lrepeat** 3 a
      *\(-> a a a*
**lrepeat** 3 [**lrepeat** 3 0]
      *\(-> \{0 0 0\} \{0 0 0\} \{0 0 0\}*
**lrepeat** 3 a b c
      *\(-> a b c a b c a b c*
**lrepeat** 3 [**lrepeat** 2 a] b c
      *\(-> \{a a\} b c \{a a\} b c \{a a\} b c*
```


## SEE ALSO

list(n), lappend(n), linsert(n), llength(n), lset(n)


## KEYWORDS

element, index, list
