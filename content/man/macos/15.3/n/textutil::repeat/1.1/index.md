+++
manpage_format = "troff"
operating_system_version = "15.3"
author = "None Specified"
keywords = ["regexp", "split", "string", "keywords", "blanks", "repetition", "category", "text", "processing"]
detected_package_version = "1.1"
description = "The package textutil::repeat provides commands to generate long strings by repeating a shorter string many times. The complete set of procedures is described below. ::textutil::repeat::strRepeat text num This command returns a string containing th..."
manpage_name = "textutil::repeat"
manpage_section = "n"
operating_system = "macos"
date = "Sun Feb 16 04:48:28 2025"
title = "textutil::repeat(n)"
+++

"textutil::repeat" n 0.7.1 textutil "Text and string utilities, macro processing"

---

## NAME

textutil::repeat - Procedures to repeat strings.

## SYNOPSIS

package require **Tcl  8.2**

package require **textutil::repeat  ?0.7?**

**::textutil::repeat::strRepeat** *text* *num*

**::textutil::repeat::blank** *num*


---


## DESCRIPTION

The package **textutil::repeat** provides commands to generate
long strings by repeating a shorter string many times.

The complete set of procedures is described below.

**::textutil::repeat::strRepeat** *text* *num*
This command returns a string containing the *text* repeated
*num* times. The repetitions are joined without characters between
them. A value of *num* <= 0 causes the command to return an empty
string.

*Note*: If the Tcl core the package is loaded in provides the
command **string repeat** then this command will be implemented in
its terms, for maximum possible speed. Otherwise a fast implementation
in Tcl will be used.

**::textutil::repeat::blank** *num*
A convenience command. Returns a string of *num* spaces.


## BUGS, IDEAS, FEEDBACK

This document, and the package it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *textutil* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## SEE ALSO

regexp(n), split(n), string(n)

## KEYWORDS

blanks, repetition, string

## CATEGORY

Text processing
