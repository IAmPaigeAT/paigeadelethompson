+++
date = "Sun Feb 16 04:48:28 2025"
title = "page_util_norm_lemon(n)"
author = "None Specified"
manpage_format = "troff"
detected_package_version = "1.1"
manpage_name = "page_util_norm_lemon"
operating_system = "macos"
manpage_section = "n"
operating_system_version = "15.3"
description = "This package provides a single utility command which takes an AST for a lemon  grammar and normalizes it in various ways. The result is called a Normalized Lemon Grammar Tree. Note that this package can only be used from within a plugin managed by..."
+++

"page_util_norm_lemon" n 1.0 page "Parser generator tools"

---

## NAME

page_util_norm_lemon - page AST normalization, LEMON

## SYNOPSIS

package require **page::util::norm_lemon  ?0.1?**

package require **snit **

**::page::util::norm::lemon** *tree*


---


## DESCRIPTION


This package provides a single utility command which takes an AST for a
lemon  grammar and normalizes it in various ways. The result
is called a *Normalized Lemon Grammar Tree*.

*Note* that this package can only be used from within a plugin
managed by the package **page::pluginmgr**.

## API


**::page::util::norm::lemon** *tree*
This command assumes the *tree* object contains for a lemon
grammar. It normalizes this tree in place. The result is called a
*Normalized Lemon Grammar Tree*.

The exact operations performed are left undocumented for the moment.


## BUGS, IDEAS, FEEDBACK

This document, will undoubtedly contain bugs and other problems.
Please report such in the category *page* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have.

## KEYWORDS

graph walking, lemon, normalization, page, parser generator, text processing, tree walking

## CATEGORY

Page Parser Generator

## COPYRIGHT


```
Copyright (c) 2007 Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

