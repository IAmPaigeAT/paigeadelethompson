+++
description = "A ttk::menubutton widget displays a textual label and/or image, and displays a menu when pressed. -class-compound-cursor -image-state-style -takefocus-text-textvariable -underline-width Specifies where the menu is to be popped up relative..."
date = "Sun Feb 16 04:48:31 2025"
keywords = ["ttk", "widget", "menu", "menubutton", "keywords", "button", "local", "variables", "mode", "nroff", "end"]
author = "None Specified"
manpage_name = "ttk::menubutton"
manpage_format = "troff"
operating_system_version = "15.3"
operating_system = "macos"
detected_package_version = "1.9"
manpage_section = "n"
title = "ttk::menubutton(n)"
+++

ttk::menubutton n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::menubutton - Widget that pops down a menu when pressed

## SYNOPSIS

**ttk::menubutton** *pathName *?*options*?

---


## DESCRIPTION


A **ttk::menubutton** widget displays a textual label and/or image,
and displays a menu when pressed.

### Standard ttk_widget

-class	-compound	-cursor
-image	-state	-style
-takefocus	-text	-textvariable
-underline	-width

See the standard options manual entry for details.


## WIDGET-SPECIFIC OPTIONS

Specifies where the menu is to be popped up relative
to the menubutton.
One of: **above**, **below**, **left**, **right**,
or **flush**.  The default is **below**.
**flush** pops the menu up directly over the menubutton.
Specifies the path name of the menu associated with the menubutton.
To be on the safe side, the menu ought to be a direct child of the
menubutton.

## WIDGET COMMAND


Menubutton widgets support the standard
**cget**, **configure**, **identify**, **instate**, and **state**
methods.  No other widget methods are used.

## STANDARD STYLES


**Ttk::menubutton** widgets support the **Toolbutton** style in all
standard themes, which is useful for creating widgets for toolbars.

## SEE ALSO

ttk::widget(n), menu(n), menubutton(n)

## KEYWORDS

widget, button, menu
'\" Local Variables:
'\" mode: nroff
'\" End:
