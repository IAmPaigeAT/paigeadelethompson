+++
manpage_section = "n"
manpage_format = "troff"
description = "This command returns a list comprised of all the args, or an empty string if no args are specified. Braces and backslashes get added as necessary, so that the lindex command may be used on the result to re-extract the original arguments, and also ..."
title = "list(n)"
operating_system_version = "15.3"
manpage_name = "list"
date = "Sun Feb 16 04:48:31 2025"
detected_package_version = "1.11"
operating_system = "macos"
author = "None Specified"
keywords = ["lappend", "lindex", "linsert", "llength", "lrange", "8", "5", "lrepeat", "lreplace", "lsearch", "lset", "lsort", "keywords", "element", "list"]
+++

list n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

list - Create a list

## SYNOPSIS

**list **?*arg arg ...*?

---



## DESCRIPTION


This command returns a list comprised of all the *arg*s,
or an empty string if no *arg*s are specified.
Braces and backslashes get added as necessary, so that the **lindex** command
may be used on the result to re-extract the original arguments, and also
so that **eval** may be used to execute the resulting list, with
*arg1* comprising the command's name and the other *arg*s comprising
its arguments.  **List** produces slightly different results than
**concat**:  **concat** removes one level of grouping before forming
the list, while **list** works directly from the original arguments.

## EXAMPLE

The command

```
**list** a b "c d e  " "  f \{g h\}"
```

will return

```
**a b \{c d e  \} \{  f \{g h\}\}**
```

while **concat** with the same arguments will return

```
**a b c d e f \{g h\}**
```



## SEE ALSO

lappend(n), lindex(n), linsert(n), llength(n), lrange(n),

>  (Added in version 8.5)
lrepeat(n),


lreplace(n), lsearch(n), lset(n), lsort(n)


## KEYWORDS

element, list
