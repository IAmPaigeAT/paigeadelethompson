+++
detected_package_version = "1.14"
title = "linsert(n)"
manpage_format = "troff"
description = "This command produces a new list from list by inserting all of the element arguments just before the indexth element of list.  Each element argument will become a separate element of the new list.  If index is less than or equal to zero, then the ..."
author = "None Specified"
manpage_name = "linsert"
date = "Sun Feb 16 04:48:31 2025"
manpage_section = "n"
operating_system_version = "15.3"
operating_system = "macos"
keywords = ["list", "lappend", "lindex", "llength", "lsearch", "lset", "lsort", "lrange", "lreplace", "8", "5", "string", "keywords", "element", "insert"]
+++

linsert n 8.2 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

linsert - Insert elements into a list

## SYNOPSIS

**linsert **list index element ?*element element ...*?

---



## DESCRIPTION


This command produces a new list from *list* by inserting all of the
*element* arguments just before the *index*'th element of
*list*.  Each *element* argument will become a separate element of
the new list.  If *index* is less than or equal to zero, then the new
elements are inserted at the beginning of the list.

>  (Added in version 8.5)
The interpretation of the *index* value is the same as
for the command **string index**, supporting simple index
arithmetic and indices relative to the end of the list.



## EXAMPLE

Putting some values into a list, first indexing from the start and
then indexing from the end, and then chaining them together:

```
set oldList \{the fox jumps over the dog\}
set midList [**linsert** $oldList 1 quick]
set newList [**linsert** $midList end-1 lazy]
# The old lists still exist though...
set newerList [**linsert** [**linsert** $oldList end-1 quick] 1 lazy]
```



## SEE ALSO

list(n), lappend(n), lindex(n), llength(n), lsearch(n),
lset(n), lsort(n), lrange(n), lreplace(n),

>  (Added in version 8.5)
string(n)




## KEYWORDS

element, insert, list
