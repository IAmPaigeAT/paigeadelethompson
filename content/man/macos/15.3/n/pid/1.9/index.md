+++
description = "If the fileId argument is given then it should normally refer to a process pipeline created with the open command. In this case the pid command will return a list whose elements are the process identifiers of all the processes in the pipeline, in ..."
operating_system_version = "15.3"
operating_system = "macos"
manpage_name = "pid"
manpage_format = "troff"
manpage_section = "n"
date = "Sun Feb 16 04:48:30 2025"
detected_package_version = "1.9"
keywords = ["exec", "open", "keywords", "file", "pipeline", "process", "identifier"]
title = "pid(n)"
author = "None Specified"
+++

pid n 7.0 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

pid - Retrieve process identifiers

## SYNOPSIS

**pid **?*fileId*?

---



## DESCRIPTION


If the *fileId* argument is given then it should normally
refer to a process pipeline created with the **open** command.
In this case the **pid** command will return a list whose elements
are the process identifiers of all the processes in the pipeline,
in order.
The list will be empty if *fileId* refers to an open file
that is not a process pipeline.
If no *fileId* argument is given then **pid** returns the process
identifier of the current process.
All process identifiers are returned as decimal strings.

## EXAMPLE

Print process information about the processes in a pipeline using the
SysV **ps** program before reading the output of that pipeline:


```
set pipeline [open "| zcat somefile.gz | grep foobar | sort -u"]
# Print process information
exec ps -fp [**pid** $pipeline] >@stdout
# Print a separator and then the output of the pipeline
puts [string repeat - 70]
puts [read $pipeline]
close $pipeline
```



## SEE ALSO

exec(n), open(n)


## KEYWORDS

file, pipeline, process identifier
