+++
date = "Sun Feb 16 04:48:31 2025"
operating_system = "macos"
operating_system_version = "15.3"
detected_package_version = "1.9"
keywords = ["clock", "keywords", "script", "time"]
title = "time(n)"
manpage_section = "n"
author = "None Specified"
manpage_name = "time"
manpage_format = "troff"
description = "This command will call the Tcl interpreter count times to evaluate script (or once if count is not specified).  It will then return a string of the form 503 microseconds per iteration which indicates the average amount of time required per iterati..."
+++

time n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

time - Time the execution of a script

## SYNOPSIS

**time **script ?*count*?

---



## DESCRIPTION


This command will call the Tcl interpreter *count*
times to evaluate *script* (or once if *count* is not
specified).  It will then return a string of the form

```
**503 microseconds per iteration**
```

which indicates the average amount of time required per iteration,
in microseconds.
Time is measured in elapsed time, not CPU time.

## EXAMPLE

Estimate how long it takes for a simple Tcl **for** loop to count to
a thousand:

```
time \{
    for \{set i 0\} \{$i<1000\} \{incr i\} \{
        # empty body
    \}
\}
```



## SEE ALSO

clock(n)


## KEYWORDS

script, time
