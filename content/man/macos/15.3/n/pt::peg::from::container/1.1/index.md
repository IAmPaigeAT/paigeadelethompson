+++
manpage_section = "n"
operating_system = "macos"
title = "pt::peg::from::container(n)"
date = "Sun Feb 16 04:48:28 2025"
detected_package_version = "1.1"
author = "None Specified"
description = "Are you lost ? Do you have trouble understanding this document ? In that case please read the overview provided by the Introduction to Parser Tools. This document is the entrypoint to the whole system the current package is a part of. This packag..."
operating_system_version = "15.3"
manpage_format = "troff"
manpage_name = "pt::peg::from::container"
+++

"pt::peg::from::container" n 0 pt "Parser Tools"

---

## NAME

pt::peg::from::container - PEG Conversion. From CONTAINER format

## SYNOPSIS

package require **Tcl  8.5**


---


## DESCRIPTION


Are you lost ?
Do you have trouble understanding this document ?
In that case please read the overview provided by the
*Introduction to Parser Tools*. This document is the
entrypoint to the whole system the current package is a part of.

This package does not exist.
There is no need for it.
The CONTAINER format for parsing expression grammars is a piece of Tcl
code which, then sourced, provides a class whose instances have the
grammar we wish to import loaded.
Another way of looking at this is, the CONTAINER output is its own
import package.

## BUGS, IDEAS, FEEDBACK

This document, and the package it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *pt* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## KEYWORDS

EBNF, LL(k), PEG, TDPL, context-free languages, expression, grammar, matching, parser, parsing expression, parsing expression grammar, push down automaton, recursive descent, state, top-down parsing languages, transducer

## CATEGORY

Parsing and Grammars

## COPYRIGHT


```
Copyright (c) 2009 Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

