+++
manpage_format = "troff"
date = "Sun Feb 16 04:48:31 2025"
detected_package_version = "1.21"
operating_system = "macos"
author = "None Specified"
title = "mathfunc(n)"
manpage_section = "n"
manpage_name = "mathfunc"
description = "The expr command handles mathematical functions of the form sin($x) or atan2($y,$x) by converting them to calls of the form [tcl::mathfunc::sin [expr {$x}]] or [tcl::mathfunc::atan2 [expr {$y}] [expr {$x}]]. A number of math functions are availabl..."
keywords = ["expr", "mathop", "namespace", "copyright", "c", "1993", "the", "regents", "of", "university", "california", "1994-2000", "sun", "microsystems", "incorporated", "2005", "2006", "by", "kevin", "b", "kenny", "kennykb", "acm", "org"]
operating_system_version = "15.3"
+++

mathfunc n 8.5 Tcl "Tcl Mathematical Functions"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

mathfunc - Mathematical functions for Tcl expressions

## SYNOPSIS

package require **Tcl 8.5**

**::tcl::mathfunc::abs** *arg*
.br
**::tcl::mathfunc::acos** *arg*
.br
**::tcl::mathfunc::asin** *arg*
.br
**::tcl::mathfunc::atan** *arg*
.br
**::tcl::mathfunc::atan2** *y* *x*
.br
**::tcl::mathfunc::bool** *arg*
.br
**::tcl::mathfunc::ceil** *arg*
.br
**::tcl::mathfunc::cos** *arg*
.br
**::tcl::mathfunc::cosh** *arg*
.br
**::tcl::mathfunc::double** *arg*
.br

>  (Added in version 8.5)
**::tcl::mathfunc::entier** *arg*
.br


**::tcl::mathfunc::exp** *arg*
.br
**::tcl::mathfunc::floor** *arg*
.br
**::tcl::mathfunc::fmod** *x* *y*
.br
**::tcl::mathfunc::hypot** *x* *y*
.br
**::tcl::mathfunc::int** *arg*
.br
**::tcl::mathfunc::isqrt** *arg*
.br
**::tcl::mathfunc::log** *arg*
.br
**::tcl::mathfunc::log10** *arg*
.br
**::tcl::mathfunc::max** *arg* ?*arg* ...?
.br
**::tcl::mathfunc::min** *arg* ?*arg* ...?
.br
**::tcl::mathfunc::pow** *x* *y*
.br
**::tcl::mathfunc::rand**
.br
**::tcl::mathfunc::round** *arg*
.br
**::tcl::mathfunc::sin** *arg*
.br
**::tcl::mathfunc::sinh** *arg*
.br
**::tcl::mathfunc::sqrt** *arg*
.br
**::tcl::mathfunc::srand** *arg*
.br
**::tcl::mathfunc::tan** *arg*
.br
**::tcl::mathfunc::tanh** *arg*
.br
**::tcl::mathfunc::wide** *arg*


---


## DESCRIPTION


The **expr** command handles mathematical functions of the form
**sin($x)** or **atan2($y,$x)** by converting them to calls of the
form **[tcl::mathfunc::sin [expr \{$x\}]]** or
**[tcl::mathfunc::atan2 [expr \{$y\}] [expr \{$x\}]]**.
A number of math functions are available by default within the
namespace **::tcl::mathfunc**; these functions are also available
for code apart from **expr**, by invoking the given commands
directly.

Tcl supports the following mathematical functions in expressions, all
of which work solely with floating-point numbers unless otherwise noted:

> .ta 3c 6c 9c
**abs**	**acos**	**asin**	**atan**
**atan2**	**bool**	**ceil**	**cos**
**cosh**	**double**	**entier**	**exp**
**floor**	**fmod**	**hypot**	**int**
**isqrt**	**log**	**log10**	**max**
**min**	**pow**	**rand**	**round**
**sin**	**sinh**	**sqrt**	**srand**
**tan**	**tanh**	**wide**



In addition to these predefined functions, applications may
define additional functions by using **proc** (or any other method,
such as **interp alias** or **Tcl_CreateObjCommand**) to define
new commands in the **tcl::mathfunc** namespace.  In addition, an
obsolete interface named **Tcl_CreateMathFunc**() is available to
extensions that are written in C. The latter interface is not recommended
for new implementations.

### DETAILED DEFINITIONS


**abs **arg
Returns the absolute value of *arg*.  *Arg* may be either
integer or floating-point, and the result is returned in the same form.

**acos **arg
Returns the arc cosine of *arg*, in the range [*0*,*pi*]
radians. *Arg* should be in the range [*-1*,*1*].

**asin **arg
Returns the arc sine of *arg*, in the range [*-pi/2*,*pi/2*]
radians.  *Arg* should be in the range [*-1*,*1*].

**atan **arg
Returns the arc tangent of *arg*, in the range [*-pi/2*,*pi/2*]
radians.

**atan2 **y x
Returns the arc tangent of *y*/*x*, in the range [*-pi*,*pi*]
radians.  *x* and *y* cannot both be 0.  If *x* is greater
than *0*, this is equivalent to
""\fBatan" \fR[\fBexpr\fR
**bool **arg
Accepts any numeric value, or any string acceptable to
**string is boolean**, and returns the corresponding
boolean value **0** or **1**.  Non-zero numbers are true.
Other numbers are false.  Non-numeric strings produce boolean value in
agreement with **string is true** and **string is false**.

**ceil **arg
Returns the smallest integral floating-point value (i.e. with a zero
fractional part) not less than *arg*.  The argument may be any
numeric value.

**cos **arg
Returns the cosine of *arg*, measured in radians.

**cosh **arg
Returns the hyperbolic cosine of *arg*.  If the result would cause
an overflow, an error is returned.

**double **arg
The argument may be any numeric value,
If *arg* is a floating-point value, returns *arg*, otherwise converts
*arg* to floating-point and returns the converted value.  May return
**Inf** or **-Inf** when the argument is a numeric value that exceeds
the floating-point range.

**entier **arg

>  (Added in version 8.5)
The argument may be any numeric value.  The integer part of *arg*
is determined and returned.  The integer range returned by this function
is unlimited, unlike **int** and **wide** which
truncate their range to fit in particular storage widths.



**exp **arg
Returns the exponential of *arg*, defined as *e****arg*.
If the result would cause an overflow, an error is returned.

**floor **arg
Returns the largest integral floating-point value (i.e. with a zero
fractional part) not greater than *arg*.  The argument may be
any numeric value.

**fmod **x y
Returns the floating-point remainder of the division of *x* by
*y*.  If *y* is 0, an error is returned.

**hypot **x y
Computes the length of the hypotenuse of a right-angled triangle
""\fBsqrt\fR" [\fBexpr\fR
**int **arg
The argument may be any numeric value.  The integer part of *arg*
is determined, and then the low order bits of that integer value up
to the machine word size are returned as an integer value.  For reference,
the number of bytes in the machine word are stored in
**tcl_platform(wordSize)**.

**isqrt **arg
Computes the integer part of the square root of *arg*.  *Arg* must be
a positive value, either an integer or a floating point number.
Unlike **sqrt**, which is limited to the precision of a floating point
number, *isqrt* will return a result of arbitrary precision.

**log **arg
Returns the natural logarithm of *arg*.  *Arg* must be a
positive value.

**log10 **arg
Returns the base 10 logarithm of *arg*.  *Arg* must be a
positive value.

**max **arg** **...
Accepts one or more numeric arguments.  Returns the one argument
with the greatest value.

**min **arg** **...
Accepts one or more numeric arguments.  Returns the one argument
with the least value.

**pow **x y
Computes the value of *x* raised to the power *y*.  If *x*
is negative, *y* must be an integer value.

**rand**
Returns a pseudo-random floating-point value in the range (*0*,*1*).
The generator algorithm is a simple linear congruential generator that
is not cryptographically secure.  Each result from **rand** completely
determines all future results from subsequent calls to **rand**, so
**rand** should not be used to generate a sequence of secrets, such as
one-time passwords.  The seed of the generator is initialized from the
internal clock of the machine or may be set with the **srand** function.

**round **arg
If *arg* is an integer value, returns *arg*, otherwise converts
*arg* to integer by rounding and returns the converted value.

**sin **arg
Returns the sine of *arg*, measured in radians.

**sinh **arg
Returns the hyperbolic sine of *arg*.  If the result would cause
an overflow, an error is returned.

**sqrt **arg
The argument may be any non-negative numeric value.  Returns a floating-point
value that is the square root of *arg*.  May return **Inf** when the
argument is a numeric value that exceeds the square of the maximum value of
the floating-point range.

**srand **arg
The *arg*, which must be an integer, is used to reset the seed for
the random number generator of **rand**.  Returns the first random
number (see **rand**) from that seed.  Each interpreter has its own seed.

**tan **arg
Returns the tangent of *arg*, measured in radians.

**tanh **arg
Returns the hyperbolic tangent of *arg*.

**wide **arg
The argument may be any numeric value.  The integer part of *arg*
is determined, and then the low order 64 bits of that integer value
are returned as an integer value.

## SEE ALSO

expr(n), mathop(n), namespace(n)

## COPYRIGHT


```
Copyright (c) 1993 The Regents of the University of California.
Copyright (c) 1994-2000 Sun Microsystems Incorporated.
Copyright (c) 2005, 2006 by Kevin B. Kenny <kennykb@acm.org>.
```

