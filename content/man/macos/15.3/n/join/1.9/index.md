+++
description = "The list argument must be a valid Tcl list. This command returns the string formed by joining all of the elements of list together with joinString separating each adjacent pair of elements. The joinString argument defaults to a space character. M..."
manpage_section = "n"
detected_package_version = "1.9"
title = "join(n)"
author = "None Specified"
operating_system_version = "15.3"
date = "Sun Feb 16 04:48:30 2025"
manpage_name = "join"
keywords = ["list", "lappend", "split", "keywords", "element", "join", "separator"]
operating_system = "macos"
manpage_format = "troff"
+++

join n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

join - Create a string by joining together list elements

## SYNOPSIS

**join **list ?*joinString*?

---



## DESCRIPTION


The *list* argument must be a valid Tcl list.
This command returns the string
formed by joining all of the elements of *list* together with
*joinString* separating each adjacent pair of elements.
The *joinString* argument defaults to a space character.

## EXAMPLES

Making a comma-separated list:

```
set data \{1 2 3 4 5\}
**join** $data ", "
     **\(-> 1, 2, 3, 4, 5**
```


Using **join** to flatten a list by a single level:

```
set data \{1 \{2 3\} 4 \{5 \{6 7\} 8\}\}
**join** $data
     **\(-> 1 2 3 4 5 \{6 7\} 8**
```



## SEE ALSO

list(n), lappend(n), split(n)


## KEYWORDS

element, join, list, separator
