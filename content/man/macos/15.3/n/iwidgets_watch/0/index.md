+++
operating_system = "macos"
author = "None Specified"
manpage_name = "iwidgets_watch"
date = "Sun Feb 16 04:48:28 2025"
description = "..."
operating_system_version = "15.3"
detected_package_version = "0"
title = "iwidgets_watch(n)"
manpage_format = "troff"
manpage_section = "n"
+++

iwidgets::watch iwid

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

iwidgets::watch - Create and manipulate time with a watch widgets

## SYNOPSIS

**iwidgets::watch** *pathName *?*options*?

## INHERITANCE

itk::Widget <- iwidgets::Watch


## STANDARD OPTIONS



```
.ta 4c 8c 12c
background	cursor	foreground	relief
```


See the "options" manual entry for details on the standard options.

## ASSOCIATED OPTIONS


See the "Canvas" manual entry for details on the above associated options.


## WIDGET-SPECIFIC OPTIONS



```
Name:	clockColor
Class:	ColorfR
Command-Line Switch:	-clockcolor
```

.IP
Fill color for the main oval encapsulating the watch, in any of the forms
acceptable to **Tk_GetColor**.  The default is "White".

```
.LP
Name:	clockStipple
Class:	BitmapfR
Command-Line Switch:	-clockstipple
```

.IP
Bitmap for the main oval encapsulating the watch, in any of the forms
acceptable to **Tk_GetBitmap**.  The default is "".


```
Name:	height
Class:	Height
Command-Line Switch:	-height
```

.IP
Specifies the height of the watch widget in any of the forms
acceptable to **Tk_GetPixels**.  The default height is 175 pixels.


```
Name:	hourColor
Class:	ColorfR
Command-Line Switch:	-hourcolor
```

.IP
Fill color for the hour hand, in any of the forms acceptable to **Tk_GetColor**.
The default is "Red".


```
Name:	hourRadius
Class:	Radius
Command-Line Switch:	-hourradius
```

.IP
Specifies the radius of the hour hand as a percentage of the radius
from the center to the out perimeter of the clock.
The value must be a fraction <= 1.  The default is ".5".


```
Name:	minuteColor
Class:	ColorfR
Command-Line Switch:	-minutecolor
```

.IP
Fill color for the minute hand, in any of the forms acceptable to **Tk_GetColor**.
The default is "Yellow".


```
Name:	minuteRadius
Class:	Radius
Command-Line Switch:	-minuteradius
```

.IP
Specifies the radius of the minute hand as a percentage of the radius
from the center to the out perimeter of the clock.
The value must be a fraction <= 1.  The default is ".8".


```
Name:	pivotColor
Class:	ColorfR
Command-Line Switch:	-pivotcolor
```

.IP
Fill color for the circle in which the watch hands rotate
in any of the forms acceptable to **Tk_GetColor**.
The default is "White".


```
Name:	pivotRadius
Class:	Radius
Command-Line Switch:	-pivotradius
```

.IP
Specifies the radius of the circle in which the watch hands rotate
as a percentage of the radius.  The value must be a fraction <= 1.
The default is ".1".


```
Name:	secondColor
Class:	ColorfR
Command-Line Switch:	-secondcolor
```

.IP
Fill color for the second hand, in any of the forms acceptable to **Tk_GetColor**.
The default is "Black".


```
Name:	secondRadius
Class:	Radius
Command-Line Switch:	-secondradius
```

.IP
Specifies the radius of the second hand as a percentage of the radius
from the center to the out perimeter of the clock.
The value must be a fraction <= 1.  The default is ".9".


```
Name:	showAmPm
Class:	ShosAmPm
Command-Line Switch:	-showampm
```

.IP
Specifies whether the AM/PM radiobuttons should be displayed, in any
of the forms acceptable to **Tcl_GetBoolean**.  The default is yes.


```
Name:	state
Class:	State
Command-Line Switch:	-state
```

.IP
Specifies the editable state for the hands on the watch.  In a normal
state, the user can select and move the hands via mouse button 1.  The
valid values are **normal**, and **disabled**.  The defult is normal.


```
Name:	tickColor
Class:	ColorfR
Command-Line Switch:	-tickcolor
```

.IP
Fill color for the 60 ticks around the perimeter of the watch,
in any of the forms acceptable to **Tk_GetColor**.  The default is "Black".


```
Name:	width
Class:	Width
Command-Line Switch:	-width
```

.IP
Specifies the width of the watch widget in any of the forms
acceptable to **Tk_GetPixels**.  The default height is 155 pixels.

---



## DESCRIPTION



The **iwidgets::watch** command creates a watch with hour, minute, and
second hands modifying the time value.


## METHODS


The **iwidgets::watch** command creates a new Tcl command whose
name is *pathName*. This command may be used to invoke various
operations on the widget. It has the following general form:

> *pathName option *?*arg arg ...*?


*Option* and the *arg*s
determine the exact behavior of the command. The following
commands are possible for watch widgets:


## WIDGET-SPECIFIC METHODS


*pathName **cget** \fIoption*
Returns the current value of the configuration option given
by *option*.
*Option* may have any of the values accepted by the **iwidgets::watch**
command.

*pathName* **configure** ?*option*? ?*value option value ...*?
Query or modify the configuration options of the widget.
If no *option* is specified, returns a list describing all of
the available options for *pathName* (see **Tk_ConfigureInfo** for
information on the format of this list).  If *option* is specified
with no *value*, then the command returns a list describing the
one named option (this list will be identical to the corresponding
sublist of the value returned if no *option* is specified).  If
one or more *option-value* pairs are specified, then the command
modifies the given widget option(s) to have the given value(s);  in
this case the command returns an empty string.
*Option* may have any of the values accepted by the **iwidgets::watch**
command.

\fIpathName **get** ?**format**?
Returns the current time of the watch in a format of
string or as an integer clock value using the **-string** and **-clicks**
format options respectively.  The default is by string.  Reference the
clock command for more information on obtaining time and its
formats.

*pathName **show** \fItime*
Changes the currently displayed time to be that of the time
argument.  The time may be specified either as a string, an
integer clock value or the keyword "now".  Reference the clock
command for more information on obtaining time and its format.

\fIpathName **watch** ?**args**?
Evaluates the specifieds **args** against the canvas component.

.ta 4c

## COMPONENTS



```
Name:	canvas
Class:	Canvas
```

.IP
The canvas component is the where the clock is drawn.  See the
Canvas widget manual entry for details.


```
Name:	frame
Class:	Frame
```

.IP
The frame component is the where the "AM" and "PM" radiobuttons are displayed.
See the Frame widget manual entry for details.


```
Name:	am
Class:	Radiobutton
```

.IP
The am component indicates whether on not the time is relative to "AM".
See the Radiobutton widget manual entry for details.


```
Name:	pm
Class:	Radiobutton
```

.IP
The pm component indicates whether on not the time is relative to "PM".
See the Radiobutton widget manual entry for details.



## EXAMPLE


> package require Iwidgets 4.0
iwidgets::watch .w -state disabled -showampm no -width 155 -height 155
pack .w -padx 10 -pady 10 -fill both -expand yes

while \{1\} \{
  after 1000
  .w show
  update
\}



## AUTHOR

John Tucker


Mark L. Ulferts

## KEYWORDS

watch, hand, ticks, pivot, widget

