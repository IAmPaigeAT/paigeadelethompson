+++
manpage_format = "troff"
manpage_section = "n"
keywords = ["file", "eof", "fblocked", "tcl_standardchannels", "keywords", "blocking", "channel", "end", "of", "line", "nonblocking", "read"]
operating_system_version = "15.3"
author = "None Specified"
description = "This command reads the next line from channelId, returns everything in the line up to (but not including) the end-of-line character(s), and discards the end-of-line character(s). ChannelId must be an identifier for an open channel such as the Tcl ..."
manpage_name = "gets"
date = "Sun Feb 16 04:48:30 2025"
title = "gets(n)"
operating_system = "macos"
detected_package_version = "1.8"
+++

gets n 7.5 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

gets - Read a line from a channel

## SYNOPSIS

**gets **channelId ?*varName*?

---



## DESCRIPTION


This command reads the next line from *channelId*, returns everything
in the line up to (but not including) the end-of-line character(s), and
discards the end-of-line character(s).

*ChannelId* must be an identifier for an open channel such as the
Tcl standard input channel (**stdin**), the return value from an
invocation of **open** or **socket**, or the result of a channel
creation command provided by a Tcl extension. The channel must have
been opened for input.

If *varName* is omitted the line is returned as the result of the
command.
If *varName* is specified then the line is placed in the variable by
that name and the return value is a count of the number of characters
returned.

If end of file occurs while scanning for an end of
line, the command returns whatever input is available up to the end of file.
If *channelId* is in nonblocking mode and there is not a full
line of input available, the command returns an empty string and
does not consume any input.
If *varName* is specified and an empty string is returned in
*varName* because of end-of-file or because of insufficient
data in nonblocking mode, then the return count is -1.
Note that if *varName* is not specified then the end-of-file
and no-full-line-available cases can
produce the same results as if there were an input line consisting
only of the end-of-line character(s).
The **eof** and **fblocked** commands can be used to distinguish
these three cases.

## EXAMPLE

This example reads a file one line at a time and prints it out with
the current line number attached to the start of each line.


```
set chan [open "some.file.txt"]
set lineNumber 0
while \{[**gets** $chan line] >= 0\} \{
    puts "[incr lineNumber]: $line"
\}
close $chan
```



## SEE ALSO

file(n), eof(n), fblocked(n), Tcl_StandardChannels(3)


## KEYWORDS

blocking, channel, end of file, end of line, line, nonblocking, read
