+++
operating_system_version = "15.3"
description = "The procedure tk_chooseDirectory pops up a dialog box for the user to select a directory. The following option-value pairs are possible as command line arguments: -initialdir dirname Specifies that the directories in directory should be displayed..."
manpage_name = "tk_choosedirectory"
title = "tk_choosedirectory(n)"
manpage_section = "n"
date = "Sun Feb 16 04:48:30 2025"
keywords = ["tk_getopenfile", "tk_getsavefile", "keywords", "directory", "selection", "dialog", "platform-specific"]
manpage_format = "troff"
operating_system = "macos"
detected_package_version = "1.9"
author = "None Specified"
+++

tk_chooseDirectory n 8.3 Tk "Tk Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

tk_chooseDirectory - pops up a dialog box for the user to select a directory.

## SYNOPSIS

**tk_chooseDirectory **?*option value ...*?

---


## DESCRIPTION


The procedure **tk_chooseDirectory** pops up a dialog box for the
user to select a directory. The following *option-value* pairs are
possible as command line arguments:

**-initialdir** *dirname*
Specifies that the directories in *directory* should be displayed
when the dialog pops up. If this parameter is not specified, then
the directories in the current working directory are displayed. If the
parameter specifies a relative path, the return value will convert the
relative path to an absolute path.

**-mustexist** *boolean*
Specifies whether the user may specify non-existent directories.  If
this parameter is true, then the user may only select directories that
already exist.  The default value is *false*.

**-parent** *window*
Makes *window* the logical parent of the dialog. The dialog
is displayed on top of its parent window. On Mac OS X, this
turns the file dialog into a sheet attached to the parent window.

**-title** *titleString*
Specifies a string to display as the title of the dialog box. If this
option is not specified, then a default title will be displayed.

## EXAMPLE


```
set dir [**tk_chooseDirectory** \\
        -initialdir ~ -title "Choose a directory"]
if \{$dir eq ""\} \{
   label .l -text "No directory selected"
\} else \{
   label .l -text "Selected $dir"
\}
```



## SEE ALSO

tk_getOpenFile(n), tk_getSaveFile(n)

## KEYWORDS

directory, selection, dialog, platform-specific
