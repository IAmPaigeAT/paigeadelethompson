+++
operating_system = "macos"
author = "None Specified"
description = "This package provides an alternative way to declare Tcl procedures and to manage its arguments. There is a lot of benefit to declare a procedure with TEPAM rather than with the Tcl standard command proc: TEPAM allows specifying inside the procedure de..."
date = "Sun Feb 16 04:48:31 2025"
detected_package_version = "1.1"
keywords = ["tepam_introduction", "tepam_procedure", "keywords", "argument", "integrity", "validation", "arguments", "procedure", "subcommand", "category", "procedures", "parameters", "options", "copyright", "c", "2009", "2010", "andreas", "drollinger"]
title = "tepam::procedure(n)"
operating_system_version = "15.3"
manpage_name = "tepam::procedure"
manpage_section = "n"
manpage_format = "troff"
+++


```
display_message -mtype Info -text "It is PM 7:00."
-> Info: It is PM 7:00.

display_message -text "It is PM 7:00." -mtype Info
-> Info: It is PM 7:00.

display_message -mtype Info -text "It is PM 7:00." -text "You should go home."
-> Info: It is PM 7:00. You should go home.

display_message -text "It is PM 7:00." -text "You should go home." -mtype Info
-> Info: It is PM 7:00. You should go home.
```

Also named arguments that have not the *-multiple* attribute can be provided multiple times. Only the last provided argument will be retained in such a case:

```
display_message -mtype Info -text "It is PM 7:00." -mtype Warning
-> Warning: It is PM 7:00.
```


### UNNAMED ARGUMENTS FIRST, NAMED ARGUMENTS LATER (TK STYLE)

A procedure that has been defined while the variable **tepam::named_arguments_first** was set to 1, or with the procedure attribute *-named_arguments_first* set to 1 has to be called in the Tcl style. The following procedure declaration will be used in this section to illustrate the meaning of this calling style:

```
set tepam::named_arguments_first 1
tepam::procedure my_proc {
   -args {
      {-n1 -default ""}
      {-n2 -default ""}
      {u1 -default ""}
      {u2 -default ""}
   }
} {
   puts "n1:'$n1', n2:'$n2', u1:'$u1', u2:'$u2'"
}
```

The unnamed arguments are placed at the end of procedure call, after the named arguments:

```
my_proc -n1 N1 -n2 N2 U1 U2
-> n1:'N1', n2:'N2', u1:'U1', u2:'U2'
```

The argument parser considers the first argument that doesn't start with the '-' character as well as all following arguments as unnamed argument:

```
my_proc U1 U2
-> n1:'', n2:'', u1:'U1', u2:'U2'
```

Named arguments can be defined multiple times. If the named argument has the *-multiply* attribute, all argument data will be collected in a list. Otherwise, only the last provided attribute data will be retained:

```
my_proc -n1 N1 -n2 N2 -n1 M1 U1 U2
-> n1:'M1', n2:'N2', u1:'U1', u2:'U2'
```

The name of the first unnamed argument has therefore not to start with the '-' character. The unnamed argument is otherwise considered as name of another named argument. This is especially important if the first unnamed argument is given by a variable that can contain any character strings:

```
my_proc -n1 N1 -n2 N2 "->" "<-"
-> my_proc: Argument '->' not known

set U1 "->"
my_proc -n1 N1 -n2 N2 $U1 U2}]
my_proc: Argument '->' not known
```

The '--' flag allows separating unambiguously the unnamed arguments from the named arguments. All data after the '--' flag will be considered as unnamed argument:

```
my_proc -n1 N1 -n2 N2 -- "->" "<-"
-> n1:'N1', n2:'N2', u1:'->', u2:'<-'

set U1 "->"
my_proc -n1 N1 -n2 N2 -- $U1 U2
-> n1:'N1', n2:'N2', u1:'->', u2:'<-'
```


### NAMED ARGUMENTS FIRST, UNNAMED ARGUMENTS LATER (TCL STYLE)

The Tk calling style is selected when a procedure is defined while the variable **tepam::named_arguments_first** is set to 0, or when the procedure attribute *-named_arguments_first* has been set to 0. The following procedure will be used in this section to illustrate this calling style:

```
set tepam::named_arguments_first 0
tepam::procedure my_proc {
   -args {
      {-n1 -default ""}
      {-n2 -default ""}
      {u1}
      {u2 -default "" -multiple}
   }
} {
   puts "n1:'$n1', n2:'$n2', u1:'$u1', u2:'$u2'"
}
```

The unnamed arguments have to be provided first in this case. The named arguments are provided afterwards:

```
my_proc U1 U2 -n1 N1 -n2 N2
-> n1:'N1', n1:'N1', u1:'U1', u2:'U2'
```

The argument parser will assign to each defined unnamed argument an argument data before it switches to read the named arguments. This default behavior changes a bit if there are unnamed arguments that are optional or that can take multiple values.

An argument data will only be assigned to an unnamed argument that is optional (that has either the *-optional* attribute or that has a default value), when the data is not beginning with the '-' character or when no named arguments are defined. The data that starts with '-' is otherwise considered as the name of a named argument.

Argument data are assigned to an argument that has the *-multiple* attribute as long as the parameter value doesn't starts with the '-' character.

This constraint that parameter values that start with the '-' character cannot be assigned to optional unnamed arguments makes the Tcl procedure calling style not suitable for all situations. The Tk style may be preferable in some cases, since it allows separating unambiguously the named arguments from the unnamed ones with the '--' flag.

Let's explore in a bit less theoretically the ways how the previously defined procedure can be called: The first example calls the procedure without any parameters, which leads to an error since *u1* is a mandatory argument:

```
my_proc
-> my_proc: Required argument is missing: u1
```

The procedure call is valid if one parameter is provided for *u1*:

```
my_proc U1
-> n1:'', n2:'', u1:'U1', u2:''
```

When more parameters are provided that are not starting with the '-' character, then they will be attributed to the unnamed arguments. *U2* will receive 3 of these parameters, since it accepts multiple values:

```
my_proc U1 U2 U3 U4
-> n1:'', n2:'', u1:'U1', u2:'U2 U3 U4'
```

As soon as one parameter starts with '-' and all unnamed arguments have been served, the argument manager tries to interpret the parameter as name of a named argument. The procedure call will fail if the intend was to attribute the parameter beginning with '-' to an unnamed argument:

```
my_proc U1 U2 U3 U4 -U5
-> my_proc: Argument '-U5' not known
```

The attribution of a parameter to a named argument will fail if there are undefined unnamed (non optional) arguments. The name specification will in this case simply be considered as a parameter value that is attributed to the *next* unnamed argument. This was certainly not the intention in the following example:

```
my_proc -n1 N1
-> n1:'', n2:'', u1:'-n1', u2:'N1'
```

The situation is completely different if values have already been attributed to all mandatory unnamed arguments. A parameter beginning with the '-' character will in this case be considered as a name identifier for a named argument:

```
my_proc U1 -n1 N1
-> n1:'N1', n2:'', u1:'U1', u2:''
```

No unnamed arguments are allowed behind the named arguments:

```
my_proc U1 -n1 N1 U2
-> my_proc: Argument 'U2' is not an option
```

The '--' flag has no special meaning if not all mandatory arguments have got assigned a value. This flag will simply be attributed to one of the unnamed arguments:

```
my_proc -- -n1 N1
-> n1:'N1', n2:'', u1:'--', u2:''
```

But the '--' flag is simply ignored if the argument parser has started to handle the named arguments:

```
my_proc U1 -- -n1 N1
-> n1:'N1', n2:'', u1:'U1', u2:''

my_proc U1 -n1 N1 -- -n2 N2
-> n1:'N1', n2:'N2', u1:'U1', u2:''
```


### RAW ARGUMENT LIST

It may be necessary sometimes that the procedure body is able to access the entire list of arguments provided during a procedure call. This can happen via the **args** variable that contains always to unprocessed argument list:

```
tepam::procedure {display_message} {
   -args {
      {-mtype -choices {Warning Error} -default Warning}
      {text -type string -multiple}

   }
} {
   puts "args: $args"
}
display_message -mtype Warning "It is 7:00"
-> args: -mtype Warning {It is 7:00}
```


## SEE ALSO

tepam_introduction(n), tepam_procedure(n)

## KEYWORDS

argument integrity, argument validation, arguments, procedure, subcommand

## CATEGORY

Procedures, arguments, parameters, options

## COPYRIGHT


```
Copyright (c) 2009/2010, Andreas Drollinger

```

