+++
operating_system_version = "15.3"
author = "None Specified"
manpage_format = "troff"
date = "Sun Feb 16 04:48:28 2025"
detected_package_version = "1.1"
manpage_section = "n"
description = "This package provides a toolbar megawidget (snidget). It makes use of the Tile/Ttk themed widget set. widget::toolbar pathname ?options? getframe add ?item? ?args? delete item1 ?item2? ?...? itemcget symbol option itemconfigure symbol ?args? i..."
manpage_name = "widget_toolbar"
operating_system = "macos"
title = "widget_toolbar(n)"
+++

"widget_toolbar" n 3.0 widget "widget::toolbar Megawidget"

---

## NAME

widget_toolbar - widget::toolbar Megawidget

## SYNOPSIS

package require **Tcl  8.4**

package require **Tk  8.4**

package require **widget  ?3.0?**

package require **widget::toolbar  ?1.0?**

**widget::toolbar** *pathname* ?options?

getframe

add ?item? ?args?

delete item1 ?item2? ?...?

itemcget symbol option

itemconfigure symbol ?args?

items ?pattern?

remove ?-destroy? item1 ?item2? ?...?


---


## DESCRIPTION

This package provides a toolbar megawidget (snidget).
It makes use of the Tile/Ttk themed widget set.


**widget::toolbar** *pathname* ?options?

getframe

add ?item? ?args?

delete item1 ?item2? ?...?

itemcget symbol option

itemconfigure symbol ?args?

items ?pattern?

remove ?-destroy? item1 ?item2? ?...?


## WIDGET OPTIONS


**-ipad**

**-pad**

**-separator**


## ITEM OPTIONS


**-pad**

**-separator**

**-sticky**

**-weight**


## EXAMPLE


```

package require widget::toolbar ; # or widget::all
set t [widget::toolbar .t]
pack $t -fill x -expand 1
$t add button [button .b -text foo]
$t add separator -pad {2 4}
$t add button [button .c -text bar]

```


## KEYWORDS

megawidget, snit, widget
