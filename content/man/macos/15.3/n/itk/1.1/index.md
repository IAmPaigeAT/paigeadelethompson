+++
detected_package_version = "1.1"
operating_system = "macos"
manpage_name = "itk"
operating_system_version = "15.3"
author = "None Specified"
manpage_section = "n"
manpage_format = "troff"
date = "Sun Feb 16 04:48:30 2025"
description = "Mega-widgets are high-level widgets that are constructed using Tk widgets as component parts, usually without any C code.  A fileselectionbox, for example, may have a few listboxes, some entry widgets and some control buttons.  These individual wid..."
title = "itk(n)"
+++

itk n 3.0 itk "[incr\ Tk]"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

itk - framework for building mega-widgets in Tcl/Tk

---



## DESCRIPTION


Mega-widgets are high-level widgets that are constructed using
Tk widgets as component parts, usually without any C code.  A
fileselectionbox, for example, may have a few listboxes, some
entry widgets and some control buttons.  These individual widgets
are put together in a way that makes them act like one big
widget.  A fileselectionbox mega-widget can be created with a
command like:

```
fileselectionbox .fsb -background blue -foreground white
```

Once it has been created, it can be reconfigured with a command
like:

```
.fsb configure -background green -foreground black
```

and all of its internal components will change color.  Each
mega-widget has a set of methods that can be used to manipulate
it.  For example, the current selection can be queried from a
fileselectionbox like this:

```
set fileName [.fsb get]
```

In effect, a mega-widget looks and acts exactly like a Tk widget,
but is considerably easier to implement.

**[incr\ Tk]** is a framework for building mega-widgets.  It
uses **[incr\ Tcl]** to support the object paradigm, and adds
base classes which provide default widget behaviors.

All **[incr\ Tk]** widgets are derived from the **Archetype**
base class.  This class manages internal component widgets,
and provides methods like "configure" and "cget" to access
configuration options.

The **Widget** base class inherits everything from **Archetype**,
and adds a Tk frame which acts as a container for the mega-widget.
It is used to build mega-widgets that sit inside of other frames
and toplevels.  Derived classes create other internal components
and pack them into the "hull" frame created by the **Widget**
base class.

The **Toplevel** base class inherits everything from **Archetype**,
but adds a Tk toplevel which acts as a container for the mega-widget.
It is used to build mega-widgets, such as dialog boxes, that have
their own toplevel window.  Derived classes create other internal
components and pack them into the "hull" toplevel created by the
**Toplevel** base class.


## [incr Widgets] LIBRARY


**[incr\ Widgets]** is a mega-widget library built using
**[incr\ Tk]**.  It can be used right out of the box, and
contains more than 30 different widget classes, including:
-
fileselectiondialog
-
tabnotebook
-
panedwindow
-
combobox
-
optionmenu
-
scrolledlistbox
-
scrolledframe
-
messagedialog
-
and many others...

The **catalog** demo in the "iwidgets/demos" directory
shows all of the available widgets in action.  Each widget
class has its own man page describing the features available.


## KEYWORDS

class, object, object-oriented, mega-widget
