+++
detected_package_version = "1.6"
operating_system = "macos"
author = "None Specified"
manpage_section = "n"
manpage_format = "troff"
date = "Sun Feb 16 04:48:28 2025"
description = "The lreverse command returns a list that has the same elements as its input list, list, except with the elements in the reverse order. lreverse {a a b c}       (-> c b a a lreverse {a b {c d} e f}       (-> f e {c d} b a list(n), lsearch(n), lso..."
operating_system_version = "15.3"
manpage_name = "lreverse"
title = "lreverse(n)"
+++

lreverse n 8.5 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

lreverse - Reverse the order of a list

## SYNOPSIS

**lreverse **list

---


## DESCRIPTION


The **lreverse** command returns a list that has the same elements as its
input list, *list*, except with the elements in the reverse order.

## EXAMPLES


```
**lreverse** \{a a b c\}
      *\(-> c b a a*
**lreverse** \{a b \{c d\} e f\}
      *\(-> f e \{c d\} b a*
```


## SEE ALSO

list(n), lsearch(n), lsort(n)


## KEYWORDS

element, list, reverse
