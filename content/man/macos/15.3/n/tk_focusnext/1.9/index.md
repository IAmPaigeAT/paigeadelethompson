+++
title = "tk_focusnext(n)"
operating_system = "macos"
manpage_format = "troff"
manpage_name = "tk_focusnext"
description = "tk_focusNext is a utility procedure used for keyboard traversal. It returns the window after window in focus order. The focus order is determined by the stacking order of windows and the structure of the window hierarchy. Among siblings, the focus..."
date = "Sun Feb 16 04:48:29 2025"
detected_package_version = "1.9"
manpage_section = "n"
operating_system_version = "15.3"
author = "None Specified"
+++

tk_focusNext n 4.0 Tk "Tk Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

tk_focusNext, tk_focusPrev, tk_focusFollowsMouse - Utility procedures for managing the input focus.

## SYNOPSIS

**tk_focusNext **window

**tk_focusPrev **window

**tk_focusFollowsMouse**

---



## DESCRIPTION


**tk_focusNext** is a utility procedure used for keyboard traversal.
It returns the
"next"window after *window* in focus order. The focus order is determined by
the stacking order of windows and the structure of the window hierarchy.
Among siblings, the focus order is the same as the stacking order, with the
lowest window being first.
If a window has children, the window is visited first, followed by
its children (recursively), followed by its next sibling.
Top-level windows other than *window* are skipped, so that
**tk_focusNext** never returns a window in a different top-level
from *window*.

After computing the next window, **tk_focusNext** examines the
window's **-takefocus** option to see whether it should be skipped.
If so, **tk_focusNext** continues on to the next window in the focus
order, until it eventually finds a window that will accept the focus
or returns back to *window*.

**tk_focusPrev** is similar to **tk_focusNext** except that it
returns the window just before *window* in the focus order.

**tk_focusFollowsMouse** changes the focus model for the application
to an implicit one where the window under the mouse gets the focus.
After this procedure is called, whenever the mouse enters a window
Tk will automatically give it the input focus.
The **focus** command may be used to move the focus to a window
other than the one under the mouse, but as soon as the mouse moves
into a new window the focus will jump to that window.
Note: at present there is no built-in support for returning the
application to an explicit focus model;  to do this you will have
to write a script that deletes the bindings created by
**tk_focusFollowsMouse**.


## KEYWORDS

focus, keyboard traversal, top-level
