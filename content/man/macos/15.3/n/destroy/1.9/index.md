+++
author = "None Specified"
operating_system_version = "15.3"
detected_package_version = "1.9"
description = "This command deletes the windows given by the window arguments, plus all of their descendants. If a window is deleted then all windows will be destroyed and the application will (normally) exit. The windows are destroyed in order, and if an error..."
manpage_section = "n"
date = "Sun Feb 16 04:48:27 2025"
manpage_format = "troff"
manpage_name = "destroy"
title = "destroy(n)"
operating_system = "macos"
+++

destroy n "" Tk "Tk Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

destroy - Destroy one or more windows

## SYNOPSIS

**destroy **?*window window ...*?

---


## DESCRIPTION


This command deletes the windows given by the
*window* arguments, plus all of their descendants.
If a *window*
"."is deleted then all windows will be destroyed and the application will
(normally) exit.
The *window*s are destroyed in order, and if an error occurs
in destroying a window the command aborts without destroying the
remaining windows.
No error is returned if *window* does not exist.

## EXAMPLE

Destroy all checkbuttons that are direct children of the given widget:

```
proc killCheckbuttonChildren \{parent\} \{
   foreach w [winfo children $parent] \{
      if \{[winfo class $w] eq "Checkbutton"\} \{
         **destroy** $w
      \}
   \}
\}
```



## KEYWORDS

application, destroy, window
