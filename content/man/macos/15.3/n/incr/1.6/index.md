+++
manpage_section = "n"
detected_package_version = "1.6"
operating_system_version = "15.3"
author = "None Specified"
keywords = ["expr", "keywords", "add", "increment", "variable", "value"]
operating_system = "macos"
date = "Sun Feb 16 04:48:27 2025"
manpage_name = "incr"
description = "Increments the value stored in the variable whose name is varName. The value of the variable must be an integer. If increment is supplied then its value (which must be an integer) is added to the value of variable varName;  otherwise 1 is added to..."
manpage_format = "troff"
title = "incr(n)"
+++

incr n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

incr - Increment the value of a variable

## SYNOPSIS

**incr **varName ?*increment*?

---



## DESCRIPTION


Increments the value stored in the variable whose name is *varName*.
The value of the variable must be an integer.
If *increment* is supplied then its value (which must be an
integer) is added to the value of variable *varName*;  otherwise
1 is added to *varName*.
The new value is stored as a decimal string in variable *varName*
and also returned as result.


>  (Added in version 8.5)
Starting with the Tcl 8.5 release, the variable *varName* passed
to **incr** may be unset, and in that case, it will be set to
the value *increment* or to the default increment value of **1**.



## EXAMPLES

Add one to the contents of the variable *x*:

```
**incr** x
```


Add 42 to the contents of the variable *x*:

```
**incr** x 42
```


Add the contents of the variable *y* to the contents of the
variable *x*:

```
**incr** x $y
```


Add nothing at all to the variable *x* (often useful for checking
whether an argument to a procedure is actually integral and generating
an error if it is not):

```
**incr** x 0
```



## SEE ALSO

expr(n)


## KEYWORDS

add, increment, variable, value
