+++
operating_system = "macos"
author = "None Specified"
title = "doctools::msgcat::toc::fr(n)"
manpage_section = "n"
detected_package_version = "1.1"
operating_system_version = "15.3"
manpage_format = "troff"
description = "The package doctools::msgcat::toc::fr is a support module providing the FR (french) language message catalog for the doctoc parser in the doctools system version 2. As such it is an internal package a regular user (developer) should not be in direc..."
manpage_name = "doctools::msgcat::toc::fr"
date = "Sun Feb 16 04:48:30 2025"
+++

"doctools::msgcat::toc::fr" n 0.1 doctools2toc "Documentation tools"

---

## NAME

doctools::msgcat::toc::fr - Message catalog for the doctoc parser (FR)

## SYNOPSIS

package require **Tcl  8.4**

package require **msgcat **

package require **doctools::msgcat::toc::fr  ?0.1?**


---


## DESCRIPTION

The package **doctools::msgcat::toc::fr** is a
support module providing the FR (french) language message catalog
for the doctoc parser in the doctools system version 2. As such it is
an internal package a regular user (developer) should not be in direct
contact with.

If you are such please go the documentation of either
[1]
**doctools::doc**,
[2]
**doctools::toc**, or
[3]
**doctools::idx**


Within the system architecture this package resides under the package
**doctools::msgcat** providing the general message catalog
management within the system. *Note* that there is *no*
explicit dependency between the manager and catalog packages. The
catalog is a plugin which is selected and loaded dynamically.

## API

This package has no exported API.

## BUGS, IDEAS, FEEDBACK

This document, and the package it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *doctools* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## KEYWORDS

FR, catalog package, doctoc, doctools, i18n, internationalization, l10n, localization, message catalog, message package

## CATEGORY

Documentation tools

## COPYRIGHT


```
Copyright (c) 2009 Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

