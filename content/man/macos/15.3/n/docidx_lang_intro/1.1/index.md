+++
manpage_section = "n"
author = "None Specified"
description = "This document is an informal introduction to version 1 of the docidx markup language based on a multitude of examples. After reading this a writer should be ready to understand the two parts of the formal specification, i.e. the docidx language syn..."
operating_system = "macos"
operating_system_version = "15.3"
manpage_format = "troff"
keywords = ["docidx_intro", "docidx_lang_cmdref", "docidx_lang_syntax", "keywords", "docidx", "commands", "language", "markup", "syntax", "semantic", "category", "documentation", "tools", "copyright", "c", "2007-2009", "andreas", "kupries", "andreas_kupries", "users", "sourceforge", "net"]
title = "docidx_lang_intro(n)"
date = "Sun Feb 16 04:48:28 2025"
detected_package_version = "1.1"
manpage_name = "docidx_lang_intro"
+++

[index_end]


The only restriction **include** has to obey is that the contents of
the included file must be valid at the place of the inclusion. I.e. a
file included before **index_begin** may contain only the templating
commands **vset** and **include**, a file included after a key
may contain only manape or url references, and other keys, etc.

### ESCAPES

Beyond the 6 commands shown so far we have two more available.
However their function is not the marking up of index structure, but
the insertion of characters, namely **[** and **]**.
These commands, **lb** and **rb** respectively, are required
because our use of [ and ] to bracket markup commands makes it
impossible to directly use [ and ] within the text.

Our example of their use are the sources of the last sentence in the
previous paragraph, with some highlighting added.

```

  ...
  These commands, [cmd lb] and [cmd lb] respectively, are required
  because our use of [lb] and [rb] to bracket markup commands makes it
  impossible to directly use [lb] and [rb] within the text.
  ...

```


## FURTHER READING

Now that this document has been digested the reader, assumed to be a
*writer* of documentation should be fortified enough to be able
to understand the formal *docidx language syntax*
specification as well. From here on out the
*docidx language command reference* will also serve as the
detailed specification and cheat sheet for all available commands and
their syntax.

To be able to validate a document while writing it, it is also
recommended to familiarize oneself with Tclapps' ultra-configurable
**dtp**.

On the other hand, docidx is perfectly suited for the automatic
generation from doctools documents, and this is the route Tcllib's
easy and simple **dtplite** goes, creating an index for a set of
documents behind the scenes, without the writer having to do so on
their own.

## BUGS, IDEAS, FEEDBACK

This document, will undoubtedly contain bugs and other problems.
Please report such in the category *doctools* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have.

## SEE ALSO

docidx_intro, docidx_lang_cmdref, docidx_lang_syntax

## KEYWORDS

docidx commands, docidx language, docidx markup, docidx syntax, markup, semantic markup

## CATEGORY

Documentation tools

## COPYRIGHT


```
Copyright (c) 2007-2009 Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

