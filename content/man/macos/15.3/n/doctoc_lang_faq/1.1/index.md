+++
operating_system = "macos"
manpage_section = "n"
keywords = ["doctoc_lang_cmdref", "doctoc_lang_intro", "doctoc_lang_syntax", "keywords", "doctoc", "commands", "language", "markup", "syntax", "examples", "faq", "semantic", "category", "documentation", "tools", "copyright", "c", "2007", "andreas", "kupries", "andreas_kupries", "users", "sourceforge", "net"]
date = "Sun Feb 16 04:48:30 2025"
description = "This document is currently mainly a placeholder, to be filled with commonly asked questions about the doctoc markup language and companions, and their answers. Please report any questions (and, if possible, answers) we should consider for this doc..."
author = "None Specified"
title = "doctoc_lang_faq(n)"
manpage_name = "doctoc_lang_faq"
detected_package_version = "1.1"
manpage_format = "troff"
operating_system_version = "15.3"
+++

"doctoc_lang_faq" n 1.0 doctools "Documentation tools"

---

## NAME

doctoc_lang_faq - doctoc language faq

## DESCRIPTION



## OVERVIEW


### WHAT IS THIS DOCUMENT?

This document is currently mainly a placeholder, to be filled with
commonly asked questions about the doctoc markup language and
companions, and their answers.

Please report any questions (and, if possible, answers) we should
consider for this document in the category *doctools* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].

## EXAMPLES


### WHERE DO I FIND DOCTOC EXAMPLES?

We have no direct examples of documents written using doctoc
markup. However the doctools processor **dtplite** does generate
a table of contents when processing a set of documents written in
doctools markup. The intermediate file for it uses doctoc markup and
is not deleted when generation completes. Such files can therefore
serve as examples.

**dtplite** is distributed as part of Tcllib, so to get it you
need one of
[1]
A CVS snapshot of Tcllib. How to retrieve such a snapshot and the
tools required for this are described at
*http://sourceforge.net/cvs/?group_id=12883*
[2]
A Tcllib release archive. They are available at
*http://sourceforge.net/project/showfiles.php?group_id=12883*


## BUGS, IDEAS, FEEDBACK

This document, will undoubtedly contain bugs and other problems.
Please report any such in the category *doctools* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
the package and/or the documentation.

## SEE ALSO

doctoc_lang_cmdref, doctoc_lang_intro, doctoc_lang_syntax

## KEYWORDS

doctoc commands, doctoc language, doctoc markup, doctoc syntax, examples, faq, markup, semantic markup

## CATEGORY

Documentation tools

## COPYRIGHT


```
Copyright (c) 2007 Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

