+++
title = "iwidgets_selectiondialog(n)"
manpage_format = "troff"
operating_system = "macos"
manpage_name = "iwidgets_selectiondialog"
operating_system_version = "15.3"
detected_package_version = "0"
author = "None Specified"
description = "The iwidgets::selectiondialog command creates a selection box similar to  the OSF/Motif standard selection dialog composite widget.  The selectiondialog is derived from the  Dialog class and is composed of a selectionbox with commands  to manipula..."
date = "Sun Feb 16 04:48:29 2025"
manpage_section = "n"
+++

iwidgets::selectiondialog iwid

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

iwidgets::selectiondialog - Create and manipulate a selection dialog widget

## SYNOPSIS

**iwidgets::selectiondialog** *pathName *?*options*?

## INHERITANCE

itk::Toplevel <- iwidgets::Shell <- iwidgets::Dialogshell <- iwidgets::Dialog <- iwidgets::Selectiondialog

## STANDARD OPTIONS



```
.ta 4c 8c 12c

activeBackground  background         borderWidth        cursor
exportSelection   foreground         highlightColor     highlightThickness
insertBackground  insertBorderWidth  insertOffTime      insertOnTime
insertWidth       selectBackground   selectBorderWidth  selectForeground
```


See the "options" manual entry for details on the standard options.

## ASSOCIATED OPTIONS



```
.ta 4c 8c 12c
textBackground	textFont
```


See the "entryfield" widget manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
labelFont	
```


See the "labeledwidget" widget manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
activeRelief	elementBorderWidth	jump	troughColor
```


See the "scrollbar" widget class manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
textBackground	textFont	
```


See the "scrolledlistbox" widget class manual entry for details on the above
associated options.
**childsitepos**	**itemsCommand**	**itemsLabel**	**itemsOn**
**selectionCommand**	**selectionLabel**	**selectionOn**


See the "selectionbox" widget manual entry for details on the above
associated options.

## INHERITED OPTIONS



```
.ta 4c 8c 12c
buttonBoxPadX	buttonBoxPadY 	buttonBoxPos	padX
padY	separator	thickness
```


See the "dialogshell" widget  manual entry for details on the above
inherited options.


```
.ta 4c 8c 12c
height	master	modality	width
```


See the "shell" widget  manual entry for details on the above
inherited options.


```
.ta 4c 8c 12c
title 
```


See the "Toplevel" widget  manual entry for details on the above
inherited options.

---



## DESCRIPTION


The **iwidgets::selectiondialog** command creates a selection box similar to
the OSF/Motif standard selection
dialog composite widget.  The selectiondialog is derived from the
Dialog class and is composed of a selectionbox with commands
to manipulate the dialog buttons.


## METHODS


The **iwidgets::selectiondialog** command creates a new Tcl command whose
name is *pathName*.  This
command may be used to invoke various
operations on the widget.  It has the following general form:

> *pathName option *?*arg arg ...*?


*Option* and the *arg*s
determine the exact behavior of the command.  The following
commands are possible for selectiondialog widgets:

## ASSOCIATED METHODS



```
.ta 4c 8c 12c
childsite	clear	get	insert
selectitem
```


See the "selectionbox" widget manual entry for details on the above
associated methods.


```
.ta 4c 8c 12c
curselection	delete	index	nearest
scan	selection	size
```


See the "listbox" widget manual entry for details on the above
associated methods.

## INHERITED METHODS



```
.ta 4c 8c 12c
add	buttonconfigure	default	hide
invoke	show	
```


See the "buttonbox" widget manual entry for details on the above
inherited methods.


```
.ta 4c 8c 12c
activate	center	deactivate
```


See the "shell" widget manual entry for details on the above
inherited methods.

## WIDGET-SPECIFIC METHODS


*pathName **cget** \fIoption*
Returns the current value of the configuration option given
by *option*.
*Option* may have any of the values accepted by the **iwidgets::selectiondialog**
command.

*pathName* **configure** ?*option*? ?*value option value ...*?
Query or modify the configuration options of the widget.
If no *option* is specified, returns a list describing all of
the available options for *pathName* (see **Tk_ConfigureInfo** for
information on the format of this list).  If *option* is specified
with no *value*, then the command returns a list describing the
one named option (this list will be identical to the corresponding
sublist of the value returned if no *option* is specified).  If
one or more *option-value* pairs are specified, then the command
modifies the given widget option(s) to have the given value(s);  in
this case the command returns an empty string.
*Option* may have any of the values accepted by the **iwidgets::selectiondialog**
command.


## COMPONENTS



```
Name:	selectionbox
Class:	Selectionbox
```

.IP
The selectionbox component is the selection box for the selection
dialog.  See the "selectionbox" widget manual entry for details on the
selectionbox component item.



## EXAMPLE


>  package require Iwidgets 4.0
 iwidgets::selectiondialog .sd
 .sd activate



## AUTHOR

Mark L. Ulferts

## KEYWORDS

selectiondialog, selectionbox, dialog, dialogshell, shell, widget

