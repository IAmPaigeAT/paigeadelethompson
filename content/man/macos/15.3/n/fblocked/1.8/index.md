+++
manpage_format = "troff"
date = "Sun Feb 16 04:48:31 2025"
operating_system_version = "15.3"
author = "None Specified"
manpage_section = "n"
keywords = ["gets", "open", "read", "socket", "tcl_standardchannels", "keywords", "blocking", "nonblocking"]
manpage_name = "fblocked"
operating_system = "macos"
detected_package_version = "1.8"
title = "fblocked(n)"
description = "The fblocked command returns 1 if the most recent input operation on channelId returned less information than requested because all available input was exhausted. For example, if gets is invoked when there are only three characters available for i..."
+++

fblocked n 7.5 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

fblocked - Test whether the last input operation exhausted all available input

## SYNOPSIS

**fblocked **channelId

---



## DESCRIPTION


The **fblocked** command returns 1 if the most recent input operation
on *channelId* returned less information than requested because all
available input was exhausted.
For example, if **gets** is invoked when there are only three
characters available for input and no end-of-line sequence, **gets**
returns an empty string and a subsequent call to **fblocked** will
return 1.

*ChannelId* must be an identifier for an open channel such as a
Tcl standard channel (**stdin**, **stdout**, or **stderr**),
the return value from an invocation of **open** or **socket**, or
the result of a channel creation command provided by a Tcl extension.

## EXAMPLE

The **fblocked** command is particularly useful when writing network
servers, as it allows you to write your code in a line-by-line style
without preventing the servicing of other connections.  This can be
seen in this simple echo-service:


```
# This is called whenever a new client connects to the server
proc connect \{chan host port\} \{
    set clientName [format <%s:%d> $host $port]
    puts "connection from $clientName"
    fconfigure $chan -blocking 0 -buffering line
    fileevent $chan readable [list echoLine $chan $clientName]
\}

# This is called whenever either at least one byte of input
# data is available, or the channel was closed by the client.
proc echoLine \{chan clientName\} \{
    gets $chan line
    if \{[eof $chan]\} \{
        puts "finishing connection from $clientName"
        close $chan
    \} elseif \{![**fblocked** $chan]\} \{
        # Didn't block waiting for end-of-line
        puts "$clientName - $line"
        puts $chan $line
    \}
\}

# Create the server socket and enter the event-loop to wait
# for incoming connections...
socket -server connect 12345
vwait forever
```



## SEE ALSO

gets(n), open(n), read(n), socket(n), Tcl_StandardChannels(3)


## KEYWORDS

blocking, nonblocking
