+++
description = "This package provides a command to take snapshots of arbitrary canvases. ::canvas::snap pathName Takes a snapshot of the canvas pathName. The result is the name of a new Tk photo image containing the snapshot. Note that this command has a side-ef..."
manpage_section = "n"
operating_system_version = "15.3"
detected_package_version = "1.1"
date = "Sun Feb 16 04:48:29 2025"
title = "canvas::snap(n)"
manpage_name = "canvas::snap"
manpage_format = "troff"
author = "None Specified"
operating_system = "macos"
+++

"canvas::snap" n 1.0.1 canvas "Variations on a canvas"

---

## NAME

canvas::snap - Canvas snapshot to Tk photo image

## SYNOPSIS

package require **Tcl  8.5**

package require **Tk  8.5**

package require **canvas::snap  ?1.0.1?**

package require **img::window **

**::canvas::snap** *pathName*


---


## DESCRIPTION

This package provides a command to take snapshots of arbitrary
canvases.

## API


**::canvas::snap** *pathName*
Takes a snapshot of the canvas *pathName*. The result is the name
of a new Tk photo image containing the snapshot.

*Note* that this command has a side-effect. To avoid having white
rectangles where other windows may overlap the canvas this command
forces the toplevel window the canvas is in to the top of the stacking
order.


## KEYWORDS

canvas, image, photo, print screen, snapshot

## COPYRIGHT


```
Copyright (c) 2004 George Petasis (http://wiki.tcl.tk/1404)
Copyright (c) 2010 Documentation, Andreas Kupries

```

