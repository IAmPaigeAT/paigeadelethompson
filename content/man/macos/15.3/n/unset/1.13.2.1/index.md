+++
date = "Sun Feb 16 04:48:30 2025"
author = "None Specified"
manpage_section = "n"
keywords = ["set", "trace", "upvar", "keywords", "remove", "variable"]
description = "This command removes one or more variables. Each name is a variable name, specified in any of the ways acceptable to the set command. If a name refers to an element of an array then that element is removed without affecting the rest of the array...."
operating_system = "macos"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_name = "unset"
title = "unset(n)"
detected_package_version = "1.13.2.1"
+++

unset n 8.4 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

unset - Delete variables

## SYNOPSIS

**unset **?*-nocomplain*? ?*--*? ?*name name name ...*?

---


## DESCRIPTION


This command removes one or more variables.
Each *name* is a variable name, specified in any of the
ways acceptable to the **set** command.
If a *name* refers to an element of an array then that
element is removed without affecting the rest of the array.
If a *name* consists of an array name with no parenthesized
index, then the entire array is deleted.
The **unset** command returns an empty string as result.
If *-nocomplain* is specified as the first argument, any possible
errors are suppressed.  The option may not be abbreviated, in order to
disambiguate it from possible variable names.  The option *--*
indicates the end of the options, and should be used if you wish to
remove a variable with the same name as any of the options.
If an error occurs, any variables after the named one causing the error
are not
deleted.  An error can occur when the named variable does not exist, or the
name refers to an array element but the variable is a scalar, or the name
refers to a variable in a non-existent namespace.

## EXAMPLE

Create an array containing a mapping from some numbers to their
squares and remove the array elements for non-prime numbers:

```
array set squares \{
    1 1    6 36
    2 4    7 49
    3 9    8 64
    4 16   9 81
    5 25  10 100
\}

puts "The squares are:"
parray squares

**unset** squares(1) squares(4) squares(6)
**unset** squares(8) squares(9) squares(10)

puts "The prime squares are:"
parray squares
```


## SEE ALSO

set(n), trace(n), upvar(n)

## KEYWORDS

remove, variable
