+++
manpage_section = "n"
operating_system_version = "15.3"
author = "None Specified"
description = "This package provides megawidgets based on the snit oo system (snidgets). It makes use of the Tile/Ttk themed widget set. widget::validate as ?options? commands: widget::calendar pathname ?options? options: widget::dateentry pathname ?options? ..."
operating_system = "macos"
detected_package_version = "1.1"
manpage_format = "troff"
date = "Sun Feb 16 04:48:28 2025"
manpage_name = "widget"
title = "widget(n)"
+++

"widget" n 3.0 widget "Megawidget package"

---

## NAME

widget - Megawidget package

## SYNOPSIS

package require **Tcl  8.4**

package require **Tk  8.4**

package require **widget  ?3.0?**

package require **snit **

**widget::validate** *as* ?options?

**widget::calendar** *pathname* ?options?

**widget::dateentry** *pathname* ?options?

**widget::dialog** *pathname* ?options?

**widget::menuentry** *pathname* ?options?

**widget::panelframe** *pathname* ?options?

**widget::ruler** *pathname* ?options?

**widget::screenruler** *pathname* ?options?

**widget::scrolledwindow** *pathname* ?options?

**widget::statusbar** *pathname* ?options?

**widget::superframe** *pathname* ?options?

**widget::toolbar** *pathname* ?options?


---


## DESCRIPTION

This package provides megawidgets based on the snit oo system (snidgets).
It makes use of the Tile/Ttk themed widget set.


**widget::validate** *as* ?options?
commands:


## WIDGETS


**widget::calendar** *pathname* ?options?
options:

**widget::dateentry** *pathname* ?options?
options:

**widget::dialog** *pathname* ?options?
options:

**widget::menuentry** *pathname* ?options?
options:

**widget::panelframe** *pathname* ?options?
options:

**widget::ruler** *pathname* ?options?
options:

**widget::screenruler** *pathname* ?options?
options:

**widget::scrolledwindow** *pathname* ?options?
options:

**widget::statusbar** *pathname* ?options?
options:

**widget::superframe** *pathname* ?options?
options:

**widget::toolbar** *pathname* ?options?
options:


## EXAMPLE


```

package require widget::superframe ; # or widget::all
pack [widget::superframe .f -type separator -text "SuperFrame:"]

```


## KEYWORDS

megawidget, snit, widget
