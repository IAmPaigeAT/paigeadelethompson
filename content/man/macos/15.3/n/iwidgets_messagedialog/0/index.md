+++
manpage_name = "iwidgets_messagedialog"
detected_package_version = "0"
description = "The iwidgets::messagedialog command creates a message dialog composite widget.  The messagedialog is derived from the Dialog class and is composed of  an image and associated message text with commands to manipulate the  dialog buttons."
title = "iwidgets_messagedialog(n)"
operating_system_version = "15.3"
date = "Sun Feb 16 04:48:29 2025"
operating_system = "macos"
author = "None Specified"
manpage_format = "troff"
manpage_section = "n"
+++

iwidgets::messagedialog iwid

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

iwidgets::messagedialog - Create and manipulate a message dialog widget

## SYNOPSIS

**iwidgets::messagedialog** *pathName *?*options*?

## INHERITANCE

itk::Toplevel <- iwidgets::Shell <- iwidgets::Dialogshell <- iwidgets::Dialog <- iwidgets::Messagedialog

## STANDARD OPTIONS



```
.ta 4c 8c 12c
background	bitmap	cursor	font
foreground	image	text
```


See the "options" manual entry for details on the standard options.

## INHERITED OPTIONS



```
.ta 4c 8c 12c
buttonBoxPadX	buttonBoxPadY	buttonBoxPos	padX
padY	separator	thickness	
```


See the "dialogshell" widget manual entry for details on the above
inherited options.


```
.ta 4c 8c 12c
master	modality
```


See the "shell" widget manual entry for details on the above
inherited options.


```
.ta 4c 8c 12c
title
```


See the "Toplevel" widget manual entry for details on the above
inherited options.


## WIDGET-SPECIFIC OPTIONS



```
Name:	imagePos
Class:	Position
Command-Line Switch:	-imagepos
```

.IP
Specifies the image position relative to the message text: **n**, **s**,
**e**, or **w**.  The default is w.


```
Name:	textPadX
Class:	Pad
Command-Line Switch:	-textpadx
```

.IP
Specifies a non-negative value indicating how much extra space to request for
the message text in the X direction.  The value may have any of the forms
acceptable to Tk_GetPixels.


```
Name:	textPadY
Class:	Pad
Command-Line Switch:	-textpady
```

.IP
Specifies a non-negative value indicating how much extra space to request for
the message text in the X direction.  The value may have any of the forms
acceptable to Tk_GetPixels.


---



## DESCRIPTION


The **iwidgets::messagedialog** command creates a message dialog composite widget.
The messagedialog is derived from the Dialog class and is composed of
an image and associated message text with commands to manipulate the
dialog buttons.


## METHODS


The **iwidgets::messagedialog** command creates a new Tcl command whose
name is *pathName*.  This
command may be used to invoke various
operations on the widget.  It has the following general form:

> *pathName option *?*arg arg ...*?


*Option* and the *arg*s
determine the exact behavior of the command.  The following
commands are possible for messagedialog widgets:


## INHERITED METHODS



```
.ta 4c 8c 12c
add	buttonconfigure	default	hide
insert	invoke	show
```


See the "buttonbox" widget manual entry for details on the above
inherited methods.


```
.ta 4c 8c 12c
childsite	
```


See the "dialogshell" widget manual entry for details on the above
inherited methods.


```
.ta 4c 8c 12c
activate	center	deactivate
```


See the "dialogshell" widget manual entry for details on the above
inherited methods.

## WIDGET-SPECIFIC METHODS


*pathName **cget** \fIoption*
Returns the current value of the configuration option given
by *option*.
*Option* may have any of the values accepted by the **iwidgets::messagedialog**
command.

*pathName* **configure** ?*option*? ?*value option value ...*?
Query or modify the configuration options of the widget.
If no *option* is specified, returns a list describing all of
the available options for *pathName* (see **Tk_ConfigureInfo** for
information on the format of this list).  If *option* is specified
with no *value*, then the command returns a list describing the
one named option (this list will be identical to the corresponding
sublist of the value returned if no *option* is specified).  If
one or more *option-value* pairs are specified, then the command
modifies the given widget option(s) to have the given value(s);  in
this case the command returns an empty string.
*Option* may have any of the values accepted by the **iwidgets::messagedialog**
command.


## COMPONENTS



```
Name:	image
Class:	Label
```

.IP
The image component is the bitmap or image of the message dialog.  See
the "label" widget manual entry for details on the image component item.


```
Name:	message
Class:	Label
```

.IP
The message component provides the textual portion of the message dialog.
See the "label" widget manual entry for details on the message component item.



## EXAMPLE


>  package require Iwidgets 4.0
 #
 # Standard question message dialog used for confirmation.
 #
 iwidgets::messagedialog .md -title "Message Dialog" -text "Are you sure ?" \
	-bitmap questhead -modality global

 .md buttonconfigure OK -text Yes
 .md buttonconfigure Cancel -text No

 if \{[.md activate]\} \{
    .md configure -text "Are you really sure ?"
    if \{[.md activate]\} \{
	puts stdout "Yes"
    \} else \{
	puts stdout "No"
    \}
 \} else \{
    puts stdout "No"
 \}

 destroy .md

 #
 # Copyright notice with automatic deactivation.
 #
 iwidgets::messagedialog .cr -title "Copyright" -bitmap @dsc.xbm -imagepos n \
     -text "Copyright 1995 DSC Communications Corporation\n \
	    All rights reserved"

 .cr hide Cancel

 .cr activate
 after 10000 ".cr deactivate"



## AUTHOR

Mark L. Ulferts

## KEYWORDS

messagedialog, dialog, dialogshell, shell, widget
