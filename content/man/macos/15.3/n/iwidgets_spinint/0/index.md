+++
manpage_section = "n"
date = "Sun Feb 16 04:48:30 2025"
operating_system = "macos"
author = "None Specified"
description = "The iwidgets::spinint command creates a spinint widget.  The spinint allows spinning of integer values within a specified range with wrap support. The spinner arrows may be drawn horizontally or vertically."
manpage_name = "iwidgets_spinint"
manpage_format = "troff"
operating_system_version = "15.3"
detected_package_version = "0"
title = "iwidgets_spinint(n)"
+++

iwidgets::spinint iwid

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

iwidgets::spinint - Create and manipulate a integer spinner widget

## SYNOPSIS

**iwidgets::spinint** *pathName *?*options*?

## INHERITANCE

itk::Widget <- iwidgets::Labeledwidget <- iwidgets::Spinner <- iwidgets::Spinint

## STANDARD OPTIONS



```
.ta 4c 8c 12c

background         borderWidth     cursor              exportSelection
foreground         highlightColor  highlightThickness  insertBackground
insertBorderWidth  insertOffTime   insertOnTime        insertWidth
justify            relief          selectBackground    selectBorderWidth
selectForeground   textVariable
```


See the "options" manual entry for details on the standard options.

## ASSOCIATED OPTIONS



```
.ta 4c 8c 12c
show	state
```


See the "entry" manual entry for details on the associated options.

## INHERITED OPTIONS



```
.ta 4c 8c 12c
command	childSitePos	fixed	focusCommand
invalid	textBackground	textFont	validate
width
```


See the "entryfield" widget manual entry for details on the above
inherited options.


```
.ta 4c 8c 12c
disabledForeground	labelBitmap	labelFont	labelImage
labelMargin	labelPos	labelText	labelVariable
state	sticky
```


See the "labeledwidget" widget manual entry for details on the above
inherited options.


```
.ta 4c 8c 12c
arroworient	decrement	increment	repeatDelay
repeatInterval
```


See the "spinner" widget manual entry for details on the above
inherited options.


## WIDGET-SPECIFIC OPTIONS



```
Name:	range
Class:	Range
Command-Line Switch:	-range
```

.IP
Specifies a two element list of minimum and maximum integer values.  The
default is no range, \{\{\} \{\}\}.


```
Name:	step
Class:	Step
Command-Line Switch:	-step
```

.IP
Specifies the increment/decrement value.  The default is 1.


```
Name:	wrap
Class:	Wrap
Command-Line Switch:	-wrap
```

.IP
Specifies whether to wrap the spinner value upon reaching the minimum
or maximum value in any of the forms acceptable to **Tcl_GetBoolean**.
The default is true.


---



## DESCRIPTION


The **iwidgets::spinint** command creates a spinint widget.  The spinint allows
"spinning" of integer values within a specified range with wrap support.
The spinner arrows may be drawn horizontally or vertically.




## METHODS


The **iwidgets::spinint** command creates a new Tcl command whose
name is *pathName*.  This
command may be used to invoke various
operations on the widget.  It has the following general form:

> *pathName option *?*arg arg ...*?


*Option* and the *arg*s
determine the exact behavior of the command.  The following
commands are possible for spinint widgets:

## ASSOCIATED METHODS



```
.ta 4c 8c 12c
delete	get	icursor	index	
insert	peek	scan	selection
xview	
```


See the "entry" manual entry for details on the associated methods.

## INHERITED METHODS



```
.ta 4c 8c 12c
childsite	clear
```


See the "entryfield" manual entry for details on the associated methods.

## WIDGET-SPECIFIC METHODS


*pathName **cget** \fIoption*
Returns the current value of the configuration option given
by *option*.
*Option* may have any of the values accepted by the **iwidgets::spinint**
command.

*pathName* **configure** ?*option*? ?*value option value ...*?
Query or modify the configuration options of the widget.
If no *option* is specified, returns a list describing all of
the available options for *pathName* (see **Tk_ConfigureInfo** for
information on the format of this list).  If *option* is specified
with no *value*, then the command returns a list describing the
one named option (this list will be identical to the corresponding
sublist of the value returned if no *option* is specified).  If
one or more *option-value* pairs are specified, then the command
modifies the given widget option(s) to have the given value(s);  in
this case the command returns an empty string.
*Option* may have any of the values accepted by the **iwidgets::spinint**
command.

\fIpathName **down**
Decrement the spinner value by the value given in the step option.

\fIpathName **up**
Increment the spinner value by the value given in the step option.


## COMPONENTS

.IP
See the "Spinner" widget manual entry for details on the integer spinner
component items.


## EXAMPLE


>  package require Iwidgets 4.0
 option add *textBackground white

 iwidgets::spinint .si -labeltext "Temperature" -labelpos w \
    -fixed yes -width 5 -range \{32 212\}

 pack .si -pady 10



## AUTHOR

Sue Yockey

## KEYWORDS

spinint, widget
