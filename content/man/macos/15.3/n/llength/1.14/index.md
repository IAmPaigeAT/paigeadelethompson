+++
title = "llength(n)"
author = "None Specified"
operating_system = "macos"
description = "Treats list as a list and returns a decimal string giving the number of elements in it."
date = "Sun Feb 16 04:48:30 2025"
manpage_section = "n"
manpage_format = "troff"
detected_package_version = "1.14"
operating_system_version = "15.3"
manpage_name = "llength"
keywords = ["list", "lappend", "lindex", "linsert", "lsearch", "lset", "lsort", "lrange", "lreplace", "keywords", "element", "length"]
+++

llength n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

llength - Count the number of elements in a list

## SYNOPSIS

**llength **list

---



## DESCRIPTION


Treats *list* as a list and returns a decimal string giving
the number of elements in it.


## EXAMPLES

The result is the number of elements:

```
% **llength** \{a b c d e\}
5
% **llength** \{a b c\}
3
% **llength** \{\}
0
```


Elements are not guaranteed to be exactly words in a dictionary sense
of course, especially when quoting is used:

```
% **llength** \{a b \{c d\} e\}
4
% **llength** \{a b \{ \} c d e\}
6
```


An empty list is not necessarily an empty string:

```
% set var \{ \}; puts "[string length $var],[**llength** $var]"
1,0
```



## SEE ALSO

list(n), lappend(n), lindex(n), linsert(n), lsearch(n),
lset(n), lsort(n), lrange(n), lreplace(n)


## KEYWORDS

element, list, length
