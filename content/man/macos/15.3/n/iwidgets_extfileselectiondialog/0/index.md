+++
author = "None Specified"
detected_package_version = "0"
title = "iwidgets_extfileselectiondialog(n)"
description = "The iwidgets::extfileselectiondialog command creates an extended file  selection dialog which is slightly different than the  fileselectiondialog widget. The differences are mostly cosmetic in that the listboxes are  within a panedwindow and the e..."
operating_system_version = "15.3"
manpage_name = "iwidgets_extfileselectiondialog"
manpage_format = "troff"
date = "Sun Feb 16 04:48:30 2025"
operating_system = "macos"
manpage_section = "n"
+++

iwidgets::extfileselectiondialog iwid

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

iwidgets::extfileselectiondialog - Create and manipulate a file selection dialog widget

## SYNOPSIS

**iwidgets::extfileselectiondialog** *pathName *?*options*?

## INHERITANCE

itk::Toplevel <- iwidgets::Shell <- iwidgets::Dialogshell <- iwidgets::Dialog <- iwidgets::Extfileselectiondialog

## STANDARD OPTIONS



```
.ta 4c 8c 12c

activeBackground   background         borderWidth         cursor
foreground         highlightColor     highlightThickness  insertBackground
insertBorderWidth  insertOffTime      insertOnTime        insertWidth
selectBackground   selectBorderWidth  selectForeground
```


See the "options" manual entry for details on the standard options.

## ASSOCIATED OPTIONS



```
.ta 4c 8c 12c
popupCursor	textBackground	textFont
```


See the "combobox" widget manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
childSitePos	directory	dirsLabel	dirSearchCommand
dirsOn	filesLabel	filesLabelOn	fileSearchCommand
filesOn	fileType	filterLabel	filterOn
invalid	mask	noMatchString	selectionLabel
selectionOn
```


See the "extfileselectionbox" widget manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
labelFont
```


See the "labeledwidget" widget manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
sashCursor
```


See the "panedwindow" widget manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
labelFont
```


See the "labeledwidget" widget manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
activeRelief	elementBorderWidth	jump	troughColor
```


See the "scrollbar" widget class manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
textBackground	textFont
```


See the "scrolledlistbox" widget manual entry for details on the above
associated options.


## INHERITED OPTIONS



```
.ta 4c 8c 12c
buttonBoxPadX	buttonBoxPadY	buttonBoxPos	padX
padY	separator	thickness
```


See the "dialogshell" widget manual entry for details on the above
inherited options.


```
.ta 4c 8c 12c
height	master	modality	width
```


See the "shell" widget manual entry for details on the above
inherited options.


```
.ta 4c 8c 12c
title 
```


See the "Toplevel" widget  manual entry for details on the above
inherited options.

---



## DESCRIPTION


The **iwidgets::extfileselectiondialog** command creates an extended file
selection dialog which is slightly different than the
fileselectiondialog widget.
The differences are mostly cosmetic in that the listboxes are
within a panedwindow and the entryfields for the filter and selection
have been replaced by comboboxes.  Other than that the interface is
practically the same.


## METHODS


The **iwidgets::extfileselectiondialog** command creates a new Tcl command whose
name is *pathName*.  This
command may be used to invoke various
operations on the widget.  It has the following general form:

> *pathName option *?*arg arg ...*?


*Option* and the *arg*s
determine the exact behavior of the command.  The following
commands are possible for extfileselectiondialog widgets:

## ASSOCIATED METHODS



```
.ta 4c 8c 12c
get	childsite	filter
```


See the "fileselectionbox" class manual entry for details on the
associated methods.

## INHERITED METHODS



```
.ta 4c 8c 12c
add	buttonconfigure	default	hide
insert	invoke	show	
```


See the "buttonbox" widget manual entry for details on the above
inherited methods.


```
.ta 4c 8c 12c
activate	center	deactivate
```


See the "shell" widget manual entry for details on the above
inherited methods.


## WIDGET-SPECIFIC METHODS


*pathName **cget** \fIoption*
Returns the current value of the configuration option given
by *option*.
*Option* may have any of the values accepted by
the **iwidgets::extfileselectiondialog** command.

*pathName* **configure** ?*option*? ?*value option value ...*?
Query or modify the configuration options of the widget.
If no *option* is specified, returns a list describing all of
the available options for *pathName* (see **Tk_ConfigureInfo** for
information on the format of this list).  If *option* is specified
with no *value*, then the command returns a list describing the
one named option (this list will be identical to the corresponding
sublist of the value returned if no *option* is specified).  If
one or more *option-value* pairs are specified, then the command
modifies the given widget option(s) to have the given value(s);  in
this case the command returns an empty string.
*Option* may have any of the values accepted by
the **iwidgets::extfileselectiondialog**
command.


## COMPONENTS



```
Name:	fsb
Class:	Fileselectionbox
```

.IP
The fsb component is the extfileselectionbox for the extfileselectiondialog.
See the "extfileselectionbox" widget manual entry for details on the fsb
component item.



## EXAMPLE


>  package require Iwidgets 4.0
 #
 # Non-modal example
 #
 proc okCallback \{\} \{
     puts "You selected [.nmfsd get]"
     .nmfsd deactivate
 \}

 iwidgets::extfileselectiondialog .nmfsd -title Non-Modal
 .nmfsd buttonconfigure OK -command okCallback

 .nmfsd activate

 #
 # Modal example
 #
 iwidgets::extfileselectiondialog .mfsd -modality application
 .mfsd center

 if \{[.mfsd activate]\} \{
     puts "You selected [.mfsd get]"
 \} else \{
     puts "You cancelled the dialog"
 \}



## AUTHOR

Mark L. Ulferts


Anthony L. Parent

## KEYWORDS

extfileselectiondialog, extfileselectionbox, dialog, dialogshell, shell, widget
