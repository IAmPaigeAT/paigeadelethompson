+++
manpage_format = "troff"
operating_system_version = "15.3"
manpage_section = "n"
author = "None Specified"
title = "ttk::combobox(n)"
manpage_name = "ttk::combobox"
keywords = ["ttk", "widget", "entry", "keywords", "choice", "list", "box", "text", "local", "variables", "mode", "nroff", "end"]
date = "Sun Feb 16 04:48:31 2025"
description = "A ttk::combobox combines a text field with a pop-down list of values; the user may select the value of the text field from among the  values in the list. -class-cursor-takefocus -style Boolean value. If set, the widget selection is linked to t..."
operating_system = "macos"
detected_package_version = "1.9"
+++

ttk::combobox n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::combobox - text field with popdown selection list

## SYNOPSIS

**ttk::combobox** *pathName *?*options*?

---


## DESCRIPTION


A **ttk::combobox** combines a text field with a pop-down list of values;
the user may select the value of the text field from among the
values in the list.

### Standard ttk_widget

-class	-cursor	-takefocus
-style

See the standard options manual entry for details.


## WIDGET-SPECIFIC OPTIONS

Boolean value.
If set, the widget selection is linked to the X selection.
Specifies how the text is aligned within the widget.
Must be one of **left**, **center**, or **right**.
Specifies the height of the pop-down listbox, in rows.
A Tcl script to evaluate immediately before displaying the listbox.
The **-postcommand** script may specify the **-values** to display.
One of **normal**, **readonly**, or **disabled**.
In the **readonly** state,
the value may not be edited directly, and
the user can only select one of the **-values** from the
dropdown list.
In the **normal** state,
the text field is directly editable.
In the **disabled** state, no interaction is possible.
Specifies the name of a variable whose value is linked
to the widget value.
Whenever the variable changes value the widget value is updated,
and vice versa.
Specifies the list of values to display in the drop-down listbox.
Specifies an integer value indicating the desired width of the entry window,
in average-size characters of the widget's font.

## WIDGET COMMAND


The following subcommands are possible for combobox widgets:
'\".TP
'\"*pathName **cget** \fIoption*
'\"Returns the current value of the specified *option*.
'\"See *ttk::widget(n)*.
'\".TP
'\"*pathName **configure** ?*option*? ?\fIvalue option value ...*?
'\"Modify or query widget options.
'\"See *ttk::widget(n)*.

*pathName **current** ?\fInewIndex*?
If *newIndex* is supplied, sets the combobox value
to the element at position *newIndex* in the list of **-values**.
Otherwise, returns the index of the current value in the list of
**-values** or **-1** if the current value does not appear in the list.

\fIpathName **get**
Returns the current value of the combobox.
'\".TP
'\"*pathName **identify **x y*
'\"Returns the name of the element at position *x*, *y*.
'\"See *ttk::widget(n)*.
'\".TP
'\"*pathName **instate **stateSpec* ?*script*?
'\"Test the widget state.
'\"See *ttk::widget(n)*.

*pathName **set** \fIvalue*
Sets the value of the combobox to *value*.
'\".TP
'\"*pathName **state** ?\fIstateSpec*?
'\"Modify or query the widget state.
'\"See *ttk::widget(n)*.

The combobox widget also supports the following **ttk::entry**
widget subcommands (see *ttk::entry(n)* for details):

> .ta 5.5c 11c
**bbox**	**delete**	**icursor**
**index**	**insert**	**selection**
**xview**


The combobox widget also supports the following generic **ttk::widget**
widget subcommands (see *ttk::widget(n)* for details):

> .ta 5.5c 11c
**cget**	**configure**	**identify**
**instate**	**state**



## VIRTUAL EVENTS


The combobox widget generates a **<<ComboboxSelected>>** virtual event
when the user selects an element from the list of values.
If the selection action unposts the listbox,
this event is delivered after the listbox is unposted.

## SEE ALSO

ttk::widget(n), ttk::entry(n)

## KEYWORDS

choice, entry, list box, text box, widget
'\" Local Variables:
'\" mode: nroff
'\" End:
