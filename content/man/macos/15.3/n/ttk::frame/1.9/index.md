+++
operating_system_version = "15.3"
manpage_name = "ttk::frame"
keywords = ["ttk", "widget", "labelframe", "frame", "keywords", "container", "local", "variables", "mode", "nroff", "end"]
author = "None Specified"
detected_package_version = "1.9"
operating_system = "macos"
title = "ttk::frame(n)"
date = "Sun Feb 16 04:48:31 2025"
description = "A ttk::frame widget is a container, used to group other widgets together. -class-cursor-takefocus -style The desired width of the widget border.  Defaults to 0.  One of the standard Tk border styles:  flat, groove, raised, ridge, solid, or su..."
manpage_format = "troff"
manpage_section = "n"
+++

ttk::frame n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::frame - Simple container widget

## SYNOPSIS

**ttk::frame** *pathName *?*options*?

---


## DESCRIPTION


A **ttk::frame** widget is a container, used to group other widgets
together.

### Standard ttk_widget

-class	-cursor	-takefocus
-style

See the standard options manual entry for details.


## WIDGET-SPECIFIC OPTIONS

The desired width of the widget border.  Defaults to 0.
One of the standard Tk border styles:
**flat**, **groove**, **raised**, **ridge**,
**solid**, or **sunken**.
Defaults to **flat**.
Additional padding to include inside the border.
If specified, the widget's requested width in pixels.
If specified, the widget's requested height in pixels.

## WIDGET COMMAND


Supports the standard widget commands
**configure**, **cget**, **identify**, **instate**, and **state**;
see *ttk::widget(n)*.

## NOTES


Note that if the **pack**, **grid**, or other geometry managers
are used to manage the children of the **frame**,
by the GM's requested size will normally take precedence
over the **frame** widget's **-width** and **-height** options.
**pack propagate** and **grid propagate** can be used
to change this.

## SEE ALSO

ttk::widget(n), ttk::labelframe(n), frame(n)

## KEYWORDS

widget, frame, container
'\" Local Variables:
'\" mode: nroff
'\" End:
