+++
detected_package_version = "1.9"
author = "None Specified"
date = "Sun Feb 16 04:48:29 2025"
manpage_name = "pkg::create"
manpage_section = "n"
manpage_format = "troff"
keywords = ["package", "keywords", "auto-load", "index", "version"]
description = "::pkg::create is a utility procedure that is part of the standard Tcl library.  It is used to create an appropriate package ifneeded command for a given package specification.  It can be used to construct a pkgIndex.tcl file for use with the packag..."
operating_system = "macos"
title = "pkg::create(n)"
operating_system_version = "15.3"
+++

pkg::create n 8.3 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

pkg::create - Construct an appropriate 'package ifneeded' command for a given package specification

## SYNOPSIS

**::pkg::create **-name packageName *-version packageVersion* ?*-load filespec*? ... ?*-source filespec*? ...

---



## DESCRIPTION


**::pkg::create** is a utility procedure that is part of the standard Tcl
library.  It is used to create an appropriate **package ifneeded**
command for a given package specification.  It can be used to construct a
**pkgIndex.tcl** file for use with the **package** mechanism.


## OPTIONS

The parameters supported are:

**-name**\0*packageName*
This parameter specifies the name of the package.  It is required.

**-version**\0*packageVersion*
This parameter specifies the version of the package.  It is required.

**-load**\0*filespec*
This parameter specifies a binary library that must be loaded with the
**load** command.  *filespec* is a list with two elements.  The
first element is the name of the file to load.  The second, optional
element is a list of commands supplied by loading that file.  If the
list of procedures is empty or omitted, **::pkg::create** will
set up the library for direct loading (see **pkg_mkIndex**).  Any
number of **-load** parameters may be specified.

**-source**\0*filespec*
This parameter is similar to the **-load** parameter, except that it
specifies a Tcl library that must be loaded with the
**source** command.  Any number of **-source** parameters may be
specified.

At least one **-load** or **-source** parameter must be given.

## SEE ALSO

package(n)

## KEYWORDS

auto-load, index, package, version
