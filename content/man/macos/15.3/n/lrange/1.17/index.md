+++
keywords = ["list", "lappend", "lindex", "linsert", "llength", "lsearch", "lset", "lreplace", "lsort", "8", "5", "string", "keywords", "element", "range", "sublist"]
description = "List must be a valid Tcl list.  This command will return a new list consisting of elements first through last, inclusive. The index values first and last are interpreted the same as index values for the command string index, supporting simple ind..."
operating_system_version = "15.3"
detected_package_version = "1.17"
manpage_section = "n"
operating_system = "macos"
manpage_name = "lrange"
title = "lrange(n)"
date = "Sun Feb 16 04:48:31 2025"
manpage_format = "troff"
author = "None Specified"
+++

lrange n 7.4 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

lrange - Return one or more adjacent elements from a list

## SYNOPSIS

**lrange **list first last

---



## DESCRIPTION


*List* must be a valid Tcl list.  This command will
return a new list consisting of elements
*first* through *last*, inclusive.

>  (Added in version 8.5)
The index values *first* and *last* are interpreted
the same as index values for the command **string index**,
supporting simple index arithmetic and indices relative to the
end of the list.


If *first* is less than zero, it is treated as if it were zero.
If *last* is greater than or equal to the number of elements
in the list, then it is treated as if it were **end**.
If *first* is greater than *last* then an empty string
is returned.
Note:
""\fBlrange" \fIlistdoes not always produce the same result as
""\fBlindex" \fIlist(although it often does for simple fields that are not enclosed in
braces); it does, however, produce exactly the same results as
""\fBlist" [lindex
## EXAMPLES

Selecting the first two elements:

```
% **lrange** \{a b c d e\} 0 1
a b
```


Selecting the last three elements:

```
% **lrange** \{a b c d e\} end-2 end
c d e
```


Selecting everything except the first and last element:

```
% **lrange** \{a b c d e\} 1 end-1
b c d
```


Selecting a single element with **lrange** is not the same as doing
so with **lindex**:

```
% set var \{some \{elements to\} select\}
some \{elements to\} select
% lindex $var 1
elements to
% **lrange** $var 1 1
\{elements to\}
```



## SEE ALSO

list(n), lappend(n), lindex(n), linsert(n), llength(n), lsearch(n),
lset(n), lreplace(n), lsort(n),

>  (Added in version 8.5)
string(n)




## KEYWORDS

element, list, range, sublist
