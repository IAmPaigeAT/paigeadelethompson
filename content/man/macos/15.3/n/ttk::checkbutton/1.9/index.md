+++
title = "ttk::checkbutton(n)"
operating_system = "macos"
author = "None Specified"
operating_system_version = "15.3"
manpage_format = "troff"
keywords = ["ttk", "widget", "radiobutton", "checkbutton", "keywords", "button", "toggle", "check", "option", "local", "variables", "mode", "nroff", "end"]
manpage_name = "ttk::checkbutton"
date = "Sun Feb 16 04:48:27 2025"
manpage_section = "n"
detected_package_version = "1.9"
description = "A ttk::checkbutton widget is used to show or change a setting. It has two states, selected and deselected.   The state of the checkbutton may be linked to a Tcl variable. -class-compound-cursor -image-state-style -takefocus-text-textvariabl..."
+++

ttk::checkbutton n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::checkbutton - On/off widget

## SYNOPSIS

**ttk::checkbutton** *pathName *?*options*?

---


## DESCRIPTION

A **ttk::checkbutton** widget is used to show or change a setting.
It has two states, selected and deselected.
The state of the checkbutton may be linked to a Tcl variable.

### Standard ttk_widget

-class	-compound	-cursor
-image	-state	-style
-takefocus	-text	-textvariable
-underline	-width

See the standard options manual entry for details.


## WIDGET-SPECIFIC OPTIONS

A Tcl script to execute whenever the widget is invoked.
The value to store in the associated **-variable**
when the widget is deselected.  Defaults to **0**.
The value to store in the associated **-variable**
when the widget is selected.  Defaults to **1**.
The name of a global variable whose value is linked to the widget.
Defaults to the widget pathname if not specified.

## WIDGET COMMAND


In addition to the standard
**cget**, **configure**, **identify**, **instate**, and **state**
commands, checkbuttons support the following additional
widget commands:

\fIpathname** invoke**
Toggles between the selected and deselected states
and evaluates the associated **-command**.
If the widget is currently selected, sets the **-variable**
to the **-offvalue** and deselects the widget;
otherwise, sets the **-variable** to the **-onvalue**
Returns the result of the **-command**.

## WIDGET STATES


The widget does not respond to user input if the **disabled** state is set.
The widget sets the **selected** state whenever
the linked **-variable** is set to the widget's **-onvalue**,
and clears it otherwise.
The widget sets the **alternate** state whenever the
linked **-variable** is unset.
(The **alternate** state may be used to indicate a
"tri-state"or
"indeterminate"selection.)

## STANDARD STYLES


**Ttk::checkbutton** widgets support the **Toolbutton** style in all
standard themes, which is useful for creating widgets for toolbars.

## SEE ALSO

ttk::widget(n), ttk::radiobutton(n), checkbutton(n)

## KEYWORDS

widget, button, toggle, check, option
'\" Local Variables:
'\" mode: nroff
'\" End:
