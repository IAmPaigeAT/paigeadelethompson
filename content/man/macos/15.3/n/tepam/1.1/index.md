+++
operating_system_version = "15.3"
manpage_name = "tepam"
description = "This document is an informal introduction into version 0.1 of TEPAM, the Tcls Enhanced Procedure and Argument Manager. Detailed information to the TEPAM package is provided in the tepam::procedure and tepam::argument_dialogbox reference manuals. Thi..."
date = "Sun Feb 16 04:48:30 2025"
title = "tepam(n)"
keywords = ["tepam_argument_dialogbox", "tepam_procedure", "keywords", "argument", "integrity", "validation", "arguments", "entry", "mask", "parameter", "form", "procedure", "subcommand", "category", "procedures", "parameters", "options", "copyright", "c", "2009", "2010", "andreas", "drollinger"]
author = "None Specified"
operating_system = "macos"
manpage_format = "troff"
manpage_section = "n"
detected_package_version = "1.1"
+++


```
display message -level 12 Hello
  -> display message: Argument (level) has to be between 1 and 10
```


## INTERACTIVE ARGUMENT DEFINITION

The most intuitive way to call the procedure is using an interactive form that allows specifying all arguments. This form will automatically be generated when the declared procedure is called with the *-interactive* flag.

```
display message -interactive
```

The generated form contains for each argument a data entry widget that is adapted to the argument type. Check buttons are used to specify flags, radio boxes for tiny choice lists, disjoint list boxes for larger choice lists and files, directories, fonts and colors can be selected with dedicated browsers.

After acknowledging the specified argument data via an OK button, the entered data are first validated, before the provided arguments are transformed into local variables and the procedure body is executed. In case the entered data are invalid, a message appears and the user can correct them until they are valid.

## FLEXIBLE ARGUMENT DIALOG BOX

The form generator that creates in the previous example the argument dialog box for the interactive procedure call is also available for other purposes than for the definition of procedure arguments. Even if Tk is well known for its code efficient way to build GUIs, the presented argument dialog box allows crating complex parameter definition forms in a still much more efficient way.

The following example tries to illustrate the simplicity to create complex data entry forms. It creates an input mask that allows specifying a file to copy, a destination folder as well as a checkbox that allows specifying if an eventual existing file can be overwritten. Comfortable browsers can be used to select files and directories. And finally, the form offers also the possibility to accept and decline the selection. Here is the code snippet that is doing all this:

```
tepam::argument_dialogbox \\
   -existingfile {-label "Source file" -variable SourceFile} \\
   -existingdirectory {-label "Destination folder" -variable DestDir} \\
   -checkbutton {-label "Overwrite existing file" -variable Overwrite}
```

The **argument_dialogbox** returns **ok** when the entered data are validated and **cancel** when the data entry has been canceled. After the validation of the entered data, the **argument_dialogbox** defines all the specified variables with the entered data inside the calling context.

A pair of arguments has to be provided to **argument_dialogbox** for each variable that has to be specified by this last one. The first argument defines the entry widget type to use to select the variable's data and the second one is a lists of attributes related to the variable and the entry widget.

Many entry widget types are available: Beside the simple generic entries, there are different kinds of list and combo boxes available, browsers for existing and new files and directories, check and radio boxes and buttons, as well as color and font pickers. If necessary, additional entry widget types can be defined.

The attribute list contains pairs of attribute names and attribute data. The primary attribute is *-variable* used to specify the variable in the calling context into which the entered data has to be stored. Another often used attribute is *-label* that allows adding a label to the data entry widget. Other attributes are available that allows specifying default values, the expected data types, valid data ranges, etc.

The next example of a more complex argument dialog box provides a good overview about the different available entry widget types and parameter attributes. The example contains also some formatting instructions like *-frame* and *-sep* which allows organizing the different entry widgets in frames and sections:

```
set ChoiceList {"Choice 1" "Choice 2" "Choice 3" "Choice 4" "Choice 5" "Choice 6"}

set Result [tepam::argument_dialogbox \\
   -title "System configuration" \\
   -context test_1 \\
   -frame {-label "Entries"} \\
      -entry {-label Entry1 -variable Entry1} \\
      -entry {-label Entry2 -variable Entry2 -default "my default"} \\
   -frame {-label "Listbox & combobox"} \\
      -listbox {-label "Listbox, single selection" -variable Listbox1 \\
                -choices {1 2 3 4 5 6 7 8} -default 1 -height 3} \\
      -listbox {-label "Listbox, multiple selection" -variable Listbox2
                -choicevariable ChoiceList -default {"Choice 2" "Choice 3"}
                -multiple_selection 1 -height 3} \\
      -disjointlistbox {-label "Disjoined listbox" -variable DisJntListbox
                        -choicevariable ChoiceList \\
                        -default {"Choice 3" "Choice 5"} -height 3} \\
      -combobox {-label "Combobox" -variable Combobox \\
                 -choices {1 2 3 4 5 6 7 8} -default 3} \\
   -frame {-label "Checkbox, radiobox and checkbutton"} \\
      -checkbox {-label Checkbox -variable Checkbox
                 -choices {bold italic underline} -choicelabels {Bold Italic Underline} \\
                 -default italic} \\
      -radiobox {-label Radiobox -variable Radiobox
                 -choices {bold italic underline} -choicelabels {Bold Italic Underline} \\
                 -default underline} \\
      -checkbutton {-label CheckButton -variable Checkbutton -default 1} \\
   -frame {-label "Files & directories"} \\
      -existingfile {-label "Input file" -variable InputFile} \\
      -file {-label "Output file" -variable OutputFile} \\
      -sep {} \\
      -existingdirectory {-label "Input directory" -variable InputDirectory} \\
      -directory {-label "Output irectory" -variable OutputDirectory} \\
   -frame {-label "Colors and fonts"} \\
      -color {-label "Background color" -variable Color -default red} \\
      -sep {} \\
      -font {-label "Font" -variable Font -default {Courier 12 italic}}]
```

The validation status is in this case stored inside the **Result** variable. If the entered data are validated, **Result** will contain **0** and the calling program can read the entered data via the variables that have been specified:

```
if {$Result=="cancel"} {
   puts "Canceled"
} else {
   puts "Arguments: "
   foreach Var {
      Entry1 Entry2
      Listbox1 Listbox2 DisJntListbox
      Combobox Checkbox Radiobox Checkbutton
      InputFile OutputFile InputDirectory OutputDirectory
      Color Font
   } {
      puts "  $Var: '[set $Var]'"
   }
}
-> Arguments:
   Entry1: 'Hello, this is a trial'
   Entry2: 'my default'
   Listbox1: '1'
   Listbox2: '{Choice 2} {Choice 3}'
   DisJntListbox: '{Choice 3} {Choice 5}'
   Combobox: '3'
   Checkbox: 'italic'
   Radiobox: 'underline'
   Checkbutton: '1'
   InputFile: 'c:\\tepam\\in.txt'
   OutputFile: 'c:\\tepam\\out.txt'
   InputDirectory: 'c:\\tepam\\input'
   OutputDirectory: 'c:\\tepam\\output'
   Color: 'red'
   Font: 'Courier 12 italic'
```


## SEE ALSO

tepam_argument_dialogbox(n), tepam_procedure(n)

## KEYWORDS

argument integrity, argument validation, arguments, entry mask, parameter entry form, procedure, subcommand

## CATEGORY

Procedures, arguments, parameters, options

## COPYRIGHT


```
Copyright (c) 2009/2010, Andreas Drollinger

```

