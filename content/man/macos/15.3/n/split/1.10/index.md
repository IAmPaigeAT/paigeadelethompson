+++
keywords = ["join", "list", "string", "keywords", "split"]
author = "None Specified"
description = "Returns a list created by splitting string at each character that is in the splitChars argument. Each element of the result list will consist of the characters from string that lie between instances of the characters in splitChars. Empty list ele..."
manpage_name = "split"
operating_system = "macos"
operating_system_version = "15.3"
manpage_section = "n"
title = "split(n)"
manpage_format = "troff"
detected_package_version = "1.10"
date = "Sun Feb 16 04:48:29 2025"
+++

split n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

split - Split a string into a proper Tcl list

## SYNOPSIS

**split **string ?*splitChars*?

---



## DESCRIPTION


Returns a list created by splitting *string* at each character
that is in the *splitChars* argument.
Each element of the result list will consist of the
characters from *string* that lie between instances of the
characters in *splitChars*.
Empty list elements will be generated if *string* contains
adjacent characters in *splitChars*, or if the first or last
character of *string* is in *splitChars*.
If *splitChars* is an empty string then each character of
*string* becomes a separate element of the result list.
*SplitChars* defaults to the standard white-space characters.

## EXAMPLES

Divide up a USENET group name into its hierarchical components:

```
**split** "comp.lang.tcl.announce" .
      *\(-> comp lang tcl announce*
```


See how the **split** command splits on *every* character in
*splitChars*, which can result in information loss if you are not
careful:

```
**split** "alpha beta gamma" "temp"
      *\(-> al \{ha b\} \{\} \{a ga\} \{\} a*
```


Extract the list words from a string that is not a well-formed list:

```
**split** "Example with \{unbalanced brace character"
      *\(-> Example with \\\{unbalanced brace character*
```


Split a string into its constituent characters

```
**split** "Hello world" \{\}
      *\(-> H e l l o \{ \} w o r l d*
```


### PARSING RECORD-ORIENTED FILES

Parse a Unix /etc/passwd file, which consists of one entry per line,
with each line consisting of a colon-separated list of fields:

```
## Read the file
set fid [open /etc/passwd]
set content [read $fid]
close $fid

## Split into records on newlines
set records [**split** $content "\\n"]

## Iterate over the records
foreach rec $records \{

   ## Split into fields on colons
   set fields [**split** $rec ":"]

   ## Assign fields to variables and print some out...
   lassign $fields \\
         userName password uid grp longName homeDir shell
   puts "$longName uses [file tail $shell] for a login shell"
\}
```



## SEE ALSO

join(n), list(n), string(n)


## KEYWORDS

list, split, string
