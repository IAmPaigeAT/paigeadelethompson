+++
operating_system = "macos"
title = "iwidgets_mainwindow(n)"
manpage_format = "troff"
date = "Sun Feb 16 04:48:28 2025"
detected_package_version = "0"
operating_system_version = "15.3"
author = "None Specified"
description = "The iwidgets::mainwindow command creates a mainwindow shell which contains a menubar, toolbar, mousebar, childsite, status line, and help line.   Each item may be filled and configured to suit individual needs."
manpage_section = "n"
manpage_name = "iwidgets_mainwindow"
+++

iwidgets::mainwindow iwid

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

iwidgets::mainwindow - Create and manipulate a mainwindow widget

## SYNOPSIS

**iwidgets::mainwindow** *pathName *?*options*?

## INHERITANCE

itk::Toplevel <- iwidgets::Shell <- iwidgets::Mainwindow

## STANDARD OPTIONS



```
.ta 4c 8c 12c

background  cursor               disabledForeground  font
foreground  highlightBackground  highlightColor      highlightThickness
```


See the "options" manual entry for details on the standard options.

## ASSOCIATED OPTIONS



```
.ta 4c 8c 12c
balloonBackground	balloonDelay1	balloonDelay2	ballonFont
balloonForeground
```


See the "toolbar" manual entry for details on the above associated options.

## INHERITED OPTIONS



```
.ta 4c 8c 12c
title 
```


See the "Toplevel" manual entry for details on the above inherited options.


```
.ta 4c 8c 12c
height	master	modality	padX
padY	width
```


See the "shell" manual entry for details on the above inherited options.


## WIDGET-SPECIFIC OPTIONS



```
Name:	helpLine
Class:	HelpLine
Command-Line Switch:	-helpline
```

.IP
Specifies whether or not to display the help line.  The value
may be given in any of the forms acceptable to Tk_GetBoolean.
The default is yes.


```
Name:	menuBarBackground
Class:	Background
Command-Line Switch:	-menubarbackground
```

.IP
Specifies the normal background color for the menubar.


```
Name:	menuBarFont
Class:	Font
Command-Line Switch:	-menubarfont
```

.IP
Specifies the font to use when drawing text inside the menubar.


```
Name:	menuBarForeround
Class:	Foreground
Command-Line Switch:	-menubarforeground
```

.IP
Specifies the normal foreground color for the menubar.


```
Name:	statusLine
Class:	StatusLine
Command-Line Switch:	-statusline
```

.IP
Specifies whether or not to display the status line.  The value
may be given in any of the forms acceptable to Tk_GetBoolean.
The default is yes.


```
Name:	toolBarBackground
Class:	Background
Command-Line Switch:	-toolbarbackground
```

.IP
Specifies the normal background color for the toolbar.


```
Name:	toolBarFont
Class:	Font
Command-Line Switch:	-toolbarfont
```

.IP
Specifies the font to use when drawing text inside the toolbar.


```
Name:	toolBarForeround
Class:	Foreground
Command-Line Switch:	-toolbarforeground
```

.IP
Specifies the normal foreground color for the toolbar.

---



## DESCRIPTION


The **iwidgets::mainwindow** command creates a mainwindow shell which contains
a menubar, toolbar, mousebar, childsite, status line, and help line.
Each item may be filled and configured to suit individual needs.


## METHODS


The **iwidgets::mainwindow** command create a new Tcl command whose
name is *pathName*.  This command may be used to invoke various
operations on the widget.  It has the following general form:

> *pathName option *?*arg arg ...*?


*Option* and the *arg*s
determine the exact behavior of the command.  The following
commands are possible for mainwindow widgets:

## INHERITED METHODS



```
.ta 4c 8c 12c
activate	center	deactivate
```


See the "shell" manual entry for details on the above inherited methods.

## WIDGET-SPECIFIC METHODS


*pathName **cget** \fIoption*
Returns the current value of the configuration option given
by *option*.
*Option* may have any of the values accepted by the **iwidgets::mainwindow**
command.

\fIpathName **childsite**
Returns the pathname of the child site widget.

*pathName* **configure** ?*option*? ?*value option value ...*?
Query or modify the configuration options of the widget.
If no *option* is specified, returns a list describing all of
the available options for *pathName* (see **Tk_ConfigureInfo** for
information on the format of this list).  If *option* is specified
with no *value*, then the command returns a list describing the
one named option (this list will be identical to the corresponding
sublist of the value returned if no *option* is specified).  If
one or more *option-value* pairs are specified, then the command
modifies the given widget option(s) to have the given value(s);  in
this case the command returns an empty string.
*Option* may have any of the values accepted by the **iwidgets::mainwindow**
command.

*pathName **menubar** ?\fIargs*?
The **menubar** method provides access to the menubar.  Invoked with
no arguments it returns the pathname of the menubar.  With arguments,
they are evaluated against the menubar which in effect provides
access to the entire API of the menubar.  See the "menubar" manual
entry for details on the commands available in the menubar.

*pathName **mousebar** ?\fIargs*?
The **mousebar** method provides access to the mousebar which is a
vertical toolbar.  Invoked with no arguments it returns the pathname
of the mousebar.  With arguments, they are evaluated against the mousebar
which in effect provides access to the entire API of the underlying
toolbar.  See the "toolbar" manual entry for details on the commands
available in the mousebar.

*pathName **msgd** ?\fIargs*?
The **msgd** method provides access to the messagedialog contained
in the mainwindow.  Invoked with no arguments it returns the pathname
of the messagedialog.  With arguments, they are evaluated against the
messagedialog which in effect provides access to the entire API of the
messagedialog.  See the "messagedialog" manual
entry for details on the commands available in the messagedialog.

*pathName **toolbar** ?\fIargs*?
The **toolbar** method provides access to the toolbar.  Invoked with
no arguments it returns the pathname of the toolbar.  With arguments,
they are evaluated against the toolbar which in effect provides
access to the entire API of the toolbar.  See the "toolbar" manual
entry for details on the commands available in the toolbar.


## COMPONENTS



```
Name:	help
Class:	Label
```

.IP
The help component provides a location for displaying any help
strings provided in the menubar, toolbar, or mousebar.  See the "label"
widget manual entry for details on the help component item.


```
Name:	menubar
Class:	Menubar
```

.IP
The menubar component is the menubar located at the top of the window.
See the "menubar" widget manual entry for details on the menubar
component item.


```
Name:	mousebar
Class:	Toolbar
```

.IP
The mousebar component is the vertical toolbar located on the right side
of the window.  See the "toolbar" widget manual entry for details on
the mousebar component item.


```
Name:	msgd
Class:	Messagedialog
```

.IP
The msgd component is a messagedialog which may be reconfigured as needed
and used repeatedly throughout the application.  See the "messagedialog"
widget manual entry for details on the messagedialog component item.


```
Name:	status
Class:	Label
```

.IP
The status component provides a location for displaying application
status information.  See the "label" widget manual entry for details
on the status component item.


```
Name:	toolbar
Class:	Toolbar
```

.IP
The toolbar component is the horizontal toolbar located on the top
of the window.  See the "toolbar" widget manual entry for details on
the toolbar component item.



## EXAMPLE


>  package require Iwidgets 4.0
 iwidgets::mainwindow .mw

 #
 # Add a File menubutton
 #
 .mw menubar add menubutton file -text "File" -underline 0 -padx 8 -pady 2 \
    -menu \{options -tearoff no
	   command new -label "New" -underline 0 \
	       -helpstr "Create a new file"
	   command open -label "Open ..." -underline 0 \
	       -helpstr "Open an existing file"
	   command save -label "Save" -underline 0 \
	       -helpstr "Save the current file"
	   command saveas -label "Save As ..." -underline 5 \
	       -helpstr "Save the file as a differnet name"
	   command print -label "Print" -underline 0 \
	       -helpstr "Print the file"
           separator sep1
	   command close -label "Close" -underline 0 \
	       -helpstr "Close the file"
	   separator sep2
	   command exit -label "Exit" -underline 1 \
	       -helpstr "Exit this application"
    \}

 #
 # Install a scrolledtext widget in the childsite.
 #
 iwidgets::scrolledtext [.mw childsite].st
 pack [.mw childsite].st -fill both -expand yes

 #
 # Activate the main window.
 #
 .mw activate



## AUTHOR

Mark L. Ulferts


John A. Tucker


## KEYWORDS

mainwindow, shell, widget
