+++
operating_system_version = "15.3"
description = "This package provides a widget to enable the user of a map display to control the zoom level. ::canvas::zoom pathName ?options? Creates the zoom control widget pathName and configures it. The methods and options supported by the new widget are des..."
manpage_format = "troff"
author = "None Specified"
operating_system = "macos"
title = "canvas::zoom(n)"
detected_package_version = "1.1"
manpage_name = "canvas::zoom"
date = "Sun Feb 16 04:48:30 2025"
manpage_section = "n"
+++

"canvas::zoom" n 0.2.1 canvas "Variations on a canvas"

---

## NAME

canvas::zoom - Zoom control for canvas::sqmap

## SYNOPSIS

package require **Tcl  8.4**

package require **Tk  8.4**

package require **snit **

package require **uevent::onidle **

package require **canvas::zoom  ?0.2.1?**

**::canvas::zoom** *pathName* ?options?


---


## DESCRIPTION

This package provides a widget to enable the user of a map display to
control the zoom level.

## API


**::canvas::zoom** *pathName* ?options?
Creates the zoom control widget *pathName* and configures it. The
methods and options supported by the new widget are described in the
following sections.

The result of the command is *pathName*.


### OPTIONS


**-orient**
The value for this option is either **vertical**, or
**horizontal**, specifying the orientation of the major axis of
the widget. The default is **vertical**.

**-levels**
The value for this option is a non-negative integer. It specifies the
number of zoom levels to support.

**-variable**
The value for this option is the name of a global or namespaced
variable which is connected with the widget. changes to the zoom level
made the widget are propagated to this variable, and in turn changes
to the variable are imported into the widget.

**-command**
This option specifies a command prefix. This callback will be invoked
whenever the zoom level is changed. It is called with two additional
arguments, the zoom control widget, and the new zoom level, in this
order.


### METHODS

The widget supports no methods beyond the standard (**configure**,
**cget**, etc.).

## KEYWORDS

zoom
