+++
operating_system_version = "15.3"
date = "Sun Feb 16 04:48:28 2025"
manpage_format = "troff"
operating_system = "macos"
description = "A ttk::scale widget is typically used to control the numeric value of a linked variable that varies uniformly over some range. A scale displays a slider that can be moved along over a trough, with the relative position of the slider over the trough..."
keywords = ["ttk", "widget", "scale", "keywords", "slider", "trough", "local", "variables", "mode", "nroff", "fill-column", "78", "end"]
title = "ttk::scale(n)"
manpage_section = "n"
manpage_name = "ttk::scale"
author = "Donal Fellows"
detected_package_version = "1.9"
+++

ttk::scale n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::scale - Create and manipulate a scale widget

## SYNOPSIS

**ttk::scale **pathName ?*options...*?

---


## DESCRIPTION


A **ttk::scale** widget is typically used to control the numeric value of a
linked variable that varies uniformly over some range. A scale displays a
*slider* that can be moved along over a *trough*, with the relative
position of the slider over the trough indicating the value of the variable.

### Standard ttk_widget

-class	-cursor	-style
-takefocus

See the standard options manual entry for details.


## WIDGET-SPECIFIC OPTIONS

Specifies the prefix of a Tcl command to invoke whenever the scale's value is
changed via a widget command. The actual command consists of this option
followed by a space and a real number indicating the new value of the scale.
A real value corresponding to the left or top end of the scale.
Specifies the desired long dimension of the scale in screen units (i.e. any of
the forms acceptable to **Tk_GetPixels**). For vertical scales this is the
scale's height; for horizontal scales it is the scale's width.
Specifies which orientation whether the widget should be laid out horizontally
or vertically. Must be either **horizontal** or **vertical** or an
abbreviation of one of these.
Specifies a real value corresponding to the right or bottom end of the scale.
This value may be either less than or greater than the **from** option.
Specifies the current floating-point value of the variable.
Specifies the name of a global variable to link to the scale. Whenever the
value of the variable changes, the scale will update to reflect this value.
Whenever the scale is manipulated interactively, the variable will be modified
to reflect the scale's new value.

## WIDGET COMMAND



*pathName **cget **option*
.
Returns the current value of the specified *option*; see
*ttk::widget(n)*.

*pathName **configure **?*option*? ?\fIvalue option value ...*?
.
Modify or query widget options; see *ttk::widget(n)*.

*pathName **get **?\fIx y*?
.
Get the current value of the **-value** option, or the value corresponding
to the coordinates *x,y* if they are specified. *X* and *y* are
pixel coordinates relative to the scale widget origin.

*pathName **identify** \fIx y*
Returns the name of the element at position *x*, *y*.
See *ttk::widget(n)*.

*pathName **instate **statespec* ?*script*?
.
Test the widget state; see *ttk::widget(n)*.

*pathName **set **value*
.
Set the value of the widget (i.e. the **-value** option) to *value*.
The value will be clipped to the range given by the **-from** and
**-to** options. Note that setting the linked variable (i.e. the variable
named in the **-variable** option) does not cause such clipping.

*pathName **state** ?\fIstateSpec*?
.
Modify or query the widget state; see *ttk::widget(n)*.

## INTERNAL COMMANDS



*pathName **coords **?\fIvalue*?
.
Get the coordinates corresponding to *value*, or the coordinates
corresponding to the current value of the **-value** option if *value*
is omitted.

## SEE ALSO

ttk::widget(n), scale(n)

## KEYWORDS

scale, slider, trough, widget
