+++
detected_package_version = "1.1"
keywords = ["expander", "format", "formatter", "keywords", "html", "tmml", "conversion", "manpage", "markup", "nroff", "category", "documentation", "tools", "copyright", "c", "2002", "andreas", "kupries", "andreas_kupries", "users", "sourceforge", "net", "2003"]
manpage_name = "mpexpand"
date = "Sun Feb 16 04:48:30 2025"
manpage_section = "n"
description = "This manpage describes a processor / converter for manpages in the doctools format as specified in doctools_fmt. The processor is based upon the package doctools. mpexpand ?-module module? format infile|- outfile|- The processor takes three argume..."
title = "mpexpand(n)"
operating_system = "macos"
manpage_format = "troff"
author = "None Specified"
operating_system_version = "15.3"
+++

"mpexpand" n 1.0 doctools "Documentation toolbox"

---

## NAME

mpexpand - Markup processor

## SYNOPSIS

**mpexpand** ?-module *module*? *format* *infile*|- *outfile*|-

**mpexpand.all** ?*-verbose*? ?*module*?


---


## DESCRIPTION


This manpage describes a processor / converter for manpages in the
doctools format as specified in **doctools_fmt**. The processor
is based upon the package **doctools**.

**mpexpand** ?-module *module*? *format* *infile*|- *outfile*|-
The processor takes three arguments, namely the code describing which
formatting to generate as the output, the file to read the markup
from, and the file to write the generated output into. If the
*infile* is "**-**" the processor will read from
**stdin**. If *outfile* is "**-**" the processor will
write to **stdout**.

If the option *-module* is present its value overrides the internal
definition of the module name.

The currently known output formats are

> 
**nroff**
The processor generates *roff output, the standard format for unix
manpages.

**html**
The processor generates HTML output, for usage in and display by web
browsers.

**tmml**
The processor generates TMML output, the Tcl Manpage Markup Language,
a derivative of XML.

**latex**
The processor generates LaTeX output.

**wiki**
The processor generates Wiki markup as understood by **wikit**.

**list**
The processor extracts the information provided by **manpage_begin**.

**null**
The processor does not generate any output.



**mpexpand.all** ?*-verbose*? ?*module*?
This command uses **mpexpand** to generate all possible output
formats for all manpages in the current directory. The manpages are
recognized through the extension "*.man*". If *-verbose* is
specified the command will list its actions before executing them.

The *module* information is passed to **mpexpand**.


## NOTES


Possible future formats are plain text, pdf and postscript.

## SEE ALSO

expander(n), format(n), formatter(n)

## KEYWORDS

HTML, TMML, conversion, manpage, markup, nroff

## CATEGORY

Documentation tools

## COPYRIGHT


```
Copyright (c) 2002 Andreas Kupries <andreas_kupries@users.sourceforge.net>
Copyright (c) 2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

