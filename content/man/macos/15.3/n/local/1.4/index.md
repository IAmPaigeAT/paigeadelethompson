+++
manpage_format = "troff"
detected_package_version = "1.4"
operating_system_version = "15.3"
operating_system = "macos"
author = "None Specified"
date = "Sun Feb 16 04:48:31 2025"
description = "The local command creates an [incr Tcl] object that is local to the current call frame.  When the call frame goes away, the object is automatically deleted.  This command is useful for creating objects that are local to a procedure. As a side effe..."
title = "local(n)"
manpage_name = "local"
manpage_section = "n"
+++

local n "" itcl "[incr\ Tcl]"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

local - create an object local to a procedure

## SYNOPSIS

**itcl::local **className objName ?*arg arg ...*?

---



## DESCRIPTION


The **local** command creates an **[incr\ Tcl]** object that
is local to the current call frame.  When the call frame goes away,
the object is automatically deleted.  This command is useful for
creating objects that are local to a procedure.

As a side effect, this command creates a variable named
"\fCitcl-local-*xxx*", where *xxx* is the name of
the object that is created.  This variable detects when the
call frame is destroyed and automatically deletes the
associated object.


## EXAMPLE

In the following example, a simple "counter" object is used
within the procedure "test".  The counter is created as a
local object, so it is automatically deleted each time the
procedure exits.  The **puts** statements included in the
constructor/destructor show the object coming and going
as the procedure is called.

```
itcl::class counter \{
    private variable count 0
    constructor \{\} \{
        puts "created: $this"
    \}
    destructor \{
        puts "deleted: $this"
    \}

    method bump \{\{by 1\}\} \{
        incr count $by
    \}
    method get \{\} \{
        return $count
    \}
\}

proc test \{val\} \{
    local counter x
    for \{set i 0\} \{$i < $val\} \{incr i\} \{
        x bump
    \}
    return [x get]
\}

set result [test 5]
puts "test: $result"

set result [test 10]
puts "test: $result"

puts "objects: [itcl::find objects *]"
```



## KEYWORDS

class, object, procedure
