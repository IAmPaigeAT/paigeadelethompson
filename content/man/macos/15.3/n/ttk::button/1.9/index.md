+++
title = "ttk::button(n)"
author = "None Specified"
operating_system_version = "15.3"
manpage_name = "ttk::button"
keywords = ["ttk", "widget", "button", "keywords", "default", "command", "local", "variables", "mode", "nroff", "end"]
manpage_section = "n"
operating_system = "macos"
manpage_format = "troff"
detected_package_version = "1.9"
date = "Sun Feb 16 04:48:31 2025"
description = "A ttk::button widget displays a textual label and/or image, and evaluates a command when pressed. -class-compound-cursor -image-state-style -takefocus-text-textvariable -underline-width A script to evaluate when the widget is invoked. Ma..."
+++

ttk::button n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::button - Widget that issues a command when pressed

## SYNOPSIS

**ttk::button** *pathName *?*options*?

---


## DESCRIPTION

A **ttk::button** widget displays a textual label and/or image,
and evaluates a command when pressed.

### Standard ttk_widget

-class	-compound	-cursor
-image	-state	-style
-takefocus	-text	-textvariable
-underline	-width

See the standard options manual entry for details.


## WIDGET-SPECIFIC OPTIONS

A script to evaluate when the widget is invoked.
May be set to one of  **normal**, **active**, or **disabled**.
In a dialog box, one button may be designated the
"default"button (meaning, roughly,
""the" one**active** indicates that this is currently the default button;
**normal** means that it may become the default button, and
**disabled** means that it is not defaultable.
The default is **normal**.

> 
Depending on the theme, the default button may be displayed
with an extra highlight ring, or with a different border color.


If greater than zero, specifies how much space, in character widths,
to allocate for the text label.
If less than zero, specifies a minimum width.
If zero or unspecified, the natural width of the text label is used.
Note that some themes may specify a non-zero **-width**
in the style.

## WIDGET COMMAND


In addition to the standard
**cget**, **configure**, **identify**, **instate**, and **state**
commands, buttons support the following additional widget commands:

\fIpathName **invoke**
Invokes the command associated with the button.

## STANDARD STYLES


**Ttk::button** widgets support the **Toolbutton** style in all standard
themes, which is useful for creating widgets for toolbars.

## COMPATIBILITY OPTIONS

May be set to **normal** or **disabled** to control the
**disabled** state bit. This is a
"write-only"option: setting it changes the widget state, but the **state**
widget command does not affect the state option.

## SEE ALSO

ttk::widget(n), button(n)

## KEYWORDS

widget, button, default, command
'\" Local Variables:
'\" mode: nroff
'\" End:
