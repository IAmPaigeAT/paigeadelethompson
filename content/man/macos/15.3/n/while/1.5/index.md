+++
date = "Sun Feb 16 04:48:29 2025"
description = "The while command evaluates test as an expression (in the same way that expr evaluates its argument). The value of the expression must a proper boolean value; if it is a true value then body is executed by passing it to the Tcl interpreter. Once ..."
title = "while(n)"
manpage_name = "while"
operating_system_version = "15.3"
keywords = ["break", "continue", "for", "foreach", "keywords", "boolean", "value", "loop", "test", "while"]
author = "None Specified"
operating_system = "macos"
detected_package_version = "1.5"
manpage_format = "troff"
manpage_section = "n"
+++

while n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

while - Execute script repeatedly as long as a condition is met

## SYNOPSIS

**while **test body

---



## DESCRIPTION


The **while** command evaluates *test* as an expression
(in the same way that **expr** evaluates its argument).
The value of the expression must a proper boolean
value; if it is a true value
then *body* is executed by passing it to the Tcl interpreter.
Once *body* has been executed then *test* is evaluated
again, and the process repeats until eventually *test*
evaluates to a false boolean value.  **Continue**
commands may be executed inside *body* to terminate the current
iteration of the loop, and **break**
commands may be executed inside *body* to cause immediate
termination of the **while** command.  The **while** command
always returns an empty string.

Note: *test* should almost always be enclosed in braces.  If not,
variable substitutions will be made before the **while**
command starts executing, which means that variable changes
made by the loop body will not be considered in the expression.
This is likely to result in an infinite loop.  If *test* is
enclosed in braces, variable substitutions are delayed until the
expression is evaluated (before
each loop iteration), so changes in the variables will be visible.
For an example, try the following script with and without the braces
around **$x<10**:

```
set x 0
**while** \{$x<10\} \{
    puts "x is $x"
    incr x
\}
```


## EXAMPLE

Read lines from a channel until we get to the end of the stream, and
print them out with a line-number prepended:

```
set lineCount 0
**while** \{[gets $chan line] >= 0\} \{
    puts "[incr lineCount]: $line"
\}
```



## SEE ALSO

break(n), continue(n), for(n), foreach(n)


## KEYWORDS

boolean value, loop, test, while
