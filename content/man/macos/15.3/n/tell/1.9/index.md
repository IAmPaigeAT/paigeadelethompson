+++
author = "None Specified"
operating_system_version = "15.3"
description = "Returns an integer string giving the current access position in channelId.  This value returned is a byte offset that can be passed to seek in order to set the channel to a particular position.  Note that this value is in terms of bytes, not charac..."
detected_package_version = "1.9"
manpage_format = "troff"
manpage_name = "tell"
manpage_section = "n"
operating_system = "macos"
title = "tell(n)"
date = "Sun Feb 16 04:48:28 2025"
keywords = ["file", "open", "close", "gets", "seek", "tcl_standardchannels", "keywords", "access", "position", "channel", "seeking"]
+++

tell n 8.1 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

tell - Return current access position for an open channel

## SYNOPSIS

**tell **channelId

---



## DESCRIPTION


Returns an integer string giving the current access position in
*channelId*.  This value returned is a byte offset that can be passed to
**seek** in order to set the channel to a particular position.  Note
that this value is in terms of bytes, not characters like **read**.
The value returned is -1 for channels that do not support
seeking.

*ChannelId* must be an identifier for an open channel such as a
Tcl standard channel (**stdin**, **stdout**, or **stderr**),
the return value from an invocation of **open** or **socket**, or
the result of a channel creation command provided by a Tcl extension.

## EXAMPLE

Read a line from a file channel only if it starts with **foobar**:

```
# Save the offset in case we need to undo the read...
set offset [**tell** $chan]
if \{[read $chan 6] eq "foobar"\} \{
    gets $chan line
\} else \{
    set line \{\}
    # Undo the read...
    seek $chan $offset
\}
```



## SEE ALSO

file(n), open(n), close(n), gets(n), seek(n), Tcl_StandardChannels(3)


## KEYWORDS

access position, channel, seeking
