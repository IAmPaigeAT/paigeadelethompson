+++
title = "break(n)"
date = "Sun Feb 16 04:48:29 2025"
keywords = ["catch", "continue", "for", "foreach", "return", "while", "keywords", "abort", "break", "loop"]
detected_package_version = "1.10"
operating_system = "macos"
description = "This command is typically invoked inside the body of a looping command such as for or foreach or while. It returns a TCL_BREAK code, which causes a break exception to occur. The exception causes the current script to be aborted out to the innermo..."
author = "None Specified"
manpage_name = "break"
manpage_section = "n"
manpage_format = "troff"
operating_system_version = "15.3"
+++

break n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

break - Abort looping command

## SYNOPSIS

**break**

---



## DESCRIPTION


This command is typically invoked inside the body of a looping command
such as **for** or **foreach** or **while**.
It returns a **TCL_BREAK** code, which causes a break exception
to occur.
The exception causes the current script to be aborted
out to the innermost containing loop command, which then
aborts its execution and returns normally.
Break exceptions are also handled in a few other situations, such
as the **catch** command, Tk event bindings, and the outermost
scripts of procedure bodies.

## EXAMPLE

Print a line for each of the integers from 0 to 5:

```
for \{set x 0\} \{$x<10\} \{incr x\} \{
   if \{$x > 5\} \{
      **break**
   \}
   puts "x is $x"
\}
```



## SEE ALSO

catch(n), continue(n), for(n), foreach(n), return(n), while(n)


## KEYWORDS

abort, break, loop
