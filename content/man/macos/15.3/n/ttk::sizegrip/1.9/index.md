+++
keywords = ["ttk", "widget", "keywords", "sizegrip", "grow", "box", "local", "variables", "mode", "nroff", "end"]
author = "None Specified"
manpage_section = "n"
operating_system_version = "15.3"
title = "ttk::sizegrip(n)"
date = "Sun Feb 16 04:48:29 2025"
detected_package_version = "1.9"
manpage_name = "ttk::sizegrip"
manpage_format = "troff"
operating_system = "macos"
description = "A ttk::sizegrip widget (also known as a grow box) allows the user to resize the containing toplevel window by pressing and dragging the grip. -class-cursor-state -style-takefocus Sizegrip widgets support the standard cget, configure, identify..."
+++

ttk::sizegrip n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::sizegrip - Bottom-right corner resize widget

## SYNOPSIS

**ttk::sizegrip** *pathName *?*options*?

---


## DESCRIPTION


A **ttk::sizegrip** widget (also known as a *grow box*)
allows the user to resize the containing toplevel window
by pressing and dragging the grip.

### Standard ttk_widget

-class	-cursor	-state
-style	-takefocus

See the standard options manual entry for details.


## WIDGET COMMAND


Sizegrip widgets support the standard
**cget**, **configure**, **identify**, **instate**, and **state**
methods.  No other widget methods are used.

## PLATFORM-SPECIFIC NOTES


On Mac OSX, toplevel windows automatically include a built-in
size grip by default.
Adding a **ttk::sizegrip** there is harmless, since
the built-in grip will just mask the widget.

## EXAMPLES


Using pack:

```
pack [ttk::frame $top.statusbar] -side bottom -fill x
pack [**ttk::sizegrip** $top.statusbar.grip] -side right -anchor se
```


Using grid:

```
grid [**ttk::sizegrip** $top.statusbar.grip] \\
    -row $lastRow -column $lastColumn -sticky se
# ... optional: add vertical scrollbar in $lastColumn,
# ... optional: add horizontal scrollbar in $lastRow
```


## BUGS


If the containing toplevel's position was specified
relative to the right or bottom of the screen
(e.g.,
""\fBwm" geometryinstead of
""\fBwm" geometrythe sizegrip widget will not resize the window.

**ttk::sizegrip** widgets only support
"southeast"resizing.

## SEE ALSO

ttk::widget(n)

## KEYWORDS

widget, sizegrip, grow box
'\" Local Variables:
'\" mode: nroff
'\" End:
