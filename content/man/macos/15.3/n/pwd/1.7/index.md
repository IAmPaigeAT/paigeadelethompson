+++
description = "Returns the absolute path name of the current working directory. Sometimes it is useful to change to a known directory when running some external command using exec, but it is important to keep the application usually running in the directory that ..."
title = "pwd(n)"
author = "None Specified"
detected_package_version = "1.7"
keywords = ["file", "cd", "glob", "filename", "keywords", "working", "directory"]
manpage_format = "troff"
manpage_name = "pwd"
operating_system_version = "15.3"
manpage_section = "n"
date = "Sun Feb 16 04:48:31 2025"
operating_system = "macos"
+++

pwd n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

pwd - Return the absolute path of the current working directory

## SYNOPSIS

**pwd**

---



## DESCRIPTION


Returns the absolute path name of the current working directory.

## EXAMPLE

Sometimes it is useful to change to a known directory when running
some external command using **exec**, but it is important to keep
the application usually running in the directory that it was started
in (unless the user specifies otherwise) since that minimizes user
confusion. The way to do this is to save the current directory while
the external command is being run:

```
set tarFile [file normalize somefile.tar]
set savedDir [**pwd**]
cd /tmp
exec tar -xf $tarFile
cd $savedDir
```


## SEE ALSO

file(n), cd(n), glob(n), filename(n)


## KEYWORDS

working directory
