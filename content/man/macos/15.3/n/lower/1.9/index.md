+++
keywords = ["raise", "keywords", "lower", "obscure", "stacking", "order"]
operating_system_version = "15.3"
manpage_section = "n"
description = "If the belowThis argument is omitted then the command lowers window so that it is below all of its siblings in the stacking order (it will be obscured by any siblings that overlap it and will not obscure any siblings). If belowThis is specified th..."
detected_package_version = "1.9"
date = "Sun Feb 16 04:48:31 2025"
operating_system = "macos"
manpage_name = "lower"
author = "None Specified"
title = "lower(n)"
manpage_format = "troff"
+++

lower n 3.3 Tk "Tk Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

lower - Change a window's position in the stacking order

## SYNOPSIS

**lower **window ?*belowThis*?

---



## DESCRIPTION


If the *belowThis* argument is omitted then the command lowers
*window* so that it is below all of its siblings in the stacking
order (it will be obscured by any siblings that overlap it and
will not obscure any siblings).
If *belowThis* is specified then it must be the path name of
a window that is either a sibling of *window* or the descendant
of a sibling of *window*.
In this case the **lower** command will insert
*window* into the stacking order just below *belowThis*
(or the ancestor of *belowThis* that is a sibling of *window*);
this could end up either raising or lowering *window*.


## SEE ALSO

raise


## KEYWORDS

lower, obscure, stacking order
