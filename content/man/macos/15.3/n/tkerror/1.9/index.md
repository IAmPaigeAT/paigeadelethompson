+++
date = "Sun Feb 16 04:48:29 2025"
author = "None Specified"
detected_package_version = "1.9"
operating_system = "macos"
manpage_name = "tkerror"
operating_system_version = "15.3"
manpage_section = "n"
title = "tkerror(n)"
description = "Note: as of Tk 4.1 the tkerror command has been renamed to bgerror because the event loop (which is what usually invokes it) is now part of Tcl.  For backward compatibility the bgerror provided by the current Tk version still tries to call tkerror..."
manpage_format = "troff"
+++

tkerror n 4.1 Tk "Tk Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

tkerror - Command invoked to process background errors

## SYNOPSIS

**tkerror **message

---



## DESCRIPTION


Note: as of Tk 4.1 the **tkerror** command has been renamed to
**bgerror** because the event loop (which is what usually invokes
it) is now part of Tcl.  For backward compatibility
the **bgerror** provided by the current Tk version still
tries to call **tkerror** if there is one (or an auto loadable one),
so old script defining that error handler should still work, but you
should anyhow modify your scripts to use **bgerror** instead
of **tkerror** because that support for the old name might vanish
in the near future. If that call fails, **bgerror**
posts a dialog showing the error and offering to see the stack trace
to the user. If you want your own error management you should
directly override **bgerror** instead of **tkerror**.
Documentation for **bgerror** is available as part of Tcl's
documentation.


## KEYWORDS

background error, reporting
