+++
operating_system = "macos"
title = "ttk::separator(n)"
manpage_name = "ttk::separator"
keywords = ["ttk", "widget", "keywords", "separator", "local", "variables", "mode", "nroff", "end"]
operating_system_version = "15.3"
detected_package_version = "1.9"
manpage_format = "troff"
author = "None Specified"
description = "A ttk::separator widget displays a horizontal or vertical separator bar. -class-cursor-state -style-takefocus One of horizontal or vertical. Specifies the orientation of the separator. Separator widgets support the standard  cget, configure,..."
manpage_section = "n"
date = "Sun Feb 16 04:48:28 2025"
+++

ttk::separator n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::separator - Separator bar

## SYNOPSIS

**ttk::separator** *pathName *?*options*?

---


## DESCRIPTION


A **ttk::separator** widget displays a horizontal or vertical separator
bar.

### Standard ttk_widget

-class	-cursor	-state
-style	-takefocus

See the standard options manual entry for details.


## WIDGET-SPECIFIC OPTIONS

One of **horizontal** or **vertical**.
Specifies the orientation of the separator.

## WIDGET COMMAND


Separator widgets support the standard
**cget**, **configure**, **identify**, **instate**, and **state**
methods.  No other widget methods are used.

## SEE ALSO

ttk::widget(n)

## KEYWORDS

widget, separator
'\" Local Variables:
'\" mode: nroff
'\" End:
