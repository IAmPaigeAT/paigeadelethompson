+++
operating_system = "macos"
operating_system_version = "15.3"
title = "puts(n)"
manpage_name = "puts"
detected_package_version = "1.13"
keywords = ["file", "fileevent", "tcl_standardchannels", "keywords", "channel", "newline", "output", "write"]
date = "Sun Feb 16 04:48:30 2025"
author = "None Specified"
manpage_section = "n"
description = "Writes the characters given by string to the channel given by channelId. ChannelId must be an identifier for an open channel such as a Tcl standard channel (stdout or stderr), the return value from an invocation of open or socket, or the result o..."
manpage_format = "troff"
+++

puts n 7.5 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

puts - Write to a channel

## SYNOPSIS

**puts **?**-nonewline**? ?*channelId*? *string*

---



## DESCRIPTION


Writes the characters given by *string* to the channel given
by *channelId*.

*ChannelId* must be an identifier for an open channel such as a
Tcl standard channel (**stdout** or **stderr**), the return
value from an invocation of **open** or **socket**, or the result
of a channel creation command provided by a Tcl extension. The channel
must have been opened for output.

If no *channelId* is specified then it defaults to
**stdout**. **Puts** normally outputs a newline character after
*string*, but this feature may be suppressed by specifying the
**-nonewline** switch.

Newline characters in the output are translated by **puts** to
platform-specific end-of-line sequences according to the current
value of the **-translation** option for the channel (for example,
on PCs newlines are normally replaced with carriage-return-linefeed
sequences.
See the **fconfigure** manual entry for a discussion on ways in
which **fconfigure** will alter output.

Tcl buffers output internally, so characters written with **puts**
may not appear immediately on the output file or device;  Tcl will
normally delay output until the buffer is full or the channel is
closed.
You can force output to appear immediately with the **flush**
command.

When the output buffer fills up, the **puts** command will normally
block until all the buffered data has been accepted for output by the
operating system.
If *channelId* is in nonblocking mode then the **puts** command
will not block even if the operating system cannot accept the data.
Instead, Tcl continues to buffer the data and writes it in the
background as fast as the underlying file or device can accept it.
The application must use the Tcl event loop for nonblocking output
to work;  otherwise Tcl never finds out that the file or device is
ready for more output data.
It is possible for an arbitrarily large amount of data to be
buffered for a channel in nonblocking mode, which could consume a
large amount of memory.
To avoid wasting memory, nonblocking I/O should normally
be used in an event-driven fashion with the **fileevent** command
(do not invoke **puts** unless you have recently been notified
via a file event that the channel is ready for more output data).

## EXAMPLES

Write a short message to the console (or wherever **stdout** is
directed):

```
**puts** "Hello, World!"
```


Print a message in several parts:

```
**puts** -nonewline "Hello, "
**puts** "World!"
```


Print a message to the standard error channel:

```
**puts** stderr "Hello, World!"
```


Append a log message to a file:

```
set chan [open my.log a]
set timestamp [clock format [clock seconds]]
**puts** $chan "$timestamp - Hello, World!"
close $chan
```



## SEE ALSO

file(n), fileevent(n), Tcl_StandardChannels(3)


## KEYWORDS

channel, newline, output, write
