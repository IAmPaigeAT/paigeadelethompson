+++
date = "Sun Feb 16 04:48:27 2025"
keywords = ["file", "fileutil", "magic", "keywords", "recognition", "type", "utilities", "mime", "category", "programming", "tools"]
title = "fileutil::magic::mimetype(n)"
manpage_section = "n"
author = "None Specified"
manpage_name = "fileutil::magic::mimetype"
description = "This package provides a command for the recognition of file types in pure Tcl. The output is standardized to mime-types. The core part of the recognizer was generated from a magic(5) file containing the checks to perform to recognize files, and a..."
operating_system_version = "15.3"
detected_package_version = "1.1"
operating_system = "macos"
manpage_format = "troff"
+++

"fileutil::magic::mimetype" n 1.0.2 fumagic "file utilities"

---

## NAME

fileutil::magic::mimetype - Procedures implementing mime-type recognition

## SYNOPSIS

package require **Tcl  8.4**

package require **fileutil::magic::mimetype  ?1.0.2?**

**::fileutil::magic::mimetype** *filename*


---


## DESCRIPTION


This package provides a command for the recognition of file types in
pure Tcl. The output is standardized to mime-types.

The core part of the recognizer was generated from a "magic(5)" file
containing the checks to perform to recognize files, and associated
mime-types.

**::fileutil::magic::mimetype** *filename*
This command is similar to the command **fileutil::fileType**.

The output of the command for the specified file is not a list of
attributes describing the type of the file, but a list of standard
mime-types the file may have.

This list will be empty if the type of the file is not recognized.


## REFERENCES

[1]
*File(1) sources* [ftp://ftp.astron.com/pub/file/]
This site contains the current sources for the file command, including
the magic definitions used by it. The latter were used by us to
generate this recognizer.


## BUGS, IDEAS, FEEDBACK

This document, and the package it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *fileutil :: magic* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## SEE ALSO

file(1), fileutil, magic(5)

## KEYWORDS

file recognition, file type, file utilities, mime, type

## CATEGORY

Programming tools
