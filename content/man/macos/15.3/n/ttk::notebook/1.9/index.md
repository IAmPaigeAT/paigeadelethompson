+++
keywords = ["ttk", "widget", "grid", "keywords", "pane", "tab", "local", "variables", "mode", "nroff", "end"]
manpage_name = "ttk::notebook"
manpage_section = "n"
description = "A ttk::notebook widget manages a collection of windows  and displays a single one at a time. Each slave window is associated with a tab, which the user may select to change the currently-displayed window. -class-cursor-takefocus -style If pres..."
date = "Sun Feb 16 04:48:29 2025"
detected_package_version = "1.9"
operating_system = "macos"
title = "ttk::notebook(n)"
author = "None Specified"
manpage_format = "troff"
operating_system_version = "15.3"
+++

ttk::notebook n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::notebook - Multi-paned container widget

## SYNOPSIS


```
ttk::notebook pathname ?options...?
.br
pathname add window ?options...?
pathname insert index window ?options...?
```


---


## DESCRIPTION

A **ttk::notebook** widget manages a collection of windows
and displays a single one at a time.
Each slave window is associated with a *tab*,
which the user may select to change the currently-displayed window.

### Standard ttk_widget

-class	-cursor	-takefocus
-style

See the standard options manual entry for details.


## WIDGET-SPECIFIC OPTIONS

If present and greater than zero,
specifies the desired height of the pane area
(not including internal padding or tabs).
Otherwise, the maximum height of all panes is used.
Specifies the amount of extra space to add around the outside
of the notebook.
The padding is a list of up to four length specifications
*left top right bottom*.
If fewer than four elements are specified,
*bottom* defaults to *top*,
*right* defaults to *left*, and
*top* defaults to *left*.
If present and greater than zero,
specifies the desired width of the pane area
(not including internal padding).
Otherwise, the maximum width of all panes is used.

## TAB OPTIONS

The following options may be specified for individual notebook panes:
Either **normal**, **disabled** or **hidden**.
If **disabled**, then the tab is not selectable.
If **hidden**, then the tab is not shown.
Specifies how the slave window is positioned within the pane area.
Value is a string containing zero or more of the characters
**n, s, e,** or **w**.
Each letter refers to a side (north, south, east, or west)
that the slave window will
"stick"to, as per the **grid** geometry manager.
Specifies the amount of extra space to add between the notebook and this pane.
Syntax is the same as for the widget **-padding** option.
Specifies a string to be displayed in the tab.
Specifies an image to display in the tab.
See *ttk_widget(n)* for details.
Specifies how to display the image relative to the text,
in the case both **-text** and **-image** are present.
See *label(n)* for legal values.
Specifies the integer index (0-based) of a character to underline
in the text string.
The underlined character is used for mnemonic activation
if **ttk::notebook::enableTraversal** is called.

## TAB IDENTIFIERS

The *tabid* argument to the following commands may take
any of the following forms:
\(bu
An integer between zero and the number of tabs;
\(bu
The name of a slave window;
\(bu
A positional specification of the form
"@\fIx\fR,\fIy\fR" ,which identifies the tab
\(bu
The literal string
"\fBcurrent\fR" ,which identifies the currently-selected tab; or:
\(bu
The literal string
"\fBend\fR" ,which returns the number of tabs
(only valid for
""\fIpathname" \fBindex\fR"
## WIDGET COMMAND


*pathname **add** *window* ?\fIoptions...*?
Adds a new tab to the notebook.
See **TAB OPTIONS** for the list of available *options*.
If *window* is currently managed by the notebook but hidden,
it is restored to its previous position.

*pathname **configure** ?\fIoptions*?
See *ttk::widget(n)*.

*pathname **cget** \fIoption*
See *ttk::widget(n)*.

*pathname **forget** \fItabid*
Removes the tab specified by *tabid*,
unmaps and unmanages the associated window.

*pathname **hide** \fItabid*
Hides the tab specified by *tabid*.
The tab will not be displayed, but the associated window
remains managed by the notebook and its configuration remembered.
Hidden tabs may be restored with the **add** command.

*pathname **identify** *component* *x* \fIy*
Returns the name of the element under the point given by *x* and *y*,
or the empty string if no component is present at that location.
The following subcommands are supported:

> 
*pathname **identify** **element** *x* \fIy*
Returns the name of the element at the specified location.

*pathname **identify** **tab** *x* \fIy*
Returns the index of the tab at the specified location.



*pathname **index** \fItabid*
Returns the numeric index of the tab specified by *tabid*,
or the total number of tabs if *tabid* is the string
"\fBend\fR" .
*pathname **insert** *pos* *subwindow* \fIoptions...*
Inserts a pane at the specified position.
*pos* is either the string **end**, an integer index,
or the name of a managed subwindow.
If *subwindow* is already managed by the notebook,
moves it to the specified position.
See **TAB OPTIONS** for the list of available options.

*pathname **instate** *statespec *?\fIscript...*?
See *ttk::widget(n)*.

*pathname **select** ?\fItabid*?
Selects the specified tab.
The associated slave window will be displayed,
and the previously-selected window (if different) is unmapped.
If *tabid* is omitted, returns the widget name of the
currently selected pane.

*pathname **state** ?\fIstatespec*?
See *ttk::widget(n)*.

*pathname **tab** *tabid* ?*-option *?\fIvalue ...*
Query or modify the options of the specific tab.
If no *-option* is specified,
returns a dictionary of the tab option values.
If one *-option* is specified,
returns the value of that *option*.
Otherwise, sets the *-option*s to the corresponding *value*s.
See **TAB OPTIONS** for the available options.

\fIpathname **tabs**
Returns the list of windows managed by the notebook.

## KEYBOARD TRAVERSAL

To enable keyboard traversal for a toplevel window
containing a notebook widget *$nb*, call:

```
ttk::notebook::enableTraversal $nb
```


This will extend the bindings for the toplevel window
containing the notebook as follows:
\(bu
**Control-Tab** selects the tab following the currently selected one.
\(bu
**Shift-Control-Tab** selects the tab preceding the currently selected one.
\(bu
**Alt-K**, where **K** is the mnemonic (underlined) character
of any tab, will select that tab.

Multiple notebooks in a single toplevel may be enabled for traversal,
including nested notebooks.
However, notebook traversal only works properly if all panes
are direct children of the notebook.

## VIRTUAL EVENTS

The notebook widget generates a **<<NotebookTabChanged>>**
virtual event after a new tab is selected.

## EXAMPLE


```
pack [**ttk::notebook** .nb]
\.nb add [frame .nb.f1] -text "First tab"
\.nb add [frame .nb.f2] -text "Second tab"
\.nb select .nb.f2
ttk::notebook::enableTraversal .nb
```


## SEE ALSO

ttk::widget(n), grid(n)

## KEYWORDS

pane, tab
'\" Local Variables:
'\" mode: nroff
'\" End:
