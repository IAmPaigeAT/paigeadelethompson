+++
operating_system = "macos"
operating_system_version = "15.3"
manpage_name = "append"
description = "Append all of the value arguments to the current value of variable varName.  If varName does not exist, it is given a value equal to the concatenation of all the value arguments. The result of this command is the new value stored in variable varN..."
manpage_section = "n"
date = "Sun Feb 16 04:48:31 2025"
manpage_format = "troff"
detected_package_version = "1.10"
title = "append(n)"
author = "None Specified"
keywords = ["concat", "lappend", "keywords", "append", "variable"]
+++

append n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

append - Append to variable

## SYNOPSIS

**append **varName ?*value value value ...*?

---



## DESCRIPTION


Append all of the *value* arguments to the current value
of variable *varName*.  If *varName* does not exist,
it is given a value equal to the concatenation of all the
*value* arguments.
The result of this command is the new value stored in variable
*varName*.
This command provides an efficient way to build up long
variables incrementally.
For example,
""\fBappend" ais much more efficient than
""\fBset" aif **$a** is long.

## EXAMPLE

Building a string of comma-separated numbers piecemeal using a loop.

```
set var 0
for \{set i 1\} \{$i<=10\} \{incr i\} \{
   **append** var "," $i
\}
puts $var
# Prints 0,1,2,3,4,5,6,7,8,9,10
```



## SEE ALSO

concat(n), lappend(n)


## KEYWORDS

append, variable
