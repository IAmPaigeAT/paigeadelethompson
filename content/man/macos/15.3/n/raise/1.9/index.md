+++
title = "raise(n)"
operating_system = "macos"
manpage_section = "n"
manpage_format = "troff"
date = "Sun Feb 16 04:48:29 2025"
keywords = ["lower", "keywords", "obscure", "raise", "stacking", "order"]
description = "If the aboveThis argument is omitted then the command raises window so that it is above all of its siblings in the stacking order (it will not be obscured by any siblings and will obscure any siblings that overlap it). If aboveThis is specified th..."
operating_system_version = "15.3"
detected_package_version = "1.9"
manpage_name = "raise"
author = "None Specified"
+++

raise n 3.3 Tk "Tk Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

raise - Change a window's position in the stacking order

## SYNOPSIS

**raise **window ?*aboveThis*?

---



## DESCRIPTION


If the *aboveThis* argument is omitted then the command raises
*window* so that it is above all of its siblings in the stacking
order (it will not be obscured by any siblings and will obscure
any siblings that overlap it).
If *aboveThis* is specified then it must be the path name of
a window that is either a sibling of *window* or the descendant
of a sibling of *window*.
In this case the **raise** command will insert
*window* into the stacking order just above *aboveThis*
(or the ancestor of *aboveThis* that is a sibling of *window*);
this could end up either raising or lowering *window*.

## EXAMPLE

Make a button appear to be in a sibling frame that was created after
it. This is is often necessary when building GUIs in the style where
you create your activity widgets first before laying them out on the
display:

```
button .b -text "Hi there!"
pack [frame .f -background blue]
pack [label .f.l1 -text "This is above"]
pack .b -in .f
pack [label .f.l2 -text "This is below"]
**raise** .b
```



## SEE ALSO

lower(n)


## KEYWORDS

obscure, raise, stacking order
