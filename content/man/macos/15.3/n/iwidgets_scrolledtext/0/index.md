+++
title = "iwidgets_scrolledtext(n)"
manpage_format = "troff"
description = "The iwidgets::scrolledtext command creates  a scrolled text widget with additional options to manage the scrollbars.  This includes options to control the method in which the scrollbars are displayed, i.e. statically or  dynamically. Options also ..."
operating_system = "macos"
date = "Sun Feb 16 04:48:29 2025"
author = "None Specified"
operating_system_version = "15.3"
manpage_section = "n"
detected_package_version = "0"
manpage_name = "iwidgets_scrolledtext"
+++

iwidgets::scrolledtext iwid

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

iwidgets::scrolledtext - Create and manipulate a scrolled text widget

## SYNOPSIS

**iwidgets::scrolledtext** *pathName *?*options*?

## INHERITANCE

itk::Widget <- iwidgets::Labeledwidget <- iwidgets::Scrolledwidget <- iwidgets::Scrolledtext

## STANDARD OPTIONS



```
.ta 4c 8c 12c
activeBackground	background	borderWidth	cursor
exportSelection		foreground	highlightColor
highlightThickness	insertBackground
insertBorderWidth	insertOffTime	insertOnTime
insertWidth		padX	padY
relief		selectBackground
selectBorderWidth	selectForeground
setGrid 
```


See the "options" manual entry for details on the standard options.

## ASSOCIATED OPTIONS



```
.ta 4c 8c 12c
activeRelief	elementBorderWidth	jump	troughColor
```


See the "scrollbar" widget manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
spacing1	spacing2	spacing3	state
tabs	wrap
```


See the "text" widget manual entry for details on the above
associated options.

## INHERITED OPTIONS



```
.ta 4c 8c 12c
disabledForeground	labelBitmap	labelFont	labelImage
labelMargin	labelPos	labelText	labelVariable
state	sticky
```


See the "labeledwidget" class manual entry for details on the inherited options.

## WIDGET-SPECIFIC OPTIONS



```
Name:	height
Class:	Height
Command-Line Switch:	-height
```

.IP
Specifies the height of the scrolled text as an entire unit.
The value may be specified in any of the forms acceptable to
**Tk_GetPixels**.  Any additional space needed to display the other
components such as labels, margins, and scrollbars force the text
to be compressed.  A value of zero along with the same value for
the width causes the value given for the visibleitems option
to be applied which administers geometry constraints in a different
manner.  The default height is zero.


```
Name:	hscrollMode
Class:	ScrollMode
Command-Line Switch:	-hscrollmode
```

.IP
Specifies the the display mode to be used for the horizontal
scrollbar: **static, dynamic,** or **none**.  In static mode, the
scroll bar is displayed at all times.  Dynamic mode displays the
scroll bar as required, and none disables the scroll bar display.  The
default is static.


```
Name:	sbWidth
Class:	Width
Command-Line Switch:	-sbwidth
```

.IP
Specifies the width of the scrollbar in any of the forms
acceptable to **Tk_GetPixels**.


```
Name:	scrollMargin
Class:	Margin
Command-Line Switch:	-scrollmargin
```

.IP
Specifies the distance between the text area and scrollbar in any of the forms
acceptable to **Tk_GetPixels**.  The default is 3 pixels.


```
Name:	textBackground
Class:	Background
Command-Line Switch:	-textbackground
```

.IP
Specifies the background color for the text area in any of the forms
acceptable to **Tk_GetColor**.


```
Name:	textFont
Class:	Font
Command-Line Switch:	-textfont
```

.IP
Specifies the font to be used in the scrolled text area.


```
Name:	visibleitems
Class:	VisibleItems
Command-Line Switch:	-visibleitems
```

.IP
Specifies the widthxheight in characters and lines for the text.
This option is only administered if the width and height options
are both set to zero, otherwise they take precedence.  The default value
is 80x24.  With the visibleitems option engaged, geometry constraints
are maintained only on the text.  The size of the other components such as
labels, margins, and scroll bars, are additive and independent,
effecting the overall size of the scrolled text.  In contrast,
should the width and height options have non zero values, they
are applied to the scrolled text as a whole.  The text
is compressed or expanded to maintain the geometry constraints.


```
Name:	vscrollMode
Class:	ScrollMode
Command-Line Switch:	-vscrollmode
```

.IP
Specifies the the display mode to be used for the vertical
scrollbar: **static, dynamic,** or **none**.  In static mode, the
scroll bar is displayed at all times.  Dynamic mode displays the
scroll bar as required, and none disables the scroll bar display.  The
default is static.


```
Name:	width
Class:	Width
Command-Line Switch:	-width
```

.IP
Specifies the width of the scrolled text as an entire unit.
The value may be specified in any of the forms acceptable to
**Tk_GetPixels**.  Any additional space needed to display the other
components such as labels, margins, and scrollbars force the text
to be compressed.  A value of zero along with the same value for
the height causes the value given for the visibleitems option
to be applied which administers geometry constraints in a different
manner.  The default width is zero.


---



## DESCRIPTION


The **iwidgets::scrolledtext** command creates
a scrolled text widget with additional options to manage
the scrollbars.  This includes options to control the method
in which the scrollbars are displayed, i.e. statically or  dynamically.
Options also exist for adding a label to the scrolled text area and
controlling its position.  Import/export of methods are provided for
file I/O.


## METHODS


The **iwidgets::scrolledtext** command creates a new Tcl command whose
name is *pathName*.  This
command may be used to invoke various
operations on the widget.  It has the following general form:

> *pathName option *?*arg arg ...*?


*Option* and the *arg*s
determine the exact behavior of the command.  The following
commands are possible for scrolledtext widgets:

## ASSOCIATED METHODS



```
.ta 4c 8c 12c
bbox	compare	debug	delete	
dlineinfo	get	image	index
insert	mark	scan	search
see	tag	window	xview
yview
```


See the "text" manual entry for details on the standard methods.


## WIDGET-SPECIFIC METHODS


*pathName **cget** \fIoption*
Returns the current value of the configuration option given
by *option*.
*Option* may have any of the values accepted by the **iwidgets::scrolledtext**
command.

\fIpathName **childsite**
Returns the child site widget path name.

\fIpathName **clear**
Clear the text area of all characters.

*pathName* **configure** ?*option*? ?*value option value ...*?
Query or modify the configuration options of the widget.
If no *option* is specified, returns a list describing all of
the available options for *pathName* (see **Tk_ConfigureInfo** for
information on the format of this list).  If *option* is specified
with no *value*, then the command returns a list describing the
one named option (this list will be identical to the corresponding
sublist of the value returned if no *option* is specified).  If
one or more *option-value* pairs are specified, then the command
modifies the given widget option(s) to have the given value(s);  in
this case the command returns an empty string.
*Option* may have any of the values accepted by the **iwidgets::scrolledtext**
command.

*pathName **import** *filename* ?\fIindex*?
Load the text from a file into the text area at the *index*.  The
*filename* must exist.

*pathName **export** \fIfilename*
Write text to a file.  If *filename* exists then contents are
replaced with text widget contents.


## COMPONENTS



```
Name:	text
Class:	Text
```

.IP
The text component is the text widget.  See the "text" widget
manual entry for details on the text component item.


```
Name:	horizsb
Class:	Scrollbar
```

.IP
The horizsb component is the horizontal scroll bar.  See the "scrollbar"
widget manual entry for details on the horizsb component item.


```
Name:	vertsb
Class:	Scrollbar
```

.IP
The vertsb component is the vertical scroll bar.  See the "scrollbar" widget
manual entry for details on the vertsb component item.



## EXAMPLE


>  package require Iwidgets 4.0
 option add *textBackground white

 iwidgets::scrolledtext .st -hscrollmode dynamic -labeltext "Password File"

 pack .st -padx 10 -pady 10 -fill both -expand yes

 .st import /etc/passwd



## AUTHOR

Mark L. Ulferts

## KEYWORDS

scrolledtext, text, widget
