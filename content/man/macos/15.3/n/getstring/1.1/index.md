+++
description = "This package provides a dialog which consists of an Entry, OK, and Cancel buttons. ::getstring::tk_getString pathName variable text ?options? Creates a dialog which prompts the user with text to input a text string. The contents of the entry are p..."
operating_system = "macos"
manpage_format = "troff"
title = "getstring(n)"
author = "None Specified"
manpage_section = "n"
detected_package_version = "1.1"
operating_system_version = "15.3"
date = "Sun Feb 16 04:48:27 2025"
manpage_name = "getstring"
+++

"getstring" n 0.1 getstring "A dialog which prompts for a string input"

---

## NAME

getstring - A string dialog

## SYNOPSIS

package require **Tcl  8.4**

package require **getstring  ?0.1?**

**::getstring::tk_getString** *pathName* *variable* *text* ?options?


---


## DESCRIPTION

This package provides a dialog which consists of an Entry, OK, and
Cancel buttons.


**::getstring::tk_getString** *pathName* *variable* *text* ?options?
Creates a dialog which prompts the user with *text* to input a text string.
The contents of the entry are put in the *variable* upon closure of the
dialog. The command returns a boolean indicating if the user pressed OK or
not. If -geometry is not specified, the dialog is centered in its parent
toplevel unless its parent is . in which case the dialog is centered in the
screen.
Options:
-title
-allowempty a boolean argument indicating if the dialog should accept an empty entry
-entryoptions simply passes its arguments through to the entry widget. This is valuble for performing extra validation
using the Entry widget validation hooks.
-geometry specifies the geometry of the window


## EXAMPLE


```

package require getstring
namespace import getstring::*

if {[tk_getString .gs text "Feed me a string please:"]} {
    puts "user entered: $text"
}


```


## KEYWORDS

dialog, entry, string
