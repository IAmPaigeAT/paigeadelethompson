+++
manpage_format = "troff"
description = "The command crypt is an interface to the crypt(3) function for the encryption of passwords. An alternative command for the same, but based on md5 is md5crypt. crypt password salt Encrypts the password using the specified salt and returns the gene..."
manpage_name = "crypt"
operating_system = "macos"
detected_package_version = "1.1"
date = "Sun Feb 16 04:48:29 2025"
operating_system_version = "15.3"
manpage_section = "n"
title = "crypt(n)"
author = "None Specified"
keywords = ["md5crypt", "trf-intro", "keywords", "authentication", "crypt", "hash", "hashing", "mac", "md5", "message", "digest", "password", "copyright", "c", "1996-2003", "andreas", "kupries", "andreas_kupries", "users", "sourceforge", "net"]
+++

"crypt" n 2.1.4  "Trf transformer commands"

---

## NAME

crypt - Password hashing based on "crypt"

## SYNOPSIS

package require **Tcl  ?8.2?**

package require **Trf  ?2.1.4?**

**crypt** *password* *salt*


---


## DESCRIPTION

The command **crypt** is an interface to the **crypt(3)**
function for the encryption of passwords. An alternative command for
the same, but based on *md5* is **md5crypt**.


**crypt** *password* *salt*
Encrypts the *password* using the specified *salt* and returns
the generated hash value as the result of the command.


## SEE ALSO

md5crypt, trf-intro

## KEYWORDS

authentication, crypt, hash, hashing, mac, md5, message digest, password

## COPYRIGHT


```
Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

