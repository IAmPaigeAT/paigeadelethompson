+++
author = "None Specified"
detected_package_version = "1.9"
date = "Sun Feb 16 04:48:31 2025"
keywords = ["ttk", "widget", "frame", "labelframe", "keywords", "container", "label", "groupbox", "local", "variables", "mode", "nroff", "end"]
manpage_name = "ttk::labelframe"
manpage_format = "troff"
operating_system_version = "15.3"
title = "ttk::labelframe(n)"
manpage_section = "n"
operating_system = "macos"
description = "A ttk::labelframe widget is a container used to group other widgets together.  It has an optional label, which may be a plain text string or another widget. -class-cursor-takefocus -style Specifies where to place the label.   Allowed values ar..."
+++

ttk::labelframe n 8.5 Tk "Tk Themed Widget"

---

## NAME

ttk::labelframe - Container widget with optional label

## SYNOPSIS

**ttk::labelframe** *pathName *?*options*?

---


## DESCRIPTION


A **ttk::labelframe** widget is a container used to group other widgets
together.  It has an optional label, which may be a plain text string or
another widget.

### Standard ttk_widget

-class	-cursor	-takefocus
-style

See the standard options manual entry for details.


## WIDGET-SPECIFIC OPTIONS

Specifies where to place the label.
Allowed values are (clockwise from the top upper left corner):
**nw**, **n**, **ne**, **en**, **e**, **es**,
**se**, **s**,**sw**, **ws**, **w** and **wn**.
The default value is theme-dependent.
Specifies the text of the label.
If set, specifies the integer index (0-based) of a character to
underline in the text string.
The underlined character is used for mnemonic activation.
Mnemonic activation for a **ttk::labelframe**
sets the keyboard focus to the first child of the **ttk::labelframe** widget.
Additional padding to include inside the border.
The name of a widget to use for the label.
If set, overrides the **-text** option.
The **-labelwidget** must be a child of the **labelframe** widget
or one of the **labelframe**'s ancestors, and must belong to the
same top-level widget as the **labelframe**.
If specified, the widget's requested width in pixels.
If specified, the widget's requested height in pixels.
(See *ttk::frame(n)* for further notes on **-width** and
**-height**).

## WIDGET COMMAND


Supports the standard widget commands
**configure**, **cget**, **identify**, **instate**, and **state**;
see *ttk::widget(n)*.

## SEE ALSO

ttk::widget(n), ttk::frame(n), labelframe(n)

## KEYWORDS

widget, frame, container, label, groupbox
'\" Local Variables:
'\" mode: nroff
'\" End:
