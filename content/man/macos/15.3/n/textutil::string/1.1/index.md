+++
operating_system = "macos"
manpage_format = "troff"
detected_package_version = "1.1"
operating_system_version = "15.3"
author = "None Specified"
description = "The package textutil::string provides miscellaneous string manipulation commands. The complete set of procedures is described below. ::textutil::string::chop string A convenience command. Removes the last character of string and returns the short..."
manpage_section = "n"
title = "textutil::string(n)"
keywords = ["regexp", "split", "string", "keywords", "capitalize", "chop", "common", "prefix", "formatting", "uncapitalize", "category", "text", "processing"]
manpage_name = "textutil::string"
date = "Sun Feb 16 04:48:28 2025"
+++

"textutil::string" n 0.7 textutil "Text and string utilities, macro processing"

---

## NAME

textutil::string - Procedures to manipulate texts and strings.

## SYNOPSIS

package require **Tcl  8.2**

package require **textutil::string  ?0.7?**

**::textutil::string::chop** *string*

**::textutil::string::tail** *string*

**::textutil::string::cap** *string*

**::textutil::string::uncap** *string*

**::textutil::string::longestCommonPrefixList** *list*

**::textutil::string::longestCommonPrefix** ?*string*...?


---


## DESCRIPTION

The package **textutil::string** provides miscellaneous string
manipulation commands.

The complete set of procedures is described below.

**::textutil::string::chop** *string*
A convenience command. Removes the last character of *string* and
returns the shortened string.

**::textutil::string::tail** *string*
A convenience command. Removes the first character of *string* and
returns the shortened string.

**::textutil::string::cap** *string*
Capitalizes the first character of *string* and returns the
modified string.

**::textutil::string::uncap** *string*
The complementary operation to **::textutil::string::cap**. Forces
the first character of *string* to lower case and returns the
modified string.

**::textutil::string::longestCommonPrefixList** *list*

**::textutil::string::longestCommonPrefix** ?*string*...?
Computes the longest common prefix for either the *string*s given
to the command, or the strings specified in the single *list*, and
returns it as the result of the command.

If no strings were specified the result is the empty string.  If only
one string was specified, the string itself is returned, as it is its
own longest common prefix.


## BUGS, IDEAS, FEEDBACK

This document, and the package it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *textutil* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## SEE ALSO

regexp(n), split(n), string(n)

## KEYWORDS

capitalize, chop, common prefix, formatting, prefix, string, uncapitalize

## CATEGORY

Text processing
