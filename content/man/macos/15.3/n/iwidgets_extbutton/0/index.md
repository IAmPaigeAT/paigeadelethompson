+++
operating_system = "macos"
manpage_format = "troff"
manpage_name = "iwidgets_extbutton"
description = "The extbutton extends the behavior of the atomic Tk button by allowing text and an image or bitmap to coexist. The user may use the -image or -bitmap options to specify an image as well as the -imagepos option to specify image position relative to ..."
title = "iwidgets_extbutton(n)"
manpage_section = "n"
operating_system_version = "15.3"
date = "Sun Feb 16 04:48:27 2025"
detected_package_version = "0"
author = "None Specified"
+++

iwidgets::extbutton iwid

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

iwidgets::extbutton - Extends the behavior of the Tk button by allowing
a bitmap or image to coexist with text.

## SYNOPSIS

**iwidgets::extbutton** *pathName *?*options*?

## INHERITANCE

itk::Widget <- iwidgets::Extbutton

## STANDARD OPTIONS



```
.ta 4c 8c 12c

activebackground    activeforeground  bitmap
background          bd                cursor
disabledforeground  font              foreground
image               justify           relief      text
```


See the "options" manual entry for details on the standard options.

## WIDGET-SPECIFIC OPTIONS

.ta 4c 8c 12c


```
Name:	bitmapforeground
Class:	Foreground
Command-Line Switch:	-bitmapforeground
```

.IP
Configures the foreground color of the bitmap.


```
Name:	command
Class:	Command
Command-Line Switch:	-command
```

.IP
Associate a command with the extbutton. Simulates a Tk button's
-command option. Invoked by either <1> events or by explicitly
calling the public invoke() method.


```
Name:   defaultring
Class:  DefaultRing
Command-Line Switch:    -defaultring
```

.IP
Boolean describing whether the extbutton displays its default ring given in
any of the forms acceptable to **Tcl_GetBoolean**.  The default is false.


```
Name:   defaultringpad
Class:  Pad
Command-Line Switch:    -defaultringpad
```

.IP
Specifies the amount of space to be allocated to the indentation of the
default ring ring given in any of the forms acceptable to **Tcl_GetPixels**.
The option has no effect if the defaultring option is set to false.  The
default is 4 pixels.


```
Name:   imagePos
Class:  Position
Command-Line Switch:    -imagepos
```

.IP
Specifies the image position relative to the message text: **n**, **ne**,
**nw**, **s**, **se**, **sw**, **w**, **wn**, **ws**, **e**,
**en**, or **es**. The default is w.


```
Name:	ringBackground
Class:	Background
Command-Line Switch:	-ringbackground
```

.IP
Configures the background color of the default ring frame (if -defaultring
is set to boolean true).


---



## DESCRIPTION


The **extbutton** extends the behavior of the atomic Tk button by
allowing text and an image or bitmap to coexist. The user may use
the -image or -bitmap options to specify an image as well as the
-imagepos option to specify image position relative to the text.
Note that the extbutton is not intended to be used without an
image/bitmap. There will be an emtpy space next to the text if
no image/bitmap is specified.


## METHODS


The **iwidgets::extbutton** command creates a new Tcl command whose
name is *pathName*.  This command may be used to invoke various
operations on the widget.  It has the following general form:

> *pathName option *?*arg arg ...*?




## INHERITED METHODS

Each of the following methods are inherited from itk::Archetype. See that
man page for details.

> \fIpathName **cget**
\fIpathName **component**
\fIpathName **config**
\fIpathName **configure**




## WIDGET-SPECIFIC METHODS


\fIpathName **invoke**
Evaluates the command fragment associated with the -command option.

\fIpathName **flash**
Simulates the Tk button's flash command.




## EXAMPLES


> package require Iwidgets 4.0
iwidgets::extbutton .eb -text "Bitmap example" -bitmap info \
  -background bisque -activeforeground red -bitmapforeground blue \
  -defaultring 1 -command \{puts "Bisque is beautiful"\}
pack .eb -expand 1

iwidgets::extbutton .eb -text "Image example" -relief ridge -image \
  [image create photo -file $itk::library/../iwidgets/demos/images/clear.gif] \
  -font 9x15bold -background lightgreen -imagepos e \
  -activebackground lightyellow
pack .eb -expand 1



## AUTHOR

Chad Smith

## KEYWORDS

button, pushbutton
