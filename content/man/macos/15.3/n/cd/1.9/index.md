+++
operating_system = "macos"
keywords = ["filename", "glob", "pwd", "keywords", "working", "directory"]
title = "cd(n)"
operating_system_version = "15.3"
author = "None Specified"
manpage_name = "cd"
manpage_section = "n"
date = "Sun Feb 16 04:48:28 2025"
detected_package_version = "1.9"
manpage_format = "troff"
description = "Change the current working directory to dirName, or to the home directory (as specified in the HOME environment variable) if dirName is not given. Returns an empty string. Note that the current working directory is a per-process resource; the cd ..."
+++

cd n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

cd - Change working directory

## SYNOPSIS

**cd **?*dirName*?

---



## DESCRIPTION


Change the current working directory to *dirName*, or to the
home directory (as specified in the HOME environment variable) if
*dirName* is not given.
Returns an empty string.
Note that the current working directory is a per-process resource; the
**cd** command changes the working directory for all interpreters
and (in a threaded environment) all threads.

## EXAMPLES

Change to the home directory of the user **fred**:

```
**cd** ~fred
```


Change to the directory **lib** that is a sibling directory of the
current one:

```
**cd** ../lib
```



## SEE ALSO

filename(n), glob(n), pwd(n)


## KEYWORDS

working directory
