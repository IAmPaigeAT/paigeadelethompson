+++
manpage_format = "troff"
detected_package_version = "1.1"
title = "treeql(n)"
manpage_name = "treeql"
operating_system_version = "15.3"
date = "Sun Feb 16 04:48:27 2025"
author = "None Specified"
manpage_section = "n"
description = "This package provides objects which can be used to query and transform tree objects following the API of tree objects created by the package struct::tree. The tree query and manipulation language used here, TreeQL, is inspired by Cost (See section..."
operating_system = "macos"
+++


## REFERENCES

[1]
*COST* [http://wiki.tcl.tk/COST] on the Tcler's Wiki.
[2]
*TreeQL* [http://wiki.tcl.tk/treeql] on the Tcler's Wiki. Discuss
this package there.


## BUGS, IDEAS, FEEDBACK

This document, and the package it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *treeql* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## KEYWORDS

Cost, DOM, TreeQL, XPath, XSLT, structured queries, tree, tree query language

## CATEGORY

Data structures

## COPYRIGHT


```
Copyright (c) 2004 Colin McCormack <coldstore@users.sourceforge.net>
Copyright (c) 2004 Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

