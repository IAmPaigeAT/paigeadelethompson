+++
keywords = ["exec", "keywords", "exit", "process"]
author = "None Specified"
description = "Terminate the process, returning returnCode to the system as the exit status. If returnCode is not specified then it defaults to 0. Since non-zero exit codes are usually interpreted as error cases by the calling process, the exit command is an im..."
detected_package_version = "1.9"
manpage_section = "n"
date = "Sun Feb 16 04:48:28 2025"
operating_system = "macos"
manpage_name = "exit"
title = "exit(n)"
manpage_format = "troff"
operating_system_version = "15.3"
+++

exit n "" Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

exit - End the application

## SYNOPSIS

**exit **?*returnCode*?

---



## DESCRIPTION


Terminate the process, returning *returnCode* to the
system as the exit status.
If *returnCode* is not specified then it defaults
to 0.

## EXAMPLE

Since non-zero exit codes are usually interpreted as error cases by
the calling process, the **exit** command is an important part of
signaling that something fatal has gone wrong. This code fragment is
useful in scripts to act as a general problem trap:

```
proc main \{\} \{
    # ... put the real main code in here ...
\}

if \{[catch \{main\} msg options]\} \{
    puts stderr "unexpected script error: $msg"
    if \{[info exist env(DEBUG)]\} \{
        puts stderr "---- BEGIN TRACE ----"
        puts stderr [dict get $options -errorinfo]
        puts stderr "---- END TRACE ----"
    \}

    # Reserve code 1 for "expected" error exits...
    **exit** 2
\}
```



## SEE ALSO

exec(n)


## KEYWORDS

exit, process
