+++
manpage_format = "troff"
operating_system = "macos"
operating_system_version = "15.3"
manpage_name = "diagram"
title = "diagram(n)"
author = "None Specified"
detected_package_version = "1.1"
date = "Sun Feb 16 04:48:30 2025"
manpage_section = "n"
description = "Welcome to diagram, a package for the easy construction of diagrams (sic), i.e. 2D vector graphics, sometimes also called pictures. Note that this package is not a replacement for Tks canvas, but rather a layer sitting on top of it, to make it eas..."
+++



*point1* **+** *point2*

IMAGE: figure-48-point-vectoradd

This command interprets two points as vectors and adds them together.
If at least one of the points is *absolute* the result is
absolute as well.
The result is a *relative* point if and only if both points are
*relative*.

*point1* **-** *point2*

IMAGE: figure-49-point-vectorsub

This command interprets two points as vectors and subtracts the second
from the first.
If at least one of the points is *absolute* the result is
absolute as well.
The result is a *relative* point if and only if both points are
*relative*.

*point* **by** *distance* *direction*
This command is a more convenient, or at least shorter, form of

```

    [$point + [by $distance $direction]]

```


*point1* **|** *point2*

IMAGE: figure-31-point-projection

This command calculates the *projection* of two points, i.e. the
result is the point having the x-coordinate of *point1* and the
y-coordinate of *point2*.

*n* **between** *poin1* *point2*

IMAGE: figure-29-point-interpolation-1

This command computes the point which is *n**100 percent of the
way between *point1* and *point2*, and returns it as its
result.
This means that for

> 
*n* == 0
The result is *point1*.

*n* == 1
The result is *point2*.

*n* == 0.5
The result is half way between the two points.


etc.
*Note* that it is allowed to use values < 0 and > 1 for *n*

**intersect** *elem1* *elem2*

IMAGE: figure-32-point-intersection

This command takes two *open* elements, computes the lines going
through their "start"- and "end"-corners, and returns the point where
these two lines intersect.
The command throws an error if the lines do not intersect, or are
coincident.

*element* **names** ?*pattern*?
This command returns a list containing the names of all corners for
the *element*. If a pattern is specified then only the names
matching it (via **string match** are returned. Otherwise all
names are returned (equivalent to a default pattern of *****).

*element* *corner*
This command returns the value for the *corner* of the
*element*.
This can be anything, including points and elements.

*element* *corner1* *corner2*...
This is a convenience shorthand for

```

[[[$elem $corner1] $corner2] ...]

```

assuming that the value for

```
 [$elem $corner1]
```

again an element.



*element* ?*corner1*... ?**names** ?*pattern*??]?


This is a convenience shorthand for




```

[[[$elem $corner1] ...] names ?pattern?]

```

assuming that the value for

```
 [$elem $corner1]
```

again an element.



****n**th** ?*corner*?


This command asks the diagram history for the **n**th element
created, searching from the beginning of the history (counting from 1)
and returns it as its result.

If the *corner* is specified then the value for this corner is
returned instead.




****n**th** **last** ?*corner*?


This command asks the diagram history for the **n**th element
created, searching from the end of the history and returns it as its
result.

If the *corner* is specified then the value for this corner is
returned instead.




****n**th** *shape* ?*corner*?


This command asks the diagram history for the **n**th element
created, of the given *shape*, searching from the beginning of the
history (counting from 1) and returns it as its result.

If the *corner* is specified then the value for this corner is
returned instead.




****n**th** **last** *shape* ?*corner*?


This command asks the diagram history for the **n**th element
created, of the given *shape*, searching from the end of the
history and returns it as its result.

If the *corner* is specified then the value for this corner is
returned instead.




**last** ?*corner*?



**last** *shape* ?*corner*?


Convenience commands mapping to "**1st last**"
and "**1st last** *shape*".




**1st**



**2nd**



**3rd**


Aliases for **1th**, **2th**, and **3th**, for readability,
usable whereever ****n**th** can ocur.







### VARIABLES


The language context contains a number of predefined variables which
hold the default values for various attributes. These variables, their
uses, and values are:




**anchor**


The default value for the attribute **anchor**.

Initialized to **center**.


The legal values are all those accepted by
*Tk_GetAnchor* [http://www.tcl.tk/man/tcl8.5/TkLib/GetAnchor.htm].




**arcradius**


The default value for the attribute **radius** of **arc**
elements.

Initialized to the pixel equivalent of **1 cm**.



**arrowhead**


The default value for the attribute **arrowhead**.

Initialized to **none**.

The legal values are


> 

**none**, **-**

Draw no arrowheads, at neither end of the line.



**start**, **first**, **<-**

Draw an arrowhead at the beginning of the line, but not at its end.



**end**, **last**, **->**

Draw an arrowhead at the end of the line, but not at its beginning.



**both**, **<->**

Draw arrowheads at both ends of the line.







**boxheight**


The default value for the attribute **height** of **box**,
**diamond** and **ellipse** elements.

Initialized to the pixel equivalent of **2 cm**.



**boxwidth**


The default value for the attribute **width** of **box**,
**diamond** and **ellipse** elements.

Initialized to the pixel equivalent of **2 cm**.



**clockwise**


The default value for the attributes **clockwise** and
**counterclockwise** of **arc** elements.

Initialized to **False**, for counter-clockwise direction.



**circleradius**


The default value for the attribute **radius** of **circle**
elements, and also the default for the attribute **chop**, when
specified without an explicit length.

Initialized to the pixel equivalent of **1 cm**.



**drumaspect**


The default value for the attribute **aspect** of **drum**
elements.

Initialized to **0.35**.



**fillcolor**


The default value for the attribute **fillcolor** of all elements
which can be filled.

Initialized to the empty string, signaling that the element is not
filled.



**justify**


The default value for the attribute **justify**.

Initialized to **left**.


The legal values are **left**, **right**, and **center**.




**linecolor**


The default value for the attribute **color** of all elements having
to draw lines (all but **text**).

Initialized to **black**.



**linestyle**


The default value for the attribute **style** of all elements
having to draw some line.

Initialized to **solid**.


The legal values are all those accepted by
*Tk_GetDash* [http://www.tcl.tk/man/tcl8.5/TkLib/GetDash.htm],

and additionally all which are listed below:



> 

**solid**, empty string
		Draw solid line.


**dash**, **dashed**, **-**
	Draw a dashed line.


**dot**, **dotted**, **.**
	Draw a dotted line.


**dash-dot**, **-.**
	  	Draw a dash-dotted line


**dash-dot-dot**, **-..**
	Draw a dash-dot-dotted line.







**linewidth**


The default value for the attribute **stroke** of all elements
having to draw some line.

Initialized to **1** (pixels).



**movelength**


The default value for the directional specification of intermediate
locations by the attribute **then** of **line** and **move**
elements.

Initialized to the pixel equivalent of **2 cm**.



**slant**


The default value for the attribute **slant** of **box** elements.

Initialized to 90 degrees, i.e. slant straight up.



**textcolor**


The default value for the attribute **textcolor** of all elements
having to draw some text.

Initialized to **black**.



**textfont**


The default value for the attribute **textfont** of all elements
having to draw some text.

Initialized to **Helvetica 12pt**.







## DIAGRAM CLASSES

on the internals of the diagram package.

Regular users of **diagram** can skip this section without
missing anything.





The main information seen here is the figure below, showing the
hierarchy of the classes implementing diagram.



IMAGE: figure-00-dependencies



At the bottom, all at the same level are the supporting packages like
**snit**, etc. These can all be found in Tcllib.





Above them is the set of diagram classes implementing the various
aspects of the system, i.e.:





**diagram**

The main class, that which is seen by the user.



**diagram::core**

The core engine, itself distributed over four helper classes.



**diagram::basic**

The implementation of the standard shapes, like box, circle, etc.,
based on the extension features of the core.



**diagram::element**

Core support class, the database of created elements. It also keeps
the history, i.e. the order in which elements were created.



**diagram::attribute**

Core support class, the generic handling of definition and processing
of attributes.



**diagram::direction**

Core support class, the database of named directions.



**diagram::navigation**

Core support class, the state of layout engine, i.e. current position
and directin, and operations on it.



**diagram::point**

General support class handling various vector operations.







## REFERENCES





## KEYWORDS

2D geometry, arc, arrow, box, canvas, circle, diagram, diamond, drawing, drum, ellipse, image, interpolation, intersection, line, move, picture, plane geometry, plotting, point, raster image, spline, text, vector


## CATEGORY

Documentation tools

