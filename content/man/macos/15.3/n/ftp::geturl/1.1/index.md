+++
description = "This package provides a command which wraps around the client side of the ftp protocol provided by package ftp to allow the retrieval of urls using the ftp schema. ::ftp::geturl url This command can be used by the generic command ::uri::geturl (S..."
detected_package_version = "1.1"
manpage_name = "ftp::geturl"
author = "None Specified"
manpage_format = "troff"
date = "Sun Feb 16 04:48:30 2025"
operating_system = "macos"
title = "ftp::geturl(n)"
keywords = ["ftpd", "mime", "pop3", "smtp", "keywords", "ftp", "internet", "net", "rfc", "959", "category", "networking"]
manpage_section = "n"
operating_system_version = "15.3"
+++

"ftp::geturl" n 0.2.1 ftp "ftp client"

---

## NAME

ftp::geturl - Uri handler for ftp urls

## SYNOPSIS

package require **Tcl  8.2**

package require **ftp::geturl  ?0.2.1?**

**::ftp::geturl** *url*


---


## DESCRIPTION

This package provides a command which wraps around the client side of
the *ftp* protocol provided by package **ftp** to allow the
retrieval of urls using the *ftp* schema.

## API


**::ftp::geturl** *url*
This command can be used by the generic command **::uri::geturl**
(See package **uri**) to retrieve the contents of ftp
urls. Internally it uses the commands of the package **ftp** to
fulfill the request.

The contents of a *ftp* url are defined as follows:

> 
*file*
The contents of the specified file itself.

*directory*
A listing of the contents of the directory in key value notation where
the file name is the key and its attributes the associated value.

*link*
The attributes of the link, including the path it refers to.




## BUGS, IDEAS, FEEDBACK

This document, and the package it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *ftp* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## SEE ALSO

ftpd, mime, pop3, smtp

## KEYWORDS

ftp, internet, net, rfc 959

## CATEGORY

Networking
