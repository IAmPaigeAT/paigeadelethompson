+++
detected_package_version = "0"
operating_system_version = "15.3"
manpage_format = "troff"
operating_system = "macos"
manpage_name = "iwidgets_fileselectionbox"
title = "iwidgets_fileselectionbox(n)"
description = "The iwidgets::fileselectionbox command creates a file selection box similar  to the OSF/Motif standard Xmfileselectionbox composite widget.  The  fileselectionbox is composed of directory and file scrolled lists as  well as filter and selection ent..."
date = "Sun Feb 16 04:48:30 2025"
author = "None Specified"
manpage_section = "n"
+++

iwidgets::fileselectionbox iwid

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

iwidgets::fileselectionbox - Create and manipulate a file selection box widget

## SYNOPSIS

**iwidgets::fileselectionbox** *pathName *?*options*?

## INHERITANCE

itk::Widget <- iwidgets::Fileselectionbox

## STANDARD OPTIONS



```
.ta 4c 8c 12c

activeBackground   background         borderWidth         cursor
foreground         highlightColor     highlightThickness  insertBackground
insertBorderWidth  insertOffTime      insertOnTime        insertWidth
selectBackground   selectBorderWidth  selectForeground
```


See the "options" manual entry for details on the standard options.

## ASSOCIATED OPTIONS



```
.ta 4c 8c 12c
textBackground	textFont
```


See the "entryfield" widget manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
labelFont
```


See the "labeledwidget" widget manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
activeRelief	elementBorderWidth	jump	troughColor
```


See the "scrollbar" widget class manual entry for details on the above
associated options.


```
.ta 4c 8c 12c
textBackground	textFont
```


See the "scrolledlistbox" widget manual entry for details on the above
associated options.

## WIDGET-SPECIFIC OPTIONS



```
Name:	automount
Class:	Automount
Command-Line Switch:	-automount
```

.IP
Specifies a list of directory prefixes to ignore. Typically, this
option would be used as follows:
    -automount \{export tmp_mnt\}


```
Name:	childSitePos
Class:	Position
Command-Line Switch:	-childsitepos
```

.IP
Specifies the position of the child site in the selection box: **n**,
**s**, **e**, **w**, **top**, **bottom**, or **center**.  The
default is s.

.IP
Specifies a Tcl command procedure which is called when an file list item is
double clicked.  Typically this occurs when mouse button 1 is double
clicked over a file name.


```
Name:	directory
Class:	Directory
Command-Line Switch:	-directory
```

.IP
Specifies the initial default directory.  The default is the present
working directory.


```
Name:	dirSearchCommand
Class:	Command
Command-Line Switch:	-dirsearchcommand
```

.IP
Specifies a Tcl command to be executed to perform a directory search.
The command will receive the current working directory and filter
mask as arguments.  The command should return a list of files which
will be placed into the directory list.


```
Name:	dirsLabel
Class:	Text
Command-Line Switch:	-dirslabel
```

.IP
Specifies the text of the label for the directory list.  The default is
"Directories".


```
Name:	dirsOn
Class:	DirsOn
Command-Line Switch:	-dirson
```

.IP
Specifies whether or not to display the directory list.  The
value may be given in any of the forms acceptable to **Tcl_GetBoolean**.
The default is true.


```
Name:	fileSearchCommand
Class:	Command
Command-Line Switch:	-filesearchcommand
```

.IP
Specifies a Tcl command to be executed to perform a file search.
The command will receive the current working directory and filter
mask as arguments.  The command should return a list of files which
will be placed into the file list.


```
Name:	filesLabel
Class:	Text
Command-Line Switch:	-fileslabel
```

.IP
Specifies the text of the label for the files list.  The default is "Files".


```
Name:	filesOn
Class:	FilesOn
Command-Line Switch:	-fileson
```

.IP
Specifies whether or not to display the files list.  The
value may be given in any of the forms acceptable to **Tcl_GetBoolean**.
The default is true.


```
Name:	fileType
Class:	FileType
Command-Line Switch:	-filetype
```

.IP
Specify the type of files which may appear in the file list: **regular**,
**directory**, or **any**.  The default is regular.


```
Name:	filterCommand
Class:	Command
Command-Line Switch:	-filtercommand
```

.IP
Specifies a Tcl command to be executed upon hitting the Return key
in the filter entry widget.


```
Name:	filterLabel
Class:	Text
Command-Line Switch:	-filterlabel
```

.IP
Specifies the text of the label for the filter entry field.  The default is
"Filter".


```
Name:	filterOn
Class:	FilterOn
Command-Line Switch:	-filteron
```

.IP
Specifies whether or not to display the filter entry.  The
value may be given in any of the forms acceptable to **Tcl_GetBoolean**.
The default is true.


```
Name:	height
Class:	Height
Command-Line Switch:	-height
```

.IP
Specifies the height of the selection box.  The value may be specified in
any of the forms acceptable to Tk_GetPixels.  The default is 360 pixels.


```
Name:	invalid
Class:	Command
Command-Line Switch:	-invalid
```

.IP
Command to be executed should the filter contents be proven
invalid.  The default is \{bell\}.


```
Name:	mask
Class:	Mask
Command-Line Switch:	-mask
```

.IP
Specifies the initial file mask string.  The default is "*".


```
Name:	noMatchString
Class:	NoMatchString
Command-Line Switch:	-nomatchstring
```

.IP
Specifies the string to be displayed in the files list should no files
match the mask.  The default is "".


```
Name:	selectDirCommand
Class:	Command
Command-Line Switch:	-selectdirommand
```

.IP
Specifies a Tcl command to be executed following selection of a
directory in the directory list.


```
Name:	selectFileCommand
Class:	Command
Command-Line Switch:	-selectfileommand
```

.IP
Specifies a Tcl command to be executed following selection of a
file in the files list.


```
Name:	selectionCommand
Class:	Command
Command-Line Switch:	-selectioncommand
```

.IP
Specifies a Tcl command to be executed upon hitting the Return key
in the selection entry widget.


```
Name:	selectionLabel
Class:	Text
Command-Line Switch:	-selectionlabel
```

.IP
Specifies the text of the label for the selection entry field.  The default
is "Selection".


```
Name:	selectionOn
Class:	SelectionOn
Command-Line Switch:	-selectionon
```

.IP
Specifies whether or not to display the selection entry.  The
value may be given in any of the forms acceptable to **Tcl_GetBoolean**.
The default is true.


```
Name:	width
Class:	Width
Command-Line Switch:	-width
```

.IP
Specifies the width of the selection box.  The value may be specified in
any of the forms acceptable to Tk_GetPixels.  The default is 470 pixels.


---



## DESCRIPTION


The **iwidgets::fileselectionbox** command creates a file selection box similar
to the OSF/Motif standard Xmfileselectionbox composite widget.  The
fileselectionbox is composed of directory and file scrolled lists as
well as filter and selection entry fields.  Bindings are in place such that
selection of a directory list item loads the filter entry field and
selection of a file list item loads the selection entry field.  Options
exist to control the appearance and actions of the widget.


## METHODS


The **iwidgets::fileselectionbox** command creates a new Tcl command whose
name is *pathName*.  This
command may be used to invoke various
operations on the widget.  It has the following general form:

> *pathName option *?*arg arg ...*?


*Option* and the *arg*s
determine the exact behavior of the command.  The following
commands are possible for fileselectionbox widgets:


## WIDGET-SPECIFIC METHODS


*pathName **cget** \fIoption*
Returns the current value of the configuration option given
by *option*.
*Option* may have any of the values accepted by the **iwidgets::fileselectionbox**
command.

\fIpathName **childsite**
Returns the child site widget path name.

*pathName* **configure** ?*option*? ?*value option value ...*?
Query or modify the configuration options of the widget.
If no *option* is specified, returns a list describing all of
the available options for *pathName* (see **Tk_ConfigureInfo** for
information on the format of this list).  If *option* is specified
with no *value*, then the command returns a list describing the
one named option (this list will be identical to the corresponding
sublist of the value returned if no *option* is specified).  If
one or more *option-value* pairs are specified, then the command
modifies the given widget option(s) to have the given value(s);  in
this case the command returns an empty string.
*Option* may have any of the values accepted by the **iwidgets::fileselectionbox**
command.

\fIpathName **filter**
Update the current contents of the file selection box based on the current
filter entry field value.

\fIpathName **get**
Returns the current value of the selection entry widget.


## COMPONENTS



```
Name:	dirs
Class:	Scrolledlistbox
```

.IP
The dirs component is the directory list box for the file selection box.
See the "scrolledlistbox" widget manual entry for details on the dirs
component item.


```
Name:	files
Class:	Scrolledlistbox
```

.IP
The files component is the file list box for the file selection box.
See the "scrolledlistbox" widget manual entry for details on the files
component item.


```
Name:	filter
Class:	Entryfield
```

.IP
The filter component is the entry field for user input of the filter value.
See the "entryfield" widget manual entry for details on the filter
component item.


```
Name:	selection
Class:	Entryfield
```

.IP
The selection component is the entry field for user input of the currently
selected file value.  See the "entryfield" widget manual entry for details
on the selection component item.



## EXAMPLE


> package require Iwidgets 4.0
iwidgets::fileselectionbox .fsb
pack .fsb -padx 10 -pady 10 -fill both -expand yes



## AUTHOR(S)

Mark L. Ulferts

## KEYWORDS

fileselectionbox, widget
