+++
description = "The command unstack is an interface to the public Tcl API function TclUnstackChannel. It unstacks the topmost transformation from the specified channel if there is any. unstack channel Removes the topmost transformation from the specified channel..."
manpage_format = "troff"
author = "None Specified"
manpage_section = "n"
operating_system_version = "15.3"
title = "unstack(n)"
manpage_name = "unstack"
keywords = ["trf-intro", "keywords", "removal", "transformation", "unstacking", "copyright", "c", "1996-2003", "andreas", "kupries", "andreas_kupries", "users", "sourceforge", "net"]
operating_system = "macos"
date = "Sun Feb 16 04:48:27 2025"
detected_package_version = "1.1"
+++

"unstack" n 2.1.4  "Trf transformer commands"

---

## NAME

unstack - Unstacking channels

## SYNOPSIS

package require **Tcl  ?8.2?**

package require **Trf  ?2.1.4?**

**unstack** *channel*


---


## DESCRIPTION

The command **unstack** is an interface to the public Tcl API
function **TclUnstackChannel**. It unstacks the topmost
transformation from the specified channel if there is any.


**unstack** *channel*
Removes the topmost transformation from the specified
*channel*. If the *channel* has no transformation associated
with it it will be closed. In other words, in this situation the
command is equivalent to **close**.


## SEE ALSO

trf-intro

## KEYWORDS

removal, transformation, unstacking

## COPYRIGHT


```
Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

