+++
operating_system = "macos"
author = "None Specified"
date = "Sun Feb 16 04:48:29 2025"
operating_system_version = "15.3"
manpage_section = "n"
title = "term(n)"
description = "It is planned to have this package provide highlevel general terminal control commands, in the vein of ncurses or similar packages. Currently nothing has been implemented however. I.e. this package is empty. It can be loaded, yet provides nothing...."
manpage_name = "term"
manpage_format = "troff"
detected_package_version = "1.1"
+++

"term" n 0.1 term "Terminal control"

---

## NAME

term - General terminal control

## SYNOPSIS

package require **Tcl  8.4**

package require **term  ?0.1?**


---


## DESCRIPTION

It is planned to have this package provide highlevel general terminal control
commands, in the vein of ncurses or similar packages. Currently nothing has
been implemented however. I.e. this package is empty. It can be loaded, yet
provides nothing.

## BUGS, IDEAS, FEEDBACK

This document, and the package it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *term* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## KEYWORDS

control, terminal

## CATEGORY

Terminal control

## COPYRIGHT


```
Copyright (c) 2006 Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

