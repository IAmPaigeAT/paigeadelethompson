+++
operating_system_version = "15.3"
manpage_section = "n"
manpage_name = "bell"
description = "This command rings the bell on the display for window and returns an empty string. If the -displayof option is omitted, the display of the applications main window is used by default. The command uses the current bell-related settings for the dis..."
manpage_format = "troff"
author = "None Specified"
operating_system = "macos"
title = "bell(n)"
detected_package_version = "1.9"
date = "Sun Feb 16 04:48:29 2025"
+++

bell n 8.4 Tk "Tk Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

bell - Ring a display's bell

## SYNOPSIS

**bell **?**-displayof **window? ?**-nice**?

---



## DESCRIPTION


This command rings the bell on the display for *window* and
returns an empty string.
If the **-displayof** option is omitted, the display of the
application's main window is used by default.
The command uses the current bell-related settings for the display, which
may be modified with programs such as **xset**.

If **-nice** is not specified, this command also resets the screen saver
for the screen.  Some screen savers will ignore this, but others will reset
so that the screen becomes visible again.


## KEYWORDS

beep, bell, ring
