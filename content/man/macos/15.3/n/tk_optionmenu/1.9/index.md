+++
description = "This procedure creates an option menubutton whose name is pathName, plus an associated menu. Together they allow the user to select one of the values given by the value arguments. The current value will be stored in the global variable whose name..."
operating_system = "macos"
manpage_section = "n"
author = "None Specified"
title = "tk_optionmenu(n)"
detected_package_version = "1.9"
date = "Sun Feb 16 04:48:28 2025"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_name = "tk_optionmenu"
+++

tk_optionMenu n 4.0 Tk "Tk Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

tk_optionMenu - Create an option menubutton and its menu

## SYNOPSIS

**tk_optionMenu **pathName varName value ?*value value ...*?

---


## DESCRIPTION


This procedure creates an option menubutton whose name is *pathName*,
plus an associated menu.
Together they allow the user to select one of the values
given by the *value* arguments.
The current value will be stored in the global variable whose
name is given by *varName* and it will also be displayed as the label
in the option menubutton.
The user can click on the menubutton to display a menu containing
all of the *value*s and thereby select a new value.
Once a new value is selected, it will be stored in the variable
and appear in the option menubutton.
The current value can also be changed by setting the variable.

The return value from **tk_optionMenu** is the name of the menu
associated with *pathName*, so that the caller can change its
configuration options or manipulate it in other ways.

## EXAMPLE


```
tk_optionMenu .foo myVar Foo Bar Boo Spong Wibble
pack .foo
```


## KEYWORDS

option menu
