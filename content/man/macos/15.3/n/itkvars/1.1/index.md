+++
manpage_section = "n"
date = "Sun Feb 16 04:48:28 2025"
operating_system = "macos"
manpage_format = "troff"
detected_package_version = "1.1"
author = "None Specified"
operating_system_version = "15.3"
description = "The following global variables are created and managed automatically by the [incr Tk] library.  Except where noted below, these variables should normally be treated as read-only by application-specific code and by users. itk::library When an inte..."
title = "itkvars(n)"
manpage_name = "itkvars"
+++

itkvars n 3.0 itk "[incr\ Tk]"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

itkvars - variables used by [incr\ Tk]

---



## DESCRIPTION


The following global variables are created and managed automatically
by the **[incr\ Tk]** library.  Except where noted below, these
variables should normally be treated as read-only by application-specific
code and by users.

**itk::library**
When an interpreter is created, **[incr\ Tk]** initializes this
variable to hold the name of a directory containing the system library
of **[incr\ Tk]** scripts.  The initial value of **itk::library**
is set from the ITK_LIBRARY environment variable if it exists,
or from a compiled-in value otherwise.

When **[incr\ Tk]** is added to an interpreter, it executes
the script "\fCinit.itk" in this directory.  This script,
in turn, looks for other script files with the name "\fCinit.*xxx*".
Mega-widget libraries will be automatically initialized if they
install a script named "\fCinit.*xxx*" in this directory,
where "*xxx*" is the name of the mega-widget library.
For example, the **[incr\ Widgets]** library installs
the script "\fCinit.iwidgets" in this directory.
This script establishes the "iwidgets" namespace, and sets
up autoloading for all **[incr\ Widgets]** commands.


## KEYWORDS

itcl, itk, variables
