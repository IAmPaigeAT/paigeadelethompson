+++
manpage_format = "troff"
manpage_section = "n"
author = "None Specified"
detected_package_version = "1.1"
date = "Sun Feb 16 04:48:28 2025"
manpage_name = "page_intro"
description = "page (short for parser generator) stands for a set of related packages which help in the construction of parser generators, and other utilities doing text processing. They are mainly geared towards supporting the Tcllib application page, with the ..."
operating_system = "macos"
operating_system_version = "15.3"
title = "page_intro(n)"
+++

"page_intro" n 1.0 page "Parser generator tools"

---

## NAME

page_intro - page introduction

## DESCRIPTION


*page* (short for *parser generator*) stands for a set of
related packages which help in the construction of parser generators,
and other utilities doing text processing.

They are mainly geared towards supporting the Tcllib application
**page**, with the package **page::pluginmgr** in a central
role as the plugin management for the application. The other packages
are performing low-level text processing and utility tasks geared
towards parser generation and mainly accessed by **page** through
plugins.

The packages implementing the plugins are not documented as regular
packages, as they cannot be loaded into a general interpreter, like
tclsh, without extensive preparation of the interpreter. Preparation
which is done for them by the plugin manager.

## BUGS, IDEAS, FEEDBACK

This document, will undoubtedly contain bugs and other problems.
Please report such in the category *page* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have.

## KEYWORDS

page, parser generator, text processing

## CATEGORY

Page Parser Generator

## COPYRIGHT


```
Copyright (c) 2007 Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

