+++
description = "This package provides a container class for finite automatons (Short: FA). It allows the incremental definition of the automaton, its manipulation and querying of the definition. While the package provides complex operations on the automaton (via..."
manpage_section = "n"
operating_system = "macos"
date = "Sun Feb 16 04:48:31 2025"
operating_system_version = "15.3"
manpage_name = "grammar::fa"
detected_package_version = "1.1"
manpage_format = "troff"
author = "None Specified"
title = "grammar::fa(n)"
+++

state in St and ending at a state in Fi whose edges have the labels
sy_1, sy_2, etc. to sy_n.
The set of all strings accepted by the FA is the *language* of
the FA. One important equivalence is that the set of languages which
can be accepted by an FA is the set of *regular languages*.

Another important concept is that of deterministic FAs. A FA is said
to be *deterministic* if for each string of input symbols there
is exactly one path in the graph of the FA beginning at the start
state and whose edges are labeled with the symbols in the string.
While it might seem that non-deterministic FAs to have more power of
recognition, this is not so. For each non-deterministic FA we can
construct a deterministic FA which accepts the same language (-->
Thompson's subset construction).

While one of the premier applications of FAs is in *parsing*,
especially in the *lexer* stage (where symbols == characters),
this is not the only possibility by far.

Quite a lot of processes can be modeled as a FA, albeit with a
possibly large set of states. For these the notion of accepting states
is often less or not relevant at all. What is needed instead is the
ability to act to state changes in the FA, i.e. to generate some
output in response to the input.
This transforms a FA into a *finite transducer*, which has an
additional set OSy of *output symbols* and also an additional
*output function* O which maps from "S x (Sy + epsilon)" to
"(Osy + epsilon)", i.e a combination of state and input, possibly
empty to an output symbol, or nothing.

For the graph representation this means that edges are additional
labeled with the output symbol to write when this edge is traversed
while matching input. Note that for an application "writing an output
symbol" can also be "executing some code".

Transducers are not handled by this package. They will get their own
package in the future.

## BUGS, IDEAS, FEEDBACK

This document, and the package it describes, will undoubtedly contain
bugs and other problems.
Please report such in the category *grammar_fa* of the
*Tcllib SF Trackers* [http://sourceforge.net/tracker/?group_id=12883].
Please also report any ideas for enhancements you may have for either
package and/or documentation.

## KEYWORDS

automaton, finite automaton, grammar, parsing, regular expression, regular grammar, regular languages, state, transducer

## CATEGORY

Grammars and finite automata

## COPYRIGHT


```
Copyright (c) 2004-2009 Andreas Kupries <andreas_kupries@users.sourceforge.net>

```

