+++
manpage_format = "troff"
operating_system_version = "15.3"
author = "None Specified"
description = "Returns 1 if an end of file condition occurred during the most recent input operation on channelId (such as gets), 0 otherwise. ChannelId must be an identifier for an open channel such as a Tcl standard channel (stdin, stdout, or stderr), the ret..."
manpage_name = "eof"
manpage_section = "n"
date = "Sun Feb 16 04:48:31 2025"
detected_package_version = "1.9"
operating_system = "macos"
title = "eof(n)"
keywords = ["file", "open", "close", "fblocked", "tcl_standardchannels", "keywords", "channel", "end", "of"]
+++

eof n 7.5 Tcl "Tcl Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

eof - Check for end of file condition on channel

## SYNOPSIS

**eof **channelId

---



## DESCRIPTION


Returns 1 if an end of file condition occurred during the most
recent input operation on *channelId* (such as **gets**),
0 otherwise.

*ChannelId* must be an identifier for an open channel such as a
Tcl standard channel (**stdin**, **stdout**, or **stderr**),
the return value from an invocation of **open** or **socket**, or
the result of a channel creation command provided by a Tcl extension.

## EXAMPLES

Read and print out the contents of a file line-by-line:

```
set f [open somefile.txt]
while \{1\} \{
    set line [gets $f]
    if \{[**eof** $f]\} \{
        close $f
        break
    \}
    puts "Read line: $line"
\}
```


Read and print out the contents of a file by fixed-size records:

```
set f [open somefile.dat]
fconfigure $f -translation binary
set recordSize 40
while \{1\} \{
    set record [read $f $recordSize]
    if \{[**eof** $f]\} \{
        close $f
        break
    \}
    puts "Read record: $record"
\}
```



## SEE ALSO

file(n), open(n), close(n), fblocked(n), Tcl_StandardChannels(3)


## KEYWORDS

channel, end of file
