+++
title = "tk_popup(n)"
manpage_section = "n"
keywords = ["bind", "menu", "tk_optionmenu", "keywords", "popup"]
manpage_format = "troff"
author = "None Specified"
date = "Sun Feb 16 04:48:31 2025"
detected_package_version = "1.9"
operating_system = "macos"
description = "This procedure posts a menu at a given position on the screen and configures Tk so that the menu and its cascaded children can be traversed with the mouse or the keyboard. Menu is the name of a menu widget and x and y are the root coordinates at w..."
manpage_name = "tk_popup"
operating_system_version = "15.3"
+++

tk_popup n 4.0 Tk "Tk Built-In Commands"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

tk_popup - Post a popup menu

## SYNOPSIS

**tk_popup **menu x y ?*entry*?

---



## DESCRIPTION


This procedure posts a menu at a given position on the screen and
configures Tk so that the menu and its cascaded children can be
traversed with the mouse or the keyboard.
*Menu* is the name of a menu widget and *x* and *y*
are the root coordinates at which to display the menu.
If *entry* is omitted or an empty string, the
menu's upper left corner is positioned at the given point.
Otherwise *entry* gives the index of an entry in *menu* and
the menu will be positioned so that the entry is positioned over
the given point.

## EXAMPLE

How to attach a simple popup menu to a widget.

```
# Create a menu
set m [menu .popupMenu]
$m add command -label "Example 1" -command bell
$m add command -label "Example 2" -command bell

# Create something to attach it to
pack [label .l -text "Click me!"]

# Arrange for the menu to pop up when the label is clicked
bind .l <1> \{**tk_popup** .popupMenu %X %Y\}
```



## SEE ALSO

bind(n), menu(n), tk_optionMenu(n)


## KEYWORDS

menu, popup
