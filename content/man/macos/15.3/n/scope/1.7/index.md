+++
manpage_name = "scope"
description = "Creates a scoped value for the specified name, which must be a variable name.  If the name is an instance variable, then the scope command returns a string of the following form: @itcl object varName This is recognized in any context as an instanc..."
manpage_section = "n"
manpage_format = "troff"
operating_system_version = "15.3"
author = "None Specified"
operating_system = "macos"
detected_package_version = "1.7"
date = "Sun Feb 16 04:48:31 2025"
title = "scope(n)"
+++

scope n "" itcl "[incr\ Tcl]"

---
'\" Note:  do not modify the .SH NAME line immediately below!

## NAME

scope - capture the namespace context for a variable

## SYNOPSIS

**itcl::scope **name

---



## DESCRIPTION


Creates a scoped value for the specified *name*, which must
be a variable name.  If the *name* is an instance variable,
then the scope command returns a string of the following form:

```
@itcl *object varName*
```

This is recognized in any context as an instance variable belonging
to *object*.  So with itcl3.0 and beyond, it is possible to use
instance variables in conjunction with widgets.  For example, if you
have an object with a private variable \fCx, and you can use
\fCx in conjunction with the \fC-textvariable option of an
entry widget.  Before itcl3.0, only common variables could be used
in this manner.

If the *name* is not an instance variable, then it must be
a common variable or a global variable.  In that case, the scope
command returns the fully qualified name of the variable, e.g.,
\fC::foo::bar::x.

If the *name* is not recognized as a variable, the scope
command returns an error.

Ordinary variable names refer to variables in the global namespace.
A scoped value captures a variable name together with its namespace
context in a way that allows it to be referenced properly later.
It is needed, for example, to wrap up variable names when a Tk
widget is used within a namespace:

```
namespace foo \{
    private variable mode 1

    radiobutton .rb1 -text "Mode #1" \
        -variable [scope mode] -value 1
    pack .rb1

    radiobutton .rb2 -text "Mode #2" \
        -variable [scope mode] -value 2
    pack .rb2
\}
```

Radiobuttons \fC.rb1 and \fC.rb2 interact via the variable
"mode" contained in the namespace "foo".  The **scope** command
guarantees this by returning the fully qualified variable name
\fC::foo::mode.

You should never use the \fC@itcl syntax directly.  For example,
it is a bad idea to write code like this:

```
set \{@itcl ::fred x\} 3
puts "value = $\{@itcl ::fred x\}"
```

Instead, you should always use the scope command to generate the
variable name dynamically.  Then, you can pass that name to a widget
or to any other bit of code in your program.


## KEYWORDS

code, namespace, variable
