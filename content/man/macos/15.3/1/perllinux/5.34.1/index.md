+++
manpage_format = "troff"
manpage_name = "perllinux"
operating_system_version = "15.3"
date = "2022-02-19"
description = "This document describes various features of Linux that will affect how Perl version 5 (hereafter just Perl) is compiled and/or runs. Normally one can install /usr/bin/perl on Linux using your distributions package manager (e.g: f(CW*(C`sudo apt-ge..."
manpage_section = "1"
detected_package_version = "5.34.1"
author = "None Specified"
title = "perllinux(1)"
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLLINUX 1"
PERLLINUX 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perllinux - Perl version 5 on Linux systems

## DESCRIPTION

Header "DESCRIPTION"
This document describes various features of Linux that will affect how Perl
version 5 (hereafter just Perl) is compiled and/or runs.

### Deploying Perl on Linux

Subsection "Deploying Perl on Linux"
Normally one can install */usr/bin/perl* on Linux using your distribution's
package manager (e.g: \f(CW\*(C`sudo apt-get install perl\*(C', or
\f(CW\*(C`sudo dnf install perl\*(C'). Note that sometimes one needs to install some
extra system packages in order to be able to use \s-1CPAN\s0 frontends, and that
messing with the system's perl is not always recommended. One can use
perlbrew <https://perlbrew.pl/> to avoid such issues.

Otherwise, perl should build fine on Linux using the mainstream compilers
\s-1GCC\s0 and clang, while following the usual instructions.

### Experimental Support for Sun Studio Compilers for Linux \s-1OS\s0

Subsection "Experimental Support for Sun Studio Compilers for Linux OS"
Sun Microsystems has released a port of their Sun Studio compilers for
Linux.  As of May 2019, the last stable release took place on 2017, and one can
buy support contracts for them.

There are some special instructions for building Perl with Sun Studio on
Linux.  Following the normal \f(CW\*(C`Configure\*(C', you have to run make as follows:

.Vb 1
    LDLOADLIBS=-lc make
.Ve

\f(CW\*(C`LDLOADLIBS\*(C' is an environment variable used by the linker to link
\f(CW\*(C`/ext\*(C' modules to glibc.  Currently, that environment variable is not getting
populated by a combination of \f(CW\*(C`Config\*(C' entries and \f(CW\*(C`ExtUtil::MakeMaker\*(C'.
While there may be a bug somewhere in Perl's configuration or
\f(CW\*(C`ExtUtil::MakeMaker\*(C' causing the problem, the most likely cause is an
incomplete understanding of Sun Studio by this author.  Further investigation
is needed to get this working better.

## AUTHOR

Header "AUTHOR"
Steve Peters <steve@fisharerojo.org>

Please report any errors, updates, or suggestions to
<https://github.com/Perl/perl5/issues>.
