+++
detected_package_version = "5.34.1"
author = "None Specified"
keywords = ["header", "see", "also", "encode", "perlmod", "perlpod"]
manpage_name = "enc2xs"
operating_system_version = "15.3"
operating_system = "macos"
manpage_section = "1"
date = "2024-12-14"
manpage_format = "troff"
description = "enc2xs builds a Perl extension for use by Encode from either Unicode Character Mapping files (.ucm) or Tcl Encoding Files (.enc). Besides being used internally during the build process of the Encode module, you can use enc2xs to add your own encodi..."
title = "enc2xs(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "ENC2XS 1"
ENC2XS 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

enc2xs -- Perl Encode Module Generator

## SYNOPSIS

Header "SYNOPSIS"
.Vb 3
  enc2xs -[options]
  enc2xs -M ModName mapfiles...
  enc2xs -C
.Ve

## DESCRIPTION

Header "DESCRIPTION"
*enc2xs* builds a Perl extension for use by Encode from either
Unicode Character Mapping files (.ucm) or Tcl Encoding Files (.enc).
Besides being used internally during the build process of the Encode
module, you can use *enc2xs* to add your own encoding to perl.
No knowledge of \s-1XS\s0 is necessary.

## Quick Guide

Header "Quick Guide"
If you want to know as little about Perl as possible but need to
add a new encoding, just read this chapter and forget the rest.

- 0.
Item "0."
Have a .ucm file ready.  You can get it from somewhere or you can write
your own from scratch or you can grab one from the Encode distribution
and customize it.  For the \s-1UCM\s0 format, see the next Chapter.  In the
example below, I'll call my theoretical encoding myascii, defined
in *my.ucm*.  \f(CW\*(C`$\*(C' is a shell prompt.
.Sp
.Vb 2
  $ ls -F
  my.ucm
.Ve

- 1.
Item "1."
Issue a command as follows;
.Sp
.Vb 5
  $ enc2xs -M My my.ucm
  generating Makefile.PL
  generating My.pm
  generating README
  generating Changes
.Ve
.Sp
Now take a look at your current directory.  It should look like this.
.Sp
.Vb 2
  $ ls -F
  Makefile.PL   My.pm         my.ucm        t/
.Ve
.Sp
The following files were created.
.Sp
.Vb 3
  Makefile.PL - MakeMaker script
  My.pm       - Encode submodule
  t/My.t      - test file
.Ve

> 
- 1.1.
Item "1.1."
If you want *.ucm installed together with the modules, do as follows;
.Sp
.Vb 3
  $ mkdir Encode
  $ mv *.ucm Encode
  $ enc2xs -M My Encode/*ucm
.Ve



> 


- 2.
Item "2."
Edit the files generated.  You don't have to if you have no time \s-1AND\s0 no
intention to give it to someone else.  But it is a good idea to edit
the pod and to add more tests.

- 3.
Item "3."
Now issue a command all Perl Mongers love:
.Sp
.Vb 2
  $ perl Makefile.PL
  Writing Makefile for Encode::My
.Ve

- 4.
Item "4."
Now all you have to do is make.
.Sp
.Vb 12
  $ make
  cp My.pm blib/lib/Encode/My.pm
  /usr/local/bin/perl /usr/local/bin/enc2xs -Q -O \\
    -o encode_t.c -f encode_t.fnm
  Reading myascii (myascii)
  Writing compiled form
  128 bytes in string tables
  384 bytes (75%) saved spotting duplicates
  1 bytes (0.775%) saved using substrings
  ....
  chmod 644 blib/arch/auto/Encode/My/My.bs
  $
.Ve
.Sp
The time it takes varies depending on how fast your machine is and
how large your encoding is.  Unless you are working on something big
like euc-tw, it won't take too long.

- 5.
Item "5."
You can \*(L"make install\*(R" already but you should test first.
.Sp
.Vb 8
  $ make test
  PERL_DL_NONLAZY=1 /usr/local/bin/perl -Iblib/arch -Iblib/lib \\
    -e \*(Aquse Test::Harness  qw(&runtests $verbose); \\
    $verbose=0; runtests @ARGV;\*(Aq t/*.t
  t/My....ok
  All tests successful.
  Files=1, Tests=2,  0 wallclock secs
   ( 0.09 cusr + 0.01 csys = 0.09 CPU)
.Ve

- 6.
Item "6."
If you are content with the test result, just \*(L"make install\*(R"

- 7.
Item "7."
If you want to add your encoding to Encode's demand-loading list
(so you don't have to \*(L"use Encode::YourEncoding\*(R"), run
.Sp
.Vb 1
  enc2xs -C
.Ve
.Sp
to update Encode::ConfigLocal, a module that controls local settings.
After that, \*(L"use Encode;\*(R" is enough to load your encodings on demand.

## The Unicode Character Map

Header "The Unicode Character Map"
Encode uses the Unicode Character Map (\s-1UCM\s0) format for source character
mappings.  This format is used by \s-1IBM\s0's \s-1ICU\s0 package and was adopted
by Nick Ing-Simmons for use with the Encode module.  Since \s-1UCM\s0 is
more flexible than Tcl's Encoding Map and far more user-friendly,
this is the recommended format for Encode now.

A \s-1UCM\s0 file looks like this.

.Vb 10
  #
  # Comments
  #
  <code_set_name> "US-ascii" # Required
  <code_set_alias> "ascii"   # Optional
  <mb_cur_min> 1             # Required; usually 1
  <mb_cur_max> 1             # Max. # of bytes/char
  <subchar> \\x3F             # Substitution char
  #
  CHARMAP
  <U0000> \\x00 |0 # <control>
  <U0001> \\x01 |0 # <control>
  <U0002> \\x02 |0 # <control>
  ....
  <U007C> \\x7C |0 # VERTICAL LINE
  <U007D> \\x7D |0 # RIGHT CURLY BRACKET
  <U007E> \\x7E |0 # TILDE
  <U007F> \\x7F |0 # <control>
  END CHARMAP
.Ve

- \(bu
Anything that follows \f(CW\*(C`#\*(C' is treated as a comment.

- \(bu
The header section continues until a line containing the word
\s-1CHARMAP.\s0 This section has a form of *<keyword> value*, one
pair per line.  Strings used as values must be quoted. Barewords are
treated as numbers.  *\\xXX* represents a byte.
.Sp
Most of the keywords are self-explanatory. *subchar* means
substitution character, not subcharacter.  When you decode a Unicode
sequence to this encoding but no matching character is found, the byte
sequence defined here will be used.  For most cases, the value here is
\\x3F; in \s-1ASCII,\s0 this is a question mark.

- \(bu
\s-1CHARMAP\s0 starts the character map section.  Each line has a form as
follows:
.Sp
.Vb 5
  <UXXXX> \\xXX.. |0 # comment
    ^     ^      ^
    |     |      +- Fallback flag
    |     +-------- Encoded byte sequence
    +-------------- Unicode Character ID in hex
.Ve
.Sp
The format is roughly the same as a header section except for the
fallback flag: | followed by 0..3.   The meaning of the possible
values is as follows:

> 
- |0
Item "|0"
Round trip safe.  A character decoded to Unicode encodes back to the
same byte sequence.  Most characters have this flag.

- |1
Item "|1"
Fallback for unicode -> encoding.  When seen, enc2xs adds this
character for the encode map only.

- |2
Item "|2"
Skip sub-char mapping should there be no code point.

- |3
Item "|3"
Fallback for encoding -> unicode.  When seen, enc2xs adds this
character for the decode map only.



> 


- \(bu
And finally, \s-1END OF CHARMAP\s0 ends the section.

When you are manually creating a \s-1UCM\s0 file, you should copy ascii.ucm
or an existing encoding which is close to yours, rather than write
your own from scratch.

When you do so, make sure you leave at least **U0000** to **U0020** as
is, unless your environment is \s-1EBCDIC.\s0

**\s-1CAVEAT\s0**: not all features in \s-1UCM\s0 are implemented.  For example,
icu:state is not used.  Because of that, you need to write a perl
module if you want to support algorithmical encodings, notably
the \s-1ISO-2022\s0 series.  Such modules include Encode::JP::2022_JP,
Encode::KR::2022_KR, and Encode::TW::HZ.

### Coping with duplicate mappings

Subsection "Coping with duplicate mappings"
When you create a map, you \s-1SHOULD\s0 make your mappings round-trip safe.
That is, \f(CW\*(C`encode(\*(Aqyour-encoding\*(Aq, decode(\*(Aqyour-encoding\*(Aq, $data)) eq
$data\*(C' stands for all characters that are marked as \f(CW\*(C`|0\*(C'.  Here is
how to make sure:

- \(bu
Sort your map in Unicode order.

- \(bu
When you have a duplicate entry, mark either one with '|1' or '|3'.

- \(bu
And make sure the '|1' or '|3' entry \s-1FOLLOWS\s0 the '|0' entry.

Here is an example from big5-eten.

.Vb 2
  <U2550> \\xF9\\xF9 |0
  <U2550> \\xA2\\xA4 |3
.Ve

Internally Encoding -> Unicode and Unicode -> Encoding Map looks like
this;

.Vb 4
  E to U               U to E
  --------------------------------------
  \\xF9\\xF9 => U2550    U2550 => \\xF9\\xF9
  \\xA2\\xA4 => U2550
.Ve

So it is round-trip safe for \\xF9\\xF9.  But if the line above is upside
down, here is what happens.

.Vb 4
  E to U               U to E
  --------------------------------------
  \\xA2\\xA4 => U2550    U2550 => \\xF9\\xF9
  (\\xF9\\xF9 => U2550 is now overwritten!)
.Ve

The Encode package comes with *ucmlint*, a crude but sufficient
utility to check the integrity of a \s-1UCM\s0 file.  Check under the
Encode/bin directory for this.

When in doubt, you can use *ucmsort*, yet another utility under
Encode/bin directory.

## Bookmarks

Header "Bookmarks"

- \(bu
\s-1ICU\s0 Home Page
<http://www.icu-project.org/>

- \(bu
\s-1ICU\s0 Character Mapping Tables
<http://site.icu-project.org/charts/charset>

- \(bu
ICU:Conversion Data
<http://www.icu-project.org/userguide/conversion-data.html>

## SEE ALSO

Header "SEE ALSO"
Encode,
perlmod,
perlpod
