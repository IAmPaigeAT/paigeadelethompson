+++
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
description = "This document describes differences between the 5.18.1 release and the 5.18.2 release. If you are upgrading from an earlier release such as 5.18.0, first read perl5181delta, which describes differences between 5.18.0 and 5.18.1. B has been upgrade..."
operating_system = "macos"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_name = "perl5182delta"
date = "2022-02-19"
author = "None Specified"
title = "perl5182delta(1)"
detected_package_version = "5.34.1"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5182DELTA 1"
PERL5182DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5182delta - what is new for perl v5.18.2

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.18.1 release and the 5.18.2
release.

If you are upgrading from an earlier release such as 5.18.0, first read
perl5181delta, which describes differences between 5.18.0 and 5.18.1.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
B has been upgraded from version 1.42_01 to 1.42_02.
.Sp
The fix for [perl #118525] introduced a regression in the behaviour of
\f(CW\*(C`B::CV::GV\*(C', changing the return value from a \f(CW\*(C`B::SPECIAL\*(C' object on
a \f(CW\*(C`NULL\*(C' \f(CW\*(C`CvGV\*(C' to \f(CW\*(C`undef\*(C'.  \f(CW\*(C`B::CV::GV\*(C' again returns a
\f(CW\*(C`B::SPECIAL\*(C' object in this case.  [perl #119413]

- \(bu
B::Concise has been upgraded from version 0.95 to 0.95_01.
.Sp
This fixes a bug in dumping unexpected SPECIALs.

- \(bu
English has been upgraded from version 1.06 to 1.06_01.  This fixes an
error about the performance of \f(CW\*(C`$\`\*(C', \f(CW$&, and \f(CW\*(C`$\*(Aq\*(C'.

- \(bu
File::Glob has been upgraded from version 1.20 to 1.20_01.

## Documentation

Header "Documentation"

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"

- \(bu
perlrepository has been restored with a pointer to more useful pages.

- \(bu
perlhack has been updated with the latest changes from blead.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Perl 5.18.1 introduced a regression along with a bugfix for lexical subs.
Some B::SPECIAL results from B::CV::GV became undefs instead.  This broke
Devel::Cover among other libraries.  This has been fixed.  [perl #119351]

- \(bu
Perl 5.18.0 introduced a regression whereby \f(CW\*(C`[:^ascii:]\*(C', if used in the same
character class as other qualifiers, would fail to match characters in the
Latin-1 block.  This has been fixed.  [perl #120799]

- \(bu
Perl 5.18.0 introduced a regression when using ->SUPER::method with \s-1AUTOLOAD\s0
by looking up \s-1AUTOLOAD\s0 from the current package, rather than the current
packageXs superclass.  This has been fixed. [perl #120694]

- \(bu
Perl 5.18.0 introduced a regression whereby \f(CW\*(C`-bareword\*(C' was no longer
permitted under the \f(CW\*(C`strict\*(C' and \f(CW\*(C`integer\*(C' pragmata when used together.  This
has been fixed.  [perl #120288]

- \(bu
Previously PerlIOBase_dup didn't check if pushing the new layer succeeded
before (optionally) setting the utf8 flag. This could cause
segfaults-by-nullpointer.  This has been fixed.

- \(bu
A buffer overflow with very long identifiers has been fixed.

- \(bu
A regression from 5.16 in the handling of padranges led to assertion failures
if a keyword plugin declined to handle the second XmyX, but only after creating
a padop.
.Sp
This affected, at least, Devel::CallParser under threaded builds.
.Sp
This has been fixed.

- \(bu
The construct \f(CW\*(C`$r=qr/.../; /$r/p\*(C' is now handled properly, an issue which
had been worsened by changes 5.18.0. [perl #118213]

## Acknowledgements

Header "Acknowledgements"
Perl 5.18.2 represents approximately 3 months of development since Perl
5.18.1 and contains approximately 980 lines of changes across 39 files from 4
authors.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers. The following people are known to have
contributed the improvements that became Perl 5.18.2:

Craig A. Berry, David Mitchell, Ricardo Signes, Tony Cook.

The list above is almost certainly incomplete as it is automatically
generated from version control history. In particular, it does not include
the names of the (very much appreciated) contributors who reported issues to
the Perl bug tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core. We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the perl bug database at
http://rt.perl.org/perlbug/ .  There may also be information at
http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send it
to perl5-security-report@perl.org.  This points to a closed subscription
unarchived mailing list, which includes all the core committers, who will be
able to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported.  Please only use this address for
security issues in the Perl core, not for modules independently distributed on
\s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
