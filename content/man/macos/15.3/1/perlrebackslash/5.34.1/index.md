+++
operating_system = "macos"
title = "perlrebackslash(1)"
detected_package_version = "5.34.1"
manpage_name = "perlrebackslash"
description = "The top level documentation about Perl regular expressions is found in perlre. This document describes all backslash and escape sequences. After explaining the role of the backslash, it lists all the sequences that have a special meaning in Perl r..."
author = "None Specified"
date = "2022-02-19"
operating_system_version = "15.3"
manpage_section = "1"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLREBACKSLASH 1"
PERLREBACKSLASH 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlrebackslash - Perl Regular Expression Backslash Sequences and Escapes

## DESCRIPTION

Header "DESCRIPTION"
The top level documentation about Perl regular expressions
is found in perlre.

This document describes all backslash and escape sequences. After
explaining the role of the backslash, it lists all the sequences that have
a special meaning in Perl regular expressions (in alphabetical order),
then describes each of them.

Most sequences are described in detail in different documents; the primary
purpose of this document is to have a quick reference guide describing all
backslash and escape sequences.

### The backslash

Subsection "The backslash"
In a regular expression, the backslash can perform one of two tasks:
it either takes away the special meaning of the character following it
(for instance, \f(CW\*(C`\\|\*(C' matches a vertical bar, it's not an alternation),
or it is the start of a backslash or escape sequence.

The rules determining what it is are quite simple: if the character
following the backslash is an \s-1ASCII\s0 punctuation (non-word) character (that is,
anything that is not a letter, digit, or underscore), then the backslash just
takes away any special meaning of the character following it.

If the character following the backslash is an \s-1ASCII\s0 letter or an \s-1ASCII\s0 digit,
then the sequence may be special; if so, it's listed below. A few letters have
not been used yet, so escaping them with a backslash doesn't change them to be
special.  A future version of Perl may assign a special meaning to them, so if
you have warnings turned on, Perl issues a warning if you use such a
sequence.  [1].

It is however guaranteed that backslash or escape sequences never have a
punctuation character following the backslash, not now, and not in a future
version of Perl 5. So it is safe to put a backslash in front of a non-word
character.

Note that the backslash itself is special; if you want to match a backslash,
you have to escape the backslash with a backslash: \f(CW\*(C`/\\\\/\*(C' matches a single
backslash.

- [1]
Item "[1]"
There is one exception. If you use an alphanumeric character as the
delimiter of your pattern (which you probably shouldn't do for readability
reasons), you have to escape the delimiter if you want to match
it. Perl won't warn then. See also \*(L"Gory details of parsing
quoted constructs\*(R" in perlop.

### All the sequences and escapes

Subsection "All the sequences and escapes"
Those not usable within a bracketed character class (like \f(CW\*(C`[\\da-z]\*(C') are marked
as \f(CW\*(C`Not in [].\*(C'

.Vb 10
 \\000              Octal escape sequence.  See also \\o\{\}.
 \\1                Absolute backreference.  Not in [].
 \\a                Alarm or bell.
 \\A                Beginning of string.  Not in [].
 \\b\{\}, \\b          Boundary. (\\b is a backspace in []).
 \\B\{\}, \\B          Not a boundary.  Not in [].
 \\cX               Control-X.
 \\d                Match any digit character.
 \\D                Match any character that isn\*(Aqt a digit.
 \\e                Escape character.
 \\E                Turn off \\Q, \\L and \\U processing.  Not in [].
 \\f                Form feed.
 \\F                Foldcase till \\E.  Not in [].
 \\g\{\}, \\g1         Named, absolute or relative backreference.
                   Not in [].
 \\G                Pos assertion.  Not in [].
 \\h                Match any horizontal whitespace character.
 \\H                Match any character that isn\*(Aqt horizontal whitespace.
 \\k\{\}, \\k<>, \\k\*(Aq\*(Aq  Named backreference.  Not in [].
 \\K                Keep the stuff left of \\K.  Not in [].
 \\l                Lowercase next character.  Not in [].
 \\L                Lowercase till \\E.  Not in [].
 \\n                (Logical) newline character.
 \\N                Match any character but newline.  Not in [].
 \\N\{\}              Named or numbered (Unicode) character or sequence.
 \\o\{\}              Octal escape sequence.
 \\p\{\}, \\pP         Match any character with the given Unicode property.
 \\P\{\}, \\PP         Match any character without the given property.
 \\Q                Quote (disable) pattern metacharacters till \\E.  Not
                   in [].
 \\r                Return character.
 \\R                Generic new line.  Not in [].
 \\s                Match any whitespace character.
 \\S                Match any character that isn\*(Aqt a whitespace.
 \\t                Tab character.
 \\u                Titlecase next character.  Not in [].
 \\U                Uppercase till \\E.  Not in [].
 \\v                Match any vertical whitespace character.
 \\V                Match any character that isn\*(Aqt vertical whitespace
 \\w                Match any word character.
 \\W                Match any character that isn\*(Aqt a word character.
 \\x\{\}, \\x00        Hexadecimal escape sequence.
 \\X                Unicode "extended grapheme cluster".  Not in [].
 \\z                End of string.  Not in [].
 \\Z                End of string.  Not in [].
.Ve

### Character Escapes

Subsection "Character Escapes"
*Fixed characters*
Subsection "Fixed characters"

A handful of characters have a dedicated *character escape*. The following
table shows them, along with their \s-1ASCII\s0 code points (in decimal and hex),
their \s-1ASCII\s0 name, the control escape on \s-1ASCII\s0 platforms and a short
description.  (For \s-1EBCDIC\s0 platforms, see \*(L"\s-1OPERATOR DIFFERENCES\*(R"\s0 in perlebcdic.)

.Vb 9
 Seq.  Code Point  ASCII   Cntrl   Description.
       Dec    Hex
  \\a     7     07    BEL    \\cG    alarm or bell
  \\b     8     08     BS    \\cH    backspace [1]
  \\e    27     1B    ESC    \\c[    escape character
  \\f    12     0C     FF    \\cL    form feed
  \\n    10     0A     LF    \\cJ    line feed [2]
  \\r    13     0D     CR    \\cM    carriage return
  \\t     9     09    TAB    \\cI    tab
.Ve

- [1]
Item "[1]"
\f(CW\*(C`\\b\*(C' is the backspace character only inside a character class. Outside a
character class, \f(CW\*(C`\\b\*(C' alone is a word-character/non-word-character
boundary, and \f(CW\*(C`\\b\{\}\*(C' is some other type of boundary.

- [2]
Item "[2]"
\f(CW\*(C`\\n\*(C' matches a logical newline. Perl converts between \f(CW\*(C`\\n\*(C' and your
\s-1OS\s0's native newline character when reading from or writing to text files.

Example
Subsection "Example"

.Vb 1
 $str =~ /\\t/;   # Matches if $str contains a (horizontal) tab.
.Ve

*Control characters*
Subsection "Control characters"

\f(CW\*(C`\\c\*(C' is used to denote a control character; the character following \f(CW\*(C`\\c\*(C'
determines the value of the construct.  For example the value of \f(CW\*(C`\\cA\*(C' is
\f(CWchr(1), and the value of \f(CW\*(C`\\cb\*(C' is \f(CWchr(2), etc.
The gory details are in \*(L"Regexp Quote-Like Operators\*(R" in perlop.  A complete
list of what \f(CWchr(1), etc. means for \s-1ASCII\s0 and \s-1EBCDIC\s0 platforms is in
\*(L"\s-1OPERATOR DIFFERENCES\*(R"\s0 in perlebcdic.

Note that \f(CW\*(C`\\c\\\*(C' alone at the end of a regular expression (or doubled-quoted
string) is not valid.  The backslash must be followed by another character.
That is, \f(CW\*(C`\\c\\\f(CIX\f(CW\*(C' means \f(CW\*(C`chr(28) . \*(Aq\f(CIX\f(CW\*(Aq\*(C' for all characters *X*.

To write platform-independent code, you must use \f(CW\*(C`\\N\{\f(CINAME\f(CW\}\*(C' instead, like
\f(CW\*(C`\\N\{ESCAPE\}\*(C' or \f(CW\*(C`\\N\{U+001B\}\*(C', see charnames.

Mnemonic: *c*ontrol character.

Example
Subsection "Example"

.Vb 1
 $str =~ /\\cK/;  # Matches if $str contains a vertical tab (control-K).
.Ve

*Named or numbered characters and character sequences*
Subsection "Named or numbered characters and character sequences"

Unicode characters have a Unicode name and numeric code point (ordinal)
value.  Use the
\f(CW\*(C`\\N\{\}\*(C' construct to specify a character by either of these values.
Certain sequences of characters also have names.

To specify by name, the name of the character or character sequence goes
between the curly braces.

To specify a character by Unicode code point, use the form \f(CW\*(C`\\N\{U+\f(CIcode
point\f(CW\}\*(C', where *code point* is a number in hexadecimal that gives the
code point that Unicode has assigned to the desired character.  It is
customary but not required to use leading zeros to pad the number to 4
digits.  Thus \f(CW\*(C`\\N\{U+0041\}\*(C' means \f(CW\*(C`LATIN CAPITAL LETTER A\*(C', and you will
rarely see it written without the two leading zeros.  \f(CW\*(C`\\N\{U+0041\}\*(C' means
\*(L"A\*(R" even on \s-1EBCDIC\s0 machines (where the ordinal value of \*(L"A\*(R" is not 0x41).

Blanks may freely be inserted adjacent to but within the braces
enclosing the name or code point.  So \f(CW\*(C`\\N\{\ U+0041\ \}\*(C' is perfectly
legal.

It is even possible to give your own names to characters and character
sequences by using the charnames module.  These custom names are
lexically scoped, and so a given code point may have different names
in different scopes.  The name used is what is in effect at the time the
\f(CW\*(C`\\N\{\}\*(C' is expanded.  For patterns in double-quotish context, that means
at the time the pattern is parsed.  But for patterns that are delimitted
by single quotes, the expansion is deferred until pattern compilation
time, which may very well have a different \f(CW\*(C`charnames\*(C' translator in
effect.

(There is an expanded internal form that you may see in debug output:
\f(CW\*(C`\\N\{U+\f(CIcode point\f(CW.\f(CIcode point\f(CW...\}\*(C'.
The \f(CW\*(C`...\*(C' means any number of these *code point*s separated by dots.
This represents the sequence formed by the characters.  This is an internal
form only, subject to change, and you should not try to use it yourself.)

Mnemonic: *N*amed character.

Note that a character or character sequence expressed as a named
or numbered character is considered a character without special
meaning by the regex engine, and will match \*(L"as is\*(R".

Example
Subsection "Example"

.Vb 1
 $str =~ /\\N\{THAI CHARACTER SO SO\}/;  # Matches the Thai SO SO character

 use charnames \*(AqCyrillic\*(Aq;            # Loads Cyrillic names.
 $str =~ /\\N\{ZHE\}\\N\{KA\}/;             # Match "ZHE" followed by "KA".
.Ve

*Octal escapes*
Subsection "Octal escapes"

There are two forms of octal escapes.  Each is used to specify a character by
its code point specified in base 8.

One form, available starting in Perl 5.14 looks like \f(CW\*(C`\\o\{...\}\*(C', where the dots
represent one or more octal digits.  It can be used for any Unicode character.

It was introduced to avoid the potential problems with the other form,
available in all Perls.  That form consists of a backslash followed by three
octal digits.  One problem with this form is that it can look exactly like an
old-style backreference (see
\*(L"Disambiguation rules between old-style octal escapes and backreferences\*(R"
below.)  You can avoid this by making the first of the three digits always a
zero, but that makes \\077 the largest code point specifiable.

In some contexts, a backslash followed by two or even one octal digits may be
interpreted as an octal escape, sometimes with a warning, and because of some
bugs, sometimes with surprising results.  Also, if you are creating a regex
out of smaller snippets concatenated together, and you use fewer than three
digits, the beginning of one snippet may be interpreted as adding digits to the
ending of the snippet before it.  See \*(L"Absolute referencing\*(R" for more
discussion and examples of the snippet problem.

Note that a character expressed as an octal escape is considered
a character without special meaning by the regex engine, and will match
\*(L"as is\*(R".

To summarize, the \f(CW\*(C`\\o\{\}\*(C' form is always safe to use, and the other form is
safe to use for code points through \\077 when you use exactly three digits to
specify them.

Mnemonic: *0*ctal or *o*ctal.

Examples (assuming an \s-1ASCII\s0 platform)
Subsection "Examples (assuming an ASCII platform)"

.Vb 12
 $str = "Perl";
 $str =~ /\\o\{120\}/;  # Match, "\\120" is "P".
 $str =~ /\\120/;     # Same.
 $str =~ /\\o\{120\}+/; # Match, "\\120" is "P",
                     # it\*(Aqs repeated at least once.
 $str =~ /\\120+/;    # Same.
 $str =~ /P\\053/;    # No match, "\\053" is "+" and taken literally.
 /\\o\{23073\}/         # Black foreground, white background smiling face.
 /\\o\{4801234567\}/    # Raises a warning, and yields chr(4).
 /\\o\{ 400\}/          # LATIN CAPITAL LETTER A WITH MACRON
 /\\o\{ 400 \}/         # Same. These show blanks are allowed adjacent to
                     # the braces
.Ve

Disambiguation rules between old-style octal escapes and backreferences
Subsection "Disambiguation rules between old-style octal escapes and backreferences"

Octal escapes of the \f(CW\*(C`\\000\*(C' form outside of bracketed character classes
potentially clash with old-style backreferences (see \*(L"Absolute referencing\*(R"
below).  They both consist of a backslash followed by numbers.  So Perl has to
use heuristics to determine whether it is a backreference or an octal escape.
Perl uses the following rules to disambiguate:

- 1.
If the backslash is followed by a single digit, it's a backreference.

- 2.
If the first digit following the backslash is a 0, it's an octal escape.

- 3.
If the number following the backslash is N (in decimal), and Perl already
has seen N capture groups, Perl considers this a backreference.  Otherwise,
it considers it an octal escape. If N has more than three digits, Perl
takes only the first three for the octal escape; the rest are matched as is.
.Sp
.Vb 6
 my $pat  = "(" x 999;
    $pat .= "a";
    $pat .= ")" x 999;
 /^($pat)\\1000$/;   #  Matches \*(Aqaa\*(Aq; there are 1000 capture groups.
 /^$pat\\1000$/;     #  Matches \*(Aqa@0\*(Aq; there are 999 capture groups
                    #  and \\1000 is seen as \\100 (a \*(Aq@\*(Aq) and a \*(Aq0\*(Aq.
.Ve

You can force a backreference interpretation always by using the \f(CW\*(C`\\g\{...\}\*(C'
form.  You can the force an octal interpretation always by using the \f(CW\*(C`\\o\{...\}\*(C'
form, or for numbers up through \\077 (= 63 decimal), by using three digits,
beginning with a \*(L"0\*(R".

*Hexadecimal escapes*
Subsection "Hexadecimal escapes"

Like octal escapes, there are two forms of hexadecimal escapes, but both start
with the sequence \f(CW\*(C`\\x\*(C'.  This is followed by either exactly two hexadecimal
digits forming a number, or a hexadecimal number of arbitrary length surrounded
by curly braces. The hexadecimal number is the code point of the character you
want to express.

Note that a character expressed as one of these escapes is considered a
character without special meaning by the regex engine, and will match
\*(L"as is\*(R".

Mnemonic: he*x*adecimal.

Examples (assuming an \s-1ASCII\s0 platform)
Subsection "Examples (assuming an ASCII platform)"

.Vb 4
 $str = "Perl";
 $str =~ /\\x50/;    # Match, "\\x50" is "P".
 $str =~ /\\x50+/;   # Match, "\\x50" is "P", it is repeated at least once
 $str =~ /P\\x2B/;   # No match, "\\x2B" is "+" and taken literally.

 /\\x\{2603\}\\x\{2602\}/ # Snowman with an umbrella.
                    # The Unicode character 2603 is a snowman,
                    # the Unicode character 2602 is an umbrella.
 /\\x\{263B\}/         # Black smiling face.
 /\\x\{263b\}/         # Same, the hex digits A - F are case insensitive.
 /\\x\{ 263b \}/       # Same, showing optional blanks adjacent to the
                    # braces
.Ve

### Modifiers

Subsection "Modifiers"
A number of backslash sequences have to do with changing the character,
or characters following them. \f(CW\*(C`\\l\*(C' will lowercase the character following
it, while \f(CW\*(C`\\u\*(C' will uppercase (or, more accurately, titlecase) the
character following it. They provide functionality similar to the
functions \f(CW\*(C`lcfirst\*(C' and \f(CW\*(C`ucfirst\*(C'.

To uppercase or lowercase several characters, one might want to use
\f(CW\*(C`\\L\*(C' or \f(CW\*(C`\\U\*(C', which will lowercase/uppercase all characters following
them, until either the end of the pattern or the next occurrence of
\f(CW\*(C`\\E\*(C', whichever comes first. They provide functionality similar to what
the functions \f(CW\*(C`lc\*(C' and \f(CW\*(C`uc\*(C' provide.

\f(CW\*(C`\\Q\*(C' is used to quote (disable) pattern metacharacters, up to the next
\f(CW\*(C`\\E\*(C' or the end of the pattern. \f(CW\*(C`\\Q\*(C' adds a backslash to any character
that could have special meaning to Perl.  In the \s-1ASCII\s0 range, it quotes
every character that isn't a letter, digit, or underscore.  See
\*(L"quotemeta\*(R" in perlfunc for details on what gets quoted for non-ASCII
code points.  Using this ensures that any character between \f(CW\*(C`\\Q\*(C' and
\f(CW\*(C`\\E\*(C' will be matched literally, not interpreted as a metacharacter by
the regex engine.

\f(CW\*(C`\\F\*(C' can be used to casefold all characters following, up to the next \f(CW\*(C`\\E\*(C'
or the end of the pattern. It provides the functionality similar to
the \f(CW\*(C`fc\*(C' function.

Mnemonic: *L*owercase, *U*ppercase, *F*old-case, *Q*uotemeta, *E*nd.

Examples
Subsection "Examples"

.Vb 7
 $sid     = "sid";
 $greg    = "GrEg";
 $miranda = "(Miranda)";
 $str     =~ /\\u$sid/;        # Matches \*(AqSid\*(Aq
 $str     =~ /\\L$greg/;       # Matches \*(Aqgreg\*(Aq
 $str     =~ /\\Q$miranda\\E/;  # Matches \*(Aq(Miranda)\*(Aq, as if the pattern
                              #   had been written as /\\(Miranda\\)/
.Ve

### Character classes

Subsection "Character classes"
Perl regular expressions have a large range of character classes. Some of
the character classes are written as a backslash sequence. We will briefly
discuss those here; full details of character classes can be found in
perlrecharclass.

\f(CW\*(C`\\w\*(C' is a character class that matches any single *word* character
(letters, digits, Unicode marks, and connector punctuation (like the
underscore)).  \f(CW\*(C`\\d\*(C' is a character class that matches any decimal
digit, while the character class \f(CW\*(C`\\s\*(C' matches any whitespace character.
New in perl 5.10.0 are the classes \f(CW\*(C`\\h\*(C' and \f(CW\*(C`\\v\*(C' which match horizontal
and vertical whitespace characters.

The exact set of characters matched by \f(CW\*(C`\\d\*(C', \f(CW\*(C`\\s\*(C', and \f(CW\*(C`\\w\*(C' varies
depending on various pragma and regular expression modifiers.  It is
possible to restrict the match to the \s-1ASCII\s0 range by using the \f(CW\*(C`/a\*(C'
regular expression modifier.  See perlrecharclass.

The uppercase variants (\f(CW\*(C`\\W\*(C', \f(CW\*(C`\\D\*(C', \f(CW\*(C`\\S\*(C', \f(CW\*(C`\\H\*(C', and \f(CW\*(C`\\V\*(C') are
character classes that match, respectively, any character that isn't a
word character, digit, whitespace, horizontal whitespace, or vertical
whitespace.

Mnemonics: *w*ord, *d*igit, *s*pace, *h*orizontal, *v*ertical.

*Unicode classes*
Subsection "Unicode classes"

\f(CW\*(C`\\pP\*(C' (where \f(CW\*(C`P\*(C' is a single letter) and \f(CW\*(C`\\p\{Property\}\*(C' are used to
match a character that matches the given Unicode property; properties
include things like \*(L"letter\*(R", or \*(L"thai character\*(R". Capitalizing the
sequence to \f(CW\*(C`\\PP\*(C' and \f(CW\*(C`\\P\{Property\}\*(C' make the sequence match a character
that doesn't match the given Unicode property. For more details, see
\*(L"Backslash sequences\*(R" in perlrecharclass and
\*(L"Unicode Character Properties\*(R" in perlunicode.

Mnemonic: *p*roperty.

### Referencing

Subsection "Referencing"
If capturing parenthesis are used in a regular expression, we can refer
to the part of the source string that was matched, and match exactly the
same thing. There are three ways of referring to such *backreference*:
absolutely, relatively, and by name.

*Absolute referencing*
Subsection "Absolute referencing"

Either \f(CW\*(C`\\g\f(CIN\f(CW\*(C' (starting in Perl 5.10.0), or \f(CW\*(C`\\\f(CIN\f(CW\*(C' (old-style) where *N*
is a positive (unsigned) decimal number of any length is an absolute reference
to a capturing group.

*N* refers to the Nth set of parentheses, so \f(CW\*(C`\\g\f(CIN\f(CW\*(C' refers to whatever has
been matched by that set of parentheses.  Thus \f(CW\*(C`\\g1\*(C' refers to the first
capture group in the regex.

The \f(CW\*(C`\\g\f(CIN\f(CW\*(C' form can be equivalently written as \f(CW\*(C`\\g\{\f(CIN\f(CW\}\*(C'
which avoids ambiguity when building a regex by concatenating shorter
strings.  Otherwise if you had a regex \f(CW\*(C`qr/$a$b/\*(C', and \f(CW$a contained
\f(CW"\\g1", and \f(CW$b contained \f(CW"37", you would get \f(CW\*(C`/\\g137/\*(C' which is
probably not what you intended.

In the \f(CW\*(C`\\\f(CIN\f(CW\*(C' form, *N* must not begin with a \*(L"0\*(R", and there must be at
least *N* capturing groups, or else *N* is considered an octal escape
(but something like \f(CW\*(C`\\18\*(C' is the same as \f(CW\*(C`\\0018\*(C'; that is, the octal escape
\f(CW"\\001" followed by a literal digit \f(CW"8").

Mnemonic: *g*roup.

Examples
Subsection "Examples"

.Vb 5
 /(\\w+) \\g1/;    # Finds a duplicated word, (e.g. "cat cat").
 /(\\w+) \\1/;     # Same thing; written old-style.
 /(\\w+) \\g\{1\}/;  # Same, using the safer braced notation
 /(\\w+) \\g\{ 1 \}/;# Same, showing optional blanks adjacent to the braces
 /(.)(.)\\g2\\g1/; # Match a four letter palindrome (e.g. "ABBA").
.Ve

*Relative referencing*
Subsection "Relative referencing"

\f(CW\*(C`\\g-\f(CIN\f(CW\*(C' (starting in Perl 5.10.0) is used for relative addressing.  (It can
be written as \f(CW\*(C`\\g\{-\f(CIN\f(CW\}\*(C'.)  It refers to the *N*th group before the
\f(CW\*(C`\\g\{-\f(CIN\f(CW\}\*(C'.

The big advantage of this form is that it makes it much easier to write
patterns with references that can be interpolated in larger patterns,
even if the larger pattern also contains capture groups.

Examples
Subsection "Examples"

.Vb 8
 /(A)        # Group 1
  (          # Group 2
    (B)      # Group 3
    \\g\{-1\}   # Refers to group 3 (B)
    \\g\{-3\}   # Refers to group 1 (A)
    \\g\{ -3 \} # Same, showing optional blanks adjacent to the braces
  )
 /x;         # Matches "ABBA".

 my $qr = qr /(.)(.)\\g\{-2\}\\g\{-1\}/;  # Matches \*(Aqabab\*(Aq, \*(Aqcdcd\*(Aq, etc.
 /$qr$qr/                           # Matches \*(Aqababcdcd\*(Aq.
.Ve

*Named referencing*
Subsection "Named referencing"

\f(CW\*(C`\\g\{\f(CIname\f(CW\}\*(C' (starting in Perl 5.10.0) can be used to back refer to a
named capture group, dispensing completely with having to think about capture
buffer positions.

To be compatible with .Net regular expressions, \f(CW\*(C`\\g\{name\}\*(C' may also be
written as \f(CW\*(C`\\k\{name\}\*(C', \f(CW\*(C`\\k<name>\*(C' or \f(CW\*(C`\\k\*(Aqname\*(Aq\*(C'.

To prevent any ambiguity, *name* must not start with a digit nor contain a
hyphen.

Examples
Subsection "Examples"

.Vb 10
 /(?<word>\\w+) \\g\{word\}/   # Finds duplicated word, (e.g. "cat cat")
 /(?<word>\\w+) \\k\{word\}/   # Same.
 /(?<word>\\w+) \\g\{ word \}/ # Same, showing optional blanks adjacent to
                           # the braces
 /(?<word>\\w+) \\k\{ word \}/ # Same.
 /(?<word>\\w+) \\k<word>/   # Same.  There are no braces, so no blanks
                           # are permitted
 /(?<letter1>.)(?<letter2>.)\\g\{letter2\}\\g\{letter1\}/
                           # Match a four letter palindrome (e.g.
                           # "ABBA")
.Ve

### Assertions

Subsection "Assertions"
Assertions are conditions that have to be true; they don't actually
match parts of the substring. There are six assertions that are written as
backslash sequences.

- \\A
Item "A"
\f(CW\*(C`\\A\*(C' only matches at the beginning of the string. If the \f(CW\*(C`/m\*(C' modifier
isn't used, then \f(CW\*(C`/\\A/\*(C' is equivalent to \f(CW\*(C`/^/\*(C'. However, if the \f(CW\*(C`/m\*(C'
modifier is used, then \f(CW\*(C`/^/\*(C' matches internal newlines, but the meaning
of \f(CW\*(C`/\\A/\*(C' isn't changed by the \f(CW\*(C`/m\*(C' modifier. \f(CW\*(C`\\A\*(C' matches at the beginning
of the string regardless whether the \f(CW\*(C`/m\*(C' modifier is used.

- \\z, \\Z
Item "z, Z"
\f(CW\*(C`\\z\*(C' and \f(CW\*(C`\\Z\*(C' match at the end of the string. If the \f(CW\*(C`/m\*(C' modifier isn't
used, then \f(CW\*(C`/\\Z/\*(C' is equivalent to \f(CW\*(C`/$/\*(C'; that is, it matches at the
end of the string, or one before the newline at the end of the string. If the
\f(CW\*(C`/m\*(C' modifier is used, then \f(CW\*(C`/$/\*(C' matches at internal newlines, but the
meaning of \f(CW\*(C`/\\Z/\*(C' isn't changed by the \f(CW\*(C`/m\*(C' modifier. \f(CW\*(C`\\Z\*(C' matches at
the end of the string (or just before a trailing newline) regardless whether
the \f(CW\*(C`/m\*(C' modifier is used.
.Sp
\f(CW\*(C`\\z\*(C' is just like \f(CW\*(C`\\Z\*(C', except that it does not match before a trailing
newline. \f(CW\*(C`\\z\*(C' matches at the end of the string only, regardless of the
modifiers used, and not just before a newline.  It is how to anchor the
match to the true end of the string under all conditions.

- \\G
Item "G"
\f(CW\*(C`\\G\*(C' is usually used only in combination with the \f(CW\*(C`/g\*(C' modifier. If the
\f(CW\*(C`/g\*(C' modifier is used and the match is done in scalar context, Perl
remembers where in the source string the last match ended, and the next time,
it will start the match from where it ended the previous time.
.Sp
\f(CW\*(C`\\G\*(C' matches the point where the previous match on that string ended,
or the beginning of that string if there was no previous match.
.Sp
Mnemonic: *G*lobal.

- \\b\{\}, \\b, \\B\{\}, \\B
Item "b\{\}, b, B\{\}, B"
\f(CW\*(C`\\b\{...\}\*(C', available starting in v5.22, matches a boundary (between two
characters, or before the first character of the string, or after the
final character of the string) based on the Unicode rules for the
boundary type specified inside the braces.  The boundary
types are given a few paragraphs below.  \f(CW\*(C`\\B\{...\}\*(C' matches at any place
between characters where \f(CW\*(C`\\b\{...\}\*(C' of the same type doesn't match.
.Sp
\f(CW\*(C`\\b\*(C' when not immediately followed by a \f(CW"\{" is available in all
Perls.  It matches at any place
between a word (something matched by \f(CW\*(C`\\w\*(C') and a non-word character
(\f(CW\*(C`\\W\*(C'); \f(CW\*(C`\\B\*(C' when not immediately followed by a \f(CW"\{" matches at any
place between characters where \f(CW\*(C`\\b\*(C' doesn't match.  To get better
word matching of natural language text, see \*(L"\\b\{wb\}\*(R" below.
.Sp
\f(CW\*(C`\\b\*(C'
and \f(CW\*(C`\\B\*(C' assume there's a non-word character before the beginning and after
the end of the source string; so \f(CW\*(C`\\b\*(C' will match at the beginning (or end)
of the source string if the source string begins (or ends) with a word
character. Otherwise, \f(CW\*(C`\\B\*(C' will match.
.Sp
Do not use something like \f(CW\*(C`\\b=head\\d\\b\*(C' and expect it to match the
beginning of a line.  It can't, because for there to be a boundary before
the non-word \*(L"=\*(R", there must be a word character immediately previous.
All plain \f(CW\*(C`\\b\*(C' and \f(CW\*(C`\\B\*(C' boundary determinations look for word
characters alone, not for
non-word characters nor for string ends.  It may help to understand how
\f(CW\*(C`\\b\*(C' and \f(CW\*(C`\\B\*(C' work by equating them as follows:
.Sp
.Vb 2
    \\b  really means    (?:(?<=\\w)(?!\\w)|(?<!\\w)(?=\\w))
    \\B  really means    (?:(?<=\\w)(?=\\w)|(?<!\\w)(?!\\w))
.Ve
.Sp
In contrast, \f(CW\*(C`\\b\{...\}\*(C' and \f(CW\*(C`\\B\{...\}\*(C' may or may not match at the
beginning and end of the line, depending on the boundary type.  These
implement the Unicode default boundaries, specified in
<https://www.unicode.org/reports/tr14/> and
<https://www.unicode.org/reports/tr29/>.
The boundary types are:

> .ie n .IP """\\b\{gcb\}"" or ""\\b\{g\}""" 4
.el .IP "\f(CW\\b\{gcb\} or \f(CW\\b\{g\}" 4
Item "b\{gcb\} or b\{g\}"
This matches a Unicode \*(L"Grapheme Cluster Boundary\*(R".  (Actually Perl
always uses the improved \*(L"extended\*(R" grapheme cluster").  These are
explained below under \f(CW"\\X".  In fact, \f(CW\*(C`\\X\*(C' is another way to get
the same functionality.  It is equivalent to \f(CW\*(C`/.+?\\b\{gcb\}/\*(C'.  Use
whichever is most convenient for your situation.
.ie n .IP """\\b\{lb\}""" 4
.el .IP "\f(CW\\b\{lb\}" 4
Item "b\{lb\}"
This matches according to the default Unicode Line Breaking Algorithm
(<https://www.unicode.org/reports/tr14/>), as customized in that
document
(Example 7 of revision 35 <https://www.unicode.org/reports/tr14/tr14-35.html#Example7>)
for better handling of numeric expressions.
.Sp
This is suitable for many purposes, but the Unicode::LineBreak module
is available on \s-1CPAN\s0 that provides many more features, including
customization.
.ie n .IP """\\b\{sb\}""" 4
.el .IP "\f(CW\\b\{sb\}" 4
Item "b\{sb\}"
This matches a Unicode \*(L"Sentence Boundary\*(R".  This is an aid to parsing
natural language sentences.  It gives good, but imperfect results.  For
example, it thinks that \*(L"Mr. Smith\*(R" is two sentences.  More details are
at <https://www.unicode.org/reports/tr29/>.  Note also that it thinks
that anything matching \*(L"\\R\*(R" (except form feed and vertical tab) is a
sentence boundary.  \f(CW\*(C`\\b\{sb\}\*(C' works with text designed for
word-processors which wrap lines
automatically for display, but hard-coded line boundaries are considered
to be essentially the ends of text blocks (paragraphs really), and hence
the ends of sentences.  \f(CW\*(C`\\b\{sb\}\*(C' doesn't do well with text containing
embedded newlines, like the source text of the document you are reading.
Such text needs to be preprocessed to get rid of the line separators
before looking for sentence boundaries.  Some people view this as a bug
in the Unicode standard, and this behavior is quite subject to change in
future Perl versions.
.ie n .IP """\\b\{wb\}""" 4
.el .IP "\f(CW\\b\{wb\}" 4
Item "b\{wb\}"
This matches a Unicode \*(L"Word Boundary\*(R", but tailored to Perl
expectations.  This gives better (though not
perfect) results for natural language processing than plain \f(CW\*(C`\\b\*(C'
(without braces) does.  For example, it understands that apostrophes can
be in the middle of words and that parentheses aren't (see the examples
below).  More details are at <https://www.unicode.org/reports/tr29/>.
.Sp
The current Unicode definition of a Word Boundary matches between every
white space character.  Perl tailors this, starting in version 5.24, to
generally not break up spans of white space, just as plain \f(CW\*(C`\\b\*(C' has
always functioned.  This allows \f(CW\*(C`\\b\{wb\}\*(C' to be a drop-in replacement for
\f(CW\*(C`\\b\*(C', but with generally better results for natural language
processing.  (The exception to this tailoring is when a span of white
space is immediately followed by something like U+0303, \s-1COMBINING TILDE.\s0
If the final space character in the span is a horizontal white space, it
is broken out so that it attaches instead to the combining character.
To be precise, if a span of white space that ends in a horizontal space
has the character immediately following it have any of the Word
Boundary property values \*(L"Extend\*(R", \*(L"Format\*(R" or \*(L"\s-1ZWJ\*(R",\s0 the boundary between the
final horizontal space character and the rest of the span matches
\f(CW\*(C`\\b\{wb\}\*(C'.  In all other cases the boundary between two white space
characters matches \f(CW\*(C`\\B\{wb\}\*(C'.)



> .Sp
It is important to realize when you use these Unicode boundaries,
that you are taking a risk that a future version of Perl which contains
a later version of the Unicode Standard will not work precisely the same
way as it did when your code was written.  These rules are not
considered stable and have been somewhat more subject to change than the
rest of the Standard.  Unicode reserves the right to change them at
will, and Perl reserves the right to update its implementation to
Unicode's new rules.  In the past, some changes have been because new
characters have been added to the Standard which have different
characteristics than all previous characters, so new rules are
formulated for handling them.  These should not cause any backward
compatibility issues.  But some changes have changed the treatment of
existing characters because the Unicode Technical Committee has decided
that the change is warranted for whatever reason.  This could be to fix
a bug, or because they think better results are obtained with the new
rule.
.Sp
It is also important to realize that these are default boundary
definitions, and that implementations may wish to tailor the results for
particular purposes and locales.  For example, some languages, such as
Japanese and Thai, require dictionary lookup to accurately determine
word boundaries.
.Sp
Mnemonic: *b*oundary.



Examples
Subsection "Examples"

.Vb 4
  "cat"   =~ /\\Acat/;     # Match.
  "cat"   =~ /cat\\Z/;     # Match.
  "cat\\n" =~ /cat\\Z/;     # Match.
  "cat\\n" =~ /cat\\z/;     # No match.

  "cat"   =~ /\\bcat\\b/;   # Matches.
  "cats"  =~ /\\bcat\\b/;   # No match.
  "cat"   =~ /\\bcat\\B/;   # No match.
  "cats"  =~ /\\bcat\\B/;   # Match.

  while ("cat dog" =~ /(\\w+)/g) \{
      print $1;           # Prints \*(Aqcatdog\*(Aq
  \}
  while ("cat dog" =~ /\\G(\\w+)/g) \{
      print $1;           # Prints \*(Aqcat\*(Aq
  \}

  my $s = "He said, \\"Is pi 3.14? (I\*(Aqm not sure).\\"";
  print join("|", $s =~ m/ ( .+? \\b     ) /xg), "\\n";
  print join("|", $s =~ m/ ( .+? \\b\{wb\} ) /xg), "\\n";
 prints
  He| |said|, "|Is| |pi| |3|.|14|? (|I|\*(Aq|m| |not| |sure
  He| |said|,| |"|Is| |pi| |3.14|?| |(|I\*(Aqm| |not| |sure|)|.|"
.Ve

### Misc

Subsection "Misc"
Here we document the backslash sequences that don't fall in one of the
categories above. These are:

- \\K
Item "K"
This appeared in perl 5.10.0. Anything matched left of \f(CW\*(C`\\K\*(C' is
not included in \f(CW$&, and will not be replaced if the pattern is
used in a substitution. This lets you write \f(CW\*(C`s/PAT1 \\K PAT2/REPL/x\*(C'
instead of \f(CW\*(C`s/(PAT1) PAT2/$\{1\}REPL/x\*(C' or \f(CW\*(C`s/(?<=PAT1) PAT2/REPL/x\*(C'.
.Sp
Mnemonic: *K*eep.

- \\N
Item "N"
This feature, available starting in v5.12,  matches any character
that is **not** a newline.  It is a short-hand for writing \f(CW\*(C`[^\\n]\*(C', and is
identical to the \f(CW\*(C`.\*(C' metasymbol, except under the \f(CW\*(C`/s\*(C' flag, which changes
the meaning of \f(CW\*(C`.\*(C', but not \f(CW\*(C`\\N\*(C'.
.Sp
Note that \f(CW\*(C`\\N\{...\}\*(C' can mean a
named or numbered character
.
.Sp
Mnemonic: Complement of *\\n*.

- \\R
Xref "\R"
Item "R"
\f(CW\*(C`\\R\*(C' matches a *generic newline*; that is, anything considered a
linebreak sequence by Unicode. This includes all characters matched by
\f(CW\*(C`\\v\*(C' (vertical whitespace), and the multi character sequence \f(CW"\\x0D\\x0A"
(carriage return followed by a line feed, sometimes called the network
newline; it's the end of line sequence used in Microsoft text files opened
in binary mode). \f(CW\*(C`\\R\*(C' is equivalent to \f(CW\*(C`(?>\\x0D\\x0A|\\v)\*(C'.  (The
reason it doesn't backtrack is that the sequence is considered
inseparable.  That means that
.Sp
.Vb 1
 "\\x0D\\x0A" =~ /^\\R\\x0A$/   # No match
.Ve
.Sp
fails, because the \f(CW\*(C`\\R\*(C' matches the entire string, and won't backtrack
to match just the \f(CW"\\x0D".)  Since
\f(CW\*(C`\\R\*(C' can match a sequence of more than one character, it cannot be put
inside a bracketed character class; \f(CW\*(C`/[\\R]/\*(C' is an error; use \f(CW\*(C`\\v\*(C'
instead.  \f(CW\*(C`\\R\*(C' was introduced in perl 5.10.0.
.Sp
Note that this does not respect any locale that might be in effect; it
matches according to the platform's native character set.
.Sp
Mnemonic: none really. \f(CW\*(C`\\R\*(C' was picked because \s-1PCRE\s0 already uses \f(CW\*(C`\\R\*(C',
and more importantly because Unicode recommends such a regular expression
metacharacter, and suggests \f(CW\*(C`\\R\*(C' as its notation.

- \\X
Xref "\X"
Item "X"
This matches a Unicode *extended grapheme cluster*.
.Sp
\f(CW\*(C`\\X\*(C' matches quite well what normal (non-Unicode-programmer) usage
would consider a single character.  As an example, consider a G with some sort
of diacritic mark, such as an arrow.  There is no such single character in
Unicode, but one can be composed by using a G followed by a Unicode \*(L"\s-1COMBINING
UPWARDS ARROW BELOW\*(R",\s0 and would be displayed by Unicode-aware software as if it
were a single character.
.Sp
The match is greedy and non-backtracking, so that the cluster is never
broken up into smaller components.
.Sp
See also \f(CW\*(C`\\b\{gcb\}\*(C'.
.Sp
Mnemonic: e*X*tended Unicode character.

Examples
Subsection "Examples"

.Vb 2
 $str =~ s/foo\\Kbar/baz/g; # Change any \*(Aqbar\*(Aq following a \*(Aqfoo\*(Aq to \*(Aqbaz\*(Aq
 $str =~ s/(.)\\K\\g1//g;    # Delete duplicated characters.

 "\\n"   =~ /^\\R$/;         # Match, \\n   is a generic newline.
 "\\r"   =~ /^\\R$/;         # Match, \\r   is a generic newline.
 "\\r\\n" =~ /^\\R$/;         # Match, \\r\\n is a generic newline.

 "P\\x\{307\}" =~ /^\\X$/     # \\X matches a P with a dot above.
.Ve
