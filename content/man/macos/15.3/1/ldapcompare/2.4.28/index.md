+++
description = "is a shell-accessible interface to the library call. opens a connection to an LDAP server, binds, and performs a compare using specified parameters.   The DN should be a distinguished name in the directory.  Attr should be a known attribute.  If ..."
detected_package_version = "2.4.28"
date = "2011-01-01"
keywords = ["ldap", "conf", "5", "ldif", "3", "ldap_compare_ext", "author", "the", "openldap", "project", "http", "www", "org", "acknowledgements", "shared", "acknowledgement", "text", "software", "is", "developed", "and", "maintained", "by", "derived", "from", "university", "of", "michigan", "release"]
operating_system = "macos"
author = "The OpenLDAP Foundation All Rights Reserved."
manpage_section = "1"
manpage_format = "troff"
manpage_name = "ldapcompare"
title = "ldapcompare(1)"
operating_system_version = "15.3"
+++

LDAPCOMPARE 1 "2011/11/24" "OpenLDAP 2.4.28"

## NAME

ldapcompare - LDAP compare tool

## SYNOPSIS

ldapcompare
[\c
-n ]
[\c
-v ]
[\c
-z ]
[\c
-M [ M ]]
[\c
-d \ debuglevel]
[\c
-D \ binddn]
[\c
-W ]
[\c
-w \ passwd]
[\c
-y \ passwdfile]
[\c
-H \ ldapuri]
[\c
-h \ ldaphost]
[\c
-p \ ldapport]
[\c
-P \ \{ 2 \||\| 3 \}]
[\c
-e \ [ ! ] *ext* [ =*extparam* ]]
[\c
-E \ [ ! ] *ext* [ =*extparam* ]]
[\c
-O \ security-properties]
[\c
-I ]
[\c
-Q ]
[\c
-U \ authcid]
[\c
-R \ realm]
[\c
-x ]
[\c
-X \ authzid]
[\c
-Y \ mech]
[\c
-Z [ Z ]]
DN
\{\c
attr: value
|
attr:: b64value\}

## DESCRIPTION

ldapcompare
is a shell-accessible interface to the
ldap_compare_ext (3)
library call.

ldapcompare
opens a connection to an LDAP server, binds, and performs a compare
using specified parameters.   The *DN* should be a distinguished
name in the directory.  *Attr* should be a known attribute.  If
followed by one colon, the assertion *value* should be provided
as a string.  If followed by two colons, the base64 encoding of the
value is provided.  The result code of the compare is provided as
the exit code and, unless ran with **-z**, the program prints
TRUE, FALSE, or UNDEFINED on standard output.


## OPTIONS


-n
Show what would be done, but don't actually perform the compare.  Useful for
debugging in conjunction with **-v**.

-v
Run in verbose mode, with many diagnostics written to standard output.

-z
Run in quiet mode, no output is written.  You must check the return
status.  Useful in shell scripts.

-M [ M ]
Enable manage DSA IT control.
-MM
makes control critical.

-d \ debuglevel
Set the LDAP debugging level to *debuglevel*.
ldapcompare
must be compiled with LDAP_DEBUG defined for this option to have any effect.

-x
Use simple authentication instead of SASL.

-D \ binddn
Use the Distinguished Name *binddn* to bind to the LDAP directory.
For SASL binds, the server is expected to ignore this value.

-W
Prompt for simple authentication.
This is used instead of specifying the password on the command line.

-w \ passwd
Use *passwd* as the password for simple authentication.

-y \ passwdfile
Use complete contents of *passwdfile* as the password for
simple authentication.
Note that *complete* means that any leading or trailing whitespaces,
including newlines, will be considered part of the password and,
unlike other software, they will not be stripped.
As a consequence, passwords stored in files by commands like
echo (1)
will not behave as expected, since
echo (1)
by default appends a trailing newline to the echoed string.
The recommended portable way to store a cleartext password in a file
for use with this option is to use
slappasswd (8)
with *\{CLEARTEXT\}* as hash and the option **-n**.

-H \ ldapuri
Specify URI(s) referring to the ldap server(s); only the protocol/host/port
fields are allowed; a list of URI, separated by whitespace or commas
is expected.

-h \ ldaphost
Specify an alternate host on which the ldap server is running.
Deprecated in favor of **-H**.

-p \ ldapport
Specify an alternate TCP port where the ldap server is listening.
Deprecated in favor of **-H**.

-P \ \{ 2 \||\| 3 \}
Specify the LDAP protocol version to use.

-e \ [ ! ] *ext* [ =*extparam* ]

-E \ [ ! ] *ext* [ =*extparam* ]

Specify general extensions with **-e** and search extensions with **-E**.
\'**!**\' indicates criticality.

General extensions:

```
  [!]assert=<filter>   (an RFC 4515 Filter)
  [!]authzid=<authzid> ("dn:<dn>" or "u:<user>")
  [!]manageDSAit
  [!]noop
  ppolicy
  [!]postread[=<attrs>]        (a comma-separated attribute list)
  [!]preread[=<attrs>] (a comma-separated attribute list)
  abandon, cancel (SIGINT sends abandon/cancel; not really controls)
```


Search extensions:

```
  [!]domainScope                               (domain scope)
  [!]mv=<filter>                               (matched values filter)
  [!]pr=<size>[/prompt|noprompt]       (paged results/prompt)
  [!]sss=[\-]<attr[:OID]>[/[\-]<attr[:OID]>...]  (server side sorting)
  [!]subentries[=true|false]           (subentries)
  [!]sync=ro[/<cookie>]                        (LDAP Sync refreshOnly)
          rp[/<cookie>][/<slimit>]     (LDAP Sync refreshAndPersist)
```


-O \ security-properties
Specify SASL security properties.

-I
Enable SASL Interactive mode.  Always prompt.  Default is to prompt
only as needed.

-Q
Enable SASL Quiet mode.  Never prompt.

-U \ authcid
Specify the authentication ID for SASL bind. The form of the ID
depends on the actual SASL mechanism used.

-R \ realm
Specify the realm of authentication ID for SASL bind. The form of the realm
depends on the actual SASL mechanism used.

-X \ authzid
Specify the requested authorization ID for SASL bind.
authzid
must be one of the following formats:
dn: "<distinguished name>"
or
u: <username>

-Y \ mech
Specify the SASL mechanism to be used for authentication. If it's not
specified, the program will choose the best mechanism the server knows.

-Z [ Z ]
Issue StartTLS (Transport Layer Security) extended operation. If you use
**-ZZ**, the command will require the operation to be successful.

## EXAMPLES


```
    ldapcompare "uid=babs,dc=example,dc=com"  sn:Jensen
    ldapcompare "uid=babs,dc=example,dc=com"  sn::SmVuc2Vu
```

are all equivalent.

## LIMITATIONS

Requiring the value be passed on the command line is limiting
and introduces some security concerns.  The command should support
a mechanism to specify the location (file name or URL) to read
the value from.

## SEE ALSO

ldap.conf (5),
ldif (5),
ldap (3),
ldap_compare_ext (3)

## AUTHOR

The OpenLDAP Project <http://www.openldap.org/>

## ACKNOWLEDGEMENTS

"OpenLDAP Software"
is developed and maintained by The OpenLDAP Project <http://www.openldap.org/>.
"OpenLDAP Software"
is derived from University of Michigan LDAP 3.3 Release.
