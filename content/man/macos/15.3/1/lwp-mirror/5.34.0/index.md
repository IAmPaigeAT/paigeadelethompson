+++
author = "None Specified"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_section = "1"
manpage_name = "lwp-mirror"
title = "lwp-mirror(1)"
operating_system = "macos"
date = "2020-04-14"
detected_package_version = "5.34.0"
keywords = ["header", "see", "also", "lwp-request", "s-1lwp", "s0", "author", "gisle", "aas", "no"]
description = "This program can be used to mirror a document from a s-1WWWs0 server.  The document is only transferred if the remote copy is newer than the local copy.  If the local copy is newer nothing happens. Use the f(CW*(C`-v*(C option to print the version..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "LWP-MIRROR 1"
LWP-MIRROR 1 "2020-04-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

lwp-mirror - Simple mirror utility

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
 lwp-mirror [-v] [-t timeout] <url> <local file>
.Ve

## DESCRIPTION

Header "DESCRIPTION"
This program can be used to mirror a document from a \s-1WWW\s0 server.  The
document is only transferred if the remote copy is newer than the local
copy.  If the local copy is newer nothing happens.

Use the \f(CW\*(C`-v\*(C' option to print the version number of this program.

The timeout value specified with the \f(CW\*(C`-t\*(C' option.  The timeout value
is the time that the program will wait for response from the remote
server before it fails.  The default unit for the timeout value is
seconds.  You might append \*(L"m\*(R" or \*(L"h\*(R" to the timeout value to make it
minutes or hours, respectively.

Because this program is implemented using the \s-1LWP\s0 library, it only
supports the protocols that \s-1LWP\s0 supports.

## SEE ALSO

Header "SEE ALSO"
lwp-request, \s-1LWP\s0

## AUTHOR

Header "AUTHOR"
Gisle Aas <gisle@aas.no>
