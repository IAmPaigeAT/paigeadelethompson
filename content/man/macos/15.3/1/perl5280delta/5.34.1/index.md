+++
title = "perl5280delta(1)"
detected_package_version = "5.34.1"
author = "None Specified"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
description = "This document describes differences between the 5.26.0 release and the 5.28.0 release. If you are upgrading from an earlier release such as 5.24.0, first read perl5260delta, which describes differences between 5.24.0 and 5.26.0. A list of changes ..."
operating_system_version = "15.3"
manpage_section = "1"
date = "2022-02-19"
manpage_name = "perl5280delta"
operating_system = "macos"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5280DELTA 1"
PERL5280DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5280delta - what is new for perl v5.28.0

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.26.0 release and the 5.28.0
release.

If you are upgrading from an earlier release such as 5.24.0, first read
perl5260delta, which describes differences between 5.24.0 and 5.26.0.

## Core Enhancements

Header "Core Enhancements"

### Unicode 10.0 is supported

Subsection "Unicode 10.0 is supported"
A list of changes is at
<http://www.unicode.org/versions/Unicode10.0.0>.
.ie n .SS """delete"" on key/value hash slices"
.el .SS "\f(CWdelete on key/value hash slices"
Subsection "delete on key/value hash slices"
\f(CW\*(C`delete\*(C' can now be used on
key/value hash slices,
returning the keys along with the deleted values.
[\s-1GH\s0 #15982] <https://github.com/Perl/perl5/issues/15982>

### Experimentally, there are now alphabetic synonyms for some regular expression assertions

Subsection "Experimentally, there are now alphabetic synonyms for some regular expression assertions"
If you find it difficult to remember how to write certain of the pattern
assertions, there are now alphabetic synonyms.

.Vb 7
 CURRENT                NEW SYNONYMS
 ------                 ------------
 (?=...)        (*pla:...) or (*positive_lookahead:...)
 (?!...)        (*nla:...) or (*negative_lookahead:...)
 (?<=...)       (*plb:...) or (*positive_lookbehind:...)
 (?<!...)       (*nlb:...) or (*negative_lookbehind:...)
 (?>...)        (*atomic:...)
.Ve

These are considered experimental, so using any of these will raise
(unless turned off) a warning in the \f(CW\*(C`experimental::alpha_assertions\*(C'
category.

### Mixed Unicode scripts are now detectable

Subsection "Mixed Unicode scripts are now detectable"
A mixture of scripts, such as Cyrillic and Latin, in a string is often
the sign of a spoofing attack.  A new regular expression construct
now allows for easy detection of these.  For example, you can say

.Vb 1
 qr/(*script_run: \\d+ \\b )/x
.Ve

And the digits matched will all be from the same set of 10.  You won't
get a look-alike digit from a different script that has a different
value than what it appears to be.

Or:

.Vb 1
 qr/(*sr: \\b \\w+ \\b )/x
.Ve

makes sure that all the characters come from the same script.

You can also combine script runs with \f(CW\*(C`(?>...)\*(C' (or
\f(CW\*(C`*atomic:...)\*(C').

Instead of writing:

.Vb 1
    (*sr:(?<...))
.Ve

you can now run:

.Vb 3
    (*asr:...)
    # or
    (*atomic_script_run:...)
.Ve

This is considered experimental, so using it will raise (unless turned
off) a warning in the \f(CW\*(C`experimental::script_run\*(C' category.

See \*(L"Script Runs\*(R" in perlre.
.ie n .SS "In-place editing with ""perl -i"" is now safer"
.el .SS "In-place editing with \f(CWperl -i is now safer"
Subsection "In-place editing with perl -i is now safer"
Previously in-place editing (\f(CW\*(C`perl -i\*(C') would delete or rename the
input file as soon as you started working on a new file.

Without backups this would result in loss of data if there was an
error, such as a full disk, when writing to the output file.

This has changed so that the input file isn't replaced until the
output file has been completely written and successfully closed.

This works by creating a work file in the same directory, which is
renamed over the input file once the output file is complete.

Incompatibilities:

- \(bu
Since this renaming needs to only happen once, if you create a thread
or child process, that renaming will only happen in the original
thread or process.

- \(bu
If you change directories while processing a file, and your operating
system doesn't provide the \f(CW\*(C`unlinkat()\*(C', \f(CW\*(C`renameat()\*(C' and \f(CW\*(C`fchmodat()\*(C'
functions, the final rename step may fail.

[\s-1GH\s0 #15216] <https://github.com/Perl/perl5/issues/15216>

### Initialisation of aggregate state variables

Subsection "Initialisation of aggregate state variables"
A persistent lexical array or hash variable can now be initialized,
by an expression such as \f(CW\*(C`state @a = qw(x y z)\*(C'.  Initialization of a
list of persistent lexical variables is still not possible.

### Full-size inode numbers

Subsection "Full-size inode numbers"
On platforms where inode numbers are of a type larger than perl's native
integer numerical types, stat will preserve the full
content of large inode numbers by returning them in the form of strings of
decimal digits.  Exact comparison of inode numbers can thus be achieved by
comparing with \f(CW\*(C`eq\*(C' rather than \f(CW\*(C`==\*(C'.  Comparison with \f(CW\*(C`==\*(C', and other
numerical operations (which are usually meaningless on inode numbers),
work as well as they did before, which is to say they fall back to
floating point, and ultimately operate on a fairly useless rounded inode
number if the real inode number is too big for the floating point format.
.ie n .SS "The ""sprintf"" %j format size modifier is now available with pre-C99 compilers"
.el .SS "The \f(CWsprintf \f(CW%j format size modifier is now available with pre-C99 compilers"
Subsection "The sprintf %j format size modifier is now available with pre-C99 compilers"
The actual size used depends on the platform, so remains unportable.

### Close-on-exec flag set atomically

Subsection "Close-on-exec flag set atomically"
When opening a file descriptor, perl now generally opens it with its
close-on-exec flag already set, on platforms that support doing so.
This improves thread safety, because it means that an \f(CW\*(C`exec\*(C' initiated
by one thread can no longer cause a file descriptor in the process
of being opened by another thread to be accidentally passed to the
executed program.

Additionally, perl now sets the close-on-exec flag more reliably, whether
it does so atomically or not.  Most file descriptors were getting the
flag set, but some were being missed.

### String- and number-specific bitwise ops are no longer experimental

Subsection "String- and number-specific bitwise ops are no longer experimental"
The new string-specific (\f(CW\*(C`&. |. ^. ~.\*(C') and number-specific (\f(CW\*(C`& | ^ ~\*(C')
bitwise operators introduced in Perl 5.22 that are available within the
scope of \f(CW\*(C`use feature \*(Aqbitwise\*(Aq\*(C' are no longer experimental.
Because the number-specific ops are spelled the same way as the existing
operators that choose their behaviour based on their operands, these
operators must still be enabled via the \*(L"bitwise\*(R" feature, in either of
these two ways:

.Vb 1
    use feature "bitwise";

    use v5.28; # "bitwise" now included
.Ve

They are also now enabled by the **-E** command-line switch.

The \*(L"bitwise\*(R" feature no longer emits a warning.  Existing code that
disables the \*(L"experimental::bitwise\*(R" warning category that the feature
previously used will continue to work.

One caveat that module authors ought to be aware of is that the numeric
operators now pass a fifth \s-1TRUE\s0 argument to overload methods.  Any methods
that check the number of operands may croak if they do not expect so many.
\s-1XS\s0 authors in particular should be aware that this:

.Vb 2
    SV *
    bitop_handler (lobj, robj, swap)
.Ve

may need to be changed to this:

.Vb 2
    SV *
    bitop_handler (lobj, robj, swap, ...)
.Ve

### Locales are now thread-safe on systems that support them

Subsection "Locales are now thread-safe on systems that support them"
These systems include Windows starting with Visual Studio 2005, and in
\s-1POSIX 2008\s0 systems.

The implication is that you are now free to use locales and change them
in a threaded environment.  Your changes affect only your thread.
See \*(L"Multi-threaded operation\*(R" in perllocale
.ie n .SS "New read-only predefined variable ""$\{^SAFE_LOCALES\}"""
.el .SS "New read-only predefined variable \f(CW$\{^SAFE_LOCALES\}"
Subsection "New read-only predefined variable $\{^SAFE_LOCALES\}"
This variable is 1 if the Perl interpreter is operating in an
environment where it is safe to use and change locales (see
perllocale.)  This variable is true when the perl is
unthreaded, or compiled in a platform that supports thread-safe locale
operation (see previous item).

## Security

Header "Security"

### [\s-1CVE-2017-12837\s0] Heap buffer overflow in regular expression compiler

Subsection "[CVE-2017-12837] Heap buffer overflow in regular expression compiler"
Compiling certain regular expression patterns with the case-insensitive
modifier could cause a heap buffer overflow and crash perl.  This has now been
fixed.
[\s-1GH\s0 #16021] <https://github.com/Perl/perl5/issues/16021>

### [\s-1CVE-2017-12883\s0] Buffer over-read in regular expression parser

Subsection "[CVE-2017-12883] Buffer over-read in regular expression parser"
For certain types of syntax error in a regular expression pattern, the error
message could either contain the contents of a random, possibly large, chunk of
memory, or could crash perl.  This has now been fixed.
[\s-1GH\s0 #16025] <https://github.com/Perl/perl5/issues/16025>
.ie n .SS "[\s-1CVE-2017-12814\s0] $ENV\{$key\} stack buffer overflow on Windows"
.el .SS "[\s-1CVE-2017-12814\s0] \f(CW$ENV\{$key\} stack buffer overflow on Windows"
Subsection "[CVE-2017-12814] $ENV\{$key\} stack buffer overflow on Windows"
A possible stack buffer overflow in the \f(CW%ENV code on Windows has been fixed
by removing the buffer completely since it was superfluous anyway.
[\s-1GH\s0 #16051] <https://github.com/Perl/perl5/issues/16051>

### Default Hash Function Change

Subsection "Default Hash Function Change"
Perl 5.28.0 retires various older hash functions which are not viewed as
sufficiently secure for use in Perl. We now support four general purpose
hash functions, Siphash (2-4 and 1-3 variants), and  Zaphod32, and StadtX
hash. In addition we support \s-1SBOX32\s0 (a form of tabular hashing) for hashing
short strings, in conjunction with any of the other hash functions provided.

By default Perl is configured to support \s-1SBOX\s0 hashing of strings up to 24
characters, in conjunction with StadtX hashing on 64 bit builds, and
Zaphod32 hashing for 32 bit builds.

You may control these settings with the following options to Configure:

.Vb 4
    -DPERL_HASH_FUNC_SIPHASH
    -DPERL_HASH_FUNC_SIPHASH13
    -DPERL_HASH_FUNC_STADTX
    -DPERL_HASH_FUNC_ZAPHOD32
.Ve

To disable \s-1SBOX\s0 hashing you can use

.Vb 1
    -DPERL_HASH_USE_SBOX32_ALSO=0
.Ve

And to set the maximum length to use \s-1SBOX32\s0 hashing on with:

.Vb 1
    -DSBOX32_MAX_LEN=16
.Ve

The maximum length allowed is 256. There probably isn't much point
in setting it higher than the default.

## Incompatible Changes

Header "Incompatible Changes"

### Subroutine attribute and signature order

Subsection "Subroutine attribute and signature order"
The experimental subroutine signatures feature has been changed so that
subroutine attributes must now come before the signature rather than
after. This is because attributes like \f(CW\*(C`:lvalue\*(C' can affect the
compilation of code within the signature, for example:

.Vb 1
    sub f :lvalue ($a = do \{ $x = "abc"; return substr($x,0,1)\}) \{ ...\}
.Ve

Note that this the second time they have been flipped:

.Vb 2
    sub f :lvalue ($a, $b) \{ ... \}; # 5.20; 5.28 onwards
    sub f ($a, $b) :lvalue \{ ... \}; # 5.22 - 5.26
.Ve

### Comma-less variable lists in formats are no longer allowed

Subsection "Comma-less variable lists in formats are no longer allowed"
Omitting the commas between variables passed to formats is no longer
allowed.  This has been deprecated since Perl 5.000.
.ie n .SS "The "":locked"" and "":unique"" attributes have been removed"
.el .SS "The \f(CW:locked and \f(CW:unique attributes have been removed"
Subsection "The :locked and :unique attributes have been removed"
These have been no-ops and deprecated since Perl 5.12 and 5.10,
respectively.
.ie n .SS """\\N\{\}"" with nothing between the braces is now illegal"
.el .SS "\f(CW\\N\{\} with nothing between the braces is now illegal"
Subsection "N\{\} with nothing between the braces is now illegal"
This has been deprecated since Perl 5.24.

### Opening the same symbol as both a file and directory handle is no longer allowed

Subsection "Opening the same symbol as both a file and directory handle is no longer allowed"
Using \f(CW\*(C`open()\*(C' and \f(CW\*(C`opendir()\*(C' to associate both a filehandle and a dirhandle
to the same symbol (glob or scalar) has been deprecated since Perl 5.10.
.ie n .SS "Use of bare ""<<"" to mean ""<<"""""" is no longer allowed"
.el .SS "Use of bare \f(CW<< to mean \f(CW<<``'' is no longer allowed"
Subsection "Use of bare << to mean <<"""" is no longer allowed"
Use of a bare terminator has been deprecated since Perl 5.000.

### Setting $/ to a reference to a non-positive integer no longer allowed

Subsection "Setting $/ to a reference to a non-positive integer no longer allowed"
This used to work like setting it to \f(CW\*(C`undef\*(C', but has been deprecated
since Perl 5.20.
.ie n .SS "Unicode code points with values exceeding ""IV_MAX"" are now fatal"
.el .SS "Unicode code points with values exceeding \f(CWIV_MAX are now fatal"
Subsection "Unicode code points with values exceeding IV_MAX are now fatal"
This was deprecated since Perl 5.24.
.ie n .SS "The ""B::OP::terse"" method has been removed"
.el .SS "The \f(CWB::OP::terse method has been removed"
Subsection "The B::OP::terse method has been removed"
Use \f(CW\*(C`B::Concise::b_terse\*(C' instead.

### Use of inherited \s-1AUTOLOAD\s0 for non-methods is no longer allowed

Subsection "Use of inherited AUTOLOAD for non-methods is no longer allowed"
This was deprecated in Perl 5.004.

### Use of strings with code points over 0xFF is not allowed for bitwise string operators

Subsection "Use of strings with code points over 0xFF is not allowed for bitwise string operators"
Code points over \f(CW0xFF do not make sense for bitwise operators and such
an operation will now croak, except for a few remaining cases. See
perldeprecation.

This was deprecated in Perl 5.24.
.ie n .SS "Setting ""$\{^ENCODING\}"" to a defined value is now illegal"
.el .SS "Setting \f(CW$\{^ENCODING\} to a defined value is now illegal"
Subsection "Setting $\{^ENCODING\} to a defined value is now illegal"
This has been deprecated since Perl 5.22 and a no-op since Perl 5.26.
.ie n .SS "Backslash no longer escapes colon in \s-1PATH\s0 for the ""-S"" switch"
.el .SS "Backslash no longer escapes colon in \s-1PATH\s0 for the \f(CW-S switch"
Subsection "Backslash no longer escapes colon in PATH for the -S switch"
Previously the \f(CW\*(C`-S\*(C' switch incorrectly treated backslash (\*(L"\\\*(R") as an
escape for colon when traversing the \f(CW\*(C`PATH\*(C' environment variable.
[\s-1GH\s0 #15584] <https://github.com/Perl/perl5/issues/15584>

### the -DH (\s-1DEBUG_H\s0) misfeature has been removed

Subsection "the -DH (DEBUG_H) misfeature has been removed"
On a perl built with debugging support, the \f(CW\*(C`H\*(C' flag to the \f(CW\*(C`-D\*(C'
debugging option has been removed. This was supposed to dump hash values,
but has been broken for many years.

### Yada-yada is now strictly a statement

Subsection "Yada-yada is now strictly a statement"
By the time of its initial stable release in Perl 5.12, the \f(CW\*(C`...\*(C'
(yada-yada) operator was explicitly intended to serve as a statement,
not an expression.  However, the original implementation was confused
on this point, leading to inconsistent parsing.  The operator was
accidentally accepted in a few situations where it did not serve as a
complete statement, such as

.Vb 2
    ... . "foo";
    ... if $a < $b;
.Ve

The parsing has now been made consistent, permitting yada-yada only as
a statement.  Affected code can use \f(CW\*(C`do\{...\}\*(C' to put a yada-yada into
an arbitrary expression context.

### Sort algorithm can no longer be specified

Subsection "Sort algorithm can no longer be specified"
Since Perl 5.8, the sort pragma has had subpragmata \f(CW\*(C`_mergesort\*(C',
\f(CW\*(C`_quicksort\*(C', and \f(CW\*(C`_qsort\*(C' that can be used to specify which algorithm
perl should use to implement the sort builtin.
This was always considered a dubious feature that might not last,
hence the underscore spellings, and they were documented as not being
portable beyond Perl 5.8.  These subpragmata have now been deleted,
and any attempt to use them is an error.  The sort pragma otherwise
remains, and the algorithm-neutral \f(CW\*(C`stable\*(C' subpragma can be used to
control sorting behaviour.
[\s-1GH\s0 #13234] <https://github.com/Perl/perl5/issues/13234>

### Over-radix digits in floating point literals

Subsection "Over-radix digits in floating point literals"
Octal and binary floating point literals used to permit any hexadecimal
digit to appear after the radix point.  The digits are now restricted
to those appropriate for the radix, as digits before the radix point
always were.
.ie n .SS "Return type of ""unpackstring()"""
.el .SS "Return type of \f(CWunpackstring()"
Subsection "Return type of unpackstring()"
The return types of the C \s-1API\s0 functions \f(CW\*(C`unpackstring()\*(C' and
\f(CW\*(C`unpack_str()\*(C' have changed from \f(CW\*(C`I32\*(C' to \f(CW\*(C`SSize_t\*(C', in order to
accommodate datasets of more than two billion items.

## Deprecations

Header "Deprecations"
.ie n .SS "Use of ""vec"" on strings with code points above 0xFF is deprecated"
.el .SS "Use of \f(CWvec on strings with code points above 0xFF is deprecated"
Subsection "Use of vec on strings with code points above 0xFF is deprecated"
Such strings are represented internally in \s-1UTF-8,\s0 and \f(CW\*(C`vec\*(C' is a
bit-oriented operation that will likely give unexpected results on those
strings.
.ie n .SS "Some uses of unescaped ""\{"" in regexes are no longer fatal"
.el .SS "Some uses of unescaped \f(CW``\{'' in regexes are no longer fatal"
Subsection "Some uses of unescaped ""\{"" in regexes are no longer fatal"
Perl 5.26.0 fatalized some uses of an unescaped left brace, but an
exception was made at the last minute, specifically crafted to be a
minimal change to allow \s-1GNU\s0 Autoconf to work.  That tool is heavily
depended upon, and continues to use the deprecated usage.  Its use of an
unescaped left brace is one where we have no intention of repurposing
\f(CW"\{" to be something other than itself.

That exception is now generalized to include various other such cases
where the \f(CW"\{" will not be repurposed.

Note that these uses continue to raise a deprecation message.
.ie n .SS "Use of unescaped ""\{"" immediately after a ""("" in regular expression patterns is deprecated"
.el .SS "Use of unescaped \f(CW``\{'' immediately after a \f(CW``('' in regular expression patterns is deprecated"
Subsection "Use of unescaped ""\{"" immediately after a ""("" in regular expression patterns is deprecated"
Using unescaped left braces is officially deprecated everywhere, but it
is not enforced in contexts where their use does not interfere with
expected extensions to the language.  A deprecation is added in this
release when the brace appears immediately after an opening parenthesis.
Before this, even if the brace was part of a legal quantifier, it was
not interpreted as such, but as the literal characters, unlike other
quantifiers that follow a \f(CW"(" which are considered errors.  Now,
their use will raise a deprecation message, unless turned off.
.ie n .SS "Assignment to $[ will be fatal in Perl 5.30"
.el .SS "Assignment to \f(CW$[ will be fatal in Perl 5.30"
Subsection "Assignment to $[ will be fatal in Perl 5.30"
Assigning a non-zero value to \f(CW$[ has been deprecated
since Perl 5.12, but was never given a deadline for removal.  This has
now been scheduled for Perl 5.30.

### \fBhostname() wont accept arguments in Perl 5.32

Subsection "hostname() won't accept arguments in Perl 5.32"
Passing arguments to \f(CW\*(C`Sys::Hostname::hostname()\*(C' was already deprecated,
but didn't have a removal date.  This has now been scheduled for Perl
5.32.  [\s-1GH\s0 #14662] <https://github.com/Perl/perl5/issues/14662>

### Module removals

Subsection "Module removals"
The following modules will be removed from the core distribution in a
future release, and will at that time need to be installed from \s-1CPAN.\s0
Distributions on \s-1CPAN\s0 which require these modules will need to list them as
prerequisites.

The core versions of these modules will now issue \f(CW"deprecated"-category
warnings to alert you to this fact.  To silence these deprecation warnings,
install the modules in question from \s-1CPAN.\s0

Note that these are (with rare exceptions) fine modules that you are encouraged
to continue to use.  Their disinclusion from core primarily hinges on their
necessity to bootstrapping a fully functional, CPAN-capable Perl installation,
not usually on concerns over their design.

- B::Debug
Item "B::Debug"
0

- Locale::Codes and its associated Country, Currency and Language modules
Item "Locale::Codes and its associated Country, Currency and Language modules"
.PD

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
The start up overhead for creating regular expression patterns with
Unicode properties (\f(CW\*(C`\\p\{...\}\*(C') has been greatly reduced in most cases.

- \(bu
Many string concatenation expressions are now considerably faster, due
to the introduction internally of a \f(CW\*(C`multiconcat\*(C' opcode which combines
multiple concatenations, and optionally a \f(CW\*(C`=\*(C' or \f(CW\*(C`.=\*(C', into a single
action. For example, apart from retrieving \f(CW$s, \f(CW$a and \f(CW$b, this
whole expression is now handled as a single op:
.Sp
.Vb 1
    $s .= "a=$a b=$b\\n"
.Ve
.Sp
As a special case, if the \s-1LHS\s0 of an assignment is a lexical variable or
\f(CW\*(C`my $s\*(C', the op itself handles retrieving the lexical variable, which
is faster.
.Sp
In general, the more the expression includes a mix of constant strings and
variable expressions, the longer the expression, and the more it mixes
together non-utf8 and utf8 strings, the more marked the performance
improvement. For example on a \f(CW\*(C`x86_64\*(C' system, this code has been
benchmarked running four times faster:
.Sp
.Vb 4
    my $s;
    my $a = "ab\\x\{100\}cde";
    my $b = "fghij";
    my $c = "\\x\{101\}klmn";

    for my $i (1..10_000_000) \{
        $s = "\\x\{100\}wxyz";
        $s .= "foo=$a bar=$b baz=$c";
    \}
.Ve
.Sp
In addition, \f(CW\*(C`sprintf\*(C' expressions which have a constant format
containing only \f(CW%s and \f(CW\*(C`%%\*(C' format elements, and which have a fixed
number of arguments, are now also optimised into a \f(CW\*(C`multiconcat\*(C' op.

- \(bu
The \f(CW\*(C`ref()\*(C' builtin is now much faster in boolean context, since it no
longer bothers to construct a temporary string like \f(CW\*(C`Foo=ARRAY(0x134af48)\*(C'.

- \(bu
\f(CW\*(C`keys()\*(C' in void and scalar contexts is now more efficient.

- \(bu
The common idiom of comparing the result of **index()** with -1 is now
specifically optimised,  e.g.
.Sp
.Vb 1
    if (index(...) != -1) \{ ... \}
.Ve

- \(bu
\f(CW\*(C`for()\*(C' loops and similar constructs are now more efficient in most cases.

- \(bu
File::Glob has been modified to remove unnecessary backtracking and
recursion, thanks to Russ Cox. See <https://research.swtch.com/glob>
for more details.

- \(bu
The XS-level \f(CW\*(C`SvTRUE()\*(C' \s-1API\s0 function is now more efficient.

- \(bu
Various integer-returning ops are now more efficient in scalar/boolean context.

- \(bu
Slightly improved performance when parsing stash names.
[\s-1GH\s0 #15689] <https://github.com/Perl/perl5/issues/15689>

- \(bu
Calls to \f(CW\*(C`require\*(C' for an already loaded module are now slightly faster.
[\s-1GH\s0 #16175] <https://github.com/Perl/perl5/issues/16175>

- \(bu
The performance of pattern matching \f(CW\*(C`[[:ascii:]]\*(C' and \f(CW\*(C`[[:^ascii:]]\*(C'
has been improved significantly except on \s-1EBCDIC\s0 platforms.

- \(bu
Various optimizations have been applied to matching regular expression
patterns, so under the right circumstances, significant performance
gains may be noticed.  But in an application with many varied patterns,
little overall improvement likely will be seen.

- \(bu
Other optimizations have been applied to \s-1UTF-8\s0 handling, but these are
not typically a major factor in most applications.

## Modules and Pragmata

Header "Modules and Pragmata"
Key highlights in this release across several modules:

### Removal of use vars

Subsection "Removal of use vars"
The usage of \f(CW\*(C`use vars\*(C' has been discouraged since the introduction of
\f(CW\*(C`our\*(C' in Perl 5.6.0. Where possible the usage of this pragma has now been
removed from the Perl source code.

This had a slight effect (for the better) on the output of \s-1WARNING_BITS\s0 in
B::Deparse.

### Use of DynaLoader changed to XSLoader in many modules

Subsection "Use of DynaLoader changed to XSLoader in many modules"
XSLoader is more modern, and most modules already require perl 5.6 or
greater, so no functionality is lost by switching. In some cases, we have
also made changes to the local implementation that may not be reflected in
the version on \s-1CPAN\s0 due to a desire to maintain more backwards
compatibility.

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Archive::Tar has been upgraded from version 2.24 to 2.30.
.Sp
This update also handled \s-1CVE-2018-12015:\s0 directory traversal
vulnerability.
[cpan #125523] <https://rt.cpan.org/Ticket/Display.html?id=125523>

- \(bu
arybase has been upgraded from version 0.12 to 0.15.

- \(bu
Attribute::Handlers has been upgraded from version 0.99 to 1.01.

- \(bu
attributes has been upgraded from version 0.29 to 0.33.

- \(bu
B has been upgraded from version 1.68 to 1.74.

- \(bu
B::Concise has been upgraded from version 0.999 to 1.003.

- \(bu
B::Debug has been upgraded from version 1.24 to 1.26.
.Sp
\s-1NOTE:\s0 B::Debug is deprecated and may be removed from a future version
of Perl.

- \(bu
B::Deparse has been upgraded from version 1.40 to 1.48.
.Sp
It includes many bug fixes, and in particular, it now deparses variable
attributes correctly:
.Sp
.Vb 2
    my $x :foo;  # used to deparse as
                 # \*(Aqattributes\*(Aq->import(\*(Aqmain\*(Aq, \\$x, \*(Aqfoo\*(Aq), my $x;
.Ve

- \(bu
base has been upgraded from version 2.25 to 2.27.

- \(bu
bignum has been upgraded from version 0.47 to 0.49.

- \(bu
blib has been upgraded from version 1.06 to 1.07.

- \(bu
bytes has been upgraded from version 1.05 to 1.06.

- \(bu
Carp has been upgraded from version 1.42 to 1.50.
.Sp
If a package on the call stack contains a constant named \f(CW\*(C`ISA\*(C', Carp no
longer throws a \*(L"Not a \s-1GLOB\s0 reference\*(R" error.
.Sp
Carp, when generating stack traces, now attempts to work around
longstanding bugs resulting from Perl's non-reference-counted stack.
[\s-1GH\s0 #9282] <https://github.com/Perl/perl5/issues/9282>
.Sp
Carp has been modified to avoid assuming that objects cannot be
overloaded without the overload module loaded (this can happen with
objects created by \s-1XS\s0 modules).  Previously, infinite recursion would
result if an XS-defined overload method itself called Carp.
[\s-1GH\s0 #16407] <https://github.com/Perl/perl5/issues/16407>
.Sp
Carp now avoids using \f(CW\*(C`overload::StrVal\*(C', partly because older versions
of overload (included with perl 5.14 and earlier) load Scalar::Util
at run time, which will fail if Carp has been invoked after a syntax error.

- \(bu
charnames has been upgraded from version 1.44 to 1.45.

- \(bu
Compress::Raw::Zlib has been upgraded from version 2.074 to 2.076.
.Sp
This addresses a security vulnerability in older versions of the 'zlib' library
(which is bundled with Compress-Raw-Zlib).

- \(bu
Config::Extensions has been upgraded from version 0.01 to 0.02.

- \(bu
Config::Perl::V has been upgraded from version 0.28 to 0.29.

- \(bu
\s-1CPAN\s0 has been upgraded from version 2.18 to 2.20.

- \(bu
Data::Dumper has been upgraded from version 2.167 to 2.170.
.Sp
Quoting of glob names now obeys the Useqq option
[\s-1GH\s0 #13274] <https://github.com/Perl/perl5/issues/13274>.
.Sp
Attempts to set an option to \f(CW\*(C`undef\*(C' through a combined getter/setter
method are no longer mistaken for getter calls
[\s-1GH\s0 #12135] <https://github.com/Perl/perl5/issues/12135>.

- \(bu
Devel::Peek has been upgraded from version 1.26 to 1.27.

- \(bu
Devel::PPPort has been upgraded from version 3.35 to 3.40.
.Sp
Devel::PPPort has moved from cpan-first to perl-first maintenance
.Sp
Primary responsibility for the code in Devel::PPPort has moved into core perl.
In a practical sense there should be no change except that hopefully it will
stay more up to date with changes made to symbols in perl, rather than needing
to be updated after the fact.

- \(bu
Digest::SHA has been upgraded from version 5.96 to 6.01.

- \(bu
DirHandle has been upgraded from version 1.04 to 1.05.

- \(bu
DynaLoader has been upgraded from version 1.42 to 1.45.
.Sp
Its documentation now shows the use of \f(CW\*(C`_\|_PACKAGE_\|_\*(C' and direct object
syntax
[\s-1GH\s0 #16190] <https://github.com/Perl/perl5/issues/16190>.

- \(bu
Encode has been upgraded from version 2.88 to 2.97.

- \(bu
encoding has been upgraded from version 2.19 to 2.22.

- \(bu
Errno has been upgraded from version 1.28 to 1.29.

- \(bu
experimental has been upgraded from version 0.016 to 0.019.

- \(bu
Exporter has been upgraded from version 5.72 to 5.73.

- \(bu
ExtUtils::CBuilder has been upgraded from version 0.280225 to 0.280230.

- \(bu
ExtUtils::Constant has been upgraded from version 0.23 to 0.25.

- \(bu
ExtUtils::Embed has been upgraded from version 1.34 to 1.35.

- \(bu
ExtUtils::Install has been upgraded from version 2.04 to 2.14.

- \(bu
ExtUtils::MakeMaker has been upgraded from version 7.24 to 7.34.

- \(bu
ExtUtils::Miniperl has been upgraded from version 1.06 to 1.08.

- \(bu
ExtUtils::ParseXS has been upgraded from version 3.34 to 3.39.

- \(bu
ExtUtils::Typemaps has been upgraded from version 3.34 to 3.38.

- \(bu
ExtUtils::XSSymSet has been upgraded from version 1.3 to 1.4.

- \(bu
feature has been upgraded from version 1.47 to 1.52.

- \(bu
fields has been upgraded from version 2.23 to 2.24.

- \(bu
File::Copy has been upgraded from version 2.32 to 2.33.
.Sp
It will now use the sub-second precision variant of **utime()** supplied by
Time::HiRes where available.
[\s-1GH\s0 #16225] <https://github.com/Perl/perl5/issues/16225>.

- \(bu
File::Fetch has been upgraded from version 0.52 to 0.56.

- \(bu
File::Glob has been upgraded from version 1.28 to 1.31.

- \(bu
File::Path has been upgraded from version 2.12_01 to 2.15.

- \(bu
File::Spec and Cwd have been upgraded from version 3.67 to 3.74.

- \(bu
File::stat has been upgraded from version 1.07 to 1.08.

- \(bu
FileCache has been upgraded from version 1.09 to 1.10.

- \(bu
Filter::Simple has been upgraded from version 0.93 to 0.95.

- \(bu
Filter::Util::Call has been upgraded from version 1.55 to 1.58.

- \(bu
GDBM_File has been upgraded from version 1.15 to 1.17.
.Sp
Its documentation now explains that \f(CW\*(C`each\*(C' and \f(CW\*(C`delete\*(C' don't mix in
hashes tied to this module
[\s-1GH\s0 #12894] <https://github.com/Perl/perl5/issues/12894>.
.Sp
It will now retry opening with an acceptable block size if asking gdbm
to default the block size failed
[\s-1GH\s0 #13232] <https://github.com/Perl/perl5/issues/13232>.

- \(bu
Getopt::Long has been upgraded from version 2.49 to 2.5.

- \(bu
Hash::Util::FieldHash has been upgraded from version 1.19 to 1.20.

- \(bu
I18N::Langinfo has been upgraded from version 0.13 to 0.17.
.Sp
This module is now available on all platforms, emulating the system
**nl_langinfo**\|(3) on systems that lack it.  Some caveats apply, as
detailed in its documentation, the most severe being
that, except for \s-1MS\s0 Windows, the \f(CW\*(C`CODESET\*(C' item is not implemented on
those systems, always returning \f(CW"".
.Sp
It now sets the \s-1UTF-8\s0 flag in its returned scalar if the string contains
legal non-ASCII \s-1UTF-8,\s0 and the locale is \s-1UTF-8\s0
[\s-1GH\s0 #15131] <https://github.com/Perl/perl5/issues/15131>.
.Sp
This update also fixes a bug in which the underlying locale was ignored
for the \f(CW\*(C`RADIXCHAR\*(C' (always was returned as a dot) and the \f(CW\*(C`THOUSEP\*(C'
(always empty).  Now the locale-appropriate values are returned.

- \(bu
I18N::LangTags has been upgraded from version 0.42 to 0.43.

- \(bu
if has been upgraded from version 0.0606 to 0.0608.

- \(bu
\s-1IO\s0 has been upgraded from version 1.38 to 1.39.

- \(bu
IO::Socket::IP has been upgraded from version 0.38 to 0.39.

- \(bu
IPC::Cmd has been upgraded from version 0.96 to 1.00.

- \(bu
\s-1JSON::PP\s0 has been upgraded from version 2.27400_02 to 2.97001.

- \(bu
The \f(CW\*(C`libnet\*(C' distribution has been upgraded from version 3.10 to 3.11.

- \(bu
List::Util has been upgraded from version 1.46_02 to 1.49.

- \(bu
Locale::Codes has been upgraded from version 3.42 to 3.56.
.Sp
**\s-1NOTE\s0**: Locale::Codes scheduled to be removed from core in Perl 5.30.

- \(bu
Locale::Maketext has been upgraded from version 1.28 to 1.29.

- \(bu
Math::BigInt has been upgraded from version 1.999806 to 1.999811.

- \(bu
Math::BigInt::FastCalc has been upgraded from version 0.5005 to 0.5006.

- \(bu
Math::BigRat has been upgraded from version 0.2611 to 0.2613.

- \(bu
Module::CoreList has been upgraded from version 5.20170530 to 5.20180622.

- \(bu
mro has been upgraded from version 1.20 to 1.22.

- \(bu
Net::Ping has been upgraded from version 2.55 to 2.62.

- \(bu
\s-1NEXT\s0 has been upgraded from version 0.67 to 0.67_01.

- \(bu
ODBM_File has been upgraded from version 1.14 to 1.15.

- \(bu
Opcode has been upgraded from version 1.39 to 1.43.

- \(bu
overload has been upgraded from version 1.28 to 1.30.

- \(bu
PerlIO::encoding has been upgraded from version 0.25 to 0.26.

- \(bu
PerlIO::scalar has been upgraded from version 0.26 to 0.29.

- \(bu
PerlIO::via has been upgraded from version 0.16 to 0.17.

- \(bu
Pod::Functions has been upgraded from version 1.11 to 1.13.

- \(bu
Pod::Html has been upgraded from version 1.2202 to 1.24.
.Sp
A title for the \s-1HTML\s0 document will now be automatically generated by
default from a \*(L"\s-1NAME\*(R"\s0 section in the \s-1POD\s0 document, as it used to be
before the module was rewritten to use Pod::Simple::XHTML to do the
core of its job
[\s-1GH\s0 #11954] <https://github.com/Perl/perl5/issues/11954>.

- \(bu
Pod::Perldoc has been upgraded from version 3.28 to 3.2801.

- \(bu
The \f(CW\*(C`podlators\*(C' distribution has been upgraded from version 4.09 to 4.10.
.Sp
Man page references and function names now follow the Linux man page
formatting standards, instead of the Solaris standard.

- \(bu
\s-1POSIX\s0 has been upgraded from version 1.76 to 1.84.
.Sp
Some more cautions were added about using locale-specific functions in
threaded applications.

- \(bu
re has been upgraded from version 0.34 to 0.36.

- \(bu
Scalar::Util has been upgraded from version 1.46_02 to 1.50.

- \(bu
SelfLoader has been upgraded from version 1.23 to 1.25.

- \(bu
Socket has been upgraded from version 2.020_03 to 2.027.

- \(bu
sort has been upgraded from version 2.02 to 2.04.

- \(bu
Storable has been upgraded from version 2.62 to 3.08.

- \(bu
Sub::Util has been upgraded from version 1.48 to 1.49.

- \(bu
subs has been upgraded from version 1.02 to 1.03.

- \(bu
Sys::Hostname has been upgraded from version 1.20 to 1.22.

- \(bu
Term::ReadLine has been upgraded from version 1.16 to 1.17.

- \(bu
Test has been upgraded from version 1.30 to 1.31.

- \(bu
Test::Harness has been upgraded from version 3.38 to 3.42.

- \(bu
Test::Simple has been upgraded from version 1.302073 to 1.302133.

- \(bu
threads has been upgraded from version 2.15 to 2.22.
.Sp
The documentation now better describes the problems that arise when
returning values from threads, and no longer warns about creating threads
in \f(CW\*(C`BEGIN\*(C' blocks.
[\s-1GH\s0 #11563] <https://github.com/Perl/perl5/issues/11563>

- \(bu
threads::shared has been upgraded from version 1.56 to 1.58.

- \(bu
Tie::Array has been upgraded from version 1.06 to 1.07.

- \(bu
Tie::StdHandle has been upgraded from version 4.4 to 4.5.

- \(bu
Time::gmtime has been upgraded from version 1.03 to 1.04.

- \(bu
Time::HiRes has been upgraded from version 1.9741 to 1.9759.

- \(bu
Time::localtime has been upgraded from version 1.02 to 1.03.

- \(bu
Time::Piece has been upgraded from version 1.31 to 1.3204.

- \(bu
Unicode::Collate has been upgraded from version 1.19 to 1.25.

- \(bu
Unicode::Normalize has been upgraded from version 1.25 to 1.26.

- \(bu
Unicode::UCD has been upgraded from version 0.68 to 0.70.
.Sp
The function \f(CW\*(C`num\*(C' now accepts an optional parameter to help in
diagnosing error returns.

- \(bu
User::grent has been upgraded from version 1.01 to 1.02.

- \(bu
User::pwent has been upgraded from version 1.00 to 1.01.

- \(bu
utf8 has been upgraded from version 1.19 to 1.21.

- \(bu
vars has been upgraded from version 1.03 to 1.04.

- \(bu
version has been upgraded from version 0.9917 to 0.9923.

- \(bu
VMS::DCLsym has been upgraded from version 1.08 to 1.09.

- \(bu
VMS::Stdio has been upgraded from version 2.41 to 2.44.

- \(bu
warnings has been upgraded from version 1.37 to 1.42.
.Sp
It now includes new functions with names ending in \f(CW\*(C`_at_level\*(C', allowing
callers to specify the exact call frame.
[\s-1GH\s0 #16257] <https://github.com/Perl/perl5/issues/16257>

- \(bu
XS::Typemap has been upgraded from version 0.15 to 0.16.

- \(bu
XSLoader has been upgraded from version 0.27 to 0.30.
.Sp
Its documentation now shows the use of \f(CW\*(C`_\|_PACKAGE_\|_\*(C', and direct object
syntax for example \f(CW\*(C`DynaLoader\*(C' usage
[\s-1GH\s0 #16190] <https://github.com/Perl/perl5/issues/16190>.
.Sp
Platforms that use \f(CW\*(C`mod2fname\*(C' to edit the names of loadable
libraries now look for bootstrap (.bs) files under the correct,
non-edited name.

### Removed Modules and Pragmata

Subsection "Removed Modules and Pragmata"

- \(bu
The \f(CW\*(C`VMS::stdio\*(C' compatibility shim has been removed.

## Documentation

Header "Documentation"

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
We have attempted to update the documentation to reflect the changes
listed in this document.  If you find any we have missed, send email
to perlbug@perl.org <mailto:perlbug@perl.org>.

Additionally, the following selected changes have been made:

*perlapi*
Subsection "perlapi"

- \(bu
The \s-1API\s0 functions \f(CW\*(C`perl_parse()\*(C', \f(CW\*(C`perl_run()\*(C', and \f(CW\*(C`perl_destruct()\*(C'
are now documented comprehensively, where previously the only
documentation was a reference to the perlembed tutorial.

- \(bu
The documentation of \f(CW\*(C`newGIVENOP()\*(C' has been belatedly updated to
account for the removal of lexical \f(CW$_.

- \(bu
The \s-1API\s0 functions \f(CW\*(C`newCONSTSUB()\*(C' and \f(CW\*(C`newCONSTSUB_flags()\*(C' are
documented much more comprehensively than before.

*perldata*
Subsection "perldata"

- \(bu
The section \*(L"Truth and Falsehood\*(R" in perlsyn has been moved into
perldata.

*perldebguts*
Subsection "perldebguts"

- \(bu
The description of the conditions under which \f(CW\*(C`DB::sub()\*(C' will be called
has been clarified.
[\s-1GH\s0 #16055] <https://github.com/Perl/perl5/issues/16055>

*perldiag*
Subsection "perldiag"

- \(bu
\*(L"Variable length lookbehind not implemented in regex m/%s/\*(R" in perldiag
.Sp
This now gives more ideas as to workarounds to the issue that was
introduced in Perl 5.18 (but not documented explicitly in its perldelta)
for the fact that some Unicode \f(CW\*(C`/i\*(C' rules cause a few sequences such as
.Sp
.Vb 1
 (?<!st)
.Ve
.Sp
to be considered variable length, and hence disallowed.

- \(bu
\*(L"Use of state \f(CW$_ is experimental\*(R" in perldiag
.Sp
This entry has been removed, as the experimental support of this construct was
removed in perl 5.24.0.

- \(bu
The diagnostic \f(CW\*(C`Initialization of state variables in list context
currently forbidden\*(C' has changed to \f(CW\*(C`Initialization of state variables
in list currently forbidden\*(C', because list-context initialization of
single aggregate state variables is now permitted.

*perlembed*
Subsection "perlembed"

- \(bu
The examples in perlembed have been made more portable in the way
they exit, and the example that gets an exit code from the embedded Perl
interpreter now gets it from the right place.  The examples that pass
a constructed argv to Perl now show the mandatory null \f(CW\*(C`argv[argc]\*(C'.

- \(bu
An example in perlembed used the string value of \f(CW\*(C`ERRSV\*(C' as a
format string when calling **croak()**.  If that string contains format
codes such as \f(CW%s this could crash the program.
.Sp
This has been changed to a call to **croak_sv()**.
.Sp
An alternative could have been to supply a trivial format string:
.Sp
.Vb 1
  croak("%s", SvPV_nolen(ERRSV));
.Ve
.Sp
or as a special case for \f(CW\*(C`ERRSV\*(C' simply:
.Sp
.Vb 1
  croak(NULL);
.Ve

*perlfunc*
Subsection "perlfunc"

- \(bu
There is now a note that warnings generated by built-in functions are
documented in perldiag and warnings.
[\s-1GH\s0 #12642] <https://github.com/Perl/perl5/issues/12642>

- \(bu
The documentation for the \f(CW\*(C`exists\*(C' operator no longer says that
autovivification behaviour \*(L"may be fixed in a future release\*(R".
We've determined that we're not going to change the default behaviour.
[\s-1GH\s0 #15231] <https://github.com/Perl/perl5/issues/15231>

- \(bu
A couple of small details in the documentation for the \f(CW\*(C`bless\*(C' operator
have been clarified.
[\s-1GH\s0 #14684] <https://github.com/Perl/perl5/issues/14684>

- \(bu
The description of \f(CW@INC hooks in the documentation for \f(CW\*(C`require\*(C'
has been corrected to say that filter subroutines receive a useless
first argument.
[\s-1GH\s0 #12569] <https://github.com/Perl/perl5/issues/12569>

- \(bu
The documentation of \f(CW\*(C`ref\*(C' has been rewritten for clarity.

- \(bu
The documentation of \f(CW\*(C`use\*(C' now explains what syntactically qualifies
as a version number for its module version checking feature.

- \(bu
The documentation of \f(CW\*(C`warn\*(C' has been updated to reflect that since Perl
5.14 it has treated complex exception objects in a manner equivalent
to \f(CW\*(C`die\*(C'.
[\s-1GH\s0 #13641] <https://github.com/Perl/perl5/issues/13641>

- \(bu
The documentation of \f(CW\*(C`die\*(C' and \f(CW\*(C`warn\*(C' has been revised for clarity.

- \(bu
The documentation of \f(CW\*(C`each\*(C' has been improved, with a slightly more
explicit description of the sharing of iterator state, and with
caveats regarding the fragility of while-each loops.
[\s-1GH\s0 #16334] <https://github.com/Perl/perl5/issues/16334>

- \(bu
Clarification to \f(CW\*(C`require\*(C' was added to explain the differences between
.Sp
.Vb 2
    require Foo::Bar;
    require "Foo/Bar.pm";
.Ve

*perlgit*
Subsection "perlgit"

- \(bu
The precise rules for identifying \f(CW\*(C`smoke-me\*(C' branches are now stated.

*perlguts*
Subsection "perlguts"

- \(bu
The section on reference counting in perlguts has been heavily revised,
to describe references in the way a programmer needs to think about them
rather than in terms of the physical data structures.

- \(bu
Improve documentation related to \s-1UTF-8\s0 multibytes.

*perlintern*
Subsection "perlintern"

- \(bu
The internal functions \f(CW\*(C`newXS_len_flags()\*(C' and \f(CW\*(C`newATTRSUB_x()\*(C' are
now documented.

*perlobj*
Subsection "perlobj"

- \(bu
The documentation about \f(CW\*(C`DESTROY\*(C' methods has been corrected, updated,
and revised, especially in regard to how they interact with exceptions.
[\s-1GH\s0 #14083] <https://github.com/Perl/perl5/issues/14083>

*perlop*
Subsection "perlop"

- \(bu
The description of the \f(CW\*(C`x\*(C' operator in perlop has been clarified.
[\s-1GH\s0 #16253] <https://github.com/Perl/perl5/issues/16253>

- \(bu
perlop has been updated to note that \f(CW\*(C`qw\*(C''s whitespace rules differ
from that of \f(CW\*(C`split\*(C''s in that only \s-1ASCII\s0 whitespace is used.

- \(bu
The general explanation of operator precedence and associativity has
been corrected and clarified.
[\s-1GH\s0 #15153] <https://github.com/Perl/perl5/issues/15153>

- \(bu
The documentation for the \f(CW\*(C`\\\*(C' referencing operator now explains the
unusual context that it supplies to its operand.
[\s-1GH\s0 #15932] <https://github.com/Perl/perl5/issues/15932>

*perlrequick*
Subsection "perlrequick"

- \(bu
Clarifications on metacharacters and character classes

*perlretut*
Subsection "perlretut"

- \(bu
Clarify metacharacters.

*perlrun*
Subsection "perlrun"

- \(bu
Clarify the differences between **-M** and **-m**.
[\s-1GH\s0 #15998] <https://github.com/Perl/perl5/issues/15998>

*perlsec*
Subsection "perlsec"

- \(bu
The documentation about set-id scripts has been updated and revised.
[\s-1GH\s0 #10289] <https://github.com/Perl/perl5/issues/10289>

- \(bu
A section about using \f(CW\*(C`sudo\*(C' to run Perl scripts has been added.

*perlsyn*
Subsection "perlsyn"

- \(bu
The section \*(L"Truth and Falsehood\*(R" in perlsyn has been removed from
that document, where it didn't belong, and merged into the existing
paragraph on the same topic in perldata.

- \(bu
The means to disambiguate between code blocks and hash constructors,
already documented in perlref, are now documented in perlsyn too.
[\s-1GH\s0 #15918] <https://github.com/Perl/perl5/issues/15918>

*perluniprops*
Subsection "perluniprops"

- \(bu
perluniprops has been updated to note that \f(CW\*(C`\\p\{Word\}\*(C' now includes
code points matching the \f(CW\*(C`\\p\{Join_Control\}\*(C' property.  The change to
the property was made in Perl 5.18, but not documented until now.  There
are currently only two code points that match this property U+200C (\s-1ZERO
WIDTH\s0 NON-JOINER) and U+200D (\s-1ZERO WIDTH JOINER\s0).

- \(bu
For each binary table or property, the documentation now includes which
characters in the range \f(CW\*(C`\\x00-\\xFF\*(C' it matches, as well as a list of
the first few ranges of code points matched above that.

*perlvar*
Subsection "perlvar"

- \(bu
The entry for \f(CW$+ in perlvar has been expanded upon to describe handling of
multiply-named capturing groups.

*perlfunc, perlop, perlsyn*
Subsection "perlfunc, perlop, perlsyn"

- \(bu
In various places, improve the documentation of the special cases
in the condition expression of a while loop, such as implicit \f(CW\*(C`defined\*(C'
and assignment to \f(CW$_.
[\s-1GH\s0 #16334] <https://github.com/Perl/perl5/issues/16334>

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### New Diagnostics

Subsection "New Diagnostics"
*New Errors*
Subsection "New Errors"

- \(bu
Can't \*(L"goto\*(R" into a \*(L"given\*(R" block
.Sp
(F) A \*(L"goto\*(R" statement was executed to jump into the middle of a \f(CW\*(C`given\*(C'
block.  You can't get there from here.  See \*(L"goto\*(R" in perlfunc.

- \(bu
Can't \*(L"goto\*(R" into a binary or list expression
.Sp
Use of \f(CW\*(C`goto\*(C' to jump into the parameter of a binary or list operator has
been prohibited, to prevent crashes and stack corruption.
[\s-1GH\s0 #15914] <https://github.com/Perl/perl5/issues/15914>
.Sp
You may only enter the *first* argument of an operator that takes a fixed
number of arguments, since this is a case that will not cause stack
corruption.
[\s-1GH\s0 #16415] <https://github.com/Perl/perl5/issues/16415>

*New Warnings*
Subsection "New Warnings"

- \(bu
Old package separator used in string
.Sp
(W syntax) You used the old package separator, \*(L"'\*(R", in a variable
named inside a double-quoted string; e.g., \f(CW"In $name\*(Aqs house".  This
is equivalent to \f(CW"In $name::s house".  If you meant the former, put
a backslash before the apostrophe (\f(CW"In $name\\\*(Aqs house").

- \(bu
\*(L"Locale '%s' contains (at least) the following characters which
have unexpected meanings: \f(CW%s  The Perl program will use the expected
meanings\*(R" in perldiag

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
A false-positive warning that was issued when using a
numerically-quantified sub-pattern in a recursive regex has been
silenced. [\s-1GH\s0 #16106] <https://github.com/Perl/perl5/issues/16106>

- \(bu
The warning about useless use of a concatenation operator in void context
is now generated for expressions with multiple concatenations, such as
\f(CW\*(C`$a.$b.$c\*(C', which used to mistakenly not warn.
[\s-1GH\s0 #3990] <https://github.com/Perl/perl5/issues/3990>

- \(bu
Warnings that a variable or subroutine \*(L"masks earlier declaration in same
...\*(R", or that an \f(CW\*(C`our\*(C' variable has been redeclared, have been moved to a
new warnings category \*(L"shadow\*(R".  Previously they were in category \*(L"misc\*(R".

- \(bu
The deprecation warning from \f(CW\*(C`Sys::Hostname::hostname()\*(C' saying that
it doesn't accept arguments now states the Perl version in which the
warning will be upgraded to an error.
[\s-1GH\s0 #14662] <https://github.com/Perl/perl5/issues/14662>

- \(bu
The perldiag entry for the error regarding a set-id script has been
expanded to make clear that the error is reporting a specific security
vulnerability, and to advise how to fix it.

- \(bu
The \f(CW\*(C`Unable to flush stdout\*(C' error message was missing a trailing
newline. [debian #875361]

## Utility Changes

Header "Utility Changes"

### perlbug

Subsection "perlbug"

- \(bu
\f(CW\*(C`--help\*(C' and \f(CW\*(C`--version\*(C' options have been added.

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
C89 requirement
.Sp
Perl has been documented as requiring a C89 compiler to build since October
1998.  A variety of simplifications have now been made to Perl's internals to
rely on the features specified by the C89 standard. We believe that this
internal change hasn't altered the set of platforms that Perl builds on, but
please report a bug if Perl now has new problems building on your platform.

- \(bu
On \s-1GCC,\s0 \f(CW\*(C`-Werror=pointer-arith\*(C' is now enabled by default,
disallowing arithmetic on void and function pointers.

- \(bu
Where an \s-1HTML\s0 version of the documentation is installed, the \s-1HTML\s0
documents now use relative links to refer to each other.  Links from
the index page of perlipc to the individual section documents are
now correct.
[\s-1GH\s0 #11941] <https://github.com/Perl/perl5/issues/11941>

- \(bu
*lib/unicore/mktables* now correctly canonicalizes the names of the
dependencies stored in the files it generates.
.Sp
*regen/mk_invlists.pl*, unlike the other *regen/*.pl* scripts, used
\f(CW$0 to name itself in the dependencies stored in the files it
generates.  It now uses a literal so that the path stored in the
generated files doesn't depend on how *regen/mk_invlists.pl* is
invoked.
.Sp
This lack of canonical names could cause test failures in *t/porting/regen.t*.
[\s-1GH\s0 #16446] <https://github.com/Perl/perl5/issues/16446>

- \(bu
New probes

> 
- \s-1HAS_BUILTIN_ADD_OVERFLOW\s0
Item "HAS_BUILTIN_ADD_OVERFLOW"
0

- \s-1HAS_BUILTIN_MUL_OVERFLOW\s0
Item "HAS_BUILTIN_MUL_OVERFLOW"

- \s-1HAS_BUILTIN_SUB_OVERFLOW\s0
Item "HAS_BUILTIN_SUB_OVERFLOW"

- \s-1HAS_THREAD_SAFE_NL_LANGINFO_L\s0
Item "HAS_THREAD_SAFE_NL_LANGINFO_L"

- \s-1HAS_LOCALECONV_L\s0
Item "HAS_LOCALECONV_L"

- \s-1HAS_MBRLEN\s0
Item "HAS_MBRLEN"

- \s-1HAS_MBRTOWC\s0
Item "HAS_MBRTOWC"

- \s-1HAS_MEMRCHR\s0
Item "HAS_MEMRCHR"

- \s-1HAS_NANOSLEEP\s0
Item "HAS_NANOSLEEP"

- \s-1HAS_STRNLEN\s0
Item "HAS_STRNLEN"

- \s-1HAS_STRTOLD_L\s0
Item "HAS_STRTOLD_L"

- I_WCHAR
Item "I_WCHAR"



> 

.PD

## Testing

Header "Testing"

- \(bu
Testing of the XS-APItest directory is now done in parallel, where
applicable.

- \(bu
Perl now includes a default *.travis.yml* file for Travis \s-1CI\s0 testing
on github mirrors.
[\s-1GH\s0 #14558] <https://github.com/Perl/perl5/issues/14558>

- \(bu
The watchdog timer count in *re/pat_psycho.t* can now be overridden.
.Sp
This test can take a long time to run, so there is a timer to keep
this in check (currently, 5 minutes). This commit adds checking
the environment variable \f(CW\*(C`PERL_TEST_TIME_OUT_FACTOR\*(C'; if set,
the time out setting is multiplied by its value.

- \(bu
*harness* no longer waits for 30 seconds when running *t/io/openpid.t*.
[\s-1GH\s0 #13535] <https://github.com/Perl/perl5/issues/13535>
[\s-1GH\s0 #16420] <https://github.com/Perl/perl5/issues/16420>

## Packaging

Header "Packaging"
For the past few years we have released perl using three different archive
formats: bzip (\f(CW\*(C`.bz2\*(C'), \s-1LZMA2\s0 (\f(CW\*(C`.xz\*(C') and gzip (\f(CW\*(C`.gz\*(C'). Since xz compresses
better and decompresses faster, and gzip is more compatible and uses less
memory, we have dropped the \f(CW\*(C`.bz2\*(C' archive format with this release.
(If this poses a problem, do let us know; see \*(L"Reporting Bugs\*(R", below.)

## Platform Support

Header "Platform Support"

### Discontinued Platforms

Subsection "Discontinued Platforms"

- PowerUX / Power \s-1MAX OS\s0
Item "PowerUX / Power MAX OS"
Compiler hints and other support for these apparently long-defunct
platforms has been removed.

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- CentOS
Item "CentOS"
Compilation on CentOS 5 is now fixed.

- Cygwin
Item "Cygwin"
A build with the quadmath library can now be done on Cygwin.

- Darwin
Item "Darwin"
Perl now correctly uses reentrant functions, like \f(CW\*(C`asctime_r\*(C', on
versions of Darwin that have support for them.

- FreeBSD
Item "FreeBSD"
FreeBSD's */usr/share/mk/sys.mk* specifies \f(CW\*(C`-O2\*(C' for
architectures other than \s-1ARM\s0 and \s-1MIPS.\s0 By default, perl is now compiled
with the same optimization levels.

- \s-1VMS\s0
Item "VMS"
Several fix-ups for *configure.com*, marking function \s-1VMS\s0 has
(or doesn't have).
.Sp
\s-1CRTL\s0 features can now be set by embedders before invoking Perl by using
the \f(CW\*(C`decc$feature_set\*(C' and \f(CW\*(C`decc$feature_set_value\*(C' functions.
Previously any attempt to set features after image initialization were
ignored.

- Windows
Item "Windows"

> 0

- \(bu
.PD
Support for compiling perl on Windows using Microsoft Visual Studio 2017
(containing Visual \*(C+ 14.1) has been added.

- \(bu
Visual \*(C+ compiler version detection has been improved to work on non-English
language systems.

- \(bu
We now set \f(CW$Config\{libpth\} correctly for 64-bit builds using Visual \*(C+
versions earlier than 14.1.



> 


## Internal Changes

Header "Internal Changes"

- \(bu
A new optimisation phase has been added to the compiler,
\f(CW\*(C`optimize_optree()\*(C', which does a top-down scan of a complete optree
just before the peephole optimiser is run. This phase is not currently
hookable.

- \(bu
An \f(CW\*(C`OP_MULTICONCAT\*(C' op has been added. At \f(CW\*(C`optimize_optree()\*(C' time, a
chain of \f(CW\*(C`OP_CONCAT\*(C' and \f(CW\*(C`OP_CONST\*(C' ops, together optionally with an
\f(CW\*(C`OP_STRINGIFY\*(C' and/or \f(CW\*(C`OP_SASSIGN\*(C', are combined into a single
\f(CW\*(C`OP_MULTICONCAT\*(C' op. The op is of type \f(CW\*(C`UNOP_AUX\*(C', and the aux array
contains the argument count, plus a pointer to a constant string and a set
of segment lengths. For example with
.Sp
.Vb 1
    my $x = "foo=$foo, bar=$bar\\n";
.Ve
.Sp
the constant string would be \f(CW"foo=, bar=\\n" and the segment lengths
would be (4,6,1). If the string contains characters such as \f(CW\*(C`\\x80\*(C', whose
representation changes under utf8, two sets of strings plus lengths are
precomputed and stored.

- \(bu
Direct access to \f(CW\*(C`PL_keyword_plugin\*(C' is not
safe in the presence of multithreading. A new
\f(CW\*(C`wrap_keyword_plugin\*(C' function has been
added to allow \s-1XS\s0 modules to safely define custom keywords even when
loaded from a thread, analogous to \f(CW\*(C`PL_check\*(C' /
\f(CW\*(C`wrap_op_checker\*(C'.

- \(bu
The \f(CW\*(C`PL_statbuf\*(C' interpreter variable has been removed.

- \(bu
The deprecated function \f(CW\*(C`to_utf8_case()\*(C', accessible from \s-1XS\s0 code, has
been removed.

- \(bu
A new function
\f(CW\*(C`is_utf8_invariant_string_loc()\*(C'
has been added that is like
\f(CW\*(C`is_utf8_invariant_string()\*(C'
but takes an extra pointer parameter into which is stored the location
of the first variant character, if any are found.

- \(bu
A new function, \f(CW\*(C`Perl_langinfo()\*(C' has been
added.  It is an (almost) drop-in replacement for the system
\f(CWnl_langinfo(3), but works on platforms that lack that; as well as
being more thread-safe, and hiding some gotchas with locale handling
from the caller.  Code that uses this, needn't use \f(CWlocaleconv(3)
(and be affected by the gotchas) to find the decimal point, thousands
separator, or currency symbol.  See \*(L"Perl_langinfo\*(R" in perlapi.

- \(bu
A new \s-1API\s0 function \f(CW\*(C`sv_rvunweaken()\*(C' has
been added to complement \f(CW\*(C`sv_rvweaken()\*(C'.
The implementation was taken from \*(L"unweaken\*(R" in Scalar::Util.

- \(bu
A new flag, \f(CW\*(C`SORTf_UNSTABLE\*(C', has been added. This will allow a
future commit to make mergesort unstable when the user specifies Xno
sort stableX, since it has been decided that mergesort should remain
stable by default.

- \(bu
\s-1XS\s0 modules can now automatically get reentrant versions of system
functions on threaded perls.
.Sp
By adding
.Sp
.Vb 1
    #define PERL_REENTRANT
.Ve
.Sp
near the beginning of an \f(CW\*(C`XS\*(C' file, it will be compiled so that
whatever reentrant functions perl knows about on that system will
automatically and invisibly be used instead of the plain, non-reentrant
versions.  For example, if you write \f(CW\*(C`getpwnam()\*(C' in your code, on a
system that has \f(CW\*(C`getpwnam_r()\*(C' all calls to the former will be translated
invisibly into the latter.  This does not happen except on threaded
perls, as they aren't needed otherwise.  Be aware that which functions
have reentrant versions varies from system to system.

- \(bu
The \f(CW\*(C`PERL_NO_OP_PARENT\*(C' build define is no longer supported, which means
that perl is now always built with \f(CW\*(C`PERL_OP_PARENT\*(C' enabled.

- \(bu
The format and content of the non-utf8 transliteration table attached to
the \f(CW\*(C`op_pv\*(C' field of \f(CW\*(C`OP_TRANS\*(C'/\f(CW\*(C`OP_TRANSR\*(C' ops has changed. It's now a
\f(CW\*(C`struct OPtrans_map\*(C'.

- \(bu
A new compiler \f(CW\*(C`#define\*(C', \f(CW\*(C`dTHX_DEBUGGING\*(C'. has been added.  This is
useful for \s-1XS\s0 or C code that only need the thread context because their
debugging statements that get compiled only under \f(CW\*(C`-DDEBUGGING\*(C' need
one.

- \(bu
A new \s-1API\s0 function \*(L"Perl_setlocale\*(R" in perlapi has been added.

- \(bu
\*(L"sync_locale\*(R" in perlapi has been revised to return a boolean as to
whether the system was using the global locale or not.

- \(bu
A new kind of magic scalar, called a \*(L"nonelem\*(R" scalar, has been introduced.
It is stored in an array to denote a non-existent element, whenever such an
element is accessed in a potential lvalue context.  It replaces the
existing \*(L"defelem\*(R" (deferred element) magic wherever this is possible,
being significantly more efficient.  This means that
\f(CW\*(C`some_sub($sparse_array[$nonelem])\*(C' no longer has to create a new magic
defelem scalar each time, as long as the element is within the array.
.Sp
It partially fixes the rare bug of deferred elements getting out of synch
with their arrays when the array is shifted or unshifted.
[\s-1GH\s0 #16364] <https://github.com/Perl/perl5/issues/16364>

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
List assignment (\f(CW\*(C`aassign\*(C') could in some rare cases allocate an
entry on the mortals stack and leave the entry uninitialized, leading to
possible crashes.
[\s-1GH\s0 #16017] <https://github.com/Perl/perl5/issues/16017>

- \(bu
Attempting to apply an attribute to an \f(CW\*(C`our\*(C' variable where a
function of that name already exists could result in a \s-1NULL\s0 pointer
being supplied where an \s-1SV\s0 was expected, crashing perl.
[perl #131597] <https://rt.perl.org/Ticket/Display.html?id=131597>

- \(bu
\f(CW\*(C`split \*(Aq \*(Aq\*(C' now correctly handles the argument being split when in the
scope of the \f(CW\*(C`unicode_strings\*(C' feature. Previously, when a string using the single-byte internal
representation contained characters that are whitespace by Unicode rules but
not by \s-1ASCII\s0 rules, it treated those characters as part of fields rather
than as field separators.
[\s-1GH\s0 #15904] <https://github.com/Perl/perl5/issues/15904>

- \(bu
Several built-in functions previously had bugs that could cause them to
write to the internal stack without allocating room for the item being
written. In rare situations, this could have led to a crash. These bugs have
now been fixed, and if any similar bugs are introduced in future, they will
be detected automatically in debugging builds.
.Sp
These internal stack usage checks introduced are also done
by the \f(CW\*(C`entersub\*(C' operator when calling XSUBs.  This means we can
report which \s-1XSUB\s0 failed to allocate enough stack space.
[\s-1GH\s0 #16126] <https://github.com/Perl/perl5/issues/16126>

- \(bu
Using a symbolic ref with postderef syntax as the key in a hash lookup was
yielding an assertion failure on debugging builds.
[\s-1GH\s0 #16029] <https://github.com/Perl/perl5/issues/16029>

- \(bu
Array and hash variables whose names begin with a caret now admit indexing
inside their curlies when interpolated into strings, as in \f(CW"$\{^CAPTURE[0]\}" to index \f(CW\*(C`@\{^CAPTURE\}\*(C'.
[\s-1GH\s0 #16050] <https://github.com/Perl/perl5/issues/16050>

- \(bu
Fetching the name of a glob that was previously \s-1UTF-8\s0 but wasn't any
longer would return that name flagged as \s-1UTF-8.\s0
[\s-1GH\s0 #15971] <https://github.com/Perl/perl5/issues/15971>

- \(bu
The perl \f(CW\*(C`sprintf()\*(C' function (via the underlying C function
\f(CW\*(C`Perl_sv_vcatpvfn_flags()\*(C') has been heavily reworked to fix many minor
bugs, including the integer wrapping of large width and precision
specifiers and potential buffer overruns. It has also been made faster in
many cases.

- \(bu
Exiting from an \f(CW\*(C`eval\*(C', whether normally or via an exception, now always
frees temporary values (possibly calling destructors) *before* setting
\f(CW$@. For example:
.Sp
.Vb 3
    sub DESTROY \{ eval \{ die "died in DESTROY"; \} \}
    eval \{ bless []; \};
    # $@ used to be equal to "died in DESTROY" here; it\*(Aqs now "".
.Ve

- \(bu
Fixed a duplicate symbol failure with \f(CW\*(C`-flto -mieee-fp\*(C' builds.
*pp.c* defined \f(CW\*(C`_LIB_VERSION\*(C' which \f(CW\*(C`-lieee\*(C' already defines.
[\s-1GH\s0 #16086] <https://github.com/Perl/perl5/issues/16086>

- \(bu
The tokenizer no longer consumes the exponent part of a floating
point number if it's incomplete.
[\s-1GH\s0 #16073] <https://github.com/Perl/perl5/issues/16073>

- \(bu
On non-threaded builds, for \f(CW\*(C`m/$null/\*(C' where \f(CW$null is an empty
string is no longer treated as if the \f(CW\*(C`/o\*(C' flag was present when the
previous matching match operator included the \f(CW\*(C`/o\*(C' flag.  The
rewriting used to implement this behavior could confuse the
interpreter.  This matches the behaviour of threaded builds.
[\s-1GH\s0 #14668] <https://github.com/Perl/perl5/issues/14668>

- \(bu
Parsing a \f(CW\*(C`sub\*(C' definition could cause a use after free if the \f(CW\*(C`sub\*(C'
keyword was followed by whitespace including newlines (and comments.)
[\s-1GH\s0 #16097] <https://github.com/Perl/perl5/issues/16097>

- \(bu
The tokenizer now correctly adjusts a parse pointer when skipping
whitespace in a \f(CW\*(C`$\{identifier\}\*(C' construct.
[perl #131949] <https://rt.perl.org/Public/Bug/Display.html?id=131949>

- \(bu
Accesses to \f(CW\*(C`$\{^LAST_FH\}\*(C' no longer assert after using any of a
variety of I/O operations on a non-glob.
[\s-1GH\s0 #15372] <https://github.com/Perl/perl5/issues/15372>

- \(bu
The XS-level \f(CW\*(C`Copy()\*(C', \f(CW\*(C`Move()\*(C', \f(CW\*(C`Zero()\*(C' macros and their variants now
assert if the pointers supplied are \f(CW\*(C`NULL\*(C'.  \s-1ISO C\s0 considers
supplying \s-1NULL\s0 pointers to the functions these macros are built upon
as undefined behaviour even when their count parameters are zero.
Based on these assertions and the original bug report three macro
calls were made conditional.
[\s-1GH\s0 #16079] <https://github.com/Perl/perl5/issues/16079>
[\s-1GH\s0 #16112] <https://github.com/Perl/perl5/issues/16112>

- \(bu
Only the \f(CW\*(C`=\*(C' operator is permitted for defining defaults for
parameters in subroutine signatures.  Previously other assignment
operators, e.g. \f(CW\*(C`+=\*(C', were also accidentally permitted.
[\s-1GH\s0 #16084] <https://github.com/Perl/perl5/issues/16084>

- \(bu
Package names are now always included in \f(CW\*(C`:prototype\*(C' warnings
[perl #131833] <https://rt.perl.org/Public/Bug/Display.html?id=131833>

- \(bu
The \f(CW\*(C`je_old_stack_hwm\*(C' field, previously only found in the \f(CW\*(C`jmpenv\*(C'
structure on debugging builds, has been added to non-debug builds as
well. This fixes an issue with some \s-1CPAN\s0 modules caused by the size of
this structure varying between debugging and non-debugging builds.
[\s-1GH\s0 #16122] <https://github.com/Perl/perl5/issues/16122>

- \(bu
The arguments to the \f(CW\*(C`ninstr()\*(C' macro are now correctly parenthesized.

- \(bu
A \s-1NULL\s0 pointer dereference in the \f(CW\*(C`S_regmatch()\*(C' function has been
fixed.
[perl #132017] <https://rt.perl.org/Public/Bug/Display.html?id=132017>

- \(bu
Calling exec \s-1PROGRAM LIST\s0 with an empty \f(CW\*(C`LIST\*(C'
has been fixed.  This should call \f(CW\*(C`execvp()\*(C' with an empty \f(CW\*(C`argv\*(C' array
(containing only the terminating \f(CW\*(C`NULL\*(C' pointer), but was instead just
returning false (and not setting \f(CW$!).
[\s-1GH\s0 #16075] <https://github.com/Perl/perl5/issues/16075>

- \(bu
The \f(CW\*(C`gv_fetchmeth_sv\*(C' C function stopped working properly in Perl 5.22 when
fetching a constant with a \s-1UTF-8\s0 name if that constant subroutine was stored in
the stash as a simple scalar reference, rather than a full typeglob.  This has
been corrected.

- \(bu
Single-letter debugger commands followed by an argument which starts with
punctuation  (e.g. \f(CW\*(C`p$^V\*(C' and \f(CW\*(C`x@ARGV\*(C') now work again.  They had been
wrongly requiring a space between the command and the argument.
[\s-1GH\s0 #13342] <https://github.com/Perl/perl5/issues/13342>

- \(bu
splice now throws an exception
(\*(L"Modification of a read-only value attempted\*(R") when modifying a read-only
array.  Until now it had been silently modifying the array.  The new behaviour
is consistent with the behaviour of push and
unshift.
[\s-1GH\s0 #15923] <https://github.com/Perl/perl5/issues/15923>

- \(bu
\f(CW\*(C`stat()\*(C', \f(CW\*(C`lstat()\*(C', and file test operators now fail if given a
filename containing a nul character, in the same way that \f(CW\*(C`open()\*(C'
already fails.

- \(bu
\f(CW\*(C`stat()\*(C', \f(CW\*(C`lstat()\*(C', and file test operators now reliably set \f(CW$! when
failing due to being applied to a closed or otherwise invalid file handle.

- \(bu
File test operators for Unix permission bits that don't exist on a
particular platform, such as \f(CW\*(C`-k\*(C' (sticky bit) on Windows, now check that
the file being tested exists before returning the blanket false result,
and yield the appropriate errors if the argument doesn't refer to a file.

- \(bu
Fixed a 'read before buffer' overrun when parsing a range starting with
\f(CW\*(C`\\N\{\}\*(C' at the beginning of the character set for the transliteration
operator.
[\s-1GH\s0 #16189] <https://github.com/Perl/perl5/issues/16189>

- \(bu
Fixed a leaked scalar when parsing an empty \f(CW\*(C`\\N\{\}\*(C' at compile-time.
[\s-1GH\s0 #16189] <https://github.com/Perl/perl5/issues/16189>

- \(bu
Calling \f(CW\*(C`do $path\*(C' on a directory or block device now yields a meaningful
error code in \f(CW$!.
[\s-1GH\s0 #14841] <https://github.com/Perl/perl5/issues/14841>

- \(bu
Regexp substitution using an overloaded replacement value that provides
a tainted stringification now correctly taints the resulting string.
[\s-1GH\s0 #12495] <https://github.com/Perl/perl5/issues/12495>

- \(bu
Lexical sub declarations in \f(CW\*(C`do\*(C' blocks such as \f(CW\*(C`do \{ my sub lex; 123 \}\*(C'
could corrupt the stack, erasing items already on the stack in the
enclosing statement.  This has been fixed.
[\s-1GH\s0 #16243] <https://github.com/Perl/perl5/issues/16243>

- \(bu
\f(CW\*(C`pack\*(C' and \f(CW\*(C`unpack\*(C' can now handle repeat counts and lengths that
exceed two billion.
[\s-1GH\s0 #13179] <https://github.com/Perl/perl5/issues/13179>

- \(bu
Digits past the radix point in octal and binary floating point literals
now have the correct weight on platforms where a floating point
significand doesn't fit into an integer type.

- \(bu
The canonical truth value no longer has a spurious special meaning as a
callable subroutine.  It used to be a magic placeholder for a missing
\f(CW\*(C`import\*(C' or \f(CW\*(C`unimport\*(C' method, but is now treated like any other string
\f(CW1.
[\s-1GH\s0 #14902] <https://github.com/Perl/perl5/issues/14902>

- \(bu
\f(CW\*(C`system\*(C' now reduces its arguments to strings in the parent process, so
any effects of stringifying them (such as overload methods being called
or warnings being emitted) are visible in the way the program expects.
[\s-1GH\s0 #13561] <https://github.com/Perl/perl5/issues/13561>

- \(bu
The \f(CW\*(C`readpipe()\*(C' built-in function now checks at compile time that
it has only one parameter expression, and puts it in scalar context,
thus ensuring that it doesn't corrupt the stack at runtime.
[\s-1GH\s0 #2793] <https://github.com/Perl/perl5/issues/2793>

- \(bu
\f(CW\*(C`sort\*(C' now performs correct reference counting when aliasing \f(CW$a and
\f(CW$b, thus avoiding premature destruction and leakage of scalars if they
are re-aliased during execution of the sort comparator.
[\s-1GH\s0 #11422] <https://github.com/Perl/perl5/issues/11422>

- \(bu
\f(CW\*(C`reverse\*(C' with no operand, reversing \f(CW$_ by default, is no longer in
danger of corrupting the stack.
[\s-1GH\s0 #16291] <https://github.com/Perl/perl5/issues/16291>

- \(bu
\f(CW\*(C`exec\*(C', \f(CW\*(C`system\*(C', et al are no longer liable to have their argument
lists corrupted by reentrant calls and by magic such as tied scalars.
[\s-1GH\s0 #15660] <https://github.com/Perl/perl5/issues/15660>

- \(bu
Perl's own \f(CW\*(C`malloc\*(C' no longer gets confused by attempts to allocate
more than a gigabyte on a 64-bit platform.
[\s-1GH\s0 #13273] <https://github.com/Perl/perl5/issues/13273>

- \(bu
Stacked file test operators in a sort comparator expression no longer
cause a crash.
[\s-1GH\s0 #15626] <https://github.com/Perl/perl5/issues/15626>

- \(bu
An identity \f(CW\*(C`tr///\*(C' transformation on a reference is no longer mistaken
for that reference for the purposes of deciding whether it can be
assigned to.
[\s-1GH\s0 #15812] <https://github.com/Perl/perl5/issues/15812>

- \(bu
Lengthy hexadecimal, octal, or binary floating point literals no
longer cause undefined behaviour when parsing digits that are of such
low significance that they can't affect the floating point value.
[\s-1GH\s0 #16114] <https://github.com/Perl/perl5/issues/16114>

- \(bu
\f(CW\*(C`open $$scalarref...\*(C' and similar invocations no longer leak the file
handle.
[\s-1GH\s0 #12593] <https://github.com/Perl/perl5/issues/12593>

- \(bu
Some convoluted kinds of regexp no longer cause an arithmetic overflow
when compiled.
[\s-1GH\s0 #16113] <https://github.com/Perl/perl5/issues/16113>

- \(bu
The default typemap, by avoiding \f(CW\*(C`newGVgen\*(C', now no longer leaks when
XSUBs return file handles (\f(CW\*(C`PerlIO *\*(C' or \f(CW\*(C`FILE *\*(C').
[\s-1GH\s0 #12593] <https://github.com/Perl/perl5/issues/12593>

- \(bu
Creating a \f(CW\*(C`BEGIN\*(C' block as an \s-1XS\s0 subroutine with a prototype no longer
crashes because of the early freeing of the subroutine.

- \(bu
The \f(CW\*(C`printf\*(C' format specifier \f(CW\*(C`%.0f\*(C' no longer rounds incorrectly
[\s-1GH\s0 #9125] <https://github.com/Perl/perl5/issues/9125>,
and now shows the correct sign for a negative zero.

- \(bu
Fixed an issue where the error \f(CW\*(C`Scalar value @arrayname[0] better
written as $arrayname\*(C' would give an error \f(CW\*(C`Cannot printf Inf with \*(Aqc\*(Aq\*(C'
when arrayname starts with \f(CW\*(C`Inf\*(C'.
[\s-1GH\s0 #16335] <https://github.com/Perl/perl5/issues/16335>

- \(bu
The Perl implementation of \f(CW\*(C`getcwd()\*(C' in \f(CW\*(C`Cwd\*(C' in the PathTools
distribution now behaves the same as \s-1XS\s0 implementation on errors: it
returns an error, and sets \f(CW$!.
[\s-1GH\s0 #16338] <https://github.com/Perl/perl5/issues/16338>

- \(bu
Vivify array elements when putting them on the stack.
Fixes [\s-1GH\s0 #5310] <https://github.com/Perl/perl5/issues/5310>
(reported in April 2002).

- \(bu
Fixed parsing of braced subscript after parens. Fixes
[\s-1GH\s0 #4688] <https://github.com/Perl/perl5/issues/4688>
(reported in December 2001).

- \(bu
\f(CW\*(C`tr/non_utf8/long_non_utf8/c\*(C' could give the wrong results when the
length of the replacement character list was greater than 0x7fff.

- \(bu
\f(CW\*(C`tr/non_utf8/non_utf8/cd\*(C' failed to add the implied
\f(CW\*(C`\\x\{100\}-\\x\{7fffffff\}\*(C' to the search character list.

- \(bu
Compilation failures within \*(L"perl-within-perl\*(R" constructs, such as with
string interpolation and the right part of \f(CW\*(C`s///e\*(C', now cause
compilation to abort earlier.
.Sp
Previously compilation could continue in order to report other errors,
but the failed sub-parse could leave partly parsed constructs on the
parser shift-reduce stack, confusing the parser, leading to perl
crashes.
[\s-1GH\s0 #14739] <https://github.com/Perl/perl5/issues/14739>

- \(bu
On threaded perls where the decimal point (radix) character is not a
dot, it has been possible for a race to occur between threads when one
needs to use the real radix character (such as with \f(CW\*(C`sprintf\*(C').  This has
now been fixed by use of a mutex on systems without thread-safe locales,
and the problem just doesn't come up on those with thread-safe locales.

- \(bu
Errors while compiling a regex character class could sometime trigger an
assertion failure.
[\s-1GH\s0 #16172] <https://github.com/Perl/perl5/issues/16172>

## Acknowledgements

Header "Acknowledgements"
Perl 5.28.0 represents approximately 13 months of development since Perl
5.26.0 and contains approximately 730,000 lines of changes across 2,200
files from 77 authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 580,000 lines of changes to 1,300 .pm, .t, .c and .h files.

Perl continues to flourish into its fourth decade thanks to a vibrant
community of users and developers. The following people are known to have
contributed the improvements that became Perl 5.28.0:

Aaron Crane, Abigail, \*(Aevar Arnfjo\*:r\*(d- Bjarmason, Alberto Simo\*~es, Alexandr
Savca, Andrew Fresh, Andy Dougherty, Andy Lester, Aristotle Pagaltzis, Ask
Bjo\*/rn Hansen, Chris 'BinGOs' Williams, Craig A. Berry, Dagfinn Ilmari
Mannsa\*oker, Dan Collins, Daniel Dragan, David Cantrell, David Mitchell,
Dmitry Ulanov, Dominic Hargreaves, E. Choroba, Eric Herman, Eugen Konkov,
Father Chrysostomos, Gene Sullivan, George Hartzell, Graham Knop, Harald
Jo\*:rg, H.Merijn Brand, Hugo van der Sanden, Jacques Germishuys, James E
Keenan, Jarkko Hietaniemi, Jerry D. Hedden, J. Nick Koston, John Lightsey,
John Peacock, John P. Linderman, John \s-1SJ\s0 Anderson, Karen Etheridge, Karl
Williamson, Ken Brown, Ken Cotterill, Leon Timmermans, Lukas Mai, Marco
Fontani, Marc-Philip Werner, Matthew Horsfall, Neil Bowers, Nicholas Clark,
Nicolas R., Niko Tyni, Pali, Paul Marquess, Peter John Acklam, Reini Urban,
Renee Baecker, Ricardo Signes, Robin Barker, Sawyer X, Scott Lanning, Sergey
Aleynikov, Shirakata Kentaro, Shoichi Kaji, Slaven Rezic, Smylers, Steffen
Mu\*:ller, Steve Hay, Sullivan Beck, Thomas Sibley, Todd Rinaldo, Tomasz
Konojacki, Tom Hukins, Tom Wyant, Tony Cook, Vitali Peil, Yves Orton,
Zefram.

The list above is almost certainly incomplete as it is automatically
generated from version control history. In particular, it does not include
the names of the (very much appreciated) contributors who reported issues to
the Perl bug tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core. We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please
see the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database
at <https://rt.perl.org/> .  There may also be information at
<http://www.perl.org/> , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5,
you can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
