+++
description = "This tool uses the Speech Synthesis manager to convert input text to audible speech and either play it through the sound output device chosen in System Preferences or save it to an s-1AIFFs0 file. Specify the text to speak on the command line. This..."
operating_system = "macos"
manpage_format = "troff"
operating_system_version = "15.3"
date = "2020-08-13"
detected_package_version = "1.0"
manpage_section = "1"
manpage_name = "say"
keywords = ["header", "see", "also", "l", "speech", "synthesis", "programming", "guide", "r"]
author = "None Specified"
title = "say(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "SAY 1"
SAY 1 "2020-08-13" "1.0" "Speech Synthesis Manager"

## NAME

say - Convert text to audible speech

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
    say [-v voice] [-r rate] [-o outfile [audio format options] | -n name:port | -a device] [-f file | string ...]
.Ve

## DESCRIPTION

Header "DESCRIPTION"
This tool uses the Speech Synthesis manager to convert input text to
audible speech and either play it through the sound output device
chosen in System Preferences or save it to an \s-1AIFF\s0 file.

## OPTIONS

Header "OPTIONS"

- \fIstring
Item "string"
Specify the text to speak on the command line. This can consist of multiple arguments,
which are considered to be separated by spaces.

- \fB-f \fIfile, \fB--input-file=\fIfile
Item "-f file, --input-file=file"
Specify a file to be spoken. If *file* is *-* or neither this parameter nor
a message is specified, read from standard input.

- \fB-v \fIvoice, \fB--voice=\fIvoice
Item "-v voice, --voice=voice"
Specify the voice to be used. Default is the voice selected in System
Preferences. To obtain a list of voices installed in the system, specify '?' as the voice name.

- \fB-r \fIrate, \fB--rate=\fIrate
Item "-r rate, --rate=rate"
Speech rate to be used, in words per minute.

- \fB-o \fIout.aiff, \fB--output-file=\fIfile
Item "-o out.aiff, --output-file=file"
Specify the path for an audio file to be written. \s-1AIFF\s0 is the default and should be
supported for most voices, but some voices support many more file formats.

- \fB-n \fIname, \fB--network-send=\fIname
Item "-n name, --network-send=name"
0

- \fB-n \fIname:port, \fB--network-send=\fIname:port
Item "-n name:port, --network-send=name:port"

- \fB-n \fI:port, \fB--network-send=\fI:port
Item "-n :port, --network-send=:port"

- \fB-n \fI:, \fB--network-send=\fI:
Item "-n :, --network-send=:"
.PD
Specify a service name (default \*(L"AUNetSend\*(R") and/or \s-1IP\s0 port to be used for redirecting
the speech output through AUNetSend.

- \fB-a \fI\s-1ID\s0, \fB--audio-device=\fI\s-1ID\s0
Item "-a ID, --audio-device=ID"
0

- \fB-a \fIname, \fB--audio-device=\fIname
Item "-a name, --audio-device=name"
.PD
Specify, by \s-1ID\s0 or name prefix, an audio device to be used to play the audio. To obtain a
list of audio output devices, specify '?' as the device name.

- \fB--progress
Item "--progress"
Display a progress meter during synthesis.

- \fB-i, \fB--interactive, \fB--interactive=\fImarkup
Item "-i, --interactive, --interactive=markup"
Print the text line by line during synthesis, highlighting words as they are spoken. Markup can
be one of

> 
- \(bu
A terminfo capability as described in **terminfo**\|(5), e.g. **bold**, **smul**, **setaf 1**.

- \(bu
A color name, one of **black**, **red**, **green**, **yellow**, **blue**, **magenta**, **cyan**, or **white**.

- \(bu
A foreground and background color from the above list, separated by a slash, e.g. **green/black**. If
the foreground color is omitted, only the background color is set.



> .Sp
If markup is not specified, it defaults to **smso**, i.e. reverse video.



If the input is a \s-1TTY,\s0 text is spoken line by line, and the output
file, if specified, will only contain audio for the last line of the input.
Otherwise, text is spoken all at once.

## AUDIO FORMATS

Header "AUDIO FORMATS"
Starting in MacOS X 10.6, file formats other than \s-1AIFF\s0 may be specified, although not all
third party synthesizers may initially support them. In simple cases, the file format can
be inferred from the extension, although generally some of the options below are required
for finer grained control:

- \fB--file-format=format
Item "--file-format=format"
The format of the file to write (\s-1AIFF,\s0 caff, m4af, \s-1WAVE\s0). Generally, it's easier to specify
a suitable file extension for the output file. To obtain a list of writable file formats,
specify '?' as the format name.

- \fB--data-format=format
Item "--data-format=format"
The format of the audio data to be stored. Formats other than linear \s-1PCM\s0 are specified by
giving their format identifiers (aac, alac). Linear \s-1PCM\s0 formats are specified as a sequence of:

> 
- Endianness (optional)
Item "Endianness (optional)"
One of **\s-1BE\s0** (big endian) or **\s-1LE\s0** (little endian). Default is native endianness.

- Data type
Item "Data type"
One of **F** (float), **I** (integer), or, rarely, **\s-1UI\s0** (unsigned integer).

- Sample size
Item "Sample size"
One of **8**, **16**, **24**, **32**, **64**.



> .Sp
Most available file formats only support a subset of these sample formats.
.Sp
To obtain a list of audio data formats for a file format specified explicitly or by file name, specify '?' as the format name.
.Sp
The format identifier optionally can be followed by **\f(CB@samplerate**** and \fB/hexflags** for the format.



- \fB--channels=channels
Item "--channels=channels"
The number of channels. This will generally be of limited use, as most speech synthesizers produce mono audio only.

- \fB--bit-rate=rate
Item "--bit-rate=rate"
The bit rate for formats like \s-1AAC.\s0 To obtain a list of valid bit rates, specify '?' as the rate. In practice, not all of these
bit rates will be available for a given format.

- \fB--quality=quality
Item "--quality=quality"
The audio converter quality level between 0 (lowest) and 127 (highest).

## ERRORS

Header "ERRORS"
**say** returns 0 if the text was spoken successfully, otherwise non-zero.
Diagnostic messages will be printed to standard error.

## EXAMPLES

Header "EXAMPLES"
.Vb 6
   say Hello, World
   say -v Alex -o hi -f hello_world.txt
   say --interactive=/green spending each day the color of the leaves
   say -o hi.aac \*(AqHello, [[slnc 200]] World\*(Aq
   say -o hi.m4a --data-format=alac Hello, World.
   say -o hi.caf --data-format=LEF32@8000 Hello, World

   say -v \*(Aq?\*(Aq
   say --file-format=?
   say --file-format=caff --data-format=?
   say -o hi.m4a --bit-rate=?
.Ve

## SEE ALSO

Header "SEE ALSO"
\*(L"Speech Synthesis Programming Guide\*(R"
