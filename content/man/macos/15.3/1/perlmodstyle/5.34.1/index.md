+++
date = "2022-02-19"
detected_package_version = "5.34.1"
operating_system = "macos"
manpage_format = "troff"
keywords = ["header", "see", "also", "perlstyle", "4", "item", "general", "perl", "style", "guide", "perlnewmod", "how", "to", "create", "a", "new", "module", "perlpod", "s-1pod", "s0", "documentation", "podchecker", "verifies", "your", "s", "correctness", "packaging", "tools", "extutils", "makemaker", "build", "testing", "test", "simple", "inline", "carp", "assert", "more", "mockobject", "https", "pause", "org", "authors", "upload", "server", "contains", "links", "information", "for", "any", "good", "book", "on", "software", "engineering", "author", "kirrily", "l", "skud", "r", "robert", "cpan"]
manpage_section = "1"
title = "perlmodstyle(1)"
description = "None Specified"
operating_system_version = "15.3"
author = "None Specified"
manpage_name = "perlmodstyle"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLMODSTYLE 1"
PERLMODSTYLE 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlmodstyle - Perl module style guide

## INTRODUCTION

Header "INTRODUCTION"
This document attempts to describe the Perl Community's \*(L"best practice\*(R"
for writing Perl modules.  It extends the recommendations found in
perlstyle , which should be considered required reading
before reading this document.

While this document is intended to be useful to all module authors, it is
particularly aimed at authors who wish to publish their modules on \s-1CPAN.\s0

The focus is on elements of style which are visible to the users of a
module, rather than those parts which are only seen by the module's
developers.  However, many of the guidelines presented in this document
can be extrapolated and applied successfully to a module's internals.

This document differs from perlnewmod in that it is a style guide
rather than a tutorial on creating \s-1CPAN\s0 modules.  It provides a
checklist against which modules can be compared to determine whether
they conform to best practice, without necessarily describing in detail
how to achieve this.

All the advice contained in this document has been gleaned from
extensive conversations with experienced \s-1CPAN\s0 authors and users.  Every
piece of advice given here is the result of previous mistakes.  This
information is here to help you avoid the same mistakes and the extra
work that would inevitably be required to fix them.

The first section of this document provides an itemized checklist;
subsequent sections provide a more detailed discussion of the items on
the list.  The final section, \*(L"Common Pitfalls\*(R", describes some of the
most popular mistakes made by \s-1CPAN\s0 authors.

## QUICK CHECKLIST

Header "QUICK CHECKLIST"
For more detail on each item in this checklist, see below.

### Before you start

Subsection "Before you start"

- \(bu
Don't re-invent the wheel

- \(bu
Patch, extend or subclass an existing module where possible

- \(bu
Do one thing and do it well

- \(bu
Choose an appropriate name

- \(bu
Get feedback before publishing

### The \s-1API\s0

Subsection "The API"

- \(bu
\s-1API\s0 should be understandable by the average programmer

- \(bu
Simple methods for simple tasks

- \(bu
Separate functionality from output

- \(bu
Consistent naming of subroutines or methods

- \(bu
Use named parameters (a hash or hashref) when there are more than two
parameters

### Stability

Subsection "Stability"

- \(bu
Ensure your module works under \f(CW\*(C`use strict\*(C' and \f(CW\*(C`-w\*(C'

- \(bu
Stable modules should maintain backwards compatibility

### Documentation

Subsection "Documentation"

- \(bu
Write documentation in \s-1POD\s0

- \(bu
Document purpose, scope and target applications

- \(bu
Document each publicly accessible method or subroutine, including params and return values

- \(bu
Give examples of use in your documentation

- \(bu
Provide a \s-1README\s0 file and perhaps also release notes, changelog, etc

- \(bu
Provide links to further information (\s-1URL,\s0 email)

### Release considerations

Subsection "Release considerations"

- \(bu
Specify pre-requisites in Makefile.PL or Build.PL

- \(bu
Specify Perl version requirements with \f(CW\*(C`use\*(C'

- \(bu
Include tests with your module

- \(bu
Choose a sensible and consistent version numbering scheme (X.YY is the common Perl module numbering scheme)

- \(bu
Increment the version number for every change, no matter how small

- \(bu
Package the module using \*(L"make dist\*(R"

- \(bu
Choose an appropriate license (GPL/Artistic is a good default)

## BEFORE YOU START WRITING A MODULE

Header "BEFORE YOU START WRITING A MODULE"
Try not to launch headlong into developing your module without spending
some time thinking first.  A little forethought may save you a vast
amount of effort later on.

### Has it been done before?

Subsection "Has it been done before?"
You may not even need to write the module.  Check whether it's already
been done in Perl, and avoid re-inventing the wheel unless you have a
good reason.

Good places to look for pre-existing modules include
MetaCPAN <https://metacpan.org> and PrePAN <http://prepan.org>
and asking on \f(CW\*(C`module-authors@perl.org\*(C'
(<https://lists.perl.org/list/module-authors.html>).

If an existing module **almost** does what you want, consider writing a
patch, writing a subclass, or otherwise extending the existing module
rather than rewriting it.

### Do one thing and do it well

Subsection "Do one thing and do it well"
At the risk of stating the obvious, modules are intended to be modular.
A Perl developer should be able to use modules to put together the
building blocks of their application.  However, it's important that the
blocks are the right shape, and that the developer shouldn't have to use
a big block when all they need is a small one.

Your module should have a clearly defined scope which is no longer than
a single sentence.  Can your module be broken down into a family of
related modules?

Bad example:

\*(L"FooBar.pm provides an implementation of the \s-1FOO\s0 protocol and the
related \s-1BAR\s0 standard.\*(R"

Good example:

\*(L"Foo.pm provides an implementation of the \s-1FOO\s0 protocol.  Bar.pm
implements the related \s-1BAR\s0 protocol.\*(R"

This means that if a developer only needs a module for the \s-1BAR\s0 standard,
they should not be forced to install libraries for \s-1FOO\s0 as well.

### Whats in a name?

Subsection "What's in a name?"
Make sure you choose an appropriate name for your module early on.  This
will help people find and remember your module, and make programming
with your module more intuitive.

When naming your module, consider the following:

- \(bu
Be descriptive (i.e. accurately describes the purpose of the module).

- \(bu
Be consistent with existing modules.

- \(bu
Reflect the functionality of the module, not the implementation.

- \(bu
Avoid starting a new top-level hierarchy, especially if a suitable
hierarchy already exists under which you could place your module.

### Get feedback before publishing

Subsection "Get feedback before publishing"
If you have never uploaded a module to \s-1CPAN\s0 before (and even if you have),
you are strongly encouraged to get feedback on PrePAN <http://prepan.org>.
PrePAN is a site dedicated to discussing ideas for \s-1CPAN\s0 modules with other
Perl developers and is a great resource for new (and experienced) Perl
developers.

You should also try to get feedback from people who are already familiar
with the module's application domain and the \s-1CPAN\s0 naming system.  Authors
of similar modules, or modules with similar names, may be a good place to
start, as are community sites like Perl Monks <https://www.perlmonks.org>.

## DESIGNING AND WRITING YOUR MODULE

Header "DESIGNING AND WRITING YOUR MODULE"
Considerations for module design and coding:

### To \s-1OO\s0 or not to \s-1OO\s0?

Subsection "To OO or not to OO?"
Your module may be object oriented (\s-1OO\s0) or not, or it may have both kinds
of interfaces available.  There are pros and cons of each technique, which
should be considered when you design your \s-1API.\s0

In *Perl Best Practices* (copyright 2004, Published by O'Reilly Media, Inc.),
Damian Conway provides a list of criteria to use when deciding if \s-1OO\s0 is the
right fit for your problem:

- \(bu
The system being designed is large, or is likely to become large.

- \(bu
The data can be aggregated into obvious structures, especially if
there's a large amount of data in each aggregate.

- \(bu
The various types of data aggregate form a natural hierarchy that
facilitates the use of inheritance and polymorphism.

- \(bu
You have a piece of data on which many different operations are
applied.

- \(bu
You need to perform the same general operations on related types of
data, but with slight variations depending on the specific type of data
the operations are applied to.

- \(bu
It's likely you'll have to add new data types later.

- \(bu
The typical interactions between pieces of data are best represented by
operators.

- \(bu
The implementation of individual components of the system is likely to
change over time.

- \(bu
The system design is already object-oriented.

- \(bu
Large numbers of other programmers will be using your code modules.

Think carefully about whether \s-1OO\s0 is appropriate for your module.
Gratuitous object orientation results in complex APIs which are
difficult for the average module user to understand or use.

### Designing your \s-1API\s0

Subsection "Designing your API"
Your interfaces should be understandable by an average Perl programmer.
The following guidelines may help you judge whether your \s-1API\s0 is
sufficiently straightforward:

- Write simple routines to do simple things.
Item "Write simple routines to do simple things."
It's better to have numerous simple routines than a few monolithic ones.
If your routine changes its behaviour significantly based on its
arguments, it's a sign that you should have two (or more) separate
routines.

- Separate functionality from output.
Item "Separate functionality from output."
Return your results in the most generic form possible and allow the user
to choose how to use them.  The most generic form possible is usually a
Perl data structure which can then be used to generate a text report,
\s-1HTML, XML,\s0 a database query, or whatever else your users require.
.Sp
If your routine iterates through some kind of list (such as a list of
files, or records in a database) you may consider providing a callback
so that users can manipulate each element of the list in turn.
File::Find provides an example of this with its
\f(CW\*(C`find(\wanted, $dir)\*(C' syntax.

- Provide sensible shortcuts and defaults.
Item "Provide sensible shortcuts and defaults."
Don't require every module user to jump through the same hoops to achieve a
simple result.  You can always include optional parameters or routines for
more complex or non-standard behaviour.  If most of your users have to
type a few almost identical lines of code when they start using your
module, it's a sign that you should have made that behaviour a default.
Another good indicator that you should use defaults is if most of your
users call your routines with the same arguments.

- Naming conventions
Item "Naming conventions"
Your naming should be consistent.  For instance, it's better to have:
.Sp
.Vb 3
        display_day();
        display_week();
        display_year();
.Ve
.Sp
than
.Sp
.Vb 3
        display_day();
        week_display();
        show_year();
.Ve
.Sp
This applies equally to method names, parameter names, and anything else
which is visible to the user (and most things that aren't!)

- Parameter passing
Item "Parameter passing"
Use named parameters.  It's easier to use a hash like this:
.Sp
.Vb 5
    $obj->do_something(
            name => "wibble",
            type => "text",
            size => 1024,
    );
.Ve
.Sp
... than to have a long list of unnamed parameters like this:
.Sp
.Vb 1
    $obj->do_something("wibble", "text", 1024);
.Ve
.Sp
While the list of arguments might work fine for one, two or even three
arguments, any more arguments become hard for the module user to
remember, and hard for the module author to manage.  If you want to add
a new parameter you will have to add it to the end of the list for
backward compatibility, and this will probably make your list order
unintuitive.  Also, if many elements may be undefined you may see the
following unattractive method calls:
.Sp
.Vb 1
    $obj->do_something(undef, undef, undef, undef, undef, 1024);
.Ve
.Sp
Provide sensible defaults for parameters which have them.  Don't make
your users specify parameters which will almost always be the same.
.Sp
The issue of whether to pass the arguments in a hash or a hashref is
largely a matter of personal style.
.Sp
The use of hash keys starting with a hyphen (\f(CW\*(C`-name\*(C') or entirely in
upper case (\f(CW\*(C`NAME\*(C') is a relic of older versions of Perl in which
ordinary lower case strings were not handled correctly by the \f(CW\*(C`=>\*(C'
operator.  While some modules retain uppercase or hyphenated argument
keys for historical reasons or as a matter of personal style, most new
modules should use simple lower case keys.  Whatever you choose, be
consistent!

### Strictness and warnings

Subsection "Strictness and warnings"
Your module should run successfully under the strict pragma and should
run without generating any warnings.  Your module should also handle
taint-checking where appropriate, though this can cause difficulties in
many cases.

### Backwards compatibility

Subsection "Backwards compatibility"
Modules which are \*(L"stable\*(R" should not break backwards compatibility
without at least a long transition phase and a major change in version
number.

### Error handling and messages

Subsection "Error handling and messages"
When your module encounters an error it should do one or more of:

- \(bu
Return an undefined value.

- \(bu
set \f(CW$Module::errstr or similar (\f(CW\*(C`errstr\*(C' is a common name used by
\s-1DBI\s0 and other popular modules; if you choose something else, be sure to
document it clearly).

- \(bu
\f(CW\*(C`warn()\*(C' or \f(CW\*(C`carp()\*(C' a message to \s-1STDERR.\s0

- \(bu
\f(CW\*(C`croak()\*(C' only when your module absolutely cannot figure out what to
do.  (\f(CW\*(C`croak()\*(C' is a better version of \f(CW\*(C`die()\*(C' for use within
modules, which reports its errors from the perspective of the caller.
See Carp for details of \f(CW\*(C`croak()\*(C', \f(CW\*(C`carp()\*(C' and other useful
routines.)

- \(bu
As an alternative to the above, you may prefer to throw exceptions using
the Error module.

Configurable error handling can be very useful to your users.  Consider
offering a choice of levels for warning and debug messages, an option to
send messages to a separate file, a way to specify an error-handling
routine, or other such features.  Be sure to default all these options
to the commonest use.

## DOCUMENTING YOUR MODULE

Header "DOCUMENTING YOUR MODULE"

### \s-1POD\s0

Subsection "POD"
Your module should include documentation aimed at Perl developers.
You should use Perl's \*(L"plain old documentation\*(R" (\s-1POD\s0) for your general
technical documentation, though you may wish to write additional
documentation (white papers, tutorials, etc) in some other format.
You need to cover the following subjects:

- \(bu
A synopsis of the common uses of the module

- \(bu
The purpose, scope and target applications of your module

- \(bu
Use of each publicly accessible method or subroutine, including
parameters and return values

- \(bu
Examples of use

- \(bu
Sources of further information

- \(bu
A contact email address for the author/maintainer

The level of detail in Perl module documentation generally goes from
less detailed to more detailed.  Your \s-1SYNOPSIS\s0 section should contain a
minimal example of use (perhaps as little as one line of code; skip the
unusual use cases or anything not needed by most users); the
\s-1DESCRIPTION\s0 should describe your module in broad terms, generally in
just a few paragraphs; more detail of the module's routines or methods,
lengthy code examples, or other in-depth material should be given in
subsequent sections.

Ideally, someone who's slightly familiar with your module should be able
to refresh their memory without hitting \*(L"page down\*(R".  As your reader
continues through the document, they should receive a progressively
greater amount of knowledge.

The recommended order of sections in Perl module documentation is:

- \(bu
\s-1NAME\s0

- \(bu
\s-1SYNOPSIS\s0

- \(bu
\s-1DESCRIPTION\s0

- \(bu
One or more sections or subsections giving greater detail of available
methods and routines and any other relevant information.

- \(bu
BUGS/CAVEATS/etc

- \(bu
\s-1AUTHOR\s0

- \(bu
\s-1SEE ALSO\s0

- \(bu
\s-1COPYRIGHT\s0 and \s-1LICENSE\s0

Keep your documentation near the code it documents (\*(L"inline\*(R"
documentation).  Include \s-1POD\s0 for a given method right above that
method's subroutine.  This makes it easier to keep the documentation up
to date, and avoids having to document each piece of code twice (once in
\s-1POD\s0 and once in comments).

### \s-1README, INSTALL,\s0 release notes, changelogs

Subsection "README, INSTALL, release notes, changelogs"
Your module should also include a \s-1README\s0 file describing the module and
giving pointers to further information (website, author email).

An \s-1INSTALL\s0 file should be included, and should contain simple installation
instructions.  When using ExtUtils::MakeMaker this will usually be:

- perl Makefile.PL
Item "perl Makefile.PL"
0

- make
Item "make"

- make test
Item "make test"

- make install
Item "make install"
.PD

When using Module::Build, this will usually be:

- perl Build.PL
Item "perl Build.PL"
0

- perl Build
Item "perl Build"

- perl Build test
Item "perl Build test"

- perl Build install
Item "perl Build install"
.PD

Release notes or changelogs should be produced for each release of your
software describing user-visible changes to your module, in terms
relevant to the user.

Unless you have good reasons for using some other format
(for example, a format used within your company),
the convention is to name your changelog file \f(CW\*(C`Changes\*(C',
and to follow the simple format described in CPAN::Changes::Spec.

## RELEASE CONSIDERATIONS

Header "RELEASE CONSIDERATIONS"

### Version numbering

Subsection "Version numbering"
Version numbers should indicate at least major and minor releases, and
possibly sub-minor releases.  A major release is one in which most of
the functionality has changed, or in which major new functionality is
added.  A minor release is one in which a small amount of functionality
has been added or changed.  Sub-minor version numbers are usually used
for changes which do not affect functionality, such as documentation
patches.

The most common \s-1CPAN\s0 version numbering scheme looks like this:

.Vb 1
    1.00, 1.10, 1.11, 1.20, 1.30, 1.31, 1.32
.Ve

A correct \s-1CPAN\s0 version number is a floating point number with at least
2 digits after the decimal.  You can test whether it conforms to \s-1CPAN\s0 by
using

.Vb 2
    perl -MExtUtils::MakeMaker -le \*(Aqprint MM->parse_version(shift)\*(Aq \\
                                                            \*(AqFoo.pm\*(Aq
.Ve

If you want to release a 'beta' or 'alpha' version of a module but
don't want \s-1CPAN\s0.pm to list it as most recent use an '_' after the
regular version number followed by at least 2 digits, eg. 1.20_01.  If
you do this, the following idiom is recommended:

.Vb 5
  our $VERSION = "1.12_01"; # so CPAN distribution will have
                            # right filename
  our $XS_VERSION = $VERSION; # only needed if you have XS code
  $VERSION = eval $VERSION; # so "use Module 0.002" won\*(Aqt warn on
                            # underscore
.Ve

With that trick MakeMaker will only read the first line and thus read
the underscore, while the perl interpreter will evaluate the \f(CW$VERSION
and convert the string into a number.  Later operations that treat
\f(CW$VERSION as a number will then be able to do so without provoking a
warning about \f(CW$VERSION not being a number.

Never release anything (even a one-word documentation patch) without
incrementing the number.  Even a one-word documentation patch should
result in a change in version at the sub-minor level.

Once picked, it is important to stick to your version scheme, without
reducing the number of digits.  This is because \*(L"downstream\*(R" packagers,
such as the FreeBSD ports system, interpret the version numbers in
various ways.  If you change the number of digits in your version scheme,
you can confuse these systems so they get the versions of your module
out of order, which is obviously bad.

### Pre-requisites

Subsection "Pre-requisites"
Module authors should carefully consider whether to rely on other
modules, and which modules to rely on.

Most importantly, choose modules which are as stable as possible.  In
order of preference:

- \(bu
Core Perl modules

- \(bu
Stable \s-1CPAN\s0 modules

- \(bu
Unstable \s-1CPAN\s0 modules

- \(bu
Modules not available from \s-1CPAN\s0

Specify version requirements for other Perl modules in the
pre-requisites in your Makefile.PL or Build.PL.

Be sure to specify Perl version requirements both in Makefile.PL or
Build.PL and with \f(CW\*(C`require 5.6.1\*(C' or similar.  See the section on
\f(CW\*(C`use VERSION\*(C' of \*(L"require\*(R" in perlfunc for details.

### Testing

Subsection "Testing"
All modules should be tested before distribution (using \*(L"make disttest\*(R"),
and the tests should also be available to people installing the modules
(using \*(L"make test\*(R").
For Module::Build you would use the \f(CW\*(C`make test\*(C' equivalent \f(CW\*(C`perl Build test\*(C'.

The importance of these tests is proportional to the alleged stability of a
module.  A module which purports to be
stable or which hopes to achieve wide
use should adhere to as strict a testing regime as possible.

Useful modules to help you write tests (with minimum impact on your
development process or your time) include Test::Simple, Carp::Assert
and Test::Inline.
For more sophisticated test suites there are Test::More and Test::MockObject.

### Packaging

Subsection "Packaging"
Modules should be packaged using one of the standard packaging tools.
Currently you have the choice between ExtUtils::MakeMaker and the
more platform independent Module::Build, allowing modules to be installed in a
consistent manner.
When using ExtUtils::MakeMaker, you can use \*(L"make dist\*(R" to create your
package.  Tools exist to help you to build your module in a
MakeMaker-friendly style.  These include ExtUtils::ModuleMaker and h2xs.
See also perlnewmod.

### Licensing

Subsection "Licensing"
Make sure that your module has a license, and that the full text of it
is included in the distribution (unless it's a common one and the terms
of the license don't require you to include it).

If you don't know what license to use, dual licensing under the \s-1GPL\s0
and Artistic licenses (the same as Perl itself) is a good idea.
See perlgpl and perlartistic.

## COMMON PITFALLS

Header "COMMON PITFALLS"

### Reinventing the wheel

Subsection "Reinventing the wheel"
There are certain application spaces which are already very, very well
served by \s-1CPAN.\s0  One example is templating systems, another is date and
time modules, and there are many more.  While it is a rite of passage to
write your own version of these things, please consider carefully
whether the Perl world really needs you to publish it.

### Trying to do too much

Subsection "Trying to do too much"
Your module will be part of a developer's toolkit.  It will not, in
itself, form the **entire** toolkit.  It's tempting to add extra features
until your code is a monolithic system rather than a set of modular
building blocks.

### Inappropriate documentation

Subsection "Inappropriate documentation"
Don't fall into the trap of writing for the wrong audience.  Your
primary audience is a reasonably experienced developer with at least
a moderate understanding of your module's application domain, who's just
downloaded your module and wants to start using it as quickly as possible.

Tutorials, end-user documentation, research papers, FAQs etc are not
appropriate in a module's main documentation.  If you really want to
write these, include them as sub-documents such as \f(CW\*(C`My::Module::Tutorial\*(C' or
\f(CW\*(C`My::Module::FAQ\*(C' and provide a link in the \s-1SEE ALSO\s0 section of the
main documentation.

## SEE ALSO

Header "SEE ALSO"

- perlstyle
Item "perlstyle"
General Perl style guide

- perlnewmod
Item "perlnewmod"
How to create a new module

- perlpod
Item "perlpod"
\s-1POD\s0 documentation

- podchecker
Item "podchecker"
Verifies your \s-1POD\s0's correctness

- Packaging Tools
Item "Packaging Tools"
ExtUtils::MakeMaker, Module::Build

- Testing tools
Item "Testing tools"
Test::Simple, Test::Inline, Carp::Assert, Test::More, Test::MockObject

- <https://pause.perl.org/>
Item "<https://pause.perl.org/>"
Perl Authors Upload Server.  Contains links to information for module
authors.

- Any good book on software engineering
Item "Any good book on software engineering"

## AUTHOR

Header "AUTHOR"
Kirrily \*(L"Skud\*(R" Robert <skud@cpan.org>
