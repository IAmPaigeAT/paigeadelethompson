+++
manpage_format = "troff"
title = "perlgov(1)"
operating_system_version = "15.3"
manpage_name = "perlgov"
manpage_section = "1"
description = "None Specified"
detected_package_version = "5.34.1"
operating_system = "macos"
author = "None Specified"
date = "2022-02-19"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLGOV 1"
PERLGOV 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlgov - Perl Rules of Governance

## PREAMBLE

Header "PREAMBLE"
We are forming a system of governance for development of the Perl programming
language.

The scope of governance includes the language definition, its
implementation, its test suite, its documentation, and the policies and
procedures by which it is developed and maintained.

The system of governance includes definitions of the groups that will make
decisions, the rules by which these groups are formed and changed, and the
enumerated powers and constraints on the activities of these governing
groups.

In forming a system of governance, we seek to achieve the following goals:

- \(bu
We want a system that is functional.  That means the governing groups may
decide to undertake large changes, or they may decide to act conservatively,
but they will act with intent and clear communication rather than fail to reach
decisions when needed.

- \(bu
We want a system that is trusted. That means that a reasonable contributor to
Perl might disagree with decisions made by the governing groups, but will
accept that they were made in good faith in consultation with relevant
communities outside the governing groups.

- \(bu
We want a system that is sustainable.  That means it has provisions to
self-modify, including ways of adding new members to the governing groups, ways
to survive members becoming inactive, and ways of amending the rules of
governance themselves if needed.

- \(bu
We want a system that is transparent.  That means that it will prefer policies
that manage ordinary matters in public, and it will prefer secrecy in a limited
number of situations.

- \(bu
We want a system that is respectful.  That means that it will establish
standards of civil discourse that allow for healthy disagreement but avoid
rancor and hostility in the community for which it is responsible.

## Mandate

Header "Mandate"
Perl language governance shall work to:

- \(bu
Maintain the quality, stability, and continuity of the Perl language and
interpreter

- \(bu
Guide the evolution of the Perl language and interpreter

- \(bu
Establish and oversee the policies, procedures, systems, and mechanisms that
enable a community of contributors to the Perl language and interpreter

- \(bu
Encourage discussion and consensus among contributors as preferential to formal
decision making by governance groups

- \(bu
Facilitate communication between contributors and external stakeholders in the
broader Perl ecosystem

## Definitions

Header "Definitions"
This document describes three roles involved in governance:
.ie n .IP """Core Team""" 4
.el .IP "``Core Team''" 4
Item "Core Team"
0
.ie n .IP """Steering Council""" 4
.el .IP "``Steering Council''" 4
Item "Steering Council"
.ie n .IP """Vote Administrator""" 4
.el .IP "``Vote Administrator''" 4
Item "Vote Administrator"
.PD

A section on each follows.

### The Core Team

Subsection "The Core Team"
The Core Team are a group of trusted volunteers involved in the ongoing
development of the Perl language and interpreter.  They are not required to be
language developers or committers.

References to specific votes are explained in the \*(L"Rules for Voting\*(R" section.

*Powers*
Subsection "Powers"

In addition to their contributions to the Perl language, the Core Team sets
the rules of Perl governance, decides who participates in what role in
governance, and delegates substantial decision making power to the Steering
Council.

Specifically:

- \(bu
They elect the Steering Council and have the power to remove Steering
Council members.

- \(bu
In concert with the Steering Council, they manage Core Team membership.

- \(bu
In concert with the Steering Council, they have the power to modify the Perl
Rules of Governance.

The Core Team do not have any authority over parts of the Perl ecosystem
unrelated to developing and releasing the language itself.  These include, but
are not limited to:

- \(bu
The Perl Foundation

- \(bu
\s-1CPAN\s0 administration and \s-1CPAN\s0 authors

- \(bu
perl.org, metacpan.org, and other community-maintained websites and services

- \(bu
Perl conferences and events, except those organized directly by the Core Team

- \(bu
Perl-related intellectual property legally owned by third-parties, except as
allowed by applicable licenses or agreements

*Membership*
Subsection "Membership"

The initial Core Team members will be specified when this document is
first ratified.

Any Core Team member may nominate someone to be added to the Core Team by
sending the nomination to the Steering Council.  The Steering Council must
approve or reject the nomination.  If approved, the Steering Council will
organize a Membership Change Vote to ratify the addition.

Core Team members should demonstrate:

- \(bu
A solid track record of being constructive and helpful

- \(bu
Significant contributions to the project's goals, in any form

- \(bu
Willingness to dedicate some time to improving Perl

Contributions are not limited to code. Here is an incomplete list of areas
where contributions may be considered for joining the Core Team:

- \(bu
Working on community management and outreach

- \(bu
Providing support on mailing lists, \s-1IRC,\s0 or other forums

- \(bu
Triaging tickets

- \(bu
Writing patches (code, docs, or tests)

- \(bu
Reviewing patches (code, docs, or tests)

- \(bu
Participating in design discussions

- \(bu
Providing expertise in a particular domain (security, i18n, etc.)

- \(bu
Managing Perl infrastructure (websites, \s-1CI,\s0 documentation, etc.)

- \(bu
Maintaining significant projects in the Perl ecosystem

- \(bu
Creating visual designs

Core Team membership acknowledges sustained and valuable efforts that align
well with the philosophy and the goals of the Perl project.

Core Team members are expected to act as role models for the community and
custodians of the project, on behalf of the community and all those who rely
on Perl.

*Term*
Subsection "Term"

Core Team members serve until they are removed.

*Removal*
Subsection "Removal"

Core Team Members may resign their position at any time.

In exceptional circumstances, it may be necessary to remove someone from the
Core Team against their will, such as for flagrant or repeated violations of a
Code of Conduct.  Any Core Team member may send a recall request to the
Steering Council naming the individual to be removed.  The Steering Council
must approve or reject the recall request.  If approved, the Steering Council
will organize a Membership Change vote to ratify the removal.

If the removed member is also on the Steering Council, then they are removed
from the Steering Council as well.

*Inactivity*
Subsection "Inactivity"

Core Team members who have stopped contributing are encouraged to declare
themselves \*(L"inactive\*(R". Inactive members do not nominate or vote.  Inactive
members may declare themselves active at any time, except when a vote has been
proposed and is not concluded.  Eligibility to nominate or vote will be
determined by the Vote Administrator.

To record and honor their contributions, inactive Core Team members will
continue to be listed alongside active members.

*No Confidence in the Steering Council*
Subsection "No Confidence in the Steering Council"

The Core Team may remove either a single Steering Council member or the entire
Steering Council via a No Confidence Vote.

A No Confidence Vote is triggered when a Core Team member calls for one
publicly on an appropriate project communication channel, and another Core
Team member seconds the proposal.

If a No Confidence Vote removes all Steering Council members, the Vote
Administrator of the No Confidence Vote will then administer an election
to select a new Steering Council.

*Amending Perl Rules of Governance*
Subsection "Amending Perl Rules of Governance"

Any Core Team member may propose amending the Perl Rules of Governance by
sending a proposal to the Steering Council.  The Steering Council must decide
to approve or reject the proposal.  If approved, the Steering Council will
administer an Amendment Vote.

*Rules for Voting*
Subsection "Rules for Voting"

Membership Change, Amendment, and No Confidence Votes require 2/3 of
participating votes from Core Team members to pass.

A Vote Administrator must be selected following the rules in the \*(L"Vote
Administrator\*(R" section.

The vote occurs in two steps:

- 1.
The Vote Administrator describes the proposal being voted upon.  The Core Team
then may discuss the matter in advance of voting.

- 2.
Active Core Team members vote in favor or against the proposal.  Voting is
performed anonymously.

For a Membership Change Vote, each phase will last one week.  For Amendment and
No Confidence Votes, each phase will last two weeks.

### The Steering Council

Subsection "The Steering Council"
The Steering Council is a 3-person committee, elected by the Core
Team.  Candidates are not required to be members of the Core Team.  Non-member
candidates are added to the Core Team if elected as if by a Membership Change
Vote.

References to specific elections are explained in the \*(L"Rules for Elections\*(R" section.

*Powers*
Subsection "Powers"

The Steering Council has broad authority to make decisions about the
development of the Perl language, the interpreter, and all other components,
systems and processes that result in new releases of the language interpreter.

For example, it can:

- \(bu
Manage the schedule and process for shipping new releases

- \(bu
Establish procedures for proposing, discussing and deciding upon changes to the
language

- \(bu
Delegate power to individuals on or outside the Steering Council

Decisions of the Steering Council will be made by majority vote of non-vacant
seats on the council.

The Steering Council should look for ways to use these powers as little as
possible.  Instead of voting, it's better to seek consensus. Instead of ruling
on individual cases, it's better to define standards and processes that apply
to all cases.

As with the Core Team, the Steering Council does not have any authority over
parts of the Perl ecosystem unrelated to developing and releasing the language
itself.

The Steering Council does not have the power to modify the Perl Rules of
Governance, except as provided in the section \*(L"Amending Perl Rules of
Governance\*(R".

*Term*
Subsection "Term"

A new Steering Council will be chosen by a Term Election within two weeks after
each stable feature release (that is, change to \f(CW\*(C`PERL_REVISION\*(C' or
\f(CW\*(C`PERL_VERSION\*(C') or after two years, whichever comes first. The council members
will serve until the completion of the next Term Election unless they are
removed.

*Removal*
Subsection "Removal"

Steering Council members may resign their position at any time.

Whenever there are vacancies on the Steering Council, the council will
organize a Special Election within one week after the vacancy occurs.  If the
entire Steering Council is ever vacant, a Term Election will be held instead.

If a Steering Council member is deceased, or drops out of touch and cannot be
contacted for a month or longer, then the rest of the council may vote to
declare their seat vacant.  If an absent member returns after such a
declaration is made, they are not reinstated automatically, but may run in the
Special Election to fill the vacancy.

Otherwise, Steering Council members may only be removed before the end of
their term through a No Confidence Vote by the Core Team.

*Rules for Elections*
Subsection "Rules for Elections"

Term and Special Election are ranked-choice votes to construct an ordered list
of candidates to fill vacancies in the Steering Council.

A Vote Administrator must be selected following the rules in the \*(L"Vote
Administrator\*(R" section.

Both Term and Special Elections occur in two stages:

- 1.
Candidates advertise their interest in serving. Candidates must be nominated by
an active Core Team member. Self-nominations are allowed.  Nominated candidates
may share a statement about their candidacy with the Core Team.

- 2.
Active Core Team Members vote by ranking all candidates.  Voting is performed
anonymously.  After voting is complete, candidates are ranked using the
Condorcet Internet Voting Service's proportional representation mode.  If a tie
occurs, it may be resolved by mutual agreement among the tied candidates, or
else the tie will be resolved through random selection by the Vote
Administrator.

Anyone voted off the Core Team is not eligible to be a candidate for Steering
Council unless re-instated to the Core Team.

For a Term Election, each phase will last two weeks.  At the end of the second
phase, the top three ranked candidates are elected as the new Steering Council.

For a Special Election, each phase will last one week.  At the end of the
second phase, vacancies are filled from the ordered list of candidates until
no vacancies remain.

The election of the first Steering Council will be a Term Election.  Ricardo
Signes will be the Vote Administrator for the initial Term Election unless he
is a candidate, in which case he will select a non-candidate administrator to
replace him.

### The Vote Administrator

Subsection "The Vote Administrator"
Every election or vote requires a Vote Administrator who manages
communication, collection of secret ballots, and all other necessary
activities to complete the voting process.

Unless otherwise specified, the Steering Council selects the Vote
Administrator.

A Vote Administrator must not be a member of the Steering Council nor a
candidate or subject of the vote.  A Vote Administrator may be a member of the
Core Team and, if so, may cast a vote while also serving as administrator.  If
the Vote Administrator becomes a candidate during an election vote, they will
appoint a non-candidate replacement.

If the entire Steering Council is vacant or is the subject of a No Confidence
Vote, then the Core Team will select a Vote Administrator by consensus.  If
consensus cannot be reached within one week, the President of The Perl
Foundation will select a Vote Administrator.

## Core Team Members

Header "Core Team Members"
The current members of the Perl Core Team are:

- \(bu
Abhijit Menon-Sen (inactive)

- \(bu
Andy Dougherty (inactive)

- \(bu
Chad Granum

- \(bu
Chris 'BinGOs' Williams

- \(bu
Craig Berry

- \(bu
Dagfinn Ilmari Mannsa\*oker

- \(bu
Dave Mitchell

- \(bu
David Golden

- \(bu
H. Merijn Brand

- \(bu
Hugo van der Sanden

- \(bu
James E Keenan

- \(bu
Jan Dubois (inactive)

- \(bu
Jesse Vincent (inactive)

- \(bu
Karen Etheridge

- \(bu
Karl Williamson

- \(bu
Leon Timmermans

- \(bu
Matthew Horsfall

- \(bu
Max Maischein

- \(bu
Neil Bowers

- \(bu
Nicholas Clark

- \(bu
Nicolas R.

- \(bu
Paul \*(L"LeoNerd\*(R" Evans

- \(bu
Philippe \*(L"BooK\*(R" Bruhat

- \(bu
Ricardo Signes

- \(bu
Steve Hay

- \(bu
Stuart Mackintosh

- \(bu
Todd Rinaldo

- \(bu
Tony Cook
