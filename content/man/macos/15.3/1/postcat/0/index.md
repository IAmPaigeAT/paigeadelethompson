+++
keywords = ["na", "postconf", "postfix", "configuration", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
title = "postcat(1)"
operating_system = "macos"
manpage_name = "postcat"
detected_package_version = "0"
author = "None Specified"
date = "Sun Feb 16 04:48:24 2025"
manpage_format = "troff"
manpage_section = "1"
operating_system_version = "15.3"
description = "The postcat(1) command prints the contents of the named files in human-readable form. The files are expected to be in Postfix queue file format. If no files are specified on the command line, the program reads from standard input."
+++

POSTCAT 1


## NAME

postcat
-
show Postfix queue file contents

## SYNOPSIS

.na

```
postcat [\-bdehnoqv] [\-c config_dir] [files...]
.SH DESCRIPTION
.ad
```

The **postcat**(1) command prints the contents of the
named *files* in human-readable form. The files are
expected to be in Postfix queue file format. If no *files*
are specified on the command line, the program reads from
standard input.

By default, **postcat**(1) shows the envelope and message
content, as if the options **-beh** were specified. To
view message content only, specify **-bh** (Postfix 2.7
and later).

Options:
**-b**
Show body content.  The **-b** option starts producing
output at the first non-header line, and stops when the end
of the message is reached.

This feature is available in Postfix 2.7 and later.

- \fB-c
The **main.cf** configuration file is in the named directory
instead of the default configuration directory.
**-d**
Print the decimal type of each record.
**-e**
Show message envelope content.

This feature is available in Postfix 2.7 and later.
**-h**
Show message header content.  The **-h** option produces
output from the beginning of the message up to, but not
including, the first non-header line.

This feature is available in Postfix 2.7 and later.
**-o**
Print the queue file offset of each record.
**-q**
Search the Postfix queue for the named *files* instead
of taking the names literally.

This feature is available in Postfix 2.0 and later.
**-v**
Enable verbose logging for debugging purposes. Multiple **-v**
options make the software increasingly verbose.

## DIAGNOSTICS


Problems are reported to the standard error stream.

## ENVIRONMENT

.na

```
.ad
```

**MAIL_CONFIG**
Directory with Postfix configuration files.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

The following **main.cf** parameters are especially relevant to
this program.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBimport_environment (see 'postconf -d'
The list of environment parameters that a privileged Postfix
process will import from a non-Postfix parent process, or name=value
environment overrides.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

## FILES

.na

```
/var/spool/postfix, Postfix queue directory
.SH "SEE ALSO"
.na

```
postconf(5), Postfix configuration
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
