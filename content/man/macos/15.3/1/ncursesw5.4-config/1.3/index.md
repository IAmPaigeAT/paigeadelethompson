+++
detected_package_version = "1.3"
description = "This is a shell script which simplifies configuring applications against a particular set of ncurses libraries. --prefix echos the package-prefix of ncurses --exec-prefix echos the executable-prefix of ncurses --cflags echos the C compiler flag..."
operating_system = "macos"
operating_system_version = "15.3"
manpage_name = "ncursesw5.4-config"
keywords = ["fbcurses", "this", "describes", "fbncurses", "version", "6", "patch", "20150808"]
date = "Sun Feb 16 04:48:20 2025"
manpage_format = "troff"
manpage_section = "1"
author = "Free Software Foundation, Inc. *"
title = "ncursesw5.4-config(1)"
+++

ncursesw5.4-config 1 ""

## NAME

ncursesw5.4-config - helper script for ncurses libraries

## SYNOPSIS

ncursesw5.4-config
[*options*]

## DESCRIPTION

This is a shell script which simplifies configuring applications against
a particular set of ncurses libraries.

## OPTIONS


**--prefix**
echos the package-prefix of ncurses

**--exec-prefix**
echos the executable-prefix of ncurses

**--cflags**
echos the C compiler flags needed to compile with ncurses

**--libs**
echos the libraries needed to link with ncurses

**--version**
echos the release+patchdate version of ncurses

**--abi-version**
echos the ABI version of ncurses

**--mouse-version**
echos the mouse-interface version of ncurses

**--bindir**
echos the directory containing ncurses programs

**--datadir**
echos the directory containing ncurses data

**--includedir**
echos the directory containing ncurses header files

**--libdir**
echos the directory containing ncurses libraries

**--mandir**
echos the directory containing ncurses manpages

**--terminfo**
echos the $TERMINFO terminfo database path, e.g.,

> /usr/share/terminfo



**--terminfo-dirs**
echos the $TERMINFO_DIRS directory list, e.g.,

> /usr/share/terminfo



**--termpath**
echos the $TERMPATH termcap list, if support for termcap is configured.

**--help**
prints this message

## SEE ALSO

**curses**(3X)

This describes **ncurses**
version 6.0 (patch 20150808).
