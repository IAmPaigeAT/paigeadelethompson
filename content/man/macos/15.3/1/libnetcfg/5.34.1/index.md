+++
keywords = ["header", "see", "also", "net", "config", "libnetfaq", "authors", "graham", "barr", "the", "original", "configure", "script", "of", "libnet", "jarkko", "hietaniemi", "conversion", "into", "libnetcfg", "for", "inclusion", "perl", "5", "8"]
operating_system = "macos"
author = "None Specified"
date = "2024-12-14"
description = "The libnetcfg utility can be used to configure the libnet. Starting from perl 5.8 libnet is part of the standard Perl distribution, but the libnetcfg can be used for any libnet installation. Without arguments libnetcfg displays the current configu..."
detected_package_version = "5.34.1"
manpage_format = "troff"
manpage_name = "libnetcfg"
operating_system_version = "15.3"
manpage_section = "1"
title = "libnetcfg(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "LIBNETCFG 1"
LIBNETCFG 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

libnetcfg - configure libnet

## DESCRIPTION

Header "DESCRIPTION"
The libnetcfg utility can be used to configure the libnet.
Starting from perl 5.8 libnet is part of the standard Perl
distribution, but the libnetcfg can be used for any libnet
installation.

## USAGE

Header "USAGE"
Without arguments libnetcfg displays the current configuration.

.Vb 10
    $ libnetcfg
    # old config ./libnet.cfg
    daytime_hosts        ntp1.none.such
    ftp_int_passive      0
    ftp_testhost         ftp.funet.fi
    inet_domain          none.such
    nntp_hosts           nntp.none.such
    ph_hosts
    pop3_hosts           pop.none.such
    smtp_hosts           smtp.none.such
    snpp_hosts
    test_exist           1
    test_hosts           1
    time_hosts           ntp.none.such
    # libnetcfg -h for help
    $
.Ve

It tells where the old configuration file was found (if found).

The \f(CW\*(C`-h\*(C' option will show a usage message.

To change the configuration you will need to use either the \f(CW\*(C`-c\*(C' or
the \f(CW\*(C`-d\*(C' options.

The default name of the old configuration file is by default
\*(L"libnet.cfg\*(R", unless otherwise specified using the -i option,
\f(CW\*(C`-i oldfile\*(C', and it is searched first from the current directory,
and then from your module path.

The default name of the new configuration file is \*(L"libnet.cfg\*(R", and by
default it is written to the current directory, unless otherwise
specified using the -o option, \f(CW\*(C`-o newfile\*(C'.

## SEE ALSO

Header "SEE ALSO"
Net::Config, libnetFAQ

## AUTHORS

Header "AUTHORS"
Graham Barr, the original Configure script of libnet.

Jarkko Hietaniemi, conversion into libnetcfg for inclusion into Perl 5.8.
