+++
description = "This document describes differences between the 5.32.0 release and the 5.32.1 release. If you are upgrading from an earlier release such as 5.30.0, first read perl5320delta, which describes differences between 5.30.0 and 5.32.0. There are no chang..."
date = "2022-02-19"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_section = "1"
detected_package_version = "5.34.1"
manpage_name = "perl5321delta"
title = "perl5321delta(1)"
manpage_format = "troff"
author = "None Specified"
operating_system_version = "15.3"
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5321DELTA 1"
PERL5321DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5321delta - what is new for perl v5.32.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.32.0 release and the 5.32.1
release.

If you are upgrading from an earlier release such as 5.30.0, first read
perl5320delta, which describes differences between 5.30.0 and 5.32.0.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with Perl 5.32.0.  If any
exist, they are bugs, and we request that you submit a report.  See
\*(L"Reporting Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Data::Dumper has been upgraded from version 2.174 to 2.174_01.
.Sp
A number of memory leaks have been fixed.

- \(bu
DynaLoader has been upgraded from version 1.47 to 1.47_01.

- \(bu
Module::CoreList has been upgraded from version 5.20200620 to 5.20210123.

- \(bu
Opcode has been upgraded from version 1.47 to 1.48.
.Sp
A warning has been added about evaluating untrusted code with the perl
interpreter.

- \(bu
Safe has been upgraded from version 2.41 to 2.41_01.
.Sp
A warning has been added about evaluating untrusted code with the perl
interpreter.

## Documentation

Header "Documentation"

### New Documentation

Subsection "New Documentation"
*perlgov*
Subsection "perlgov"

Documentation of the newly formed rules of governance for Perl.

*perlsecpolicy*
Subsection "perlsecpolicy"

Documentation of how the Perl security team operates and how the team evaluates
new security reports.

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
We have attempted to update the documentation to reflect the changes listed in
this document.  If you find any we have missed, open an issue at
<https://github.com/Perl/perl5/issues>.

Additionally, the following selected changes have been made:

*perlop*
Subsection "perlop"

- \(bu
Document range op behaviour change.

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
\\K not permitted in lookahead/lookbehind in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
This error was incorrectly produced in some cases involving nested lookarounds.
This has been fixed.
.Sp
[\s-1GH\s0 #18123 <https://github.com/Perl/perl5/issues/18123>]

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
Newer 64-bit versions of the Intel C/\*(C+ compiler are now recognized and have
the correct flags set.

- \(bu
We now trap \s-1SIGBUS\s0 when *Configure* checks for \f(CW\*(C`va_copy\*(C'.
.Sp
On several systems the attempt to determine if we need \f(CW\*(C`va_copy\*(C' or similar
results in a \s-1SIGBUS\s0 instead of the expected \s-1SIGSEGV,\s0 which previously caused a
core dump.
.Sp
[\s-1GH\s0 #18148 <https://github.com/Perl/perl5/issues/18148>]

## Testing

Header "Testing"
Tests were added and changed to reflect the other additions and changes in this
release.

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- MacOS (Darwin)
Item "MacOS (Darwin)"
The hints file for darwin has been updated to handle future macOS versions
beyond 10.  Perl can now be built on macOS Big Sur.
.Sp
[\s-1GH\s0 #17946 <https://github.com/Perl/perl5/issues/17946>,
\s-1GH\s0 #18406 <https://github.com/Perl/perl5/issues/18406>]

- Minix
Item "Minix"
Build errors on Minix have been fixed.
.Sp
[\s-1GH\s0 #17908 <https://github.com/Perl/perl5/issues/17908>]

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Some list assignments involving \f(CW\*(C`undef\*(C' on the left-hand side were
over-optimized and produced incorrect results.
.Sp
[\s-1GH\s0 #16685 <https://github.com/Perl/perl5/issues/16685>,
\s-1GH\s0 #17816 <https://github.com/Perl/perl5/issues/17816>]

- \(bu
Fixed a bug in which some regexps with recursive subpatterns matched
incorrectly.
.Sp
[\s-1GH\s0 #18096 <https://github.com/Perl/perl5/issues/18096>]

- \(bu
Fixed a deadlock that hung the build when Perl is compiled for debugging memory
problems and has \s-1PERL_MEM_LOG\s0 enabled.
.Sp
[\s-1GH\s0 #18341 <https://github.com/Perl/perl5/issues/18341>]

- \(bu
Fixed a crash in the use of chained comparison operators when run under \*(L"no
warnings 'uninitialized'\*(R".
.Sp
[\s-1GH\s0 #17917 <https://github.com/Perl/perl5/issues/17917>,
\s-1GH\s0 #18380 <https://github.com/Perl/perl5/issues/18380>]

- \(bu
Exceptions thrown from destructors during global destruction are no longer
swallowed.
.Sp
[\s-1GH\s0 #18063 <https://github.com/Perl/perl5/issues/18063>]

## Acknowledgements

Header "Acknowledgements"
Perl 5.32.1 represents approximately 7 months of development since Perl 5.32.0
and contains approximately 7,000 lines of changes across 80 files from 23
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 1,300 lines of changes to 23 .pm, .t, .c and .h files.

Perl continues to flourish into its fourth decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.32.1:

Adam Hartley, Andy Dougherty, Dagfinn Ilmari Mannsa\*oker, Dan Book, David
Mitchell, Graham Knop, Graham Ollis, Hauke D, H.Merijn Brand, Hugo van der
Sanden, John Lightsey, Karen Etheridge, Karl Williamson, Leon Timmermans, Max
Maischein, Nicolas R., Ricardo Signes, Richard Leach, Sawyer X, Sevan Janiyan,
Steve Hay, Tom Hukins, Tony Cook.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database at
<https://github.com/Perl/perl5/issues>.  There may also be information at
<http://www.perl.org/>, the Perl Home Page.

If you believe you have an unreported bug, please open an issue at
<https://github.com/Perl/perl5/issues>.  Be sure to trim your bug down to a
tiny but sufficient test case.

If the bug you are reporting has security implications which make it
inappropriate to send to a public issue tracker, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec for details of how to
report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5, you
can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
