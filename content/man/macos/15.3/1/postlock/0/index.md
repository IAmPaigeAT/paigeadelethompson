+++
operating_system_version = "15.3"
detected_package_version = "0"
manpage_name = "postlock"
description = "The postlock(1) command locks file for exclusive access, and executes command. The locking method is compatible with the Postfix UNIX-style local delivery agent."
operating_system = "macos"
date = "Sun Feb 16 04:48:22 2025"
keywords = ["na", "postconf", "configuration", "parameters", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
manpage_section = "1"
title = "postlock(1)"
manpage_format = "troff"
author = "None Specified"
+++

POSTLOCK 1


## NAME

postlock
-
lock mail folder and execute command

## SYNOPSIS

.na

```
```

**postlock** [**-c **config_dir] [**-l **lock_style]
        [**-v**] *file command...*

## DESCRIPTION


The **postlock**(1) command locks *file* for exclusive
access, and executes *command*. The locking method is
compatible with the Postfix UNIX-style local delivery agent.

Options:

- \fB-c
Read the **main.cf** configuration file in the named directory
instead of the default configuration directory.

- \fB-l
Override the locking method specified via the
**mailbox_delivery_lock** configuration parameter (see below).
**-v**
Enable verbose logging for debugging purposes. Multiple **-v**
options make the software increasingly verbose.

Arguments:
*file*
A mailbox file. The user should have read/write permission.
*command...*
The command to execute while *file* is locked for exclusive
access.  The command is executed directly, i.e. without
interpretation by a shell command interpreter.

## DIAGNOSTICS


The result status is 75 (EX_TEMPFAIL) when **postlock**(1)
could not perform the requested operation.  Otherwise, the
exit status is the exit status from the command.

## BUGS


With remote file systems, the ability to acquire a lock does not
necessarily eliminate access conflicts. Avoid file access by
processes running on different machines.

## ENVIRONMENT

.na

```
.ad
```

**MAIL_CONFIG**
Directory with Postfix configuration files.
**MAIL_VERBOSE**
Enable verbose logging for debugging purposes.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

The following **main.cf** parameters are especially relevant to
this program.
The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

## LOCKING CONTROLS

.na

```
.ad
```


- \fBdeliver_lock_attempts
The maximal number of attempts to acquire an exclusive lock on a
mailbox file or **bounce**(8) logfile.

- \fBdeliver_lock_delay
The time between attempts to acquire an exclusive lock on a mailbox
file or **bounce**(8) logfile.

- \fBstale_lock_time
The time after which a stale exclusive mailbox lockfile is removed.

- \fBmailbox_delivery_lock (see 'postconf -d'
How to lock a UNIX-style **local**(8) mailbox before attempting delivery.

## RESOURCE AND RATE CONTROLS

.na

```
.ad
```


- \fBfork_attempts
The maximal number of attempts to fork() a child process.

- \fBfork_delay
The delay between attempts to fork() a child process.

## MISCELLANEOUS CONTROLS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBimport_environment (see 'postconf -d'
The list of environment parameters that a privileged Postfix
process will import from a non-Postfix parent process, or name=value
environment overrides.

## SEE ALSO

.na

```
postconf(5), configuration parameters
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
