+++
author = "None Specified"
date = "2022-02-19"
manpage_name = "perlcn"
operating_system_version = "15.3"
manpage_format = "troff"
manpage_section = "1"
operating_system = "macos"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "encode", "cn", "encoding", "perluniintro", "perlunicode", "authors", "jarkko", "hietaniemi", "jhi", "iki", "fi", "audrey", "tang", "s-1xx", "s0", "audreyt", "org", "sizhe", "zhao", "prc", "outlook", "com"]
description = "s-1XXXXs0 Perl s-1XXXs0! X 5.8.0 s-1XXX,s0 Perl s-1XXXXXXs0 Unicode (s-1XXXs0) s-1XX, XXXXXXXXXXXXXXXXXXXs0; s-1CJKs0 (s-1XXXs0) s-1XXXXXXXX.s0 Unicode s-1XXXXXXX, XXXXXXXXXXXX: XXXX, XXXX, XXXXXXXXs0 (s-1XXX, XXXX, XXXX, XXXX, XXX, XXXX, XXs0). ..."
title = "perlcn(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLCN 1"
PERLCN 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"
\s-1XXXXXXXXXXXXXXXXXX, XXXXXXXXXXXX.
XXXXXX POD\s0 (\s-1XXXXXX\s0) \s-1XX\s0; \s-1XXXXXXXXXXXXXX,
XXXXXX. XXXXXXXXXXX, XXX\s0 perlpod \s-1XXXX.\s0

## NAME

perlcn - XXXX Perl XX

## DESCRIPTION

Header "DESCRIPTION"
\s-1XXXX\s0 Perl \s-1XXX\s0!

X 5.8.0 \s-1XXX,\s0 Perl \s-1XXXXXX\s0 Unicode (\s-1XXX\s0) \s-1XX,
XXXXXXXXXXXXXXXXXXX\s0; \s-1CJK\s0 (\s-1XXX\s0) \s-1XXXXXXXX.\s0
Unicode \s-1XXXXXXX, XXXXXXXXXXXX: XXXX, XXXX,
XXXXXXXX\s0 (\s-1XXX, XXXX, XXXX, XXXX, XXX,
XXXX, XX\s0). \s-1XXXXXXXXXXXXXX\s0 (X \s-1PC XXXX\s0).

Perl \s-1XXX\s0 Unicode \s-1XXXX. XXX\s0 Perl \s-1XXXXXXXXXX\s0 Unicode
\s-1XX\s0; Perl \s-1XXXXXXX\s0 (\s-1XXXXXXXXX\s0) \s-1XXX\s0 Unicode \s-1XXXX.
XXXXXXX, XXXXX\s0 Unicode \s-1XXXXXXXXXXXX,\s0 Perl
\s-1XXX\s0 Encode \s-1XXXX, XXXXXXXXXXXXXXXXXXXXX.\s0

Encode \s-1XXXXXXXXXXXXXXXXX\s0 ('gb2312' \s-1XX\s0 'euc-cn'):

.Vb 6
    euc-cn      Unix XXXXX, XXXXXXXXX
    gb2312-raw  XXXXX (XXX) GB2312 XXX
    gb12345     XXXXXXXXXXXXXX
    iso-ir-165  GB2312 + GB6345 + GB8565 + XXXX
    cp936       XXX 936, XXXX \*(AqGBK\*(Aq (XXXXX) XX
    hz          7 XXXXX GB2312 XX
.Ve

\s-1XXXX, X\s0 EUC-CN \s-1XXXXXXX\s0 Unicode, \s-1XXXXXXXX:\s0

.Vb 1
    perl -Mencoding=euc-cn,STDOUT,utf8 -pe1 < file.euc-cn > file.utf8
.Ve

Perl \s-1XXXX\s0 \*(L"piconv\*(R", \s-1XXXXX\s0 Perl \s-1XXXXXXXXXXX, XXXX:\s0

.Vb 2
    piconv -f euc-cn -t utf8 < file.euc-cn > file.utf8
    piconv -f utf8 -t euc-cn < file.utf8 > file.euc-cn
.Ve

\s-1XX, XX\s0 encoding \s-1XX, XXXXXXXXXXXXXXXX, XXXX:\s0

.Vb 7
    #!/usr/bin/env perl
    # XX euc-cn XXXX; XXXXXXXXXXXXX euc-cn XX
    use encoding \*(Aqeuc-cn\*(Aq, STDIN => \*(Aqeuc-cn\*(Aq, STDOUT => \*(Aqeuc-cn\*(Aq;
    print length("XX");      #  2 (XXXXXXX)
    print length(\*(AqXX\*(Aq);      #  4 (XXXXXXX)
    print index("XXXX", "XX"); # -1 (XXXXXXXX)
    print index(\*(AqXXXX\*(Aq, \*(AqXX\*(Aq); #  1 (XXXXXXXX)
.Ve

\s-1XXXXXXXX, \*(L"X\*(R" XXXXXXX \*(L"X\*(R" XXXXXXXXX\s0 EUC-CN
\s-1XX \*(L"X\*(R"\s0; \*(L"X\*(R" \s-1XXXXXXXX \*(L"X\*(R" XXXXXXXXX \*(L"X\*(R".
XXXXXX\s0 EUC-CN \s-1XXXXXXXXXXX.\s0

### \s-1XXXXXXX\s0

Subsection "XXXXXXX"
\s-1XXXXXXXXXXX, XXX CPAN\s0 (<https://www.cpan.org/>) \s-1XX\s0
Encode::HanExtra \s-1XX. XXXXXXXXXXX:\s0

.Vb 1
    gb18030     XXXXXXX, XXXXXX
.Ve

\s-1XX,\s0 Encode::HanConvert \s-1XXXXXXXXXXXXXXXX:\s0

.Vb 2
    big5-simp   Big5 XXXXX Unicode XXXXXX
    gbk-trad    GBK XXXXX Unicode XXXXXX
.Ve

\s-1XXX GBK X\s0 Big5 \s-1XXXX, XXXXXXXXX\s0 b2g.pl X g2b.pl \s-1XXXX,
XXXXXXXXXXX:\s0

.Vb 3
    use Encode::HanConvert;
    $euc_cn = big5_to_gb($big5); # X Big5 XX GBK
    $big5 = gb_to_big5($euc_cn); # X GBK XX Big5
.Ve

### \s-1XXXXXX\s0

Subsection "XXXXXX"
\s-1XXX\s0 Perl \s-1XXXXXXXXX\s0 (\s-1XXXXXXXXX\s0), \s-1XXXXXXX\s0
Perl \s-1XXX, XX\s0 Unicode \s-1XXXXX. XX, XXXXXXXXX:\s0

### \s-1XX\s0 Perl \s-1XXXXX\s0

Subsection "XX Perl XXXXX"

- <https://www.perl.org/>
Item "<https://www.perl.org/>"

Perl \s-1XXX\s0

- <https://www.perl.com/>
Item "<https://www.perl.com/>"
X Perl \s-1XXXXXXXXXX\s0

- <https://www.cpan.org/>
Item "<https://www.cpan.org/>"
Perl \s-1XXXXX\s0 (Comprehensive Perl Archive Network)

- <https://lists.perl.org/>
Item "<https://lists.perl.org/>"
Perl \s-1XXXXXX\s0

### \s-1XX\s0 Perl \s-1XXX\s0

Subsection "XX Perl XXX"

- <http://www.oreilly.com.cn/index.php?func=booklist&cat=68>
Item "<http://www.oreilly.com.cn/index.php?func=booklist&cat=68>"
\s-1XXXXXXXXX\s0 Perl \s-1XX\s0

### Perl \s-1XXXXX\s0

Subsection "Perl XXXXX"

- <https://www.pm.org/groups/asia.html>
Item "<https://www.pm.org/groups/asia.html>"
\s-1XX\s0 Perl \s-1XXXXX\s0

### Unicode \s-1XXXX\s0

Subsection "Unicode XXXX"

- <https://www.unicode.org/>
Item "<https://www.unicode.org/>"
Unicode \s-1XXXX\s0 (Unicode \s-1XXXXXX\s0)

- <https://www.cl.cam.ac.uk/%7Emgk25/unicode.html>
Item "<https://www.cl.cam.ac.uk/%7Emgk25/unicode.html>"
Unix/Linux \s-1XX UTF-8 X\s0 Unicode \s-1XXXXXX\s0

## SEE ALSO

Header "SEE ALSO"
Encode, Encode::CN, encoding, perluniintro, perlunicode

## AUTHORS

Header "AUTHORS"
Jarkko Hietaniemi <jhi@iki.fi>

Audrey Tang (\s-1XX\s0) <audreyt@audreyt.org>

Sizhe Zhao <prc.zhao@outlook.com>
