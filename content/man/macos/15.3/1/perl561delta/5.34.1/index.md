+++
detected_package_version = "5.34.1"
date = "2022-02-19"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "how", "to", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information", "history", "written", "by", "gurusamy", "sarathy", "figsar", "activestate", "com", "with", "many", "contributions", "from", "porters", "send", "omissions", "or", "corrections", "fiperlbug", "org"]
manpage_name = "perl561delta"
operating_system = "macos"
title = "perl561delta(1)"
manpage_section = "1"
description = "This document describes differences between the 5.005 release and the 5.6.1 release. This section contains a summary of the changes between the 5.6.0 release and the 5.6.1 release.  More details about the changes mentioned here may be found in the..."
operating_system_version = "15.3"
manpage_format = "troff"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL561DELTA 1"
PERL561DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl561delta - what's new for perl v5.6.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.005 release and the 5.6.1
release.

## Summary of changes between 5.6.0 and 5.6.1

Header "Summary of changes between 5.6.0 and 5.6.1"
This section contains a summary of the changes between the 5.6.0 release
and the 5.6.1 release.  More details about the changes mentioned here
may be found in the *Changes* files that accompany the Perl source
distribution.  See perlhack for pointers to online resources where you
can inspect the individual patches described by these changes.

### Security Issues

Subsection "Security Issues"
suidperl will not run /bin/mail anymore, because some platforms have
a /bin/mail that is vulnerable to buffer overflow attacks.

Note that suidperl is neither built nor installed by default in
any recent version of perl.  Use of suidperl is highly discouraged.
If you think you need it, try alternatives such as sudo first.
See http://www.courtesan.com/sudo/ .

### Core bug fixes

Subsection "Core bug fixes"
This is not an exhaustive list.  It is intended to cover only the
significant user-visible changes.
.ie n .IP """UNIVERSAL::isa()""" 4
.el .IP "\f(CWUNIVERSAL::isa()" 4
Item "UNIVERSAL::isa()"
A bug in the caching mechanism used by \f(CW\*(C`UNIVERSAL::isa()\*(C' that affected
base.pm has been fixed.  The bug has existed since the 5.005 releases,
but wasn't tickled by base.pm in those releases.

- Memory leaks
Item "Memory leaks"
Various cases of memory leaks and attempts to access uninitialized memory
have been cured.  See \*(L"Known Problems\*(R" below for further issues.

- Numeric conversions
Item "Numeric conversions"
Numeric conversions did not recognize changes in the string value
properly in certain circumstances.
.Sp
In other situations, large unsigned numbers (those above 2**31) could
sometimes lose their unsignedness, causing bogus results in arithmetic
operations.
.Sp
Integer modulus on large unsigned integers sometimes returned
incorrect values.
.Sp
Perl 5.6.0 generated \*(L"not a number\*(R" warnings on certain conversions where
previous versions didn't.
.Sp
These problems have all been rectified.
.Sp
Infinity is now recognized as a number.

- qw(a\\\\b)
Item "qw(ab)"
In Perl 5.6.0, qw(a\\\\b) produced a string with two backslashes instead
of one, in a departure from the behavior in previous versions.  The
older behavior has been reinstated.

- \fBcaller()
Item "caller()"
**caller()** could cause core dumps in certain situations.  Carp was sometimes
affected by this problem.

- Bugs in regular expressions
Item "Bugs in regular expressions"
Pattern matches on overloaded values are now handled correctly.
.Sp
Perl 5.6.0 parsed m/\\x\{ab\}/ incorrectly, leading to spurious warnings.
This has been corrected.
.Sp
The \s-1RE\s0 engine found in Perl 5.6.0 accidentally pessimised certain kinds
of simple pattern matches.  These are now handled better.
.Sp
Regular expression debug output (whether through \f(CW\*(C`use re \*(Aqdebug\*(Aq\*(C'
or via \f(CW\*(C`-Dr\*(C') now looks better.
.Sp
Multi-line matches like \f(CW\*(C`"a\\nxb\\n" =~ /(?!\\A)x/m\*(C' were flawed.  The
bug has been fixed.
.Sp
Use of $& could trigger a core dump under some situations.  This
is now avoided.
.Sp
Match variables \f(CW$1 et al., weren't being unset when a pattern match
was backtracking, and the anomaly showed up inside \f(CW\*(C`/...(?\{ ... \}).../\*(C'
etc.  These variables are now tracked correctly.
.Sp
**pos()** did not return the correct value within s///ge in earlier
versions.  This is now handled correctly.
.ie n .IP """slurp"" mode" 4
.el .IP "``slurp'' mode" 4
Item "slurp mode"
**readline()** on files opened in \*(L"slurp\*(R" mode could return an extra "" at
the end in certain situations.  This has been corrected.

- Autovivification of symbolic references to special variables
Item "Autovivification of symbolic references to special variables"
Autovivification of symbolic references of special variables described
in perlvar (as in \f(CW\*(C`$\{$num\}\*(C') was accidentally disabled.  This works
again now.

- Lexical warnings
Item "Lexical warnings"
Lexical warnings now propagate correctly into \f(CW\*(C`eval "..."\*(C'.
.Sp
\f(CW\*(C`use warnings qw(FATAL all)\*(C' did not work as intended.  This has been
corrected.
.Sp
Lexical warnings could leak into other scopes in some situations.
This is now fixed.
.Sp
**warnings::enabled()** now reports the state of $^W correctly if the caller
isn't using lexical warnings.

- Spurious warnings and errors
Item "Spurious warnings and errors"
Perl 5.6.0 could emit spurious warnings about redefinition of **dl_error()**
when statically building extensions into perl.  This has been corrected.
.Sp
\*(L"our\*(R" variables could result in bogus \*(L"Variable will not stay shared\*(R"
warnings.  This is now fixed.
.Sp
\*(L"our\*(R" variables of the same name declared in two sibling blocks
resulted in bogus warnings about \*(L"redeclaration\*(R" of the variables.
The problem has been corrected.

- \fBglob()
Item "glob()"
Compatibility of the builtin **glob()** with old csh-based glob has been
improved with the addition of \s-1GLOB_ALPHASORT\s0 option.  See \f(CW\*(C`File::Glob\*(C'.
.Sp
**File::Glob::glob()** has been renamed to **File::Glob::bsd_glob()**
because the name clashes with the builtin **glob()**.  The older
name is still available for compatibility, but is deprecated.
.Sp
Spurious syntax errors generated in certain situations, when **glob()**
caused File::Glob to be loaded for the first time, have been fixed.

- Tainting
Item "Tainting"
Some cases of inconsistent taint propagation (such as within hash
values) have been fixed.
.Sp
The tainting behavior of **sprintf()** has been rationalized.  It does
not taint the result of floating point formats anymore, making the
behavior consistent with that of string interpolation.

- \fBsort()
Item "sort()"
Arguments to **sort()** weren't being provided the right **wantarray()** context.
The comparison block is now run in scalar context, and the arguments to
be sorted are always provided list context.
.Sp
**sort()** is also fully reentrant, in the sense that the sort function
can itself call **sort()**.  This did not work reliably in previous releases.

- #line directives
Item "#line directives"
#line directives now work correctly when they appear at the very
beginning of \f(CW\*(C`eval "..."\*(C'.

- Subroutine prototypes
Item "Subroutine prototypes"
The (\) prototype now works properly.

- \fBmap()
Item "map()"
**map()** could get pathologically slow when the result list it generates
is larger than the source list.  The performance has been improved for
common scenarios.

- Debugger
Item "Debugger"
Debugger exit code now reflects the script exit code.
.Sp
Condition \f(CW"0" in breakpoints is now treated correctly.
.Sp
The \f(CW\*(C`d\*(C' command now checks the line number.
.Sp
\f(CW$. is no longer corrupted by the debugger.
.Sp
All debugger output now correctly goes to the socket if RemotePort
is set.

- \s-1PERL5OPT\s0
Item "PERL5OPT"
\s-1PERL5OPT\s0 can be set to more than one switch group.  Previously,
it used to be limited to one group of options only.

- \fBchop()
Item "chop()"
chop(@list) in list context returned the characters chopped in reverse
order.  This has been reversed to be in the right order.

- Unicode support
Item "Unicode support"
Unicode support has seen a large number of incremental improvements,
but continues to be highly experimental.  It is not expected to be
fully supported in the 5.6.x maintenance releases.
.Sp
**substr()**, **join()**, **repeat()**, **reverse()**, **quotemeta()** and string
concatenation were all handling Unicode strings incorrectly in
Perl 5.6.0.  This has been corrected.
.Sp
Support for \f(CW\*(C`tr///CU\*(C' and \f(CW\*(C`tr///UC\*(C' etc., have been removed since
we realized the interface is broken.  For similar functionality,
see \*(L"pack\*(R" in perlfunc.
.Sp
The Unicode Character Database has been updated to version 3.0.1
with additions made available to the public as of August 30, 2000.
.Sp
The Unicode character classes \\p\{Blank\} and \\p\{SpacePerl\} have been
added.  \*(L"Blank\*(R" is like C **isblank()**, that is, it contains only
\*(L"horizontal whitespace\*(R" (the space character is, the newline isn't),
and the \*(L"SpacePerl\*(R" is the Unicode equivalent of \f(CW\*(C`\\s\*(C' (\\p\{Space\}
isn't, since that includes the vertical tabulator character, whereas
\f(CW\*(C`\\s\*(C' doesn't.)
.Sp
If you are experimenting with Unicode support in perl, the development
versions of Perl may have more to offer.  In particular, I/O layers
are now available in the development track, but not in the maintenance
track, primarily to do backward compatibility issues.  Unicode support
is also evolving rapidly on a daily basis in the development track\*(--the
maintenance track only reflects the most conservative of these changes.

- 64-bit support
Item "64-bit support"
Support for 64-bit platforms has been improved, but continues to be
experimental.  The level of support varies greatly among platforms.

- Compiler
Item "Compiler"
The B Compiler and its various backends have had many incremental
improvements, but they continue to remain highly experimental.  Use in
production environments is discouraged.
.Sp
The perlcc tool has been rewritten so that the user interface is much
more like that of a C compiler.
.Sp
The perlbc tools has been removed.  Use \f(CW\*(C`perlcc -B\*(C' instead.

- Lvalue subroutines
Item "Lvalue subroutines"
There have been various bugfixes to support lvalue subroutines better.
However, the feature still remains experimental.

- IO::Socket
Item "IO::Socket"
IO::Socket::INET failed to open the specified port if the service
name was not known.  It now correctly uses the supplied port number
as is.

- File::Find
Item "File::Find"
File::Find now **chdir()**s correctly when chasing symbolic links.

- xsubpp
Item "xsubpp"
xsubpp now tolerates embedded \s-1POD\s0 sections.
.ie n .IP """no Module;""" 4
.el .IP "\f(CWno Module;" 4
Item "no Module;"
\f(CW\*(C`no Module;\*(C' does not produce an error even if Module does not have an
**unimport()** method.  This parallels the behavior of \f(CW\*(C`use\*(C' vis-a-vis
\f(CW\*(C`import\*(C'.

- Tests
Item "Tests"
A large number of tests have been added.

### Core features

Subsection "Core features"
**untie()** will now call an \s-1**UNTIE\s0()** hook if it exists.  See perltie
for details.

The \f(CW\*(C`-DT\*(C' command line switch outputs copious tokenizing information.
See perlrun.

Arrays are now always interpolated in double-quotish strings.  Previously,
\f(CW"foo@bar.com" used to be a fatal error at compile time, if an array
\f(CW@bar was not used or declared.  This transitional behavior was
intended to help migrate perl4 code, and is deemed to be no longer useful.
See \*(L"Arrays now always interpolate into double-quoted strings\*(R".

**keys()**, **each()**, **pop()**, **push()**, **shift()**, **splice()** and **unshift()**
can all be overridden now.

\f(CW\*(C`my _\|_PACKAGE_\|_ $obj\*(C' now does the expected thing.

### Configuration issues

Subsection "Configuration issues"
On some systems (\s-1IRIX\s0 and Solaris among them) the system malloc is demonstrably
better.  While the defaults haven't been changed in order to retain binary
compatibility with earlier releases, you may be better off building perl
with \f(CW\*(C`Configure -Uusemymalloc ...\*(C' as discussed in the *\s-1INSTALL\s0* file.

\f(CW\*(C`Configure\*(C' has been enhanced in various ways:

- \(bu
Minimizes use of temporary files.

- \(bu
By default, does not link perl with libraries not used by it, such as
the various dbm libraries.  SunOS 4.x hints preserve behavior on that
platform.

- \(bu
Support for pdp11-style memory models has been removed due to obsolescence.

- \(bu
Building outside the source tree is supported on systems that have
symbolic links. This is done by running
.Sp
.Vb 2
    sh /path/to/source/Configure -Dmksymlinks ...
    make all test install
.Ve
.Sp
in a directory other than the perl source directory.  See *\s-1INSTALL\s0*.

- \(bu
\f(CW\*(C`Configure -S\*(C' can be run non-interactively.

### Documentation

Subsection "Documentation"
\s-1README\s0.aix, \s-1README\s0.solaris and \s-1README\s0.macos have been added.
\s-1README\s0.posix-bc has been renamed to \s-1README\s0.bs2000.  These are
installed as perlaix, perlsolaris, perlmacos, and
perlbs2000 respectively.

The following pod documents are brand new:

.Vb 7
    perlclib    Internal replacements for standard C library functions
    perldebtut  Perl debugging tutorial
    perlebcdic  Considerations for running Perl on EBCDIC platforms
    perlnewmod  Perl modules: preparing a new module for distribution
    perlrequick Perl regular expressions quick start
    perlretut   Perl regular expressions tutorial
    perlutil    utilities packaged with the Perl distribution
.Ve

The *\s-1INSTALL\s0* file has been expanded to cover various issues, such as
64-bit support.

A longer list of contributors has been added to the source distribution.
See the file \f(CW\*(C`AUTHORS\*(C'.

Numerous other changes have been made to the included documentation and FAQs.

### Bundled modules

Subsection "Bundled modules"
The following modules have been added.

- B::Concise
Item "B::Concise"
Walks Perl syntax tree, printing concise info about ops.  See B::Concise.

- File::Temp
Item "File::Temp"
Returns name and handle of a temporary file safely.  See File::Temp.

- Pod::LaTeX
Item "Pod::LaTeX"
Converts Pod data to formatted LaTeX.  See Pod::LaTeX.

- Pod::Text::Overstrike
Item "Pod::Text::Overstrike"
Converts \s-1POD\s0 data to formatted overstrike text.  See Pod::Text::Overstrike.

The following modules have been upgraded.

- \s-1CGI\s0
Item "CGI"
\s-1CGI\s0 v2.752 is now included.

- \s-1CPAN\s0
Item "CPAN"
\s-1CPAN\s0 v1.59_54 is now included.

- Class::Struct
Item "Class::Struct"
Various bugfixes have been added.

- DB_File
Item "DB_File"
DB_File v1.75 supports newer Berkeley \s-1DB\s0 versions, among other
improvements.

- Devel::Peek
Item "Devel::Peek"
Devel::Peek has been enhanced to support dumping of memory statistics,
when perl is built with the included **malloc()**.

- File::Find
Item "File::Find"
File::Find now supports pre and post-processing of the files in order
to **sort()** them, etc.

- Getopt::Long
Item "Getopt::Long"
Getopt::Long v2.25 is included.

- IO::Poll
Item "IO::Poll"
Various bug fixes have been included.

- IPC::Open3
Item "IPC::Open3"
IPC::Open3 allows use of numeric file descriptors.

- Math::BigFloat
Item "Math::BigFloat"
The **fmod()** function supports modulus operations.  Various bug fixes
have also been included.

- Math::Complex
Item "Math::Complex"
Math::Complex handles inf, NaN etc., better.

- Net::Ping
Item "Net::Ping"
**ping()** could fail on odd number of data bytes, and when the echo service
isn't running.  This has been corrected.

- Opcode
Item "Opcode"
A memory leak has been fixed.

- Pod::Parser
Item "Pod::Parser"
Version 1.13 of the Pod::Parser suite is included.

- Pod::Text
Item "Pod::Text"
Pod::Text and related modules have been upgraded to the versions
in podlators suite v2.08.

- SDBM_File
Item "SDBM_File"
On dosish platforms, some keys went missing because of lack of support for
files with \*(L"holes\*(R".  A workaround for the problem has been added.

- Sys::Syslog
Item "Sys::Syslog"
Various bug fixes have been included.

- Tie::RefHash
Item "Tie::RefHash"
Now supports Tie::RefHash::Nestable to automagically tie hashref values.

- Tie::SubstrHash
Item "Tie::SubstrHash"
Various bug fixes have been included.

### Platform-specific improvements

Subsection "Platform-specific improvements"
The following new ports are now available.

- \s-1NCR\s0 MP-RAS
Item "NCR MP-RAS"
0

- NonStop-UX
Item "NonStop-UX"
.PD

Perl now builds under Amdahl \s-1UTS.\s0

Perl has also been verified to build under Amiga \s-1OS.\s0

Support for \s-1EPOC\s0 has been much improved.  See \s-1README\s0.epoc.

Building perl with -Duseithreads or -Duse5005threads now works
under HP-UX 10.20 (previously it only worked under 10.30 or later).
You will need a thread library package installed.  See \s-1README\s0.hpux.

Long doubles should now work under Linux.

Mac \s-1OS\s0 Classic is now supported in the mainstream source package.
See \s-1README\s0.macos.

Support for MPE/iX has been updated.  See \s-1README\s0.mpeix.

Support for \s-1OS/2\s0 has been improved.  See \f(CW\*(C`os2/Changes\*(C' and \s-1README\s0.os2.

Dynamic loading on z/OS (formerly \s-1OS/390\s0) has been improved.  See
\s-1README\s0.os390.

Support for \s-1VMS\s0 has seen many incremental improvements, including
better support for operators like backticks and **system()**, and better
\f(CW%ENV handling.  See \f(CW\*(C`README.vms\*(C' and perlvms.

Support for Stratus \s-1VOS\s0 has been improved.  See \f(CW\*(C`vos/Changes\*(C' and \s-1README\s0.vos.

Support for Windows has been improved.

- \(bu
**fork()** emulation has been improved in various ways, but still continues
to be experimental.  See perlfork for known bugs and caveats.

- \(bu
\f(CW%SIG has been enabled under \s-1USE_ITHREADS,\s0 but its use is completely
unsupported under all configurations.

- \(bu
Borland \*(C+ v5.5 is now a supported compiler that can build Perl.
However, the generated binaries continue to be incompatible with those
generated by the other supported compilers (\s-1GCC\s0 and Visual \*(C+).

- \(bu
Non-blocking waits for child processes (or pseudo-processes) are
supported via \f(CW\*(C`waitpid($pid, &POSIX::WNOHANG)\*(C'.

- \(bu
A memory leak in **accept()** has been fixed.

- \(bu
**wait()**, **waitpid()** and backticks now return the correct exit status under
Windows 9x.

- \(bu
Trailing new \f(CW%ENV entries weren't propagated to child processes.  This
is now fixed.

- \(bu
Current directory entries in \f(CW%ENV are now correctly propagated to child
processes.

- \(bu
Duping socket handles with open(F, \*(L">&MYSOCK\*(R") now works under Windows 9x.

- \(bu
The makefiles now provide a single switch to bulk-enable all the features
enabled in ActiveState ActivePerl (a popular binary distribution).

- \(bu
**Win32::GetCwd()** correctly returns C:\\ instead of C: when at the drive root.
Other bugs in **chdir()** and **Cwd::cwd()** have also been fixed.

- \(bu
**fork()** correctly returns undef and sets \s-1EAGAIN\s0 when it runs out of
pseudo-process handles.

- \(bu
ExtUtils::MakeMaker now uses \f(CW$ENV\{\s-1LIB\s0\} to search for libraries.

- \(bu
\s-1UNC\s0 path handling is better when perl is built to support **fork()**.

- \(bu
A handle leak in socket handling has been fixed.

- \(bu
**send()** works from within a pseudo-process.

Unless specifically qualified otherwise, the remainder of this document
covers changes between the 5.005 and 5.6.0 releases.

## Core Enhancements

Header "Core Enhancements"

### Interpreter cloning, threads, and concurrency

Subsection "Interpreter cloning, threads, and concurrency"
Perl 5.6.0 introduces the beginnings of support for running multiple
interpreters concurrently in different threads.  In conjunction with
the **perl_clone()** \s-1API\s0 call, which can be used to selectively duplicate
the state of any given interpreter, it is possible to compile a
piece of code once in an interpreter, clone that interpreter
one or more times, and run all the resulting interpreters in distinct
threads.

On the Windows platform, this feature is used to emulate **fork()** at the
interpreter level.  See perlfork for details about that.

This feature is still in evolution.  It is eventually meant to be used
to selectively clone a subroutine and data reachable from that
subroutine in a separate interpreter and run the cloned subroutine
in a separate thread.  Since there is no shared data between the
interpreters, little or no locking will be needed (unless parts of
the symbol table are explicitly shared).  This is obviously intended
to be an easy-to-use replacement for the existing threads support.

Support for cloning interpreters and interpreter concurrency can be
enabled using the -Dusethreads Configure option (see win32/Makefile for
how to enable it on Windows.)  The resulting perl executable will be
functionally identical to one that was built with -Dmultiplicity, but
the **perl_clone()** \s-1API\s0 call will only be available in the former.

-Dusethreads enables the cpp macro \s-1USE_ITHREADS\s0 by default, which in turn
enables Perl source code changes that provide a clear separation between
the op tree and the data it operates with.  The former is immutable, and
can therefore be shared between an interpreter and all of its clones,
while the latter is considered local to each interpreter, and is therefore
copied for each clone.

Note that building Perl with the -Dusemultiplicity Configure option
is adequate if you wish to run multiple **independent** interpreters
concurrently in different threads.  -Dusethreads only provides the
additional functionality of the **perl_clone()** \s-1API\s0 call and other
support for running **cloned** interpreters concurrently.

.Vb 2
    NOTE: This is an experimental feature.  Implementation details are
    subject to change.
.Ve

### Lexically scoped warning categories

Subsection "Lexically scoped warning categories"
You can now control the granularity of warnings emitted by perl at a finer
level using the \f(CW\*(C`use warnings\*(C' pragma.  warnings and perllexwarn
have copious documentation on this feature.

### Unicode and \s-1UTF-8\s0 support

Subsection "Unicode and UTF-8 support"
Perl now uses \s-1UTF-8\s0 as its internal representation for character
strings.  The \f(CW\*(C`utf8\*(C' and \f(CW\*(C`bytes\*(C' pragmas are used to control this support
in the current lexical scope.  See perlunicode, utf8 and bytes for
more information.

This feature is expected to evolve quickly to support some form of I/O
disciplines that can be used to specify the kind of input and output data
(bytes or characters).  Until that happens, additional modules from \s-1CPAN\s0
will be needed to complete the toolkit for dealing with Unicode.

.Vb 2
    NOTE: This should be considered an experimental feature.  Implementation
    details are subject to change.
.Ve

### Support for interpolating named characters

Subsection "Support for interpolating named characters"
The new \f(CW\*(C`\\N\*(C' escape interpolates named characters within strings.
For example, \f(CW"Hi! \\N\{WHITE SMILING FACE\}" evaluates to a string
with a Unicode smiley face at the end.
.ie n .SS """our"" declarations"
.el .SS "``our'' declarations"
Subsection "our declarations"
An \*(L"our\*(R" declaration introduces a value that can be best understood
as a lexically scoped symbolic alias to a global variable in the
package that was current where the variable was declared.  This is
mostly useful as an alternative to the \f(CW\*(C`vars\*(C' pragma, but also provides
the opportunity to introduce typing and other attributes for such
variables.  See \*(L"our\*(R" in perlfunc.

### Support for strings represented as a vector of ordinals

Subsection "Support for strings represented as a vector of ordinals"
Literals of the form \f(CW\*(C`v1.2.3.4\*(C' are now parsed as a string composed
of characters with the specified ordinals.  This is an alternative, more
readable way to construct (possibly Unicode) strings instead of
interpolating characters, as in \f(CW"\\x\{1\}\\x\{2\}\\x\{3\}\\x\{4\}".  The leading
\f(CW\*(C`v\*(C' may be omitted if there are more than two ordinals, so \f(CW1.2.3 is
parsed the same as \f(CW\*(C`v1.2.3\*(C'.

Strings written in this form are also useful to represent version \*(L"numbers\*(R".
It is easy to compare such version \*(L"numbers\*(R" (which are really just plain
strings) using any of the usual string comparison operators \f(CW\*(C`eq\*(C', \f(CW\*(C`ne\*(C',
\f(CW\*(C`lt\*(C', \f(CW\*(C`gt\*(C', etc., or perform bitwise string operations on them using \f(CW\*(C`|\*(C',
\f(CW\*(C`&\*(C', etc.

In conjunction with the new \f(CW$^V magic variable (which contains
the perl version as a string), such literals can be used as a readable way
to check if you're running a particular version of Perl:

.Vb 4
    # this will parse in older versions of Perl also
    if ($^V and $^V gt v5.6.0) \{
        # new features supported
    \}
.Ve

\f(CW\*(C`require\*(C' and \f(CW\*(C`use\*(C' also have some special magic to support such literals.
They will be interpreted as a version rather than as a module name:

.Vb 2
    require v5.6.0;             # croak if $^V lt v5.6.0
    use v5.6.0;                 # same, but croaks at compile-time
.Ve

Alternatively, the \f(CW\*(C`v\*(C' may be omitted if there is more than one dot:

.Vb 2
    require 5.6.0;
    use 5.6.0;
.Ve

Also, \f(CW\*(C`sprintf\*(C' and \f(CW\*(C`printf\*(C' support the Perl-specific format flag \f(CW%v
to print ordinals of characters in arbitrary strings:

.Vb 3
    printf "v%vd", $^V;         # prints current version, such as "v5.5.650"
    printf "%*vX", ":", $addr;  # formats IPv6 address
    printf "%*vb", " ", $bits;  # displays bitstring
.Ve

See \*(L"Scalar value constructors\*(R" in perldata for additional information.

### Improved Perl version numbering system

Subsection "Improved Perl version numbering system"
Beginning with Perl version 5.6.0, the version number convention has been
changed to a \*(L"dotted integer\*(R" scheme that is more commonly found in open
source projects.

Maintenance versions of v5.6.0 will be released as v5.6.1, v5.6.2 etc.
The next development series following v5.6.0 will be numbered v5.7.x,
beginning with v5.7.0, and the next major production release following
v5.6.0 will be v5.8.0.

The English module now sets \f(CW$PERL_VERSION to $^V (a string value) rather
than \f(CW$] (a numeric value).  (This is a potential incompatibility.
Send us a report via perlbug if you are affected by this.)

The v1.2.3 syntax is also now legal in Perl.
See \*(L"Support for strings represented as a vector of ordinals\*(R" for more on that.

To cope with the new versioning system's use of at least three significant
digits for each version component, the method used for incrementing the
subversion number has also changed slightly.  We assume that versions older
than v5.6.0 have been incrementing the subversion component in multiples of
10.  Versions after v5.6.0 will increment them by 1.  Thus, using the new
notation, 5.005_03 is the \*(L"same\*(R" as v5.5.30, and the first maintenance
version following v5.6.0 will be v5.6.1 (which should be read as being
equivalent to a floating point value of 5.006_001 in the older format,
stored in \f(CW$]).

### New syntax for declaring subroutine attributes

Subsection "New syntax for declaring subroutine attributes"
Formerly, if you wanted to mark a subroutine as being a method call or
as requiring an automatic **lock()** when it is entered, you had to declare
that with a \f(CW\*(C`use attrs\*(C' pragma in the body of the subroutine.
That can now be accomplished with declaration syntax, like this:

.Vb 5
    sub mymethod : locked method;
    ...
    sub mymethod : locked method \{
        ...
    \}

    sub othermethod :locked :method;
    ...
    sub othermethod :locked :method \{
        ...
    \}
.Ve

(Note how only the first \f(CW\*(C`:\*(C' is mandatory, and whitespace surrounding
the \f(CW\*(C`:\*(C' is optional.)

*AutoSplit.pm* and *SelfLoader.pm* have been updated to keep the attributes
with the stubs they provide.  See attributes.

### File and directory handles can be autovivified

Subsection "File and directory handles can be autovivified"
Similar to how constructs such as \f(CW\*(C`$x->[0]\*(C' autovivify a reference,
handle constructors (**open()**, **opendir()**, **pipe()**, **socketpair()**, **sysopen()**,
**socket()**, and **accept()**) now autovivify a file or directory handle
if the handle passed to them is an uninitialized scalar variable.  This
allows the constructs such as \f(CW\*(C`open(my $fh, ...)\*(C' and \f(CW\*(C`open(local $fh,...)\*(C'
to be used to create filehandles that will conveniently be closed
automatically when the scope ends, provided there are no other references
to them.  This largely eliminates the need for typeglobs when opening
filehandles that must be passed around, as in the following example:

.Vb 5
    sub myopen \{
        open my $fh, "@_"
             or die "Can\*(Aqt open \*(Aq@_\*(Aq: $!";
        return $fh;
    \}

    \{
        my $f = myopen("</etc/motd");
        print <$f>;
        # $f implicitly closed here
    \}
.Ve

### \fBopen() with more than two arguments

Subsection "open() with more than two arguments"
If **open()** is passed three arguments instead of two, the second argument
is used as the mode and the third argument is taken to be the file name.
This is primarily useful for protecting against unintended magic behavior
of the traditional two-argument form.  See \*(L"open\*(R" in perlfunc.

### 64-bit support

Subsection "64-bit support"
Any platform that has 64-bit integers either

.Vb 3
        (1) natively as longs or ints
        (2) via special compiler flags
        (3) using long long or int64_t
.Ve

is able to use \*(L"quads\*(R" (64-bit integers) as follows:

- \(bu
constants (decimal, hexadecimal, octal, binary) in the code

- \(bu
arguments to **oct()** and **hex()**

- \(bu
arguments to **print()**, **printf()** and **sprintf()** (flag prefixes ll, L, q)

- \(bu
printed as such

- \(bu
**pack()** and **unpack()** \*(L"q\*(R" and \*(L"Q\*(R" formats

- \(bu
in basic arithmetics: + - * / % (\s-1NOTE:\s0 operating close to the limits
of the integer values may produce surprising results)

- \(bu
in bit arithmetics: & | ^ ~ << >> (\s-1NOTE:\s0 these used to be forced
to be 32 bits wide but now operate on the full native width.)

- \(bu
**vec()**

Note that unless you have the case (a) you will have to configure
and compile Perl using the -Duse64bitint Configure flag.

.Vb 2
    NOTE: The Configure flags -Duselonglong and -Duse64bits have been
    deprecated.  Use -Duse64bitint instead.
.Ve

There are actually two modes of 64-bitness: the first one is achieved
using Configure -Duse64bitint and the second one using Configure
-Duse64bitall.  The difference is that the first one is minimal and
the second one maximal.  The first works in more places than the second.

The \f(CW\*(C`use64bitint\*(C' does only as much as is required to get 64-bit
integers into Perl (this may mean, for example, using \*(L"long longs\*(R")
while your memory may still be limited to 2 gigabytes (because your
pointers could still be 32-bit).  Note that the name \f(CW\*(C`64bitint\*(C' does
not imply that your C compiler will be using 64-bit \f(CW\*(C`int\*(C's (it might,
but it doesn't have to): the \f(CW\*(C`use64bitint\*(C' means that you will be
able to have 64 bits wide scalar values.

The \f(CW\*(C`use64bitall\*(C' goes all the way by attempting to switch also
integers (if it can), longs (and pointers) to being 64-bit.  This may
create an even more binary incompatible Perl than -Duse64bitint: the
resulting executable may not run at all in a 32-bit box, or you may
have to reboot/reconfigure/rebuild your operating system to be 64-bit
aware.

Natively 64-bit systems like Alpha and Cray need neither -Duse64bitint
nor -Duse64bitall.

Last but not least: note that due to Perl's habit of always using
floating point numbers, the quads are still not true integers.
When quads overflow their limits (0...18_446_744_073_709_551_615 unsigned,
-9_223_372_036_854_775_808...9_223_372_036_854_775_807 signed), they
are silently promoted to floating point numbers, after which they will
start losing precision (in their lower digits).

.Vb 4
    NOTE: 64-bit support is still experimental on most platforms.
    Existing support only covers the LP64 data model.  In particular, the
    LLP64 data model is not yet supported.  64-bit libraries and system
    APIs on many platforms have not stabilized--your mileage may vary.
.Ve

### Large file support

Subsection "Large file support"
If you have filesystems that support \*(L"large files\*(R" (files larger than
2 gigabytes), you may now also be able to create and access them from
Perl.

.Vb 2
    NOTE: The default action is to enable large file support, if
    available on the platform.
.Ve

If the large file support is on, and you have a Fcntl constant
O_LARGEFILE, the O_LARGEFILE is automatically added to the flags
of **sysopen()**.

Beware that unless your filesystem also supports \*(L"sparse files\*(R" seeking
to umpteen petabytes may be inadvisable.

Note that in addition to requiring a proper file system to do large
files you may also need to adjust your per-process (or your
per-system, or per-process-group, or per-user-group) maximum filesize
limits before running Perl scripts that try to handle large files,
especially if you intend to write such files.

Finally, in addition to your process/process group maximum filesize
limits, you may have quota limits on your filesystems that stop you
(your user id or your user group id) from using large files.

Adjusting your process/user/group/file system/operating system limits
is outside the scope of Perl core language.  For process limits, you
may try increasing the limits using your shell's limits/limit/ulimit
command before running Perl.  The BSD::Resource extension (not
included with the standard Perl distribution) may also be of use, it
offers the getrlimit/setrlimit interface that can be used to adjust
process resource usage limits, including the maximum filesize limit.

### Long doubles

Subsection "Long doubles"
In some systems you may be able to use long doubles to enhance the
range and precision of your double precision floating point numbers
(that is, Perl's numbers).  Use Configure -Duselongdouble to enable
this support (if it is available).
.ie n .SS """more bits"""
.el .SS "``more bits''"
Subsection "more bits"
You can \*(L"Configure -Dusemorebits\*(R" to turn on both the 64-bit support
and the long double support.

### Enhanced support for \fBsort() subroutines

Subsection "Enhanced support for sort() subroutines"
Perl subroutines with a prototype of \f(CW\*(C`($$)\*(C', and XSUBs in general, can
now be used as sort subroutines.  In either case, the two elements to
be compared are passed as normal parameters in \f(CW@_.  See \*(L"sort\*(R" in perlfunc.

For unprototyped sort subroutines, the historical behavior of passing
the elements to be compared as the global variables \f(CW$a and \f(CW$b remains
unchanged.
.ie n .SS """sort $coderef @foo"" allowed"
.el .SS "\f(CWsort $coderef @foo allowed"
Subsection "sort $coderef @foo allowed"
**sort()** did not accept a subroutine reference as the comparison
function in earlier versions.  This is now permitted.

### File globbing implemented internally

Subsection "File globbing implemented internally"
Perl now uses the File::Glob implementation of the **glob()** operator
automatically.  This avoids using an external csh process and the
problems associated with it.

.Vb 2
    NOTE: This is currently an experimental feature.  Interfaces and
    implementation are subject to change.
.Ve

### Support for \s-1CHECK\s0 blocks

Subsection "Support for CHECK blocks"
In addition to \f(CW\*(C`BEGIN\*(C', \f(CW\*(C`INIT\*(C', \f(CW\*(C`END\*(C', \f(CW\*(C`DESTROY\*(C' and \f(CW\*(C`AUTOLOAD\*(C',
subroutines named \f(CW\*(C`CHECK\*(C' are now special.  These are queued up during
compilation and behave similar to \s-1END\s0 blocks, except they are called at
the end of compilation rather than at the end of execution.  They cannot
be called directly.

### \s-1POSIX\s0 character class syntax [: :] supported

Subsection "POSIX character class syntax [: :] supported"
For example to match alphabetic characters use /[[:alpha:]]/.
See perlre for details.

### Better pseudo-random number generator

Subsection "Better pseudo-random number generator"
In 5.005_0x and earlier, perl's **rand()** function used the C library
**rand**\|(3) function.  As of 5.005_52, Configure tests for **drand48()**,
**random()**, and **rand()** (in that order) and picks the first one it finds.

These changes should result in better random numbers from **rand()**.
.ie n .SS "Improved ""qw//"" operator"
.el .SS "Improved \f(CWqw// operator"
Subsection "Improved qw// operator"
The \f(CW\*(C`qw//\*(C' operator is now evaluated at compile time into a true list
instead of being replaced with a run time call to \f(CW\*(C`split()\*(C'.  This
removes the confusing misbehaviour of \f(CW\*(C`qw//\*(C' in scalar context, which
had inherited that behaviour from **split()**.

Thus:

.Vb 1
    $foo = ($bar) = qw(a b c); print "$foo|$bar\\n";
.Ve

now correctly prints \*(L"3|a\*(R", instead of \*(L"2|a\*(R".

### Better worst-case behavior of hashes

Subsection "Better worst-case behavior of hashes"
Small changes in the hashing algorithm have been implemented in
order to improve the distribution of lower order bits in the
hashed value.  This is expected to yield better performance on
keys that are repeated sequences.

### \fBpack() format Z supported

Subsection "pack() format 'Z' supported"
The new format type 'Z' is useful for packing and unpacking null-terminated
strings.  See \*(L"pack\*(R" in perlfunc.

### \fBpack() format modifier ! supported

Subsection "pack() format modifier '!' supported"
The new format type modifier '!' is useful for packing and unpacking
native shorts, ints, and longs.  See \*(L"pack\*(R" in perlfunc.

### \fBpack() and \fBunpack() support counted strings

Subsection "pack() and unpack() support counted strings"
The template character '/' can be used to specify a counted string
type to be packed or unpacked.  See \*(L"pack\*(R" in perlfunc.

### Comments in \fBpack() templates

Subsection "Comments in pack() templates"
The '#' character in a template introduces a comment up to
end of the line.  This facilitates documentation of **pack()**
templates.

### Weak references

Subsection "Weak references"
In previous versions of Perl, you couldn't cache objects so as
to allow them to be deleted if the last reference from outside
the cache is deleted.  The reference in the cache would hold a
reference count on the object and the objects would never be
destroyed.

Another familiar problem is with circular references.  When an
object references itself, its reference count would never go
down to zero, and it would not get destroyed until the program
is about to exit.

Weak references solve this by allowing you to \*(L"weaken\*(R" any
reference, that is, make it not count towards the reference count.
When the last non-weak reference to an object is deleted, the object
is destroyed and all the weak references to the object are
automatically undef-ed.

To use this feature, you need the Devel::WeakRef package from \s-1CPAN,\s0 which
contains additional documentation.

.Vb 1
    NOTE: This is an experimental feature.  Details are subject to change.
.Ve

### Binary numbers supported

Subsection "Binary numbers supported"
Binary numbers are now supported as literals, in s?printf formats, and
\f(CW\*(C`oct()\*(C':

.Vb 2
    $answer = 0b101010;
    printf "The answer is: %b\\n", oct("0b101010");
.Ve

### Lvalue subroutines

Subsection "Lvalue subroutines"
Subroutines can now return modifiable lvalues.
See \*(L"Lvalue subroutines\*(R" in perlsub.

.Vb 1
    NOTE: This is an experimental feature.  Details are subject to change.
.Ve

### Some arrows may be omitted in calls through references

Subsection "Some arrows may be omitted in calls through references"
Perl now allows the arrow to be omitted in many constructs
involving subroutine calls through references.  For example,
\f(CW\*(C`$foo[10]->(\*(Aqfoo\*(Aq)\*(C' may now be written \f(CW\*(C`$foo[10](\*(Aqfoo\*(Aq)\*(C'.
This is rather similar to how the arrow may be omitted from
\f(CW\*(C`$foo[10]->\{\*(Aqfoo\*(Aq\}\*(C'.  Note however, that the arrow is still
required for \f(CW\*(C`foo(10)->(\*(Aqbar\*(Aq)\*(C'.

### Boolean assignment operators are legal lvalues

Subsection "Boolean assignment operators are legal lvalues"
Constructs such as \f(CW\*(C`($a ||= 2) += 1\*(C' are now allowed.

### \fBexists() is supported on subroutine names

Subsection "exists() is supported on subroutine names"
The **exists()** builtin now works on subroutine names.  A subroutine
is considered to exist if it has been declared (even if implicitly).
See \*(L"exists\*(R" in perlfunc for examples.

### \fBexists() and \fBdelete() are supported on array elements

Subsection "exists() and delete() are supported on array elements"
The **exists()** and **delete()** builtins now work on simple arrays as well.
The behavior is similar to that on hash elements.

**exists()** can be used to check whether an array element has been
initialized.  This avoids autovivifying array elements that don't exist.
If the array is tied, the \s-1**EXISTS\s0()** method in the corresponding tied
package will be invoked.

**delete()** may be used to remove an element from the array and return
it.  The array element at that position returns to its uninitialized
state, so that testing for the same element with **exists()** will return
false.  If the element happens to be the one at the end, the size of
the array also shrinks up to the highest element that tests true for
**exists()**, or 0 if none such is found.  If the array is tied, the \s-1**DELETE\s0()**
method in the corresponding tied package will be invoked.

See \*(L"exists\*(R" in perlfunc and \*(L"delete\*(R" in perlfunc for examples.

### Pseudo-hashes work better

Subsection "Pseudo-hashes work better"
Dereferencing some types of reference values in a pseudo-hash,
such as \f(CW\*(C`$ph->\{foo\}[1]\*(C', was accidentally disallowed.  This has
been corrected.

When applied to a pseudo-hash element, **exists()** now reports whether
the specified value exists, not merely if the key is valid.

**delete()** now works on pseudo-hashes.  When given a pseudo-hash element
or slice it deletes the values corresponding to the keys (but not the keys
themselves).  See \*(L"Pseudo-hashes: Using an array as a hash\*(R" in perlref.

Pseudo-hash slices with constant keys are now optimized to array lookups
at compile-time.

List assignments to pseudo-hash slices are now supported.

The \f(CW\*(C`fields\*(C' pragma now provides ways to create pseudo-hashes, via
**fields::new()** and **fields::phash()**.  See fields.

.Vb 3
    NOTE: The pseudo-hash data type continues to be experimental.
    Limiting oneself to the interface elements provided by the
    fields pragma will provide protection from any future changes.
.Ve

### Automatic flushing of output buffers

Subsection "Automatic flushing of output buffers"
**fork()**, **exec()**, **system()**, qx//, and pipe **open()**s now flush buffers
of all files opened for output when the operation was attempted.  This
mostly eliminates confusing buffering mishaps suffered by users unaware
of how Perl internally handles I/O.

This is not supported on some platforms like Solaris where a suitably
correct implementation of fflush(\s-1NULL\s0) isn't available.

### Better diagnostics on meaningless filehandle operations

Subsection "Better diagnostics on meaningless filehandle operations"
Constructs such as \f(CW\*(C`open(<FH>)\*(C' and \f(CW\*(C`close(<FH>)\*(C'
are compile time errors.  Attempting to read from filehandles that
were opened only for writing will now produce warnings (just as
writing to read-only filehandles does).

### Where possible, buffered data discarded from duped input filehandle

Subsection "Where possible, buffered data discarded from duped input filehandle"
\f(CW\*(C`open(NEW, "<&OLD")\*(C' now attempts to discard any data that
was previously read and buffered in \f(CW\*(C`OLD\*(C' before duping the handle.
On platforms where doing this is allowed, the next read operation
on \f(CW\*(C`NEW\*(C' will return the same data as the corresponding operation
on \f(CW\*(C`OLD\*(C'.  Formerly, it would have returned the data from the start
of the following disk block instead.

### \fBeof() has the same old magic as <>

Subsection "eof() has the same old magic as <>"
\f(CW\*(C`eof()\*(C' would return true if no attempt to read from \f(CW\*(C`<>\*(C' had
yet been made.  \f(CW\*(C`eof()\*(C' has been changed to have a little magic of its
own, it now opens the \f(CW\*(C`<>\*(C' files.

### \fBbinmode() can be used to set :crlf and :raw modes

Subsection "binmode() can be used to set :crlf and :raw modes"
**binmode()** now accepts a second argument that specifies a discipline
for the handle in question.  The two pseudo-disciplines \*(L":raw\*(R" and
\*(L":crlf\*(R" are currently supported on DOS-derivative platforms.
See \*(L"binmode\*(R" in perlfunc and open.
.ie n .SS """-T"" filetest recognizes \s-1UTF-8\s0 encoded files as ""text"""
.el .SS "\f(CW-T filetest recognizes \s-1UTF-8\s0 encoded files as ``text''"
Subsection "-T filetest recognizes UTF-8 encoded files as text"
The algorithm used for the \f(CW\*(C`-T\*(C' filetest has been enhanced to
correctly identify \s-1UTF-8\s0 content as \*(L"text\*(R".

### \fBsystem(), backticks and pipe open now reflect \fBexec() failure

Subsection "system(), backticks and pipe open now reflect exec() failure"
On Unix and similar platforms, **system()**, **qx()** and open(\s-1FOO,\s0 \*(L"cmd |\*(R")
etc., are implemented via **fork()** and **exec()**.  When the underlying
**exec()** fails, earlier versions did not report the error properly,
since the **exec()** happened to be in a different process.

The child process now communicates with the parent about the
error in launching the external command, which allows these
constructs to return with their usual error value and set $!.

### Improved diagnostics

Subsection "Improved diagnostics"
Line numbers are no longer suppressed (under most likely circumstances)
during the global destruction phase.

Diagnostics emitted from code running in threads other than the main
thread are now accompanied by the thread \s-1ID.\s0

Embedded null characters in diagnostics now actually show up.  They
used to truncate the message in prior versions.

\f(CW$foo::a and \f(CW$foo::b are now exempt from \*(L"possible typo\*(R" warnings only
if **sort()** is encountered in package \f(CW\*(C`foo\*(C'.

Unrecognized alphabetic escapes encountered when parsing quote
constructs now generate a warning, since they may take on new
semantics in later versions of Perl.

Many diagnostics now report the internal operation in which the warning
was provoked, like so:

.Vb 2
    Use of uninitialized value in concatenation (.) at (eval 1) line 1.
    Use of uninitialized value in print at (eval 1) line 1.
.Ve

Diagnostics  that occur within eval may also report the file and line
number where the eval is located, in addition to the eval sequence
number and the line number within the evaluated text itself.  For
example:

.Vb 1
    Not enough arguments for scalar at (eval 4)[newlib/perl5db.pl:1411] line 2, at EOF
.Ve

### Diagnostics follow \s-1STDERR\s0

Subsection "Diagnostics follow STDERR"
Diagnostic output now goes to whichever file the \f(CW\*(C`STDERR\*(C' handle
is pointing at, instead of always going to the underlying C runtime
library's \f(CW\*(C`stderr\*(C'.

### More consistent close-on-exec behavior

Subsection "More consistent close-on-exec behavior"
On systems that support a close-on-exec flag on filehandles, the
flag is now set for any handles created by **pipe()**, **socketpair()**,
**socket()**, and **accept()**, if that is warranted by the value of $^F
that may be in effect.  Earlier versions neglected to set the flag
for handles created with these operators.  See \*(L"pipe\*(R" in perlfunc,
\*(L"socketpair\*(R" in perlfunc, \*(L"socket\*(R" in perlfunc, \*(L"accept\*(R" in perlfunc,
and \*(L"$^F\*(R" in perlvar.

### \fBsyswrite() ease-of-use

Subsection "syswrite() ease-of-use"
The length argument of \f(CW\*(C`syswrite()\*(C' has become optional.

### Better syntax checks on parenthesized unary operators

Subsection "Better syntax checks on parenthesized unary operators"
Expressions such as:

.Vb 3
    print defined(&foo,&bar,&baz);
    print uc("foo","bar","baz");
    undef($foo,&bar);
.Ve

used to be accidentally allowed in earlier versions, and produced
unpredictable behaviour.  Some produced ancillary warnings
when used in this way; others silently did the wrong thing.

The parenthesized forms of most unary operators that expect a single
argument now ensure that they are not called with more than one
argument, making the cases shown above syntax errors.  The usual
behaviour of:

.Vb 3
    print defined &foo, &bar, &baz;
    print uc "foo", "bar", "baz";
    undef $foo, &bar;
.Ve

remains unchanged.  See perlop.

### Bit operators support full native integer width

Subsection "Bit operators support full native integer width"
The bit operators (& | ^ ~ << >>) now operate on the full native
integral width (the exact size of which is available in \f(CW$Config\{ivsize\}).
For example, if your platform is either natively 64-bit or if Perl
has been configured to use 64-bit integers, these operations apply
to 8 bytes (as opposed to 4 bytes on 32-bit platforms).
For portability, be sure to mask off the excess bits in the result of
unary \f(CW\*(C`~\*(C', e.g., \f(CW\*(C`~$x & 0xffffffff\*(C'.

### Improved security features

Subsection "Improved security features"
More potentially unsafe operations taint their results for improved
security.

The \f(CW\*(C`passwd\*(C' and \f(CW\*(C`shell\*(C' fields returned by the **getpwent()**, **getpwnam()**,
and **getpwuid()** are now tainted, because the user can affect their own
encrypted password and login shell.

The variable modified by **shmread()**, and messages returned by **msgrcv()**
(and its object-oriented interface IPC::SysV::Msg::rcv) are also tainted,
because other untrusted processes can modify messages and shared memory
segments for their own nefarious purposes.

### More functional bareword prototype (*)

Subsection "More functional bareword prototype (*)"
Bareword prototypes have been rationalized to enable them to be used
to override builtins that accept barewords and interpret them in
a special way, such as \f(CW\*(C`require\*(C' or \f(CW\*(C`do\*(C'.

Arguments prototyped as \f(CW\*(C`*\*(C' will now be visible within the subroutine
as either a simple scalar or as a reference to a typeglob.
See \*(L"Prototypes\*(R" in perlsub.
.ie n .SS """require"" and ""do"" may be overridden"
.el .SS "\f(CWrequire and \f(CWdo may be overridden"
Subsection "require and do may be overridden"
\f(CW\*(C`require\*(C' and \f(CW\*(C`do \*(Aqfile\*(Aq\*(C' operations may be overridden locally
by importing subroutines of the same name into the current package
(or globally by importing them into the \s-1CORE::GLOBAL::\s0 namespace).
Overriding \f(CW\*(C`require\*(C' will also affect \f(CW\*(C`use\*(C', provided the override
is visible at compile-time.
See \*(L"Overriding Built-in Functions\*(R" in perlsub.

### $^X variables may now have names longer than one character

Subsection "$^X variables may now have names longer than one character"
Formerly, $^X was synonymous with $\{\*(L"\\cX\*(R"\}, but $^XY was a syntax
error.  Now variable names that begin with a control character may be
arbitrarily long.  However, for compatibility reasons, these variables
*must* be written with explicit braces, as \f(CW\*(C`$\{^XY\}\*(C' for example.
\f(CW\*(C`$\{^XYZ\}\*(C' is synonymous with $\{\*(L"\\cXYZ\*(R"\}.  Variable names with more
than one control character, such as \f(CW\*(C`$\{^XY^Z\}\*(C', are illegal.

The old syntax has not changed.  As before, `^X' may be either a
literal control-X character or the two-character sequence `caret' plus
`X'.  When braces are omitted, the variable name stops after the
control character.  Thus \f(CW"$^XYZ" continues to be synonymous with
\f(CW\*(C`$^X . "YZ"\*(C' as before.

As before, lexical variables may not have names beginning with control
characters.  As before, variables whose names begin with a control
character are always forced to be in package `main'.  All such variables
are reserved for future extensions, except those that begin with
\f(CW\*(C`^_\*(C', which may be used by user programs and are guaranteed not to
acquire special meaning in any future version of Perl.
.ie n .SS "New variable $^C reflects ""-c"" switch"
.el .SS "New variable $^C reflects \f(CW-c switch"
Subsection "New variable $^C reflects -c switch"
\f(CW$^C has a boolean value that reflects whether perl is being run
in compile-only mode (i.e. via the \f(CW\*(C`-c\*(C' switch).  Since
\s-1BEGIN\s0 blocks are executed under such conditions, this variable
enables perl code to determine whether actions that make sense
only during normal running are warranted.  See perlvar.

### New variable $^V contains Perl version as a string

Subsection "New variable $^V contains Perl version as a string"
\f(CW$^V contains the Perl version number as a string composed of
characters whose ordinals match the version numbers, i.e. v5.6.0.
This may be used in string comparisons.

See \f(CW\*(C`Support for strings represented as a vector of ordinals\*(C' for an
example.

### Optional Y2K warnings

Subsection "Optional Y2K warnings"
If Perl is built with the cpp macro \f(CW\*(C`PERL_Y2KWARN\*(C' defined,
it emits optional warnings when concatenating the number 19
with another number.

This behavior must be specifically enabled when running Configure.
See *\s-1INSTALL\s0* and *\s-1README.Y2K\s0*.

### Arrays now always interpolate into double-quoted strings

Subsection "Arrays now always interpolate into double-quoted strings"
In double-quoted strings, arrays now interpolate, no matter what.  The
behavior in earlier versions of perl 5 was that arrays would interpolate
into strings if the array had been mentioned before the string was
compiled, and otherwise Perl would raise a fatal compile-time error.
In versions 5.000 through 5.003, the error was

.Vb 1
        Literal @example now requires backslash
.Ve

In versions 5.004_01 through 5.6.0, the error was

.Vb 1
        In string, @example now must be written as \\@example
.Ve

The idea here was to get people into the habit of writing
\f(CW"fred\\@example.com" when they wanted a literal \f(CW\*(C`@\*(C' sign, just as
they have always written \f(CW"Give me back my \\$5" when they wanted a
literal \f(CW\*(C`$\*(C' sign.

Starting with 5.6.1, when Perl now sees an \f(CW\*(C`@\*(C' sign in a
double-quoted string, it *always* attempts to interpolate an array,
regardless of whether or not the array has been used or declared
already.  The fatal error has been downgraded to an optional warning:

.Vb 1
        Possible unintended interpolation of @example in string
.Ve

This warns you that \f(CW"fred@example.com" is going to turn into
\f(CW\*(C`fred.com\*(C' if you don't backslash the \f(CW\*(C`@\*(C'.
See http://perl.plover.com/at-error.html for more details
about the history here.

### @- and @+ provide starting/ending offsets of regex submatches

Subsection "@- and @+ provide starting/ending offsets of regex submatches"
The new magic variables @- and @+ provide the starting and ending
offsets, respectively, of $&, \f(CW$1, \f(CW$2, etc.  See perlvar for
details.

## Modules and Pragmata

Header "Modules and Pragmata"

### Modules

Subsection "Modules"

- attributes
Item "attributes"
While used internally by Perl as a pragma, this module also
provides a way to fetch subroutine and variable attributes.
See attributes.

- B
Item "B"
The Perl Compiler suite has been extensively reworked for this
release.  More of the standard Perl test suite passes when run
under the Compiler, but there is still a significant way to
go to achieve production quality compiled executables.
.Sp
.Vb 3
    NOTE: The Compiler suite remains highly experimental.  The
    generated code may not be correct, even when it manages to execute
    without errors.
.Ve

- Benchmark
Item "Benchmark"
Overall, Benchmark results exhibit lower average error and better timing
accuracy.
.Sp
You can now run tests for *n* seconds instead of guessing the right
number of tests to run: e.g., timethese(-5, ...) will run each
code for at least 5 \s-1CPU\s0 seconds.  Zero as the \*(L"number of repetitions\*(R"
means \*(L"for at least 3 \s-1CPU\s0 seconds\*(R".  The output format has also
changed.  For example:
.Sp
.Vb 1
   use Benchmark;$x=3;timethese(-5,\{a=>sub\{$x*$x\},b=>sub\{$x**2\}\})
.Ve
.Sp
will now output something like this:
.Sp
.Vb 3
   Benchmark: running a, b, each for at least 5 CPU seconds...
            a:  5 wallclock secs ( 5.77 usr +  0.00 sys =  5.77 CPU) @ 200551.91/s (n=1156516)
            b:  4 wallclock secs ( 5.00 usr +  0.02 sys =  5.02 CPU) @ 159605.18/s (n=800686)
.Ve
.Sp
New features: \*(L"each for at least N \s-1CPU\s0 seconds...\*(R", \*(L"wallclock secs\*(R",
and the \*(L"@ operations/CPU second (n=operations)\*(R".
.Sp
**timethese()** now returns a reference to a hash of Benchmark objects containing
the test results, keyed on the names of the tests.
.Sp
**timethis()** now returns the iterations field in the Benchmark result object
instead of 0.
.Sp
**timethese()**, **timethis()**, and the new **cmpthese()** (see below) can also take
a format specifier of 'none' to suppress output.
.Sp
A new function **countit()** is just like **timeit()** except that it takes a
\s-1TIME\s0 instead of a \s-1COUNT.\s0
.Sp
A new function **cmpthese()** prints a chart comparing the results of each test
returned from a **timethese()** call.  For each possible pair of tests, the
percentage speed difference (iters/sec or seconds/iter) is shown.
.Sp
For other details, see Benchmark.

- ByteLoader
Item "ByteLoader"
The ByteLoader is a dedicated extension to generate and run
Perl bytecode.  See ByteLoader.

- constant
Item "constant"
References can now be used.
.Sp
The new version also allows a leading underscore in constant names, but
disallows a double leading underscore (as in \*(L"_\|_LINE_\|_\*(R").  Some other names
are disallowed or warned against, including \s-1BEGIN, END,\s0 etc.  Some names
which were forced into main:: used to fail silently in some cases; now they're
fatal (outside of main::) and an optional warning (inside of main::).
The ability to detect whether a constant had been set with a given name has
been added.
.Sp
See constant.

- charnames
Item "charnames"
This pragma implements the \f(CW\*(C`\\N\*(C' string escape.  See charnames.

- Data::Dumper
Item "Data::Dumper"
A \f(CW\*(C`Maxdepth\*(C' setting can be specified to avoid venturing
too deeply into deep data structures.  See Data::Dumper.
.Sp
The \s-1XSUB\s0 implementation of **Dump()** is now automatically called if the
\f(CW\*(C`Useqq\*(C' setting is not in use.
.Sp
Dumping \f(CW\*(C`qr//\*(C' objects works correctly.

- \s-1DB\s0
Item "DB"
\f(CW\*(C`DB\*(C' is an experimental module that exposes a clean abstraction
to Perl's debugging \s-1API.\s0

- DB_File
Item "DB_File"
DB_File can now be built with Berkeley \s-1DB\s0 versions 1, 2 or 3.
See \f(CW\*(C`ext/DB_File/Changes\*(C'.

- Devel::DProf
Item "Devel::DProf"
Devel::DProf, a Perl source code profiler has been added.  See
Devel::DProf and dprofpp.

- Devel::Peek
Item "Devel::Peek"
The Devel::Peek module provides access to the internal representation
of Perl variables and data.  It is a data debugging tool for the \s-1XS\s0 programmer.

- Dumpvalue
Item "Dumpvalue"
The Dumpvalue module provides screen dumps of Perl data.

- DynaLoader
Item "DynaLoader"
DynaLoader now supports a **dl_unload_file()** function on platforms that
support unloading shared objects using **dlclose()**.
.Sp
Perl can also optionally arrange to unload all extension shared objects
loaded by Perl.  To enable this, build Perl with the Configure option
\f(CW\*(C`-Accflags=-DDL_UNLOAD_ALL_AT_EXIT\*(C'.  (This maybe useful if you are
using Apache with mod_perl.)

- English
Item "English"
\f(CW$PERL_VERSION now stands for \f(CW$^V (a string value) rather than for \f(CW$]
(a numeric value).

- Env
Item "Env"
Env now supports accessing environment variables like \s-1PATH\s0 as array
variables.

- Fcntl
Item "Fcntl"
More Fcntl constants added: F_SETLK64, F_SETLKW64, O_LARGEFILE for
large file (more than 4GB) access (\s-1NOTE:\s0 the O_LARGEFILE is
automatically added to **sysopen()** flags if large file support has been
configured, as is the default), Free/Net/OpenBSD locking behaviour
flags F_FLOCK, F_POSIX, Linux F_SHLCK, and O_ACCMODE: the combined
mask of O_RDONLY, O_WRONLY, and O_RDWR.  The **seek()**/**sysseek()**
constants \s-1SEEK_SET, SEEK_CUR,\s0 and \s-1SEEK_END\s0 are available via the
\f(CW\*(C`:seek\*(C' tag.  The **chmod()**/**stat()** S_IF* constants and S_IS* functions
are available via the \f(CW\*(C`:mode\*(C' tag.

- File::Compare
Item "File::Compare"
A **compare_text()** function has been added, which allows custom
comparison functions.  See File::Compare.

- File::Find
Item "File::Find"
File::Find now works correctly when the **wanted()** function is either
autoloaded or is a symbolic reference.
.Sp
A bug that caused File::Find to lose track of the working directory
when pruning top-level directories has been fixed.
.Sp
File::Find now also supports several other options to control its
behavior.  It can follow symbolic links if the \f(CW\*(C`follow\*(C' option is
specified.  Enabling the \f(CW\*(C`no_chdir\*(C' option will make File::Find skip
changing the current directory when walking directories.  The \f(CW\*(C`untaint\*(C'
flag can be useful when running with taint checks enabled.
.Sp
See File::Find.

- File::Glob
Item "File::Glob"
This extension implements BSD-style file globbing.  By default,
it will also be used for the internal implementation of the **glob()**
operator.  See File::Glob.

- File::Spec
Item "File::Spec"
New methods have been added to the File::Spec module: **devnull()** returns
the name of the null device (/dev/null on Unix) and **tmpdir()** the name of
the temp directory (normally /tmp on Unix).  There are now also methods
to convert between absolute and relative filenames: **abs2rel()** and
**rel2abs()**.  For compatibility with operating systems that specify volume
names in file paths, the **splitpath()**, **splitdir()**, and **catdir()** methods
have been added.

- File::Spec::Functions
Item "File::Spec::Functions"
The new File::Spec::Functions modules provides a function interface
to the File::Spec module.  Allows shorthand
.Sp
.Vb 1
    $fullname = catfile($dir1, $dir2, $file);
.Ve
.Sp
instead of
.Sp
.Vb 1
    $fullname = File::Spec->catfile($dir1, $dir2, $file);
.Ve

- Getopt::Long
Item "Getopt::Long"
Getopt::Long licensing has changed to allow the Perl Artistic License
as well as the \s-1GPL.\s0 It used to be \s-1GPL\s0 only, which got in the way of
non-GPL applications that wanted to use Getopt::Long.
.Sp
Getopt::Long encourages the use of Pod::Usage to produce help
messages. For example:
.Sp
.Vb 7
    use Getopt::Long;
    use Pod::Usage;
    my $man = 0;
    my $help = 0;
    GetOptions(\*(Aqhelp|?\*(Aq => \\$help, man => \\$man) or pod2usage(2);
    pod2usage(1) if $help;
    pod2usage(-exitstatus => 0, -verbose => 2) if $man;

    _\|_END_\|_

    =head1 NAME

    sample - Using Getopt::Long and Pod::Usage

    =head1 SYNOPSIS

    sample [options] [file ...]

     Options:
       -help            brief help message
       -man             full documentation

    =head1 OPTIONS

    =over 8

    =item B<-help>

    Print a brief help message and exits.

    =item B<-man>

    Prints the manual page and exits.

    =back

    =head1 DESCRIPTION

    B<This program> will read the given input file(s) and do something
    useful with the contents thereof.

    =cut
.Ve
.Sp
See Pod::Usage for details.
.Sp
A bug that prevented the non-option call-back <> from being
specified as the first argument has been fixed.
.Sp
To specify the characters < and > as option starters, use ><. Note,
however, that changing option starters is strongly deprecated.

- \s-1IO\s0
Item "IO"
**write()** and **syswrite()** will now accept a single-argument
form of the call, for consistency with Perl's **syswrite()**.
.Sp
You can now create a TCP-based IO::Socket::INET without forcing
a connect attempt.  This allows you to configure its options
(like making it non-blocking) and then call **connect()** manually.
.Sp
A bug that prevented the **IO::Socket::protocol()** accessor
from ever returning the correct value has been corrected.
.Sp
IO::Socket::connect now uses non-blocking \s-1IO\s0 instead of **alarm()**
to do connect timeouts.
.Sp
IO::Socket::accept now uses **select()** instead of **alarm()** for doing
timeouts.
.Sp
IO::Socket::INET->new now sets $! correctly on failure. $@ is
still set for backwards compatibility.

- \s-1JPL\s0
Item "JPL"
Java Perl Lingo is now distributed with Perl.  See jpl/README
for more information.

- lib
Item "lib"
\f(CW\*(C`use lib\*(C' now weeds out any trailing duplicate entries.
\f(CW\*(C`no lib\*(C' removes all named entries.

- Math::BigInt
Item "Math::BigInt"
The bitwise operations \f(CW\*(C`<<\*(C', \f(CW\*(C`>>\*(C', \f(CW\*(C`&\*(C', \f(CW\*(C`|\*(C',
and \f(CW\*(C`~\*(C' are now supported on bigints.

- Math::Complex
Item "Math::Complex"
The accessor methods Re, Im, arg, abs, rho, and theta can now also
act as mutators (accessor \f(CW$z->**Re()**, mutator \f(CW$z->**Re**\|(3)).
.Sp
The class method \f(CW\*(C`display_format\*(C' and the corresponding object method
\f(CW\*(C`display_format\*(C', in addition to accepting just one argument, now can
also accept a parameter hash.  Recognized keys of a parameter hash are
\f(CW"style", which corresponds to the old one parameter case, and two
new parameters: \f(CW"format", which is a **printf()**-style format string
(defaults usually to \f(CW"%.15g", you can revert to the default by
setting the format string to \f(CW\*(C`undef\*(C') used for both parts of a
complex number, and \f(CW"polar_pretty_print" (defaults to true),
which controls whether an attempt is made to try to recognize small
multiples and rationals of pi (2pi, pi/2) at the argument (angle) of a
polar complex number.
.Sp
The potentially disruptive change is that in list context both methods
now *return the parameter hash*, instead of only the value of the
\f(CW"style" parameter.

- Math::Trig
Item "Math::Trig"
A little bit of radial trigonometry (cylindrical and spherical),
radial coordinate conversions, and the great circle distance were added.

- Pod::Parser, Pod::InputObjects
Item "Pod::Parser, Pod::InputObjects"
Pod::Parser is a base class for parsing and selecting sections of
pod documentation from an input stream.  This module takes care of
identifying pod paragraphs and commands in the input and hands off the
parsed paragraphs and commands to user-defined methods which are free
to interpret or translate them as they see fit.
.Sp
Pod::InputObjects defines some input objects needed by Pod::Parser, and
for advanced users of Pod::Parser that need more about a command besides
its name and text.
.Sp
As of release 5.6.0 of Perl, Pod::Parser is now the officially sanctioned
\*(L"base parser code\*(R" recommended for use by all pod2xxx translators.
Pod::Text (pod2text) and Pod::Man (pod2man) have already been converted
to use Pod::Parser and efforts to convert Pod::HTML (pod2html) are already
underway.  For any questions or comments about pod parsing and translating
issues and utilities, please use the pod-people@perl.org mailing list.
.Sp
For further information, please see Pod::Parser and Pod::InputObjects.

- Pod::Checker, podchecker
Item "Pod::Checker, podchecker"
This utility checks pod files for correct syntax, according to
perlpod.  Obvious errors are flagged as such, while warnings are
printed for mistakes that can be handled gracefully.  The checklist is
not complete yet.  See Pod::Checker.

- Pod::ParseUtils, Pod::Find
Item "Pod::ParseUtils, Pod::Find"
These modules provide a set of gizmos that are useful mainly for pod
translators.  Pod::Find traverses directory structures and
returns found pod files, along with their canonical names (like
\f(CW\*(C`File::Spec::Unix\*(C').  Pod::ParseUtils contains
**Pod::List** (useful for storing pod list information), **Pod::Hyperlink**
(for parsing the contents of \f(CW\*(C`L<>\*(C' sequences) and **Pod::Cache**
(for caching information about pod files, e.g., link nodes).

- Pod::Select, podselect
Item "Pod::Select, podselect"
Pod::Select is a subclass of Pod::Parser which provides a function
named \*(L"**podselect()**\*(R" to filter out user-specified sections of raw pod
documentation from an input stream. podselect is a script that provides
access to Pod::Select from other scripts to be used as a filter.
See Pod::Select.

- Pod::Usage, pod2usage
Item "Pod::Usage, pod2usage"
Pod::Usage provides the function \*(L"**pod2usage()**\*(R" to print usage messages for
a Perl script based on its embedded pod documentation.  The **pod2usage()**
function is generally useful to all script authors since it lets them
write and maintain a single source (the pods) for documentation, thus
removing the need to create and maintain redundant usage message text
consisting of information already in the pods.
.Sp
There is also a pod2usage script which can be used from other kinds of
scripts to print usage messages from pods (even for non-Perl scripts
with pods embedded in comments).
.Sp
For details and examples, please see Pod::Usage.

- Pod::Text and Pod::Man
Item "Pod::Text and Pod::Man"
Pod::Text has been rewritten to use Pod::Parser.  While **pod2text()** is
still available for backwards compatibility, the module now has a new
preferred interface.  See Pod::Text for the details.  The new Pod::Text
module is easily subclassed for tweaks to the output, and two such
subclasses (Pod::Text::Termcap for man-page-style bold and underlining
using termcap information, and Pod::Text::Color for markup with \s-1ANSI\s0 color
sequences) are now standard.
.Sp
pod2man has been turned into a module, Pod::Man, which also uses
Pod::Parser.  In the process, several outstanding bugs related to quotes
in section headers, quoting of code escapes, and nested lists have been
fixed.  pod2man is now a wrapper script around this module.

- SDBM_File
Item "SDBM_File"
An \s-1EXISTS\s0 method has been added to this module (and **sdbm_exists()** has
been added to the underlying sdbm library), so one can now call exists
on an SDBM_File tied hash and get the correct result, rather than a
runtime error.
.Sp
A bug that may have caused data loss when more than one disk block
happens to be read from the database in a single \s-1**FETCH\s0()** has been
fixed.

- Sys::Syslog
Item "Sys::Syslog"
Sys::Syslog now uses XSUBs to access facilities from syslog.h so it
no longer requires syslog.ph to exist.

- Sys::Hostname
Item "Sys::Hostname"
Sys::Hostname now uses XSUBs to call the C library's **gethostname()** or
**uname()** if they exist.

- Term::ANSIColor
Item "Term::ANSIColor"
Term::ANSIColor is a very simple module to provide easy and readable
access to the \s-1ANSI\s0 color and highlighting escape sequences, supported by
most \s-1ANSI\s0 terminal emulators.  It is now included standard.

- Time::Local
Item "Time::Local"
The **timelocal()** and **timegm()** functions used to silently return bogus
results when the date fell outside the machine's integer range.  They
now consistently **croak()** if the date falls in an unsupported range.

- Win32
Item "Win32"
The error return value in list context has been changed for all functions
that return a list of values.  Previously these functions returned a list
with a single element \f(CW\*(C`undef\*(C' if an error occurred.  Now these functions
return the empty list in these situations.  This applies to the following
functions:
.Sp
.Vb 2
    Win32::FsType
    Win32::GetOSVersion
.Ve
.Sp
The remaining functions are unchanged and continue to return \f(CW\*(C`undef\*(C' on
error even in list context.
.Sp
The Win32::SetLastError(\s-1ERROR\s0) function has been added as a complement
to the **Win32::GetLastError()** function.
.Sp
The new Win32::GetFullPathName(\s-1FILENAME\s0) returns the full absolute
pathname for \s-1FILENAME\s0 in scalar context.  In list context it returns
a two-element list containing the fully qualified directory name and
the filename.  See Win32.

- XSLoader
Item "XSLoader"
The XSLoader extension is a simpler alternative to DynaLoader.
See XSLoader.

- \s-1DBM\s0 Filters
Item "DBM Filters"
A new feature called \*(L"\s-1DBM\s0 Filters\*(R" has been added to all the
\s-1DBM\s0 modules--DB_File, GDBM_File, NDBM_File, ODBM_File, and SDBM_File.
\s-1DBM\s0 Filters add four new methods to each \s-1DBM\s0 module:
.Sp
.Vb 4
    filter_store_key
    filter_store_value
    filter_fetch_key
    filter_fetch_value
.Ve
.Sp
These can be used to filter key-value pairs before the pairs are
written to the database or just after they are read from the database.
See perldbmfilter for further information.

### Pragmata

Subsection "Pragmata"
\f(CW\*(C`use attrs\*(C' is now obsolete, and is only provided for
backward-compatibility.  It's been replaced by the \f(CW\*(C`sub : attributes\*(C'
syntax.  See \*(L"Subroutine Attributes\*(R" in perlsub and attributes.

Lexical warnings pragma, \f(CW\*(C`use warnings;\*(C', to control optional warnings.
See perllexwarn.

\f(CW\*(C`use filetest\*(C' to control the behaviour of filetests (\f(CW\*(C`-r\*(C' \f(CW\*(C`-w\*(C'
...).  Currently only one subpragma implemented, \*(L"use filetest
'access';\*(R", that uses **access**\|(2) or equivalent to check permissions
instead of using **stat**\|(2) as usual.  This matters in filesystems
where there are ACLs (access control lists): the **stat**\|(2) might lie,
but **access**\|(2) knows better.

The \f(CW\*(C`open\*(C' pragma can be used to specify default disciplines for
handle constructors (e.g. **open()**) and for qx//.  The two
pseudo-disciplines \f(CW\*(C`:raw\*(C' and \f(CW\*(C`:crlf\*(C' are currently supported on
DOS-derivative platforms (i.e. where binmode is not a no-op).
See also \*(L"**binmode()** can be used to set :crlf and :raw modes\*(R".

## Utility Changes

Header "Utility Changes"

### dprofpp

Subsection "dprofpp"
\f(CW\*(C`dprofpp\*(C' is used to display profile data generated using \f(CW\*(C`Devel::DProf\*(C'.
See dprofpp.

### find2perl

Subsection "find2perl"
The \f(CW\*(C`find2perl\*(C' utility now uses the enhanced features of the File::Find
module.  The -depth and -follow options are supported.  Pod documentation
is also included in the script.

### h2xs

Subsection "h2xs"
The \f(CW\*(C`h2xs\*(C' tool can now work in conjunction with \f(CW\*(C`C::Scan\*(C' (available
from \s-1CPAN\s0) to automatically parse real-life header files.  The \f(CW\*(C`-M\*(C',
\f(CW\*(C`-a\*(C', \f(CW\*(C`-k\*(C', and \f(CW\*(C`-o\*(C' options are new.

### perlcc

Subsection "perlcc"
\f(CW\*(C`perlcc\*(C' now supports the C and Bytecode backends.  By default,
it generates output from the simple C backend rather than the
optimized C backend.

Support for non-Unix platforms has been improved.

### perldoc

Subsection "perldoc"
\f(CW\*(C`perldoc\*(C' has been reworked to avoid possible security holes.
It will not by default let itself be run as the superuser, but you
may still use the **-U** switch to try to make it drop privileges
first.

### The Perl Debugger

Subsection "The Perl Debugger"
Many bug fixes and enhancements were added to *perl5db.pl*, the
Perl debugger.  The help documentation was rearranged.  New commands
include \f(CW\*(C`< ?\*(C', \f(CW\*(C`> ?\*(C', and \f(CW\*(C`\{ ?\*(C' to list out current
actions, \f(CW\*(C`man \f(CIdocpage\f(CW\*(C' to run your doc viewer on some perl
docset, and support for quoted options.  The help information was
rearranged, and should be viewable once again if you're using **less**
as your pager.  A serious security hole was plugged\*(--you should
immediately remove all older versions of the Perl debugger as
installed in previous releases, all the way back to perl3, from
your system to avoid being bitten by this.

## Improved Documentation

Header "Improved Documentation"
Many of the platform-specific \s-1README\s0 files are now part of the perl
installation.  See perl for the complete list.

- perlapi.pod
Item "perlapi.pod"
The official list of public Perl \s-1API\s0 functions.

- perlboot.pod
Item "perlboot.pod"
A tutorial for beginners on object-oriented Perl.

- perlcompile.pod
Item "perlcompile.pod"
An introduction to using the Perl Compiler suite.

- perldbmfilter.pod
Item "perldbmfilter.pod"
A howto document on using the \s-1DBM\s0 filter facility.

- perldebug.pod
Item "perldebug.pod"
All material unrelated to running the Perl debugger, plus all
low-level guts-like details that risked crushing the casual user
of the debugger, have been relocated from the old manpage to the
next entry below.

- perldebguts.pod
Item "perldebguts.pod"
This new manpage contains excessively low-level material not related
to the Perl debugger, but slightly related to debugging Perl itself.
It also contains some arcane internal details of how the debugging
process works that may only be of interest to developers of Perl
debuggers.

- perlfork.pod
Item "perlfork.pod"
Notes on the **fork()** emulation currently available for the Windows platform.

- perlfilter.pod
Item "perlfilter.pod"
An introduction to writing Perl source filters.

- perlhack.pod
Item "perlhack.pod"
Some guidelines for hacking the Perl source code.

- perlintern.pod
Item "perlintern.pod"
A list of internal functions in the Perl source code.
(List is currently empty.)

- perllexwarn.pod
Item "perllexwarn.pod"
Introduction and reference information about lexically scoped
warning categories.

- perlnumber.pod
Item "perlnumber.pod"
Detailed information about numbers as they are represented in Perl.

- perlopentut.pod
Item "perlopentut.pod"
A tutorial on using **open()** effectively.

- perlreftut.pod
Item "perlreftut.pod"
A tutorial that introduces the essentials of references.

- perltootc.pod
Item "perltootc.pod"
A tutorial on managing class data for object modules.

- perltodo.pod
Item "perltodo.pod"
Discussion of the most often wanted features that may someday be
supported in Perl.

- perlunicode.pod
Item "perlunicode.pod"
An introduction to Unicode support features in Perl.

## Performance enhancements

Header "Performance enhancements"
.ie n .SS "Simple **sort()** using \{ $a <=> $b \} and the like are optimized"
.el .SS "Simple **sort()** using \{ \f(CW$a <=> \f(CW$b \} and the like are optimized"
Subsection "Simple sort() using \{ $a <=> $b \} and the like are optimized"
Many common **sort()** operations using a simple inlined block are now
optimized for faster performance.

### Optimized assignments to lexical variables

Subsection "Optimized assignments to lexical variables"
Certain operations in the \s-1RHS\s0 of assignment statements have been
optimized to directly set the lexical variable on the \s-1LHS,\s0
eliminating redundant copying overheads.

### Faster subroutine calls

Subsection "Faster subroutine calls"
Minor changes in how subroutine calls are handled internally
provide marginal improvements in performance.

### \fBdelete(), \fBeach(), \fBvalues() and hash iteration are faster

Subsection "delete(), each(), values() and hash iteration are faster"
The hash values returned by **delete()**, **each()**, **values()** and hashes in a
list context are the actual values in the hash, instead of copies.
This results in significantly better performance, because it eliminates
needless copying in most situations.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"

### -Dusethreads means something different

Subsection "-Dusethreads means something different"
The -Dusethreads flag now enables the experimental interpreter-based thread
support by default.  To get the flavor of experimental threads that was in
5.005 instead, you need to run Configure with \*(L"-Dusethreads -Duse5005threads\*(R".

As of v5.6.0, interpreter-threads support is still lacking a way to
create new threads from Perl (i.e., \f(CW\*(C`use Thread;\*(C' will not work with
interpreter threads).  \f(CW\*(C`use Thread;\*(C' continues to be available when you
specify the -Duse5005threads option to Configure, bugs and all.

.Vb 2
    NOTE: Support for threads continues to be an experimental feature.
    Interfaces and implementation are subject to sudden and drastic changes.
.Ve

### New Configure flags

Subsection "New Configure flags"
The following new flags may be enabled on the Configure command line
by running Configure with \f(CW\*(C`-Dflag\*(C'.

.Vb 3
    usemultiplicity
    usethreads useithreads      (new interpreter threads: no Perl API yet)
    usethreads use5005threads   (threads as they were in 5.005)

    use64bitint                 (equal to now deprecated \*(Aquse64bits\*(Aq)
    use64bitall

    uselongdouble
    usemorebits
    uselargefiles
    usesocks                    (only SOCKS v5 supported)
.Ve

### Threadedness and 64-bitness now more daring

Subsection "Threadedness and 64-bitness now more daring"
The Configure options enabling the use of threads and the use of
64-bitness are now more daring in the sense that they no more have an
explicit list of operating systems of known threads/64-bit
capabilities.  In other words: if your operating system has the
necessary APIs and datatypes, you should be able just to go ahead and
use them, for threads by Configure -Dusethreads, and for 64 bits
either explicitly by Configure -Duse64bitint or implicitly if your
system has 64-bit wide datatypes.  See also \*(L"64-bit support\*(R".

### Long Doubles

Subsection "Long Doubles"
Some platforms have \*(L"long doubles\*(R", floating point numbers of even
larger range than ordinary \*(L"doubles\*(R".  To enable using long doubles for
Perl's scalars, use -Duselongdouble.

### -Dusemorebits

Subsection "-Dusemorebits"
You can enable both -Duse64bitint and -Duselongdouble with -Dusemorebits.
See also \*(L"64-bit support\*(R".

### -Duselargefiles

Subsection "-Duselargefiles"
Some platforms support system APIs that are capable of handling large files
(typically, files larger than two gigabytes).  Perl will try to use these
APIs if you ask for -Duselargefiles.

See \*(L"Large file support\*(R" for more information.

### installusrbinperl

Subsection "installusrbinperl"
You can use \*(L"Configure -Uinstallusrbinperl\*(R" which causes installperl
to skip installing perl also as /usr/bin/perl.  This is useful if you
prefer not to modify /usr/bin for some reason or another but harmful
because many scripts assume to find Perl in /usr/bin/perl.

### \s-1SOCKS\s0 support

Subsection "SOCKS support"
You can use \*(L"Configure -Dusesocks\*(R" which causes Perl to probe
for the \s-1SOCKS\s0 proxy protocol library (v5, not v4).  For more information
on \s-1SOCKS,\s0 see:

.Vb 1
    http://www.socks.nec.com/
.Ve
.ie n .SS """-A"" flag"
.el .SS "\f(CW-A flag"
Subsection "-A flag"
You can \*(L"post-edit\*(R" the Configure variables using the Configure \f(CW\*(C`-A\*(C'
switch.  The editing happens immediately after the platform specific
hints files have been processed but before the actual configuration
process starts.  Run \f(CW\*(C`Configure -h\*(C' to find out the full \f(CW\*(C`-A\*(C' syntax.

### Enhanced Installation Directories

Subsection "Enhanced Installation Directories"
The installation structure has been enriched to improve the support
for maintaining multiple versions of perl, to provide locations for
vendor-supplied modules, scripts, and manpages, and to ease maintenance
of locally-added modules, scripts, and manpages.  See the section on
Installation Directories in the \s-1INSTALL\s0 file for complete details.
For most users building and installing from source, the defaults should
be fine.

If you previously used \f(CW\*(C`Configure -Dsitelib\*(C' or \f(CW\*(C`-Dsitearch\*(C' to set
special values for library directories, you might wish to consider using
the new \f(CW\*(C`-Dsiteprefix\*(C' setting instead.  Also, if you wish to re-use a
config.sh file from an earlier version of perl, you should be sure to
check that Configure makes sensible choices for the new directories.
See \s-1INSTALL\s0 for complete details.

### gcc automatically tried if cc does not seem to be working

Subsection "gcc automatically tried if 'cc' does not seem to be working"
In many platforms the vendor-supplied 'cc' is too stripped-down to
build Perl (basically, the 'cc' doesn't do \s-1ANSI C\s0).  If this seems
to be the case and the 'cc' does not seem to be the \s-1GNU C\s0 compiler
'gcc', an automatic attempt is made to find and use 'gcc' instead.

## Platform specific changes

Header "Platform specific changes"

### Supported platforms

Subsection "Supported platforms"

- \(bu
The Mach CThreads (\s-1NEXTSTEP, OPENSTEP\s0) are now supported by the Thread
extension.

- \(bu
GNU/Hurd is now supported.

- \(bu
Rhapsody/Darwin is now supported.

- \(bu
\s-1EPOC\s0 is now supported (on Psion 5).

- \(bu
The cygwin port (formerly cygwin32) has been greatly improved.

### \s-1DOS\s0

Subsection "DOS"

- \(bu
Perl now works with djgpp 2.02 (and 2.03 alpha).

- \(bu
Environment variable names are not converted to uppercase any more.

- \(bu
Incorrect exit codes from backticks have been fixed.

- \(bu
This port continues to use its own builtin globbing (not File::Glob).

### \s-1OS390\s0 (OpenEdition \s-1MVS\s0)

Subsection "OS390 (OpenEdition MVS)"
Support for this \s-1EBCDIC\s0 platform has not been renewed in this release.
There are difficulties in reconciling Perl's standardization on \s-1UTF-8\s0
as its internal representation for characters with the \s-1EBCDIC\s0 character
set, because the two are incompatible.

It is unclear whether future versions will renew support for this
platform, but the possibility exists.

### \s-1VMS\s0

Subsection "VMS"
Numerous revisions and extensions to configuration, build, testing, and
installation process to accommodate core changes and VMS-specific options.

Expand \f(CW%ENV-handling code to allow runtime mapping to logical names,
\s-1CLI\s0 symbols, and \s-1CRTL\s0 environ array.

Extension of subprocess invocation code to accept filespecs as command
\*(L"verbs\*(R".

Add to Perl command line processing the ability to use default file types and
to recognize Unix-style \f(CW\*(C`2>&1\*(C'.

Expansion of File::Spec::VMS routines, and integration into ExtUtils::MM_VMS.

Extension of ExtUtils::MM_VMS to handle complex extensions more flexibly.

Barewords at start of Unix-syntax paths may be treated as text rather than
only as logical names.

Optional secure translation of several logical names used internally by Perl.

Miscellaneous bugfixing and porting of new core code to \s-1VMS.\s0

Thanks are gladly extended to the many people who have contributed \s-1VMS\s0
patches, testing, and ideas.

### Win32

Subsection "Win32"
Perl can now emulate **fork()** internally, using multiple interpreters running
in different concurrent threads.  This support must be enabled at build
time.  See perlfork for detailed information.

When given a pathname that consists only of a drivename, such as \f(CW\*(C`A:\*(C',
**opendir()** and **stat()** now use the current working directory for the drive
rather than the drive root.

The builtin \s-1XSUB\s0 functions in the Win32:: namespace are documented.  See
Win32.

$^X now contains the full path name of the running executable.

A **Win32::GetLongPathName()** function is provided to complement
**Win32::GetFullPathName()** and **Win32::GetShortPathName()**.  See Win32.

**POSIX::uname()** is supported.

system(1,...) now returns true process IDs rather than process
handles.  **kill()** accepts any real process id, rather than strictly
return values from system(1,...).

For better compatibility with Unix, \f(CW\*(C`kill(0, $pid)\*(C' can now be used to
test whether a process exists.

The \f(CW\*(C`Shell\*(C' module is supported.

Better support for building Perl under command.com in Windows 95
has been added.

Scripts are read in binary mode by default to allow ByteLoader (and
the filter mechanism in general) to work properly.  For compatibility,
the \s-1DATA\s0 filehandle will be set to text mode if a carriage return is
detected at the end of the line containing the _\|_END_\|_ or _\|_DATA_\|_
token; if not, the \s-1DATA\s0 filehandle will be left open in binary mode.
Earlier versions always opened the \s-1DATA\s0 filehandle in text mode.

The **glob()** operator is implemented via the \f(CW\*(C`File::Glob\*(C' extension,
which supports glob syntax of the C shell.  This increases the flexibility
of the **glob()** operator, but there may be compatibility issues for
programs that relied on the older globbing syntax.  If you want to
preserve compatibility with the older syntax, you might want to run
perl with \f(CW\*(C`-MFile::DosGlob\*(C'.  For details and compatibility information,
see File::Glob.

## Significant bug fixes

Header "Significant bug fixes"

### <\s-1HANDLE\s0> on empty files

Subsection "<HANDLE> on empty files"
With \f(CW$/ set to \f(CW\*(C`undef\*(C', \*(L"slurping\*(R" an empty file returns a string of
zero length (instead of \f(CW\*(C`undef\*(C', as it used to) the first time the
\s-1HANDLE\s0 is read after \f(CW$/ is set to \f(CW\*(C`undef\*(C'.  Further reads yield
\f(CW\*(C`undef\*(C'.

This means that the following will append \*(L"foo\*(R" to an empty file (it used
to do nothing):

.Vb 1
    perl -0777 -pi -e \*(Aqs/^/foo/\*(Aq empty_file
.Ve

The behaviour of:

.Vb 1
    perl -pi -e \*(Aqs/^/foo/\*(Aq empty_file
.Ve

is unchanged (it continues to leave the file empty).
.ie n .SS """eval \*(Aq...\*(Aq"" improvements"
.el .SS "\f(CWeval \*(Aq...\*(Aq improvements"
Subsection "eval ... improvements"
Line numbers (as reflected by **caller()** and most diagnostics) within
\f(CW\*(C`eval \*(Aq...\*(Aq\*(C' were often incorrect where here documents were involved.
This has been corrected.

Lexical lookups for variables appearing in \f(CW\*(C`eval \*(Aq...\*(Aq\*(C' within
functions that were themselves called within an \f(CW\*(C`eval \*(Aq...\*(Aq\*(C' were
searching the wrong place for lexicals.  The lexical search now
correctly ends at the subroutine's block boundary.

The use of \f(CW\*(C`return\*(C' within \f(CW\*(C`eval \{...\}\*(C' caused $@ not to be reset
correctly when no exception occurred within the eval.  This has
been fixed.

Parsing of here documents used to be flawed when they appeared as
the replacement expression in \f(CW\*(C`eval \*(Aqs/.../.../e\*(Aq\*(C'.  This has
been fixed.

### All compilation errors are true errors

Subsection "All compilation errors are true errors"
Some \*(L"errors\*(R" encountered at compile time were by necessity
generated as warnings followed by eventual termination of the
program.  This enabled more such errors to be reported in a
single run, rather than causing a hard stop at the first error
that was encountered.

The mechanism for reporting such errors has been reimplemented
to queue compile-time errors and report them at the end of the
compilation as true errors rather than as warnings.  This fixes
cases where error messages leaked through in the form of warnings
when code was compiled at run time using \f(CW\*(C`eval STRING\*(C', and
also allows such errors to be reliably trapped using \f(CW\*(C`eval "..."\*(C'.

### Implicitly closed filehandles are safer

Subsection "Implicitly closed filehandles are safer"
Sometimes implicitly closed filehandles (as when they are localized,
and Perl automatically closes them on exiting the scope) could
inadvertently set $? or $!.  This has been corrected.

### Behavior of list slices is more consistent

Subsection "Behavior of list slices is more consistent"
When taking a slice of a literal list (as opposed to a slice of
an array or hash), Perl used to return an empty list if the
result happened to be composed of all undef values.

The new behavior is to produce an empty list if (and only if)
the original list was empty.  Consider the following example:

.Vb 1
    @a = (1,undef,undef,2)[2,1,2];
.Ve

The old behavior would have resulted in \f(CW@a having no elements.
The new behavior ensures it has three undefined elements.

Note in particular that the behavior of slices of the following
cases remains unchanged:

.Vb 5
    @a = ()[1,2];
    @a = (getpwent)[7,0];
    @a = (anything_returning_empty_list())[2,1,2];
    @a = @b[2,1,2];
    @a = @c\{\*(Aqa\*(Aq,\*(Aqb\*(Aq,\*(Aqc\*(Aq\};
.Ve

See perldata.
.ie n .SS """(\\$)"" prototype and $foo\{a\}"
.el .SS "\f(CW(\\$) prototype and \f(CW$foo\{a\}"
Subsection "($) prototype and $foo\{a\}"
A scalar reference prototype now correctly allows a hash or
array element in that slot.
.ie n .SS """goto &sub"" and \s-1AUTOLOAD\s0"
.el .SS "\f(CWgoto &sub and \s-1AUTOLOAD\s0"
Subsection "goto &sub and AUTOLOAD"
The \f(CW\*(C`goto &sub\*(C' construct works correctly when \f(CW&sub happens
to be autoloaded.
.ie n .SS """-bareword"" allowed under ""use integer"""
.el .SS "\f(CW-bareword allowed under \f(CWuse integer"
Subsection "-bareword allowed under use integer"
The autoquoting of barewords preceded by \f(CW\*(C`-\*(C' did not work
in prior versions when the \f(CW\*(C`integer\*(C' pragma was enabled.
This has been fixed.

### Failures in \s-1\fBDESTROY\s0()

Subsection "Failures in DESTROY()"
When code in a destructor threw an exception, it went unnoticed
in earlier versions of Perl, unless someone happened to be
looking in $@ just after the point the destructor happened to
run.  Such failures are now visible as warnings when warnings are
enabled.

### Locale bugs fixed

Subsection "Locale bugs fixed"
**printf()** and **sprintf()** previously reset the numeric locale
back to the default \*(L"C\*(R" locale.  This has been fixed.

Numbers formatted according to the local numeric locale
(such as using a decimal comma instead of a decimal dot) caused
\*(L"isn't numeric\*(R" warnings, even while the operations accessing
those numbers produced correct results.  These warnings have been
discontinued.

### Memory leaks

Subsection "Memory leaks"
The \f(CW\*(C`eval \*(Aqreturn sub \{...\}\*(Aq\*(C' construct could sometimes leak
memory.  This has been fixed.

Operations that aren't filehandle constructors used to leak memory
when used on invalid filehandles.  This has been fixed.

Constructs that modified \f(CW@_ could fail to deallocate values
in \f(CW@_ and thus leak memory.  This has been corrected.

### Spurious subroutine stubs after failed subroutine calls

Subsection "Spurious subroutine stubs after failed subroutine calls"
Perl could sometimes create empty subroutine stubs when a
subroutine was not found in the package.  Such cases stopped
later method lookups from progressing into base packages.
This has been corrected.
.ie n .SS "Taint failures under ""-U"""
.el .SS "Taint failures under \f(CW-U"
Subsection "Taint failures under -U"
When running in unsafe mode, taint violations could sometimes
cause silent failures.  This has been fixed.
.ie n .SS "\s-1END\s0 blocks and the ""-c"" switch"
.el .SS "\s-1END\s0 blocks and the \f(CW-c switch"
Subsection "END blocks and the -c switch"
Prior versions used to run \s-1BEGIN\s0 **and** \s-1END\s0 blocks when Perl was
run in compile-only mode.  Since this is typically not the expected
behavior, \s-1END\s0 blocks are not executed anymore when the \f(CW\*(C`-c\*(C' switch
is used, or if compilation fails.

See \*(L"Support for \s-1CHECK\s0 blocks\*(R" for how to run things when the compile
phase ends.

### Potential to leak \s-1DATA\s0 filehandles

Subsection "Potential to leak DATA filehandles"
Using the \f(CW\*(C`_\|_DATA_\|_\*(C' token creates an implicit filehandle to
the file that contains the token.  It is the program's
responsibility to close it when it is done reading from it.

This caveat is now better explained in the documentation.
See perldata.

## New or Changed Diagnostics

Header "New or Changed Diagnostics"
.ie n .IP """%s"" variable %s masks earlier declaration in same %s" 4
.el .IP "``%s'' variable \f(CW%s masks earlier declaration in same \f(CW%s" 4
Item "%s variable %s masks earlier declaration in same %s"
(W misc) A \*(L"my\*(R" or \*(L"our\*(R" variable has been redeclared in the current scope or statement,
effectively eliminating all access to the previous instance.  This is almost
always a typographical error.  Note that the earlier variable will still exist
until the end of the scope or until all closure referents to it are
destroyed.
.ie n .IP """my sub"" not yet implemented" 4
.el .IP "``my sub'' not yet implemented" 4
Item "my sub not yet implemented"
(F) Lexically scoped subroutines are not yet implemented.  Don't try that
yet.
.ie n .IP """our"" variable %s redeclared" 4
.el .IP "``our'' variable \f(CW%s redeclared" 4
Item "our variable %s redeclared"
(W misc) You seem to have already declared the same global once before in the
current lexical scope.
.ie n .IP "'!' allowed only after types %s" 4
.el .IP "'!' allowed only after types \f(CW%s" 4
Item "'!' allowed only after types %s"
(F) The '!' is allowed in **pack()** and **unpack()** only after certain types.
See \*(L"pack\*(R" in perlfunc.

- / cannot take a count
Item "/ cannot take a count"
(F) You had an unpack template indicating a counted-length string,
but you have also specified an explicit size for the string.
See \*(L"pack\*(R" in perlfunc.

- / must be followed by a, A or Z
Item "/ must be followed by a, A or Z"
(F) You had an unpack template indicating a counted-length string,
which must be followed by one of the letters a, A or Z
to indicate what sort of string is to be unpacked.
See \*(L"pack\*(R" in perlfunc.

- / must be followed by a*, A* or Z*
Item "/ must be followed by a*, A* or Z*"
(F) You had a pack template indicating a counted-length string,
Currently the only things that can have their length counted are a*, A* or Z*.
See \*(L"pack\*(R" in perlfunc.

- / must follow a numeric type
Item "/ must follow a numeric type"
(F) You had an unpack template that contained a '#',
but this did not follow some numeric unpack specification.
See \*(L"pack\*(R" in perlfunc.

- /%s/: Unrecognized escape \\\\%c passed through
Item "/%s/: Unrecognized escape %c passed through"
(W regexp) You used a backslash-character combination which is not recognized
by Perl.  This combination appears in an interpolated variable or a
\f(CW\*(C`\*(Aq\*(C'-delimited regular expression.  The character was understood literally.

- /%s/: Unrecognized escape \\\\%c in character class passed through
Item "/%s/: Unrecognized escape %c in character class passed through"
(W regexp) You used a backslash-character combination which is not recognized
by Perl inside character classes.  The character was understood literally.
.ie n .IP "/%s/ should probably be written as ""%s""" 4
.el .IP "/%s/ should probably be written as ``%s''" 4
Item "/%s/ should probably be written as %s"
(W syntax) You have used a pattern where Perl expected to find a string,
as in the first argument to \f(CW\*(C`join\*(C'.  Perl will treat the true
or false result of matching the pattern against \f(CW$_ as the string,
which is probably not what you had in mind.

- %s() called too early to check prototype
Item "%s() called too early to check prototype"
(W prototype) You've called a function that has a prototype before the parser saw a
definition or declaration for it, and Perl could not check that the call
conforms to the prototype.  You need to either add an early prototype
declaration for the subroutine in question, or move the subroutine
definition ahead of the call to get proper prototype checking.  Alternatively,
if you are certain that you're calling the function correctly, you may put
an ampersand before the name to avoid the warning.  See perlsub.
.ie n .IP "%s argument is not a \s-1HASH\s0 or \s-1ARRAY\s0 element" 4
.el .IP "\f(CW%s argument is not a \s-1HASH\s0 or \s-1ARRAY\s0 element" 4
Item "%s argument is not a HASH or ARRAY element"
(F) The argument to **exists()** must be a hash or array element, such as:
.Sp
.Vb 2
    $foo\{$bar\}
    $ref->\{"susie"\}[12]
.Ve
.ie n .IP "%s argument is not a \s-1HASH\s0 or \s-1ARRAY\s0 element or slice" 4
.el .IP "\f(CW%s argument is not a \s-1HASH\s0 or \s-1ARRAY\s0 element or slice" 4
Item "%s argument is not a HASH or ARRAY element or slice"
(F) The argument to **delete()** must be either a hash or array element, such as:
.Sp
.Vb 2
    $foo\{$bar\}
    $ref->\{"susie"\}[12]
.Ve
.Sp
or a hash or array slice, such as:
.Sp
.Vb 2
    @foo[$bar, $baz, $xyzzy]
    @\{$ref->[12]\}\{"susie", "queue"\}
.Ve
.ie n .IP "%s argument is not a subroutine name" 4
.el .IP "\f(CW%s argument is not a subroutine name" 4
Item "%s argument is not a subroutine name"
(F) The argument to **exists()** for \f(CW\*(C`exists &sub\*(C' must be a subroutine
name, and not a subroutine call.  \f(CW\*(C`exists &sub()\*(C' will generate this error.
.ie n .IP "%s package attribute may clash with future reserved word: %s" 4
.el .IP "\f(CW%s package attribute may clash with future reserved word: \f(CW%s" 4
Item "%s package attribute may clash with future reserved word: %s"
(W reserved) A lowercase attribute name was used that had a package-specific handler.
That name might have a meaning to Perl itself some day, even though it
doesn't yet.  Perhaps you should use a mixed-case attribute name, instead.
See attributes.
.ie n .IP "(in cleanup) %s" 4
.el .IP "(in cleanup) \f(CW%s" 4
Item "(in cleanup) %s"
(W misc) This prefix usually indicates that a \s-1**DESTROY\s0()** method raised
the indicated exception.  Since destructors are usually called by
the system at arbitrary points during execution, and often a vast
number of times, the warning is issued only once for any number
of failures that would otherwise result in the same message being
repeated.
.Sp
Failure of user callbacks dispatched using the \f(CW\*(C`G_KEEPERR\*(C' flag
could also result in this warning.  See \*(L"G_KEEPERR\*(R" in perlcall.

- <> should be quotes
Item "<> should be quotes"
(F) You wrote \f(CW\*(C`require <file>\*(C' when you should have written
\f(CW\*(C`require \*(Aqfile\*(Aq\*(C'.

- Attempt to join self
Item "Attempt to join self"
(F) You tried to join a thread from within itself, which is an
impossible task.  You may be joining the wrong thread, or you may
need to move the **join()** to some other thread.

- Bad evalled substitution pattern
Item "Bad evalled substitution pattern"
(F) You've used the /e switch to evaluate the replacement for a
substitution, but perl found a syntax error in the code to evaluate,
most likely an unexpected right brace '\}'.

- Bad \fBrealloc() ignored
Item "Bad realloc() ignored"
(S) An internal routine called **realloc()** on something that had never been
**malloc()**ed in the first place. Mandatory, but can be disabled by
setting environment variable \f(CW\*(C`PERL_BADFREE\*(C' to 1.

- Bareword found in conditional
Item "Bareword found in conditional"
(W bareword) The compiler found a bareword where it expected a conditional,
which often indicates that an || or && was parsed as part of the
last argument of the previous construct, for example:
.Sp
.Vb 1
    open FOO || die;
.Ve
.Sp
It may also indicate a misspelled constant that has been interpreted
as a bareword:
.Sp
.Vb 2
    use constant TYPO => 1;
    if (TYOP) \{ print "foo" \}
.Ve
.Sp
The \f(CW\*(C`strict\*(C' pragma is useful in avoiding such errors.

- Binary number > 0b11111111111111111111111111111111 non-portable
Item "Binary number > 0b11111111111111111111111111111111 non-portable"
(W portable) The binary number you specified is larger than 2**32-1
(4294967295) and therefore non-portable between systems.  See
perlport for more on portability concerns.

- Bit vector size > 32 non-portable
Item "Bit vector size > 32 non-portable"
(W portable) Using bit vector sizes larger than 32 is non-portable.
.ie n .IP "Buffer overflow in prime_env_iter: %s" 4
.el .IP "Buffer overflow in prime_env_iter: \f(CW%s" 4
Item "Buffer overflow in prime_env_iter: %s"
(W internal) A warning peculiar to \s-1VMS.\s0  While Perl was preparing to iterate over
\f(CW%ENV, it encountered a logical name or symbol definition which was too long,
so it was truncated to the string shown.
.ie n .IP "Can't check filesystem of script ""%s""" 4
.el .IP "Can't check filesystem of script ``%s''" 4
Item "Can't check filesystem of script %s"
(P) For some reason you can't check the filesystem of the script for nosuid.
.ie n .IP "Can't declare class for non-scalar %s in ""%s""" 4
.el .IP "Can't declare class for non-scalar \f(CW%s in ``%s''" 4
Item "Can't declare class for non-scalar %s in %s"
(S) Currently, only scalar variables can declared with a specific class
qualifier in a \*(L"my\*(R" or \*(L"our\*(R" declaration.  The semantics may be extended
for other types of variables in future.
.ie n .IP "Can't declare %s in ""%s""" 4
.el .IP "Can't declare \f(CW%s in ``%s''" 4
Item "Can't declare %s in %s"
(F) Only scalar, array, and hash variables may be declared as \*(L"my\*(R" or
\*(L"our\*(R" variables.  They must have ordinary identifiers as names.

- Can't ignore signal \s-1CHLD,\s0 forcing to default
Item "Can't ignore signal CHLD, forcing to default"
(W signal) Perl has detected that it is being run with the \s-1SIGCHLD\s0 signal
(sometimes known as \s-1SIGCLD\s0) disabled.  Since disabling this signal
will interfere with proper determination of exit status of child
processes, Perl has reset the signal to its default value.
This situation typically indicates that the parent program under
which Perl may be running (e.g., cron) is being very careless.

- Can't modify non-lvalue subroutine call
Item "Can't modify non-lvalue subroutine call"
(F) Subroutines meant to be used in lvalue context should be declared as
such, see \*(L"Lvalue subroutines\*(R" in perlsub.

- Can't read \s-1CRTL\s0 environ
Item "Can't read CRTL environ"
(S) A warning peculiar to \s-1VMS.\s0  Perl tried to read an element of \f(CW%ENV
from the \s-1CRTL\s0's internal environment array and discovered the array was
missing.  You need to figure out where your \s-1CRTL\s0 misplaced its environ
or define *\s-1PERL_ENV_TABLES\s0* (see perlvms) so that environ is not searched.
.ie n .IP "Can't remove %s: %s, skipping file" 4
.el .IP "Can't remove \f(CW%s: \f(CW%s, skipping file" 4
Item "Can't remove %s: %s, skipping file"
(S) You requested an inplace edit without creating a backup file.  Perl
was unable to remove the original file to replace it with the modified
file.  The file was left unmodified.
.ie n .IP "Can't return %s from lvalue subroutine" 4
.el .IP "Can't return \f(CW%s from lvalue subroutine" 4
Item "Can't return %s from lvalue subroutine"
(F) Perl detected an attempt to return illegal lvalues (such
as temporary or readonly values) from a subroutine used as an lvalue.
This is not allowed.

- Can't weaken a nonreference
Item "Can't weaken a nonreference"
(F) You attempted to weaken something that was not a reference.  Only
references can be weakened.

- Character class [:%s:] unknown
Item "Character class [:%s:] unknown"
(F) The class in the character class [: :] syntax is unknown.
See perlre.

- Character class syntax [%s] belongs inside character classes
Item "Character class syntax [%s] belongs inside character classes"
(W unsafe) The character class constructs [: :], [= =], and [. .]  go
*inside* character classes, the [] are part of the construct,
for example: /[012[:alpha:]345]/.  Note that [= =] and [. .]
are not currently implemented; they are simply placeholders for
future extensions.
.ie n .IP "Constant is not %s reference" 4
.el .IP "Constant is not \f(CW%s reference" 4
Item "Constant is not %s reference"
(F) A constant value (perhaps declared using the \f(CW\*(C`use constant\*(C' pragma)
is being dereferenced, but it amounts to the wrong type of reference.  The
message indicates the type of reference that was expected. This usually
indicates a syntax error in dereferencing the constant value.
See \*(L"Constant Functions\*(R" in perlsub and constant.
.ie n .IP "constant(%s): %s" 4
.el .IP "constant(%s): \f(CW%s" 4
Item "constant(%s): %s"
(F) The parser found inconsistencies either while attempting to define an
overloaded constant, or when trying to find the character name specified
in the \f(CW\*(C`\\N\{...\}\*(C' escape.  Perhaps you forgot to load the corresponding
\f(CW\*(C`overload\*(C' or \f(CW\*(C`charnames\*(C' pragma?  See charnames and overload.

- CORE::%s is not a keyword
Item "CORE::%s is not a keyword"
(F) The \s-1CORE::\s0 namespace is reserved for Perl keywords.

- defined(@array) is deprecated
Item "defined(@array) is deprecated"
(D) **defined()** is not usually useful on arrays because it checks for an
undefined *scalar* value.  If you want to see if the array is empty,
just use \f(CW\*(C`if (@array) \{ # not empty \}\*(C' for example.

- defined(%hash) is deprecated
Item "defined(%hash) is deprecated"
(D) **defined()** is not usually useful on hashes because it checks for an
undefined *scalar* value.  If you want to see if the hash is empty,
just use \f(CW\*(C`if (%hash) \{ # not empty \}\*(C' for example.

- Did not produce a valid header
Item "Did not produce a valid header"
See Server error.
.ie n .IP "(Did you mean ""local"" instead of ""our""?)" 4
.el .IP "(Did you mean ``local'' instead of ``our''?)" 4
Item "(Did you mean local instead of our?)"
(W misc) Remember that \*(L"our\*(R" does not localize the declared global variable.
You have declared it again in the same lexical scope, which seems superfluous.

- Document contains no data
Item "Document contains no data"
See Server error.
.ie n .IP "entering effective %s failed" 4
.el .IP "entering effective \f(CW%s failed" 4
Item "entering effective %s failed"
(F) While under the \f(CW\*(C`use filetest\*(C' pragma, switching the real and
effective uids or gids failed.
.ie n .IP "false [] range ""%s"" in regexp" 4
.el .IP "false [] range ``%s'' in regexp" 4
Item "false [] range %s in regexp"
(W regexp) A character class range must start and end at a literal character, not
another character class like \f(CW\*(C`\\d\*(C' or \f(CW\*(C`[:alpha:]\*(C'.  The \*(L"-\*(R" in your false
range is interpreted as a literal \*(L"-\*(R".  Consider quoting the \*(L"-\*(R",  \*(L"\\-\*(R".
See perlre.
.ie n .IP "Filehandle %s opened only for output" 4
.el .IP "Filehandle \f(CW%s opened only for output" 4
Item "Filehandle %s opened only for output"
(W io) You tried to read from a filehandle opened only for writing.  If you
intended it to be a read/write filehandle, you needed to open it with
\*(L"+<\*(R" or \*(L"+>\*(R" or \*(L"+>>\*(R" instead of with \*(L"<\*(R" or nothing.  If
you intended only to read from the file, use \*(L"<\*(R".  See
\*(L"open\*(R" in perlfunc.
.ie n .IP "**flock()** on closed filehandle %s" 4
.el .IP "**flock()** on closed filehandle \f(CW%s" 4
Item "flock() on closed filehandle %s"
(W closed) The filehandle you're attempting to **flock()** got itself closed some
time before now.  Check your logic flow.  **flock()** operates on filehandles.
Are you attempting to call **flock()** on a dirhandle by the same name?
.ie n .IP "Global symbol ""%s"" requires explicit package name" 4
.el .IP "Global symbol ``%s'' requires explicit package name" 4
Item "Global symbol %s requires explicit package name"
(F) You've said \*(L"use strict vars\*(R", which indicates that all variables
must either be lexically scoped (using \*(L"my\*(R"), declared beforehand using
\*(L"our\*(R", or explicitly qualified to say which package the global variable
is in (using \*(L"::\*(R").

- Hexadecimal number > 0xffffffff non-portable
Item "Hexadecimal number > 0xffffffff non-portable"
(W portable) The hexadecimal number you specified is larger than 2**32-1
(4294967295) and therefore non-portable between systems.  See
perlport for more on portability concerns.
.ie n .IP "Ill-formed \s-1CRTL\s0 environ value ""%s""" 4
.el .IP "Ill-formed \s-1CRTL\s0 environ value ``%s''" 4
Item "Ill-formed CRTL environ value %s"
(W internal) A warning peculiar to \s-1VMS.\s0  Perl tried to read the \s-1CRTL\s0's internal
environ array, and encountered an element without the \f(CW\*(C`=\*(C' delimiter
used to separate keys from values.  The element is ignored.

- Ill-formed message in prime_env_iter: |%s|
Item "Ill-formed message in prime_env_iter: |%s|"
(W internal) A warning peculiar to \s-1VMS.\s0  Perl tried to read a logical name
or \s-1CLI\s0 symbol definition when preparing to iterate over \f(CW%ENV, and
didn't see the expected delimiter between key and value, so the
line was ignored.
.ie n .IP "Illegal binary digit %s" 4
.el .IP "Illegal binary digit \f(CW%s" 4
Item "Illegal binary digit %s"
(F) You used a digit other than 0 or 1 in a binary number.
.ie n .IP "Illegal binary digit %s ignored" 4
.el .IP "Illegal binary digit \f(CW%s ignored" 4
Item "Illegal binary digit %s ignored"
(W digit) You may have tried to use a digit other than 0 or 1 in a binary number.
Interpretation of the binary number stopped before the offending digit.

- Illegal number of bits in vec
Item "Illegal number of bits in vec"
(F) The number of bits in **vec()** (the third argument) must be a power of
two from 1 to 32 (or 64, if your platform supports that).
.ie n .IP "Integer overflow in %s number" 4
.el .IP "Integer overflow in \f(CW%s number" 4
Item "Integer overflow in %s number"
(W overflow) The hexadecimal, octal or binary number you have specified either
as a literal or as an argument to **hex()** or **oct()** is too big for your
architecture, and has been converted to a floating point number.  On a
32-bit architecture the largest hexadecimal, octal or binary number
representable without overflow is 0xFFFFFFFF, 037777777777, or
0b11111111111111111111111111111111 respectively.  Note that Perl
transparently promotes all numbers to a floating point representation
internally\*(--subject to loss of precision errors in subsequent
operations.
.ie n .IP "Invalid %s attribute: %s" 4
.el .IP "Invalid \f(CW%s attribute: \f(CW%s" 4
Item "Invalid %s attribute: %s"
The indicated attribute for a subroutine or variable was not recognized
by Perl or by a user-supplied handler.  See attributes.
.ie n .IP "Invalid %s attributes: %s" 4
.el .IP "Invalid \f(CW%s attributes: \f(CW%s" 4
Item "Invalid %s attributes: %s"
The indicated attributes for a subroutine or variable were not recognized
by Perl or by a user-supplied handler.  See attributes.
.ie n .IP "invalid [] range ""%s"" in regexp" 4
.el .IP "invalid [] range ``%s'' in regexp" 4
Item "invalid [] range %s in regexp"
The offending range is now explicitly displayed.
.ie n .IP "Invalid separator character %s in attribute list" 4
.el .IP "Invalid separator character \f(CW%s in attribute list" 4
Item "Invalid separator character %s in attribute list"
(F) Something other than a colon or whitespace was seen between the
elements of an attribute list.  If the previous attribute
had a parenthesised parameter list, perhaps that list was terminated
too soon.  See attributes.
.ie n .IP "Invalid separator character %s in subroutine attribute list" 4
.el .IP "Invalid separator character \f(CW%s in subroutine attribute list" 4
Item "Invalid separator character %s in subroutine attribute list"
(F) Something other than a colon or whitespace was seen between the
elements of a subroutine attribute list.  If the previous attribute
had a parenthesised parameter list, perhaps that list was terminated
too soon.
.ie n .IP "leaving effective %s failed" 4
.el .IP "leaving effective \f(CW%s failed" 4
Item "leaving effective %s failed"
(F) While under the \f(CW\*(C`use filetest\*(C' pragma, switching the real and
effective uids or gids failed.
.ie n .IP "Lvalue subs returning %s not implemented yet" 4
.el .IP "Lvalue subs returning \f(CW%s not implemented yet" 4
Item "Lvalue subs returning %s not implemented yet"
(F) Due to limitations in the current implementation, array and hash
values cannot be returned in subroutines used in lvalue context.
See \*(L"Lvalue subroutines\*(R" in perlsub.
.ie n .IP "Method %s not permitted" 4
.el .IP "Method \f(CW%s not permitted" 4
Item "Method %s not permitted"
See Server error.
.ie n .IP "Missing %sbrace%s on \\N\{\}" 4
.el .IP "Missing \f(CW%sbrace%s on \\N\{\}" 4
Item "Missing %sbrace%s on N\{\}"
(F) Wrong syntax of character name literal \f(CW\*(C`\\N\{charname\}\*(C' within
double-quotish context.

- Missing command in piped open
Item "Missing command in piped open"
(W pipe) You used the \f(CW\*(C`open(FH, "| command")\*(C' or \f(CW\*(C`open(FH, "command |")\*(C'
construction, but the command was missing or blank.
.ie n .IP "Missing name in ""my sub""" 4
.el .IP "Missing name in ``my sub''" 4
Item "Missing name in my sub"
(F) The reserved syntax for lexically scoped subroutines requires that they
have a name with which they can be found.
.ie n .IP "No %s specified for -%c" 4
.el .IP "No \f(CW%s specified for -%c" 4
Item "No %s specified for -%c"
(F) The indicated command line switch needs a mandatory argument, but
you haven't specified one.
.ie n .IP "No package name allowed for variable %s in ""our""" 4
.el .IP "No package name allowed for variable \f(CW%s in ``our''" 4
Item "No package name allowed for variable %s in our"
(F) Fully qualified variable names are not allowed in \*(L"our\*(R" declarations,
because that doesn't make much sense under existing semantics.  Such
syntax is reserved for future extensions.

- No space allowed after -%c
Item "No space allowed after -%c"
(F) The argument to the indicated command line switch must follow immediately
after the switch, without intervening spaces.

- no \s-1UTC\s0 offset information; assuming local time is \s-1UTC\s0
Item "no UTC offset information; assuming local time is UTC"
(S) A warning peculiar to \s-1VMS.\s0  Perl was unable to find the local
timezone offset, so it's assuming that local system time is equivalent
to \s-1UTC.\s0  If it's not, define the logical name *\s-1SYS$TIMEZONE_DIFFERENTIAL\s0*
to translate to the number of seconds which need to be added to \s-1UTC\s0 to
get local time.

- Octal number > 037777777777 non-portable
Item "Octal number > 037777777777 non-portable"
(W portable) The octal number you specified is larger than 2**32-1 (4294967295)
and therefore non-portable between systems.  See perlport for more
on portability concerns.
.Sp
See also perlport for writing portable code.

- panic: del_backref
Item "panic: del_backref"
(P) Failed an internal consistency check while trying to reset a weak
reference.

- panic: kid popen errno read
Item "panic: kid popen errno read"
(F) forked child returned an incomprehensible message about its errno.

- panic: magic_killbackrefs
Item "panic: magic_killbackrefs"
(P) Failed an internal consistency check while trying to reset all weak
references to an object.
.ie n .IP "Parentheses missing around ""%s"" list" 4
.el .IP "Parentheses missing around ``%s'' list" 4
Item "Parentheses missing around %s list"
(W parenthesis) You said something like
.Sp
.Vb 1
    my $foo, $bar = @_;
.Ve
.Sp
when you meant
.Sp
.Vb 1
    my ($foo, $bar) = @_;
.Ve
.Sp
Remember that \*(L"my\*(R", \*(L"our\*(R", and \*(L"local\*(R" bind tighter than comma.
.ie n .IP "Possible unintended interpolation of %s in string" 4
.el .IP "Possible unintended interpolation of \f(CW%s in string" 4
Item "Possible unintended interpolation of %s in string"
(W ambiguous) It used to be that Perl would try to guess whether you
wanted an array interpolated or a literal @.  It no longer does this;
arrays are now *always* interpolated into strings.  This means that
if you try something like:
.Sp
.Vb 1
        print "fred@example.com";
.Ve
.Sp
and the array \f(CW@example doesn't exist, Perl is going to print
\f(CW\*(C`fred.com\*(C', which is probably not what you wanted.  To get a literal
\f(CW\*(C`@\*(C' sign in a string, put a backslash before it, just as you would
to get a literal \f(CW\*(C`$\*(C' sign.
.ie n .IP "Possible Y2K bug: %s" 4
.el .IP "Possible Y2K bug: \f(CW%s" 4
Item "Possible Y2K bug: %s"
(W y2k) You are concatenating the number 19 with another number, which
could be a potential Year 2000 problem.
.ie n .IP "pragma ""attrs"" is deprecated, use ""sub \s-1NAME : ATTRS""\s0 instead" 4
.el .IP "pragma ``attrs'' is deprecated, use ``sub \s-1NAME : ATTRS''\s0 instead" 4
Item "pragma attrs is deprecated, use sub NAME : ATTRS instead"
(W deprecated) You have written something like this:
.Sp
.Vb 4
    sub doit
    \{
        use attrs qw(locked);
    \}
.Ve
.Sp
You should use the new declaration syntax instead.
.Sp
.Vb 3
    sub doit : locked
    \{
        ...
.Ve
.Sp
The \f(CW\*(C`use attrs\*(C' pragma is now obsolete, and is only provided for
backward-compatibility. See \*(L"Subroutine Attributes\*(R" in perlsub.

- Premature end of script headers
Item "Premature end of script headers"
See Server error.

- Repeat count in pack overflows
Item "Repeat count in pack overflows"
(F) You can't specify a repeat count so large that it overflows
your signed integers.  See \*(L"pack\*(R" in perlfunc.

- Repeat count in unpack overflows
Item "Repeat count in unpack overflows"
(F) You can't specify a repeat count so large that it overflows
your signed integers.  See \*(L"unpack\*(R" in perlfunc.

- \fBrealloc() of freed memory ignored
Item "realloc() of freed memory ignored"
(S) An internal routine called **realloc()** on something that had already
been freed.

- Reference is already weak
Item "Reference is already weak"
(W misc) You have attempted to weaken a reference that is already weak.
Doing so has no effect.

- setpgrp can't take arguments
Item "setpgrp can't take arguments"
(F) Your system has the **setpgrp()** from \s-1BSD 4.2,\s0 which takes no arguments,
unlike \s-1POSIX\s0 **setpgid()**, which takes a process \s-1ID\s0 and process group \s-1ID.\s0

- Strange *+?\{\} on zero-length expression
Item "Strange *+?\{\} on zero-length expression"
(W regexp) You applied a regular expression quantifier in a place where it
makes no sense, such as on a zero-width assertion.
Try putting the quantifier inside the assertion instead.  For example,
the way to match \*(L"abc\*(R" provided that it is followed by three
repetitions of \*(L"xyz\*(R" is \f(CW\*(C`/abc(?=(?:xyz)\{3\})/\*(C', not \f(CW\*(C`/abc(?=xyz)\{3\}/\*(C'.
.ie n .IP "switching effective %s is not implemented" 4
.el .IP "switching effective \f(CW%s is not implemented" 4
Item "switching effective %s is not implemented"
(F) While under the \f(CW\*(C`use filetest\*(C' pragma, we cannot switch the
real and effective uids or gids.

- This Perl can't reset \s-1CRTL\s0 environ elements (%s)
Item "This Perl can't reset CRTL environ elements (%s)"
0

- This Perl can't set \s-1CRTL\s0 environ elements (%s=%s)
Item "This Perl can't set CRTL environ elements (%s=%s)"
.PD
(W internal) Warnings peculiar to \s-1VMS.\s0  You tried to change or delete an element
of the \s-1CRTL\s0's internal environ array, but your copy of Perl wasn't
built with a \s-1CRTL\s0 that contained the **setenv()** function.  You'll need to
rebuild Perl with a \s-1CRTL\s0 that does, or redefine *\s-1PERL_ENV_TABLES\s0* (see
perlvms) so that the environ array isn't the target of the change to
\f(CW%ENV which produced the warning.
.ie n .IP "Too late to run %s block" 4
.el .IP "Too late to run \f(CW%s block" 4
Item "Too late to run %s block"
(W void) A \s-1CHECK\s0 or \s-1INIT\s0 block is being defined during run time proper,
when the opportunity to run them has already passed.  Perhaps you are
loading a file with \f(CW\*(C`require\*(C' or \f(CW\*(C`do\*(C' when you should be using
\f(CW\*(C`use\*(C' instead.  Or perhaps you should put the \f(CW\*(C`require\*(C' or \f(CW\*(C`do\*(C'
inside a \s-1BEGIN\s0 block.

- Unknown \fBopen() mode '%s'
Item "Unknown open() mode '%s'"
(F) The second argument of 3-argument **open()** is not among the list
of valid modes: \f(CW\*(C`<\*(C', \f(CW\*(C`>\*(C', \f(CW\*(C`>>\*(C', \f(CW\*(C`+<\*(C',
\f(CW\*(C`+>\*(C', \f(CW\*(C`+>>\*(C', \f(CW\*(C`-|\*(C', \f(CW\*(C`|-\*(C'.
.ie n .IP "Unknown process %x sent message to prime_env_iter: %s" 4
.el .IP "Unknown process \f(CW%x sent message to prime_env_iter: \f(CW%s" 4
Item "Unknown process %x sent message to prime_env_iter: %s"
(P) An error peculiar to \s-1VMS.\s0  Perl was reading values for \f(CW%ENV before
iterating over it, and someone else stuck a message in the stream of
data Perl expected.  Someone's very confused, or perhaps trying to
subvert Perl's population of \f(CW%ENV for nefarious purposes.

- Unrecognized escape \\\\%c passed through
Item "Unrecognized escape %c passed through"
(W misc) You used a backslash-character combination which is not recognized
by Perl.  The character was understood literally.

- Unterminated attribute parameter in attribute list
Item "Unterminated attribute parameter in attribute list"
(F) The lexer saw an opening (left) parenthesis character while parsing an
attribute list, but the matching closing (right) parenthesis
character was not found.  You may need to add (or remove) a backslash
character to get your parentheses to balance.  See attributes.

- Unterminated attribute list
Item "Unterminated attribute list"
(F) The lexer found something other than a simple identifier at the start
of an attribute, and it wasn't a semicolon or the start of a
block.  Perhaps you terminated the parameter list of the previous attribute
too soon.  See attributes.

- Unterminated attribute parameter in subroutine attribute list
Item "Unterminated attribute parameter in subroutine attribute list"
(F) The lexer saw an opening (left) parenthesis character while parsing a
subroutine attribute list, but the matching closing (right) parenthesis
character was not found.  You may need to add (or remove) a backslash
character to get your parentheses to balance.

- Unterminated subroutine attribute list
Item "Unterminated subroutine attribute list"
(F) The lexer found something other than a simple identifier at the start
of a subroutine attribute, and it wasn't a semicolon or the start of a
block.  Perhaps you terminated the parameter list of the previous attribute
too soon.
.ie n .IP "Value of \s-1CLI\s0 symbol ""%s"" too long" 4
.el .IP "Value of \s-1CLI\s0 symbol ``%s'' too long" 4
Item "Value of CLI symbol %s too long"
(W misc) A warning peculiar to \s-1VMS.\s0  Perl tried to read the value of an \f(CW%ENV
element from a \s-1CLI\s0 symbol table, and found a resultant string longer
than 1024 characters.  The return value has been truncated to 1024
characters.

- Version number must be a constant number
Item "Version number must be a constant number"
(P) The attempt to translate a \f(CW\*(C`use Module n.n LIST\*(C' statement into
its equivalent \f(CW\*(C`BEGIN\*(C' block found an internal inconsistency with
the version number.

## New tests

Header "New tests"

- lib/attrs
Item "lib/attrs"
Compatibility tests for \f(CW\*(C`sub : attrs\*(C' vs the older \f(CW\*(C`use attrs\*(C'.

- lib/env
Item "lib/env"
Tests for new environment scalar capability (e.g., \f(CW\*(C`use Env qw($BAR);\*(C').

- lib/env-array
Item "lib/env-array"
Tests for new environment array capability (e.g., \f(CW\*(C`use Env qw(@PATH);\*(C').

- lib/io_const
Item "lib/io_const"
\s-1IO\s0 constants (SEEK_*, _IO*).

- lib/io_dir
Item "lib/io_dir"
Directory-related \s-1IO\s0 methods (new, read, close, rewind, tied delete).

- lib/io_multihomed
Item "lib/io_multihomed"
\s-1INET\s0 sockets with multi-homed hosts.

- lib/io_poll
Item "lib/io_poll"
\s-1IO\s0 **poll()**.

- lib/io_unix
Item "lib/io_unix"
\s-1UNIX\s0 sockets.

- op/attrs
Item "op/attrs"
Regression tests for \f(CW\*(C`my ($x,@y,%z) : attrs\*(C' and <sub : attrs>.

- op/filetest
Item "op/filetest"
File test operators.

- op/lex_assign
Item "op/lex_assign"
Verify operations that access pad objects (lexicals and temporaries).

- op/exists_sub
Item "op/exists_sub"
Verify \f(CW\*(C`exists &sub\*(C' operations.

## Incompatible Changes

Header "Incompatible Changes"

### Perl Source Incompatibilities

Subsection "Perl Source Incompatibilities"
Beware that any new warnings that have been added or old ones
that have been enhanced are **not** considered incompatible changes.

Since all new warnings must be explicitly requested via the \f(CW\*(C`-w\*(C'
switch or the \f(CW\*(C`warnings\*(C' pragma, it is ultimately the programmer's
responsibility to ensure that warnings are enabled judiciously.

- \s-1CHECK\s0 is a new keyword
Item "CHECK is a new keyword"
All subroutine definitions named \s-1CHECK\s0 are now special.  See
\f(CW\*(C`/"Support for CHECK blocks"\*(C' for more information.

- Treatment of list slices of undef has changed
Item "Treatment of list slices of undef has changed"
There is a potential incompatibility in the behavior of list slices
that are comprised entirely of undefined values.
See \*(L"Behavior of list slices is more consistent\*(R".
.ie n .IP "Format of $English::PERL_VERSION is different" 4
.el .IP "Format of \f(CW$English::PERL_VERSION is different" 4
Item "Format of $English::PERL_VERSION is different"
The English module now sets \f(CW$PERL_VERSION to $^V (a string value) rather
than \f(CW$] (a numeric value).  This is a potential incompatibility.
Send us a report via perlbug if you are affected by this.
.Sp
See \*(L"Improved Perl version numbering system\*(R" for the reasons for
this change.
.ie n .IP "Literals of the form 1.2.3 parse differently" 4
.el .IP "Literals of the form \f(CW1.2.3 parse differently" 4
Item "Literals of the form 1.2.3 parse differently"
Previously, numeric literals with more than one dot in them were
interpreted as a floating point number concatenated with one or more
numbers.  Such \*(L"numbers\*(R" are now parsed as strings composed of the
specified ordinals.
.Sp
For example, \f(CW\*(C`print 97.98.99\*(C' used to output \f(CW97.9899 in earlier
versions, but now prints \f(CW\*(C`abc\*(C'.
.Sp
See \*(L"Support for strings represented as a vector of ordinals\*(R".

- Possibly changed pseudo-random number generator
Item "Possibly changed pseudo-random number generator"
Perl programs that depend on reproducing a specific set of pseudo-random
numbers may now produce different output due to improvements made to the
**rand()** builtin.  You can use \f(CW\*(C`sh Configure -Drandfunc=rand\*(C' to obtain
the old behavior.
.Sp
See \*(L"Better pseudo-random number generator\*(R".

- Hashing function for hash keys has changed
Item "Hashing function for hash keys has changed"
Even though Perl hashes are not order preserving, the apparently
random order encountered when iterating on the contents of a hash
is actually determined by the hashing algorithm used.  Improvements
in the algorithm may yield a random order that is **different** from
that of previous versions, especially when iterating on hashes.
.Sp
See \*(L"Better worst-case behavior of hashes\*(R" for additional
information.
.ie n .IP """undef"" fails on read only values" 4
.el .IP "\f(CWundef fails on read only values" 4
Item "undef fails on read only values"
Using the \f(CW\*(C`undef\*(C' operator on a readonly value (such as \f(CW$1) has
the same effect as assigning \f(CW\*(C`undef\*(C' to the readonly value\*(--it
throws an exception.

- Close-on-exec bit may be set on pipe and socket handles
Item "Close-on-exec bit may be set on pipe and socket handles"
Pipe and socket handles are also now subject to the close-on-exec
behavior determined by the special variable $^F.
.Sp
See \*(L"More consistent close-on-exec behavior\*(R".
.ie n .IP "Writing ""$$1"" to mean ""$\{$\}1"" is unsupported" 4
.el .IP "Writing \f(CW``$$1'' to mean \f(CW``$\{$\}1'' is unsupported" 4
Item "Writing ""$$1"" to mean ""$\{$\}1"" is unsupported"
Perl 5.004 deprecated the interpretation of \f(CW$$1 and
similar within interpolated strings to mean \f(CW\*(C`$$ . "1"\*(C',
but still allowed it.
.Sp
In Perl 5.6.0 and later, \f(CW"$$1" always means \f(CW"$\{$1\}".
.ie n .IP "**delete()**, **each()**, **values()** and ""\\(%h)""" 4
.el .IP "**delete()**, **each()**, **values()** and \f(CW\\(%h)" 4
Item "delete(), each(), values() and )"
operate on aliases to values, not copies
.Sp
**delete()**, **each()**, **values()** and hashes (e.g. \f(CW\*(C`\\(%h)\*(C')
in a list context return the actual
values in the hash, instead of copies (as they used to in earlier
versions).  Typical idioms for using these constructs copy the
returned values, but this can make a significant difference when
creating references to the returned values.  Keys in the hash are still
returned as copies when iterating on a hash.
.Sp
See also \*(L"**delete()**, **each()**, **values()** and hash iteration are faster\*(R".

- vec(\s-1EXPR,OFFSET,BITS\s0) enforces powers-of-two \s-1BITS\s0
Item "vec(EXPR,OFFSET,BITS) enforces powers-of-two BITS"
**vec()** generates a run-time error if the \s-1BITS\s0 argument is not
a valid power-of-two integer.

- Text of some diagnostic output has changed
Item "Text of some diagnostic output has changed"
Most references to internal Perl operations in diagnostics
have been changed to be more descriptive.  This may be an
issue for programs that may incorrectly rely on the exact
text of diagnostics for proper functioning.
.ie n .IP """%@"" has been removed" 4
.el .IP "\f(CW%@ has been removed" 4
Item "%@ has been removed"
The undocumented special variable \f(CW\*(C`%@\*(C' that used to accumulate
\*(L"background\*(R" errors (such as those that happen in \s-1**DESTROY\s0()**)
has been removed, because it could potentially result in memory
leaks.

- Parenthesized \fBnot() behaves like a list operator
Item "Parenthesized not() behaves like a list operator"
The \f(CW\*(C`not\*(C' operator now falls under the \*(L"if it looks like a function,
it behaves like a function\*(R" rule.
.Sp
As a result, the parenthesized form can be used with \f(CW\*(C`grep\*(C' and \f(CW\*(C`map\*(C'.
The following construct used to be a syntax error before, but it works
as expected now:
.Sp
.Vb 1
    grep not($_), @things;
.Ve
.Sp
On the other hand, using \f(CW\*(C`not\*(C' with a literal list slice may not
work.  The following previously allowed construct:
.Sp
.Vb 1
    print not (1,2,3)[0];
.Ve
.Sp
needs to be written with additional parentheses now:
.Sp
.Vb 1
    print not((1,2,3)[0]);
.Ve
.Sp
The behavior remains unaffected when \f(CW\*(C`not\*(C' is not followed by parentheses.
.ie n .IP "Semantics of bareword prototype ""(*)"" have changed" 4
.el .IP "Semantics of bareword prototype \f(CW(*) have changed" 4
Item "Semantics of bareword prototype (*) have changed"
The semantics of the bareword prototype \f(CW\*(C`*\*(C' have changed.  Perl 5.005
always coerced simple scalar arguments to a typeglob, which wasn't useful
in situations where the subroutine must distinguish between a simple
scalar and a typeglob.  The new behavior is to not coerce bareword
arguments to a typeglob.  The value will always be visible as either
a simple scalar or as a reference to a typeglob.
.Sp
See \*(L"More functional bareword prototype (*)\*(R".

- Semantics of bit operators may have changed on 64-bit platforms
Item "Semantics of bit operators may have changed on 64-bit platforms"
If your platform is either natively 64-bit or if Perl has been
configured to used 64-bit integers, i.e., \f(CW$Config\{ivsize\} is 8,
there may be a potential incompatibility in the behavior of bitwise
numeric operators (& | ^ ~ << >>).  These operators used to strictly
operate on the lower 32 bits of integers in previous versions, but now
operate over the entire native integral width.  In particular, note
that unary \f(CW\*(C`~\*(C' will produce different results on platforms that have
different \f(CW$Config\{ivsize\}.  For portability, be sure to mask off
the excess bits in the result of unary \f(CW\*(C`~\*(C', e.g., \f(CW\*(C`~$x & 0xffffffff\*(C'.
.Sp
See \*(L"Bit operators support full native integer width\*(R".

- More builtins taint their results
Item "More builtins taint their results"
As described in \*(L"Improved security features\*(R", there may be more
sources of taint in a Perl program.
.Sp
To avoid these new tainting behaviors, you can build Perl with the
Configure option \f(CW\*(C`-Accflags=-DINCOMPLETE_TAINTS\*(C'.  Beware that the
ensuing perl binary may be insecure.

### C Source Incompatibilities

Subsection "C Source Incompatibilities"
.ie n .IP """PERL_POLLUTE""" 4
.el .IP "\f(CWPERL_POLLUTE" 4
Item "PERL_POLLUTE"
Release 5.005 grandfathered old global symbol names by providing preprocessor
macros for extension source compatibility.  As of release 5.6.0, these
preprocessor definitions are not available by default.  You need to explicitly
compile perl with \f(CW\*(C`-DPERL_POLLUTE\*(C' to get these definitions.  For
extensions still using the old symbols, this option can be
specified via MakeMaker:
.Sp
.Vb 1
    perl Makefile.PL POLLUTE=1
.Ve
.ie n .IP """PERL_IMPLICIT_CONTEXT""" 4
.el .IP "\f(CWPERL_IMPLICIT_CONTEXT" 4
Item "PERL_IMPLICIT_CONTEXT"
This new build option provides a set of macros for all \s-1API\s0 functions
such that an implicit interpreter/thread context argument is passed to
every \s-1API\s0 function.  As a result of this, something like \f(CW\*(C`sv_setsv(foo,bar)\*(C'
amounts to a macro invocation that actually translates to something like
\f(CW\*(C`Perl_sv_setsv(my_perl,foo,bar)\*(C'.  While this is generally expected
to not have any significant source compatibility issues, the difference
between a macro and a real function call will need to be considered.
.Sp
This means that there **is** a source compatibility issue as a result of
this if your extensions attempt to use pointers to any of the Perl \s-1API\s0
functions.
.Sp
Note that the above issue is not relevant to the default build of
Perl, whose interfaces continue to match those of prior versions
(but subject to the other options described here).
.Sp
See \*(L"Background and \s-1PERL_IMPLICIT_CONTEXT\*(R"\s0 in perlguts for detailed information
on the ramifications of building Perl with this option.
.Sp
.Vb 3
    NOTE: PERL_IMPLICIT_CONTEXT is automatically enabled whenever Perl is built
    with one of -Dusethreads, -Dusemultiplicity, or both.  It is not
    intended to be enabled by users at this time.
.Ve
.ie n .IP """PERL_POLLUTE_MALLOC""" 4
.el .IP "\f(CWPERL_POLLUTE_MALLOC" 4
Item "PERL_POLLUTE_MALLOC"
Enabling Perl's malloc in release 5.005 and earlier caused the namespace of
the system's malloc family of functions to be usurped by the Perl versions,
since by default they used the same names.  Besides causing problems on
platforms that do not allow these functions to be cleanly replaced, this
also meant that the system versions could not be called in programs that
used Perl's malloc.  Previous versions of Perl have allowed this behaviour
to be suppressed with the \s-1HIDEMYMALLOC\s0 and \s-1EMBEDMYMALLOC\s0 preprocessor
definitions.
.Sp
As of release 5.6.0, Perl's malloc family of functions have default names
distinct from the system versions.  You need to explicitly compile perl with
\f(CW\*(C`-DPERL_POLLUTE_MALLOC\*(C' to get the older behaviour.  \s-1HIDEMYMALLOC\s0
and \s-1EMBEDMYMALLOC\s0 have no effect, since the behaviour they enabled is now
the default.
.Sp
Note that these functions do **not** constitute Perl's memory allocation \s-1API.\s0
See \*(L"Memory Allocation\*(R" in perlguts for further information about that.

### Compatible C Source \s-1API\s0 Changes

Subsection "Compatible C Source API Changes"
.ie n .IP """PATCHLEVEL"" is now ""PERL_VERSION""" 4
.el .IP "\f(CWPATCHLEVEL is now \f(CWPERL_VERSION" 4
Item "PATCHLEVEL is now PERL_VERSION"
The cpp macros \f(CW\*(C`PERL_REVISION\*(C', \f(CW\*(C`PERL_VERSION\*(C', and \f(CW\*(C`PERL_SUBVERSION\*(C'
are now available by default from perl.h, and reflect the base revision,
patchlevel, and subversion respectively.  \f(CW\*(C`PERL_REVISION\*(C' had no
prior equivalent, while \f(CW\*(C`PERL_VERSION\*(C' and \f(CW\*(C`PERL_SUBVERSION\*(C' were
previously available as \f(CW\*(C`PATCHLEVEL\*(C' and \f(CW\*(C`SUBVERSION\*(C'.
.Sp
The new names cause less pollution of the **cpp** namespace and reflect what
the numbers have come to stand for in common practice.  For compatibility,
the old names are still supported when *patchlevel.h* is explicitly
included (as required before), so there is no source incompatibility
from the change.

### Binary Incompatibilities

Subsection "Binary Incompatibilities"
In general, the default build of this release is expected to be binary
compatible for extensions built with the 5.005 release or its maintenance
versions.  However, specific platforms may have broken binary compatibility
due to changes in the defaults used in hints files.  Therefore, please be
sure to always check the platform-specific \s-1README\s0 files for any notes to
the contrary.

The usethreads or usemultiplicity builds are **not** binary compatible
with the corresponding builds in 5.005.

On platforms that require an explicit list of exports (\s-1AIX, OS/2\s0 and Windows,
among others), purely internal symbols such as parser functions and the
run time opcodes are not exported by default.  Perl 5.005 used to export
all functions irrespective of whether they were considered part of the
public \s-1API\s0 or not.

For the full list of public \s-1API\s0 functions, see perlapi.

## Known Problems

Header "Known Problems"

### Localizing a tied hash element may leak memory

Subsection "Localizing a tied hash element may leak memory"
As of the 5.6.1 release, there is a known leak when code such as this
is executed:

.Vb 2
    use Tie::Hash;
    tie my %tie_hash => \*(AqTie::StdHash\*(Aq;

    ...

    local($tie_hash\{Foo\}) = 1; # leaks
.Ve

### Known test failures

Subsection "Known test failures"

- \(bu
64-bit builds
.Sp
Subtest #15 of lib/b.t may fail under 64-bit builds on platforms such
as HP-UX \s-1PA64\s0 and Linux \s-1IA64.\s0  The issue is still being investigated.
.Sp
The lib/io_multihomed test may hang in HP-UX if Perl has been
configured to be 64-bit.  Because other 64-bit platforms do not
hang in this test, HP-UX is suspect.  All other tests pass
in 64-bit HP-UX.  The test attempts to create and connect to
\*(L"multihomed\*(R" sockets (sockets which have multiple \s-1IP\s0 addresses).
.Sp
Note that 64-bit support is still experimental.

- \(bu
Failure of Thread tests
.Sp
The subtests 19 and 20 of lib/thr5005.t test are known to fail due to
fundamental problems in the 5.005 threading implementation.  These are
not new failures\*(--Perl 5.005_0x has the same bugs, but didn't have these
tests.  (Note that support for 5.005-style threading remains experimental.)

- \(bu
\s-1NEXTSTEP 3.3 POSIX\s0 test failure
.Sp
In \s-1NEXTSTEP 3\s0.3p2 the implementation of the **strftime**\|(3) in the
operating system libraries is buggy: the \f(CW%j format numbers the days of
a month starting from zero, which, while being logical to programmers,
will cause the subtests 19 to 27 of the lib/posix test may fail.

- \(bu
Tru64 (aka Digital \s-1UNIX,\s0 aka \s-1DEC OSF/1\s0) lib/sdbm test failure with gcc
.Sp
If compiled with gcc 2.95 the lib/sdbm test will fail (dump core).
The cure is to use the vendor cc, it comes with the operating system
and produces good code.

### \s-1EBCDIC\s0 platforms not fully supported

Subsection "EBCDIC platforms not fully supported"
In earlier releases of Perl, \s-1EBCDIC\s0 environments like \s-1OS390\s0 (also
known as Open Edition \s-1MVS\s0) and VM-ESA were supported.  Due to changes
required by the \s-1UTF-8\s0 (Unicode) support, the \s-1EBCDIC\s0 platforms are not
supported in Perl 5.6.0.

The 5.6.1 release improves support for \s-1EBCDIC\s0 platforms, but they
are not fully supported yet.

### UNICOS/mk \s-1CC\s0 failures during Configure run

Subsection "UNICOS/mk CC failures during Configure run"
In UNICOS/mk the following errors may appear during the Configure run:

.Vb 6
        Guessing which symbols your C compiler and preprocessor define...
        CC-20 cc: ERROR File = try.c, Line = 3
        ...
          bad switch yylook 79bad switch yylook 79bad switch yylook 79bad switch yylook 79#ifdef A29K
        ...
        4 errors detected in the compilation of "try.c".
.Ve

The culprit is the broken awk of UNICOS/mk.  The effect is fortunately
rather mild: Perl itself is not adversely affected by the error, only
the h2ph utility coming with Perl, and that is rather rarely needed
these days.

### Arrow operator and arrays

Subsection "Arrow operator and arrays"
When the left argument to the arrow operator \f(CW\*(C`->\*(C' is an array, or
the \f(CW\*(C`scalar\*(C' operator operating on an array, the result of the
operation must be considered erroneous. For example:

.Vb 2
    @x->[2]
    scalar(@x)->[2]
.Ve

These expressions will get run-time errors in some future release of
Perl.

### Experimental features

Subsection "Experimental features"
As discussed above, many features are still experimental.  Interfaces and
implementation of these features are subject to change, and in extreme cases,
even subject to removal in some future release of Perl.  These features
include the following:

- Threads
Item "Threads"
0

- Unicode
Item "Unicode"

- 64-bit support
Item "64-bit support"

- Lvalue subroutines
Item "Lvalue subroutines"

- Weak references
Item "Weak references"

- The pseudo-hash data type
Item "The pseudo-hash data type"

- The Compiler suite
Item "The Compiler suite"

- Internal implementation of file globbing
Item "Internal implementation of file globbing"

- The \s-1DB\s0 module
Item "The DB module"

- The regular expression code constructs:
Item "The regular expression code constructs:"
.PD
\f(CW\*(C`(?\{ code \})\*(C' and \f(CW\*(C`(??\{ code \})\*(C'

## Obsolete Diagnostics

Header "Obsolete Diagnostics"

- Character class syntax [: :] is reserved for future extensions
Item "Character class syntax [: :] is reserved for future extensions"
(W) Within regular expression character classes ([]) the syntax beginning
with \*(L"[:\*(R" and ending with \*(L":]\*(R" is reserved for future extensions.
If you need to represent those character sequences inside a regular
expression character class, just quote the square brackets with the
backslash: \*(L"\\[:\*(R" and \*(L":\\]\*(R".

- Ill-formed logical name |%s| in prime_env_iter
Item "Ill-formed logical name |%s| in prime_env_iter"
(W) A warning peculiar to \s-1VMS.\s0  A logical name was encountered when preparing
to iterate over \f(CW%ENV which violates the syntactic rules governing logical
names.  Because it cannot be translated normally, it is skipped, and will not
appear in \f(CW%ENV.  This may be a benign occurrence, as some software packages
might directly modify logical name tables and introduce nonstandard names,
or it may indicate that a logical name table has been corrupted.

- In string, @%s now must be written as \\@%s
Item "In string, @%s now must be written as @%s"
The description of this error used to say:
.Sp
.Vb 2
        (Someday it will simply assume that an unbackslashed @
         interpolates an array.)
.Ve
.Sp
That day has come, and this fatal error has been removed.  It has been
replaced by a non-fatal warning instead.
See \*(L"Arrays now always interpolate into double-quoted strings\*(R" for
details.
.ie n .IP "Probable precedence problem on %s" 4
.el .IP "Probable precedence problem on \f(CW%s" 4
Item "Probable precedence problem on %s"
(W) The compiler found a bareword where it expected a conditional,
which often indicates that an || or && was parsed as part of the
last argument of the previous construct, for example:
.Sp
.Vb 1
    open FOO || die;
.Ve

- regexp too big
Item "regexp too big"
(F) The current implementation of regular expressions uses shorts as
address offsets within a string.  Unfortunately this means that if
the regular expression compiles to longer than 32767, it'll blow up.
Usually when you want a regular expression this big, there is a better
way to do it with multiple statements.  See perlre.
.ie n .IP "Use of ""$$<digit>"" to mean ""$\{$\}<digit>"" is deprecated" 4
.el .IP "Use of ``$$<digit>'' to mean ``$\{$\}<digit>'' is deprecated" 4
Item "Use of $$<digit> to mean $\{$\}<digit> is deprecated"
(D) Perl versions before 5.004 misinterpreted any type marker followed
by \*(L"$\*(R" and a digit.  For example, \*(L"$$0\*(R" was incorrectly taken to mean
\*(L"$\{$\}0\*(R" instead of \*(L"$\{$0\}\*(R".  This bug is (mostly) fixed in Perl 5.004.
.Sp
However, the developers of Perl 5.004 could not fix this bug completely,
because at least two widely-used modules depend on the old meaning of
\*(L"$$0\*(R" in a string.  So Perl 5.004 still interprets \*(L"$$<digit>\*(R" in the
old (broken) way inside strings; but it generates this message as a
warning.  And in Perl 5.005, this special treatment will cease.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the
articles recently posted to the comp.lang.perl.misc newsgroup.
There may also be information at http://www.perl.com/ , the Perl
Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for exhaustive details on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.

## HISTORY

Header "HISTORY"
Written by Gurusamy Sarathy <*gsar@ActiveState.com*>, with many
contributions from The Perl Porters.

Send omissions or corrections to <*perlbug@perl.org*>.
