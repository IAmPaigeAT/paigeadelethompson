+++
author = "None Specified"
description = "This document describes differences between the 5.10.0 release and the 5.12.0 release. Many of the bug fixes in 5.12.0 are already included in the 5.10.1 maintenance release. You can see the list of those changes in the 5.10.1 release notes (perl..."
manpage_section = "1"
operating_system_version = "15.3"
operating_system = "macos"
manpage_name = "perl5120delta"
date = "2022-02-19"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information", "http", "dev", "org", "perl5", "errata", "html", "a", "list", "issues", "found", "after", "this", "release", "as", "well", "s-1cpan", "modules", "known", "be", "incompatible", "with"]
title = "perl5120delta(1)"
detected_package_version = "5.34.1"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5120DELTA 1"
PERL5120DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5120delta - what is new for perl v5.12.0

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.10.0 release and the
5.12.0 release.

Many of the bug fixes in 5.12.0 are already included in the 5.10.1
maintenance release.

You can see the list of those changes in the 5.10.1 release notes
(perl5101delta).

## Core Enhancements

Header "Core Enhancements"
.ie n .SS "New ""package NAME VERSION"" syntax"
.el .SS "New \f(CWpackage NAME VERSION syntax"
Subsection "New package NAME VERSION syntax"
This new syntax allows a module author to set the \f(CW$VERSION of a namespace
when the namespace is declared with 'package'. It eliminates the need
for \f(CW\*(C`our $VERSION = ...\*(C' and similar constructs. E.g.

.Vb 2
      package Foo::Bar 1.23;
      # $Foo::Bar::VERSION == 1.23
.Ve

There are several advantages to this:

- \(bu
\f(CW$VERSION is parsed in exactly the same way as \f(CW\*(C`use NAME VERSION\*(C'

- \(bu
\f(CW$VERSION is set at compile time

- \(bu
\f(CW$VERSION is a version object that provides proper overloading of
comparison operators so comparing \f(CW$VERSION to decimal (1.23) or
dotted-decimal (v1.2.3) version numbers works correctly.

- \(bu
Eliminates \f(CW\*(C`$VERSION = ...\*(C' and \f(CW\*(C`eval $VERSION\*(C' clutter

- \(bu
As it requires \s-1VERSION\s0 to be a numeric literal or v-string
literal, it can be statically parsed by toolchain modules
without \f(CW\*(C`eval\*(C' the way \s-1MM-\s0>parse_version does for \f(CW\*(C`$VERSION = ...\*(C'

It does not break old code with only \f(CW\*(C`package NAME\*(C', but code that uses
\f(CW\*(C`package NAME VERSION\*(C' will need to be restricted to perl 5.12.0 or newer
This is analogous to the change to \f(CW\*(C`open\*(C' from two-args to three-args.
Users requiring the latest Perl will benefit, and perhaps after several
years, it will become a standard practice.

However, \f(CW\*(C`package NAME VERSION\*(C' requires a new, 'strict' version
number format. See \*(L"Version number formats\*(R" for details.
.ie n .SS "The ""..."" operator"
.el .SS "The \f(CW... operator"
Subsection "The ... operator"
A new operator, \f(CW\*(C`...\*(C', nicknamed the Yada Yada operator, has been added.
It is intended to mark placeholder code that is not yet implemented.
See \*(L"Yada Yada Operator\*(R" in perlop.

### Implicit strictures

Subsection "Implicit strictures"
Using the \f(CW\*(C`use VERSION\*(C' syntax with a version number greater or equal
to 5.11.0 will lexically enable strictures just like \f(CW\*(C`use strict\*(C'
would do (in addition to enabling features.) The following:

.Vb 1
    use 5.12.0;
.Ve

means:

.Vb 2
    use strict;
    use feature \*(Aq:5.12\*(Aq;
.Ve

### Unicode improvements

Subsection "Unicode improvements"
Perl 5.12 comes with Unicode 5.2, the latest version available to
us at the time of release.  This version of Unicode was released in
October 2009. See <http://www.unicode.org/versions/Unicode5.2.0> for
further details about what's changed in this version of the standard.
See perlunicode for instructions on installing and using other versions
of Unicode.

Additionally, Perl's developers have significantly improved Perl's Unicode
implementation. For full details, see \*(L"Unicode overhaul\*(R" below.

### Y2038 compliance

Subsection "Y2038 compliance"
Perl's core time-related functions are now Y2038 compliant. (It may not mean much to you, but your kids will love it!)

### qr overloading

Subsection "qr overloading"
It is now possible to overload the \f(CW\*(C`qr//\*(C' operator, that is,
conversion to regexp, like it was already possible to overload
conversion to boolean, string or number of objects. It is invoked when
an object appears on the right hand side of the \f(CW\*(C`=~\*(C' operator or when
it is interpolated into a regexp. See overload.

### Pluggable keywords

Subsection "Pluggable keywords"
Extension modules can now cleanly hook into the Perl parser to define
new kinds of keyword-headed expression and compound statement. The
syntax following the keyword is defined entirely by the extension. This
allows a completely non-Perl sublanguage to be parsed inline, with the
correct ops cleanly generated.

See \*(L"PL_keyword_plugin\*(R" in perlapi for the mechanism. The Perl core
source distribution also includes a new module
XS::APItest::KeywordRPN, which implements reverse Polish notation
arithmetic via pluggable keywords. This module is mainly used for test
purposes, and is not normally installed, but also serves as an example
of how to use the new mechanism.

Perl's developers consider this feature to be experimental. We may remove
it or change it in a backwards-incompatible way in Perl 5.14.

### APIs for more internals

Subsection "APIs for more internals"
The lowest layers of the lexer and parts of the pad system now have C
APIs available to \s-1XS\s0 extensions. These are necessary to support proper
use of pluggable keywords, but have other uses too. The new APIs are
experimental, and only cover a small proportion of what would be
necessary to take full advantage of the core's facilities in these
areas. It is intended that the Perl 5.13 development cycle will see the
addition of a full range of clean, supported interfaces.

Perl's developers consider this feature to be experimental. We may remove
it or change it in a backwards-incompatible way in Perl 5.14.

### Overridable function lookup

Subsection "Overridable function lookup"
Where an extension module hooks the creation of rv2cv ops to modify the
subroutine lookup process, this now works correctly for bareword
subroutine calls. This means that prototypes on subroutines referenced
this way will be processed correctly. (Previously bareword subroutine
names were initially looked up, for parsing purposes, by an unhookable
mechanism, so extensions could only properly influence subroutine names
that appeared with an \f(CW\*(C`&\*(C' sigil.)

### A proper interface for pluggable Method Resolution Orders

Subsection "A proper interface for pluggable Method Resolution Orders"
As of Perl 5.12.0 there is a new interface for plugging and using method
resolution orders other than the default linear depth first search.
The C3 method resolution order added in 5.10.0 has been re-implemented as
a plugin, without changing its Perl-space interface. See perlmroapi for
more information.
.ie n .SS """\\N"" experimental regex escape"
.el .SS "\f(CW\\N experimental regex escape"
Subsection "N experimental regex escape"
Perl now supports \f(CW\*(C`\\N\*(C', a new regex escape which you can think of as
the inverse of \f(CW\*(C`\\n\*(C'. It will match any character that is not a newline,
independently from the presence or absence of the single line match
modifier \f(CW\*(C`/s\*(C'. It is not usable within a character class.  \f(CW\*(C`\\N\{3\}\*(C'
means to match 3 non-newlines; \f(CW\*(C`\\N\{5,\}\*(C' means to match at least 5.
\f(CW\*(C`\\N\{NAME\}\*(C' still means the character or sequence named \f(CW\*(C`NAME\*(C', but
\f(CW\*(C`NAME\*(C' no longer can be things like \f(CW3, or \f(CW\*(C`5,\*(C'.

This will break a custom charnames translator which allows numbers for character names, as \f(CW\*(C`\\N\{3\}\*(C' will
now mean to match 3 non-newline characters, and not the character whose
name is \f(CW3. (No name defined by the Unicode standard is a number,
so only custom translators might be affected.)

Perl's developers are somewhat concerned about possible user confusion
with the existing \f(CW\*(C`\\N\{...\}\*(C' construct which matches characters by their
Unicode name. Consequently, this feature is experimental. We may remove
it or change it in a backwards-incompatible way in Perl 5.14.

### DTrace support

Subsection "DTrace support"
Perl now has some support for DTrace. See \*(L"DTrace support\*(R" in *\s-1INSTALL\s0*.
.ie n .SS "Support for ""configure_requires"" in \s-1CPAN\s0 module metadata"
.el .SS "Support for \f(CWconfigure_requires in \s-1CPAN\s0 module metadata"
Subsection "Support for configure_requires in CPAN module metadata"
Both \f(CW\*(C`CPAN\*(C' and \f(CW\*(C`CPANPLUS\*(C' now support the \f(CW\*(C`configure_requires\*(C'
keyword in the *\s-1META\s0.yml* metadata file included in most recent \s-1CPAN\s0
distributions.  This allows distribution authors to specify configuration
prerequisites that must be installed before running *Makefile.PL*
or *Build.PL*.

See the documentation for \f(CW\*(C`ExtUtils::MakeMaker\*(C' or \f(CW\*(C`Module::Build\*(C' for
more on how to specify \f(CW\*(C`configure_requires\*(C' when creating a distribution
for \s-1CPAN.\s0
.ie n .SS """each"", ""keys"", ""values"" are now more flexible"
.el .SS "\f(CWeach, \f(CWkeys, \f(CWvalues are now more flexible"
Subsection "each, keys, values are now more flexible"
The \f(CW\*(C`each\*(C', \f(CW\*(C`keys\*(C', \f(CW\*(C`values\*(C' function can now operate on arrays.
.ie n .SS """when"" as a statement modifier"
.el .SS "\f(CWwhen as a statement modifier"
Subsection "when as a statement modifier"
\f(CW\*(C`when\*(C' is now allowed to be used as a statement modifier.
.ie n .SS "$, flexibility"
.el .SS "\f(CW$, flexibility"
Subsection "$, flexibility"
The variable \f(CW$, may now be tied.

### // in when clauses

Subsection "// in when clauses"
// now behaves like || in when clauses

### Enabling warnings from your shell environment

Subsection "Enabling warnings from your shell environment"
You can now set \f(CW\*(C`-W\*(C' from the \f(CW\*(C`PERL5OPT\*(C' environment variable
.ie n .SS """delete local"""
.el .SS "\f(CWdelete local"
Subsection "delete local"
\f(CW\*(C`delete local\*(C' now allows you to locally delete a hash entry.

### New support for Abstract namespace sockets

Subsection "New support for Abstract namespace sockets"
Abstract namespace sockets are Linux-specific socket type that live in
\s-1AF_UNIX\s0 family, slightly abusing it to be able to use arbitrary
character arrays as addresses: They start with nul byte and are not
terminated by nul byte, but with the length passed to the **socket()**
system call.

### 32-bit limit on substr arguments removed

Subsection "32-bit limit on substr arguments removed"
The 32-bit limit on \f(CW\*(C`substr\*(C' arguments has now been removed. The full
range of the system's signed and unsigned integers is now available for
the \f(CW\*(C`pos\*(C' and \f(CW\*(C`len\*(C' arguments.

## Potentially Incompatible Changes

Header "Potentially Incompatible Changes"

### Deprecations warn by default

Subsection "Deprecations warn by default"
Over the years, Perl's developers have deprecated a number of language
features for a variety of reasons.  Perl now defaults to issuing a
warning if a deprecated language feature is used. Many of the deprecations
Perl now warns you about have been deprecated for many years.  You can
find a list of what was deprecated in a given release of Perl in the
\f(CW\*(C`perl5xxdelta.pod\*(C' file for that release.

To disable this feature in a given lexical scope, you should use \f(CW\*(C`no
warnings \*(Aqdeprecated\*(Aq;\*(C' For information about which language features
are deprecated and explanations of various deprecation warnings, please
see perldiag. See \*(L"Deprecations\*(R" below for the list of features
and modules Perl's developers have deprecated as part of this release.

### Version number formats

Subsection "Version number formats"
Acceptable version number formats have been formalized into \*(L"strict\*(R" and
\*(L"lax\*(R" rules. \f(CW\*(C`package NAME VERSION\*(C' takes a strict version number.
\f(CW\*(C`UNIVERSAL::VERSION\*(C' and the version object constructors take lax
version numbers. Providing an invalid version will result in a fatal
error. The version argument in \f(CW\*(C`use NAME VERSION\*(C' is first parsed as a
numeric literal or v-string and then passed to \f(CW\*(C`UNIVERSAL::VERSION\*(C'
(and must then pass the \*(L"lax\*(R" format test).

These formats are documented fully in the version module. To a first
approximation, a \*(L"strict\*(R" version number is a positive decimal number
(integer or decimal-fraction) without exponentiation or else a
dotted-decimal v-string with a leading 'v' character and at least three
components. A \*(L"lax\*(R" version number allows v-strings with fewer than
three components or without a leading 'v'. Under \*(L"lax\*(R" rules, both
decimal and dotted-decimal versions may have a trailing \*(L"alpha\*(R"
component separated by an underscore character after a fractional or
dotted-decimal component.

The version module adds \f(CW\*(C`version::is_strict\*(C' and \f(CW\*(C`version::is_lax\*(C'
functions to check a scalar against these rules.
.ie n .SS "@INC reorganization"
.el .SS "\f(CW@INC reorganization"
Subsection "@INC reorganization"
In \f(CW@INC, \f(CW\*(C`ARCHLIB\*(C' and \f(CW\*(C`PRIVLIB\*(C' now occur after the current
version's \f(CW\*(C`site_perl\*(C' and \f(CW\*(C`vendor_perl\*(C'.  Modules installed into
\f(CW\*(C`site_perl\*(C' and \f(CW\*(C`vendor_perl\*(C' will now be loaded in preference to
those installed in \f(CW\*(C`ARCHLIB\*(C' and \f(CW\*(C`PRIVLIB\*(C'.

### REGEXPs are now first class

Subsection "REGEXPs are now first class"
Internally, Perl now treats compiled regular expressions (such as
those created with \f(CW\*(C`qr//\*(C') as first class entities. Perl modules which
serialize, deserialize or otherwise have deep interaction with Perl's
internal data structures need to be updated for this change.  Most
affected \s-1CPAN\s0 modules have already been updated as of this writing.

### Switch statement changes

Subsection "Switch statement changes"
The \f(CW\*(C`given\*(C'/\f(CW\*(C`when\*(C' switch statement handles complex statements better
than Perl 5.10.0 did (These enhancements are also available in
5.10.1 and subsequent 5.10 releases.) There are two new cases where
\f(CW\*(C`when\*(C' now interprets its argument as a boolean, instead of an
expression to be used in a smart match:

- flip-flop operators
Item "flip-flop operators"
The \f(CW\*(C`..\*(C' and \f(CW\*(C`...\*(C' flip-flop operators are now evaluated in boolean
context, following their usual semantics; see \*(L"Range Operators\*(R" in perlop.
.Sp
Note that, as in perl 5.10.0, \f(CW\*(C`when (1..10)\*(C' will not work to test
whether a given value is an integer between 1 and 10; you should use
\f(CW\*(C`when ([1..10])\*(C' instead (note the array reference).
.Sp
However, contrary to 5.10.0, evaluating the flip-flop operators in
boolean context ensures it can now be useful in a \f(CW\*(C`when()\*(C', notably
for implementing bistable conditions, like in:
.Sp
.Vb 3
    when (/^=begin/ .. /^=end/) \{
      # do something
    \}
.Ve

- defined-or operator
Item "defined-or operator"
A compound expression involving the defined-or operator, as in
\f(CW\*(C`when (expr1 // expr2)\*(C', will be treated as boolean if the first
expression is boolean. (This just extends the existing rule that applies
to the regular or operator, as in \f(CW\*(C`when (expr1 || expr2)\*(C'.)

### Smart match changes

Subsection "Smart match changes"
Since Perl 5.10.0, Perl's developers have made a number of changes to
the smart match operator. These, of course, also alter the behaviour
of the switch statements where smart matching is implicitly used.
These changes were also made for the 5.10.1 release, and will remain in
subsequent 5.10 releases.

*Changes to type-based dispatch*
Subsection "Changes to type-based dispatch"

The smart match operator \f(CW\*(C`~~\*(C' is no longer commutative. The behaviour of
a smart match now depends primarily on the type of its right hand
argument. Moreover, its semantics have been adjusted for greater
consistency or usefulness in several cases. While the general backwards
compatibility is maintained, several changes must be noted:

- \(bu
Code references with an empty prototype are no longer treated specially.
They are passed an argument like the other code references (even if they
choose to ignore it).

- \(bu
\f(CW\*(C`%hash ~~ sub \{\}\*(C' and \f(CW\*(C`@array ~~ sub \{\}\*(C' now test that the subroutine
returns a true value for each key of the hash (or element of the
array), instead of passing the whole hash or array as a reference to
the subroutine.

- \(bu
Due to the commutativity breakage, code references are no longer
treated specially when appearing on the left of the \f(CW\*(C`~~\*(C' operator,
but like any vulgar scalar.

- \(bu
\f(CW\*(C`undef ~~ %hash\*(C' is always false (since \f(CW\*(C`undef\*(C' can't be a key in a
hash). No implicit conversion to \f(CW"" is done (as was the case in perl
5.10.0).

- \(bu
\f(CW\*(C`$scalar ~~ @array\*(C' now always distributes the smart match across the
elements of the array. It's true if one element in \f(CW@array verifies
\f(CW\*(C`$scalar ~~ $element\*(C'. This is a generalization of the old behaviour
that tested whether the array contained the scalar.

The full dispatch table for the smart match operator is given in
\*(L"Smart matching in detail\*(R" in perlsyn.

*Smart match and overloading*
Subsection "Smart match and overloading"

According to the rule of dispatch based on the rightmost argument type,
when an object overloading \f(CW\*(C`~~\*(C' appears on the right side of the
operator, the overload routine will always be called (with a 3rd argument
set to a true value, see overload.) However, when the object will
appear on the left, the overload routine will be called only when the
rightmost argument is a simple scalar. This way, distributivity of smart
match across arrays is not broken, as well as the other behaviours with
complex types (coderefs, hashes, regexes). Thus, writers of overloading
routines for smart match mostly need to worry only with comparing
against a scalar, and possibly with stringification overloading; the
other common cases will be automatically handled consistently.

\f(CW\*(C`~~\*(C' will now refuse to work on objects that do not overload it (in order
to avoid relying on the object's underlying structure). (However, if the
object overloads the stringification or the numification operators, and
if overload fallback is active, it will be used instead, as usual.)

### Other potentially incompatible changes

Subsection "Other potentially incompatible changes"

- \(bu
The definitions of a number of Unicode properties have changed to match
those of the current Unicode standard. These are listed above under
\*(L"Unicode overhaul\*(R". This change may break code that expects the old
definitions.

- \(bu
The boolkeys op has moved to the group of hash ops. This breaks binary
compatibility.

- \(bu
Filehandles are now always blessed into \f(CW\*(C`IO::File\*(C'.
.Sp
The previous behaviour was to bless Filehandles into FileHandle
(an empty proxy class) if it was loaded into memory and otherwise
to bless them into \f(CW\*(C`IO::Handle\*(C'.

- \(bu
The semantics of \f(CW\*(C`use feature :5.10*\*(C' have changed slightly.
See \*(L"Modules and Pragmata\*(R" for more information.

- \(bu
Perl's developers now use git, rather than Perforce.  This should be
a purely internal change only relevant to people actively working on
the core.  However, you may see minor difference in perl as a consequence
of the change.  For example in some of details of the output of \f(CW\*(C`perl
-V\*(C'. See perlrepository for more information.

- \(bu
As part of the \f(CW\*(C`Test::Harness\*(C' 2.x to 3.x upgrade, the experimental
\f(CW\*(C`Test::Harness::Straps\*(C' module has been removed.
See \*(L"Modules and Pragmata\*(R" for more details.

- \(bu
As part of the \f(CW\*(C`ExtUtils::MakeMaker\*(C' upgrade, the
\f(CW\*(C`ExtUtils::MakeMaker::bytes\*(C' and \f(CW\*(C`ExtUtils::MakeMaker::vmsish\*(C' modules
have been removed from this distribution.

- \(bu
\f(CW\*(C`Module::CoreList\*(C' no longer contains the \f(CW%:patchlevel hash.

- \(bu
\f(CW\*(C`length undef\*(C' now returns undef.

- \(bu
Unsupported private C \s-1API\s0 functions are now declared \*(L"static\*(R" to prevent
leakage to Perl's public \s-1API.\s0

- \(bu
To support the bootstrapping process, *miniperl* no longer builds with
\s-1UTF-8\s0 support in the regexp engine.
.Sp
This allows a build to complete with \s-1PERL_UNICODE\s0 set and a \s-1UTF-8\s0 locale.
Without this there's a bootstrapping problem, as miniperl can't load
the \s-1UTF-8\s0 components of the regexp engine, because they're not yet built.

- \(bu
*miniperl*'s \f(CW@INC is now restricted to just \f(CW\*(C`-I...\*(C', the split of
\f(CW$ENV\{PERL5LIB\}, and "\f(CW\*(C`.\*(C'"

- \(bu
A space or a newline is now required after a \f(CW"#line XXX" directive.

- \(bu
Tied filehandles now have an additional method \s-1EOF\s0 which provides the
\s-1EOF\s0 type.

- \(bu
To better match all other flow control statements, \f(CW\*(C`foreach\*(C' may no
longer be used as an attribute.

- \(bu
Perl's command-line switch \*(L"-P\*(R", which was deprecated in version 5.10.0, has
now been removed. The \s-1CPAN\s0 module \f(CW\*(C`Filter::cpp\*(C' can be used as an
alternative.

## Deprecations

Header "Deprecations"
From time to time, Perl's developers find it necessary to deprecate
features or modules we've previously shipped as part of the core
distribution. We are well aware of the pain and frustration that a
backwards-incompatible change to Perl can cause for developers building
or maintaining software in Perl. You can be sure that when we deprecate
a functionality or syntax, it isn't a choice we make lightly. Sometimes,
we choose to deprecate functionality or syntax because it was found to
be poorly designed or implemented. Sometimes, this is because they're
holding back other features or causing performance problems. Sometimes,
the reasons are more complex. Wherever possible, we try to keep deprecated
functionality available to developers in its previous form for at least
one major release. So long as a deprecated feature isn't actively
disrupting our ability to maintain and extend Perl, we'll try to leave
it in place as long as possible.

The following items are now deprecated:

- suidperl
Item "suidperl"
\f(CW\*(C`suidperl\*(C' is no longer part of Perl. It used to provide a mechanism to
emulate setuid permission bits on systems that don't support it properly.
.ie n .IP "Use of "":="" to mean an empty attribute list" 4
.el .IP "Use of \f(CW:= to mean an empty attribute list" 4
Item "Use of := to mean an empty attribute list"
An accident of Perl's parser meant that these constructions were all
equivalent:
.Sp
.Vb 3
    my $pi := 4;
    my $pi : = 4;
    my $pi :  = 4;
.Ve
.Sp
with the \f(CW\*(C`:\*(C' being treated as the start of an attribute list, which
ends before the \f(CW\*(C`=\*(C'. As whitespace is not significant here, all are
parsed as an empty attribute list, hence all the above are equivalent
to, and better written as
.Sp
.Vb 1
    my $pi = 4;
.Ve
.Sp
because no attribute processing is done for an empty list.
.Sp
As is, this meant that \f(CW\*(C`:=\*(C' cannot be used as a new token, without
silently changing the meaning of existing code. Hence that particular
form is now deprecated, and will become a syntax error. If it is
absolutely necessary to have empty attribute lists (for example,
because of a code generator) then avoid the warning by adding a space
before the \f(CW\*(C`=\*(C'.
.ie n .IP """UNIVERSAL->import()""" 4
.el .IP "\f(CWUNIVERSAL->import()" 4
Item "UNIVERSAL->import()"
The method \f(CW\*(C`UNIVERSAL->import()\*(C' is now deprecated. Attempting to
pass import arguments to a \f(CW\*(C`use UNIVERSAL\*(C' statement will result in a
deprecation warning.
.ie n .IP "Use of ""goto"" to jump into a construct" 4
.el .IP "Use of ``goto'' to jump into a construct" 4
Item "Use of goto to jump into a construct"
Using \f(CW\*(C`goto\*(C' to jump from an outer scope into an inner scope is now
deprecated. This rare use case was causing problems in the
implementation of scopes.

- Custom character names in \\N\{name\} that don't look like names
Item "Custom character names in N\{name\} that don't look like names"
In \f(CW\*(C`\\N\{\f(CIname\f(CW\}\*(C', *name* can be just about anything. The standard
Unicode names have a very limited domain, but a custom name translator
could create names that are, for example, made up entirely of punctuation
symbols. It is now deprecated to make names that don't begin with an
alphabetic character, and aren't alphanumeric or contain other than
a very few other characters, namely spaces, dashes, parentheses
and colons. Because of the added meaning of \f(CW\*(C`\\N\*(C' (See \f(CW\*(C`"\\N"
experimental regex escape\*(C'), names that look like curly brace -enclosed
quantifiers won't work. For example, \f(CW\*(C`\\N\{3,4\}\*(C' now means to match 3 to
4 non-newlines; before a custom name \f(CW\*(C`3,4\*(C' could have been created.

- Deprecated Modules
Item "Deprecated Modules"
The following modules will be removed from the core distribution in a
future release, and should be installed from \s-1CPAN\s0 instead. Distributions
on \s-1CPAN\s0 which require these should add them to their prerequisites. The
core versions of these modules warnings will issue a deprecation warning.
.Sp
If you ship a packaged version of Perl, either alone or as part of a
larger system, then you should carefully consider the repercussions of
core module deprecations. You may want to consider shipping your default
build of Perl with packages for some or all deprecated modules which
install into \f(CW\*(C`vendor\*(C' or \f(CW\*(C`site\*(C' perl library directories. This will
inhibit the deprecation warnings.
.Sp
Alternatively, you may want to consider patching *lib/deprecate.pm*
to provide deprecation warnings specific to your packaging system
or distribution of Perl, consistent with how your packaging system
or distribution manages a staged transition from a release where the
installation of a single package provides the given functionality, to
a later release where the system administrator needs to know to install
multiple packages to get that same functionality.
.Sp
You can silence these deprecation warnings by installing the modules
in question from \s-1CPAN.\s0  To install the latest version of all of them,
just install \f(CW\*(C`Task::Deprecations::5_12\*(C'.

> 
- Class::ISA
Item "Class::ISA"
0

- Pod::Plainer
Item "Pod::Plainer"

- Shell
Item "Shell"

- Switch
Item "Switch"
.PD
Switch is buggy and should be avoided. You may find Perl's new
\f(CW\*(C`given\*(C'/\f(CW\*(C`when\*(C' feature a suitable replacement.  See \*(L"Switch
statements\*(R" in perlsyn for more information.



> 


- Assignment to $[
Item "Assignment to $["
0

- Use of the attribute :locked on subroutines
Item "Use of the attribute :locked on subroutines"
.ie n .IP "Use of ""locked"" with the attributes pragma" 4
.el .IP "Use of ``locked'' with the attributes pragma" 4
Item "Use of locked with the attributes pragma"
.ie n .IP "Use of ""unique"" with the attributes pragma" 4
.el .IP "Use of ``unique'' with the attributes pragma" 4
Item "Use of unique with the attributes pragma"

- Perl_pmflag
Item "Perl_pmflag"
.PD
\f(CW\*(C`Perl_pmflag\*(C' is no longer part of Perl's public \s-1API.\s0 Calling it now
generates a deprecation warning, and it will be removed in a future
release. Although listed as part of the \s-1API,\s0 it was never documented,
and only ever used in *toke.c*, and prior to 5.10, *regcomp.c*. In
core, it has been replaced by a static function.

- Numerous Perl 4-era libraries
Item "Numerous Perl 4-era libraries"
*termcap.pl*, *tainted.pl*, *stat.pl*, *shellwords.pl*, *pwd.pl*,
*open3.pl*, *open2.pl*, *newgetopt.pl*, *look.pl*, *find.pl*,
*finddepth.pl*, *importenv.pl*, *hostname.pl*, *getopts.pl*,
*getopt.pl*, *getcwd.pl*, *flush.pl*, *fastcwd.pl*, *exceptions.pl*,
*ctime.pl*, *complete.pl*, *cacheout.pl*, *bigrat.pl*, *bigint.pl*,
*bigfloat.pl*, *assert.pl*, *abbrev.pl*, *dotsh.pl*, and
*timelocal.pl* are all now deprecated.  Earlier, Perl's developers
intended to remove these libraries from Perl's core for the 5.14.0 release.
.Sp
During final testing before the release of 5.12.0, several developers
discovered current production code using these ancient libraries, some
inside the Perl core itself.  Accordingly, the pumpking granted them
a stay of execution. They will begin to warn about their deprecation
in the 5.14.0 release and will be removed in the 5.16.0 release.

## Unicode overhaul

Header "Unicode overhaul"
Perl's developers have made a concerted effort to update Perl to be in
sync with the latest Unicode standard. Changes for this include:

Perl can now handle every Unicode character property. New documentation,
perluniprops, lists all available non-Unihan character properties. By
default, perl does not expose Unihan, deprecated or Unicode-internal
properties.  See below for more details on these; there is also a section
in the pod listing them, and explaining why they are not exposed.

Perl now fully supports the Unicode compound-style of using \f(CW\*(C`=\*(C'
and \f(CW\*(C`:\*(C' in writing regular expressions: \f(CW\*(C`\\p\{property=value\}\*(C' and
\f(CW\*(C`\\p\{property:value\}\*(C' (both of which mean the same thing).

Perl now fully supports the Unicode loose matching rules for text between
the braces in \f(CW\*(C`\\p\{...\}\*(C' constructs. In addition, Perl allows underscores
between digits of numbers.

Perl now accepts all the Unicode-defined synonyms for properties and
property values.

\f(CW\*(C`qr/\\X/\*(C', which matches a Unicode logical character, has
been expanded to work better with various Asian languages. It
now is defined as an *extended grapheme cluster*. (See
<http://www.unicode.org/reports/tr29/>).  Anything matched previously
and that made sense will continue to be accepted.   Additionally:

- \(bu
\f(CW\*(C`\\X\*(C' will not break apart a \f(CW\*(C`CR\ LF\*(C' sequence.

- \(bu
\f(CW\*(C`\\X\*(C' will now match a sequence which includes the \f(CW\*(C`ZWJ\*(C' and \f(CW\*(C`ZWNJ\*(C'
characters.

- \(bu
\f(CW\*(C`\\X\*(C' will now always match at least one character, including an initial
mark.  Marks generally come after a base character, but it is possible in
Unicode to have them in isolation, and \f(CW\*(C`\\X\*(C' will now handle that case,
for example at the beginning of a line, or after a \f(CW\*(C`ZWSP\*(C'. And this is
the part where \f(CW\*(C`\\X\*(C' doesn't match the things that it used to that don't
make sense. Formerly, for example, you could have the nonsensical case
of an accented \s-1LF.\s0

- \(bu
\f(CW\*(C`\\X\*(C' will now match a (Korean) Hangul syllable sequence, and the Thai
and Lao exception cases.

Otherwise, this change should be transparent for the non-affected
languages.

\f(CW\*(C`\\p\{...\}\*(C' matches using the Canonical_Combining_Class property were
completely broken in previous releases of Perl.  They should now work
correctly.

Before Perl 5.12, the Unicode \f(CW\*(C`Decomposition_Type=Compat\*(C' property
and a Perl extension had the same name, which led to neither matching
all the correct values (with more than 100 mistakes in one, and several
thousand in the other). The Perl extension has now been renamed to be
\f(CW\*(C`Decomposition_Type=Noncanonical\*(C' (short: \f(CW\*(C`dt=noncanon\*(C'). It has the
same meaning as was previously intended, namely the union of all the
non-canonical Decomposition types, with Unicode \f(CW\*(C`Compat\*(C' being just
one of those.

\f(CW\*(C`\\p\{Decomposition_Type=Canonical\}\*(C' now includes the Hangul syllables.

\f(CW\*(C`\\p\{Uppercase\}\*(C' and \f(CW\*(C`\\p\{Lowercase\}\*(C' now work as the Unicode standard
says they should.  This means they each match a few more characters than
they used to.

\f(CW\*(C`\\p\{Cntrl\}\*(C' now matches the same characters as \f(CW\*(C`\\p\{Control\}\*(C'. This
means it no longer will match Private Use (gc=co), Surrogates (gc=cs),
nor Format (gc=cf) code points. The Format code points represent the
biggest possible problem. All but 36 of them are either officially
deprecated or strongly discouraged from being used. Of those 36, likely
the most widely used are the soft hyphen (U+00AD), and \s-1BOM, ZWSP, ZWNJ,
WJ,\s0 and similar characters, plus bidirectional controls.

\f(CW\*(C`\\p\{Alpha\}\*(C' now matches the same characters as \f(CW\*(C`\\p\{Alphabetic\}\*(C'. Before
5.12, Perl's definition included a number of things that aren't
really alpha (all marks) while omitting many that were. The definitions
of \f(CW\*(C`\\p\{Alnum\}\*(C' and \f(CW\*(C`\\p\{Word\}\*(C' depend on Alpha's definition and have
changed accordingly.

\f(CW\*(C`\\p\{Word\}\*(C' no longer incorrectly matches non-word characters such
as fractions.

\f(CW\*(C`\\p\{Print\}\*(C' no longer matches the line control characters: Tab, \s-1LF,
CR, FF, VT,\s0 and \s-1NEL.\s0 This brings it in line with standards and the
documentation.

\f(CW\*(C`\\p\{XDigit\}\*(C' now matches the same characters as \f(CW\*(C`\\p\{Hex_Digit\}\*(C'. This
means that in addition to the characters it currently matches,
\f(CW\*(C`[A-Fa-f0-9]\*(C', it will also match the 22 fullwidth equivalents, for
example U+FF10: \s-1FULLWIDTH DIGIT ZERO.\s0

The Numeric type property has been extended to include the Unihan
characters.

There is a new Perl extension, the 'Present_In', or simply 'In',
property. This is an extension of the Unicode Age property, but
\f(CW\*(C`\\p\{In=5.0\}\*(C' matches any code point whose usage has been determined
*as of* Unicode version 5.0. The \f(CW\*(C`\\p\{Age=5.0\}\*(C' only matches code points
added in *precisely* version 5.0.

A number of properties now have the correct values for unassigned
code points. The affected properties are Bidi_Class, East_Asian_Width,
Joining_Type, Decomposition_Type, Hangul_Syllable_Type, Numeric_Type,
and Line_Break.

The Default_Ignorable_Code_Point, ID_Continue, and ID_Start properties
are now up to date with current Unicode definitions.

Earlier versions of Perl erroneously exposed certain properties that
are supposed to be Unicode internal-only.  Use of these in regular
expressions will now generate, if enabled, a deprecation warning message.
The properties are: Other_Alphabetic, Other_Default_Ignorable_Code_Point,
Other_Grapheme_Extend, Other_ID_Continue, Other_ID_Start, Other_Lowercase,
Other_Math, and Other_Uppercase.

It is now possible to change which Unicode properties Perl understands
on a per-installation basis. As mentioned above, certain properties
are turned off by default.  These include all the Unihan properties
(which should be accessible via the \s-1CPAN\s0 module Unicode::Unihan) and any
deprecated or Unicode internal-only property that Perl has never exposed.

The generated files in the \f(CW\*(C`lib/unicore/To\*(C' directory are now more
clearly marked as being stable, directly usable by applications.  New hash
entries in them give the format of the normal entries, which allows for
easier machine parsing. Perl can generate files in this directory for
any property, though most are suppressed.  You can find instructions
for changing which are written in perluniprops.

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules and Pragmata

Subsection "New Modules and Pragmata"
.ie n .IP """autodie""" 4
.el .IP "\f(CWautodie" 4
Item "autodie"
\f(CW\*(C`autodie\*(C' is a new lexically-scoped alternative for the \f(CW\*(C`Fatal\*(C' module.
The bundled version is 2.06_01. Note that in this release, using a string
eval when \f(CW\*(C`autodie\*(C' is in effect can cause the autodie behaviour to leak
into the surrounding scope. See \*(L"\s-1BUGS\*(R"\s0 in autodie for more details.
.Sp
Version 2.06_01 has been added to the Perl core.
.ie n .IP """Compress::Raw::Bzip2""" 4
.el .IP "\f(CWCompress::Raw::Bzip2" 4
Item "Compress::Raw::Bzip2"
Version 2.024 has been added to the Perl core.
.ie n .IP """overloading""" 4
.el .IP "\f(CWoverloading" 4
Item "overloading"
\f(CW\*(C`overloading\*(C' allows you to lexically disable or enable overloading
for some or all operations.
.Sp
Version 0.001 has been added to the Perl core.
.ie n .IP """parent""" 4
.el .IP "\f(CWparent" 4
Item "parent"
\f(CW\*(C`parent\*(C' establishes an \s-1ISA\s0 relationship with base classes at compile
time. It provides the key feature of \f(CW\*(C`base\*(C' without further unwanted
behaviors.
.Sp
Version 0.223 has been added to the Perl core.
.ie n .IP """Parse::CPAN::Meta""" 4
.el .IP "\f(CWParse::CPAN::Meta" 4
Item "Parse::CPAN::Meta"
Version 1.40 has been added to the Perl core.
.ie n .IP """VMS::DCLsym""" 4
.el .IP "\f(CWVMS::DCLsym" 4
Item "VMS::DCLsym"
Version 1.03 has been added to the Perl core.
.ie n .IP """VMS::Stdio""" 4
.el .IP "\f(CWVMS::Stdio" 4
Item "VMS::Stdio"
Version 2.4 has been added to the Perl core.
.ie n .IP """XS::APItest::KeywordRPN""" 4
.el .IP "\f(CWXS::APItest::KeywordRPN" 4
Item "XS::APItest::KeywordRPN"
Version 0.003 has been added to the Perl core.

### Updated Pragmata

Subsection "Updated Pragmata"
.ie n .IP """base""" 4
.el .IP "\f(CWbase" 4
Item "base"
Upgraded from version 2.13 to 2.15.
.ie n .IP """bignum""" 4
.el .IP "\f(CWbignum" 4
Item "bignum"
Upgraded from version 0.22 to 0.23.
.ie n .IP """charnames""" 4
.el .IP "\f(CWcharnames" 4
Item "charnames"
\f(CW\*(C`charnames\*(C' now contains the Unicode *NameAliases.txt* database file.
This has the effect of adding some extra \f(CW\*(C`\\N\*(C' character names that
formerly wouldn't have been recognised; for example, \f(CW"\\N\{LATIN CAPITAL
LETTER GHA\}".
.Sp
Upgraded from version 1.06 to 1.07.
.ie n .IP """constant""" 4
.el .IP "\f(CWconstant" 4
Item "constant"
Upgraded from version 1.13 to 1.20.
.ie n .IP """diagnostics""" 4
.el .IP "\f(CWdiagnostics" 4
Item "diagnostics"
\f(CW\*(C`diagnostics\*(C' now supports %.0f formatting internally.
.Sp
\f(CW\*(C`diagnostics\*(C' no longer suppresses \f(CW\*(C`Use of uninitialized value in range
(or flip)\*(C' warnings. [perl #71204]
.Sp
Upgraded from version 1.17 to 1.19.
.ie n .IP """feature""" 4
.el .IP "\f(CWfeature" 4
Item "feature"
In \f(CW\*(C`feature\*(C', the meaning of the \f(CW\*(C`:5.10\*(C' and \f(CW\*(C`:5.10.X\*(C' feature
bundles has changed slightly. The last component, if any (i.e. \f(CW\*(C`X\*(C') is
simply ignored.  This is predicated on the assumption that new features
will not, in general, be added to maintenance releases. So \f(CW\*(C`:5.10\*(C'
and \f(CW\*(C`:5.10.X\*(C' have identical effect. This is a change to the behaviour
documented for 5.10.0.
.Sp
\f(CW\*(C`feature\*(C' now includes the \f(CW\*(C`unicode_strings\*(C' feature:
.Sp
.Vb 1
    use feature "unicode_strings";
.Ve
.Sp
This pragma turns on Unicode semantics for the case-changing operations
(\f(CW\*(C`uc\*(C', \f(CW\*(C`lc\*(C', \f(CW\*(C`ucfirst\*(C', \f(CW\*(C`lcfirst\*(C') on strings that don't have the
internal \s-1UTF-8\s0 flag set, but that contain single-byte characters between
128 and 255.
.Sp
Upgraded from version 1.11 to 1.16.
.ie n .IP """less""" 4
.el .IP "\f(CWless" 4
Item "less"
\f(CW\*(C`less\*(C' now includes the \f(CW\*(C`stash_name\*(C' method to allow subclasses of
\f(CW\*(C`less\*(C' to pick where in %^H to store their stash.
.Sp
Upgraded from version 0.02 to 0.03.
.ie n .IP """lib""" 4
.el .IP "\f(CWlib" 4
Item "lib"
Upgraded from version 0.5565 to 0.62.
.ie n .IP """mro""" 4
.el .IP "\f(CWmro" 4
Item "mro"
\f(CW\*(C`mro\*(C' is now implemented as an \s-1XS\s0 extension. The documented interface has
not changed. Code relying on the implementation detail that some \f(CW\*(C`mro::\*(C'
methods happened to be available at all times gets to \*(L"keep both pieces\*(R".
.Sp
Upgraded from version 1.00 to 1.02.
.ie n .IP """overload""" 4
.el .IP "\f(CWoverload" 4
Item "overload"
\f(CW\*(C`overload\*(C' now allow overloading of 'qr'.
.Sp
Upgraded from version 1.06 to 1.10.
.ie n .IP """threads""" 4
.el .IP "\f(CWthreads" 4
Item "threads"
Upgraded from version 1.67 to 1.75.
.ie n .IP """threads::shared""" 4
.el .IP "\f(CWthreads::shared" 4
Item "threads::shared"
Upgraded from version 1.14 to 1.32.
.ie n .IP """version""" 4
.el .IP "\f(CWversion" 4
Item "version"
\f(CW\*(C`version\*(C' now has support for \*(L"Version number formats\*(R" as described
earlier in this document and in its own documentation.
.Sp
Upgraded from version 0.74 to 0.82.
.ie n .IP """warnings""" 4
.el .IP "\f(CWwarnings" 4
Item "warnings"
\f(CW\*(C`warnings\*(C' has a new \f(CW\*(C`warnings::fatal_enabled()\*(C' function.  It also
includes a new \f(CW\*(C`illegalproto\*(C' warning category. See also \*(L"New or
Changed Diagnostics\*(R" for this change.
.Sp
Upgraded from version 1.06 to 1.09.

### Updated Modules

Subsection "Updated Modules"
.ie n .IP """Archive::Extract""" 4
.el .IP "\f(CWArchive::Extract" 4
Item "Archive::Extract"
Upgraded from version 0.24 to 0.38.
.ie n .IP """Archive::Tar""" 4
.el .IP "\f(CWArchive::Tar" 4
Item "Archive::Tar"
Upgraded from version 1.38 to 1.54.
.ie n .IP """Attribute::Handlers""" 4
.el .IP "\f(CWAttribute::Handlers" 4
Item "Attribute::Handlers"
Upgraded from version 0.79 to 0.87.
.ie n .IP """AutoLoader""" 4
.el .IP "\f(CWAutoLoader" 4
Item "AutoLoader"
Upgraded from version 5.63 to 5.70.
.ie n .IP """B::Concise""" 4
.el .IP "\f(CWB::Concise" 4
Item "B::Concise"
Upgraded from version 0.74 to 0.78.
.ie n .IP """B::Debug""" 4
.el .IP "\f(CWB::Debug" 4
Item "B::Debug"
Upgraded from version 1.05 to 1.12.
.ie n .IP """B::Deparse""" 4
.el .IP "\f(CWB::Deparse" 4
Item "B::Deparse"
Upgraded from version 0.83 to 0.96.
.ie n .IP """B::Lint""" 4
.el .IP "\f(CWB::Lint" 4
Item "B::Lint"
Upgraded from version 1.09 to 1.11_01.
.ie n .IP """CGI""" 4
.el .IP "\f(CWCGI" 4
Item "CGI"
Upgraded from version 3.29 to 3.48.
.ie n .IP """Class::ISA""" 4
.el .IP "\f(CWClass::ISA" 4
Item "Class::ISA"
Upgraded from version 0.33 to 0.36.
.Sp
\s-1NOTE:\s0 \f(CW\*(C`Class::ISA\*(C' is deprecated and may be removed from a future
version of Perl.
.ie n .IP """Compress::Raw::Zlib""" 4
.el .IP "\f(CWCompress::Raw::Zlib" 4
Item "Compress::Raw::Zlib"
Upgraded from version 2.008 to 2.024.
.ie n .IP """CPAN""" 4
.el .IP "\f(CWCPAN" 4
Item "CPAN"
Upgraded from version 1.9205 to 1.94_56.
.ie n .IP """CPANPLUS""" 4
.el .IP "\f(CWCPANPLUS" 4
Item "CPANPLUS"
Upgraded from version 0.84 to 0.90.
.ie n .IP """CPANPLUS::Dist::Build""" 4
.el .IP "\f(CWCPANPLUS::Dist::Build" 4
Item "CPANPLUS::Dist::Build"
Upgraded from version 0.06_02 to 0.46.
.ie n .IP """Data::Dumper""" 4
.el .IP "\f(CWData::Dumper" 4
Item "Data::Dumper"
Upgraded from version 2.121_14 to 2.125.
.ie n .IP """DB_File""" 4
.el .IP "\f(CWDB_File" 4
Item "DB_File"
Upgraded from version 1.816_1 to 1.820.
.ie n .IP """Devel::PPPort""" 4
.el .IP "\f(CWDevel::PPPort" 4
Item "Devel::PPPort"
Upgraded from version 3.13 to 3.19.
.ie n .IP """Digest""" 4
.el .IP "\f(CWDigest" 4
Item "Digest"
Upgraded from version 1.15 to 1.16.
.ie n .IP """Digest::MD5""" 4
.el .IP "\f(CWDigest::MD5" 4
Item "Digest::MD5"
Upgraded from version 2.36_01 to 2.39.
.ie n .IP """Digest::SHA""" 4
.el .IP "\f(CWDigest::SHA" 4
Item "Digest::SHA"
Upgraded from version 5.45 to 5.47.
.ie n .IP """Encode""" 4
.el .IP "\f(CWEncode" 4
Item "Encode"
Upgraded from version 2.23 to 2.39.
.ie n .IP """Exporter""" 4
.el .IP "\f(CWExporter" 4
Item "Exporter"
Upgraded from version 5.62 to 5.64_01.
.ie n .IP """ExtUtils::CBuilder""" 4
.el .IP "\f(CWExtUtils::CBuilder" 4
Item "ExtUtils::CBuilder"
Upgraded from version 0.21 to 0.27.
.ie n .IP """ExtUtils::Command""" 4
.el .IP "\f(CWExtUtils::Command" 4
Item "ExtUtils::Command"
Upgraded from version 1.13 to 1.16.
.ie n .IP """ExtUtils::Constant""" 4
.el .IP "\f(CWExtUtils::Constant" 4
Item "ExtUtils::Constant"
Upgraded from version 0.2 to 0.22.
.ie n .IP """ExtUtils::Install""" 4
.el .IP "\f(CWExtUtils::Install" 4
Item "ExtUtils::Install"
Upgraded from version 1.44 to 1.55.
.ie n .IP """ExtUtils::MakeMaker""" 4
.el .IP "\f(CWExtUtils::MakeMaker" 4
Item "ExtUtils::MakeMaker"
Upgraded from version 6.42 to 6.56.
.ie n .IP """ExtUtils::Manifest""" 4
.el .IP "\f(CWExtUtils::Manifest" 4
Item "ExtUtils::Manifest"
Upgraded from version 1.51_01 to 1.57.
.ie n .IP """ExtUtils::ParseXS""" 4
.el .IP "\f(CWExtUtils::ParseXS" 4
Item "ExtUtils::ParseXS"
Upgraded from version 2.18_02 to 2.21.
.ie n .IP """File::Fetch""" 4
.el .IP "\f(CWFile::Fetch" 4
Item "File::Fetch"
Upgraded from version 0.14 to 0.24.
.ie n .IP """File::Path""" 4
.el .IP "\f(CWFile::Path" 4
Item "File::Path"
Upgraded from version 2.04 to 2.08_01.
.ie n .IP """File::Temp""" 4
.el .IP "\f(CWFile::Temp" 4
Item "File::Temp"
Upgraded from version 0.18 to 0.22.
.ie n .IP """Filter::Simple""" 4
.el .IP "\f(CWFilter::Simple" 4
Item "Filter::Simple"
Upgraded from version 0.82 to 0.84.
.ie n .IP """Filter::Util::Call""" 4
.el .IP "\f(CWFilter::Util::Call" 4
Item "Filter::Util::Call"
Upgraded from version 1.07 to 1.08.
.ie n .IP """Getopt::Long""" 4
.el .IP "\f(CWGetopt::Long" 4
Item "Getopt::Long"
Upgraded from version 2.37 to 2.38.
.ie n .IP """IO""" 4
.el .IP "\f(CWIO" 4
Item "IO"
Upgraded from version 1.23_01 to 1.25_02.
.ie n .IP """IO::Zlib""" 4
.el .IP "\f(CWIO::Zlib" 4
Item "IO::Zlib"
Upgraded from version 1.07 to 1.10.
.ie n .IP """IPC::Cmd""" 4
.el .IP "\f(CWIPC::Cmd" 4
Item "IPC::Cmd"
Upgraded from version 0.40_1 to 0.54.
.ie n .IP """IPC::SysV""" 4
.el .IP "\f(CWIPC::SysV" 4
Item "IPC::SysV"
Upgraded from version 1.05 to 2.01.
.ie n .IP """Locale::Maketext""" 4
.el .IP "\f(CWLocale::Maketext" 4
Item "Locale::Maketext"
Upgraded from version 1.12 to 1.14.
.ie n .IP """Locale::Maketext::Simple""" 4
.el .IP "\f(CWLocale::Maketext::Simple" 4
Item "Locale::Maketext::Simple"
Upgraded from version 0.18 to 0.21.
.ie n .IP """Log::Message""" 4
.el .IP "\f(CWLog::Message" 4
Item "Log::Message"
Upgraded from version 0.01 to 0.02.
.ie n .IP """Log::Message::Simple""" 4
.el .IP "\f(CWLog::Message::Simple" 4
Item "Log::Message::Simple"
Upgraded from version 0.04 to 0.06.
.ie n .IP """Math::BigInt""" 4
.el .IP "\f(CWMath::BigInt" 4
Item "Math::BigInt"
Upgraded from version 1.88 to 1.89_01.
.ie n .IP """Math::BigInt::FastCalc""" 4
.el .IP "\f(CWMath::BigInt::FastCalc" 4
Item "Math::BigInt::FastCalc"
Upgraded from version 0.16 to 0.19.
.ie n .IP """Math::BigRat""" 4
.el .IP "\f(CWMath::BigRat" 4
Item "Math::BigRat"
Upgraded from version 0.21 to 0.24.
.ie n .IP """Math::Complex""" 4
.el .IP "\f(CWMath::Complex" 4
Item "Math::Complex"
Upgraded from version 1.37 to 1.56.
.ie n .IP """Memoize""" 4
.el .IP "\f(CWMemoize" 4
Item "Memoize"
Upgraded from version 1.01_02 to 1.01_03.
.ie n .IP """MIME::Base64""" 4
.el .IP "\f(CWMIME::Base64" 4
Item "MIME::Base64"
Upgraded from version 3.07_01 to 3.08.
.ie n .IP """Module::Build""" 4
.el .IP "\f(CWModule::Build" 4
Item "Module::Build"
Upgraded from version 0.2808_01 to 0.3603.
.ie n .IP """Module::CoreList""" 4
.el .IP "\f(CWModule::CoreList" 4
Item "Module::CoreList"
Upgraded from version 2.12 to 2.29.
.ie n .IP """Module::Load""" 4
.el .IP "\f(CWModule::Load" 4
Item "Module::Load"
Upgraded from version 0.12 to 0.16.
.ie n .IP """Module::Load::Conditional""" 4
.el .IP "\f(CWModule::Load::Conditional" 4
Item "Module::Load::Conditional"
Upgraded from version 0.22 to 0.34.
.ie n .IP """Module::Loaded""" 4
.el .IP "\f(CWModule::Loaded" 4
Item "Module::Loaded"
Upgraded from version 0.01 to 0.06.
.ie n .IP """Module::Pluggable""" 4
.el .IP "\f(CWModule::Pluggable" 4
Item "Module::Pluggable"
Upgraded from version 3.6 to 3.9.
.ie n .IP """Net::Ping""" 4
.el .IP "\f(CWNet::Ping" 4
Item "Net::Ping"
Upgraded from version 2.33 to 2.36.
.ie n .IP """NEXT""" 4
.el .IP "\f(CWNEXT" 4
Item "NEXT"
Upgraded from version 0.60_01 to 0.64.
.ie n .IP """Object::Accessor""" 4
.el .IP "\f(CWObject::Accessor" 4
Item "Object::Accessor"
Upgraded from version 0.32 to 0.36.
.ie n .IP """Package::Constants""" 4
.el .IP "\f(CWPackage::Constants" 4
Item "Package::Constants"
Upgraded from version 0.01 to 0.02.
.ie n .IP """PerlIO""" 4
.el .IP "\f(CWPerlIO" 4
Item "PerlIO"
Upgraded from version 1.04 to 1.06.
.ie n .IP """Pod::Parser""" 4
.el .IP "\f(CWPod::Parser" 4
Item "Pod::Parser"
Upgraded from version 1.35 to 1.37.
.ie n .IP """Pod::Perldoc""" 4
.el .IP "\f(CWPod::Perldoc" 4
Item "Pod::Perldoc"
Upgraded from version 3.14_02 to 3.15_02.
.ie n .IP """Pod::Plainer""" 4
.el .IP "\f(CWPod::Plainer" 4
Item "Pod::Plainer"
Upgraded from version 0.01 to 1.02.
.Sp
\s-1NOTE:\s0 \f(CW\*(C`Pod::Plainer\*(C' is deprecated and may be removed from a future
version of Perl.
.ie n .IP """Pod::Simple""" 4
.el .IP "\f(CWPod::Simple" 4
Item "Pod::Simple"
Upgraded from version 3.05 to 3.13.
.ie n .IP """Safe""" 4
.el .IP "\f(CWSafe" 4
Item "Safe"
Upgraded from version 2.12 to 2.22.
.ie n .IP """SelfLoader""" 4
.el .IP "\f(CWSelfLoader" 4
Item "SelfLoader"
Upgraded from version 1.11 to 1.17.
.ie n .IP """Storable""" 4
.el .IP "\f(CWStorable" 4
Item "Storable"
Upgraded from version 2.18 to 2.22.
.ie n .IP """Switch""" 4
.el .IP "\f(CWSwitch" 4
Item "Switch"
Upgraded from version 2.13 to 2.16.
.Sp
\s-1NOTE:\s0 \f(CW\*(C`Switch\*(C' is deprecated and may be removed from a future version
of Perl.
.ie n .IP """Sys::Syslog""" 4
.el .IP "\f(CWSys::Syslog" 4
Item "Sys::Syslog"
Upgraded from version 0.22 to 0.27.
.ie n .IP """Term::ANSIColor""" 4
.el .IP "\f(CWTerm::ANSIColor" 4
Item "Term::ANSIColor"
Upgraded from version 1.12 to 2.02.
.ie n .IP """Term::UI""" 4
.el .IP "\f(CWTerm::UI" 4
Item "Term::UI"
Upgraded from version 0.18 to 0.20.
.ie n .IP """Test""" 4
.el .IP "\f(CWTest" 4
Item "Test"
Upgraded from version 1.25 to 1.25_02.
.ie n .IP """Test::Harness""" 4
.el .IP "\f(CWTest::Harness" 4
Item "Test::Harness"
Upgraded from version 2.64 to 3.17.
.ie n .IP """Test::Simple""" 4
.el .IP "\f(CWTest::Simple" 4
Item "Test::Simple"
Upgraded from version 0.72 to 0.94.
.ie n .IP """Text::Balanced""" 4
.el .IP "\f(CWText::Balanced" 4
Item "Text::Balanced"
Upgraded from version 2.0.0 to 2.02.
.ie n .IP """Text::ParseWords""" 4
.el .IP "\f(CWText::ParseWords" 4
Item "Text::ParseWords"
Upgraded from version 3.26 to 3.27.
.ie n .IP """Text::Soundex""" 4
.el .IP "\f(CWText::Soundex" 4
Item "Text::Soundex"
Upgraded from version 3.03 to 3.03_01.
.ie n .IP """Thread::Queue""" 4
.el .IP "\f(CWThread::Queue" 4
Item "Thread::Queue"
Upgraded from version 2.00 to 2.11.
.ie n .IP """Thread::Semaphore""" 4
.el .IP "\f(CWThread::Semaphore" 4
Item "Thread::Semaphore"
Upgraded from version 2.01 to 2.09.
.ie n .IP """Tie::RefHash""" 4
.el .IP "\f(CWTie::RefHash" 4
Item "Tie::RefHash"
Upgraded from version 1.37 to 1.38.
.ie n .IP """Time::HiRes""" 4
.el .IP "\f(CWTime::HiRes" 4
Item "Time::HiRes"
Upgraded from version 1.9711 to 1.9719.
.ie n .IP """Time::Local""" 4
.el .IP "\f(CWTime::Local" 4
Item "Time::Local"
Upgraded from version 1.18 to 1.1901_01.
.ie n .IP """Time::Piece""" 4
.el .IP "\f(CWTime::Piece" 4
Item "Time::Piece"
Upgraded from version 1.12 to 1.15.
.ie n .IP """Unicode::Collate""" 4
.el .IP "\f(CWUnicode::Collate" 4
Item "Unicode::Collate"
Upgraded from version 0.52 to 0.52_01.
.ie n .IP """Unicode::Normalize""" 4
.el .IP "\f(CWUnicode::Normalize" 4
Item "Unicode::Normalize"
Upgraded from version 1.02 to 1.03.
.ie n .IP """Win32""" 4
.el .IP "\f(CWWin32" 4
Item "Win32"
Upgraded from version 0.34 to 0.39.
.ie n .IP """Win32API::File""" 4
.el .IP "\f(CWWin32API::File" 4
Item "Win32API::File"
Upgraded from version 0.1001_01 to 0.1101.
.ie n .IP """XSLoader""" 4
.el .IP "\f(CWXSLoader" 4
Item "XSLoader"
Upgraded from version 0.08 to 0.10.

### Removed Modules and Pragmata

Subsection "Removed Modules and Pragmata"
.ie n .IP """attrs""" 4
.el .IP "\f(CWattrs" 4
Item "attrs"
Removed from the Perl core.  Prior version was 1.02.
.ie n .IP """CPAN::API::HOWTO""" 4
.el .IP "\f(CWCPAN::API::HOWTO" 4
Item "CPAN::API::HOWTO"
Removed from the Perl core.  Prior version was 'undef'.
.ie n .IP """CPAN::DeferedCode""" 4
.el .IP "\f(CWCPAN::DeferedCode" 4
Item "CPAN::DeferedCode"
Removed from the Perl core.  Prior version was 5.50.
.ie n .IP """CPANPLUS::inc""" 4
.el .IP "\f(CWCPANPLUS::inc" 4
Item "CPANPLUS::inc"
Removed from the Perl core.  Prior version was 'undef'.
.ie n .IP """DCLsym""" 4
.el .IP "\f(CWDCLsym" 4
Item "DCLsym"
Removed from the Perl core.  Prior version was 1.03.
.ie n .IP """ExtUtils::MakeMaker::bytes""" 4
.el .IP "\f(CWExtUtils::MakeMaker::bytes" 4
Item "ExtUtils::MakeMaker::bytes"
Removed from the Perl core.  Prior version was 6.42.
.ie n .IP """ExtUtils::MakeMaker::vmsish""" 4
.el .IP "\f(CWExtUtils::MakeMaker::vmsish" 4
Item "ExtUtils::MakeMaker::vmsish"
Removed from the Perl core.  Prior version was 6.42.
.ie n .IP """Stdio""" 4
.el .IP "\f(CWStdio" 4
Item "Stdio"
Removed from the Perl core.  Prior version was 2.3.
.ie n .IP """Test::Harness::Assert""" 4
.el .IP "\f(CWTest::Harness::Assert" 4
Item "Test::Harness::Assert"
Removed from the Perl core.  Prior version was 0.02.
.ie n .IP """Test::Harness::Iterator""" 4
.el .IP "\f(CWTest::Harness::Iterator" 4
Item "Test::Harness::Iterator"
Removed from the Perl core.  Prior version was 0.02.
.ie n .IP """Test::Harness::Point""" 4
.el .IP "\f(CWTest::Harness::Point" 4
Item "Test::Harness::Point"
Removed from the Perl core.  Prior version was 0.01.
.ie n .IP """Test::Harness::Results""" 4
.el .IP "\f(CWTest::Harness::Results" 4
Item "Test::Harness::Results"
Removed from the Perl core.  Prior version was 0.01.
.ie n .IP """Test::Harness::Straps""" 4
.el .IP "\f(CWTest::Harness::Straps" 4
Item "Test::Harness::Straps"
Removed from the Perl core.  Prior version was 0.26_01.
.ie n .IP """Test::Harness::Util""" 4
.el .IP "\f(CWTest::Harness::Util" 4
Item "Test::Harness::Util"
Removed from the Perl core.  Prior version was 0.01.
.ie n .IP """XSSymSet""" 4
.el .IP "\f(CWXSSymSet" 4
Item "XSSymSet"
Removed from the Perl core.  Prior version was 1.1.

### Deprecated Modules and Pragmata

Subsection "Deprecated Modules and Pragmata"
See \*(L"Deprecated Modules\*(R" above.

## Documentation

Header "Documentation"

### New Documentation

Subsection "New Documentation"

- \(bu
perlhaiku contains instructions on how to build perl for the Haiku
platform.

- \(bu
perlmroapi describes the new interface for pluggable Method Resolution
Orders.

- \(bu
perlperf, by Richard Foley, provides an introduction to the use of
performance and optimization techniques which can be used with particular
reference to perl programs.

- \(bu
perlrepository describes how to access the perl source using the *git*
version control system.

- \(bu
perlpolicy extends the \*(L"Social contract about contributed modules\*(R" into
the beginnings of a document on Perl porting policies.

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"

- \(bu
The various large *Changes** files (which listed every change made
to perl over the last 18 years) have been removed, and replaced by a
small file, also called *Changes*, which just explains how that same
information may be extracted from the git version control system.

- \(bu
*Porting/patching.pod* has been deleted, as it mainly described
interacting with the old Perforce-based repository, which is now obsolete.
Information still relevant has been moved to perlrepository.

- \(bu
The syntax \f(CW\*(C`unless (EXPR) BLOCK else BLOCK\*(C' is now documented as valid,
as is the syntax \f(CW\*(C`unless (EXPR) BLOCK elsif (EXPR) BLOCK ... else
BLOCK\*(C', although actually using the latter may not be the best idea for
the readability of your source code.

- \(bu
Documented -X overloading.

- \(bu
Documented that \f(CW\*(C`when()\*(C' treats specially most of the filetest operators

- \(bu
Documented \f(CW\*(C`when\*(C' as a syntax modifier.

- \(bu
Eliminated \*(L"Old Perl threads tutorial\*(R", which described 5005 threads.
.Sp
*pod/perlthrtut.pod* is the same material reworked for ithreads.

- \(bu
Correct previous documentation: v-strings are not deprecated
.Sp
With version objects, we need them to use \s-1MODULE VERSION\s0 syntax. This
patch removes the deprecation notice.

- \(bu
Security contact information is now part of perlsec.

- \(bu
A significant fraction of the core documentation has been updated to
clarify the behavior of Perl's Unicode handling.
.Sp
Much of the remaining core documentation has been reviewed and edited
for clarity, consistent use of language, and to fix the spelling of Tom
Christiansen's name.

- \(bu
The Pod specification (perlpodspec) has been updated to bring the
specification in line with modern usage already supported by most Pod
systems. A parameter string may now follow the format name in a
\*(L"begin/end\*(R" region. Links to URIs with a text description are now
allowed. The usage of \f(CW\*(C`L<"section">\*(C' has been marked as
deprecated.

- \(bu
if.pm has been documented in \*(L"use\*(R" in perlfunc as a means to get
conditional loading of modules despite the implicit \s-1BEGIN\s0 block around
\f(CW\*(C`use\*(C'.

- \(bu
The documentation for \f(CW$1 in perlvar.pod has been clarified.

- \(bu
\f(CW\*(C`\\N\{U+\f(CIcode point\f(CW\}\*(C' is now documented.

## Selected Performance Enhancements

Header "Selected Performance Enhancements"

- \(bu
A new internal cache means that \f(CW\*(C`isa()\*(C' will often be faster.

- \(bu
The implementation of \f(CW\*(C`C3\*(C' Method Resolution Order has been
optimised - linearisation for classes with single inheritance is 40%
faster. Performance for multiple inheritance is unchanged.

- \(bu
Under \f(CW\*(C`use locale\*(C', the locale-relevant information is now cached on
read-only values, such as the list returned by \f(CW\*(C`keys %hash\*(C'. This makes
operations such as \f(CW\*(C`sort keys %hash\*(C' in the scope of \f(CW\*(C`use locale\*(C'
much faster.

- \(bu
Empty \f(CW\*(C`DESTROY\*(C' methods are no longer called.

- \(bu
\f(CW\*(C`Perl_sv_utf8_upgrade()\*(C' is now faster.

- \(bu
\f(CW\*(C`keys\*(C' on empty hash is now faster.

- \(bu
\f(CW\*(C`if (%foo)\*(C' has been optimized to be faster than \f(CW\*(C`if (keys %foo)\*(C'.

- \(bu
The string repetition operator (\f(CW\*(C`$str x $num\*(C') is now several times
faster when \f(CW$str has length one or \f(CW$num is large.

- \(bu
Reversing an array to itself (as in \f(CW\*(C`@a = reverse @a\*(C') in void context
now happens in-place and is several orders of magnitude faster than
it used to be. It will also preserve non-existent elements whenever
possible, i.e. for non magical arrays or tied arrays with \f(CW\*(C`EXISTS\*(C'
and \f(CW\*(C`DELETE\*(C' methods.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"

- \(bu
perlapi, perlintern, perlmodlib and perltoc are now all
generated at build time, rather than being shipped as part of the release.

- \(bu
If \f(CW\*(C`vendorlib\*(C' and \f(CW\*(C`vendorarch\*(C' are the same, then they are only added
to \f(CW@INC once.

- \(bu
\f(CW$Config\{usedevel\} and the C-level \f(CW\*(C`PERL_USE_DEVEL\*(C' are now defined if
perl is built with  \f(CW\*(C`-Dusedevel\*(C'.

- \(bu
*Configure* will enable use of \f(CW\*(C`-fstack-protector\*(C', to provide protection
against stack-smashing attacks, if the compiler supports it.

- \(bu
*Configure* will now determine the correct prototypes for re-entrant
functions and for \f(CW\*(C`gconvert\*(C' if you are using a \*(C+ compiler rather
than a C compiler.

- \(bu
On Unix, if you build from a tree containing a git repository, the
configuration process will note the commit hash you have checked out, for
display in the output of \f(CW\*(C`perl -v\*(C' and \f(CW\*(C`perl -V\*(C'. Unpushed local commits
are automatically added to the list of local patches displayed by
\f(CW\*(C`perl -V\*(C'.

- \(bu
Perl now supports SystemTap's \f(CW\*(C`dtrace\*(C' compatibility layer and an
issue with linking \f(CW\*(C`miniperl\*(C' has been fixed in the process.

- \(bu
perldoc now uses \f(CW\*(C`less -R\*(C' instead of \f(CW\*(C`less\*(C' for improved behaviour
in the face of \f(CW\*(C`groff\*(C''s new usage of \s-1ANSI\s0 escape codes.

- \(bu
\f(CW\*(C`perl -V\*(C' now reports use of the compile-time options \f(CW\*(C`USE_PERL_ATOF\*(C' and
\f(CW\*(C`USE_ATTRIBUTES_FOR_PERLIO\*(C'.

- \(bu
As part of the flattening of *ext*, all extensions on all platforms are
built by *make_ext.pl*. This replaces the Unix-specific
*ext/util/make_ext*, VMS-specific *make_ext.com* and Win32-specific
*win32/buildext.pl*.

## Internal Changes

Header "Internal Changes"
Each release of Perl sees numerous internal changes which shouldn't
affect day to day usage but may still be notable for developers working
with Perl's source code.

- \(bu
The J.R.R. Tolkien quotes at the head of C source file have been checked
and proper citations added, thanks to a patch from Tom Christiansen.

- \(bu
The internal structure of the dual-life modules traditionally found in
the *lib/* and *ext/* directories in the perl source has changed
significantly. Where possible, dual-lifed modules have been extracted
from *lib/* and *ext/*.
.Sp
Dual-lifed modules maintained by Perl's developers as part of the Perl
core now live in *dist/*.  Dual-lifed modules maintained primarily on
\s-1CPAN\s0 now live in *cpan/*.  When reporting a bug in a module located
under *cpan/*, please send your bug report directly to the module's
bug tracker or author, rather than Perl's bug tracker.

- \(bu
\f(CW\*(C`\\N\{...\}\*(C' now compiles better, always forces \s-1UTF-8\s0 internal representation
.Sp
Perl's developers have fixed several problems with the recognition of
\f(CW\*(C`\\N\{...\}\*(C' constructs.  As part of this, perl will store any scalar
or regex containing \f(CW\*(C`\\N\{\f(CIname\f(CW\}\*(C' or \f(CW\*(C`\\N\{U+\f(CIcode point\f(CW\}\*(C' in its
definition in \s-1UTF-8\s0 format. (This was true previously for all occurrences
of \f(CW\*(C`\\N\{\f(CIname\f(CW\}\*(C' that did not use a custom translator, but now it's
always true.)

- \(bu
Perl_magic_setmglob now knows about globs, fixing \s-1RT\s0 #71254.

- \(bu
\f(CW\*(C`SVt_RV\*(C' no longer exists. RVs are now stored in IVs.

- \(bu
\f(CW\*(C`Perl_vcroak()\*(C' now accepts a null first argument. In addition, a full
audit was made of the \*(L"not \s-1NULL\*(R"\s0 compiler annotations, and those for
several other internal functions were corrected.

- \(bu
New macros \f(CW\*(C`dSAVEDERRNO\*(C', \f(CW\*(C`dSAVE_ERRNO\*(C', \f(CW\*(C`SAVE_ERRNO\*(C', \f(CW\*(C`RESTORE_ERRNO\*(C'
have been added to formalise the temporary saving of the \f(CW\*(C`errno\*(C'
variable.

- \(bu
The function \f(CW\*(C`Perl_sv_insert_flags\*(C' has been added to augment
\f(CW\*(C`Perl_sv_insert\*(C'.

- \(bu
The function \f(CW\*(C`Perl_newSV_type(type)\*(C' has been added, equivalent to
\f(CW\*(C`Perl_newSV()\*(C' followed by \f(CW\*(C`Perl_sv_upgrade(type)\*(C'.

- \(bu
The function \f(CW\*(C`Perl_newSVpvn_flags()\*(C' has been added, equivalent to
\f(CW\*(C`Perl_newSVpvn()\*(C' and then performing the action relevant to the flag.
.Sp
Two flag bits are currently supported.

> 
- \(bu
\f(CW\*(C`SVf_UTF8\*(C' will call \f(CW\*(C`SvUTF8_on()\*(C' for you. (Note that this does
not convert a sequence of \s-1ISO 8859-1\s0 characters to \s-1UTF-8\s0). A wrapper,
\f(CW\*(C`newSVpvn_utf8()\*(C' is available for this.

- \(bu
\f(CW\*(C`SVs_TEMP\*(C' now calls \f(CW\*(C`Perl_sv_2mortal()\*(C' on the new \s-1SV.\s0



> .Sp
There is also a wrapper that takes constant strings, \f(CW\*(C`newSVpvs_flags()\*(C'.



- \(bu
The function \f(CW\*(C`Perl_croak_xs_usage\*(C' has been added as a wrapper to
\f(CW\*(C`Perl_croak\*(C'.

- \(bu
Perl now exports the functions \f(CW\*(C`PerlIO_find_layer\*(C' and \f(CW\*(C`PerlIO_list_alloc\*(C'.

- \(bu
\f(CW\*(C`PL_na\*(C' has been exterminated from the core code, replaced by local
\s-1STRLEN\s0 temporaries, or \f(CW\*(C`*_nolen()\*(C' calls. Either approach is faster than
\f(CW\*(C`PL_na\*(C', which is a pointer dereference into the interpreter structure
under ithreads, and a global variable otherwise.

- \(bu
\f(CW\*(C`Perl_mg_free()\*(C' used to leave freed memory accessible via \f(CW\*(C`SvMAGIC()\*(C'
on the scalar. It now updates the linked list to remove each piece of
magic as it is freed.

- \(bu
Under ithreads, the regex in \f(CW\*(C`PL_reg_curpm\*(C' is now reference
counted. This eliminates a lot of hackish workarounds to cope with it
not being reference counted.

- \(bu
\f(CW\*(C`Perl_mg_magical()\*(C' would sometimes incorrectly turn on \f(CW\*(C`SvRMAGICAL()\*(C'.
This has been fixed.

- \(bu
The *public* \s-1IV\s0 and \s-1NV\s0 flags are now not set if the string value has
trailing \*(L"garbage\*(R". This behaviour is consistent with not setting the
public \s-1IV\s0 or \s-1NV\s0 flags if the value is out of range for the type.

- \(bu
Uses of \f(CW\*(C`Nullav\*(C', \f(CW\*(C`Nullcv\*(C', \f(CW\*(C`Nullhv\*(C', \f(CW\*(C`Nullop\*(C', \f(CW\*(C`Nullsv\*(C' etc have
been replaced by \f(CW\*(C`NULL\*(C' in the core code, and non-dual-life modules,
as \f(CW\*(C`NULL\*(C' is clearer to those unfamiliar with the core code.

- \(bu
A macro \f(CWMUTABLE_PTR(p) has been added, which on (non-pedantic) gcc will
not cast away \f(CW\*(C`const\*(C', returning a \f(CW\*(C`void *\*(C'. Macros \f(CW\*(C`MUTABLE_SV(av)\*(C',
\f(CW\*(C`MUTABLE_SV(cv)\*(C' etc build on this, casting to \f(CW\*(C`AV *\*(C' etc without
casting away \f(CW\*(C`const\*(C'. This allows proper compile-time auditing of
\f(CW\*(C`const\*(C' correctness in the core, and helped picked up some errors
(now fixed).

- \(bu
Macros \f(CW\*(C`mPUSHs()\*(C' and \f(CW\*(C`mXPUSHs()\*(C' have been added, for pushing SVs on the
stack and mortalizing them.

- \(bu
Use of the private structure \f(CW\*(C`mro_meta\*(C' has changed slightly. Nothing
outside the core should be accessing this directly anyway.

- \(bu
A new tool, *Porting/expand-macro.pl* has been added, that allows you
to view how a C preprocessor macro would be expanded when compiled.
This is handy when trying to decode the macro hell that is the perl
guts.

## Testing

Header "Testing"

### Testing improvements

Subsection "Testing improvements"

- Parallel tests
Item "Parallel tests"
The core distribution can now run its regression tests in parallel on
Unix-like platforms. Instead of running \f(CW\*(C`make test\*(C', set \f(CW\*(C`TEST_JOBS\*(C' in
your environment to the number of tests to run in parallel, and run
\f(CW\*(C`make test_harness\*(C'. On a Bourne-like shell, this can be done as
.Sp
.Vb 1
    TEST_JOBS=3 make test_harness  # Run 3 tests in parallel
.Ve
.Sp
An environment variable is used, rather than parallel make itself, because
TAP::Harness needs to be able to schedule individual non-conflicting test
scripts itself, and there is no standard interface to \f(CW\*(C`make\*(C' utilities to
interact with their job schedulers.
.Sp
Note that currently some test scripts may fail when run in parallel (most
notably \f(CW\*(C`ext/IO/t/io_dir.t\*(C'). If necessary run just the failing scripts
again sequentially and see if the failures go away.

- Test harness flexibility
Item "Test harness flexibility"
It's now possible to override \f(CW\*(C`PERL5OPT\*(C' and friends in *t/TEST*

- Test watchdog
Item "Test watchdog"
Several tests that have the potential to hang forever if they fail now
incorporate a \*(L"watchdog\*(R" functionality that will kill them after a timeout,
which helps ensure that \f(CW\*(C`make test\*(C' and \f(CW\*(C`make test_harness\*(C' run to
completion automatically.

### New Tests

Subsection "New Tests"
Perl's developers have added a number of new tests to the core.
In addition to the items listed below, many modules updated from \s-1CPAN\s0
incorporate new tests.

- \(bu
Significant cleanups to core tests to ensure that language and
interpreter features are not used before they're tested.

- \(bu
\f(CW\*(C`make test_porting\*(C' now runs a number of important pre-commit checks
which might be of use to anyone working on the Perl core.

- \(bu
*t/porting/podcheck.t* automatically checks the well-formedness of
\s-1POD\s0 found in all .pl, .pm and .pod files in the *\s-1MANIFEST\s0*, other than in
dual-lifed modules which are primarily maintained outside the Perl core.

- \(bu
*t/porting/manifest.t* now tests that all files listed in \s-1MANIFEST\s0
are present.

- \(bu
*t/op/while_readdir.t* tests that a bare readdir in while loop sets \f(CW$_.

- \(bu
*t/comp/retainedlines.t* checks that the debugger can retain source
lines from \f(CW\*(C`eval\*(C'.

- \(bu
*t/io/perlio_fail.t* checks that bad layers fail.

- \(bu
*t/io/perlio_leaks.t* checks that PerlIO layers are not leaking.

- \(bu
*t/io/perlio_open.t* checks that certain special forms of open work.

- \(bu
*t/io/perlio.t* includes general PerlIO tests.

- \(bu
*t/io/pvbm.t* checks that there is no unexpected interaction between
the internal types \f(CW\*(C`PVBM\*(C' and \f(CW\*(C`PVGV\*(C'.

- \(bu
*t/mro/package_aliases.t* checks that mro works properly in the presence
of aliased packages.

- \(bu
*t/op/dbm.t* tests \f(CW\*(C`dbmopen\*(C' and \f(CW\*(C`dbmclose\*(C'.

- \(bu
*t/op/index_thr.t* tests the interaction of \f(CW\*(C`index\*(C' and threads.

- \(bu
*t/op/pat_thr.t* tests the interaction of esoteric patterns and threads.

- \(bu
*t/op/qr_gc.t* tests that \f(CW\*(C`qr\*(C' doesn't leak.

- \(bu
*t/op/reg_email_thr.t* tests the interaction of regex recursion and threads.

- \(bu
*t/op/regexp_qr_embed_thr.t* tests the interaction of patterns with
embedded \f(CW\*(C`qr//\*(C' and threads.

- \(bu
*t/op/regexp_unicode_prop.t* tests Unicode properties in regular
expressions.

- \(bu
*t/op/regexp_unicode_prop_thr.t* tests the interaction of Unicode
properties and threads.

- \(bu
*t/op/reg_nc_tie.t* tests the tied methods of \f(CW\*(C`Tie::Hash::NamedCapture\*(C'.

- \(bu
*t/op/reg_posixcc.t* checks that \s-1POSIX\s0 character classes behave
consistently.

- \(bu
*t/op/re.t* checks that exportable \f(CW\*(C`re\*(C' functions in *universal.c* work.

- \(bu
*t/op/setpgrpstack.t* checks that \f(CW\*(C`setpgrp\*(C' works.

- \(bu
*t/op/substr_thr.t* tests the interaction of \f(CW\*(C`substr\*(C' and threads.

- \(bu
*t/op/upgrade.t* checks that upgrading and assigning scalars works.

- \(bu
*t/uni/lex_utf8.t* checks that Unicode in the lexer works.

- \(bu
*t/uni/tie.t* checks that Unicode and \f(CW\*(C`tie\*(C' work.

- \(bu
*t/comp/final_line_num.t* tests whether line numbers are correct at \s-1EOF\s0

- \(bu
*t/comp/form_scope.t* tests format scoping.

- \(bu
*t/comp/line_debug.t* tests whether \f(CW\*(C`@\{"_<$file"\}\*(C' works.

- \(bu
*t/op/filetest_t.t* tests if -t file test works.

- \(bu
*t/op/qr.t* tests \f(CW\*(C`qr\*(C'.

- \(bu
*t/op/utf8cache.t* tests malfunctions of the utf8 cache.

- \(bu
*t/re/uniprops.t* test unicodes \f(CW\*(C`\\p\{\}\*(C' regex constructs.

- \(bu
*t/op/filehandle.t* tests some suitably portable filetest operators
to check that they work as expected, particularly in the light of some
internal changes made in how filehandles are blessed.

- \(bu
*t/op/time_loop.t* tests that unix times greater than \f(CW\*(C`2**63\*(C', which
can now be handed to \f(CW\*(C`gmtime\*(C' and \f(CW\*(C`localtime\*(C', do not cause an internal
overflow or an excessively long loop.

## New or Changed Diagnostics

Header "New or Changed Diagnostics"

### New Diagnostics

Subsection "New Diagnostics"

- \(bu
\s-1SV\s0 allocation tracing has been added to the diagnostics enabled by \f(CW\*(C`-Dm\*(C'.
The tracing can alternatively output via the \f(CW\*(C`PERL_MEM_LOG\*(C' mechanism, if
that was enabled when the *perl* binary was compiled.

- \(bu
Smartmatch resolution tracing has been added as a new diagnostic. Use
\f(CW\*(C`-DM\*(C' to enable it.

- \(bu
A new debugging flag \f(CW\*(C`-DB\*(C' now dumps subroutine definitions, leaving
\f(CW\*(C`-Dx\*(C' for its original purpose of dumping syntax trees.

- \(bu
Perl 5.12 provides a number of new diagnostic messages to help you write
better code.  See perldiag for details of these new messages.

> 
- \(bu
\f(CW\*(C`Bad plugin affecting keyword \*(Aq%s\*(Aq\*(C'

- \(bu
\f(CW\*(C`gmtime(%.0f) too large\*(C'

- \(bu
\f(CW\*(C`Lexing code attempted to stuff non-Latin-1 character into Latin-1 input\*(C'

- \(bu
\f(CW\*(C`Lexing code internal error (%s)\*(C'

- \(bu
\f(CW\*(C`localtime(%.0f) too large\*(C'

- \(bu
\f(CW\*(C`Overloaded dereference did not return a reference\*(C'

- \(bu
\f(CW\*(C`Overloaded qr did not return a REGEXP\*(C'

- \(bu
\f(CW\*(C`Perl_pmflag() is deprecated, and will be removed from the XS API\*(C'

- \(bu
\f(CW\*(C`lvalue attribute ignored after the subroutine has been defined\*(C'
.Sp
This new warning is issued when one attempts to mark a subroutine as
lvalue after it has been defined.

- \(bu
Perl now warns you if \f(CW\*(C`++\*(C' or \f(CW\*(C`--\*(C' are unable to change the value
because it's beyond the limit of representation.
.Sp
This uses a new warnings category: \*(L"imprecision\*(R".

- \(bu
\f(CW\*(C`lc\*(C', \f(CW\*(C`uc\*(C', \f(CW\*(C`lcfirst\*(C', and \f(CW\*(C`ucfirst\*(C' warn when passed undef.

- \(bu
\f(CW\*(C`Show constant in "Useless use of a constant in void context"\*(C'

- \(bu
\f(CW\*(C`Prototype after \*(Aq%s\*(Aq\*(C'

- \(bu
\f(CW\*(C`panic: sv_chop %s\*(C'
.Sp
This new fatal error occurs when the C routine \f(CW\*(C`Perl_sv_chop()\*(C' was
passed a position that is not within the scalar's string buffer. This
could be caused by buggy \s-1XS\s0 code, and at this point recovery is not
possible.

- \(bu
The fatal error \f(CW\*(C`Malformed UTF-8 returned by \\N\*(C' is now produced if the
\f(CW\*(C`charnames\*(C' handler returns malformed \s-1UTF-8.\s0

- \(bu
If an unresolved named character or sequence was encountered when
compiling a regex pattern then the fatal error \f(CW\*(C`\\N\{NAME\} must be resolved
by the lexer\*(C' is now produced. This can happen, for example, when using a
single-quotish context like \f(CW\*(C`$re = \*(Aq\\N\{SPACE\}\*(Aq; /$re/;\*(C'. See perldiag
for more examples of how the lexer can get bypassed.

- \(bu
\f(CW\*(C`Invalid hexadecimal number in \\N\{U+...\}\*(C' is a new fatal error
triggered when the character constant represented by \f(CW\*(C`...\*(C' is not a
valid hexadecimal number.

- \(bu
The new meaning of \f(CW\*(C`\\N\*(C' as \f(CW\*(C`[^\\n]\*(C' is not valid in a bracketed character
class, just like \f(CW\*(C`.\*(C' in a character class loses its special meaning,
and will cause the fatal error \f(CW\*(C`\\N in a character class must be a named
character: \\N\{...\}\*(C'.

- \(bu
The rules on what is legal for the \f(CW\*(C`...\*(C' in \f(CW\*(C`\\N\{...\}\*(C' have been
tightened up so that unless the \f(CW\*(C`...\*(C' begins with an alphabetic
character and continues with a combination of alphanumerics, dashes,
spaces, parentheses or colons then the warning \f(CW\*(C`Deprecated character(s)
in \\N\{...\} starting at \*(Aq%s\*(Aq\*(C' is now issued.

- \(bu
The warning \f(CW\*(C`Using just the first characters returned by \\N\{\}\*(C' will
be issued if the \f(CW\*(C`charnames\*(C' handler returns a sequence of characters
which exceeds the limit of the number of characters that can be used. The
message will indicate which characters were used and which were discarded.



> 


### Changed Diagnostics

Subsection "Changed Diagnostics"
A number of existing diagnostic messages have been improved or corrected:

- \(bu
A new warning category \f(CW\*(C`illegalproto\*(C' allows finer-grained control of
warnings around function prototypes.
.Sp
The two warnings:

> .ie n .IP """Illegal character in prototype for %s : %s""" 4
.el .IP "\f(CWIllegal character in prototype for %s : %s" 4
Item "Illegal character in prototype for %s : %s"
0
.ie n .IP """Prototype after \*(Aq%c\*(Aq for %s : %s""" 4
.el .IP "\f(CWPrototype after \*(Aq%c\*(Aq for %s : %s" 4
Item "Prototype after %c for %s : %s"



> .PD
.Sp
have been moved from the \f(CW\*(C`syntax\*(C' top-level warnings category into a new
first-level category, \f(CW\*(C`illegalproto\*(C'. These two warnings are currently
the only ones emitted during parsing of an invalid/illegal prototype,
so one can now use
.Sp
.Vb 1
  no warnings \*(Aqillegalproto\*(Aq;
.Ve
.Sp
to suppress only those, but not other syntax-related warnings. Warnings
where prototypes are changed, ignored, or not met are still in the
\f(CW\*(C`prototype\*(C' category as before.



- \(bu
\f(CW\*(C`Deep recursion on subroutine "%s"\*(C'
.Sp
It is now possible to change the depth threshold for this warning from the
default of 100, by recompiling the *perl* binary, setting the C
pre-processor macro \f(CW\*(C`PERL_SUB_DEPTH_WARN\*(C' to the desired value.

- \(bu
\f(CW\*(C`Illegal character in prototype\*(C' warning is now more precise
when reporting illegal characters after _

- \(bu
mro merging error messages are now very similar to those produced by
Algorithm::C3.

- \(bu
Amelioration of the error message \*(L"Unrecognized character \f(CW%s in column \f(CW%d\*(R"
.Sp
Changes the error message to \*(L"Unrecognized character \f(CW%s; marked by <--
\s-1HERE\s0 after \f(CW%s<-- \s-1HERE\s0 near column \f(CW%d\*(R". This should make it a little
simpler to spot and correct the suspicious character.

- \(bu
Perl now explicitly points to \f(CW$. when it causes an uninitialized
warning for ranges in scalar context.

- \(bu
\f(CW\*(C`split\*(C' now warns when called in void context.

- \(bu
\f(CW\*(C`printf\*(C'-style functions called with too few arguments will now issue the
warning \f(CW"Missing argument in %s" [perl #71000]

- \(bu
Perl now properly returns a syntax error instead of segfaulting
if \f(CW\*(C`each\*(C', \f(CW\*(C`keys\*(C', or \f(CW\*(C`values\*(C' is used without an argument.

- \(bu
\f(CW\*(C`tell()\*(C' now fails properly if called without an argument and when no
previous file was read.
.Sp
\f(CW\*(C`tell()\*(C' now returns \f(CW\*(C`-1\*(C', and sets errno to \f(CW\*(C`EBADF\*(C', thus restoring
the 5.8.x behaviour.

- \(bu
\f(CW\*(C`overload\*(C' no longer implicitly unsets fallback on repeated 'use
overload' lines.

- \(bu
**POSIX::strftime()** can now handle Unicode characters in the format string.

- \(bu
The \f(CW\*(C`syntax\*(C' category was removed from 5 warnings that should only be in
\f(CW\*(C`deprecated\*(C'.

- \(bu
Three fatal \f(CW\*(C`pack\*(C'/\f(CW\*(C`unpack\*(C' error messages have been normalized to
\f(CW\*(C`panic: %s\*(C'

- \(bu
\f(CW\*(C`Unicode character is illegal\*(C' has been rephrased to be more accurate
.Sp
It now reads \f(CW\*(C`Unicode non-character is illegal in interchange\*(C' and the
perldiag documentation has been expanded a bit.

- \(bu
Currently, all but the first of the several characters that the
\f(CW\*(C`charnames\*(C' handler may return are discarded when used in a regular
expression pattern bracketed character class. If this happens then the
warning \f(CW\*(C`Using just the first character returned by \\N\{\} in character
class\*(C' will be issued.

- \(bu
The warning \f(CW\*(C`Missing right brace on \\N\{\} or unescaped left brace after
\\N.  Assuming the latter\*(C' will be issued if Perl encounters a \f(CW\*(C`\\N\{\*(C'
but doesn't find a matching \f(CW\*(C`\}\*(C'. In this case Perl doesn't know if it
was mistakenly omitted, or if \*(L"match non-newline\*(R" followed by "match
a \f(CW\*(C`\{\*(C'" was desired.  It assumes the latter because that is actually a
valid interpretation as written, unlike the other case.  If you meant
the former, you need to add the matching right brace.  If you did mean
the latter, you can silence this warning by writing instead \f(CW\*(C`\\N\\\{\*(C'.

- \(bu
\f(CW\*(C`gmtime\*(C' and \f(CW\*(C`localtime\*(C' called with numbers smaller than they can
reliably handle will now issue the warnings \f(CW\*(C`gmtime(%.0f) too small\*(C'
and \f(CW\*(C`localtime(%.0f) too small\*(C'.

The following diagnostic messages have been removed:

- \(bu
\f(CW\*(C`Runaway format\*(C'

- \(bu
\f(CW\*(C`Can\*(Aqt locate package %s for the parents of %s\*(C'
.Sp
In general this warning it only got produced in
conjunction with other warnings, and removing it allowed an \s-1ISA\s0 lookup
optimisation to be added.

- \(bu
\f(CW\*(C`v-string in use/require is non-portable\*(C'

## Utility Changes

Header "Utility Changes"

- \(bu
*h2ph* now looks in \f(CW\*(C`include-fixed\*(C' too, which is a recent addition
to gcc's search path.

- \(bu
*h2xs* no longer incorrectly treats enum values like macros.
It also now handles \*(C+ style comments (\f(CW\*(C`//\*(C') properly in enums.

- \(bu
*perl5db.pl* now supports \f(CW\*(C`LVALUE\*(C' subroutines.  Additionally, the
debugger now correctly handles proxy constant subroutines, and
subroutine stubs.

- \(bu
*perlbug* now uses \f(CW%Module::CoreList::bug_tracker to print out
upstream bug tracker URLs.  If a user identifies a particular module
as the topic of their bug report and we're able to divine the \s-1URL\s0 for
its upstream bug tracker, perlbug now provide a message to the user
explaining that the core copies the \s-1CPAN\s0 version directly, and provide
the \s-1URL\s0 for reporting the bug directly to the upstream author.
.Sp
*perlbug* no longer reports \*(L"Message sent\*(R" when it hasn't actually sent
the message

- \(bu
*perlthanks* is a new utility for sending non-bug-reports to the
authors and maintainers of Perl. Getting nothing but bug reports can
become a bit demoralising. If Perl 5.12 works well for you, please try
out *perlthanks*. It will make the developers smile.

- \(bu
Perl's developers have fixed bugs in *a2p* having to do with the
\f(CW\*(C`match()\*(C' operator in list context.  Additionally, *a2p* no longer
generates code that uses the \f(CW$[ variable.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
U+0FFFF is now a legal character in regular expressions.

- \(bu
pp_qr now always returns a new regexp \s-1SV.\s0 Resolves \s-1RT\s0 #69852.
.Sp
Instead of returning a(nother) reference to the (pre-compiled) regexp
in the optree, use **reg_temp_copy()** to create a copy of it, and return a
reference to that. This resolves issues about Regexp::DESTROY not being
called in a timely fashion (the original bug tracked by \s-1RT\s0 #69852), as
well as bugs related to blessing regexps, and of assigning to regexps,
as described in correspondence added to the ticket.
.Sp
It transpires that we also need to undo the **SvPVX()** sharing when ithreads
cloning a Regexp \s-1SV,\s0 because mother_re is set to \s-1NULL,\s0 instead of a
cloned copy of the mother_re. This change might fix bugs with regexps
and threads in certain other situations, but as yet neither tests nor
bug reports have indicated any problems, so it might not actually be an
edge case that it's possible to reach.

- \(bu
Several compilation errors and segfaults when perl was built with \f(CW\*(C`-Dmad\*(C'
were fixed.

- \(bu
Fixes for lexer \s-1API\s0 changes in 5.11.2 which broke NYTProf's savesrc option.

- \(bu
\f(CW\*(C`-t\*(C' should only return \s-1TRUE\s0 for file handles connected to a \s-1TTY\s0
.Sp
The Microsoft C version of \f(CW\*(C`isatty()\*(C' returns \s-1TRUE\s0 for all character mode
devices, including the */dev/null*-style \*(L"nul\*(R" device and printers like
\*(L"lpt1\*(R".

- \(bu
Fixed a regression caused by commit fafafbaf which caused a panic during
parameter passing [perl #70171]

- \(bu
On systems which in-place edits without backup files, -i'*' now works as
the documentation says it does [perl #70802]

- \(bu
Saving and restoring magic flags no longer loses readonly flag.

- \(bu
The malformed syntax \f(CW\*(C`grep EXPR LIST\*(C' (note the missing comma) no longer
causes abrupt and total failure.

- \(bu
Regular expressions compiled with \f(CW\*(C`qr\{\}\*(C' literals properly set \f(CW\*(C`$\*(Aq\*(C' when
matching again.

- \(bu
Using named subroutines with \f(CW\*(C`sort\*(C' should no longer lead to bus errors
[perl #71076]

- \(bu
Numerous bugfixes catch small issues caused by the recently-added Lexer \s-1API.\s0

- \(bu
Smart match against \f(CW@_ sometimes gave false negatives. [perl #71078]

- \(bu
\f(CW$@ may now be assigned a read-only value (without error or busting
the stack).

- \(bu
\f(CW\*(C`sort\*(C' called recursively from within an active comparison subroutine no
longer causes a bus error if run multiple times. [perl #71076]

- \(bu
Tie::Hash::NamedCapture::* will not abort if passed bad input (\s-1RT\s0 #71828)

- \(bu
\f(CW@_ and \f(CW$_ no longer leak under threads (\s-1RT\s0 #34342 and #41138, also
#70602, #70974)

- \(bu
\f(CW\*(C`-I\*(C' on shebang line now adds directories in front of \f(CW@INC
as documented, and as does \f(CW\*(C`-I\*(C' when specified on the command-line.

- \(bu
\f(CW\*(C`kill\*(C' is now fatal when called on non-numeric process identifiers.
Previously, an \f(CW\*(C`undef\*(C' process identifier would be interpreted as a
request to kill process 0, which would terminate the current process
group on \s-1POSIX\s0 systems. Since process identifiers are always integers,
killing a non-numeric process is now fatal.

- \(bu
5.10.0 inadvertently disabled an optimisation, which caused a measurable
performance drop in list assignment, such as is often used to assign
function parameters from \f(CW@_. The optimisation has been re-instated, and
the performance regression fixed. (This fix is also present in 5.10.1)

- \(bu
Fixed memory leak on \f(CW\*(C`while (1) \{ map 1, 1 \}\*(C' [\s-1RT\s0 #53038].

- \(bu
Some potential coredumps in PerlIO fixed [\s-1RT\s0 #57322,54828].

- \(bu
The debugger now works with lvalue subroutines.

- \(bu
The debugger's \f(CW\*(C`m\*(C' command was broken on modules that defined constants
[\s-1RT\s0 #61222].

- \(bu
\f(CW\*(C`crypt\*(C' and string complement could return tainted values for untainted
arguments [\s-1RT\s0 #59998].

- \(bu
The \f(CW\*(C`-i\*(C'*.suffix* command-line switch now recreates the file using
restricted permissions, before changing its mode to match the original
file. This eliminates a potential race condition [\s-1RT\s0 #60904].

- \(bu
On some Unix systems, the value in \f(CW$? would not have the top bit set
(\f(CW\*(C`$? & 128\*(C') even if the child core dumped.

- \(bu
Under some circumstances, \f(CW$^R could incorrectly become undefined
[\s-1RT\s0 #57042].

- \(bu
In the \s-1XS API,\s0 various hash functions, when passed a pre-computed hash where
the key is \s-1UTF-8,\s0 might result in an incorrect lookup.

- \(bu
\s-1XS\s0 code including *\s-1XSUB\s0.h* before *perl.h* gave a compile-time error
[\s-1RT\s0 #57176].

- \(bu
\f(CW\*(C`$object->isa(\*(AqFoo\*(Aq)\*(C' would report false if the package \f(CW\*(C`Foo\*(C'
didn't exist, even if the object's \f(CW@ISA contained \f(CW\*(C`Foo\*(C'.

- \(bu
Various bugs in the new-to 5.10.0 mro code, triggered by manipulating
\f(CW@ISA, have been found and fixed.

- \(bu
Bitwise operations on references could crash the interpreter, e.g.
\f(CW\*(C`$x=\\$y; $x |= "foo"\*(C' [\s-1RT\s0 #54956].

- \(bu
Patterns including alternation might be sensitive to the internal \s-1UTF-8\s0
representation, e.g.
.Sp
.Vb 3
    my $byte = chr(192);
    my $utf8 = chr(192); utf8::upgrade($utf8);
    $utf8 =~ /$byte|X\}/i;       # failed in 5.10.0
.Ve

- \(bu
Within UTF8-encoded Perl source files (i.e. where \f(CW\*(C`use utf8\*(C' is in
effect), double-quoted literal strings could be corrupted where a \f(CW\*(C`\\xNN\*(C',
\f(CW\*(C`\\0NNN\*(C' or \f(CW\*(C`\\N\{\}\*(C' is followed by a literal character with ordinal value
greater than 255 [\s-1RT\s0 #59908].

- \(bu
\f(CW\*(C`B::Deparse\*(C' failed to correctly deparse various constructs:
\f(CW\*(C`readpipe STRING\*(C' [\s-1RT\s0 #62428], \f(CW\*(C`CORE::require(STRING)\*(C' [\s-1RT\s0 #62488],
\f(CW\*(C`sub foo(_)\*(C' [\s-1RT\s0 #62484].

- \(bu
Using \f(CW\*(C`setpgrp\*(C' with no arguments could corrupt the perl stack.

- \(bu
The block form of \f(CW\*(C`eval\*(C' is now specifically trappable by \f(CW\*(C`Safe\*(C' and
\f(CW\*(C`ops\*(C'. Previously it was erroneously treated like string \f(CW\*(C`eval\*(C'.

- \(bu
In 5.10.0, the two characters \f(CW\*(C`[~\*(C' were sometimes parsed as the smart
match operator (\f(CW\*(C`~~\*(C') [\s-1RT\s0 #63854].

- \(bu
In 5.10.0, the \f(CW\*(C`*\*(C' quantifier in patterns was sometimes treated as
\f(CW\*(C`\{0,32767\}\*(C' [\s-1RT\s0 #60034, #60464]. For example, this match would fail:
.Sp
.Vb 1
    ("ab" x 32768) =~ /^(ab)*$/
.Ve

- \(bu
\f(CW\*(C`shmget\*(C' was limited to a 32 bit segment size on a 64 bit \s-1OS\s0 [\s-1RT\s0 #63924].

- \(bu
Using \f(CW\*(C`next\*(C' or \f(CW\*(C`last\*(C' to exit a \f(CW\*(C`given\*(C' block no longer produces a
spurious warning like the following:
.Sp
.Vb 1
    Exiting given via last at foo.pl line 123
.Ve

- \(bu
Assigning a format to a glob could corrupt the format; e.g.:
.Sp
.Vb 1
     *bar=*foo\{FORMAT\}; # foo format now bad
.Ve

- \(bu
Attempting to coerce a typeglob to a string or number could cause an
assertion failure. The correct error message is now generated,
\f(CW\*(C`Can\*(Aqt coerce GLOB to \f(CI$type\f(CW\*(C'.

- \(bu
Under \f(CW\*(C`use filetest \*(Aqaccess\*(Aq\*(C', \f(CW\*(C`-x\*(C' was using the wrong access
mode. This has been fixed [\s-1RT\s0 #49003].

- \(bu
\f(CW\*(C`length\*(C' on a tied scalar that returned a Unicode value would not be
correct the first time. This has been fixed.

- \(bu
Using an array \f(CW\*(C`tie\*(C' inside in array \f(CW\*(C`tie\*(C' could \s-1SEGV.\s0 This has been
fixed. [\s-1RT\s0 #51636]

- \(bu
A race condition inside \f(CW\*(C`PerlIOStdio_close()\*(C' has been identified and
fixed. This used to cause various threading issues, including SEGVs.

- \(bu
In \f(CW\*(C`unpack\*(C', the use of \f(CW\*(C`()\*(C' groups in scalar context was internally
placing a list on the interpreter's stack, which manifested in various
ways, including SEGVs. This is now fixed [\s-1RT\s0 #50256].

- \(bu
Magic was called twice in \f(CW\*(C`substr\*(C', \f(CW\*(C`\$x\*(C', \f(CW\*(C`tie $x, $m\*(C' and \f(CW\*(C`chop\*(C'.
These have all been fixed.

- \(bu
A 5.10.0 optimisation to clear the temporary stack within the implicit
loop of \f(CW\*(C`s///ge\*(C' has been reverted, as it turned out to be the cause of
obscure bugs in seemingly unrelated parts of the interpreter [commit
ef0d4e17921ee3de].

- \(bu
The line numbers for warnings inside \f(CW\*(C`elsif\*(C' are now correct.

- \(bu
The \f(CW\*(C`..\*(C' operator now works correctly with ranges whose ends are at or
close to the values of the smallest and largest integers.

- \(bu
\f(CW\*(C`binmode STDIN, \*(Aq:raw\*(Aq\*(C' could lead to segmentation faults on some platforms.
This has been fixed [\s-1RT\s0 #54828].

- \(bu
An off-by-one error meant that \f(CW\*(C`index $str, ...\*(C' was effectively being
executed as \f(CW\*(C`index "$str\\0", ...\*(C'. This has been fixed [\s-1RT\s0 #53746].

- \(bu
Various leaks associated with named captures in regexes have been fixed
[\s-1RT\s0 #57024].

- \(bu
A weak reference to a hash would leak. This was affecting \f(CW\*(C`DBI\*(C'
[\s-1RT\s0 #56908].

- \(bu
Using (?|) in a regex could cause a segfault [\s-1RT\s0 #59734].

- \(bu
Use of a \s-1UTF-8\s0 \f(CW\*(C`tr//\*(C' within a closure could cause a segfault [\s-1RT\s0 #61520].

- \(bu
Calling \f(CW\*(C`Perl_sv_chop()\*(C' or otherwise upgrading an \s-1SV\s0 could result in an
unaligned 64-bit access on the \s-1SPARC\s0 architecture [\s-1RT\s0 #60574].

- \(bu
In the 5.10.0 release, \f(CW\*(C`inc_version_list\*(C' would incorrectly list
\f(CW\*(C`5.10.*\*(C' after \f(CW\*(C`5.8.*\*(C'; this affected the \f(CW@INC search order
[\s-1RT\s0 #67628].

- \(bu
In 5.10.0, \f(CW\*(C`pack "a*", $tainted_value\*(C' returned a non-tainted value
[\s-1RT\s0 #52552].

- \(bu
In 5.10.0, \f(CW\*(C`printf\*(C' and \f(CW\*(C`sprintf\*(C' could produce the fatal error
\f(CW\*(C`panic: utf8_mg_pos_cache_update\*(C' when printing \s-1UTF-8\s0 strings
[\s-1RT\s0 #62666].

- \(bu
In the 5.10.0 release, a dynamically created \f(CW\*(C`AUTOLOAD\*(C' method might be
missed (method cache issue) [\s-1RT\s0 #60220,60232].

- \(bu
In the 5.10.0 release, a combination of \f(CW\*(C`use feature\*(C' and \f(CW\*(C`//ee\*(C' could
cause a memory leak [\s-1RT\s0 #63110].

- \(bu
\f(CW\*(C`-C\*(C' on the shebang (\f(CW\*(C`#!\*(C') line is once more permitted if it is also
specified on the command line. \f(CW\*(C`-C\*(C' on the shebang line used to be a
silent no-op *if* it was not also on the command line, so perl 5.10.0
disallowed it, which broke some scripts. Now perl checks whether it is
also on the command line and only dies if it is not [\s-1RT\s0 #67880].

- \(bu
In 5.10.0, certain types of re-entrant regular expression could crash,
or cause the following assertion failure [\s-1RT\s0 #60508]:
.Sp
.Vb 1
    Assertion rx->sublen >= (s - rx->subbeg) + i failed
.Ve

- \(bu
Perl now includes previously missing files from the Unicode Character
Database.

- \(bu
Perl now honors \f(CW\*(C`TMPDIR\*(C' when opening an anonymous temporary file.

## Platform Specific Changes

Header "Platform Specific Changes"
Perl is incredibly portable. In general, if a platform has a C compiler,
someone has ported Perl to it (or will soon).  We're happy to announce
that Perl 5.12 includes support for several new platforms.  At the same
time, it's time to bid farewell to some (very) old friends.

### New Platforms

Subsection "New Platforms"

- Haiku
Item "Haiku"
Perl's developers have merged patches from Haiku's maintainers. Perl
should now build on Haiku.

- MirOS \s-1BSD\s0
Item "MirOS BSD"
Perl should now build on MirOS \s-1BSD.\s0

### Discontinued Platforms

Subsection "Discontinued Platforms"

- Domain/OS
Item "Domain/OS"
0

- MiNT
Item "MiNT"

- Tenon MachTen
Item "Tenon MachTen"
.PD

### Updated Platforms

Subsection "Updated Platforms"

- \s-1AIX\s0
Item "AIX"

> 0

- \(bu
.PD
Removed *libbsd* for \s-1AIX 5L\s0 and 6.1. Only \f(CW\*(C`flock()\*(C' was used from
*libbsd*.

- \(bu
Removed *libgdbm* for \s-1AIX 5L\s0 and 6.1 if *libgdbm* < 1.8.3-5 is
installed.  The *libgdbm* is delivered as an optional package with the
\s-1AIX\s0 Toolbox.  Unfortunately the versions below 1.8.3-5 are broken.

- \(bu
Hints changes mean that \s-1AIX 4.2\s0 should work again.



> 


- Cygwin
Item "Cygwin"

> 0

- \(bu
.PD
Perl now supports IPv6 on Cygwin 1.7 and newer.

- \(bu
On Cygwin we now strip the last number from the \s-1DLL.\s0 This has been the
behaviour in the cygwin.com build for years. The hints files have been
updated.



> 


- Darwin (Mac \s-1OS X\s0)
Item "Darwin (Mac OS X)"

> 0

- \(bu
.PD
Skip testing the be_BY.CP1131 locale on Darwin 10 (Mac \s-1OS X 10.6\s0),
as it's still buggy.

- \(bu
Correct infelicities in the regexp used to identify buggy locales
on Darwin 8 and 9 (Mac \s-1OS X 10.4\s0 and 10.5, respectively).



> 


- DragonFly \s-1BSD\s0
Item "DragonFly BSD"

> 0

- \(bu
.PD
Fix thread library selection [perl #69686]



> 


- FreeBSD
Item "FreeBSD"

> 0

- \(bu
.PD
The hints files now identify the correct threading libraries on FreeBSD 7
and later.



> 


- Irix
Item "Irix"

> 0

- \(bu
.PD
We now work around a bizarre preprocessor bug in the Irix 6.5 compiler:
\f(CW\*(C`cc -E -\*(C' unfortunately goes into K&R mode, but \f(CW\*(C`cc -E file.c\*(C' doesn't.



> 


- NetBSD
Item "NetBSD"

> 0

- \(bu
.PD
Hints now supports versions 5.*.



> 


- OpenVMS
Item "OpenVMS"

> 0

- \(bu
.PD
\f(CW\*(C`-UDEBUGGING\*(C' is now the default on \s-1VMS.\s0
.Sp
Like it has been everywhere else for ages and ages. Also make command-line
selection of -UDEBUGGING and -DDEBUGGING work in configure.com; before
the only way to turn it off was by saying no in answer to the interactive
question.

- \(bu
The default pipe buffer size on \s-1VMS\s0 has been updated to 8192 on 64-bit
systems.

- \(bu
Reads from the in-memory temporary files of \f(CW\*(C`PerlIO::scalar\*(C' used to fail
if \f(CW$/ was set to a numeric reference (to indicate record-style reads).
This is now fixed.

- \(bu
\s-1VMS\s0 now supports \f(CW\*(C`getgrgid\*(C'.

- \(bu
Many improvements and cleanups have been made to the \s-1VMS\s0 file name handling
and conversion code.

- \(bu
Enabling the \f(CW\*(C`PERL_VMS_POSIX_EXIT\*(C' logical name now encodes a \s-1POSIX\s0 exit
status in a \s-1VMS\s0 condition value for better interaction with \s-1GNV\s0's bash
shell and other utilities that depend on \s-1POSIX\s0 exit values. See
\*(L"$?\*(R" in perlvms for details.

- \(bu
\f(CW\*(C`File::Copy\*(C' now detects Unix compatibility mode on \s-1VMS.\s0



> 


- Stratus \s-1VOS\s0
Item "Stratus VOS"

> 0

- \(bu
.PD
Various changes from Stratus have been merged in.



> 


- Symbian
Item "Symbian"

> 0

- \(bu
.PD
There is now support for Symbian S60 3.2 \s-1SDK\s0 and S60 5.0 \s-1SDK.\s0



> 


- Windows
Item "Windows"

> 0

- \(bu
.PD
Perl 5.12 supports Windows 2000 and later. The supporting code for
legacy versions of Windows is still included, but will be removed
during the next development cycle.

- \(bu
Initial support for building Perl with MinGW-w64 is now available.

- \(bu
*perl.exe* now includes a manifest resource to specify the \f(CW\*(C`trustInfo\*(C'
settings for Windows Vista and later. Without this setting Windows
would treat *perl.exe* as a legacy application and apply various
heuristics like redirecting access to protected file system areas
(like the \*(L"Program Files\*(R" folder) to the users \*(L"VirtualStore\*(R"
instead of generating a proper \*(L"permission denied\*(R" error.
.Sp
The manifest resource also requests the Microsoft Common-Controls
version 6.0 (themed controls introduced in Windows \s-1XP\s0).  Check out the
Win32::VisualStyles module on \s-1CPAN\s0 to switch back to old style
unthemed controls for legacy applications.

- \(bu
The \f(CW\*(C`-t\*(C' filetest operator now only returns true if the filehandle
is connected to a console window.  In previous versions of Perl it
would return true for all character mode devices, including *\s-1NUL\s0*
and *\s-1LPT1\s0*.

- \(bu
The \f(CW\*(C`-p\*(C' filetest operator now works correctly, and the
Fcntl::S_IFIFO constant is defined when Perl is compiled with
Microsoft Visual C.  In previous Perl versions \f(CW\*(C`-p\*(C' always
returned a false value, and the Fcntl::S_IFIFO constant
was not defined.
.Sp
This bug is specific to Microsoft Visual C and never affected
Perl binaries built with MinGW.

- \(bu
The socket error codes are now more widely supported:  The \s-1POSIX\s0
module will define the symbolic names, like \s-1POSIX::EWOULDBLOCK,\s0
and stringification of socket error codes in $! works as well
now;
.Sp
.Vb 2
  C:\\>perl -MPOSIX -E "$!=POSIX::EWOULDBLOCK; say $!"
  A non-blocking socket operation could not be completed immediately.
.Ve

- \(bu
**flock()** will now set sensible error codes in $!.  Previous Perl versions
copied the value of $^E into $!, which caused much confusion.

- \(bu
**select()** now supports all empty \f(CW\*(C`fd_set\*(C's more correctly.

- \(bu
\f(CW\*(Aq.\\foo\*(Aq and \f(CW\*(Aq..\\foo\*(Aq  were treated differently than
\f(CW\*(Aq./foo\*(Aq and \f(CW\*(Aq../foo\*(Aq by \f(CW\*(C`do\*(C' and \f(CW\*(C`require\*(C' [\s-1RT\s0 #63492].

- \(bu
Improved message window handling means that \f(CW\*(C`alarm\*(C' and \f(CW\*(C`kill\*(C' messages
will no longer be dropped under race conditions.

- \(bu
Various bits of Perl's build infrastructure are no longer converted to
win32 line endings at release time. If this hurts you, please report the
problem with the perlbug program included with perl.



> 


## Known Problems

Header "Known Problems"
This is a list of some significant unfixed bugs, which are regressions
from either 5.10.x or 5.8.x.

- \(bu
Some \s-1CPANPLUS\s0 tests may fail if there is a functioning file
*../../cpanp-run-perl* outside your build directory. The failure
shouldn't imply there's a problem with the actual functional
software. The bug is already fixed in [\s-1RT\s0 #74188] and is scheduled for
inclusion in perl-v5.12.1.

- \(bu
\f(CW\*(C`List::Util::first\*(C' misbehaves in the presence of a lexical \f(CW$_
(typically introduced by \f(CW\*(C`my $_\*(C' or implicitly by \f(CW\*(C`given\*(C'). The variable
which gets set for each iteration is the package variable \f(CW$_, not the
lexical \f(CW$_ [\s-1RT\s0 #67694].
.Sp
A similar issue may occur in other modules that provide functions which
take a block as their first argument, like
.Sp
.Vb 1
    foo \{ ... $_ ...\} list
.Ve

- \(bu
Some regexes may run much more slowly when run in a child thread compared
with the thread the pattern was compiled into [\s-1RT\s0 #55600].

- \(bu
Things like \f(CW\*(C`"\\N\{LATIN SMALL LIGATURE FF\}" =~ /\\N\{LATIN SMALL LETTER F\}+/\*(C'
will appear to hang as they get into a very long running loop [\s-1RT\s0 #72998].

- \(bu
Several porters have reported mysterious crashes when Perl's entire
test suite is run after a build on certain Windows 2000 systems. When
run by hand, the individual tests reportedly work fine.

## Errata

Header "Errata"

- \(bu
This one is actually a change introduced in 5.10.0, but it was missed
from that release's perldelta, so it is mentioned here instead.
.Sp
A bugfix related to the handling of the \f(CW\*(C`/m\*(C' modifier and \f(CW\*(C`qr\*(C' resulted
in a change of behaviour between 5.8.x and 5.10.0:
.Sp
.Vb 2
    # matches in 5.8.x, doesn\*(Aqt match in 5.10.0
    $re = qr/^bar/; "foo\\nbar" =~ /$re/m;
.Ve

## Acknowledgements

Header "Acknowledgements"
Perl 5.12.0 represents approximately two years of development since
Perl 5.10.0 and contains over 750,000 lines of changes across over
3,000 files from over 200 authors and committers.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers.  The following people are known to
have contributed the improvements that became Perl 5.12.0:

Aaron Crane, Abe Timmerman, Abhijit Menon-Sen, Abigail, Adam Russell,
Adriano Ferreira, \*(Aevar Arnfjo\*:r\*(d- Bjarmason, Alan Grover, Alexandr
Ciornii, Alex Davies, Alex Vandiver, Andreas Koenig, Andrew Rodland,
andrew@sundale.net, Andy Armstrong, Andy Dougherty, Jose AUGUSTE-ETIENNE,
Benjamin Smith, Ben Morrow, bharanee rathna, Bo Borgerson, Bo Lindbergh,
Brad Gilbert, Bram, Brendan O'Dea, brian d foy, Charles Bailey,
Chip Salzenberg, Chris 'BinGOs' Williams, Christoph Lamprecht, Chris
Williams, chromatic, Claes Jakobsson, Craig A. Berry, Dan Dascalescu,
Daniel Frederick Crisman, Daniel M. Quinlan, Dan Jacobson, Dan Kogai,
Dave Mitchell, Dave Rolsky, David Cantrell, David Dick, David Golden,
David Mitchell, David M. Syzdek, David Nicol, David Wheeler, Dennis
Kaarsemaker, Dintelmann, Peter, Dominic Dunlop, Dr.Ruud, Duke Leto,
Enrico Sorcinelli, Eric Brine, Father Chrysostomos, Florian Ragwitz,
Frank Wiegand, Gabor Szabo, Gene Sullivan, Geoffrey T. Dairiki, George
Greer, Gerard Goossen, Gisle Aas, Goro Fuji, Graham Barr, Green, Paul,
Hans Dieter Pearcey, Harmen, H. Merijn Brand, Hugo van der Sanden,
Ian Goodacre, Igor Sutton, Ingo Weinhold, James Bence, James Mastros,
Jan Dubois, Jari Aalto, Jarkko Hietaniemi, Jay Hannah, Jerry Hedden,
Jesse Vincent, Jim Cromie, Jody Belka, John E. Malmberg, John Malmberg,
John Peacock, John Peacock via \s-1RT,\s0 John P. Linderman, John Wright,
Josh ben Jore, Jos I. Boumans, Karl Williamson, Kenichi Ishigaki, Ken
Williams, Kevin Brintnall, Kevin Ryde, Kurt Starsinic, Leon Brocard,
Lubomir Rintel, Luke Ross, Marcel Gru\*:nauer, Marcus Holland-Moritz, Mark
Jason Dominus, Marko Asplund, Martin Hasch, Mashrab Kuvatov, Matt Kraai,
Matt S Trout, Max Maischein, Michael Breen, Michael Cartmell, Michael
G Schwern, Michael Witten, Mike Giroux, Milosz Tanski, Moritz Lenz,
Nicholas Clark, Nick Cleaton, Niko Tyni, Offer Kaye, Osvaldo Villalon,
Paul Fenwick, Paul Gaborit, Paul Green, Paul Johnson, Paul Marquess,
Philip Hazel, Philippe Bruhat, Rafael Garcia-Suarez, Rainer Tammer,
Rajesh Mandalemula, Reini Urban, Rene\*'e Ba\*:cker, Ricardo Signes,
Ricardo \s-1SIGNES,\s0 Richard Foley, Rich Rauenzahn, Rick Delaney, Risto
Kankkunen, Robert May, Roberto C. Sanchez, Robin Barker, \s-1SADAHIRO\s0
Tomoyuki, Salvador Ortiz Garcia, Sam Vilain, Scott Lanning, Se\*'bastien
Aperghis-Tramoni, Se\*'rgio Durigan Ju\*'nior, Shlomi Fish, Simon 'corecode'
Schubert, Sisyphus, Slaven Rezic, Smylers, Steffen Mu\*:ller, Steffen
Ullrich, Stepan Kasal, Steve Hay, Steven Schubiger, Steve Peters, Tels,
The Doctor, Tim Bunce, Tim Jenness, Todd Rinaldo, Tom Christiansen,
Tom Hukins, Tom Wyant, Tony Cook, Torsten Schoenfeld, Tye McQueen,
Vadim Konovalov, Vincent Pit, Hio \s-1YAMASHINA,\s0 Yasuhiro Matsumoto,
Yitzchak Scott-Thoennes, Yuval Kogman, Yves Orton, Zefram, Zsban Ambrus

This is woefully incomplete as it's automatically generated from version
control history.  In particular, it doesn't include the names of the
(very much appreciated) contributors who reported issues in previous
versions of Perl that helped make Perl 5.12.0 better. For a more complete
list of all of Perl's historical contributors, please see the \f(CW\*(C`AUTHORS\*(C'
file in the Perl 5.12.0 distribution.

Our \*(L"retired\*(R" pumpkings Nicholas Clark and Rafael Garcia-Suarez
deserve special thanks for their brilliant and substantive ongoing
contributions. Nicholas personally authored over 30% of the patches
since 5.10.0. Rafael comes in second in patch authorship with 11%,
but is first by a long shot in committing patches authored by others,
pushing 44% of the commits since 5.10.0 in this category, often after
providing considerable coaching to the patch authors. These statistics
in no way comprise all of their contributions, but express in shorthand
that we couldn't have done it without them.

Many of the changes included in this version originated in the \s-1CPAN\s0
modules included in Perl's core. We're grateful to the entire \s-1CPAN\s0
community for helping Perl to flourish.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at <http://rt.perl.org/perlbug/>. There may also be
information at <http://www.perl.org/>, the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release. Be sure to trim your bug down
to a tiny but sufficient test case. Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analyzed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes
all the core committers, who will be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for
security issues in the Perl core, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.

<http://dev.perl.org/perl5/errata.html> for a list of issues
found after this release, as well as a list of \s-1CPAN\s0 modules known
to be incompatible with this release.
