+++
detected_package_version = "5.34.1"
manpage_format = "troff"
manpage_section = "1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
operating_system = "macos"
manpage_name = "perl5184delta"
operating_system_version = "15.3"
title = "perl5184delta(1)"
description = "This document describes differences between the 5.18.4 release and the 5.18.2 release.  Please note:  This document ignores perl 5.18.3, a broken release which existed for a few hours only. If you are upgrading from an earlier release such as 5.18...."
date = "2022-02-19"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5184DELTA 1"
PERL5184DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5184delta - what is new for perl v5.18.4

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.18.4 release and the 5.18.2
release.  **Please note:**  This document ignores perl 5.18.3, a broken release
which existed for a few hours only.

If you are upgrading from an earlier release such as 5.18.1, first read
perl5182delta, which describes differences between 5.18.1 and 5.18.2.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Digest::SHA has been upgraded from 5.84_01 to 5.84_02.

- \(bu
perl5db.pl has been upgraded from version 1.39_10 to 1.39_11.
.Sp
This fixes a crash in tab completion, where available. [perl #120827]  Also,
filehandle information is properly reset after a pager is run. [perl #121456]

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- Win32
Item "Win32"

> 0

- \(bu
.PD
Introduced by
[\s-1GH\s0 #12161] <https://github.com/Perl/perl5/issues/12161>, a memory
leak on every call to \f(CW\*(C`system\*(C' and backticks (\f(CW\*(C` \`\` \*(C'), on most Win32 Perls
starting from 5.18.0 has been fixed.  The memory leak only occurred if you
enabled pseudo-fork in your build of Win32 Perl, and were running that build on
Server 2003 R2 or newer \s-1OS.\s0  The leak does not appear on WinXP \s-1SP3.\s0
[\s-1GH\s0 #13741] <https://github.com/Perl/perl5/issues/13741>



> 


## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
The debugger now properly resets filehandles as needed. [perl #121456]

- \(bu
A segfault in Digest::SHA has been addressed.  [perl #121421]

- \(bu
perl can again be built with \s-1USE_64_BIT_INT,\s0 with Visual C 2003, 32 bit.
[perl #120925]

- \(bu
A leading \{ (brace) in formats is properly parsed again. [perl #119973]

- \(bu
Copy the values used to perturb hash iteration when cloning an
interpreter.  This was fairly harmless but caused \f(CW\*(C`valgrind\*(C' to
complain. [perl #121336]

- \(bu
In Perl v5.18 \f(CW\*(C`undef *_; goto &sub\*(C' and \f(CW\*(C`local *_; goto &sub\*(C' started
crashing.  This has been fixed. [perl #119949]

## Acknowledgements

Header "Acknowledgements"
Perl 5.18.4 represents approximately 9 months of development since Perl 5.18.2
and contains approximately 2,000 lines of changes across 53 files from 13
authors.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers. The following people are known to have contributed the
improvements that became Perl 5.18.4:

Daniel Dragan, David Mitchell, Doug Bell, Father Chrysostomos, Hiroo Hayashi,
James E Keenan, Karl Williamson, Mark Shelor, Ricardo Signes, Shlomi Fish,
Smylers, Steve Hay, Tony Cook.

The list above is almost certainly incomplete as it is automatically generated
from version control history. In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core. We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the perl bug database at
http://rt.perl.org/perlbug/ .  There may also be information at
http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send it
to perl5-security-report@perl.org.  This points to a closed subscription
unarchived mailing list, which includes all the core committers, who will be
able to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported.  Please only use this address for
security issues in the Perl core, not for modules independently distributed on
\s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
