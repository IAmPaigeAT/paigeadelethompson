+++
manpage_name = "perlootut"
title = "perlootut(1)"
date = "2022-02-19"
description = "This document provides an introduction to object-oriented programming in Perl. It begins with a brief overview of the concepts behind object oriented design. Then it introduces several different s-1OOs0 systems from s-1CPANs0 <https://www.cpan.org>..."
author = "None Specified"
operating_system = "macos"
manpage_section = "1"
operating_system_version = "15.3"
detected_package_version = "5.34.1"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLOOTUT 1"
PERLOOTUT 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlootut - Object-Oriented Programming in Perl Tutorial

## DATE

Header "DATE"
This document was created in February, 2011, and the last major
revision was in February, 2013.

If you are reading this in the future then it's possible that the state
of the art has changed. We recommend you start by reading the perlootut
document in the latest stable release of Perl, rather than this
version.

## DESCRIPTION

Header "DESCRIPTION"
This document provides an introduction to object-oriented programming
in Perl. It begins with a brief overview of the concepts behind object
oriented design. Then it introduces several different \s-1OO\s0 systems from
\s-1CPAN\s0 <https://www.cpan.org> which build on top of what Perl
provides.

By default, Perl's built-in \s-1OO\s0 system is very minimal, leaving you to
do most of the work. This minimalism made a lot of sense in 1994, but
in the years since Perl 5.0 we've seen a number of common patterns
emerge in Perl \s-1OO.\s0 Fortunately, Perl's flexibility has allowed a rich
ecosystem of Perl \s-1OO\s0 systems to flourish.

If you want to know how Perl \s-1OO\s0 works under the hood, the perlobj
document explains the nitty gritty details.

This document assumes that you already understand the basics of Perl
syntax, variable types, operators, and subroutine calls. If you don't
understand these concepts yet, please read perlintro first. You
should also read the perlsyn, perlop, and perlsub documents.

## OBJECT-ORIENTED FUNDAMENTALS

Header "OBJECT-ORIENTED FUNDAMENTALS"
Most object systems share a number of common concepts. You've probably
heard terms like \*(L"class\*(R", \*(L"object, \*(R"method\*(L", and \*(R"attribute" before.
Understanding the concepts will make it much easier to read and write
object-oriented code. If you're already familiar with these terms, you
should still skim this section, since it explains each concept in terms
of Perl's \s-1OO\s0 implementation.

Perl's \s-1OO\s0 system is class-based. Class-based \s-1OO\s0 is fairly common. It's
used by Java, \*(C+, C#, Python, Ruby, and many other languages. There
are other object orientation paradigms as well. JavaScript is the most
popular language to use another paradigm. JavaScript's \s-1OO\s0 system is
prototype-based.

### Object

Subsection "Object"
An **object** is a data structure that bundles together data and
subroutines which operate on that data. An object's data is called
**attributes**, and its subroutines are called **methods**. An object can
be thought of as a noun (a person, a web service, a computer).

An object represents a single discrete thing. For example, an object
might represent a file. The attributes for a file object might include
its path, content, and last modification time. If we created an object
to represent */etc/hostname* on a machine named \*(L"foo.example.com\*(R",
that object's path would be \*(L"/etc/hostname\*(R", its content would be
\*(L"foo\\n\*(R", and it's last modification time would be 1304974868 seconds
since the beginning of the epoch.

The methods associated with a file might include \f(CW\*(C`rename()\*(C' and
\f(CW\*(C`write()\*(C'.

In Perl most objects are hashes, but the \s-1OO\s0 systems we recommend keep
you from having to worry about this. In practice, it's best to consider
an object's internal data structure opaque.

### Class

Subsection "Class"
A **class** defines the behavior of a category of objects. A class is a
name for a category (like \*(L"File\*(R"), and a class also defines the
behavior of objects in that category.

All objects belong to a specific class. For example, our
*/etc/hostname* object belongs to the \f(CW\*(C`File\*(C' class. When we want to
create a specific object, we start with its class, and **construct** or
**instantiate** an object. A specific object is often referred to as an
**instance** of a class.

In Perl, any package can be a class. The difference between a package
which is a class and one which isn't is based on how the package is
used. Here's our \*(L"class declaration\*(R" for the \f(CW\*(C`File\*(C' class:

.Vb 1
  package File;
.Ve

In Perl, there is no special keyword for constructing an object.
However, most \s-1OO\s0 modules on \s-1CPAN\s0 use a method named \f(CW\*(C`new()\*(C' to
construct a new object:

.Vb 5
  my $hostname = File->new(
      path          => \*(Aq/etc/hostname\*(Aq,
      content       => "foo\\n",
      last_mod_time => 1304974868,
  );
.Ve

(Don't worry about that \f(CW\*(C`->\*(C' operator, it will be explained
later.)

*Blessing*
Subsection "Blessing"

As we said earlier, most Perl objects are hashes, but an object can be
an instance of any Perl data type (scalar, array, etc.). Turning a
plain data structure into an object is done by **blessing** that data
structure using Perl's \f(CW\*(C`bless\*(C' function.

While we strongly suggest you don't build your objects from scratch,
you should know the term **bless**. A **blessed** data structure (aka \*(L"a
referent\*(R") is an object. We sometimes say that an object has been
\*(L"blessed into a class\*(R".

Once a referent has been blessed, the \f(CW\*(C`blessed\*(C' function from the
Scalar::Util core module can tell us its class name. This subroutine
returns an object's class when passed an object, and false otherwise.

.Vb 1
  use Scalar::Util \*(Aqblessed\*(Aq;

  print blessed($hash);      # undef
  print blessed($hostname);  # File
.Ve

*Constructor*
Subsection "Constructor"

A **constructor** creates a new object. In Perl, a class's constructor
is just another method, unlike some other languages, which provide
syntax for constructors. Most Perl classes use \f(CW\*(C`new\*(C' as the name for
their constructor:

.Vb 1
  my $file = File->new(...);
.Ve

### Methods

Subsection "Methods"
You already learned that a **method** is a subroutine that operates on
an object. You can think of a method as the things that an object can
*do*. If an object is a noun, then methods are its verbs (save, print,
open).

In Perl, methods are simply subroutines that live in a class's package.
Methods are always written to receive the object as their first
argument:

.Vb 2
  sub print_info \{
      my $self = shift;

      print "This file is at ", $self->path, "\\n";
  \}

  $file->print_info;
  # The file is at /etc/hostname
.Ve

What makes a method special is *how it's called*. The arrow operator
(\f(CW\*(C`->\*(C') tells Perl that we are calling a method.

When we make a method call, Perl arranges for the method's **invocant**
to be passed as the first argument. **Invocant** is a fancy name for the
thing on the left side of the arrow. The invocant can either be a class
name or an object. We can also pass additional arguments to the method:

.Vb 3
  sub print_info \{
      my $self   = shift;
      my $prefix = shift // "This file is at ";

      print $prefix, ", ", $self->path, "\\n";
  \}

  $file->print_info("The file is located at ");
  # The file is located at /etc/hostname
.Ve

### Attributes

Subsection "Attributes"
Each class can define its **attributes**. When we instantiate an object,
we assign values to those attributes. For example, every \f(CW\*(C`File\*(C' object
has a path. Attributes are sometimes called **properties**.

Perl has no special syntax for attributes. Under the hood, attributes
are often stored as keys in the object's underlying hash, but don't
worry about this.

We recommend that you only access attributes via **accessor** methods.
These are methods that can get or set the value of each attribute. We
saw this earlier in the \f(CW\*(C`print_info()\*(C' example, which calls \f(CW\*(C`$self->path\*(C'.

You might also see the terms **getter** and **setter**. These are two
types of accessors. A getter gets the attribute's value, while a setter
sets it. Another term for a setter is **mutator**

Attributes are typically defined as read-only or read-write. Read-only
attributes can only be set when the object is first created, while
read-write attributes can be altered at any time.

The value of an attribute may itself be another object. For example,
instead of returning its last mod time as a number, the \f(CW\*(C`File\*(C' class
could return a DateTime object representing that value.

It's possible to have a class that does not expose any publicly
settable attributes. Not every class has attributes and methods.

### Polymorphism

Subsection "Polymorphism"
**Polymorphism** is a fancy way of saying that objects from two
different classes share an \s-1API.\s0 For example, we could have \f(CW\*(C`File\*(C' and
\f(CW\*(C`WebPage\*(C' classes which both have a \f(CW\*(C`print_content()\*(C' method. This
method might produce different output for each class, but they share a
common interface.

While the two classes may differ in many ways, when it comes to the
\f(CW\*(C`print_content()\*(C' method, they are the same. This means that we can
try to call the \f(CW\*(C`print_content()\*(C' method on an object of either class,
and **we don't have to know what class the object belongs to!**

Polymorphism is one of the key concepts of object-oriented design.

### Inheritance

Subsection "Inheritance"
**Inheritance** lets you create a specialized version of an existing
class. Inheritance lets the new class reuse the methods and attributes
of another class.

For example, we could create an \f(CW\*(C`File::MP3\*(C' class which **inherits**
from \f(CW\*(C`File\*(C'. An \f(CW\*(C`File::MP3\*(C' **is-a** *more specific* type of \f(CW\*(C`File\*(C'.
All mp3 files are files, but not all files are mp3 files.

We often refer to inheritance relationships as **parent-child** or
\f(CW\*(C`superclass\*(C'/\f(CW\*(C`subclass\*(C' relationships. Sometimes we say that the
child has an **is-a** relationship with its parent class.

\f(CW\*(C`File\*(C' is a **superclass** of \f(CW\*(C`File::MP3\*(C', and \f(CW\*(C`File::MP3\*(C' is a
**subclass** of \f(CW\*(C`File\*(C'.

.Vb 1
  package File::MP3;

  use parent \*(AqFile\*(Aq;
.Ve

The parent module is one of several ways that Perl lets you define
inheritance relationships.

Perl allows multiple inheritance, which means that a class can inherit
from multiple parents. While this is possible, we strongly recommend
against it. Generally, you can use **roles** to do everything you can do
with multiple inheritance, but in a cleaner way.

Note that there's nothing wrong with defining multiple subclasses of a
given class. This is both common and safe. For example, we might define
\f(CW\*(C`File::MP3::FixedBitrate\*(C' and \f(CW\*(C`File::MP3::VariableBitrate\*(C' classes to
distinguish between different types of mp3 file.

*Overriding methods and method resolution*
Subsection "Overriding methods and method resolution"

Inheritance allows two classes to share code. By default, every method
in the parent class is also available in the child. The child can
explicitly **override** a parent's method to provide its own
implementation. For example, if we have an \f(CW\*(C`File::MP3\*(C' object, it has
the \f(CW\*(C`print_info()\*(C' method from \f(CW\*(C`File\*(C':

.Vb 6
  my $cage = File::MP3->new(
      path          => \*(Aqmp3s/My-Body-Is-a-Cage.mp3\*(Aq,
      content       => $mp3_data,
      last_mod_time => 1304974868,
      title         => \*(AqMy Body Is a Cage\*(Aq,
  );

  $cage->print_info;
  # The file is at mp3s/My-Body-Is-a-Cage.mp3
.Ve

If we wanted to include the mp3's title in the greeting, we could
override the method:

.Vb 1
  package File::MP3;

  use parent \*(AqFile\*(Aq;

  sub print_info \{
      my $self = shift;

      print "This file is at ", $self->path, "\\n";
      print "Its title is ", $self->title, "\\n";
  \}

  $cage->print_info;
  # The file is at mp3s/My-Body-Is-a-Cage.mp3
  # Its title is My Body Is a Cage
.Ve

The process of determining what method should be used is called
**method resolution**. What Perl does is look at the object's class
first (\f(CW\*(C`File::MP3\*(C' in this case). If that class defines the method,
then that class's version of the method is called. If not, Perl looks
at each parent class in turn. For \f(CW\*(C`File::MP3\*(C', its only parent is
\f(CW\*(C`File\*(C'. If \f(CW\*(C`File::MP3\*(C' does not define the method, but \f(CW\*(C`File\*(C' does,
then Perl calls the method in \f(CW\*(C`File\*(C'.

If \f(CW\*(C`File\*(C' inherited from \f(CW\*(C`DataSource\*(C', which inherited from \f(CW\*(C`Thing\*(C',
then Perl would keep looking \*(L"up the chain\*(R" if necessary.

It is possible to explicitly call a parent method from a child:

.Vb 1
  package File::MP3;

  use parent \*(AqFile\*(Aq;

  sub print_info \{
      my $self = shift;

      $self->SUPER::print_info();
      print "Its title is ", $self->title, "\\n";
  \}
.Ve

The \f(CW\*(C`SUPER::\*(C' bit tells Perl to look for the \f(CW\*(C`print_info()\*(C' in the
\f(CW\*(C`File::MP3\*(C' class's inheritance chain. When it finds the parent class
that implements this method, the method is called.

We mentioned multiple inheritance earlier. The main problem with
multiple inheritance is that it greatly complicates method resolution.
See perlobj for more details.

### Encapsulation

Subsection "Encapsulation"
**Encapsulation** is the idea that an object is opaque. When another
developer uses your class, they don't need to know *how* it is
implemented, they just need to know *what* it does.

Encapsulation is important for several reasons. First, it allows you to
separate the public \s-1API\s0 from the private implementation. This means you
can change that implementation without breaking the \s-1API.\s0

Second, when classes are well encapsulated, they become easier to
subclass. Ideally, a subclass uses the same APIs to access object data
that its parent class uses. In reality, subclassing sometimes involves
violating encapsulation, but a good \s-1API\s0 can minimize the need to do
this.

We mentioned earlier that most Perl objects are implemented as hashes
under the hood. The principle of encapsulation tells us that we should
not rely on this. Instead, we should use accessor methods to access the
data in that hash. The object systems that we recommend below all
automate the generation of accessor methods. If you use one of them,
you should never have to access the object as a hash directly.

### Composition

Subsection "Composition"
In object-oriented code, we often find that one object references
another object. This is called **composition**, or a **has-a**
relationship.

Earlier, we mentioned that the \f(CW\*(C`File\*(C' class's \f(CW\*(C`last_mod_time\*(C'
accessor could return a DateTime object. This is a perfect example
of composition. We could go even further, and make the \f(CW\*(C`path\*(C' and
\f(CW\*(C`content\*(C' accessors return objects as well. The \f(CW\*(C`File\*(C' class would
then be **composed** of several other objects.

### Roles

Subsection "Roles"
**Roles** are something that a class *does*, rather than something that
it *is*. Roles are relatively new to Perl, but have become rather
popular. Roles are **applied** to classes. Sometimes we say that classes
**consume** roles.

Roles are an alternative to inheritance for providing polymorphism.
Let's assume we have two classes, \f(CW\*(C`Radio\*(C' and \f(CW\*(C`Computer\*(C'. Both of
these things have on/off switches. We want to model that in our class
definitions.

We could have both classes inherit from a common parent, like
\f(CW\*(C`Machine\*(C', but not all machines have on/off switches. We could create
a parent class called \f(CW\*(C`HasOnOffSwitch\*(C', but that is very artificial.
Radios and computers are not specializations of this parent. This
parent is really a rather ridiculous creation.

This is where roles come in. It makes a lot of sense to create a
\f(CW\*(C`HasOnOffSwitch\*(C' role and apply it to both classes. This role would
define a known \s-1API\s0 like providing \f(CW\*(C`turn_on()\*(C' and \f(CW\*(C`turn_off()\*(C'
methods.

Perl does not have any built-in way to express roles. In the past,
people just bit the bullet and used multiple inheritance. Nowadays,
there are several good choices on \s-1CPAN\s0 for using roles.

### When to Use \s-1OO\s0

Subsection "When to Use OO"
Object Orientation is not the best solution to every problem. In \fIPerl
Best Practices (copyright 2004, Published by O'Reilly Media, Inc.),
Damian Conway provides a list of criteria to use when deciding if \s-1OO\s0 is
the right fit for your problem:

- \(bu
The system being designed is large, or is likely to become large.

- \(bu
The data can be aggregated into obvious structures, especially if
there's a large amount of data in each aggregate.

- \(bu
The various types of data aggregate form a natural hierarchy that
facilitates the use of inheritance and polymorphism.

- \(bu
You have a piece of data on which many different operations are
applied.

- \(bu
You need to perform the same general operations on related types of
data, but with slight variations depending on the specific type of data
the operations are applied to.

- \(bu
It's likely you'll have to add new data types later.

- \(bu
The typical interactions between pieces of data are best represented by
operators.

- \(bu
The implementation of individual components of the system is likely to
change over time.

- \(bu
The system design is already object-oriented.

- \(bu
Large numbers of other programmers will be using your code modules.

## PERL OO SYSTEMS

Header "PERL OO SYSTEMS"
As we mentioned before, Perl's built-in \s-1OO\s0 system is very minimal, but
also quite flexible. Over the years, many people have developed systems
which build on top of Perl's built-in system to provide more features
and convenience.

We strongly recommend that you use one of these systems. Even the most
minimal of them eliminates a lot of repetitive boilerplate. There's
really no good reason to write your classes from scratch in Perl.

If you are interested in the guts underlying these systems, check out
perlobj.

### Moose

Subsection "Moose"
Moose bills itself as a \*(L"postmodern object system for Perl 5\*(R". Don't
be scared, the \*(L"postmodern\*(R" label is a callback to Larry's description
of Perl as \*(L"the first postmodern computer language\*(R".

\f(CW\*(C`Moose\*(C' provides a complete, modern \s-1OO\s0 system. Its biggest influence
is the Common Lisp Object System, but it also borrows ideas from
Smalltalk and several other languages. \f(CW\*(C`Moose\*(C' was created by Stevan
Little, and draws heavily from his work on the Raku \s-1OO\s0 design.

Here is our \f(CW\*(C`File\*(C' class using \f(CW\*(C`Moose\*(C':

.Vb 2
  package File;
  use Moose;

  has path          => ( is => \*(Aqro\*(Aq );
  has content       => ( is => \*(Aqro\*(Aq );
  has last_mod_time => ( is => \*(Aqro\*(Aq );

  sub print_info \{
      my $self = shift;

      print "This file is at ", $self->path, "\\n";
  \}
.Ve

\f(CW\*(C`Moose\*(C' provides a number of features:

- \(bu
Declarative sugar
.Sp
\f(CW\*(C`Moose\*(C' provides a layer of declarative \*(L"sugar\*(R" for defining classes.
That sugar is just a set of exported functions that make declaring how
your class works simpler and more palatable.  This lets you describe
*what* your class is, rather than having to tell Perl *how* to
implement your class.
.Sp
The \f(CW\*(C`has()\*(C' subroutine declares an attribute, and \f(CW\*(C`Moose\*(C'
automatically creates accessors for these attributes. It also takes
care of creating a \f(CW\*(C`new()\*(C' method for you. This constructor knows
about the attributes you declared, so you can set them when creating a
new \f(CW\*(C`File\*(C'.

- \(bu
Roles built-in
.Sp
\f(CW\*(C`Moose\*(C' lets you define roles the same way you define classes:
.Sp
.Vb 2
  package HasOnOffSwitch;
  use Moose::Role;

  has is_on => (
      is  => \*(Aqrw\*(Aq,
      isa => \*(AqBool\*(Aq,
  );

  sub turn_on \{
      my $self = shift;
      $self->is_on(1);
  \}

  sub turn_off \{
      my $self = shift;
      $self->is_on(0);
  \}
.Ve

- \(bu
A miniature type system
.Sp
In the example above, you can see that we passed \f(CW\*(C`isa => \*(AqBool\*(Aq\*(C'
to \f(CW\*(C`has()\*(C' when creating our \f(CW\*(C`is_on\*(C' attribute. This tells \f(CW\*(C`Moose\*(C'
that this attribute must be a boolean value. If we try to set it to an
invalid value, our code will throw an error.

- \(bu
Full introspection and manipulation
.Sp
Perl's built-in introspection features are fairly minimal. \f(CW\*(C`Moose\*(C'
builds on top of them and creates a full introspection layer for your
classes. This lets you ask questions like \*(L"what methods does the File
class implement?\*(R" It also lets you modify your classes
programmatically.

- \(bu
Self-hosted and extensible
.Sp
\f(CW\*(C`Moose\*(C' describes itself using its own introspection \s-1API.\s0 Besides
being a cool trick, this means that you can extend \f(CW\*(C`Moose\*(C' using
\f(CW\*(C`Moose\*(C' itself.

- \(bu
Rich ecosystem
.Sp
There is a rich ecosystem of \f(CW\*(C`Moose\*(C' extensions on \s-1CPAN\s0 under the
MooseX <https://metacpan.org/search?q=MooseX>
namespace. In addition, many modules on \s-1CPAN\s0 already use \f(CW\*(C`Moose\*(C',
providing you with lots of examples to learn from.

- \(bu
Many more features
.Sp
\f(CW\*(C`Moose\*(C' is a very powerful tool, and we can't cover all of its
features here. We encourage you to learn more by reading the \f(CW\*(C`Moose\*(C'
documentation, starting with
Moose::Manual <https://metacpan.org/pod/Moose::Manual>.

Of course, \f(CW\*(C`Moose\*(C' isn't perfect.

\f(CW\*(C`Moose\*(C' can make your code slower to load. \f(CW\*(C`Moose\*(C' itself is not
small, and it does a *lot* of code generation when you define your
class. This code generation means that your runtime code is as fast as
it can be, but you pay for this when your modules are first loaded.

This load time hit can be a problem when startup speed is important,
such as with a command-line script or a \*(L"plain vanilla\*(R" \s-1CGI\s0 script that
must be loaded each time it is executed.

Before you panic, know that many people do use \f(CW\*(C`Moose\*(C' for
command-line tools and other startup-sensitive code. We encourage you
to try \f(CW\*(C`Moose\*(C' out first before worrying about startup speed.

\f(CW\*(C`Moose\*(C' also has several dependencies on other modules. Most of these
are small stand-alone modules, a number of which have been spun off
from \f(CW\*(C`Moose\*(C'. \f(CW\*(C`Moose\*(C' itself, and some of its dependencies, require a
compiler. If you need to install your software on a system without a
compiler, or if having *any* dependencies is a problem, then \f(CW\*(C`Moose\*(C'
may not be right for you.

*Moo*
Subsection "Moo"

If you try \f(CW\*(C`Moose\*(C' and find that one of these issues is preventing you
from using \f(CW\*(C`Moose\*(C', we encourage you to consider Moo next. \f(CW\*(C`Moo\*(C'
implements a subset of \f(CW\*(C`Moose\*(C''s functionality in a simpler package.
For most features that it does implement, the end-user \s-1API\s0 is
*identical* to \f(CW\*(C`Moose\*(C', meaning you can switch from \f(CW\*(C`Moo\*(C' to
\f(CW\*(C`Moose\*(C' quite easily.

\f(CW\*(C`Moo\*(C' does not implement most of \f(CW\*(C`Moose\*(C''s introspection \s-1API,\s0 so it's
often faster when loading your modules. Additionally, none of its
dependencies require \s-1XS,\s0 so it can be installed on machines without a
compiler.

One of \f(CW\*(C`Moo\*(C''s most compelling features is its interoperability with
\f(CW\*(C`Moose\*(C'. When someone tries to use \f(CW\*(C`Moose\*(C''s introspection \s-1API\s0 on a
\f(CW\*(C`Moo\*(C' class or role, it is transparently inflated into a \f(CW\*(C`Moose\*(C'
class or role. This makes it easier to incorporate \f(CW\*(C`Moo\*(C'-using code
into a \f(CW\*(C`Moose\*(C' code base and vice versa.

For example, a \f(CW\*(C`Moose\*(C' class can subclass a \f(CW\*(C`Moo\*(C' class using
\f(CW\*(C`extends\*(C' or consume a \f(CW\*(C`Moo\*(C' role using \f(CW\*(C`with\*(C'.

The \f(CW\*(C`Moose\*(C' authors hope that one day \f(CW\*(C`Moo\*(C' can be made obsolete by
improving \f(CW\*(C`Moose\*(C' enough, but for now it provides a worthwhile
alternative to \f(CW\*(C`Moose\*(C'.

### Class::Accessor

Subsection "Class::Accessor"
Class::Accessor is the polar opposite of \f(CW\*(C`Moose\*(C'. It provides very
few features, nor is it self-hosting.

It is, however, very simple, pure Perl, and it has no non-core
dependencies. It also provides a \*(L"Moose-like\*(R" \s-1API\s0 on demand for the
features it supports.

Even though it doesn't do much, it is still preferable to writing your
own classes from scratch.

Here's our \f(CW\*(C`File\*(C' class with \f(CW\*(C`Class::Accessor\*(C':

.Vb 2
  package File;
  use Class::Accessor \*(Aqantlers\*(Aq;

  has path          => ( is => \*(Aqro\*(Aq );
  has content       => ( is => \*(Aqro\*(Aq );
  has last_mod_time => ( is => \*(Aqro\*(Aq );

  sub print_info \{
      my $self = shift;

      print "This file is at ", $self->path, "\\n";
  \}
.Ve

The \f(CW\*(C`antlers\*(C' import flag tells \f(CW\*(C`Class::Accessor\*(C' that you want to
define your attributes using \f(CW\*(C`Moose\*(C'-like syntax. The only parameter
that you can pass to \f(CW\*(C`has\*(C' is \f(CW\*(C`is\*(C'. We recommend that you use this
Moose-like syntax if you choose \f(CW\*(C`Class::Accessor\*(C' since it means you
will have a smoother upgrade path if you later decide to move to
\f(CW\*(C`Moose\*(C'.

Like \f(CW\*(C`Moose\*(C', \f(CW\*(C`Class::Accessor\*(C' generates accessor methods and a
constructor for your class.

### Class::Tiny

Subsection "Class::Tiny"
Finally, we have Class::Tiny. This module truly lives up to its
name. It has an incredibly minimal \s-1API\s0 and absolutely no dependencies
on any recent Perl. Still, we think it's a lot easier to use than
writing your own \s-1OO\s0 code from scratch.

Here's our \f(CW\*(C`File\*(C' class once more:

.Vb 2
  package File;
  use Class::Tiny qw( path content last_mod_time );

  sub print_info \{
      my $self = shift;

      print "This file is at ", $self->path, "\\n";
  \}
.Ve

That's it!

With \f(CW\*(C`Class::Tiny\*(C', all accessors are read-write. It generates a
constructor for you, as well as the accessors you define.

You can also use Class::Tiny::Antlers for \f(CW\*(C`Moose\*(C'-like syntax.

### Role::Tiny

Subsection "Role::Tiny"
As we mentioned before, roles provide an alternative to inheritance,
but Perl does not have any built-in role support. If you choose to use
Moose, it comes with a full-fledged role implementation. However, if
you use one of our other recommended \s-1OO\s0 modules, you can still use
roles with Role::Tiny

\f(CW\*(C`Role::Tiny\*(C' provides some of the same features as Moose's role
system, but in a much smaller package. Most notably, it doesn't support
any sort of attribute declaration, so you have to do that by hand.
Still, it's useful, and works well with \f(CW\*(C`Class::Accessor\*(C' and
\f(CW\*(C`Class::Tiny\*(C'

### \s-1OO\s0 System Summary

Subsection "OO System Summary"
Here's a brief recap of the options we covered:

- \(bu
Moose
.Sp
\f(CW\*(C`Moose\*(C' is the maximal option. It has a lot of features, a big
ecosystem, and a thriving user base. We also covered Moo briefly.
\f(CW\*(C`Moo\*(C' is \f(CW\*(C`Moose\*(C' lite, and a reasonable alternative when Moose
doesn't work for your application.

- \(bu
Class::Accessor
.Sp
\f(CW\*(C`Class::Accessor\*(C' does a lot less than \f(CW\*(C`Moose\*(C', and is a nice
alternative if you find \f(CW\*(C`Moose\*(C' overwhelming. It's been around a long
time and is well battle-tested. It also has a minimal \f(CW\*(C`Moose\*(C'
compatibility mode which makes moving from \f(CW\*(C`Class::Accessor\*(C' to
\f(CW\*(C`Moose\*(C' easy.

- \(bu
Class::Tiny
.Sp
\f(CW\*(C`Class::Tiny\*(C' is the absolute minimal option. It has no dependencies,
and almost no syntax to learn. It's a good option for a super minimal
environment and for throwing something together quickly without having
to worry about details.

- \(bu
Role::Tiny
.Sp
Use \f(CW\*(C`Role::Tiny\*(C' with \f(CW\*(C`Class::Accessor\*(C' or \f(CW\*(C`Class::Tiny\*(C' if you find
yourself considering multiple inheritance. If you go with \f(CW\*(C`Moose\*(C', it
comes with its own role implementation.

### Other \s-1OO\s0 Systems

Subsection "Other OO Systems"
There are literally dozens of other OO-related modules on \s-1CPAN\s0 besides
those covered here, and you're likely to run across one or more of them
if you work with other people's code.

In addition, plenty of code in the wild does all of its \s-1OO\s0 \*(L"by hand\*(R",
using just the Perl built-in \s-1OO\s0 features. If you need to maintain such
code, you should read perlobj to understand exactly how Perl's
built-in \s-1OO\s0 works.

## CONCLUSION

Header "CONCLUSION"
As we said before, Perl's minimal \s-1OO\s0 system has led to a profusion of
\s-1OO\s0 systems on \s-1CPAN.\s0 While you can still drop down to the bare metal and
write your classes by hand, there's really no reason to do that with
modern Perl.

For small systems, Class::Tiny and Class::Accessor both provide
minimal object systems that take care of basic boilerplate for you.

For bigger projects, Moose provides a rich set of features that will
let you focus on implementing your business logic. Moo provides a
nice alternative to Moose when you want a lot of features but need
faster compile time or to avoid \s-1XS.\s0

We encourage you to play with and evaluate Moose, Moo,
Class::Accessor, and Class::Tiny to see which \s-1OO\s0 system is right
for you.
