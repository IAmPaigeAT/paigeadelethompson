+++
detected_package_version = "2.0"
author = "None Specified"
manpage_name = "cancel"
description = "The cancel command cancels print jobs. If no destination or id is specified, the currently printing job on the default destination is canceled. The following options are recognized by cancel: Cancel all jobs on the named destination, or all jobs on..."
operating_system_version = "15.3"
title = "cancel(1)"
operating_system = "macos"
date = "Sun Feb 16 04:48:23 2025"
manpage_format = "troff"
keywords = ["cupsd", "conf", "5", "lp", "1", "lpmove", "8", "lpstat", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
manpage_section = "1"
+++

cancel 1 "CUPS" "26 April 2019" "Apple Inc."

## NAME

cancel - cancel jobs

## SYNOPSIS

cancel
[
-E
] [
-U
username
] [
-a
] [
-h
hostname[:port]
] [
-u
username
] [
-x
] [
id
] [
destination
] [
destination-id
]

## DESCRIPTION

The **cancel** command cancels print jobs.
If no *destination* or *id* is specified, the currently printing job on the default destination is canceled.

## OPTIONS

The following options are recognized by **cancel**:

-a
Cancel all jobs on the named destination, or all jobs on all
destinations if none is provided.

-E
Forces encryption when connecting to the server.

**-h **hostname[*:port*]
Specifies an alternate server.

**-U **username
Specifies the username to use when connecting to the server.

**-u **username
Cancels jobs owned by *username*.

-x
Deletes job data files in addition to canceling.

## CONFORMING TO

Unlike the System V printing system, CUPS allows printer names to contain any printable character except SPACE, TAB, "/", or "#". Also, printer and class names are *not* case-sensitive.

## EXAMPLES

Cancel the current print job:

```

    cancel

```

Cancel job "myprinter-42":

```

    cancel myprinter\-42

```

Cancel all jobs:

```

    cancel \-a
```


## NOTES

Administrators wishing to prevent unauthorized cancellation of jobs via the *-u* option should require authentication for Cancel-Jobs operations in
cupsd.conf (5).

## SEE ALSO

cupsd.conf (5),
lp (1),
lpmove (8),
lpstat (1),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
