+++
title = "binhex(1)"
manpage_name = "binhex"
operating_system = "macos"
description = "None Specified"
detected_package_version = "5.34.0"
date = "2015-11-15"
manpage_format = "troff"
operating_system_version = "15.3"
author = "None Specified"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "BINHEX 1"
BINHEX 1 "2015-11-15" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

binhex.pl - use Convert::BinHex to encode files as BinHex

## USAGE

Header "USAGE"
Usage:

.Vb 1
    binhex.pl [options] file ... file
.Ve

Where the options are:

.Vb 2
    -o dir    Output in given directory (default outputs in file\*(Aqs directory)
    -v        Verbose output (normally just one line per file is shown)
.Ve

## DESCRIPTION

Header "DESCRIPTION"
Each file is converted to file.hqx.

## WARNINGS

Header "WARNINGS"
Largely untested.

## AUTHOR

Header "AUTHOR"
Paul J. Schinder (\s-1NASA/GSFC\s0) mostly, though Eryq can't seem to keep
his grubby paws off anything...
