+++
manpage_section = "1"
manpage_name = "postdrop"
title = "postdrop(1)"
description = "The postdrop(1) command creates a file in the maildrop directory and copies its standard input to the file."
operating_system_version = "15.3"
author = "None Specified"
date = "Sun Feb 16 04:48:18 2025"
detected_package_version = "0"
keywords = ["na", "sendmail", "compatibility", "interface", "postconf", "configuration", "parameters", "syslogd", "system", "logging", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
operating_system = "macos"
manpage_format = "troff"
+++

POSTDROP 1


## NAME

postdrop
-
Postfix mail posting utility

## SYNOPSIS

.na

```
postdrop [\-rv] [\-c config_dir]
.SH DESCRIPTION
.ad
```

The **postdrop**(1) command creates a file in the **maildrop**
directory and copies its standard input to the file.

Options:

- \fB-c
The **main.cf** configuration file is in the named directory
instead of the default configuration directory. See also the
MAIL_CONFIG environment setting below.
**-r**
Use a Postfix-internal protocol for reading the message from
standard input, and for reporting status information on standard
output. This is currently the only supported method.
**-v**
Enable verbose logging for debugging purposes. Multiple **-v**
options make the software increasingly verbose. As of Postfix 2.3,
this option is available for the super-user only.

## SECURITY

.na

```
.ad
```

The command is designed to run with set-group ID privileges, so
that it can write to the **maildrop** queue directory and so that
it can connect to Postfix daemon processes.

## DIAGNOSTICS


Fatal errors: malformed input, I/O error, out of memory. Problems
are logged to **syslogd**(8) and to the standard error stream.
When the input is incomplete, or when the process receives a HUP,
INT, QUIT or TERM signal, the queue file is deleted.

## ENVIRONMENT

.na

```
.ad
```

MAIL_CONFIG
Directory with the **main.cf** file. In order to avoid exploitation
of set-group ID privileges, a non-standard directory is allowed only
if:

> \(bu
The name is listed in the standard **main.cf** file with the
**alternate_config_directories** configuration parameter.
\(bu
The command is invoked by the super-user.



## CONFIGURATION PARAMETERS

.na

```
.ad
```

The following **main.cf** parameters are especially relevant to
this program.
The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

- \fBalternate_config_directories
A list of non-default Postfix configuration directories that may
be specified with "-c config_directory" on the command line, or
via the MAIL_CONFIG environment parameter.

- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBimport_environment (see 'postconf -d'
The list of environment parameters that a Postfix process will
import from a non-Postfix parent process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

- \fBtrigger_timeout
The time limit for sending a trigger to a Postfix daemon (for
example, the **pickup**(8) or **qmgr**(8) daemon).

Available in Postfix version 2.2 and later:

- \fBauthorized_submit_users
List of users who are authorized to submit mail with the **sendmail**(1)
command (and with the privileged **postdrop**(1) helper command).

## FILES

.na

```
/var/spool/postfix/maildrop, maildrop queue
.SH "SEE ALSO"
.na

```
sendmail(1), compatibility interface
postconf(5), configuration parameters
syslogd(8), system logging
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
