+++
author = "None Specified"
manpage_format = "troff"
detected_package_version = "5.34.1"
manpage_section = "1"
description = "The four f(CW*(C`filter_**(C methods shown above are available in all the s-1DBMs0 modules that ship with Perl, namely DB_File, GDBM_File, NDBM_File, ODBM_File and SDBM_File. Each of the methods works identically, and is used to install (or unins..."
manpage_name = "perldbmfilter"
date = "2022-02-19"
title = "perldbmfilter(1)"
operating_system = "macos"
keywords = ["header", "see", "also", "db_file", "gdbm_file", "ndbm_file", "odbm_file", "and", "sdbm_file", "author", "paul", "marquess"]
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLDBMFILTER 1"
PERLDBMFILTER 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perldbmfilter - Perl DBM Filters

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
    $db = tie %hash, \*(AqDBM\*(Aq, ...

    $old_filter = $db->filter_store_key  ( sub \{ ... \} );
    $old_filter = $db->filter_store_value( sub \{ ... \} );
    $old_filter = $db->filter_fetch_key  ( sub \{ ... \} );
    $old_filter = $db->filter_fetch_value( sub \{ ... \} );
.Ve

## DESCRIPTION

Header "DESCRIPTION"
The four \f(CW\*(C`filter_*\*(C' methods shown above are available in all the \s-1DBM\s0
modules that ship with Perl, namely DB_File, GDBM_File, NDBM_File,
ODBM_File and SDBM_File.

Each of the methods works identically, and is used to install (or
uninstall) a single \s-1DBM\s0 Filter. The only difference between them is the
place that the filter is installed.

To summarise:

- \fBfilter_store_key
Item "filter_store_key"
If a filter has been installed with this method, it will be invoked
every time you write a key to a \s-1DBM\s0 database.

- \fBfilter_store_value
Item "filter_store_value"
If a filter has been installed with this method, it will be invoked
every time you write a value to a \s-1DBM\s0 database.

- \fBfilter_fetch_key
Item "filter_fetch_key"
If a filter has been installed with this method, it will be invoked
every time you read a key from a \s-1DBM\s0 database.

- \fBfilter_fetch_value
Item "filter_fetch_value"
If a filter has been installed with this method, it will be invoked
every time you read a value from a \s-1DBM\s0 database.

You can use any combination of the methods from none to all four.

All filter methods return the existing filter, if present, or \f(CW\*(C`undef\*(C'
if not.

To delete a filter pass \f(CW\*(C`undef\*(C' to it.

### The Filter

Subsection "The Filter"
When each filter is called by Perl, a local copy of \f(CW$_ will contain
the key or value to be filtered. Filtering is achieved by modifying
the contents of \f(CW$_. The return code from the filter is ignored.

### An Example: the \s-1NULL\s0 termination problem.

Subsection "An Example: the NULL termination problem."
\s-1DBM\s0 Filters are useful for a class of problems where you *always*
want to make the same transformation to all keys, all values or both.

For example, consider the following scenario. You have a \s-1DBM\s0 database
that you need to share with a third-party C application. The C application
assumes that *all* keys and values are \s-1NULL\s0 terminated. Unfortunately
when Perl writes to \s-1DBM\s0 databases it doesn't use \s-1NULL\s0 termination, so
your Perl application will have to manage \s-1NULL\s0 termination itself. When
you write to the database you will have to use something like this:

.Vb 1
    $hash\{"$key\\0"\} = "$value\\0";
.Ve

Similarly the \s-1NULL\s0 needs to be taken into account when you are considering
the length of existing keys/values.

It would be much better if you could ignore the \s-1NULL\s0 terminations issue
in the main application code and have a mechanism that automatically
added the terminating \s-1NULL\s0 to all keys and values whenever you write to
the database and have them removed when you read from the database. As I'm
sure you have already guessed, this is a problem that \s-1DBM\s0 Filters can
fix very easily.

.Vb 4
    use strict;
    use warnings;
    use SDBM_File;
    use Fcntl;

    my %hash;
    my $filename = "filt";
    unlink $filename;

    my $db = tie(%hash, \*(AqSDBM_File\*(Aq, $filename, O_RDWR|O_CREAT, 0640)
      or die "Cannot open $filename: $!\\n";

    # Install DBM Filters
    $db->filter_fetch_key  ( sub \{ s/\\0$//    \} );
    $db->filter_store_key  ( sub \{ $_ .= "\\0" \} );
    $db->filter_fetch_value(
        sub \{ no warnings \*(Aquninitialized\*(Aq; s/\\0$// \} );
    $db->filter_store_value( sub \{ $_ .= "\\0" \} );

    $hash\{"abc"\} = "def";
    my $a = $hash\{"ABC"\};
    # ...
    undef $db;
    untie %hash;
.Ve

The code above uses SDBM_File, but it will work with any of the \s-1DBM\s0
modules.

Hopefully the contents of each of the filters should be
self-explanatory. Both \*(L"fetch\*(R" filters remove the terminating \s-1NULL,\s0
and both \*(L"store\*(R" filters add a terminating \s-1NULL.\s0

### Another Example: Key is a C int.

Subsection "Another Example: Key is a C int."
Here is another real-life example. By default, whenever Perl writes to
a \s-1DBM\s0 database it always writes the key and value as strings. So when
you use this:

.Vb 1
    $hash\{12345\} = "something";
.Ve

the key 12345 will get stored in the \s-1DBM\s0 database as the 5 byte string
\*(L"12345\*(R". If you actually want the key to be stored in the \s-1DBM\s0 database
as a C int, you will have to use \f(CW\*(C`pack\*(C' when writing, and \f(CW\*(C`unpack\*(C'
when reading.

Here is a \s-1DBM\s0 Filter that does it:

.Vb 6
    use strict;
    use warnings;
    use DB_File;
    my %hash;
    my $filename = "filt";
    unlink $filename;


    my $db = tie %hash, \*(AqDB_File\*(Aq, $filename, O_CREAT|O_RDWR, 0666,
        $DB_HASH or die "Cannot open $filename: $!\\n";

    $db->filter_fetch_key  ( sub \{ $_ = unpack("i", $_) \} );
    $db->filter_store_key  ( sub \{ $_ = pack ("i", $_) \} );
    $hash\{123\} = "def";
    # ...
    undef $db;
    untie %hash;
.Ve

The code above uses DB_File, but again it will work with any of the
\s-1DBM\s0 modules.

This time only two filters have been used; we only need to manipulate
the contents of the key, so it wasn't necessary to install any value
filters.

## SEE ALSO

Header "SEE ALSO"
DB_File, GDBM_File, NDBM_File, ODBM_File and SDBM_File.

## AUTHOR

Header "AUTHOR"
Paul Marquess
