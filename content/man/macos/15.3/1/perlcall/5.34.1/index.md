+++
keywords = ["header", "see", "also", "perlxs", "perlguts", "perlembed", "author", "paul", "marquess", "special", "thanks", "to", "the", "following", "people", "who", "assisted", "in", "creation", "of", "document", "jeff", "okamoto", "tim", "bunce", "nick", "gianniotis", "steve", "kelem", "gurusamy", "sarathy", "and", "larry", "wall", "date", "last", "updated", "for", "perl", "5", "23", "1"]
manpage_section = "1"
manpage_name = "perlcall"
title = "perlcall(1)"
date = "2022-02-19"
operating_system_version = "15.3"
operating_system = "macos"
author = "None Specified"
description = "The purpose of this document is to show you how to call Perl subroutines directly from C, i.e., how to write callbacks. Apart from discussing the C interface provided by Perl for writing callbacks the document uses a series of examples to show how ..."
detected_package_version = "5.34.1"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLCALL 1"
PERLCALL 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlcall - Perl calling conventions from C

## DESCRIPTION

Header "DESCRIPTION"
The purpose of this document is to show you how to call Perl subroutines
directly from C, i.e., how to write *callbacks*.

Apart from discussing the C interface provided by Perl for writing
callbacks the document uses a series of examples to show how the
interface actually works in practice.  In addition some techniques for
coding callbacks are covered.

Examples where callbacks are necessary include

- \(bu
An Error Handler
.Sp
You have created an \s-1XSUB\s0 interface to an application's C \s-1API.\s0
.Sp
A fairly common feature in applications is to allow you to define a C
function that will be called whenever something nasty occurs. What we
would like is to be able to specify a Perl subroutine that will be
called instead.

- \(bu
An Event-Driven Program
.Sp
The classic example of where callbacks are used is when writing an
event driven program, such as for an X11 application.  In this case
you register functions to be called whenever specific events occur,
e.g., a mouse button is pressed, the cursor moves into a window or a
menu item is selected.

Although the techniques described here are applicable when embedding
Perl in a C program, this is not the primary goal of this document.
There are other details that must be considered and are specific to
embedding Perl. For details on embedding Perl in C refer to
perlembed.

Before you launch yourself head first into the rest of this document,
it would be a good idea to have read the following two documents\*(--perlxs
and perlguts.

## THE CALL_ FUNCTIONS

Header "THE CALL_ FUNCTIONS"
Although this stuff is easier to explain using examples, you first need
be aware of a few important definitions.

Perl has a number of C functions that allow you to call Perl
subroutines.  They are

.Vb 4
    I32 call_sv(SV* sv, I32 flags);
    I32 call_pv(char *subname, I32 flags);
    I32 call_method(char *methname, I32 flags);
    I32 call_argv(char *subname, I32 flags, char **argv);
.Ve

The key function is *call_sv*.  All the other functions are
fairly simple wrappers which make it easier to call Perl subroutines in
special cases. At the end of the day they will all call *call_sv*
to invoke the Perl subroutine.

All the *call_** functions have a \f(CW\*(C`flags\*(C' parameter which is
used to pass a bit mask of options to Perl.  This bit mask operates
identically for each of the functions.  The settings available in the
bit mask are discussed in \*(L"\s-1FLAG VALUES\*(R"\s0.

Each of the functions will now be discussed in turn.

- call_sv
Item "call_sv"
*call_sv* takes two parameters. The first, \f(CW\*(C`sv\*(C', is an SV*.
This allows you to specify the Perl subroutine to be called either as a
C string (which has first been converted to an \s-1SV\s0) or a reference to a
subroutine. The section, \*(L"Using call_sv\*(R", shows how you can make
use of *call_sv*.

- call_pv
Item "call_pv"
The function, *call_pv*, is similar to *call_sv* except it
expects its first parameter to be a C char* which identifies the Perl
subroutine you want to call, e.g., \f(CW\*(C`call_pv("fred", 0)\*(C'.  If the
subroutine you want to call is in another package, just include the
package name in the string, e.g., \f(CW"pkg::fred".

- call_method
Item "call_method"
The function *call_method* is used to call a method from a Perl
class.  The parameter \f(CW\*(C`methname\*(C' corresponds to the name of the method
to be called.  Note that the class that the method belongs to is passed
on the Perl stack rather than in the parameter list. This class can be
either the name of the class (for a static method) or a reference to an
object (for a virtual method).  See perlobj for more information on
static and virtual methods and \*(L"Using call_method\*(R" for an example
of using *call_method*.

- call_argv
Item "call_argv"
*call_argv* calls the Perl subroutine specified by the C string
stored in the \f(CW\*(C`subname\*(C' parameter. It also takes the usual \f(CW\*(C`flags\*(C'
parameter.  The final parameter, \f(CW\*(C`argv\*(C', consists of a NULL-terminated
list of C strings to be passed as parameters to the Perl subroutine.
See \*(L"Using call_argv\*(R".

All the functions return an integer. This is a count of the number of
items returned by the Perl subroutine. The actual items returned by the
subroutine are stored on the Perl stack.

As a general rule you should *always* check the return value from
these functions.  Even if you are expecting only a particular number of
values to be returned from the Perl subroutine, there is nothing to
stop someone from doing something unexpected\*(--don't say you haven't
been warned.

## FLAG VALUES

Header "FLAG VALUES"
The \f(CW\*(C`flags\*(C' parameter in all the *call_** functions is one of \f(CW\*(C`G_VOID\*(C',
\f(CW\*(C`G_SCALAR\*(C', or \f(CW\*(C`G_ARRAY\*(C', which indicate the call context, \s-1OR\s0'ed together
with a bit mask of any combination of the other G_* symbols defined below.

### G_VOID

Subsection "G_VOID"
Calls the Perl subroutine in a void context.

This flag has 2 effects:

- 1.
It indicates to the subroutine being called that it is executing in
a void context (if it executes *wantarray* the result will be the
undefined value).

- 2.
It ensures that nothing is actually returned from the subroutine.

The value returned by the *call_** function indicates how many
items have been returned by the Perl subroutine\*(--in this case it will
be 0.

### G_SCALAR

Subsection "G_SCALAR"
Calls the Perl subroutine in a scalar context.  This is the default
context flag setting for all the *call_** functions.

This flag has 2 effects:

- 1.
It indicates to the subroutine being called that it is executing in a
scalar context (if it executes *wantarray* the result will be false).

- 2.
It ensures that only a scalar is actually returned from the subroutine.
The subroutine can, of course,  ignore the *wantarray* and return a
list anyway. If so, then only the last element of the list will be
returned.

The value returned by the *call_** function indicates how many
items have been returned by the Perl subroutine - in this case it will
be either 0 or 1.

If 0, then you have specified the G_DISCARD flag.

If 1, then the item actually returned by the Perl subroutine will be
stored on the Perl stack - the section \*(L"Returning a Scalar\*(R" shows how
to access this value on the stack.  Remember that regardless of how
many items the Perl subroutine returns, only the last one will be
accessible from the stack - think of the case where only one value is
returned as being a list with only one element.  Any other items that
were returned will not exist by the time control returns from the
*call_** function.  The section \*(L"Returning a List in Scalar
Context\*(R" shows an example of this behavior.

### G_ARRAY

Subsection "G_ARRAY"
Calls the Perl subroutine in a list context.

As with G_SCALAR, this flag has 2 effects:

- 1.
It indicates to the subroutine being called that it is executing in a
list context (if it executes *wantarray* the result will be true).

- 2.
It ensures that all items returned from the subroutine will be
accessible when control returns from the *call_** function.

The value returned by the *call_** function indicates how many
items have been returned by the Perl subroutine.

If 0, then you have specified the G_DISCARD flag.

If not 0, then it will be a count of the number of items returned by
the subroutine. These items will be stored on the Perl stack.  The
section \*(L"Returning a List of Values\*(R" gives an example of using the
G_ARRAY flag and the mechanics of accessing the returned items from the
Perl stack.

### G_DISCARD

Subsection "G_DISCARD"
By default, the *call_** functions place the items returned from
by the Perl subroutine on the stack.  If you are not interested in
these items, then setting this flag will make Perl get rid of them
automatically for you.  Note that it is still possible to indicate a
context to the Perl subroutine by using either G_SCALAR or G_ARRAY.

If you do not set this flag then it is *very* important that you make
sure that any temporaries (i.e., parameters passed to the Perl
subroutine and values returned from the subroutine) are disposed of
yourself.  The section \*(L"Returning a Scalar\*(R" gives details of how to
dispose of these temporaries explicitly and the section \*(L"Using Perl to
Dispose of Temporaries\*(R" discusses the specific circumstances where you
can ignore the problem and let Perl deal with it for you.

### G_NOARGS

Subsection "G_NOARGS"
Whenever a Perl subroutine is called using one of the *call_**
functions, it is assumed by default that parameters are to be passed to
the subroutine.  If you are not passing any parameters to the Perl
subroutine, you can save a bit of time by setting this flag.  It has
the effect of not creating the \f(CW@_ array for the Perl subroutine.

Although the functionality provided by this flag may seem
straightforward, it should be used only if there is a good reason to do
so.  The reason for being cautious is that, even if you have specified
the G_NOARGS flag, it is still possible for the Perl subroutine that
has been called to think that you have passed it parameters.

In fact, what can happen is that the Perl subroutine you have called
can access the \f(CW@_ array from a previous Perl subroutine.  This will
occur when the code that is executing the *call_** function has
itself been called from another Perl subroutine. The code below
illustrates this

.Vb 2
    sub fred
      \{ print "@_\\n"  \}

    sub joe
      \{ &fred \}

    &joe(1,2,3);
.Ve

This will print

.Vb 1
    1 2 3
.Ve

What has happened is that \f(CW\*(C`fred\*(C' accesses the \f(CW@_ array which
belongs to \f(CW\*(C`joe\*(C'.

### G_EVAL

Subsection "G_EVAL"
It is possible for the Perl subroutine you are calling to terminate
abnormally, e.g., by calling *die* explicitly or by not actually
existing.  By default, when either of these events occurs, the
process will terminate immediately.  If you want to trap this
type of event, specify the G_EVAL flag.  It will put an *eval \{ \}*
around the subroutine call.

Whenever control returns from the *call_** function you need to
check the \f(CW$@ variable as you would in a normal Perl script.

The value returned from the *call_** function is dependent on
what other flags have been specified and whether an error has
occurred.  Here are all the different cases that can occur:

- \(bu
If the *call_** function returns normally, then the value
returned is as specified in the previous sections.

- \(bu
If G_DISCARD is specified, the return value will always be 0.

- \(bu
If G_ARRAY is specified *and* an error has occurred, the return value
will always be 0.

- \(bu
If G_SCALAR is specified *and* an error has occurred, the return value
will be 1 and the value on the top of the stack will be *undef*. This
means that if you have already detected the error by checking \f(CW$@ and
you want the program to continue, you must remember to pop the *undef*
from the stack.

See \*(L"Using G_EVAL\*(R" for details on using G_EVAL.

### G_KEEPERR

Subsection "G_KEEPERR"
Using the G_EVAL flag described above will always set \f(CW$@: clearing
it if there was no error, and setting it to describe the error if there
was an error in the called code.  This is what you want if your intention
is to handle possible errors, but sometimes you just want to trap errors
and stop them interfering with the rest of the program.

This scenario will mostly be applicable to code that is meant to be called
from within destructors, asynchronous callbacks, and signal handlers.
In such situations, where the code being called has little relation to the
surrounding dynamic context, the main program needs to be insulated from
errors in the called code, even if they can't be handled intelligently.
It may also be useful to do this with code for \f(CW\*(C`_\|_DIE_\|_\*(C' or \f(CW\*(C`_\|_WARN_\|_\*(C'
hooks, and \f(CW\*(C`tie\*(C' functions.

The G_KEEPERR flag is meant to be used in conjunction with G_EVAL in
*call_** functions that are used to implement such code, or with
\f(CW\*(C`eval_sv\*(C'.  This flag has no effect on the \f(CW\*(C`call_*\*(C' functions when
G_EVAL is not used.

When G_KEEPERR is used, any error in the called code will terminate the
call as usual, and the error will not propagate beyond the call (as usual
for G_EVAL), but it will not go into \f(CW$@.  Instead the error will be
converted into a warning, prefixed with the string \*(L"\\t(in cleanup)\*(R".
This can be disabled using \f(CW\*(C`no warnings \*(Aqmisc\*(Aq\*(C'.  If there is no error,
\f(CW$@ will not be cleared.

Note that the G_KEEPERR flag does not propagate into inner evals; these
may still set \f(CW$@.

The G_KEEPERR flag was introduced in Perl version 5.002.

See \*(L"Using G_KEEPERR\*(R" for an example of a situation that warrants the
use of this flag.

### Determining the Context

Subsection "Determining the Context"
As mentioned above, you can determine the context of the currently
executing subroutine in Perl with *wantarray*.  The equivalent test
can be made in C by using the \f(CW\*(C`GIMME_V\*(C' macro, which returns
\f(CW\*(C`G_ARRAY\*(C' if you have been called in a list context, \f(CW\*(C`G_SCALAR\*(C' if
in a scalar context, or \f(CW\*(C`G_VOID\*(C' if in a void context (i.e., the
return value will not be used).  An older version of this macro is
called \f(CW\*(C`GIMME\*(C'; in a void context it returns \f(CW\*(C`G_SCALAR\*(C' instead of
\f(CW\*(C`G_VOID\*(C'.  An example of using the \f(CW\*(C`GIMME_V\*(C' macro is shown in
section \*(L"Using \s-1GIMME_V\*(R"\s0.

## EXAMPLES

Header "EXAMPLES"
Enough of the definition talk! Let's have a few examples.

Perl provides many macros to assist in accessing the Perl stack.
Wherever possible, these macros should always be used when interfacing
to Perl internals.  We hope this should make the code less vulnerable
to any changes made to Perl in the future.

Another point worth noting is that in the first series of examples I
have made use of only the *call_pv* function.  This has been done
to keep the code simpler and ease you into the topic.  Wherever
possible, if the choice is between using *call_pv* and
*call_sv*, you should always try to use *call_sv*.  See
\*(L"Using call_sv\*(R" for details.

### No Parameters, Nothing Returned

Subsection "No Parameters, Nothing Returned"
This first trivial example will call a Perl subroutine, *PrintUID*, to
print out the \s-1UID\s0 of the process.

.Vb 4
    sub PrintUID
    \{
        print "UID is $<\\n";
    \}
.Ve

and here is a C function to call it

.Vb 4
    static void
    call_PrintUID()
    \{
        dSP;

        PUSHMARK(SP);
        call_pv("PrintUID", G_DISCARD|G_NOARGS);
    \}
.Ve

Simple, eh?

A few points to note about this example:

- 1.
Ignore \f(CW\*(C`dSP\*(C' and \f(CW\*(C`PUSHMARK(SP)\*(C' for now. They will be discussed in
the next example.

- 2.
We aren't passing any parameters to *PrintUID* so G_NOARGS can be
specified.

- 3.
We aren't interested in anything returned from *PrintUID*, so
G_DISCARD is specified. Even if *PrintUID* was changed to
return some value(s), having specified G_DISCARD will mean that they
will be wiped by the time control returns from *call_pv*.

- 4.
As *call_pv* is being used, the Perl subroutine is specified as a
C string. In this case the subroutine name has been 'hard-wired' into the
code.

- 5.
Because we specified G_DISCARD, it is not necessary to check the value
returned from *call_pv*. It will always be 0.

### Passing Parameters

Subsection "Passing Parameters"
Now let's make a slightly more complex example. This time we want to
call a Perl subroutine, \f(CW\*(C`LeftString\*(C', which will take 2 parameters\*(--a
string ($s) and an integer ($n).  The subroutine will simply
print the first \f(CW$n characters of the string.

So the Perl subroutine would look like this:

.Vb 5
    sub LeftString
    \{
        my($s, $n) = @_;
        print substr($s, 0, $n), "\\n";
    \}
.Ve

The C function required to call *LeftString* would look like this:

.Vb 6
    static void
    call_LeftString(a, b)
    char * a;
    int b;
    \{
        dSP;

        ENTER;
        SAVETMPS;

        PUSHMARK(SP);
        EXTEND(SP, 2);
        PUSHs(sv_2mortal(newSVpv(a, 0)));
        PUSHs(sv_2mortal(newSViv(b)));
        PUTBACK;

        call_pv("LeftString", G_DISCARD);

        FREETMPS;
        LEAVE;
    \}
.Ve

Here are a few notes on the C function *call_LeftString*.

- 1.
Parameters are passed to the Perl subroutine using the Perl stack.
This is the purpose of the code beginning with the line \f(CW\*(C`dSP\*(C' and
ending with the line \f(CW\*(C`PUTBACK\*(C'.  The \f(CW\*(C`dSP\*(C' declares a local copy
of the stack pointer.  This local copy should **always** be accessed
as \f(CW\*(C`SP\*(C'.

- 2.
If you are going to put something onto the Perl stack, you need to know
where to put it. This is the purpose of the macro \f(CW\*(C`dSP\*(C'--it declares
and initializes a *local* copy of the Perl stack pointer.
.Sp
All the other macros which will be used in this example require you to
have used this macro.
.Sp
The exception to this rule is if you are calling a Perl subroutine
directly from an \s-1XSUB\s0 function. In this case it is not necessary to
use the \f(CW\*(C`dSP\*(C' macro explicitly\*(--it will be declared for you
automatically.

- 3.
Any parameters to be pushed onto the stack should be bracketed by the
\f(CW\*(C`PUSHMARK\*(C' and \f(CW\*(C`PUTBACK\*(C' macros.  The purpose of these two macros, in
this context, is to count the number of parameters you are
pushing automatically.  Then whenever Perl is creating the \f(CW@_ array for the
subroutine, it knows how big to make it.
.Sp
The \f(CW\*(C`PUSHMARK\*(C' macro tells Perl to make a mental note of the current
stack pointer. Even if you aren't passing any parameters (like the
example shown in the section \*(L"No Parameters, Nothing Returned\*(R") you
must still call the \f(CW\*(C`PUSHMARK\*(C' macro before you can call any of the
*call_** functions\*(--Perl still needs to know that there are no
parameters.
.Sp
The \f(CW\*(C`PUTBACK\*(C' macro sets the global copy of the stack pointer to be
the same as our local copy. If we didn't do this, *call_pv*
wouldn't know where the two parameters we pushed were\*(--remember that
up to now all the stack pointer manipulation we have done is with our
local copy, *not* the global copy.

- 4.
Next, we come to \s-1EXTEND\s0 and PUSHs. This is where the parameters
actually get pushed onto the stack. In this case we are pushing a
string and an integer.
.Sp
Alternatively you can use the **XPUSHs()** macro, which combines a
\f(CW\*(C`EXTEND(SP, 1)\*(C' and \f(CW\*(C`PUSHs()\*(C'.  This is less efficient if you're
pushing multiple values.
.Sp
See \*(L"XSUBs and the Argument Stack\*(R" in perlguts for details
on how the \s-1PUSH\s0 macros work.

- 5.
Because we created temporary values (by means of **sv_2mortal()** calls)
we will have to tidy up the Perl stack and dispose of mortal SVs.
.Sp
This is the purpose of
.Sp
.Vb 2
    ENTER;
    SAVETMPS;
.Ve
.Sp
at the start of the function, and
.Sp
.Vb 2
    FREETMPS;
    LEAVE;
.Ve
.Sp
at the end. The \f(CW\*(C`ENTER\*(C'/\f(CW\*(C`SAVETMPS\*(C' pair creates a boundary for any
temporaries we create.  This means that the temporaries we get rid of
will be limited to those which were created after these calls.
.Sp
The \f(CW\*(C`FREETMPS\*(C'/\f(CW\*(C`LEAVE\*(C' pair will get rid of any values returned by
the Perl subroutine (see next example), plus it will also dump the
mortal SVs we have created.  Having \f(CW\*(C`ENTER\*(C'/\f(CW\*(C`SAVETMPS\*(C' at the
beginning of the code makes sure that no other mortals are destroyed.
.Sp
Think of these macros as working a bit like \f(CW\*(C`\{\*(C' and \f(CW\*(C`\}\*(C' in Perl
to limit the scope of local variables.
.Sp
See the section \*(L"Using Perl to Dispose of Temporaries\*(R" for details of
an alternative to using these macros.

- 6.
Finally, *LeftString* can now be called via the *call_pv* function.
The only flag specified this time is G_DISCARD. Because we are passing
2 parameters to the Perl subroutine this time, we have not specified
G_NOARGS.

### Returning a Scalar

Subsection "Returning a Scalar"
Now for an example of dealing with the items returned from a Perl
subroutine.

Here is a Perl subroutine, *Adder*, that takes 2 integer parameters
and simply returns their sum.

.Vb 5
    sub Adder
    \{
        my($a, $b) = @_;
        $a + $b;
    \}
.Ve

Because we are now concerned with the return value from *Adder*, the C
function required to call it is now a bit more complex.

.Vb 7
    static void
    call_Adder(a, b)
    int a;
    int b;
    \{
        dSP;
        int count;

        ENTER;
        SAVETMPS;

        PUSHMARK(SP);
        EXTEND(SP, 2);
        PUSHs(sv_2mortal(newSViv(a)));
        PUSHs(sv_2mortal(newSViv(b)));
        PUTBACK;

        count = call_pv("Adder", G_SCALAR);

        SPAGAIN;

        if (count != 1)
            croak("Big trouble\\n");

        printf ("The sum of %d and %d is %d\\n", a, b, POPi);

        PUTBACK;
        FREETMPS;
        LEAVE;
    \}
.Ve

Points to note this time are

- 1.
The only flag specified this time was G_SCALAR. That means that the \f(CW@_
array will be created and that the value returned by *Adder* will
still exist after the call to *call_pv*.

- 2.
The purpose of the macro \f(CW\*(C`SPAGAIN\*(C' is to refresh the local copy of the
stack pointer. This is necessary because it is possible that the memory
allocated to the Perl stack has been reallocated during the
*call_pv* call.
.Sp
If you are making use of the Perl stack pointer in your code you must
always refresh the local copy using \s-1SPAGAIN\s0 whenever you make use
of the *call_** functions or any other Perl internal function.

- 3.
Although only a single value was expected to be returned from *Adder*,
it is still good practice to check the return code from *call_pv*
anyway.
.Sp
Expecting a single value is not quite the same as knowing that there
will be one. If someone modified *Adder* to return a list and we
didn't check for that possibility and take appropriate action the Perl
stack would end up in an inconsistent state. That is something you
*really* don't want to happen ever.

- 4.
The \f(CW\*(C`POPi\*(C' macro is used here to pop the return value from the stack.
In this case we wanted an integer, so \f(CW\*(C`POPi\*(C' was used.
.Sp
Here is the complete list of \s-1POP\s0 macros available, along with the types
they return.
.Sp
.Vb 8
    POPs        SV
    POPp        pointer (PV)
    POPpbytex   pointer to bytes (PV)
    POPn        double (NV)
    POPi        integer (IV)
    POPu        unsigned integer (UV)
    POPl        long
    POPul       unsigned long
.Ve
.Sp
Since these macros have side-effects don't use them as arguments to
macros that may evaluate their argument several times, for example:
.Sp
.Vb 3
  /* Bad idea, don\*(Aqt do this */
  STRLEN len;
  const char *s = SvPV(POPs, len);
.Ve
.Sp
Instead, use a temporary:
.Sp
.Vb 3
  STRLEN len;
  SV *sv = POPs;
  const char *s = SvPV(sv, len);
.Ve
.Sp
or a macro that guarantees it will evaluate its arguments only once:
.Sp
.Vb 2
  STRLEN len;
  const char *s = SvPVx(POPs, len);
.Ve

- 5.
The final \f(CW\*(C`PUTBACK\*(C' is used to leave the Perl stack in a consistent
state before exiting the function.  This is necessary because when we
popped the return value from the stack with \f(CW\*(C`POPi\*(C' it updated only our
local copy of the stack pointer.  Remember, \f(CW\*(C`PUTBACK\*(C' sets the global
stack pointer to be the same as our local copy.

### Returning a List of Values

Subsection "Returning a List of Values"
Now, let's extend the previous example to return both the sum of the
parameters and the difference.

Here is the Perl subroutine

.Vb 5
    sub AddSubtract
    \{
       my($a, $b) = @_;
       ($a+$b, $a-$b);
    \}
.Ve

and this is the C function

.Vb 7
    static void
    call_AddSubtract(a, b)
    int a;
    int b;
    \{
        dSP;
        int count;

        ENTER;
        SAVETMPS;

        PUSHMARK(SP);
        EXTEND(SP, 2);
        PUSHs(sv_2mortal(newSViv(a)));
        PUSHs(sv_2mortal(newSViv(b)));
        PUTBACK;

        count = call_pv("AddSubtract", G_ARRAY);

        SPAGAIN;

        if (count != 2)
            croak("Big trouble\\n");

        printf ("%d - %d = %d\\n", a, b, POPi);
        printf ("%d + %d = %d\\n", a, b, POPi);

        PUTBACK;
        FREETMPS;
        LEAVE;
    \}
.Ve

If *call_AddSubtract* is called like this

.Vb 1
    call_AddSubtract(7, 4);
.Ve

then here is the output

.Vb 2
    7 - 4 = 3
    7 + 4 = 11
.Ve

Notes

- 1.
We wanted list context, so G_ARRAY was used.

- 2.
Not surprisingly \f(CW\*(C`POPi\*(C' is used twice this time because we were
retrieving 2 values from the stack. The important thing to note is that
when using the \f(CW\*(C`POP*\*(C' macros they come off the stack in *reverse*
order.

### Returning a List in Scalar Context

Subsection "Returning a List in Scalar Context"
Say the Perl subroutine in the previous section was called in a scalar
context, like this

.Vb 8
    static void
    call_AddSubScalar(a, b)
    int a;
    int b;
    \{
        dSP;
        int count;
        int i;

        ENTER;
        SAVETMPS;

        PUSHMARK(SP);
        EXTEND(SP, 2);
        PUSHs(sv_2mortal(newSViv(a)));
        PUSHs(sv_2mortal(newSViv(b)));
        PUTBACK;

        count = call_pv("AddSubtract", G_SCALAR);

        SPAGAIN;

        printf ("Items Returned = %d\\n", count);

        for (i = 1; i <= count; ++i)
            printf ("Value %d = %d\\n", i, POPi);

        PUTBACK;
        FREETMPS;
        LEAVE;
    \}
.Ve

The other modification made is that *call_AddSubScalar* will print the
number of items returned from the Perl subroutine and their value (for
simplicity it assumes that they are integer).  So if
*call_AddSubScalar* is called

.Vb 1
    call_AddSubScalar(7, 4);
.Ve

then the output will be

.Vb 2
    Items Returned = 1
    Value 1 = 3
.Ve

In this case the main point to note is that only the last item in the
list is returned from the subroutine. *AddSubtract* actually made it back to
*call_AddSubScalar*.

### Returning Data from Perl via the Parameter List

Subsection "Returning Data from Perl via the Parameter List"
It is also possible to return values directly via the parameter
list\*(--whether it is actually desirable to do it is another matter entirely.

The Perl subroutine, *Inc*, below takes 2 parameters and increments
each directly.

.Vb 5
    sub Inc
    \{
        ++ $_[0];
        ++ $_[1];
    \}
.Ve

and here is a C function to call it.

.Vb 9
    static void
    call_Inc(a, b)
    int a;
    int b;
    \{
        dSP;
        int count;
        SV * sva;
        SV * svb;

        ENTER;
        SAVETMPS;

        sva = sv_2mortal(newSViv(a));
        svb = sv_2mortal(newSViv(b));

        PUSHMARK(SP);
        EXTEND(SP, 2);
        PUSHs(sva);
        PUSHs(svb);
        PUTBACK;

        count = call_pv("Inc", G_DISCARD);

        if (count != 0)
            croak ("call_Inc: expected 0 values from \*(AqInc\*(Aq, got %d\\n",
                   count);

        printf ("%d + 1 = %d\\n", a, SvIV(sva));
        printf ("%d + 1 = %d\\n", b, SvIV(svb));

        FREETMPS;
        LEAVE;
    \}
.Ve

To be able to access the two parameters that were pushed onto the stack
after they return from *call_pv* it is necessary to make a note
of their addresses\*(--thus the two variables \f(CW\*(C`sva\*(C' and \f(CW\*(C`svb\*(C'.

The reason this is necessary is that the area of the Perl stack which
held them will very likely have been overwritten by something else by
the time control returns from *call_pv*.

### Using G_EVAL

Subsection "Using G_EVAL"
Now an example using G_EVAL. Below is a Perl subroutine which computes
the difference of its 2 parameters. If this would result in a negative
result, the subroutine calls *die*.

.Vb 3
    sub Subtract
    \{
        my ($a, $b) = @_;

        die "death can be fatal\\n" if $a < $b;

        $a - $b;
    \}
.Ve

and some C to call it

.Vb 8
 static void
 call_Subtract(a, b)
 int a;
 int b;
 \{
     dSP;
     int count;
     SV *err_tmp;

     ENTER;
     SAVETMPS;

     PUSHMARK(SP);
     EXTEND(SP, 2);
     PUSHs(sv_2mortal(newSViv(a)));
     PUSHs(sv_2mortal(newSViv(b)));
     PUTBACK;

     count = call_pv("Subtract", G_EVAL|G_SCALAR);

     SPAGAIN;

     /* Check the eval first */
     err_tmp = ERRSV;
     if (SvTRUE(err_tmp))
     \{
         printf ("Uh oh - %s\\n", SvPV_nolen(err_tmp));
         POPs;
     \}
     else
     \{
       if (count != 1)
        croak("call_Subtract: wanted 1 value from \*(AqSubtract\*(Aq, got %d\\n",
              count);

         printf ("%d - %d = %d\\n", a, b, POPi);
     \}

     PUTBACK;
     FREETMPS;
     LEAVE;
 \}
.Ve

If *call_Subtract* is called thus

.Vb 1
    call_Subtract(4, 5)
.Ve

the following will be printed

.Vb 1
    Uh oh - death can be fatal
.Ve

Notes

- 1.
We want to be able to catch the *die* so we have used the G_EVAL
flag.  Not specifying this flag would mean that the program would
terminate immediately at the *die* statement in the subroutine
*Subtract*.

- 2.
The code
.Sp
.Vb 6
    err_tmp = ERRSV;
    if (SvTRUE(err_tmp))
    \{
        printf ("Uh oh - %s\\n", SvPV_nolen(err_tmp));
        POPs;
    \}
.Ve
.Sp
is the direct equivalent of this bit of Perl
.Sp
.Vb 1
    print "Uh oh - $@\\n" if $@;
.Ve
.Sp
\f(CW\*(C`PL_errgv\*(C' is a perl global of type \f(CW\*(C`GV *\*(C' that points to the symbol
table entry containing the error.  \f(CW\*(C`ERRSV\*(C' therefore refers to the C
equivalent of \f(CW$@.  We use a local temporary, \f(CW\*(C`err_tmp\*(C', since
\f(CW\*(C`ERRSV\*(C' is a macro that calls a function, and \f(CW\*(C`SvTRUE(ERRSV)\*(C' would
end up calling that function multiple times.

- 3.
Note that the stack is popped using \f(CW\*(C`POPs\*(C' in the block where
\f(CW\*(C`SvTRUE(err_tmp)\*(C' is true.  This is necessary because whenever a
*call_** function invoked with G_EVAL|G_SCALAR returns an error,
the top of the stack holds the value *undef*. Because we want the
program to continue after detecting this error, it is essential that
the stack be tidied up by removing the *undef*.

### Using G_KEEPERR

Subsection "Using G_KEEPERR"
Consider this rather facetious example, where we have used an \s-1XS\s0
version of the call_Subtract example above inside a destructor:

.Vb 9
    package Foo;
    sub new \{ bless \{\}, $_[0] \}
    sub Subtract \{
        my($a,$b) = @_;
        die "death can be fatal" if $a < $b;
        $a - $b;
    \}
    sub DESTROY \{ call_Subtract(5, 4); \}
    sub foo \{ die "foo dies"; \}

    package main;
    \{
        my $foo = Foo->new;
        eval \{ $foo->foo \};
    \}
    print "Saw: $@" if $@;             # should be, but isn\*(Aqt
.Ve

This example will fail to recognize that an error occurred inside the
\f(CW\*(C`eval \{\}\*(C'.  Here's why: the call_Subtract code got executed while perl
was cleaning up temporaries when exiting the outer braced block, and because
call_Subtract is implemented with *call_pv* using the G_EVAL
flag, it promptly reset \f(CW$@.  This results in the failure of the
outermost test for \f(CW$@, and thereby the failure of the error trap.

Appending the G_KEEPERR flag, so that the *call_pv* call in
call_Subtract reads:

.Vb 1
        count = call_pv("Subtract", G_EVAL|G_SCALAR|G_KEEPERR);
.Ve

will preserve the error and restore reliable error handling.

### Using call_sv

Subsection "Using call_sv"
In all the previous examples I have 'hard-wired' the name of the Perl
subroutine to be called from C.  Most of the time though, it is more
convenient to be able to specify the name of the Perl subroutine from
within the Perl script, and you'll want to use
call_sv.

Consider the Perl code below

.Vb 4
    sub fred
    \{
        print "Hello there\\n";
    \}

    CallSubPV("fred");
.Ve

Here is a snippet of \s-1XSUB\s0 which defines *CallSubPV*.

.Vb 6
    void
    CallSubPV(name)
        char *  name
        CODE:
        PUSHMARK(SP);
        call_pv(name, G_DISCARD|G_NOARGS);
.Ve

That is fine as far as it goes. The thing is, the Perl subroutine
can be specified as only a string, however, Perl allows references
to subroutines and anonymous subroutines.
This is where *call_sv* is useful.

The code below for *CallSubSV* is identical to *CallSubPV* except
that the \f(CW\*(C`name\*(C' parameter is now defined as an SV* and we use
*call_sv* instead of *call_pv*.

.Vb 6
    void
    CallSubSV(name)
        SV *    name
        CODE:
        PUSHMARK(SP);
        call_sv(name, G_DISCARD|G_NOARGS);
.Ve

Because we are using an \s-1SV\s0 to call *fred* the following can all be used:

.Vb 5
    CallSubSV("fred");
    CallSubSV(\fred);
    $ref = \fred;
    CallSubSV($ref);
    CallSubSV( sub \{ print "Hello there\\n" \} );
.Ve

As you can see, *call_sv* gives you much greater flexibility in
how you can specify the Perl subroutine.

You should note that, if it is necessary to store the \s-1SV\s0 (\f(CW\*(C`name\*(C' in the
example above) which corresponds to the Perl subroutine so that it can
be used later in the program, it not enough just to store a copy of the
pointer to the \s-1SV.\s0 Say the code above had been like this:

.Vb 1
    static SV * rememberSub;

    void
    SaveSub1(name)
        SV *    name
        CODE:
        rememberSub = name;

    void
    CallSavedSub1()
        CODE:
        PUSHMARK(SP);
        call_sv(rememberSub, G_DISCARD|G_NOARGS);
.Ve

The reason this is wrong is that, by the time you come to use the
pointer \f(CW\*(C`rememberSub\*(C' in \f(CW\*(C`CallSavedSub1\*(C', it may or may not still refer
to the Perl subroutine that was recorded in \f(CW\*(C`SaveSub1\*(C'.  This is
particularly true for these cases:

.Vb 2
    SaveSub1(\fred);
    CallSavedSub1();

    SaveSub1( sub \{ print "Hello there\\n" \} );
    CallSavedSub1();
.Ve

By the time each of the \f(CW\*(C`SaveSub1\*(C' statements above has been executed,
the SV*s which corresponded to the parameters will no longer exist.
Expect an error message from Perl of the form

.Vb 1
    Can\*(Aqt use an undefined value as a subroutine reference at ...
.Ve

for each of the \f(CW\*(C`CallSavedSub1\*(C' lines.

Similarly, with this code

.Vb 4
    $ref = \fred;
    SaveSub1($ref);
    $ref = 47;
    CallSavedSub1();
.Ve

you can expect one of these messages (which you actually get is dependent on
the version of Perl you are using)

.Vb 2
    Not a CODE reference at ...
    Undefined subroutine &main::47 called ...
.Ve

The variable \f(CW$ref may have referred to the subroutine \f(CW\*(C`fred\*(C'
whenever the call to \f(CW\*(C`SaveSub1\*(C' was made but by the time
\f(CW\*(C`CallSavedSub1\*(C' gets called it now holds the number \f(CW47. Because we
saved only a pointer to the original \s-1SV\s0 in \f(CW\*(C`SaveSub1\*(C', any changes to
\f(CW$ref will be tracked by the pointer \f(CW\*(C`rememberSub\*(C'. This means that
whenever \f(CW\*(C`CallSavedSub1\*(C' gets called, it will attempt to execute the
code which is referenced by the SV* \f(CW\*(C`rememberSub\*(C'.  In this case
though, it now refers to the integer \f(CW47, so expect Perl to complain
loudly.

A similar but more subtle problem is illustrated with this code:

.Vb 4
    $ref = \fred;
    SaveSub1($ref);
    $ref = \joe;
    CallSavedSub1();
.Ve

This time whenever \f(CW\*(C`CallSavedSub1\*(C' gets called it will execute the Perl
subroutine \f(CW\*(C`joe\*(C' (assuming it exists) rather than \f(CW\*(C`fred\*(C' as was
originally requested in the call to \f(CW\*(C`SaveSub1\*(C'.

To get around these problems it is necessary to take a full copy of the
\s-1SV.\s0  The code below shows \f(CW\*(C`SaveSub2\*(C' modified to do that.

.Vb 2
    /* this isn\*(Aqt thread-safe */
    static SV * keepSub = (SV*)NULL;

    void
    SaveSub2(name)
        SV *    name
        CODE:
        /* Take a copy of the callback */
        if (keepSub == (SV*)NULL)
            /* First time, so create a new SV */
            keepSub = newSVsv(name);
        else
            /* Been here before, so overwrite */
            SvSetSV(keepSub, name);

    void
    CallSavedSub2()
        CODE:
        PUSHMARK(SP);
        call_sv(keepSub, G_DISCARD|G_NOARGS);
.Ve

To avoid creating a new \s-1SV\s0 every time \f(CW\*(C`SaveSub2\*(C' is called,
the function first checks to see if it has been called before.  If not,
then space for a new \s-1SV\s0 is allocated and the reference to the Perl
subroutine \f(CW\*(C`name\*(C' is copied to the variable \f(CW\*(C`keepSub\*(C' in one
operation using \f(CW\*(C`newSVsv\*(C'.  Thereafter, whenever \f(CW\*(C`SaveSub2\*(C' is called,
the existing \s-1SV,\s0 \f(CW\*(C`keepSub\*(C', is overwritten with the new value using
\f(CW\*(C`SvSetSV\*(C'.

Note: using a static or global variable to store the \s-1SV\s0 isn't
thread-safe.  You can either use the \f(CW\*(C`MY_CXT\*(C' mechanism documented in
\*(L"Safely Storing Static Data in \s-1XS\*(R"\s0 in perlxs which is fast, or store the
values in perl global variables, using **get_sv()**, which is much slower.

### Using call_argv

Subsection "Using call_argv"
Here is a Perl subroutine which prints whatever parameters are passed
to it.

.Vb 3
    sub PrintList
    \{
        my(@list) = @_;

        foreach (@list) \{ print "$_\\n" \}
    \}
.Ve

And here is an example of *call_argv* which will call
*PrintList*.

.Vb 1
    static char * words[] = \{"alpha", "beta", "gamma", "delta", NULL\};

    static void
    call_PrintList()
    \{
        call_argv("PrintList", G_DISCARD, words);
    \}
.Ve

Note that it is not necessary to call \f(CW\*(C`PUSHMARK\*(C' in this instance.
This is because *call_argv* will do it for you.

### Using call_method

Subsection "Using call_method"
Consider the following Perl code:

.Vb 2
    \{
        package Mine;

        sub new
        \{
            my($type) = shift;
            bless [@_]
        \}

        sub Display
        \{
            my ($self, $index) = @_;
            print "$index: $$self[$index]\\n";
        \}

        sub PrintID
        \{
            my($class) = @_;
            print "This is Class $class version 1.0\\n";
        \}
    \}
.Ve

It implements just a very simple class to manage an array.  Apart from
the constructor, \f(CW\*(C`new\*(C', it declares methods, one static and one
virtual. The static method, \f(CW\*(C`PrintID\*(C', prints out simply the class
name and a version number. The virtual method, \f(CW\*(C`Display\*(C', prints out a
single element of the array.  Here is an all-Perl example of using it.

.Vb 3
    $a = Mine->new(\*(Aqred\*(Aq, \*(Aqgreen\*(Aq, \*(Aqblue\*(Aq);
    $a->Display(1);
    Mine->PrintID;
.Ve

will print

.Vb 2
    1: green
    This is Class Mine version 1.0
.Ve

Calling a Perl method from C is fairly straightforward. The following
things are required:

- \(bu
A reference to the object for a virtual method or the name of the class
for a static method

- \(bu
The name of the method

- \(bu
Any other parameters specific to the method

Here is a simple \s-1XSUB\s0 which illustrates the mechanics of calling both
the \f(CW\*(C`PrintID\*(C' and \f(CW\*(C`Display\*(C' methods from C.

.Vb 11
    void
    call_Method(ref, method, index)
        SV *    ref
        char *  method
        int             index
        CODE:
        PUSHMARK(SP);
        EXTEND(SP, 2);
        PUSHs(ref);
        PUSHs(sv_2mortal(newSViv(index)));
        PUTBACK;

        call_method(method, G_DISCARD);

    void
    call_PrintID(class, method)
        char *  class
        char *  method
        CODE:
        PUSHMARK(SP);
        XPUSHs(sv_2mortal(newSVpv(class, 0)));
        PUTBACK;

        call_method(method, G_DISCARD);
.Ve

So the methods \f(CW\*(C`PrintID\*(C' and \f(CW\*(C`Display\*(C' can be invoked like this:

.Vb 3
    $a = Mine->new(\*(Aqred\*(Aq, \*(Aqgreen\*(Aq, \*(Aqblue\*(Aq);
    call_Method($a, \*(AqDisplay\*(Aq, 1);
    call_PrintID(\*(AqMine\*(Aq, \*(AqPrintID\*(Aq);
.Ve

The only thing to note is that, in both the static and virtual methods,
the method name is not passed via the stack\*(--it is used as the first
parameter to *call_method*.

### Using \s-1GIMME_V\s0

Subsection "Using GIMME_V"
Here is a trivial \s-1XSUB\s0 which prints the context in which it is
currently executing.

.Vb 10
    void
    PrintContext()
        CODE:
        U8 gimme = GIMME_V;
        if (gimme == G_VOID)
            printf ("Context is Void\\n");
        else if (gimme == G_SCALAR)
            printf ("Context is Scalar\\n");
        else
            printf ("Context is Array\\n");
.Ve

And here is some Perl to test it.

.Vb 3
    PrintContext;
    $a = PrintContext;
    @a = PrintContext;
.Ve

The output from that will be

.Vb 3
    Context is Void
    Context is Scalar
    Context is Array
.Ve

### Using Perl to Dispose of Temporaries

Subsection "Using Perl to Dispose of Temporaries"
In the examples given to date, any temporaries created in the callback
(i.e., parameters passed on the stack to the *call_** function or
values returned via the stack) have been freed by one of these methods:

- \(bu
Specifying the G_DISCARD flag with *call_**

- \(bu
Explicitly using the \f(CW\*(C`ENTER\*(C'/\f(CW\*(C`SAVETMPS\*(C'--\f(CW\*(C`FREETMPS\*(C'/\f(CW\*(C`LEAVE\*(C' pairing

There is another method which can be used, namely letting Perl do it
for you automatically whenever it regains control after the callback
has terminated.  This is done by simply not using the

.Vb 5
    ENTER;
    SAVETMPS;
    ...
    FREETMPS;
    LEAVE;
.Ve

sequence in the callback (and not, of course, specifying the G_DISCARD
flag).

If you are going to use this method you have to be aware of a possible
memory leak which can arise under very specific circumstances.  To
explain these circumstances you need to know a bit about the flow of
control between Perl and the callback routine.

The examples given at the start of the document (an error handler and
an event driven program) are typical of the two main sorts of flow
control that you are likely to encounter with callbacks.  There is a
very important distinction between them, so pay attention.

In the first example, an error handler, the flow of control could be as
follows.  You have created an interface to an external library.
Control can reach the external library like this

.Vb 1
    perl --> XSUB --> external library
.Ve

Whilst control is in the library, an error condition occurs. You have
previously set up a Perl callback to handle this situation, so it will
get executed. Once the callback has finished, control will drop back to
Perl again.  Here is what the flow of control will be like in that
situation

.Vb 7
    perl --> XSUB --> external library
                      ...
                      error occurs
                      ...
                      external library --> call_* --> perl
                                                          |
    perl <-- XSUB <-- external library <-- call_* <----+
.Ve

After processing of the error using *call_** is completed,
control reverts back to Perl more or less immediately.

In the diagram, the further right you go the more deeply nested the
scope is.  It is only when control is back with perl on the extreme
left of the diagram that you will have dropped back to the enclosing
scope and any temporaries you have left hanging around will be freed.

In the second example, an event driven program, the flow of control
will be more like this

.Vb 10
    perl --> XSUB --> event handler
                      ...
                      event handler --> call_* --> perl
                                                       |
                      event handler <-- call_* <----+
                      ...
                      event handler --> call_* --> perl
                                                       |
                      event handler <-- call_* <----+
                      ...
                      event handler --> call_* --> perl
                                                       |
                      event handler <-- call_* <----+
.Ve

In this case the flow of control can consist of only the repeated
sequence

.Vb 1
    event handler --> call_* --> perl
.Ve

for practically the complete duration of the program.  This means that
control may *never* drop back to the surrounding scope in Perl at the
extreme left.

So what is the big problem? Well, if you are expecting Perl to tidy up
those temporaries for you, you might be in for a long wait.  For Perl
to dispose of your temporaries, control must drop back to the
enclosing scope at some stage.  In the event driven scenario that may
never happen.  This means that, as time goes on, your program will
create more and more temporaries, none of which will ever be freed. As
each of these temporaries consumes some memory your program will
eventually consume all the available memory in your system\*(--kapow!

So here is the bottom line\*(--if you are sure that control will revert
back to the enclosing Perl scope fairly quickly after the end of your
callback, then it isn't absolutely necessary to dispose explicitly of
any temporaries you may have created. Mind you, if you are at all
uncertain about what to do, it doesn't do any harm to tidy up anyway.

### Strategies for Storing Callback Context Information

Subsection "Strategies for Storing Callback Context Information"
Potentially one of the trickiest problems to overcome when designing a
callback interface can be figuring out how to store the mapping between
the C callback function and the Perl equivalent.

To help understand why this can be a real problem first consider how a
callback is set up in an all C environment.  Typically a C \s-1API\s0 will
provide a function to register a callback.  This will expect a pointer
to a function as one of its parameters.  Below is a call to a
hypothetical function \f(CW\*(C`register_fatal\*(C' which registers the C function
to get called when a fatal error occurs.

.Vb 1
    register_fatal(cb1);
.Ve

The single parameter \f(CW\*(C`cb1\*(C' is a pointer to a function, so you must
have defined \f(CW\*(C`cb1\*(C' in your code, say something like this

.Vb 6
    static void
    cb1()
    \{
        printf ("Fatal Error\\n");
        exit(1);
    \}
.Ve

Now change that to call a Perl subroutine instead

.Vb 1
    static SV * callback = (SV*)NULL;

    static void
    cb1()
    \{
        dSP;

        PUSHMARK(SP);

        /* Call the Perl sub to process the callback */
        call_sv(callback, G_DISCARD);
    \}


    void
    register_fatal(fn)
        SV *    fn
        CODE:
        /* Remember the Perl sub */
        if (callback == (SV*)NULL)
            callback = newSVsv(fn);
        else
            SvSetSV(callback, fn);

        /* register the callback with the external library */
        register_fatal(cb1);
.Ve

where the Perl equivalent of \f(CW\*(C`register_fatal\*(C' and the callback it
registers, \f(CW\*(C`pcb1\*(C', might look like this

.Vb 2
    # Register the sub pcb1
    register_fatal(\pcb1);

    sub pcb1
    \{
        die "I\*(Aqm dying...\\n";
    \}
.Ve

The mapping between the C callback and the Perl equivalent is stored in
the global variable \f(CW\*(C`callback\*(C'.

This will be adequate if you ever need to have only one callback
registered at any time. An example could be an error handler like the
code sketched out above. Remember though, repeated calls to
\f(CW\*(C`register_fatal\*(C' will replace the previously registered callback
function with the new one.

Say for example you want to interface to a library which allows asynchronous
file i/o.  In this case you may be able to register a callback whenever
a read operation has completed. To be of any use we want to be able to
call separate Perl subroutines for each file that is opened.  As it
stands, the error handler example above would not be adequate as it
allows only a single callback to be defined at any time. What we
require is a means of storing the mapping between the opened file and
the Perl subroutine we want to be called for that file.

Say the i/o library has a function \f(CW\*(C`asynch_read\*(C' which associates a C
function \f(CW\*(C`ProcessRead\*(C' with a file handle \f(CW\*(C`fh\*(C'--this assumes that it
has also provided some routine to open the file and so obtain the file
handle.

.Vb 1
    asynch_read(fh, ProcessRead)
.Ve

This may expect the C *ProcessRead* function of this form

.Vb 7
    void
    ProcessRead(fh, buffer)
    int fh;
    char *      buffer;
    \{
         ...
    \}
.Ve

To provide a Perl interface to this library we need to be able to map
between the \f(CW\*(C`fh\*(C' parameter and the Perl subroutine we want called.  A
hash is a convenient mechanism for storing this mapping.  The code
below shows a possible implementation

.Vb 1
    static HV * Mapping = (HV*)NULL;

    void
    asynch_read(fh, callback)
        int     fh
        SV *    callback
        CODE:
        /* If the hash doesn\*(Aqt already exist, create it */
        if (Mapping == (HV*)NULL)
            Mapping = newHV();

        /* Save the fh -> callback mapping */
        hv_store(Mapping, (char*)&fh, sizeof(fh), newSVsv(callback), 0);

        /* Register with the C Library */
        asynch_read(fh, asynch_read_if);
.Ve

and \f(CW\*(C`asynch_read_if\*(C' could look like this

.Vb 7
    static void
    asynch_read_if(fh, buffer)
    int fh;
    char *      buffer;
    \{
        dSP;
        SV ** sv;

        /* Get the callback associated with fh */
        sv =  hv_fetch(Mapping, (char*)&fh , sizeof(fh), FALSE);
        if (sv == (SV**)NULL)
            croak("Internal error...\\n");

        PUSHMARK(SP);
        EXTEND(SP, 2);
        PUSHs(sv_2mortal(newSViv(fh)));
        PUSHs(sv_2mortal(newSVpv(buffer, 0)));
        PUTBACK;

        /* Call the Perl sub */
        call_sv(*sv, G_DISCARD);
    \}
.Ve

For completeness, here is \f(CW\*(C`asynch_close\*(C'.  This shows how to remove
the entry from the hash \f(CW\*(C`Mapping\*(C'.

.Vb 6
    void
    asynch_close(fh)
        int     fh
        CODE:
        /* Remove the entry from the hash */
        (void) hv_delete(Mapping, (char*)&fh, sizeof(fh), G_DISCARD);

        /* Now call the real asynch_close */
        asynch_close(fh);
.Ve

So the Perl interface would look like this

.Vb 4
    sub callback1
    \{
        my($handle, $buffer) = @_;
    \}

    # Register the Perl callback
    asynch_read($fh, \callback1);

    asynch_close($fh);
.Ve

The mapping between the C callback and Perl is stored in the global
hash \f(CW\*(C`Mapping\*(C' this time. Using a hash has the distinct advantage that
it allows an unlimited number of callbacks to be registered.

What if the interface provided by the C callback doesn't contain a
parameter which allows the file handle to Perl subroutine mapping?  Say
in the asynchronous i/o package, the callback function gets passed only
the \f(CW\*(C`buffer\*(C' parameter like this

.Vb 6
    void
    ProcessRead(buffer)
    char *      buffer;
    \{
        ...
    \}
.Ve

Without the file handle there is no straightforward way to map from the
C callback to the Perl subroutine.

In this case a possible way around this problem is to predefine a
series of C functions to act as the interface to Perl, thus

.Vb 3
    #define MAX_CB              3
    #define NULL_HANDLE -1
    typedef void (*FnMap)();

    struct MapStruct \{
        FnMap    Function;
        SV *     PerlSub;
        int      Handle;
      \};

    static void  fn1();
    static void  fn2();
    static void  fn3();

    static struct MapStruct Map [MAX_CB] =
        \{
            \{ fn1, NULL, NULL_HANDLE \},
            \{ fn2, NULL, NULL_HANDLE \},
            \{ fn3, NULL, NULL_HANDLE \}
        \};

    static void
    Pcb(index, buffer)
    int index;
    char * buffer;
    \{
        dSP;

        PUSHMARK(SP);
        XPUSHs(sv_2mortal(newSVpv(buffer, 0)));
        PUTBACK;

        /* Call the Perl sub */
        call_sv(Map[index].PerlSub, G_DISCARD);
    \}

    static void
    fn1(buffer)
    char * buffer;
    \{
        Pcb(0, buffer);
    \}

    static void
    fn2(buffer)
    char * buffer;
    \{
        Pcb(1, buffer);
    \}

    static void
    fn3(buffer)
    char * buffer;
    \{
        Pcb(2, buffer);
    \}

    void
    array_asynch_read(fh, callback)
        int             fh
        SV *    callback
        CODE:
        int index;
        int null_index = MAX_CB;

        /* Find the same handle or an empty entry */
        for (index = 0; index < MAX_CB; ++index)
        \{
            if (Map[index].Handle == fh)
                break;

            if (Map[index].Handle == NULL_HANDLE)
                null_index = index;
        \}

        if (index == MAX_CB && null_index == MAX_CB)
            croak ("Too many callback functions registered\\n");

        if (index == MAX_CB)
            index = null_index;

        /* Save the file handle */
        Map[index].Handle = fh;

        /* Remember the Perl sub */
        if (Map[index].PerlSub == (SV*)NULL)
            Map[index].PerlSub = newSVsv(callback);
        else
            SvSetSV(Map[index].PerlSub, callback);

        asynch_read(fh, Map[index].Function);

    void
    array_asynch_close(fh)
        int     fh
        CODE:
        int index;

        /* Find the file handle */
        for (index = 0; index < MAX_CB; ++ index)
            if (Map[index].Handle == fh)
                break;

        if (index == MAX_CB)
            croak ("could not close fh %d\\n", fh);

        Map[index].Handle = NULL_HANDLE;
        SvREFCNT_dec(Map[index].PerlSub);
        Map[index].PerlSub = (SV*)NULL;

        asynch_close(fh);
.Ve

In this case the functions \f(CW\*(C`fn1\*(C', \f(CW\*(C`fn2\*(C', and \f(CW\*(C`fn3\*(C' are used to
remember the Perl subroutine to be called. Each of the functions holds
a separate hard-wired index which is used in the function \f(CW\*(C`Pcb\*(C' to
access the \f(CW\*(C`Map\*(C' array and actually call the Perl subroutine.

There are some obvious disadvantages with this technique.

Firstly, the code is considerably more complex than with the previous
example.

Secondly, there is a hard-wired limit (in this case 3) to the number of
callbacks that can exist simultaneously. The only way to increase the
limit is by modifying the code to add more functions and then
recompiling.  None the less, as long as the number of functions is
chosen with some care, it is still a workable solution and in some
cases is the only one available.

To summarize, here are a number of possible methods for you to consider
for storing the mapping between C and the Perl callback

- 1. Ignore the problem - Allow only 1 callback
Item "1. Ignore the problem - Allow only 1 callback"
For a lot of situations, like interfacing to an error handler, this may
be a perfectly adequate solution.

- 2. Create a sequence of callbacks - hard wired limit
Item "2. Create a sequence of callbacks - hard wired limit"
If it is impossible to tell from the parameters passed back from the C
callback what the context is, then you may need to create a sequence of C
callback interface functions, and store pointers to each in an array.

- 3. Use a parameter to map to the Perl callback
Item "3. Use a parameter to map to the Perl callback"
A hash is an ideal mechanism to store the mapping between C and Perl.

### Alternate Stack Manipulation

Subsection "Alternate Stack Manipulation"
Although I have made use of only the \f(CW\*(C`POP*\*(C' macros to access values
returned from Perl subroutines, it is also possible to bypass these
macros and read the stack using the \f(CW\*(C`ST\*(C' macro (See perlxs for a
full description of the \f(CW\*(C`ST\*(C' macro).

Most of the time the \f(CW\*(C`POP*\*(C' macros should be adequate; the main
problem with them is that they force you to process the returned values
in sequence. This may not be the most suitable way to process the
values in some cases. What we want is to be able to access the stack in
a random order. The \f(CW\*(C`ST\*(C' macro as used when coding an \s-1XSUB\s0 is ideal
for this purpose.

The code below is the example given in the section \*(L"Returning a List
of Values\*(R" recoded to use \f(CW\*(C`ST\*(C' instead of \f(CW\*(C`POP*\*(C'.

.Vb 8
    static void
    call_AddSubtract2(a, b)
    int a;
    int b;
    \{
        dSP;
        I32 ax;
        int count;

        ENTER;
        SAVETMPS;

        PUSHMARK(SP);
        EXTEND(SP, 2);
        PUSHs(sv_2mortal(newSViv(a)));
        PUSHs(sv_2mortal(newSViv(b)));
        PUTBACK;

        count = call_pv("AddSubtract", G_ARRAY);

        SPAGAIN;
        SP -= count;
        ax = (SP - PL_stack_base) + 1;

        if (count != 2)
            croak("Big trouble\\n");

        printf ("%d + %d = %d\\n", a, b, SvIV(ST(0)));
        printf ("%d - %d = %d\\n", a, b, SvIV(ST(1)));

        PUTBACK;
        FREETMPS;
        LEAVE;
    \}
.Ve

Notes

- 1.
Notice that it was necessary to define the variable \f(CW\*(C`ax\*(C'.  This is
because the \f(CW\*(C`ST\*(C' macro expects it to exist.  If we were in an \s-1XSUB\s0 it
would not be necessary to define \f(CW\*(C`ax\*(C' as it is already defined for
us.

- 2.
The code
.Sp
.Vb 3
        SPAGAIN;
        SP -= count;
        ax = (SP - PL_stack_base) + 1;
.Ve
.Sp
sets the stack up so that we can use the \f(CW\*(C`ST\*(C' macro.

- 3.
Unlike the original coding of this example, the returned
values are not accessed in reverse order.  So \f(CWST(0) refers to the
first value returned by the Perl subroutine and \f(CW\*(C`ST(count-1)\*(C'
refers to the last.

### Creating and Calling an Anonymous Subroutine in C

Subsection "Creating and Calling an Anonymous Subroutine in C"
As we've already shown, \f(CW\*(C`call_sv\*(C' can be used to invoke an
anonymous subroutine.  However, our example showed a Perl script
invoking an \s-1XSUB\s0 to perform this operation.  Let's see how it can be
done inside our C code:

.Vb 1
 ...

 SV *cvrv
    = eval_pv("sub \{
                print \*(AqYou will not find me cluttering any namespace!\*(Aq
               \}", TRUE);

 ...

 call_sv(cvrv, G_VOID|G_NOARGS);
.Ve

\f(CW\*(C`eval_pv\*(C' is used to compile the anonymous subroutine, which
will be the return value as well (read more about \f(CW\*(C`eval_pv\*(C' in
\*(L"eval_pv\*(R" in perlapi).  Once this code reference is in hand, it
can be mixed in with all the previous examples we've shown.

## LIGHTWEIGHT CALLBACKS

Header "LIGHTWEIGHT CALLBACKS"
Sometimes you need to invoke the same subroutine repeatedly.
This usually happens with a function that acts on a list of
values, such as Perl's built-in **sort()**. You can pass a
comparison function to **sort()**, which will then be invoked
for every pair of values that needs to be compared. The **first()**
and **reduce()** functions from List::Util follow a similar
pattern.

In this case it is possible to speed up the routine (often
quite substantially) by using the lightweight callback \s-1API.\s0
The idea is that the calling context only needs to be
created and destroyed once, and the sub can be called
arbitrarily many times in between.

It is usual to pass parameters using global variables (typically
\f(CW$_ for one parameter, or \f(CW$a and \f(CW$b for two parameters) rather
than via \f(CW@_. (It is possible to use the \f(CW@_ mechanism if you know
what you're doing, though there is as yet no supported \s-1API\s0 for
it. It's also inherently slower.)

The pattern of macro calls is like this:

.Vb 3
    dMULTICALL;                 /* Declare local variables */
    U8 gimme = G_SCALAR;        /* context of the call: G_SCALAR,
                                 * G_ARRAY, or G_VOID */

    PUSH_MULTICALL(cv);         /* Set up the context for calling cv,
                                   and set local vars appropriately */

    /* loop */ \{
        /* set the value(s) af your parameter variables */
        MULTICALL;              /* Make the actual call */
    \} /* end of loop */

    POP_MULTICALL;              /* Tear down the calling context */
.Ve

For some concrete examples, see the implementation of the
**first()** and **reduce()** functions of List::Util 1.18. There you
will also find a header file that emulates the multicall \s-1API\s0
on older versions of perl.

## SEE ALSO

Header "SEE ALSO"
perlxs, perlguts, perlembed

## AUTHOR

Header "AUTHOR"
Paul Marquess

Special thanks to the following people who assisted in the creation of
the document.

Jeff Okamoto, Tim Bunce, Nick Gianniotis, Steve Kelem, Gurusamy Sarathy
and Larry Wall.

## DATE

Header "DATE"
Last updated for perl 5.23.1.
