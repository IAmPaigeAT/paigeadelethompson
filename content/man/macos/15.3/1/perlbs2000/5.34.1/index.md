+++
detected_package_version = "5.34.1"
manpage_name = "perlbs2000"
title = "perlbs2000(1)"
manpage_section = "1"
operating_system_version = "15.3"
author = "None Specified"
operating_system = "macos"
date = "2022-02-19"
description = "This is a ported perl for the s-1POSIXs0 subsystem in s-1BS2000 VERSION OSD V3.1As0 or later.  It may work on other versions, but we started porting and testing it with 3.1A and are currently using Version V4.0A. You may need the following s-1GNUs0..."
keywords = ["header", "see", "also", "s-1install", "s0", "perlport", "mailing", "list", "subsection", "if", "you", "are", "interested", "in", "the", "z", "os", "formerly", "known", "as", "s-1os", "390", "and", "posix-bc", "s-1bs2000", "ports", "of", "perl", "then", "perl-mvs", "to", "subscribe", "send", "an", "empty", "message", "perl-mvs-subscribe", "org", "vb", "1", "https", "lists", "html", "ve", "there", "web", "archives", "at", "www", "nntp", "group", "mvs", "history", "this", "document", "was", "originally", "written", "by", "thomas", "dorner", "for", "5", "005", "release", "podified", "6", "11", "july", "2000"]
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLBS2000 1"
PERLBS2000 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlbs2000 - building and installing Perl for BS2000.

This document needs to be updated, but we don't know what it should say.
Please submit comments to <https://github.com/Perl/perl5/issues>.

## SYNOPSIS

Header "SYNOPSIS"
This document will help you Configure, build, test and install Perl
on \s-1BS2000\s0 in the \s-1POSIX\s0 subsystem.

## DESCRIPTION

Header "DESCRIPTION"
This is a ported perl for the \s-1POSIX\s0 subsystem in \s-1BS2000 VERSION OSD
V3.1A\s0 or later.  It may work on other versions, but we started porting
and testing it with 3.1A and are currently using Version V4.0A.

You may need the following \s-1GNU\s0 programs in order to install perl:

### gzip on \s-1BS2000\s0

Subsection "gzip on BS2000"
We used version 1.2.4, which could be installed out of the box with
one failure during 'make check'.

### bison on \s-1BS2000\s0

Subsection "bison on BS2000"
The yacc coming with \s-1BS2000 POSIX\s0 didn't work for us.  So we had to
use bison.  We had to make a few changes to perl in order to use the
pure (reentrant) parser of bison.  We used version 1.25, but we had to
add a few changes due to \s-1EBCDIC.\s0  See below for more details
concerning yacc.

### Unpacking Perl Distribution on \s-1BS2000\s0

Subsection "Unpacking Perl Distribution on BS2000"
To extract an \s-1ASCII\s0 tar archive on \s-1BS2000 POSIX\s0 you need an \s-1ASCII\s0
filesystem (we used the mountpoint /usr/local/ascii for this).  Now
you extract the archive in the \s-1ASCII\s0 filesystem without
I/O-conversion:

cd /usr/local/ascii
export IO_CONVERSION=NO
gunzip < /usr/local/src/perl.tar.gz | pax -r

You may ignore the error message for the first element of the archive
(this doesn't look like a tar archive / skipping to next file...),
it's only the directory which will be created automatically anyway.

After extracting the archive you copy the whole directory tree to your
\s-1EBCDIC\s0 filesystem.  **This time you use I/O-conversion**:

cd /usr/local/src
IO_CONVERSION=YES
cp -r /usr/local/ascii/perl5.005_02 ./

### Compiling Perl on \s-1BS2000\s0

Subsection "Compiling Perl on BS2000"
There is a \*(L"hints\*(R" file for \s-1BS2000\s0 called hints.posix-bc (because
posix-bc is the \s-1OS\s0 name given by `uname`) that specifies the correct
values for most things.  The major problem is (of course) the \s-1EBCDIC\s0
character set.  We have german \s-1EBCDIC\s0 version.

Because of our problems with the native yacc we used \s-1GNU\s0 bison to
generate a pure (=reentrant) parser for perly.y.  So our yacc is
really the following script:

-----8<-----/usr/local/bin/yacc-----8<-----
#! /usr/bin/sh

# Bison as a reentrant yacc:

# save parameters:
params="\*(L"
while [[ $# -gt 1 ]]; do
    params=\*(R"$params \f(CW$1"
    shift
done

# add flag \f(CW%pure_parser:

tmpfile=/tmp/bison.$$.y
echo \f(CW%pure_parser > \f(CW$tmpfile
cat \f(CW$1 >> \f(CW$tmpfile

# call bison:

echo \*(L"/usr/local/bin/bison --yacc \f(CW$params \f(CW$1\\t\\t\\t(Pure Parser)\*(R"
/usr/local/bin/bison --yacc \f(CW$params \f(CW$tmpfile

# cleanup:

rm -f \f(CW$tmpfile
-----8<----------8<-----

We still use the normal yacc for a2p.y though!!!  We made a softlink
called byacc to distinguish between the two versions:

ln -s /usr/bin/yacc /usr/local/bin/byacc

We build perl using \s-1GNU\s0 make.  We tried the native make once and it
worked too.

### Testing Perl on \s-1BS2000\s0

Subsection "Testing Perl on BS2000"
We still got a few errors during \f(CW\*(C`make test\*(C'.  Some of them are the
result of using bison.  Bison prints *parser error* instead of \fIsyntax
error, so we may ignore them.  The following list shows
our errors, your results may differ:

op/numconvert.......FAILED tests 1409-1440
op/regexp...........FAILED tests 483, 496
op/regexp_noamp.....FAILED tests 483, 496
pragma/overload.....FAILED tests 152-153, 170-171
pragma/warnings.....FAILED tests 14, 82, 129, 155, 192, 205, 207
lib/bigfloat........FAILED tests 351-352, 355
lib/bigfltpm........FAILED tests 354-355, 358
lib/complex.........FAILED tests 267, 487
lib/dumper..........FAILED tests 43, 45
Failed 11/231 test scripts, 95.24% okay. 57/10595 subtests failed, 99.46% okay.

### Installing Perl on \s-1BS2000\s0

Subsection "Installing Perl on BS2000"
We have no nroff on \s-1BS2000 POSIX\s0 (yet), so we ignored any errors while
installing the documentation.

### Using Perl in the Posix-Shell of \s-1BS2000\s0

Subsection "Using Perl in the Posix-Shell of BS2000"
\s-1BS2000 POSIX\s0 doesn't support the shebang notation
(\f(CW\*(C`#!/usr/local/bin/perl\*(C'), so you have to use the following lines
instead:

: # use perl
    eval 'exec /usr/local/bin/perl -S \f(CW$0 $\{1+\*(L"$@\*(R"\}'
        if 0; # ^ Run only under a shell
.ie n .SS "Using Perl in ""native"" \s-1BS2000\s0"
.el .SS "Using Perl in ``native'' \s-1BS2000\s0"
Subsection "Using Perl in native BS2000"
We don't have much experience with this yet, but try the following:

Copy your Perl executable to a \s-1BS2000 LLM\s0 using bs2cp:

\f(CW\*(C`bs2cp /usr/local/bin/perl \*(Aqbs2:perl(perl,l)\*(Aq\*(C'

Now you can start it with the following (\s-1SDF\s0) command:

\f(CW\*(C`/START-PROG FROM-FILE=*MODULE(PERL,PERL),PROG-MODE=*ANY,RUN-MODE=*ADV\*(C'

First you get the \s-1BS2000\s0 commandline prompt ('*').  Here you may enter
your parameters, e.g. \f(CW\*(C`-e \*(Aqprint "Hello World!\\\\n";\*(Aq\*(C' (note the
double backslash!) or \f(CW\*(C`-w\*(C' and the name of your Perl script.
Filenames starting with \f(CW\*(C`/\*(C' are searched in the Posix filesystem,
others are searched in the \s-1BS2000\s0 filesystem.  You may even use
wildcards if you put a \f(CW\*(C`%\*(C' in front of your filename (e.g. \f(CW\*(C`-w
checkfiles.pl %*.c\*(C').  Read your C/\*(C+ manual for additional
possibilities of the commandline prompt (look for
PARAMETER-PROMPTING).

### Floating point anomalies on \s-1BS2000\s0

Subsection "Floating point anomalies on BS2000"
There appears to be a bug in the floating point implementation on \s-1BS2000 POSIX\s0
systems such that calling **int()** on the product of a number and a small
magnitude number is not the same as calling **int()** on the quotient of
that number and a large magnitude number.  For example, in the following
Perl code:

.Vb 4
    my $x = 100000.0;
    my $y = int($x * 1e-5) * 1e5; # \*(Aq0\*(Aq
    my $z = int($x / 1e+5) * 1e5;  # \*(Aq100000\*(Aq
    print "\\$y is $y and \\$z is $z\\n"; # $y is 0 and $z is 100000
.Ve

Although one would expect the quantities \f(CW$y and \f(CW$z to be the same and equal
to 100000 they will differ and instead will be 0 and 100000 respectively.

### Using PerlIO and different encodings on \s-1ASCII\s0 and \s-1EBCDIC\s0 partitions

Subsection "Using PerlIO and different encodings on ASCII and EBCDIC partitions"
Since version 5.8 Perl uses the new PerlIO on \s-1BS2000.\s0  This enables
you using different encodings per \s-1IO\s0 channel.  For example you may use

.Vb 9
    use Encode;
    open($f, ">:encoding(ascii)", "test.ascii");
    print $f "Hello World!\\n";
    open($f, ">:encoding(posix-bc)", "test.ebcdic");
    print $f "Hello World!\\n";
    open($f, ">:encoding(latin1)", "test.latin1");
    print $f "Hello World!\\n";
    open($f, ">:encoding(utf8)", "test.utf8");
    print $f "Hello World!\\n";
.Ve

to get two files containing \*(L"Hello World!\\n\*(R" in \s-1ASCII, EBCDIC, ISO\s0
Latin-1 (in this example identical to \s-1ASCII\s0) respective UTF-EBCDIC (in
this example identical to normal \s-1EBCDIC\s0).  See the documentation of
Encode::PerlIO for details.

As the PerlIO layer uses raw \s-1IO\s0 internally, all this totally ignores
the type of your filesystem (\s-1ASCII\s0 or \s-1EBCDIC\s0) and the \s-1IO_CONVERSION\s0
environment variable.  If you want to get the old behavior, that the
\s-1BS2000 IO\s0 functions determine conversion depending on the filesystem
PerlIO still is your friend.  You use \s-1IO_CONVERSION\s0 as usual and tell
Perl, that it should use the native \s-1IO\s0 layer:

.Vb 2
    export IO_CONVERSION=YES
    export PERLIO=stdio
.Ve

Now your \s-1IO\s0 would be \s-1ASCII\s0 on \s-1ASCII\s0 partitions and \s-1EBCDIC\s0 on \s-1EBCDIC\s0
partitions.  See the documentation of PerlIO (without \f(CW\*(C`Encode::\*(C'!)
for further possibilities.

## AUTHORS

Header "AUTHORS"
Thomas Dorner

## SEE ALSO

Header "SEE ALSO"
\s-1INSTALL\s0, perlport.

### Mailing list

Subsection "Mailing list"
If you are interested in the z/OS (formerly known as \s-1OS/390\s0)
and POSIX-BC (\s-1BS2000\s0) ports of Perl then see the perl-mvs mailing list.
To subscribe, send an empty message to perl-mvs-subscribe@perl.org.

See also:

.Vb 1
    https://lists.perl.org/list/perl-mvs.html
.Ve

There are web archives of the mailing list at:

.Vb 1
    https://www.nntp.perl.org/group/perl.mvs/
.Ve

## HISTORY

Header "HISTORY"
This document was originally written by Thomas Dorner for the 5.005
release of Perl.

This document was podified for the 5.6 release of perl 11 July 2000.
