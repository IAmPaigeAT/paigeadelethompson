+++
manpage_section = "1"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "how", "to", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information", "history", "written", "by", "gurusamy", "sarathy", "figsar", "activestate", "com", "with", "many", "contributions", "from", "porters", "send", "omissions", "or", "corrections", "fiperlbug"]
manpage_name = "perl5005delta"
date = "2022-02-19"
description = "This document describes differences between the 5.004 release and this one. Perl is now developed on two tracks: a maintenance track that makes small, safe updates to released production versions with emphasis on compatibility; and a development tr..."
author = "None Specified"
title = "perl5005delta(1)"
operating_system_version = "15.3"
operating_system = "macos"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5005DELTA 1"
PERL5005DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5005delta - what's new for perl5.005

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.004 release and this one.

## About the new versioning system

Header "About the new versioning system"
Perl is now developed on two tracks: a maintenance track that makes
small, safe updates to released production versions with emphasis on
compatibility; and a development track that pursues more aggressive
evolution.  Maintenance releases (which should be considered production
quality) have subversion numbers that run from \f(CW1 to \f(CW49, and
development releases (which should be considered \*(L"alpha\*(R" quality) run
from \f(CW50 to \f(CW99.

Perl 5.005 is the combined product of the new dual-track development
scheme.

## Incompatible Changes

Header "Incompatible Changes"

### \s-1WARNING:\s0  This version is not binary compatible with Perl 5.004.

Subsection "WARNING: This version is not binary compatible with Perl 5.004."
Starting with Perl 5.004_50 there were many deep and far-reaching changes
to the language internals.  If you have dynamically loaded extensions
that you built under perl 5.003 or 5.004, you can continue to use them
with 5.004, but you will need to rebuild and reinstall those extensions
to use them 5.005.  See *\s-1INSTALL\s0* for detailed instructions on how to
upgrade.

### Default installation structure has changed

Subsection "Default installation structure has changed"
The new Configure defaults are designed to allow a smooth upgrade from
5.004 to 5.005, but you should read *\s-1INSTALL\s0* for a detailed
discussion of the changes in order to adapt them to your system.

### Perl Source Compatibility

Subsection "Perl Source Compatibility"
When none of the experimental features are enabled, there should be
very few user-visible Perl source compatibility issues.

If threads are enabled, then some caveats apply. \f(CW@_ and \f(CW$_ become
lexical variables.  The effect of this should be largely transparent to
the user, but there are some boundary conditions under which user will
need to be aware of the issues.  For example, \f(CW\*(C`local(@_)\*(C' results in
a \*(L"Can't localize lexical variable \f(CW@_ ...\*(R" message.  This may be enabled
in a future version.

Some new keywords have been introduced.  These are generally expected to
have very little impact on compatibility.  See "New \f(CW\*(C`INIT\*(C' keyword",
"New \f(CW\*(C`lock\*(C' keyword", and "New \f(CW\*(C`qr//\*(C' operator".

Certain barewords are now reserved.  Use of these will provoke a warning
if you have asked for them with the \f(CW\*(C`-w\*(C' switch.
See "\f(CW\*(C`our\*(C' is now a reserved word".

### C Source Compatibility

Subsection "C Source Compatibility"
There have been a large number of changes in the internals to support
the new features in this release.

- \(bu
Core sources now require \s-1ANSI C\s0 compiler
.Sp
An \s-1ANSI C\s0 compiler is now **required** to build perl.  See *\s-1INSTALL\s0*.

- \(bu
All Perl global variables must now be referenced with an explicit prefix
.Sp
All Perl global variables that are visible for use by extensions now
have a \f(CW\*(C`PL_\*(C' prefix.  New extensions should \f(CW\*(C`not\*(C' refer to perl globals
by their unqualified names.  To preserve sanity, we provide limited
backward compatibility for globals that are being widely used like
\f(CW\*(C`sv_undef\*(C' and \f(CW\*(C`na\*(C' (which should now be written as \f(CW\*(C`PL_sv_undef\*(C',
\f(CW\*(C`PL_na\*(C' etc.)
.Sp
If you find that your \s-1XS\s0 extension does not compile anymore because a
perl global is not visible, try adding a \f(CW\*(C`PL_\*(C' prefix to the global
and rebuild.
.Sp
It is strongly recommended that all functions in the Perl \s-1API\s0 that don't
begin with \f(CW\*(C`perl\*(C' be referenced with a \f(CW\*(C`Perl_\*(C' prefix.  The bare function
names without the \f(CW\*(C`Perl_\*(C' prefix are supported with macros, but this
support may cease in a future release.
.Sp
See perlapi.

- \(bu
Enabling threads has source compatibility issues
.Sp
Perl built with threading enabled requires extensions to use the new
\f(CW\*(C`dTHR\*(C' macro to initialize the handle to access per-thread data.
If you see a compiler error that talks about the variable \f(CW\*(C`thr\*(C' not
being declared (when building a module that has \s-1XS\s0 code),  you need
to add \f(CW\*(C`dTHR;\*(C' at the beginning of the block that elicited the error.
.Sp
The \s-1API\s0 function \f(CW\*(C`perl_get_sv("@",GV_ADD)\*(C' should be used instead of
directly accessing perl globals as \f(CW\*(C`GvSV(errgv)\*(C'.  The \s-1API\s0 call is
backward compatible with existing perls and provides source compatibility
with threading is enabled.
.Sp
See \*(L"C Source Compatibility\*(R" for more information.

### Binary Compatibility

Subsection "Binary Compatibility"
This version is \s-1NOT\s0 binary compatible with older versions.  All extensions
will need to be recompiled.  Further binaries built with threads enabled
are incompatible with binaries built without.  This should largely be
transparent to the user, as all binary incompatible configurations have
their own unique architecture name, and extension binaries get installed at
unique locations.  This allows coexistence of several configurations in
the same directory hierarchy.  See *\s-1INSTALL\s0*.

### Security fixes may affect compatibility

Subsection "Security fixes may affect compatibility"
A few taint leaks and taint omissions have been corrected.  This may lead
to \*(L"failure\*(R" of scripts that used to work with older versions.  Compiling
with -DINCOMPLETE_TAINTS provides a perl with minimal amounts of changes
to the tainting behavior.  But note that the resulting perl will have
known insecurities.

Oneliners with the \f(CW\*(C`-e\*(C' switch do not create temporary files anymore.

### Relaxed new mandatory warnings introduced in 5.004

Subsection "Relaxed new mandatory warnings introduced in 5.004"
Many new warnings that were introduced in 5.004 have been made
optional.  Some of these warnings are still present, but perl's new
features make them less often a problem.  See \*(L"New Diagnostics\*(R".

### Licensing

Subsection "Licensing"
Perl has a new Social Contract for contributors.  See *Porting/Contract*.

The license included in much of the Perl documentation has changed.
Most of the Perl documentation was previously under the implicit \s-1GNU\s0
General Public License or the Artistic License (at the user's choice).
Now much of the documentation unambiguously states the terms under which
it may be distributed.  Those terms are in general much less restrictive
than the \s-1GNU GPL.\s0  See perl and the individual perl manpages listed
therein.

## Core Changes

Header "Core Changes"

### Threads

Subsection "Threads"
\s-1WARNING:\s0 Threading is considered an **experimental** feature.  Details of the
implementation may change without notice.  There are known limitations
and some bugs.  These are expected to be fixed in future versions.

See *\s-1README\s0.threads*.

### Compiler

Subsection "Compiler"
\s-1WARNING:\s0 The Compiler and related tools are considered **experimental**.
Features may change without notice, and there are known limitations
and bugs.  Since the compiler is fully external to perl, the default
configuration will build and install it.

The Compiler produces three different types of transformations of a
perl program.  The C backend generates C code that captures perl's state
just before execution begins.  It eliminates the compile-time overheads
of the regular perl interpreter, but the run-time performance remains
comparatively the same.  The \s-1CC\s0 backend generates optimized C code
equivalent to the code path at run-time.  The \s-1CC\s0 backend has greater
potential for big optimizations, but only a few optimizations are
implemented currently.  The Bytecode backend generates a platform
independent bytecode representation of the interpreter's state
just before execution.  Thus, the Bytecode back end also eliminates
much of the compilation overhead of the interpreter.

The compiler comes with several valuable utilities.

\f(CW\*(C`B::Lint\*(C' is an experimental module to detect and warn about suspicious
code, especially the cases that the \f(CW\*(C`-w\*(C' switch does not detect.

\f(CW\*(C`B::Deparse\*(C' can be used to demystify perl code, and understand
how perl optimizes certain constructs.

\f(CW\*(C`B::Xref\*(C' generates cross reference reports of all definition and use
of variables, subroutines and formats in a program.

\f(CW\*(C`B::Showlex\*(C' show the lexical variables used by a subroutine or file
at a glance.

\f(CW\*(C`perlcc\*(C' is a simple frontend for compiling perl.

See \f(CW\*(C`ext/B/README\*(C', B, and the respective compiler modules.

### Regular Expressions

Subsection "Regular Expressions"
Perl's regular expression engine has been seriously overhauled, and
many new constructs are supported.  Several bugs have been fixed.

Here is an itemized summary:

- Many new and improved optimizations
Item "Many new and improved optimizations"
Changes in the \s-1RE\s0 engine:
.Sp
.Vb 7
        Unneeded nodes removed;
        Substrings merged together;
        New types of nodes to process (SUBEXPR)* and similar expressions
            quickly, used if the SUBEXPR has no side effects and matches
            strings of the same length;
        Better optimizations by lookup for constant substrings;
        Better search for constants substrings anchored by $ ;
.Ve
.Sp
Changes in Perl code using \s-1RE\s0 engine:
.Sp
.Vb 5
        More optimizations to s/longer/short/;
        study() was not working;
        /blah/ may be optimized to an analogue of index() if $& $\` $\*(Aq not seen;
        Unneeded copying of matched-against string removed;
        Only matched part of the string is copying if $\` $\*(Aq were not seen;
.Ve

- Many bug fixes
Item "Many bug fixes"
Note that only the major bug fixes are listed here.  See *Changes* for others.
.Sp
.Vb 10
        Backtracking might not restore start of $3.
        No feedback if max count for * or + on "complex" subexpression
            was reached, similarly (but at compile time) for \{3,34567\}
        Primitive restrictions on max count introduced to decrease a
            possibility of a segfault;
        (ZERO-LENGTH)* could segfault;
        (ZERO-LENGTH)* was prohibited;
        Long REs were not allowed;
        /RE/g could skip matches at the same position after a
          zero-length match;
.Ve

- New regular expression constructs
Item "New regular expression constructs"
The following new syntax elements are supported:
.Sp
.Vb 8
        (?<=RE)
        (?<!RE)
        (?\{ CODE \})
        (?i-x)
        (?i:RE)
        (?(COND)YES_RE|NO_RE)
        (?>RE)
        \\z
.Ve

- New operator for precompiled regular expressions
Item "New operator for precompiled regular expressions"
See "New \f(CW\*(C`qr//\*(C' operator".

- Other improvements
Item "Other improvements"
.Vb 7
        Better debugging output (possibly with colors),
            even from non-debugging Perl;
        RE engine code now looks like C, not like assembler;
        Behaviour of RE modifiable by \`use re\*(Aq directive;
        Improved documentation;
        Test suite significantly extended;
        Syntax [:^upper:] etc., reserved inside character classes;
.Ve

- Incompatible changes
Item "Incompatible changes"
.Vb 4
        (?i) localized inside enclosing group;
        $( is not interpolated into RE any more;
        /RE/g may match at the same position (with non-zero length)
            after a zero-length match (bug fix).
.Ve

See perlre and perlop.

### Improved \fBmalloc()

Subsection "Improved malloc()"
See banner at the beginning of \f(CW\*(C`malloc.c\*(C' for details.

### Quicksort is internally implemented

Subsection "Quicksort is internally implemented"
Perl now contains its own highly optimized **qsort()** routine.  The new **qsort()**
is resistant to inconsistent comparison functions, so Perl's \f(CW\*(C`sort()\*(C' will
not provoke coredumps any more when given poorly written sort subroutines.
(Some C library \f(CW\*(C`qsort()\*(C's that were being used before used to have this
problem.)  In our testing, the new \f(CW\*(C`qsort()\*(C' required the minimal number
of pair-wise compares on average, among all known \f(CW\*(C`qsort()\*(C' implementations.

See \f(CW\*(C`perlfunc/sort\*(C'.

### Reliable signals

Subsection "Reliable signals"
Perl's signal handling is susceptible to random crashes, because signals
arrive asynchronously, and the Perl runtime is not reentrant at arbitrary
times.

However, one experimental implementation of reliable signals is available
when threads are enabled.  See \f(CW\*(C`Thread::Signal\*(C'.  Also see *\s-1INSTALL\s0* for
how to build a Perl capable of threads.

### Reliable stack pointers

Subsection "Reliable stack pointers"
The internals now reallocate the perl stack only at predictable times.
In particular, magic calls never trigger reallocations of the stack,
because all reentrancy of the runtime is handled using a \*(L"stack of stacks\*(R".
This should improve reliability of cached stack pointers in the internals
and in XSUBs.

### More generous treatment of carriage returns

Subsection "More generous treatment of carriage returns"
Perl used to complain if it encountered literal carriage returns in
scripts.  Now they are mostly treated like whitespace within program text.
Inside string literals and here documents, literal carriage returns are
ignored if they occur paired with linefeeds, or get interpreted as whitespace
if they stand alone.  This behavior means that literal carriage returns
in files should be avoided.  You can get the older, more compatible (but
less generous) behavior by defining the preprocessor symbol
\f(CW\*(C`PERL_STRICT_CR\*(C' when building perl.  Of course, all this has nothing
whatever to do with how escapes like \f(CW\*(C`\\r\*(C' are handled within strings.

Note that this doesn't somehow magically allow you to keep all text files
in \s-1DOS\s0 format.  The generous treatment only applies to files that perl
itself parses.  If your C compiler doesn't allow carriage returns in
files, you may still be unable to build modules that need a C compiler.

### Memory leaks

Subsection "Memory leaks"
\f(CW\*(C`substr\*(C', \f(CW\*(C`pos\*(C' and \f(CW\*(C`vec\*(C' don't leak memory anymore when used in lvalue
context.  Many small leaks that impacted applications that embed multiple
interpreters have been fixed.

### Better support for multiple interpreters

Subsection "Better support for multiple interpreters"
The build-time option \f(CW\*(C`-DMULTIPLICITY\*(C' has had many of the details
reworked.  Some previously global variables that should have been
per-interpreter now are.  With care, this allows interpreters to call
each other.  See the \f(CW\*(C`PerlInterp\*(C' extension on \s-1CPAN.\s0

### Behavior of \fBlocal() on array and hash elements is now well-defined

Subsection "Behavior of local() on array and hash elements is now well-defined"
See \*(L"Temporary Values via **local()**\*(R" in perlsub.
.ie n .SS """%!"" is transparently tied to the Errno module"
.el .SS "\f(CW%! is transparently tied to the Errno module"
Subsection "%! is transparently tied to the Errno module"
See perlvar, and Errno.

### Pseudo-hashes are supported

Subsection "Pseudo-hashes are supported"
See perlref.
.ie n .SS """EXPR foreach EXPR"" is supported"
.el .SS "\f(CWEXPR foreach EXPR is supported"
Subsection "EXPR foreach EXPR is supported"
See perlsyn.

### Keywords can be globally overridden

Subsection "Keywords can be globally overridden"
See perlsub.
.ie n .SS "$^E is meaningful on Win32"
.el .SS "\f(CW$^E is meaningful on Win32"
Subsection "$^E is meaningful on Win32"
See perlvar.
.ie n .SS """foreach (1..1000000)"" optimized"
.el .SS "\f(CWforeach (1..1000000) optimized"
Subsection "foreach (1..1000000) optimized"
\f(CW\*(C`foreach (1..1000000)\*(C' is now optimized into a counting loop.  It does
not try to allocate a 1000000-size list anymore.
.ie n .SS """Foo::"" can be used as implicitly quoted package name"
.el .SS "\f(CWFoo:: can be used as implicitly quoted package name"
Subsection "Foo:: can be used as implicitly quoted package name"
Barewords caused unintuitive behavior when a subroutine with the same
name as a package happened to be defined.  Thus, \f(CW\*(C`new Foo @args\*(C',
use the result of the call to \f(CW\*(C`Foo()\*(C' instead of \f(CW\*(C`Foo\*(C' being treated
as a literal.  The recommended way to write barewords in the indirect
object slot is \f(CW\*(C`new Foo:: @args\*(C'.  Note that the method \f(CW\*(C`new()\*(C' is
called with a first argument of \f(CW\*(C`Foo\*(C', not \f(CW\*(C`Foo::\*(C' when you do that.
.ie n .SS """exists $Foo::\{Bar::\}"" tests existence of a package"
.el .SS "\f(CWexists $Foo::\{Bar::\} tests existence of a package"
Subsection "exists $Foo::\{Bar::\} tests existence of a package"
It was impossible to test for the existence of a package without
actually creating it before.  Now \f(CW\*(C`exists $Foo::\{Bar::\}\*(C' can be
used to test if the \f(CW\*(C`Foo::Bar\*(C' namespace has been created.

### Better locale support

Subsection "Better locale support"
See perllocale.

### Experimental support for 64-bit platforms

Subsection "Experimental support for 64-bit platforms"
Perl5 has always had 64-bit support on systems with 64-bit longs.
Starting with 5.005, the beginnings of experimental support for systems
with 32-bit long and 64-bit 'long long' integers has been added.
If you add -DUSE_LONG_LONG to your ccflags in config.sh (or manually
define it in perl.h) then perl will be built with 'long long' support.
There will be many compiler warnings, and the resultant perl may not
work on all systems.  There are many other issues related to
third-party extensions and libraries.  This option exists to allow
people to work on those issues.

### \fBprototype() returns useful results on builtins

Subsection "prototype() returns useful results on builtins"
See \*(L"prototype\*(R" in perlfunc.

### Extended support for exception handling

Subsection "Extended support for exception handling"
\f(CW\*(C`die()\*(C' now accepts a reference value, and \f(CW$@ gets set to that
value in exception traps.  This makes it possible to propagate
exception objects.  This is an undocumented **experimental** feature.

### Re-blessing in \s-1\fBDESTROY\s0() supported for chaining \s-1\fBDESTROY\s0() methods

Subsection "Re-blessing in DESTROY() supported for chaining DESTROY() methods"
See \*(L"Destructors\*(R" in perlobj.
.ie n .SS "All ""printf"" format conversions are handled internally"
.el .SS "All \f(CWprintf format conversions are handled internally"
Subsection "All printf format conversions are handled internally"
See \*(L"printf\*(R" in perlfunc.
.ie n .SS "New ""INIT"" keyword"
.el .SS "New \f(CWINIT keyword"
Subsection "New INIT keyword"
\f(CW\*(C`INIT\*(C' subs are like \f(CW\*(C`BEGIN\*(C' and \f(CW\*(C`END\*(C', but they get run just before
the perl runtime begins execution.  e.g., the Perl Compiler makes use of
\f(CW\*(C`INIT\*(C' blocks to initialize and resolve pointers to XSUBs.
.ie n .SS "New ""lock"" keyword"
.el .SS "New \f(CWlock keyword"
Subsection "New lock keyword"
The \f(CW\*(C`lock\*(C' keyword is the fundamental synchronization primitive
in threaded perl.  When threads are not enabled, it is currently a noop.

To minimize impact on source compatibility this keyword is \*(L"weak\*(R", i.e., any
user-defined subroutine of the same name overrides it, unless a \f(CW\*(C`use Thread\*(C'
has been seen.
.ie n .SS "New ""qr//"" operator"
.el .SS "New \f(CWqr// operator"
Subsection "New qr// operator"
The \f(CW\*(C`qr//\*(C' operator, which is syntactically similar to the other quote-like
operators, is used to create precompiled regular expressions.  This compiled
form can now be explicitly passed around in variables, and interpolated in
other regular expressions.  See perlop.
.ie n .SS """our"" is now a reserved word"
.el .SS "\f(CWour is now a reserved word"
Subsection "our is now a reserved word"
Calling a subroutine with the name \f(CW\*(C`our\*(C' will now provoke a warning when
using the \f(CW\*(C`-w\*(C' switch.

### Tied arrays are now fully supported

Subsection "Tied arrays are now fully supported"
See Tie::Array.

### Tied handles support is better

Subsection "Tied handles support is better"
Several missing hooks have been added.  There is also a new base class for
\s-1TIEARRAY\s0 implementations.  See Tie::Array.

### 4th argument to substr

Subsection "4th argument to substr"
**substr()** can now both return and replace in one operation.  The optional
4th argument is the replacement string.  See \*(L"substr\*(R" in perlfunc.

### Negative \s-1LENGTH\s0 argument to splice

Subsection "Negative LENGTH argument to splice"
**splice()** with a negative \s-1LENGTH\s0 argument now work similar to what the
\s-1LENGTH\s0 did for **substr()**.  Previously a negative \s-1LENGTH\s0 was treated as
0.  See \*(L"splice\*(R" in perlfunc.

### Magic lvalues are now more magical

Subsection "Magic lvalues are now more magical"
When you say something like \f(CW\*(C`substr($x, 5) = "hi"\*(C', the scalar returned
by **substr()** is special, in that any modifications to it affect \f(CW$x.
(This is called a 'magic lvalue' because an 'lvalue' is something on
the left side of an assignment.)  Normally, this is exactly what you
would expect to happen, but Perl uses the same magic if you use **substr()**,
**pos()**, or **vec()** in a context where they might be modified, like taking
a reference with \f(CW\*(C`\\\*(C' or as an argument to a sub that modifies \f(CW@_.
In previous versions, this 'magic' only went one way, but now changes
to the scalar the magic refers to ($x in the above example) affect the
magic lvalue too. For instance, this code now acts differently:

.Vb 6
    $x = "hello";
    sub printit \{
        $x = "g\*(Aqbye";
        print $_[0], "\\n";
    \}
    printit(substr($x, 0, 5));
.Ve

In previous versions, this would print \*(L"hello\*(R", but it now prints \*(L"g'bye\*(R".

### <> now reads in records

Subsection "<> now reads in records"
If \f(CW$/ is a reference to an integer, or a scalar that holds an integer,
<> will read in records instead of lines. For more info, see
\*(L"$/\*(R" in perlvar.

## Supported Platforms

Header "Supported Platforms"
Configure has many incremental improvements.  Site-wide policy for building
perl can now be made persistent, via Policy.sh.  Configure also records
the command-line arguments used in *config.sh*.

### New Platforms

Subsection "New Platforms"
BeOS is now supported.  See *\s-1README\s0.beos*.

\s-1DOS\s0 is now supported under the \s-1DJGPP\s0 tools.  See *\s-1README\s0.dos* (installed
as perldos on some systems).

MiNT is now supported.  See *\s-1README\s0.mint*.

MPE/iX is now supported.  See \s-1README\s0.mpeix.

\s-1MVS\s0 (aka \s-1OS390,\s0 aka Open Edition) is now supported.  See *\s-1README\s0.os390*
(installed as perlos390 on some systems).

Stratus \s-1VOS\s0 is now supported.  See *\s-1README\s0.vos*.

### Changes in existing support

Subsection "Changes in existing support"
Win32 support has been vastly enhanced.  Support for Perl Object, a \*(C+
encapsulation of Perl.  \s-1GCC\s0 and \s-1EGCS\s0 are now supported on Win32.
See *\s-1README\s0.win32*, aka perlwin32.

\s-1VMS\s0 configuration system has been rewritten.  See *\s-1README\s0.vms* (installed
as *README_vms* on some systems).

The hints files for most Unix platforms have seen incremental improvements.

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules

Subsection "New Modules"

- B
Item "B"
Perl compiler and tools.  See B.

- Data::Dumper
Item "Data::Dumper"
A module to pretty print Perl data.  See Data::Dumper.

- Dumpvalue
Item "Dumpvalue"
A module to dump perl values to the screen. See Dumpvalue.

- Errno
Item "Errno"
A module to look up errors more conveniently.  See Errno.

- File::Spec
Item "File::Spec"
A portable \s-1API\s0 for file operations.

- ExtUtils::Installed
Item "ExtUtils::Installed"
Query and manage installed modules.

- ExtUtils::Packlist
Item "ExtUtils::Packlist"
Manipulate .packlist files.

- Fatal
Item "Fatal"
Make functions/builtins succeed or die.

- IPC::SysV
Item "IPC::SysV"
Constants and other support infrastructure for System V \s-1IPC\s0 operations
in perl.

- Test
Item "Test"
A framework for writing test suites.

- Tie::Array
Item "Tie::Array"
Base class for tied arrays.

- Tie::Handle
Item "Tie::Handle"
Base class for tied handles.

- Thread
Item "Thread"
Perl thread creation, manipulation, and support.

- attrs
Item "attrs"
Set subroutine attributes.

- fields
Item "fields"
Compile-time class fields.

- re
Item "re"
Various pragmata to control behavior of regular expressions.

### Changes in existing modules

Subsection "Changes in existing modules"

- Benchmark
Item "Benchmark"
You can now run tests for *x* seconds instead of guessing the right
number of tests to run.
.Sp
Keeps better time.

- Carp
Item "Carp"
Carp has a new function **cluck()**. **cluck()** warns, like **carp()**, but also adds
a stack backtrace to the error message, like **confess()**.

- \s-1CGI\s0
Item "CGI"
\s-1CGI\s0 has been updated to version 2.42.

- Fcntl
Item "Fcntl"
More Fcntl constants added: F_SETLK64, F_SETLKW64, O_LARGEFILE for
large (more than 4G) file access (the 64-bit support is not yet
working, though, so no need to get overly excited), Free/Net/OpenBSD
locking behaviour flags F_FLOCK, F_POSIX, Linux F_SHLCK, and
O_ACCMODE: the mask of O_RDONLY, O_WRONLY, and O_RDWR.

- Math::Complex
Item "Math::Complex"
The accessors methods Re, Im, arg, abs, rho, theta, methods can
($z->**Re()**) now also act as mutators ($z->**Re**\|(3)).

- Math::Trig
Item "Math::Trig"
A little bit of radial trigonometry (cylindrical and spherical) added,
for example the great circle distance.

- \s-1POSIX\s0
Item "POSIX"
\s-1POSIX\s0 now has its own platform-specific hints files.

- DB_File
Item "DB_File"
DB_File supports version 2.x of Berkeley \s-1DB.\s0  See \f(CW\*(C`ext/DB_File/Changes\*(C'.

- MakeMaker
Item "MakeMaker"
MakeMaker now supports writing empty makefiles, provides a way to
specify that site **umask()** policy should be honored.  There is also
better support for manipulation of .packlist files, and getting
information about installed modules.
.Sp
Extensions that have both architecture-dependent and
architecture-independent files are now always installed completely in
the architecture-dependent locations.  Previously, the shareable parts
were shared both across architectures and across perl versions and were
therefore liable to be overwritten with newer versions that might have
subtle incompatibilities.

- \s-1CPAN\s0
Item "CPAN"
See perlmodinstall and \s-1CPAN\s0.

- Cwd
Item "Cwd"
Cwd::cwd is faster on most platforms.

## Utility Changes

Header "Utility Changes"
\f(CW\*(C`h2ph\*(C' and related utilities have been vastly overhauled.

\f(CW\*(C`perlcc\*(C', a new experimental front end for the compiler is available.

The crude \s-1GNU\s0 \f(CW\*(C`configure\*(C' emulator is now called \f(CW\*(C`configure.gnu\*(C' to
avoid trampling on \f(CW\*(C`Configure\*(C' under case-insensitive filesystems.

\f(CW\*(C`perldoc\*(C' used to be rather slow.  The slower features are now optional.
In particular, case-insensitive searches need the \f(CW\*(C`-i\*(C' switch, and
recursive searches need \f(CW\*(C`-r\*(C'.  You can set these switches in the
\f(CW\*(C`PERLDOC\*(C' environment variable to get the old behavior.

## Documentation Changes

Header "Documentation Changes"
Config.pm now has a glossary of variables.

*Porting/patching.pod* has detailed instructions on how to create and
submit patches for perl.

perlport specifies guidelines on how to write portably.

perlmodinstall describes how to fetch and install modules from \f(CW\*(C`CPAN\*(C'
sites.

Some more Perl traps are documented now.  See perltrap.

perlopentut gives a tutorial on using **open()**.

perlreftut gives a tutorial on references.

perlthrtut gives a tutorial on threads.

## New Diagnostics

Header "New Diagnostics"

- Ambiguous call resolved as CORE::%s(), qualify as such or use &
Item "Ambiguous call resolved as CORE::%s(), qualify as such or use &"
(W) A subroutine you have declared has the same name as a Perl keyword,
and you have used the name without qualification for calling one or the
other.  Perl decided to call the builtin because the subroutine is
not imported.
.Sp
To force interpretation as a subroutine call, either put an ampersand
before the subroutine name, or qualify the name with its package.
Alternatively, you can import the subroutine (or pretend that it's
imported with the \f(CW\*(C`use subs\*(C' pragma).
.Sp
To silently interpret it as the Perl operator, use the \f(CW\*(C`CORE::\*(C' prefix
on the operator (e.g. \f(CW\*(C`CORE::log($x)\*(C') or by declaring the subroutine
to be an object method (see \*(L"attrs\*(R").

- Bad index while coercing array into hash
Item "Bad index while coercing array into hash"
(F) The index looked up in the hash found as the 0'th element of a
pseudo-hash is not legal.  Index values must be at 1 or greater.
See perlref.
.ie n .IP "Bareword ""%s"" refers to nonexistent package" 4
.el .IP "Bareword ``%s'' refers to nonexistent package" 4
Item "Bareword %s refers to nonexistent package"
(W) You used a qualified bareword of the form \f(CW\*(C`Foo::\*(C', but
the compiler saw no other uses of that namespace before that point.
Perhaps you need to predeclare a package?
.ie n .IP "Can't call method ""%s"" on an undefined value" 4
.el .IP "Can't call method ``%s'' on an undefined value" 4
Item "Can't call method %s on an undefined value"
(F) You used the syntax of a method call, but the slot filled by the
object reference or package name contains an undefined value.
Something like this will reproduce the error:
.Sp
.Vb 3
    $BADREF = 42;
    process $BADREF 1,2,3;
    $BADREF->process(1,2,3);
.Ve
.ie n .IP "Can't check filesystem of script ""%s"" for nosuid" 4
.el .IP "Can't check filesystem of script ``%s'' for nosuid" 4
Item "Can't check filesystem of script %s for nosuid"
(P) For some reason you can't check the filesystem of the script for nosuid.

- Can't coerce array into hash
Item "Can't coerce array into hash"
(F) You used an array where a hash was expected, but the array has no
information on how to map from keys to array indices.  You can do that
only with arrays that have a hash reference at index 0.

- Can't goto subroutine from an eval-string
Item "Can't goto subroutine from an eval-string"
(F) The \*(L"goto subroutine\*(R" call can't be used to jump out of an eval \*(L"string\*(R".
(You can use it to jump out of an eval \{\s-1BLOCK\s0\}, but you probably don't want to.)

- Can't localize pseudo-hash element
Item "Can't localize pseudo-hash element"
(F) You said something like \f(CW\*(C`local $ar->\{\*(Aqkey\*(Aq\}\*(C', where \f(CW$ar is
a reference to a pseudo-hash.  That hasn't been implemented yet, but
you can get a similar effect by localizing the corresponding array
element directly: \f(CW\*(C`local $ar->[$ar->[0]\{\*(Aqkey\*(Aq\}]\*(C'.

- Can't use %%! because Errno.pm is not available
Item "Can't use %%! because Errno.pm is not available"
(F) The first time the %! hash is used, perl automatically loads the
Errno.pm module. The Errno module is expected to tie the %! hash to
provide symbolic names for \f(CW$! errno values.
.ie n .IP "Cannot find an opnumber for ""%s""" 4
.el .IP "Cannot find an opnumber for ``%s''" 4
Item "Cannot find an opnumber for %s"
(F) A string of a form \f(CW\*(C`CORE::word\*(C' was given to **prototype()**, but
there is no builtin with the name \f(CW\*(C`word\*(C'.

- Character class syntax [. .] is reserved for future extensions
Item "Character class syntax [. .] is reserved for future extensions"
(W) Within regular expression character classes ([]) the syntax beginning
with \*(L"[.\*(R" and ending with \*(L".]\*(R" is reserved for future extensions.
If you need to represent those character sequences inside a regular
expression character class, just quote the square brackets with the
backslash: \*(L"\\[.\*(R" and \*(L".\\]\*(R".

- Character class syntax [: :] is reserved for future extensions
Item "Character class syntax [: :] is reserved for future extensions"
(W) Within regular expression character classes ([]) the syntax beginning
with \*(L"[:\*(R" and ending with \*(L":]\*(R" is reserved for future extensions.
If you need to represent those character sequences inside a regular
expression character class, just quote the square brackets with the
backslash: \*(L"\\[:\*(R" and \*(L":\\]\*(R".

- Character class syntax [= =] is reserved for future extensions
Item "Character class syntax [= =] is reserved for future extensions"
(W) Within regular expression character classes ([]) the syntax
beginning with \*(L"[=\*(R" and ending with \*(L"=]\*(R" is reserved for future extensions.
If you need to represent those character sequences inside a regular
expression character class, just quote the square brackets with the
backslash: \*(L"\\[=\*(R" and \*(L"=\\]\*(R".
.ie n .IP "%s: Eval-group in insecure regular expression" 4
.el .IP "\f(CW%s: Eval-group in insecure regular expression" 4
Item "%s: Eval-group in insecure regular expression"
(F) Perl detected tainted data when trying to compile a regular expression
that contains the \f(CW\*(C`(?\{ ... \})\*(C' zero-width assertion, which is unsafe.
See \*(L"(?\{ code \})\*(R" in perlre, and perlsec.
.ie n .IP "%s: Eval-group not allowed, use re 'eval'" 4
.el .IP "\f(CW%s: Eval-group not allowed, use re 'eval'" 4
Item "%s: Eval-group not allowed, use re 'eval'"
(F) A regular expression contained the \f(CW\*(C`(?\{ ... \})\*(C' zero-width assertion,
but that construct is only allowed when the \f(CW\*(C`use re \*(Aqeval\*(Aq\*(C' pragma is
in effect.  See \*(L"(?\{ code \})\*(R" in perlre.
.ie n .IP "%s: Eval-group not allowed at run time" 4
.el .IP "\f(CW%s: Eval-group not allowed at run time" 4
Item "%s: Eval-group not allowed at run time"
(F) Perl tried to compile a regular expression containing the \f(CW\*(C`(?\{ ... \})\*(C'
zero-width assertion at run time, as it would when the pattern contains
interpolated values.  Since that is a security risk, it is not allowed.
If you insist, you may still do this by explicitly building the pattern
from an interpolated string at run time and using that in an **eval()**.
See \*(L"(?\{ code \})\*(R" in perlre.

- Explicit blessing to '' (assuming package main)
Item "Explicit blessing to '' (assuming package main)"
(W) You are blessing a reference to a zero length string.  This has
the effect of blessing the reference into the package main.  This is
usually not what you want.  Consider providing a default target
package, e.g. bless($ref, \f(CW$p || 'MyPackage');

- Illegal hex digit ignored
Item "Illegal hex digit ignored"
(W) You may have tried to use a character other than 0 - 9 or A - F in a
hexadecimal number.  Interpretation of the hexadecimal number stopped
before the illegal character.

- No such array field
Item "No such array field"
(F) You tried to access an array as a hash, but the field name used is
not defined.  The hash at index 0 should map all valid field names to
array indices for that to work.
.ie n .IP "No such field ""%s"" in variable %s of type %s" 4
.el .IP "No such field ``%s'' in variable \f(CW%s of type \f(CW%s" 4
Item "No such field %s in variable %s of type %s"
(F) You tried to access a field of a typed variable where the type
does not know about the field name.  The field names are looked up in
the \f(CW%FIELDS hash in the type package at compile time.  The \f(CW%FIELDS hash
is usually set up with the 'fields' pragma.

- Out of memory during ridiculously large request
Item "Out of memory during ridiculously large request"
(F) You can't allocate more than 2^31+\*(L"small amount\*(R" bytes.  This error
is most likely to be caused by a typo in the Perl program. e.g., \f(CW$arr[time]
instead of \f(CW$arr[$time].

- Range iterator outside integer range
Item "Range iterator outside integer range"
(F) One (or both) of the numeric arguments to the range operator \*(L"..\*(R"
are outside the range which can be represented by integers internally.
One possible workaround is to force Perl to use magical string
increment by prepending \*(L"0\*(R" to your numbers.
.ie n .IP "Recursive inheritance detected while looking for method '%s' %s" 4
.el .IP "Recursive inheritance detected while looking for method '%s' \f(CW%s" 4
Item "Recursive inheritance detected while looking for method '%s' %s"
(F) More than 100 levels of inheritance were encountered while invoking a
method.  Probably indicates an unintended loop in your inheritance hierarchy.

- Reference found where even-sized list expected
Item "Reference found where even-sized list expected"
(W) You gave a single reference where Perl was expecting a list with
an even number of elements (for assignment to a hash). This
usually means that you used the anon hash constructor when you meant
to use parens. In any case, a hash requires key/value **pairs**.
.Sp
.Vb 4
    %hash = \{ one => 1, two => 2, \};   # WRONG
    %hash = [ qw/ an anon array / ];   # WRONG
    %hash = ( one => 1, two => 2, );   # right
    %hash = qw( one 1 two 2 );                 # also fine
.Ve

- Undefined value assigned to typeglob
Item "Undefined value assigned to typeglob"
(W) An undefined value was assigned to a typeglob, a la \f(CW\*(C`*foo = undef\*(C'.
This does nothing.  It's possible that you really mean \f(CW\*(C`undef *foo\*(C'.
.ie n .IP "Use of reserved word ""%s"" is deprecated" 4
.el .IP "Use of reserved word ``%s'' is deprecated" 4
Item "Use of reserved word %s is deprecated"
(D) The indicated bareword is a reserved word.  Future versions of perl
may use it as a keyword, so you're better off either explicitly quoting
the word in a manner appropriate for its context of use, or using a
different name altogether.  The warning can be suppressed for subroutine
names by either adding a \f(CW\*(C`&\*(C' prefix, or using a package qualifier,
e.g. \f(CW\*(C`&our()\*(C', or \f(CW\*(C`Foo::our()\*(C'.

- perl: warning: Setting locale failed.
Item "perl: warning: Setting locale failed."
(S) The whole warning message will look something like:
.Sp
.Vb 6
       perl: warning: Setting locale failed.
       perl: warning: Please check that your locale settings:
               LC_ALL = "En_US",
               LANG = (unset)
           are supported and installed on your system.
       perl: warning: Falling back to the standard locale ("C").
.Ve
.Sp
Exactly what were the failed locale settings varies.  In the above the
settings were that the \s-1LC_ALL\s0 was \*(L"En_US\*(R" and the \s-1LANG\s0 had no value.
This error means that Perl detected that you and/or your system
administrator have set up the so-called variable system but Perl could
not use those settings.  This was not dead serious, fortunately: there
is a \*(L"default locale\*(R" called \*(L"C\*(R" that Perl can and will use, the
script will be run.  Before you really fix the problem, however, you
will get the same error message each time you run Perl.  How to really
fix the problem can be found in \*(L"\s-1LOCALE PROBLEMS\*(R"\s0 in perllocale.

## Obsolete Diagnostics

Header "Obsolete Diagnostics"

- Can't \fBmktemp()
Item "Can't mktemp()"
(F) The **mktemp()** routine failed for some reason while trying to process
a **-e** switch.  Maybe your /tmp partition is full, or clobbered.
.Sp
Removed because **-e** doesn't use temporary files any more.
.ie n .IP "Can't write to temp file for **-e**: %s" 4
.el .IP "Can't write to temp file for **-e**: \f(CW%s" 4
Item "Can't write to temp file for -e: %s"
(F) The write routine failed for some reason while trying to process
a **-e** switch.  Maybe your /tmp partition is full, or clobbered.
.Sp
Removed because **-e** doesn't use temporary files any more.

- Cannot open temporary file
Item "Cannot open temporary file"
(F) The create routine failed for some reason while trying to process
a **-e** switch.  Maybe your /tmp partition is full, or clobbered.
.Sp
Removed because **-e** doesn't use temporary files any more.

- regexp too big
Item "regexp too big"
(F) The current implementation of regular expressions uses shorts as
address offsets within a string.  Unfortunately this means that if
the regular expression compiles to longer than 32767, it'll blow up.
Usually when you want a regular expression this big, there is a better
way to do it with multiple statements.  See perlre.

## Configuration Changes

Header "Configuration Changes"
You can use \*(L"Configure -Uinstallusrbinperl\*(R" which causes installperl
to skip installing perl also as /usr/bin/perl.  This is useful if you
prefer not to modify /usr/bin for some reason or another but harmful
because many scripts assume to find Perl in /usr/bin/perl.

## BUGS

Header "BUGS"
If you find what you think is a bug, you might check the headers of
recently posted articles in the comp.lang.perl.misc newsgroup.
There may also be information at http://www.perl.com/perl/ , the Perl
Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Make sure you trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to <*perlbug@perl.com*> to be
analysed by the Perl porting team.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for exhaustive details on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.

## HISTORY

Header "HISTORY"
Written by Gurusamy Sarathy <*gsar@activestate.com*>, with many contributions
from The Perl Porters.

Send omissions or corrections to <*perlbug@perl.com*>.
