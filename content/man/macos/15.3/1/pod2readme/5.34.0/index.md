+++
date = "2018-10-31"
operating_system = "macos"
author = "None Specified"
operating_system_version = "15.3"
manpage_name = "pod2readme"
detected_package_version = "5.34.0"
keywords = ["header", "see", "also", "pod2text", "pod2markdown"]
manpage_format = "troff"
manpage_section = "1"
title = "pod2readme(1)"
description = "This utility will use Pod::Readme to extract a s-1READMEs0 file from a s-1PODs0 document. It works by extracting and filtering the s-1POD,s0 and then calling the appropriate filter program to convert the s-1PODs0 to another format. By default, f(C..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "POD2README 1"
POD2README 1 "2018-10-31" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

pod2readme - Intelligently generate a README file from POD

## USAGE

Header "USAGE"
.Vb 1
    pod2readme [-cfho] [long options...] input-file [output-file] [target]

        Intelligently generate a README file from POD

        -t --target     target type (default: \*(Aqreadme\*(Aq)
        -f --format     output format (default: \*(Aqtext\*(Aq)
        -b --backup     backup output file
        -o --output     output filename (default based on target)
        -c --stdout     output to stdout (console)
        -F --force      only update if files are changed
        -h --help       print usage and exit
.Ve

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
    pod2readme -f markdown lib/MyApp.pm
.Ve

## DESCRIPTION

Header "DESCRIPTION"
This utility will use Pod::Readme to extract a *\s-1README\s0* file from
a \s-1POD\s0 document.

It works by extracting and filtering the \s-1POD,\s0 and then calling the
appropriate filter program to convert the \s-1POD\s0 to another format.

## OPTIONS

Header "OPTIONS"
.ie n .SS """--backup"""
.el .SS "\f(CW--backup"
Subsection "--backup"
By default, \f(CW\*(C`pod2readme\*(C' will back up the output file. To disable
this, use the \f(CW\*(C`--no-backup\*(C' option.
.ie n .SS """--output"""
.el .SS "\f(CW--output"
Subsection "--output"
Specifies the name of the output file. If omitted, it will use the
second command line argument, or default to the \f(CW\*(C`--target\*(C' plus the
corresponding extension of the \f(CW\*(C`--format\*(C'.

For all intents, the default is *\s-1README\s0*.

If a format other than \*(L"text\*(R" is chosen, then the appropriate
extension will be added, e.g. for \*(L"markdown\*(R", the default output file
is *\s-1README\s0.md*.
.ie n .SS """--target"""
.el .SS "\f(CW--target"
Subsection "--target"
The target of the filter, which defaults to \*(L"readme\*(R".
.ie n .SS """--format"""
.el .SS "\f(CW--format"
Subsection "--format"
The output format, which defaults to \*(L"text\*(R".

Other supposed formats are \*(L"github\*(R", \*(L"html\*(R", \*(L"latex\*(R", \*(L"man\*(R",
\*(L"markdown\*(R", \*(L"pod\*(R", \*(L"rtf\*(R", and \*(L"xhtml\*(R". You can also use \*(L"gfm\*(R"
instead of \*(L"github\*(R". Similary you can use \*(L"md\*(R" for \*(L"markdown\*(R".
.ie n .SS """--stdout"""
.el .SS "\f(CW--stdout"
Subsection "--stdout"
If enabled, it will output to the console instead of \f(CW\*(C`--output\*(C'.
.ie n .SS """--force"""
.el .SS "\f(CW--force"
Subsection "--force"
By default, the *\s-1README\s0* will be generated if the source files have
been changed.  Using \f(CW\*(C`--force\*(C' will force the file to be updated.

Note: \s-1POD\s0 format files will always be updated.
.ie n .SS """--help"""
.el .SS "\f(CW--help"
Subsection "--help"
Prints the usage and exits.

## SEE ALSO

Header "SEE ALSO"
pod2text, pod2markdown.
