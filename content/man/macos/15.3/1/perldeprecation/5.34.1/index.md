+++
detected_package_version = "5.34.1"
operating_system_version = "15.3"
manpage_format = "troff"
date = "2022-02-19"
manpage_section = "1"
description = "The purpose of this document is to document what has been deprecated in Perl, and by which version the deprecated feature will disappear, or, for already removed features, when it was removed. This document will try to discuss what alternatives for..."
operating_system = "macos"
author = "None Specified"
keywords = ["header", "see", "also", "warnings", "diagnostics"]
title = "perldeprecation(1)"
manpage_name = "perldeprecation"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLDEPRECATION 1"
PERLDEPRECATION 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perldeprecation - list Perl deprecations

## DESCRIPTION

Header "DESCRIPTION"
The purpose of this document is to document what has been deprecated
in Perl, and by which version the deprecated feature will disappear,
or, for already removed features, when it was removed.

This document will try to discuss what alternatives for the deprecated
features are available.

The deprecated features will be grouped by the version of Perl in
which they will be removed.

### Perl 5.34

Subsection "Perl 5.34"
There are no deprecations or fatalizations scheduled for Perl 5.34.

### Perl 5.32

Subsection "Perl 5.32"
*Constants from lexical variables potentially modified elsewhere*
Subsection "Constants from lexical variables potentially modified elsewhere"

You wrote something like

.Vb 2
    my $var;
    $sub = sub () \{ $var \};
.Ve

but \f(CW$var is referenced elsewhere and could be modified after the \f(CW\*(C`sub\*(C'
expression is evaluated.  Either it is explicitly modified elsewhere
(\f(CW\*(C`$var = 3\*(C') or it is passed to a subroutine or to an operator like
\f(CW\*(C`printf\*(C' or \f(CW\*(C`map\*(C', which may or may not modify the variable.

Traditionally, Perl has captured the value of the variable at that
point and turned the subroutine into a constant eligible for inlining.
In those cases where the variable can be modified elsewhere, this
breaks the behavior of closures, in which the subroutine captures
the variable itself, rather than its value, so future changes to the
variable are reflected in the subroutine's return value.

If you intended for the subroutine to be eligible for inlining, then
make sure the variable is not referenced elsewhere, possibly by
copying it:

.Vb 2
    my $var2 = $var;
    $sub = sub () \{ $var2 \};
.Ve

If you do want this subroutine to be a closure that reflects future
changes to the variable that it closes over, add an explicit \f(CW\*(C`return\*(C':

.Vb 2
    my $var;
    $sub = sub () \{ return $var \};
.Ve

This usage was deprecated and as of Perl 5.32 is no longer allowed.

*Use of strings with code points over 0xFF as arguments to \f(CI\*(C`vec\*(C'\fI*
Subsection "Use of strings with code points over 0xFF as arguments to vec"

\f(CW\*(C`vec\*(C' views its string argument as a sequence of bits.  A string
containing a code point over 0xFF is nonsensical.  This usage is
deprecated in Perl 5.28, and was removed in Perl 5.32.

*Use of code points over 0xFF in string bitwise operators*
Subsection "Use of code points over 0xFF in string bitwise operators"

The string bitwise operators, \f(CW\*(C`&\*(C', \f(CW\*(C`|\*(C', \f(CW\*(C`^\*(C', and \f(CW\*(C`~\*(C', treat their
operands as strings of bytes. As such, values above 0xFF are
nonsensical. Some instances of these have been deprecated since Perl
5.24, and were made fatal in 5.28, but it turns out that in cases where
the wide characters did not affect the end result, no deprecation
notice was raised, and so remain legal.  Now, all occurrences either are
fatal or raise a deprecation warning, so that the remaining legal
occurrences became fatal in 5.32.

An example of this is

.Vb 1
 "" & "\\x\{100\}"
.Ve

The wide character is not used in the \f(CW\*(C`&\*(C' operation because the left
operand is shorter.  This now throws an exception.

*\f(BIhostname()\fI doesn't accept any arguments*
Subsection "hostname() doesn't accept any arguments"

The function \f(CW\*(C`hostname()\*(C' in the Sys::Hostname module has always
been documented to be called with no arguments.  Historically it has not
enforced this, and has actually accepted and ignored any arguments.  As a
result, some users have got the mistaken impression that an argument does
something useful.  To avoid these bugs, the function is being made strict.
Passing arguments was deprecated in Perl 5.28 and became fatal in Perl 5.32.

*Unescaped left braces in regular expressions*
Subsection "Unescaped left braces in regular expressions"

The simple rule to remember, if you want to match a literal \f(CW\*(C`\{\*(C'
character (U+007B \f(CW\*(C`LEFT CURLY BRACKET\*(C') in a regular expression
pattern, is to escape each literal instance of it in some way.
Generally easiest is to precede it with a backslash, like \f(CW\*(C`\\\{\*(C'
or enclose it in square brackets (\f(CW\*(C`[\{]\*(C').  If the pattern
delimiters are also braces, any matching right brace (\f(CW\*(C`\}\*(C') should
also be escaped to avoid confusing the parser, for example,

.Vb 1
 qr\{abc\\\{def\\\}ghi\}
.Ve

Forcing literal \f(CW\*(C`\{\*(C' characters to be escaped will enable the Perl
language to be extended in various ways in future releases.  To avoid
needlessly breaking existing code, the restriction is not enforced in
contexts where there are unlikely to ever be extensions that could
conflict with the use there of \f(CW\*(C`\{\*(C' as a literal.  A non-deprecation
warning that the left brace is being taken literally is raised in
contexts where there could be confusion about it.

Literal uses of \f(CW\*(C`\{\*(C' were deprecated in Perl 5.20, and some uses of it
started to give deprecation warnings since. These cases were made fatal
in Perl 5.26. Due to an oversight, not all cases of a use of a literal
\f(CW\*(C`\{\*(C' got a deprecation warning.  Some cases started warning in Perl 5.26,
and were made fatal in Perl 5.30.  Other cases started in Perl 5.28,
and were made fatal in 5.32.

*In \s-1XS\s0 code, use of various macros dealing with \s-1UTF-8.\s0*
Subsection "In XS code, use of various macros dealing with UTF-8."

The macros below now require an extra parameter than in versions prior
to Perl 5.32.  The final parameter in each one is a pointer into the
string supplied by the first parameter beyond which the input will not
be read.  This prevents potential reading beyond the end of the buffer.
\f(CW\*(C`isALPHANUMERIC_utf8\*(C',
\f(CW\*(C`isASCII_utf8\*(C',
\f(CW\*(C`isBLANK_utf8\*(C',
\f(CW\*(C`isCNTRL_utf8\*(C',
\f(CW\*(C`isDIGIT_utf8\*(C',
\f(CW\*(C`isIDFIRST_utf8\*(C',
\f(CW\*(C`isPSXSPC_utf8\*(C',
\f(CW\*(C`isSPACE_utf8\*(C',
\f(CW\*(C`isVERTWS_utf8\*(C',
\f(CW\*(C`isWORDCHAR_utf8\*(C',
\f(CW\*(C`isXDIGIT_utf8\*(C',
\f(CW\*(C`isALPHANUMERIC_LC_utf8\*(C',
\f(CW\*(C`isALPHA_LC_utf8\*(C',
\f(CW\*(C`isASCII_LC_utf8\*(C',
\f(CW\*(C`isBLANK_LC_utf8\*(C',
\f(CW\*(C`isCNTRL_LC_utf8\*(C',
\f(CW\*(C`isDIGIT_LC_utf8\*(C',
\f(CW\*(C`isGRAPH_LC_utf8\*(C',
\f(CW\*(C`isIDCONT_LC_utf8\*(C',
\f(CW\*(C`isIDFIRST_LC_utf8\*(C',
\f(CW\*(C`isLOWER_LC_utf8\*(C',
\f(CW\*(C`isPRINT_LC_utf8\*(C',
\f(CW\*(C`isPSXSPC_LC_utf8\*(C',
\f(CW\*(C`isPUNCT_LC_utf8\*(C',
\f(CW\*(C`isSPACE_LC_utf8\*(C',
\f(CW\*(C`isUPPER_LC_utf8\*(C',
\f(CW\*(C`isWORDCHAR_LC_utf8\*(C',
\f(CW\*(C`isXDIGIT_LC_utf8\*(C',
\f(CW\*(C`toFOLD_utf8\*(C',
\f(CW\*(C`toLOWER_utf8\*(C',
\f(CW\*(C`toTITLE_utf8\*(C',
and
\f(CW\*(C`toUPPER_utf8\*(C'.

Since Perl 5.26, this functionality with the extra parameter has been
available by using a corresponding macro to each one of these, and whose
name is formed by appending \f(CW\*(C`_safe\*(C' to the base name.  There is no
change to the functionality of those.  For example, \f(CW\*(C`isDIGIT_utf8_safe\*(C'
corresponds to \f(CW\*(C`isDIGIT_utf8\*(C', and both now behave identically.  All
are documented in \*(L"Character case changing\*(R" in perlapi and
\*(L"Character classification\*(R" in perlapi.

This change was originally scheduled for 5.30, but was delayed until
5.32.

*\f(CI\*(C`File::Glob::glob()\*(C'\fI was removed*
Subsection "File::Glob::glob() was removed"

\f(CW\*(C`File::Glob\*(C' has a function called \f(CW\*(C`glob\*(C', which just calls
\f(CW\*(C`bsd_glob\*(C'.

\f(CW\*(C`File::Glob::glob()\*(C' was deprecated in Perl 5.8. A deprecation
message was issued from Perl 5.26 onwards, and the function has now
disappeared in Perl 5.30.

Code using \f(CW\*(C`File::Glob::glob()\*(C' should call
\f(CW\*(C`File::Glob::bsd_glob()\*(C' instead.

### Perl 5.30

Subsection "Perl 5.30"
*\f(CI$*\fI is no longer supported*
Subsection "$* is no longer supported"

Before Perl 5.10, setting \f(CW$* to a true value globally enabled
multi-line matching within a string. This relique from the past lost
its special meaning in 5.10. Use of this variable will be a fatal error
in Perl 5.30, freeing the variable up for a future special meaning.

To enable multiline matching one should use the \f(CW\*(C`/m\*(C' regexp
modifier (possibly in combination with \f(CW\*(C`/s\*(C'). This can be set
on a per match bases, or can be enabled per lexical scope (including
a whole file) with \f(CW\*(C`use re \*(Aq/m\*(Aq\*(C'.

*\f(CI$#\fI is no longer supported*
Subsection "$# is no longer supported"

This variable used to have a special meaning \*(-- it could be used
to control how numbers were formatted when printed. This seldom
used functionality was removed in Perl 5.10. In order to free up
the variable for a future special meaning, its use will be a fatal
error in Perl 5.30.

To specify how numbers are formatted when printed, one is advised
to use \f(CW\*(C`printf\*(C' or \f(CW\*(C`sprintf\*(C' instead.

*Assigning non-zero to \f(CI$[\fI is fatal*
Subsection "Assigning non-zero to $[ is fatal"

This variable (and the corresponding \f(CW\*(C`array_base\*(C' feature and
arybase module) allowed changing the base for array and string
indexing operations.

Setting this to a non-zero value has been deprecated since Perl 5.12 and
throws a fatal error as of Perl 5.30.

*\f(CI\*(C`File::Glob::glob()\*(C'\fI will disappear*
Subsection "File::Glob::glob() will disappear"

\f(CW\*(C`File::Glob\*(C' has a function called \f(CW\*(C`glob\*(C', which just calls
\f(CW\*(C`bsd_glob\*(C'. However, its prototype is different from the prototype
of \f(CW\*(C`CORE::glob\*(C', and hence, \f(CW\*(C`File::Glob::glob\*(C' should not
be used.

\f(CW\*(C`File::Glob::glob()\*(C' was deprecated in Perl 5.8. A deprecation
message was issued from Perl 5.26 onwards, and the function will
disappear in Perl 5.30.

Code using \f(CW\*(C`File::Glob::glob()\*(C' should call
\f(CW\*(C`File::Glob::bsd_glob()\*(C' instead.

*Unescaped left braces in regular expressions (for 5.30)*
Subsection "Unescaped left braces in regular expressions (for 5.30)"

See \*(L"Unescaped left braces in regular expressions\*(R" above.

*Unqualified \f(CI\*(C`dump()\*(C'\fI*
Subsection "Unqualified dump()"

Use of \f(CW\*(C`dump()\*(C' instead of \f(CW\*(C`CORE::dump()\*(C' was deprecated in Perl 5.8,
and an unqualified \f(CW\*(C`dump()\*(C' will no longer be available in Perl 5.30.

See \*(L"dump\*(R" in perlfunc.

*Using \f(BImy()\fI in false conditional.*
Subsection "Using my() in false conditional."

There has been a long-standing bug in Perl that causes a lexical variable
not to be cleared at scope exit when its declaration includes a false
conditional.  Some people have exploited this bug to achieve a kind of
static variable.  To allow us to fix this bug, people should not be
relying on this behavior.

Instead, it's recommended one uses \f(CW\*(C`state\*(C' variables to achieve the
same effect:

.Vb 4
    use 5.10.0;
    sub count \{state $counter; return ++ $counter\}
    say count ();    # Prints 1
    say count ();    # Prints 2
.Ve

\f(CW\*(C`state\*(C' variables were introduced in Perl 5.10.

Alternatively, you can achieve a similar static effect by
declaring the variable in a separate block outside the function, e.g.,

.Vb 1
    sub f \{ my $x if 0; return $x++ \}
.Ve

becomes

.Vb 1
    \{ my $x; sub f \{ return $x++ \} \}
.Ve

The use of \f(CW\*(C`my()\*(C' in a false conditional has been deprecated in
Perl 5.10, and became a fatal error in Perl 5.30.

*Reading/writing bytes from/to :utf8 handles.*
Subsection "Reading/writing bytes from/to :utf8 handles."

The **sysread()**, **recv()**, **syswrite()** and **send()** operators are
deprecated on handles that have the \f(CW\*(C`:utf8\*(C' layer, either explicitly, or
implicitly, eg., with the \f(CW\*(C`:encoding(UTF-16LE)\*(C' layer.

Both **sysread()** and **recv()** currently use only the \f(CW\*(C`:utf8\*(C' flag for the stream,
ignoring the actual layers.  Since **sysread()** and **recv()** do no \s-1UTF-8\s0
validation they can end up creating invalidly encoded scalars.

Similarly, **syswrite()** and **send()** use only the \f(CW\*(C`:utf8\*(C' flag, otherwise ignoring
any layers.  If the flag is set, both write the value \s-1UTF-8\s0 encoded, even if
the layer is some different encoding, such as the example above.

Ideally, all of these operators would completely ignore the \f(CW\*(C`:utf8\*(C' state,
working only with bytes, but this would result in silently breaking existing
code.  To avoid this a future version of perl will throw an exception when
any of **sysread()**, **recv()**, **syswrite()** or **send()** are called on handle with the
\f(CW\*(C`:utf8\*(C' layer.

In Perl 5.30, it will no longer be possible to use **sysread()**, **recv()**,
**syswrite()** or **send()** to read or send bytes from/to :utf8 handles.

*Use of unassigned code point or non-standalone grapheme for a delimiter.*
Subsection "Use of unassigned code point or non-standalone grapheme for a delimiter."

A grapheme is what appears to a native-speaker of a language to be a
character.  In Unicode (and hence Perl) a grapheme may actually be
several adjacent characters that together form a complete grapheme.  For
example, there can be a base character, like \*(L"R\*(R" and an accent, like a
circumflex \*(L"^\*(R", that appear to be a single character when displayed,
with the circumflex hovering over the \*(L"R\*(R".

As of Perl 5.30, use of delimiters which are non-standalone graphemes is
fatal, in order to move the language to be able to accept
multi-character graphemes as delimiters.

Also, as of Perl 5.30, delimiters which are unassigned code points
but that may someday become assigned are prohibited.  Otherwise, code
that works today would fail to compile if the currently unassigned
delimiter ends up being something that isn't a stand-alone grapheme.
Because Unicode is never going to assign non-character code
points, nor code points that are
above the legal Unicode maximum, those can be delimiters.

### Perl 5.28

Subsection "Perl 5.28"
*Attributes \f(CI\*(C`:locked\*(C'\fI and \f(CI\*(C`:unique\*(C'\fI*
Subsection "Attributes :locked and :unique"

The attributes \f(CW\*(C`:locked\*(C' (on code references) and \f(CW\*(C`:unique\*(C'
(on array, hash and scalar references) have had no effect since
Perl 5.005 and Perl 5.8.8 respectively. Their use has been deprecated
since.

As of Perl 5.28, these attributes are syntax errors. Since the
attributes do not do anything, removing them from your code fixes
the syntax error; and removing them will not influence the behaviour
of your code.

*Bare here-document terminators*
Subsection "Bare here-document terminators"

Perl has allowed you to use a bare here-document terminator to have the
here-document end at the first empty line. This practise was deprecated
in Perl 5.000; as of Perl 5.28, using a bare here-document terminator
throws a fatal error.

You are encouraged to use the explicitly quoted form if you wish to
use an empty line as the terminator of the here-document:

.Vb 2
  print <<"";
    Print this line.

  # Previous blank line ends the here-document.
.Ve

*Setting $/ to a reference to a non-positive integer*
Subsection "Setting $/ to a reference to a non-positive integer"

You assigned a reference to a scalar to \f(CW$/ where the
referenced item is not a positive integer.  In older perls this **appeared**
to work the same as setting it to \f(CW\*(C`undef\*(C' but was in fact internally
different, less efficient and with very bad luck could have resulted in
your file being split by a stringified form of the reference.

In Perl 5.20.0 this was changed so that it would be **exactly** the same as
setting \f(CW$/ to undef, with the exception that this warning would be
thrown.

As of Perl 5.28, setting \f(CW$/ to a reference of a non-positive
integer throws a fatal error.

You are recommended to change your code to set \f(CW$/ to \f(CW\*(C`undef\*(C' explicitly
if you wish to slurp the file.

*Limit on the value of Unicode code points.*
Subsection "Limit on the value of Unicode code points."

Unicode only allows code points up to 0x10FFFF, but Perl allows
much larger ones. Up till Perl 5.28, it was allowed to use code
points exceeding the maximum value of an integer (\f(CW\*(C`IV_MAX\*(C').
However, that did break the perl interpreter in some constructs,
including causing it to hang in a few cases.  The known problem
areas were in \f(CW\*(C`tr///\*(C', regular expression pattern matching using
quantifiers, as quote delimiters in \f(CW\*(C`q\f(CIX\f(CW...\f(CIX\f(CW\*(C' (where *X* is
the \f(CW\*(C`chr()\*(C' of a large code point), and as the upper limits in
loops.

The use of out of range code points was deprecated in Perl 5.24; as of
Perl 5.28 using a code point exceeding \f(CW\*(C`IV_MAX\*(C' throws a fatal error.

If your code is to run on various platforms, keep in mind that the upper
limit depends on the platform. It is much larger on 64-bit word sizes
than 32-bit ones. For 32-bit integers, \f(CW\*(C`IV_MAX\*(C' equals \f(CW0x7FFFFFFF,
for 64-bit integers, \f(CW\*(C`IV_MAX\*(C' equals \f(CW0x7FFFFFFFFFFFFFFF.

*Use of comma-less variable list in formats.*
Subsection "Use of comma-less variable list in formats."

It was allowed to use a list of variables in a format, without
separating them with commas. This usage has been deprecated
for a long time, and as of Perl 5.28, this throws a fatal error.

*Use of \f(CI\*(C`\\N\{\}\*(C'\fI*
Subsection "Use of N\{\}"

Use of \f(CW\*(C`\\N\{\}\*(C' with nothing between the braces was deprecated in
Perl 5.24, and throws a fatal error as of Perl 5.28.

Since such a construct is equivalent to using an empty string,
you are recommended to remove such \f(CW\*(C`\\N\{\}\*(C' constructs.

*Using the same symbol to open a filehandle and a dirhandle*
Subsection "Using the same symbol to open a filehandle and a dirhandle"

It used to be legal to use \f(CW\*(C`open()\*(C' to associate both a
filehandle and a dirhandle to the same symbol (glob or scalar).
This idiom is likely to be confusing, and it was deprecated in
Perl 5.10.

Using the same symbol to \f(CW\*(C`open()\*(C' a filehandle and a dirhandle
throws a fatal error as of Perl 5.28.

You should be using two different symbols instead.

*$\{^ENCODING\} is no longer supported.*
Subsection "$\{^ENCODING\} is no longer supported."

The special variable \f(CW\*(C`$\{^ENCODING\}\*(C' was used to implement
the \f(CW\*(C`encoding\*(C' pragma. Setting this variable to anything other
than \f(CW\*(C`undef\*(C' was deprecated in Perl 5.22. Full deprecation
of the variable happened in Perl 5.25.3.

Setting this variable to anything other than an undefined value
throws a fatal error as of Perl 5.28.

*\f(CI\*(C`B::OP::terse\*(C'\fI*
Subsection "B::OP::terse"

This method, which just calls \f(CW\*(C`B::Concise::b_terse\*(C', has been
deprecated, and disappeared in Perl 5.28. Please use
\f(CW\*(C`B::Concise\*(C' instead.

*Use of inherited \s-1AUTOLOAD\s0 for non-method \f(CI%s::\fI%s() is no longer allowed*
Subsection "Use of inherited AUTOLOAD for non-method %s::%s() is no longer allowed"

As an (ahem) accidental feature, \f(CW\*(C`AUTOLOAD\*(C' subroutines were looked
up as methods (using the \f(CW@ISA hierarchy) even when the subroutines
to be autoloaded were called as plain functions (e.g. \f(CW\*(C`Foo::bar()\*(C'),
not as methods (e.g. \f(CW\*(C`Foo->bar()\*(C' or \f(CW\*(C`$obj->bar()\*(C').

This bug was deprecated in Perl 5.004, has been rectified in Perl 5.28
by using method lookup only for methods' \f(CW\*(C`AUTOLOAD\*(C's.

The simple rule is:  Inheritance will not work when autoloading
non-methods.  The simple fix for old code is:  In any module that used
to depend on inheriting \f(CW\*(C`AUTOLOAD\*(C' for non-methods from a base class
named \f(CW\*(C`BaseClass\*(C', execute \f(CW\*(C`*AUTOLOAD = \BaseClass::AUTOLOAD\*(C' during
startup.

In code that currently says \f(CW\*(C`use AutoLoader; @ISA = qw(AutoLoader);\*(C'
you should remove AutoLoader from \f(CW@ISA and change \f(CW\*(C`use AutoLoader;\*(C' to
\f(CW\*(C`use AutoLoader \*(AqAUTOLOAD\*(Aq;\*(C'.

*Use of code points over 0xFF in string bitwise operators*
Subsection "Use of code points over 0xFF in string bitwise operators"

The string bitwise operators, \f(CW\*(C`&\*(C', \f(CW\*(C`|\*(C', \f(CW\*(C`^\*(C', and \f(CW\*(C`~\*(C', treat
their operands as strings of bytes. As such, values above 0xFF
are nonsensical. Using such code points with these operators
was deprecated in Perl 5.24, and is fatal as of Perl 5.28.

*In \s-1XS\s0 code, use of \f(CI\*(C`to_utf8_case()\*(C'\fI*
Subsection "In XS code, use of to_utf8_case()"

This function has been removed as of Perl 5.28; instead convert to call
the appropriate one of:
\f(CW\*(C`toFOLD_utf8_safe\*(C'.
\f(CW\*(C`toLOWER_utf8_safe\*(C',
\f(CW\*(C`toTITLE_utf8_safe\*(C',
or
\f(CW\*(C`toUPPER_utf8_safe\*(C'.

### Perl 5.26

Subsection "Perl 5.26"
*\f(CI\*(C`--libpods\*(C'\fI in \f(CI\*(C`Pod::Html\*(C'\fI*
Subsection "--libpods in Pod::Html"

Since Perl 5.18, the option \f(CW\*(C`--libpods\*(C' has been deprecated, and
using this option did not do anything other than producing a warning.

The \f(CW\*(C`--libpods\*(C' option is no longer recognized as of Perl 5.26.

*The utilities \f(CI\*(C`c2ph\*(C'\fI and \f(CI\*(C`pstruct\*(C'\fI*
Subsection "The utilities c2ph and pstruct"

These old, perl3-era utilities have been deprecated in favour of
\f(CW\*(C`h2xs\*(C' for a long time. As of Perl 5.26, they have been removed.

*Trapping \f(CI\*(C`$SIG \{_\|_DIE_\|_\}\*(C'\fI other than during program exit.*
Subsection "Trapping $SIG \{__DIE__\} other than during program exit."

The \f(CW$SIG\{_\|_DIE_\|_\} hook is called even inside an \f(CW\*(C`eval()\*(C'. It was
never intended to happen this way, but an implementation glitch made
this possible. This used to be deprecated, as it allowed strange action
at a distance like rewriting a pending exception in \f(CW$@. Plans to
rectify this have been scrapped, as users found that rewriting a
pending exception is actually a useful feature, and not a bug.

Perl never issued a deprecation warning for this; the deprecation
was by documentation policy only. But this deprecation has been
lifted as of Perl 5.26.

*Malformed \s-1UTF-8\s0 string in \*(L"%s\*(R"*
Subsection "Malformed UTF-8 string in %s"

This message indicates a bug either in the Perl core or in \s-1XS\s0
code. Such code was trying to find out if a character, allegedly
stored internally encoded as \s-1UTF-8,\s0 was of a given type, such as
being punctuation or a digit.  But the character was not encoded
in legal \s-1UTF-8.\s0  The \f(CW%s is replaced by a string that can be used
by knowledgeable people to determine what the type being checked
against was.

Passing malformed strings was deprecated in Perl 5.18, and
became fatal in Perl 5.26.

### Perl 5.24

Subsection "Perl 5.24"
*Use of \f(CI*glob\{FILEHANDLE\}\fI*
Subsection "Use of *glob\{FILEHANDLE\}"

The use of \f(CW*glob\{FILEHANDLE\} was deprecated in Perl 5.8.
The intention was to use \f(CW*glob\{IO\} instead, for which
\f(CW*glob\{FILEHANDLE\} is an alias.

However, this feature was undeprecated in Perl 5.24.

*Calling POSIX::%s() is deprecated*
Subsection "Calling POSIX::%s() is deprecated"

The following functions in the \f(CW\*(C`POSIX\*(C' module are no longer available:
\f(CW\*(C`isalnum\*(C', \f(CW\*(C`isalpha\*(C', \f(CW\*(C`iscntrl\*(C', \f(CW\*(C`isdigit\*(C', \f(CW\*(C`isgraph\*(C', \f(CW\*(C`islower\*(C',
\f(CW\*(C`isprint\*(C', \f(CW\*(C`ispunct\*(C', \f(CW\*(C`isspace\*(C', \f(CW\*(C`isupper\*(C', and \f(CW\*(C`isxdigit\*(C'.  The
functions are buggy and don't work on \s-1UTF-8\s0 encoded strings.  See their
entries in \s-1POSIX\s0 for more information.

The functions were deprecated in Perl 5.20, and removed in Perl 5.24.

### Perl 5.16

Subsection "Perl 5.16"
*Use of \f(CI%s\fI on a handle without * is deprecated*
Subsection "Use of %s on a handle without * is deprecated"

It used to be possible to use \f(CW\*(C`tie\*(C', \f(CW\*(C`tied\*(C' or \f(CW\*(C`untie\*(C' on a scalar
while the scalar holds a typeglob. This caused its filehandle to be
tied. It left no way to tie the scalar itself when it held a typeglob,
and no way to untie a scalar that had had a typeglob assigned to it.

This was deprecated in Perl 5.14, and the bug was fixed in Perl 5.16.

So now \f(CW\*(C`tie $scalar\*(C' will always tie the scalar, not the handle it holds.
To tie the handle, use \f(CW\*(C`tie *$scalar\*(C' (with an explicit asterisk).  The same
applies to \f(CW\*(C`tied *$scalar\*(C' and \f(CW\*(C`untie *$scalar\*(C'.

## SEE ALSO

Header "SEE ALSO"
warnings, diagnostics.
