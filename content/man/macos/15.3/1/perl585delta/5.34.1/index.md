+++
operating_system = "macos"
manpage_section = "1"
author = "None Specified"
title = "perl585delta(1)"
detected_package_version = "5.34.1"
manpage_name = "perl585delta"
description = "This document describes differences between the 5.8.4 release and the 5.8.5 release. There are no changes incompatible with 5.8.4. Perls regular expression engine now contains support for matching on the intersection of two Unicode character clas..."
manpage_format = "troff"
date = "2022-02-19"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "how", "to", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL585DELTA 1"
PERL585DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl585delta - what is new for perl v5.8.5

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.8.4 release and
the 5.8.5 release.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes incompatible with 5.8.4.

## Core Enhancements

Header "Core Enhancements"
Perl's regular expression engine now contains support for matching on the
intersection of two Unicode character classes. You can also now refer to
user-defined character classes from within other user defined character
classes.

## Modules and Pragmata

Header "Modules and Pragmata"

- \(bu
Carp improved to work nicely with Safe. Carp's message reporting should now
be anomaly free - it will always print out line number information.

- \(bu
\s-1CGI\s0 upgraded to version 3.05

- \(bu
charnames now avoids clobbering \f(CW$_

- \(bu
Digest upgraded to version 1.08

- \(bu
Encode upgraded to version 2.01

- \(bu
FileCache upgraded to version 1.04

- \(bu
libnet upgraded to version 1.19

- \(bu
Pod::Parser upgraded to version 1.28

- \(bu
Pod::Perldoc upgraded to version 3.13

- \(bu
Pod::LaTeX upgraded to version 0.57

- \(bu
Safe now works properly with Carp

- \(bu
Scalar-List-Utils upgraded to version 1.14

- \(bu
Shell's documentation has been re-written, and its historical partial
auto-quoting of command arguments can now be disabled.

- \(bu
Test upgraded to version 1.25

- \(bu
Test::Harness upgraded to version 2.42

- \(bu
Time::Local upgraded to version 1.10

- \(bu
Unicode::Collate upgraded to version 0.40

- \(bu
Unicode::Normalize upgraded to version 0.30

## Utility Changes

Header "Utility Changes"

### Perls debugger

Subsection "Perl's debugger"
The debugger can now emulate stepping backwards, by restarting and rerunning
all bar the last command from a saved command history.

### h2ph

Subsection "h2ph"
*h2ph* is now able to understand a very limited set of C inline functions
\*(-- basically, the inline functions that look like \s-1CPP\s0 macros. This has
been introduced to deal with some of the headers of the newest versions of
the glibc. The standard warning still applies; to quote *h2ph*'s
documentation, *you may need to dicker with the files produced*.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"
Perl 5.8.5 should build cleanly from source on LynxOS.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
The in-place sort optimisation introduced in 5.8.4 had a bug. For example,
in code such as
.Sp
.Vb 1
    @a = sort ($b, @a)
.Ve
.Sp
the result would omit the value \f(CW$b. This is now fixed.

- \(bu
The optimisation for unnecessary assignments introduced in 5.8.4 could give
spurious warnings. This has been fixed.

- \(bu
Perl should now correctly detect and read BOM-marked and (BOMless) \s-1UTF-16\s0
scripts of either endianness.

- \(bu
Creating a new thread when weak references exist was buggy, and would often
cause warnings at interpreter destruction time. The known bug is now fixed.

- \(bu
Several obscure bugs involving manipulating Unicode strings with \f(CW\*(C`substr\*(C' have
been fixed.

- \(bu
Previously if Perl's file globbing function encountered a directory that it
did not have permission to open it would return immediately, leading to
unexpected truncation of the list of results. This has been fixed, to be
consistent with Unix shells' globbing behaviour.

- \(bu
Thread creation time could vary wildly between identical runs. This was caused
by a poor hashing algorithm in the thread cloning routines, which has now
been fixed.

- \(bu
The internals of the ithreads implementation were not checking if OS-level
thread creation had failed. threads->**create()** now returns \f(CW\*(C`undef\*(C' in if
thread creation fails instead of crashing perl.

## New or Changed Diagnostics

Header "New or Changed Diagnostics"

- \(bu
Perl -V has several improvements

> 
- \(bu
correctly outputs local patch names that contain embedded code snippets
or other characters that used to confuse it.

- \(bu
arguments to -V that look like regexps will give multiple lines of output.

- \(bu
a trailing colon suppresses the linefeed and ';'  terminator, allowing
embedding of queries into shell commands.

- \(bu
a leading colon removes the 'name=' part of the response, allowing mapping to
any name.



> 


- \(bu
When perl fails to find the specified script, it now outputs a second line
suggesting that the user use the \f(CW\*(C`-S\*(C' flag:
.Sp
.Vb 3
    $ perl5.8.5 missing.pl
    Can\*(Aqt open perl script "missing.pl": No such file or directory.
    Use -S to search $PATH for it.
.Ve

## Changed Internals

Header "Changed Internals"
The Unicode character class files used by the regular expression engine are
now built at build time from the supplied Unicode consortium data files,
instead of being shipped prebuilt. This makes the compressed Perl source
tarball about 200K smaller. A side effect is that the layout of files inside
lib/unicore has changed.

## Known Problems

Header "Known Problems"
The regression test *t/uni/class.t* is now performing considerably more
tests, and can take several minutes to run even on a fast machine.

## Platform Specific Problems

Header "Platform Specific Problems"
This release is known not to build on Windows 95.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://bugs.perl.org.  There may also be
information at http://www.perl.org, the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.  You can browse and search
the Perl 5 bugs at http://bugs.perl.org/

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for exhaustive details on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
