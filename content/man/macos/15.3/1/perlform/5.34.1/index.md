+++
manpage_format = "troff"
title = "perlform(1)"
date = "2022-02-19"
description = "Perl has a mechanism to help you generate simple reports and charts.  To facilitate this, Perl helps you code up your output page close to how it will look when its printed.  It can keep track of things like how many lines are on a page, what page..."
detected_package_version = "5.34.1"
manpage_section = "1"
manpage_name = "perlform"
operating_system = "macos"
operating_system_version = "15.3"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLFORM 1"
PERLFORM 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlform - Perl formats
Xref "format report chart"

## DESCRIPTION

Header "DESCRIPTION"
Perl has a mechanism to help you generate simple reports and charts.  To
facilitate this, Perl helps you code up your output page close to how it
will look when it's printed.  It can keep track of things like how many
lines are on a page, what page you're on, when to print page headers,
etc.  Keywords are borrowed from \s-1FORTRAN:\s0 **format()** to declare and **write()**
to execute; see their entries in perlfunc.  Fortunately, the layout is
much more legible, more like \s-1BASIC\s0's \s-1PRINT USING\s0 statement.  Think of it
as a poor man's **nroff**\|(1).
Xref "nroff"

Formats, like packages and subroutines, are declared rather than
executed, so they may occur at any point in your program.  (Usually it's
best to keep them all together though.) They have their own namespace
apart from all the other \*(L"types\*(R" in Perl.  This means that if you have a
function named \*(L"Foo\*(R", it is not the same thing as having a format named
\*(L"Foo\*(R".  However, the default name for the format associated with a given
filehandle is the same as the name of the filehandle.  Thus, the default
format for \s-1STDOUT\s0 is named \*(L"\s-1STDOUT\*(R",\s0 and the default format for filehandle
\s-1TEMP\s0 is named \*(L"\s-1TEMP\*(R".\s0  They just look the same.  They aren't.

Output record formats are declared as follows:

.Vb 3
    format NAME =
    FORMLIST
    .
.Ve

If the name is omitted, format \*(L"\s-1STDOUT\*(R"\s0 is defined. A single \*(L".\*(R" in
column 1 is used to terminate a format.  \s-1FORMLIST\s0 consists of a sequence
of lines, each of which may be one of three types:

- 1.
A comment, indicated by putting a '#' in the first column.

- 2.
A \*(L"picture\*(R" line giving the format for one output line.

- 3.
An argument line supplying values to plug into the previous picture line.

Picture lines contain output field definitions, intermingled with
literal text. These lines do not undergo any kind of variable interpolation.
Field definitions are made up from a set of characters, for starting and
extending a field to its desired width. This is the complete set of
characters for field definitions:
Xref "format, picture line @ ^ < | > # 0 . ... @* ^* ~ ~~"

.Vb 10
   @    start of regular field
   ^    start of special field
   <    pad character for left justification
   |    pad character for centering
   >    pad character for right justification
   #    pad character for a right-justified numeric field
   0    instead of first #: pad number with leading zeroes
   .    decimal point within a numeric field
   ...  terminate a text field, show "..." as truncation evidence
   @*   variable width field for a multi-line value
   ^*   variable width field for next line of a multi-line value
   ~    suppress line with all fields empty
   ~~   repeat line until all fields are exhausted
.Ve

Each field in a picture line starts with either \*(L"@\*(R" (at) or \*(L"^\*(R" (caret),
indicating what we'll call, respectively, a \*(L"regular\*(R" or \*(L"special\*(R" field.
The choice of pad characters determines whether a field is textual or
numeric. The tilde operators are not part of a field.  Let's look at
the various possibilities in detail.

### Text Fields

Xref "format, text field"
Subsection "Text Fields"
The length of the field is supplied by padding out the field with multiple
\*(L"<\*(R", \*(L">\*(R", or \*(L"|\*(R" characters to specify a non-numeric field with,
respectively, left justification, right justification, or centering.
For a regular field, the value (up to the first newline) is taken and
printed according to the selected justification, truncating excess characters.
If you terminate a text field with \*(L"...\*(R", three dots will be shown if
the value is truncated. A special text field may be used to do rudimentary
multi-line text block filling; see \*(L"Using Fill Mode\*(R" for details.

.Vb 7
   Example:
      format STDOUT =
      @<<<<<<   @||||||   @>>>>>>
      "left",   "middle", "right"
      .
   Output:
      left      middle    right
.Ve

### Numeric Fields

Xref "# format, numeric field"
Subsection "Numeric Fields"
Using \*(L"#\*(R" as a padding character specifies a numeric field, with
right justification. An optional \*(L".\*(R" defines the position of the
decimal point. With a \*(L"0\*(R" (zero) instead of the first \*(L"#\*(R", the
formatted number will be padded with leading zeroes if necessary.
A special numeric field is blanked out if the value is undefined.
If the resulting value would exceed the width specified the field is
filled with \*(L"#\*(R" as overflow evidence.

.Vb 7
   Example:
      format STDOUT =
      @###   @.###   @##.###  @###   @###   ^####
       42,   3.1415,  undef,    0, 10000,   undef
      .
   Output:
        42   3.142     0.000     0   ####
.Ve

### The Field @* for Variable-Width Multi-Line Text

Xref "@*"
Subsection "The Field @* for Variable-Width Multi-Line Text"
The field \*(L"@*\*(R" can be used for printing multi-line, nontruncated
values; it should (but need not) appear by itself on a line. A final
line feed is chomped off, but all other characters are emitted verbatim.

### The Field ^* for Variable-Width One-line-at-a-time Text

Xref "^*"
Subsection "The Field ^* for Variable-Width One-line-at-a-time Text"
Like \*(L"@*\*(R", this is a variable-width field. The value supplied must be a
scalar variable. Perl puts the first line (up to the first \*(L"\\n\*(R") of the
text into the field, and then chops off the front of the string so that
the next time the variable is referenced, more of the text can be printed.
The variable will *not* be restored.

.Vb 12
   Example:
      $text = "line 1\\nline 2\\nline 3";
      format STDOUT =
      Text: ^*
            $text
      ~~    ^*
            $text
      .
   Output:
      Text: line 1
            line 2
            line 3
.Ve

### Specifying Values

Xref "format, specifying values"
Subsection "Specifying Values"
The values are specified on the following format line in the same order as
the picture fields.  The expressions providing the values must be
separated by commas.  They are all evaluated in a list context
before the line is processed, so a single list expression could produce
multiple list elements.  The expressions may be spread out to more than
one line if enclosed in braces.  If so, the opening brace must be the first
token on the first line.  If an expression evaluates to a number with a
decimal part, and if the corresponding picture specifies that the decimal
part should appear in the output (that is, any picture except multiple \*(L"#\*(R"
characters **without** an embedded \*(L".\*(R"), the character used for the decimal
point is determined by the current \s-1LC_NUMERIC\s0 locale if \f(CW\*(C`use locale\*(C' is in
effect.  This means that, if, for example, the run-time environment happens
to specify a German locale, \*(L",\*(R" will be used instead of the default \*(L".\*(R".  See
perllocale and \*(L"\s-1WARNINGS\*(R"\s0 for more information.

### Using Fill Mode

Xref "format, fill mode"
Subsection "Using Fill Mode"
On text fields the caret enables a kind of fill mode.  Instead of an
arbitrary expression, the value supplied must be a scalar variable
that contains a text string.  Perl puts the next portion of the text into
the field, and then chops off the front of the string so that the next time
the variable is referenced, more of the text can be printed.  (Yes, this
means that the variable itself is altered during execution of the **write()**
call, and is not restored.)  The next portion of text is determined by
a crude line-breaking algorithm. You may use the carriage return character
(\f(CW\*(C`\\r\*(C') to force a line break. You can change which characters are legal
to break on by changing the variable \f(CW$: (that's
\f(CW$FORMAT_LINE_BREAK_CHARACTERS if you're using the English module) to a
list of the desired characters.

Normally you would use a sequence of fields in a vertical stack associated
with the same scalar variable to print out a block of text. You might wish
to end the final field with the text \*(L"...\*(R", which will appear in the output
if the text was too long to appear in its entirety.

### Suppressing Lines Where All Fields Are Void

Xref "format, suppressing lines"
Subsection "Suppressing Lines Where All Fields Are Void"
Using caret fields can produce lines where all fields are blank. You can
suppress such lines by putting a \*(L"~\*(R" (tilde) character anywhere in the
line.  The tilde will be translated to a space upon output.

### Repeating Format Lines

Xref "format, repeating lines"
Subsection "Repeating Format Lines"
If you put two contiguous tilde characters \*(L"~~\*(R" anywhere into a line,
the line will be repeated until all the fields on the line are exhausted,
i.e. undefined. For special (caret) text fields this will occur sooner or
later, but if you use a text field of the at variety, the  expression you
supply had better not give the same value every time forever! (\f(CW\*(C`shift(@f)\*(C'
is a simple example that would work.)  Don't use a regular (at) numeric
field in such lines, because it will never go blank.

### Top of Form Processing

Xref "format, top of form top header"
Subsection "Top of Form Processing"
Top-of-form processing is by default handled by a format with the
same name as the current filehandle with \*(L"_TOP\*(R" concatenated to it.
It's triggered at the top of each page.  See \*(L"write\*(R" in perlfunc.

Examples:

.Vb 10
 # a report on the /etc/passwd file
 format STDOUT_TOP =
                         Passwd File
 Name                Login    Office   Uid   Gid Home
 ------------------------------------------------------------------
 .
 format STDOUT =
 @<<<<<<<<<<<<<<<<<< @||||||| @<<<<<<@>>>> @>>>> @<<<<<<<<<<<<<<<<<
 $name,              $login,  $office,$uid,$gid, $home
 .


 # a report from a bug report form
 format STDOUT_TOP =
                         Bug Reports
 @<<<<<<<<<<<<<<<<<<<<<<<     @|||         @>>>>>>>>>>>>>>>>>>>>>>>
 $system,                      $%,         $date
 ------------------------------------------------------------------
 .
 format STDOUT =
 Subject: @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          $subject
 Index: @<<<<<<<<<<<<<<<<<<<<<<<<<<<< ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        $index,                       $description
 Priority: @<<<<<<<<<< Date: @<<<<<<< ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
           $priority,        $date,   $description
 From: @<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
       $from,                         $description
 Assigned to: @<<<<<<<<<<<<<<<<<<<<<< ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
              $programmer,            $description
 ~                                    ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                                      $description
 ~                                    ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                                      $description
 ~                                    ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                                      $description
 ~                                    ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                                      $description
 ~                                    ^<<<<<<<<<<<<<<<<<<<<<<<...
                                      $description
 .
.Ve

It is possible to intermix **print()**s with **write()**s on the same output
channel, but you'll have to handle \f(CW\*(C`$-\*(C' (\f(CW$FORMAT_LINES_LEFT)
yourself.

### Format Variables

Xref "format variables format, variables"
Subsection "Format Variables"
The current format name is stored in the variable \f(CW$~ (\f(CW$FORMAT_NAME),
and the current top of form format name is in \f(CW$^ (\f(CW$FORMAT_TOP_NAME).
The current output page number is stored in \f(CW$% (\f(CW$FORMAT_PAGE_NUMBER),
and the number of lines on the page is in \f(CW$= (\f(CW$FORMAT_LINES_PER_PAGE).
Whether to autoflush output on this handle is stored in \f(CW$|
(\f(CW$OUTPUT_AUTOFLUSH).  The string output before each top of page (except
the first) is stored in \f(CW$^L (\f(CW$FORMAT_FORMFEED).  These variables are
set on a per-filehandle basis, so you'll need to **select()** into a different
one to affect them:

.Vb 4
    select((select(OUTF),
            $~ = "My_Other_Format",
            $^ = "My_Top_Format"
           )[0]);
.Ve

Pretty ugly, eh?  It's a common idiom though, so don't be too surprised
when you see it.  You can at least use a temporary variable to hold
the previous filehandle: (this is a much better approach in general,
because not only does legibility improve, you now have an intermediary
stage in the expression to single-step the debugger through):

.Vb 4
    $ofh = select(OUTF);
    $~ = "My_Other_Format";
    $^ = "My_Top_Format";
    select($ofh);
.Ve

If you use the English module, you can even read the variable names:

.Vb 5
    use English;
    $ofh = select(OUTF);
    $FORMAT_NAME     = "My_Other_Format";
    $FORMAT_TOP_NAME = "My_Top_Format";
    select($ofh);
.Ve

But you still have those funny **select()**s.  So just use the FileHandle
module.  Now, you can access these special variables using lowercase
method names instead:

.Vb 3
    use FileHandle;
    format_name     OUTF "My_Other_Format";
    format_top_name OUTF "My_Top_Format";
.Ve

Much better!

## NOTES

Header "NOTES"
Because the values line may contain arbitrary expressions (for at fields,
not caret fields), you can farm out more sophisticated processing
to other functions, like **sprintf()** or one of your own.  For example:

.Vb 4
    format Ident =
        @<<<<<<<<<<<<<<<
        &commify($n)
    .
.Ve

To get a real at or caret into the field, do this:

.Vb 4
    format Ident =
    I have an @ here.
            "@"
    .
.Ve

To center a whole line of text, do something like this:

.Vb 4
    format Ident =
    @|||||||||||||||||||||||||||||||||||||||||||||||
            "Some text line"
    .
.Ve

There is no builtin way to say \*(L"float this to the right hand side
of the page, however wide it is.\*(R"  You have to specify where it goes.
The truly desperate can generate their own format on the fly, based
on the current number of columns, and then **eval()** it:

.Vb 9
    $format  = "format STDOUT = \\n"
             . \*(Aq^\*(Aq . \*(Aq<\*(Aq x $cols . "\\n"
             . \*(Aq$entry\*(Aq . "\\n"
             . "\\t^" . "<" x ($cols-8) . "~~\\n"
             . \*(Aq$entry\*(Aq . "\\n"
             . ".\\n";
    print $format if $Debugging;
    eval $format;
    die $@ if $@;
.Ve

Which would generate a format looking something like this:

.Vb 6
 format STDOUT =
 ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 $entry
         ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<~~
 $entry
 .
.Ve

Here's a little program that's somewhat like **fmt**\|(1):

.Vb 3
 format =
 ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ~~
 $_

 .

 $/ = \*(Aq\*(Aq;
 while (<>) \{
     s/\\s*\\n\\s*/ /g;
     write;
 \}
.Ve

### Footers

Xref "format, footer footer"
Subsection "Footers"
While \f(CW$FORMAT_TOP_NAME contains the name of the current header format,
there is no corresponding mechanism to automatically do the same thing
for a footer.  Not knowing how big a format is going to be until you
evaluate it is one of the major problems.  It's on the \s-1TODO\s0 list.

Here's one strategy:  If you have a fixed-size footer, you can get footers
by checking \f(CW$FORMAT_LINES_LEFT before each **write()** and print the footer
yourself if necessary.

Here's another strategy: Open a pipe to yourself, using \f(CW\*(C`open(MYSELF, "|-")\*(C'
(see \*(L"open\*(R" in perlfunc) and always **write()** to \s-1MYSELF\s0 instead of \s-1STDOUT.\s0
Have your child process massage its \s-1STDIN\s0 to rearrange headers and footers
however you like.  Not very convenient, but doable.

### Accessing Formatting Internals

Xref "format, internals"
Subsection "Accessing Formatting Internals"
For low-level access to the formatting mechanism, you may use **formline()**
and access \f(CW$^A (the \f(CW$ACCUMULATOR variable) directly.

For example:

.Vb 3
    $str = formline <<\*(AqEND\*(Aq, 1,2,3;
    @<<<  @|||  @>>>
    END

    print "Wow, I just stored \*(Aq$^A\*(Aq in the accumulator!\\n";
.Ve

Or to make an **swrite()** subroutine, which is to **write()** what **sprintf()**
is to **printf()**, do this:

.Vb 8
    use Carp;
    sub swrite \{
        croak "usage: swrite PICTURE ARGS" unless @_;
        my $format = shift;
        $^A = "";
        formline($format,@_);
        return $^A;
    \}

    $string = swrite(<<\*(AqEND\*(Aq, 1, 2, 3);
 Check me out
 @<<<  @|||  @>>>
 END
    print $string;
.Ve

## WARNINGS

Header "WARNINGS"
The lone dot that ends a format can also prematurely end a mail
message passing through a misconfigured Internet mailer (and based on
experience, such misconfiguration is the rule, not the exception).  So
when sending format code through mail, you should indent it so that
the format-ending dot is not on the left margin; this will prevent
\s-1SMTP\s0 cutoff.

Lexical variables (declared with \*(L"my\*(R") are not visible within a
format unless the format is declared within the scope of the lexical
variable.

If a program's environment specifies an \s-1LC_NUMERIC\s0 locale and \f(CW\*(C`use
locale\*(C' is in effect when the format is declared, the locale is used
to specify the decimal point character in formatted output.  Formatted
output cannot be controlled by \f(CW\*(C`use locale\*(C' at the time when **write()**
is called. See perllocale for further discussion of locale handling.

Within strings that are to be displayed in a fixed-length text field,
each control character is substituted by a space. (But remember the
special meaning of \f(CW\*(C`\\r\*(C' when using fill mode.) This is done to avoid
misalignment when control characters \*(L"disappear\*(R" on some output media.
