+++
manpage_format = "troff"
operating_system_version = "15.3"
manpage_name = "encode_keychange"
author = "None Specified"
manpage_section = "1"
date = "2006-01-01"
operating_system = "macos"
title = "encode_keychange(1)"
keywords = ["the", "localized", "key", "method", "is", "defined", "in", "rfc", "2274", "sections", "2", "6", "and", "a", "originally", "documented", "u", "blumenthal", "n", "c", "hien", "b", "wijnen", "derivation", "for", "network", "management", "applications", "ieee", "magazine", "april", "may", "issue", "1997"]
detected_package_version = "0"
description = "produces a KeyChange string using the old and new passphrases  as described in Section 5 of RFC 2274 User-based Security Model (USM) for  version 3 of the Simple Network Management Protocol (SNMPv3). -t option is mandatory and specifies the hash ..."
+++

encode_keychange 1 "16 Nov 2006" V5.6.2.1 "Net-SNMP"

## NAME

encode_keychange - produce the KeyChange string for SNMPv3

## SYNOPSIS

encode_keychange
-t md5|sha1
[*OPTIONS*]

## DESCRIPTION

encode_keychange
produces a KeyChange string using the old and new passphrases
as described in Section 5 of RFC 2274 "User-based Security Model (USM) for
version 3 of the Simple Network Management Protocol (SNMPv3)". **-t**
option is mandatory and specifies the hash transform type to use.

The transform is used to convert passphrase to master key for a given
user (Ku), convert master key to the localized key (Kul), and to hash the
old Kul with the random bits.

Passphrases are obtained by examining a number of sources until success
(in order listed):
.IP
command line options (see
-N
and
-O
options below);
.IP
the file
$HOME/.snmp/passphrase.ek
which should only contain two lines with old and new passphrase;
.IP
standard input **-or-**  user input from the terminal.



## OPTIONS


**-E** [0x]<*engineID*> EngineID used for Kul generation.
<*engineID*> is intepreted as a hex string when preceeded by 0x,
otherwise it is treated as a text string. If no <*engineID*> is
specified, it is constructed from the first IP address for the local
host.

**-f**
Force passphrases to be read from standard input.

**-h**
Display the help message.

**-N** "<*new_passphrase*>"
Passphrase used to generate the new Ku.

**-O** "<*old_passphrase*>"
Passphrase used to generate the old Ku.

**-P**
Turn off the prompt for passphrases when getting data from standard input.

**-v**
Be verbose.

**-V**
Echo passphrases to terminal.



## SEE ALSO

The localized key method is defined in RFC 2274, Sections 2.6 and A.2, and
originally documented in
.IP
U. Blumenthal, N. C. Hien, B. Wijnen,
"Key Derivation for Network Management Applications",
IEEE Network Magazine, April/May issue, 1997.
