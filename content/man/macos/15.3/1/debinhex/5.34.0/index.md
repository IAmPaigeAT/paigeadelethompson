+++
title = "debinhex(1)"
operating_system_version = "15.3"
manpage_name = "debinhex"
manpage_section = "1"
description = "Each file is expected to be a BinHex file.  By default, the output file is given the name that the BinHex file dictates, regardless of the name of the BinHex file. Largely untested. Paul J. Schinder (s-1NASA/GSFCs0) mostly, though Eryq cant seem ..."
manpage_format = "troff"
detected_package_version = "5.34.0"
date = "2015-11-15"
author = "None Specified"
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "DEBINHEX 1"
DEBINHEX 1 "2015-11-15" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

debinhex.pl - use Convert::BinHex to decode BinHex files

## USAGE

Header "USAGE"
Usage:

.Vb 1
    debinhex.pl [options] file ... file
.Ve

Where the options are:

.Vb 2
    -o dir    Output in given directory (default outputs in file\*(Aqs directory)
    -v        Verbose output (normally just one line per file is shown)
.Ve

## DESCRIPTION

Header "DESCRIPTION"
Each file is expected to be a BinHex file.  By default, the output file is
given the name that the BinHex file dictates, regardless of the name of
the BinHex file.

## WARNINGS

Header "WARNINGS"
Largely untested.

## AUTHORS

Header "AUTHORS"
Paul J. Schinder (\s-1NASA/GSFC\s0) mostly, though Eryq can't seem to keep
his grubby paws off anything...

Sören M. Andersen (somian), made it actually work under Perl 5.8.7 on MSWin32.
