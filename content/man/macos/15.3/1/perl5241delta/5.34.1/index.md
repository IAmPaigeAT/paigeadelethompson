+++
author = "None Specified"
manpage_section = "1"
manpage_format = "troff"
detected_package_version = "5.34.1"
operating_system = "macos"
description = "This document describes differences between the 5.24.0 release and the 5.24.1 release. If you are upgrading from an earlier release such as 5.22.0, first read perl5240delta, which describes differences between 5.22.0 and 5.24.0. Previously PerlIO ..."
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
date = "2022-02-19"
title = "perl5241delta(1)"
manpage_name = "perl5241delta"
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5241DELTA 1"
PERL5241DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5241delta - what is new for perl v5.24.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.24.0 release and the 5.24.1
release.

If you are upgrading from an earlier release such as 5.22.0, first read
perl5240delta, which describes differences between 5.22.0 and 5.24.0.

## Security

Header "Security"

### \fB-Di switch is now required for PerlIO debugging output

Subsection "-Di switch is now required for PerlIO debugging output"
Previously PerlIO debugging output would be sent to the file specified by the
\f(CW\*(C`PERLIO_DEBUG\*(C' environment variable if perl wasn't running setuid and the
**-T** or **-t** switches hadn't been parsed yet.

If perl performed output at a point where it hadn't yet parsed its switches
this could result in perl creating or overwriting the file named by
\f(CW\*(C`PERLIO_DEBUG\*(C' even when the **-T** switch had been supplied.

Perl now requires the **-Di** switch to produce PerlIO debugging output.  By
default this is written to \f(CW\*(C`stderr\*(C', but can optionally be redirected to a
file by setting the \f(CW\*(C`PERLIO_DEBUG\*(C' environment variable.

If perl is running setuid or the **-T** switch was supplied \f(CW\*(C`PERLIO_DEBUG\*(C' is
ignored and the debugging output is sent to \f(CW\*(C`stderr\*(C' as for any other **-D**
switch.
.ie n .SS "Core modules and tools no longer search *"".""* for optional modules"
.el .SS "Core modules and tools no longer search *``.''* for optional modules"
Subsection "Core modules and tools no longer search . for optional modules"
The tools and many modules supplied in core no longer search the default
current directory entry in \f(CW@INC for optional modules.  For
example, Storable will remove the final *\*(L".\*(R"* from \f(CW@INC before trying to
load Log::Agent.

This prevents an attacker injecting an optional module into a process run by
another user where the current directory is writable by the attacker, e.g. the
*/tmp* directory.

In most cases this removal should not cause problems, but difficulties were
encountered with base, which treats every module name supplied as optional.
These difficulties have not yet been resolved, so for this release there are no
changes to base.  We hope to have a fix for base in Perl 5.24.2.

To protect your own code from this attack, either remove the default *\*(L".\*(R"*
entry from \f(CW@INC at the start of your script, so:

.Vb 3
  #!/usr/bin/perl
  use strict;
  ...
.Ve

becomes:

.Vb 4
  #!/usr/bin/perl
  BEGIN \{ pop @INC if $INC[-1] eq \*(Aq.\*(Aq \}
  use strict;
  ...
.Ve

or for modules, remove *\*(L".\*(R"* from a localized \f(CW@INC, so:

.Vb 1
  my $can_foo = eval \{ require Foo; \}
.Ve

becomes:

.Vb 5
  my $can_foo = eval \{
      local @INC = @INC;
      pop @INC if $INC[-1] eq \*(Aq.\*(Aq;
      require Foo;
  \};
.Ve

## Incompatible Changes

Header "Incompatible Changes"
Other than the security changes above there are no changes intentionally
incompatible with Perl 5.24.0.  If any exist, they are bugs, and we request
that you submit a report.  See \*(L"Reporting Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Archive::Tar has been upgraded from version 2.04 to 2.04_01.

- \(bu
bignum has been upgraded from version 0.42 to 0.42_01.

- \(bu
\s-1CPAN\s0 has been upgraded from version 2.11 to 2.11_01.

- \(bu
Digest has been upgraded from version 1.17 to 1.17_01.

- \(bu
Digest::SHA has been upgraded from version 5.95 to 5.95_01.

- \(bu
Encode has been upgraded from version 2.80 to 2.80_01.

- \(bu
ExtUtils::MakeMaker has been upgraded from version 7.10_01 to 7.10_02.

- \(bu
File::Fetch has been upgraded from version 0.48 to 0.48_01.

- \(bu
File::Spec has been upgraded from version 3.63 to 3.63_01.

- \(bu
HTTP::Tiny has been upgraded from version 0.056 to 0.056_001.

- \(bu
\s-1IO\s0 has been upgraded from version 1.36 to 1.36_01.

- \(bu
The IO-Compress modules have been upgraded from version 2.069 to 2.069_001.

- \(bu
IPC::Cmd has been upgraded from version 0.92 to 0.92_01.

- \(bu
\s-1JSON::PP\s0 has been upgraded from version 2.27300 to 2.27300_01.

- \(bu
Locale::Maketext has been upgraded from version 1.26 to 1.26_01.

- \(bu
Locale::Maketext::Simple has been upgraded from version 0.21 to 0.21_01.

- \(bu
Memoize has been upgraded from version 1.03 to 1.03_01.

- \(bu
Module::CoreList has been upgraded from version 5.20160506 to 5.20170114_24.

- \(bu
Net::Ping has been upgraded from version 2.43 to 2.43_01.

- \(bu
Parse::CPAN::Meta has been upgraded from version 1.4417 to 1.4417_001.

- \(bu
Pod::Html has been upgraded from version 1.22 to 1.2201.

- \(bu
Pod::Perldoc has been upgraded from version 3.25_02 to 3.25_03.

- \(bu
Storable has been upgraded from version 2.56 to 2.56_01.

- \(bu
Sys::Syslog has been upgraded from version 0.33 to 0.33_01.

- \(bu
Test has been upgraded from version 1.28 to 1.28_01.

- \(bu
Test::Harness has been upgraded from version 3.36 to 3.36_01.

- \(bu
XSLoader has been upgraded from version 0.21 to 0.22, fixing a security hole
in which binary files could be loaded from a path outside of \f(CW@INC.
[\s-1GH\s0 #15418] <https://github.com/Perl/perl5/issues/15418>

## Documentation

Header "Documentation"

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perlapio*
Subsection "perlapio"

- \(bu
The documentation of \f(CW\*(C`PERLIO_DEBUG\*(C' has been updated.

*perlrun*
Subsection "perlrun"

- \(bu
The new **-Di** switch has been documented, and the documentation of
\f(CW\*(C`PERLIO_DEBUG\*(C' has been updated.

## Testing

Header "Testing"

- \(bu
A new test script, *t/run/switchDx.t*, has been added to test that the new
**-Di** switch is working correctly.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
The change to hashbang redirection introduced in Perl 5.24.0, whereby perl
would redirect to another interpreter (Perl 6) if it found a hashbang path
which contains \*(L"perl\*(R" followed by \*(L"6\*(R", has been reverted because it broke in
cases such as \f(CW\*(C`#!/opt/perl64/bin/perl\*(C'.

## Acknowledgements

Header "Acknowledgements"
Perl 5.24.1 represents approximately 8 months of development since Perl 5.24.0
and contains approximately 8,100 lines of changes across 240 files from 18
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 2,200 lines of changes to 170 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.24.1:

Aaron Crane, Alex Vandiver, Aristotle Pagaltzis, Chad Granum, Chris 'BinGOs'
Williams, Craig A. Berry, Father Chrysostomos, James E Keenan, Jarkko
Hietaniemi, Karen Etheridge, Leon Timmermans, Matthew Horsfall, Ricardo Signes,
Sawyer X, Se\*'bastien Aperghis-Tramoni, Stevan Little, Steve Hay, Tony Cook.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the Perl bug database at
<https://rt.perl.org/> .  There may also be information at
<http://www.perl.org/> , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec for details of how to
report the issue.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
