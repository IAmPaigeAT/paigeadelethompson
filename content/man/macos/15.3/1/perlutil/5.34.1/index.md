+++
detected_package_version = "5.34.1"
operating_system = "macos"
title = "perlutil(1)"
operating_system_version = "15.3"
date = "2022-02-19"
keywords = ["header", "see", "also", "perldoc", "pod2man", "pod2text", "pod2html", "pod2usage", "podchecker", "splain", "pl2pm", "perlbug", "h2ph", "h2xs", "enc2xs", "xsubpp", "cpan", "encguess", "instmodsh", "json_pp", "piconv", "prove", "corelist", "ptar", "ptardiff", "shasum", "streamzip", "zipdetails"]
manpage_format = "troff"
description = "Along with the Perl interpreter itself, the Perl distribution installs a range of utilities on your system. There are also several utilities which are used by the Perl distribution itself as part of the install process. This document exists to list..."
manpage_name = "perlutil"
author = "None Specified"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLUTIL 1"
PERLUTIL 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlutil - utilities packaged with the Perl distribution

## DESCRIPTION

Header "DESCRIPTION"
Along with the Perl interpreter itself, the Perl distribution installs a
range of utilities on your system. There are also several utilities
which are used by the Perl distribution itself as part of the install
process. This document exists to list all of these utilities, explain
what they are for and provide pointers to each module's documentation,
if appropriate.

## LIST OF UTILITIES

Header "LIST OF UTILITIES"

### Documentation

Subsection "Documentation"

- perldoc
Item "perldoc"
The main interface to Perl's documentation is *perldoc*, although
if you're reading this, it's more than likely that you've already found
it. *perldoc* will extract and format the documentation from any file
in the current directory, any Perl module installed on the system, or
any of the standard documentation pages, such as this one. Use
\f(CW\*(C`perldoc <name>\*(C' to get information on any of the utilities
described in this document.

- pod2man
Item "pod2man"
0

- pod2text
Item "pod2text"
.PD
If it's run from a terminal, *perldoc* will usually call *pod2man* to
translate \s-1POD\s0 (Plain Old Documentation - see perlpod for an
explanation) into a manpage, and then run *man* to display it; if
*man* isn't available, *pod2text* will be used instead and the output
piped through your favourite pager.

- pod2html
Item "pod2html"
As well as these two, there is another converter: *pod2html* will
produce \s-1HTML\s0 pages from \s-1POD.\s0

- pod2usage
Item "pod2usage"
If you just want to know how to use the utilities described here,
*pod2usage* will just extract the \*(L"\s-1USAGE\*(R"\s0 section; some of
the utilities will automatically call *pod2usage* on themselves when
you call them with \f(CW\*(C`-help\*(C'.

- podchecker
Item "podchecker"
If you're writing your own documentation in \s-1POD,\s0 the *podchecker*
utility will look for errors in your markup.

- splain
Item "splain"
*splain* is an interface to perldiag - paste in your error message
to it, and it'll explain it for you.

- \fIroffitall
Item "roffitall"
The *roffitall* utility is not installed on your system but lives in
the *pod/* directory of your Perl source kit; it converts all the
documentation from the distribution to **roff* format, and produces a
typeset PostScript or text file of the whole lot.

### Converters

Subsection "Converters"

- pl2pm
Item "pl2pm"
To help you convert legacy programs to more modern Perl, the
*pl2pm* utility will help you convert old-style Perl 4 libraries
to new-style Perl5 modules.

### Administration

Subsection "Administration"

- libnetcfg
Item "libnetcfg"
To display and change the libnet configuration run the libnetcfg command.

- perlivp
Item "perlivp"
The *perlivp* program is set up at Perl source code build time to test
the Perl version it was built under.  It can be used after running \f(CW\*(C`make
install\*(C' (or your platform's equivalent procedure) to verify that perl
and its libraries have been installed correctly.

### Development

Subsection "Development"
There are a set of utilities which help you in developing Perl programs,
and in particular, extending Perl with C.

- perlbug
Item "perlbug"
*perlbug* used to be the recommended way to report bugs in the perl
interpreter itself or any of the standard library modules back to the
developers; bug reports and patches should now be submitted to
<https://github.com/Perl/perl5/issues>.

- perlthanks
Item "perlthanks"
This program provides an easy way to send a thank-you message back to the
authors and maintainers of perl. It's just *perlbug* installed under
another name.

- h2ph
Item "h2ph"
Back before Perl had the \s-1XS\s0 system for connecting with C libraries,
programmers used to get library constants by reading through the C
header files. You may still see \f(CW\*(C`require \*(Aqsyscall.ph\*(Aq\*(C' or similar
around - the *.ph* file should be created by running *h2ph* on the
corresponding *.h* file. See the *h2ph* documentation for more on how
to convert a whole bunch of header files at once.

- h2xs
Item "h2xs"
*h2xs* converts C header files into \s-1XS\s0 modules, and will try and write
as much glue between C libraries and Perl modules as it can. It's also
very useful for creating skeletons of pure Perl modules.

- enc2xs
Item "enc2xs"
*enc2xs* builds a Perl extension for use by Encode from either
Unicode Character Mapping files (.ucm) or Tcl Encoding Files (.enc).
Besides being used internally during the build process of the Encode
module, you can use *enc2xs* to add your own encoding to perl.
No knowledge of \s-1XS\s0 is necessary.

- xsubpp
Item "xsubpp"
*xsubpp* is a compiler to convert Perl \s-1XS\s0 code into C code.
It is typically run by the makefiles created by ExtUtils::MakeMaker.
.Sp
*xsubpp* will compile \s-1XS\s0 code into C code by embedding the constructs
necessary to let C functions manipulate Perl values and creates the glue
necessary to let Perl access those functions.

- prove
Item "prove"
*prove* is a command-line interface to the test-running functionality
of Test::Harness.  It's an alternative to \f(CW\*(C`make test\*(C'.

- corelist
Item "corelist"
A command-line front-end to Module::CoreList, to query what modules
were shipped with given versions of perl.

### General tools

Subsection "General tools"
A few general-purpose tools are shipped with perl, mostly because they
came along modules included in the perl distribution.

- encguess
Item "encguess"
*encguess* will attempt to guess the character encoding of files.

- json_pp
Item "json_pp"
*json_pp* is a pure Perl \s-1JSON\s0 converter and formatter.

- piconv
Item "piconv"
*piconv* is a Perl version of **iconv**\|(1), a character encoding converter
widely available for various Unixen today.  This script was primarily a
technology demonstrator for Perl v5.8.0, but you can use piconv in the
place of iconv for virtually any case.

- ptar
Item "ptar"
*ptar* is a tar-like program, written in pure Perl.

- ptardiff
Item "ptardiff"
*ptardiff* is a small utility that produces a diff between an extracted
archive and an unextracted one. (Note that this utility requires the
Text::Diff module to function properly; this module isn't distributed
with perl, but is available from the \s-1CPAN.\s0)

- ptargrep
Item "ptargrep"
*ptargrep* is a utility to apply pattern matching to the contents of files
in a tar archive.

- shasum
Item "shasum"
This utility, that comes with the Digest::SHA module, is used to print
or verify \s-1SHA\s0 checksums.

- streamzip
Item "streamzip"
*streamzip* compresses data streamed to \s-1STDIN\s0 into a streamed zip container.

- zipdetails
Item "zipdetails"
*zipdetails* displays information about the internal record structure of the zip file.
It is not concerned with displaying any details of the compressed data stored in the zip file.

### Installation

Subsection "Installation"
These utilities help manage extra Perl modules that don't come with the perl
distribution.

- cpan
Item "cpan"
*cpan* is a command-line interface to \s-1CPAN\s0.pm.  It allows you to install
modules or distributions from \s-1CPAN,\s0 or just get information about them, and
a lot more.  It is similar to the command line mode of the \s-1CPAN\s0 module,
.Sp
.Vb 1
    perl -MCPAN -e shell
.Ve

- instmodsh
Item "instmodsh"
A little interface to ExtUtils::Installed to examine installed modules,
validate your packlists and even create a tarball from an installed module.

## SEE ALSO

Header "SEE ALSO"
perldoc, pod2man, pod2text, pod2html, pod2usage,
podchecker, splain, pl2pm,
perlbug, h2ph, h2xs, enc2xs,
xsubpp, cpan, encguess, instmodsh, json_pp,
piconv, prove, corelist, ptar,
ptardiff, shasum, streamzip, zipdetails
