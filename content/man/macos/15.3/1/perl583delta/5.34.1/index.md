+++
title = "perl583delta(1)"
detected_package_version = "5.34.1"
manpage_section = "1"
operating_system = "macos"
date = "2022-02-19"
manpage_name = "perl583delta"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "how", "to", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
author = "None Specified"
operating_system_version = "15.3"
description = "This document describes differences between the 5.8.2 release and the 5.8.3 release. If you are upgrading from an earlier release such as 5.6.1, first read the perl58delta, which describes differences between 5.6.0 and 5.8.0, and the perl581delta ..."
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL583DELTA 1"
PERL583DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl583delta - what is new for perl v5.8.3

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.8.2 release and
the 5.8.3 release.

If you are upgrading from an earlier release such as 5.6.1, first read
the perl58delta, which describes differences between 5.6.0 and
5.8.0, and the perl581delta and perl582delta, which describe differences
between 5.8.0, 5.8.1 and 5.8.2

## Incompatible Changes

Header "Incompatible Changes"
There are no changes incompatible with 5.8.2.

## Core Enhancements

Header "Core Enhancements"
A \f(CW\*(C`SCALAR\*(C' method is now available for tied hashes. This is called when
a tied hash is used in scalar context, such as

.Vb 3
    if (%tied_hash) \{
        ...
    \}
.Ve

The old behaviour was that \f(CW%tied_hash would return whatever would have been
returned for that hash before the hash was tied (so usually 0). The new
behaviour in the absence of a \s-1SCALAR\s0 method is to return \s-1TRUE\s0 if in the
middle of an \f(CW\*(C`each\*(C' iteration, and otherwise call \s-1FIRSTKEY\s0 to check if the
hash is empty (making sure that a subsequent \f(CW\*(C`each\*(C' will also begin by
calling \s-1FIRSTKEY\s0). Please see \*(L"\s-1SCALAR\*(R"\s0 in perltie for the full details and
caveats.

## Modules and Pragmata

Header "Modules and Pragmata"

- \s-1CGI\s0
Item "CGI"
0

- Cwd
Item "Cwd"

- Digest
Item "Digest"

- Digest::MD5
Item "Digest::MD5"

- Encode
Item "Encode"

- File::Spec
Item "File::Spec"

- FindBin
Item "FindBin"
.PD
A function \f(CW\*(C`again\*(C' is provided to resolve problems where modules in different
directories wish to use FindBin.

- List::Util
Item "List::Util"
You can now weaken references to read only values.

- Math::BigInt
Item "Math::BigInt"
0

- PodParser
Item "PodParser"

- Pod::Perldoc
Item "Pod::Perldoc"

- \s-1POSIX\s0
Item "POSIX"

- Unicode::Collate
Item "Unicode::Collate"

- Unicode::Normalize
Item "Unicode::Normalize"

- Test::Harness
Item "Test::Harness"

- threads::shared
Item "threads::shared"
.PD
\f(CW\*(C`cond_wait\*(C' has a new two argument form. \f(CW\*(C`cond_timedwait\*(C' has been added.

## Utility Changes

Header "Utility Changes"
\f(CW\*(C`find2perl\*(C' now assumes \f(CW\*(C`-print\*(C' as a default action. Previously, it
needed to be specified explicitly.

A new utility, \f(CW\*(C`prove\*(C', makes it easy to run an individual regression test
at the command line. \f(CW\*(C`prove\*(C' is part of Test::Harness, which users of earlier
Perl versions can install from \s-1CPAN.\s0

## New Documentation

Header "New Documentation"
The documentation has been revised in places to produce more standard manpages.

The documentation for the special code blocks (\s-1BEGIN, CHECK, INIT, END\s0)
has been improved.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"
Perl now builds on OpenVMS I64

## Selected Bug Fixes

Header "Selected Bug Fixes"
Using **substr()** on a \s-1UTF8\s0 string could cause subsequent accesses on that
string to return garbage. This was due to incorrect \s-1UTF8\s0 offsets being
cached, and is now fixed.

**join()** could return garbage when the same **join()** statement was used to
process 8 bit data having earlier processed \s-1UTF8\s0 data, due to the flags
on that statement's temporary workspace not being reset correctly. This
is now fixed.

\f(CW\*(C`$a .. $b\*(C' will now work as expected when either \f(CW$a or \f(CW$b is \f(CW\*(C`undef\*(C'

Using Unicode keys with tied hashes should now work correctly.

Reading $^E now preserves $!. Previously, the C code implementing $^E
did not preserve \f(CW\*(C`errno\*(C', so reading $^E could cause \f(CW\*(C`errno\*(C' and therefore
\f(CW$! to change unexpectedly.

Reentrant functions will (once more) work with \*(C+. 5.8.2 introduced a bugfix
which accidentally broke the compilation of Perl extensions written in \*(C+

## New or Changed Diagnostics

Header "New or Changed Diagnostics"
The fatal error \*(L"\s-1DESTROY\s0 created new reference to dead object\*(R" is now
documented in perldiag.

## Changed Internals

Header "Changed Internals"
The hash code has been refactored to reduce source duplication. The
external interface is unchanged, and aside from the bug fixes described
above, there should be no change in behaviour.

\f(CW\*(C`hv_clear_placeholders\*(C' is now part of the perl \s-1API\s0

Some C macros have been tidied. In particular macros which create temporary
local variables now name these variables more defensively, which should
avoid bugs where names clash.

<signal.h> is now always included.

## Configuration and Building

Header "Configuration and Building"
\f(CW\*(C`Configure\*(C' now invokes callbacks regardless of the value of the variable
they are called for. Previously callbacks were only invoked in the
\f(CW\*(C`case $variable $define)\*(C' branch. This change should only affect platform
maintainers writing configuration hints files.

## Platform Specific Problems

Header "Platform Specific Problems"
The regression test ext/threads/shared/t/wait.t fails on early RedHat 9
and HP-UX 10.20 due to bugs in their threading implementations.
RedHat users should see https://rhn.redhat.com/errata/RHBA-2003-136.html
and consider upgrading their glibc.

## Known Problems

Header "Known Problems"
Detached threads aren't supported on Windows yet, as they may lead to
memory access violation problems.

There is a known race condition opening scripts in \f(CW\*(C`suidperl\*(C'. \f(CW\*(C`suidperl\*(C'
is neither built nor installed by default, and has been deprecated since
perl 5.8.0. You are advised to replace use of suidperl with tools such
as sudo ( http://www.courtesan.com/sudo/ )

We have a backlog of unresolved bugs. Dealing with bugs and bug reports
is unglamorous work; not something ideally suited to volunteer labour,
but that is all that we have.

The perl5 development team are implementing changes to help address this
problem, which should go live in early 2004.

## Future Directions

Header "Future Directions"
Code freeze for the next maintenance release (5.8.4) is on March 31st 2004,
with release expected by mid April. Similarly 5.8.5's freeze will be at
the end of June, with release by mid July.

## Obituary

Header "Obituary"
Iain 'Spoon' Truskett, Perl hacker, author of perlreref and
contributor to \s-1CPAN,\s0 died suddenly on 29th December 2003, aged 24.
He will be missed.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://bugs.perl.org.  There may also be
information at http://www.perl.org, the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.  You can browse and search
the Perl 5 bugs at http://bugs.perl.org/

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for exhaustive details on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
