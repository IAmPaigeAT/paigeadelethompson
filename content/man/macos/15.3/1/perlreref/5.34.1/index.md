+++
author = "None Specified"
operating_system_version = "15.3"
manpage_name = "perlreref"
date = "2022-02-19"
manpage_format = "troff"
description = "This is a quick reference to Perls regular expressions. For full information see perlre and perlop, as well as the *(Ls-1SEE ALSO*(Rs0 section in this document. f(CW*(C`=~*(C determines to which variable the regex is applied. In its absence, f..."
title = "perlreref(1)"
manpage_section = "1"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "bu", "4", "perlretut", "for", "a", "tutorial", "on", "regular", "expressions", "perlrequick", "rapid", "perlre", "more", "details", "perlvar", "the", "variables", "perlop", "operators", "perlfunc", "functions", "perlfaq6", "faqs", "perlrebackslash", "reference", "backslash", "sequences", "perlrecharclass", "character", "classes", "re", "module", "to", "alter", "behaviour", "and", "aid", "debugging", "l", "r", "in", "perldebug", "perluniintro", "perlunicode", "charnames", "perllocale", "regexes", "internationalisation", "fimastering", "by", "jeffrey", "friedl", "http", "oreilly", "com", "catalog", "9780596528126", "thorough", "grounding", "topic", "thanks", "david", "p", "c", "wollmann", "richard", "soderberg", "sean", "m", "burke", "tom", "christiansen", "jim", "cromie", "goff", "useful", "advice"]
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLREREF 1"
PERLREREF 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlreref - Perl Regular Expressions Reference

## DESCRIPTION

Header "DESCRIPTION"
This is a quick reference to Perl's regular expressions.
For full information see perlre and perlop, as well
as the \*(L"\s-1SEE ALSO\*(R"\s0 section in this document.

### \s-1OPERATORS\s0

Subsection "OPERATORS"
\f(CW\*(C`=~\*(C' determines to which variable the regex is applied.
In its absence, \f(CW$_ is used.

.Vb 1
    $var =~ /foo/;
.Ve

\f(CW\*(C`!~\*(C' determines to which variable the regex is applied,
and negates the result of the match; it returns
false if the match succeeds, and true if it fails.

.Vb 1
    $var !~ /foo/;
.Ve

\f(CW\*(C`m/pattern/msixpogcdualn\*(C' searches a string for a pattern match,
applying the given options.

.Vb 10
    m  Multiline mode - ^ and $ match internal lines
    s  match as a Single line - . matches \\n
    i  case-Insensitive
    x  eXtended legibility - free whitespace and comments
    p  Preserve a copy of the matched string -
       $\{^PREMATCH\}, $\{^MATCH\}, $\{^POSTMATCH\} will be defined.
    o  compile pattern Once
    g  Global - all occurrences
    c  don\*(Aqt reset pos on failed matches when using /g
    a  restrict \\d, \\s, \\w and [:posix:] to match ASCII only
    aa (two a\*(Aqs) also /i matches exclude ASCII/non-ASCII
    l  match according to current locale
    u  match according to Unicode rules
    d  match according to native rules unless something indicates
       Unicode
    n  Non-capture mode. Don\*(Aqt let () fill in $1, $2, etc...
.Ve

If 'pattern' is an empty string, the last *successfully* matched
regex is used. Delimiters other than '/' may be used for both this
operator and the following ones. The leading \f(CW\*(C`m\*(C' can be omitted
if the delimiter is '/'.

\f(CW\*(C`qr/pattern/msixpodualn\*(C' lets you store a regex in a variable,
or pass one around. Modifiers as for \f(CW\*(C`m//\*(C', and are stored
within the regex.

\f(CW\*(C`s/pattern/replacement/msixpogcedual\*(C' substitutes matches of
'pattern' with 'replacement'. Modifiers as for \f(CW\*(C`m//\*(C',
with two additions:

.Vb 2
    e  Evaluate \*(Aqreplacement\*(Aq as an expression
    r  Return substitution and leave the original string untouched.
.Ve

'e' may be specified multiple times. 'replacement' is interpreted
as a double quoted string unless a single-quote (\f(CW\*(C`\*(Aq\*(C') is the delimiter.

\f(CW\*(C`m?pattern?\*(C' is like \f(CW\*(C`m/pattern/\*(C' but matches only once. No alternate
delimiters can be used.  Must be reset with **reset()**.

### \s-1SYNTAX\s0

Subsection "SYNTAX"
.Vb 10
 \\       Escapes the character immediately following it
 .       Matches any single character except a newline (unless /s is
           used)
 ^       Matches at the beginning of the string (or line, if /m is used)
 $       Matches at the end of the string (or line, if /m is used)
 *       Matches the preceding element 0 or more times
 +       Matches the preceding element 1 or more times
 ?       Matches the preceding element 0 or 1 times
 \{...\}   Specifies a range of occurrences for the element preceding it
 [...]   Matches any one of the characters contained within the brackets
 (...)   Groups subexpressions for capturing to $1, $2...
 (?:...) Groups subexpressions without capturing (cluster)
 |       Matches either the subexpression preceding or following it
 \\g1 or \\g\{1\}, \\g2 ...    Matches the text from the Nth group
 \\1, \\2, \\3 ...           Matches the text from the Nth group
 \\g-1 or \\g\{-1\}, \\g-2 ... Matches the text from the Nth previous group
 \\g\{name\}     Named backreference
 \\k<name>     Named backreference
 \\k\*(Aqname\*(Aq     Named backreference
 (?P=name)    Named backreference (python syntax)
.Ve

### \s-1ESCAPE SEQUENCES\s0

Subsection "ESCAPE SEQUENCES"
These work as in normal strings.

.Vb 10
   \\a       Alarm (beep)
   \\e       Escape
   \\f       Formfeed
   \\n       Newline
   \\r       Carriage return
   \\t       Tab
   \\037     Char whose ordinal is the 3 octal digits, max \\777
   \\o\{2307\} Char whose ordinal is the octal number, unrestricted
   \\x7f     Char whose ordinal is the 2 hex digits, max \\xFF
   \\x\{263a\} Char whose ordinal is the hex number, unrestricted
   \\cx      Control-x
   \\N\{name\} A named Unicode character or character sequence
   \\N\{U+263D\} A Unicode character by hex ordinal

   \\l  Lowercase next character
   \\u  Titlecase next character
   \\L  Lowercase until \\E
   \\U  Uppercase until \\E
   \\F  Foldcase until \\E
   \\Q  Disable pattern metacharacters until \\E
   \\E  End modification
.Ve

For Titlecase, see \*(L"Titlecase\*(R".

This one works differently from normal strings:

.Vb 1
   \\b  An assertion, not backspace, except in a character class
.Ve

### \s-1CHARACTER CLASSES\s0

Subsection "CHARACTER CLASSES"
.Vb 4
   [amy]    Match \*(Aqa\*(Aq, \*(Aqm\*(Aq or \*(Aqy\*(Aq
   [f-j]    Dash specifies "range"
   [f-j-]   Dash escaped or at start or end means \*(Aqdash\*(Aq
   [^f-j]   Caret indicates "match any character _except_ these"
.Ve

The following sequences (except \f(CW\*(C`\\N\*(C') work within or without a character class.
The first six are locale aware, all are Unicode aware. See perllocale
and perlunicode for details.

.Vb 10
   \\d      A digit
   \\D      A nondigit
   \\w      A word character
   \\W      A non-word character
   \\s      A whitespace character
   \\S      A non-whitespace character
   \\h      A horizontal whitespace
   \\H      A non horizontal whitespace
   \\N      A non newline (when not followed by \*(Aq\{NAME\}\*(Aq;;
           not valid in a character class; equivalent to [^\\n]; it\*(Aqs
           like \*(Aq.\*(Aq without /s modifier)
   \\v      A vertical whitespace
   \\V      A non vertical whitespace
   \\R      A generic newline           (?>\\v|\\x0D\\x0A)

   \\pP     Match P-named (Unicode) property
   \\p\{...\} Match Unicode property with name longer than 1 character
   \\PP     Match non-P
   \\P\{...\} Match lack of Unicode property with name longer than 1 char
   \\X      Match Unicode extended grapheme cluster
.Ve

\s-1POSIX\s0 character classes and their Unicode and Perl equivalents:

.Vb 3
            ASCII-         Full-
   POSIX    range          range    backslash
 [[:...:]]  \\p\{...\}        \\p\{...\}   sequence    Description

 -----------------------------------------------------------------------
 alnum   PosixAlnum       XPosixAlnum            \*(Aqalpha\*(Aq plus \*(Aqdigit\*(Aq
 alpha   PosixAlpha       XPosixAlpha            Alphabetic characters
 ascii   ASCII                                   Any ASCII character
 blank   PosixBlank       XPosixBlank   \\h       Horizontal whitespace;
                                                   full-range also
                                                   written as
                                                   \\p\{HorizSpace\} (GNU
                                                   extension)
 cntrl   PosixCntrl       XPosixCntrl            Control characters
 digit   PosixDigit       XPosixDigit   \\d       Decimal digits
 graph   PosixGraph       XPosixGraph            \*(Aqalnum\*(Aq plus \*(Aqpunct\*(Aq
 lower   PosixLower       XPosixLower            Lowercase characters
 print   PosixPrint       XPosixPrint            \*(Aqgraph\*(Aq plus \*(Aqspace\*(Aq,
                                                   but not any Controls
 punct   PosixPunct       XPosixPunct            Punctuation and Symbols
                                                   in ASCII-range; just
                                                   punct outside it
 space   PosixSpace       XPosixSpace   \\s       Whitespace
 upper   PosixUpper       XPosixUpper            Uppercase characters
 word    PosixWord        XPosixWord    \\w       \*(Aqalnum\*(Aq + Unicode marks
                                                    + connectors, like
                                                    \*(Aq_\*(Aq (Perl extension)
 xdigit  ASCII_Hex_Digit  XPosixDigit            Hexadecimal digit,
                                                    ASCII-range is
                                                    [0-9A-Fa-f]
.Ve

Also, various synonyms like \f(CW\*(C`\\p\{Alpha\}\*(C' for \f(CW\*(C`\\p\{XPosixAlpha\}\*(C'; all listed
in \*(L"Properties accessible through \\p\{\} and \\P\{\}\*(R" in perluniprops

Within a character class:

.Vb 3
    POSIX      traditional   Unicode
  [:digit:]       \\d        \\p\{Digit\}
  [:^digit:]      \\D        \\P\{Digit\}
.Ve

### \s-1ANCHORS\s0

Subsection "ANCHORS"
All are zero-width assertions.

.Vb 11
   ^  Match string start (or line, if /m is used)
   $  Match string end (or line, if /m is used) or before newline
   \\b\{\} Match boundary of type specified within the braces
   \\B\{\} Match wherever \\b\{\} doesn\*(Aqt match
   \\b Match word boundary (between \\w and \\W)
   \\B Match except at word boundary (between \\w and \\w or \\W and \\W)
   \\A Match string start (regardless of /m)
   \\Z Match string end (before optional newline)
   \\z Match absolute string end
   \\G Match where previous m//g left off
   \\K Keep the stuff left of the \\K, don\*(Aqt include it in $&
.Ve

### \s-1QUANTIFIERS\s0

Subsection "QUANTIFIERS"
Quantifiers are greedy by default and match the **longest** leftmost.

.Vb 10
   Maximal Minimal Possessive Allowed range
   ------- ------- ---------- -------------
   \{n,m\}   \{n,m\}?  \{n,m\}+     Must occur at least n times
                              but no more than m times
   \{n,\}    \{n,\}?   \{n,\}+      Must occur at least n times
   \{,n\}    \{,n\}?   \{,n\}+      Must occur at most n times
   \{n\}     \{n\}?    \{n\}+       Must occur exactly n times
   *       *?      *+         0 or more times (same as \{0,\})
   +       +?      ++         1 or more times (same as \{1,\})
   ?       ??      ?+         0 or 1 time (same as \{0,1\})
.Ve

The possessive forms (new in Perl 5.10) prevent backtracking: what gets
matched by a pattern with a possessive quantifier will not be backtracked
into, even if that causes the whole match to fail.

### \s-1EXTENDED CONSTRUCTS\s0

Subsection "EXTENDED CONSTRUCTS"
.Vb 10
   (?#text)          A comment
   (?:...)           Groups subexpressions without capturing (cluster)
   (?pimsx-imsx:...) Enable/disable option (as per m// modifiers)
   (?=...)           Zero-width positive lookahead assertion
   (*pla:...)        Same, starting in 5.32; experimentally in 5.28
   (*positive_lookahead:...) Same, same versions as *pla
   (?!...)           Zero-width negative lookahead assertion
   (*nla:...)        Same, starting in 5.32; experimentally in 5.28
   (*negative_lookahead:...) Same, same versions as *nla
   (?<=...)          Zero-width positive lookbehind assertion
   (*plb:...)        Same, starting in 5.32; experimentally in 5.28
   (*positive_lookbehind:...) Same, same versions as *plb
   (?<!...)          Zero-width negative lookbehind assertion
   (*nlb:...)        Same, starting in 5.32; experimentally in 5.28
   (*negative_lookbehind:...) Same, same versions as *plb
   (?>...)           Grab what we can, prohibit backtracking
   (*atomic:...)     Same, starting in 5.32; experimentally in 5.28
   (?|...)           Branch reset
   (?<name>...)      Named capture
   (?\*(Aqname\*(Aq...)      Named capture
   (?P<name>...)     Named capture (python syntax)
   (?[...])          Extended bracketed character class
   (?\{ code \})       Embedded code, return value becomes $^R
   (??\{ code \})      Dynamic regex, return value used as regex
   (?N)              Recurse into subpattern number N
   (?-N), (?+N)      Recurse into Nth previous/next subpattern
   (?R), (?0)        Recurse at the beginning of the whole pattern
   (?&name)          Recurse into a named subpattern
   (?P>name)         Recurse into a named subpattern (python syntax)
   (?(cond)yes|no)
   (?(cond)yes)      Conditional expression, where "(cond)" can be:
                     (?=pat)   lookahead; also (*pla:pat)
                               (*positive_lookahead:pat)
                     (?!pat)   negative lookahead; also (*nla:pat)
                               (*negative_lookahead:pat)
                     (?<=pat)  lookbehind; also (*plb:pat)
                               (*lookbehind:pat)
                     (?<!pat)  negative lookbehind; also (*nlb:pat)
                               (*negative_lookbehind:pat)
                     (N)       subpattern N has matched something
                     (<name>)  named subpattern has matched something
                     (\*(Aqname\*(Aq)  named subpattern has matched something
                     (?\{code\}) code condition
                     (R)       true if recursing
                     (RN)      true if recursing into Nth subpattern
                     (R&name)  true if recursing into named subpattern
                     (DEFINE)  always false, no no-pattern allowed
.Ve

### \s-1VARIABLES\s0

Subsection "VARIABLES"
.Vb 1
   $_    Default variable for operators to use

   $\`    Everything prior to matched string
   $&    Entire matched string
   $\*(Aq    Everything after to matched string

   $\{^PREMATCH\}   Everything prior to matched string
   $\{^MATCH\}      Entire matched string
   $\{^POSTMATCH\}  Everything after to matched string
.Ve

Note to those still using Perl 5.18 or earlier:
The use of \f(CW\*(C`$\`\*(C', \f(CW$& or \f(CW\*(C`$\*(Aq\*(C' will slow down **all** regex use
within your program. Consult perlvar for \f(CW\*(C`@-\*(C'
to see equivalent expressions that won't cause slow down.
See also Devel::SawAmpersand. Starting with Perl 5.10, you
can also use the equivalent variables \f(CW\*(C`$\{^PREMATCH\}\*(C', \f(CW\*(C`$\{^MATCH\}\*(C'
and \f(CW\*(C`$\{^POSTMATCH\}\*(C', but for them to be defined, you have to
specify the \f(CW\*(C`/p\*(C' (preserve) modifier on your regular expression.
In Perl 5.20, the use of \f(CW\*(C`$\`\*(C', \f(CW$& and \f(CW\*(C`$\*(Aq\*(C' makes no speed difference.

.Vb 8
   $1, $2 ...  hold the Xth captured expr
   $+    Last parenthesized pattern match
   $^N   Holds the most recently closed capture
   $^R   Holds the result of the last (?\{...\}) expr
   @-    Offsets of starts of groups. $-[0] holds start of whole match
   @+    Offsets of ends of groups. $+[0] holds end of whole match
   %+    Named capture groups
   %-    Named capture groups, as array refs
.Ve

Captured groups are numbered according to their *opening* paren.

### \s-1FUNCTIONS\s0

Subsection "FUNCTIONS"
.Vb 5
   lc          Lowercase a string
   lcfirst     Lowercase first char of a string
   uc          Uppercase a string
   ucfirst     Titlecase first char of a string
   fc          Foldcase a string

   pos         Return or set current match position
   quotemeta   Quote metacharacters
   reset       Reset m?pattern? status
   study       Analyze string for optimizing matching

   split       Use a regex to split a string into parts
.Ve

The first five of these are like the escape sequences \f(CW\*(C`\\L\*(C', \f(CW\*(C`\\l\*(C',
\f(CW\*(C`\\U\*(C', \f(CW\*(C`\\u\*(C', and \f(CW\*(C`\\F\*(C'.  For Titlecase, see \*(L"Titlecase\*(R"; For
Foldcase, see \*(L"Foldcase\*(R".

### \s-1TERMINOLOGY\s0

Subsection "TERMINOLOGY"
*Titlecase*
Subsection "Titlecase"

Unicode concept which most often is equal to uppercase, but for
certain characters like the German \*(L"sharp s\*(R" there is a difference.

*Foldcase*
Subsection "Foldcase"

Unicode form that is useful when comparing strings regardless of case,
as certain characters have complex one-to-many case mappings. Primarily a
variant of lowercase.

## AUTHOR

Header "AUTHOR"
Iain Truskett. Updated by the Perl 5 Porters.

This document may be distributed under the same terms as Perl itself.

## SEE ALSO

Header "SEE ALSO"

- \(bu
perlretut for a tutorial on regular expressions.

- \(bu
perlrequick for a rapid tutorial.

- \(bu
perlre for more details.

- \(bu
perlvar for details on the variables.

- \(bu
perlop for details on the operators.

- \(bu
perlfunc for details on the functions.

- \(bu
perlfaq6 for FAQs on regular expressions.

- \(bu
perlrebackslash for a reference on backslash sequences.

- \(bu
perlrecharclass for a reference on character classes.

- \(bu
The re module to alter behaviour and aid
debugging.

- \(bu
\*(L"Debugging Regular Expressions\*(R" in perldebug

- \(bu
perluniintro, perlunicode, charnames and perllocale
for details on regexes and internationalisation.

- \(bu
*Mastering Regular Expressions* by Jeffrey Friedl
(<http://oreilly.com/catalog/9780596528126/>) for a thorough grounding and
reference on the topic.

## THANKS

Header "THANKS"
David P.C. Wollmann,
Richard Soderberg,
Sean M. Burke,
Tom Christiansen,
Jim Cromie,
and
Jeffrey Goff
for useful advice.
