+++
operating_system_version = "15.3"
manpage_section = "1"
description = "This document describes differences between the 5.8.8 release and the 5.8.9 release. The 5.8.9 release will be the last significant release of the 5.8.x series. Any future releases of 5.8.x will likely only be to deal with security issues, and pla..."
manpage_name = "perl589delta"
author = "None Specified"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "how", "to", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_format = "troff"
operating_system = "macos"
title = "perl589delta(1)"
date = "2022-02-19"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL589DELTA 1"
PERL589DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl589delta - what is new for perl v5.8.9

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.8.8 release and
the 5.8.9 release.

## Notice

Header "Notice"
The 5.8.9 release will be the last significant release of the 5.8.x
series. Any future releases of 5.8.x will likely only be to deal with
security issues, and platform build failures. Hence you should look to
migrating to 5.10.x, if you have not started already.
See \*(L"Known Problems\*(R" for more information.

## Incompatible Changes

Header "Incompatible Changes"
A particular construction in the source code of extensions written in \*(C+
may need changing. See \*(L"Changed Internals\*(R" for more details. All
extensions written in C, most written in \*(C+, and all existing compiled
extensions are unaffected. This was necessary to improve \*(C+ support.

Other than this, there are no changes intentionally incompatible with 5.8.8.
If any exist, they are bugs and reports are welcome.

## Core Enhancements

Header "Core Enhancements"

### Unicode Character Database 5.1.0.

Subsection "Unicode Character Database 5.1.0."
The copy of the Unicode Character Database included in Perl 5.8 has
been updated to 5.1.0 from 4.1.0. See
<http://www.unicode.org/versions/Unicode5.1.0/#NotableChanges> for the
notable changes.

### stat and -X on directory handles

Subsection "stat and -X on directory handles"
It is now possible to call \f(CW\*(C`stat\*(C' and the \f(CW\*(C`-X\*(C' filestat operators on
directory handles. As both directory and file handles are barewords, there
can be ambiguities over which was intended. In these situations the file
handle semantics are preferred. Both also treat \f(CW*FILE\{IO\} filehandles
like \f(CW*FILE filehandles.
.ie n .SS "Source filters in @INC"
.el .SS "Source filters in \f(CW@INC"
Subsection "Source filters in @INC"
It's possible to enhance the mechanism of subroutine hooks in \f(CW@INC by
adding a source filter on top of the filehandle opened and returned by the
hook. This feature was planned a long time ago, but wasn't quite working
until now. See \*(L"require\*(R" in perlfunc for details. (Nicholas Clark)

### Exceptions in constant folding

Subsection "Exceptions in constant folding"
The constant folding routine is now wrapped in an exception handler, and
if folding throws an exception (such as attempting to evaluate 0/0), perl
now retains the current optree, rather than aborting the whole program.
Without this change, programs would not compile if they had expressions that
happened to generate exceptions, even though those expressions were in code
that could never be reached at runtime. (Nicholas Clark, Dave Mitchell)
.ie n .SS """no VERSION"""
.el .SS "\f(CWno VERSION"
Subsection "no VERSION"
You can now use \f(CW\*(C`no\*(C' followed by a version number to specify that you
want to use a version of perl older than the specified one.

### Improved internal \s-1UTF-8\s0 caching code

Subsection "Improved internal UTF-8 caching code"
The code that caches calculated \s-1UTF-8\s0 byte offsets for character offsets for
a string has been re-written. Several bugs have been located and eliminated,
and the code now makes better use of the information it has, so should be
faster. In particular, it doesn't scan to the end of a string before
calculating an offset within the string, which should speed up some operations
on long strings. It is now possible to disable the caching code at run time,
to verify that it is not the cause of suspected problems.

### Runtime relocatable installations

Subsection "Runtime relocatable installations"
There is now *Configure* support for creating a perl tree that is relocatable
at run time. see \*(L"Relocatable installations\*(R".

### New internal variables

Subsection "New internal variables"
.ie n .IP """$\{^CHILD_ERROR_NATIVE\}""" 4
.el .IP "\f(CW$\{^CHILD_ERROR_NATIVE\}" 4
Item "$\{^CHILD_ERROR_NATIVE\}"
This variable gives the native status returned by the last pipe close,
backtick command, successful call to \f(CW\*(C`wait\*(C' or \f(CW\*(C`waitpid\*(C', or from the
\f(CW\*(C`system\*(C' operator. See perlvar for details. (Contributed by Gisle Aas.)
.ie n .IP """$\{^UTF8CACHE\}""" 4
.el .IP "\f(CW$\{^UTF8CACHE\}" 4
Item "$\{^UTF8CACHE\}"
This variable controls the state of the internal \s-1UTF-8\s0 offset caching code.
1 for on (the default), 0 for off, -1 to debug the caching code by checking
all its results against linear scans, and panicking on any discrepancy.
.ie n .SS """readpipe"" is now overridable"
.el .SS "\f(CWreadpipe is now overridable"
Subsection "readpipe is now overridable"
The built-in function \f(CW\*(C`readpipe\*(C' is now overridable. Overriding it permits
also to override its operator counterpart, \f(CW\*(C`qx//\*(C' (also known as \f(CW\*(C`\`\`\*(C').

### simple exception handling macros

Subsection "simple exception handling macros"
Perl 5.8.9 (and 5.10.0 onwards) now provides a couple of macros to do very
basic exception handling in \s-1XS\s0 modules. You can use these macros if you call
code that may \f(CW\*(C`croak\*(C', but you need to do some cleanup before giving control
back to Perl. See \*(L"Exception Handling\*(R" in perlguts for more details.

### -D option enhancements

Subsection "-D option enhancements"

- \(bu
\f(CW\*(C`-Dq\*(C' suppresses the *\s-1EXECUTING...\s0* message when running under \f(CW\*(C`-D\*(C'

- \(bu
\f(CW\*(C`-Dl\*(C' logs runops loop entry and exit, and jump level popping.

- \(bu
\f(CW\*(C`-Dv\*(C' displays the process id as part of the trace output.

### XS-assisted \s-1SWASHGET\s0

Subsection "XS-assisted SWASHGET"
Some pure-perl code that the regexp engine was using to retrieve Unicode
properties and transliteration mappings has been reimplemented in \s-1XS\s0
for faster execution.
(\s-1SADAHIRO\s0 Tomoyuki)

### Constant subroutines

Subsection "Constant subroutines"
The interpreter internals now support a far more memory efficient form of
inlineable constants. Storing a reference to a constant value in a symbol
table is equivalent to a full typeglob referencing a constant subroutine,
but using about 400 bytes less memory. This proxy constant subroutine is
automatically upgraded to a real typeglob with subroutine if necessary.
The approach taken is analogous to the existing space optimisation for
subroutine stub declarations, which are stored as plain scalars in place
of the full typeglob.

However, to aid backwards compatibility of existing code, which (wrongly)
does not expect anything other than typeglobs in symbol tables, nothing in
core uses this feature, other than the regression tests.

Stubs for prototyped subroutines have been stored in symbol tables as plain
strings, and stubs for unprototyped subroutines as the number -1, since 5.005,
so code which assumes that the core only places typeglobs in symbol tables
has been making incorrect assumptions for over 10 years.

## New Platforms

Header "New Platforms"
Compile support added for:

- \(bu
DragonFlyBSD

- \(bu
MidnightBSD

- \(bu
MirOS \s-1BSD\s0

- \(bu
\s-1RISC OS\s0

- \(bu
Cray XT4/Catamount

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules

Subsection "New Modules"

- \(bu
\f(CW\*(C`Module::Pluggable\*(C' is a simple framework to create modules that accept
pluggable sub-modules. The bundled version is 3.8

- \(bu
\f(CW\*(C`Module::CoreList\*(C' is a hash of hashes that is keyed on perl version as
indicated in \f(CW$]. The bundled version is 2.17

- \(bu
\f(CW\*(C`Win32API::File\*(C' now available in core on Microsoft Windows. The bundled
version is 0.1001_01

- \(bu
\f(CW\*(C`Devel::InnerPackage\*(C' finds all the packages defined by a single file. It is
part of the \f(CW\*(C`Module::Pluggable\*(C' distribution. The bundled version is 0.3

### Updated Modules

Subsection "Updated Modules"

- \(bu
\f(CW\*(C`attributes\*(C' upgraded to version 0.09

- \(bu
\f(CW\*(C`AutoLoader\*(C' upgraded to version 5.67

- \(bu
\f(CW\*(C`AutoSplit\*(C' upgraded to 1.06

- \(bu
\f(CW\*(C`autouse\*(C' upgraded to version 1.06

- \(bu
\f(CW\*(C`B\*(C' upgraded from 1.09_01 to 1.19

> 
- \(bu
provides new pad related abstraction macros \f(CW\*(C`B::NV::COP_SEQ_RANGE_LOW\*(C',
\f(CW\*(C`B::NV::COP_SEQ_RANGE_HIGH\*(C', \f(CW\*(C`B::NV::PARENT_PAD_INDEX\*(C',
\f(CW\*(C`B::NV::PARENT_FAKELEX_FLAGS\*(C', which hides the difference in storage in
5.10.0 and later.

- \(bu
provides \f(CW\*(C`B::sub_generation\*(C', which exposes \f(CW\*(C`PL_sub_generation\*(C'

- \(bu
provides \f(CW\*(C`B::GV::isGV_with_GP\*(C', which on pre-5.10 perls always returns true.

- \(bu
New type \f(CW\*(C`B::HE\*(C' added with methods \f(CW\*(C`VAL\*(C', \f(CW\*(C`HASH\*(C' and \f(CW\*(C`SVKEY_force\*(C'

- \(bu
The \f(CW\*(C`B::GVf_IMPORTED_CV\*(C' flag is now set correctly when a proxy
constant subroutine is imported.

- \(bu
bugs fixed in the handling of \f(CW\*(C`PMOP\*(C's.

- \(bu
\f(CW\*(C`B::BM::PREVIOUS\*(C' returns now \f(CW\*(C`U32\*(C', not \f(CW\*(C`U16\*(C'.
\f(CW\*(C`B::CV::START\*(C' and \f(CW\*(C`B:CV::ROOT\*(C' return now \f(CW\*(C`NULL\*(C' on an \s-1XSUB,\s0
\f(CW\*(C`B::CV::XSUB\*(C' and \f(CW\*(C`B::CV::XSUBANY\*(C' return 0 on a non-XSUB.



> 


- \(bu
\f(CW\*(C`B::C\*(C' upgraded to 1.05

- \(bu
\f(CW\*(C`B::Concise\*(C' upgraded to 0.76

> 
- \(bu
new option \f(CW\*(C`-src\*(C' causes the rendering of each statement (starting with
the nextstate \s-1OP\s0) to be preceded by the first line of source code that
generates it.

- \(bu
new option \f(CW\*(C`-stash="somepackage"\*(C', \f(CW\*(C`require\*(C's \*(L"somepackage\*(R", and then renders
each function defined in its namespace.

- \(bu
now has documentation of detailed hint symbols.



> 


- \(bu
\f(CW\*(C`B::Debug\*(C' upgraded to version 1.05

- \(bu
\f(CW\*(C`B::Deparse\*(C' upgraded to version 0.87

> 
- \(bu
properly deparse \f(CW\*(C`print readpipe $x, $y\*(C'.

- \(bu
now handles \f(CW\*(C`\*(Aq\*(Aq->()\*(C', \f(CW\*(C`::()\*(C', \f(CW\*(C`sub :: \{\}\*(C', *etc.* correctly [\s-1RT\s0 #43010].
All bugs in parsing these kinds of syntax are now fixed:
.Sp
.Vb 5
    perl -MO=Deparse -e \*(Aq"my %h = "->()\*(Aq
    perl -MO=Deparse -e \*(Aq::->()\*(Aq
    perl -MO=Deparse -e \*(Aqsub :: \{\}\*(Aq
    perl -MO=Deparse -e \*(Aqpackage a; sub a::b::c \{\}\*(Aq
    perl -MO=Deparse -e \*(Aqsub the::main::road \{\}\*(Aq
.Ve

- \(bu
does **not** deparse \f(CW$^H\{v_string\}, which is automatically set by the
internals.



> 


- \(bu
\f(CW\*(C`B::Lint\*(C' upgraded to version 1.11

- \(bu
\f(CW\*(C`B::Terse\*(C' upgraded to version 1.05

- \(bu
\f(CW\*(C`base\*(C' upgraded to version 2.13

> 
- \(bu
loading a module via base.pm would mask a global \f(CW$SIG\{_\|_DIE_\|_\} in that
module.

- \(bu
push all classes at once in \f(CW@ISA



> 


- \(bu
\f(CW\*(C`Benchmark\*(C' upgraded to version 1.10

- \(bu
\f(CW\*(C`bigint\*(C' upgraded to 0.23

- \(bu
\f(CW\*(C`bignum\*(C' upgraded to 0.23

- \(bu
\f(CW\*(C`bigrat\*(C' upgraded to 0.23

- \(bu
\f(CW\*(C`blib\*(C' upgraded to 0.04

- \(bu
\f(CW\*(C`Carp\*(C' upgraded to version 1.10
.Sp
The argument backtrace code now shows \f(CW\*(C`undef\*(C' as \f(CW\*(C`undef\*(C',
instead of a string *\*(L"undef\*(R"*.

- \(bu
\f(CW\*(C`CGI\*(C' upgraded to version 3.42

- \(bu
\f(CW\*(C`charnames\*(C' upgraded to 1.06

- \(bu
\f(CW\*(C`constant\*(C' upgraded to version 1.17

- \(bu
\f(CW\*(C`CPAN\*(C' upgraded to version 1.9301

- \(bu
\f(CW\*(C`Cwd\*(C' upgraded to version 3.29 with some platform specific
improvements (including for \s-1VMS\s0).

- \(bu
\f(CW\*(C`Data::Dumper\*(C' upgraded to version 2.121_17

> 
- \(bu
Fixes hash iterator current position with the pure Perl version [\s-1RT\s0 #40668]

- \(bu
Performance enhancements, which will be most evident on platforms where
repeated calls to C's \f(CW\*(C`realloc()\*(C' are slow, such as Win32.



> 


- \(bu
\f(CW\*(C`DB_File\*(C' upgraded to version 1.817

- \(bu
\f(CW\*(C`DB_Filter\*(C' upgraded to version 0.02

- \(bu
\f(CW\*(C`Devel::DProf\*(C' upgraded to version 20080331.00

- \(bu
\f(CW\*(C`Devel::Peek\*(C' upgraded to version 1.04

- \(bu
\f(CW\*(C`Devel::PPPort\*(C' upgraded to version 3.14

- \(bu
\f(CW\*(C`diagnostics\*(C' upgraded to version 1.16

- \(bu
\f(CW\*(C`Digest\*(C' upgraded to version 1.15

- \(bu
\f(CW\*(C`Digest::MD5\*(C' upgraded to version 2.37

- \(bu
\f(CW\*(C`DirHandle\*(C' upgraded to version 1.02

> 
- \(bu
now localises \f(CW$., \f(CW$@, \f(CW$!, \f(CW$^E, and \f(CW$? before closing the
directory handle to suppress leaking any side effects of warnings about it
already being closed.



> 


- \(bu
\f(CW\*(C`DynaLoader\*(C' upgraded to version 1.09
.Sp
\f(CW\*(C`DynaLoader\*(C' can now dynamically load a loadable object from a file with a
non-default file extension.

- \(bu
\f(CW\*(C`Encode\*(C' upgraded to version 2.26
.Sp
\f(CW\*(C`Encode::Alias\*(C' includes a fix for encoding \*(L"646\*(R" on Solaris (better known as
\s-1ASCII\s0).

- \(bu
\f(CW\*(C`English\*(C' upgraded to version 1.03

- \(bu
\f(CW\*(C`Errno\*(C' upgraded to version 1.10

- \(bu
\f(CW\*(C`Exporter\*(C' upgraded to version 5.63

- \(bu
\f(CW\*(C`ExtUtils::Command\*(C' upgraded to version 1.15

- \(bu
\f(CW\*(C`ExtUtils::Constant\*(C' upgraded to version 0.21

- \(bu
\f(CW\*(C`ExtUtils::Embed\*(C' upgraded to version 1.28

- \(bu
\f(CW\*(C`ExtUtils::Install\*(C' upgraded to version 1.50_01

- \(bu
\f(CW\*(C`ExtUtils::Installed\*(C' upgraded to version 1.43

- \(bu
\f(CW\*(C`ExtUtils::MakeMaker\*(C' upgraded to version 6.48

> 
- \(bu
support for \f(CW\*(C`INSTALLSITESCRIPT\*(C' and \f(CW\*(C`INSTALLVENDORSCRIPT\*(C'
configuration.



> 


- \(bu
\f(CW\*(C`ExtUtils::Manifest\*(C' upgraded to version 1.55

- \(bu
\f(CW\*(C`ExtUtils::ParseXS\*(C' upgraded to version 2.19

- \(bu
\f(CW\*(C`Fatal\*(C' upgraded to version 1.06

> 
- \(bu
allows built-ins in \f(CW\*(C`CORE::GLOBAL\*(C' to be made fatal.



> 


- \(bu
\f(CW\*(C`Fcntl\*(C' upgraded to version 1.06

- \(bu
\f(CW\*(C`fields\*(C' upgraded to version 2.12

- \(bu
\f(CW\*(C`File::Basename\*(C' upgraded to version 2.77

- \(bu
\f(CW\*(C`FileCache\*(C' upgraded to version 1.07

- \(bu
\f(CW\*(C`File::Compare\*(C' upgraded to 1.1005

- \(bu
\f(CW\*(C`File::Copy\*(C' upgraded to 2.13

> 
- \(bu
now uses 3-arg open.



> 


- \(bu
\f(CW\*(C`File::DosGlob\*(C' upgraded to 1.01

- \(bu
\f(CW\*(C`File::Find\*(C' upgraded to version 1.13

- \(bu
\f(CW\*(C`File::Glob\*(C' upgraded to version 1.06

> 
- \(bu
fixes spurious results with brackets inside braces.



> 


- \(bu
\f(CW\*(C`File::Path\*(C' upgraded to version 2.07_02

- \(bu
\f(CW\*(C`File::Spec\*(C' upgraded to version 3.29

> 
- \(bu
improved handling of bad arguments.

- \(bu
some platform specific improvements (including for \s-1VMS\s0 and Cygwin), with
an optimisation on \f(CW\*(C`abs2rel\*(C' when handling both relative arguments.



> 


- \(bu
\f(CW\*(C`File::stat\*(C' upgraded to version 1.01

- \(bu
\f(CW\*(C`File::Temp\*(C' upgraded to version 0.20

- \(bu
\f(CW\*(C`filetest\*(C' upgraded to version 1.02

- \(bu
\f(CW\*(C`Filter::Util::Call\*(C' upgraded to version 1.07

- \(bu
\f(CW\*(C`Filter::Simple\*(C' upgraded to version 0.83

- \(bu
\f(CW\*(C`FindBin\*(C' upgraded to version 1.49

- \(bu
\f(CW\*(C`GDBM_File\*(C' upgraded to version 1.09

- \(bu
\f(CW\*(C`Getopt::Long\*(C' upgraded to version 2.37

- \(bu
\f(CW\*(C`Getopt::Std\*(C' upgraded to version 1.06

- \(bu
\f(CW\*(C`Hash::Util\*(C' upgraded to version 0.06

- \(bu
\f(CW\*(C`if\*(C' upgraded to version 0.05

- \(bu
\f(CW\*(C`IO\*(C' upgraded to version 1.23
.Sp
Reduced number of calls to \f(CW\*(C`getpeername\*(C' in \f(CW\*(C`IO::Socket\*(C'

- \(bu
\f(CW\*(C`IPC::Open\*(C' upgraded to version 1.03

- \(bu
\f(CW\*(C`IPC::Open3\*(C' upgraded to version 1.03

- \(bu
\f(CW\*(C`IPC::SysV\*(C' upgraded to version 2.00

- \(bu
\f(CW\*(C`lib\*(C' upgraded to version 0.61

> 
- \(bu
avoid warning about loading *.par* files.



> 


- \(bu
\f(CW\*(C`libnet\*(C' upgraded to version 1.22

- \(bu
\f(CW\*(C`List::Util\*(C' upgraded to 1.19

- \(bu
\f(CW\*(C`Locale::Maketext\*(C' upgraded to 1.13

- \(bu
\f(CW\*(C`Math::BigFloat\*(C' upgraded to version 1.60

- \(bu
\f(CW\*(C`Math::BigInt\*(C' upgraded to version 1.89

- \(bu
\f(CW\*(C`Math::BigRat\*(C' upgraded to version 0.22

> 
- \(bu
implements new \f(CW\*(C`as_float\*(C' method.



> 


- \(bu
\f(CW\*(C`Math::Complex\*(C' upgraded to version 1.54.

- \(bu
\f(CW\*(C`Math::Trig\*(C' upgraded to version 1.18.

- \(bu
\f(CW\*(C`NDBM_File\*(C' upgraded to version 1.07

> 
- \(bu
improve *g++* handling for systems using \s-1GDBM\s0 compatibility headers.



> 


- \(bu
\f(CW\*(C`Net::Ping\*(C' upgraded to version 2.35

- \(bu
\f(CW\*(C`NEXT\*(C' upgraded to version 0.61

> 
- \(bu
fix several bugs with \f(CW\*(C`NEXT\*(C' when working with \f(CW\*(C`AUTOLOAD\*(C', \f(CW\*(C`eval\*(C' block, and
within overloaded stringification.



> 


- \(bu
\f(CW\*(C`ODBM_File\*(C' upgraded to 1.07

- \(bu
\f(CW\*(C`open\*(C' upgraded to 1.06

- \(bu
\f(CW\*(C`ops\*(C' upgraded to 1.02

- \(bu
\f(CW\*(C`PerlIO::encoding\*(C' upgraded to version 0.11

- \(bu
\f(CW\*(C`PerlIO::scalar\*(C' upgraded to version 0.06

> 
- \(bu
[\s-1RT\s0 #40267] \f(CW\*(C`PerlIO::scalar\*(C' doesn't respect readonly-ness.



> 


- \(bu
\f(CW\*(C`PerlIO::via\*(C' upgraded to version 0.05

- \(bu
\f(CW\*(C`Pod::Html\*(C' upgraded to version 1.09

- \(bu
\f(CW\*(C`Pod::Parser\*(C' upgraded to version 1.35

- \(bu
\f(CW\*(C`Pod::Usage\*(C' upgraded to version 1.35

- \(bu
\f(CW\*(C`POSIX\*(C' upgraded to version 1.15

> 
- \(bu
\f(CW\*(C`POSIX\*(C' constants that duplicate those in \f(CW\*(C`Fcntl\*(C' are now imported from
\f(CW\*(C`Fcntl\*(C' and re-exported, rather than being duplicated by \f(CW\*(C`POSIX\*(C'

- \(bu
\f(CW\*(C`POSIX::remove\*(C' can remove empty directories.

- \(bu
\f(CW\*(C`POSIX::setlocale\*(C' safer to call multiple times.

- \(bu
\f(CW\*(C`POSIX::SigRt\*(C' added, which provides access to \s-1POSIX\s0 realtime signal
functionality on systems that support it.



> 


- \(bu
\f(CW\*(C`re\*(C' upgraded to version 0.06_01

- \(bu
\f(CW\*(C`Safe\*(C' upgraded to version 2.16

- \(bu
\f(CW\*(C`Scalar::Util\*(C' upgraded to 1.19

- \(bu
\f(CW\*(C`SDBM_File\*(C' upgraded to version 1.06

- \(bu
\f(CW\*(C`SelfLoader\*(C' upgraded to version 1.17

- \(bu
\f(CW\*(C`Shell\*(C' upgraded to version 0.72

- \(bu
\f(CW\*(C`sigtrap\*(C' upgraded to version 1.04

- \(bu
\f(CW\*(C`Socket\*(C' upgraded to version 1.81

> 
- \(bu
this fixes an optimistic use of \f(CW\*(C`gethostbyname\*(C'



> 


- \(bu
\f(CW\*(C`Storable\*(C' upgraded to 2.19

- \(bu
\f(CW\*(C`Switch\*(C' upgraded to version 2.13

- \(bu
\f(CW\*(C`Sys::Syslog\*(C' upgraded to version 0.27

- \(bu
\f(CW\*(C`Term::ANSIColor\*(C' upgraded to version 1.12

- \(bu
\f(CW\*(C`Term::Cap\*(C' upgraded to version 1.12

- \(bu
\f(CW\*(C`Term::ReadLine\*(C' upgraded to version 1.03

- \(bu
\f(CW\*(C`Test::Builder\*(C' upgraded to version 0.80

- \(bu
\f(CW\*(C`Test::Harness\*(C' upgraded version to 2.64

> 
- \(bu
this makes it able to handle newlines.



> 


- \(bu
\f(CW\*(C`Test::More\*(C' upgraded to version 0.80

- \(bu
\f(CW\*(C`Test::Simple\*(C' upgraded to version 0.80

- \(bu
\f(CW\*(C`Text::Balanced\*(C' upgraded to version 1.98

- \(bu
\f(CW\*(C`Text::ParseWords\*(C' upgraded to version 3.27

- \(bu
\f(CW\*(C`Text::Soundex\*(C' upgraded to version 3.03

- \(bu
\f(CW\*(C`Text::Tabs\*(C' upgraded to version 2007.1117

- \(bu
\f(CW\*(C`Text::Wrap\*(C' upgraded to version 2006.1117

- \(bu
\f(CW\*(C`Thread\*(C' upgraded to version 2.01

- \(bu
\f(CW\*(C`Thread::Semaphore\*(C' upgraded to version 2.09

- \(bu
\f(CW\*(C`Thread::Queue\*(C' upgraded to version 2.11

> 
- \(bu
added capability to add complex structures (e.g., hash of hashes) to queues.

- \(bu
added capability to dequeue multiple items at once.

- \(bu
added new methods to inspect and manipulate queues:  \f(CW\*(C`peek\*(C', \f(CW\*(C`insert\*(C' and
\f(CW\*(C`extract\*(C'



> 


- \(bu
\f(CW\*(C`Tie::Handle\*(C' upgraded to version 4.2

- \(bu
\f(CW\*(C`Tie::Hash\*(C' upgraded to version 1.03

- \(bu
\f(CW\*(C`Tie::Memoize\*(C' upgraded to version 1.1

> 
- \(bu
\f(CW\*(C`Tie::Memoize::EXISTS\*(C' now correctly caches its results.



> 


- \(bu
\f(CW\*(C`Tie::RefHash\*(C' upgraded to version 1.38

- \(bu
\f(CW\*(C`Tie::Scalar\*(C' upgraded to version 1.01

- \(bu
\f(CW\*(C`Tie::StdHandle\*(C' upgraded to version 4.2

- \(bu
\f(CW\*(C`Time::gmtime\*(C' upgraded to version 1.03

- \(bu
\f(CW\*(C`Time::Local\*(C' upgraded to version 1.1901

- \(bu
\f(CW\*(C`Time::HiRes\*(C' upgraded to version 1.9715 with various build improvements
(including \s-1VMS\s0) and minor platform-specific bug fixes (including
for HP-UX 11 ia64).

- \(bu
\f(CW\*(C`threads\*(C' upgraded to 1.71

> 
- \(bu
new thread state information methods: \f(CW\*(C`is_running\*(C', \f(CW\*(C`is_detached\*(C'
and \f(CW\*(C`is_joinable\*(C'.  \f(CW\*(C`list\*(C' method enhanced to return running or joinable
threads.

- \(bu
new thread signal method: \f(CW\*(C`kill\*(C'

- \(bu
added capability to specify thread stack size.

- \(bu
added capability to control thread exiting behavior.  Added a new \f(CW\*(C`exit\*(C'
method.



> 


- \(bu
\f(CW\*(C`threads::shared\*(C' upgraded to version 1.27

> 
- \(bu
smaller and faster implementation that eliminates one internal structure and
the consequent level of indirection.

- \(bu
user locks are now stored in a safer manner.

- \(bu
new function \f(CW\*(C`shared_clone\*(C' creates a copy of an object leaving
shared elements as-is and deep-cloning non-shared elements.

- \(bu
added new \f(CW\*(C`is_shared\*(C' method.



> 


- \(bu
\f(CW\*(C`Unicode::Normalize\*(C' upgraded to version 1.02

- \(bu
\f(CW\*(C`Unicode::UCD\*(C' upgraded to version 0.25

- \(bu
\f(CW\*(C`warnings\*(C' upgraded to version 1.05_01

- \(bu
\f(CW\*(C`Win32\*(C' upgraded to version 0.38

> 
- \(bu
added new function \f(CW\*(C`GetCurrentProcessId\*(C' which returns the regular Windows
process identifier of the current process, even when called from within a fork.



> 


- \(bu
\f(CW\*(C`XSLoader\*(C' upgraded to version 0.10

- \(bu
\f(CW\*(C`XS::APItest\*(C' and \f(CW\*(C`XS::Typemap\*(C' are for internal use only and hence
no longer installed. Many more tests have been added to \f(CW\*(C`XS::APItest\*(C'.

## Utility Changes

Header "Utility Changes"

### debugger upgraded to version 1.31

Subsection "debugger upgraded to version 1.31"

- \(bu
Andreas Ko\*:nig contributed two functions to save and load the debugger
history.

- \(bu
\f(CW\*(C`NEXT::AUTOLOAD\*(C' no longer emits warnings under the debugger.

- \(bu
The debugger should now correctly find tty the device on \s-1OS X 10.5\s0 and \s-1VMS\s0
when the program \f(CW\*(C`fork\*(C's.

- \(bu
\s-1LVALUE\s0 subs now work inside the debugger.

### \fIperlthanks

Subsection "perlthanks"
Perl 5.8.9 adds a new utility *perlthanks*, which is a variant of *perlbug*,
but for sending non-bug-reports to the authors and maintainers of Perl.
Getting nothing but bug reports can become a bit demoralising - we'll see if
this changes things.

### \fIperlbug

Subsection "perlbug"
*perlbug* now checks if you're reporting about a non-core module and suggests
you report it to the \s-1CPAN\s0 author instead.

### \fIh2xs

Subsection "h2xs"

- \(bu
won't define an empty string as a constant [\s-1RT\s0 #25366]

- \(bu
has examples for \f(CW\*(C`h2xs -X\*(C'

### \fIh2ph

Subsection "h2ph"

- \(bu
now attempts to deal sensibly with the difference in path implications
between \f(CW"" and \f(CW\*(C`<>\*(C' quoting in \f(CW\*(C`#include\*(C' statements.

- \(bu
now generates correct code for \f(CW\*(C`#if defined A || defined B\*(C'
[\s-1RT\s0 #39130]

## New Documentation

Header "New Documentation"
As usual, the documentation received its share of corrections, clarifications
and other nitfixes. More \f(CW\*(C`\*(C' tags were added for indexing.
Xref "..."

perlunitut is a tutorial written by Juerd Waalboer on Unicode-related
terminology and how to correctly handle Unicode in Perl scripts.

perlunicode is updated in section user defined properties.

perluniintro has been updated in the example of detecting data that is not
valid in particular encoding.

perlcommunity provides an overview of the Perl Community along with further
resources.

\s-1CORE\s0 documents the pseudo-namespace for Perl's core routines.

## Changes to Existing Documentation

Header "Changes to Existing Documentation"
perlglossary adds *deprecated modules and features* and *to be dropped modules*.

perlhack has been updated and added resources on smoke testing.

The Perl FAQs (*perlfaq1*..*perlfaq9*) have been updated.

perlcheat is updated with better details on \f(CW\*(C`\\w\*(C', \f(CW\*(C`\\d\*(C', and \f(CW\*(C`\\s\*(C'.

perldebug is updated with information on how to call the debugger.

perldiag documentation updated with *subroutine with an ampersand* on the
argument to \f(CW\*(C`exists\*(C' and \f(CW\*(C`delete\*(C' and also several terminology updates on
warnings.

perlfork documents the limitation of \f(CW\*(C`exec\*(C' inside pseudo-processes.

perlfunc:

- \(bu
Documentation is fixed in section \f(CW\*(C`caller\*(C' and \f(CW\*(C`pop\*(C'.

- \(bu
Function \f(CW\*(C`alarm\*(C' now mentions \f(CW\*(C`Time::HiRes::ualarm\*(C' in preference
to \f(CW\*(C`select\*(C'.

- \(bu
Regarding precedence in \f(CW\*(C`-X\*(C', filetest operators are the same as unary
operators, but not regarding parsing and parentheses (spotted by Eirik Berg
Hanssen).

- \(bu
\f(CW\*(C`reverse\*(C' function documentation received scalar context examples.

perllocale documentation is adjusted for number localization and
\f(CW\*(C`POSIX::setlocale\*(C' to fix Debian bug #379463.

perlmodlib is updated with \f(CW\*(C`CPAN::API::HOWTO\*(C' and
\f(CW\*(C`Sys::Syslog::win32::Win32\*(C'

perlre documentation updated to reflect the differences between
\f(CW\*(C`[[:xxxxx:]]\*(C' and \f(CW\*(C`\\p\{IsXxxxx\}\*(C' matches. Also added section on \f(CW\*(C`/g\*(C' and
\f(CW\*(C`/c\*(C' modifiers.

perlreguts describe the internals of the regular expressions engine. It has
been contributed by Yves Orton.

perlrebackslash describes all perl regular expression backslash and escape
sequences.

perlrecharclass describes the syntax and use of character classes in
Perl Regular Expressions.

perlrun is updated to clarify on the hash seed *\s-1PERL_HASH_SEED\s0*. Also more
information in options \f(CW\*(C`-x\*(C' and \f(CW\*(C`-u\*(C'.

perlsub example is updated to use a lexical variable for \f(CW\*(C`opendir\*(C' syntax.

perlvar fixes confusion about real \s-1GID\s0 \f(CW$( and effective \s-1GID\s0 \f(CW$).

Perl thread tutorial example is fixed in section
\*(L"Queues: Passing Data Around\*(R" in perlthrtut and perlthrtut.

perlhack documentation extensively improved by Jarkko Hietaniemi and others.

perltoot provides information on modifying \f(CW@UNIVERSAL::ISA.

perlport documentation extended to include different \f(CW\*(C`kill(-9, ...)\*(C'
semantics on Windows. It also clearly states \f(CW\*(C`dump\*(C' is not supported on Win32
and cygwin.

*\s-1INSTALL\s0* has been updated and modernised.

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
The default since perl 5.000 has been for perl to create an empty scalar
with every new typeglob. The increased use of lexical variables means that
most are now unused. Thanks to Nicholas Clark's efforts, Perl can now be
compiled with \f(CW\*(C`-DPERL_DONT_CREATE_GVSV\*(C' to avoid creating these empty scalars.
This will significantly decrease the number of scalars allocated for all
configurations, and the number of scalars that need to be copied for ithread
creation. Whilst this option is binary compatible with existing perl
installations, it does change a long-standing assumption about the
internals, hence it is not enabled by default, as some third party code may
rely on the old behaviour.
.Sp
We would recommend testing with this configuration on new deployments of
perl, particularly for multi-threaded servers, to see whether all third party
code is compatible with it, as this configuration may give useful performance
improvements. For existing installations we would not recommend changing to
this configuration unless thorough testing is performed before deployment.

- \(bu
\f(CW\*(C`diagnostics\*(C' no longer uses \f(CW$&, which results in large speedups
for regexp matching in all code using it.

- \(bu
Regular expressions classes of a single character are now treated the same as
if the character had been used as a literal, meaning that code that uses
char-classes as an escaping mechanism will see a speedup. (Yves Orton)

- \(bu
Creating anonymous array and hash references (ie. \f(CW\*(C`[]\*(C' and \f(CW\*(C`\{\}\*(C') now incurs
no more overhead than creating an anonymous list or hash. Nicholas Clark
provided changes with a saving of two ops and one stack push, which was measured
as a slightly better than 5% improvement for these operations.

- \(bu
Many calls to \f(CW\*(C`strlen()\*(C' have been eliminated, either because the length was
already known, or by adopting or enhancing APIs that pass lengths. This has
been aided by the adoption of a \f(CW\*(C`my_sprintf()\*(C' wrapper, which returns the
correct C89 value - the length of the formatted string. Previously we could
not rely on the return value of \f(CW\*(C`sprintf()\*(C', because on some ancient but
extant platforms it still returns \f(CW\*(C`char *\*(C'.

- \(bu
\f(CW\*(C`index\*(C' is now faster if the search string is stored in \s-1UTF-8\s0 but only contains
characters in the Latin-1 range.

- \(bu
The Unicode swatch cache inside the regexp engine is now used. (the lookup had
a key mismatch, present since the initial implementation). [\s-1RT\s0 #42839]

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"

### Relocatable installations

Subsection "Relocatable installations"
There is now *Configure* support for creating a relocatable perl tree. If
you *Configure* with \f(CW\*(C`-Duserelocatableinc\*(C', then the paths in \f(CW@INC (and
everything else in \f(CW%Config) can be optionally located via the path of the
*perl* executable.

At start time, if any paths in \f(CW@INC or \f(CW\*(C`Config\*(C' that *Configure* marked
as relocatable (by starting them with \f(CW".../"), then they are prefixed the
directory of \f(CW$^X. This allows the relocation can be configured on a
per-directory basis, although the default with \f(CW\*(C`-Duserelocatableinc\*(C' is that
everything is relocated. The initial install is done to the original configured
prefix.

### Configuration improvements

Subsection "Configuration improvements"
*Configure* is now better at removing temporary files. Tom Callaway
(from RedHat) also contributed patches that complete the set of flags
passed to the compiler and the linker, in particular that \f(CW\*(C`-fPIC\*(C' is now
enabled on Linux. It will also croak when your */dev/null* isn't a device.

A new configuration variable \f(CW\*(C`d_pseudofork\*(C' has been to *Configure*, and is
available as  \f(CW$Config\{d_pseudofork\} in the \f(CW\*(C`Config\*(C' module. This
distinguishes real \f(CW\*(C`fork\*(C' support from the pseudofork emulation used on
Windows platforms.

*Config.pod* and *config.sh* are now placed correctly for cross-compilation.

\f(CW$Config\{useshrplib\} is now 'true' rather than 'yes' when using a shared perl
library.

### Compilation improvements

Subsection "Compilation improvements"
Parallel makes should work properly now, although there may still be problems
if \f(CW\*(C`make test\*(C' is instructed to run in parallel.

Many compilation warnings have been cleaned up. A very stubborn compiler
warning in \f(CW\*(C`S_emulate_eaccess()\*(C' was killed after six attempts.
*g++* support has been tuned, especially for FreeBSD.

*mkppport* has been integrated, and all *ppport.h* files in the core will now
be autogenerated at build time (and removed during cleanup).

### Installation improvements.

Subsection "Installation improvements."
*installman* now works with \f(CW\*(C`-Duserelocatableinc\*(C' and \f(CW\*(C`DESTDIR\*(C'.

*installperl* no longer installs:

- \(bu
static library files of statically linked extensions when a shared perl library
is being used. (They are not needed. See \*(L"Windows\*(R" below).

- \(bu
*\s-1SIGNATURE\s0* and *PAUSE*.pub* (\s-1CPAN\s0 files)

- \(bu
*\s-1NOTES\s0* and *\s-1PATCHING\s0* (ExtUtils files)

- \(bu
*perlld* and *ld2* (Cygwin files)

### Platform Specific Changes

Subsection "Platform Specific Changes"
There are improved hints for \s-1AIX,\s0 Cygwin, \s-1DEC/OSF,\s0 FreeBSD, \s-1HP/UX,\s0 Irix 6
Linux, MachTen, NetBSD, \s-1OS/390, QNX, SCO,\s0 Solaris, SunOS, System V Release 5.x
(UnixWare 7, OpenUNIX 8), Ultrix, \s-1UMIPS,\s0 uts and \s-1VOS.\s0

*FreeBSD*
Subsection "FreeBSD"

- \(bu
Drop \f(CW\*(C`-std=c89\*(C' and \f(CW\*(C`-ansi\*(C' if using \f(CW\*(C`long long\*(C' as the main integral type,
else in FreeBSD 6.2 (and perhaps other releases), system headers do not
declare some functions required by perl.

*Solaris*
Subsection "Solaris"

- \(bu
Starting with Solaris 10, we do not want versioned shared libraries, because
those often indicate a private use only library. These problems could often
be triggered when SUNWbdb (Berkeley \s-1DB\s0) was installed. Hence if Solaris 10
is detected set \f(CW\*(C`ignore_versioned_solibs=y\*(C'.

*\s-1VMS\s0*
Subsection "VMS"

- \(bu
Allow \s-1IEEE\s0 math to be deselected on OpenVMS I64 (but it remains the default).

- \(bu
Record \s-1IEEE\s0 usage in \f(CW\*(C`config.h\*(C'

- \(bu
Help older \s-1VMS\s0 compilers by using \f(CW\*(C`ccflags\*(C' when building \f(CW\*(C`munchconfig.exe\*(C'.

- \(bu
Don't try to build old \f(CW\*(C`Thread\*(C' extension on \s-1VMS\s0 when \f(CW\*(C`-Duseithreads\*(C' has
been chosen.

- \(bu
Passing a raw string of \*(L"NaN\*(R" to *nawk* causes a core dump - so the string
has been changed to \*(L"*NaN*\*(R"

- \(bu
*t/op/stat.t* tests will now test hard links on \s-1VMS\s0 if they are supported.

*Windows*
Subsection "Windows"

- \(bu
When using a shared perl library *installperl* no longer installs static
library files, import library files and export library files (of statically
linked extensions) and empty bootstrap files (of dynamically linked
extensions). This fixes a problem building PAR-Packer on Win32 with a debug
build of perl.

- \(bu
Various improvements to the win32 build process, including support for Visual
\*(C+ 2005 Express Edition (aka Visual \*(C+ 8.x).

- \(bu
*perl.exe* will now have an icon if built with MinGW or Borland.

- \(bu
Improvements to the perl-static.exe build process.

- \(bu
Add Win32 makefile option to link all extensions statically.

- \(bu
The *WinCE* directory has been merged into the *Win32* directory.

- \(bu
\f(CW\*(C`setlocale\*(C' tests have been re-enabled for Windows \s-1XP\s0 onwards.

## Selected Bug Fixes

Header "Selected Bug Fixes"

### Unicode

Subsection "Unicode"
Many many bugs related to the internal Unicode implementation (\s-1UTF-8\s0) have
been fixed. In particular, long standing bugs related to returning Unicode
via \f(CW\*(C`tie\*(C', overloading or \f(CW$@ are now gone, some of which were never
reported.

\f(CW\*(C`unpack\*(C' will internally convert the string back from \s-1UTF-8\s0 on numeric types.
This is a compromise between the full consistency now in 5.10, and the current
behaviour, which is often used as a \*(L"feature\*(R" on string types.

Using \f(CW\*(C`:crlf\*(C' and \f(CW\*(C`UTF-16\*(C' \s-1IO\s0 layers together will now work.

Fixed problems with \f(CW\*(C`split\*(C', Unicode \f(CW\*(C`/\\s+/\*(C' and \f(CW\*(C`/ \\0/\*(C'.

Fixed bug \s-1RT\s0 #40641 - encoding of Unicode characters in regular expressions.

Fixed a bug where using certain patterns in a regexp led to a panic.
[\s-1RT\s0 #45337]

Perl no longer segfaults (due to infinite internal recursion) if the locale's
character is not \s-1UTF-8\s0 [\s-1RT\s0 #41442]:

.Vb 2
    use open \*(Aq:locale\*(Aq;
    print STDERR "\\x\{201e\}"; # &bdquo;
.Ve

### PerlIO

Subsection "PerlIO"
Inconsistencies have been fixed in the reference counting PerlIO uses to keep
track of Unix file descriptors, and the \s-1API\s0 used by \s-1XS\s0 code to manage getting
and releasing \f(CW\*(C`FILE *\*(C's

### Magic

Subsection "Magic"
Several bugs have been fixed in Magic, the internal system used to implement
features such as \f(CW\*(C`tie\*(C', tainting and threads sharing.

\f(CW\*(C`undef @array\*(C' on a tied array now correctly calls the \f(CW\*(C`CLEAR\*(C' method.

Some of the bitwise ops were not checking whether their arguments were magical
before using them. [\s-1RT\s0 #24816]

Magic is no longer invoked twice by the expression \f(CW\*(C`\$x\*(C'

A bug with assigning large numbers and tainting has been resolved.
[\s-1RT\s0 #40708]

A new entry has been added to the \s-1MAGIC\s0 vtable - \f(CW\*(C`svt_local\*(C'. This is used
when copying magic to the new value during \f(CW\*(C`local\*(C', allowing certain problems
with localising shared variables to be resolved.

For the implementation details, see \*(L"Magic Virtual Tables\*(R" in perlguts.

### Reblessing overloaded objects now works

Subsection "Reblessing overloaded objects now works"
Internally, perl object-ness is on the referent, not the reference, even
though methods can only be called via a reference. However, the original
implementation of overloading stored flags related to overloading on the
reference, relying on the flags being copied when the reference was copied,
or set at the creation of a new reference. This manifests in a bug - if you
rebless an object from a class that has overloading, into one that does not,
then any other existing references think that they (still) point to an
overloaded object, choose these C code paths, and then throw errors.
Analogously, blessing into an overloaded class when other references exist will
result in them not using overloading.

The implementation has been fixed for 5.10, but this fix changes the semantics
of flag bits, so is not binary compatible, so can't be applied to 5.8.9.
However, 5.8.9 has a work-around that implements the same bug fix. If the
referent has multiple references, then all the other references are located and
corrected. A full search is avoided whenever possible by scanning lexicals
outwards from the current subroutine, and the argument stack.

A certain well known Linux vendor applied incomplete versions of this bug fix
to their */usr/bin/perl* and then prematurely closed bug reports about
performance issues without consulting back upstream. This not being enough,
they then proceeded to ignore the necessary fixes to these unreleased changes
for 11 months, until massive pressure was applied by their long-suffering
paying customers, catalysed by the failings being featured on a prominent blog
and Slashdot.
.ie n .SS """strict"" now propagates correctly into string evals"
.el .SS "\f(CWstrict now propagates correctly into string evals"
Subsection "strict now propagates correctly into string evals"
Under 5.8.8 and earlier:

.Vb 3
    $ perl5.8.8 -e \*(Aquse strict; eval "use foo bar" or die $@\*(Aq
    Can\*(Aqt locate foo.pm in @INC (@INC contains: ... .) at (eval 1) line 2.
    BEGIN failed--compilation aborted at (eval 1) line 2.
.Ve

Under 5.8.9 and later:

.Vb 2
    $ perl5.8.9 -e \*(Aquse strict; eval "use foo bar" or die $@\*(Aq
    Bareword "bar" not allowed while "strict subs" in use at (eval 1) line 1.
.Ve

This may cause problems with programs that parse the error message and rely
on the buggy behaviour.

### Other fixes

Subsection "Other fixes"

- \(bu
The tokenizer no longer treats \f(CW\*(C`=cute\*(C' (and other words beginning
with \f(CW\*(C`=cut\*(C') as a synonym for \f(CW\*(C`=cut\*(C'.

- \(bu
Calling \f(CW\*(C`CORE::require\*(C'
.Sp
\f(CW\*(C`CORE::require\*(C' and \f(CW\*(C`CORE::do\*(C' were always parsed as \f(CW\*(C`require\*(C' and \f(CW\*(C`do\*(C'
when they were overridden. This is now fixed.

- \(bu
Stopped memory leak on long */etc/groups* entries.

- \(bu
\f(CW\*(C`while (my $x ...) \{ ...; redo \}\*(C' shouldn't \f(CW\*(C`undef $x\*(C'.
.Sp
In the presence of \f(CW\*(C`my\*(C' in the conditional of a \f(CW\*(C`while()\*(C', \f(CW\*(C`until()\*(C',
or \f(CW\*(C`for(;;)\*(C' loop, we now add an extra scope to the body so that \f(CW\*(C`redo\*(C'
doesn't \f(CW\*(C`undef\*(C' the lexical.

- \(bu
The \f(CW\*(C`encoding\*(C' pragma now correctly ignores anything following an \f(CW\*(C`@\*(C'
character in the \f(CW\*(C`LC_ALL\*(C' and \f(CW\*(C`LANG\*(C' environment variables. [\s-1RT\s0 # 49646]

- \(bu
A segfault observed with some *gcc* 3.3 optimisations is resolved.

- \(bu
A possible segfault when \f(CW\*(C`unpack\*(C' used in scalar context with \f(CW\*(C`()\*(C' groups
is resolved. [\s-1RT\s0 #50256]

- \(bu
Resolved issue where \f(CW$! could be changed by a signal handler interrupting
a \f(CW\*(C`system\*(C' call.

- \(bu
Fixed bug \s-1RT\s0 #37886, symbolic dereferencing was allowed in the argument of
\f(CW\*(C`defined\*(C' even under the influence of \f(CW\*(C`use strict \*(Aqrefs\*(Aq\*(C'.

- \(bu
Fixed bug \s-1RT\s0 #43207, where \f(CW\*(C`lc\*(C'/\f(CW\*(C`uc\*(C' inside \f(CW\*(C`sort\*(C' affected the return
value.

- \(bu
Fixed bug \s-1RT\s0 #45607, where \f(CW\*(C`*\{"BONK"\} = \\{"BONK"\}\*(C' didn't work correctly.

- \(bu
Fixed bug \s-1RT\s0 #35878, croaking from a \s-1XSUB\s0 called via \f(CW\*(C`goto &xsub\*(C' corrupts perl
internals.

- \(bu
Fixed bug \s-1RT\s0 #32539, *DynaLoader.o* is moved into *libperl.so* to avoid the
need to statically link DynaLoader into the stub perl executable. With this
*libperl.so* provides everything needed to get a functional embedded perl
interpreter to run.

- \(bu
Fix bug \s-1RT\s0 #36267 so that assigning to a tied hash doesn't change the
underlying hash.

- \(bu
Fix bug \s-1RT\s0 #6006, regexp replaces using large replacement variables
fail some of the time, *i.e.* when substitution contains something
like \f(CW\*(C`$\{10\}\*(C' (note the bracket) instead of just \f(CW$10.

- \(bu
Fix bug \s-1RT\s0 #45053, \f(CW\*(C`Perl_newCONSTSUB()\*(C' is now thread safe.

### Platform Specific Fixes

Subsection "Platform Specific Fixes"
*Darwin / MacOS X*
Subsection "Darwin / MacOS X"

- \(bu
Various improvements to 64 bit builds.

- \(bu
Mutex protection added in \f(CW\*(C`PerlIOStdio_close()\*(C' to avoid race conditions.
Hopefully this fixes failures in the threads tests *free.t* and *blocks.t*.

- \(bu
Added forked terminal support to the debugger, with the ability to update the
window title.

*\s-1OS/2\s0*
Subsection "OS/2"

- \(bu
A build problem with specifying \f(CW\*(C`USE_MULTI\*(C' and \f(CW\*(C`USE_ITHREADS\*(C' but without
\f(CW\*(C`USE_IMP_SYS\*(C' has been fixed.

- \(bu
\f(CW\*(C`OS2::REXX\*(C' upgraded to version 1.04

*Tru64*
Subsection "Tru64"

- \(bu
Aligned floating point build policies for *cc* and *gcc*.

*RedHat Linux*
Subsection "RedHat Linux"

- \(bu
Revisited a patch from 5.6.1 for \s-1RH7.2\s0 for Intel's *icc* [\s-1RT\s0 #7916], added an
additional check for \f(CW$Config\{gccversion\}.

*Solaris/i386*
Subsection "Solaris/i386"

- \(bu
Use \f(CW\*(C`-DPTR_IS_LONG\*(C' when using 64 bit integers

*\s-1VMS\s0*
Subsection "VMS"

- \(bu
Fixed \f(CW\*(C`PerlIO::Scalar\*(C' in-memory file record-style reads.

- \(bu
pipe shutdown at process exit should now be more robust.

- \(bu
Bugs in \s-1VMS\s0 exit handling tickled by \f(CW\*(C`Test::Harness\*(C' 2.64 have been fixed.

- \(bu
Fix \f(CW\*(C`fcntl()\*(C' locking capability test in *configure.com*.

- \(bu
Replaced \f(CW\*(C`shrplib=\*(Aqdefine\*(Aq\*(C' with \f(CW\*(C`useshrplib=\*(Aqtrue\*(Aq\*(C' on \s-1VMS.\s0

*Windows*
Subsection "Windows"

- \(bu
\f(CW\*(C`File::Find\*(C' used to fail when the target directory is a bare drive letter and
\f(CW\*(C`no_chdir\*(C' is 1 (the default is 0). [\s-1RT\s0 #41555]

- \(bu
A build problem with specifying \f(CW\*(C`USE_MULTI\*(C' and \f(CW\*(C`USE_ITHREADS\*(C' but without
\f(CW\*(C`USE_IMP_SYS\*(C' has been fixed.

- \(bu
The process id is no longer truncated to 16 bits on some Windows platforms
( http://bugs.activestate.com/show_bug.cgi?id=72443 )

- \(bu
Fixed bug \s-1RT\s0 #54828 in *perlio.c* where calling \f(CW\*(C`binmode\*(C' on Win32 and Cygwin
may cause a segmentation fault.

### Smaller fixes

Subsection "Smaller fixes"

- \(bu
It is now possible to overload \f(CW\*(C`eq\*(C' when using \f(CW\*(C`nomethod\*(C'.

- \(bu
Various problems using \f(CW\*(C`overload\*(C' with 64 bit integers corrected.

- \(bu
The reference count of \f(CW\*(C`PerlIO\*(C' file descriptors is now correctly handled.

- \(bu
On \s-1VMS,\s0 escaped dots will be preserved when converted to Unix syntax.

- \(bu
\f(CW\*(C`keys %+\*(C' no longer throws an \f(CW\*(Aqambiguous\*(Aq warning.

- \(bu
Using \f(CW\*(C`#!perl -d\*(C' could trigger an assertion, which has been fixed.

- \(bu
Don't stringify tied code references in \f(CW@INC when calling \f(CW\*(C`require\*(C'.

- \(bu
Code references in \f(CW@INC report the correct file name when \f(CW\*(C`_\|_FILE_\|_\*(C' is
used.

- \(bu
Width and precision in sprintf didn't handle characters above 255 correctly.
[\s-1RT\s0 #40473]

- \(bu
List slices with indices out of range now work more consistently.
[\s-1RT\s0 #39882]

- \(bu
A change introduced with perl 5.8.1 broke the parsing of arguments of the form
\f(CW\*(C`-foo=bar\*(C' with the \f(CW\*(C`-s\*(C' on the <#!> line. This has been fixed. See
http://bugs.activestate.com/show_bug.cgi?id=43483

- \(bu
\f(CW\*(C`tr///\*(C' is now threadsafe. Previously it was storing a swash inside its \s-1OP,\s0
rather than in a pad.

- \(bu
*pod2html* labels anchors more consistently and handles nested definition
lists better.

- \(bu
\f(CW\*(C`threads\*(C' cleanup veto has been extended to include \f(CW\*(C`perl_free()\*(C' and
\f(CW\*(C`perl_destruct()\*(C'

- \(bu
On some systems, changes to \f(CW$ENV\{TZ\} would not always be
respected by the underlying calls to \f(CW\*(C`localtime_r()\*(C'.  Perl now
forces the inspection of the environment on these systems.

- \(bu
The special variable \f(CW$^R is now more consistently set when executing
regexps using the \f(CW\*(C`(?\{...\})\*(C' construct.  In particular, it will still
be set even if backreferences or optional sub-patterns \f(CW\*(C`(?:...)?\*(C' are
used.

## New or Changed Diagnostics

Header "New or Changed Diagnostics"
.ie n .SS "panic: sv_chop %s"
.el .SS "panic: sv_chop \f(CW%s"
Subsection "panic: sv_chop %s"
This new fatal error occurs when the C routine \f(CW\*(C`Perl_sv_chop()\*(C' was passed a
position that is not within the scalar's string buffer. This is caused by
buggy \s-1XS\s0 code, and at this point recovery is not possible.

### Maximal count of pending signals (%s) exceeded

Subsection "Maximal count of pending signals (%s) exceeded"
This new fatal error occurs when the perl process has to abort due to
too many pending signals, which is bound to prevent perl from being
able to handle further incoming signals safely.
.ie n .SS "panic: attempt to call %s in %s"
.el .SS "panic: attempt to call \f(CW%s in \f(CW%s"
Subsection "panic: attempt to call %s in %s"
This new fatal error occurs when the \s-1ACL\s0 version file test operator is used
where it is not available on the current platform. Earlier checks mean that
it should never be possible to get this.

### \s-1FETCHSIZE\s0 returned a negative value

Subsection "FETCHSIZE returned a negative value"
New error indicating that a tied array has claimed to have a negative
number of elements.
.ie n .SS "Can't upgrade %s (%d) to %d"
.el .SS "Can't upgrade \f(CW%s (%d) to \f(CW%d"
Subsection "Can't upgrade %s (%d) to %d"
Previously the internal error from the \s-1SV\s0 upgrade code was the less informative
*Can't upgrade that kind of scalar*. It now reports the current internal type,
and the new type requested.
.ie n .SS "%s argument is not a \s-1HASH\s0 or \s-1ARRAY\s0 element or a subroutine"
.el .SS "\f(CW%s argument is not a \s-1HASH\s0 or \s-1ARRAY\s0 element or a subroutine"
Subsection "%s argument is not a HASH or ARRAY element or a subroutine"
This error, thrown if an invalid argument is provided to \f(CW\*(C`exists\*(C' now
correctly includes \*(L"or a subroutine\*(R". [\s-1RT\s0 #38955]
.ie n .SS "Cannot make the non-overridable builtin %s fatal"
.el .SS "Cannot make the non-overridable builtin \f(CW%s fatal"
Subsection "Cannot make the non-overridable builtin %s fatal"
This error in \f(CW\*(C`Fatal\*(C' previously did not show the name of the builtin in
question (now represented by \f(CW%s above).
.ie n .SS "Unrecognized character '%s' in column %d"
.el .SS "Unrecognized character '%s' in column \f(CW%d"
Subsection "Unrecognized character '%s' in column %d"
This error previously did not state the column.

### Offset outside string

Subsection "Offset outside string"
This can now also be generated by a \f(CW\*(C`seek\*(C' on a file handle using
\f(CW\*(C`PerlIO::scalar\*(C'.

### Invalid escape in the specified encoding in regexp; marked by <-- \s-1HERE\s0 in m/%s/

Subsection "Invalid escape in the specified encoding in regexp; marked by <-- HERE in m/%s/"
New error, introduced as part of the fix to \s-1RT\s0 #40641 to handle encoding
of Unicode characters in regular expression comments.

### Your machine doesnt support dump/undump.

Subsection "Your machine doesn't support dump/undump."
A more informative fatal error issued when calling \f(CW\*(C`dump\*(C' on Win32 and
Cygwin. (Given that the purpose of \f(CW\*(C`dump\*(C' is to abort with a core dump,
and core dumps can't be produced on these platforms, this is more useful than
silently exiting.)

## Changed Internals

Header "Changed Internals"
The perl sources can now be compiled with a \*(C+ compiler instead of a C
compiler. A necessary implementation details is that under \*(C+, the macro
\f(CW\*(C`XS\*(C' used to define XSUBs now includes an \f(CW\*(C`extern "C"\*(C' definition. A side
effect of this is that **\*(C+** code that used the construction

.Vb 1
    typedef XS(SwigPerlWrapper);
.Ve

now needs to be written

.Vb 1
    typedef XSPROTO(SwigPerlWrapper);
.Ve

using the new \f(CW\*(C`XSPROTO\*(C' macro, in order to compile. C extensions are
unaffected, although C extensions are encouraged to use \f(CW\*(C`XSPROTO\*(C' too.
This change was present in the 5.10.0 release of perl, so any actively
maintained code that happened to use this construction should already have
been adapted. Code that needs changing will fail with a compilation error.

\f(CW\*(C`set\*(C' magic on localizing/assigning to a magic variable will now only
trigger for *container magics*, i.e. it will for \f(CW%ENV or \f(CW%SIG
but not for \f(CW$#array.

The new \s-1API\s0 macro \f(CW\*(C`newSVpvs()\*(C' can be used in place of constructions such as
\f(CW\*(C`newSVpvn("ISA", 3)\*(C'. It takes a single string constant, and at C compile
time determines its length.

The new \s-1API\s0 function \f(CW\*(C`Perl_newSV_type()\*(C' can be used as a more efficient
replacement of the common idiom

.Vb 2
    sv = newSV(0);
    sv_upgrade(sv, type);
.Ve

Similarly \f(CW\*(C`Perl_newSVpvn_flags()\*(C' can be used to combine
\f(CW\*(C`Perl_newSVpv()\*(C' with \f(CW\*(C`Perl_sv_2mortal()\*(C' or the equivalent
\f(CW\*(C`Perl_sv_newmortal()\*(C' with \f(CW\*(C`Perl_sv_setpvn()\*(C'

Two new macros \f(CW\*(C`mPUSHs()\*(C' and \f(CW\*(C`mXPUSHs()\*(C' are added, to make it easier to
push mortal SVs onto the stack. They were then used to fix several bugs where
values on the stack had not been mortalised.

A \f(CW\*(C`Perl_signbit()\*(C' function was added to test the sign of an \f(CW\*(C`NV\*(C'. It
maps to the system one when available.

\f(CW\*(C`Perl_av_reify()\*(C', \f(CW\*(C`Perl_lex_end()\*(C', \f(CW\*(C`Perl_mod()\*(C', \f(CW\*(C`Perl_op_clear()\*(C',
\f(CW\*(C`Perl_pop_return()\*(C', \f(CW\*(C`Perl_qerror()\*(C', \f(CW\*(C`Perl_setdefout()\*(C',
\f(CW\*(C`Perl_vivify_defelem()\*(C' and \f(CW\*(C`Perl_yylex()\*(C' are now visible to extensions.
This was required to allow \f(CW\*(C`Data::Alias\*(C' to work on Windows.

\f(CW\*(C`Perl_find_runcv()\*(C' is now visible to perl core extensions. This was required
to allow \f(CW\*(C`Sub::Current\*(C' to work on Windows.

\f(CW\*(C`ptr_table*\*(C' functions are now available in unthreaded perl. \f(CW\*(C`Storable\*(C'
takes advantage of this.

There have been many small cleanups made to the internals. In particular,
\f(CW\*(C`Perl_sv_upgrade()\*(C' has been simplified considerably, with a straight-through
code path that uses \f(CW\*(C`memset()\*(C' and \f(CW\*(C`memcpy()\*(C' to initialise the new body,
rather than assignment via multiple temporary variables. It has also
benefited from simplification and de-duplication of the arena management
code.

A lot of small improvements in the code base were made due to reports from
the Coverity static code analyzer.

Corrected use and documentation of \f(CW\*(C`Perl_gv_stashpv()\*(C', \f(CW\*(C`Perl_gv_stashpvn()\*(C',
\f(CW\*(C`Perl_gv_stashsv()\*(C' functions (last parameter is a bitmask, not boolean).

\f(CW\*(C`PERL_SYS_INIT\*(C', \f(CW\*(C`PERL_SYS_INIT3\*(C' and \f(CW\*(C`PERL_SYS_TERM\*(C' macros have been
changed into functions.

\f(CW\*(C`PERLSYS_TERM\*(C' no longer requires a context. \f(CW\*(C`PerlIO_teardown()\*(C'
is now called without a context, and debugging output in this function has
been disabled because that required that an interpreter was present, an invalid
assumption at termination time.

All compile time options which affect binary compatibility have been grouped
together into a global variable (\f(CW\*(C`PL_bincompat_options\*(C').

The values of \f(CW\*(C`PERL_REVISION\*(C', \f(CW\*(C`PERL_VERSION\*(C' and \f(CW\*(C`PERL_SUBVERSION\*(C' are
now baked into global variables (and hence into any shared perl library).
Additionally under \f(CW\*(C`MULTIPLICITY\*(C', the perl executable now records the size of
the interpreter structure (total, and for this version). Coupled with
\f(CW\*(C`PL_bincompat_options\*(C' this will allow 5.8.10 (and later), when compiled with a
shared perl library, to perform sanity checks in \f(CW\*(C`main()\*(C' to verify that the
shared library is indeed binary compatible.

Symbolic references can now have embedded NULs. The new public function
\f(CW\*(C`Perl_get_cvn_flags()\*(C' can be used in extensions if you have to handle them.

### Macro cleanups

Subsection "Macro cleanups"
The core code, and \s-1XS\s0 code in *ext* that is not dual-lived on \s-1CPAN,\s0 no longer
uses the macros \f(CW\*(C`PL_na\*(C', \f(CW\*(C`NEWSV()\*(C', \f(CW\*(C`Null()\*(C', \f(CW\*(C`Nullav\*(C', \f(CW\*(C`Nullcv\*(C',
\f(CW\*(C`Nullhv\*(C', \f(CW\*(C`Nullhv\*(C' *etc*. Their use is discouraged in new code,
particularly \f(CW\*(C`PL_na\*(C', which is a small performance hit.

## New Tests

Header "New Tests"
Many modules updated from \s-1CPAN\s0 incorporate new tests. Some core specific
tests have been added:

- ext/DynaLoader/t/DynaLoader.t
Item "ext/DynaLoader/t/DynaLoader.t"
Tests for the \f(CW\*(C`DynaLoader\*(C' module.

- t/comp/fold.t
Item "t/comp/fold.t"
Tests for compile-time constant folding.

- t/io/pvbm.t
Item "t/io/pvbm.t"
Tests incorporated from 5.10.0 which check that there is no unexpected
interaction between the internal types \f(CW\*(C`PVBM\*(C' and \f(CW\*(C`PVGV\*(C'.

- t/lib/proxy_constant_subs.t
Item "t/lib/proxy_constant_subs.t"
Tests for the new form of constant subroutines.

- t/op/attrhand.t
Item "t/op/attrhand.t"
Tests for \f(CW\*(C`Attribute::Handlers\*(C'.

- t/op/dbm.t
Item "t/op/dbm.t"
Tests for \f(CW\*(C`dbmopen\*(C'.

- t/op/inccode-tie.t
Item "t/op/inccode-tie.t"
Calls all tests in *t/op/inccode.t* after first tying \f(CW@INC.

- t/op/incfilter.t
Item "t/op/incfilter.t"
Tests for source filters returned from code references in \f(CW@INC.

- t/op/kill0.t
Item "t/op/kill0.t"
Tests for \s-1RT\s0 #30970.

- t/op/qrstack.t
Item "t/op/qrstack.t"
Tests for \s-1RT\s0 #41484.

- t/op/qr.t
Item "t/op/qr.t"
Tests for the \f(CW\*(C`qr//\*(C' construct.

- t/op/regexp_qr_embed.t
Item "t/op/regexp_qr_embed.t"
Tests for the \f(CW\*(C`qr//\*(C' construct within another regexp.

- t/op/regexp_qr.t
Item "t/op/regexp_qr.t"
Tests for the \f(CW\*(C`qr//\*(C' construct.

- t/op/rxcode.t
Item "t/op/rxcode.t"
Tests for \s-1RT\s0 #32840.

- t/op/studytied.t
Item "t/op/studytied.t"
Tests for \f(CW\*(C`study\*(C' on tied scalars.

- t/op/substT.t
Item "t/op/substT.t"
Tests for \f(CW\*(C`subst\*(C' run under \f(CW\*(C`-T\*(C' mode.

- t/op/symbolcache.t
Item "t/op/symbolcache.t"
Tests for \f(CW\*(C`undef\*(C' and \f(CW\*(C`delete\*(C' on stash entries that are bound to
subroutines or methods.

- t/op/upgrade.t
Item "t/op/upgrade.t"
Tests for \f(CW\*(C`Perl_sv_upgrade()\*(C'.

- t/mro/package_aliases.t
Item "t/mro/package_aliases.t"
\s-1MRO\s0 tests for \f(CW\*(C`isa\*(C' and package aliases.

- t/pod/twice.t
Item "t/pod/twice.t"
Tests for calling \f(CW\*(C`Pod::Parser\*(C' twice.

- t/run/cloexec.t
Item "t/run/cloexec.t"
Tests for inheriting file descriptors across \f(CW\*(C`exec\*(C' (close-on-exec).

- t/uni/cache.t
Item "t/uni/cache.t"
Tests for the \s-1UTF-8\s0 caching code.

- t/uni/chr.t
Item "t/uni/chr.t"
Test that strange encodings do not upset \f(CW\*(C`Perl_pp_chr()\*(C'.

- t/uni/greek.t
Item "t/uni/greek.t"
Tests for \s-1RT\s0 #40641.

- t/uni/latin2.t
Item "t/uni/latin2.t"
Tests for \s-1RT\s0 #40641.

- t/uni/overload.t
Item "t/uni/overload.t"
Tests for returning Unicode from overloaded values.

- t/uni/tie.t
Item "t/uni/tie.t"
Tests for returning Unicode from tied variables.

## Known Problems

Header "Known Problems"
There are no known new bugs.

However, programs that rely on bugs that have been fixed will have problems.
Also, many bug fixes present in 5.10.0 can't be back-ported to the 5.8.x
branch, because they require changes that are binary incompatible, or because
the code changes are too large and hence too risky to incorporate.

We have only limited volunteer labour, and the maintenance burden is
getting increasingly complex. Hence this will be the last significant
release of the 5.8.x series. Any future releases of 5.8.x will likely
only be to deal with security issues, and platform build
failures. Hence you should look to migrating to 5.10.x, if you have
not started already. Alternatively, if business requirements constrain
you to continue to use 5.8.x, you may wish to consider commercial
support from firms such as ActiveState.

## Platform Specific Notes

Header "Platform Specific Notes"

### Win32

Subsection "Win32"
\f(CW\*(C`readdir()\*(C', \f(CW\*(C`cwd()\*(C', \f(CW$^X and \f(CW@INC now use the alternate (short)
filename if the long name is outside the current codepage (Jan Dubois).

*Updated Modules*
Subsection "Updated Modules"

- \(bu
\f(CW\*(C`Win32\*(C' upgraded to version 0.38. Now has a documented 'WinVista' response
from \f(CW\*(C`GetOSName\*(C' and support for Vista's privilege elevation in \f(CW\*(C`IsAdminUser\*(C'.
Support for Unicode characters in path names. Improved cygwin and Win64
compatibility.

- \(bu
\f(CW\*(C`Win32API\*(C' updated to 0.1001_01

- \(bu
\f(CW\*(C`killpg()\*(C' support added to \f(CW\*(C`MSWin32\*(C' (Jan Dubois).

- \(bu
\f(CW\*(C`File::Spec::Win32\*(C' upgraded to version 3.2701

### \s-1OS/2\s0

Subsection "OS/2"
*Updated Modules*
Subsection "Updated Modules"

- \(bu
\f(CW\*(C`OS2::Process\*(C' upgraded to 1.03
.Sp
Ilya Zakharevich has added and documented several \f(CW\*(C`Window*\*(C' and \f(CW\*(C`Clipbrd*\*(C'
functions.

- \(bu
\f(CW\*(C`OS2::REXX::DLL\*(C', \f(CW\*(C`OS2::REXX\*(C' updated to version 1.03

### \s-1VMS\s0

Subsection "VMS"
*Updated Modules*
Subsection "Updated Modules"

- \(bu
\f(CW\*(C`DCLsym\*(C' upgraded to version 1.03

- \(bu
\f(CW\*(C`Stdio\*(C' upgraded to version 2.4

- \(bu
\f(CW\*(C`VMS::XSSymSet\*(C' upgraded to 1.1.

## Obituary

Header "Obituary"
Nick Ing-Simmons, long time Perl hacker, author of the \f(CW\*(C`Tk\*(C' and \f(CW\*(C`Encode\*(C'
modules, *perlio.c* in the core, and 5.003_02 pumpking, died of a heart
attack on 25th September 2006. He will be missed.

## Acknowledgements

Header "Acknowledgements"
Some of the work in this release was funded by a \s-1TPF\s0 grant.

Steve Hay worked behind the scenes working out the causes of the differences
between core modules, their \s-1CPAN\s0 releases, and previous core releases, and
the best way to rectify them. He doesn't want to do it again. I know this
feeling, and I'm very glad he did it this time, instead of me.

Paul Fenwick assembled a team of 18 volunteers, who broke the back of writing
this document. In particular, Bradley Dean, Eddy Tan, and Vincent Pit
provided half the team's contribution.

Schwern verified the list of updated module versions, correcting quite a few
errors that I (and everyone else) had missed, both wrongly stated module
versions, and changed modules that had not been listed.

The crack Berlin-based \s-1QA\s0 team of Andreas Ko\*:nig and Slaven Rezic
tirelessly re-built snapshots, tested most everything \s-1CPAN\s0 against
them, and then identified the changes responsible for any module regressions,
ensuring that several show-stopper bugs were stomped before the first release
candidate was cut.

The other core committers contributed most of the changes, and applied most
of the patches sent in by the hundreds of contributors listed in *\s-1AUTHORS\s0*.

And obviously, Larry Wall, without whom we wouldn't have Perl.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://bugs.perl.org.  There may also be
information at http://www.perl.org, the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.  You can browse and search
the Perl 5 bugs at http://bugs.perl.org/

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes
all the core committers, who will be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for security
issues in the Perl core, not for modules independently distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for exhaustive details on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
