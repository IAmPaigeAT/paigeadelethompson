+++
title = "snmptranslate(1)"
manpage_format = "troff"
detected_package_version = "0"
manpage_section = "1"
manpage_name = "snmptranslate"
keywords = ["snmpcmd", "variables", "rfc", "2578-2580"]
description = "is an application that translates one or more SNMP object identifier values from their symbolic (textual) forms into their numerical forms (or vice versa).   OID is either a numeric or textual object identifier. Turn on debugging output for the gi..."
author = "Sun Microsystems, Inc. All rights reserved."
operating_system = "macos"
operating_system_version = "15.3"
date = "2010-01-01"
+++

SNMPTRANSLATE 1 "20 Jul 2010" V5.6.2.1 "Net-SNMP"

## NAME

snmptranslate - translate MIB OID names between numeric and textual forms

## SYNOPSIS

snmptranslate
[OPTIONS] OID [OID]...

## DESCRIPTION

snmptranslate
is an application that translates one or more SNMP object identifier
values from their symbolic (textual) forms into their numerical forms
(or vice versa).

OID is either a numeric or textual object identifier.

## OPTIONS


-D\fI[TOKEN[,...]]
Turn on debugging output for the given
"TOKEN" "(s)."
Try
ALL
for extremely verbose output.

-h
Display a brief usage message and then exit.

-m " MIBLIST"
Specifies a colon separated list of MIB modules to load for this
application.  This overrides the environment variable MIBS.
.IP
The special keyword
ALL
is used to specify all modules in all directories when searching for MIB
files.  Every file whose name does not begin with "." will be parsed as
if it were a MIB file.

-M " DIRLIST"
Specifies a colon separated list of directories to search for MIBs.
This overrides the environment variable MIBDIRS.

-T " TRANSOPTS"
Provides control over the translation of the OID values.  The
following
TRANSOPTS
are available:

> 
-Td
Print full details of the specified OID.

-Tp
Print a graphical tree, rooted at the specified OID.

-Ta
Dump the loaded MIB in a trivial form.

-Tl
Dump a labeled form of all objects.

-To
Dump a numeric form of all objects.

-Ts
Dump a symbolic form of all objects.

-Tt
Dump a tree form of the loaded MIBs (mostly useful for debugging).

-Tz
Dump a numeric and labeled form of all objects (compatible with MIB2SCHEMA format).



-V
Display version information for the application and then exit.

-w " WIDTH"
Specifies the width of
-Tp
and
-Td
output. The default is very large.

In addition to the above options,
snmptranslate
takes the OID input
( -I ),
MIB parsing
( -M )
and OID output
( -O )
options described in the
"INPUT OPTIONS" ", " "MIB PARSING OPTIONS" " and " "OUTPUT OPTIONS"
sections of the
snmpcmd(1)
manual page.

## EXAMPLES


- \(bu
snmptranslate -On -IR sysDescr
.br
will translate "sysDescr" to a more qualified form:
.IP
system.sysDescr

- \(bu
snmptranslate -Onf -IR sysDescr
.br
will translate "sysDecr" to:
.IP
.iso.org.dod.internet.mgmt.mib-2.system.sysDescr

- \(bu
snmptranslate -Td -OS system.sysDescr
.br
will translate "sysDecr" into:
.IP

```
SNMPv2-MIB::sysDescr
sysDescr OBJECT-TYPE
  -- FROM SNMPv2-MIB
  -- TEXTUAL CONVENTION DisplayString
  SYNTAX OCTET STRING (0..255)
  DISPLAY-HINT "255a"
  MAX-ACCESS read-only
  STATUS current
  DESCRIPTION "A textual description of the entity. This
               value should include the full name and
               version identification of the system's
               hardware type, software operating-system,
               and networking software."
::= { iso(1) org(3) dod(6) internet(1) mgmt(2) mib-2(1) system(1) 1 }
```


- \(bu
snmptranslate -Tp -OS system
.br
will print the following tree:
.IP

```
+--system(1)
   |
   +-- -R-- String    sysDescr(1)
   |        Textual Convention: DisplayString
   |        Size: 0..255
   +-- -R-- ObjID     sysObjectID(2)
   +-- -R-- TimeTicks sysUpTime(3)
   +-- -RW- String    sysContact(4)
   |        Textual Convention: DisplayString
   |        Size: 0..255
   +-- -RW- String    sysName(5)
   |        Textual Convention: DisplayString
   |        Size: 0..255
   +-- -RW- String    sysLocation(6)
   |        Textual Convention: DisplayString
   |        Size: 0..255
   +-- -R-- Integer   sysServices(7)
   +-- -R-- TimeTicks sysORLastChange(8)
   |        Textual Convention: TimeStamp
   |
   +--sysORTable(9)
      |
      +--sysOREntry(1)
         |
         +-- ---- Integer   sysORIndex(1)
         +-- -R-- ObjID     sysORID(2)
         +-- -R-- String    sysORDescr(3)
         |        Textual Convention: DisplayString
         |        Size: 0..255
         +-- -R-- TimeTicks sysORUpTime(4)
                  Textual Convention: TimeStamp

```



- \(bu
snmptranslate -Ta | head
.br
will produce the following dump:
.IP

```
dump DEFINITIONS ::= BEGIN
org ::= { iso 3 }
dod ::= { org 6 }
internet ::= { dod 1 }
directory ::= { internet 1 }
mgmt ::= { internet 2 }
experimental ::= { internet 3 }
private ::= { internet 4 }
security ::= { internet 5 }
snmpV2 ::= { internet 6 }
```



- \(bu
snmptranslate -Tl | head
.br
will produce the following dump:
.IP

```
.RI .iso(1).org(3)
.RI .iso(1).org(3).dod(6)
.RI .iso(1).org(3).dod(6).internet(1)
.RI .iso(1).org(3).dod(6).internet(1).directory(1)
.RI .iso(1).org(3).dod(6).internet(1).mgmt(2)
.RI .iso(1).org(3).dod(6).internet(1).mgmt(2).mib-2(1)
.RI .iso(1).org(3).dod(6).internet(1).mgmt(2).mib-2(1).system(1)
.RI .iso(1).org(3).dod(6).internet(1).mgmt(2).mib-2(1).system(1).sysDescr(1)
.RI .iso(1).org(3).dod(6).internet(1).mgmt(2).mib-2(1).system(1).sysObjectID(2)
.RI .iso(1).org(3).dod(6).internet(1).mgmt(2).mib-2(1).system(1).sysUpTime(3)
```



- \(bu
snmptranslate -To | head
.br
will produce the following dump
.IP

```
.RI .1.3
.RI .1.3.6
.RI .1.3.6.1
.RI .1.3.6.1.1
.RI .1.3.6.1.2
.RI .1.3.6.1.2.1
.RI .1.3.6.1.2.1.1
.RI .1.3.6.1.2.1.1.1
.RI .1.3.6.1.2.1.1.2
.RI .1.3.6.1.2.1.1.3
```



- \(bu
snmptranslate -Ts | head
.br
will produce the following dump
.IP

```
.RI .iso.org
.RI .iso.org.dod
.RI .iso.org.dod.internet
.RI .iso.org.dod.internet.directory
.RI .iso.org.dod.internet.mgmt
.RI .iso.org.dod.internet.mgmt.mib-2
.RI .iso.org.dod.internet.mgmt.mib-2.system
.RI .iso.org.dod.internet.mgmt.mib-2.system.sysDescr
.RI .iso.org.dod.internet.mgmt.mib-2.system.sysObjectID
.RI .iso.org.dod.internet.mgmt.mib-2.system.sysUpTime
```



- \(bu
snmptranslate -Tt | head
.br
will produce the following dump
.IP

```
  org(3) type=0
    dod(6) type=0
      internet(1) type=0
        directory(1) type=0
        mgmt(2) type=0
          mib-2(1) type=0
            system(1) type=0
              sysDescr(1) type=2 tc=4 hint=255a
              sysObjectID(2) type=1
              sysUpTime(3) type=8
```


## SEE ALSO

snmpcmd(1), variables(5), RFC 2578-2580.
