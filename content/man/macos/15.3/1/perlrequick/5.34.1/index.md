+++
manpage_format = "troff"
manpage_section = "1"
manpage_name = "perlrequick"
keywords = ["header", "see", "also", "this", "is", "just", "a", "quick", "start", "guide", "for", "more", "in-depth", "tutorial", "on", "regexes", "perlretut", "and", "the", "reference", "page", "perlre", "author", "copyright", "c", "2000", "mark", "kvale", "all", "rights", "reserved", "document", "may", "be", "distributed", "under", "same", "terms", "as", "perl", "itself", "acknowledgments", "subsection", "would", "like", "to", "thank", "mark-jason", "dominus", "tom", "christiansen", "ilya", "zakharevich", "brad", "hughes", "mike", "giroux", "their", "helpful", "comments"]
description = "This page covers the very basics of understanding, creating and using regular expressions (regexes) in Perl. This page assumes you already know things, like what a *(Lpattern*(R is, and the basic syntax of using them.  If you dont, see perlret..."
operating_system_version = "15.3"
detected_package_version = "5.34.1"
date = "2022-02-19"
title = "perlrequick(1)"
operating_system = "macos"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLREQUICK 1"
PERLREQUICK 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlrequick - Perl regular expressions quick start

## DESCRIPTION

Header "DESCRIPTION"
This page covers the very basics of understanding, creating and
using regular expressions ('regexes') in Perl.

## The Guide

Header "The Guide"
This page assumes you already know things, like what a \*(L"pattern\*(R" is, and
the basic syntax of using them.  If you don't, see perlretut.

### Simple word matching

Subsection "Simple word matching"
The simplest regex is simply a word, or more generally, a string of
characters.  A regex consisting of a word matches any string that
contains that word:

.Vb 1
    "Hello World" =~ /World/;  # matches
.Ve

In this statement, \f(CW\*(C`World\*(C' is a regex and the \f(CW\*(C`//\*(C' enclosing
\f(CW\*(C`/World/\*(C' tells Perl to search a string for a match.  The operator
\f(CW\*(C`=~\*(C' associates the string with the regex match and produces a true
value if the regex matched, or false if the regex did not match.  In
our case, \f(CW\*(C`World\*(C' matches the second word in \f(CW"Hello World", so the
expression is true.  This idea has several variations.

Expressions like this are useful in conditionals:

.Vb 1
    print "It matches\\n" if "Hello World" =~ /World/;
.Ve

The sense of the match can be reversed by using \f(CW\*(C`!~\*(C' operator:

.Vb 1
    print "It doesn\*(Aqt match\\n" if "Hello World" !~ /World/;
.Ve

The literal string in the regex can be replaced by a variable:

.Vb 2
    $greeting = "World";
    print "It matches\\n" if "Hello World" =~ /$greeting/;
.Ve

If you're matching against \f(CW$_, the \f(CW\*(C`$_ =~\*(C' part can be omitted:

.Vb 2
    $_ = "Hello World";
    print "It matches\\n" if /World/;
.Ve

Finally, the \f(CW\*(C`//\*(C' default delimiters for a match can be changed to
arbitrary delimiters by putting an \f(CW\*(Aqm\*(Aq out front:

.Vb 4
    "Hello World" =~ m!World!;   # matches, delimited by \*(Aq!\*(Aq
    "Hello World" =~ m\{World\};   # matches, note the matching \*(Aq\{\}\*(Aq
    "/usr/bin/perl" =~ m"/perl"; # matches after \*(Aq/usr/bin\*(Aq,
                                 # \*(Aq/\*(Aq becomes an ordinary char
.Ve

Regexes must match a part of the string *exactly* in order for the
statement to be true:

.Vb 3
    "Hello World" =~ /world/;  # doesn\*(Aqt match, case sensitive
    "Hello World" =~ /o W/;    # matches, \*(Aq \*(Aq is an ordinary char
    "Hello World" =~ /World /; # doesn\*(Aqt match, no \*(Aq \*(Aq at end
.Ve

Perl will always match at the earliest possible point in the string:

.Vb 2
    "Hello World" =~ /o/;       # matches \*(Aqo\*(Aq in \*(AqHello\*(Aq
    "That hat is red" =~ /hat/; # matches \*(Aqhat\*(Aq in \*(AqThat\*(Aq
.Ve

Not all characters can be used 'as is' in a match.  Some characters,
called **metacharacters**, are considered special, and reserved for use
in regex notation.  The metacharacters are

.Vb 1
    \{\}[]()^$.|*+?\\
.Ve

A metacharacter can be matched literally by putting a backslash before
it:

.Vb 4
    "2+2=4" =~ /2+2/;    # doesn\*(Aqt match, + is a metacharacter
    "2+2=4" =~ /2\\+2/;   # matches, \\+ is treated like an ordinary +
    \*(AqC:\\WIN32\*(Aq =~ /C:\\\\WIN/;                       # matches
    "/usr/bin/perl" =~ /\\/usr\\/bin\\/perl/;  # matches
.Ve

In the last regex, the forward slash \f(CW\*(Aq/\*(Aq is also backslashed,
because it is used to delimit the regex.

Most of the metacharacters aren't always special, and other characters
(such as the ones delimitting the pattern) become special under various
circumstances.  This can be confusing and lead to unexpected results.
\f(CW\*(C`use\ re\ \*(Aqstrict\*(Aq\*(C' can notify you of potential
pitfalls.

Non-printable \s-1ASCII\s0 characters are represented by **escape sequences**.
Common examples are \f(CW\*(C`\\t\*(C' for a tab, \f(CW\*(C`\\n\*(C' for a newline, and \f(CW\*(C`\\r\*(C'
for a carriage return.  Arbitrary bytes are represented by octal
escape sequences, e.g., \f(CW\*(C`\\033\*(C', or hexadecimal escape sequences,
e.g., \f(CW\*(C`\\x1B\*(C':

.Vb 3
    "1000\\t2000" =~ m(0\\t2)  # matches
    "cat" =~ /\\143\\x61\\x74/  # matches in ASCII, but
                             # a weird way to spell cat
.Ve

Regexes are treated mostly as double-quoted strings, so variable
substitution works:

.Vb 3
    $foo = \*(Aqhouse\*(Aq;
    \*(Aqcathouse\*(Aq =~ /cat$foo/;   # matches
    \*(Aqhousecat\*(Aq =~ /$\{foo\}cat/; # matches
.Ve

With all of the regexes above, if the regex matched anywhere in the
string, it was considered a match.  To specify *where* it should
match, we would use the **anchor** metacharacters \f(CW\*(C`^\*(C' and \f(CW\*(C`$\*(C'.  The
anchor \f(CW\*(C`^\*(C' means match at the beginning of the string and the anchor
\f(CW\*(C`$\*(C' means match at the end of the string, or before a newline at the
end of the string.  Some examples:

.Vb 5
    "housekeeper" =~ /keeper/;         # matches
    "housekeeper" =~ /^keeper/;        # doesn\*(Aqt match
    "housekeeper" =~ /keeper$/;        # matches
    "housekeeper\\n" =~ /keeper$/;      # matches
    "housekeeper" =~ /^housekeeper$/;  # matches
.Ve

### Using character classes

Subsection "Using character classes"
A **character class** allows a set of possible characters, rather than
just a single character, to match at a particular point in a regex.
There are a number of different types of character classes, but usually
when people use this term, they are referring to the type described in
this section, which are technically called \*(L"Bracketed character
classes\*(R", because they are denoted by brackets \f(CW\*(C`[...]\*(C', with the set of
characters to be possibly matched inside.  But we'll drop the \*(L"bracketed\*(R"
below to correspond with common usage.  Here are some examples of
(bracketed) character classes:

.Vb 3
    /cat/;            # matches \*(Aqcat\*(Aq
    /[bcr]at/;        # matches \*(Aqbat\*(Aq, \*(Aqcat\*(Aq, or \*(Aqrat\*(Aq
    "abc" =~ /[cab]/; # matches \*(Aqa\*(Aq
.Ve

In the last statement, even though \f(CW\*(Aqc\*(Aq is the first character in
the class, the earliest point at which the regex can match is \f(CW\*(Aqa\*(Aq.

.Vb 3
    /[yY][eE][sS]/; # match \*(Aqyes\*(Aq in a case-insensitive way
                    # \*(Aqyes\*(Aq, \*(AqYes\*(Aq, \*(AqYES\*(Aq, etc.
    /yes/i;         # also match \*(Aqyes\*(Aq in a case-insensitive way
.Ve

The last example shows a match with an \f(CW\*(Aqi\*(Aq **modifier**, which makes
the match case-insensitive.

Character classes also have ordinary and special characters, but the
sets of ordinary and special characters inside a character class are
different than those outside a character class.  The special
characters for a character class are \f(CW\*(C`-]\\^$\*(C' and are matched using an
escape:

.Vb 5
   /[\\]c]def/; # matches \*(Aq]def\*(Aq or \*(Aqcdef\*(Aq
   $x = \*(Aqbcr\*(Aq;
   /[$x]at/;   # matches \*(Aqbat, \*(Aqcat\*(Aq, or \*(Aqrat\*(Aq
   /[\\$x]at/;  # matches \*(Aq$at\*(Aq or \*(Aqxat\*(Aq
   /[\\\\$x]at/; # matches \*(Aq\\at\*(Aq, \*(Aqbat, \*(Aqcat\*(Aq, or \*(Aqrat\*(Aq
.Ve

The special character \f(CW\*(Aq-\*(Aq acts as a range operator within character
classes, so that the unwieldy \f(CW\*(C`[0123456789]\*(C' and \f(CW\*(C`[abc...xyz]\*(C'
become the svelte \f(CW\*(C`[0-9]\*(C' and \f(CW\*(C`[a-z]\*(C':

.Vb 2
    /item[0-9]/;  # matches \*(Aqitem0\*(Aq or ... or \*(Aqitem9\*(Aq
    /[0-9a-fA-F]/;  # matches a hexadecimal digit
.Ve

If \f(CW\*(Aq-\*(Aq is the first or last character in a character class, it is
treated as an ordinary character.

The special character \f(CW\*(C`^\*(C' in the first position of a character class
denotes a **negated character class**, which matches any character but
those in the brackets.  Both \f(CW\*(C`[...]\*(C' and \f(CW\*(C`[^...]\*(C' must match a
character, or the match fails.  Then

.Vb 4
    /[^a]at/;  # doesn\*(Aqt match \*(Aqaat\*(Aq or \*(Aqat\*(Aq, but matches
               # all other \*(Aqbat\*(Aq, \*(Aqcat, \*(Aq0at\*(Aq, \*(Aq%at\*(Aq, etc.
    /[^0-9]/;  # matches a non-numeric character
    /[a^]at/;  # matches \*(Aqaat\*(Aq or \*(Aq^at\*(Aq; here \*(Aq^\*(Aq is ordinary
.Ve

Perl has several abbreviations for common character classes. (These
definitions are those that Perl uses in ASCII-safe mode with the \f(CW\*(C`/a\*(C' modifier.
Otherwise they could match many more non-ASCII Unicode characters as
well.  See \*(L"Backslash sequences\*(R" in perlrecharclass for details.)

- \(bu
\\d is a digit and represents
.Sp
.Vb 1
    [0-9]
.Ve

- \(bu
\\s is a whitespace character and represents
.Sp
.Vb 1
    [\\ \\t\\r\\n\\f]
.Ve

- \(bu
\\w is a word character (alphanumeric or _) and represents
.Sp
.Vb 1
    [0-9a-zA-Z_]
.Ve

- \(bu
\\D is a negated \\d; it represents any character but a digit
.Sp
.Vb 1
    [^0-9]
.Ve

- \(bu
\\S is a negated \\s; it represents any non-whitespace character
.Sp
.Vb 1
    [^\\s]
.Ve

- \(bu
\\W is a negated \\w; it represents any non-word character
.Sp
.Vb 1
    [^\\w]
.Ve

- \(bu
The period '.' matches any character but \*(L"\\n\*(R"

The \f(CW\*(C`\\d\\s\\w\\D\\S\\W\*(C' abbreviations can be used both inside and outside
of character classes.  Here are some in use:

.Vb 7
    /\\d\\d:\\d\\d:\\d\\d/; # matches a hh:mm:ss time format
    /[\\d\\s]/;         # matches any digit or whitespace character
    /\\w\\W\\w/;         # matches a word char, followed by a
                      # non-word char, followed by a word char
    /..rt/;           # matches any two chars, followed by \*(Aqrt\*(Aq
    /end\\./;          # matches \*(Aqend.\*(Aq
    /end[.]/;         # same thing, matches \*(Aqend.\*(Aq
.Ve

The **word\ anchor**\  \f(CW\*(C`\\b\*(C' matches a boundary between a word
character and a non-word character \f(CW\*(C`\\w\\W\*(C' or \f(CW\*(C`\\W\\w\*(C':

.Vb 4
    $x = "Housecat catenates house and cat";
    $x =~ /\\bcat/;  # matches cat in \*(Aqcatenates\*(Aq
    $x =~ /cat\\b/;  # matches cat in \*(Aqhousecat\*(Aq
    $x =~ /\\bcat\\b/;  # matches \*(Aqcat\*(Aq at end of string
.Ve

In the last example, the end of the string is considered a word
boundary.

For natural language processing (so that, for example, apostrophes are
included in words), use instead \f(CW\*(C`\\b\{wb\}\*(C'

.Vb 1
    "don\*(Aqt" =~ / .+? \\b\{wb\} /x;  # matches the whole string
.Ve

### Matching this or that

Subsection "Matching this or that"
We can match different character strings with the **alternation**
metacharacter \f(CW\*(Aq|\*(Aq.  To match \f(CW\*(C`dog\*(C' or \f(CW\*(C`cat\*(C', we form the regex
\f(CW\*(C`dog|cat\*(C'.  As before, Perl will try to match the regex at the
earliest possible point in the string.  At each character position,
Perl will first try to match the first alternative, \f(CW\*(C`dog\*(C'.  If
\f(CW\*(C`dog\*(C' doesn't match, Perl will then try the next alternative, \f(CW\*(C`cat\*(C'.
If \f(CW\*(C`cat\*(C' doesn't match either, then the match fails and Perl moves to
the next position in the string.  Some examples:

.Vb 2
    "cats and dogs" =~ /cat|dog|bird/;  # matches "cat"
    "cats and dogs" =~ /dog|cat|bird/;  # matches "cat"
.Ve

Even though \f(CW\*(C`dog\*(C' is the first alternative in the second regex,
\f(CW\*(C`cat\*(C' is able to match earlier in the string.

.Vb 2
    "cats"          =~ /c|ca|cat|cats/; # matches "c"
    "cats"          =~ /cats|cat|ca|c/; # matches "cats"
.Ve

At a given character position, the first alternative that allows the
regex match to succeed will be the one that matches. Here, all the
alternatives match at the first string position, so the first matches.

### Grouping things and hierarchical matching

Subsection "Grouping things and hierarchical matching"
The **grouping** metacharacters \f(CW\*(C`()\*(C' allow a part of a regex to be
treated as a single unit.  Parts of a regex are grouped by enclosing
them in parentheses.  The regex \f(CW\*(C`house(cat|keeper)\*(C' means match
\f(CW\*(C`house\*(C' followed by either \f(CW\*(C`cat\*(C' or \f(CW\*(C`keeper\*(C'.  Some more examples
are

.Vb 2
    /(a|b)b/;    # matches \*(Aqab\*(Aq or \*(Aqbb\*(Aq
    /(^a|b)c/;   # matches \*(Aqac\*(Aq at start of string or \*(Aqbc\*(Aq anywhere

    /house(cat|)/;  # matches either \*(Aqhousecat\*(Aq or \*(Aqhouse\*(Aq
    /house(cat(s|)|)/;  # matches either \*(Aqhousecats\*(Aq or \*(Aqhousecat\*(Aq or
                        # \*(Aqhouse\*(Aq.  Note groups can be nested.

    "20" =~ /(19|20|)\\d\\d/;  # matches the null alternative \*(Aq()\\d\\d\*(Aq,
                             # because \*(Aq20\\d\\d\*(Aq can\*(Aqt match
.Ve

### Extracting matches

Subsection "Extracting matches"
The grouping metacharacters \f(CW\*(C`()\*(C' also allow the extraction of the
parts of a string that matched.  For each grouping, the part that
matched inside goes into the special variables \f(CW$1, \f(CW$2, etc.
They can be used just as ordinary variables:

.Vb 5
    # extract hours, minutes, seconds
    $time =~ /(\\d\\d):(\\d\\d):(\\d\\d)/;  # match hh:mm:ss format
    $hours = $1;
    $minutes = $2;
    $seconds = $3;
.Ve

In list context, a match \f(CW\*(C`/regex/\*(C' with groupings will return the
list of matched values \f(CW\*(C`($1,$2,...)\*(C'.  So we could rewrite it as

.Vb 1
    ($hours, $minutes, $second) = ($time =~ /(\\d\\d):(\\d\\d):(\\d\\d)/);
.Ve

If the groupings in a regex are nested, \f(CW$1 gets the group with the
leftmost opening parenthesis, \f(CW$2 the next opening parenthesis,
etc.  For example, here is a complex regex and the matching variables
indicated below it:

.Vb 2
    /(ab(cd|ef)((gi)|j))/;
     1  2      34
.Ve

Associated with the matching variables \f(CW$1, \f(CW$2, ... are
the **backreferences** \f(CW\*(C`\\g1\*(C', \f(CW\*(C`\\g2\*(C', ...  Backreferences are
matching variables that can be used *inside* a regex:

.Vb 1
    /(\\w\\w\\w)\\s\\g1/; # find sequences like \*(Aqthe the\*(Aq in string
.Ve

\f(CW$1, \f(CW$2, ... should only be used outside of a regex, and \f(CW\*(C`\\g1\*(C',
\f(CW\*(C`\\g2\*(C', ... only inside a regex.

### Matching repetitions

Subsection "Matching repetitions"
The **quantifier** metacharacters \f(CW\*(C`?\*(C', \f(CW\*(C`*\*(C', \f(CW\*(C`+\*(C', and \f(CW\*(C`\{\}\*(C' allow us
to determine the number of repeats of a portion of a regex we
consider to be a match.  Quantifiers are put immediately after the
character, character class, or grouping that we want to specify.  They
have the following meanings:

- \(bu
\f(CW\*(C`a?\*(C' = match 'a' 1 or 0 times

- \(bu
\f(CW\*(C`a*\*(C' = match 'a' 0 or more times, i.e., any number of times

- \(bu
\f(CW\*(C`a+\*(C' = match 'a' 1 or more times, i.e., at least once

- \(bu
\f(CW\*(C`a\{n,m\}\*(C' = match at least \f(CW\*(C`n\*(C' times, but not more than \f(CW\*(C`m\*(C'
times.

- \(bu
\f(CW\*(C`a\{n,\}\*(C' = match at least \f(CW\*(C`n\*(C' or more times

- \(bu
\f(CW\*(C`a\{,n\}\*(C' = match \f(CW\*(C`n\*(C' times or fewer

- \(bu
\f(CW\*(C`a\{n\}\*(C' = match exactly \f(CW\*(C`n\*(C' times

Here are some examples:

.Vb 6
    /[a-z]+\\s+\\d*/;  # match a lowercase word, at least some space, and
                     # any number of digits
    /(\\w+)\\s+\\g1/;    # match doubled words of arbitrary length
    $year =~ /^\\d\{2,4\}$/;  # make sure year is at least 2 but not more
                           # than 4 digits
    $year =~ /^\\d\{ 4 \}$|^\\d\{2\}$/; # better match; throw out 3 digit dates
.Ve

These quantifiers will try to match as much of the string as possible,
while still allowing the regex to match.  So we have

.Vb 5
    $x = \*(Aqthe cat in the hat\*(Aq;
    $x =~ /^(.*)(at)(.*)$/; # matches,
                            # $1 = \*(Aqthe cat in the h\*(Aq
                            # $2 = \*(Aqat\*(Aq
                            # $3 = \*(Aq\*(Aq   (0 matches)
.Ve

The first quantifier \f(CW\*(C`.*\*(C' grabs as much of the string as possible
while still having the regex match. The second quantifier \f(CW\*(C`.*\*(C' has
no string left to it, so it matches 0 times.

### More matching

Subsection "More matching"
There are a few more things you might want to know about matching
operators.
The global modifier \f(CW\*(C`/g\*(C' allows the matching operator to match
within a string as many times as possible.  In scalar context,
successive matches against a string will have \f(CW\*(C`/g\*(C' jump from match
to match, keeping track of position in the string as it goes along.
You can get or set the position with the \f(CW\*(C`pos()\*(C' function.
For example,

.Vb 4
    $x = "cat dog house"; # 3 words
    while ($x =~ /(\\w+)/g) \{
        print "Word is $1, ends at position ", pos $x, "\\n";
    \}
.Ve

prints

.Vb 3
    Word is cat, ends at position 3
    Word is dog, ends at position 7
    Word is house, ends at position 13
.Ve

A failed match or changing the target string resets the position.  If
you don't want the position reset after failure to match, add the
\f(CW\*(C`/c\*(C', as in \f(CW\*(C`/regex/gc\*(C'.

In list context, \f(CW\*(C`/g\*(C' returns a list of matched groupings, or if
there are no groupings, a list of matches to the whole regex.  So

.Vb 4
    @words = ($x =~ /(\\w+)/g);  # matches,
                                # $word[0] = \*(Aqcat\*(Aq
                                # $word[1] = \*(Aqdog\*(Aq
                                # $word[2] = \*(Aqhouse\*(Aq
.Ve

### Search and replace

Subsection "Search and replace"
Search and replace is performed using \f(CW\*(C`s/regex/replacement/modifiers\*(C'.
The \f(CW\*(C`replacement\*(C' is a Perl double-quoted string that replaces in the
string whatever is matched with the \f(CW\*(C`regex\*(C'.  The operator \f(CW\*(C`=~\*(C' is
also used here to associate a string with \f(CW\*(C`s///\*(C'.  If matching
against \f(CW$_, the \f(CW\*(C`$_\ =~\*(C' can be dropped.  If there is a match,
\f(CW\*(C`s///\*(C' returns the number of substitutions made; otherwise it returns
false.  Here are a few examples:

.Vb 5
    $x = "Time to feed the cat!";
    $x =~ s/cat/hacker/;   # $x contains "Time to feed the hacker!"
    $y = "\*(Aqquoted words\*(Aq";
    $y =~ s/^\*(Aq(.*)\*(Aq$/$1/;  # strip single quotes,
                           # $y contains "quoted words"
.Ve

With the \f(CW\*(C`s///\*(C' operator, the matched variables \f(CW$1, \f(CW$2, etc.
are immediately available for use in the replacement expression. With
the global modifier, \f(CW\*(C`s///g\*(C' will search and replace all occurrences
of the regex in the string:

.Vb 4
    $x = "I batted 4 for 4";
    $x =~ s/4/four/;   # $x contains "I batted four for 4"
    $x = "I batted 4 for 4";
    $x =~ s/4/four/g;  # $x contains "I batted four for four"
.Ve

The non-destructive modifier \f(CW\*(C`s///r\*(C' causes the result of the substitution
to be returned instead of modifying \f(CW$_ (or whatever variable the
substitute was bound to with \f(CW\*(C`=~\*(C'):

.Vb 3
    $x = "I like dogs.";
    $y = $x =~ s/dogs/cats/r;
    print "$x $y\\n"; # prints "I like dogs. I like cats."

    $x = "Cats are great.";
    print $x =~ s/Cats/Dogs/r =~ s/Dogs/Frogs/r =~
        s/Frogs/Hedgehogs/r, "\\n";
    # prints "Hedgehogs are great."

    @foo = map \{ s/[a-z]/X/r \} qw(a b c 1 2 3);
    # @foo is now qw(X X X 1 2 3)
.Ve

The evaluation modifier \f(CW\*(C`s///e\*(C' wraps an \f(CW\*(C`eval\{...\}\*(C' around the
replacement string and the evaluated result is substituted for the
matched substring.  Some examples:

.Vb 3
    # reverse all the words in a string
    $x = "the cat in the hat";
    $x =~ s/(\\w+)/reverse $1/ge;   # $x contains "eht tac ni eht tah"

    # convert percentage to decimal
    $x = "A 39% hit rate";
    $x =~ s!(\\d+)%!$1/100!e;       # $x contains "A 0.39 hit rate"
.Ve

The last example shows that \f(CW\*(C`s///\*(C' can use other delimiters, such as
\f(CW\*(C`s!!!\*(C' and \f(CW\*(C`s\{\}\{\}\*(C', and even \f(CW\*(C`s\{\}//\*(C'.  If single quotes are used
\f(CW\*(C`s\*(Aq\*(Aq\*(Aq\*(C', then the regex and replacement are treated as single-quoted
strings.

### The split operator

Subsection "The split operator"
\f(CW\*(C`split /regex/, string\*(C' splits \f(CW\*(C`string\*(C' into a list of substrings
and returns that list.  The regex determines the character sequence
that \f(CW\*(C`string\*(C' is split with respect to.  For example, to split a
string into words, use

.Vb 4
    $x = "Calvin and Hobbes";
    @word = split /\\s+/, $x;  # $word[0] = \*(AqCalvin\*(Aq
                              # $word[1] = \*(Aqand\*(Aq
                              # $word[2] = \*(AqHobbes\*(Aq
.Ve

To extract a comma-delimited list of numbers, use

.Vb 4
    $x = "1.618,2.718,   3.142";
    @const = split /,\\s*/, $x;  # $const[0] = \*(Aq1.618\*(Aq
                                # $const[1] = \*(Aq2.718\*(Aq
                                # $const[2] = \*(Aq3.142\*(Aq
.Ve

If the empty regex \f(CW\*(C`//\*(C' is used, the string is split into individual
characters.  If the regex has groupings, then the list produced contains
the matched substrings from the groupings as well:

.Vb 6
    $x = "/usr/bin";
    @parts = split m!(/)!, $x;  # $parts[0] = \*(Aq\*(Aq
                                # $parts[1] = \*(Aq/\*(Aq
                                # $parts[2] = \*(Aqusr\*(Aq
                                # $parts[3] = \*(Aq/\*(Aq
                                # $parts[4] = \*(Aqbin\*(Aq
.Ve

Since the first character of \f(CW$x matched the regex, \f(CW\*(C`split\*(C' prepended
an empty initial element to the list.
.ie n .SS """use re \*(Aqstrict\*(Aq"""
.el .SS "\f(CWuse re \*(Aqstrict\*(Aq"
Subsection "use re strict"
New in v5.22, this applies stricter rules than otherwise when compiling
regular expression patterns.  It can find things that, while legal, may
not be what you intended.

See 'strict' in re.

## BUGS

Header "BUGS"
None.

## SEE ALSO

Header "SEE ALSO"
This is just a quick start guide.  For a more in-depth tutorial on
regexes, see perlretut and for the reference page, see perlre.

## AUTHOR AND COPYRIGHT

Header "AUTHOR AND COPYRIGHT"
Copyright (c) 2000 Mark Kvale
All rights reserved.

This document may be distributed under the same terms as Perl itself.

### Acknowledgments

Subsection "Acknowledgments"
The author would like to thank Mark-Jason Dominus, Tom Christiansen,
Ilya Zakharevich, Brad Hughes, and Mike Giroux for all their helpful
comments.
