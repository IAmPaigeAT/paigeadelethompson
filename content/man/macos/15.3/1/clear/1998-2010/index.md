+++
author = ",2013 Free Software Foundation, Inc. *"
date = "Sun Feb 16 04:48:17 2025"
detected_package_version = "1998-2010"
manpage_format = "troff"
description = "clear clears your screen if this is possible, including its scrollback buffer (if the extended E3 capability is defined). clear looks in the environment for the terminal type and then in the terminfo database to determine how to clear the screen...."
operating_system_version = "15.3"
keywords = ["fbtput", "fbterminfo", "n", "this", "describes", "fbncurses", "version", "5", "7", "patch", "20081102"]
title = "clear(1)"
manpage_section = "1"
operating_system = "macos"
manpage_name = "clear"
+++

clear 1 ""
.ds n 5

## NAME

**clear** - clear the terminal screen

## SYNOPSIS

**clear**
.br

## DESCRIPTION

**clear** clears your screen if this is possible,
including its scrollback buffer (if the extended "E3" capability is defined).
**clear** looks in the environment for the terminal type and then in the
**terminfo** database to determine how to clear the screen.

**clear** ignores any command-line parameters that may be present.

## SEE ALSO

**tput**(1), **terminfo**(\*n)

This describes **ncurses**
version 5.7 (patch 20081102).
