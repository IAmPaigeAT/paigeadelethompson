+++
operating_system = "macos"
title = "perluniintro(1)"
manpage_name = "perluniintro"
author = "None Specified"
description = "This document gives a general idea of Unicode and how to use Unicode in Perl.  See *(LFurther Resources*(R for references to more in-depth treatments of Unicode. Unicode is a character set standard which plans to codify all of the writing system..."
manpage_format = "troff"
detected_package_version = "5.34.1"
manpage_section = "1"
date = "2022-02-19"
keywords = ["header", "see", "also", "perlunitut", "perlunicode", "encode", "open", "utf8", "bytes", "perlretut", "perlrun", "unicode", "collate", "normalize", "ucd", "acknowledgments", "thanks", "to", "the", "kind", "readers", "of", "perl5-porters", "perl", "org", "perl-unicode", "linux-utf8", "nl", "linux", "and", "unicore", "mailing", "lists", "for", "their", "valuable", "feedback", "author", "copyright", "license", "2001-2011", "jarkko", "hietaniemi", "jhi", "iki", "fi", "now", "maintained", "by", "5", "porters", "this", "document", "may", "be", "distributed", "under", "same", "terms", "as", "itself"]
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLUNIINTRO 1"
PERLUNIINTRO 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perluniintro - Perl Unicode introduction

## DESCRIPTION

Header "DESCRIPTION"
This document gives a general idea of Unicode and how to use Unicode
in Perl.  See \*(L"Further Resources\*(R" for references to more in-depth
treatments of Unicode.

### Unicode

Subsection "Unicode"
Unicode is a character set standard which plans to codify all of the
writing systems of the world, plus many other symbols.

Unicode and \s-1ISO/IEC 10646\s0 are coordinated standards that unify
almost all other modern character set standards,
covering more than 80 writing systems and hundreds of languages,
including all commercially-important modern languages.  All characters
in the largest Chinese, Japanese, and Korean dictionaries are also
encoded. The standards will eventually cover almost all characters in
more than 250 writing systems and thousands of languages.
Unicode 1.0 was released in October 1991, and 6.0 in October 2010.

A Unicode *character* is an abstract entity.  It is not bound to any
particular integer width, especially not to the C language \f(CW\*(C`char\*(C'.
Unicode is language-neutral and display-neutral: it does not encode the
language of the text, and it does not generally define fonts or other graphical
layout details.  Unicode operates on characters and on text built from
those characters.

Unicode defines characters like \f(CW\*(C`LATIN CAPITAL LETTER A\*(C' or \f(CW\*(C`GREEK
SMALL LETTER ALPHA\*(C' and unique numbers for the characters, in this
case 0x0041 and 0x03B1, respectively.  These unique numbers are called
*code points*.  A code point is essentially the position of the
character within the set of all possible Unicode characters, and thus in
Perl, the term *ordinal* is often used interchangeably with it.

The Unicode standard prefers using hexadecimal notation for the code
points.  If numbers like \f(CW0x0041 are unfamiliar to you, take a peek
at a later section, \*(L"Hexadecimal Notation\*(R".  The Unicode standard
uses the notation \f(CW\*(C`U+0041 LATIN CAPITAL LETTER A\*(C', to give the
hexadecimal code point and the normative name of the character.

Unicode also defines various *properties* for the characters, like
\*(L"uppercase\*(R" or \*(L"lowercase\*(R", \*(L"decimal digit\*(R", or \*(L"punctuation\*(R";
these properties are independent of the names of the characters.
Furthermore, various operations on the characters like uppercasing,
lowercasing, and collating (sorting) are defined.

A Unicode *logical* \*(L"character\*(R" can actually consist of more than one internal
*actual* \*(L"character\*(R" or code point.  For Western languages, this is adequately
modelled by a *base character* (like \f(CW\*(C`LATIN CAPITAL LETTER A\*(C') followed
by one or more *modifiers* (like \f(CW\*(C`COMBINING ACUTE ACCENT\*(C').  This sequence of
base character and modifiers is called a \fIcombining character
sequence.  Some non-western languages require more complicated
models, so Unicode created the *grapheme cluster* concept, which was
later further refined into the *extended grapheme cluster*.  For
example, a Korean Hangul syllable is considered a single logical
character, but most often consists of three actual
Unicode characters: a leading consonant followed by an interior vowel followed
by a trailing consonant.

Whether to call these extended grapheme clusters \*(L"characters\*(R" depends on your
point of view. If you are a programmer, you probably would tend towards seeing
each element in the sequences as one unit, or \*(L"character\*(R".  However from
the user's point of view, the whole sequence could be seen as one
\*(L"character\*(R" since that's probably what it looks like in the context of the
user's language.  In this document, we take the programmer's point of
view: one \*(L"character\*(R" is one Unicode code point.

For some combinations of base character and modifiers, there are
*precomposed* characters.  There is a single character equivalent, for
example, for the sequence \f(CW\*(C`LATIN CAPITAL LETTER A\*(C' followed by
\f(CW\*(C`COMBINING ACUTE ACCENT\*(C'.  It is called  \f(CW\*(C`LATIN CAPITAL LETTER A WITH
ACUTE\*(C'.  These precomposed characters are, however, only available for
some combinations, and are mainly meant to support round-trip
conversions between Unicode and legacy standards (like \s-1ISO 8859\s0).  Using
sequences, as Unicode does, allows for needing fewer basic building blocks
(code points) to express many more potential grapheme clusters.  To
support conversion between equivalent forms, various \fInormalization
forms are also defined.  Thus, \f(CW\*(C`LATIN CAPITAL LETTER A WITH ACUTE\*(C' is
in *Normalization Form Composed*, (abbreviated \s-1NFC\s0), and the sequence
\f(CW\*(C`LATIN CAPITAL LETTER A\*(C' followed by \f(CW\*(C`COMBINING ACUTE ACCENT\*(C'
represents the same character in *Normalization Form Decomposed* (\s-1NFD\s0).

Because of backward compatibility with legacy encodings, the \*(L"a unique
number for every character\*(R" idea breaks down a bit: instead, there is
\*(L"at least one number for every character\*(R".  The same character could
be represented differently in several legacy encodings.  The
converse is not true: some code points do not have an assigned
character.  Firstly, there are unallocated code points within
otherwise used blocks.  Secondly, there are special Unicode control
characters that do not represent true characters.

When Unicode was first conceived, it was thought that all the world's
characters could be represented using a 16-bit word; that is a maximum of
\f(CW0x10000 (or 65,536) characters would be needed, from \f(CW0x0000 to
\f(CW0xFFFF.  This soon proved to be wrong, and since Unicode 2.0 (July
1996), Unicode has been defined all the way up to 21 bits (\f(CW0x10FFFF),
and Unicode 3.1 (March 2001) defined the first characters above \f(CW0xFFFF.
The first \f(CW0x10000 characters are called the *Plane 0*, or the
*Basic Multilingual Plane* (\s-1BMP\s0).  With Unicode 3.1, 17 (yes,
seventeen) planes in all were defined\*(--but they are nowhere near full of
defined characters, yet.

When a new language is being encoded, Unicode generally will choose a
\f(CW\*(C`block\*(C' of consecutive unallocated code points for its characters.  So
far, the number of code points in these blocks has always been evenly
divisible by 16.  Extras in a block, not currently needed, are left
unallocated, for future growth.  But there have been occasions when
a later release needed more code points than the available extras, and a
new block had to allocated somewhere else, not contiguous to the initial
one, to handle the overflow.  Thus, it became apparent early on that
\*(L"block\*(R" wasn't an adequate organizing principle, and so the \f(CW\*(C`Script\*(C'
property was created.  (Later an improved script property was added as
well, the \f(CW\*(C`Script_Extensions\*(C' property.)  Those code points that are in
overflow blocks can still
have the same script as the original ones.  The script concept fits more
closely with natural language: there is \f(CW\*(C`Latin\*(C' script, \f(CW\*(C`Greek\*(C'
script, and so on; and there are several artificial scripts, like
\f(CW\*(C`Common\*(C' for characters that are used in multiple scripts, such as
mathematical symbols.  Scripts usually span varied parts of several
blocks.  For more information about scripts, see \*(L"Scripts\*(R" in perlunicode.
The division into blocks exists, but it is almost completely
accidental\*(--an artifact of how the characters have been and still are
allocated.  (Note that this paragraph has oversimplified things for the
sake of this being an introduction.  Unicode doesn't really encode
languages, but the writing systems for them\*(--their scripts; and one
script can be used by many languages.  Unicode also encodes things that
aren't really about languages, such as symbols like \f(CW\*(C`BAGGAGE CLAIM\*(C'.)

The Unicode code points are just abstract numbers.  To input and
output these abstract numbers, the numbers must be *encoded* or
*serialised* somehow.  Unicode defines several \fIcharacter encoding
forms, of which *\s-1UTF-8\s0* is the most popular.  \s-1UTF-8\s0 is a
variable length encoding that encodes Unicode characters as 1 to 4
bytes.  Other encodings
include \s-1UTF-16\s0 and \s-1UTF-32\s0 and their big- and little-endian variants
(\s-1UTF-8\s0 is byte-order independent).  The \s-1ISO/IEC 10646\s0 defines the \s-1UCS-2\s0
and \s-1UCS-4\s0 encoding forms.

For more information about encodings\*(--for instance, to learn what
*surrogates* and *byte order marks* (BOMs) are\*(--see perlunicode.

### Perls Unicode Support

Subsection "Perl's Unicode Support"
Starting from Perl v5.6.0, Perl has had the capacity to handle Unicode
natively.  Perl v5.8.0, however, is the first recommended release for
serious Unicode work.  The maintenance release 5.6.1 fixed many of the
problems of the initial Unicode implementation, but for example
regular expressions still do not work with Unicode in 5.6.1.
Perl v5.14.0 is the first release where Unicode support is
(almost) seamlessly integratable without some gotchas. (There are a few
exceptions. Firstly, some differences in quotemeta
were fixed starting in Perl 5.16.0. Secondly, some differences in
the range operator were fixed starting in
Perl 5.26.0. Thirdly, some differences in split were fixed
started in Perl 5.28.0.)

To enable this
seamless support, you should \f(CW\*(C`use feature \*(Aqunicode_strings\*(Aq\*(C' (which is
automatically selected if you \f(CW\*(C`use 5.012\*(C' or higher).  See feature.
(5.14 also fixes a number of bugs and departures from the Unicode
standard.)

Before Perl v5.8.0, the use of \f(CW\*(C`use utf8\*(C' was used to declare
that operations in the current block or file would be Unicode-aware.
This model was found to be wrong, or at least clumsy: the \*(L"Unicodeness\*(R"
is now carried with the data, instead of being attached to the
operations.
Starting with Perl v5.8.0, only one case remains where an explicit \f(CW\*(C`use
utf8\*(C' is needed: if your Perl script itself is encoded in \s-1UTF-8,\s0 you can
use \s-1UTF-8\s0 in your identifier names, and in string and regular expression
literals, by saying \f(CW\*(C`use utf8\*(C'.  This is not the default because
scripts with legacy 8-bit data in them would break.  See utf8.

### Perls Unicode Model

Subsection "Perl's Unicode Model"
Perl supports both pre-5.6 strings of eight-bit native bytes, and
strings of Unicode characters.  The general principle is that Perl tries
to keep its data as eight-bit bytes for as long as possible, but as soon
as Unicodeness cannot be avoided, the data is transparently upgraded
to Unicode.  Prior to Perl v5.14.0, the upgrade was not completely
transparent (see \*(L"The \*(R"Unicode Bug"" in perlunicode), and for backwards
compatibility, full transparency is not gained unless \f(CW\*(C`use feature
\*(Aqunicode_strings\*(Aq\*(C' (see feature) or \f(CW\*(C`use 5.012\*(C' (or higher) is
selected.

Internally, Perl currently uses either whatever the native eight-bit
character set of the platform (for example Latin-1) is, defaulting to
\s-1UTF-8,\s0 to encode Unicode strings. Specifically, if all code points in
the string are \f(CW0xFF or less, Perl uses the native eight-bit
character set.  Otherwise, it uses \s-1UTF-8.\s0

A user of Perl does not normally need to know nor care how Perl
happens to encode its internal strings, but it becomes relevant when
outputting Unicode strings to a stream without a PerlIO layer (one with
the \*(L"default\*(R" encoding).  In such a case, the raw bytes used internally
(the native character set or \s-1UTF-8,\s0 as appropriate for each string)
will be used, and a \*(L"Wide character\*(R" warning will be issued if those
strings contain a character beyond 0x00FF.

For example,

.Vb 1
      perl -e \*(Aqprint "\\x\{DF\}\\n", "\\x\{0100\}\\x\{DF\}\\n"\*(Aq
.Ve

produces a fairly useless mixture of native bytes and \s-1UTF-8,\s0 as well
as a warning:

.Vb 1
     Wide character in print at ...
.Ve

To output \s-1UTF-8,\s0 use the \f(CW\*(C`:encoding\*(C' or \f(CW\*(C`:utf8\*(C' output layer.  Prepending

.Vb 1
      binmode(STDOUT, ":utf8");
.Ve

to this sample program ensures that the output is completely \s-1UTF-8,\s0
and removes the program's warning.

You can enable automatic UTF-8-ification of your standard file
handles, default \f(CW\*(C`open()\*(C' layer, and \f(CW@ARGV by using either
the \f(CW\*(C`-C\*(C' command line switch or the \f(CW\*(C`PERL_UNICODE\*(C' environment
variable, see perlrun for the
documentation of the \f(CW\*(C`-C\*(C' switch.

Note that this means that Perl expects other software to work the same
way:
if Perl has been led to believe that \s-1STDIN\s0 should be \s-1UTF-8,\s0 but then
\s-1STDIN\s0 coming in from another command is not \s-1UTF-8,\s0 Perl will likely
complain about the malformed \s-1UTF-8.\s0

All features that combine Unicode and I/O also require using the new
PerlIO feature.  Almost all Perl 5.8 platforms do use PerlIO, though:
you can see whether yours is by running \*(L"perl -V\*(R" and looking for
\f(CW\*(C`useperlio=define\*(C'.

### Unicode and \s-1EBCDIC\s0

Subsection "Unicode and EBCDIC"
Perl 5.8.0 added support for Unicode on \s-1EBCDIC\s0 platforms.  This support
was allowed to lapse in later releases, but was revived in 5.22.
Unicode support is somewhat more complex to implement since additional
conversions are needed.  See perlebcdic for more information.

On \s-1EBCDIC\s0 platforms, the internal Unicode encoding form is UTF-EBCDIC
instead of \s-1UTF-8.\s0  The difference is that as \s-1UTF-8\s0 is \*(L"ASCII-safe\*(R" in
that \s-1ASCII\s0 characters encode to \s-1UTF-8\s0 as-is, while UTF-EBCDIC is
\*(L"EBCDIC-safe\*(R", in that all the basic characters (which includes all
those that have \s-1ASCII\s0 equivalents (like \f(CW"A", \f(CW"0", \f(CW"%", *etc.*)
are the same in both \s-1EBCDIC\s0 and UTF-EBCDIC.  Often, documentation
will use the term \*(L"\s-1UTF-8\*(R"\s0 to mean UTF-EBCDIC as well.  This is the case
in this document.

### Creating Unicode

Subsection "Creating Unicode"
This section applies fully to Perls starting with v5.22.  Various
caveats for earlier releases are in the \*(L"Earlier releases caveats\*(R"
subsection below.

To create Unicode characters in literals,
use the \f(CW\*(C`\\N\{...\}\*(C' notation in double-quoted strings:

.Vb 2
 my $smiley_from_name = "\\N\{WHITE SMILING FACE\}";
 my $smiley_from_code_point = "\\N\{U+263a\}";
.Ve

Similarly, they can be used in regular expression literals

.Vb 2
 $smiley =~ /\\N\{WHITE SMILING FACE\}/;
 $smiley =~ /\\N\{U+263a\}/;
.Ve

or, starting in v5.32:

.Vb 2
 $smiley =~ /\\p\{Name=WHITE SMILING FACE\}/;
 $smiley =~ /\\p\{Name=whitesmilingface\}/;
.Ve

At run-time you can use:

.Vb 4
 use charnames ();
 my $hebrew_alef_from_name
                      = charnames::string_vianame("HEBREW LETTER ALEF");
 my $hebrew_alef_from_code_point = charnames::string_vianame("U+05D0");
.Ve

Naturally, \f(CW\*(C`ord()\*(C' will do the reverse: it turns a character into
a code point.

There are other runtime options as well.  You can use \f(CW\*(C`pack()\*(C':

.Vb 1
 my $hebrew_alef_from_code_point = pack("U", 0x05d0);
.Ve

Or you can use \f(CW\*(C`chr()\*(C', though it is less convenient in the general
case:

.Vb 2
 $hebrew_alef_from_code_point = chr(utf8::unicode_to_native(0x05d0));
 utf8::upgrade($hebrew_alef_from_code_point);
.Ve

The \f(CW\*(C`utf8::unicode_to_native()\*(C' and \f(CW\*(C`utf8::upgrade()\*(C' aren't needed if
the argument is above 0xFF, so the above could have been written as

.Vb 1
 $hebrew_alef_from_code_point = chr(0x05d0);
.Ve

since 0x5d0 is above 255.

\f(CW\*(C`\\x\{\}\*(C' and \f(CW\*(C`\\o\{\}\*(C' can also be used to specify code points at compile
time in double-quotish strings, but, for backward compatibility with
older Perls, the same rules apply as with \f(CW\*(C`chr()\*(C' for code points less
than 256.

\f(CW\*(C`utf8::unicode_to_native()\*(C' is used so that the Perl code is portable
to \s-1EBCDIC\s0 platforms.  You can omit it if you're *really* sure no one
will ever want to use your code on a non-ASCII platform.  Starting in
Perl v5.22, calls to it on \s-1ASCII\s0 platforms are optimized out, so there's
no performance penalty at all in adding it.  Or you can simply use the
other constructs that don't require it.

See \*(L"Further Resources\*(R" for how to find all these names and numeric
codes.

*Earlier releases caveats*
Subsection "Earlier releases caveats"

On \s-1EBCDIC\s0 platforms, prior to v5.22, using \f(CW\*(C`\\N\{U+...\}\*(C' doesn't work
properly.

Prior to v5.16, using \f(CW\*(C`\\N\{...\}\*(C' with a character name (as opposed to a
\f(CW\*(C`U+...\*(C' code point) required a \f(CW\*(C`use\ charnames\ :full\*(C'.

Prior to v5.14, there were some bugs in \f(CW\*(C`\\N\{...\}\*(C' with a character name
(as opposed to a \f(CW\*(C`U+...\*(C' code point).

\f(CW\*(C`charnames::string_vianame()\*(C' was introduced in v5.14.  Prior to that,
\f(CW\*(C`charnames::vianame()\*(C' should work, but only if the argument is of the
form \f(CW"U+...".  Your best bet there for runtime Unicode by character
name is probably:

.Vb 3
 use charnames ();
 my $hebrew_alef_from_name
                  = pack("U", charnames::vianame("HEBREW LETTER ALEF"));
.Ve

### Handling Unicode

Subsection "Handling Unicode"
Handling Unicode is for the most part transparent: just use the
strings as usual.  Functions like \f(CW\*(C`index()\*(C', \f(CW\*(C`length()\*(C', and
\f(CW\*(C`substr()\*(C' will work on the Unicode characters; regular expressions
will work on the Unicode characters (see perlunicode and perlretut).

Note that Perl considers grapheme clusters to be separate characters, so for
example

.Vb 2
 print length("\\N\{LATIN CAPITAL LETTER A\}\\N\{COMBINING ACUTE ACCENT\}"),
       "\\n";
.Ve

will print 2, not 1.  The only exception is that regular expressions
have \f(CW\*(C`\\X\*(C' for matching an extended grapheme cluster.  (Thus \f(CW\*(C`\\X\*(C' in a
regular expression would match the entire sequence of both the example
characters.)

Life is not quite so transparent, however, when working with legacy
encodings, I/O, and certain special cases:

### Legacy Encodings

Subsection "Legacy Encodings"
When you combine legacy data and Unicode, the legacy data needs
to be upgraded to Unicode.  Normally the legacy data is assumed to be
\s-1ISO 8859-1\s0 (or \s-1EBCDIC,\s0 if applicable).

The \f(CW\*(C`Encode\*(C' module knows about many encodings and has interfaces
for doing conversions between those encodings:

.Vb 2
    use Encode \*(Aqdecode\*(Aq;
    $data = decode("iso-8859-3", $data); # convert from legacy
.Ve

### Unicode I/O

Subsection "Unicode I/O"
Normally, writing out Unicode data

.Vb 1
    print FH $some_string_with_unicode, "\\n";
.Ve

produces raw bytes that Perl happens to use to internally encode the
Unicode string.  Perl's internal encoding depends on the system as
well as what characters happen to be in the string at the time. If
any of the characters are at code points \f(CW0x100 or above, you will get
a warning.  To ensure that the output is explicitly rendered in the
encoding you desire\*(--and to avoid the warning\*(--open the stream with
the desired encoding. Some examples:

.Vb 1
    open FH, ">:utf8", "file";

    open FH, ">:encoding(ucs2)",      "file";
    open FH, ">:encoding(UTF-8)",     "file";
    open FH, ">:encoding(shift_jis)", "file";
.Ve

and on already open streams, use \f(CW\*(C`binmode()\*(C':

.Vb 1
    binmode(STDOUT, ":utf8");

    binmode(STDOUT, ":encoding(ucs2)");
    binmode(STDOUT, ":encoding(UTF-8)");
    binmode(STDOUT, ":encoding(shift_jis)");
.Ve

The matching of encoding names is loose: case does not matter, and
many encodings have several aliases.  Note that the \f(CW\*(C`:utf8\*(C' layer
must always be specified exactly like that; it is *not* subject to
the loose matching of encoding names. Also note that currently \f(CW\*(C`:utf8\*(C' is unsafe for
input, because it accepts the data without validating that it is indeed valid
\s-1UTF-8\s0; you should instead use \f(CW\*(C`:encoding(UTF-8)\*(C' (with or without a
hyphen).

See PerlIO for the \f(CW\*(C`:utf8\*(C' layer, PerlIO::encoding and
Encode::PerlIO for the \f(CW\*(C`:encoding()\*(C' layer, and
Encode::Supported for many encodings supported by the \f(CW\*(C`Encode\*(C'
module.

Reading in a file that you know happens to be encoded in one of the
Unicode or legacy encodings does not magically turn the data into
Unicode in Perl's eyes.  To do that, specify the appropriate
layer when opening files

.Vb 2
    open(my $fh,\*(Aq<:encoding(UTF-8)\*(Aq, \*(Aqanything\*(Aq);
    my $line_of_unicode = <$fh>;

    open(my $fh,\*(Aq<:encoding(Big5)\*(Aq, \*(Aqanything\*(Aq);
    my $line_of_unicode = <$fh>;
.Ve

The I/O layers can also be specified more flexibly with
the \f(CW\*(C`open\*(C' pragma.  See open, or look at the following example.

.Vb 8
    use open \*(Aq:encoding(UTF-8)\*(Aq; # input/output default encoding will be
                                 # UTF-8
    open X, ">file";
    print X chr(0x100), "\\n";
    close X;
    open Y, "<file";
    printf "%#x\\n", ord(<Y>); # this should print 0x100
    close Y;
.Ve

With the \f(CW\*(C`open\*(C' pragma you can use the \f(CW\*(C`:locale\*(C' layer

.Vb 10
    BEGIN \{ $ENV\{LC_ALL\} = $ENV\{LANG\} = \*(Aqru_RU.KOI8-R\*(Aq \}
    # the :locale will probe the locale environment variables like
    # LC_ALL
    use open OUT => \*(Aq:locale\*(Aq; # russki parusski
    open(O, ">koi8");
    print O chr(0x430); # Unicode CYRILLIC SMALL LETTER A = KOI8-R 0xc1
    close O;
    open(I, "<koi8");
    printf "%#x\\n", ord(<I>), "\\n"; # this should print 0xc1
    close I;
.Ve

These methods install a transparent filter on the I/O stream that
converts data from the specified encoding when it is read in from the
stream.  The result is always Unicode.

The open pragma affects all the \f(CW\*(C`open()\*(C' calls after the pragma by
setting default layers.  If you want to affect only certain
streams, use explicit layers directly in the \f(CW\*(C`open()\*(C' call.

You can switch encodings on an already opened stream by using
\f(CW\*(C`binmode()\*(C'; see \*(L"binmode\*(R" in perlfunc.

The \f(CW\*(C`:locale\*(C' does not currently work with
\f(CW\*(C`open()\*(C' and \f(CW\*(C`binmode()\*(C', only with the \f(CW\*(C`open\*(C' pragma.  The
\f(CW\*(C`:utf8\*(C' and \f(CW\*(C`:encoding(...)\*(C' methods do work with all of \f(CW\*(C`open()\*(C',
\f(CW\*(C`binmode()\*(C', and the \f(CW\*(C`open\*(C' pragma.

Similarly, you may use these I/O layers on output streams to
automatically convert Unicode to the specified encoding when it is
written to the stream. For example, the following snippet copies the
contents of the file \*(L"text.jis\*(R" (encoded as \s-1ISO-2022-JP,\s0 aka \s-1JIS\s0) to
the file \*(L"text.utf8\*(R", encoded as \s-1UTF-8:\s0

.Vb 3
    open(my $nihongo, \*(Aq<:encoding(iso-2022-jp)\*(Aq, \*(Aqtext.jis\*(Aq);
    open(my $unicode, \*(Aq>:utf8\*(Aq,                  \*(Aqtext.utf8\*(Aq);
    while (<$nihongo>) \{ print $unicode $_ \}
.Ve

The naming of encodings, both by the \f(CW\*(C`open()\*(C' and by the \f(CW\*(C`open\*(C'
pragma allows for flexible names: \f(CW\*(C`koi8-r\*(C' and \f(CW\*(C`KOI8R\*(C' will both be
understood.

Common encodings recognized by \s-1ISO, MIME, IANA,\s0 and various other
standardisation organisations are recognised; for a more detailed
list see Encode::Supported.

\f(CW\*(C`read()\*(C' reads characters and returns the number of characters.
\f(CW\*(C`seek()\*(C' and \f(CW\*(C`tell()\*(C' operate on byte counts, as does \f(CW\*(C`sysseek()\*(C'.

\f(CW\*(C`sysread()\*(C' and \f(CW\*(C`syswrite()\*(C' should not be used on file handles with
character encoding layers, they behave badly, and that behaviour has
been deprecated since perl 5.24.

Notice that because of the default behaviour of not doing any
conversion upon input if there is no default layer,
it is easy to mistakenly write code that keeps on expanding a file
by repeatedly encoding the data:

.Vb 8
    # BAD CODE WARNING
    open F, "file";
    local $/; ## read in the whole file of 8-bit characters
    $t = <F>;
    close F;
    open F, ">:encoding(UTF-8)", "file";
    print F $t; ## convert to UTF-8 on output
    close F;
.Ve

If you run this code twice, the contents of the *file* will be twice
\s-1UTF-8\s0 encoded.  A \f(CW\*(C`use open \*(Aq:encoding(UTF-8)\*(Aq\*(C' would have avoided the
bug, or explicitly opening also the *file* for input as \s-1UTF-8.\s0

**\s-1NOTE\s0**: the \f(CW\*(C`:utf8\*(C' and \f(CW\*(C`:encoding\*(C' features work only if your
Perl has been built with PerlIO, which is the default
on most systems.

### Displaying Unicode As Text

Subsection "Displaying Unicode As Text"
Sometimes you might want to display Perl scalars containing Unicode as
simple \s-1ASCII\s0 (or \s-1EBCDIC\s0) text.  The following subroutine converts
its argument so that Unicode characters with code points greater than
255 are displayed as \f(CW\*(C`\\x\{...\}\*(C', control characters (like \f(CW\*(C`\\n\*(C') are
displayed as \f(CW\*(C`\\x..\*(C', and the rest of the characters as themselves:

.Vb 9
 sub nice_string \{
        join("",
        map \{ $_ > 255                    # if wide character...
              ? sprintf("\\\\x\{%04X\}", $_)  # \\x\{...\}
              : chr($_) =~ /[[:cntrl:]]/  # else if control character...
                ? sprintf("\\\\x%02X", $_)  # \\x..
                : quotemeta(chr($_))      # else quoted or as themselves
        \} unpack("W*", $_[0]));           # unpack Unicode characters
   \}
.Ve

For example,

.Vb 1
   nice_string("foo\\x\{100\}bar\\n")
.Ve

returns the string

.Vb 1
   \*(Aqfoo\\x\{0100\}bar\\x0A\*(Aq
.Ve

which is ready to be printed.

(\f(CW\*(C`\\\\x\{\}\*(C' is used here instead of \f(CW\*(C`\\\\N\{\}\*(C', since it's most likely that
you want to see what the native values are.)

### Special Cases

Subsection "Special Cases"

- \(bu
Starting in Perl 5.28, it is illegal for bit operators, like \f(CW\*(C`~\*(C', to
operate on strings containing code points above 255.

- \(bu
The **vec()** function may produce surprising results if
used on strings containing characters with ordinal values above
255. In such a case, the results are consistent with the internal
encoding of the characters, but not with much else. So don't do
that, and starting in Perl 5.28, a deprecation message is issued if you
do so, becoming illegal in Perl 5.32.

- \(bu
Peeking At Perl's Internal Encoding
.Sp
Normal users of Perl should never care how Perl encodes any particular
Unicode string (because the normal ways to get at the contents of a
string with Unicode\*(--via input and output\*(--should always be via
explicitly-defined I/O layers). But if you must, there are two
ways of looking behind the scenes.
.Sp
One way of peeking inside the internal encoding of Unicode characters
is to use \f(CW\*(C`unpack("C*", ...\*(C' to get the bytes of whatever the string
encoding happens to be, or \f(CW\*(C`unpack("U0..", ...)\*(C' to get the bytes of the
\s-1UTF-8\s0 encoding:
.Sp
.Vb 2
    # this prints  c4 80  for the UTF-8 bytes 0xc4 0x80
    print join(" ", unpack("U0(H2)*", pack("U", 0x100))), "\\n";
.Ve
.Sp
Yet another way would be to use the Devel::Peek module:
.Sp
.Vb 1
    perl -MDevel::Peek -e \*(AqDump(chr(0x100))\*(Aq
.Ve
.Sp
That shows the \f(CW\*(C`UTF8\*(C' flag in \s-1FLAGS\s0 and both the \s-1UTF-8\s0 bytes
and Unicode characters in \f(CW\*(C`PV\*(C'.  See also later in this document
the discussion about the \f(CW\*(C`utf8::is_utf8()\*(C' function.

### Advanced Topics

Subsection "Advanced Topics"

- \(bu
String Equivalence
.Sp
The question of string equivalence turns somewhat complicated
in Unicode: what do you mean by \*(L"equal\*(R"?
.Sp
(Is \f(CW\*(C`LATIN CAPITAL LETTER A WITH ACUTE\*(C' equal to
\f(CW\*(C`LATIN CAPITAL LETTER A\*(C'?)
.Sp
The short answer is that by default Perl compares equivalence (\f(CW\*(C`eq\*(C',
\f(CW\*(C`ne\*(C') based only on code points of the characters.  In the above
case, the answer is no (because 0x00C1 != 0x0041).  But sometimes, any
\s-1CAPITAL LETTER A\s0's should be considered equal, or even A's of any case.
.Sp
The long answer is that you need to consider character normalization
and casing issues: see Unicode::Normalize, Unicode Technical Report #15,
Unicode Normalization Forms <https://www.unicode.org/unicode/reports/tr15> and
sections on case mapping in the Unicode Standard <https://www.unicode.org>.
.Sp
As of Perl 5.8.0, the \*(L"Full\*(R" case-folding of \fICase
Mappings/SpecialCasing is implemented, but bugs remain in \f(CW\*(C`qr//i\*(C' with them,
mostly fixed by 5.14, and essentially entirely by 5.18.

- \(bu
String Collation
.Sp
People like to see their strings nicely sorted\*(--or as Unicode
parlance goes, collated.  But again, what do you mean by collate?
.Sp
(Does \f(CW\*(C`LATIN CAPITAL LETTER A WITH ACUTE\*(C' come before or after
\f(CW\*(C`LATIN CAPITAL LETTER A WITH GRAVE\*(C'?)
.Sp
The short answer is that by default, Perl compares strings (\f(CW\*(C`lt\*(C',
\f(CW\*(C`le\*(C', \f(CW\*(C`cmp\*(C', \f(CW\*(C`ge\*(C', \f(CW\*(C`gt\*(C') based only on the code points of the
characters.  In the above case, the answer is \*(L"after\*(R", since
\f(CW0x00C1 > \f(CW0x00C0.
.Sp
The long answer is that \*(L"it depends\*(R", and a good answer cannot be
given without knowing (at the very least) the language context.
See Unicode::Collate, and *Unicode Collation Algorithm*
<https://www.unicode.org/unicode/reports/tr10/>

### Miscellaneous

Subsection "Miscellaneous"

- \(bu
Character Ranges and Classes
.Sp
Character ranges in regular expression bracketed character classes ( e.g.,
\f(CW\*(C`/[a-z]/\*(C') and in the \f(CW\*(C`tr///\*(C' (also known as \f(CW\*(C`y///\*(C') operator are not
magically Unicode-aware.  What this means is that \f(CW\*(C`[A-Za-z]\*(C' will not
magically start to mean \*(L"all alphabetic letters\*(R" (not that it does mean that
even for 8-bit characters; for those, if you are using locales (perllocale),
use \f(CW\*(C`/[[:alpha:]]/\*(C'; and if not, use the 8-bit-aware property \f(CW\*(C`\\p\{alpha\}\*(C').
.Sp
All the properties that begin with \f(CW\*(C`\\p\*(C' (and its inverse \f(CW\*(C`\\P\*(C') are actually
character classes that are Unicode-aware.  There are dozens of them, see
perluniprops.
.Sp
Starting in v5.22, you can use Unicode code points as the end points of
regular expression pattern character ranges, and the range will include
all Unicode code points that lie between those end points, inclusive.
.Sp
.Vb 1
 qr/ [ \\N\{U+03\} - \\N\{U+20\} ] /xx
.Ve
.Sp
includes the code points
\f(CW\*(C`\\N\{U+03\}\*(C', \f(CW\*(C`\\N\{U+04\}\*(C', ..., \f(CW\*(C`\\N\{U+20\}\*(C'.
.Sp
This also works for ranges in \f(CW\*(C`tr///\*(C' starting in Perl v5.24.

- \(bu
String-To-Number Conversions
.Sp
Unicode does define several other decimal\*(--and numeric\*(--characters
besides the familiar 0 to 9, such as the Arabic and Indic digits.
Perl does not support string-to-number conversion for digits other
than \s-1ASCII\s0 \f(CW0 to \f(CW9 (and \s-1ASCII\s0 \f(CW\*(C`a\*(C' to \f(CW\*(C`f\*(C' for hexadecimal).
To get safe conversions from any Unicode string, use
\*(L"**num()**\*(R" in Unicode::UCD.

### Questions With Answers

Subsection "Questions With Answers"

- \(bu
Will My Old Scripts Break?
.Sp
Very probably not.  Unless you are generating Unicode characters
somehow, old behaviour should be preserved.  About the only behaviour
that has changed and which could start generating Unicode is the old
behaviour of \f(CW\*(C`chr()\*(C' where supplying an argument more than 255
produced a character modulo 255.  \f(CW\*(C`chr(300)\*(C', for example, was equal
to \f(CW\*(C`chr(45)\*(C' or \*(L"-\*(R" (in \s-1ASCII\s0), now it is \s-1LATIN CAPITAL LETTER I WITH
BREVE.\s0

- \(bu
How Do I Make My Scripts Work With Unicode?
.Sp
Very little work should be needed since nothing changes until you
generate Unicode data.  The most important thing is getting input as
Unicode; for that, see the earlier I/O discussion.
To get full seamless Unicode support, add
\f(CW\*(C`use feature \*(Aqunicode_strings\*(Aq\*(C' (or \f(CW\*(C`use 5.012\*(C' or higher) to your
script.

- \(bu
How Do I Know Whether My String Is In Unicode?
.Sp
You shouldn't have to care.  But you may if your Perl is before 5.14.0
or you haven't specified \f(CW\*(C`use feature \*(Aqunicode_strings\*(Aq\*(C' or \f(CW\*(C`use
5.012\*(C' (or higher) because otherwise the rules for the code points
in the range 128 to 255 are different depending on
whether the string they are contained within is in Unicode or not.
(See \*(L"When Unicode Does Not Happen\*(R" in perlunicode.)
.Sp
To determine if a string is in Unicode, use:
.Sp
.Vb 1
    print utf8::is_utf8($string) ? 1 : 0, "\\n";
.Ve
.Sp
But note that this doesn't mean that any of the characters in the
string are necessary \s-1UTF-8\s0 encoded, or that any of the characters have
code points greater than 0xFF (255) or even 0x80 (128), or that the
string has any characters at all.  All the \f(CW\*(C`is_utf8()\*(C' does is to
return the value of the internal \*(L"utf8ness\*(R" flag attached to the
\f(CW$string.  If the flag is off, the bytes in the scalar are interpreted
as a single byte encoding.  If the flag is on, the bytes in the scalar
are interpreted as the (variable-length, potentially multi-byte) \s-1UTF-8\s0 encoded
code points of the characters.  Bytes added to a \s-1UTF-8\s0 encoded string are
automatically upgraded to \s-1UTF-8.\s0  If mixed non-UTF-8 and \s-1UTF-8\s0 scalars
are merged (double-quoted interpolation, explicit concatenation, or
printf/sprintf parameter substitution), the result will be \s-1UTF-8\s0 encoded
as if copies of the byte strings were upgraded to \s-1UTF-8:\s0 for example,
.Sp
.Vb 3
    $a = "ab\\x80c";
    $b = "\\x\{100\}";
    print "$a = $b\\n";
.Ve
.Sp
the output string will be UTF-8-encoded \f(CW\*(C`ab\\x80c = \\x\{100\}\\n\*(C', but
\f(CW$a will stay byte-encoded.
.Sp
Sometimes you might really need to know the byte length of a string
instead of the character length. For that use the \f(CW\*(C`bytes\*(C' pragma
and the \f(CW\*(C`length()\*(C' function:
.Sp
.Vb 6
    my $unicode = chr(0x100);
    print length($unicode), "\\n"; # will print 1
    use bytes;
    print length($unicode), "\\n"; # will print 2
                                  # (the 0xC4 0x80 of the UTF-8)
    no bytes;
.Ve

- \(bu
How Do I Find Out What Encoding a File Has?
.Sp
You might try Encode::Guess, but it has a number of limitations.

- \(bu
How Do I Detect Data That's Not Valid In a Particular Encoding?
.Sp
Use the \f(CW\*(C`Encode\*(C' package to try converting it.
For example,
.Sp
.Vb 1
    use Encode \*(Aqdecode\*(Aq;

    if (eval \{ decode(\*(AqUTF-8\*(Aq, $string, Encode::FB_CROAK); 1 \}) \{
        # $string is valid UTF-8
    \} else \{
        # $string is not valid UTF-8
    \}
.Ve
.Sp
Or use \f(CW\*(C`unpack\*(C' to try decoding it:
.Sp
.Vb 2
    use warnings;
    @chars = unpack("C0U*", $string_of_bytes_that_I_think_is_utf8);
.Ve
.Sp
If invalid, a \f(CW\*(C`Malformed UTF-8 character\*(C' warning is produced. The \*(L"C0\*(R" means
\*(L"process the string character per character\*(R".  Without that, the
\f(CW\*(C`unpack("U*", ...)\*(C' would work in \f(CW\*(C`U0\*(C' mode (the default if the format
string starts with \f(CW\*(C`U\*(C') and it would return the bytes making up the \s-1UTF-8\s0
encoding of the target string, something that will always work.

- \(bu
How Do I Convert Binary Data Into a Particular Encoding, Or Vice Versa?
.Sp
This probably isn't as useful as you might think.
Normally, you shouldn't need to.
.Sp
In one sense, what you are asking doesn't make much sense: encodings
are for characters, and binary data are not \*(L"characters\*(R", so converting
\*(L"data\*(R" into some encoding isn't meaningful unless you know in what
character set and encoding the binary data is in, in which case it's
not just binary data, now is it?
.Sp
If you have a raw sequence of bytes that you know should be
interpreted via a particular encoding, you can use \f(CW\*(C`Encode\*(C':
.Sp
.Vb 2
    use Encode \*(Aqfrom_to\*(Aq;
    from_to($data, "iso-8859-1", "UTF-8"); # from latin-1 to UTF-8
.Ve
.Sp
The call to \f(CW\*(C`from_to()\*(C' changes the bytes in \f(CW$data, but nothing
material about the nature of the string has changed as far as Perl is
concerned.  Both before and after the call, the string \f(CW$data
contains just a bunch of 8-bit bytes. As far as Perl is concerned,
the encoding of the string remains as \*(L"system-native 8-bit bytes\*(R".
.Sp
You might relate this to a fictional 'Translate' module:
.Sp
.Vb 4
   use Translate;
   my $phrase = "Yes";
   Translate::from_to($phrase, \*(Aqenglish\*(Aq, \*(Aqdeutsch\*(Aq);
   ## phrase now contains "Ja"
.Ve
.Sp
The contents of the string changes, but not the nature of the string.
Perl doesn't know any more after the call than before that the
contents of the string indicates the affirmative.
.Sp
Back to converting data.  If you have (or want) data in your system's
native 8-bit encoding (e.g. Latin-1, \s-1EBCDIC,\s0 etc.), you can use
pack/unpack to convert to/from Unicode.
.Sp
.Vb 2
    $native_string  = pack("W*", unpack("U*", $Unicode_string));
    $Unicode_string = pack("U*", unpack("W*", $native_string));
.Ve
.Sp
If you have a sequence of bytes you **know** is valid \s-1UTF-8,\s0
but Perl doesn't know it yet, you can make Perl a believer, too:
.Sp
.Vb 2
    $Unicode = $bytes;
    utf8::decode($Unicode);
.Ve
.Sp
or:
.Sp
.Vb 1
    $Unicode = pack("U0a*", $bytes);
.Ve
.Sp
You can find the bytes that make up a \s-1UTF-8\s0 sequence with
.Sp
.Vb 1
    @bytes = unpack("C*", $Unicode_string)
.Ve
.Sp
and you can create well-formed Unicode with
.Sp
.Vb 1
    $Unicode_string = pack("U*", 0xff, ...)
.Ve

- \(bu
How Do I Display Unicode?  How Do I Input Unicode?
.Sp
See <http://www.alanwood.net/unicode/> and
<http://www.cl.cam.ac.uk/~mgk25/unicode.html>

- \(bu
How Does Unicode Work With Traditional Locales?
.Sp
If your locale is a \s-1UTF-8\s0 locale, starting in Perl v5.26, Perl works
well for all categories; before this, starting with Perl v5.20, it works
for all categories but \f(CW\*(C`LC_COLLATE\*(C', which deals with
sorting and the \f(CW\*(C`cmp\*(C' operator.  But note that the standard
\f(CW\*(C`Unicode::Collate\*(C' and \f(CW\*(C`Unicode::Collate::Locale\*(C' modules offer
much more powerful solutions to collation issues, and work on earlier
releases.
.Sp
For other locales, starting in Perl 5.16, you can specify
.Sp
.Vb 1
    use locale \*(Aq:not_characters\*(Aq;
.Ve
.Sp
to get Perl to work well with them.  The catch is that you
have to translate from the locale character set to/from Unicode
yourself.  See \*(L"Unicode I/O\*(R" above for how to
.Sp
.Vb 1
    use open \*(Aq:locale\*(Aq;
.Ve
.Sp
to accomplish this, but full details are in \*(L"Unicode and
\s-1UTF-8\*(R"\s0 in perllocale, including gotchas that happen if you don't specify
\f(CW\*(C`:not_characters\*(C'.

### Hexadecimal Notation

Subsection "Hexadecimal Notation"
The Unicode standard prefers using hexadecimal notation because
that more clearly shows the division of Unicode into blocks of 256 characters.
Hexadecimal is also simply shorter than decimal.  You can use decimal
notation, too, but learning to use hexadecimal just makes life easier
with the Unicode standard.  The \f(CW\*(C`U+HHHH\*(C' notation uses hexadecimal,
for example.

The \f(CW\*(C`0x\*(C' prefix means a hexadecimal number, the digits are 0-9 *and*
a-f (or A-F, case doesn't matter).  Each hexadecimal digit represents
four bits, or half a byte.  \f(CW\*(C`print 0x..., "\\n"\*(C' will show a
hexadecimal number in decimal, and \f(CW\*(C`printf "%x\\n", $decimal\*(C' will
show a decimal number in hexadecimal.  If you have just the
\*(L"hex digits\*(R" of a hexadecimal number, you can use the \f(CW\*(C`hex()\*(C' function.

.Vb 6
    print 0x0009, "\\n";    # 9
    print 0x000a, "\\n";    # 10
    print 0x000f, "\\n";    # 15
    print 0x0010, "\\n";    # 16
    print 0x0011, "\\n";    # 17
    print 0x0100, "\\n";    # 256

    print 0x0041, "\\n";    # 65

    printf "%x\\n",  65;    # 41
    printf "%#x\\n", 65;    # 0x41

    print hex("41"), "\\n"; # 65
.Ve

### Further Resources

Subsection "Further Resources"

- \(bu
Unicode Consortium
.Sp
<https://www.unicode.org/>

- \(bu
Unicode \s-1FAQ\s0
.Sp
<https://www.unicode.org/unicode/faq/>

- \(bu
Unicode Glossary
.Sp
<https://www.unicode.org/glossary/>

- \(bu
Unicode Recommended Reading List
.Sp
The Unicode Consortium has a list of articles and books, some of which
give a much more in depth treatment of Unicode:
<http://unicode.org/resources/readinglist.html>

- \(bu
Unicode Useful Resources
.Sp
<https://www.unicode.org/unicode/onlinedat/resources.html>

- \(bu
Unicode and Multilingual Support in \s-1HTML,\s0 Fonts, Web Browsers and Other Applications
.Sp
<http://www.alanwood.net/unicode/>

- \(bu
\s-1UTF-8\s0 and Unicode \s-1FAQ\s0 for Unix/Linux
.Sp
<http://www.cl.cam.ac.uk/~mgk25/unicode.html>

- \(bu
Legacy Character Sets
.Sp
<http://www.czyborra.com/>
<http://www.eki.ee/letter/>

- \(bu
You can explore various information from the Unicode data files using
the \f(CW\*(C`Unicode::UCD\*(C' module.

## UNICODE IN OLDER PERLS

Header "UNICODE IN OLDER PERLS"
If you cannot upgrade your Perl to 5.8.0 or later, you can still
do some Unicode processing by using the modules \f(CW\*(C`Unicode::String\*(C',
\f(CW\*(C`Unicode::Map8\*(C', and \f(CW\*(C`Unicode::Map\*(C', available from \s-1CPAN.\s0
If you have the \s-1GNU\s0 recode installed, you can also use the
Perl front-end \f(CW\*(C`Convert::Recode\*(C' for character conversions.

The following are fast conversions from \s-1ISO 8859-1\s0 (Latin-1) bytes
to \s-1UTF-8\s0 bytes and back, the code works even with older Perl 5 versions.

.Vb 2
    # ISO 8859-1 to UTF-8
    s/([\\x80-\\xFF])/chr(0xC0|ord($1)>>6).chr(0x80|ord($1)&0x3F)/eg;

    # UTF-8 to ISO 8859-1
    s/([\\xC2\\xC3])([\\x80-\\xBF])/chr(ord($1)<<6&0xC0|ord($2)&0x3F)/eg;
.Ve

## SEE ALSO

Header "SEE ALSO"
perlunitut, perlunicode, Encode, open, utf8, bytes,
perlretut, perlrun, Unicode::Collate, Unicode::Normalize,
Unicode::UCD

## ACKNOWLEDGMENTS

Header "ACKNOWLEDGMENTS"
Thanks to the kind readers of the perl5-porters@perl.org,
perl-unicode@perl.org, linux-utf8@nl.linux.org, and unicore@unicode.org
mailing lists for their valuable feedback.

## AUTHOR, COPYRIGHT, AND LICENSE

Header "AUTHOR, COPYRIGHT, AND LICENSE"
Copyright 2001-2011 Jarkko Hietaniemi <jhi@iki.fi>.
Now maintained by Perl 5 Porters.

This document may be distributed under the same terms as Perl itself.
