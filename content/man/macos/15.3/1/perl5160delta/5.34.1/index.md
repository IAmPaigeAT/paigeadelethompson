+++
manpage_format = "troff"
description = "This document describes differences between the 5.14.0 release and the 5.16.0 release. If you are upgrading from an earlier release such as 5.12.0, first read perl5140delta, which describes differences between 5.12.0 and 5.14.0. Some bug fixes in..."
operating_system = "macos"
title = "perl5160delta(1)"
operating_system_version = "15.3"
manpage_section = "1"
date = "2022-02-19"
detected_package_version = "5.34.1"
manpage_name = "perl5160delta"
author = "None Specified"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5160DELTA 1"
PERL5160DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5160delta - what is new for perl v5.16.0

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.14.0 release and
the 5.16.0 release.

If you are upgrading from an earlier release such as 5.12.0, first read
perl5140delta, which describes differences between 5.12.0 and
5.14.0.

Some bug fixes in this release have been backported to later
releases of 5.14.x.  Those are indicated with the 5.14.x version in
parentheses.

## Notice

Header "Notice"
With the release of Perl 5.16.0, the 5.12.x series of releases is now out of
its support period.  There may be future 5.12.x releases, but only in the
event of a critical security issue.  Users of Perl 5.12 or earlier should
consider upgrading to a more recent release of Perl.

This policy is described in greater detail in
perlpolicy.

## Core Enhancements

Header "Core Enhancements"
.ie n .SS """use \f(CIVERSION"""
.el .SS "\f(CWuse \f(CIVERSION\f(CW"
Subsection "use VERSION"
As of this release, version declarations like \f(CW\*(C`use v5.16\*(C' now disable
all features before enabling the new feature bundle.  This means that
the following holds true:

.Vb 4
    use 5.016;
    # only 5.16 features enabled here
    use 5.014;
    # only 5.14 features enabled here (not 5.16)
.Ve

\f(CW\*(C`use v5.12\*(C' and higher continue to enable strict, but explicit \f(CW\*(C`use
strict\*(C' and \f(CW\*(C`no strict\*(C' now override the version declaration, even
when they come first:

.Vb 3
    no strict;
    use 5.012;
    # no strict here
.Ve

There is a new \*(L":default\*(R" feature bundle that represents the set of
features enabled before any version declaration or \f(CW\*(C`use feature\*(C' has
been seen.  Version declarations below 5.10 now enable the \*(L":default\*(R"
feature set.  This does not actually change the behavior of \f(CW\*(C`use
v5.8\*(C', because features added to the \*(L":default\*(R" set are those that were
traditionally enabled by default, before they could be turned off.

\f(CW\*(C`no feature\*(C' now resets to the default feature set.  To disable all
features (which is likely to be a pretty special-purpose request, since
it presumably won't match any named set of semantics) you can now
write \f(CW\*(C`no feature \*(Aq:all\*(Aq\*(C'.

\f(CW$[ is now disabled under \f(CW\*(C`use v5.16\*(C'.  It is part of the default
feature set and can be turned on or off explicitly with \f(CW\*(C`use feature
\*(Aqarray_base\*(Aq\*(C'.
.ie n .SS """_\|_SUB_\|_"""
.el .SS "\f(CW_\|_SUB_\|_"
Subsection "__SUB__"
The new \f(CW\*(C`_\|_SUB_\|_\*(C' token, available under the \f(CW\*(C`current_sub\*(C' feature
(see feature) or \f(CW\*(C`use v5.16\*(C', returns a reference to the current
subroutine, making it easier to write recursive closures.

### New and Improved Built-ins

Subsection "New and Improved Built-ins"
*More consistent \f(CI\*(C`eval\*(C'\fI*
Subsection "More consistent eval"

The \f(CW\*(C`eval\*(C' operator sometimes treats a string argument as a sequence of
characters and sometimes as a sequence of bytes, depending on the
internal encoding.  The internal encoding is not supposed to make any
difference, but there is code that relies on this inconsistency.

The new \f(CW\*(C`unicode_eval\*(C' and \f(CW\*(C`evalbytes\*(C' features (enabled under \f(CW\*(C`use
5.16.0\*(C') resolve this.  The \f(CW\*(C`unicode_eval\*(C' feature causes \f(CW\*(C`eval
$string\*(C' to treat the string always as Unicode.  The \f(CW\*(C`evalbytes\*(C'
features provides a function, itself called \f(CW\*(C`evalbytes\*(C', which
evaluates its argument always as a string of bytes.

These features also fix oddities with source filters leaking to outer
dynamic scopes.

See feature for more detail.

*\f(CI\*(C`substr\*(C'\fI lvalue revamp*
Subsection "substr lvalue revamp"

When \f(CW\*(C`substr\*(C' is called in lvalue or potential lvalue context with two
or three arguments, a special lvalue scalar is returned that modifies
the original string (the first argument) when assigned to.

Previously, the offsets (the second and third arguments) passed to
\f(CW\*(C`substr\*(C' would be converted immediately to match the string, negative
offsets being translated to positive and offsets beyond the end of the
string being truncated.

Now, the offsets are recorded without modification in the special
lvalue scalar that is returned, and the original string is not even
looked at by \f(CW\*(C`substr\*(C' itself, but only when the returned lvalue is
read or modified.

These changes result in an incompatible change:

If the original string changes length after the call to \f(CW\*(C`substr\*(C' but
before assignment to its return value, negative offsets will remember
their position from the end of the string, affecting code like this:

.Vb 5
    my $string = "string";
    my $lvalue = \\substr $string, -4, 2;
    print $$lvalue, "\\n"; # prints "ri"
    $string = "bailing twine";
    print $$lvalue, "\\n"; # prints "wi"; used to print "il"
.Ve

The same thing happens with an omitted third argument.  The returned
lvalue will always extend to the end of the string, even if the string
becomes longer.

Since this change also allowed many bugs to be fixed (see
"The \f(CW\*(C`substr\*(C' operator"), and since the behavior
of negative offsets has never been specified, the
change was deemed acceptable.

*Return value of \f(CI\*(C`tied\*(C'\fI*
Subsection "Return value of tied"

The value returned by \f(CW\*(C`tied\*(C' on a tied variable is now the actual
scalar that holds the object to which the variable is tied.  This
lets ties be weakened with \f(CW\*(C`Scalar::Util::weaken(tied
$tied_variable)\*(C'.

### Unicode Support

Subsection "Unicode Support"
*Supports (\fIalmost\fI) Unicode 6.1*
Subsection "Supports (almost) Unicode 6.1"

Besides the addition of whole new scripts, and new characters in
existing scripts, this new version of Unicode, as always, makes some
changes to existing characters.  One change that may trip up some
applications is that the General Category of two characters in the
Latin-1 range, \s-1PILCROW SIGN\s0 and \s-1SECTION SIGN,\s0 has been changed from
Other_Symbol to Other_Punctuation.  The same change has been made for
a character in each of Tibetan, Ethiopic, and Aegean.
The code points U+3248..U+324F (\s-1CIRCLED NUMBER TEN ON BLACK SQUARE\s0
through \s-1CIRCLED NUMBER EIGHTY ON BLACK SQUARE\s0) have had their General
Category changed from Other_Symbol to Other_Numeric.  The Line Break
property has changes for Hebrew and Japanese; and because of
other changes in 6.1, the Perl regular expression construct \f(CW\*(C`\\X\*(C' now
works differently for some characters in Thai and Lao.

New aliases (synonyms) have been defined for many property values;
these, along with the previously existing ones, are all cross-indexed in
perluniprops.

The return value of \f(CW\*(C`charnames::viacode()\*(C' is affected by other
changes:

.Vb 10
 Code point      Old Name             New Name
   U+000A    LINE FEED (LF)        LINE FEED
   U+000C    FORM FEED (FF)        FORM FEED
   U+000D    CARRIAGE RETURN (CR)  CARRIAGE RETURN
   U+0085    NEXT LINE (NEL)       NEXT LINE
   U+008E    SINGLE-SHIFT 2        SINGLE-SHIFT-2
   U+008F    SINGLE-SHIFT 3        SINGLE-SHIFT-3
   U+0091    PRIVATE USE 1         PRIVATE USE-1
   U+0092    PRIVATE USE 2         PRIVATE USE-2
   U+2118    SCRIPT CAPITAL P      WEIERSTRASS ELLIPTIC FUNCTION
.Ve

Perl will accept any of these names as input, but
\f(CW\*(C`charnames::viacode()\*(C' now returns the new name of each pair.  The
change for U+2118 is considered by Unicode to be a correction, that is
the original name was a mistake (but again, it will remain forever valid
to use it to refer to U+2118).  But most of these changes are the
fallout of the mistake Unicode 6.0 made in naming a character used in
Japanese cell phones to be \*(L"\s-1BELL\*(R",\s0 which conflicts with the longstanding
industry use of (and Unicode's recommendation to use) that name
to mean the \s-1ASCII\s0 control character at U+0007.  Therefore, that name
has been deprecated in Perl since v5.14, and any use of it will raise a
warning message (unless turned off).  The name \*(L"\s-1ALERT\*(R"\s0 is now the
preferred name for this code point, with \*(L"\s-1BEL\*(R"\s0 an acceptable short
form.  The name for the new cell phone character, at code point U+1F514,
remains undefined in this version of Perl (hence we don't
implement quite all of Unicode 6.1), but starting in v5.18, \s-1BELL\s0 will mean
this character, and not U+0007.

Unicode has taken steps to make sure that this sort of mistake does not
happen again.  The Standard now includes all generally accepted
names and abbreviations for control characters, whereas previously it
didn't (though there were recommended names for most of them, which Perl
used).  This means that most of those recommended names are now
officially in the Standard.  Unicode did not recommend names for the
four code points listed above between U+008E and U+008F, and in
standardizing them Unicode subtly changed the names that Perl had
previously given them, by replacing the final blank in each name by a
hyphen.  Unicode also officially accepts names that Perl had deprecated,
such as \s-1FILE SEPARATOR.\s0  Now the only deprecated name is \s-1BELL.\s0
Finally, Perl now uses the new official names instead of the old
(now considered obsolete) names for the first four code points in the
list above (the ones which have the parentheses in them).

Now that the names have been placed in the Unicode standard, these kinds
of changes should not happen again, though corrections, such as to
U+2118, are still possible.

Unicode also added some name abbreviations, which Perl now accepts:
\s-1SP\s0 for \s-1SPACE\s0;
\s-1TAB\s0 for \s-1CHARACTER TABULATION\s0;
\s-1NEW LINE, END OF LINE, NL,\s0 and \s-1EOL\s0 for \s-1LINE FEED\s0;
LOCKING-SHIFT \s-1ONE\s0 for \s-1SHIFT OUT\s0;
LOCKING-SHIFT \s-1ZERO\s0 for \s-1SHIFT IN\s0;
and \s-1ZWNBSP\s0 for \s-1ZERO WIDTH\s0 NO-BREAK \s-1SPACE.\s0

More details on this version of Unicode are provided in
<http://www.unicode.org/versions/Unicode6.1.0/>.

*\f(CI\*(C`use charnames\*(C'\fI is no longer needed for \f(CI\*(C`\\N\{\f(CIname\f(CI\}\*(C'\fI*
Subsection "use charnames is no longer needed for N\{name\}"

When \f(CW\*(C`\\N\{\f(CIname\f(CW\}\*(C' is encountered, the \f(CW\*(C`charnames\*(C' module is now
automatically loaded when needed as if the \f(CW\*(C`:full\*(C' and \f(CW\*(C`:short\*(C'
options had been specified.  See charnames for more information.

*\f(CI\*(C`\\N\{...\}\*(C'\fI can now have Unicode loose name matching*
Subsection "N\{...\} can now have Unicode loose name matching"

This is described in the \f(CW\*(C`charnames\*(C' item in
\*(L"Updated Modules and Pragmata\*(R" below.

*Unicode Symbol Names*
Subsection "Unicode Symbol Names"

Perl now has proper support for Unicode in symbol names.  It used to be
that \f(CW\*(C`*\{$foo\}\*(C' would ignore the internal \s-1UTF8\s0 flag and use the bytes of
the underlying representation to look up the symbol.  That meant that
\f(CW\*(C`*\{"\\x\{100\}"\}\*(C' and \f(CW\*(C`*\{"\\xc4\\x80"\}\*(C' would return the same thing.  All
these parts of Perl have been fixed to account for Unicode:

- \(bu
Method names (including those passed to \f(CW\*(C`use overload\*(C')

- \(bu
Typeglob names (including names of variables, subroutines, and filehandles)

- \(bu
Package names

- \(bu
\f(CW\*(C`goto\*(C'

- \(bu
Symbolic dereferencing

- \(bu
Second argument to \f(CW\*(C`bless()\*(C' and \f(CW\*(C`tie()\*(C'

- \(bu
Return value of \f(CW\*(C`ref()\*(C'

- \(bu
Subroutine prototypes

- \(bu
Attributes

- \(bu
Various warnings and error messages that mention variable names or values,
methods, etc.

In addition, a parsing bug has been fixed that prevented \f(CW\*(C`*\{e\*'\}\*(C' from
implicitly quoting the name, but instead interpreted it as \f(CW\*(C`*\{+e\*'\}\*(C', which
would cause a strict violation.

\f(CW\*(C`*\{"*a::b"\}\*(C' automatically strips off the * if it is followed by an \s-1ASCII\s0
letter.  That has been extended to all Unicode identifier characters.

One-character non-ASCII non-punctuation variables (like \f(CW\*(C`$e\*'\*(C') are now
subject to \*(L"Used only once\*(R" warnings.  They used to be exempt, as they
were treated as punctuation variables.

Also, single-character Unicode punctuation variables (like \f(CW$X) are now
supported [perl #69032].

*Improved ability to mix locales and Unicode, including \s-1UTF-8\s0 locales*
Subsection "Improved ability to mix locales and Unicode, including UTF-8 locales"

An optional parameter has been added to \f(CW\*(C`use locale\*(C'

.Vb 1
 use locale \*(Aq:not_characters\*(Aq;
.Ve

which tells Perl to use all but the \f(CW\*(C`LC_CTYPE\*(C' and \f(CW\*(C`LC_COLLATE\*(C'
portions of the current locale.  Instead, the character set is assumed
to be Unicode.  This lets locales and Unicode be seamlessly mixed,
including the increasingly frequent \s-1UTF-8\s0 locales.  When using this
hybrid form of locales, the \f(CW\*(C`:locale\*(C' layer to the open pragma can
be used to interface with the file system, and there are \s-1CPAN\s0 modules
available for \s-1ARGV\s0 and environment variable conversions.

Full details are in perllocale.

*New function \f(CI\*(C`fc\*(C'\fI and corresponding escape sequence \f(CI\*(C`\\F\*(C'\fI for Unicode foldcase*
Subsection "New function fc and corresponding escape sequence F for Unicode foldcase"

Unicode foldcase is an extension to lowercase that gives better results
when comparing two strings case-insensitively.  It has long been used
internally in regular expression \f(CW\*(C`/i\*(C' matching.  Now it is available
explicitly through the new \f(CW\*(C`fc\*(C' function call (enabled by
\f(CW"use\ feature\ \*(Aqfc\*(Aq", or \f(CW\*(C`use v5.16\*(C', or explicitly callable via
\f(CW\*(C`CORE::fc\*(C') or through the new \f(CW\*(C`\\F\*(C' sequence in double-quotish
strings.

Full details are in \*(L"fc\*(R" in perlfunc.

*The Unicode \f(CI\*(C`Script_Extensions\*(C'\fI property is now supported.*
Subsection "The Unicode Script_Extensions property is now supported."

New in Unicode 6.0, this is an improved \f(CW\*(C`Script\*(C' property.  Details
are in \*(L"Scripts\*(R" in perlunicode.

### \s-1XS\s0 Changes

Subsection "XS Changes"
*Improved typemaps for Some Builtin Types*
Subsection "Improved typemaps for Some Builtin Types"

Most \s-1XS\s0 authors will know there is a longstanding bug in the
\s-1OUTPUT\s0 typemap for T_AVREF (\f(CW\*(C`AV*\*(C'), T_HVREF (\f(CW\*(C`HV*\*(C'), T_CVREF (\f(CW\*(C`CV*\*(C'),
and T_SVREF (\f(CW\*(C`SVREF\*(C' or \f(CW\*(C`\\$foo\*(C') that requires manually decrementing
the reference count of the return value instead of the typemap taking
care of this.  For backwards-compatibility, this cannot be changed in the
default typemaps.  But we now provide additional typemaps
\f(CW\*(C`T_AVREF_REFCOUNT_FIXED\*(C', etc. that do not exhibit this bug.  Using
them in your extension is as simple as having one line in your
\f(CW\*(C`TYPEMAP\*(C' section:

.Vb 1
  HV*   T_HVREF_REFCOUNT_FIXED
.Ve

*\f(CI\*(C`is_utf8_char()\*(C'\fI*
Subsection "is_utf8_char()"

The XS-callable function \f(CW\*(C`is_utf8_char()\*(C', when presented with
malformed \s-1UTF-8\s0 input, can read up to 12 bytes beyond the end of the
string.  This cannot be fixed without changing its \s-1API,\s0 and so its
use is now deprecated.  Use \f(CW\*(C`is_utf8_char_buf()\*(C' (described just below)
instead.

*Added \f(CI\*(C`is_utf8_char_buf()\*(C'\fI*
Subsection "Added is_utf8_char_buf()"

This function is designed to replace the deprecated \*(L"**is_utf8_char()**\*(R"
function.  It includes an extra parameter to make sure it doesn't read
past the end of the input buffer.

*Other \f(CI\*(C`is_utf8_foo()\*(C'\fI functions, as well as \f(CI\*(C`utf8_to_foo()\*(C'\fI, etc.*
Subsection "Other is_utf8_foo() functions, as well as utf8_to_foo(), etc."

Most other XS-callable functions that take \s-1UTF-8\s0 encoded input
implicitly assume that the \s-1UTF-8\s0 is valid (not malformed) with respect to
buffer length.  Do not do things such as change a character's case or
see if it is alphanumeric without first being sure that it is valid
\s-1UTF-8.\s0  This can be safely done for a whole string by using one of the
functions \f(CW\*(C`is_utf8_string()\*(C', \f(CW\*(C`is_utf8_string_loc()\*(C', and
\f(CW\*(C`is_utf8_string_loclen()\*(C'.

*New Pad \s-1API\s0*
Subsection "New Pad API"

Many new functions have been added to the \s-1API\s0 for manipulating lexical
pads.  See \*(L"Pad Data Structures\*(R" in perlapi for more information.

### Changes to Special Variables

Subsection "Changes to Special Variables"
*\f(CI$$\fI can be assigned to*
Subsection "$$ can be assigned to"

\f(CW$$ was made read-only in Perl 5.8.0.  But only sometimes: \f(CW\*(C`local $$\*(C'
would make it writable again.  Some \s-1CPAN\s0 modules were using \f(CW\*(C`local $$\*(C' or
\s-1XS\s0 code to bypass the read-only check, so there is no reason to keep \f(CW$$
read-only.  (This change also allowed a bug to be fixed while maintaining
backward compatibility.)

*\f(CI$^X\fI converted to an absolute path on FreeBSD, \s-1OS X\s0 and Solaris*
Subsection "$^X converted to an absolute path on FreeBSD, OS X and Solaris"

\f(CW$^X is now converted to an absolute path on \s-1OS X,\s0 FreeBSD (without
needing */proc* mounted) and Solaris 10 and 11.  This augments the
previous approach of using */proc* on Linux, FreeBSD, and NetBSD
(in all cases, where mounted).

This makes relocatable perl installations more useful on these platforms.
(See \*(L"Relocatable \f(CW@INC\*(R" in *\s-1INSTALL\s0*)

### Debugger Changes

Subsection "Debugger Changes"
*Features inside the debugger*
Subsection "Features inside the debugger"

The current Perl's feature bundle is now enabled for commands entered
in the interactive debugger.

*New option for the debugger's \f(BIt\fI command*
Subsection "New option for the debugger's t command"

The **t** command in the debugger, which toggles tracing mode, now
accepts a numeric argument that determines how many levels of subroutine
calls to trace.

*\f(CI\*(C`enable\*(C'\fI and \f(CI\*(C`disable\*(C'\fI*
Subsection "enable and disable"

The debugger now has \f(CW\*(C`disable\*(C' and \f(CW\*(C`enable\*(C' commands for disabling
existing breakpoints and re-enabling them.  See perldebug.

*Breakpoints with file names*
Subsection "Breakpoints with file names"

The debugger's \*(L"b\*(R" command for setting breakpoints now lets a line
number be prefixed with a file name.  See
\*(L"b [file]:[line] [condition]\*(R" in perldebug.
.ie n .SS "The ""CORE"" Namespace"
.el .SS "The \f(CWCORE Namespace"
Subsection "The CORE Namespace"
*The \f(CI\*(C`CORE::\*(C'\fI prefix*
Subsection "The CORE:: prefix"

The \f(CW\*(C`CORE::\*(C' prefix can now be used on keywords enabled by
feature.pm, even outside the scope of \f(CW\*(C`use feature\*(C'.

*Subroutines in the \f(CI\*(C`CORE\*(C'\fI namespace*
Subsection "Subroutines in the CORE namespace"

Many Perl keywords are now available as subroutines in the \s-1CORE\s0 namespace.
This lets them be aliased:

.Vb 2
    BEGIN \{ *entangle = \CORE::tie \}
    entangle $variable, $package, @args;
.Ve

And for prototypes to be bypassed:

.Vb 5
    sub mytie(\\[%$*@]$@) \{
        my ($ref, $pack, @args) = @_;
        ... do something ...
        goto &CORE::tie;
    \}
.Ve

Some of these cannot be called through references or via \f(CW&foo syntax,
but must be called as barewords.

See \s-1CORE\s0 for details.

### Other Changes

Subsection "Other Changes"
*Anonymous handles*
Subsection "Anonymous handles"

Automatically generated file handles are now named _\|_ANONIO_\|_ when the
variable name cannot be determined, rather than \f(CW$_\|_ANONIO_\|_.

*Autoloaded sort Subroutines*
Subsection "Autoloaded sort Subroutines"

Custom sort subroutines can now be autoloaded [perl #30661]:

.Vb 2
    sub AUTOLOAD \{ ... \}
    @sorted = sort foo @list; # uses AUTOLOAD
.Ve

*\f(CI\*(C`continue\*(C'\fI no longer requires the \*(L"switch\*(R" feature*
Subsection "continue no longer requires the switch feature"

The \f(CW\*(C`continue\*(C' keyword has two meanings.  It can introduce a \f(CW\*(C`continue\*(C'
block after a loop, or it can exit the current \f(CW\*(C`when\*(C' block.  Up to now,
the latter meaning was valid only with the \*(L"switch\*(R" feature enabled, and
was a syntax error otherwise.  Since the main purpose of feature.pm is to
avoid conflicts with user-defined subroutines, there is no reason for
\f(CW\*(C`continue\*(C' to depend on it.

*DTrace probes for interpreter phase change*
Subsection "DTrace probes for interpreter phase change"

The \f(CW\*(C`phase-change\*(C' probes will fire when the interpreter's phase
changes, which tracks the \f(CW\*(C`$\{^GLOBAL_PHASE\}\*(C' variable.  \f(CW\*(C`arg0\*(C' is
the new phase name; \f(CW\*(C`arg1\*(C' is the old one.  This is useful
for limiting your instrumentation to one or more of: compile time,
run time, or destruct time.

*\f(CI\*(C`_\|_FILE_\|_()\*(C'\fI Syntax*
Subsection "__FILE__() Syntax"

The \f(CW\*(C`_\|_FILE_\|_\*(C', \f(CW\*(C`_\|_LINE_\|_\*(C' and \f(CW\*(C`_\|_PACKAGE_\|_\*(C' tokens can now be written
with an empty pair of parentheses after them.  This makes them parse the
same way as \f(CW\*(C`time\*(C', \f(CW\*(C`fork\*(C' and other built-in functions.

*The \f(CI\*(C`\\$\*(C'\fI prototype accepts any scalar lvalue*
Subsection "The $ prototype accepts any scalar lvalue"

The \f(CW\*(C`\\$\*(C' and \f(CW\*(C`\\[$]\*(C' subroutine prototypes now accept any scalar lvalue
argument.  Previously they accepted only scalars beginning with \f(CW\*(C`$\*(C' and
hash and array elements.  This change makes them consistent with the way
the built-in \f(CW\*(C`read\*(C' and \f(CW\*(C`recv\*(C' functions (among others) parse their
arguments.  This means that one can override the built-in functions with
custom subroutines that parse their arguments the same way.

*\f(CI\*(C`_\*(C'\fI in subroutine prototypes*
Subsection "_ in subroutine prototypes"

The \f(CW\*(C`_\*(C' character in subroutine prototypes is now allowed before \f(CW\*(C`@\*(C' or
\f(CW\*(C`%\*(C'.

## Security

Header "Security"
.ie n .SS "Use ""is_utf8_char_buf()"" and not ""is_utf8_char()"""
.el .SS "Use \f(CWis_utf8_char_buf() and not \f(CWis_utf8_char()"
Subsection "Use is_utf8_char_buf() and not is_utf8_char()"
The latter function is now deprecated because its \s-1API\s0 is insufficient to
guarantee that it doesn't read (up to 12 bytes in the worst case) beyond
the end of its input string.  See
**is_utf8_char_buf()**.

### Malformed \s-1UTF-8\s0 input could cause attempts to read beyond the end of the buffer

Subsection "Malformed UTF-8 input could cause attempts to read beyond the end of the buffer"
Two new XS-accessible functions, \f(CW\*(C`utf8_to_uvchr_buf()\*(C' and
\f(CW\*(C`utf8_to_uvuni_buf()\*(C' are now available to prevent this, and the Perl
core has been converted to use them.
See \*(L"Internal Changes\*(R".
.ie n .SS """File::Glob::bsd_glob()"" memory error with \s-1GLOB_ALTDIRFUNC\s0 (\s-1CVE-2011-2728\s0)."
.el .SS "\f(CWFile::Glob::bsd_glob() memory error with \s-1GLOB_ALTDIRFUNC\s0 (\s-1CVE-2011-2728\s0)."
Subsection "File::Glob::bsd_glob() memory error with GLOB_ALTDIRFUNC (CVE-2011-2728)."
Calling \f(CW\*(C`File::Glob::bsd_glob\*(C' with the unsupported flag
\s-1GLOB_ALTDIRFUNC\s0 would cause an access violation / segfault.  A Perl
program that accepts a flags value from an external source could expose
itself to denial of service or arbitrary code execution attacks.  There
are no known exploits in the wild.  The problem has been corrected by
explicitly disabling all unsupported flags and setting unused function
pointers to null.  Bug reported by Cle\*'ment Lecigne. (5.14.2)
.ie n .SS "Privileges are now set correctly when assigning to $("
.el .SS "Privileges are now set correctly when assigning to \f(CW$("
Subsection "Privileges are now set correctly when assigning to $("
A hypothetical bug (probably unexploitable in practice) because the
incorrect setting of the effective group \s-1ID\s0 while setting \f(CW$( has been
fixed.  The bug would have affected only systems that have \f(CW\*(C`setresgid()\*(C'
but not \f(CW\*(C`setregid()\*(C', but no such systems are known to exist.

## Deprecations

Header "Deprecations"

### Dont read the Unicode data base files in \fIlib/unicore

Subsection "Don't read the Unicode data base files in lib/unicore"
It is now deprecated to directly read the Unicode data base files.
These are stored in the *lib/unicore* directory.  Instead, you should
use the new functions in Unicode::UCD.  These provide a stable \s-1API,\s0
and give complete information.

Perl may at some point in the future change or remove these files.  The
file which applications were most likely to have used is
*lib/unicore/ToDigit.pl*.  \*(L"**prop_invmap()**\*(R" in Unicode::UCD can be used to
get at its data instead.
.ie n .SS "\s-1XS\s0 functions ""is_utf8_char()"", ""utf8_to_uvchr()"" and ""utf8_to_uvuni()"""
.el .SS "\s-1XS\s0 functions \f(CWis_utf8_char(), \f(CWutf8_to_uvchr() and \f(CWutf8_to_uvuni()"
Subsection "XS functions is_utf8_char(), utf8_to_uvchr() and utf8_to_uvuni()"
This function is deprecated because it could read beyond the end of the
input string.  Use the new **is_utf8_char_buf()**,
\f(CW\*(C`utf8_to_uvchr_buf()\*(C' and \f(CW\*(C`utf8_to_uvuni_buf()\*(C' instead.

## Future Deprecations

Header "Future Deprecations"
This section serves as a notice of features that are *likely* to be
removed or deprecated in the next release of
perl (5.18.0).  If your code depends on these features, you should
contact the Perl 5 Porters via the mailing
list <http://lists.perl.org/list/perl5-porters.html> or perlbug to
explain your use case and inform the deprecation process.

### Core Modules

Subsection "Core Modules"
These modules may be marked as deprecated *from the core*.  This only
means that they will no longer be installed by default with the core
distribution, but will remain available on the \s-1CPAN.\s0

- \(bu
\s-1CPANPLUS\s0

- \(bu
Filter::Simple

- \(bu
PerlIO::mmap

- \(bu
Pod::LaTeX

- \(bu
Pod::Parser

- \(bu
SelfLoader

- \(bu
Text::Soundex

- \(bu
Thread.pm

### Platforms with no supporting programmers

Subsection "Platforms with no supporting programmers"
These platforms will probably have their
special build support removed during the
5.17.0 development series.

- \(bu
BeOS

- \(bu
djgpp

- \(bu
dgux

- \(bu
\s-1EPOC\s0

- \(bu
MPE/iX

- \(bu
Rhapsody

- \(bu
\s-1UTS\s0

- \(bu
\s-1VM/ESA\s0

### Other Future Deprecations

Subsection "Other Future Deprecations"

- \(bu
Swapping of $< and $>
.Sp
For more information about this future deprecation, see the relevant \s-1RT\s0
ticket <https://github.com/Perl/perl5/issues/11547>.

- \(bu
sfio, stdio
.Sp
Perl supports being built without PerlIO proper, using a stdio or sfio
wrapper instead.  A perl build like this will not support \s-1IO\s0 layers and
thus Unicode \s-1IO,\s0 making it rather handicapped.
.Sp
PerlIO supports a \f(CW\*(C`stdio\*(C' layer if stdio use is desired, and similarly a
sfio layer could be produced.

- \(bu
Unescaped literal \f(CW"\{" in regular expressions.
.Sp
Starting with v5.20, it is planned to require a literal \f(CW"\{" to be
escaped, for example by preceding it with a backslash.  In v5.18, a
deprecated warning message will be emitted for all such uses.
This affects only patterns that are to match a literal \f(CW"\{".  Other
uses of this character, such as part of a quantifier or sequence as in
those below, are completely unaffected:
.Sp
.Vb 3
    /foo\{3,5\}/
    /\\p\{Alphabetic\}/
    /\\N\{DIGIT ZERO\}
.Ve
.Sp
Removing this will permit extensions to Perl's pattern syntax and better
error checking for existing syntax.  See \*(L"Quantifiers\*(R" in perlre for an
example.

- \(bu
Revamping \f(CW"\\Q" semantics in double-quotish strings when combined with other escapes.
.Sp
There are several bugs and inconsistencies involving combinations
of \f(CW\*(C`\\Q\*(C' and escapes like \f(CW\*(C`\\x\*(C', \f(CW\*(C`\\L\*(C', etc., within a \f(CW\*(C`\\Q...\\E\*(C' pair.
These need to be fixed, and doing so will necessarily change current
behavior.  The changes have not yet been settled.

## Incompatible Changes

Header "Incompatible Changes"

### Special blocks called in void context

Subsection "Special blocks called in void context"
Special blocks (\f(CW\*(C`BEGIN\*(C', \f(CW\*(C`CHECK\*(C', \f(CW\*(C`INIT\*(C', \f(CW\*(C`UNITCHECK\*(C', \f(CW\*(C`END\*(C') are now
called in void context.  This avoids wasteful copying of the result of the
last statement [perl #108794].
.ie n .SS "The ""overloading"" pragma and regexp objects"
.el .SS "The \f(CWoverloading pragma and regexp objects"
Subsection "The overloading pragma and regexp objects"
With \f(CW\*(C`no overloading\*(C', regular expression objects returned by \f(CW\*(C`qr//\*(C' are
now stringified as \*(L"Regexp=REGEXP(0xbe600d)\*(R" instead of the regular
expression itself [perl #108780].

### Two \s-1XS\s0 typemap Entries removed

Subsection "Two XS typemap Entries removed"
Two presumably unused \s-1XS\s0 typemap entries have been removed from the
core typemap: T_DATAUNIT and T_CALLBACK.  If you are, against all odds,
a user of these, please see the instructions on how to restore them
in perlxstypemap.

### Unicode 6.1 has incompatibilities with Unicode 6.0

Subsection "Unicode 6.1 has incompatibilities with Unicode 6.0"
These are detailed in \*(L"Supports (almost) Unicode 6.1\*(R" above.
You can compile this version of Perl to use Unicode 6.0.  See
\*(L"Hacking Perl to work on earlier Unicode versions (for very serious hackers only)\*(R" in perlunicode.

### Borland compiler

Subsection "Borland compiler"
All support for the Borland compiler has been dropped.  The code had not
worked for a long time anyway.

### Certain deprecated Unicode properties are no longer supported by default

Subsection "Certain deprecated Unicode properties are no longer supported by default"
Perl should never have exposed certain Unicode properties that are used
by Unicode internally and not meant to be publicly available.  Use of
these has generated deprecated warning messages since Perl 5.12.  The
removed properties are Other_Alphabetic,
Other_Default_Ignorable_Code_Point, Other_Grapheme_Extend,
Other_ID_Continue, Other_ID_Start, Other_Lowercase, Other_Math, and
Other_Uppercase.

Perl may be recompiled to include any or all of them; instructions are
given in
\*(L"Unicode character properties that are \s-1NOT\s0 accepted by Perl\*(R" in perluniprops.

### Dereferencing \s-1IO\s0 thingies as typeglobs

Subsection "Dereferencing IO thingies as typeglobs"
The \f(CW\*(C`*\{...\}\*(C' operator, when passed a reference to an \s-1IO\s0 thingy (as in
\f(CW\*(C`*\{*STDIN\{IO\}\}\*(C'), creates a new typeglob containing just that \s-1IO\s0 object.
Previously, it would stringify as an empty string, but some operators would
treat it as undefined, producing an \*(L"uninitialized\*(R" warning.
Now it stringifies as _\|_ANONIO_\|_ [perl #96326].

### User-defined case-changing operations

Subsection "User-defined case-changing operations"
This feature was deprecated in Perl 5.14, and has now been removed.
The \s-1CPAN\s0 module Unicode::Casing provides better functionality without
the drawbacks that this feature had, as are detailed in the 5.14
documentation:
<http://perldoc.perl.org/5.14.0/perlunicode.html#User-Defined-Case-Mappings-%28for-serious-hackers-only%29>

### XSUBs are now static

Subsection "XSUBs are now 'static'"
\s-1XSUB C\s0 functions are now 'static', that is, they are not visible from
outside the compilation unit.  Users can use the new \f(CW\*(C`XS_EXTERNAL(name)\*(C'
and \f(CW\*(C`XS_INTERNAL(name)\*(C' macros to pick the desired linking behavior.
The ordinary \f(CW\*(C`XS(name)\*(C' declaration for XSUBs will continue to declare
non-'static' XSUBs for compatibility, but the \s-1XS\s0 compiler,
ExtUtils::ParseXS (\f(CW\*(C`xsubpp\*(C') will emit 'static' XSUBs by default.
ExtUtils::ParseXS's behavior can be reconfigured from \s-1XS\s0 using the
\f(CW\*(C`EXPORT_XSUB_SYMBOLS\*(C' keyword.  See perlxs for details.

### Weakening read-only references

Subsection "Weakening read-only references"
Weakening read-only references is no longer permitted.  It should never
have worked anyway, and could sometimes result in crashes.

### Tying scalars that hold typeglobs

Subsection "Tying scalars that hold typeglobs"
Attempting to tie a scalar after a typeglob was assigned to it would
instead tie the handle in the typeglob's \s-1IO\s0 slot.  This meant that it was
impossible to tie the scalar itself.  Similar problems affected \f(CW\*(C`tied\*(C' and
\f(CW\*(C`untie\*(C': \f(CW\*(C`tied $scalar\*(C' would return false on a tied scalar if the last
thing returned was a typeglob, and \f(CW\*(C`untie $scalar\*(C' on such a tied scalar
would do nothing.

We fixed this problem before Perl 5.14.0, but it caused problems with some
\s-1CPAN\s0 modules, so we put in a deprecation cycle instead.

Now the deprecation has been removed and this bug has been fixed.  So
\f(CW\*(C`tie $scalar\*(C' will always tie the scalar, not the handle it holds.  To tie
the handle, use \f(CW\*(C`tie *$scalar\*(C' (with an explicit asterisk).  The same
applies to \f(CW\*(C`tied *$scalar\*(C' and \f(CW\*(C`untie *$scalar\*(C'.
.ie n .SS "IPC::Open3 no longer provides ""xfork()"", ""xclose_on_exec()"" and ""xpipe_anon()"""
.el .SS "IPC::Open3 no longer provides \f(CWxfork(), \f(CWxclose_on_exec() and \f(CWxpipe_anon()"
Subsection "IPC::Open3 no longer provides xfork(), xclose_on_exec() and xpipe_anon()"
All three functions were private, undocumented, and unexported.  They do
not appear to be used by any code on \s-1CPAN.\s0  Two have been inlined and one
deleted entirely.
.ie n .SS "$$ no longer caches \s-1PID\s0"
.el .SS "\f(CW$$ no longer caches \s-1PID\s0"
Subsection "$$ no longer caches PID"
Previously, if one called **fork**\|(3) from C, Perl's
notion of \f(CW$$ could go out of sync with what **getpid()** returns.  By always
fetching the value of \f(CW$$ via **getpid()**, this potential bug is eliminated.
Code that depends on the caching behavior will break.  As described in
Core Enhancements,
\f(CW$$ is now writable, but it will be reset during a
fork.
.ie n .SS "$$ and ""getppid()"" no longer emulate \s-1POSIX\s0 semantics under LinuxThreads"
.el .SS "\f(CW$$ and \f(CWgetppid() no longer emulate \s-1POSIX\s0 semantics under LinuxThreads"
Subsection "$$ and getppid() no longer emulate POSIX semantics under LinuxThreads"
The \s-1POSIX\s0 emulation of \f(CW$$ and \f(CW\*(C`getppid()\*(C' under the obsolete
LinuxThreads implementation has been removed.
This only impacts users of Linux 2.4 and
users of Debian GNU/kFreeBSD up to and including 6.0, not the vast
majority of Linux installations that use \s-1NPTL\s0 threads.

This means that \f(CW\*(C`getppid()\*(C', like \f(CW$$, is now always guaranteed to
return the \s-1OS\s0's idea of the current state of the process, not perl's
cached version of it.

See the documentation for $$ for details.
.ie n .SS "$<, $>, $( and $) are no longer cached"
.el .SS "\f(CW$<, \f(CW$>, \f(CW$( and \f(CW$) are no longer cached"
Subsection "$<, $>, $( and $) are no longer cached"
Similarly to the changes to \f(CW$$ and \f(CW\*(C`getppid()\*(C', the internal
caching of \f(CW$<, \f(CW$>, \f(CW$( and \f(CW$) has been removed.

When we cached these values our idea of what they were would drift out
of sync with reality if someone (e.g., someone embedding perl) called
\f(CW\*(C`sete?[ug]id()\*(C' without updating \f(CW\*(C`PL_e?[ug]id\*(C'.  Having to deal with
this complexity wasn't worth it given how cheap the \f(CW\*(C`gete?[ug]id()\*(C'
system call is.

This change will break a handful of \s-1CPAN\s0 modules that use the XS-level
\f(CW\*(C`PL_uid\*(C', \f(CW\*(C`PL_gid\*(C', \f(CW\*(C`PL_euid\*(C' or \f(CW\*(C`PL_egid\*(C' variables.

The fix for those breakages is to use \f(CW\*(C`PerlProc_gete?[ug]id()\*(C' to
retrieve them (e.g., \f(CW\*(C`PerlProc_getuid()\*(C'), and not to assign to
\f(CW\*(C`PL_e?[ug]id\*(C' if you change the \s-1UID/GID/EUID/EGID.\s0  There is no longer
any need to do so since perl will always retrieve the up-to-date
version of those values from the \s-1OS.\s0
.ie n .SS "Which Non-ASCII characters get quoted by ""quotemeta"" and ""\\Q"" has changed"
.el .SS "Which Non-ASCII characters get quoted by \f(CWquotemeta and \f(CW\\Q has changed"
Subsection "Which Non-ASCII characters get quoted by quotemeta and Q has changed"
This is unlikely to result in a real problem, as Perl does not attach
special meaning to any non-ASCII character, so it is currently
irrelevant which are quoted or not.  This change fixes bug [perl #77654] and
brings Perl's behavior more into line with Unicode's recommendations.
See \*(L"quotemeta\*(R" in perlfunc.

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
Improved performance for Unicode properties in regular expressions
.Sp
Matching a code point against a Unicode property is now done via a
binary search instead of linear.  This means for example that the worst
case for a 1000 item property is 10 probes instead of 1000.  This
inefficiency has been compensated for in the past by permanently storing
in a hash the results of a given probe plus the results for the adjacent
64 code points, under the theory that near-by code points are likely to
be searched for.  A separate hash was used for each mention of a Unicode
property in each regular expression.  Thus, \f(CW\*(C`qr/\\p\{foo\}abc\\p\{foo\}/\*(C'
would generate two hashes.  Any probes in one instance would be unknown
to the other, and the hashes could expand separately to be quite large
if the regular expression were used on many different widely-separated
code points.
Now, however, there is just one hash shared by all instances of a given
property.  This means that if \f(CW\*(C`\\p\{foo\}\*(C' is matched against \*(L"A\*(R" in one
regular expression in a thread, the result will be known immediately to
all regular expressions, and the relentless march of using up memory is
slowed considerably.

- \(bu
Version declarations with the \f(CW\*(C`use\*(C' keyword (e.g., \f(CW\*(C`use 5.012\*(C') are now
faster, as they enable features without loading *feature.pm*.

- \(bu
\f(CW\*(C`local $_\*(C' is faster now, as it no longer iterates through magic that it
is not going to copy anyway.

- \(bu
Perl 5.12.0 sped up the destruction of objects whose classes define
empty \f(CW\*(C`DESTROY\*(C' methods (to prevent autoloading), by simply not
calling such empty methods.  This release takes this optimization a
step further, by not calling any \f(CW\*(C`DESTROY\*(C' method that begins with a
\f(CW\*(C`return\*(C' statement.  This can be useful for destructors that are only
used for debugging:
.Sp
.Vb 2
    use constant DEBUG => 1;
    sub DESTROY \{ return unless DEBUG; ... \}
.Ve
.Sp
Constant-folding will reduce the first statement to \f(CW\*(C`return;\*(C' if \s-1DEBUG\s0
is set to 0, triggering this optimization.

- \(bu
Assigning to a variable that holds a typeglob or copy-on-write scalar
is now much faster.  Previously the typeglob would be stringified or
the copy-on-write scalar would be copied before being clobbered.

- \(bu
Assignment to \f(CW\*(C`substr\*(C' in void context is now more than twice its
previous speed.  Instead of creating and returning a special lvalue
scalar that is then assigned to, \f(CW\*(C`substr\*(C' modifies the original string
itself.

- \(bu
\f(CW\*(C`substr\*(C' no longer calculates a value to return when called in void
context.

- \(bu
Due to changes in File::Glob, Perl's \f(CW\*(C`glob\*(C' function and its \f(CW\*(C`<...>\*(C' equivalent are now much faster.  The splitting of the pattern
into words has been rewritten in C, resulting in speed-ups of 20% for
some cases.
.Sp
This does not affect \f(CW\*(C`glob\*(C' on \s-1VMS,\s0 as it does not use File::Glob.

- \(bu
The short-circuiting operators \f(CW\*(C`&&\*(C', \f(CW\*(C`||\*(C', and \f(CW\*(C`//\*(C', when chained
(such as \f(CW\*(C`$a || $b || $c\*(C'), are now considerably faster to short-circuit,
due to reduced optree traversal.

- \(bu
The implementation of \f(CW\*(C`s///r\*(C' makes one fewer copy of the scalar's value.

- \(bu
Recursive calls to lvalue subroutines in lvalue scalar context use less
memory.

## Modules and Pragmata

Header "Modules and Pragmata"

### Deprecated Modules

Subsection "Deprecated Modules"

- Version::Requirements
Item "Version::Requirements"
Version::Requirements is now \s-1DEPRECATED,\s0 use CPAN::Meta::Requirements,
which is a drop-in replacement.  It will be deleted from perl.git blead
in v5.17.0.

### New Modules and Pragmata

Subsection "New Modules and Pragmata"

- \(bu
arybase \*(-- this new module implements the \f(CW$[ variable.

- \(bu
PerlIO::mmap 0.010 has been added to the Perl core.
.Sp
The \f(CW\*(C`mmap\*(C' PerlIO layer is no longer implemented by perl itself, but has
been moved out into the new PerlIO::mmap module.

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"
This is only an overview of selected module updates.  For a complete list of
updates, run:

.Vb 1
    $ corelist --diff 5.14.0 5.16.0
.Ve

You can substitute your favorite version in place of 5.14.0, too.

- \(bu
Archive::Extract has been upgraded from version 0.48 to 0.58.
.Sp
Includes a fix for FreeBSD to only use \f(CW\*(C`unzip\*(C' if it is located in
\f(CW\*(C`/usr/local/bin\*(C', as FreeBSD 9.0 will ship with a limited \f(CW\*(C`unzip\*(C' in
\f(CW\*(C`/usr/bin\*(C'.

- \(bu
Archive::Tar has been upgraded from version 1.76 to 1.82.
.Sp
Adjustments to handle files >8gb (>0777777777777 octal) and a feature
to return the \s-1MD5SUM\s0 of files in the archive.

- \(bu
base has been upgraded from version 2.16 to 2.18.
.Sp
\f(CW\*(C`base\*(C' no longer sets a module's \f(CW$VERSION to \*(L"-1\*(R" when a module it
loads does not define a \f(CW$VERSION.  This change has been made because
\*(L"-1\*(R" is not a valid version number under the new \*(L"lax\*(R" criteria used
internally by \f(CW\*(C`UNIVERSAL::VERSION\*(C'.  (See version for more on \*(L"lax\*(R"
version criteria.)
.Sp
\f(CW\*(C`base\*(C' no longer internally skips loading modules it has already loaded
and instead relies on \f(CW\*(C`require\*(C' to inspect \f(CW%INC.  This fixes a bug
when \f(CW\*(C`base\*(C' is used with code that clear \f(CW%INC to force a module to
be reloaded.

- \(bu
Carp has been upgraded from version 1.20 to 1.26.
.Sp
It now includes last read filehandle info and puts a dot after the file
and line number, just like errors from \f(CW\*(C`die\*(C' [perl #106538].

- \(bu
charnames has been updated from version 1.18 to 1.30.
.Sp
\f(CW\*(C`charnames\*(C' can now be invoked with a new option, \f(CW\*(C`:loose\*(C',
which is like the existing \f(CW\*(C`:full\*(C' option, but enables Unicode loose
name matching.  Details are in \*(L"\s-1LOOSE MATCHES\*(R"\s0 in charnames.

- \(bu
B::Deparse has been upgraded from version 1.03 to 1.14.  This fixes
numerous deparsing bugs.

- \(bu
\s-1CGI\s0 has been upgraded from version 3.52 to 3.59.
.Sp
It uses the public and documented \s-1FCGI\s0.pm \s-1API\s0 in CGI::Fast.  CGI::Fast was
using an \s-1FCGI API\s0 that was deprecated and removed from documentation
more than ten years ago.  Usage of this deprecated \s-1API\s0 with \s-1FCGI\s0 >=
0.70 or \s-1FCGI\s0 <= 0.73 introduces a security issue.
<https://rt.cpan.org/Public/Bug/Display.html?id=68380>
<http://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2011-2766>
.Sp
Things that may break your code:
.Sp
\f(CW\*(C`url()\*(C' was fixed to return \f(CW\*(C`PATH_INFO\*(C' when it is explicitly requested
with either the \f(CW\*(C`path=>1\*(C' or \f(CW\*(C`path_info=>1\*(C' flag.
.Sp
If your code is running under mod_rewrite (or compatible) and you are
calling \f(CW\*(C`self_url()\*(C' or you are calling \f(CW\*(C`url()\*(C' and passing
\f(CW\*(C`path_info=>1\*(C', these methods will actually be returning
\f(CW\*(C`PATH_INFO\*(C' now, as you have explicitly requested or \f(CW\*(C`self_url()\*(C'
has requested on your behalf.
.Sp
The \f(CW\*(C`PATH_INFO\*(C' has been omitted in such URLs since the issue was
introduced in the 3.12 release in December, 2005.
.Sp
This bug is so old your application may have come to depend on it or
workaround it. Check for application before upgrading to this release.
.Sp
Examples of affected method calls:
.Sp
.Vb 5
  $q->url(-absolute => 1, -query => 1, -path_info => 1);
  $q->url(-path=>1);
  $q->url(-full=>1,-path=>1);
  $q->url(-rewrite=>1,-path=>1);
  $q->self_url();
.Ve
.Sp
We no longer read from \s-1STDIN\s0 when the Content-Length is not set,
preventing requests with no Content-Length from sometimes freezing.
This is consistent with the \s-1CGI RFC 3875,\s0 and is also consistent with
CGI::Simple.  However, the old behavior may have been expected by some
command-line uses of \s-1CGI\s0.pm.
.Sp
In addition, the \s-1DELETE HTTP\s0 verb is now supported.

- \(bu
Compress::Zlib has been upgraded from version 2.035 to 2.048.
.Sp
IO::Compress::Zip and IO::Uncompress::Unzip now have support for \s-1LZMA\s0
(method 14).  There is a fix for a \s-1CRC\s0 issue in IO::Compress::Unzip and
it supports Streamed Stored context now.  And fixed a Zip64 issue in
IO::Compress::Zip when the content size was exactly 0xFFFFFFFF.

- \(bu
Digest::SHA has been upgraded from version 5.61 to 5.71.
.Sp
Added \s-1BITS\s0 mode to the addfile method and shasum.  This makes
partial-byte inputs possible via files/STDIN and lets shasum check
all 8074 \s-1NIST\s0 Msg vectors, where previously special programming was
required to do this.

- \(bu
Encode has been upgraded from version 2.42 to 2.44.
.Sp
Missing aliases added, a deep recursion error fixed and various
documentation updates.
.Sp
Addressed 'decode_xs n-byte heap-overflow' security bug in Unicode.xs
(\s-1CVE-2011-2939\s0). (5.14.2)

- \(bu
ExtUtils::CBuilder updated from version 0.280203 to 0.280206.
.Sp
The new version appends \s-1CFLAGS\s0 and \s-1LDFLAGS\s0 to their Config.pm
counterparts.

- \(bu
ExtUtils::ParseXS has been upgraded from version 2.2210 to 3.16.
.Sp
Much of ExtUtils::ParseXS, the module behind the \s-1XS\s0 compiler \f(CW\*(C`xsubpp\*(C',
was rewritten and cleaned up.  It has been made somewhat more extensible
and now finally uses strictures.
.Sp
The typemap logic has been moved into a separate module,
ExtUtils::Typemaps.  See \*(L"New Modules and Pragmata\*(R", above.
.Sp
For a complete set of changes, please see the ExtUtils::ParseXS
changelog, available on the \s-1CPAN.\s0

- \(bu
File::Glob has been upgraded from version 1.12 to 1.17.
.Sp
On Windows, tilde (~) expansion now checks the \f(CW\*(C`USERPROFILE\*(C' environment
variable, after checking \f(CW\*(C`HOME\*(C'.
.Sp
It has a new \f(CW\*(C`:bsd_glob\*(C' export tag, intended to replace \f(CW\*(C`:glob\*(C'.  Like
\f(CW\*(C`:glob\*(C' it overrides \f(CW\*(C`glob\*(C' with a function that does not split the glob
pattern into words, but, unlike \f(CW\*(C`:glob\*(C', it iterates properly in scalar
context, instead of returning the last file.
.Sp
There are other changes affecting Perl's own \f(CW\*(C`glob\*(C' operator (which uses
File::Glob internally, except on \s-1VMS\s0).  See \*(L"Performance Enhancements\*(R"
and \*(L"Selected Bug Fixes\*(R".

- \(bu
FindBin updated from version 1.50 to 1.51.
.Sp
It no longer returns a wrong result if a script of the same name as the
current one exists in the path and is executable.

- \(bu
HTTP::Tiny has been upgraded from version 0.012 to 0.017.
.Sp
Added support for using \f(CW$ENV\{http_proxy\} to set the default proxy host.
.Sp
Adds additional shorthand methods for all common \s-1HTTP\s0 verbs,
a \f(CW\*(C`post_form()\*(C' method for POST-ing x-www-form-urlencoded data and
a \f(CW\*(C`www_form_urlencode()\*(C' utility method.

- \(bu
\s-1IO\s0 has been upgraded from version 1.25_04 to 1.25_06, and IO::Handle
from version 1.31 to 1.33.
.Sp
Together, these upgrades fix a problem with IO::Handle's \f(CW\*(C`getline\*(C' and
\f(CW\*(C`getlines\*(C' methods.  When these methods are called on the special \s-1ARGV\s0
handle, the next file is automatically opened, as happens with the built-in
\f(CW\*(C`<>\*(C' and \f(CW\*(C`readline\*(C' functions.  But, unlike the built-ins, these
methods were not respecting the caller's use of the open pragma and
applying the appropriate I/O layers to the newly-opened file
[rt.cpan.org #66474].

- \(bu
IPC::Cmd has been upgraded from version 0.70 to 0.76.
.Sp
Capturing of command output (both \f(CW\*(C`STDOUT\*(C' and \f(CW\*(C`STDERR\*(C') is now supported
using IPC::Open3 on MSWin32 without requiring IPC::Run.

- \(bu
IPC::Open3 has been upgraded from version 1.09 to 1.12.
.Sp
Fixes a bug which prevented use of \f(CW\*(C`open3\*(C' on Windows when \f(CW*STDIN,
\f(CW*STDOUT or \f(CW*STDERR had been localized.
.Sp
Fixes a bug which prevented duplicating numeric file descriptors on Windows.
.Sp
\f(CW\*(C`open3\*(C' with \*(L"-\*(R" for the program name works once more.  This was broken in
version 1.06 (and hence in Perl 5.14.0) [perl #95748].

- \(bu
Locale::Codes has been upgraded from version 3.16 to 3.21.
.Sp
Added Language Extension codes (langext) and Language Variation codes (langvar)
as defined in the \s-1IANA\s0 language registry.
.Sp
Added language codes from \s-1ISO 639-5\s0
.Sp
Added language/script codes from the \s-1IANA\s0 language subtag registry
.Sp
Fixed an uninitialized value warning [rt.cpan.org #67438].
.Sp
Fixed the return value for the all_XXX_codes and all_XXX_names functions
[rt.cpan.org #69100].
.Sp
Reorganized modules to move Locale::MODULE to Locale::Codes::MODULE to allow
for cleaner future additions.  The original four modules (Locale::Language,
Locale::Currency, Locale::Country, Locale::Script) will continue to work, but
all new sets of codes will be added in the Locale::Codes namespace.
.Sp
The code2XXX, XXX2code, all_XXX_codes, and all_XXX_names functions now
support retired codes.  All codesets may be specified by a constant or
by their name now.  Previously, they were specified only by a constant.
.Sp
The alias_code function exists for backward compatibility.  It has been
replaced by rename_country_code.  The alias_code function will be
removed some time after September, 2013.
.Sp
All work is now done in the central module (Locale::Codes).  Previously,
some was still done in the wrapper modules (Locale::Codes::*).  Added
Language Family codes (langfam) as defined in \s-1ISO 639-5.\s0

- \(bu
Math::BigFloat has been upgraded from version 1.993 to 1.997.
.Sp
The \f(CW\*(C`numify\*(C' method has been corrected to return a normalized Perl number
(the result of \f(CW\*(C`0 + $thing\*(C'), instead of a string [rt.cpan.org #66732].

- \(bu
Math::BigInt has been upgraded from version 1.994 to 1.998.
.Sp
It provides a new \f(CW\*(C`bsgn\*(C' method that complements the \f(CW\*(C`babs\*(C' method.
.Sp
It fixes the internal \f(CW\*(C`objectify\*(C' function's handling of \*(L"foreign objects\*(R"
so they are converted to the appropriate class (Math::BigInt or
Math::BigFloat).

- \(bu
Math::BigRat has been upgraded from version 0.2602 to 0.2603.
.Sp
\f(CW\*(C`int()\*(C' on a Math::BigRat object containing -1/2 now creates a
Math::BigInt containing 0, rather than -0.  Math::BigInt does not even
support negative zero, so the resulting object was actually malformed
[perl #95530].

- \(bu
Math::Complex has been upgraded from version 1.56 to 1.59
and Math::Trig from version 1.2 to 1.22.
.Sp
Fixes include: correct copy constructor usage; fix polarwise formatting with
numeric format specifier; and more stable \f(CW\*(C`great_circle_direction\*(C' algorithm.

- \(bu
Module::CoreList has been upgraded from version 2.51 to 2.66.
.Sp
The \f(CW\*(C`corelist\*(C' utility now understands the \f(CW\*(C`-r\*(C' option for displaying
Perl release dates and the \f(CW\*(C`--diff\*(C' option to print the set of modlib
changes between two perl distributions.

- \(bu
Module::Metadata has been upgraded from version 1.000004 to 1.000009.
.Sp
Adds \f(CW\*(C`provides\*(C' method to generate a \s-1CPAN META\s0 provides data structure
correctly; use of \f(CW\*(C`package_versions_from_directory\*(C' is discouraged.

- \(bu
ODBM_File has been upgraded from version 1.10 to 1.12.
.Sp
The \s-1XS\s0 code is now compiled with \f(CW\*(C`PERL_NO_GET_CONTEXT\*(C', which will aid
performance under ithreads.

- \(bu
open has been upgraded from version 1.08 to 1.10.
.Sp
It no longer turns off layers on standard handles when invoked without the
\*(L":std\*(R" directive.  Similarly, when invoked *with* the \*(L":std\*(R" directive, it
now clears layers on \s-1STDERR\s0 before applying the new ones, and not just on
\s-1STDIN\s0 and \s-1STDOUT\s0 [perl #92728].

- \(bu
overload has been upgraded from version 1.13 to 1.18.
.Sp
\f(CW\*(C`overload::Overloaded\*(C' no longer calls \f(CW\*(C`can\*(C' on the class, but uses
another means to determine whether the object has overloading.  It was
never correct for it to call \f(CW\*(C`can\*(C', as overloading does not respect
\s-1AUTOLOAD.\s0  So classes that autoload methods and implement \f(CW\*(C`can\*(C' no longer
have to account for overloading [perl #40333].
.Sp
A warning is now produced for invalid arguments.  See \*(L"New Diagnostics\*(R".

- \(bu
PerlIO::scalar has been upgraded from version 0.11 to 0.14.
.Sp
(This is the module that implements \f(CW\*(C`open $fh, \*(Aq>\*(Aq, \\$scalar\*(C'.)
.Sp
It fixes a problem with \f(CW\*(C`open my $fh, ">", \\$scalar\*(C' not working if
\f(CW$scalar is a copy-on-write scalar. (5.14.2)
.Sp
It also fixes a hang that occurs with \f(CW\*(C`readline\*(C' or \f(CW\*(C`<$fh>\*(C' if a
typeglob has been assigned to \f(CW$scalar [perl #92258].
.Sp
It no longer assumes during \f(CW\*(C`seek\*(C' that \f(CW$scalar is a string internally.
If it didn't crash, it was close to doing so [perl #92706].  Also, the
internal print routine no longer assumes that the position set by \f(CW\*(C`seek\*(C'
is valid, but extends the string to that position, filling the intervening
bytes (between the old length and the seek position) with nulls
[perl #78980].
.Sp
Printing to an in-memory handle now works if the \f(CW$scalar holds a reference,
stringifying the reference before modifying it.  References used to be
treated as empty strings.
.Sp
Printing to an in-memory handle no longer crashes if the \f(CW$scalar happens to
hold a number internally, but no string buffer.
.Sp
Printing to an in-memory handle no longer creates scalars that confuse
the regular expression engine [perl #108398].

- \(bu
Pod::Functions has been upgraded from version 1.04 to 1.05.
.Sp
*Functions.pm* is now generated at perl build time from annotations in
*perlfunc.pod*.  This will ensure that Pod::Functions and perlfunc
remain in synchronisation.

- \(bu
Pod::Html has been upgraded from version 1.11 to 1.1502.
.Sp
This is an extensive rewrite of Pod::Html to use Pod::Simple under
the hood.  The output has changed significantly.

- \(bu
Pod::Perldoc has been upgraded from version 3.15_03 to 3.17.
.Sp
It corrects the search paths on \s-1VMS\s0 [perl #90640]. (5.14.1)
.Sp
The **-v** option now fetches the right section for \f(CW$0.
.Sp
This upgrade has numerous significant fixes.  Consult its changelog on
the \s-1CPAN\s0 for more information.

- \(bu
\s-1POSIX\s0 has been upgraded from version 1.24 to 1.30.
.Sp
\s-1POSIX\s0 no longer uses AutoLoader.  Any code which was relying on this
implementation detail was buggy, and may fail because of this change.
The module's Perl code has been considerably simplified, roughly halving
the number of lines, with no change in functionality.  The \s-1XS\s0 code has
been refactored to reduce the size of the shared object by about 12%,
with no change in functionality.  More \s-1POSIX\s0 functions now have tests.
.Sp
\f(CW\*(C`sigsuspend\*(C' and \f(CW\*(C`pause\*(C' now run signal handlers before returning, as the
whole point of these two functions is to wait until a signal has
arrived, and then return *after* it has been triggered.  Delayed, or
\*(L"safe\*(R", signals were preventing that from happening, possibly resulting in
race conditions [perl #107216].
.Sp
\f(CW\*(C`POSIX::sleep\*(C' is now a direct call into the underlying \s-1OS\s0 \f(CW\*(C`sleep\*(C'
function, instead of being a Perl wrapper on \f(CW\*(C`CORE::sleep\*(C'.
\f(CW\*(C`POSIX::dup2\*(C' now returns the correct value on Win32 (*i.e.*, the file
descriptor).  \f(CW\*(C`POSIX::SigSet\*(C' \f(CW\*(C`sigsuspend\*(C' and \f(CW\*(C`sigpending\*(C' and
\f(CW\*(C`POSIX::pause\*(C' now dispatch safe signals immediately before returning to
their caller.
.Sp
\f(CW\*(C`POSIX::Termios::setattr\*(C' now defaults the third argument to \f(CW\*(C`TCSANOW\*(C',
instead of 0. On most platforms \f(CW\*(C`TCSANOW\*(C' is defined to be 0, but on some
0 is not a valid parameter, which caused a call with defaults to fail.

- \(bu
Socket has been upgraded from version 1.94 to 2.001.
.Sp
It has new functions and constants for handling IPv6 sockets:
.Sp
.Vb 11
    pack_ipv6_mreq
    unpack_ipv6_mreq
    IPV6_ADD_MEMBERSHIP
    IPV6_DROP_MEMBERSHIP
    IPV6_MTU
    IPV6_MTU_DISCOVER
    IPV6_MULTICAST_HOPS
    IPV6_MULTICAST_IF
    IPV6_MULTICAST_LOOP
    IPV6_UNICAST_HOPS
    IPV6_V6ONLY
.Ve

- \(bu
Storable has been upgraded from version 2.27 to 2.34.
.Sp
It no longer turns copy-on-write scalars into read-only scalars when
freezing and thawing.

- \(bu
Sys::Syslog has been upgraded from version 0.27 to 0.29.
.Sp
This upgrade closes many outstanding bugs.

- \(bu
Term::ANSIColor has been upgraded from version 3.00 to 3.01.
.Sp
Only interpret an initial array reference as a list of colors, not any initial
reference, allowing the colored function to work properly on objects with
stringification defined.

- \(bu
Term::ReadLine has been upgraded from version 1.07 to 1.09.
.Sp
Term::ReadLine now supports any event loop, including unpublished ones and
simple IO::Select, loops without the need to rewrite existing code for
any particular framework [perl #108470].

- \(bu
threads::shared has been upgraded from version 1.37 to 1.40.
.Sp
Destructors on shared objects used to be ignored sometimes if the objects
were referenced only by shared data structures.  This has been mostly
fixed, but destructors may still be ignored if the objects still exist at
global destruction time [perl #98204].

- \(bu
Unicode::Collate has been upgraded from version 0.73 to 0.89.
.Sp
Updated to \s-1CLDR 1.9.1\s0
.Sp
Locales updated to \s-1CLDR 2.0:\s0 mk, mt, nb, nn, ro, ru, sk, sr, sv, uk,
zh_\|_pinyin, zh_\|_stroke
.Sp
Newly supported locales: bn, fa, ml, mr, or, pa, sa, si, si_\|_dictionary,
sr_Latn, sv_\|_reformed, ta, te, th, ur, wae.
.Sp
Tailored compatibility ideographs as well as unified ideographs for the
locales: ja, ko, zh_\|_big5han, zh_\|_gb2312han, zh_\|_pinyin, zh_\|_stroke.
.Sp
Locale/*.pl files are now searched for in \f(CW@INC.

- \(bu
Unicode::Normalize has been upgraded from version 1.10 to 1.14.
.Sp
Fixes for the removal of *unicore/CompositionExclusions.txt* from core.

- \(bu
Unicode::UCD has been upgraded from version 0.32 to 0.43.
.Sp
This adds four new functions:  \f(CW\*(C`prop_aliases()\*(C' and
\f(CW\*(C`prop_value_aliases()\*(C', which are used to find all Unicode-approved
synonyms for property names, or to convert from one name to another;
\f(CW\*(C`prop_invlist\*(C' which returns all code points matching a given
Unicode binary property; and \f(CW\*(C`prop_invmap\*(C' which returns the complete
specification of a given Unicode property.

- \(bu
Win32API::File has been upgraded from version 0.1101 to 0.1200.
.Sp
Added SetStdHandle and GetStdHandle functions

### Removed Modules and Pragmata

Subsection "Removed Modules and Pragmata"
As promised in Perl 5.14.0's release notes, the following modules have
been removed from the core distribution, and if needed should be installed
from \s-1CPAN\s0 instead.

- \(bu
Devel::DProf has been removed from the Perl core.  Prior version was
20110228.00.

- \(bu
Shell has been removed from the Perl core.  Prior version was 0.72_01.

- \(bu
Several old perl4-style libraries which have been deprecated with 5.14
are now removed:
.Sp
.Vb 5
    abbrev.pl assert.pl bigfloat.pl bigint.pl bigrat.pl cacheout.pl
    complete.pl ctime.pl dotsh.pl exceptions.pl fastcwd.pl flush.pl
    getcwd.pl getopt.pl getopts.pl hostname.pl importenv.pl
    lib/find\{,depth\}.pl look.pl newgetopt.pl open2.pl open3.pl
    pwd.pl shellwords.pl stat.pl tainted.pl termcap.pl timelocal.pl
.Ve
.Sp
They can be found on \s-1CPAN\s0 as Perl4::CoreLibs.

## Documentation

Header "Documentation"

### New Documentation

Subsection "New Documentation"
*perldtrace*
Subsection "perldtrace"

perldtrace describes Perl's DTrace support, listing the provided probes
and gives examples of their use.

*perlexperiment*
Subsection "perlexperiment"

This document is intended to provide a list of experimental features in
Perl.  It is still a work in progress.

*perlootut*
Subsection "perlootut"

This a new \s-1OO\s0 tutorial.  It focuses on basic \s-1OO\s0 concepts, and then recommends
that readers choose an \s-1OO\s0 framework from \s-1CPAN.\s0

*perlxstypemap*
Subsection "perlxstypemap"

The new manual describes the \s-1XS\s0 typemapping mechanism in unprecedented
detail and combines new documentation with information extracted from
perlxs and the previously unofficial list of all core typemaps.

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perlapi*
Subsection "perlapi"

- \(bu
The \s-1HV API\s0 has long accepted negative lengths to show that the key is
in \s-1UTF8.\s0  This is now documented.

- \(bu
The \f(CW\*(C`boolSV()\*(C' macro is now documented.

*perlfunc*
Subsection "perlfunc"

- \(bu
\f(CW\*(C`dbmopen\*(C' treats a 0 mode as a special case, that prevents a nonexistent
file from being created.  This has been the case since Perl 5.000, but was
never documented anywhere.  Now the perlfunc entry mentions it
[perl #90064].

- \(bu
As an accident of history, \f(CW\*(C`open $fh, \*(Aq<:\*(Aq, ...\*(C' applies the default
layers for the platform (\f(CW\*(C`:raw\*(C' on Unix, \f(CW\*(C`:crlf\*(C' on Windows), ignoring
whatever is declared by open.pm.  This seems such a useful feature
it has been documented in perlfunc and open.

- \(bu
The entry for \f(CW\*(C`split\*(C' has been rewritten.  It is now far clearer than
before.

*perlguts*
Subsection "perlguts"

- \(bu
A new section, Autoloading with XSUBs,
has been added, which explains the two APIs for accessing the name of the
autoloaded sub.

- \(bu
Some function descriptions in perlguts were confusing, as it was
not clear whether they referred to the function above or below the
description.  This has been clarified [perl #91790].

*perlobj*
Subsection "perlobj"

- \(bu
This document has been rewritten from scratch, and its coverage of various \s-1OO\s0
concepts has been expanded.

*perlop*
Subsection "perlop"

- \(bu
Documentation of the smartmatch operator has been reworked and moved from
perlsyn to perlop where it belongs.
.Sp
It has also been corrected for the case of \f(CW\*(C`undef\*(C' on the left-hand
side.  The list of different smart match behaviors had an item in the
wrong place.

- \(bu
Documentation of the ellipsis statement (\f(CW\*(C`...\*(C') has been reworked and
moved from perlop to perlsyn.

- \(bu
The explanation of bitwise operators has been expanded to explain how they
work on Unicode strings (5.14.1).

- \(bu
More examples for \f(CW\*(C`m//g\*(C' have been added (5.14.1).

- \(bu
The \f(CW\*(C`<<\\FOO\*(C' here-doc syntax has been documented (5.14.1).

*perlpragma*
Subsection "perlpragma"

- \(bu
There is now a standard convention for naming keys in the \f(CW\*(C`%^H\*(C',
documented under Key naming.

*\*(L"Laundering and Detecting Tainted Data\*(R" in perlsec*
Subsection "Laundering and Detecting Tainted Data in perlsec"

- \(bu
The example function for checking for taintedness contained a subtle
error.  \f(CW$@ needs to be localized to prevent its changing this
global's value outside the function.  The preferred method to check for
this remains \*(L"tainted\*(R" in Scalar::Util.

*perllol*
Subsection "perllol"

- \(bu
perllol has been expanded with examples using the new \f(CW\*(C`push $scalar\*(C'
syntax introduced in Perl 5.14.0 (5.14.1).

*perlmod*
Subsection "perlmod"

- \(bu
perlmod now states explicitly that some types of explicit symbol table
manipulation are not supported.  This codifies what was effectively already
the case [perl #78074].

*perlpodstyle*
Subsection "perlpodstyle"

- \(bu
The tips on which formatting codes to use have been corrected and greatly
expanded.

- \(bu
There are now a couple of example one-liners for previewing \s-1POD\s0 files after
they have been edited.

*perlre*
Subsection "perlre"

- \(bu
The \f(CW\*(C`(*COMMIT)\*(C' directive is now listed in the right section
(Verbs without an argument).

*perlrun*
Subsection "perlrun"

- \(bu
perlrun has undergone a significant clean-up.  Most notably, the
**-0x...** form of the **-0** flag has been clarified, and the final section
on environment variables has been corrected and expanded (5.14.1).

*perlsub*
Subsection "perlsub"

- \(bu
The ($;) prototype syntax, which has existed for rather a long time, is now
documented in perlsub.  It lets a unary function have the same
precedence as a list operator.

*perltie*
Subsection "perltie"

- \(bu
The required syntax for tying handles has been documented.

*perlvar*
Subsection "perlvar"

- \(bu
The documentation for $! has been corrected and clarified.
It used to state that $! could be \f(CW\*(C`undef\*(C', which is not the case.  It was
also unclear whether system calls set C's \f(CW\*(C`errno\*(C' or Perl's \f(CW$!
[perl #91614].

- \(bu
Documentation for $$ has been amended with additional
cautions regarding changing the process \s-1ID.\s0

*Other Changes*
Subsection "Other Changes"

- \(bu
perlxs was extended with documentation on inline typemaps.

- \(bu
perlref has a new Circular References
section explaining how circularities may not be freed and how to solve that
with weak references.

- \(bu
Parts of perlapi were clarified, and Perl equivalents of some C
functions have been added as an additional mode of exposition.

- \(bu
A few parts of perlre and perlrecharclass were clarified.

### Removed Documentation

Subsection "Removed Documentation"
*Old \s-1OO\s0 Documentation*
Subsection "Old OO Documentation"

The old \s-1OO\s0 tutorials, perltoot, perltooc, and perlboot, have been
removed.  The perlbot (bag of object tricks) document has been removed
as well.

*Development Deltas*
Subsection "Development Deltas"

The perldelta files for development releases are no longer packaged with
perl.  These can still be found in the perl source code repository.

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### New Diagnostics

Subsection "New Diagnostics"
*New Errors*
Subsection "New Errors"

- \(bu
Cannot set tied \f(CW@DB::args
.Sp
This error occurs when \f(CW\*(C`caller\*(C' tries to set \f(CW@DB::args but finds it
tied.  Before this error was added, it used to crash instead.

- \(bu
Cannot tie unreifiable array
.Sp
This error is part of a safety check that the \f(CW\*(C`tie\*(C' operator does before
tying a special array like \f(CW@_.  You should never see this message.

- \(bu
&CORE::%s cannot be called directly
.Sp
This occurs when a subroutine in the \f(CW\*(C`CORE::\*(C' namespace is called
with \f(CW&foo syntax or through a reference.  Some subroutines
in this package cannot yet be called that way, but must be
called as barewords.  See "Subroutines in the \f(CW\*(C`CORE\*(C' namespace", above.

- \(bu
Source filters apply only to byte streams
.Sp
This new error occurs when you try to activate a source filter (usually by
loading a source filter module) within a string passed to \f(CW\*(C`eval\*(C' under the
\f(CW\*(C`unicode_eval\*(C' feature.

*New Warnings*
Subsection "New Warnings"

- \(bu
defined(@array) is deprecated
.Sp
The long-deprecated \f(CW\*(C`defined(@array)\*(C' now also warns for package variables.
Previously it issued a warning for lexical variables only.

- \(bu
**length()** used on \f(CW%s
.Sp
This new warning occurs when \f(CW\*(C`length\*(C' is used on an array or hash, instead
of \f(CW\*(C`scalar(@array)\*(C' or \f(CW\*(C`scalar(keys %hash)\*(C'.

- \(bu
lvalue attribute \f(CW%s already-defined subroutine
.Sp
attributes.pm now emits this warning when the :lvalue
attribute is applied to a Perl subroutine that has already been defined, as
doing so can have unexpected side-effects.

- \(bu
overload arg '%s' is invalid
.Sp
This warning, in the \*(L"overload\*(R" category, is produced when the overload
pragma is given an argument it doesn't recognize, presumably a mistyped
operator.

- \(bu
$[ used in \f(CW%s (did you mean $] ?)
.Sp
This new warning exists to catch the mistaken use of \f(CW$[ in version
checks.  \f(CW$], not \f(CW$[, contains the version number.

- \(bu
Useless assignment to a temporary
.Sp
Assigning to a temporary scalar returned
from an lvalue subroutine now produces this
warning [perl #31946].

- \(bu
Useless use of \\E
.Sp
\f(CW\*(C`\\E\*(C' does nothing unless preceded by \f(CW\*(C`\\Q\*(C', \f(CW\*(C`\\L\*(C' or \f(CW\*(C`\\U\*(C'.

### Removed Errors

Subsection "Removed Errors"

- \(bu
\*(L"sort is now a reserved word\*(R"
.Sp
This error used to occur when \f(CW\*(C`sort\*(C' was called without arguments,
followed by \f(CW\*(C`;\*(C' or \f(CW\*(C`)\*(C'.  (E.g., \f(CW\*(C`sort;\*(C' would die, but \f(CW\*(C`\{sort\}\*(C' was
\s-1OK.\s0)  This error message was added in Perl 3 to catch code like
\f(CW\*(C`close(sort)\*(C' which would no longer work.  More than two decades later,
this message is no longer appropriate.  Now \f(CW\*(C`sort\*(C' without arguments is
always allowed, and returns an empty list, as it did in those cases
where it was already allowed [perl #90030].

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
The \*(L"Applying pattern match...\*(R" or similar warning produced when an
array or hash is on the left-hand side of the \f(CW\*(C`=~\*(C' operator now
mentions the name of the variable.

- \(bu
The \*(L"Attempt to free non-existent shared string\*(R" has had the spelling
of \*(L"non-existent\*(R" corrected to \*(L"nonexistent\*(R".  It was already listed
with the correct spelling in perldiag.

- \(bu
The error messages for using \f(CW\*(C`default\*(C' and \f(CW\*(C`when\*(C' outside a
topicalizer have been standardized to match the messages for \f(CW\*(C`continue\*(C'
and loop controls.  They now read 'Can't \*(L"default\*(R" outside a
topicalizer' and 'Can't \*(L"when\*(R" outside a topicalizer'.  They both used
to be 'Can't use **when()** outside a topicalizer' [perl #91514].

- \(bu
The message, \*(L"Code point 0x%X is not Unicode, no properties match it;
all inverse properties do\*(R" has been changed to \*(L"Code point 0x%X is not
Unicode, all \\p\{\} matches fail; all \\P\{\} matches succeed\*(R".

- \(bu
Redefinition warnings for constant subroutines used to be mandatory,
even occurring under \f(CW\*(C`no warnings\*(C'.  Now they respect the warnings
pragma.

- \(bu
The \*(L"glob failed\*(R" warning message is now suppressible via \f(CW\*(C`no warnings\*(C'
[perl #111656].

- \(bu
The Invalid version format
error message now says \*(L"negative version number\*(R" within the parentheses,
rather than \*(L"non-numeric data\*(R", for negative numbers.

- \(bu
The two warnings
Possible attempt to put comments in **qw()** list
and
Possible attempt to separate words with commas
are no longer mutually exclusive: the same \f(CW\*(C`qw\*(C' construct may produce
both.

- \(bu
The uninitialized warning for \f(CW\*(C`y///r\*(C' when \f(CW$_ is implicit and
undefined now mentions the variable name, just like the non-/r variation
of the operator.

- \(bu
The 'Use of \*(L"foo\*(R" without parentheses is ambiguous' warning has been
extended to apply also to user-defined subroutines with a (;$)
prototype, and not just to built-in functions.

- \(bu
Warnings that mention the names of lexical (\f(CW\*(C`my\*(C') variables with
Unicode characters in them now respect the presence or absence of the
\f(CW\*(C`:utf8\*(C' layer on the output handle, instead of outputting \s-1UTF8\s0
regardless.  Also, the correct names are included in the strings passed
to \f(CW$SIG\{_\|_WARN_\|_\} handlers, rather than the raw \s-1UTF8\s0 bytes.

## Utility Changes

Header "Utility Changes"
*h2ph*
Subsection "h2ph"

- \(bu
h2ph used to generate code of the form
.Sp
.Vb 3
  unless(defined(&FOO)) \{
    sub FOO () \{42;\}
  \}
.Ve
.Sp
But the subroutine is a compile-time declaration, and is hence unaffected
by the condition.  It has now been corrected to emit a string \f(CW\*(C`eval\*(C'
around the subroutine [perl #99368].

*splain*
Subsection "splain"

- \(bu
*splain* no longer emits backtraces with the first line number repeated.
.Sp
This:
.Sp
.Vb 6
    Uncaught exception from user code:
            Cannot fwiddle the fwuddle at -e line 1.
     at -e line 1
            main::baz() called at -e line 1
            main::bar() called at -e line 1
            main::foo() called at -e line 1
.Ve
.Sp
has become this:
.Sp
.Vb 5
    Uncaught exception from user code:
            Cannot fwiddle the fwuddle at -e line 1.
            main::baz() called at -e line 1
            main::bar() called at -e line 1
            main::foo() called at -e line 1
.Ve

- \(bu
Some error messages consist of multiple lines that are listed as separate
entries in perldiag.  splain has been taught to find the separate
entries in these cases, instead of simply failing to find the message.

*zipdetails*
Subsection "zipdetails"

- \(bu
This is a new utility, included as part of an
IO::Compress::Base upgrade.
.Sp
zipdetails displays information about the internal record structure
of the zip file.  It is not concerned with displaying any details of
the compressed data stored in the zip file.

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
*regexp.h* has been modified for compatibility with \s-1GCC\s0's **-Werror**
option, as used by some projects that include perl's header files (5.14.1).

- \(bu
\f(CW\*(C`USE_LOCALE\{,_COLLATE,_CTYPE,_NUMERIC\}\*(C' have been added the output of perl -V
as they have affect the behavior of the interpreter binary (albeit
in only a small area).

- \(bu
The code and tests for IPC::Open2 have been moved from *ext/IPC-Open2*
into *ext/IPC-Open3*, as \f(CW\*(C`IPC::Open2::open2()\*(C' is implemented as a thin
wrapper around \f(CW\*(C`IPC::Open3::_open3()\*(C', and hence is very tightly coupled to
it.

- \(bu
The magic types and magic vtables are now generated from data in a new script
*regen/mg_vtable.pl*, instead of being maintained by hand.  As different
\s-1EBCDIC\s0 variants can't agree on the code point for '~', the character to code
point conversion is done at build time by *generate_uudmap* to a new generated
header *mg_data.h*.  \f(CW\*(C`PL_vtbl_bm\*(C' and \f(CW\*(C`PL_vtbl_fm\*(C' are now defined by the
pre-processor as \f(CW\*(C`PL_vtbl_regexp\*(C', instead of being distinct C variables.
\f(CW\*(C`PL_vtbl_sig\*(C' has been removed.

- \(bu
Building with \f(CW\*(C`-DPERL_GLOBAL_STRUCT\*(C' works again.  This configuration is not
generally used.

- \(bu
Perl configured with *\s-1MAD\s0* now correctly frees \f(CW\*(C`MADPROP\*(C' structures when
OPs are freed.  \f(CW\*(C`MADPROP\*(C's are now allocated with \f(CW\*(C`PerlMemShared_malloc()\*(C'

- \(bu
*makedef.pl* has been refactored.  This should have no noticeable affect on
any of the platforms that use it as part of their build (\s-1AIX, VMS,\s0 Win32).

- \(bu
\f(CW\*(C`useperlio\*(C' can no longer be disabled.

- \(bu
The file *global.sym* is no longer needed, and has been removed.  It
contained a list of all exported functions, one of the files generated by
*regen/embed.pl* from data in *embed.fnc* and *regen/opcodes*.  The code
has been refactored so that the only user of *global.sym*, *makedef.pl*,
now reads *embed.fnc* and *regen/opcodes* directly, removing the need to
store the list of exported functions in an intermediate file.
.Sp
As *global.sym* was never installed, this change should not be visible
outside the build process.

- \(bu
*pod/buildtoc*, used by the build process to build perltoc, has been
refactored and simplified.  It now contains only code to build perltoc;
the code to regenerate Makefiles has been moved to *Porting/pod_rules.pl*.
It's a bug if this change has any material effect on the build process.

- \(bu
*pod/roffitall* is now built by *pod/buildtoc*, instead of being
shipped with the distribution.  Its list of manpages is now generated
(and therefore current).  See also \s-1RT\s0 #103202 for an unresolved related
issue.

- \(bu
The man page for \f(CW\*(C`XS::Typemap\*(C' is no longer installed.  \f(CW\*(C`XS::Typemap\*(C'
is a test module which is not installed, hence installing its
documentation makes no sense.

- \(bu
The -Dusesitecustomize and -Duserelocatableinc options now work
together properly.

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"
*Cygwin*
Subsection "Cygwin"

- \(bu
Since version 1.7, Cygwin supports native \s-1UTF-8\s0 paths.  If Perl is built
under that environment, directory and filenames will be \s-1UTF-8\s0 encoded.

- \(bu
Cygwin does not initialize all original Win32 environment variables.  See
*\s-1README\s0.cygwin* for a discussion of the newly-added
\f(CW\*(C`Cygwin::sync_winenv()\*(C' function [perl #110190] and for
further links.

*HP-UX*
Subsection "HP-UX"

- \(bu
HP-UX \s-1PA-RISC/64\s0 now supports gcc-4.x
.Sp
A fix to correct the socketsize now makes the test suite pass on HP-UX
PA-RISC for 64bitall builds. (5.14.2)

*\s-1VMS\s0*
Subsection "VMS"

- \(bu
Remove unnecessary includes, fix miscellaneous compiler warnings and
close some unclosed comments on *vms/vms.c*.

- \(bu
Remove sockadapt layer from the \s-1VMS\s0 build.

- \(bu
Explicit support for \s-1VMS\s0 versions before v7.0 and \s-1DEC C\s0 versions
before v6.0 has been removed.

- \(bu
Since Perl 5.10.1, the home-grown \f(CW\*(C`stat\*(C' wrapper has been unable to
distinguish between a directory name containing an underscore and an
otherwise-identical filename containing a dot in the same position
(e.g., t/test_pl as a directory and t/test.pl as a file).  This problem
has been corrected.

- \(bu
The build on \s-1VMS\s0 now permits names of the resulting symbols in C code for
Perl longer than 31 characters.  Symbols like
\f(CW\*(C`Perl_\|_it_was_the_best_of_times_it_was_the_worst_of_times\*(C' can now be
created freely without causing the \s-1VMS\s0 linker to seize up.

*GNU/Hurd*
Subsection "GNU/Hurd"

- \(bu
Numerous build and test failures on GNU/Hurd have been resolved with hints
for building \s-1DBM\s0 modules, detection of the library search path, and enabling
of large file support.

*OpenVOS*
Subsection "OpenVOS"

- \(bu
Perl is now built with dynamic linking on OpenVOS, the minimum supported
version of which is now Release 17.1.0.

*SunOS*
Subsection "SunOS"

The \s-1CC\s0 workshop \*(C+ compiler is now detected and used on systems that ship
without cc.

## Internal Changes

Header "Internal Changes"

- \(bu
The compiled representation of formats is now stored via the \f(CW\*(C`mg_ptr\*(C' of
their \f(CW\*(C`PERL_MAGIC_fm\*(C'.  Previously it was stored in the string buffer,
beyond \f(CW\*(C`SvLEN()\*(C', the regular end of the string.  \f(CW\*(C`SvCOMPILED()\*(C' and
\f(CW\*(C`SvCOMPILED_\{on,off\}()\*(C' now exist solely for compatibility for \s-1XS\s0 code.
The first is always 0, the other two now no-ops. (5.14.1)

- \(bu
Some global variables have been marked \f(CW\*(C`const\*(C', members in the interpreter
structure have been re-ordered, and the opcodes have been re-ordered.  The
op \f(CW\*(C`OP_AELEMFAST\*(C' has been split into \f(CW\*(C`OP_AELEMFAST\*(C' and \f(CW\*(C`OP_AELEMFAST_LEX\*(C'.

- \(bu
When empting a hash of its elements (e.g., via undef(%h), or \f(CW%h=()), HvARRAY
field is no longer temporarily zeroed.  Any destructors called on the freed
elements see the remaining elements.  Thus, \f(CW%h=() becomes more like
\f(CW\*(C`delete $h\{$_\} for keys %h\*(C'.

- \(bu
Boyer-Moore compiled scalars are now PVMGs, and the Boyer-Moore tables are now
stored via the mg_ptr of their \f(CW\*(C`PERL_MAGIC_bm\*(C'.
Previously they were PVGVs, with the tables stored in
the string buffer, beyond \f(CW\*(C`SvLEN()\*(C'.  This eliminates
the last place where the core stores data beyond \f(CW\*(C`SvLEN()\*(C'.

- \(bu
Simplified logic in \f(CW\*(C`Perl_sv_magic()\*(C' introduces a small change of
behavior for error cases involving unknown magic types.  Previously, if
\f(CW\*(C`Perl_sv_magic()\*(C' was passed a magic type unknown to it, it would

> 
- 1.
Croak \*(L"Modification of a read-only value attempted\*(R" if read only

- 2.
Return without error if the \s-1SV\s0 happened to already have this magic

- 3.
otherwise croak \*(L"Don't know how to handle magic of type \\\\%o\*(R"



> .Sp
Now it will always croak \*(L"Don't know how to handle magic of type \\\\%o\*(R", even
on read-only values, or SVs which already have the unknown magic type.



- \(bu
The experimental \f(CW\*(C`fetch_cop_label\*(C' function has been renamed to
\f(CW\*(C`cop_fetch_label\*(C'.

- \(bu
The \f(CW\*(C`cop_store_label\*(C' function has been added to the \s-1API,\s0 but is
experimental.

- \(bu
*embedvar.h* has been simplified, and one level of macro indirection for
PL_* variables has been removed for the default (non-multiplicity)
configuration.  PERLVAR*() macros now directly expand their arguments to
tokens such as \f(CW\*(C`PL_defgv\*(C', instead of expanding to \f(CW\*(C`PL_Idefgv\*(C', with
*embedvar.h* defining a macro to map \f(CW\*(C`PL_Idefgv\*(C' to \f(CW\*(C`PL_defgv\*(C'.  \s-1XS\s0 code
which has unwarranted chumminess with the implementation may need updating.

- \(bu
An \s-1API\s0 has been added to explicitly choose whether to export \s-1XSUB\s0
symbols.  More detail can be found in the comments for commit e64345f8.

- \(bu
The \f(CW\*(C`is_gv_magical_sv\*(C' function has been eliminated and merged with
\f(CW\*(C`gv_fetchpvn_flags\*(C'.  It used to be called to determine whether a \s-1GV\s0
should be autovivified in rvalue context.  Now it has been replaced with a
new \f(CW\*(C`GV_ADDMG\*(C' flag (not part of the \s-1API\s0).

- \(bu
The returned code point from the function \f(CW\*(C`utf8n_to_uvuni()\*(C'
when the input is malformed \s-1UTF-8,\s0 malformations are allowed, and
\f(CW\*(C`utf8\*(C' warnings are off is now the Unicode \s-1REPLACEMENT CHARACTER\s0
whenever the malformation is such that no well-defined code point can be
computed.  Previously the returned value was essentially garbage.  The
only malformations that have well-defined values are a zero-length
string (0 is the return), and overlong \s-1UTF-8\s0 sequences.

- \(bu
Padlists are now marked \f(CW\*(C`AvREAL\*(C'; i.e., reference-counted.  They have
always been reference-counted, but were not marked real, because *pad.c*
did its own clean-up, instead of using the usual clean-up code in *sv.c*.
That caused problems in thread cloning, so now the \f(CW\*(C`AvREAL\*(C' flag is on,
but is turned off in *pad.c* right before the padlist is freed (after
*pad.c* has done its custom freeing of the pads).

- \(bu
All C files that make up the Perl core have been converted to \s-1UTF-8.\s0

- \(bu
These new functions have been added as part of the work on Unicode symbols:
.Sp
.Vb 10
    HvNAMELEN
    HvNAMEUTF8
    HvENAMELEN
    HvENAMEUTF8
    gv_init_pv
    gv_init_pvn
    gv_init_pvsv
    gv_fetchmeth_pv
    gv_fetchmeth_pvn
    gv_fetchmeth_sv
    gv_fetchmeth_pv_autoload
    gv_fetchmeth_pvn_autoload
    gv_fetchmeth_sv_autoload
    gv_fetchmethod_pv_flags
    gv_fetchmethod_pvn_flags
    gv_fetchmethod_sv_flags
    gv_autoload_pv
    gv_autoload_pvn
    gv_autoload_sv
    newGVgen_flags
    sv_derived_from_pv
    sv_derived_from_pvn
    sv_derived_from_sv
    sv_does_pv
    sv_does_pvn
    sv_does_sv
    whichsig_pv
    whichsig_pvn
    whichsig_sv
    newCONSTSUB_flags
.Ve
.Sp
The gv_fetchmethod_*_flags functions, like gv_fetchmethod_flags, are
experimental and may change in a future release.

- \(bu
The following functions were added.  These are *not* part of the \s-1API:\s0
.Sp
.Vb 9
    GvNAMEUTF8
    GvENAMELEN
    GvENAME_HEK
    CopSTASH_flags
    CopSTASH_flags_set
    PmopSTASH_flags
    PmopSTASH_flags_set
    sv_sethek
    HEKfARG
.Ve
.Sp
There is also a \f(CW\*(C`HEKf\*(C' macro corresponding to \f(CW\*(C`SVf\*(C', for
interpolating HEKs in formatted strings.

- \(bu
\f(CW\*(C`sv_catpvn_flags\*(C' takes a couple of new internal-only flags,
\f(CW\*(C`SV_CATBYTES\*(C' and \f(CW\*(C`SV_CATUTF8\*(C', which tell it whether the char array to
be concatenated is \s-1UTF8.\s0  This allows for more efficient concatenation than
creating temporary SVs to pass to \f(CW\*(C`sv_catsv\*(C'.

- \(bu
For \s-1XS AUTOLOAD\s0 subs, \f(CW$AUTOLOAD is set once more, as it was in 5.6.0.  This
is in addition to setting \f(CW\*(C`SvPVX(cv)\*(C', for compatibility with 5.8 to 5.14.
See \*(L"Autoloading with XSUBs\*(R" in perlguts.

- \(bu
Perl now checks whether the array (the linearized isa) returned by a \s-1MRO\s0
plugin begins with the name of the class itself, for which the array was
created, instead of assuming that it does.  This prevents the first element
from being skipped during method lookup.  It also means that
\f(CW\*(C`mro::get_linear_isa\*(C' may return an array with one more element than the
\s-1MRO\s0 plugin provided [perl #94306].

- \(bu
\f(CW\*(C`PL_curstash\*(C' is now reference-counted.

- \(bu
There are now feature bundle hints in \f(CW\*(C`PL_hints\*(C' (\f(CW$^H) that version
declarations use, to avoid having to load *feature.pm*.  One setting of
the hint bits indicates a \*(L"custom\*(R" feature bundle, which means that the
entries in \f(CW\*(C`%^H\*(C' still apply.  *feature.pm* uses that.
.Sp
The \f(CW\*(C`HINT_FEATURE_MASK\*(C' macro is defined in *perl.h* along with other
hints.  Other macros for setting and testing features and bundles are in
the new *feature.h*.  \f(CW\*(C`FEATURE_IS_ENABLED\*(C' (which has moved to
*feature.h*) is no longer used throughout the codebase, but more specific
macros, e.g., \f(CW\*(C`FEATURE_SAY_IS_ENABLED\*(C', that are defined in *feature.h*.

- \(bu
*lib/feature.pm* is now a generated file, created by the new
*regen/feature.pl* script, which also generates *feature.h*.

- \(bu
Tied arrays are now always \f(CW\*(C`AvREAL\*(C'.  If \f(CW@_ or \f(CW\*(C`DB::args\*(C' is tied, it
is reified first, to make sure this is always the case.

- \(bu
Two new functions \f(CW\*(C`utf8_to_uvchr_buf()\*(C' and \f(CW\*(C`utf8_to_uvuni_buf()\*(C' have
been added.  These are the same as \f(CW\*(C`utf8_to_uvchr\*(C' and
\f(CW\*(C`utf8_to_uvuni\*(C' (which are now deprecated), but take an extra parameter
that is used to guard against reading beyond the end of the input
string.
See \*(L"utf8_to_uvchr_buf\*(R" in perlapi and \*(L"utf8_to_uvuni_buf\*(R" in perlapi.

- \(bu
The regular expression engine now does \s-1TRIE\s0 case insensitive matches
under Unicode. This may change the output of \f(CW\*(C`use re \*(Aqdebug\*(Aq;\*(C',
and will speed up various things.

- \(bu
There is a new \f(CW\*(C`wrap_op_checker()\*(C' function, which provides a thread-safe
alternative to writing to \f(CW\*(C`PL_check\*(C' directly.

## Selected Bug Fixes

Header "Selected Bug Fixes"

### Array and hash

Subsection "Array and hash"

- \(bu
A bug has been fixed that would cause a \*(L"Use of freed value in iteration\*(R"
error if the next two hash elements that would be iterated over are
deleted [perl #85026]. (5.14.1)

- \(bu
Deleting the current hash iterator (the hash element that would be returned
by the next call to \f(CW\*(C`each\*(C') in void context used not to free it
[perl #85026].

- \(bu
Deletion of methods via \f(CW\*(C`delete $Class::\{method\}\*(C' syntax used to update
method caches if called in void context, but not scalar or list context.

- \(bu
When hash elements are deleted in void context, the internal hash entry is
now freed before the value is freed, to prevent destructors called by that
latter freeing from seeing the hash in an inconsistent state.  It was
possible to cause double-frees if the destructor freed the hash itself
[perl #100340].

- \(bu
A \f(CW\*(C`keys\*(C' optimization in Perl 5.12.0 to make it faster on empty hashes
caused \f(CW\*(C`each\*(C' not to reset the iterator if called after the last element
was deleted.

- \(bu
Freeing deeply nested hashes no longer crashes [perl #44225].

- \(bu
It is possible from \s-1XS\s0 code to create hashes with elements that have no
values.  The hash element and slice operators used to crash
when handling these in lvalue context.  They now
produce a \*(L"Modification of non-creatable hash value attempted\*(R" error
message.

- \(bu
If list assignment to a hash or array triggered destructors that freed the
hash or array itself, a crash would ensue.  This is no longer the case
[perl #107440].

- \(bu
It used to be possible to free the typeglob of a localized array or hash
(e.g., \f(CW\*(C`local @\{"x"\}; delete $::\{x\}\*(C'), resulting in a crash on scope exit.

- \(bu
Some core bugs affecting Hash::Util have been fixed: locking a hash
element that is a glob copy no longer causes the next assignment to it to
corrupt the glob (5.14.2), and unlocking a hash element that holds a
copy-on-write scalar no longer causes modifications to that scalar to
modify other scalars that were sharing the same string buffer.

### C \s-1API\s0 fixes

Subsection "C API fixes"

- \(bu
The \f(CW\*(C`newHVhv\*(C' \s-1XS\s0 function now works on tied hashes, instead of crashing or
returning an empty hash.

- \(bu
The \f(CW\*(C`SvIsCOW\*(C' C macro now returns false for read-only copies of typeglobs,
such as those created by:
.Sp
.Vb 2
  $hash\{elem\} = *foo;
  Hash::Util::lock_value %hash, \*(Aqelem\*(Aq;
.Ve
.Sp
It used to return true.

- \(bu
The \f(CW\*(C`SvPVutf8\*(C' C function no longer tries to modify its argument,
resulting in errors [perl #108994].

- \(bu
\f(CW\*(C`SvPVutf8\*(C' now works properly with magical variables.

- \(bu
\f(CW\*(C`SvPVbyte\*(C' now works properly non-PVs.

- \(bu
When presented with malformed \s-1UTF-8\s0 input, the XS-callable functions
\f(CW\*(C`is_utf8_string()\*(C', \f(CW\*(C`is_utf8_string_loc()\*(C', and
\f(CW\*(C`is_utf8_string_loclen()\*(C' could read beyond the end of the input
string by up to 12 bytes.  This no longer happens.  [perl #32080].
However, currently, \f(CW\*(C`is_utf8_char()\*(C' still has this defect, see
\*(L"**is_utf8_char()**\*(R" above.

- \(bu
The C-level \f(CW\*(C`pregcomp\*(C' function could become confused about whether the
pattern was in \s-1UTF8\s0 if the pattern was an overloaded, tied, or otherwise
magical scalar [perl #101940].

### Compile-time hints

Subsection "Compile-time hints"

- \(bu
Tying \f(CW\*(C`%^H\*(C' no longer causes perl to crash or ignore the contents of
\f(CW\*(C`%^H\*(C' when entering a compilation scope [perl #106282].

- \(bu
\f(CW\*(C`eval $string\*(C' and \f(CW\*(C`require\*(C' used not to
localize \f(CW\*(C`%^H\*(C' during compilation if it
was empty at the time the \f(CW\*(C`eval\*(C' call itself was compiled.  This could
lead to scary side effects, like \f(CW\*(C`use re "/m"\*(C' enabling other flags that
the surrounding code was trying to enable for its caller [perl #68750].

- \(bu
\f(CW\*(C`eval $string\*(C' and \f(CW\*(C`require\*(C' no longer localize hints (\f(CW$^H and \f(CW\*(C`%^H\*(C')
at run time, but only during compilation of the \f(CW$string or required file.
This makes \f(CW\*(C`BEGIN \{ $^H\{foo\}=7 \}\*(C' equivalent to
\f(CW\*(C`BEGIN \{ eval \*(Aq$^H\{foo\}=7\*(Aq \}\*(C' [perl #70151].

- \(bu
Creating a \s-1BEGIN\s0 block from \s-1XS\s0 code (via \f(CW\*(C`newXS\*(C' or \f(CW\*(C`newATTRSUB\*(C') would,
on completion, make the hints of the current compiling code the current
hints.  This could cause warnings to occur in a non-warning scope.

### Copy-on-write scalars

Subsection "Copy-on-write scalars"
Copy-on-write or shared hash key scalars
were introduced in 5.8.0, but most Perl code
did not encounter them (they were used mostly internally).  Perl
5.10.0 extended them, such that assigning \f(CW\*(C`_\|_PACKAGE_\|_\*(C' or a
hash key to a scalar would make it copy-on-write.  Several parts
of Perl were not updated to account for them, but have now been fixed.

- \(bu
\f(CW\*(C`utf8::decode\*(C' had a nasty bug that would modify copy-on-write scalars'
string buffers in place (i.e., skipping the copy).  This could result in
hashes having two elements with the same key [perl #91834]. (5.14.2)

- \(bu
Lvalue subroutines were not allowing \s-1COW\s0 scalars to be returned.  This was
fixed for lvalue scalar context in Perl 5.12.3 and 5.14.0, but list context
was not fixed until this release.

- \(bu
Elements of restricted hashes (see the fields pragma) containing
copy-on-write values couldn't be deleted, nor could such hashes be cleared
(\f(CW\*(C`%hash = ()\*(C'). (5.14.2)

- \(bu
Localizing a tied variable used to make it read-only if it contained a
copy-on-write string. (5.14.2)

- \(bu
Assigning a copy-on-write string to a stash
element no longer causes a double free.  Regardless of this change, the
results of such assignments are still undefined.

- \(bu
Assigning a copy-on-write string to a tied variable no longer stops that
variable from being tied if it happens to be a \s-1PVMG\s0 or \s-1PVLV\s0 internally.

- \(bu
Doing a substitution on a tied variable returning a copy-on-write
scalar used to cause an assertion failure or an \*(L"Attempt to free
nonexistent shared string\*(R" warning.

- \(bu
This one is a regression from 5.12: In 5.14.0, the bitwise assignment
operators \f(CW\*(C`|=\*(C', \f(CW\*(C`^=\*(C' and \f(CW\*(C`&=\*(C' started leaving the left-hand side
undefined if it happened to be a copy-on-write string [perl #108480].

- \(bu
Storable, Devel::Peek and PerlIO::scalar had similar problems.
See \*(L"Updated Modules and Pragmata\*(R", above.

### The debugger

Subsection "The debugger"

- \(bu
*dumpvar.pl*, and therefore the \f(CW\*(C`x\*(C' command in the debugger, have been
fixed to handle objects blessed into classes whose names contain \*(L"=\*(R".  The
contents of such objects used not to be dumped [perl #101814].

- \(bu
The \*(L"R\*(R" command for restarting a debugger session has been fixed to work on
Windows, or any other system lacking a \f(CW\*(C`POSIX::_SC_OPEN_MAX\*(C' constant
[perl #87740].

- \(bu
The \f(CW\*(C`#line 42 foo\*(C' directive used not to update the arrays of lines used
by the debugger if it occurred in a string eval.  This was partially fixed
in 5.14, but it worked only for a single \f(CW\*(C`#line 42 foo\*(C' in each eval.  Now
it works for multiple.

- \(bu
When subroutine calls are intercepted by the debugger, the name of the
subroutine or a reference to it is stored in \f(CW$DB::sub, for the debugger
to access.  Sometimes (such as \f(CW\*(C`$foo = *bar; undef *bar; &$foo\*(C')
\f(CW$DB::sub would be set to a name that could not be used to find the
subroutine, and so the debugger's attempt to call it would fail.  Now the
check to see whether a reference is needed is more robust, so those
problems should not happen anymore [rt.cpan.org #69862].

- \(bu
Every subroutine has a filename associated with it that the debugger uses.
The one associated with constant subroutines used to be misallocated when
cloned under threads.  Consequently, debugging threaded applications could
result in memory corruption [perl #96126].

### Dereferencing operators

Subsection "Dereferencing operators"

- \(bu
\f(CW\*(C`defined($\{"..."\})\*(C', \f(CW\*(C`defined(*\{"..."\})\*(C', etc., used to
return true for most, but not all built-in variables, if
they had not been used yet.  This bug affected \f(CW\*(C`$\{^GLOBAL_PHASE\}\*(C' and
\f(CW\*(C`$\{^UTF8CACHE\}\*(C', among others.  It also used to return false if the
package name was given as well (\f(CW\*(C`$\{"::!"\}\*(C') [perl #97978, #97492].

- \(bu
Perl 5.10.0 introduced a similar bug: \f(CW\*(C`defined(*\{"foo"\})\*(C' where \*(L"foo\*(R"
represents the name of a built-in global variable used to return false if
the variable had never been used before, but only on the *first* call.
This, too, has been fixed.

- \(bu
Since 5.6.0, \f(CW\*(C`*\{ ... \}\*(C' has been inconsistent in how it treats undefined
values.  It would die in strict mode or lvalue context for most undefined
values, but would be treated as the empty string (with a warning) for the
specific scalar return by \f(CW\*(C`undef()\*(C' (\f(CW&PL_sv_undef internally).  This
has been corrected.  \f(CW\*(C`undef()\*(C' is now treated like other undefined
scalars, as in Perl 5.005.

### Filehandle, last-accessed

Subsection "Filehandle, last-accessed"
Perl has an internal variable that stores the last filehandle to be
accessed.  It is used by \f(CW$. and by \f(CW\*(C`tell\*(C' and \f(CW\*(C`eof\*(C' without
arguments.

- \(bu
It used to be possible to set this internal variable to a glob copy and
then modify that glob copy to be something other than a glob, and still
have the last-accessed filehandle associated with the variable after
assigning a glob to it again:
.Sp
.Vb 4
    my $foo = *STDOUT;  # $foo is a glob copy
    <$foo>;             # $foo is now the last-accessed handle
    $foo = 3;           # no longer a glob
    $foo = *STDERR;     # still the last-accessed handle
.Ve
.Sp
Now the \f(CW\*(C`$foo = 3\*(C' assignment unsets that internal variable, so there
is no last-accessed filehandle, just as if \f(CW\*(C`<$foo>\*(C' had never
happened.
.Sp
This also prevents some unrelated handle from becoming the last-accessed
handle if \f(CW$foo falls out of scope and the same internal \s-1SV\s0 gets used for
another handle [perl #97988].

- \(bu
A regression in 5.14 caused these statements not to set that internal
variable:
.Sp
.Vb 8
    my $fh = *STDOUT;
    tell $fh;
    eof  $fh;
    seek $fh, 0,0;
    tell     *$fh;
    eof      *$fh;
    seek     *$fh, 0,0;
    readline *$fh;
.Ve
.Sp
This is now fixed, but \f(CW\*(C`tell *\{ *$fh \}\*(C' still has the problem, and it
is not clear how to fix it [perl #106536].
.ie n .SS "Filetests and ""stat"""
.el .SS "Filetests and \f(CWstat"
Subsection "Filetests and stat"
The term \*(L"filetests\*(R" refers to the operators that consist of a hyphen
followed by a single letter: \f(CW\*(C`-r\*(C', \f(CW\*(C`-x\*(C', \f(CW\*(C`-M\*(C', etc.  The term \*(L"stacked\*(R"
when applied to filetests means followed by another filetest operator
sharing the same operand, as in \f(CW\*(C`-r -x -w $fooo\*(C'.

- \(bu
\f(CW\*(C`stat\*(C' produces more consistent warnings.  It no longer warns for \*(L"_\*(R"
[perl #71002] and no longer skips the warning at times for other unopened
handles.  It no longer warns about an unopened handle when the operating
system's \f(CW\*(C`fstat\*(C' function fails.

- \(bu
\f(CW\*(C`stat\*(C' would sometimes return negative numbers for large inode numbers,
because it was using the wrong internal C type. [perl #84590]

- \(bu
\f(CW\*(C`lstat\*(C' is documented to fall back to \f(CW\*(C`stat\*(C' (with a warning) when given
a filehandle.  When passed an \s-1IO\s0 reference, it was actually doing the
equivalent of \f(CW\*(C`stat\ _\*(C' and ignoring the handle.

- \(bu
\f(CW\*(C`-T _\*(C' with no preceding \f(CW\*(C`stat\*(C' used to produce a
confusing \*(L"uninitialized\*(R" warning, even though there
is no visible uninitialized value to speak of.

- \(bu
\f(CW\*(C`-T\*(C', \f(CW\*(C`-B\*(C', \f(CW\*(C`-l\*(C' and \f(CW\*(C`-t\*(C' now work
when stacked with other filetest operators
[perl #77388].

- \(bu
In 5.14.0, filetest ops (\f(CW\*(C`-r\*(C', \f(CW\*(C`-x\*(C', etc.) started calling \s-1FETCH\s0 on a
tied argument belonging to the previous argument to a list operator, if
called with a bareword argument or no argument at all.  This has been
fixed, so \f(CW\*(C`push @foo, $tied, -r\*(C' no longer calls \s-1FETCH\s0 on \f(CW$tied.

- \(bu
In Perl 5.6, \f(CW\*(C`-l\*(C' followed by anything other than a bareword would treat
its argument as a file name.  That was changed in 5.8 for glob references
(\f(CW\*(C`\\*foo\*(C'), but not for globs themselves (\f(CW*foo).  \f(CW\*(C`-l\*(C' started
returning \f(CW\*(C`undef\*(C' for glob references without setting the last
stat buffer that the \*(L"_\*(R" handle uses, but only if warnings
were turned on.  With warnings off, it was the same as 5.6.
In other words, it was simply buggy and inconsistent.  Now the 5.6
behavior has been restored.

- \(bu
\f(CW\*(C`-l\*(C' followed by a bareword no longer \*(L"eats\*(R" the previous argument to
the list operator in whose argument list it resides.  Hence,
\f(CW\*(C`print "bar", -l foo\*(C' now actually prints \*(L"bar\*(R", because \f(CW\*(C`-l\*(C'
on longer eats it.

- \(bu
Perl keeps several internal variables to keep track of the last stat
buffer, from which file(handle) it originated, what type it was, and
whether the last stat succeeded.
.Sp
There were various cases where these could get out of synch, resulting in
inconsistent or erratic behavior in edge cases (every mention of \f(CW\*(C`-T\*(C'
applies to \f(CW\*(C`-B\*(C' as well):

> 
- \(bu
\f(CW\*(C`-T \f(CIHANDLE\f(CW\*(C', even though it does a \f(CW\*(C`stat\*(C', was not resetting the last
stat type, so an \f(CW\*(C`lstat _\*(C' following it would merrily return the wrong
results.  Also, it was not setting the success status.

- \(bu
Freeing the handle last used by \f(CW\*(C`stat\*(C' or a filetest could result in
\f(CW\*(C`-T\ _\*(C' using an unrelated handle.

- \(bu
\f(CW\*(C`stat\*(C' with an \s-1IO\s0 reference would not reset the stat type or record the
filehandle for \f(CW\*(C`-T\ _\*(C' to use.

- \(bu
Fatal warnings could cause the stat buffer not to be reset
for a filetest operator on an unopened filehandle or \f(CW\*(C`-l\*(C' on any handle.
Fatal warnings also stopped \f(CW\*(C`-T\*(C' from setting \f(CW$!.

- \(bu
When the last stat was on an unreadable file, \f(CW\*(C`-T _\*(C' is supposed to
return \f(CW\*(C`undef\*(C', leaving the last stat buffer unchanged.  But it was
setting the stat type, causing \f(CW\*(C`lstat _\*(C' to stop working.

- \(bu
\f(CW\*(C`-T \f(CIFILENAME\f(CW\*(C' was not resetting the internal stat buffers for
unreadable files.



> .Sp
These have all been fixed.



### Formats

Subsection "Formats"

- \(bu
Several edge cases have been fixed with formats and \f(CW\*(C`formline\*(C';
in particular, where the format itself is potentially variable (such as
with ties and overloading), and where the format and data differ in their
encoding.  In both these cases, it used to possible for the output to be
corrupted [perl #91032].

- \(bu
\f(CW\*(C`formline\*(C' no longer converts its argument into a string in-place.  So
passing a reference to \f(CW\*(C`formline\*(C' no longer destroys the reference
[perl #79532].

- \(bu
Assignment to \f(CW$^A (the format output accumulator) now recalculates
the number of lines output.
.ie n .SS """given"" and ""when"""
.el .SS "\f(CWgiven and \f(CWwhen"
Subsection "given and when"

- \(bu
\f(CW\*(C`given\*(C' was not scoping its implicit \f(CW$_ properly, resulting in memory
leaks or \*(L"Variable is not available\*(R" warnings [perl #94682].

- \(bu
\f(CW\*(C`given\*(C' was not calling set-magic on the implicit lexical \f(CW$_ that it
uses.  This meant, for example, that \f(CW\*(C`pos\*(C' would be remembered from one
execution of the same \f(CW\*(C`given\*(C' block to the next, even if the input were a
different variable [perl #84526].

- \(bu
\f(CW\*(C`when\*(C' blocks are now capable of returning variables declared inside the
enclosing \f(CW\*(C`given\*(C' block [perl #93548].
.ie n .SS "The ""glob"" operator"
.el .SS "The \f(CWglob operator"
Subsection "The glob operator"

- \(bu
On OSes other than \s-1VMS,\s0 Perl's \f(CW\*(C`glob\*(C' operator (and the \f(CW\*(C`<...>\*(C' form)
use File::Glob underneath.  File::Glob splits the pattern into words,
before feeding each word to its \f(CW\*(C`bsd_glob\*(C' function.
.Sp
There were several inconsistencies in the way the split was done.  Now
quotation marks (' and ") are always treated as shell-style word delimiters
(that allow whitespace as part of a word) and backslashes are always
preserved, unless they exist to escape quotation marks.  Before, those
would only sometimes be the case, depending on whether the pattern
contained whitespace.  Also, escaped whitespace at the end of the pattern
is no longer stripped [perl #40470].

- \(bu
\f(CW\*(C`CORE::glob\*(C' now works as a way to call the default globbing function.  It
used to respect overrides, despite the \f(CW\*(C`CORE::\*(C' prefix.

- \(bu
Under miniperl (used to configure modules when perl itself is built),
\f(CW\*(C`glob\*(C' now clears \f(CW%ENV before calling csh, since the latter croaks on some
systems if it does not like the contents of the \s-1LS_COLORS\s0 environment
variable [perl #98662].

### Lvalue subroutines

Subsection "Lvalue subroutines"

- \(bu
Explicit return now returns the actual argument passed to return, instead
of copying it [perl #72724, #72706].

- \(bu
Lvalue subroutines used to enforce lvalue syntax (i.e., whatever can go on
the left-hand side of \f(CW\*(C`=\*(C') for the last statement and the arguments to
return.  Since lvalue subroutines are not always called in lvalue context,
this restriction has been lifted.

- \(bu
Lvalue subroutines are less restrictive about what values can be returned.
It used to croak on values returned by \f(CW\*(C`shift\*(C' and \f(CW\*(C`delete\*(C' and from
other subroutines, but no longer does so [perl #71172].

- \(bu
Empty lvalue subroutines (\f(CW\*(C`sub :lvalue \{\}\*(C') used to return \f(CW@_ in list
context.  All subroutines used to do this, but regular subs were fixed in
Perl 5.8.2.  Now lvalue subroutines have been likewise fixed.

- \(bu
Autovivification now works on values returned from lvalue subroutines
[perl #7946], as does returning \f(CW\*(C`keys\*(C' in lvalue context.

- \(bu
Lvalue subroutines used to copy their return values in rvalue context.  Not
only was this a waste of \s-1CPU\s0 cycles, but it also caused bugs.  A \f(CW\*(C`($)\*(C'
prototype would cause an lvalue sub to copy its return value [perl #51408],
and \f(CW\*(C`while(lvalue_sub() =~ m/.../g) \{ ... \}\*(C' would loop endlessly
[perl #78680].

- \(bu
When called in potential lvalue context
(e.g., subroutine arguments or a list
passed to \f(CW\*(C`for\*(C'), lvalue subroutines used to copy
any read-only value that was returned.  E.g., \f(CW\*(C` sub :lvalue \{ $] \} \*(C'
would not return \f(CW$], but a copy of it.

- \(bu
When called in potential lvalue context, an lvalue subroutine returning
arrays or hashes used to bind the arrays or hashes to scalar variables,
resulting in bugs.  This was fixed in 5.14.0 if an array were the first
thing returned from the subroutine (but not for \f(CW\*(C`$scalar, @array\*(C' or
hashes being returned).  Now a more general fix has been applied
[perl #23790].

- \(bu
Method calls whose arguments were all surrounded with \f(CW\*(C`my()\*(C' or \f(CW\*(C`our()\*(C'
(as in \f(CW\*(C`$object->method(my($a,$b))\*(C') used to force lvalue context on
the subroutine.  This would prevent lvalue methods from returning certain
values.

- \(bu
Lvalue sub calls that are not determined to be such at compile time
(\f(CW&$name or &\{\*(L"name\*(R"\}) are no longer exempt from strict refs if they
occur in the last statement of an lvalue subroutine [perl #102486].

- \(bu
Sub calls whose subs are not visible at compile time, if
they occurred in the last statement of an lvalue subroutine,
would reject non-lvalue subroutines and die with \*(L"Can't modify non-lvalue
subroutine call\*(R" [perl #102486].
.Sp
Non-lvalue sub calls whose subs *are* visible at compile time exhibited
the opposite bug.  If the call occurred in the last statement of an lvalue
subroutine, there would be no error when the lvalue sub was called in
lvalue context.  Perl would blindly assign to the temporary value returned
by the non-lvalue subroutine.

- \(bu
\f(CW\*(C`AUTOLOAD\*(C' routines used to take precedence over the actual sub being
called (i.e., when autoloading wasn't needed), for sub calls in lvalue or
potential lvalue context, if the subroutine was not visible at compile
time.

- \(bu
Applying the \f(CW\*(C`:lvalue\*(C' attribute to an \s-1XSUB\s0 or to an aliased subroutine
stub with \f(CW\*(C`sub foo :lvalue;\*(C' syntax stopped working in Perl 5.12.
This has been fixed.

- \(bu
Applying the :lvalue attribute to subroutine that is already defined does
not work properly, as the attribute changes the way the sub is compiled.
Hence, Perl 5.12 began warning when an attempt is made to apply the
attribute to an already defined sub.  In such cases, the attribute is
discarded.
.Sp
But the change in 5.12 missed the case where custom attributes are also
present: that case still silently and ineffectively applied the attribute.
That omission has now been corrected.  \f(CW\*(C`sub foo :lvalue :Whatever\*(C' (when
\f(CW\*(C`foo\*(C' is already defined) now warns about the :lvalue attribute, and does
not apply it.

- \(bu
A bug affecting lvalue context propagation through nested lvalue subroutine
calls has been fixed.  Previously, returning a value in nested rvalue
context would be treated as lvalue context by the inner subroutine call,
resulting in some values (such as read-only values) being rejected.

### Overloading

Subsection "Overloading"

- \(bu
Arithmetic assignment (\f(CW\*(C`$left += $right\*(C') involving overloaded objects
that rely on the 'nomethod' override no longer segfault when the left
operand is not overloaded.

- \(bu
Errors that occur when methods cannot be found during overloading now
mention the correct package name, as they did in 5.8.x, instead of
erroneously mentioning the \*(L"overload\*(R" package, as they have since 5.10.0.

- \(bu
Undefining \f(CW%overload:: no longer causes a crash.

### Prototypes of built-in keywords

Subsection "Prototypes of built-in keywords"

- \(bu
The \f(CW\*(C`prototype\*(C' function no longer dies for the \f(CW\*(C`_\|_FILE_\|_\*(C', \f(CW\*(C`_\|_LINE_\|_\*(C'
and \f(CW\*(C`_\|_PACKAGE_\|_\*(C' directives.  It now returns an empty-string prototype
for them, because they are syntactically indistinguishable from nullary
functions like \f(CW\*(C`time\*(C'.

- \(bu
\f(CW\*(C`prototype\*(C' now returns \f(CW\*(C`undef\*(C' for all overridable infix operators,
such as \f(CW\*(C`eq\*(C', which are not callable in any way resembling functions.
It used to return incorrect prototypes for some and die for others
[perl #94984].

- \(bu
The prototypes of several built-in functions\*(--\f(CW\*(C`getprotobynumber\*(C', \f(CW\*(C`lock\*(C',
\f(CW\*(C`not\*(C' and \f(CW\*(C`select\*(C'--have been corrected, or at least are now closer to
reality than before.

### Regular expressions

Subsection "Regular expressions"

- \(bu
\f(CW\*(C`/[[:ascii:]]/\*(C' and \f(CW\*(C`/[[:blank:]]/\*(C' now use locale rules under
\f(CW\*(C`use locale\*(C' when the platform supports that.  Previously, they used
the platform's native character set.

- \(bu
\f(CW\*(C`m/[[:ascii:]]/i\*(C' and \f(CW\*(C`/\\p\{ASCII\}/i\*(C' now match identically (when not
under a differing locale).  This fixes a regression introduced in 5.14
in which the first expression could match characters outside of \s-1ASCII,\s0
such as the \s-1KELVIN SIGN.\s0

- \(bu
\f(CW\*(C`/.*/g\*(C' would sometimes refuse to match at the end of a string that ends
with \*(L"\\n\*(R".  This has been fixed [perl #109206].

- \(bu
Starting with 5.12.0, Perl used to get its internal bookkeeping muddled up
after assigning \f(CW\*(C`$\{ qr// \}\*(C' to a hash element and locking it with
Hash::Util.  This could result in double frees, crashes, or erratic
behavior.

- \(bu
The new (in 5.14.0) regular expression modifier \f(CW\*(C`/a\*(C' when repeated like
\f(CW\*(C`/aa\*(C' forbids the characters outside the \s-1ASCII\s0 range that match
characters inside that range from matching under \f(CW\*(C`/i\*(C'.  This did not
work under some circumstances, all involving alternation, such as:
.Sp
.Vb 1
 "\\N\{KELVIN SIGN\}" =~ /k|foo/iaa;
.Ve
.Sp
succeeded inappropriately.  This is now fixed.

- \(bu
5.14.0 introduced some memory leaks in regular expression character
classes such as \f(CW\*(C`[\\w\\s]\*(C', which have now been fixed. (5.14.1)

- \(bu
An edge case in regular expression matching could potentially loop.
This happened only under \f(CW\*(C`/i\*(C' in bracketed character classes that have
characters with multi-character folds, and the target string to match
against includes the first portion of the fold, followed by another
character that has a multi-character fold that begins with the remaining
portion of the fold, plus some more.
.Sp
.Vb 1
 "s\\N\{U+DF\}" =~ /[\\x\{DF\}foo]/i
.Ve
.Sp
is one such case.  \f(CW\*(C`\\xDF\*(C' folds to \f(CW"ss". (5.14.1)

- \(bu
A few characters in regular expression pattern matches did not
match correctly in some circumstances, all involving \f(CW\*(C`/i\*(C'.  The
affected characters are:
\s-1COMBINING GREEK YPOGEGRAMMENI,
GREEK CAPITAL LETTER IOTA,
GREEK CAPITAL LETTER UPSILON,
GREEK PROSGEGRAMMENI,
GREEK SMALL LETTER IOTA WITH DIALYTIKA AND OXIA,
GREEK SMALL LETTER IOTA WITH DIALYTIKA AND TONOS,
GREEK SMALL LETTER UPSILON WITH DIALYTIKA AND OXIA,
GREEK SMALL LETTER UPSILON WITH DIALYTIKA AND TONOS,
LATIN SMALL LETTER LONG S,
LATIN SMALL LIGATURE LONG S T,\s0
and
\s-1LATIN SMALL LIGATURE ST.\s0

- \(bu
A memory leak regression in regular expression compilation
under threading has been fixed.

- \(bu
A regression introduced in 5.14.0 has
been fixed.  This involved an inverted
bracketed character class in a regular expression that consisted solely
of a Unicode property.  That property wasn't getting inverted outside the
Latin1 range.

- \(bu
Three problematic Unicode characters now work better in regex pattern matching under \f(CW\*(C`/i\*(C'.
.Sp
In the past, three Unicode characters:
\s-1LATIN SMALL LETTER SHARP S,
GREEK SMALL LETTER IOTA WITH DIALYTIKA AND TONOS,\s0
and
\s-1GREEK SMALL LETTER UPSILON WITH DIALYTIKA AND TONOS,\s0
along with the sequences that they fold to
(including \*(L"ss\*(R" for \s-1LATIN SMALL LETTER SHARP S\s0),
did not properly match under \f(CW\*(C`/i\*(C'.  5.14.0 fixed some of these cases,
but introduced others, including a panic when one of the characters or
sequences was used in the \f(CW\*(C`(?(DEFINE)\*(C' regular expression predicate.
The known bugs that were introduced in 5.14 have now been fixed; as well
as some other edge cases that have never worked until now.  These all
involve using the characters and sequences outside bracketed character
classes under \f(CW\*(C`/i\*(C'.  This closes [perl #98546].
.Sp
There remain known problems when using certain characters with
multi-character folds inside bracketed character classes, including such
constructs as \f(CW\*(C`qr/[\\N\{LATIN SMALL LETTER SHARP\}a-z]/i\*(C'.  These
remaining bugs are addressed in [perl #89774].

- \(bu
\s-1RT\s0 #78266: The regex engine has been leaking memory when accessing
named captures that weren't matched as part of a regex ever since 5.10
when they were introduced; e.g., this would consume over a hundred \s-1MB\s0 of
memory:
.Sp
.Vb 6
    for (1..10_000_000) \{
        if ("foo" =~ /(foo|(?<capture>bar))?/) \{
            my $capture = $+\{capture\}
        \}
    \}
    system "ps -o rss $$"\*(Aq
.Ve

- \(bu
In 5.14, \f(CW\*(C`/[[:lower:]]/i\*(C' and \f(CW\*(C`/[[:upper:]]/i\*(C' no longer matched the
opposite case.  This has been fixed [perl #101970].

- \(bu
A regular expression match with an overloaded object on the right-hand side
would sometimes stringify the object too many times.

- \(bu
A regression has been fixed that was introduced in 5.14, in \f(CW\*(C`/i\*(C'
regular expression matching, in which a match improperly fails if the
pattern is in \s-1UTF-8,\s0 the target string is not, and a Latin-1 character
precedes a character in the string that should match the pattern.
[perl #101710]

- \(bu
In case-insensitive regular expression pattern matching, no longer on
\s-1UTF-8\s0 encoded strings does the scan for the start of match look only at
the first possible position.  This caused matches such as
\f(CW\*(C`"f\\x\{FB00\}" =~ /ff/i\*(C' to fail.

- \(bu
The regexp optimizer no longer crashes on debugging builds when merging
fixed-string nodes with inconvenient contents.

- \(bu
A panic involving the combination of the regular expression modifiers
\f(CW\*(C`/aa\*(C' and the \f(CW\*(C`\\b\*(C' escape sequence introduced in 5.14.0 has been
fixed [perl #95964]. (5.14.2)

- \(bu
The combination of the regular expression modifiers \f(CW\*(C`/aa\*(C' and the \f(CW\*(C`\\b\*(C'
and \f(CW\*(C`\\B\*(C' escape sequences did not work properly on \s-1UTF-8\s0 encoded
strings.  All non-ASCII characters under \f(CW\*(C`/aa\*(C' should be treated as
non-word characters, but what was happening was that Unicode rules were
used to determine wordness/non-wordness for non-ASCII characters.  This
is now fixed [perl #95968].

- \(bu
\f(CW\*(C`(?foo: ...)\*(C' no longer loses passed in character set.

- \(bu
The trie optimization used to have problems with alternations containing
an empty \f(CW\*(C`(?:)\*(C', causing \f(CW\*(C`"x" =~ /\\A(?>(?:(?:)A|B|C?x))\\z/\*(C' not to
match, whereas it should [perl #111842].

- \(bu
Use of lexical (\f(CW\*(C`my\*(C') variables in code blocks embedded in regular
expressions will no longer result in memory corruption or crashes.
.Sp
Nevertheless, these code blocks are still experimental, as there are still
problems with the wrong variables being closed over (in loops for instance)
and with abnormal exiting (e.g., \f(CW\*(C`die\*(C') causing memory corruption.

- \(bu
The \f(CW\*(C`\\h\*(C', \f(CW\*(C`\\H\*(C', \f(CW\*(C`\\v\*(C' and \f(CW\*(C`\\V\*(C' regular expression metacharacters used to
cause a panic error message when trying to match at the end of the
string [perl #96354].

- \(bu
The abbreviations for four C1 control characters \f(CW\*(C`MW\*(C' \f(CW\*(C`PM\*(C', \f(CW\*(C`RI\*(C', and
\f(CW\*(C`ST\*(C' were previously unrecognized by \f(CW\*(C`\\N\{\}\*(C', **vianame()**, and
**string_vianame()**.

- \(bu
Mentioning a variable named \*(L"&\*(R" other than \f(CW$& (i.e., \f(CW\*(C`@&\*(C' or \f(CW\*(C`%&\*(C') no
longer stops \f(CW$& from working.  The same applies to variables named \*(L"'\*(R"
and \*(L"`\*(R" [perl #24237].

- \(bu
Creating a \f(CW\*(C`UNIVERSAL::AUTOLOAD\*(C' sub no longer stops \f(CW\*(C`%+\*(C', \f(CW\*(C`%-\*(C' and
\f(CW\*(C`%!\*(C' from working some of the time [perl #105024].

### Smartmatching

Subsection "Smartmatching"

- \(bu
\f(CW\*(C`~~\*(C' now correctly handles the precedence of Any~~Object, and is not tricked
by an overloaded object on the left-hand side.

- \(bu
In Perl 5.14.0, \f(CW\*(C`$tainted ~~ @array\*(C' stopped working properly.  Sometimes
it would erroneously fail (when \f(CW$tainted contained a string that occurs
in the array *after* the first element) or erroneously succeed (when
\f(CW\*(C`undef\*(C' occurred after the first element) [perl #93590].
.ie n .SS "The ""sort"" operator"
.el .SS "The \f(CWsort operator"
Subsection "The sort operator"

- \(bu
\f(CW\*(C`sort\*(C' was not treating \f(CW\*(C`sub \{\}\*(C' and \f(CW\*(C`sub \{()\}\*(C' as equivalent when
such a sub was provided as the comparison routine.  It used to croak on
\f(CW\*(C`sub \{()\}\*(C'.

- \(bu
\f(CW\*(C`sort\*(C' now works once more with custom sort routines that are XSUBs.  It
stopped working in 5.10.0.

- \(bu
\f(CW\*(C`sort\*(C' with a constant for a custom sort routine, although it produces
unsorted results, no longer crashes.  It started crashing in 5.10.0.

- \(bu
Warnings emitted by \f(CW\*(C`sort\*(C' when a custom comparison routine returns a
non-numeric value now contain \*(L"in sort\*(R" and show the line number of the
\f(CW\*(C`sort\*(C' operator, rather than the last line of the comparison routine.  The
warnings also now occur only if warnings are enabled in the scope where
\f(CW\*(C`sort\*(C' occurs.  Previously the warnings would occur if enabled in the
comparison routine's scope.

- \(bu
\f(CW\*(C`sort \{ $a <=> $b \}\*(C', which is optimized internally, now produces
\*(L"uninitialized\*(R" warnings for NaNs (not-a-number values), since \f(CW\*(C`<=>\*(C'
returns \f(CW\*(C`undef\*(C' for those.  This brings it in line with
\f(CW\*(C`sort\ \{\ 1;\ $a\ <=>\ $b\ \}\*(C' and other more complex cases, which are not
optimized [perl #94390].
.ie n .SS "The ""substr"" operator"
.el .SS "The \f(CWsubstr operator"
Subsection "The substr operator"

- \(bu
Tied (and otherwise magical) variables are no longer exempt from the
\*(L"Attempt to use reference as lvalue in substr\*(R" warning.

- \(bu
That warning now occurs when the returned lvalue is assigned to, not
when \f(CW\*(C`substr\*(C' itself is called.  This makes a difference only if the
return value of \f(CW\*(C`substr\*(C' is referenced and later assigned to.

- \(bu
Passing a substring of a read-only value or a typeglob to a function
(potential lvalue context) no longer causes an immediate \*(L"Can't coerce\*(R"
or \*(L"Modification of a read-only value\*(R" error.  That error occurs only
if the passed value is assigned to.
.Sp
The same thing happens with the \*(L"substr outside of string\*(R" error.  If
the lvalue is only read from, not written to, it is now just a warning, as
with rvalue \f(CW\*(C`substr\*(C'.

- \(bu
\f(CW\*(C`substr\*(C' assignments no longer call \s-1FETCH\s0 twice if the first argument
is a tied variable, just once.

### Support for embedded nulls

Subsection "Support for embedded nulls"
Some parts of Perl did not work correctly with nulls (\f(CW\*(C`chr 0\*(C') embedded in
strings.  That meant that, for instance, \f(CW\*(C`$m = "a\\0b"; foo->$m\*(C' would
call the \*(L"a\*(R" method, instead of the actual method name contained in \f(CW$m.
These parts of perl have been fixed to support nulls:

- \(bu
Method names

- \(bu
Typeglob names (including filehandle and subroutine names)

- \(bu
Package names, including the return value of \f(CW\*(C`ref()\*(C'

- \(bu
Typeglob elements (\f(CW*foo\{"THING\\0stuff"\})

- \(bu
Signal names

- \(bu
Various warnings and error messages that mention variable names or values,
methods, etc.

One side effect of these changes is that blessing into \*(L"\\0\*(R" no longer
causes \f(CW\*(C`ref()\*(C' to return false.

### Threading bugs

Subsection "Threading bugs"

- \(bu
Typeglobs returned from threads are no longer cloned if the parent thread
already has a glob with the same name.  This means that returned
subroutines will now assign to the right package variables [perl #107366].

- \(bu
Some cases of threads crashing due to memory allocation during cloning have
been fixed [perl #90006].

- \(bu
Thread joining would sometimes emit \*(L"Attempt to free unreferenced scalar\*(R"
warnings if \f(CW\*(C`caller\*(C' had been used from the \f(CW\*(C`DB\*(C' package before thread
creation [perl #98092].

- \(bu
Locking a subroutine (via \f(CW\*(C`lock &sub\*(C') is no longer a compile-time error
for regular subs.  For lvalue subroutines, it no longer tries to return the
sub as a scalar, resulting in strange side effects like \f(CW\*(C`ref \\$_\*(C'
returning \*(L"\s-1CODE\*(R"\s0 in some instances.
.Sp
\f(CW\*(C`lock &sub\*(C' is now a run-time error if threads::shared is loaded (a
no-op otherwise), but that may be rectified in a future version.

### Tied variables

Subsection "Tied variables"

- \(bu
Various cases in which \s-1FETCH\s0 was being ignored or called too many times
have been fixed:

> 
- \(bu
\f(CW\*(C`PerlIO::get_layers\*(C' [perl #97956]

- \(bu
\f(CW\*(C`$tied =~ y/a/b/\*(C', \f(CW\*(C`chop $tied\*(C' and \f(CW\*(C`chomp $tied\*(C' when \f(CW$tied holds a
reference.

- \(bu
When calling \f(CW\*(C`local $_\*(C' [perl #105912]

- \(bu
Four-argument \f(CW\*(C`select\*(C'

- \(bu
A tied buffer passed to \f(CW\*(C`sysread\*(C'

- \(bu
\f(CW\*(C`$tied .= <>\*(C'

- \(bu
Three-argument \f(CW\*(C`open\*(C', the third being a tied file handle
(as in \f(CW\*(C`open $fh, ">&", $tied\*(C')

- \(bu
\f(CW\*(C`sort\*(C' with a reference to a tied glob for the comparison routine.

- \(bu
\f(CW\*(C`..\*(C' and \f(CW\*(C`...\*(C' in list context [perl #53554].

- \(bu
\f(CW\*(C`$\{$tied\}\*(C', \f(CW\*(C`@\{$tied\}\*(C', \f(CW\*(C`%\{$tied\}\*(C' and \f(CW\*(C`*\{$tied\}\*(C' where the tied
variable returns a string (\f(CW\*(C`&\{\}\*(C' was unaffected)

- \(bu
\f(CW\*(C`defined $\{ $tied_variable \}\*(C'

- \(bu
Various functions that take a filehandle argument in rvalue context
(\f(CW\*(C`close\*(C', \f(CW\*(C`readline\*(C', etc.) [perl #97482]

- \(bu
Some cases of dereferencing a complex expression, such as
\f(CW\*(C`$\{ (), $tied \} = 1\*(C', used to call \f(CW\*(C`FETCH\*(C' multiple times, but now call
it once.

- \(bu
\f(CW\*(C`$tied->method\*(C' where \f(CW$tied returns a package name\*(--even resulting in
a failure to call the method, due to memory corruption

- \(bu
Assignments like \f(CW\*(C`*$tied = \\{"..."\}\*(C' and \f(CW\*(C`*glob = $tied\*(C'

- \(bu
\f(CW\*(C`chdir\*(C', \f(CW\*(C`chmod\*(C', \f(CW\*(C`chown\*(C', \f(CW\*(C`utime\*(C', \f(CW\*(C`truncate\*(C', \f(CW\*(C`stat\*(C', \f(CW\*(C`lstat\*(C' and
the filetest ops (\f(CW\*(C`-r\*(C', \f(CW\*(C`-x\*(C', etc.)



> 


- \(bu
\f(CW\*(C`caller\*(C' sets \f(CW@DB::args to the subroutine arguments when called from
the \s-1DB\s0 package.  It used to crash when doing so if \f(CW@DB::args happened to
be tied.  Now it croaks instead.

- \(bu
Tying an element of \f(CW%ENV or \f(CW\*(C`%^H\*(C' and then deleting that element would
result in a call to the tie object's \s-1DELETE\s0 method, even though tying the
element itself is supposed to be equivalent to tying a scalar (the element
is, of course, a scalar) [perl #67490].

- \(bu
When Perl autovivifies an element of a tied array or hash (which entails
calling \s-1STORE\s0 with a new reference), it now calls \s-1FETCH\s0 immediately after
the \s-1STORE,\s0 instead of assuming that \s-1FETCH\s0 would have returned the same
reference.  This can make it easier to implement tied objects [perl #35865, #43011].

- \(bu
Four-argument \f(CW\*(C`select\*(C' no longer produces its \*(L"Non-string passed as
bitmask\*(R" warning on tied or tainted variables that are strings.

- \(bu
Localizing a tied scalar that returns a typeglob no longer stops it from
being tied till the end of the scope.

- \(bu
Attempting to \f(CW\*(C`goto\*(C' out of a tied handle method used to cause memory
corruption or crashes.  Now it produces an error message instead
[perl #8611].

- \(bu
A bug has been fixed that occurs when a tied variable is used as a
subroutine reference:  if the last thing assigned to or returned from the
variable was a reference or typeglob, the \f(CW\*(C`\$tied\*(C' could either crash or
return the wrong subroutine.  The reference case is a regression introduced
in Perl 5.10.0.  For typeglobs, it has probably never worked till now.

### Version objects and vstrings

Subsection "Version objects and vstrings"

- \(bu
The bitwise complement operator (and possibly other operators, too) when
passed a vstring would leave vstring magic attached to the return value,
even though the string had changed.  This meant that
\f(CW\*(C`version->new(~v1.2.3)\*(C' would create a version looking like \*(L"v1.2.3\*(R"
even though the string passed to \f(CW\*(C`version->new\*(C' was actually
\*(L"\\376\\375\\374\*(R".  This also caused B::Deparse to deparse \f(CW\*(C`~v1.2.3\*(C'
incorrectly, without the \f(CW\*(C`~\*(C' [perl #29070].

- \(bu
Assigning a vstring to a magic (e.g., tied, \f(CW$!) variable and then
assigning something else used to blow away all magic.  This meant that
tied variables would come undone, \f(CW$! would stop getting updated on
failed system calls, \f(CW$| would stop setting autoflush, and other
mischief would take place.  This has been fixed.

- \(bu
\f(CW\*(C`version->new("version")\*(C' and \f(CW\*(C`printf "%vd", "version"\*(C' no longer
crash [perl #102586].

- \(bu
Version comparisons, such as those that happen implicitly with \f(CW\*(C`use
v5.43\*(C', no longer cause locale settings to change [perl #105784].

- \(bu
Version objects no longer cause memory leaks in boolean context
[perl #109762].

### Warnings, redefinition

Subsection "Warnings, redefinition"

- \(bu
Subroutines from the \f(CW\*(C`autouse\*(C' namespace are once more exempt from
redefinition warnings.  This used to work in 5.005, but was broken in
5.6 for most subroutines.  For subs created via \s-1XS\s0 that redefine
subroutines from the \f(CW\*(C`autouse\*(C' package, this stopped working in 5.10.

- \(bu
New XSUBs now produce redefinition warnings if they overwrite existing
subs, as they did in 5.8.x.  (The \f(CW\*(C`autouse\*(C' logic was reversed in
5.10-14.  Only subroutines from the \f(CW\*(C`autouse\*(C' namespace would warn
when clobbered.)

- \(bu
\f(CW\*(C`newCONSTSUB\*(C' used to use compile-time warning hints, instead of
run-time hints.  The following code should never produce a redefinition
warning, but it used to, if \f(CW\*(C`newCONSTSUB\*(C' redefined an existing
subroutine:
.Sp
.Vb 5
    use warnings;
    BEGIN \{
        no warnings;
        some_XS_function_that_calls_new_CONSTSUB();
    \}
.Ve

- \(bu
Redefinition warnings for constant subroutines are on by default (what
are known as severe warnings in perldiag).  This occurred only
when it was a glob assignment or declaration of a Perl subroutine that
caused the warning.  If the creation of XSUBs triggered the warning, it
was not a default warning.  This has been corrected.

- \(bu
The internal check to see whether a redefinition warning should occur
used to emit \*(L"uninitialized\*(R" warnings in cases like this:
.Sp
.Vb 4
    use warnings "uninitialized";
    use constant \{u => undef, v => undef\};
    sub foo()\{u\}
    sub foo()\{v\}
.Ve
.ie n .SS "Warnings, ""Uninitialized"""
.el .SS "Warnings, ``Uninitialized''"
Subsection "Warnings, Uninitialized"

- \(bu
Various functions that take a filehandle argument in rvalue context
(\f(CW\*(C`close\*(C', \f(CW\*(C`readline\*(C', etc.) used to warn twice for an undefined handle
[perl #97482].

- \(bu
\f(CW\*(C`dbmopen\*(C' now only warns once, rather than three times, if the mode
argument is \f(CW\*(C`undef\*(C' [perl #90064].

- \(bu
The \f(CW\*(C`+=\*(C' operator does not usually warn when the left-hand side is
\f(CW\*(C`undef\*(C', but it was doing so for tied variables.  This has been fixed
[perl #44895].

- \(bu
A bug fix in Perl 5.14 introduced a new bug, causing \*(L"uninitialized\*(R"
warnings to report the wrong variable if the operator in question had
two operands and one was \f(CW\*(C`%\{...\}\*(C' or \f(CW\*(C`@\{...\}\*(C'.  This has been fixed
[perl #103766].

- \(bu
\f(CW\*(C`..\*(C' and \f(CW\*(C`...\*(C' in list context now mention the name of the variable in
\*(L"uninitialized\*(R" warnings for string (as opposed to numeric) ranges.

### Weak references

Subsection "Weak references"

- \(bu
Weakening the first argument to an automatically-invoked \f(CW\*(C`DESTROY\*(C' method
could result in erroneous \*(L"\s-1DESTROY\s0 created new reference\*(R" errors or
crashes.  Now it is an error to weaken a read-only reference.

- \(bu
Weak references to lexical hashes going out of scope were not going stale
(becoming undefined), but continued to point to the hash.

- \(bu
Weak references to lexical variables going out of scope are now broken
before any magical methods (e.g., \s-1DESTROY\s0 on a tie object) are called.
This prevents such methods from modifying the variable that will be seen
the next time the scope is entered.

- \(bu
Creating a weak reference to an \f(CW@ISA array or accessing the array index
(\f(CW$#ISA) could result in confused internal bookkeeping for elements
later added to the \f(CW@ISA array.  For instance, creating a weak
reference to the element itself could push that weak reference on to \f(CW@ISA;
and elements added after use of \f(CW$#ISA would be ignored by method lookup
[perl #85670].

### Other notable fixes

Subsection "Other notable fixes"

- \(bu
\f(CW\*(C`quotemeta\*(C' now quotes consistently the same non-ASCII characters under
\f(CW\*(C`use feature \*(Aqunicode_strings\*(Aq\*(C', regardless of whether the string is
encoded in \s-1UTF-8\s0 or not, hence fixing the last vestiges (we hope) of the
notorious \*(L"The \*(R"Unicode Bug"" in perlunicode.  [perl #77654].
.Sp
Which of these code points is quoted has changed, based on Unicode's
recommendations.  See \*(L"quotemeta\*(R" in perlfunc for details.

- \(bu
\f(CW\*(C`study\*(C' is now a no-op, presumably fixing all outstanding bugs related to
study causing regex matches to behave incorrectly!

- \(bu
When one writes \f(CW\*(C`open foo || die\*(C', which used to work in Perl 4, a
\*(L"Precedence problem\*(R" warning is produced.  This warning used erroneously to
apply to fully-qualified bareword handle names not followed by \f(CW\*(C`||\*(C'.  This
has been corrected.

- \(bu
After package aliasing (\f(CW\*(C`*foo:: = *bar::\*(C'), \f(CW\*(C`select\*(C' with 0 or 1 argument
would sometimes return a name that could not be used to refer to the
filehandle, or sometimes it would return \f(CW\*(C`undef\*(C' even when a filehandle
was selected.  Now it returns a typeglob reference in such cases.

- \(bu
\f(CW\*(C`PerlIO::get_layers\*(C' no longer ignores some arguments that it thinks are
numeric, while treating others as filehandle names.  It is now consistent
for flat scalars (i.e., not references).

- \(bu
Unrecognized switches on \f(CW\*(C`#!\*(C' line
.Sp
If a switch, such as **-x**, that cannot occur on the \f(CW\*(C`#!\*(C' line is used
there, perl dies with \*(L"Can't emulate...\*(R".
.Sp
It used to produce the same message for switches that perl did not
recognize at all, whether on the command line or the \f(CW\*(C`#!\*(C' line.
.Sp
Now it produces the \*(L"Unrecognized switch\*(R" error message [perl #104288].

- \(bu
\f(CW\*(C`system\*(C' now temporarily blocks the \s-1SIGCHLD\s0 signal handler, to prevent the
signal handler from stealing the exit status [perl #105700].

- \(bu
The \f(CW%n formatting code for \f(CW\*(C`printf\*(C' and \f(CW\*(C`sprintf\*(C', which causes the number
of characters to be assigned to the next argument, now actually
assigns the number of characters, instead of the number of bytes.
.Sp
It also works now with special lvalue functions like \f(CW\*(C`substr\*(C' and with
nonexistent hash and array elements [perl #3471, #103492].

- \(bu
Perl skips copying values returned from a subroutine, for the sake of
speed, if doing so would make no observable difference.  Because of faulty
logic, this would happen with the
result of \f(CW\*(C`delete\*(C', \f(CW\*(C`shift\*(C' or \f(CW\*(C`splice\*(C', even if the result was
referenced elsewhere.  It also did so with tied variables about to be freed
[perl #91844, #95548].

- \(bu
\f(CW\*(C`utf8::decode\*(C' now refuses to modify read-only scalars [perl #91850].

- \(bu
Freeing \f(CW$_ inside a \f(CW\*(C`grep\*(C' or \f(CW\*(C`map\*(C' block, a code block embedded in a
regular expression, or an \f(CW@INC filter (a subroutine returned by a
subroutine in \f(CW@INC) used to result in double frees or crashes
[perl #91880, #92254, #92256].

- \(bu
\f(CW\*(C`eval\*(C' returns \f(CW\*(C`undef\*(C' in scalar context or an empty list in list
context when there is a run-time error.  When \f(CW\*(C`eval\*(C' was passed a
string in list context and a syntax error occurred, it used to return a
list containing a single undefined element.  Now it returns an empty
list in list context for all errors [perl #80630].

- \(bu
\f(CW\*(C`goto &func\*(C' no longer crashes, but produces an error message, when
the unwinding of the current subroutine's scope fires a destructor that
undefines the subroutine being \*(L"goneto\*(R" [perl #99850].

- \(bu
Perl now holds an extra reference count on the package that code is
currently compiling in.  This means that the following code no longer
crashes [perl #101486]:
.Sp
.Vb 3
    package Foo;
    BEGIN \{*Foo:: = *Bar::\}
    sub foo;
.Ve

- \(bu
The \f(CW\*(C`x\*(C' repetition operator no longer crashes on 64-bit builds with large
repeat counts [perl #94560].

- \(bu
Calling \f(CW\*(C`require\*(C' on an implicit \f(CW$_ when \f(CW*CORE::GLOBAL::require has
been overridden does not segfault anymore, and \f(CW$_ is now passed to the
overriding subroutine [perl #78260].

- \(bu
\f(CW\*(C`use\*(C' and \f(CW\*(C`require\*(C' are no longer affected by the I/O layers active in
the caller's scope (enabled by open.pm) [perl #96008].

- \(bu
\f(CW\*(C`our $::e\*'; $e\*'\*(C' (which is invalid) no longer produces the \*(L"Compilation
error at lib/utf8_heavy.pl...\*(R" error message, which it started emitting in
5.10.0 [perl #99984].

- \(bu
On 64-bit systems, \f(CW\*(C`read()\*(C' now understands large string offsets beyond
the 32-bit range.

- \(bu
Errors that occur when processing subroutine attributes no longer cause the
subroutine's op tree to leak.

- \(bu
Passing the same constant subroutine to both \f(CW\*(C`index\*(C' and \f(CW\*(C`formline\*(C' no
longer causes one or the other to fail [perl #89218]. (5.14.1)

- \(bu
List assignment to lexical variables declared with attributes in the same
statement (\f(CW\*(C`my ($x,@y) : blimp = (72,94)\*(C') stopped working in Perl 5.8.0.
It has now been fixed.

- \(bu
Perl 5.10.0 introduced some faulty logic that made \*(L"U*\*(R" in the middle of
a pack template equivalent to \*(L"U0\*(R" if the input string was empty.  This has
been fixed [perl #90160]. (5.14.2)

- \(bu
Destructors on objects were not called during global destruction on objects
that were not referenced by any scalars.  This could happen if an array
element were blessed (e.g., \f(CW\*(C`bless \\$a[0]\*(C') or if a closure referenced a
blessed variable (\f(CW\*(C`bless \\my @a; sub foo \{ @a \}\*(C').
.Sp
Now there is an extra pass during global destruction to fire destructors on
any objects that might be left after the usual passes that check for
objects referenced by scalars [perl #36347].

- \(bu
Fixed a case where it was possible that a freed buffer may have been read
from when parsing a here document [perl #90128]. (5.14.1)

- \(bu
\f(CW\*(C`each(\f(CIARRAY\f(CW)\*(C' is now wrapped in \f(CW\*(C`defined(...)\*(C', like \f(CW\*(C`each(\f(CIHASH\f(CW)\*(C',
inside a \f(CW\*(C`while\*(C' condition [perl #90888].

- \(bu
A problem with context propagation when a \f(CW\*(C`do\*(C' block is an argument to
\f(CW\*(C`return\*(C' has been fixed.  It used to cause \f(CW\*(C`undef\*(C' to be returned in
certain cases of a \f(CW\*(C`return\*(C' inside an \f(CW\*(C`if\*(C' block which itself is followed by
another \f(CW\*(C`return\*(C'.

- \(bu
Calling \f(CW\*(C`index\*(C' with a tainted constant no longer causes constants in
subsequently compiled code to become tainted [perl #64804].

- \(bu
Infinite loops like \f(CW\*(C`1 while 1\*(C' used to stop \f(CW\*(C`strict \*(Aqsubs\*(Aq\*(C' mode from
working for the rest of the block.

- \(bu
For list assignments like \f(CW\*(C`($a,$b) = ($b,$a)\*(C', Perl has to make a copy of
the items on the right-hand side before assignment them to the left.  For
efficiency's sake, it assigns the values on the right straight to the items
on the left if no one variable is mentioned on both sides, as in \f(CW\*(C`($a,$b) =
($c,$d)\*(C'.  The logic for determining when it can cheat was faulty, in that
\f(CW\*(C`&&\*(C' and \f(CW\*(C`||\*(C' on the right-hand side could fool it.  So \f(CW\*(C`($a,$b) =
$some_true_value && ($b,$a)\*(C' would end up assigning the value of \f(CW$b to
both scalars.

- \(bu
Perl no longer tries to apply lvalue context to the string in
\f(CW\*(C`("string", $variable) ||= 1\*(C' (which used to be an error).  Since the
left-hand side of \f(CW\*(C`||=\*(C' is evaluated in scalar context, that's a scalar
comma operator, which gives all but the last item void context.  There is
no such thing as void lvalue context, so it was a mistake for Perl to try
to force it [perl #96942].

- \(bu
\f(CW\*(C`caller\*(C' no longer leaks memory when called from the \s-1DB\s0 package if
\f(CW@DB::args was assigned to after the first call to \f(CW\*(C`caller\*(C'.  Carp
was triggering this bug [perl #97010]. (5.14.2)

- \(bu
\f(CW\*(C`close\*(C' and similar filehandle functions, when called on built-in global
variables (like \f(CW$+), used to die if the variable happened to hold the
undefined value, instead of producing the usual \*(L"Use of uninitialized
value\*(R" warning.

- \(bu
When autovivified file handles were introduced in Perl 5.6.0, \f(CW\*(C`readline\*(C'
was inadvertently made to autovivify when called as \f(CW\*(C`readline($foo)\*(C' (but
not as \f(CW\*(C`<$foo>\*(C').  It has now been fixed never to autovivify.

- \(bu
Calling an undefined anonymous subroutine (e.g., what \f(CW$x holds after
\f(CW\*(C`undef &\{$x = sub\{\}\}\*(C') used to cause a \*(L"Not a \s-1CODE\s0 reference\*(R" error, which
has been corrected to \*(L"Undefined subroutine called\*(R" [perl #71154].

- \(bu
Causing \f(CW@DB::args to be freed between uses of \f(CW\*(C`caller\*(C' no longer
results in a crash [perl #93320].

- \(bu
\f(CW\*(C`setpgrp($foo)\*(C' used to be equivalent to \f(CW\*(C`($foo, setpgrp)\*(C', because
\f(CW\*(C`setpgrp\*(C' was ignoring its argument if there was just one.  Now it is
equivalent to \f(CW\*(C`setpgrp($foo,0)\*(C'.

- \(bu
\f(CW\*(C`shmread\*(C' was not setting the scalar flags correctly when reading from
shared memory, causing the existing cached numeric representation in the
scalar to persist [perl #98480].

- \(bu
\f(CW\*(C`++\*(C' and \f(CW\*(C`--\*(C' now work on copies of globs, instead of dying.

- \(bu
\f(CW\*(C`splice()\*(C' doesn't warn when truncating
.Sp
You can now limit the size of an array using \f(CW\*(C`splice(@a,MAX_LEN)\*(C' without
worrying about warnings.

- \(bu
\f(CW$$ is no longer tainted.  Since this value comes directly from
\f(CW\*(C`getpid()\*(C', it is always safe.

- \(bu
The parser no longer leaks a filehandle if \s-1STDIN\s0 was closed before parsing
started [perl #37033].

- \(bu
\f(CW\*(C`die;\*(C' with a non-reference, non-string, or magical (e.g., tainted)
value in $@ now properly propagates that value [perl #111654].

## Known Problems

Header "Known Problems"

- \(bu
On Solaris, we have two kinds of failure.
.Sp
If *make* is Sun's *make*, we get an error about a badly formed macro
assignment in the *Makefile*.  That happens when *./Configure* tries to
make depends.  *Configure* then exits 0, but further *make*-ing fails.
.Sp
If *make* is *gmake*, *Configure* completes, then we get errors related
to */usr/include/stdbool.h*

- \(bu
On Win32, a number of tests hang unless \s-1STDERR\s0 is redirected.  The cause of
this is still under investigation.

- \(bu
When building as root with a umask that prevents files from being
other-readable, *t/op/filetest.t* will fail.  This is a test bug, not a
bug in perl's behavior.

- \(bu
Configuring with a recent gcc and link-time-optimization, such as
\f(CW\*(C`Configure -Doptimize=\*(Aq-O2 -flto\*(Aq\*(C' fails
because the optimizer optimizes away some of Configure's tests.  A
workaround is to omit the \f(CW\*(C`-flto\*(C' flag when running Configure, but add
it back in while actually building, something like
.Sp
.Vb 2
    sh Configure -Doptimize=-O2
    make OPTIMIZE=\*(Aq-O2 -flto\*(Aq
.Ve

- \(bu
The following \s-1CPAN\s0 modules have test failures with perl 5.16.  Patches have
been submitted for all of these, so hopefully there will be new releases
soon:

> 
- \(bu
Date::Pcalc version 6.1

- \(bu
Module::CPANTS::Analyse version 0.85
.Sp
This fails due to problems in Module::Find 0.10 and File::MMagic
1.27.

- \(bu
PerlIO::Util version 0.72



> 


## Acknowledgements

Header "Acknowledgements"
Perl 5.16.0 represents approximately 12 months of development since Perl
5.14.0 and contains approximately 590,000 lines of changes across 2,500
files from 139 authors.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers.  The following people are known to
have contributed the improvements that became Perl 5.16.0:

Aaron Crane, Abhijit Menon-Sen, Abigail, Alan Haggai Alavi, Alberto
Simo\*~es, Alexandr Ciornii, Andreas Ko\*:nig, Andy Dougherty, Aristotle
Pagaltzis, Bo Johansson, Bo Lindbergh, Breno G. de Oliveira, brian d
foy, Brian Fraser, Brian Greenfield, Carl Hayter, Chas. Owens,
Chia-liang Kao, Chip Salzenberg, Chris 'BinGOs' Williams, Christian
Hansen, Christopher J. Madsen, chromatic, Claes Jacobsson, Claudio
Ramirez, Craig A. Berry, Damian Conway, Daniel Kahn Gillmor, Darin
McBride, Dave Rolsky, David Cantrell, David Golden, David Leadbeater,
David Mitchell, Dee Newcum, Dennis Kaarsemaker, Dominic Hargreaves,
Douglas Christopher Wilson, Eric Brine, Father Chrysostomos, Florian
Ragwitz, Frederic Briere, George Greer, Gerard Goossen, Gisle Aas,
H.Merijn Brand, Hojung Youn, Ian Goodacre, James E Keenan, Jan Dubois,
Jerry D. Hedden, Jesse Luehrs, Jesse Vincent, Jilles Tjoelker, Jim
Cromie, Jim Meyering, Joel Berger, Johan Vromans, Johannes Plunien, John
Hawkinson, John P. Linderman, John Peacock, Joshua ben Jore, Juerd
Waalboer, Karl Williamson, Karthik Rajagopalan, Keith Thompson, Kevin J.
Woolley, Kevin Ryde, Laurent Dami, Leo Lapworth, Leon Brocard, Leon
Timmermans, Louis Strous, Lukas Mai, Marc Green, Marcel Gru\*:nauer, Mark
A.  Stratman, Mark Dootson, Mark Jason Dominus, Martin Hasch, Matthew
Horsfall, Max Maischein, Michael G Schwern, Michael Witten, Mike
Sheldrake, Moritz Lenz, Nicholas Clark, Niko Tyni, Nuno Carvalho, Pau
Amma, Paul Evans, Paul Green, Paul Johnson, Perlover, Peter John Acklam,
Peter Martini, Peter Scott, Phil Monsen, Pino Toscano, Rafael
Garcia-Suarez, Rainer Tammer, Reini Urban, Ricardo Signes, Robin Barker,
Rodolfo Carvalho, Salvador Fandin\*~o, Sam Kimbrel, Samuel Thibault, Shawn
M Moore, Shigeya Suzuki, Shirakata Kentaro, Shlomi Fish, Sisyphus,
Slaven Rezic, Spiros Denaxas, Steffen Mu\*:ller, Steffen Schwigon, Stephen
Bennett, Stephen Oberholtzer, Stevan Little, Steve Hay, Steve Peters,
Thomas Sibley, Thorsten Glaser, Timothe Litt, Todd Rinaldo, Tom
Christiansen, Tom Hukins, Tony Cook, Vadim Konovalov, Vincent Pit,
Vladimir Timofeev, Walt Mankowski, Yves Orton, Zefram, Zsba\*'n Ambrus,
\*(Aevar Arnfjo\*:r\*(d- Bjarmason.

The list above is almost certainly incomplete as it is automatically
generated from version control history.  In particular, it does not
include the names of the (very much appreciated) contributors who
reported issues to the Perl bug tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0
modules included in Perl's core.  We're grateful to the entire \s-1CPAN\s0
community for helping Perl to flourish.

For a more complete list of all of Perl's historical contributors,
please see the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at <http://rt.perl.org/perlbug/>.  There may also be
information at <http://www.perl.org/>, the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please
send it to perl5-security-report@perl.org.  This points to a closed
subscription unarchived mailing list, which includes all core
committers, who will be able to help assess the impact of issues, figure
out a resolution, and help co-ordinate the release of patches to
mitigate or fix the problem across all platforms on which Perl is
supported.  Please use this address only for security issues in the Perl
core, not for modules independently distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
