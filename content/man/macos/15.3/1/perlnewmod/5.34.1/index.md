+++
detected_package_version = "5.34.1"
operating_system = "macos"
manpage_section = "1"
keywords = ["header", "see", "also", "perlmod", "perlmodlib", "perlmodinstall", "h2xs", "strict", "carp", "exporter", "perlpod", "test", "simple", "more", "extutils", "makemaker", "module", "build", "starter", "http", "www", "cpan", "org", "ken", "williams", "tutorial", "on", "building", "your", "own", "at", "mathforum", "perl_modules", "html"]
title = "perlnewmod(1)"
operating_system_version = "15.3"
manpage_name = "perlnewmod"
date = "2022-02-19"
author = "None Specified"
description = "This document gives you some suggestions about how to go about writing Perl modules, preparing them for distribution, and making them available via s-1CPAN.s0 One of the things that makes Perl really powerful is the fact that Perl hackers tend to ..."
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLNEWMOD 1"
PERLNEWMOD 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlnewmod - preparing a new module for distribution

## DESCRIPTION

Header "DESCRIPTION"
This document gives you some suggestions about how to go about writing
Perl modules, preparing them for distribution, and making them available
via \s-1CPAN.\s0

One of the things that makes Perl really powerful is the fact that Perl
hackers tend to want to share the solutions to problems they've faced,
so you and I don't have to battle with the same problem again.

The main way they do this is by abstracting the solution into a Perl
module. If you don't know what one of these is, the rest of this
document isn't going to be much use to you. You're also missing out on
an awful lot of useful code; consider having a look at perlmod,
perlmodlib and perlmodinstall before coming back here.

When you've found that there isn't a module available for what you're
trying to do, and you've had to write the code yourself, consider
packaging up the solution into a module and uploading it to \s-1CPAN\s0 so that
others can benefit.

You should also take a look at perlmodstyle for best practices in
making a module.

### Warning

Subsection "Warning"
We're going to primarily concentrate on Perl-only modules here, rather
than \s-1XS\s0 modules. \s-1XS\s0 modules serve a rather different purpose, and
you should consider different things before distributing them - the
popularity of the library you are gluing, the portability to other
operating systems, and so on. However, the notes on preparing the Perl
side of the module and packaging and distributing it will apply equally
well to an \s-1XS\s0 module as a pure-Perl one.

### What should I make into a module?

Subsection "What should I make into a module?"
You should make a module out of any code that you think is going to be
useful to others. Anything that's likely to fill a hole in the communal
library and which someone else can slot directly into their program. Any
part of your code which you can isolate and extract and plug into
something else is a likely candidate.

Let's take an example. Suppose you're reading in data from a local
format into a hash-of-hashes in Perl, turning that into a tree, walking
the tree and then piping each node to an Acme Transmogrifier Server.

Now, quite a few people have the Acme Transmogrifier, and you've had to
write something to talk the protocol from scratch - you'd almost
certainly want to make that into a module. The level at which you pitch
it is up to you: you might want protocol-level modules analogous to
Net::SMTP which then talk to higher level modules analogous
to Mail::Send. The choice is yours, but you do want to get
a module out for that server protocol.

Nobody else on the planet is going to talk your local data format, so we
can ignore that. But what about the thing in the middle? Building tree
structures from Perl variables and then traversing them is a nice,
general problem, and if nobody's already written a module that does
that, you might want to modularise that code too.

So hopefully you've now got a few ideas about what's good to modularise.
Let's now see how it's done.

### Step-by-step: Preparing the ground

Subsection "Step-by-step: Preparing the ground"
Before we even start scraping out the code, there are a few things we'll
want to do in advance.

- Look around
Item "Look around"
Dig into a bunch of modules to see how they're written. I'd suggest
starting with Text::Tabs, since it's in the standard
library and is nice and simple, and then looking at something a little
more complex like File::Copy.  For object oriented
code, WWW::Mechanize or the \f(CW\*(C`Email::*\*(C' modules provide some good
examples.
.Sp
These should give you an overall feel for how modules are laid out and
written.

- Check it's new
Item "Check it's new"
There are a lot of modules on \s-1CPAN,\s0 and it's easy to miss one that's
similar to what you're planning on contributing. Have a good plough
through <http://metacpan.org> and make sure you're not the one
reinventing the wheel!

- Discuss the need
Item "Discuss the need"
You might love it. You might feel that everyone else needs it. But there
might not actually be any real demand for it out there. If you're unsure
about the demand your module will have, consider asking the
\f(CW\*(C`module-authors@perl.org\*(C' mailing list (send an email to
\f(CW\*(C`module-authors-subscribe@perl.org\*(C' to subscribe; see
<https://lists.perl.org/list/module-authors.html> for more information
and a link to the archives).

- Choose a name
Item "Choose a name"
Perl modules included on \s-1CPAN\s0 have a naming hierarchy you should try to
fit in with. See perlmodlib for more details on how this works, and
browse around \s-1CPAN\s0 and the modules list to get a feel of it. At the very
least, remember this: modules should be title capitalised, (This::Thing)
fit in with a category, and explain their purpose succinctly.

- Check again
Item "Check again"
While you're doing that, make really sure you haven't missed a module
similar to the one you're about to write.
.Sp
When you've got your name sorted out and you're sure that your module is
wanted and not currently available, it's time to start coding.

### Step-by-step: Making the module

Subsection "Step-by-step: Making the module"

- Start with \fImodule-starter or \fIh2xs
Item "Start with module-starter or h2xs"
The *module-starter* utility is distributed as part of the
Module::Starter \s-1CPAN\s0 package.  It creates a directory
with stubs of all the necessary files to start a new module, according
to recent \*(L"best practice\*(R" for module development, and is invoked from
the command line, thus:
.Sp
.Vb 2
    module-starter --module=Foo::Bar \\
       --author="Your Name" --email=yourname@cpan.org
.Ve
.Sp
If you do not wish to install the Module::Starter
package from \s-1CPAN,\s0 *h2xs* is an older tool, originally intended for the
development of \s-1XS\s0 modules, which comes packaged with the Perl
distribution.
.Sp
A typical invocation of h2xs for a pure Perl module is:
.Sp
.Vb 1
    h2xs -AX --skip-exporter --use-new-tests -n Foo::Bar
.Ve
.Sp
The \f(CW\*(C`-A\*(C' omits the Autoloader code, \f(CW\*(C`-X\*(C' omits \s-1XS\s0 elements,
\f(CW\*(C`--skip-exporter\*(C' omits the Exporter code, \f(CW\*(C`--use-new-tests\*(C' sets up a
modern testing environment, and \f(CW\*(C`-n\*(C' specifies the name of the module.

- Use strict and warnings
Item "Use strict and warnings"
A module's code has to be warning and strict-clean, since you can't
guarantee the conditions that it'll be used under. Besides, you wouldn't
want to distribute code that wasn't warning or strict-clean anyway,
right?

- Use Carp
Item "Use Carp"
The Carp module allows you to present your error messages from
the caller's perspective; this gives you a way to signal a problem with
the caller and not your module. For instance, if you say this:
.Sp
.Vb 1
    warn "No hostname given";
.Ve
.Sp
the user will see something like this:
.Sp
.Vb 2
 No hostname given at
 /usr/local/lib/perl5/site_perl/5.6.0/Net/Acme.pm line 123.
.Ve
.Sp
which looks like your module is doing something wrong. Instead, you want
to put the blame on the user, and say this:
.Sp
.Vb 1
    No hostname given at bad_code, line 10.
.Ve
.Sp
You do this by using Carp and replacing your \f(CW\*(C`warn\*(C's with
\f(CW\*(C`carp\*(C's. If you need to \f(CW\*(C`die\*(C', say \f(CW\*(C`croak\*(C' instead. However, keep
\f(CW\*(C`warn\*(C' and \f(CW\*(C`die\*(C' in place for your sanity checks - where it really is
your module at fault.

- Use Exporter - wisely!
Item "Use Exporter - wisely!"
Exporter gives you a standard way of exporting symbols and
subroutines from your module into the caller's namespace. For instance,
saying \f(CW\*(C`use Net::Acme qw(&frob)\*(C' would import the \f(CW\*(C`frob\*(C' subroutine.
.Sp
The package variable \f(CW@EXPORT will determine which symbols will get
exported when the caller simply says \f(CW\*(C`use Net::Acme\*(C' - you will hardly
ever want to put anything in there. \f(CW@EXPORT_OK, on the other hand,
specifies which symbols you're willing to export. If you do want to
export a bunch of symbols, use the \f(CW%EXPORT_TAGS and define a standard
export set - look at Exporter for more details.

- Use plain old documentation
Item "Use plain old documentation"
The work isn't over until the paperwork is done, and you're going to
need to put in some time writing some documentation for your module.
\f(CW\*(C`module-starter\*(C' or \f(CW\*(C`h2xs\*(C' will provide a stub for you to fill in; if
you're not sure about the format, look at perlpod for an
introduction. Provide a good synopsis of how your module is used in
code, a description, and then notes on the syntax and function of the
individual subroutines or methods. Use Perl comments for developer notes
and \s-1POD\s0 for end-user notes.

- Write tests
Item "Write tests"
You're encouraged to create self-tests for your module to ensure it's
working as intended on the myriad platforms Perl supports; if you upload
your module to \s-1CPAN,\s0 a host of testers will build your module and send
you the results of the tests. Again, \f(CW\*(C`module-starter\*(C' and \f(CW\*(C`h2xs\*(C'
provide a test framework which you can extend - you should do something
more than just checking your module will compile.
Test::Simple and Test::More are good
places to start when writing a test suite.

- Write the \fI\s-1README\s0
Item "Write the README"
If you're uploading to \s-1CPAN,\s0 the automated gremlins will extract the
\s-1README\s0 file and place that in your \s-1CPAN\s0 directory. It'll also appear in
the main *by-module* and *by-category* directories if you make it onto
the modules list. It's a good idea to put here what the module actually
does in detail.

- Write \fIChanges
Item "Write Changes"
Add any user-visible changes since the last release to your *Changes*
file.

### Step-by-step: Distributing your module

Subsection "Step-by-step: Distributing your module"

- Get a \s-1CPAN\s0 user \s-1ID\s0
Item "Get a CPAN user ID"
Every developer publishing modules on \s-1CPAN\s0 needs a \s-1CPAN ID.\s0  Visit
\f(CW\*(C`<http://pause.perl.org/>\*(C', select \*(L"Request \s-1PAUSE\s0 Account\*(R", and wait for
your request to be approved by the \s-1PAUSE\s0 administrators.
.ie n .IP """perl Makefile.PL; make test; make distcheck; make dist""" 3
.el .IP "\f(CWperl Makefile.PL; make test; make distcheck; make dist" 3
Item "perl Makefile.PL; make test; make distcheck; make dist"
Once again, \f(CW\*(C`module-starter\*(C' or \f(CW\*(C`h2xs\*(C' has done all the work for you.
They produce the standard \f(CW\*(C`Makefile.PL\*(C' you see when you download and
install modules, and this produces a Makefile with a \f(CW\*(C`dist\*(C' target.
.Sp
Once you've ensured that your module passes its own tests - always a
good thing to make sure - you can \f(CW\*(C`make distcheck\*(C' to make sure
everything looks \s-1OK,\s0 followed by \f(CW\*(C`make dist\*(C', and the Makefile will
hopefully produce you a nice tarball of your module, ready for upload.

- Upload the tarball
Item "Upload the tarball"
The email you got when you received your \s-1CPAN ID\s0 will tell you how to
log in to \s-1PAUSE,\s0 the Perl Authors Upload SErver. From the menus there,
you can upload your module to \s-1CPAN.\s0
.Sp
Alternatively you can use the *cpan-upload* script, part of the
CPAN::Uploader distribution on \s-1CPAN.\s0

- Fix bugs!
Item "Fix bugs!"
Once you start accumulating users, they'll send you bug reports. If
you're lucky, they'll even send you patches. Welcome to the joys of
maintaining a software project...

## AUTHOR

Header "AUTHOR"
Simon Cozens, \f(CW\*(C`simon@cpan.org\*(C'

Updated by Kirrily \*(L"Skud\*(R" Robert, \f(CW\*(C`skud@cpan.org\*(C'

## SEE ALSO

Header "SEE ALSO"
perlmod, perlmodlib, perlmodinstall, h2xs, strict,
Carp, Exporter, perlpod, Test::Simple, Test::More
ExtUtils::MakeMaker, Module::Build, Module::Starter
<http://www.cpan.org/>, Ken Williams' tutorial on building your own
module at <http://mathforum.org/~ken/perl_modules.html>
