+++
author = "None Specified"
manpage_name = "perl581delta"
title = "perl581delta(1)"
manpage_section = "1"
date = "2022-02-19"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "how", "to", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
operating_system_version = "15.3"
operating_system = "macos"
detected_package_version = "5.34.1"
description = "This document describes differences between the 5.8.0 release and the 5.8.1 release. If you are upgrading from an earlier release such as 5.6.1, first read the perl58delta, which describes differences between 5.6.0 and 5.8.0. In case you are wond..."
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL581DELTA 1"
PERL581DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl581delta - what is new for perl v5.8.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.8.0 release and
the 5.8.1 release.

If you are upgrading from an earlier release such as 5.6.1, first read
the perl58delta, which describes differences between 5.6.0 and
5.8.0.

In case you are wondering about 5.6.1, it was bug-fix-wise rather
identical to the development release 5.7.1.  Confused?  This timeline
hopefully helps a bit: it lists the new major releases, their maintenance
releases, and the development releases.

.Vb 1
          New     Maintenance  Development

          5.6.0                             2000-Mar-22
                               5.7.0        2000-Sep-02
                  5.6.1                     2001-Apr-08
                               5.7.1        2001-Apr-09
                               5.7.2        2001-Jul-13
                               5.7.3        2002-Mar-05
          5.8.0                             2002-Jul-18
                  5.8.1                     2003-Sep-25
.Ve

## Incompatible Changes

Header "Incompatible Changes"

### Hash Randomisation

Subsection "Hash Randomisation"
Mainly due to security reasons, the \*(L"random ordering\*(R" of hashes
has been made even more random.  Previously while the order of hash
elements from **keys()**, **values()**, and **each()** was essentially random,
it was still repeatable.  Now, however, the order varies between
different runs of Perl.

**Perl has never guaranteed any ordering of the hash keys**, and the
ordering has already changed several times during the lifetime of
Perl 5.  Also, the ordering of hash keys has always been, and
continues to be, affected by the insertion order.

The added randomness may affect applications.

One possible scenario is when output of an application has included
hash data.  For example, if you have used the Data::Dumper module to
dump data into different files, and then compared the files to see
whether the data has changed, now you will have false positives since
the order in which hashes are dumped will vary.  In general the cure
is to sort the keys (or the values); in particular for Data::Dumper to
use the \f(CW\*(C`Sortkeys\*(C' option.  If some particular order is really
important, use tied hashes: for example the Tie::IxHash module
which by default preserves the order in which the hash elements
were added.

More subtle problem is reliance on the order of \*(L"global destruction\*(R".
That is what happens at the end of execution: Perl destroys all data
structures, including user data.  If your destructors (the \s-1DESTROY\s0
subroutines) have assumed any particular ordering to the global
destruction, there might be problems ahead.  For example, in a
destructor of one object you cannot assume that objects of any other
class are still available, unless you hold a reference to them.
If the environment variable \s-1PERL_DESTRUCT_LEVEL\s0 is set to a non-zero
value, or if Perl is exiting a spawned thread, it will also destruct
the ordinary references and the symbol tables that are no longer in use.
You can't call a class method or an ordinary function on a class that
has been collected that way.

The hash randomisation is certain to reveal hidden assumptions about
some particular ordering of hash elements, and outright bugs: it
revealed a few bugs in the Perl core and core modules.

To disable the hash randomisation in runtime, set the environment
variable \s-1PERL_HASH_SEED\s0 to 0 (zero) before running Perl (for more
information see \*(L"\s-1PERL_HASH_SEED\*(R"\s0 in perlrun), or to disable the feature
completely in compile time, compile with \f(CW\*(C`-DNO_HASH_SEED\*(C' (see *\s-1INSTALL\s0*).

See \*(L"Algorithmic Complexity Attacks\*(R" in perlsec for the original
rationale behind this change.

### \s-1UTF-8\s0 On Filehandles No Longer Activated By Locale

Subsection "UTF-8 On Filehandles No Longer Activated By Locale"
In Perl 5.8.0 all filehandles, including the standard filehandles,
were implicitly set to be in Unicode \s-1UTF-8\s0 if the locale settings
indicated the use of \s-1UTF-8.\s0  This feature caused too many problems,
so the feature was turned off and redesigned: see \*(L"Core Enhancements\*(R".
.ie n .SS "Single-number v-strings are no longer v-strings before ""=>"""
.el .SS "Single-number v-strings are no longer v-strings before ``=>''"
Subsection "Single-number v-strings are no longer v-strings before =>"
The version strings or v-strings (see \*(L"Version Strings\*(R" in perldata)
feature introduced in Perl 5.6.0 has been a source of some confusion\*(--
especially when the user did not want to use it, but Perl thought it
knew better.  Especially troublesome has been the feature that before
a \*(L"=>\*(R" a version string (a \*(L"v\*(R" followed by digits) has been interpreted
as a v-string instead of a string literal.  In other words:

.Vb 1
        %h = ( v65 => 42 );
.Ve

has meant since Perl 5.6.0

.Vb 1
        %h = ( \*(AqA\*(Aq => 42 );
.Ve

(at least in platforms of \s-1ASCII\s0 progeny)  Perl 5.8.1 restores the
more natural interpretation

.Vb 1
        %h = ( \*(Aqv65\*(Aq => 42 );
.Ve

The multi-number v-strings like v65.66 and 65.66.67 still continue to
be v-strings in Perl 5.8.

### (Win32) The -C Switch Has Been Repurposed

Subsection "(Win32) The -C Switch Has Been Repurposed"
The -C switch has changed in an incompatible way.  The old semantics
of this switch only made sense in Win32 and only in the \*(L"use utf8\*(R"
universe in 5.6.x releases, and do not make sense for the Unicode
implementation in 5.8.0.  Since this switch could not have been used
by anyone, it has been repurposed.  The behavior that this switch
enabled in 5.6.x releases may be supported in a transparent,
data-dependent fashion in a future release.

For the new life of this switch, see \*(L"\s-1UTF-8\s0 no longer default under
\s-1UTF-8\s0 locales\*(R", and \*(L"-C\*(R" in perlrun.

### (Win32) The /d Switch Of cmd.exe

Subsection "(Win32) The /d Switch Of cmd.exe"
Perl 5.8.1 uses the /d switch when running the cmd.exe shell
internally for **system()**, backticks, and when opening pipes to external
programs.  The extra switch disables the execution of AutoRun commands
from the registry, which is generally considered undesirable when
running external programs.  If you wish to retain compatibility with
the older behavior, set \s-1PERL5SHELL\s0 in your environment to \f(CW\*(C`cmd /x/c\*(C'.

## Core Enhancements

Header "Core Enhancements"

### \s-1UTF-8\s0 no longer default under \s-1UTF-8\s0 locales

Subsection "UTF-8 no longer default under UTF-8 locales"
In Perl 5.8.0 many Unicode features were introduced.   One of them
was found to be of more nuisance than benefit: the automagic
(and silent) \*(L"UTF-8-ification\*(R" of filehandles, including the
standard filehandles, if the user's locale settings indicated
use of \s-1UTF-8.\s0

For example, if you had \f(CW\*(C`en_US.UTF-8\*(C' as your locale, your \s-1STDIN\s0 and
\s-1STDOUT\s0 were automatically \*(L"\s-1UTF-8\*(R",\s0 in other words an implicit
binmode(..., \*(L":utf8\*(R") was made.  This meant that trying to print, say,
**chr**\|(0xff), ended up printing the bytes 0xc3 0xbf.  Hardly what
you had in mind unless you were aware of this feature of Perl 5.8.0.
The problem is that the vast majority of people weren't: for example
in RedHat releases 8 and 9 the **default** locale setting is \s-1UTF-8,\s0 so
all RedHat users got \s-1UTF-8\s0 filehandles, whether they wanted it or not.
The pain was intensified by the Unicode implementation of Perl 5.8.0
(still) having nasty bugs, especially related to the use of s/// and
tr///.  (Bugs that have been fixed in 5.8.1)

Therefore a decision was made to backtrack the feature and change it
from implicit silent default to explicit conscious option.  The new
Perl command line option \f(CW\*(C`-C\*(C' and its counterpart environment
variable \s-1PERL_UNICODE\s0 can now be used to control how Perl and Unicode
interact at interfaces like I/O and for example the command line
arguments.  See \*(L"-C\*(R" in perlrun and \*(L"\s-1PERL_UNICODE\*(R"\s0 in perlrun for more
information.

### Unsafe signals again available

Subsection "Unsafe signals again available"
In Perl 5.8.0 the so-called \*(L"safe signals\*(R" were introduced.  This
means that Perl no longer handles signals immediately but instead
\*(L"between opcodes\*(R", when it is safe to do so.  The earlier immediate
handling easily could corrupt the internal state of Perl, resulting
in mysterious crashes.

However, the new safer model has its problems too.  Because now an
opcode, a basic unit of Perl execution, is never interrupted but
instead let to run to completion, certain operations that can take a
long time now really do take a long time.  For example, certain
network operations have their own blocking and timeout mechanisms, and
being able to interrupt them immediately would be nice.

Therefore perl 5.8.1 introduces a \*(L"backdoor\*(R" to restore the pre-5.8.0
(pre-5.7.3, really) signal behaviour.  Just set the environment variable
\s-1PERL_SIGNALS\s0 to \f(CW\*(C`unsafe\*(C', and the old immediate (and unsafe)
signal handling behaviour returns.  See \*(L"\s-1PERL_SIGNALS\*(R"\s0 in perlrun
and \*(L"Deferred Signals (Safe Signals)\*(R" in perlipc.

In completely unrelated news, you can now use safe signals with
POSIX::SigAction.  See \*(L"POSIX::SigAction\*(R" in \s-1POSIX\s0.

### Tied Arrays with Negative Array Indices

Subsection "Tied Arrays with Negative Array Indices"
Formerly, the indices passed to \f(CW\*(C`FETCH\*(C', \f(CW\*(C`STORE\*(C', \f(CW\*(C`EXISTS\*(C', and
\f(CW\*(C`DELETE\*(C' methods in tied array class were always non-negative.  If
the actual argument was negative, Perl would call \s-1FETCHSIZE\s0 implicitly
and add the result to the index before passing the result to the tied
array method.  This behaviour is now optional.  If the tied array class
contains a package variable named \f(CW$NEGATIVE_INDICES which is set to
a true value, negative values will be passed to \f(CW\*(C`FETCH\*(C', \f(CW\*(C`STORE\*(C',
\f(CW\*(C`EXISTS\*(C', and \f(CW\*(C`DELETE\*(C' unchanged.

### local $\{$x\}

Subsection "local $\{$x\}"
The syntaxes

.Vb 3
        local $\{$x\}
        local @\{$x\}
        local %\{$x\}
.Ve

now do localise variables, given that the \f(CW$x is a valid variable name.

### Unicode Character Database 4.0.0

Subsection "Unicode Character Database 4.0.0"
The copy of the Unicode Character Database included in Perl 5.8 has
been updated to 4.0.0 from 3.2.0.  This means for example that the
Unicode character properties are as in Unicode 4.0.0.

### Deprecation Warnings

Subsection "Deprecation Warnings"
There is one new feature deprecation.  Perl 5.8.0 forgot to add
some deprecation warnings, these warnings have now been added.
Finally, a reminder of an impending feature removal.

*(Reminder) Pseudo-hashes are deprecated (really)*
Subsection "(Reminder) Pseudo-hashes are deprecated (really)"

Pseudo-hashes were deprecated in Perl 5.8.0 and will be removed in
Perl 5.10.0, see perl58delta for details.  Each attempt to access
pseudo-hashes will trigger the warning \f(CW\*(C`Pseudo-hashes are deprecated\*(C'.
If you really want to continue using pseudo-hashes but not to see the
deprecation warnings, use:

.Vb 1
    no warnings \*(Aqdeprecated\*(Aq;
.Ve

Or you can continue to use the fields pragma, but please don't
expect the data structures to be pseudohashes any more.

*(Reminder) 5.005-style threads are deprecated (really)*
Subsection "(Reminder) 5.005-style threads are deprecated (really)"

5.005-style threads (activated by \f(CW\*(C`use Thread;\*(C') were deprecated in
Perl 5.8.0 and will be removed after Perl 5.8, see perl58delta for
details.  Each 5.005-style thread creation will trigger the warning
\f(CW\*(C`5.005 threads are deprecated\*(C'.  If you really want to continue
using the 5.005 threads but not to see the deprecation warnings, use:

.Vb 1
    no warnings \*(Aqdeprecated\*(Aq;
.Ve

*(Reminder) The $* variable is deprecated (really)*
Subsection "(Reminder) The $* variable is deprecated (really)"

The \f(CW$* variable controlling multi-line matching has been deprecated
and will be removed after 5.8.  The variable has been deprecated for a
long time, and a deprecation warning \f(CW\*(C`Use of $* is deprecated\*(C' is given,
now the variable will just finally be removed.  The functionality has
been supplanted by the \f(CW\*(C`/s\*(C' and \f(CW\*(C`/m\*(C' modifiers on pattern matching.
If you really want to continue using the \f(CW$*-variable but not to see
the deprecation warnings, use:

.Vb 1
    no warnings \*(Aqdeprecated\*(Aq;
.Ve

### Miscellaneous Enhancements

Subsection "Miscellaneous Enhancements"
\f(CW\*(C`map\*(C' in void context is no longer expensive. \f(CW\*(C`map\*(C' is now context
aware, and will not construct a list if called in void context.

If a socket gets closed by the server while printing to it, the client
now gets a \s-1SIGPIPE.\s0  While this new feature was not planned, it fell
naturally out of PerlIO changes, and is to be considered an accidental
feature.

PerlIO::get_layers(\s-1FH\s0) returns the names of the PerlIO layers
active on a filehandle.

PerlIO::via layers can now have an optional \s-1UTF8\s0 method to
indicate whether the layer wants to \*(L"auto-:utf8\*(R" the stream.

**utf8::is_utf8()** has been added as a quick way to test whether
a scalar is encoded internally in \s-1UTF-8\s0 (Unicode).

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules And Pragmata

Subsection "Updated Modules And Pragmata"
The following modules and pragmata have been updated since Perl 5.8.0:

- base
Item "base"
0

- B::Bytecode
Item "B::Bytecode"
.PD
In much better shape than it used to be.  Still far from perfect, but
maybe worth a try.

- B::Concise
Item "B::Concise"
0

- B::Deparse
Item "B::Deparse"

- Benchmark
Item "Benchmark"
.PD
An optional feature, \f(CW\*(C`:hireswallclock\*(C', now allows for high
resolution wall clock times (uses Time::HiRes).

- ByteLoader
Item "ByteLoader"
See B::Bytecode.

- bytes
Item "bytes"
Now has bytes::substr.

- \s-1CGI\s0
Item "CGI"
0

- charnames
Item "charnames"
.PD
One can now have custom character name aliases.

- \s-1CPAN\s0
Item "CPAN"
There is now a simple command line frontend to the \s-1CPAN\s0.pm
module called *cpan*.

- Data::Dumper
Item "Data::Dumper"
A new option, Pair, allows choosing the separator between hash keys
and values.

- DB_File
Item "DB_File"
0

- Devel::PPPort
Item "Devel::PPPort"

- Digest::MD5
Item "Digest::MD5"

- Encode
Item "Encode"
.PD
Significant updates on the encoding pragma functionality
(tr/// and the \s-1DATA\s0 filehandle, formats).
.Sp
If a filehandle has been marked as to have an encoding, unmappable
characters are detected already during input, not later (when the
corrupted data is being used).
.Sp
The \s-1ISO 8859-6\s0 conversion table has been corrected (the 0x30..0x39
erroneously mapped to U+0660..U+0669, instead of U+0030..U+0039).  The
\s-1GSM 03.38\s0 conversion did not handle escape sequences correctly.  The
\s-1UTF-7\s0 encoding has been added (making Encode feature-complete with
Unicode::String).

- fields
Item "fields"
0

- libnet
Item "libnet"

- Math::BigInt
Item "Math::BigInt"
.PD
A lot of bugs have been fixed since v1.60, the version included in Perl
v5.8.0. Especially noteworthy are the bug in Calc that caused div and mod to
fail for some large values, and the fixes to the handling of bad inputs.
.Sp
Some new features were added, e.g. the **broot()** method, you can now pass
parameters to **config()** to change some settings at runtime, and it is now
possible to trap the creation of NaN and infinity.
.Sp
As usual, some optimizations took place and made the math overall a tad
faster. In some cases, quite a lot faster, actually. Especially alternative
libraries like Math::BigInt::GMP benefit from this. In addition, a lot of the
quite clunky routines like **fsqrt()** and **flog()** are now much much faster.

- MIME::Base64
Item "MIME::Base64"
0

- \s-1NEXT\s0
Item "NEXT"
.PD
Diamond inheritance now works.

- Net::Ping
Item "Net::Ping"
0

- PerlIO::scalar
Item "PerlIO::scalar"
.PD
Reading from non-string scalars (like the special variables, see
perlvar) now works.

- podlators
Item "podlators"
0

- Pod::LaTeX
Item "Pod::LaTeX"

- PodParsers
Item "PodParsers"

- Pod::Perldoc
Item "Pod::Perldoc"
.PD
Complete rewrite.  As a side-effect, no longer refuses to startup when
run by root.

- Scalar::Util
Item "Scalar::Util"
New utilities: refaddr, isvstring, looks_like_number, set_prototype.

- Storable
Item "Storable"
Can now store code references (via B::Deparse, so not foolproof).

- strict
Item "strict"
Earlier versions of the strict pragma did not check the parameters
implicitly passed to its \*(L"import\*(R" (use) and \*(L"unimport\*(R" (no) routine.
This caused the false idiom such as:
.Sp
.Vb 2
        use strict qw(@ISA);
        @ISA = qw(Foo);
.Ve
.Sp
This however (probably) raised the false expectation that the strict
refs, vars and subs were being enforced (and that \f(CW@ISA was somehow
\*(L"declared\*(R").  But the strict refs, vars, and subs are **not** enforced
when using this false idiom.
.Sp
Starting from Perl 5.8.1, the above **will** cause an error to be
raised.  This may cause programs which used to execute seemingly
correctly without warnings and errors to fail when run under 5.8.1.
This happens because
.Sp
.Vb 1
        use strict qw(@ISA);
.Ve
.Sp
will now fail with the error:
.Sp
.Vb 1
        Unknown \*(Aqstrict\*(Aq tag(s) \*(Aq@ISA\*(Aq
.Ve
.Sp
The remedy to this problem is to replace this code with the correct idiom:
.Sp
.Vb 3
        use strict;
        use vars qw(@ISA);
        @ISA = qw(Foo);
.Ve

- Term::ANSIcolor
Item "Term::ANSIcolor"
0

- Test::Harness
Item "Test::Harness"
.PD
Now much more picky about extra or missing output from test scripts.

- Test::More
Item "Test::More"
0

- Test::Simple
Item "Test::Simple"

- Text::Balanced
Item "Text::Balanced"

- Time::HiRes
Item "Time::HiRes"
.PD
Use of **nanosleep()**, if available, allows mixing subsecond sleeps with
alarms.

- threads
Item "threads"
Several fixes, for example for **join()** problems and memory
leaks.  In some platforms (like Linux) that use glibc the minimum memory
footprint of one ithread has been reduced by several hundred kilobytes.

- threads::shared
Item "threads::shared"
Many memory leaks have been fixed.

- Unicode::Collate
Item "Unicode::Collate"
0

- Unicode::Normalize
Item "Unicode::Normalize"

- Win32::GetFolderPath
Item "Win32::GetFolderPath"

- Win32::GetOSVersion
Item "Win32::GetOSVersion"
.PD
Now returns extra information.

## Utility Changes

Header "Utility Changes"
The \f(CW\*(C`h2xs\*(C' utility now produces a more modern layout:
*Foo-Bar/lib/Foo/Bar.pm* instead of *Foo/Bar/Bar.pm*.
Also, the boilerplate test is now called *t/Foo-Bar.t*
instead of *t/1.t*.

The Perl debugger (*lib/perl5db.pl*) has now been extensively
documented and bugs found while documenting have been fixed.

\f(CW\*(C`perldoc\*(C' has been rewritten from scratch to be more robust and
feature rich.

\f(CW\*(C`perlcc -B\*(C' works now at least somewhat better, while \f(CW\*(C`perlcc -c\*(C'
is rather more broken.  (The Perl compiler suite as a whole continues
to be experimental.)

## New Documentation

Header "New Documentation"
perl573delta has been added to list the differences between the
(now quite obsolete) development releases 5.7.2 and 5.7.3.

perl58delta has been added: it is the perldelta of 5.8.0, detailing
the differences between 5.6.0 and 5.8.0.

perlartistic has been added: it is the Artistic License in pod format,
making it easier for modules to refer to it.

perlcheat has been added: it is a Perl cheat sheet.

perlgpl has been added: it is the \s-1GNU\s0 General Public License in pod
format, making it easier for modules to refer to it.

perlmacosx has been added to tell about the installation and use
of Perl in Mac \s-1OS X.\s0

perlos400 has been added to tell about the installation and use
of Perl in \s-1OS/400 PASE.\s0

perlreref has been added: it is a regular expressions quick reference.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"
The Unix standard Perl location, */usr/bin/perl*, is no longer
overwritten by default if it exists.  This change was very prudent
because so many Unix vendors already provide a */usr/bin/perl*,
but simultaneously many system utilities may depend on that
exact version of Perl, so better not to overwrite it.

One can now specify installation directories for site and vendor man
and \s-1HTML\s0 pages, and site and vendor scripts.  See *\s-1INSTALL\s0*.

One can now specify a destination directory for Perl installation
by specifying the \s-1DESTDIR\s0 variable for \f(CW\*(C`make install\*(C'.  (This feature
is slightly different from the previous \f(CW\*(C`Configure -Dinstallprefix=...\*(C'.)
See *\s-1INSTALL\s0*.

gcc versions 3.x introduced a new warning that caused a lot of noise
during Perl compilation: \f(CW\*(C`gcc -Ialreadyknowndirectory (warning:
changing search order)\*(C'.  This warning has now been avoided by
Configure weeding out such directories before the compilation.

One can now build subsets of Perl core modules by using the
Configure flags \f(CW\*(C`-Dnoextensions=...\*(C' and \f(CW\*(C`-Donlyextensions=...\*(C',
see *\s-1INSTALL\s0*.

### Platform-specific enhancements

Subsection "Platform-specific enhancements"
In Cygwin Perl can now be built with threads (\f(CW\*(C`Configure -Duseithreads\*(C').
This works with both Cygwin 1.3.22 and Cygwin 1.5.3.

In newer FreeBSD releases Perl 5.8.0 compilation failed because of
trying to use *malloc.h*, which in FreeBSD is just a dummy file, and
a fatal error to even try to use.  Now *malloc.h* is not used.

Perl is now known to build also in Hitachi HI-UXMPP.

Perl is now known to build again in LynxOS.

Mac \s-1OS X\s0 now installs with Perl version number embedded in
installation directory names for easier upgrading of user-compiled
Perl, and the installation directories in general are more standard.
In other words, the default installation no longer breaks the
Apple-provided Perl.  On the other hand, with \f(CW\*(C`Configure -Dprefix=/usr\*(C'
you can now really replace the Apple-supplied Perl (**please be careful**).

Mac \s-1OS X\s0 now builds Perl statically by default.  This change was done
mainly for faster startup times.  The Apple-provided Perl is still
dynamically linked and shared, and you can enable the sharedness for
your own Perl builds by \f(CW\*(C`Configure -Duseshrplib\*(C'.

Perl has been ported to \s-1IBM\s0's \s-1OS/400 PASE\s0 environment.  The best way
to build a Perl for \s-1PASE\s0 is to use an \s-1AIX\s0 host as a cross-compilation
environment.  See \s-1README\s0.os400.

Yet another cross-compilation option has been added: now Perl builds
on OpenZaurus, a Linux distribution based on Mandrake + Embedix for
the Sharp Zaurus \s-1PDA.\s0  See the Cross/README file.

Tru64 when using gcc 3 drops the optimisation for *toke.c* to \f(CW\*(C`-O2\*(C'
because of gigantic memory use with the default \f(CW\*(C`-O3\*(C'.

Tru64 can now build Perl with the newer Berkeley DBs.

Building Perl on WinCE has been much enhanced, see *\s-1README\s0.ce*
and *\s-1README\s0.perlce*.

## Selected Bug Fixes

Header "Selected Bug Fixes"

### Closures, eval and lexicals

Subsection "Closures, eval and lexicals"
There have been many fixes in the area of anonymous subs, lexicals and
closures.  Although this means that Perl is now more \*(L"correct\*(R", it is
possible that some existing code will break that happens to rely on
the faulty behaviour.  In practice this is unlikely unless your code
contains a very complex nesting of anonymous subs, evals and lexicals.

### Generic fixes

Subsection "Generic fixes"
If an input filehandle is marked \f(CW\*(C`:utf8\*(C' and Perl sees illegal \s-1UTF-8\s0
coming in when doing \f(CW\*(C`<FH>\*(C', if warnings are enabled a warning is
immediately given - instead of being silent about it and Perl being
unhappy about the broken data later.  (The \f(CW\*(C`:encoding(utf8)\*(C' layer
also works the same way.)

binmode(\s-1SOCKET,\s0 \*(L":utf8\*(R") only worked on the input side, not on the
output side of the socket.  Now it works both ways.

For threaded Perls certain system database functions like **getpwent()**
and **getgrent()** now grow their result buffer dynamically, instead of
failing.  This means that at sites with lots of users and groups the
functions no longer fail by returning only partial results.

Perl 5.8.0 had accidentally broken the capability for users
to define their own uppercase<->lowercase Unicode mappings
(as advertised by the Camel).  This feature has been fixed and
is also documented better.

In 5.8.0 this

.Vb 1
        $some_unicode .= <FH>;
.Ve

didn't work correctly but instead corrupted the data.  This has now
been fixed.

Tied methods like \s-1FETCH\s0 etc. may now safely access tied values, i.e.
resulting in a recursive call to \s-1FETCH\s0 etc.  Remember to break the
recursion, though.

At startup Perl blocks the \s-1SIGFPE\s0 signal away since there isn't much
Perl can do about it.  Previously this blocking was in effect also for
programs executed from within Perl.  Now Perl restores the original
\s-1SIGFPE\s0 handling routine, whatever it was, before running external
programs.

Linenumbers in Perl scripts may now be greater than 65536, or 2**16.
(Perl scripts have always been able to be larger than that, it's just
that the linenumber for reported errors and warnings have \*(L"wrapped
around\*(R".)  While scripts that large usually indicate a need to rethink
your code a bit, such Perl scripts do exist, for example as results
from generated code.  Now linenumbers can go all the way to
4294967296, or 2**32.

### Platform-specific fixes

Subsection "Platform-specific fixes"
Linux

- \(bu
Setting \f(CW$0 works again (with certain limitations that
Perl cannot do much about: see \*(L"$0\*(R" in perlvar)

HP-UX

- \(bu
Setting \f(CW$0 now works.

\s-1VMS\s0

- \(bu
Configuration now tests for the presence of \f(CW\*(C`poll()\*(C', and IO::Poll
now uses the vendor-supplied function if detected.

- \(bu
A rare access violation at Perl start-up could occur if the Perl image was
installed with privileges or if there was an identifier with the
subsystem attribute set in the process's rightslist.  Either of these
circumstances triggered tainting code that contained a pointer bug.
The faulty pointer arithmetic has been fixed.

- \(bu
The length limit on values (not keys) in the \f(CW%ENV hash has been raised
from 255 bytes to 32640 bytes (except when the \s-1PERL_ENV_TABLES\s0 setting
overrides the default use of logical names for \f(CW%ENV).  If it is
necessary to access these long values from outside Perl, be aware that
they are implemented using search list logical names that store the
value in pieces, each 255-byte piece (up to 128 of them) being an
element in the search list. When doing a lookup in \f(CW%ENV from within
Perl, the elements are combined into a single value.  The existing
VMS-specific ability to access individual elements of a search list
logical name via the \f(CW$ENV\{'foo;N'\} syntax (where N is the search list
index) is unimpaired.

- \(bu
The piping implementation now uses local rather than global \s-1DCL\s0
symbols for inter-process communication.

- \(bu
File::Find could become confused when navigating to a relative
directory whose name collided with a logical name.  This problem has
been corrected by adding directory syntax to relative path names, thus
preventing logical name translation.

Win32

- \(bu
A memory leak in the **fork()** emulation has been fixed.

- \(bu
The return value of the **ioctl()** built-in function was accidentally
broken in 5.8.0.  This has been corrected.

- \(bu
The internal message loop executed by perl during blocking operations
sometimes interfered with messages that were external to Perl.
This often resulted in blocking operations terminating prematurely or
returning incorrect results, when Perl was executing under environments
that could generate Windows messages.  This has been corrected.

- \(bu
Pipes and sockets are now automatically in binary mode.

- \(bu
The four-argument form of **select()** did not preserve $! (errno) properly
when there were errors in the underlying call.  This is now fixed.

- \(bu
The \*(L"\s-1CR CR LF\*(R"\s0 problem of has been fixed, binmode(\s-1FH,\s0 \*(L":crlf\*(R")
is now effectively a no-op.

## New or Changed Diagnostics

Header "New or Changed Diagnostics"
All the warnings related to **pack()** and **unpack()** were made more
informative and consistent.
.ie n .SS "Changed ""A thread exited while %d threads were running"""
.el .SS "Changed ``A thread exited while \f(CW%d threads were running''"
Subsection "Changed A thread exited while %d threads were running"
The old version

.Vb 1
    A thread exited while %d other threads were still running
.Ve

was misleading because the \*(L"other\*(R" included also the thread giving
the warning.
.ie n .SS "Removed ""Attempt to clear a restricted hash"""
.el .SS "Removed ``Attempt to clear a restricted hash''"
Subsection "Removed Attempt to clear a restricted hash"
It is not illegal to clear a restricted hash, so the warning
was removed.
.ie n .SS "New ""Illegal declaration of anonymous subroutine"""
.el .SS "New ``Illegal declaration of anonymous subroutine''"
Subsection "New Illegal declaration of anonymous subroutine"
You must specify the block of code for \f(CW\*(C`sub\*(C'.
.ie n .SS "Changed ""Invalid range ""%s"" in transliteration operator"""
.el .SS "Changed ``Invalid range ''%s`` in transliteration operator''"
Subsection "Changed Invalid range %s in transliteration operator"
The old version

.Vb 1
    Invalid [] range "%s" in transliteration operator
.Ve

was simply wrong because there are no \*(L"[] ranges\*(R" in tr///.
.ie n .SS "New ""Missing control char name in \\c"""
.el .SS "New ``Missing control char name in \\c''"
Subsection "New Missing control char name in c"
Self-explanatory.
.ie n .SS "New ""Newline in left-justified string for %s"""
.el .SS "New ``Newline in left-justified string for \f(CW%s''"
Subsection "New Newline in left-justified string for %s"
The padding spaces would appear after the newline, which is
probably not what you had in mind.
.ie n .SS "New ""Possible precedence problem on bitwise %c operator"""
.el .SS "New ``Possible precedence problem on bitwise \f(CW%c operator''"
Subsection "New Possible precedence problem on bitwise %c operator"
If you think this

.Vb 1
    $x & $y == 0
.Ve

tests whether the bitwise \s-1AND\s0 of \f(CW$x and \f(CW$y is zero,
you will like this warning.
.ie n .SS "New ""Pseudo-hashes are deprecated"""
.el .SS "New ``Pseudo-hashes are deprecated''"
Subsection "New Pseudo-hashes are deprecated"
This warning should have been already in 5.8.0, since they are.
.ie n .SS "New ""**read()** on %s filehandle %s"""
.el .SS "New ``**read()** on \f(CW%s filehandle \f(CW%s''"
Subsection "New read() on %s filehandle %s"
You cannot **read()** (or **sysread()**) from a closed or unopened filehandle.
.ie n .SS "New ""5.005 threads are deprecated"""
.el .SS "New ``5.005 threads are deprecated''"
Subsection "New 5.005 threads are deprecated"
This warning should have been already in 5.8.0, since they are.
.ie n .SS "New ""Tied variable freed while still in use"""
.el .SS "New ``Tied variable freed while still in use''"
Subsection "New Tied variable freed while still in use"
Something pulled the plug on a live tied variable, Perl plays
safe by bailing out.
.ie n .SS "New ""To%s: illegal mapping '%s'"""
.el .SS "New ``To%s: illegal mapping '%s'''"
Subsection "New To%s: illegal mapping '%s'"
An illegal user-defined Unicode casemapping was specified.
.ie n .SS "New ""Use of freed value in iteration"""
.el .SS "New ``Use of freed value in iteration''"
Subsection "New Use of freed value in iteration"
Something modified the values being iterated over.  This is not good.

## Changed Internals

Header "Changed Internals"
These news matter to you only if you either write \s-1XS\s0 code or like to
know about or hack Perl internals (using Devel::Peek or any of the
\f(CW\*(C`B::\*(C' modules counts), or like to run Perl with the \f(CW\*(C`-D\*(C' option.

The embedding examples of perlembed have been reviewed to be
up to date and consistent: for example, the correct use of
\s-1**PERL_SYS_INIT3\s0()** and \s-1**PERL_SYS_TERM\s0()**.

Extensive reworking of the pad code (the code responsible
for lexical variables) has been conducted by Dave Mitchell.

Extensive work on the v-strings by John Peacock.

\s-1UTF-8\s0 length and position cache: to speed up the handling of Unicode
(\s-1UTF-8\s0) scalars, a cache was introduced.  Potential problems exist if
an extension bypasses the official APIs and directly modifies the \s-1PV\s0
of an \s-1SV:\s0 the \s-1UTF-8\s0 cache does not get cleared as it should.

APIs obsoleted in Perl 5.8.0, like sv_2pv, sv_catpvn, sv_catsv,
sv_setsv, are again available.

Certain Perl core C APIs like cxinc and regatom are no longer
available at all to code outside the Perl core of the Perl core
extensions.  This is intentional.  They never should have been
available with the shorter names, and if you application depends on
them, you should (be ashamed and) contact perl5-porters to discuss
what are the proper APIs.

Certain Perl core C APIs like \f(CW\*(C`Perl_list\*(C' are no longer available
without their \f(CW\*(C`Perl_\*(C' prefix.  If your \s-1XS\s0 module stops working
because some functions cannot be found, in many cases a simple fix is
to add the \f(CW\*(C`Perl_\*(C' prefix to the function and the thread context
\f(CW\*(C`aTHX_\*(C' as the first argument of the function call.  This is also how
it should always have been done: letting the Perl_-less forms to leak
from the core was an accident.  For cleaner embedding you can also
force this for all APIs by defining at compile time the cpp define
\s-1PERL_NO_SHORT_NAMES.\s0

**Perl_save_bool()** has been added.

Regexp objects (those created with \f(CW\*(C`qr\*(C') now have S-magic rather than
R-magic.  This fixed regexps of the form /...(??\{...;$x\})/ to no
longer ignore changes made to \f(CW$x.  The S-magic avoids dropping
the caching optimization and making (??\{...\}) constructs obscenely
slow (and consequently useless).  See also \*(L"Magic Variables\*(R" in perlguts.
Regexp::Copy was affected by this change.

The Perl internal debugging macros \s-1**DEBUG\s0()** and \s-1**DEB\s0()** have been renamed
to \s-1**PERL_DEBUG\s0()** and \s-1**PERL_DEB\s0()** to avoid namespace conflicts.

\f(CW\*(C`-DL\*(C' removed (the leaktest had been broken and unsupported for years,
use alternative debugging mallocs or tools like valgrind and Purify).

Verbose modifier \f(CW\*(C`v\*(C' added for \f(CW\*(C`-DXv\*(C' and \f(CW\*(C`-Dsv\*(C', see perlrun.

## New Tests

Header "New Tests"
In Perl 5.8.0 there were about 69000 separate tests in about 700 test files,
in Perl 5.8.1 there are about 77000 separate tests in about 780 test files.
The exact numbers depend on the Perl configuration and on the operating
system platform.

## Known Problems

Header "Known Problems"
The hash randomisation mentioned in \*(L"Incompatible Changes\*(R" is definitely
problematic: it will wake dormant bugs and shake out bad assumptions.

If you want to use mod_perl 2.x with Perl 5.8.1, you will need
mod_perl-1.99_10 or higher.  Earlier versions of mod_perl 2.x
do not work with the randomised hashes.  (mod_perl 1.x works fine.)
You will also need Apache::Test 1.04 or higher.

Many of the rarer platforms that worked 100% or pretty close to it
with perl 5.8.0 have been left a little bit untended since their
maintainers have been otherwise busy lately, and therefore there will
be more failures on those platforms.  Such platforms include Mac \s-1OS\s0
Classic, \s-1IBM\s0 z/OS (and other \s-1EBCDIC\s0 platforms), and NetWare.  The most
common Perl platforms (Unix and Unix-like, Microsoft platforms, and
\s-1VMS\s0) have large enough testing and expert population that they are
doing well.

### Tied hashes in scalar context

Subsection "Tied hashes in scalar context"
Tied hashes do not currently return anything useful in scalar context,
for example when used as boolean tests:

.Vb 1
        if (%tied_hash) \{ ... \}
.Ve

The current nonsensical behaviour is always to return false,
regardless of whether the hash is empty or has elements.

The root cause is that there is no interface for the implementors of
tied hashes to implement the behaviour of a hash in scalar context.

### Net::Ping 450_service and 510_ping_udp failures

Subsection "Net::Ping 450_service and 510_ping_udp failures"
The subtests 9 and 18 of lib/Net/Ping/t/450_service.t, and the
subtest 2 of lib/Net/Ping/t/510_ping_udp.t might fail if you have
an unusual networking setup.  For example in the latter case the
test is trying to send a \s-1UDP\s0 ping to the \s-1IP\s0 address 127.0.0.1.

### B::C

Subsection "B::C"
The C-generating compiler backend B::C (the frontend being
\f(CW\*(C`perlcc -c\*(C') is even more broken than it used to be because of
the extensive lexical variable changes.  (The good news is that
B::Bytecode and ByteLoader are better than they used to be.)

## Platform Specific Problems

Header "Platform Specific Problems"

### \s-1EBCDIC\s0 Platforms

Subsection "EBCDIC Platforms"
\s-1IBM\s0 z/OS and other \s-1EBCDIC\s0 platforms continue to be problematic
regarding Unicode support.  Many Unicode tests are skipped when
they really should be fixed.

### Cygwin 1.5 problems

Subsection "Cygwin 1.5 problems"
In Cygwin 1.5 the *io/tell* and *op/sysio* tests have failures for
some yet unknown reason.  In 1.5.5 the threads tests stress_cv,
stress_re, and stress_string are failing unless the environment
variable \s-1PERLIO\s0 is set to \*(L"perlio\*(R" (which makes also the io/tell
failure go away).

Perl 5.8.1 does build and work well with Cygwin 1.3: with (uname -a)
\f(CW\*(C`CYGWIN_NT-5.0 ... 1.3.22(0.78/3/2) 2003-03-18 09:20 i686 ...\*(C'
a 100% \*(L"make test\*(R"  was achieved with \f(CW\*(C`Configure -des -Duseithreads\*(C'.

### HP-UX: \s-1HP\s0 cc warnings about sendfile and sendpath

Subsection "HP-UX: HP cc warnings about sendfile and sendpath"
With certain \s-1HP C\s0 compiler releases (e.g. B.11.11.02) you will
get many warnings like this (lines wrapped for easier reading):

.Vb 6
  cc: "/usr/include/sys/socket.h", line 504: warning 562:
    Redeclaration of "sendfile" with a different storage class specifier:
      "sendfile" will have internal linkage.
  cc: "/usr/include/sys/socket.h", line 505: warning 562:
    Redeclaration of "sendpath" with a different storage class specifier:
      "sendpath" will have internal linkage.
.Ve

The warnings show up both during the build of Perl and during certain
lib/ExtUtils tests that invoke the C compiler.  The warning, however,
is not serious and can be ignored.

### \s-1IRIX:\s0 t/uni/tr_7jis.t falsely failing

Subsection "IRIX: t/uni/tr_7jis.t falsely failing"
The test t/uni/tr_7jis.t is known to report failure under 'make test'
or the test harness with certain releases of \s-1IRIX\s0 (at least \s-1IRIX 6.5\s0
and MIPSpro Compilers Version 7.3.1.1m), but if run manually the test
fully passes.

### Mac \s-1OS X:\s0 no usemymalloc

Subsection "Mac OS X: no usemymalloc"
The Perl malloc (\f(CW\*(C`-Dusemymalloc\*(C') does not work at all in Mac \s-1OS X.\s0
This is not that serious, though, since the native malloc works just
fine.

### Tru64: No threaded builds with \s-1GNU\s0 cc (gcc)

Subsection "Tru64: No threaded builds with GNU cc (gcc)"
In the latest Tru64 releases (e.g. v5.1B or later) gcc cannot be used
to compile a threaded Perl (-Duseithreads) because the system
\f(CW\*(C`<pthread.h>\*(C' file doesn't know about gcc.

### Win32: sysopen, sysread, syswrite

Subsection "Win32: sysopen, sysread, syswrite"
As of the 5.8.0 release, **sysopen()**/**sysread()**/**syswrite()** do not behave
like they used to in 5.6.1 and earlier with respect to \*(L"text\*(R" mode.
These built-ins now always operate in \*(L"binary\*(R" mode (even if **sysopen()**
was passed the O_TEXT flag, or if **binmode()** was used on the file
handle).  Note that this issue should only make a difference for disk
files, as sockets and pipes have always been in \*(L"binary\*(R" mode in the
Windows port.  As this behavior is currently considered a bug,
compatible behavior may be re-introduced in a future release.  Until
then, the use of **sysopen()**, **sysread()** and **syswrite()** is not supported
for \*(L"text\*(R" mode operations.

## Future Directions

Header "Future Directions"
The following things **might** happen in future.  The first publicly
available releases having these characteristics will be the developer
releases Perl 5.9.x, culminating in the Perl 5.10.0 release.  These
are our best guesses at the moment: we reserve the right to rethink.

- \(bu
PerlIO will become The Default.  Currently (in Perl 5.8.x) the stdio
library is still used if Perl thinks it can use certain tricks to
make stdio go **really** fast.  For future releases our goal is to
make PerlIO go even faster.

- \(bu
A new feature called *assertions* will be available.  This means that
one can have code called assertions sprinkled in the code: usually
they are optimised away, but they can be enabled with the \f(CW\*(C`-A\*(C' option.

- \(bu
A new operator \f(CW\*(C`//\*(C' (defined-or) will be available.  This means that
one will be able to say
.Sp
.Vb 1
    $a // $b
.Ve
.Sp
instead of
.Sp
.Vb 1
   defined $a ? $a : $b
.Ve
.Sp
and
.Sp
.Vb 1
   $c //= $d;
.Ve
.Sp
instead of
.Sp
.Vb 1
   $c = $d unless defined $c;
.Ve
.Sp
The operator will have the same precedence and associativity as \f(CW\*(C`||\*(C'.
A source code patch against the Perl 5.8.1 sources will be available
in \s-1CPAN\s0 as *authors/id/H/HM/HMBRAND/dor-5.8.1.diff*.

- \(bu
\f(CW\*(C`unpack()\*(C' will default to unpacking the \f(CW$_.

- \(bu
Various Copy-On-Write techniques will be investigated in hopes
of speeding up Perl.

- \(bu
\s-1CPANPLUS,\s0 Inline, and Module::Build will become core modules.

- \(bu
The ability to write true lexically scoped pragmas will be introduced.

- \(bu
Work will continue on the bytecompiler and byteloader.

- \(bu
v-strings as they currently exist are scheduled to be deprecated.  The
v-less form (1.2.3) will become a \*(L"version object\*(R" when used with \f(CW\*(C`use\*(C',
\f(CW\*(C`require\*(C', and \f(CW$VERSION.  $^V will also be a \*(L"version object\*(R" so the
printf(\*(L"%vd\*(R",...) construct will no longer be needed.  The v-ful version
(v1.2.3) will become obsolete.  The equivalence of strings and v-strings (e.g.
that currently 5.8.0 is equal to \*(L"\\5\\8\\0\*(R") will go away.  \fBThere may be no
deprecation warning for v-strings, though: it is quite hard to detect when
v-strings are being used safely, and when they are not.

- \(bu
5.005 Threads Will Be Removed

- \(bu
The \f(CW$* Variable Will Be Removed
(it was deprecated a long time ago)

- \(bu
Pseudohashes Will Be Removed

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://bugs.perl.org/ .  There may also be
information at http://www.perl.com/ , the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.  You can browse and search
the Perl 5 bugs at http://bugs.perl.org/

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for exhaustive details on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
