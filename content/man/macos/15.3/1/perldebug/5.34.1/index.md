+++
title = "perldebug(1)"
manpage_format = "troff"
manpage_section = "1"
operating_system_version = "15.3"
date = "2022-02-19"
keywords = ["header", "see", "also", "you", "do", "have", "f", "cw", "c", "use", "strict", "and", "warnings", "enabled", "don", "t", "perldebtut", "perldebguts", "perl5db", "pl", "re", "s-1db", "s0", "devel", "nytprof", "dumpvalue", "perlrun", "when", "debugging", "a", "script", "that", "uses", "is", "thus", "normally", "found", "in", "path", "the", "s", "option", "causes", "perl", "to", "search", "for", "it", "so", "type", "or", "which", "scriptname", "vb", "1", "sd", "foo", "ve", "bugs", "cannot", "get", "stack", "frame", "information", "any", "fashion", "debug", "functions", "were", "not", "compiled", "by", "such", "as", "those", "from", "extensions", "if", "alter", "your", "_", "arguments", "subroutine", "with", "shift", "pop", "backtrace", "will", "show", "original", "values", "debugger", "does", "currently", "work", "conjunction", "fb-w", "command-line", "switch", "because", "itself", "free", "of", "slow", "syscall", "like", "wait", "ing", "accept", "read", "keyboard", "socket", "haven", "set", "up", "own", "sig", "int", "handler", "then", "won", "be", "able", "ctrl-c", "way", "back", "doesn", "understand", "needs", "raise", "an", "exception", "fblongjmp", "3", "out", "syscalls"]
detected_package_version = "5.34.1"
manpage_name = "perldebug"
description = "First of all, have you tried using f(CW*(C`use strict;*(C and f(CW*(C`use warnings;*(C? If youre new to the Perl debugger, you may prefer to read perldebtut, which is a tutorial introduction to the debugger. If youre looking for the nitty grit..."
operating_system = "macos"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLDEBUG 1"
PERLDEBUG 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perldebug - Perl debugging
Xref "debug debugger"

## DESCRIPTION

Header "DESCRIPTION"
First of all, have you tried using \f(CW\*(C`use strict;\*(C' and
\f(CW\*(C`use warnings;\*(C'?

If you're new to the Perl debugger, you may prefer to read
perldebtut, which is a tutorial introduction to the debugger.

If you're looking for the nitty gritty details of how the debugger is
*implemented*, you may prefer to read perldebguts.

For in-depth technical usage details, see perl5db.pl, the documentation
of the debugger itself.

## The Perl Debugger

Header "The Perl Debugger"
If you invoke Perl with the **-d** switch, your script runs under the
Perl source debugger.  This works like an interactive Perl
environment, prompting for debugger commands that let you examine
source code, set breakpoints, get stack backtraces, change the values of
variables, etc.  This is so convenient that you often fire up
the debugger all by itself just to test out Perl constructs
interactively to see what they do.  For example:
Xref "-d"

.Vb 1
    $ perl -d -e 42
.Ve

In Perl, the debugger is not a separate program the way it usually is in the
typical compiled environment.  Instead, the **-d** flag tells the compiler
to insert source information into the parse trees it's about to hand off
to the interpreter.  That means your code must first compile correctly
for the debugger to work on it.  Then when the interpreter starts up, it
preloads a special Perl library file containing the debugger.

The program will halt *right before* the first run-time executable
statement (but see below regarding compile-time statements) and ask you
to enter a debugger command.  Contrary to popular expectations, whenever
the debugger halts and shows you a line of code, it always displays the
line it's *about* to execute, rather than the one it has just executed.

Any command not recognized by the debugger is directly executed
(\f(CW\*(C`eval\*(C''d) as Perl code in the current package.  (The debugger
uses the \s-1DB\s0 package for keeping its own state information.)

Note that the said \f(CW\*(C`eval\*(C' is bound by an implicit scope. As a
result any newly introduced lexical variable or any modified
capture buffer content is lost after the eval. The debugger is a
nice environment to learn Perl, but if you interactively experiment using
material which should be in the same scope, stuff it in one line.

For any text entered at the debugger prompt, leading and trailing whitespace
is first stripped before further processing.  If a debugger command
coincides with some function in your own program, merely precede the
function with something that doesn't look like a debugger command, such
as a leading \f(CW\*(C`;\*(C' or perhaps a \f(CW\*(C`+\*(C', or by wrapping it with parentheses
or braces.

### Calling the Debugger

Subsection "Calling the Debugger"
There are several ways to call the debugger:

- perl -d program_name
Item "perl -d program_name"
On the given program identified by \f(CW\*(C`program_name\*(C'.

- perl -d -e 0
Item "perl -d -e 0"
Interactively supply an arbitrary \f(CW\*(C`expression\*(C' using \f(CW\*(C`-e\*(C'.

- perl -d:ptkdb program_name
Item "perl -d:ptkdb program_name"
Debug a given program via the \f(CW\*(C`Devel::ptkdb\*(C' \s-1GUI.\s0

- perl -dt threaded_program_name
Item "perl -dt threaded_program_name"
Debug a given program using threads (experimental).

### Debugger Commands

Subsection "Debugger Commands"
The interactive debugger understands the following commands:

- h
Xref "debugger command, h"
Item "h"
Prints out a summary help message

- h [command]
Item "h [command]"
Prints out a help message for the given debugger command.

- h h
Item "h h"
The special argument of \f(CW\*(C`h h\*(C' produces the entire help page, which is quite long.
.Sp
If the output of the \f(CW\*(C`h h\*(C' command (or any command, for that matter) scrolls
past your screen, precede the command with a leading pipe symbol so
that it's run through your pager, as in
.Sp
.Vb 1
    DB> |h h
.Ve
.Sp
You may change the pager which is used via \f(CW\*(C`o pager=...\*(C' command.

- p expr
Xref "debugger command, p"
Item "p expr"
Same as \f(CW\*(C`print \{$DB::OUT\} expr\*(C' in the current package.  In particular,
because this is just Perl's own \f(CW\*(C`print\*(C' function, this means that nested
data structures and objects are not dumped, unlike with the \f(CW\*(C`x\*(C' command.
.Sp
The \f(CW\*(C`DB::OUT\*(C' filehandle is opened to */dev/tty*, regardless of
where \s-1STDOUT\s0 may be redirected to.

- x [maxdepth] expr
Xref "debugger command, x"
Item "x [maxdepth] expr"
Evaluates its expression in list context and dumps out the result in a
pretty-printed fashion.  Nested data structures are printed out
recursively, unlike the real \f(CW\*(C`print\*(C' function in Perl.  When dumping
hashes, you'll probably prefer 'x \\%h' rather than 'x \f(CW%h'.
See Dumpvalue if you'd like to do this yourself.
.Sp
The output format is governed by multiple options described under
\*(L"Configurable Options\*(R".
.Sp
If the \f(CW\*(C`maxdepth\*(C' is included, it must be a numeral *N*; the value is
dumped only *N* levels deep, as if the \f(CW\*(C`dumpDepth\*(C' option had been
temporarily set to *N*.

- V [pkg [vars]]
Xref "debugger command, V"
Item "V [pkg [vars]]"
Display all (or some) variables in package (defaulting to \f(CW\*(C`main\*(C')
using a data pretty-printer (hashes show their keys and values so
you see what's what, control characters are made printable, etc.).
Make sure you don't put the type specifier (like \f(CW\*(C`$\*(C') there, just
the symbol names, like this:
.Sp
.Vb 1
    V DB filename line
.Ve
.Sp
Use \f(CW\*(C`~pattern\*(C' and \f(CW\*(C`!pattern\*(C' for positive and negative regexes.
.Sp
This is similar to calling the \f(CW\*(C`x\*(C' command on each applicable var.

- X [vars]
Xref "debugger command, X"
Item "X [vars]"
Same as \f(CW\*(C`V currentpackage [vars]\*(C'.

- y [level [vars]]
Xref "debugger command, y"
Item "y [level [vars]]"
Display all (or some) lexical variables (mnemonic: \f(CW\*(C`mY\*(C' variables)
in the current scope or *level* scopes higher.  You can limit the
variables that you see with *vars* which works exactly as it does
for the \f(CW\*(C`V\*(C' and \f(CW\*(C`X\*(C' commands.  Requires the \f(CW\*(C`PadWalker\*(C' module
version 0.08 or higher; will warn if this isn't installed.  Output
is pretty-printed in the same style as for \f(CW\*(C`V\*(C' and the format is
controlled by the same options.

- T
Xref "debugger command, T backtrace stack, backtrace"
Item "T"
Produce a stack backtrace.  See below for details on its output.

- s [expr]
Xref "debugger command, s step"
Item "s [expr]"
Single step.  Executes until the beginning of another
statement, descending into subroutine calls.  If an expression is
supplied that includes function calls, it too will be single-stepped.

- n [expr]
Xref "debugger command, n"
Item "n [expr]"
Next.  Executes over subroutine calls, until the beginning
of the next statement.  If an expression is supplied that includes
function calls, those functions will be executed with stops before
each statement.

- r
Xref "debugger command, r"
Item "r"
Continue until the return from the current subroutine.
Dump the return value if the \f(CW\*(C`PrintRet\*(C' option is set (default).

- <\s-1CR\s0>
Item "<CR>"
Repeat last \f(CW\*(C`n\*(C' or \f(CW\*(C`s\*(C' command.

- c [line|sub]
Xref "debugger command, c"
Item "c [line|sub]"
Continue, optionally inserting a one-time-only breakpoint
at the specified line or subroutine.

- l
Xref "debugger command, l"
Item "l"
List next window of lines.

- l min+incr
Item "l min+incr"
List \f(CW\*(C`incr+1\*(C' lines starting at \f(CW\*(C`min\*(C'.

- l min-max
Item "l min-max"
List lines \f(CW\*(C`min\*(C' through \f(CW\*(C`max\*(C'.  \f(CW\*(C`l -\*(C' is synonymous to \f(CW\*(C`-\*(C'.

- l line
Item "l line"
List a single line.

- l subname
Item "l subname"
List first window of lines from subroutine.  *subname* may
be a variable that contains a code reference.

- -
Xref "debugger command, -"
List previous window of lines.

- v [line]
Xref "debugger command, v"
Item "v [line]"
View a few lines of code around the current line.

- .
Xref "debugger command, ."
Return the internal debugger pointer to the line last
executed, and print out that line.

- f filename
Xref "debugger command, f"
Item "f filename"
Switch to viewing a different file or \f(CW\*(C`eval\*(C' statement.  If *filename*
is not a full pathname found in the values of \f(CW%INC, it is considered
a regex.
.Sp
\f(CW\*(C`eval\*(C'ed strings (when accessible) are considered to be filenames:
\f(CW\*(C`f (eval 7)\*(C' and \f(CW\*(C`f eval 7\\b\*(C' access the body of the 7th \f(CW\*(C`eval\*(C'ed string
(in the order of execution).  The bodies of the currently executed \f(CW\*(C`eval\*(C'
and of \f(CW\*(C`eval\*(C'ed strings that define subroutines are saved and thus
accessible.

- /pattern/
Item "/pattern/"
Search forwards for pattern (a Perl regex); final / is optional.
The search is case-insensitive by default.

- ?pattern?
Item "?pattern?"
Search backwards for pattern; final ? is optional.
The search is case-insensitive by default.

- L [abw]
Xref "debugger command, L"
Item "L [abw]"
List (default all) actions, breakpoints and watch expressions

- S [[!]regex]
Xref "debugger command, S"
Item "S [[!]regex]"
List subroutine names [not] matching the regex.

- t [n]
Xref "debugger command, t"
Item "t [n]"
Toggle trace mode (see also the \f(CW\*(C`AutoTrace\*(C' option).
Optional argument is the maximum number of levels to trace below
the current one; anything deeper than that will be silent.

- t [n] expr
Xref "debugger command, t"
Item "t [n] expr"
Trace through execution of \f(CW\*(C`expr\*(C'.
Optional first argument is the maximum number of levels to trace below
the current one; anything deeper than that will be silent.
See \*(L"Frame Listing Output Examples\*(R" in perldebguts for examples.

- b
Xref "breakpoint debugger command, b"
Item "b"
Sets breakpoint on current line

- b [line] [condition]
Xref "breakpoint debugger command, b"
Item "b [line] [condition]"
Set a breakpoint before the given line.  If a condition
is specified, it's evaluated each time the statement is reached: a
breakpoint is taken only if the condition is true.  Breakpoints may
only be set on lines that begin an executable statement.  Conditions
don't use \f(CW\*(C`if\*(C':
.Sp
.Vb 3
    b 237 $x > 30
    b 237 ++$count237 < 11
    b 33 /pattern/i
.Ve
.Sp
If the line number is \f(CW\*(C`.\*(C', sets a breakpoint on the current line:
.Sp
.Vb 1
    b . $n > 100
.Ve

- b [file]:[line] [condition]
Xref "breakpoint debugger command, b"
Item "b [file]:[line] [condition]"
Set a breakpoint before the given line in a (possibly different) file.  If a
condition is specified, it's evaluated each time the statement is reached: a
breakpoint is taken only if the condition is true.  Breakpoints may only be set
on lines that begin an executable statement.  Conditions don't use \f(CW\*(C`if\*(C':
.Sp
.Vb 2
    b lib/MyModule.pm:237 $x > 30
    b /usr/lib/perl5/site_perl/CGI.pm:100 ++$count100 < 11
.Ve

- b subname [condition]
Xref "breakpoint debugger command, b"
Item "b subname [condition]"
Set a breakpoint before the first line of the named subroutine.  *subname* may
be a variable containing a code reference (in this case *condition*
is not supported).

- b postpone subname [condition]
Xref "breakpoint debugger command, b"
Item "b postpone subname [condition]"
Set a breakpoint at first line of subroutine after it is compiled.

- b load filename
Xref "breakpoint debugger command, b"
Item "b load filename"
Set a breakpoint before the first executed line of the *filename*,
which should be a full pathname found amongst the \f(CW%INC values.

- b compile subname
Xref "breakpoint debugger command, b"
Item "b compile subname"
Sets a breakpoint before the first statement executed after the specified
subroutine is compiled.

- B line
Xref "breakpoint debugger command, B"
Item "B line"
Delete a breakpoint from the specified *line*.

- B *
Xref "breakpoint debugger command, B"
Item "B *"
Delete all installed breakpoints.

- disable [file]:[line]
Xref "breakpoint debugger command, disable disable"
Item "disable [file]:[line]"
Disable the breakpoint so it won't stop the execution of the program.
Breakpoints are enabled by default and can be re-enabled using the \f(CW\*(C`enable\*(C'
command.

- disable [line]
Xref "breakpoint debugger command, disable disable"
Item "disable [line]"
Disable the breakpoint so it won't stop the execution of the program.
Breakpoints are enabled by default and can be re-enabled using the \f(CW\*(C`enable\*(C'
command.
.Sp
This is done for a breakpoint in the current file.

- enable [file]:[line]
Xref "breakpoint debugger command, disable disable"
Item "enable [file]:[line]"
Enable the breakpoint so it will stop the execution of the program.

- enable [line]
Xref "breakpoint debugger command, disable disable"
Item "enable [line]"
Enable the breakpoint so it will stop the execution of the program.
.Sp
This is done for a breakpoint in the current file.

- a [line] command
Xref "debugger command, a"
Item "a [line] command"
Set an action to be done before the line is executed.  If *line* is
omitted, set an action on the line about to be executed.
The sequence of steps taken by the debugger is
.Sp
.Vb 5
  1. check for a breakpoint at this line
  2. print the line if necessary (tracing)
  3. do any actions associated with that line
  4. prompt user if at a breakpoint or in single-step
  5. evaluate line
.Ve
.Sp
For example, this will print out \f(CW$foo every time line
53 is passed:
.Sp
.Vb 1
    a 53 print "DB FOUND $foo\\n"
.Ve

- A line
Xref "debugger command, A"
Item "A line"
Delete an action from the specified line.

- A *
Xref "debugger command, A"
Item "A *"
Delete all installed actions.

- w expr
Xref "debugger command, w"
Item "w expr"
Add a global watch-expression. Whenever a watched global changes the
debugger will stop and display the old and new values.

- W expr
Xref "debugger command, W"
Item "W expr"
Delete watch-expression

- W *
Xref "debugger command, W"
Item "W *"
Delete all watch-expressions.

- o
Xref "debugger command, o"
Item "o"
Display all options.

- o booloption ...
Xref "debugger command, o"
Item "o booloption ..."
Set each listed Boolean option to the value \f(CW1.

- o anyoption? ...
Xref "debugger command, o"
Item "o anyoption? ..."
Print out the value of one or more options.

- o option=value ...
Xref "debugger command, o"
Item "o option=value ..."
Set the value of one or more options.  If the value has internal
whitespace, it should be quoted.  For example, you could set \f(CW\*(C`o
pager="less -MQeicsNfr"\*(C' to call **less** with those specific options.
You may use either single or double quotes, but if you do, you must
escape any embedded instances of same sort of quote you began with,
as well as any escaping any escapes that immediately precede that
quote but which are not meant to escape the quote itself.  In other
words, you follow single-quoting rules irrespective of the quote;
eg: \f(CW\*(C`o option=\*(Aqthis isn\\\*(Aqt bad\*(Aq\*(C' or \f(CW\*(C`o option="She said, \\"Isn\*(Aqt
it?\\""\*(C'.
.Sp
For historical reasons, the \f(CW\*(C`=value\*(C' is optional, but defaults to
1 only where it is safe to do so\*(--that is, mostly for Boolean
options.  It is always better to assign a specific value using \f(CW\*(C`=\*(C'.
The \f(CW\*(C`option\*(C' can be abbreviated, but for clarity probably should
not be.  Several options can be set together.  See \*(L"Configurable Options\*(R"
for a list of these.

- < ?
Xref "debugger command, <"
List out all pre-prompt Perl command actions.

- < [ command ]
Xref "debugger command, <"
Item "< [ command ]"
Set an action (Perl command) to happen before every debugger prompt.
A multi-line command may be entered by backslashing the newlines.

- < *
Xref "debugger command, <"
Delete all pre-prompt Perl command actions.

- << command
Xref "debugger command, <<"
Item "<< command"
Add an action (Perl command) to happen before every debugger prompt.
A multi-line command may be entered by backwhacking the newlines.

- > ?
Xref "debugger command, >"
List out post-prompt Perl command actions.

- > command
Xref "debugger command, >"
Item "> command"
Set an action (Perl command) to happen after the prompt when you've
just given a command to return to executing the script.  A multi-line
command may be entered by backslashing the newlines (we bet you
couldn't have guessed this by now).

- > *
Xref "debugger command, >"
Delete all post-prompt Perl command actions.

- >> command
Xref "debugger command, >>"
Item ">> command"
Adds an action (Perl command) to happen after the prompt when you've
just given a command to return to executing the script.  A multi-line
command may be entered by backslashing the newlines.

- \{ ?
Xref "debugger command, \{"
List out pre-prompt debugger commands.

- \{ [ command ]
Item "\{ [ command ]"
Set an action (debugger command) to happen before every debugger prompt.
A multi-line command may be entered in the customary fashion.
.Sp
Because this command is in some senses new, a warning is issued if
you appear to have accidentally entered a block instead.  If that's
what you mean to do, write it as with \f(CW\*(C`;\{ ... \}\*(C' or even
\f(CW\*(C`do \{ ... \}\*(C'.

- \{ *
Xref "debugger command, \{"
Delete all pre-prompt debugger commands.

- \{\{ command
Xref "debugger command, \{\{"
Item "\{\{ command"
Add an action (debugger command) to happen before every debugger prompt.
A multi-line command may be entered, if you can guess how: see above.

- ! number
Xref "debugger command, !"
Item "! number"
Redo a previous command (defaults to the previous command).

- ! -number
Xref "debugger command, !"
Item "! -number"
Redo number'th previous command.

- ! pattern
Xref "debugger command, !"
Item "! pattern"
Redo last command that started with pattern.
See \f(CW\*(C`o recallCommand\*(C', too.

- !! cmd
Xref "debugger command, !!"
Item "!! cmd"
Run cmd in a subprocess (reads from \s-1DB::IN,\s0 writes to \s-1DB::OUT\s0) See
\f(CW\*(C`o shellBang\*(C', also.  Note that the user's current shell (well,
their \f(CW$ENV\{SHELL\} variable) will be used, which can interfere
with proper interpretation of exit status or signal and coredump
information.

- source file
Xref "debugger command, source"
Item "source file"
Read and execute debugger commands from *file*.
*file* may itself contain \f(CW\*(C`source\*(C' commands.

- H -number
Xref "debugger command, H"
Item "H -number"
Display last n commands.  Only commands longer than one character are
listed.  If *number* is omitted, list them all.

- q or ^D
Xref "debugger command, q debugger command, ^D"
Item "q or ^D"
Quit.  (\*(L"quit\*(R" doesn't work for this, unless you've made an alias)
This is the only supported way to exit the debugger, though typing
\f(CW\*(C`exit\*(C' twice might work.
.Sp
Set the \f(CW\*(C`inhibit_exit\*(C' option to 0 if you want to be able to step
off the end the script.  You may also need to set \f(CW$finished to 0
if you want to step through global destruction.

- R
Xref "debugger command, R"
Item "R"
Restart the debugger by \f(CW\*(C`exec()\*(C'ing a new session.  We try to maintain
your history across this, but internal settings and command-line options
may be lost.
.Sp
The following setting are currently preserved: history, breakpoints,
actions, debugger options, and the Perl command-line
options **-w**, **-I**, and **-e**.

- |dbcmd
Xref "debugger command, |"
Item "|dbcmd"
Run the debugger command, piping \s-1DB::OUT\s0 into your current pager.

- ||dbcmd
Xref "debugger command, ||"
Item "||dbcmd"
Same as \f(CW\*(C`|dbcmd\*(C' but \s-1DB::OUT\s0 is temporarily \f(CW\*(C`select\*(C'ed as well.

- = [alias value]
Xref "debugger command, ="
Item "= [alias value]"
Define a command alias, like
.Sp
.Vb 1
    = quit q
.Ve
.Sp
or list current aliases.

- command
Item "command"
Execute command as a Perl statement.  A trailing semicolon will be
supplied.  If the Perl statement would otherwise be confused for a
Perl debugger, use a leading semicolon, too.

- m expr
Xref "debugger command, m"
Item "m expr"
List which methods may be called on the result of the evaluated
expression.  The expression may evaluated to a reference to a
blessed object, or to a package name.

- M
Xref "debugger command, M"
Item "M"
Display all loaded modules and their versions.

- man [manpage]
Xref "debugger command, man"
Item "man [manpage]"
Despite its name, this calls your system's default documentation
viewer on the given page, or on the viewer itself if *manpage* is
omitted.  If that viewer is **man**, the current \f(CW\*(C`Config\*(C' information
is used to invoke **man** using the proper \s-1MANPATH\s0 or **-M**\ *manpath* option.  Failed lookups of the form \f(CW\*(C`XXX\*(C' that match
known manpages of the form *perlXXX* will be retried.  This lets
you type \f(CW\*(C`man debug\*(C' or \f(CW\*(C`man op\*(C' from the debugger.
.Sp
On systems traditionally bereft of a usable **man** command, the
debugger invokes **perldoc**.  Occasionally this determination is
incorrect due to recalcitrant vendors or rather more felicitously,
to enterprising users.  If you fall into either category, just
manually set the \f(CW$DB::doccmd variable to whatever viewer to view
the Perl documentation on your system.  This may be set in an rc
file, or through direct assignment.  We're still waiting for a
working example of something along the lines of:
.Sp
.Vb 1
    $DB::doccmd = \*(Aqnetscape -remote http://something.here/\*(Aq;
.Ve

### Configurable Options

Subsection "Configurable Options"
The debugger has numerous options settable using the \f(CW\*(C`o\*(C' command,
either interactively or from the environment or an rc file. The file
is named *./.perldb* or *~/.perldb* under Unix with */dev/tty*,
*perldb.ini* otherwise.
.ie n .IP """recallCommand"", ""ShellBang""" 12
.el .IP "\f(CWrecallCommand, \f(CWShellBang" 12
Xref "debugger option, recallCommand debugger option, ShellBang"
Item "recallCommand, ShellBang"
The characters used to recall a command or spawn a shell.  By
default, both are set to \f(CW\*(C`!\*(C', which is unfortunate.
.ie n .IP """pager""" 12
.el .IP "\f(CWpager" 12
Xref "debugger option, pager"
Item "pager"
Program to use for output of pager-piped commands (those beginning
with a \f(CW\*(C`|\*(C' character.)  By default, \f(CW$ENV\{PAGER\} will be used.
Because the debugger uses your current terminal characteristics
for bold and underlining, if the chosen pager does not pass escape
sequences through unchanged, the output of some debugger commands
will not be readable when sent through the pager.
.ie n .IP """tkRunning""" 12
.el .IP "\f(CWtkRunning" 12
Xref "debugger option, tkRunning"
Item "tkRunning"
Run Tk while prompting (with ReadLine).
.ie n .IP """signalLevel"", ""warnLevel"", ""dieLevel""" 12
.el .IP "\f(CWsignalLevel, \f(CWwarnLevel, \f(CWdieLevel" 12
Xref "debugger option, signalLevel debugger option, warnLevel debugger option, dieLevel"
Item "signalLevel, warnLevel, dieLevel"
Level of verbosity.  By default, the debugger leaves your exceptions
and warnings alone, because altering them can break correctly running
programs.  It will attempt to print a message when uncaught \s-1INT, BUS,\s0 or
\s-1SEGV\s0 signals arrive.  (But see the mention of signals in \*(L"\s-1BUGS\*(R"\s0 below.)
.Sp
To disable this default safe mode, set these values to something higher
than 0.  At a level of 1, you get backtraces upon receiving any kind
of warning (this is often annoying) or exception (this is
often valuable).  Unfortunately, the debugger cannot discern fatal
exceptions from non-fatal ones.  If \f(CW\*(C`dieLevel\*(C' is even 1, then your
non-fatal exceptions are also traced and unceremoniously altered if they
came from \f(CW\*(C`eval\*(Aqed\*(C' strings or from any kind of \f(CW\*(C`eval\*(C' within modules
you're attempting to load.  If \f(CW\*(C`dieLevel\*(C' is 2, the debugger doesn't
care where they came from:  It usurps your exception handler and prints
out a trace, then modifies all exceptions with its own embellishments.
This may perhaps be useful for some tracing purposes, but tends to hopelessly
destroy any program that takes its exception handling seriously.
.ie n .IP """AutoTrace""" 12
.el .IP "\f(CWAutoTrace" 12
Xref "debugger option, AutoTrace"
Item "AutoTrace"
Trace mode (similar to \f(CW\*(C`t\*(C' command, but can be put into
\f(CW\*(C`PERLDB_OPTS\*(C').
.ie n .IP """LineInfo""" 12
.el .IP "\f(CWLineInfo" 12
Xref "debugger option, LineInfo"
Item "LineInfo"
File or pipe to print line number info to.  If it is a pipe (say,
\f(CW\*(C`|visual_perl_db\*(C'), then a short message is used.  This is the
mechanism used to interact with a slave editor or visual debugger,
such as the special \f(CW\*(C`vi\*(C' or \f(CW\*(C`emacs\*(C' hooks, or the \f(CW\*(C`ddd\*(C' graphical
debugger.
.ie n .IP """inhibit_exit""" 12
.el .IP "\f(CWinhibit_exit" 12
Xref "debugger option, inhibit_exit"
Item "inhibit_exit"
If 0, allows *stepping off* the end of the script.
.ie n .IP """PrintRet""" 12
.el .IP "\f(CWPrintRet" 12
Xref "debugger option, PrintRet"
Item "PrintRet"
Print return value after \f(CW\*(C`r\*(C' command if set (default).
.ie n .IP """ornaments""" 12
.el .IP "\f(CWornaments" 12
Xref "debugger option, ornaments"
Item "ornaments"
Affects screen appearance of the command line (see Term::ReadLine).
There is currently no way to disable these, which can render
some output illegible on some displays, or with some pagers.
This is considered a bug.
.ie n .IP """frame""" 12
.el .IP "\f(CWframe" 12
Xref "debugger option, frame"
Item "frame"
Affects the printing of messages upon entry and exit from subroutines.  If
\f(CW\*(C`frame & 2\*(C' is false, messages are printed on entry only. (Printing
on exit might be useful if interspersed with other messages.)
.Sp
If \f(CW\*(C`frame & 4\*(C', arguments to functions are printed, plus context
and caller info.  If \f(CW\*(C`frame & 8\*(C', overloaded \f(CW\*(C`stringify\*(C' and
\f(CW\*(C`tie\*(C'd \f(CW\*(C`FETCH\*(C' is enabled on the printed arguments.  If \f(CW\*(C`frame
& 16\*(C', the return value from the subroutine is printed.
.Sp
The length at which the argument list is truncated is governed by the
next option:
.ie n .IP """maxTraceLen""" 12
.el .IP "\f(CWmaxTraceLen" 12
Xref "debugger option, maxTraceLen"
Item "maxTraceLen"
Length to truncate the argument list when the \f(CW\*(C`frame\*(C' option's
bit 4 is set.
.ie n .IP """windowSize""" 12
.el .IP "\f(CWwindowSize" 12
Xref "debugger option, windowSize"
Item "windowSize"
Change the size of code list window (default is 10 lines).

The following options affect what happens with \f(CW\*(C`V\*(C', \f(CW\*(C`X\*(C', and \f(CW\*(C`x\*(C'
commands:
.ie n .IP """arrayDepth"", ""hashDepth""" 12
.el .IP "\f(CWarrayDepth, \f(CWhashDepth" 12
Xref "debugger option, arrayDepth debugger option, hashDepth"
Item "arrayDepth, hashDepth"
Print only first N elements ('' for all).
.ie n .IP """dumpDepth""" 12
.el .IP "\f(CWdumpDepth" 12
Xref "debugger option, dumpDepth"
Item "dumpDepth"
Limit recursion depth to N levels when dumping structures.
Negative values are interpreted as infinity.  Default: infinity.
.ie n .IP """compactDump"", ""veryCompact""" 12
.el .IP "\f(CWcompactDump, \f(CWveryCompact" 12
Xref "debugger option, compactDump debugger option, veryCompact"
Item "compactDump, veryCompact"
Change the style of array and hash output.  If \f(CW\*(C`compactDump\*(C', short array
may be printed on one line.
.ie n .IP """globPrint""" 12
.el .IP "\f(CWglobPrint" 12
Xref "debugger option, globPrint"
Item "globPrint"
Whether to print contents of globs.
.ie n .IP """DumpDBFiles""" 12
.el .IP "\f(CWDumpDBFiles" 12
Xref "debugger option, DumpDBFiles"
Item "DumpDBFiles"
Dump arrays holding debugged files.
.ie n .IP """DumpPackages""" 12
.el .IP "\f(CWDumpPackages" 12
Xref "debugger option, DumpPackages"
Item "DumpPackages"
Dump symbol tables of packages.
.ie n .IP """DumpReused""" 12
.el .IP "\f(CWDumpReused" 12
Xref "debugger option, DumpReused"
Item "DumpReused"
Dump contents of \*(L"reused\*(R" addresses.
.ie n .IP """quote"", ""HighBit"", ""undefPrint""" 12
.el .IP "\f(CWquote, \f(CWHighBit, \f(CWundefPrint" 12
Xref "debugger option, quote debugger option, HighBit debugger option, undefPrint"
Item "quote, HighBit, undefPrint"
Change the style of string dump.  The default value for \f(CW\*(C`quote\*(C'
is \f(CW\*(C`auto\*(C'; one can enable double-quotish or single-quotish format
by setting it to \f(CW\*(C`"\*(C' or \f(CW\*(C`\*(Aq\*(C', respectively.  By default, characters
with their high bit set are printed verbatim.
.ie n .IP """UsageOnly""" 12
.el .IP "\f(CWUsageOnly" 12
Xref "debugger option, UsageOnly"
Item "UsageOnly"
Rudimentary per-package memory usage dump.  Calculates total
size of strings found in variables in the package.  This does not
include lexicals in a module's file scope, or lost in closures.
.ie n .IP """HistFile""" 12
.el .IP "\f(CWHistFile" 12
Xref "debugger option, history, HistFile"
Item "HistFile"
The path of the file from which the history (assuming a usable
Term::ReadLine backend) will be read on the debugger's startup, and to which
it will be saved on shutdown (for persistence across sessions). Similar in
concept to Bash's \f(CW\*(C`.bash_history\*(C' file.
.ie n .IP """HistSize""" 12
.el .IP "\f(CWHistSize" 12
Xref "debugger option, history, HistSize"
Item "HistSize"
The count of the saved lines in the history (assuming \f(CW\*(C`HistFile\*(C' above).

After the rc file is read, the debugger reads the \f(CW$ENV\{PERLDB_OPTS\}
environment variable and parses this as the remainder of a \*(L"O ...\*(R"
line as one might enter at the debugger prompt.  You may place the
initialization options \f(CW\*(C`TTY\*(C', \f(CW\*(C`noTTY\*(C', \f(CW\*(C`ReadLine\*(C', and \f(CW\*(C`NonStop\*(C'
there.

If your rc file contains:

.Vb 1
  parse_options("NonStop=1 LineInfo=db.out AutoTrace");
.Ve

then your script will run without human intervention, putting trace
information into the file *db.out*.  (If you interrupt it, you'd
better reset \f(CW\*(C`LineInfo\*(C' to */dev/tty* if you expect to see anything.)
.ie n .IP """TTY""" 12
.el .IP "\f(CWTTY" 12
Xref "debugger option, TTY"
Item "TTY"
The \s-1TTY\s0 to use for debugging I/O.
.ie n .IP """noTTY""" 12
.el .IP "\f(CWnoTTY" 12
Xref "debugger option, noTTY"
Item "noTTY"
If set, the debugger goes into \f(CW\*(C`NonStop\*(C' mode and will not connect to a \s-1TTY.\s0  If
interrupted (or if control goes to the debugger via explicit setting of
\f(CW$DB::signal or \f(CW$DB::single from the Perl script), it connects to a \s-1TTY\s0
specified in the \f(CW\*(C`TTY\*(C' option at startup, or to a tty found at
runtime using the \f(CW\*(C`Term::Rendezvous\*(C' module of your choice.
.Sp
This module should implement a method named \f(CW\*(C`new\*(C' that returns an object
with two methods: \f(CW\*(C`IN\*(C' and \f(CW\*(C`OUT\*(C'.  These should return filehandles to use
for debugging input and output correspondingly.  The \f(CW\*(C`new\*(C' method should
inspect an argument containing the value of \f(CW$ENV\{PERLDB_NOTTY\} at
startup, or \f(CW"$ENV\{HOME\}/.perldbtty$$" otherwise.  This file is not
inspected for proper ownership, so security hazards are theoretically
possible.
.ie n .IP """ReadLine""" 12
.el .IP "\f(CWReadLine" 12
Xref "debugger option, ReadLine"
Item "ReadLine"
If false, readline support in the debugger is disabled in order
to debug applications that themselves use ReadLine.
.ie n .IP """NonStop""" 12
.el .IP "\f(CWNonStop" 12
Xref "debugger option, NonStop"
Item "NonStop"
If set, the debugger goes into non-interactive mode until interrupted, or
programmatically by setting \f(CW$DB::signal or \f(CW$DB::single.

Here's an example of using the \f(CW$ENV\{PERLDB_OPTS\} variable:

.Vb 1
    $ PERLDB_OPTS="NonStop frame=2" perl -d myprogram
.Ve

That will run the script **myprogram** without human intervention,
printing out the call tree with entry and exit points.  Note that
\f(CW\*(C`NonStop=1 frame=2\*(C' is equivalent to \f(CW\*(C`N f=2\*(C', and that originally,
options could be uniquely abbreviated by the first letter (modulo
the \f(CW\*(C`Dump*\*(C' options).  It is nevertheless recommended that you
always spell them out in full for legibility and future compatibility.

Other examples include

.Vb 1
    $ PERLDB_OPTS="NonStop LineInfo=listing frame=2" perl -d myprogram
.Ve

which runs script non-interactively, printing info on each entry
into a subroutine and each executed line into the file named *listing*.
(If you interrupt it, you would better reset \f(CW\*(C`LineInfo\*(C' to something
\*(L"interactive\*(R"!)

Other examples include (using standard shell syntax to show environment
variable settings):

.Vb 2
  $ ( PERLDB_OPTS="NonStop frame=1 AutoTrace LineInfo=tperl.out"
      perl -d myprogram )
.Ve

which may be useful for debugging a program that uses \f(CW\*(C`Term::ReadLine\*(C'
itself.  Do not forget to detach your shell from the \s-1TTY\s0 in the window that
corresponds to */dev/ttyXX*, say, by issuing a command like

.Vb 1
  $ sleep 1000000
.Ve

See \*(L"Debugger Internals\*(R" in perldebguts for details.

### Debugger Input/Output

Subsection "Debugger Input/Output"

- Prompt
Item "Prompt"
The debugger prompt is something like
.Sp
.Vb 1
    DB<8>
.Ve
.Sp
or even
.Sp
.Vb 1
    DB<<17>>
.Ve
.Sp
where that number is the command number, and which you'd use to
access with the built-in **csh**-like history mechanism.  For example,
\f(CW\*(C`!17\*(C' would repeat command number 17.  The depth of the angle
brackets indicates the nesting depth of the debugger.  You could
get more than one set of brackets, for example, if you'd already
at a breakpoint and then printed the result of a function call that
itself has a breakpoint, or you step into an expression via \f(CW\*(C`s/n/t
expression\*(C' command.

- Multiline commands
Item "Multiline commands"
If you want to enter a multi-line command, such as a subroutine
definition with several statements or a format, escape the newline
that would normally end the debugger command with a backslash.
Here's an example:
.Sp
.Vb 7
      DB<1> for (1..4) \{         \\
      cont:     print "ok\\n";   \\
      cont: \}
      ok
      ok
      ok
      ok
.Ve
.Sp
Note that this business of escaping a newline is specific to interactive
commands typed into the debugger.

- Stack backtrace
Xref "backtrace stack, backtrace"
Item "Stack backtrace"
Here's an example of what a stack backtrace via \f(CW\*(C`T\*(C' command might
look like:
.Sp
.Vb 5
 $ = main::infested called from file \*(AqAmbulation.pm\*(Aq line 10
 @ = Ambulation::legs(1, 2, 3, 4) called from file \*(Aqcamel_flea\*(Aq
                                                          line 7
 $ = main::pests(\*(Aqbactrian\*(Aq, 4) called from file \*(Aqcamel_flea\*(Aq
                                                          line 4
.Ve
.Sp
The left-hand character up there indicates the context in which the
function was called, with \f(CW\*(C`$\*(C' and \f(CW\*(C`@\*(C' meaning scalar or list
contexts respectively, and \f(CW\*(C`.\*(C' meaning void context (which is
actually a sort of scalar context).  The display above says
that you were in the function \f(CW\*(C`main::infested\*(C' when you ran the
stack dump, and that it was called in scalar context from line
10 of the file *Ambulation.pm*, but without any arguments at all,
meaning it was called as \f(CW&infested.  The next stack frame shows
that the function \f(CW\*(C`Ambulation::legs\*(C' was called in list context
from the *camel_flea* file with four arguments.  The last stack
frame shows that \f(CW\*(C`main::pests\*(C' was called in scalar context,
also from *camel_flea*, but from line 4.
.Sp
If you execute the \f(CW\*(C`T\*(C' command from inside an active \f(CW\*(C`use\*(C'
statement, the backtrace will contain both a \f(CW\*(C`require\*(C' frame and
an \f(CW\*(C`eval\*(C' frame.

- Line Listing Format
Item "Line Listing Format"
This shows the sorts of output the \f(CW\*(C`l\*(C' command can produce:
.Sp
.Vb 11
   DB<<13>> l
 101:        @i\{@i\} = ();
 102:b       @isa\{@i,$pack\} = ()
 103             if(exists $i\{$prevpack\} || exists $isa\{$pack\});
 104     \}
 105
 106     next
 107==>      if(exists $isa\{$pack\});
 108
 109:a   if ($extra-- > 0) \{
 110:        %isa = ($pack,1);
.Ve
.Sp
Breakable lines are marked with \f(CW\*(C`:\*(C'.  Lines with breakpoints are
marked by \f(CW\*(C`b\*(C' and those with actions by \f(CW\*(C`a\*(C'.  The line that's
about to be executed is marked by \f(CW\*(C`==>\*(C'.
.Sp
Please be aware that code in debugger listings may not look the same
as your original source code.  Line directives and external source
filters can alter the code before Perl sees it, causing code to move
from its original positions or take on entirely different forms.

- Frame listing
Item "Frame listing"
When the \f(CW\*(C`frame\*(C' option is set, the debugger would print entered (and
optionally exited) subroutines in different styles.  See perldebguts
for incredibly long examples of these.

### Debugging Compile-Time Statements

Subsection "Debugging Compile-Time Statements"
If you have compile-time executable statements (such as code within
\s-1BEGIN, UNITCHECK\s0 and \s-1CHECK\s0 blocks or \f(CW\*(C`use\*(C' statements), these will
*not* be stopped by debugger, although \f(CW\*(C`require\*(C's and \s-1INIT\s0 blocks
will, and compile-time statements can be traced with the \f(CW\*(C`AutoTrace\*(C'
option set in \f(CW\*(C`PERLDB_OPTS\*(C').  From your own Perl code, however, you
can transfer control back to the debugger using the following
statement, which is harmless if the debugger is not running:

.Vb 1
    $DB::single = 1;
.Ve

If you set \f(CW$DB::single to 2, it's equivalent to having
just typed the \f(CW\*(C`n\*(C' command, whereas a value of 1 means the \f(CW\*(C`s\*(C'
command.  The \f(CW$DB::trace  variable should be set to 1 to simulate
having typed the \f(CW\*(C`t\*(C' command.

Another way to debug compile-time code is to start the debugger, set a
breakpoint on the *load* of some module:

.Vb 2
    DB<7> b load f:/perllib/lib/Carp.pm
  Will stop on load of \*(Aqf:/perllib/lib/Carp.pm\*(Aq.
.Ve

and then restart the debugger using the \f(CW\*(C`R\*(C' command (if possible).  One can use \f(CW\*(C`b
compile subname\*(C' for the same purpose.

### Debugger Customization

Subsection "Debugger Customization"
The debugger probably contains enough configuration hooks that you
won't ever have to modify it yourself.  You may change the behaviour
of the debugger from within the debugger using its \f(CW\*(C`o\*(C' command, from
the command line via the \f(CW\*(C`PERLDB_OPTS\*(C' environment variable, and
from customization files.

You can do some customization by setting up a *.perldb* file, which
contains initialization code.  For instance, you could make aliases
like these (the last one is one people expect to be there):

.Vb 4
    $DB::alias\{\*(Aqlen\*(Aq\}  = \*(Aqs/^len(.*)/p length($1)/\*(Aq;
    $DB::alias\{\*(Aqstop\*(Aq\} = \*(Aqs/^stop (at|in)/b/\*(Aq;
    $DB::alias\{\*(Aqps\*(Aq\}   = \*(Aqs/^ps\\b/p scalar /\*(Aq;
    $DB::alias\{\*(Aqquit\*(Aq\} = \*(Aqs/^quit(\\s*)/exit/\*(Aq;
.Ve

You can change options from *.perldb* by using calls like this one;

.Vb 1
    parse_options("NonStop=1 LineInfo=db.out AutoTrace=1 frame=2");
.Ve

The code is executed in the package \f(CW\*(C`DB\*(C'.  Note that *.perldb* is
processed before processing \f(CW\*(C`PERLDB_OPTS\*(C'.  If *.perldb* defines the
subroutine \f(CW\*(C`afterinit\*(C', that function is called after debugger
initialization ends.  *.perldb* may be contained in the current
directory, or in the home directory.  Because this file is sourced
in by Perl and may contain arbitrary commands, for security reasons,
it must be owned by the superuser or the current user, and writable
by no one but its owner.

You can mock \s-1TTY\s0 input to debugger by adding arbitrary commands to
\f(CW@DB::typeahead. For example, your *.perldb* file might contain:

.Vb 1
    sub afterinit \{ push @DB::typeahead, "b 4", "b 6"; \}
.Ve

Which would attempt to set breakpoints on lines 4 and 6 immediately
after debugger initialization. Note that \f(CW@DB::typeahead is not a supported
interface and is subject to change in future releases.

If you want to modify the debugger, copy *perl5db.pl* from the
Perl library to another name and hack it to your heart's content.
You'll then want to set your \f(CW\*(C`PERL5DB\*(C' environment variable to say
something like this:

.Vb 1
    BEGIN \{ require "myperl5db.pl" \}
.Ve

As a last resort, you could also use \f(CW\*(C`PERL5DB\*(C' to customize the debugger
by directly setting internal variables or calling debugger functions.

Note that any variables and functions that are not documented in
this document (or in perldebguts) are considered for internal
use only, and as such are subject to change without notice.

### Readline Support / History in the Debugger

Subsection "Readline Support / History in the Debugger"
As shipped, the only command-line history supplied is a simplistic one
that checks for leading exclamation points.  However, if you install
the Term::ReadKey and Term::ReadLine modules from \s-1CPAN\s0 (such as
Term::ReadLine::Gnu, Term::ReadLine::Perl, ...) you will
have full editing capabilities much like those \s-1GNU\s0 *readline*(3) provides.
Look for these in the *modules/by-module/Term* directory on \s-1CPAN.\s0
These do not support normal **vi** command-line editing, however.

A rudimentary command-line completion is also available, including
lexical variables in the current scope if the \f(CW\*(C`PadWalker\*(C' module
is installed.

Without Readline support you may see the symbols \*(L"^[[A\*(R", \*(L"^[[C\*(R", \*(L"^[[B\*(R",
\*(L"^[[D\*(R"\*(L", \*(R"^H", ... when using the arrow keys and/or the backspace key.

### Editor Support for Debugging

Subsection "Editor Support for Debugging"
If you have the \s-1GNU\s0's version of **emacs** installed on your system,
it can interact with the Perl debugger to provide an integrated
software development environment reminiscent of its interactions
with C debuggers.

Recent versions of Emacs come with a
start file for making **emacs** act like a
syntax-directed editor that understands (some of) Perl's syntax.
See perlfaq3.

Users of **vi** should also look into **vim** and **gvim**, the mousey
and windy version, for coloring of Perl keywords.

Note that only perl can truly parse Perl, so all such \s-1CASE\s0 tools
fall somewhat short of the mark, especially if you don't program
your Perl as a C programmer might.

### The Perl Profiler

Xref "profile profiling profiler"
Subsection "The Perl Profiler"
If you wish to supply an alternative debugger for Perl to run,
invoke your script with a colon and a package argument given to the
**-d** flag.  Perl's alternative debuggers include a Perl profiler,
Devel::NYTProf, which is available separately as a \s-1CPAN\s0
distribution.  To profile your Perl program in the file *mycode.pl*,
just type:

.Vb 1
    $ perl -d:NYTProf mycode.pl
.Ve

When the script terminates the profiler will create a database of the
profile information that you can turn into reports using the profiler's
tools. See <perlperf> for details.

## Debugging Regular Expressions

Xref "regular expression, debugging regex, debugging regexp, debugging"
Header "Debugging Regular Expressions"
\f(CW\*(C`use re \*(Aqdebug\*(Aq\*(C' enables you to see the gory details of how the Perl
regular expression engine works. In order to understand this typically
voluminous output, one must not only have some idea about how regular
expression matching works in general, but also know how Perl's regular
expressions are internally compiled into an automaton. These matters
are explored in some detail in
\*(L"Debugging Regular Expressions\*(R" in perldebguts.

## Debugging Memory Usage

Xref "memory usage"
Header "Debugging Memory Usage"
Perl contains internal support for reporting its own memory usage,
but this is a fairly advanced concept that requires some understanding
of how memory allocation works.
See \*(L"Debugging Perl Memory Usage\*(R" in perldebguts for the details.

## SEE ALSO

Header "SEE ALSO"
You do have \f(CW\*(C`use strict\*(C' and \f(CW\*(C`use warnings\*(C' enabled, don't you?

perldebtut,
perldebguts,
perl5db.pl,
re,
\s-1DB\s0,
Devel::NYTProf,
Dumpvalue,
and
perlrun.

When debugging a script that uses #! and is thus normally found in
\f(CW$PATH, the -S option causes perl to search \f(CW$PATH for it, so you don't
have to type the path or \f(CW\*(C`which $scriptname\*(C'.

.Vb 1
  $ perl -Sd foo.pl
.Ve

## BUGS

Header "BUGS"
You cannot get stack frame information or in any fashion debug functions
that were not compiled by Perl, such as those from C or \*(C+ extensions.

If you alter your \f(CW@_ arguments in a subroutine (such as with \f(CW\*(C`shift\*(C'
or \f(CW\*(C`pop\*(C'), the stack backtrace will not show the original values.

The debugger does not currently work in conjunction with the **-W**
command-line switch, because it itself is not free of warnings.

If you're in a slow syscall (like \f(CW\*(C`wait\*(C'ing, \f(CW\*(C`accept\*(C'ing, or \f(CW\*(C`read\*(C'ing
from your keyboard or a socket) and haven't set up your own \f(CW$SIG\{INT\}
handler, then you won't be able to CTRL-C your way back to the debugger,
because the debugger's own \f(CW$SIG\{INT\} handler doesn't understand that
it needs to raise an exception to **longjmp**\|(3) out of slow syscalls.
