+++
detected_package_version = "5.34.1"
operating_system = "macos"
manpage_section = "1"
manpage_name = "perl5340delta"
author = "None Specified"
operating_system_version = "15.3"
manpage_format = "troff"
date = "2022-02-26"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
description = "This document describes differences between the 5.32.0 release and the 5.34.0 release. If you are upgrading from an earlier release such as 5.30.0, first read perl5320delta, which describes differences between 5.30.0 and 5.32.0. An initial experim..."
title = "perl5340delta(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5340DELTA 1"
PERL5340DELTA 1 "2022-02-26" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5340delta - what is new for perl v5.34.0

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.32.0 release and the 5.34.0
release.

If you are upgrading from an earlier release such as 5.30.0, first read
perl5320delta, which describes differences between 5.30.0 and 5.32.0.

## Core Enhancements

Header "Core Enhancements"

### Experimental Try/Catch Syntax

Subsection "Experimental Try/Catch Syntax"
An initial experimental attempt at providing \f(CW\*(C`try\*(C'/\f(CW\*(C`catch\*(C' notation has
been added.

.Vb 1
    use feature \*(Aqtry\*(Aq;

    try \{
        a_function();
    \}
    catch ($e) \{
        warn "An error occurred: $e";
    \}
.Ve

For more information, see \*(L"Try Catch Exception Handling\*(R" in perlsyn.
.ie n .SS """qr/\{,n\}/"" is now accepted"
.el .SS "\f(CWqr/\{,n\}/ is now accepted"
Subsection "qr/\{,n\}/ is now accepted"
An empty lower bound is now accepted for regular expression quantifiers,
like \f(CW\*(C`\{,3\}\*(C'.

### Blanks freely allowed within but adjacent to curly braces

Subsection "Blanks freely allowed within but adjacent to curly braces"
(in double-quotish contexts and regular expression patterns)

This means you can write things like \f(CW\*(C`\\x\{\ FFFC\ \}\*(C' if you like.  This
applies to all such constructs, namely \f(CW\*(C`\\b\{\}\*(C', \f(CW\*(C`\\g\{\}\*(C', \f(CW\*(C`\\k\{\}\*(C',
\f(CW\*(C`\\N\{\}\*(C', \f(CW\*(C`\\o\{\}\*(C', and \f(CW\*(C`\\x\{\}\*(C'; as well as the regular expression
quantifier \f(CW\*(C`\{\f(CIm\f(CW,\f(CIn\f(CW\}\*(C'.  \f(CW\*(C`\\p\{\}\*(C' and \f(CW\*(C`\\P\{\}\*(C' retain their
already-existing, even looser, rules mandated by the Unicode standard
(see \*(L"Properties accessible through \\p\{\} and \\P\{\}\*(R" in perluniprops).

This ability is in effect regardless of the presence of the \f(CW\*(C`/x\*(C'
regular expression pattern modifier.

Additionally, the comma in a regular expression braced quantifier may
have blanks (tabs or spaces) before and/or after the comma, like
\f(CW\*(C`qr/a\{\ 5,\ 7\ \}/\*(C'.
.ie n .SS "New octal syntax ""0o\f(CIddddd"""
.el .SS "New octal syntax \f(CW0o\f(CIddddd\f(CW"
Subsection "New octal syntax 0oddddd"
It is now possible to specify octal literals with \f(CW\*(C`0o\*(C' prefixes,
as in \f(CW\*(C`0o123_456\*(C', parallel to the existing construct to specify
hexadecimal literal \f(CW\*(C`0x\f(CIddddd\f(CW\*(C' and binary literal \f(CW\*(C`0b\f(CIddddd\f(CW\*(C'.
Also, the builtin \f(CW\*(C`oct()\*(C' function now accepts this new syntax.

See \*(L"Scalar value constructors\*(R" in perldata and \*(L"oct \s-1EXPR\*(R"\s0 in perlfunc.

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
Fix a memory leak in RegEx
[\s-1GH\s0 #18604 <https://github.com/Perl/perl5/issues/18604>]

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules and Pragmata

Subsection "New Modules and Pragmata"

- \(bu
ExtUtils::PL2Bat 0.004 has been added to the Perl core.
.Sp
This module is a generalization of the \f(CW\*(C`pl2bat\*(C' script. It being a script has
led to at least two forks of this code; this module will unify them under one
implementation with tests.

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Archive::Tar has been upgraded from version 2.36 to 2.38.

- \(bu
autodie has been upgraded from version 2.32 to 2.34.

- \(bu
B has been upgraded from version 1.80 to 1.82.

- \(bu
B::Deparse has been upgraded from version 1.54 to 1.56.

- \(bu
bytes has been upgraded from version 1.07 to 1.08.

- \(bu
Carp has been upgraded from version 1.50 to 1.52.

- \(bu
Compress::Raw::Bzip2 has been upgraded from version 2.093 to 2.101.

- \(bu
Compress::Raw::Zlib has been upgraded from version 2.093 to 2.101.

- \(bu
Config::Perl::V has been upgraded from version 0.32 to 0.33.

- \(bu
\s-1CPAN\s0 has been upgraded from version 2.27 to 2.28.

- \(bu
Data::Dumper has been upgraded from version 2.174 to 2.179.

- \(bu
\s-1DB\s0 has been upgraded from version 1.58 to 1.59.

- \(bu
DB_File has been upgraded from version 1.853 to 1.855.

- \(bu
Devel::Peek has been upgraded from version 1.28 to 1.30.

- \(bu
Devel::PPPort has been upgraded from version 3.57 to 3.62.
.Sp
New \f(CW\*(C`PERL_VERSION_*\*(C' comparison macros are now available.
.Sp
\f(CW\*(C`ppport.h --api-info\*(C' no longer includes non-API info unless that is the only
match

- \(bu
Digest has been upgraded from version 1.17_01 to 1.19.

- \(bu
Digest::MD5 has been upgraded from version 2.55_01 to 2.58.

- \(bu
DynaLoader has been upgraded from version 1.47 to 1.50.

- \(bu
Encode has been upgraded from version 3.06 to 3.08.

- \(bu
Env has been upgraded from version 1.04 to 1.05.

- \(bu
Errno has been upgraded from version 1.30 to 1.33.

- \(bu
experimental has been upgraded from version 0.020 to 0.024.

- \(bu
Exporter has been upgraded from version 5.74 to 5.76.

- \(bu
ExtUtils::CBuilder has been upgraded from version 0.280234 to 0.280236.

- \(bu
ExtUtils::Install has been upgraded from version 2.14 to 2.20.

- \(bu
ExtUtils::MakeMaker has been upgraded from version 7.44 to 7.62.

- \(bu
ExtUtils::Manifest has been upgraded from version 1.72 to 1.73.

- \(bu
ExtUtils::Miniperl has been upgraded from version 1.09 to 1.10.

- \(bu
ExtUtils::ParseXS has been upgraded from version 3.40 to 3.43.

- \(bu
ExtUtils::Typemaps has been upgraded from version 3.38 to 3.43.

- \(bu
Fcntl has been upgraded from version 1.13 to 1.14.

- \(bu
feature has been upgraded from version 1.58 to 1.64.
.Sp
Added the default enabled \f(CW\*(C`bareword_filehandles\*(C' feature.
.Sp
A new multidimensional
feature has been added, which is enabled by
default but allows turning off multi-dimensional array
emulation.

- \(bu
File::Copy has been upgraded from version 2.34 to 2.35.

- \(bu
File::Fetch has been upgraded from version 0.56 to 1.00.

- \(bu
File::Find has been upgraded from version 1.37 to 1.39.

- \(bu
File::Path has been upgraded from version 2.16 to 2.18.

- \(bu
File::Spec has been upgraded from version 3.78 to 3.80.

- \(bu
File::Temp has been upgraded from version 0.2309 to 0.2311.

- \(bu
Filter::Util::Call has been upgraded from version 1.59 to 1.60.

- \(bu
FindBin has been upgraded from version 1.51 to 1.52.

- \(bu
GDBM_File has been upgraded from version 1.18 to 1.19.
.Sp
New functions and compatibility for newer versions of \s-1GDBM.\s0
[\s-1GH\s0 #18435 <https://github.com/Perl/perl5/pull/18435>]

- \(bu
Getopt::Long has been upgraded from version 2.51 to 2.52.

- \(bu
Getopt::Std has been upgraded from version 1.12 to 1.13.

- \(bu
Hash::Util has been upgraded from version 0.23 to 0.25.

- \(bu
Hash::Util::FieldHash has been upgraded from version 1.20 to 1.21.

- \(bu
I18N::LangTags has been upgraded from version 0.44 to 0.45.

- \(bu
if has been upgraded from version 0.0608 to 0.0609.

- \(bu
\s-1IO\s0 has been upgraded from version 1.43 to 1.46.
.Sp
IO::Socket now stores error messages in \f(CW$IO::Socket::errstr, in
addition to in \f(CW$@.
.Sp
The \f(CW\*(C`error\*(C' method now reports the error state for both the input and
output streams for sockets and character devices.  Similarly
\f(CW\*(C`clearerr\*(C' now clears the error state for both streams.
.Sp
A spurious error reported for regular file handles has been
fixed in IO::Handle.
[\s-1GH\s0 #18019 <https://github.com/Perl/perl5/issues/18019>]

- \(bu
IO-Compress has been upgraded from version 2.093 to 2.102.
.Sp
bin/zipdetails version 2.02

- \(bu
IO::Socket::IP has been upgraded from version 0.39 to 0.41.

- \(bu
IO::Zlib has been upgraded from version 1.10 to 1.11.

- \(bu
IPC::SysV has been upgraded from version 2.07 to 2.09.

- \(bu
\s-1JSON::PP\s0 has been upgraded from version 4.04 to 4.06.

- \(bu
The libnet distribution has been upgraded from version 3.11 to 3.13.

- \(bu
locale has been upgraded from version 1.09 to 1.10.

- \(bu
Math::Complex has been upgraded from version 1.5901 to 1.5902.

- \(bu
MIME::Base64 has been upgraded from version 3.15 to 3.16.

- \(bu
Module::CoreList has been upgraded from version 5.20200620 to 5.20210520.

- \(bu
Module::Load has been upgraded from version 0.34 to 0.36.

- \(bu
Module::Load::Conditional has been upgraded from version 0.70 to 0.74.

- \(bu
mro has been upgraded from version 1.23 to 1.25_001.

- \(bu
Net::Ping has been upgraded from version 2.72 to 2.74.

- \(bu
\s-1NEXT\s0 has been upgraded from version 0.67_01 to 0.68.

- \(bu
ODBM_File has been upgraded from version 1.16 to 1.17.

- \(bu
Opcode has been upgraded from version 1.47 to 1.50.

- \(bu
overload has been upgraded from version 1.31 to 1.33.

- \(bu
perlfaq has been upgraded from version 5.20200523 to 5.20210411.

- \(bu
PerlIO::encoding has been upgraded from version 0.28 to 0.30.

- \(bu
PerlIO::mmap has been upgraded from version 0.016 to 0.017.

- \(bu
PerlIO::scalar has been upgraded from version 0.30 to 0.31.

- \(bu
PerlIO::via::QuotedPrint has been upgraded from version 0.08 to 0.09.

- \(bu
Pod::Checker has been upgraded from version 1.73 to 1.74.

- \(bu
Pod::Html has been upgraded from version 1.25 to 1.27.

- \(bu
Pod::Simple has been upgraded from version 3.40 to 3.42.

- \(bu
Pod::Usage has been upgraded from version 1.69 to 2.01.

- \(bu
\s-1POSIX\s0 has been upgraded from version 1.94 to 1.97.
.Sp
**POSIX::signbit()** behaviour has been improved.
[\s-1GH\s0 #18441 <https://github.com/Perl/perl5/pull/18441>]
.Sp
Documentation for \f(CW\*(C`asctime\*(C' clarifies that the result is always in English.
(Use \f(CW\*(C`strftime\*(C' for a localized result.)

- \(bu
re has been upgraded from version 0.40 to 0.41.
.Sp
(See under \*(L"Internal Changes\*(R" for more information.)

- \(bu
Safe has been upgraded from version 2.41 to 2.43.

- \(bu
Socket has been upgraded from version 2.029 to 2.031.

- \(bu
Storable has been upgraded from version 3.21 to 3.23.

- \(bu
strict has been upgraded from version 1.11 to 1.12.

- \(bu
subs has been upgraded from version 1.03 to 1.04.

- \(bu
Symbol has been upgraded from version 1.08 to 1.09.

- \(bu
Test::Harness has been upgraded from version 3.42 to 3.43.

- \(bu
Test::Simple has been upgraded from version 1.302175 to 1.302183.

- \(bu
Text::Balanced has been upgraded from version 2.03 to 2.04.

- \(bu
threads has been upgraded from version 2.25 to 2.26.

- \(bu
threads::shared has been upgraded from version 1.61 to 1.62.

- \(bu
Tie::RefHash has been upgraded from version 1.39 to 1.40.

- \(bu
Time::HiRes has been upgraded from version 1.9764 to 1.9767.

- \(bu
Time::Local has been upgraded from version 1.28 to 1.30.

- \(bu
Unicode::Collate has been upgraded from version 1.27 to 1.29.

- \(bu
Unicode::Normalize has been upgraded from version 1.27 to 1.28.

- \(bu
utf8 has been upgraded from version 1.22 to 1.24.

- \(bu
version has been upgraded from version 0.9924 to 0.9928.

- \(bu
warnings has been upgraded from version 1.47 to 1.51.

- \(bu
Win32 has been upgraded from version 0.53 to 0.57.
.Sp
Fix calling convention for \f(CW\*(C`PFNRegGetValueA\*(C'.
.Sp
Added \f(CW\*(C`Win32::IsSymlinkCreationAllowed()\*(C',
\f(CW\*(C`Win32::IsDeveloperModeEnabled()\*(C', and \f(CW\*(C`Win32::GetProcessPrivileges()\*(C'.
.Sp
Removed old code for versions before Windows 2000.

- \(bu
XS::APItest has been upgraded from version 1.09 to 1.16.

- \(bu
XS::Typemap has been upgraded from version 0.17 to 0.18.

## Documentation

Header "Documentation"

### New Documentation

Subsection "New Documentation"
*perldocstyle*
Subsection "perldocstyle"

This document is a guide for the authorship and maintenance of the
documentation that ships with Perl.

*perlgov*
Subsection "perlgov"

This document describes the goals, scope, system, and rules for Perl's new
governance model.

Other pod files, most notably perlpolicy, were amended to reflect
its adoption.

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
We have attempted to update the documentation to reflect the changes
listed in this document.  If you find any we have missed, open an issue
at <https://github.com/Perl/perl5/issues>.

Additionally, the following selected changes have been made:

- \(bu
perlapi, perlguts, perlxs, and perlxstut now prefer \f(CW\*(C`SvPVbyte\*(C'
over \f(CW\*(C`SvPV\*(C'.

- \(bu
References to **Pumpking** have been replaced with a more accurate term or
**Steering Council** where appropriate.

- \(bu
**The Perl Steering Council** is now the fallback contact for security issues.

*perlapi*
Subsection "perlapi"

- \(bu
Efforts continue in improving the presentation of this document, and to
document more \s-1API\s0 elements.

*perlcommunity*
Subsection "perlcommunity"

- \(bu
The freenode \s-1IRC URL\s0 has been updated.

*perldebguts*
Subsection "perldebguts"

- \(bu
Corrected the description of the scalar \f(CW\*(C`$\{"_<$filename"\}\*(C'
variables.

*perldiag*
Subsection "perldiag"

- \(bu
Now documents additional examples of \*(L"not imported\*(R" warnings.

*perlfaq*
Subsection "perlfaq"

- \(bu
The Perl \s-1FAQ\s0 was updated to \s-1CPAN\s0 version 5.20201107 with minor
improvements.

*perlfunc*
Subsection "perlfunc"

- \(bu
**my()** and **state()** now explicitly warn
the reader that lexical variables should typically not be redeclared
within the same scope or statement.
[\s-1GH\s0 #18389 <https://github.com/Perl/perl5/issues/18389>]

- \(bu
The localtime entry has been improved and now
also states that the result of the function is always in English.

- \(bu
**msgsnd()** documented a length field included in the
packed \f(CW\*(C`MSG\*(C' parameter to \f(CW\*(C`msgsnd()\*(C', but there was no such field.
\f(CW\*(C`MSG\*(C' contains only the type and the message content.

- \(bu
Better explanation of what happens when \f(CW\*(C`sleep\*(C' is called with a zero or
negative value.

- \(bu
Simplify the \f(CW\*(C`split()\*(C' documentation by removing the \f(CW\*(C`join()\*(C's from the
examples
[\s-1GH\s0 #18676 <https://github.com/Perl/perl5/issues/18676>]

*perlgit*
Subsection "perlgit"

- \(bu
document how to create a remote-tracking branch for every \s-1PR\s0

- \(bu
document how to get a \s-1PR\s0 as a local branch

*perlguts*
Subsection "perlguts"

- \(bu
perlguts now explains in greater detail the need to consult \f(CW\*(C`SvUTF8\*(C'
when calling \f(CW\*(C`SvPV\*(C' (or variants). A new \*(L"How do I pass a Perl string to a C
library?\*(R" section in the same document discusses when to use which style of
macro to read an \s-1SV\s0's string value.

- \(bu
Corrected \f(CW\*(C`my_rpeep\*(C' example in perlguts.

- \(bu
A section has been added on the formatted printing of special sizes.

*perlop*
Subsection "perlop"

- \(bu
The \f(CW\*(C`<>\*(C' and \f(CW\*(C`<<>>\*(C' operators are commonly referred to as
the diamond and double diamond operators respectively, but that wasn't
mentioned previously in their documentation.

- \(bu
Document range op behavior change.

*perlpacktut*
Subsection "perlpacktut"

- \(bu
Incorrect variables used in an example have been fixed.

*perlsyn*
Subsection "perlsyn"

- \(bu
Document that **caller()** does not see try\{\} blocks

- \(bu
A new example shows how a lexical \f(CW\*(C`my\*(C' variable can be declared
during the initialization of a \f(CW\*(C`for\*(C' loop.

*perlunifaq*
Subsection "perlunifaq"

- \(bu
Fix description of what Perl does with unencoded strings

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### New Diagnostics

Subsection "New Diagnostics"
*New Errors*
Subsection "New Errors"

- \(bu
Bareword filehandle \*(L"%s\*(R" not allowed under 'no feature \*(L"bareword_filehandles\*(R"'
.Sp
This accompanies the new
bareword_filehandles feature.

- \(bu
Multidimensional hash lookup is disabled
.Sp
This accompanies the new
multidimensional feature.

*New Warnings*
Subsection "New Warnings"

- \(bu
Wide character in setenv key (encoding to utf8)
.Sp
Attempts to put wide characters into environment variable keys via \f(CW%ENV now
provoke this warning.

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
Error \f(CW%s in expansion of \f(CW%s
.Sp
An error was encountered in handling a user-defined property
(\*(L"User-Defined Character Properties\*(R" in perlunicode).  These are
programmer written subroutines, hence subject to errors that may
prevent them from compiling or running.

- \(bu
Infinite recursion in user-defined property
.Sp
A user-defined property (\*(L"User-Defined Character Properties\*(R" in perlunicode)
can depend on the definitions of other user-defined
properties.  If the chain of dependencies leads back to this property,
infinite recursion would occur, were it not for the check that raised
this error.

- \(bu
Timeout waiting for another thread to define \\p\{%s\}
.Sp
The first time a user-defined property
(\*(L"User-Defined Character Properties\*(R" in perlunicode) is used, its
definition is looked up and converted into an internal form for more
efficient handling in subsequent uses.  There could be a race if two or
more threads tried to do this processing nearly simultaneously.

- \(bu
Unknown user-defined property name \\p\{%s\}
.Sp
You specified to use a property within the \f(CW\*(C`\\p\{...\}\*(C' which was a
syntactically valid user-defined property, but no definition was found
for it

- \(bu
Too few arguments for subroutine '%s' (got \f(CW%d; expected \f(CW%d)
.Sp
Subroutine argument-count mismatch errors now include the number of
given and expected arguments.

- \(bu
Too many arguments for subroutine '%s' (got \f(CW%d; expected \f(CW%d)
.Sp
Subroutine argument-count mismatch errors now include the number of
given and expected arguments.

- \(bu
Lost precision when \f(CW%s \f(CW%f by 1
.Sp
This warning was only issued for positive too-large values when
incrementing, and only for negative ones when decrementing.
It is now issued for both positive or negative too-large values.
[\s-1GH\s0 #18333 <https://github.com/Perl/perl5/issues/18333>]

- \(bu
\\K not permitted in lookahead/lookbehind in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
This error was incorrectly produced in some cases involving nested
lookarounds.  This has been fixed.
[\s-1GH\s0 #18123 <https://github.com/Perl/perl5/issues/18123>]

- \(bu
Use of uninitialized value%s
.Sp
This warning may now include the array or hash index when the
uninitialized value is the result of an element not found.  This will
only happen if the index is a simple non-magical variable.

## Utility Changes

Header "Utility Changes"

### perl5db.pl (the debugger)

Subsection "perl5db.pl (the debugger)"

- \(bu
New option: \f(CW\*(C`HistItemMinLength\*(C'
.Sp
This option controls the minimum length a command must be to get stored in
history.  Traditionally, this has been fixed at 2.  Changes to the debugger
are often perilous, and new bugs should be reported so the debugger can be
debugged.

- \(bu
Fix to \f(CW\*(C`i\*(C' and \f(CW\*(C`l\*(C' commands
.Sp
The \f(CW\*(C`i $var\*(C' and \f(CW\*(C`l $var\*(C' commands work again with lexical variables.

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
Prevented incpath to spill into libpth

- \(bu
Use realpath if available. (This might catch more duplicate paths.)

- \(bu
Only include real existing paths.

- \(bu
Filter inc paths out of libpth.

- \(bu
stadtx hash support has been removed
.Sp
stadtx support has been entirely removed.  Previously, it could be requested
with \f(CW\*(C`PERL_HASH_FUNC_STADTX\*(C', and was default in 64-bit builds.  It has been
replaced with SipHash.  SipHash has been more rigorously reviewed than stadtx.

- \(bu
Configure
.Sp
A new probe checks for buggy libc implementations of the \f(CW\*(C`gcvt\*(C'/\f(CW\*(C`qgcvt\*(C'
functions.
[\s-1GH\s0 #18170 <https://github.com/Perl/perl5/issues/18170>]

- \(bu
\f(CW\*(C`-Dusedefaultstrict\*(C'
.Sp
Perl can now be built with strict on by default (using the configuration
option \f(CW\*(C`-Dusedefaultstrict\*(C'.
.Sp
These strict defaults do not apply when \f(CW\*(C`perl\*(C' is run via \f(CW\*(C`-e\*(C' or \f(CW\*(C`-E\*(C'.
.Sp
This setting provides a diagnostic mechanism intended for development
purposes only and is thus undefined by default.

- \(bu
The minimum supported Bison version is now 2.4, and the maximum is 3.7.

- \(bu
Newer 64-bit versions of the Intel C/\*(C+ compiler are now recognised
and have the correct flags set.

- \(bu
We now trap \s-1SIGBUS\s0 when *Configure* checks for \f(CW\*(C`va_copy\*(C'.
.Sp
On several systems the attempt to determine if we need \f(CW\*(C`va_copy\*(C' or similar
results in a \s-1SIGBUS\s0 instead of the expected \s-1SIGSEGV,\s0 which previously caused a
core dump.
.Sp
[\s-1GH\s0 #18148 <https://github.com/Perl/perl5/issues/18148>]

## Testing

Header "Testing"
Tests were added and changed to reflect the other additions and
changes in this release.  Furthermore, these significant changes were
made:

- \(bu
Split Config-dependent tests in *t/opbasic/arith.t* to *t/op/arith2.t*

- \(bu
*t/re/opt.t* was added, providing a test harness for regexp optimization.
[\s-1GH\s0 #18213 <https://github.com/Perl/perl5/pull/18213>]

- \(bu
A workaround for \s-1CPAN\s0 distributions needing dot in \f(CW@INC has been removed
[\s-1GH\s0 #18394 <https://github.com/Perl/perl5/pull/18394>].
All distributions that previously required the workaround have now been
adapted.

- \(bu
When testing in parallel on many-core platforms, you can now cause the
test suite to finish somewhat earlier, but with less logical ordering of
the tests, by setting
.Sp
.Vb 1
 PERL_TEST_HARNESS_ASAP=1
.Ve
.Sp
while running the test suite.

## Platform Support

Header "Platform Support"

### New Platforms

Subsection "New Platforms"

- 9front
Item "9front"
Allow building Perl on i386 9front systems (a fork of plan9).

### Updated Platforms

Subsection "Updated Platforms"

- Plan9
Item "Plan9"
Improve support for Plan9 on i386 platforms.

- MacOS (Darwin)
Item "MacOS (Darwin)"
The hints file for darwin has been updated to handle future MacOS versions
beyond 10. [\s-1GH\s0 #17946 <https://github.com/Perl/perl5/issues/17946>]

### Discontinued Platforms

Subsection "Discontinued Platforms"

- Symbian
Item "Symbian"
Support code relating to Symbian has been removed.  Symbian was an
operating system for mobile devices.  The port was last updated in July
2009, and the platform itself in October 2012.

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- DragonFlyBSD
Item "DragonFlyBSD"
Tests were updated to workaround DragonFlyBSD bugs in tc*()
functions <https://bugs.dragonflybsd.org/issues/3252> and ctime
updates <https://bugs.dragonflybsd.org/issues/3251>.

- Mac \s-1OS X\s0
Item "Mac OS X"
A number of system libraries no longer exist as actual files on Big Sur,
even though \f(CW\*(C`dlopen\*(C' will pretend they do, so now we fall back to \f(CW\*(C`dlopen\*(C'
if a library file can not be found.
[\s-1GH\s0 #18407 <https://github.com/Perl/perl5/issues/18407>]

- Windows
Item "Windows"
Reading non-ASCII characters from the console when its codepage was set to
65001 (\s-1UTF-8\s0) was broken due to a bug in Windows. A workaround for this
problem has been implemented.
[\s-1GH\s0 #18701 <https://github.com/Perl/perl5/issues/18701>]
.Sp
Building with mingw.org compilers (version 3.4.5 or later) using mingw runtime
versions < 3.22 now works again.  This was broken in Perl 5.31.4.
.Sp
Building with mingw.org compilers (version 3.4.5 or later) using mingw runtime
versions >= 3.21 now works (for compilers up to version 5.3.0).
.Sp
*Makefile.mk*, and thus support for dmake, has been removed. It is still
possible to build Perl on Windows using nmake (Makefile) and \s-1GNU\s0 make
(GNUmakefile).
[\s-1GH\s0 #18511 <https://github.com/Perl/perl5/pull/18511>]
.Sp
perl can now be built with \f(CW\*(C`USE_QUADMATH\*(C' on \s-1MS\s0 Windows using
(32-bit and 64-bit) mingw-w64 ports of gcc.
[\s-1GH\s0 #18465 <https://github.com/Perl/perl5/pull/18465>]
.Sp
The *pl2bat.pl* utility now needs to \f(CW\*(C`use ExtUtils::PL2Bat\*(C'. This could
cause failures in parallel builds.
.Sp
Windows now supports **symlink()** and
**readlink()**, and **lstat()** is no
longer an alias for **stat()**.
[\s-1GH\s0 #18005 <https://github.com/Perl/perl5/issues/18005>].
.Sp
Unlike \s-1POSIX\s0 systems, creating a symbolic link on Windows requires
either elevated privileges or Windows 10 1703 or later with Developer
Mode enabled.
.Sp
**stat()**, including \f(CW\*(C`stat FILEHANDLE\*(C', and **lstat()** now uses our own
implementation that populates the device \f(CW\*(C`dev\*(C' and inode numbers
\f(CW\*(C`ino\*(C' returned rather than always returning zero.  The number of
links \f(CW\*(C`nlink\*(C' field is now always populated.
.Sp
\f(CW\*(C`$\{^WIN32_SLOPPY_STAT\}\*(C'  previously
controlled whether the \f(CW\*(C`nlink\*(C' field was populated requiring a
separate Windows \s-1API\s0 call to fetch, since \f(CW\*(C`nlink\*(C' and the other
information required for \f(CW\*(C`stat()\*(C' is now retrieved in a single \s-1API\s0 call.
.Sp
The \f(CW\*(C`-r\*(C' and \f(CW\*(C`-w\*(C' operators now return true for the \f(CW\*(C`STDIN\*(C',
\f(CW\*(C`STDOUT\*(C' and \f(CW\*(C`STDERR\*(C' handles.  Unfortunately it still won't return
true for duplicates of those handles.
[\s-1GH\s0 #8502 <https://github.com/Perl/perl5/issues/8502>].
.Sp
The times returned by **stat()** and **lstat()** are no longer incorrect
across Daylight Savings Time adjustments.
[\s-1GH\s0 #6080 <https://github.com/Perl/perl5/issues/6080>].
.Sp
\f(CW\*(C`-x\*(C' on a filehandle should now match \f(CW\*(C`-x\*(C' on the corresponding
filename on Vista or later.
[\s-1GH\s0 #4145 <https://github.com/Perl/perl5/issues/4145>].
.Sp
\f(CW\*(C`-e \*(Aq"\*(Aq\*(C' no longer incorrectly returns true.
[\s-1GH\s0 #12431 <https://github.com/Perl/perl5/issues/12431>].
.Sp
The same manifest is now used for Visual \*(C+ and gcc builds.
.Sp
Previously, \s-1MSVC\s0 builds were using the **/manifestdependency** flag instead of
embedding *perlexe.manifest*, which caused issues such as \f(CW\*(C`GetVersionEx()\*(C'
returning the wrong version number on Windows 10.

- z/OS
Item "z/OS"
The locale categories \f(CW\*(C`LC_SYNTAX\*(C' and \f(CW\*(C`LC_TOD\*(C' are now recognized.
Perl doesn't do anything with these, except it now allows you to specify
them.  They are included in \f(CW\*(C`LC_ALL\*(C'.

## Internal Changes

Header "Internal Changes"

- \(bu
Corrected handling of double and long double parameters for perl's
implementation of formatted output for \f(CW\*(C`-Dusequadmath\*(C' builds.
.Sp
This applies to \f(CW\*(C`PerlIO_printf()\*(C', \f(CW\*(C`croak()\*(C', \f(CW\*(C`warn()\*(C', \f(CW\*(C`sv_catpvf()\*(C' and
their variants.
.Sp
Previously in \f(CW\*(C`quadmath\*(C' builds, code like:
.Sp
.Vb 1
  PerlIO_printf(PerlIO_stderr(), "%g", somedouble);
.Ve
.Sp
or
.Sp
.Vb 1
  PerlIO_printf(PerlIO_stderr(), "%Lg", somelongdouble);
.Ve
.Sp
would erroneously throw an exception \*(L"panic: quadmath invalid format
...\*(R", since the code added for quadmath builds assumed \f(CW\*(C`NV\*(C's were the
only floating point format passed into these functions.
.Sp
This code would also process the standard C long double specifier \f(CW\*(C`L\*(C'
as if it expected an \f(CW\*(C`NV\*(C' (\f(CW\*(C`_\|_float128\*(C' for quadmath builds),
resulting in undefined behaviour.
.Sp
These functions now correctly accept doubles, long doubles and NVs.

- \(bu
Previously the right operand of bitwise shift operators (shift amount)
was implicitly cast from \s-1IV\s0 to int, but it might lead wrong results
if \s-1IV\s0 does not fit in int.
.Sp
And also, shifting \s-1INT_MIN\s0 bits used to yield the shiftee unchanged
(treated as 0-bit shift instead of negative shift).

- \(bu
A set of \f(CW\*(C`cop_hints_exists_\{pv,pvn,pvs,sv\}\*(C' functions was added,
to support checking for the existence of keys in the hints hash of a
specific cop without needing to create a mortal copy of said value.

- \(bu
An aid has been added for using the \f(CW\*(C`DEBUG\*(C' macros when debugging \s-1XS\s0 or
C code. The comments in *perl.h* describe \f(CW\*(C`DEBUG_PRE_STMTS\*(C' and
\f(CW\*(C`DEBUG_POST_STMTS\*(C'. which you can \f(CW\*(C`#define\*(C' to do things like save and
restore \f(CW\*(C`errno\*(C', in case the \f(CW\*(C`DEBUG\*(C' calls are interfering with that,
or to display timestamps, or which thread it's coming from, or the
location of the call, or whatever.  You can make a quick hack to help
you track something down without having to edit individual \f(CW\*(C`DEBUG\*(C'
calls.

- \(bu
Make \f(CW\*(C`REFCOUNTED_HE_EXISTS\*(C' available outside of core

- \(bu
All \f(CW\*(C`SvTRUE\*(C'-ish functions now evaluate their arguments exactly once.
In 5.32, plain "\f(CW\*(C`SvTRUE\*(C'" in perlapi was changed to do that; now the rest
do as well.

- \(bu
Unicode is now a first class citizen when considering the pattern /A*B/ where
A and B are arbitrary.  The pattern matching code tries to make a tight loop
to match the span of A's.  The logic of this was now really updated with
support for \s-1UTF-8.\s0

- \(bu
The re module has a new function \f(CW\*(C`optimization\*(C', which can return a
hashref of optimization data discovered about a compiled regexp.

- \(bu
The \f(CW\*(C`PERL_GLOBAL_STRUCT\*(C' compilation option has been removed, and
with it the need or the \f(CW\*(C`dVAR\*(C' macro.  \f(CW\*(C`dVAR\*(C' remains defined as a
no-op outside \f(CW\*(C`PERL_CORE\*(C' for backwards compatiblity with \s-1XS\s0 modules.

- \(bu
A new savestack type \f(CW\*(C`SAVEt_HINTS_HH\*(C' has been added, which neatens the
previous behaviour of \f(CW\*(C`SAVEt_HINTS\*(C'.  On previous versions the types and
values pushed to the save stack would depend on whether the hints included the
\f(CW\*(C`HINT_LOCALIZE_HH\*(C' bit, which complicates external code that inspects the
save stack. The new version uses a different savestack type to indicate the
difference.

- \(bu
A new \s-1API\s0 function \*(L"av_count\*(R" in perlapi has been added which gives a
clearly named way to find how many elements are in an array.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Setting \f(CW%ENV now properly handles upgraded strings in the key. Previously
Perl sent the \s-1SV\s0's internal \s-1PV\s0 directly to the \s-1OS\s0; now it will handle keys
as it has handled values since 5.18: attempt to downgrade the string first;
if that fails then warn and use the utf8 form.

- \(bu
Fix a memory leak in regcomp.c
[\s-1GH\s0 #18604 <https://github.com/Perl/perl5/issues/18604>]

- \(bu
pack/unpack format 'D' now works on all systems that could support it
.Sp
Previously if \f(CW\*(C`NV == long double\*(C', now it is supported on all platforms that
have long doubles. In particular that means it is now also supported on
quadmath platforms.

- \(bu
Skip trying to constant fold an incomplete op tree
[\s-1GH\s0 #18380 <https://github.com/Perl/perl5/issues/18380>]
.Sp
Constant folding of chained comparison op trees could fail under certain
conditions, causing perl to crash. As a quick fix, constant folding is
now skipped for such op trees. This also addresses
[\s-1GH\s0 #17917 <https://github.com/Perl/perl5/issues/17917>].

- \(bu
\f(CW%g formatting broken on Ubuntu-18.04, \f(CW\*(C`NVSIZE == 8\*(C'
[\s-1GH\s0 #18170 <https://github.com/Perl/perl5/issues/18170>]
.Sp
Buggy libc implementations of the \f(CW\*(C`gcvt\*(C' and \f(CW\*(C`qgcvt\*(C' functions
caused \f(CW\*(C`(s)printf\*(C' to incorrectly truncate \f(CW%g formatted numbers.
A new Configure probe now checks for this, with the result that the libc
\f(CW\*(C`sprintf\*(C' will be used in place of \f(CW\*(C`gcvt\*(C' and \f(CW\*(C`qgcvt\*(C'.
.Sp
Tests added as part of this fix also revealed related problems in
some Windows builds. The makefiles for \s-1MINGW\s0 builds on Windows have
thus been adjusted to use \f(CW\*(C`USE_MINGW_ANSI_STDIO\*(C' by default, ensuring
that they also provide correct \f(CW\*(C`(s)printf\*(C' formatting of numbers.

- \(bu
*op.c*: croak on \f(CW\*(C`my $_\*(C' when \f(CW\*(C`use utf8\*(C' is in effect
[\s-1GH\s0 #18449 <https://github.com/Perl/perl5/issues/18449>]
.Sp
The lexical topic feature experiment was removed in Perl v5.24 and
declaring \f(CW\*(C`my $_\*(C' became a compile time error. However, it was previously
still possible to make this declaration if \f(CW\*(C`use utf8\*(C' was in effect.

- \(bu
*regexec.c*: Fix assertion failure
[\s-1GH\s0 #18451 <https://github.com/Perl/perl5/issues/18451>]
.Sp
Fuzzing triggered an assertion failure in the regexp engine when too many
characters were copied into a buffer.

- \(bu
**semctl()**, **msgctl()**, and
**shmctl()** now properly reset the \s-1UTF-8\s0 flag on the
\f(CW\*(C`ARG\*(C' parameter if it's modified for \f(CW\*(C`IPC_STAT\*(C' or \f(CW\*(C`GETALL\*(C'
operations.

- \(bu
\f(CW\*(C`semctl()\*(C', \f(CW\*(C`msgctl()\*(C', and \f(CW\*(C`shmctl()\*(C' now attempt to downgrade the \f(CW\*(C`ARG\*(C'
parameter if its value is being used as input to \f(CW\*(C`IPC_SET\*(C' or
\f(CW\*(C`SETALL\*(C' calls.  A failed downgrade will thrown an exception.

- \(bu
In cases where \f(CW\*(C`semctl()\*(C', \f(CW\*(C`msgctl()\*(C' or \f(CW\*(C`shmctl()\*(C' would treat the \f(CW\*(C`ARG\*(C'
parameter as a pointer, an undefined value no longer generates a
warning.  In most such calls the pointer isn't used anyway and this
allows you to supply \f(CW\*(C`undef\*(C' for a value not used by the underlying
function.

- \(bu
**semop()** now downgrades the \f(CW\*(C`OPSTRING\*(C' parameter,
**msgsnd()** now downgrades the \f(CW\*(C`MSG\*(C' parameter and
shmwrite now downgrades the \f(CW\*(C`STRING\*(C' parameter
to treat them as bytes.  Previously they would be left upgraded,
providing a corrupted structure to the underlying function call.

- \(bu
**msgrcv()** now properly resets the \s-1UTF-8\s0 flag the
\f(CW\*(C`VAR\*(C' parameter when it is modified.  Previously the \s-1UTF-8\s0 flag could
be left on, resulting in a possibly corrupt result in \f(CW\*(C`VAR\*(C'.

- \(bu
Magic is now called correctly for stacked file test operators.
[\s-1GH\s0 #18293 <https://github.com/Perl/perl5/issues/18293>]

- \(bu
The \f(CW\*(C`@ary = split(...)\*(C' optimization no longer switches in the target
array as the value stack.
[\s-1GH\s0 #18232 <https://github.com/Perl/perl5/issues/18232>]
Also see discussion at
<https://github.com/Perl/perl5/pull/18014#issuecomment-671299506>.

- \(bu
Fixed a bug in which some regexps with recursive subpatterns matched
incorrectly.
.Sp
[\s-1GH\s0 #18096 <https://github.com/Perl/perl5/issues/18096>]

- \(bu
On Win32, \f(CW\*(C`waitpid(-1, WNOHANG)\*(C' could sometimes have a very large
timeout.  [\s-1GH\s0 #16529 <https://github.com/Perl/perl5/issues/16529>]

- \(bu
\f(CW\*(C`MARK\*(C' and hence \f(CW\*(C`items\*(C' are now correctly initialized in \f(CW\*(C`BOOT\*(C' XSUBs.

- \(bu
Some list assignments involving \f(CW\*(C`undef\*(C' on the left-hand side were
over-optimized and produced incorrect results.
[\s-1GH\s0 #16685 <https://github.com/Perl/perl5/issues/16685>],
[\s-1GH\s0 #17816 <https://github.com/Perl/perl5/issues/17816>]

## Known Problems

Header "Known Problems"
None

## Errata From Previous Releases

Header "Errata From Previous Releases"
None

## Obituary

Header "Obituary"
Kent Fredric (\s-1KENTNL\s0) passed away in February 2021.  A native of New Zealand
and a self-described \*(L"huge geek,\*(R" Kent was the author or maintainer of 178
\s-1CPAN\s0 distributions, the Perl maintainer for the Gentoo Linux distribution and
a contributor to the Perl core distribution.  He is mourned by his family,
friends and open source software communities worldwide.

## Acknowledgements

Header "Acknowledgements"
Perl 5.34.0 represents approximately 11 months of development since Perl
5.32.0 and contains approximately 280,000 lines of changes across 2,100
files from 78 authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 150,000 lines of changes to 1,300 .pm, .t, .c and .h files.

Perl continues to flourish into its fourth decade thanks to a vibrant
community of users and developers. The following people are known to have
contributed the improvements that became Perl 5.34.0:

Aaron Crane, Adam Hartley, Andy Dougherty, Ben Cornett, Branislav
Zahradni\*'k, brian d foy, Chris 'BinGOs' Williams, Christian Walde
(Mithaldu), Craig A. Berry, Dagfinn Ilmari Mannsa\*oker, Dan Book, Daniel
Bo\*:hmer, Daniel Lau\*:gt, Dan Kogai, David Cantrell, David Mitchell, Dominic
Hamon, E. Choroba, Ed J, Eric Herman, Eric Lindblad, Eugene Alvin Villar,
Felipe Gasper, Giovanni Tataranni, Graham Knop, Graham Ollis, Hauke D,
H.Merijn Brand, Hugo van der Sanden, Ichinose Shogo, Ivan Baidakou, Jae
Bradley, James E Keenan, Jason McIntosh, jkahrman, John Karr, John Lightsey,
Kang-min Liu, Karen Etheridge, Karl Williamson, Keith Thompson, Leon
Timmermans, Marc Reisner, Marcus Holland-Moritz, Max Maischein, Michael G
Schwern, Nicholas Clark, Nicolas R., Paul Evans, Petr Pi\*'saX, raiph, Renee
Baecker, Ricardo Signes, Richard Leach, Romano, Ryan Voots, Samanta Navarro,
Samuel Thibault, Sawyer X, Scott Baker, Sergey Poznyakoff, Sevan Janiyan,
Shirakata Kentaro, Shlomi Fish, Sisyphus, Sizhe Zhao, Steve Hay, \s-1TAKAI\s0
Kousuke, Thibault Duponchelle, Todd Rinaldo, Tomasz Konojacki, Tom Hukins,
Tom Stellard, Tony Cook, vividsnow, Yves Orton, Zakariyya Mughal,
\s-1XXXXXX XXXXXXXX.\s0

The list above is almost certainly incomplete as it is automatically
generated from version control history. In particular, it does not include
the names of the (very much appreciated) contributors who reported issues to
the Perl bug tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core. We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please
see the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database
at <https://github.com/Perl/perl5/issues>.  There may also be information at
<http://www.perl.org/>, the Perl Home Page.

If you believe you have an unreported bug, please open an issue at
<https://github.com/Perl/perl5/issues>.  Be sure to trim your bug down to a
tiny but sufficient test case.

If the bug you are reporting has security implications which make it
inappropriate to send to a public issue tracker, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5,
you can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
