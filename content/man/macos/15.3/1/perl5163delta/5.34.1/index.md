+++
description = "This document describes differences between the 5.16.2 release and the 5.16.3 release. If you are upgrading from an earlier release such as 5.16.1, first read perl5162delta, which describes differences between 5.16.1 and 5.16.2. No changes since ..."
detected_package_version = "5.34.1"
manpage_format = "troff"
operating_system = "macos"
manpage_name = "perl5163delta"
date = "2022-02-19"
manpage_section = "1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
author = "None Specified"
operating_system_version = "15.3"
title = "perl5163delta(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5163DELTA 1"
PERL5163DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5163delta - what is new for perl v5.16.3

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.16.2 release and
the 5.16.3 release.

If you are upgrading from an earlier release such as 5.16.1, first read
perl5162delta, which describes differences between 5.16.1 and
5.16.2.

## Core Enhancements

Header "Core Enhancements"
No changes since 5.16.0.

## Security

Header "Security"
This release contains one major and a number of minor security fixes.
These latter are included mainly to allow the test suite to pass cleanly
with the clang compiler's address sanitizer facility.

### \s-1CVE-2013-1667:\s0 memory exhaustion with arbitrary hash keys

Subsection "CVE-2013-1667: memory exhaustion with arbitrary hash keys"
With a carefully crafted set of hash keys (for example arguments on a
\s-1URL\s0), it is possible to cause a hash to consume a large amount of memory
and \s-1CPU,\s0 and thus possibly to achieve a Denial-of-Service.

This problem has been fixed.

### wrap-around with \s-1IO\s0 on long strings

Subsection "wrap-around with IO on long strings"
Reading or writing strings greater than 2**31 bytes in size could segfault
due to integer wraparound.

This problem has been fixed.

### memory leak in Encode

Subsection "memory leak in Encode"
The \s-1UTF-8\s0 encoding implementation in Encode.xs had a memory leak which has been
fixed.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.16.0. If any
exist, they are bugs and reports are welcome.

## Deprecations

Header "Deprecations"
There have been no deprecations since 5.16.0.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Encode has been upgraded from version 2.44 to version 2.44_01.

- \(bu
Module::CoreList has been upgraded from version 2.76 to version 2.76_02.

- \(bu
XS::APItest has been upgraded from version 0.38 to version 0.39.

## Known Problems

Header "Known Problems"
None.

## Acknowledgements

Header "Acknowledgements"
Perl 5.16.3 represents approximately 4 months of development since Perl 5.16.2
and contains approximately 870 lines of changes across 39 files from 7 authors.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers. The following people are known to have contributed the
improvements that became Perl 5.16.3:

Andy Dougherty, Chris 'BinGOs' Williams, Dave Rolsky, David Mitchell, Michael
Schroeder, Ricardo Signes, Yves Orton.

The list above is almost certainly incomplete as it is automatically generated
from version control history. In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please
send it to perl5-security-report@perl.org. This points to a closed
subscription unarchived mailing list, which includes all the core
committers, who will be able to help assess the impact of issues, figure
out a resolution, and help co-ordinate the release of patches to
mitigate or fix the problem across all platforms on which Perl is
supported. Please only use this address for security issues in the Perl
core, not for modules independently distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
