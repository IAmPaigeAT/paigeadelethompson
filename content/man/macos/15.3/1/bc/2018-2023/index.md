+++
description = "bc(1) is an interactive processor for a language first standardized in 1991 by POSIX. (See the f[B]STANDARDSf[R] section.) The language provides unlimited precision decimal arithmetic and is somewhat C-like, but there are differences. Such differ..."
operating_system_version = "15.3"
title = "bc(1)"
date = "Sun Feb 16 04:48:19 2025"
manpage_name = "bc"
author = "Gavin D. Howard and contributors."
manpage_section = "1"
operating_system = "macos"
keywords = ["dc", "standards", "bc", "is", "compliant", "with", "the", "ieee", "std", "1003", "1-2017", "lq", "posix", "rq", "specification", "at", "https", "pubs", "opengroup", "org", "onlinepubs", "9699919799", "utilities", "html", "flags", "f", "b", "efghiqsvvw", "r", "all", "long", "options", "and", "extensions", "noted", "above", "are", "to", "that", "in", "addition", "behavior", "of", "quit", "implements", "an", "interpretation", "different", "from", "known", "implementations", "for", "more", "information", "see", "statements", "subsection", "syntax", "section", "note", "explicitly", "says", "only", "accepts", "numbers", "use", "a", "period", "as", "radix", "point", "regardless", "value", "lc_numeric", "this", "supports", "error", "messages", "locales", "thus", "it", "lc_messages", "bugs", "before", "version", "6", "1", "had", "incorrect", "statement", "no", "other", "report", "git", "gavinhoward", "com", "gavin", "authors", "d", "howard", "contributors"]
manpage_format = "troff"
detected_package_version = "2018-2023"
+++

"BC" "1" "February 2023" "Gavin D. Howard" "General Commands Manual"

## NAME


bc - arbitrary-precision decimal arithmetic language and calculator

## SYNOPSIS


\f[B]bc\f[R] [\f[B]-cCghilPqRsvVw\f[R]] [\f[B]--digit-clamp\f[R]]
[\f[B]--no-digit-clamp\f[R]] [\f[B]--global-stacks\f[R]]
[\f[B]--help\f[R]] [\f[B]--interactive\f[R]] [\f[B]--mathlib\f[R]]
[\f[B]--no-prompt\f[R]] [\f[B]--no-read-prompt\f[R]] [\f[B]--quiet\f[R]]
[\f[B]--standard\f[R]] [\f[B]--warn\f[R]] [\f[B]--version\f[R]]
[\f[B]-e\f[R] \f[I]expr\f[R]]
[\f[B]--expression\f[R]=\f[I]expr\f[R]...]
[\f[B]-f\f[R] \f[I]file\f[R]...]
[\f[B]--file\f[R]=\f[I]file\f[R]...]
[\f[I]file\f[R]...]
[\f[B]-I\f[R] \f[I]ibase\f[R]] [\f[B]--ibase\f[R]=\f[I]ibase\f[R]]
[\f[B]-O\f[R] \f[I]obase\f[R]] [\f[B]--obase\f[R]=\f[I]obase\f[R]]
[\f[B]-S\f[R] \f[I]scale\f[R]] [\f[B]--scale\f[R]=\f[I]scale\f[R]]
[\f[B]-E\f[R] \f[I]seed\f[R]] [\f[B]--seed\f[R]=\f[I]seed\f[R]]

## DESCRIPTION


bc(1) is an interactive processor for a language first standardized in
1991 by POSIX.
(See the \f[B]STANDARDS\f[R] section.)
The language provides unlimited precision decimal arithmetic and is
somewhat C-like, but there are differences.
Such differences will be noted in this document.

After parsing and handling options, this bc(1) reads any files given on
the command line and executes them before reading from \f[B]stdin\f[R].

This bc(1) is a drop-in replacement for \f[I]any\f[R] bc(1), including
(and especially) the GNU bc(1).
It also has many extensions and extra features beyond other
implementations.

\f[B]Note\f[R]: If running this bc(1) on \f[I]any\f[R] script meant for
another bc(1) gives a parse error, it is probably because a word this
bc(1) reserves as a keyword is used as the name of a function, variable,
or array.
To fix that, use the command-line option \f[B]-r\f[R] \f[I]keyword\f[R],
where \f[I]keyword\f[R] is the keyword that is used as a name in the
script.
For more information, see the \f[B]OPTIONS\f[R] section.

If parsing scripts meant for other bc(1) implementations still does not
work, that is a bug and should be reported.
See the \f[B]BUGS\f[R] section.

## OPTIONS


The following are the options that bc(1) accepts.

\f[B]-C\f[R], \f[B]--no-digit-clamp\f[R]
Disables clamping of digits greater than or equal to the current
\f[B]ibase\f[R] when parsing numbers.

> 
This means that the value added to a number from a digit is always that
digit\[cq]s value multiplied by the value of ibase raised to the power
of the digit\[cq]s position, which starts from 0 at the least
significant digit.

If this and/or the \f[B]-c\f[R] or \f[B]--digit-clamp\f[R] options are
given multiple times, the last one given is used.

This option overrides the \f[B]BC_DIGIT_CLAMP\f[R] environment variable
(see the \f[B]ENVIRONMENT VARIABLES\f[R] section) and the default, which
can be queried with the \f[B]-h\f[R] or \f[B]--help\f[R] options.

This is a \f[B]non-portable extension\f[R].



\f[B]-c\f[R], \f[B]--digit-clamp\f[R]
Enables clamping of digits greater than or equal to the current
\f[B]ibase\f[R] when parsing numbers.

> 
This means that digits that the value added to a number from a digit
that is greater than or equal to the ibase is the value of ibase minus 1
all multiplied by the value of ibase raised to the power of the
digit\[cq]s position, which starts from 0 at the least significant
digit.

If this and/or the \f[B]-C\f[R] or \f[B]--no-digit-clamp\f[R] options
are given multiple times, the last one given is used.

This option overrides the \f[B]BC_DIGIT_CLAMP\f[R] environment variable
(see the \f[B]ENVIRONMENT VARIABLES\f[R] section) and the default, which
can be queried with the \f[B]-h\f[R] or \f[B]--help\f[R] options.

This is a \f[B]non-portable extension\f[R].



\f[B]-E\f[R] \f[I]seed\f[R], \f[B]--seed\f[R]=\f[I]seed\f[R]
Sets the builtin variable \f[B]seed\f[R] to the value \f[I]seed\f[R]
assuming that \f[I]seed\f[R] is in base 10.
It is a fatal error if \f[I]seed\f[R] is not a valid number.

> 
If multiple instances of this option are given, the last is used.

This is a \f[B]non-portable extension\f[R].



\f[B]-e\f[R] \f[I]expr\f[R], \f[B]--expression\f[R]=\f[I]expr\f[R]
Evaluates \f[I]expr\f[R].
If multiple expressions are given, they are evaluated in order.
If files are given as well (see the \f[B]-f\f[R] and \f[B]--file\f[R]
options), the expressions and files are evaluated in the order given.
This means that if a file is given before an expression, the file is
read in and evaluated first.

> 
If this option is given on the command-line (i.e., not in
\f[B]BC_ENV_ARGS\f[R], see the \f[B]ENVIRONMENT VARIABLES\f[R] section),
then after processing all expressions and files, bc(1) will exit, unless
\f[B]-\f[R] (\f[B]stdin\f[R]) was given as an argument at least once to
\f[B]-f\f[R] or \f[B]--file\f[R], whether on the command-line or in
\f[B]BC_ENV_ARGS\f[R].
However, if any other \f[B]-e\f[R], \f[B]--expression\f[R],
\f[B]-f\f[R], or \f[B]--file\f[R] arguments are given after
\f[B]-f-\f[R] or equivalent is given, bc(1) will give a fatal error and
exit.

This is a \f[B]non-portable extension\f[R].



\f[B]-f\f[R] \f[I]file\f[R], \f[B]--file\f[R]=\f[I]file\f[R]
Reads in \f[I]file\f[R] and evaluates it, line by line, as though it
were read through \f[B]stdin\f[R].
If expressions are also given (see the \f[B]-e\f[R] and
\f[B]--expression\f[R] options), the expressions are evaluated in the
order given.

> 
If this option is given on the command-line (i.e., not in
\f[B]BC_ENV_ARGS\f[R], see the \f[B]ENVIRONMENT VARIABLES\f[R] section),
then after processing all expressions and files, bc(1) will exit, unless
\f[B]-\f[R] (\f[B]stdin\f[R]) was given as an argument at least once to
\f[B]-f\f[R] or \f[B]--file\f[R].
However, if any other \f[B]-e\f[R], \f[B]--expression\f[R],
\f[B]-f\f[R], or \f[B]--file\f[R] arguments are given after
\f[B]-f-\f[R] or equivalent is given, bc(1) will give a fatal error and
exit.

This is a \f[B]non-portable extension\f[R].



\f[B]-g\f[R], \f[B]--global-stacks\f[R]
Turns the globals \f[B]ibase\f[R], \f[B]obase\f[R], \f[B]scale\f[R], and
\f[B]seed\f[R] into stacks.

> 
This has the effect that a copy of the current value of all four are
pushed onto a stack for every function call, as well as popped when
every function returns.
This means that functions can assign to any and all of those globals
without worrying that the change will affect other functions.
Thus, a hypothetical function named \f[B]output(x,b)\f[R] that simply
printed \f[B]x\f[R] in base \f[B]b\f[R] could be written like this:
.IP

```
\f[C]
define void output(x, b) {
    obase=b
    x
}
\f[R]
```


instead of like this:
.IP

```
\f[C]
define void output(x, b) {
    auto c
    c=obase
    obase=b
    x
    obase=c
}
\f[R]
```


This makes writing functions much easier.

(\f[B]Note\f[R]: the function \f[B]output(x,b)\f[R] exists in the
extended math library.
See the \f[B]LIBRARY\f[R] section.)

However, since using this flag means that functions cannot set
\f[B]ibase\f[R], \f[B]obase\f[R], \f[B]scale\f[R], or \f[B]seed\f[R]
globally, functions that are made to do so cannot work anymore.
There are two possible use cases for that, and each has a solution.

First, if a function is called on startup to turn bc(1) into a number
converter, it is possible to replace that capability with various shell
aliases.
Examples:
.IP

```
\f[C]
alias d2o=\[dq]bc -e ibase=A -e obase=8\[dq]
alias h2b=\[dq]bc -e ibase=G -e obase=2\[dq]
\f[R]
```


Second, if the purpose of a function is to set \f[B]ibase\f[R],
\f[B]obase\f[R], \f[B]scale\f[R], or \f[B]seed\f[R] globally for any
other purpose, it could be split into one to four functions (based on
how many globals it sets) and each of those functions could return the
desired value for a global.

For functions that set \f[B]seed\f[R], the value assigned to
\f[B]seed\f[R] is not propagated to parent functions.
This means that the sequence of pseudo-random numbers that they see will
not be the same sequence of pseudo-random numbers that any parent sees.
This is only the case once \f[B]seed\f[R] has been set.

If a function desires to not affect the sequence of pseudo-random
numbers of its parents, but wants to use the same \f[B]seed\f[R], it can
use the following line:
.IP

```
\f[C]
seed = seed
\f[R]
```


If the behavior of this option is desired for every run of bc(1), then
users could make sure to define \f[B]BC_ENV_ARGS\f[R] and include this
option (see the \f[B]ENVIRONMENT VARIABLES\f[R] section for more
details).

If \f[B]-s\f[R], \f[B]-w\f[R], or any equivalents are used, this option
is ignored.

This is a \f[B]non-portable extension\f[R].



\f[B]-h\f[R], \f[B]--help\f[R]
Prints a usage message and exits.

\f[B]-I\f[R] \f[I]ibase\f[R], \f[B]--ibase\f[R]=\f[I]ibase\f[R]
Sets the builtin variable \f[B]ibase\f[R] to the value \f[I]ibase\f[R]
assuming that \f[I]ibase\f[R] is in base 10.
It is a fatal error if \f[I]ibase\f[R] is not a valid number.

> 
If multiple instances of this option are given, the last is used.

This is a \f[B]non-portable extension\f[R].



\f[B]-i\f[R], \f[B]--interactive\f[R]
Forces interactive mode.
(See the \f[B]INTERACTIVE MODE\f[R] section.)

> 
This is a \f[B]non-portable extension\f[R].



\f[B]-L\f[R], \f[B]--no-line-length\f[R]
Disables line length checking and prints numbers without backslashes and
newlines.
In other words, this option sets \f[B]BC_LINE_LENGTH\f[R] to \f[B]0\f[R]
(see the \f[B]ENVIRONMENT VARIABLES\f[R] section).

> 
This is a \f[B]non-portable extension\f[R].



\f[B]-l\f[R], \f[B]--mathlib\f[R]
Sets \f[B]scale\f[R] (see the \f[B]SYNTAX\f[R] section) to \f[B]20\f[R]
and loads the included math library and the extended math library before
running any code, including any expressions or files specified on the
command line.

> 
To learn what is in the libraries, see the \f[B]LIBRARY\f[R] section.



\f[B]-O\f[R] \f[I]obase\f[R], \f[B]--obase\f[R]=\f[I]obase\f[R]
Sets the builtin variable \f[B]obase\f[R] to the value \f[I]obase\f[R]
assuming that \f[I]obase\f[R] is in base 10.
It is a fatal error if \f[I]obase\f[R] is not a valid number.

> 
If multiple instances of this option are given, the last is used.

This is a \f[B]non-portable extension\f[R].



\f[B]-P\f[R], \f[B]--no-prompt\f[R]
Disables the prompt in TTY mode.
(The prompt is only enabled in TTY mode.
See the \f[B]TTY MODE\f[R] section.)
This is mostly for those users that do not want a prompt or are not used
to having them in bc(1).
Most of those users would want to put this option in
\f[B]BC_ENV_ARGS\f[R] (see the \f[B]ENVIRONMENT VARIABLES\f[R] section).

> 
These options override the \f[B]BC_PROMPT\f[R] and \f[B]BC_TTY_MODE\f[R]
environment variables (see the \f[B]ENVIRONMENT VARIABLES\f[R] section).

This is a \f[B]non-portable extension\f[R].



\f[B]-q\f[R], \f[B]--quiet\f[R]
This option is for compatibility with the GNU bc(1)
(https://www.gnu.org/software/bc/); it is a no-op.
Without this option, GNU bc(1) prints a copyright header.
This bc(1) only prints the copyright header if one or more of the
\f[B]-v\f[R], \f[B]-V\f[R], or \f[B]--version\f[R] options are given
unless the \f[B]BC_BANNER\f[R] environment variable is set and contains
a non-zero integer or if this bc(1) was built with the header displayed
by default.
If \f[I]any\f[R] of that is the case, then this option \f[I]does\f[R]
prevent bc(1) from printing the header.

> 
This is a \f[B]non-portable extension\f[R].



\f[B]-R\f[R], \f[B]--no-read-prompt\f[R]
Disables the read prompt in TTY mode.
(The read prompt is only enabled in TTY mode.
See the \f[B]TTY MODE\f[R] section.)
This is mostly for those users that do not want a read prompt or are not
used to having them in bc(1).
Most of those users would want to put this option in
\f[B]BC_ENV_ARGS\f[R] (see the \f[B]ENVIRONMENT VARIABLES\f[R] section).
This option is also useful in hash bang lines of bc(1) scripts that
prompt for user input.

> 
This option does not disable the regular prompt because the read prompt
is only used when the \f[B]read()\f[R] built-in function is called.

These options \f[I]do\f[R] override the \f[B]BC_PROMPT\f[R] and
\f[B]BC_TTY_MODE\f[R] environment variables (see the \f[B]ENVIRONMENT
VARIABLES\f[R] section), but only for the read prompt.

This is a \f[B]non-portable extension\f[R].



\f[B]-r\f[R] \f[I]keyword\f[R], \f[B]--redefine\f[R]=\f[I]keyword\f[R]
Redefines \f[I]keyword\f[R] in order to allow it to be used as a
function, variable, or array name.
This is useful when this bc(1) gives parse errors when parsing scripts
meant for other bc(1) implementations.

> 
The keywords this bc(1) allows to be redefined are:

- \[bu]
\f[B]abs\f[R]

- \[bu]
\f[B]asciify\f[R]

- \[bu]
\f[B]continue\f[R]

- \[bu]
\f[B]divmod\f[R]

- \[bu]
\f[B]else\f[R]

- \[bu]
\f[B]halt\f[R]

- \[bu]
\f[B]irand\f[R]

- \[bu]
\f[B]last\f[R]

- \[bu]
\f[B]limits\f[R]

- \[bu]
\f[B]maxibase\f[R]

- \[bu]
\f[B]maxobase\f[R]

- \[bu]
\f[B]maxrand\f[R]

- \[bu]
\f[B]maxscale\f[R]

- \[bu]
\f[B]modexp\f[R]

- \[bu]
\f[B]print\f[R]

- \[bu]
\f[B]rand\f[R]

- \[bu]
\f[B]read\f[R]

- \[bu]
\f[B]seed\f[R]

- \[bu]
\f[B]stream\f[R]

If any of those keywords are used as a function, variable, or array name
in a script, use this option with the keyword as the argument.
If multiple are used, use this option for all of them; it can be used
multiple times.

Keywords are \f[I]not\f[R] redefined when parsing the builtin math
library (see the \f[B]LIBRARY\f[R] section).

It is a fatal error to redefine keywords mandated by the POSIX standard
(see the \f[B]STANDARDS\f[R] section).
It is a fatal error to attempt to redefine words that this bc(1) does
not reserve as keywords.



\f[B]-S\f[R] \f[I]scale\f[R], \f[B]--scale\f[R]=\f[I]scale\f[R]
Sets the builtin variable \f[B]scale\f[R] to the value \f[I]scale\f[R]
assuming that \f[I]scale\f[R] is in base 10.
It is a fatal error if \f[I]scale\f[R] is not a valid number.

> 
If multiple instances of this option are given, the last is used.

This is a \f[B]non-portable extension\f[R].



\f[B]-s\f[R], \f[B]--standard\f[R]
Process exactly the language defined by the standard (see the
\f[B]STANDARDS\f[R] section) and error if any extensions are used.

> 
This is a \f[B]non-portable extension\f[R].



\f[B]-v\f[R], \f[B]-V\f[R], \f[B]--version\f[R]
Print the version information (copyright header) and exits.

> 
This is a \f[B]non-portable extension\f[R].



\f[B]-w\f[R], \f[B]--warn\f[R]
Like \f[B]-s\f[R] and \f[B]--standard\f[R], except that warnings (and
not errors) are printed for non-standard extensions and execution
continues normally.

> 
This is a \f[B]non-portable extension\f[R].



\f[B]-z\f[R], \f[B]--leading-zeroes\f[R]
Makes bc(1) print all numbers greater than \f[B]-1\f[R] and less than
\f[B]1\f[R], and not equal to \f[B]0\f[R], with a leading zero.

> 
This can be set for individual numbers with the \f[B]plz(x)\f[R],
\f[B]plznl(x)\f[R], \f[B]pnlz(x)\f[R], and \f[B]pnlznl(x)\f[R] functions
in the extended math library (see the \f[B]LIBRARY\f[R] section).

This is a \f[B]non-portable extension\f[R].



All long options are \f[B]non-portable extensions\f[R].

## STDIN


If no files or expressions are given by the \f[B]-f\f[R],
\f[B]--file\f[R], \f[B]-e\f[R], or \f[B]--expression\f[R] options, then
bc(1) reads from \f[B]stdin\f[R].

However, there are a few caveats to this.

First, \f[B]stdin\f[R] is evaluated a line at a time.
The only exception to this is if the parse cannot complete.
That means that starting a string without ending it or starting a
function, \f[B]if\f[R] statement, or loop without ending it will also
cause bc(1) to not execute.

Second, after an \f[B]if\f[R] statement, bc(1) doesn\[cq]t know if an
\f[B]else\f[R] statement will follow, so it will not execute until it
knows there will not be an \f[B]else\f[R] statement.

## STDOUT


Any non-error output is written to \f[B]stdout\f[R].
In addition, if history (see the \f[B]HISTORY\f[R] section) and the
prompt (see the \f[B]TTY MODE\f[R] section) are enabled, both are output
to \f[B]stdout\f[R].

\f[B]Note\f[R]: Unlike other bc(1) implementations, this bc(1) will
issue a fatal error (see the \f[B]EXIT STATUS\f[R] section) if it cannot
write to \f[B]stdout\f[R], so if \f[B]stdout\f[R] is closed, as in
\f[B]bc >&-\f[R], it will quit with an error.
This is done so that bc(1) can report problems when \f[B]stdout\f[R] is
redirected to a file.

If there are scripts that depend on the behavior of other bc(1)
implementations, it is recommended that those scripts be changed to
redirect \f[B]stdout\f[R] to \f[B]/dev/null\f[R].

## STDERR


Any error output is written to \f[B]stderr\f[R].

\f[B]Note\f[R]: Unlike other bc(1) implementations, this bc(1) will
issue a fatal error (see the \f[B]EXIT STATUS\f[R] section) if it cannot
write to \f[B]stderr\f[R], so if \f[B]stderr\f[R] is closed, as in
\f[B]bc 2>&-\f[R], it will quit with an error.
This is done so that bc(1) can exit with an error code when
\f[B]stderr\f[R] is redirected to a file.

If there are scripts that depend on the behavior of other bc(1)
implementations, it is recommended that those scripts be changed to
redirect \f[B]stderr\f[R] to \f[B]/dev/null\f[R].

## SYNTAX


The syntax for bc(1) programs is mostly C-like, with some differences.
This bc(1) follows the POSIX standard (see the \f[B]STANDARDS\f[R]
section), which is a much more thorough resource for the language this
bc(1) accepts.
This section is meant to be a summary and a listing of all the
extensions to the standard.

In the sections below, \f[B]E\f[R] means expression, \f[B]S\f[R] means
statement, and \f[B]I\f[R] means identifier.

Identifiers (\f[B]I\f[R]) start with a lowercase letter and can be
followed by any number (up to \f[B]BC_NAME_MAX-1\f[R]) of lowercase
letters (\f[B]a-z\f[R]), digits (\f[B]0-9\f[R]), and underscores
(\f[B]_\f[R]).
The regex is \f[B][a-z][a-z0-9_]*\f[R].
Identifiers with more than one character (letter) are a
\f[B]non-portable extension\f[R].

\f[B]ibase\f[R] is a global variable determining how to interpret
constant numbers.
It is the \[lq]input\[rq] base, or the number base used for interpreting
input numbers.
\f[B]ibase\f[R] is initially \f[B]10\f[R].
If the \f[B]-s\f[R] (\f[B]--standard\f[R]) and \f[B]-w\f[R]
(\f[B]--warn\f[R]) flags were not given on the command line, the max
allowable value for \f[B]ibase\f[R] is \f[B]36\f[R].
Otherwise, it is \f[B]16\f[R].
The min allowable value for \f[B]ibase\f[R] is \f[B]2\f[R].
The max allowable value for \f[B]ibase\f[R] can be queried in bc(1)
programs with the \f[B]maxibase()\f[R] built-in function.

\f[B]obase\f[R] is a global variable determining how to output results.
It is the \[lq]output\[rq] base, or the number base used for outputting
numbers.
\f[B]obase\f[R] is initially \f[B]10\f[R].
The max allowable value for \f[B]obase\f[R] is \f[B]BC_BASE_MAX\f[R] and
can be queried in bc(1) programs with the \f[B]maxobase()\f[R] built-in
function.
The min allowable value for \f[B]obase\f[R] is \f[B]0\f[R].
If \f[B]obase\f[R] is \f[B]0\f[R], values are output in scientific
notation, and if \f[B]obase\f[R] is \f[B]1\f[R], values are output in
engineering notation.
Otherwise, values are output in the specified base.

Outputting in scientific and engineering notations are \f[B]non-portable
extensions\f[R].

The \f[I]scale\f[R] of an expression is the number of digits in the
result of the expression right of the decimal point, and \f[B]scale\f[R]
is a global variable that sets the precision of any operations, with
exceptions.
\f[B]scale\f[R] is initially \f[B]0\f[R].
\f[B]scale\f[R] cannot be negative.
The max allowable value for \f[B]scale\f[R] is \f[B]BC_SCALE_MAX\f[R]
and can be queried in bc(1) programs with the \f[B]maxscale()\f[R]
built-in function.

bc(1) has both \f[I]global\f[R] variables and \f[I]local\f[R] variables.
All \f[I]local\f[R] variables are local to the function; they are
parameters or are introduced in the \f[B]auto\f[R] list of a function
(see the \f[B]FUNCTIONS\f[R] section).
If a variable is accessed which is not a parameter or in the
\f[B]auto\f[R] list, it is assumed to be \f[I]global\f[R].
If a parent function has a \f[I]local\f[R] variable version of a
variable that a child function considers \f[I]global\f[R], the value of
that \f[I]global\f[R] variable in the child function is the value of the
variable in the parent function, not the value of the actual
\f[I]global\f[R] variable.

All of the above applies to arrays as well.

The value of a statement that is an expression (i.e., any of the named
expressions or operands) is printed unless the lowest precedence
operator is an assignment operator \f[I]and\f[R] the expression is
notsurrounded by parentheses.

The value that is printed is also assigned to the special variable
\f[B]last\f[R].
A single dot (\f[B].\f[R]) may also be used as a synonym for
\f[B]last\f[R].
These are \f[B]non-portable extensions\f[R].

Either semicolons or newlines may separate statements.

### Comments


There are two kinds of comments:

- 1.
Block comments are enclosed in \f[B]/*\f[R] and \f[B]*/\f[R].

- 2.
Line comments go from \f[B]#\f[R] until, and not including, the next
newline.
This is a \f[B]non-portable extension\f[R].

### Named Expressions


The following are named expressions in bc(1):

- 1.
Variables: \f[B]I\f[R]

- 2.
Array Elements: \f[B]I[E]\f[R]

- 3.
\f[B]ibase\f[R]

- 4.
\f[B]obase\f[R]

- 5.
\f[B]scale\f[R]

- 6.
\f[B]seed\f[R]

- 7.
\f[B]last\f[R] or a single dot (\f[B].\f[R])

Numbers 6 and 7 are \f[B]non-portable extensions\f[R].

The meaning of \f[B]seed\f[R] is dependent on the current pseudo-random
number generator but is guaranteed to not change except for new major
versions.

The \f[I]scale\f[R] and sign of the value may be significant.

If a previously used \f[B]seed\f[R] value is assigned to \f[B]seed\f[R]
and used again, the pseudo-random number generator is guaranteed to
produce the same sequence of pseudo-random numbers as it did when the
\f[B]seed\f[R] value was previously used.

The exact value assigned to \f[B]seed\f[R] is not guaranteed to be
returned if \f[B]seed\f[R] is queried again immediately.
However, if \f[B]seed\f[R] \f[I]does\f[R] return a different value, both
values, when assigned to \f[B]seed\f[R], are guaranteed to produce the
same sequence of pseudo-random numbers.
This means that certain values assigned to \f[B]seed\f[R] will
\f[I]not\f[R] produce unique sequences of pseudo-random numbers.
The value of \f[B]seed\f[R] will change after any use of the
\f[B]rand()\f[R] and \f[B]irand(E)\f[R] operands (see the
\f[I]Operands\f[R] subsection below), except if the parameter passed to
\f[B]irand(E)\f[R] is \f[B]0\f[R], \f[B]1\f[R], or negative.

There is no limit to the length (number of significant decimal digits)
or \f[I]scale\f[R] of the value that can be assigned to \f[B]seed\f[R].

Variables and arrays do not interfere; users can have arrays named the
same as variables.
This also applies to functions (see the \f[B]FUNCTIONS\f[R] section), so
a user can have a variable, array, and function that all have the same
name, and they will not shadow each other, whether inside of functions
or not.

Named expressions are required as the operand of
\f[B]increment\f[R]/\f[B]decrement\f[R] operators and as the left side
of \f[B]assignment\f[R] operators (see the \f[I]Operators\f[R]
subsection).

### Operands


The following are valid operands in bc(1):

-  1.
Numbers (see the \f[I]Numbers\f[R] subsection below).

-  2.
Array indices (\f[B]I[E]\f[R]).

-  3.
\f[B](E)\f[R]: The value of \f[B]E\f[R] (used to change precedence).

-  4.
\f[B]sqrt(E)\f[R]: The square root of \f[B]E\f[R].
\f[B]E\f[R] must be non-negative.

-  5.
\f[B]length(E)\f[R]: The number of significant decimal digits in
\f[B]E\f[R].
Returns \f[B]1\f[R] for \f[B]0\f[R] with no decimal places.
If given a string, the length of the string is returned.
Passing a string to \f[B]length(E)\f[R] is a \f[B]non-portable
extension\f[R].

-  6.
\f[B]length(I[])\f[R]: The number of elements in the array \f[B]I\f[R].
This is a \f[B]non-portable extension\f[R].

-  7.
\f[B]scale(E)\f[R]: The \f[I]scale\f[R] of \f[B]E\f[R].

-  8.
\f[B]abs(E)\f[R]: The absolute value of \f[B]E\f[R].
This is a \f[B]non-portable extension\f[R].

-  9.
\f[B]is_number(E)\f[R]: \f[B]1\f[R] if the given argument is a number,
\f[B]0\f[R] if it is a string.
This is a \f[B]non-portable extension\f[R].

- 10.
\f[B]is_string(E)\f[R]: \f[B]1\f[R] if the given argument is a string,
\f[B]0\f[R] if it is a number.
This is a \f[B]non-portable extension\f[R].

- 11.
\f[B]modexp(E, E, E)\f[R]: Modular exponentiation, where the first
expression is the base, the second is the exponent, and the third is the
modulus.
All three values must be integers.
The second argument must be non-negative.
The third argument must be non-zero.
This is a \f[B]non-portable extension\f[R].

- 12.
\f[B]divmod(E, E, I[])\f[R]: Division and modulus in one operation.
This is for optimization.
The first expression is the dividend, and the second is the divisor,
which must be non-zero.
The return value is the quotient, and the modulus is stored in index
\f[B]0\f[R] of the provided array (the last argument).
This is a \f[B]non-portable extension\f[R].

- 13.
\f[B]asciify(E)\f[R]: If \f[B]E\f[R] is a string, returns a string that
is the first letter of its argument.
If it is a number, calculates the number mod \f[B]256\f[R] and returns
that number as a one-character string.
This is a \f[B]non-portable extension\f[R].

- 14.
\f[B]asciify(I[])\f[R]: A string that is made up of the characters that
would result from running \f[B]asciify(E)\f[R] on each element of the
array identified by the argument.
This allows creating multi-character strings and storing them.
This is a \f[B]non-portable extension\f[R].

- 15.
\f[B]I()\f[R], \f[B]I(E)\f[R], \f[B]I(E, E)\f[R], and so on, where
\f[B]I\f[R] is an identifier for a non-\f[B]void\f[R] function (see the
\f[I]Void Functions\f[R] subsection of the \f[B]FUNCTIONS\f[R] section).
The \f[B]E\f[R] argument(s) may also be arrays of the form
\f[B]I[]\f[R], which will automatically be turned into array references
(see the \f[I]Array References\f[R] subsection of the
\f[B]FUNCTIONS\f[R] section) if the corresponding parameter in the
function definition is an array reference.

- 16.
\f[B]read()\f[R]: Reads a line from \f[B]stdin\f[R] and uses that as an
expression.
The result of that expression is the result of the \f[B]read()\f[R]
operand.
This is a \f[B]non-portable extension\f[R].

- 17.
\f[B]maxibase()\f[R]: The max allowable \f[B]ibase\f[R].
This is a \f[B]non-portable extension\f[R].

- 18.
\f[B]maxobase()\f[R]: The max allowable \f[B]obase\f[R].
This is a \f[B]non-portable extension\f[R].

- 19.
\f[B]maxscale()\f[R]: The max allowable \f[B]scale\f[R].
This is a \f[B]non-portable extension\f[R].

- 20.
\f[B]line_length()\f[R]: The line length set with
\f[B]BC_LINE_LENGTH\f[R] (see the \f[B]ENVIRONMENT VARIABLES\f[R]
section).
This is a \f[B]non-portable extension\f[R].

- 21.
\f[B]global_stacks()\f[R]: \f[B]0\f[R] if global stacks are not enabled
with the \f[B]-g\f[R] or \f[B]--global-stacks\f[R] options, non-zero
otherwise.
See the \f[B]OPTIONS\f[R] section.
This is a \f[B]non-portable extension\f[R].

- 22.
\f[B]leading_zero()\f[R]: \f[B]0\f[R] if leading zeroes are not enabled
with the \f[B]-z\f[R] or \f[B]\[en]leading-zeroes\f[R] options, non-zero
otherwise.
See the \f[B]OPTIONS\f[R] section.
This is a \f[B]non-portable extension\f[R].

- 23.
\f[B]rand()\f[R]: A pseudo-random integer between \f[B]0\f[R]
(inclusive) and \f[B]BC_RAND_MAX\f[R] (inclusive).
Using this operand will change the value of \f[B]seed\f[R].
This is a \f[B]non-portable extension\f[R].

- 24.
\f[B]irand(E)\f[R]: A pseudo-random integer between \f[B]0\f[R]
(inclusive) and the value of \f[B]E\f[R] (exclusive).
If \f[B]E\f[R] is negative or is a non-integer (\f[B]E\f[R]\[cq]s
\f[I]scale\f[R] is not \f[B]0\f[R]), an error is raised, and bc(1)
resets (see the \f[B]RESET\f[R] section) while \f[B]seed\f[R] remains
unchanged.
If \f[B]E\f[R] is larger than \f[B]BC_RAND_MAX\f[R], the higher bound is
honored by generating several pseudo-random integers, multiplying them
by appropriate powers of \f[B]BC_RAND_MAX+1\f[R], and adding them
together.
Thus, the size of integer that can be generated with this operand is
unbounded.
Using this operand will change the value of \f[B]seed\f[R], unless the
value of \f[B]E\f[R] is \f[B]0\f[R] or \f[B]1\f[R].
In that case, \f[B]0\f[R] is returned, and \f[B]seed\f[R] is
\f[I]not\f[R] changed.
This is a \f[B]non-portable extension\f[R].

- 25.
\f[B]maxrand()\f[R]: The max integer returned by \f[B]rand()\f[R].
This is a \f[B]non-portable extension\f[R].

The integers generated by \f[B]rand()\f[R] and \f[B]irand(E)\f[R] are
guaranteed to be as unbiased as possible, subject to the limitations of
the pseudo-random number generator.

\f[B]Note\f[R]: The values returned by the pseudo-random number
generator with \f[B]rand()\f[R] and \f[B]irand(E)\f[R] are guaranteed to
\f[I]NOT\f[R] be cryptographically secure.
This is a consequence of using a seeded pseudo-random number generator.
However, they \f[I]are\f[R] guaranteed to be reproducible with identical
\f[B]seed\f[R] values.
This means that the pseudo-random values from bc(1) should only be used
where a reproducible stream of pseudo-random numbers is
\f[I]ESSENTIAL\f[R].
In any other case, use a non-seeded pseudo-random number generator.

### Numbers


Numbers are strings made up of digits, uppercase letters, and at most
\f[B]1\f[R] period for a radix.
Numbers can have up to \f[B]BC_NUM_MAX\f[R] digits.
Uppercase letters are equal to \f[B]9\f[R] plus their position in the
alphabet, starting from \f[B]1\f[R] (i.e., \f[B]A\f[R] equals
\f[B]10\f[R], or \f[B]9+1\f[R]).

If a digit or letter makes no sense with the current value of
\f[B]ibase\f[R] (i.e., they are greater than or equal to the current
value of \f[B]ibase\f[R]), then the behavior depends on the existence of
the \f[B]-c\f[R]/\f[B]--digit-clamp\f[R] or
\f[B]-C\f[R]/\f[B]--no-digit-clamp\f[R] options (see the
\f[B]OPTIONS\f[R] section), the existence and setting of the
\f[B]BC_DIGIT_CLAMP\f[R] environment variable (see the \f[B]ENVIRONMENT
VARIABLES\f[R] section), or the default, which can be queried with the
\f[B]-h\f[R]/\f[B]--help\f[R] option.

If clamping is off, then digits or letters that are greater than or
equal to the current value of \f[B]ibase\f[R] are not changed.
Instead, their given value is multiplied by the appropriate power of
\f[B]ibase\f[R] and added into the number.
This means that, with an \f[B]ibase\f[R] of \f[B]3\f[R], the number
\f[B]AB\f[R] is equal to \f[B]3\[ha]1*A+3\[ha]0*B\f[R], which is
\f[B]3\f[R] times \f[B]10\f[R] plus \f[B]11\f[R], or \f[B]41\f[R].

If clamping is on, then digits or letters that are greater than or equal
to the current value of \f[B]ibase\f[R] are set to the value of the
highest valid digit in \f[B]ibase\f[R] before being multiplied by the
appropriate power of \f[B]ibase\f[R] and added into the number.
This means that, with an \f[B]ibase\f[R] of \f[B]3\f[R], the number
\f[B]AB\f[R] is equal to \f[B]3\[ha]1*2+3\[ha]0*2\f[R], which is
\f[B]3\f[R] times \f[B]2\f[R] plus \f[B]2\f[R], or \f[B]8\f[R].

There is one exception to clamping: single-character numbers (i.e.,
\f[B]A\f[R] alone).
Such numbers are never clamped and always take the value they would have
in the highest possible \f[B]ibase\f[R].
This means that \f[B]A\f[R] alone always equals decimal \f[B]10\f[R] and
\f[B]Z\f[R] alone always equals decimal \f[B]35\f[R].
This behavior is mandated by the standard (see the STANDARDS section)
and is meant to provide an easy way to set the current \f[B]ibase\f[R]
(with the \f[B]i\f[R] command) regardless of the current value of
\f[B]ibase\f[R].

If clamping is on, and the clamped value of a character is needed, use a
leading zero, i.e., for \f[B]A\f[R], use \f[B]0A\f[R].

In addition, bc(1) accepts numbers in scientific notation.
These have the form \f[B]<number>e<integer>\f[R].
The exponent (the portion after the \f[B]e\f[R]) must be an integer.
An example is \f[B]1.89237e9\f[R], which is equal to
\f[B]1892370000\f[R].
Negative exponents are also allowed, so \f[B]4.2890e-3\f[R] is equal to
\f[B]0.0042890\f[R].

Using scientific notation is an error or warning if the \f[B]-s\f[R] or
\f[B]-w\f[R], respectively, command-line options (or equivalents) are
given.

\f[B]WARNING\f[R]: Both the number and the exponent in scientific
notation are interpreted according to the current \f[B]ibase\f[R], but
the number is still multiplied by \f[B]10\[ha]exponent\f[R] regardless
of the current \f[B]ibase\f[R].
For example, if \f[B]ibase\f[R] is \f[B]16\f[R] and bc(1) is given the
number string \f[B]FFeA\f[R], the resulting decimal number will be
\f[B]2550000000000\f[R], and if bc(1) is given the number string
\f[B]10e-4\f[R], the resulting decimal number will be \f[B]0.0016\f[R].

Accepting input as scientific notation is a \f[B]non-portable
extension\f[R].

### Operators


The following arithmetic and logical operators can be used.
They are listed in order of decreasing precedence.
Operators in the same group have the same precedence.

\f[B]++\f[R] \f[B]--\f[R]
Type: Prefix and Postfix

> 
Associativity: None

Description: \f[B]increment\f[R], \f[B]decrement\f[R]



\f[B]-\f[R] \f[B]!\f[R]
Type: Prefix

> 
Associativity: None

Description: \f[B]negation\f[R], \f[B]boolean not\f[R]



\f[B]$\f[R]
Type: Postfix

> 
Associativity: None

Description: \f[B]truncation\f[R]



\f[B]\[at]\f[R]
Type: Binary

> 
Associativity: Right

Description: \f[B]set precision\f[R]



\f[B]\[ha]\f[R]
Type: Binary

> 
Associativity: Right

Description: \f[B]power\f[R]



\f[B]*\f[R] \f[B]/\f[R] \f[B]%\f[R]
Type: Binary

> 
Associativity: Left

Description: \f[B]multiply\f[R], \f[B]divide\f[R], \f[B]modulus\f[R]



\f[B]+\f[R] \f[B]-\f[R]
Type: Binary

> 
Associativity: Left

Description: \f[B]add\f[R], \f[B]subtract\f[R]



\f[B]<<\f[R] \f[B]>>\f[R]
Type: Binary

> 
Associativity: Left

Description: \f[B]shift left\f[R], \f[B]shift right\f[R]



\f[B]=\f[R] \f[B]<<=\f[R] \f[B]>>=\f[R] \f[B]+=\f[R] \f[B]-=\f[R] \f[B]*=\f[R] \f[B]/=\f[R] \f[B]%=\f[R] \f[B]\[ha]=\f[R] \f[B]\[at]=\f[R]
Type: Binary

> 
Associativity: Right

Description: \f[B]assignment\f[R]



\f[B]==\f[R] \f[B]<=\f[R] \f[B]>=\f[R] \f[B]!=\f[R] \f[B]<\f[R] \f[B]>\f[R]
Type: Binary

> 
Associativity: Left

Description: \f[B]relational\f[R]



\f[B]&&\f[R]
Type: Binary

> 
Associativity: Left

Description: \f[B]boolean and\f[R]



\f[B]||\f[R]
Type: Binary

> 
Associativity: Left

Description: \f[B]boolean or\f[R]



The operators will be described in more detail below.

\f[B]++\f[R] \f[B]--\f[R]
The prefix and postfix \f[B]increment\f[R] and \f[B]decrement\f[R]
operators behave exactly like they would in C.
They require a named expression (see the \f[I]Named Expressions\f[R]
subsection) as an operand.

> 
The prefix versions of these operators are more efficient; use them
where possible.



\f[B]-\f[R]
The \f[B]negation\f[R] operator returns \f[B]0\f[R] if a user attempts
to negate any expression with the value \f[B]0\f[R].
Otherwise, a copy of the expression with its sign flipped is returned.

\f[B]!\f[R]
The \f[B]boolean not\f[R] operator returns \f[B]1\f[R] if the expression
is \f[B]0\f[R], or \f[B]0\f[R] otherwise.

> 
This is a \f[B]non-portable extension\f[R].



\f[B]$\f[R]
The \f[B]truncation\f[R] operator returns a copy of the given expression
with all of its \f[I]scale\f[R] removed.

> 
This is a \f[B]non-portable extension\f[R].



\f[B]\[at]\f[R]
The \f[B]set precision\f[R] operator takes two expressions and returns a
copy of the first with its \f[I]scale\f[R] equal to the value of the
second expression.
That could either mean that the number is returned without change (if
the \f[I]scale\f[R] of the first expression matches the value of the
second expression), extended (if it is less), or truncated (if it is
more).

> 
The second expression must be an integer (no \f[I]scale\f[R]) and
non-negative.

This is a \f[B]non-portable extension\f[R].



\f[B]\[ha]\f[R]
The \f[B]power\f[R] operator (not the \f[B]exclusive or\f[R] operator,
as it would be in C) takes two expressions and raises the first to the
power of the value of the second.
The \f[I]scale\f[R] of the result is equal to \f[B]scale\f[R].

> 
The second expression must be an integer (no \f[I]scale\f[R]), and if it
is negative, the first value must be non-zero.



\f[B]*\f[R]
The \f[B]multiply\f[R] operator takes two expressions, multiplies them,
and returns the product.
If \f[B]a\f[R] is the \f[I]scale\f[R] of the first expression and
\f[B]b\f[R] is the \f[I]scale\f[R] of the second expression, the
\f[I]scale\f[R] of the result is equal to
\f[B]min(a+b,max(scale,a,b))\f[R] where \f[B]min()\f[R] and
\f[B]max()\f[R] return the obvious values.

\f[B]/\f[R]
The \f[B]divide\f[R] operator takes two expressions, divides them, and
returns the quotient.
The \f[I]scale\f[R] of the result shall be the value of \f[B]scale\f[R].

> 
The second expression must be non-zero.



\f[B]%\f[R]
The \f[B]modulus\f[R] operator takes two expressions, \f[B]a\f[R] and
\f[B]b\f[R], and evaluates them by 1) Computing \f[B]a/b\f[R] to current
\f[B]scale\f[R] and 2) Using the result of step 1 to calculate
\f[B]a-(a/b)*b\f[R] to \f[I]scale\f[R]
\f[B]max(scale+scale(b),scale(a))\f[R].

> 
The second expression must be non-zero.



\f[B]+\f[R]
The \f[B]add\f[R] operator takes two expressions, \f[B]a\f[R] and
\f[B]b\f[R], and returns the sum, with a \f[I]scale\f[R] equal to the
max of the \f[I]scale\f[R]s of \f[B]a\f[R] and \f[B]b\f[R].

\f[B]-\f[R]
The \f[B]subtract\f[R] operator takes two expressions, \f[B]a\f[R] and
\f[B]b\f[R], and returns the difference, with a \f[I]scale\f[R] equal to
the max of the \f[I]scale\f[R]s of \f[B]a\f[R] and \f[B]b\f[R].

\f[B]<<\f[R]
The \f[B]left shift\f[R] operator takes two expressions, \f[B]a\f[R] and
\f[B]b\f[R], and returns a copy of the value of \f[B]a\f[R] with its
decimal point moved \f[B]b\f[R] places to the right.

> 
The second expression must be an integer (no \f[I]scale\f[R]) and
non-negative.

This is a \f[B]non-portable extension\f[R].



\f[B]>>\f[R]
The \f[B]right shift\f[R] operator takes two expressions, \f[B]a\f[R]
and \f[B]b\f[R], and returns a copy of the value of \f[B]a\f[R] with its
decimal point moved \f[B]b\f[R] places to the left.

> 
The second expression must be an integer (no \f[I]scale\f[R]) and
non-negative.

This is a \f[B]non-portable extension\f[R].



\f[B]=\f[R] \f[B]<<=\f[R] \f[B]>>=\f[R] \f[B]+=\f[R] \f[B]-=\f[R] \f[B]*=\f[R] \f[B]/=\f[R] \f[B]%=\f[R] \f[B]\[ha]=\f[R] \f[B]\[at]=\f[R]
The \f[B]assignment\f[R] operators take two expressions, \f[B]a\f[R] and
\f[B]b\f[R] where \f[B]a\f[R] is a named expression (see the \f[I]Named
Expressions\f[R] subsection).

> 
For \f[B]=\f[R], \f[B]b\f[R] is copied and the result is assigned to
\f[B]a\f[R].
For all others, \f[B]a\f[R] and \f[B]b\f[R] are applied as operands to
the corresponding arithmetic operator and the result is assigned to
\f[B]a\f[R].

The \f[B]assignment\f[R] operators that correspond to operators that are
extensions are themselves \f[B]non-portable extensions\f[R].



\f[B]==\f[R] \f[B]<=\f[R] \f[B]>=\f[R] \f[B]!=\f[R] \f[B]<\f[R] \f[B]>\f[R]
The \f[B]relational\f[R] operators compare two expressions, \f[B]a\f[R]
and \f[B]b\f[R], and if the relation holds, according to C language
semantics, the result is \f[B]1\f[R].
Otherwise, it is \f[B]0\f[R].

> 
Note that unlike in C, these operators have a lower precedence than the
\f[B]assignment\f[R] operators, which means that \f[B]a=b>c\f[R] is
interpreted as \f[B](a=b)>c\f[R].

Also, unlike the standard (see the \f[B]STANDARDS\f[R] section)
requires, these operators can appear anywhere any other expressions can
be used.
This allowance is a \f[B]non-portable extension\f[R].



\f[B]&&\f[R]
The \f[B]boolean and\f[R] operator takes two expressions and returns
\f[B]1\f[R] if both expressions are non-zero, \f[B]0\f[R] otherwise.

> 
This is \f[I]not\f[R] a short-circuit operator.

This is a \f[B]non-portable extension\f[R].



\f[B]||\f[R]
The \f[B]boolean or\f[R] operator takes two expressions and returns
\f[B]1\f[R] if one of the expressions is non-zero, \f[B]0\f[R]
otherwise.

> 
This is \f[I]not\f[R] a short-circuit operator.

This is a \f[B]non-portable extension\f[R].



### Statements


The following items are statements:

-  1.
\f[B]E\f[R]

-  2.
\f[B]\{\f[R] \f[B]S\f[R] \f[B];\f[R] ...
\f[B];\f[R] \f[B]S\f[R] \f[B]\}\f[R]

-  3.
\f[B]if\f[R] \f[B](\f[R] \f[B]E\f[R] \f[B])\f[R] \f[B]S\f[R]

-  4.
\f[B]if\f[R] \f[B](\f[R] \f[B]E\f[R] \f[B])\f[R] \f[B]S\f[R]
\f[B]else\f[R] \f[B]S\f[R]

-  5.
\f[B]while\f[R] \f[B](\f[R] \f[B]E\f[R] \f[B])\f[R] \f[B]S\f[R]

-  6.
\f[B]for\f[R] \f[B](\f[R] \f[B]E\f[R] \f[B];\f[R] \f[B]E\f[R]
\f[B];\f[R] \f[B]E\f[R] \f[B])\f[R] \f[B]S\f[R]

-  7.
An empty statement

-  8.
\f[B]break\f[R]

-  9.
\f[B]continue\f[R]

- 10.
\f[B]quit\f[R]

- 11.
\f[B]halt\f[R]

- 12.
\f[B]limits\f[R]

- 13.
A string of characters, enclosed in double quotes

- 14.
\f[B]print\f[R] \f[B]E\f[R] \f[B],\f[R] ...
\f[B],\f[R] \f[B]E\f[R]

- 15.
\f[B]stream\f[R] \f[B]E\f[R] \f[B],\f[R] ...
\f[B],\f[R] \f[B]E\f[R]

- 16.
\f[B]I()\f[R], \f[B]I(E)\f[R], \f[B]I(E, E)\f[R], and so on, where
\f[B]I\f[R] is an identifier for a \f[B]void\f[R] function (see the
\f[I]Void Functions\f[R] subsection of the \f[B]FUNCTIONS\f[R] section).
The \f[B]E\f[R] argument(s) may also be arrays of the form
\f[B]I[]\f[R], which will automatically be turned into array references
(see the \f[I]Array References\f[R] subsection of the
\f[B]FUNCTIONS\f[R] section) if the corresponding parameter in the
function definition is an array reference.

Numbers 4, 9, 11, 12, 14, 15, and 16 are \f[B]non-portable
extensions\f[R].

Also, as a \f[B]non-portable extension\f[R], any or all of the
expressions in the header of a for loop may be omitted.
If the condition (second expression) is omitted, it is assumed to be a
constant \f[B]1\f[R].

The \f[B]break\f[R] statement causes a loop to stop iterating and resume
execution immediately following a loop.
This is only allowed in loops.

The \f[B]continue\f[R] statement causes a loop iteration to stop early
and returns to the start of the loop, including testing the loop
condition.
This is only allowed in loops.

The \f[B]if\f[R] \f[B]else\f[R] statement does the same thing as in C.

The \f[B]quit\f[R] statement causes bc(1) to quit, even if it is on a
branch that will not be executed (it is a compile-time command).

\f[B]Warning\f[R]: The behavior of this bc(1) on \f[B]quit\f[R] is
slightly different from other bc(1) implementations.
Other bc(1) implementations will exit as soon as they finish parsing the
line that a \f[B]quit\f[R] command is on.
This bc(1) will execute any completed and executable statements that
occur before the \f[B]quit\f[R] statement before exiting.

In other words, for the bc(1) code below:
.IP

```
\f[C]
for (i = 0; i < 3; ++i) i; quit
\f[R]
```


Other bc(1) implementations will print nothing, and this bc(1) will
print \f[B]0\f[R], \f[B]1\f[R], and \f[B]2\f[R] on successive lines
before exiting.

The \f[B]halt\f[R] statement causes bc(1) to quit, if it is executed.
(Unlike \f[B]quit\f[R] if it is on a branch of an \f[B]if\f[R] statement
that is not executed, bc(1) does not quit.)

The \f[B]limits\f[R] statement prints the limits that this bc(1) is
subject to.
This is like the \f[B]quit\f[R] statement in that it is a compile-time
command.

An expression by itself is evaluated and printed, followed by a newline.

Both scientific notation and engineering notation are available for
printing the results of expressions.
Scientific notation is activated by assigning \f[B]0\f[R] to
\f[B]obase\f[R], and engineering notation is activated by assigning
\f[B]1\f[R] to \f[B]obase\f[R].
To deactivate them, just assign a different value to \f[B]obase\f[R].

Scientific notation and engineering notation are disabled if bc(1) is
run with either the \f[B]-s\f[R] or \f[B]-w\f[R] command-line options
(or equivalents).

Printing numbers in scientific notation and/or engineering notation is a
\f[B]non-portable extension\f[R].

### Strings


If strings appear as a statement by themselves, they are printed without
a trailing newline.

In addition to appearing as a lone statement by themselves, strings can
be assigned to variables and array elements.
They can also be passed to functions in variable parameters.

If any statement that expects a string is given a variable that had a
string assigned to it, the statement acts as though it had received a
string.

If any math operation is attempted on a string or a variable or array
element that has been assigned a string, an error is raised, and bc(1)
resets (see the \f[B]RESET\f[R] section).

Assigning strings to variables and array elements and passing them to
functions are \f[B]non-portable extensions\f[R].

### Print Statement


The \[lq]expressions\[rq] in a \f[B]print\f[R] statement may also be
strings.
If they are, there are backslash escape sequences that are interpreted
specially.
What those sequences are, and what they cause to be printed, are shown
below:

\f[B]\[rs]a\f[R]: \f[B]\[rs]a\f[R]

\f[B]\[rs]b\f[R]: \f[B]\[rs]b\f[R]

\f[B]\[rs]\[rs]\f[R]: \f[B]\[rs]\f[R]

\f[B]\[rs]e\f[R]: \f[B]\[rs]\f[R]

\f[B]\[rs]f\f[R]: \f[B]\[rs]f\f[R]

\f[B]\[rs]n\f[R]: \f[B]\[rs]n\f[R]

\f[B]\[rs]q\f[R]: \f[B]\[lq]\f[R]

\f[B]\[rs]r\f[R]: \f[B]\[rs]r\f[R]

\f[B]\[rs]t\f[R]: \f[B]\[rs]t\f[R]

Any other character following a backslash causes the backslash and
character to be printed as-is.

Any non-string expression in a print statement shall be assigned to
\f[B]last\f[R], like any other expression that is printed.

### Stream Statement


The \[lq]expressions in a \f[B]stream\f[R] statement may also be
strings.

If a \f[B]stream\f[R] statement is given a string, it prints the string
as though the string had appeared as its own statement.
In other words, the \f[B]stream\f[R] statement prints strings normally,
without a newline.

If a \f[B]stream\f[R] statement is given a number, a copy of it is
truncated and its absolute value is calculated.
The result is then printed as though \f[B]obase\f[R] is \f[B]256\f[R]
and each digit is interpreted as an 8-bit ASCII character, making it a
byte stream.

### Order of Evaluation


All expressions in a statment are evaluated left to right, except as
necessary to maintain order of operations.
This means, for example, assuming that \f[B]i\f[R] is equal to
\f[B]0\f[R], in the expression
.IP

```
\f[C]
a[i++] = i++
\f[R]
```


the first (or 0th) element of \f[B]a\f[R] is set to \f[B]1\f[R], and
\f[B]i\f[R] is equal to \f[B]2\f[R] at the end of the expression.

This includes function arguments.
Thus, assuming \f[B]i\f[R] is equal to \f[B]0\f[R], this means that in
the expression
.IP

```
\f[C]
x(i++, i++)
\f[R]
```


the first argument passed to \f[B]x()\f[R] is \f[B]0\f[R], and the
second argument is \f[B]1\f[R], while \f[B]i\f[R] is equal to
\f[B]2\f[R] before the function starts executing.

## FUNCTIONS


Function definitions are as follows:
.IP

```
\f[C]
define I(I,...,I){
    auto I,...,I
    S;...;S
    return(E)
}
\f[R]
```


Any \f[B]I\f[R] in the parameter list or \f[B]auto\f[R] list may be
replaced with \f[B]I[]\f[R] to make a parameter or \f[B]auto\f[R] var an
array, and any \f[B]I\f[R] in the parameter list may be replaced with
\f[B]*I[]\f[R] to make a parameter an array reference.
Callers of functions that take array references should not put an
asterisk in the call; they must be called with just \f[B]I[]\f[R] like
normal array parameters and will be automatically converted into
references.

As a \f[B]non-portable extension\f[R], the opening brace of a
\f[B]define\f[R] statement may appear on the next line.

As a \f[B]non-portable extension\f[R], the return statement may also be
in one of the following forms:

- 1.
\f[B]return\f[R]

- 2.
\f[B]return\f[R] \f[B](\f[R] \f[B])\f[R]

- 3.
\f[B]return\f[R] \f[B]E\f[R]

The first two, or not specifying a \f[B]return\f[R] statement, is
equivalent to \f[B]return (0)\f[R], unless the function is a
\f[B]void\f[R] function (see the \f[I]Void Functions\f[R] subsection
below).

### Void Functions


Functions can also be \f[B]void\f[R] functions, defined as follows:
.IP

```
\f[C]
define void I(I,...,I){
    auto I,...,I
    S;...;S
    return
}
\f[R]
```


They can only be used as standalone expressions, where such an
expression would be printed alone, except in a print statement.

Void functions can only use the first two \f[B]return\f[R] statements
listed above.
They can also omit the return statement entirely.

The word \[lq]void\[rq] is not treated as a keyword; it is still
possible to have variables, arrays, and functions named \f[B]void\f[R].
The word \[lq]void\[rq] is only treated specially right after the
\f[B]define\f[R] keyword.

This is a \f[B]non-portable extension\f[R].

### Array References


For any array in the parameter list, if the array is declared in the
form
.IP

```
\f[C]
*I[]
\f[R]
```


it is a \f[B]reference\f[R].
Any changes to the array in the function are reflected, when the
function returns, to the array that was passed in.

Other than this, all function arguments are passed by value.

This is a \f[B]non-portable extension\f[R].

## LIBRARY


All of the functions below, including the functions in the extended math
library (see the \f[I]Extended Library\f[R] subsection below), are
available when the \f[B]-l\f[R] or \f[B]--mathlib\f[R] command-line
flags are given, except that the extended math library is not available
when the \f[B]-s\f[R] option, the \f[B]-w\f[R] option, or equivalents
are given.

### Standard Library


The standard (see the \f[B]STANDARDS\f[R] section) defines the following
functions for the math library:

\f[B]s(x)\f[R]
Returns the sine of \f[B]x\f[R], which is assumed to be in radians.

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]c(x)\f[R]
Returns the cosine of \f[B]x\f[R], which is assumed to be in radians.

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]a(x)\f[R]
Returns the arctangent of \f[B]x\f[R], in radians.

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]l(x)\f[R]
Returns the natural logarithm of \f[B]x\f[R].

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]e(x)\f[R]
Returns the mathematical constant \f[B]e\f[R] raised to the power of
\f[B]x\f[R].

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]j(x, n)\f[R]
Returns the bessel integer order \f[B]n\f[R] (truncated) of \f[B]x\f[R].

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



### Extended Library


The extended library is \f[I]not\f[R] loaded when the
\f[B]-s\f[R]/\f[B]--standard\f[R] or \f[B]-w\f[R]/\f[B]--warn\f[R]
options are given since they are not part of the library defined by the
standard (see the \f[B]STANDARDS\f[R] section).

The extended library is a \f[B]non-portable extension\f[R].

\f[B]p(x, y)\f[R]
Calculates \f[B]x\f[R] to the power of \f[B]y\f[R], even if \f[B]y\f[R]
is not an integer, and returns the result to the current
\f[B]scale\f[R].

> 
It is an error if \f[B]y\f[R] is negative and \f[B]x\f[R] is
\f[B]0\f[R].

This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]r(x, p)\f[R]
Returns \f[B]x\f[R] rounded to \f[B]p\f[R] decimal places according to
the rounding mode round half away from \f[B]0\f[R]
(https://en.wikipedia.org/wiki/Rounding#Round_half_away_from_zero).

\f[B]ceil(x, p)\f[R]
Returns \f[B]x\f[R] rounded to \f[B]p\f[R] decimal places according to
the rounding mode round away from \f[B]0\f[R]
(https://en.wikipedia.org/wiki/Rounding#Rounding_away_from_zero).

\f[B]f(x)\f[R]
Returns the factorial of the truncated absolute value of \f[B]x\f[R].

\f[B]perm(n, k)\f[R]
Returns the permutation of the truncated absolute value of \f[B]n\f[R]
of the truncated absolute value of \f[B]k\f[R], if \f[B]k <= n\f[R].
If not, it returns \f[B]0\f[R].

\f[B]comb(n, k)\f[R]
Returns the combination of the truncated absolute value of \f[B]n\f[R]
of the truncated absolute value of \f[B]k\f[R], if \f[B]k <= n\f[R].
If not, it returns \f[B]0\f[R].

\f[B]l2(x)\f[R]
Returns the logarithm base \f[B]2\f[R] of \f[B]x\f[R].

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]l10(x)\f[R]
Returns the logarithm base \f[B]10\f[R] of \f[B]x\f[R].

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]log(x, b)\f[R]
Returns the logarithm base \f[B]b\f[R] of \f[B]x\f[R].

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]cbrt(x)\f[R]
Returns the cube root of \f[B]x\f[R].

\f[B]root(x, n)\f[R]
Calculates the truncated value of \f[B]n\f[R], \f[B]r\f[R], and returns
the \f[B]r\f[R]th root of \f[B]x\f[R] to the current \f[B]scale\f[R].

> 
If \f[B]r\f[R] is \f[B]0\f[R] or negative, this raises an error and
causes bc(1) to reset (see the \f[B]RESET\f[R] section).
It also raises an error and causes bc(1) to reset if \f[B]r\f[R] is even
and \f[B]x\f[R] is negative.



\f[B]gcd(a, b)\f[R]
Returns the greatest common divisor (factor) of the truncated absolute
value of \f[B]a\f[R] and the truncated absolute value of \f[B]b\f[R].

\f[B]lcm(a, b)\f[R]
Returns the least common multiple of the truncated absolute value of
\f[B]a\f[R] and the truncated absolute value of \f[B]b\f[R].

\f[B]pi(p)\f[R]
Returns \f[B]pi\f[R] to \f[B]p\f[R] decimal places.

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]t(x)\f[R]
Returns the tangent of \f[B]x\f[R], which is assumed to be in radians.

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]a2(y, x)\f[R]
Returns the arctangent of \f[B]y/x\f[R], in radians.
If both \f[B]y\f[R] and \f[B]x\f[R] are equal to \f[B]0\f[R], it raises
an error and causes bc(1) to reset (see the \f[B]RESET\f[R] section).
Otherwise, if \f[B]x\f[R] is greater than \f[B]0\f[R], it returns
\f[B]a(y/x)\f[R].
If \f[B]x\f[R] is less than \f[B]0\f[R], and \f[B]y\f[R] is greater than
or equal to \f[B]0\f[R], it returns \f[B]a(y/x)+pi\f[R].
If \f[B]x\f[R] is less than \f[B]0\f[R], and \f[B]y\f[R] is less than
\f[B]0\f[R], it returns \f[B]a(y/x)-pi\f[R].
If \f[B]x\f[R] is equal to \f[B]0\f[R], and \f[B]y\f[R] is greater than
\f[B]0\f[R], it returns \f[B]pi/2\f[R].
If \f[B]x\f[R] is equal to \f[B]0\f[R], and \f[B]y\f[R] is less than
\f[B]0\f[R], it returns \f[B]-pi/2\f[R].

> 
This function is the same as the \f[B]atan2()\f[R] function in many
programming languages.

This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]sin(x)\f[R]
Returns the sine of \f[B]x\f[R], which is assumed to be in radians.

> 
This is an alias of \f[B]s(x)\f[R].

This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]cos(x)\f[R]
Returns the cosine of \f[B]x\f[R], which is assumed to be in radians.

> 
This is an alias of \f[B]c(x)\f[R].

This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]tan(x)\f[R]
Returns the tangent of \f[B]x\f[R], which is assumed to be in radians.

> 
If \f[B]x\f[R] is equal to \f[B]1\f[R] or \f[B]-1\f[R], this raises an
error and causes bc(1) to reset (see the \f[B]RESET\f[R] section).

This is an alias of \f[B]t(x)\f[R].

This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]atan(x)\f[R]
Returns the arctangent of \f[B]x\f[R], in radians.

> 
This is an alias of \f[B]a(x)\f[R].

This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]atan2(y, x)\f[R]
Returns the arctangent of \f[B]y/x\f[R], in radians.
If both \f[B]y\f[R] and \f[B]x\f[R] are equal to \f[B]0\f[R], it raises
an error and causes bc(1) to reset (see the \f[B]RESET\f[R] section).
Otherwise, if \f[B]x\f[R] is greater than \f[B]0\f[R], it returns
\f[B]a(y/x)\f[R].
If \f[B]x\f[R] is less than \f[B]0\f[R], and \f[B]y\f[R] is greater than
or equal to \f[B]0\f[R], it returns \f[B]a(y/x)+pi\f[R].
If \f[B]x\f[R] is less than \f[B]0\f[R], and \f[B]y\f[R] is less than
\f[B]0\f[R], it returns \f[B]a(y/x)-pi\f[R].
If \f[B]x\f[R] is equal to \f[B]0\f[R], and \f[B]y\f[R] is greater than
\f[B]0\f[R], it returns \f[B]pi/2\f[R].
If \f[B]x\f[R] is equal to \f[B]0\f[R], and \f[B]y\f[R] is less than
\f[B]0\f[R], it returns \f[B]-pi/2\f[R].

> 
This function is the same as the \f[B]atan2()\f[R] function in many
programming languages.

This is an alias of \f[B]a2(y, x)\f[R].

This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]r2d(x)\f[R]
Converts \f[B]x\f[R] from radians to degrees and returns the result.

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]d2r(x)\f[R]
Converts \f[B]x\f[R] from degrees to radians and returns the result.

> 
This is a transcendental function (see the \f[I]Transcendental
Functions\f[R] subsection below).



\f[B]frand(p)\f[R]
Generates a pseudo-random number between \f[B]0\f[R] (inclusive) and
\f[B]1\f[R] (exclusive) with the number of decimal digits after the
decimal point equal to the truncated absolute value of \f[B]p\f[R].
If \f[B]p\f[R] is not \f[B]0\f[R], then calling this function will
change the value of \f[B]seed\f[R].
If \f[B]p\f[R] is \f[B]0\f[R], then \f[B]0\f[R] is returned, and
\f[B]seed\f[R] is \f[I]not\f[R] changed.

\f[B]ifrand(i, p)\f[R]
Generates a pseudo-random number that is between \f[B]0\f[R] (inclusive)
and the truncated absolute value of \f[B]i\f[R] (exclusive) with the
number of decimal digits after the decimal point equal to the truncated
absolute value of \f[B]p\f[R].
If the absolute value of \f[B]i\f[R] is greater than or equal to
\f[B]2\f[R], and \f[B]p\f[R] is not \f[B]0\f[R], then calling this
function will change the value of \f[B]seed\f[R]; otherwise, \f[B]0\f[R]
is returned and \f[B]seed\f[R] is not changed.

\f[B]srand(x)\f[R]
Returns \f[B]x\f[R] with its sign flipped with probability
\f[B]0.5\f[R].
In other words, it randomizes the sign of \f[B]x\f[R].

\f[B]brand()\f[R]
Returns a random boolean value (either \f[B]0\f[R] or \f[B]1\f[R]).

\f[B]band(a, b)\f[R]
Takes the truncated absolute value of both \f[B]a\f[R] and \f[B]b\f[R]
and calculates and returns the result of the bitwise \f[B]and\f[R]
operation between them.

> 
If you want to use signed two\[cq]s complement arguments, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bor(a, b)\f[R]
Takes the truncated absolute value of both \f[B]a\f[R] and \f[B]b\f[R]
and calculates and returns the result of the bitwise \f[B]or\f[R]
operation between them.

> 
If you want to use signed two\[cq]s complement arguments, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bxor(a, b)\f[R]
Takes the truncated absolute value of both \f[B]a\f[R] and \f[B]b\f[R]
and calculates and returns the result of the bitwise \f[B]xor\f[R]
operation between them.

> 
If you want to use signed two\[cq]s complement arguments, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bshl(a, b)\f[R]
Takes the truncated absolute value of both \f[B]a\f[R] and \f[B]b\f[R]
and calculates and returns the result of \f[B]a\f[R] bit-shifted left by
\f[B]b\f[R] places.

> 
If you want to use signed two\[cq]s complement arguments, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bshr(a, b)\f[R]
Takes the truncated absolute value of both \f[B]a\f[R] and \f[B]b\f[R]
and calculates and returns the truncated result of \f[B]a\f[R]
bit-shifted right by \f[B]b\f[R] places.

> 
If you want to use signed two\[cq]s complement arguments, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bnotn(x, n)\f[R]
Takes the truncated absolute value of \f[B]x\f[R] and does a bitwise not
as though it has the same number of bytes as the truncated absolute
value of \f[B]n\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bnot8(x)\f[R]
Does a bitwise not of the truncated absolute value of \f[B]x\f[R] as
though it has \f[B]8\f[R] binary digits (1 unsigned byte).

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bnot16(x)\f[R]
Does a bitwise not of the truncated absolute value of \f[B]x\f[R] as
though it has \f[B]16\f[R] binary digits (2 unsigned bytes).

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bnot32(x)\f[R]
Does a bitwise not of the truncated absolute value of \f[B]x\f[R] as
though it has \f[B]32\f[R] binary digits (4 unsigned bytes).

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bnot64(x)\f[R]
Does a bitwise not of the truncated absolute value of \f[B]x\f[R] as
though it has \f[B]64\f[R] binary digits (8 unsigned bytes).

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bnot(x)\f[R]
Does a bitwise not of the truncated absolute value of \f[B]x\f[R] as
though it has the minimum number of power of two unsigned bytes.

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brevn(x, n)\f[R]
Runs a bit reversal on the truncated absolute value of \f[B]x\f[R] as
though it has the same number of 8-bit bytes as the truncated absolute
value of \f[B]n\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brev8(x)\f[R]
Runs a bit reversal on the truncated absolute value of \f[B]x\f[R] as
though it has 8 binary digits (1 unsigned byte).

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brev16(x)\f[R]
Runs a bit reversal on the truncated absolute value of \f[B]x\f[R] as
though it has 16 binary digits (2 unsigned bytes).

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brev32(x)\f[R]
Runs a bit reversal on the truncated absolute value of \f[B]x\f[R] as
though it has 32 binary digits (4 unsigned bytes).

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brev64(x)\f[R]
Runs a bit reversal on the truncated absolute value of \f[B]x\f[R] as
though it has 64 binary digits (8 unsigned bytes).

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brev(x)\f[R]
Runs a bit reversal on the truncated absolute value of \f[B]x\f[R] as
though it has the minimum number of power of two unsigned bytes.

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]broln(x, p, n)\f[R]
Does a left bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has the same number of unsigned 8-bit bytes as
the truncated absolute value of \f[B]n\f[R], by the number of places
equal to the truncated absolute value of \f[B]p\f[R] modded by the
\f[B]2\f[R] to the power of the number of binary digits in \f[B]n\f[R]
8-bit bytes.

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brol8(x, p)\f[R]
Does a left bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has \f[B]8\f[R] binary digits (\f[B]1\f[R]
unsigned byte), by the number of places equal to the truncated absolute
value of \f[B]p\f[R] modded by \f[B]2\f[R] to the power of \f[B]8\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brol16(x, p)\f[R]
Does a left bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has \f[B]16\f[R] binary digits (\f[B]2\f[R]
unsigned bytes), by the number of places equal to the truncated absolute
value of \f[B]p\f[R] modded by \f[B]2\f[R] to the power of \f[B]16\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brol32(x, p)\f[R]
Does a left bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has \f[B]32\f[R] binary digits (\f[B]2\f[R]
unsigned bytes), by the number of places equal to the truncated absolute
value of \f[B]p\f[R] modded by \f[B]2\f[R] to the power of \f[B]32\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brol64(x, p)\f[R]
Does a left bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has \f[B]64\f[R] binary digits (\f[B]2\f[R]
unsigned bytes), by the number of places equal to the truncated absolute
value of \f[B]p\f[R] modded by \f[B]2\f[R] to the power of \f[B]64\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brol(x, p)\f[R]
Does a left bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has the minimum number of power of two
unsigned 8-bit bytes, by the number of places equal to the truncated
absolute value of \f[B]p\f[R] modded by 2 to the power of the number of
binary digits in the minimum number of 8-bit bytes.

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]brorn(x, p, n)\f[R]
Does a right bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has the same number of unsigned 8-bit bytes as
the truncated absolute value of \f[B]n\f[R], by the number of places
equal to the truncated absolute value of \f[B]p\f[R] modded by the
\f[B]2\f[R] to the power of the number of binary digits in \f[B]n\f[R]
8-bit bytes.

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bror8(x, p)\f[R]
Does a right bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has \f[B]8\f[R] binary digits (\f[B]1\f[R]
unsigned byte), by the number of places equal to the truncated absolute
value of \f[B]p\f[R] modded by \f[B]2\f[R] to the power of \f[B]8\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bror16(x, p)\f[R]
Does a right bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has \f[B]16\f[R] binary digits (\f[B]2\f[R]
unsigned bytes), by the number of places equal to the truncated absolute
value of \f[B]p\f[R] modded by \f[B]2\f[R] to the power of \f[B]16\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bror32(x, p)\f[R]
Does a right bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has \f[B]32\f[R] binary digits (\f[B]2\f[R]
unsigned bytes), by the number of places equal to the truncated absolute
value of \f[B]p\f[R] modded by \f[B]2\f[R] to the power of \f[B]32\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bror64(x, p)\f[R]
Does a right bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has \f[B]64\f[R] binary digits (\f[B]2\f[R]
unsigned bytes), by the number of places equal to the truncated absolute
value of \f[B]p\f[R] modded by \f[B]2\f[R] to the power of \f[B]64\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bror(x, p)\f[R]
Does a right bitwise rotatation of the truncated absolute value of
\f[B]x\f[R], as though it has the minimum number of power of two
unsigned 8-bit bytes, by the number of places equal to the truncated
absolute value of \f[B]p\f[R] modded by 2 to the power of the number of
binary digits in the minimum number of 8-bit bytes.

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bmodn(x, n)\f[R]
Returns the modulus of the truncated absolute value of \f[B]x\f[R] by
\f[B]2\f[R] to the power of the multiplication of the truncated absolute
value of \f[B]n\f[R] and \f[B]8\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bmod8(x, n)\f[R]
Returns the modulus of the truncated absolute value of \f[B]x\f[R] by
\f[B]2\f[R] to the power of \f[B]8\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bmod16(x, n)\f[R]
Returns the modulus of the truncated absolute value of \f[B]x\f[R] by
\f[B]2\f[R] to the power of \f[B]16\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bmod32(x, n)\f[R]
Returns the modulus of the truncated absolute value of \f[B]x\f[R] by
\f[B]2\f[R] to the power of \f[B]32\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bmod64(x, n)\f[R]
Returns the modulus of the truncated absolute value of \f[B]x\f[R] by
\f[B]2\f[R] to the power of \f[B]64\f[R].

> 
If you want to a use signed two\[cq]s complement argument, use
\f[B]s2u(x)\f[R] to convert.



\f[B]bunrev(t)\f[R]
Assumes \f[B]t\f[R] is a bitwise-reversed number with an extra set bit
one place more significant than the real most significant bit (which was
the least significant bit in the original number).
This number is reversed and returned without the extra set bit.

> 
This function is used to implement other bitwise functions; it is not
meant to be used by users, but it can be.



\f[B]plz(x)\f[R]
If \f[B]x\f[R] is not equal to \f[B]0\f[R] and greater that \f[B]-1\f[R]
and less than \f[B]1\f[R], it is printed with a leading zero, regardless
of the use of the \f[B]-z\f[R] option (see the \f[B]OPTIONS\f[R]
section) and without a trailing newline.

> 
Otherwise, \f[B]x\f[R] is printed normally, without a trailing newline.



\f[B]plznl(x)\f[R]
If \f[B]x\f[R] is not equal to \f[B]0\f[R] and greater that \f[B]-1\f[R]
and less than \f[B]1\f[R], it is printed with a leading zero, regardless
of the use of the \f[B]-z\f[R] option (see the \f[B]OPTIONS\f[R]
section) and with a trailing newline.

> 
Otherwise, \f[B]x\f[R] is printed normally, with a trailing newline.



\f[B]pnlz(x)\f[R]
If \f[B]x\f[R] is not equal to \f[B]0\f[R] and greater that \f[B]-1\f[R]
and less than \f[B]1\f[R], it is printed without a leading zero,
regardless of the use of the \f[B]-z\f[R] option (see the
\f[B]OPTIONS\f[R] section) and without a trailing newline.

> 
Otherwise, \f[B]x\f[R] is printed normally, without a trailing newline.



\f[B]pnlznl(x)\f[R]
If \f[B]x\f[R] is not equal to \f[B]0\f[R] and greater that \f[B]-1\f[R]
and less than \f[B]1\f[R], it is printed without a leading zero,
regardless of the use of the \f[B]-z\f[R] option (see the
\f[B]OPTIONS\f[R] section) and with a trailing newline.

> 
Otherwise, \f[B]x\f[R] is printed normally, with a trailing newline.



\f[B]ubytes(x)\f[R]
Returns the numbers of unsigned integer bytes required to hold the
truncated absolute value of \f[B]x\f[R].

\f[B]sbytes(x)\f[R]
Returns the numbers of signed, two\[cq]s-complement integer bytes
required to hold the truncated value of \f[B]x\f[R].

\f[B]s2u(x)\f[R]
Returns \f[B]x\f[R] if it is non-negative.
If it \f[I]is\f[R] negative, then it calculates what \f[B]x\f[R] would
be as a 2\[cq]s-complement signed integer and returns the non-negative
integer that would have the same representation in binary.

\f[B]s2un(x,n)\f[R]
Returns \f[B]x\f[R] if it is non-negative.
If it \f[I]is\f[R] negative, then it calculates what \f[B]x\f[R] would
be as a 2\[cq]s-complement signed integer with \f[B]n\f[R] bytes and
returns the non-negative integer that would have the same representation
in binary.
If \f[B]x\f[R] cannot fit into \f[B]n\f[R] 2\[cq]s-complement signed
bytes, it is truncated to fit.

\f[B]hex(x)\f[R]
Outputs the hexadecimal (base \f[B]16\f[R]) representation of
\f[B]x\f[R].

> 
This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]binary(x)\f[R]
Outputs the binary (base \f[B]2\f[R]) representation of \f[B]x\f[R].

> 
This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]output(x, b)\f[R]
Outputs the base \f[B]b\f[R] representation of \f[B]x\f[R].

> 
This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]uint(x)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
an unsigned integer in as few power of two bytes as possible.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer or is negative, an error message is
printed instead, but bc(1) is not reset (see the \f[B]RESET\f[R]
section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]int(x)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
a signed, two\[cq]s-complement integer in as few power of two bytes as
possible.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer, an error message is printed instead,
but bc(1) is not reset (see the \f[B]RESET\f[R] section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]uintn(x, n)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
an unsigned integer in \f[B]n\f[R] bytes.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer, is negative, or cannot fit into
\f[B]n\f[R] bytes, an error message is printed instead, but bc(1) is not
reset (see the \f[B]RESET\f[R] section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]intn(x, n)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
a signed, two\[cq]s-complement integer in \f[B]n\f[R] bytes.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer or cannot fit into \f[B]n\f[R] bytes,
an error message is printed instead, but bc(1) is not reset (see the
\f[B]RESET\f[R] section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]uint8(x)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
an unsigned integer in \f[B]1\f[R] byte.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer, is negative, or cannot fit into
\f[B]1\f[R] byte, an error message is printed instead, but bc(1) is not
reset (see the \f[B]RESET\f[R] section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]int8(x)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
a signed, two\[cq]s-complement integer in \f[B]1\f[R] byte.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer or cannot fit into \f[B]1\f[R] byte, an
error message is printed instead, but bc(1) is not reset (see the
\f[B]RESET\f[R] section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]uint16(x)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
an unsigned integer in \f[B]2\f[R] bytes.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer, is negative, or cannot fit into
\f[B]2\f[R] bytes, an error message is printed instead, but bc(1) is not
reset (see the \f[B]RESET\f[R] section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]int16(x)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
a signed, two\[cq]s-complement integer in \f[B]2\f[R] bytes.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer or cannot fit into \f[B]2\f[R] bytes,
an error message is printed instead, but bc(1) is not reset (see the
\f[B]RESET\f[R] section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]uint32(x)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
an unsigned integer in \f[B]4\f[R] bytes.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer, is negative, or cannot fit into
\f[B]4\f[R] bytes, an error message is printed instead, but bc(1) is not
reset (see the \f[B]RESET\f[R] section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]int32(x)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
a signed, two\[cq]s-complement integer in \f[B]4\f[R] bytes.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer or cannot fit into \f[B]4\f[R] bytes,
an error message is printed instead, but bc(1) is not reset (see the
\f[B]RESET\f[R] section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]uint64(x)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
an unsigned integer in \f[B]8\f[R] bytes.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer, is negative, or cannot fit into
\f[B]8\f[R] bytes, an error message is printed instead, but bc(1) is not
reset (see the \f[B]RESET\f[R] section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]int64(x)\f[R]
Outputs the representation, in binary and hexadecimal, of \f[B]x\f[R] as
a signed, two\[cq]s-complement integer in \f[B]8\f[R] bytes.
Both outputs are split into bytes separated by spaces.

> 
If \f[B]x\f[R] is not an integer or cannot fit into \f[B]8\f[R] bytes,
an error message is printed instead, but bc(1) is not reset (see the
\f[B]RESET\f[R] section).

This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]hex_uint(x, n)\f[R]
Outputs the representation of the truncated absolute value of
\f[B]x\f[R] as an unsigned integer in hexadecimal using \f[B]n\f[R]
bytes.
Not all of the value will be output if \f[B]n\f[R] is too small.

> 
This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]binary_uint(x, n)\f[R]
Outputs the representation of the truncated absolute value of
\f[B]x\f[R] as an unsigned integer in binary using \f[B]n\f[R] bytes.
Not all of the value will be output if \f[B]n\f[R] is too small.

> 
This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]output_uint(x, n)\f[R]
Outputs the representation of the truncated absolute value of
\f[B]x\f[R] as an unsigned integer in the current \f[B]obase\f[R] (see
the \f[B]SYNTAX\f[R] section) using \f[B]n\f[R] bytes.
Not all of the value will be output if \f[B]n\f[R] is too small.

> 
This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



\f[B]output_byte(x, i)\f[R]
Outputs byte \f[B]i\f[R] of the truncated absolute value of \f[B]x\f[R],
where \f[B]0\f[R] is the least significant byte and \f[B]number_of_bytes
- 1\f[R] is the most significant byte.

> 
This is a \f[B]void\f[R] function (see the \f[I]Void Functions\f[R]
subsection of the \f[B]FUNCTIONS\f[R] section).



### Transcendental Functions


All transcendental functions can return slightly inaccurate results, up
to 1 ULP (https://en.wikipedia.org/wiki/Unit_in_the_last_place).
This is unavoidable, and the article at
https://people.eecs.berkeley.edu/\[ti]wkahan/LOG10HAF.TXT explains why
it is impossible and unnecessary to calculate exact results for the
transcendental functions.

Because of the possible inaccuracy, I recommend that users call those
functions with the precision (\f[B]scale\f[R]) set to at least 1 higher
than is necessary.
If exact results are \f[I]absolutely\f[R] required, users can double the
precision (\f[B]scale\f[R]) and then truncate.

The transcendental functions in the standard math library are:

- \[bu]
\f[B]s(x)\f[R]

- \[bu]
\f[B]c(x)\f[R]

- \[bu]
\f[B]a(x)\f[R]

- \[bu]
\f[B]l(x)\f[R]

- \[bu]
\f[B]e(x)\f[R]

- \[bu]
\f[B]j(x, n)\f[R]

The transcendental functions in the extended math library are:

- \[bu]
\f[B]l2(x)\f[R]

- \[bu]
\f[B]l10(x)\f[R]

- \[bu]
\f[B]log(x, b)\f[R]

- \[bu]
\f[B]pi(p)\f[R]

- \[bu]
\f[B]t(x)\f[R]

- \[bu]
\f[B]a2(y, x)\f[R]

- \[bu]
\f[B]sin(x)\f[R]

- \[bu]
\f[B]cos(x)\f[R]

- \[bu]
\f[B]tan(x)\f[R]

- \[bu]
\f[B]atan(x)\f[R]

- \[bu]
\f[B]atan2(y, x)\f[R]

- \[bu]
\f[B]r2d(x)\f[R]

- \[bu]
\f[B]d2r(x)\f[R]

## RESET


When bc(1) encounters an error or a signal that it has a non-default
handler for, it resets.
This means that several things happen.

First, any functions that are executing are stopped and popped off the
stack.
The behavior is not unlike that of exceptions in programming languages.
Then the execution point is set so that any code waiting to execute
(after all functions returned) is skipped.

Thus, when bc(1) resets, it skips any remaining code waiting to be
executed.
Then, if it is interactive mode, and the error was not a fatal error
(see the \f[B]EXIT STATUS\f[R] section), it asks for more input;
otherwise, it exits with the appropriate return code.

Note that this reset behavior is different from the GNU bc(1), which
attempts to start executing the statement right after the one that
caused an error.

## PERFORMANCE


Most bc(1) implementations use \f[B]char\f[R] types to calculate the
value of \f[B]1\f[R] decimal digit at a time, but that can be slow.
This bc(1) does something different.

It uses large integers to calculate more than \f[B]1\f[R] decimal digit
at a time.
If built in a environment where \f[B]BC_LONG_BIT\f[R] (see the
\f[B]LIMITS\f[R] section) is \f[B]64\f[R], then each integer has
\f[B]9\f[R] decimal digits.
If built in an environment where \f[B]BC_LONG_BIT\f[R] is \f[B]32\f[R]
then each integer has \f[B]4\f[R] decimal digits.
This value (the number of decimal digits per large integer) is called
\f[B]BC_BASE_DIGS\f[R].

The actual values of \f[B]BC_LONG_BIT\f[R] and \f[B]BC_BASE_DIGS\f[R]
can be queried with the \f[B]limits\f[R] statement.

In addition, this bc(1) uses an even larger integer for overflow
checking.
This integer type depends on the value of \f[B]BC_LONG_BIT\f[R], but is
always at least twice as large as the integer type used to store digits.

## LIMITS


The following are the limits on bc(1):

\f[B]BC_LONG_BIT\f[R]
The number of bits in the \f[B]long\f[R] type in the environment where
bc(1) was built.
This determines how many decimal digits can be stored in a single large
integer (see the \f[B]PERFORMANCE\f[R] section).

\f[B]BC_BASE_DIGS\f[R]
The number of decimal digits per large integer (see the
\f[B]PERFORMANCE\f[R] section).
Depends on \f[B]BC_LONG_BIT\f[R].

\f[B]BC_BASE_POW\f[R]
The max decimal number that each large integer can store (see
\f[B]BC_BASE_DIGS\f[R]) plus \f[B]1\f[R].
Depends on \f[B]BC_BASE_DIGS\f[R].

\f[B]BC_OVERFLOW_MAX\f[R]
The max number that the overflow type (see the \f[B]PERFORMANCE\f[R]
section) can hold.
Depends on \f[B]BC_LONG_BIT\f[R].

\f[B]BC_BASE_MAX\f[R]
The maximum output base.
Set at \f[B]BC_BASE_POW\f[R].

\f[B]BC_DIM_MAX\f[R]
The maximum size of arrays.
Set at \f[B]SIZE_MAX-1\f[R].

\f[B]BC_SCALE_MAX\f[R]
The maximum \f[B]scale\f[R].
Set at \f[B]BC_OVERFLOW_MAX-1\f[R].

\f[B]BC_STRING_MAX\f[R]
The maximum length of strings.
Set at \f[B]BC_OVERFLOW_MAX-1\f[R].

\f[B]BC_NAME_MAX\f[R]
The maximum length of identifiers.
Set at \f[B]BC_OVERFLOW_MAX-1\f[R].

\f[B]BC_NUM_MAX\f[R]
The maximum length of a number (in decimal digits), which includes
digits after the decimal point.
Set at \f[B]BC_OVERFLOW_MAX-1\f[R].

\f[B]BC_RAND_MAX\f[R]
The maximum integer (inclusive) returned by the \f[B]rand()\f[R]
operand.
Set at \f[B]2\[ha]BC_LONG_BIT-1\f[R].

Exponent
The maximum allowable exponent (positive or negative).
Set at \f[B]BC_OVERFLOW_MAX\f[R].

Number of vars
The maximum number of vars/arrays.
Set at \f[B]SIZE_MAX-1\f[R].

The actual values can be queried with the \f[B]limits\f[R] statement.

These limits are meant to be effectively non-existent; the limits are so
large (at least on 64-bit machines) that there should not be any point
at which they become a problem.
In fact, memory should be exhausted before these limits should be hit.

## ENVIRONMENT VARIABLES


As \f[B]non-portable extensions\f[R], bc(1) recognizes the following
environment variables:

\f[B]POSIXLY_CORRECT\f[R]
If this variable exists (no matter the contents), bc(1) behaves as if
the \f[B]-s\f[R] option was given.

\f[B]BC_ENV_ARGS\f[R]
This is another way to give command-line arguments to bc(1).
They should be in the same format as all other command-line arguments.
These are always processed first, so any files given in
\f[B]BC_ENV_ARGS\f[R] will be processed before arguments and files given
on the command-line.
This gives the user the ability to set up \[lq]standard\[rq] options and
files to be used at every invocation.
The most useful thing for such files to contain would be useful
functions that the user might want every time bc(1) runs.

> 
The code that parses \f[B]BC_ENV_ARGS\f[R] will correctly handle quoted
arguments, but it does not understand escape sequences.
For example, the string \f[B]\[lq]/home/gavin/some bc file.bc\[rq]\f[R]
will be correctly parsed, but the string \f[B]\[lq]/home/gavin/some
\[dq]bc\[dq] file.bc\[rq]\f[R] will include the backslashes.

The quote parsing will handle either kind of quotes, \f[B]\[cq]\f[R] or
\f[B]\[lq]\f[R].
Thus, if you have a file with any number of single quotes in the name,
you can use double quotes as the outside quotes, as in \f[B]\[lq]some
`bc' file.bc\[rq]\f[R], and vice versa if you have a file with double
quotes.
However, handling a file with both kinds of quotes in
\f[B]BC_ENV_ARGS\f[R] is not supported due to the complexity of the
parsing, though such files are still supported on the command-line where
the parsing is done by the shell.



\f[B]BC_LINE_LENGTH\f[R]
If this environment variable exists and contains an integer that is
greater than \f[B]1\f[R] and is less than \f[B]UINT16_MAX\f[R]
(\f[B]2\[ha]16-1\f[R]), bc(1) will output lines to that length,
including the backslash (\f[B]\[rs]\f[R]).
The default line length is \f[B]70\f[R].

> 
The special value of \f[B]0\f[R] will disable line length checking and
print numbers without regard to line length and without backslashes and
newlines.



\f[B]BC_BANNER\f[R]
If this environment variable exists and contains an integer, then a
non-zero value activates the copyright banner when bc(1) is in
interactive mode, while zero deactivates it.

> 
If bc(1) is not in interactive mode (see the \f[B]INTERACTIVE MODE\f[R]
section), then this environment variable has no effect because bc(1)
does not print the banner when not in interactive mode.

This environment variable overrides the default, which can be queried
with the \f[B]-h\f[R] or \f[B]--help\f[R] options.



\f[B]BC_SIGINT_RESET\f[R]
If bc(1) is not in interactive mode (see the \f[B]INTERACTIVE MODE\f[R]
section), then this environment variable has no effect because bc(1)
exits on \f[B]SIGINT\f[R] when not in interactive mode.

> 
However, when bc(1) is in interactive mode, then if this environment
variable exists and contains an integer, a non-zero value makes bc(1)
reset on \f[B]SIGINT\f[R], rather than exit, and zero makes bc(1) exit.
If this environment variable exists and is \f[I]not\f[R] an integer,
then bc(1) will exit on \f[B]SIGINT\f[R].

This environment variable overrides the default, which can be queried
with the \f[B]-h\f[R] or \f[B]--help\f[R] options.



\f[B]BC_TTY_MODE\f[R]
If TTY mode is \f[I]not\f[R] available (see the \f[B]TTY MODE\f[R]
section), then this environment variable has no effect.

> 
However, when TTY mode is available, then if this environment variable
exists and contains an integer, then a non-zero value makes bc(1) use
TTY mode, and zero makes bc(1) not use TTY mode.

This environment variable overrides the default, which can be queried
with the \f[B]-h\f[R] or \f[B]--help\f[R] options.



\f[B]BC_PROMPT\f[R]
If TTY mode is \f[I]not\f[R] available (see the \f[B]TTY MODE\f[R]
section), then this environment variable has no effect.

> 
However, when TTY mode is available, then if this environment variable
exists and contains an integer, a non-zero value makes bc(1) use a
prompt, and zero or a non-integer makes bc(1) not use a prompt.
If this environment variable does not exist and \f[B]BC_TTY_MODE\f[R]
does, then the value of the \f[B]BC_TTY_MODE\f[R] environment variable
is used.

This environment variable and the \f[B]BC_TTY_MODE\f[R] environment
variable override the default, which can be queried with the
\f[B]-h\f[R] or \f[B]--help\f[R] options.



\f[B]BC_EXPR_EXIT\f[R]
If any expressions or expression files are given on the command-line
with \f[B]-e\f[R], \f[B]--expression\f[R], \f[B]-f\f[R], or
\f[B]--file\f[R], then if this environment variable exists and contains
an integer, a non-zero value makes bc(1) exit after executing the
expressions and expression files, and a zero value makes bc(1) not exit.

> 
This environment variable overrides the default, which can be queried
with the \f[B]-h\f[R] or \f[B]--help\f[R] options.



\f[B]BC_DIGIT_CLAMP\f[R]
When parsing numbers and if this environment variable exists and
contains an integer, a non-zero value makes bc(1) clamp digits that are
greater than or equal to the current \f[B]ibase\f[R] so that all such
digits are considered equal to the \f[B]ibase\f[R] minus 1, and a zero
value disables such clamping so that those digits are always equal to
their value, which is multiplied by the power of the \f[B]ibase\f[R].

> 
This never applies to single-digit numbers, as per the standard (see the
\f[B]STANDARDS\f[R] section).

This environment variable overrides the default, which can be queried
with the \f[B]-h\f[R] or \f[B]--help\f[R] options.



## EXIT STATUS


bc(1) returns the following exit statuses:

\f[B]0\f[R]
No error.

\f[B]1\f[R]
A math error occurred.
This follows standard practice of using \f[B]1\f[R] for expected errors,
since math errors will happen in the process of normal execution.

> 
Math errors include divide by \f[B]0\f[R], taking the square root of a
negative number, using a negative number as a bound for the
pseudo-random number generator, attempting to convert a negative number
to a hardware integer, overflow when converting a number to a hardware
integer, overflow when calculating the size of a number, and attempting
to use a non-integer where an integer is required.

Converting to a hardware integer happens for the second operand of the
power (\f[B]\[ha]\f[R]), places (\f[B]\[at]\f[R]), left shift
(\f[B]<<\f[R]), and right shift (\f[B]>>\f[R]) operators and their
corresponding assignment operators.



\f[B]2\f[R]
A parse error occurred.

> 
Parse errors include unexpected \f[B]EOF\f[R], using an invalid
character, failing to find the end of a string or comment, using a token
where it is invalid, giving an invalid expression, giving an invalid
print statement, giving an invalid function definition, attempting to
assign to an expression that is not a named expression (see the
\f[I]Named Expressions\f[R] subsection of the \f[B]SYNTAX\f[R] section),
giving an invalid \f[B]auto\f[R] list, having a duplicate
\f[B]auto\f[R]/function parameter, failing to find the end of a code
block, attempting to return a value from a \f[B]void\f[R] function,
attempting to use a variable as a reference, and using any extensions
when the option \f[B]-s\f[R] or any equivalents were given.



\f[B]3\f[R]
A runtime error occurred.

> 
Runtime errors include assigning an invalid number to any global
(\f[B]ibase\f[R], \f[B]obase\f[R], or \f[B]scale\f[R]), giving a bad
expression to a \f[B]read()\f[R] call, calling \f[B]read()\f[R] inside
of a \f[B]read()\f[R] call, type errors, passing the wrong number of
arguments to functions, attempting to call an undefined function, and
attempting to use a \f[B]void\f[R] function call as a value in an
expression.



\f[B]4\f[R]
A fatal error occurred.

> 
Fatal errors include memory allocation errors, I/O errors, failing to
open files, attempting to use files that do not have only ASCII
characters (bc(1) only accepts ASCII characters), attempting to open a
directory as a file, and giving invalid command-line options.



The exit status \f[B]4\f[R] is special; when a fatal error occurs, bc(1)
always exits and returns \f[B]4\f[R], no matter what mode bc(1) is in.

The other statuses will only be returned when bc(1) is not in
interactive mode (see the \f[B]INTERACTIVE MODE\f[R] section), since
bc(1) resets its state (see the \f[B]RESET\f[R] section) and accepts
more input when one of those errors occurs in interactive mode.
This is also the case when interactive mode is forced by the
\f[B]-i\f[R] flag or \f[B]--interactive\f[R] option.

These exit statuses allow bc(1) to be used in shell scripting with error
checking, and its normal behavior can be forced by using the
\f[B]-i\f[R] flag or \f[B]--interactive\f[R] option.

## INTERACTIVE MODE


Per the standard (see the \f[B]STANDARDS\f[R] section), bc(1) has an
interactive mode and a non-interactive mode.
Interactive mode is turned on automatically when both \f[B]stdin\f[R]
and \f[B]stdout\f[R] are hooked to a terminal, but the \f[B]-i\f[R] flag
and \f[B]--interactive\f[R] option can turn it on in other situations.

In interactive mode, bc(1) attempts to recover from errors (see the
\f[B]RESET\f[R] section), and in normal execution, flushes
\f[B]stdout\f[R] as soon as execution is done for the current input.
bc(1) may also reset on \f[B]SIGINT\f[R] instead of exit, depending on
the contents of, or default for, the \f[B]BC_SIGINT_RESET\f[R]
environment variable (see the \f[B]ENVIRONMENT VARIABLES\f[R] section).

## TTY MODE


If \f[B]stdin\f[R], \f[B]stdout\f[R], and \f[B]stderr\f[R] are all
connected to a TTY, then \[lq]TTY mode\[rq] is considered to be
available, and thus, bc(1) can turn on TTY mode, subject to some
settings.

If there is the environment variable \f[B]BC_TTY_MODE\f[R] in the
environment (see the \f[B]ENVIRONMENT VARIABLES\f[R] section), then if
that environment variable contains a non-zero integer, bc(1) will turn
on TTY mode when \f[B]stdin\f[R], \f[B]stdout\f[R], and \f[B]stderr\f[R]
are all connected to a TTY.
If the \f[B]BC_TTY_MODE\f[R] environment variable exists but is
\f[I]not\f[R] a non-zero integer, then bc(1) will not turn TTY mode on.

If the environment variable \f[B]BC_TTY_MODE\f[R] does \f[I]not\f[R]
exist, the default setting is used.
The default setting can be queried with the \f[B]-h\f[R] or
\f[B]--help\f[R] options.

TTY mode is different from interactive mode because interactive mode is
required in the bc(1) standard (see the \f[B]STANDARDS\f[R] section),
and interactive mode requires only \f[B]stdin\f[R] and \f[B]stdout\f[R]
to be connected to a terminal.

### Command-Line History


Command-line history is only enabled if TTY mode is, i.e., that
\f[B]stdin\f[R], \f[B]stdout\f[R], and \f[B]stderr\f[R] are connected to
a TTY and the \f[B]BC_TTY_MODE\f[R] environment variable (see the
\f[B]ENVIRONMENT VARIABLES\f[R] section) and its default do not disable
TTY mode.
See the \f[B]COMMAND LINE HISTORY\f[R] section for more information.

### Prompt


If TTY mode is available, then a prompt can be enabled.
Like TTY mode itself, it can be turned on or off with an environment
variable: \f[B]BC_PROMPT\f[R] (see the \f[B]ENVIRONMENT VARIABLES\f[R]
section).

If the environment variable \f[B]BC_PROMPT\f[R] exists and is a non-zero
integer, then the prompt is turned on when \f[B]stdin\f[R],
\f[B]stdout\f[R], and \f[B]stderr\f[R] are connected to a TTY and the
\f[B]-P\f[R] and \f[B]--no-prompt\f[R] options were not used.
The read prompt will be turned on under the same conditions, except that
the \f[B]-R\f[R] and \f[B]--no-read-prompt\f[R] options must also not be
used.

However, if \f[B]BC_PROMPT\f[R] does not exist, the prompt can be
enabled or disabled with the \f[B]BC_TTY_MODE\f[R] environment variable,
the \f[B]-P\f[R] and \f[B]--no-prompt\f[R] options, and the \f[B]-R\f[R]
and \f[B]--no-read-prompt\f[R] options.
See the \f[B]ENVIRONMENT VARIABLES\f[R] and \f[B]OPTIONS\f[R] sections
for more details.

## SIGNAL HANDLING


Sending a \f[B]SIGINT\f[R] will cause bc(1) to do one of two things.

If bc(1) is not in interactive mode (see the \f[B]INTERACTIVE MODE\f[R]
section), or the \f[B]BC_SIGINT_RESET\f[R] environment variable (see the
\f[B]ENVIRONMENT VARIABLES\f[R] section), or its default, is either not
an integer or it is zero, bc(1) will exit.

However, if bc(1) is in interactive mode, and the
\f[B]BC_SIGINT_RESET\f[R] or its default is an integer and non-zero,
then bc(1) will stop executing the current input and reset (see the
\f[B]RESET\f[R] section) upon receiving a \f[B]SIGINT\f[R].

Note that \[lq]current input\[rq] can mean one of two things.
If bc(1) is processing input from \f[B]stdin\f[R] in interactive mode,
it will ask for more input.
If bc(1) is processing input from a file in interactive mode, it will
stop processing the file and start processing the next file, if one
exists, or ask for input from \f[B]stdin\f[R] if no other file exists.

This means that if a \f[B]SIGINT\f[R] is sent to bc(1) as it is
executing a file, it can seem as though bc(1) did not respond to the
signal since it will immediately start executing the next file.
This is by design; most files that users execute when interacting with
bc(1) have function definitions, which are quick to parse.
If a file takes a long time to execute, there may be a bug in that file.
The rest of the files could still be executed without problem, allowing
the user to continue.

\f[B]SIGTERM\f[R] and \f[B]SIGQUIT\f[R] cause bc(1) to clean up and
exit, and it uses the default handler for all other signals.
The one exception is \f[B]SIGHUP\f[R]; in that case, and only when bc(1)
is in TTY mode (see the \f[B]TTY MODE\f[R] section), a \f[B]SIGHUP\f[R]
will cause bc(1) to clean up and exit.

## COMMAND LINE HISTORY


bc(1) supports interactive command-line editing.

If bc(1) can be in TTY mode (see the \f[B]TTY MODE\f[R] section),
history can be enabled.
This means that command-line history can only be enabled when
\f[B]stdin\f[R], \f[B]stdout\f[R], and \f[B]stderr\f[R] are all
connected to a TTY.

Like TTY mode itself, it can be turned on or off with the environment
variable \f[B]BC_TTY_MODE\f[R] (see the \f[B]ENVIRONMENT VARIABLES\f[R]
section).

If history is enabled, previous lines can be recalled and edited with
the arrow keys.

\f[B]Note\f[R]: tabs are converted to 8 spaces.

## LOCALES


This bc(1) ships with support for adding error messages for different
locales and thus, supports \f[B]LC_MESSAGES\f[R].

## SEE ALSO


dc(1)

## STANDARDS


bc(1) is compliant with the IEEE Std 1003.1-2017
(\[lq]POSIX.1-2017\[rq]) specification at
https://pubs.opengroup.org/onlinepubs/9699919799/utilities/bc.html .
The flags \f[B]-efghiqsvVw\f[R], all long options, and the extensions
noted above are extensions to that specification.

In addition, the behavior of the \f[B]quit\f[R] implements an
interpretation of that specification that is different from all known
implementations.
For more information see the \f[B]Statements\f[R] subsection of the
\f[B]SYNTAX\f[R] section.

Note that the specification explicitly says that bc(1) only accepts
numbers that use a period (\f[B].\f[R]) as a radix point, regardless of
the value of \f[B]LC_NUMERIC\f[R].

This bc(1) supports error messages for different locales, and thus, it
supports \f[B]LC_MESSAGES\f[R].

## BUGS


Before version \f[B]6.1.0\f[R], this bc(1) had incorrect behavior for
the \f[B]quit\f[R] statement.

No other bugs are known.
Report bugs at https://git.gavinhoward.com/gavin/bc .

## AUTHORS


Gavin D.
Howard <gavin@gavinhoward.com> and contributors.
