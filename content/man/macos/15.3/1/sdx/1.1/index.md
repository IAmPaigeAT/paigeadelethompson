+++
operating_system = "macos"
manpage_name = "sdx"
description = "sdx combines a number of functions into a single command-line developer utility. Its most common use is to create, browse, and unravel Starkits: sdx qwrap myscript.tcl ?options ...? create a starkit from a single Tcl script sdx wrap mystar.kit ?op..."
detected_package_version = "1.1"
author = "None Specified"
operating_system_version = "15.3"
title = "sdx(1)"
manpage_format = "troff"
date = "Sun Feb 16 04:48:19 2025"
manpage_section = "1"
keywords = ["fihttp", "code", "google", "com", "p", "tclkit", "wiki", "tcl", "tk", "sdx", "starkit", "www", "equi4", "html", "keywords"]
+++

"sdx" 1 2.0  "sdx"

---

## NAME

sdx - Starkit Developer eXtension

## DESCRIPTION


**sdx** combines a number of functions into a single command-line developer
utility. Its most common use is to create, browse, and unravel Starkits:

**sdx** **qwrap** *myscript.tcl* ?*options* ...?
create a starkit from a single Tcl script

**sdx** **wrap** *mystar.kit* ?*options* ...?
create a starkit from a mystar.vfs directory structure

**sdx** **unwrap** *mystar.kit*
the reverse of wrap, lets you dissect any starkit

**sdx** **lsk** *mystar.kit*
list the contents of a starkit, as seen when mounted in Tcl

**sdx** **version** *mystar.kit*
calculate the version ID of a starkit, report newest file found inside

**sdx** **mkpack** *oldstar.kit* *newstar.kit*
copy and optimally pack the Metakit data by removing all unused areas

**sdx** **mksplit** *mystar.kit*
split a starkit into mystar.head and a mystar.tail parts

**sdx** also has other, less frequently used commands, see
**sdx** **help** and **sdx** **help** *command* for more
information.

**sdx** is itself a Starkit, you can inspect its contents by doing
**sdx** **unwrap** **`which sdx`**.

## SEE ALSO

*http://code.google.com/p/tclkit/*, *http://wiki.tcl.tk/sdx/*, *http://wiki.tcl.tk/starkit/*, *http://www.equi4.com/starkit/*, *http://www.equi4.com/starkit/sdx.html*

## KEYWORDS

Starkit, Tcl
