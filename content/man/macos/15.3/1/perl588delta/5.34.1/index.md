+++
operating_system = "macos"
author = "None Specified"
operating_system_version = "15.3"
title = "perl588delta(1)"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "how", "to", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
description = "This document describes differences between the 5.8.7 release and the 5.8.8 release. There are no changes intentionally incompatible with 5.8.7. If any exist, they are bugs and reports are welcome. f(CW*(C`chdir*(C, f(CW*(C`chmod*(C and f(CW*(C`..."
manpage_section = "1"
date = "2022-02-19"
manpage_format = "troff"
manpage_name = "perl588delta"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL588DELTA 1"
PERL588DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl588delta - what is new for perl v5.8.8

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.8.7 release and
the 5.8.8 release.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.8.7. If any exist,
they are bugs and reports are welcome.

## Core Enhancements

Header "Core Enhancements"

- \(bu
\f(CW\*(C`chdir\*(C', \f(CW\*(C`chmod\*(C' and \f(CW\*(C`chown\*(C' can now work on filehandles as well as
filenames, if the system supports respectively \f(CW\*(C`fchdir\*(C', \f(CW\*(C`fchmod\*(C' and
\f(CW\*(C`fchown\*(C', thanks to a patch provided by Gisle Aas.

## Modules and Pragmata

Header "Modules and Pragmata"

- \(bu
\f(CW\*(C`Attribute::Handlers\*(C' upgraded to version 0.78_02

> 
- \(bu
Documentation typo fix



> 


- \(bu
\f(CW\*(C`attrs\*(C' upgraded to version 1.02

> 
- \(bu
Internal cleanup only



> 


- \(bu
\f(CW\*(C`autouse\*(C' upgraded to version 1.05

> 
- \(bu
Simplified implementation



> 


- \(bu
\f(CW\*(C`B\*(C' upgraded to version 1.09_01

> 
- \(bu
The inheritance hierarchy of the \f(CW\*(C`B::\*(C' modules has been corrected;
\f(CW\*(C`B::NV\*(C' now inherits from \f(CW\*(C`B::SV\*(C' (instead of \f(CW\*(C`B::IV\*(C').



> 


- \(bu
\f(CW\*(C`blib\*(C' upgraded to version 1.03

> 
- \(bu
Documentation typo fix



> 


- \(bu
\f(CW\*(C`ByteLoader\*(C' upgraded to version 0.06

> 
- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`CGI\*(C' upgraded to version 3.15

> 
- \(bu
Extraneous \*(L"?\*(R" from \f(CW\*(C`self_url()\*(C' removed

- \(bu
\f(CW\*(C`scrolling_list()\*(C' select attribute fixed

- \(bu
\f(CW\*(C`virtual_port\*(C' now works properly with the https protocol

- \(bu
\f(CW\*(C`upload_hook()\*(C' and \f(CW\*(C`append()\*(C' now works in function-oriented mode

- \(bu
\f(CW\*(C`POST_MAX\*(C' doesn't cause the client to hang any more

- \(bu
Automatic tab indexes are now disabled and new \f(CW\*(C`-tabindex\*(C' pragma has
been added to turn automatic indexes back on

- \(bu
\f(CW\*(C`end_form()\*(C' doesn't emit empty (and non-validating) \f(CW\*(C`<div>\*(C'

- \(bu
\f(CW\*(C`CGI::Carp\*(C' works better in certain mod_perl configurations

- \(bu
Setting \f(CW$CGI::TMPDIRECTORY is now effective

- \(bu
Enhanced documentation



> 


- \(bu
\f(CW\*(C`charnames\*(C' upgraded to version 1.05

> 
- \(bu
\f(CW\*(C`viacode()\*(C' now accept hex strings and has been optimized.



> 


- \(bu
\f(CW\*(C`CPAN\*(C' upgraded to version 1.76_02

> 
- \(bu
1 minor bug fix for Win32



> 


- \(bu
\f(CW\*(C`Cwd\*(C' upgraded to version 3.12

> 
- \(bu
\f(CW\*(C`canonpath()\*(C' on Win32 now collapses *foo\\..* sections correctly.

- \(bu
Improved behaviour on Symbian \s-1OS.\s0

- \(bu
Enhanced documentation and typo fixes

- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`Data::Dumper\*(C' upgraded to version 2.121_08

> 
- \(bu
A problem where \f(CW\*(C`Data::Dumper\*(C' would sometimes update the iterator state
of hashes has been fixed

- \(bu
Numeric labels now work

- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`DB\*(C' upgraded to version 1.01

> 
- \(bu
A problem where the state of the regexp engine would sometimes get clobbered when running
under the debugger has been fixed.



> 


- \(bu
\f(CW\*(C`DB_File\*(C' upgraded to version 1.814

> 
- \(bu
Adds support for Berkeley \s-1DB 4.4.\s0



> 


- \(bu
\f(CW\*(C`Devel::DProf\*(C' upgraded to version 20050603.00

> 
- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`Devel::Peek\*(C' upgraded to version 1.03

> 
- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`Devel::PPPort\*(C' upgraded to version 3.06_01

> 
- \(bu
\f(CW\*(C`--compat-version\*(C' argument checking has been improved

- \(bu
Files passed on the command line are filtered by default

- \(bu
\f(CW\*(C`--nofilter\*(C' option to override the filtering has been added

- \(bu
Enhanced documentation



> 


- \(bu
\f(CW\*(C`diagnostics\*(C' upgraded to version 1.15

> 
- \(bu
Documentation typo fix



> 


- \(bu
\f(CW\*(C`Digest\*(C' upgraded to version 1.14

> 
- \(bu
The constructor now knows which module implements \s-1SHA-224\s0

- \(bu
Documentation tweaks and typo fixes



> 


- \(bu
\f(CW\*(C`Digest::MD5\*(C' upgraded to version 2.36

> 
- \(bu
\f(CW\*(C`XSLoader\*(C' is now used for faster loading

- \(bu
Enhanced documentation including \s-1MD5\s0 weaknesses discovered lately



> 


- \(bu
\f(CW\*(C`Dumpvalue\*(C' upgraded to version 1.12

> 
- \(bu
Documentation fix



> 


- \(bu
\f(CW\*(C`DynaLoader\*(C' upgraded but unfortunately we're not able to increment its version number :-(

> 
- \(bu
Implements \f(CW\*(C`dl_unload_file\*(C' on Win32

- \(bu
Internal cleanup

- \(bu
\f(CW\*(C`XSLoader\*(C' 0.06 incorporated; small optimisation for calling
\f(CW\*(C`bootstrap_inherit()\*(C' and documentation enhancements.



> 


- \(bu
\f(CW\*(C`Encode\*(C' upgraded to version 2.12

> 
- \(bu
A coderef is now acceptable for \f(CW\*(C`CHECK\*(C'!

- \(bu
3 new characters added to the \s-1ISO-8859-7\s0 encoding

- \(bu
New encoding \f(CW\*(C`MIME-Header-ISO_2022_JP\*(C' added

- \(bu
Problem with partial characters and \f(CW\*(C`encoding(utf-8-strict)\*(C' fixed.

- \(bu
Documentation enhancements and typo fixes



> 


- \(bu
\f(CW\*(C`English\*(C' upgraded to version 1.02

> 
- \(bu
the \f(CW$COMPILING variable has been added



> 


- \(bu
\f(CW\*(C`ExtUtils::Constant\*(C' upgraded to version 0.17

> 
- \(bu
Improved compatibility with older versions of perl



> 


- \(bu
\f(CW\*(C`ExtUtils::MakeMaker\*(C' upgraded to version 6.30 (was 6.17)

> 
- \(bu
Too much to list here;  see <http://search.cpan.org/dist/ExtUtils-MakeMaker/Changes>



> 


- \(bu
\f(CW\*(C`File::Basename\*(C' upgraded to version 2.74, with changes contributed by Michael Schwern.

> 
- \(bu
Documentation clarified and errors corrected.

- \(bu
\f(CW\*(C`basename\*(C' now strips trailing path separators before processing the name.

- \(bu
\f(CW\*(C`basename\*(C' now returns \f(CW\*(C`/\*(C' for parameter \f(CW\*(C`/\*(C', to make \f(CW\*(C`basename\*(C'
consistent with the shell utility of the same name.

- \(bu
The suffix is no longer stripped if it is identical to the remaining characters
in the name, again for consistency with the shell utility.

- \(bu
Some internal code cleanup.



> 


- \(bu
\f(CW\*(C`File::Copy\*(C' upgraded to version 2.09

> 
- \(bu
Copying a file onto itself used to fail.

- \(bu
Moving a file between file systems now preserves the access and
modification time stamps



> 


- \(bu
\f(CW\*(C`File::Find\*(C' upgraded to version 1.10

> 
- \(bu
Win32 portability fixes

- \(bu
Enhanced documentation



> 


- \(bu
\f(CW\*(C`File::Glob\*(C' upgraded to version 1.05

> 
- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`File::Path\*(C' upgraded to version 1.08

> 
- \(bu
\f(CW\*(C`mkpath\*(C' now preserves \f(CW\*(C`errno\*(C' when \f(CW\*(C`mkdir\*(C' fails



> 


- \(bu
\f(CW\*(C`File::Spec\*(C' upgraded to version 3.12

> 
- \(bu
\f(CW\*(C`File::Spec->rootdir()\*(C' now returns \f(CW\*(C`\\\*(C' on Win32, instead of \f(CW\*(C`/\*(C'

- \(bu
\f(CW$^O could sometimes become tainted. This has been fixed.

- \(bu
\f(CW\*(C`canonpath\*(C' on Win32 now collapses \f(CW\*(C`foo/..\*(C' (or \f(CW\*(C`foo\\..\*(C') sections
correctly, rather than doing the \*(L"misguided\*(R" work it was previously doing.
Note that \f(CW\*(C`canonpath\*(C' on Unix still does **not** collapse these sections, as
doing so would be incorrect.

- \(bu
Some documentation improvements

- \(bu
Some internal code cleanup



> 


- \(bu
\f(CW\*(C`FileCache\*(C' upgraded to version 1.06

> 
- \(bu
\s-1POD\s0 formatting errors in the documentation fixed



> 


- \(bu
\f(CW\*(C`Filter::Simple\*(C' upgraded to version 0.82

- \(bu
\f(CW\*(C`FindBin\*(C' upgraded to version 1.47

> 
- \(bu
Now works better with directories where access rights are more
restrictive than usual.



> 


- \(bu
\f(CW\*(C`GDBM_File\*(C' upgraded to version 1.08

> 
- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`Getopt::Long\*(C' upgraded to version 2.35

> 
- \(bu
\f(CW\*(C`prefix_pattern\*(C' has now been complemented by a new configuration
option \f(CW\*(C`long_prefix_pattern\*(C' that allows the user to specify what
prefix patterns should have long option style semantics applied.

- \(bu
Options can now take multiple values at once (experimental)

- \(bu
Various bug fixes



> 


- \(bu
\f(CW\*(C`if\*(C' upgraded to version 0.05

> 
- \(bu
Give more meaningful error messages from \f(CW\*(C`if\*(C' when invoked with a
condition in list context.

- \(bu
Restore backwards compatibility with earlier versions of perl



> 


- \(bu
\f(CW\*(C`IO\*(C' upgraded to version 1.22

> 
- \(bu
Enhanced documentation

- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`IPC::Open2\*(C' upgraded to version 1.02

> 
- \(bu
Enhanced documentation



> 


- \(bu
\f(CW\*(C`IPC::Open3\*(C' upgraded to version 1.02

> 
- \(bu
Enhanced documentation



> 


- \(bu
\f(CW\*(C`List::Util\*(C' upgraded to version 1.18 (was 1.14)

> 
- \(bu
Fix pure-perl version of \f(CW\*(C`refaddr\*(C' to avoid blessing an un-blessed reference

- \(bu
Use \f(CW\*(C`XSLoader\*(C' for faster loading

- \(bu
Fixed various memory leaks

- \(bu
Internal cleanup and portability fixes



> 


- \(bu
\f(CW\*(C`Math::Complex\*(C' upgraded to version 1.35

> 
- \(bu
\f(CW\*(C`atan2(0, i)\*(C' now works, as do all the (computable) complex argument cases

- \(bu
Fixes for certain bugs in \f(CW\*(C`make\*(C' and \f(CW\*(C`emake\*(C'

- \(bu
Support returning the *k*th root directly

- \(bu
Support \f(CW\*(C`[2,-3pi/8]\*(C' in \f(CW\*(C`emake\*(C'

- \(bu
Support \f(CW\*(C`inf\*(C' for \f(CW\*(C`make\*(C'/\f(CW\*(C`emake\*(C'

- \(bu
Document \f(CW\*(C`make\*(C'/\f(CW\*(C`emake\*(C' more visibly



> 


- \(bu
\f(CW\*(C`Math::Trig\*(C' upgraded to version 1.03

> 
- \(bu
Add more great circle routines: \f(CW\*(C`great_circle_waypoint\*(C' and
\f(CW\*(C`great_circle_destination\*(C'



> 


- \(bu
\f(CW\*(C`MIME::Base64\*(C' upgraded to version 3.07

> 
- \(bu
Use \f(CW\*(C`XSLoader\*(C' for faster loading

- \(bu
Enhanced documentation

- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`NDBM_File\*(C' upgraded to version 1.06

> 
- \(bu
Enhanced documentation



> 


- \(bu
\f(CW\*(C`ODBM_File\*(C' upgraded to version 1.06

> 
- \(bu
Documentation typo fixed

- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`Opcode\*(C' upgraded to version 1.06

> 
- \(bu
Enhanced documentation

- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`open\*(C' upgraded to version 1.05

> 
- \(bu
Enhanced documentation



> 


- \(bu
\f(CW\*(C`overload\*(C' upgraded to version 1.04

> 
- \(bu
Enhanced documentation



> 


- \(bu
\f(CW\*(C`PerlIO\*(C' upgraded to version 1.04

> 
- \(bu
\f(CW\*(C`PerlIO::via\*(C' iterate over layers properly now

- \(bu
\f(CW\*(C`PerlIO::scalar\*(C' understands \f(CW\*(C`$/ = ""\*(C' now

- \(bu
\f(CW\*(C`encoding(utf-8-strict)\*(C' with partial characters now works

- \(bu
Enhanced documentation

- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`Pod::Functions\*(C' upgraded to version 1.03

> 
- \(bu
Documentation typos fixed



> 


- \(bu
\f(CW\*(C`Pod::Html\*(C' upgraded to version 1.0504

> 
- \(bu
\s-1HTML\s0 output will now correctly link
to \f(CW\*(C`=item\*(C's on the same page, and should be valid \s-1XHTML.\s0

- \(bu
Variable names are recognized as intended

- \(bu
Documentation typos fixed



> 


- \(bu
\f(CW\*(C`Pod::Parser\*(C' upgraded to version 1.32

> 
- \(bu
Allow files that start with \f(CW\*(C`=head\*(C' on the first line

- \(bu
Win32 portability fix

- \(bu
Exit status of \f(CW\*(C`pod2usage\*(C' fixed

- \(bu
New \f(CW\*(C`-noperldoc\*(C' switch for \f(CW\*(C`pod2usage\*(C'

- \(bu
Arbitrary \s-1URL\s0 schemes now allowed

- \(bu
Documentation typos fixed



> 


- \(bu
\f(CW\*(C`POSIX\*(C' upgraded to version 1.09

> 
- \(bu
Documentation typos fixed

- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`re\*(C' upgraded to version 0.05

> 
- \(bu
Documentation typo fixed



> 


- \(bu
\f(CW\*(C`Safe\*(C' upgraded to version 2.12

> 
- \(bu
Minor documentation enhancement



> 


- \(bu
\f(CW\*(C`SDBM_File\*(C' upgraded to version 1.05

> 
- \(bu
Documentation typo fixed

- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`Socket\*(C' upgraded to version 1.78

> 
- \(bu
Internal cleanup



> 


- \(bu
\f(CW\*(C`Storable\*(C' upgraded to version 2.15

> 
- \(bu
This includes the \f(CW\*(C`STORABLE_attach\*(C' hook functionality added by
Adam Kennedy, and more frugal memory requirements when storing under \f(CW\*(C`ithreads\*(C', by
using the \f(CW\*(C`ithreads\*(C' cloning tracking code.



> 


- \(bu
\f(CW\*(C`Switch\*(C' upgraded to version 2.10_01

> 
- \(bu
Documentation typos fixed



> 


- \(bu
\f(CW\*(C`Sys::Syslog\*(C' upgraded to version 0.13

> 
- \(bu
Now provides numeric macros and meaningful \f(CW\*(C`Exporter\*(C' tags.

- \(bu
No longer uses \f(CW\*(C`Sys::Hostname\*(C' as it may provide useless values in
unconfigured network environments, so instead uses \f(CW\*(C`INADDR_LOOPBACK\*(C' directly.

- \(bu
\f(CW\*(C`syslog()\*(C' now uses local timestamp.

- \(bu
\f(CW\*(C`setlogmask()\*(C' now behaves like its C counterpart.

- \(bu
\f(CW\*(C`setlogsock()\*(C' will now \f(CW\*(C`croak()\*(C' as documented.

- \(bu
Improved error and warnings messages.

- \(bu
Improved documentation.



> 


- \(bu
\f(CW\*(C`Term::ANSIColor\*(C' upgraded to version 1.10

> 
- \(bu
Fixes a bug in \f(CW\*(C`colored\*(C' when \f(CW$EACHLINE is set that caused it to not color
lines consisting solely of 0 (literal zero).

- \(bu
Improved tests.



> 


- \(bu
\f(CW\*(C`Term::ReadLine\*(C' upgraded to version 1.02

> 
- \(bu
Documentation tweaks



> 


- \(bu
\f(CW\*(C`Test::Harness\*(C' upgraded to version 2.56 (was 2.48)

> 
- \(bu
The \f(CW\*(C`Test::Harness\*(C' timer is now off by default.

- \(bu
Now shows elapsed time in milliseconds.

- \(bu
Various bug fixes



> 


- \(bu
\f(CW\*(C`Test::Simple\*(C' upgraded to version 0.62 (was 0.54)

> 
- \(bu
\f(CW\*(C`is_deeply()\*(C' no longer fails to work for many cases

- \(bu
Various minor bug fixes

- \(bu
Documentation enhancements



> 


- \(bu
\f(CW\*(C`Text::Tabs\*(C' upgraded to version 2005.0824

> 
- \(bu
Provides a faster implementation of \f(CW\*(C`expand\*(C'



> 


- \(bu
\f(CW\*(C`Text::Wrap\*(C' upgraded to version 2005.082401

> 
- \(bu
Adds \f(CW$Text::Wrap::separator2, which allows you to preserve existing newlines
but add line-breaks with some other string.



> 


- \(bu
\f(CW\*(C`threads\*(C' upgraded to version 1.07

> 
- \(bu
\f(CW\*(C`threads\*(C' will now honour \f(CW\*(C`no warnings \*(Aqthreads\*(Aq\*(C'

- \(bu
A thread's interpreter is now freed after \f(CW\*(C`$t->join()\*(C' rather than after
\f(CW\*(C`undef $t\*(C', which should fix some \f(CW\*(C`ithreads\*(C' memory leaks. (Fixed by Dave
Mitchell)

- \(bu
Some documentation typo fixes.



> 


- \(bu
\f(CW\*(C`threads::shared\*(C' upgraded to version 0.94

> 
- \(bu
Documentation changes only

- \(bu
Note: An improved implementation of \f(CW\*(C`threads::shared\*(C' is available on
\s-1CPAN\s0 - this will be merged into 5.8.9 if it proves stable.



> 


- \(bu
\f(CW\*(C`Tie::Hash\*(C' upgraded to version 1.02

> 
- \(bu
Documentation typo fixed



> 


- \(bu
\f(CW\*(C`Time::HiRes\*(C' upgraded to version 1.86 (was 1.66)

> 
- \(bu
\f(CW\*(C`clock_nanosleep()\*(C' and \f(CW\*(C`clock()\*(C' functions added

- \(bu
Support for the \s-1POSIX\s0 \f(CW\*(C`clock_gettime()\*(C' and \f(CW\*(C`clock_getres()\*(C' has been added

- \(bu
Return \f(CW\*(C`undef\*(C' or an empty list if the C \f(CW\*(C`gettimeofday()\*(C' function fails

- \(bu
Improved \f(CW\*(C`nanosleep\*(C' detection

- \(bu
Internal cleanup

- \(bu
Enhanced documentation



> 


- \(bu
\f(CW\*(C`Unicode::Collate\*(C' upgraded to version 0.52

> 
- \(bu
Now implements \s-1UCA\s0 Revision 14 (based on Unicode 4.1.0).

- \(bu
\f(CW\*(C`Unicode::Collate->new\*(C' method no longer overwrites user's \f(CW$_

- \(bu
Enhanced documentation



> 


- \(bu
\f(CW\*(C`Unicode::UCD\*(C' upgraded to version 0.24

> 
- \(bu
Documentation typos fixed



> 


- \(bu
\f(CW\*(C`User::grent\*(C' upgraded to version 1.01

> 
- \(bu
Documentation typo fixed



> 


- \(bu
\f(CW\*(C`utf8\*(C' upgraded to version 1.06

> 
- \(bu
Documentation typos fixed



> 


- \(bu
\f(CW\*(C`vmsish\*(C' upgraded to version 1.02

> 
- \(bu
Documentation typos fixed



> 


- \(bu
\f(CW\*(C`warnings\*(C' upgraded to version 1.05

> 
- \(bu
Gentler messing with \f(CW\*(C`Carp::\*(C' internals

- \(bu
Internal cleanup

- \(bu
Documentation update



> 


- \(bu
\f(CW\*(C`Win32\*(C' upgraded to version 0.2601

> 
- \(bu
Provides Windows Vista support to \f(CW\*(C`Win32::GetOSName\*(C'

- \(bu
Documentation enhancements



> 


- \(bu
\f(CW\*(C`XS::Typemap\*(C' upgraded to version 0.02

> 
- \(bu
Internal cleanup



> 


## Utility Changes

Header "Utility Changes"
.ie n .SS """h2xs"" enhancements"
.el .SS "\f(CWh2xs enhancements"
Subsection "h2xs enhancements"
\f(CW\*(C`h2xs\*(C' implements new option \f(CW\*(C`--use-xsloader\*(C' to force use of
\f(CW\*(C`XSLoader\*(C' even in backwards compatible modules.

The handling of authors' names that had apostrophes has been fixed.

Any enums with negative values are now skipped.
.ie n .SS """perlivp"" enhancements"
.el .SS "\f(CWperlivp enhancements"
Subsection "perlivp enhancements"
\f(CW\*(C`perlivp\*(C' implements new option \f(CW\*(C`-a\*(C' and will not check for **.ph*
files by default any more.  Use the \f(CW\*(C`-a\*(C' option to run *all* tests.

## New Documentation

Header "New Documentation"
The perlglossary manpage is a glossary of terms used in the Perl
documentation, technical and otherwise, kindly provided by O'Reilly Media,
inc.

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
Weak reference creation is now *O(1)* rather than *O(n)*, courtesy of
Nicholas Clark. Weak reference deletion remains *O(n)*, but if deletion only
happens at program exit, it may be skipped completely.

- \(bu
Salvador Fandin\*~o provided improvements to reduce the memory usage of \f(CW\*(C`sort\*(C'
and to speed up some cases.

- \(bu
Jarkko Hietaniemi and Andy Lester worked to mark as much data as possible in
the C source files as \f(CW\*(C`static\*(C', to increase the proportion of the executable
file that the operating system can share between process, and thus reduce
real memory usage on multi-user systems.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"
Parallel makes should work properly now, although there may still be problems
if \f(CW\*(C`make test\*(C' is instructed to run in parallel.

Building with Borland's compilers on Win32 should work more smoothly. In
particular Steve Hay has worked to side step many warnings emitted by their
compilers and at least one C compiler internal error.

\f(CW\*(C`Configure\*(C' will now detect \f(CW\*(C`clearenv\*(C' and \f(CW\*(C`unsetenv\*(C', thanks to a patch
from Alan Burlison. It will also probe for \f(CW\*(C`futimes\*(C' and whether \f(CW\*(C`sprintf\*(C'
correctly returns the length of the formatted string, which will both be used
in perl 5.8.9.

There are improved hints for next-3.0, vmesa, \s-1IX,\s0 Darwin, Solaris, Linux,
\s-1DEC/OSF,\s0 HP-UX and MPE/iX

Perl extensions on Windows now can be statically built into the Perl \s-1DLL,\s0
thanks to a work by Vadim Konovalov. (This improvement was actually in 5.8.7,
but was accidentally omitted from perl587delta).

## Selected Bug Fixes

Header "Selected Bug Fixes"

### no warnings category works correctly with -w

Subsection "no warnings 'category' works correctly with -w"
Previously when running with warnings enabled globally via \f(CW\*(C`-w\*(C', selective
disabling of specific warning categories would actually turn off all warnings.
This is now fixed; now \f(CW\*(C`no warnings \*(Aqio\*(Aq;\*(C' will only turn off warnings in the
\f(CW\*(C`io\*(C' class. Previously it would erroneously turn off all warnings.

This bug fix may cause some programs to start correctly issuing warnings.

### Remove over-optimisation

Subsection "Remove over-optimisation"
Perl 5.8.4 introduced a change so that assignments of \f(CW\*(C`undef\*(C' to a
scalar, or of an empty list to an array or a hash, were optimised away. As
this could cause problems when \f(CW\*(C`goto\*(C' jumps were involved, this change
has been backed out.

### \fBsprintf() fixes

Subsection "sprintf() fixes"
Using the **sprintf()** function with some formats could lead to a buffer
overflow in some specific cases. This has been fixed, along with several
other bugs, notably in bounds checking.

In related fixes, it was possible for badly written code that did not follow
the documentation of \f(CW\*(C`Sys::Syslog\*(C' to have formatting vulnerabilities.
\f(CW\*(C`Sys::Syslog\*(C' has been changed to protect people from poor quality third
party code.

### Debugger and Unicode slowdown

Subsection "Debugger and Unicode slowdown"
It had been reported that running under perl's debugger when processing
Unicode data could cause unexpectedly large slowdowns. The most likely cause
of this was identified and fixed by Nicholas Clark.

### Smaller fixes

Subsection "Smaller fixes"

- \(bu
\f(CW\*(C`FindBin\*(C' now works better with directories where access rights are more
restrictive than usual.

- \(bu
Several memory leaks in ithreads were closed. An improved implementation of
\f(CW\*(C`threads::shared\*(C' is available on \s-1CPAN\s0 - this will be merged into 5.8.9 if
it proves stable.

- \(bu
Trailing spaces are now trimmed from \f(CW$! and \f(CW$^E.

- \(bu
Operations that require perl to read a process's list of groups, such as reads
of \f(CW$( and \f(CW$), now dynamically allocate memory rather than using a
fixed sized array. The fixed size array could cause C stack exhaustion on
systems configured to use large numbers of groups.

- \(bu
\f(CW\*(C`PerlIO::scalar\*(C' now works better with non-default \f(CW$/ settings.

- \(bu
You can now use the \f(CW\*(C`x\*(C' operator to repeat a \f(CW\*(C`qw//\*(C' list. This used
to raise a syntax error.

- \(bu
The debugger now traces correctly execution in eval("")uated code that
contains #line directives.

- \(bu
The value of the \f(CW\*(C`open\*(C' pragma is no longer ignored for three-argument
opens.

- \(bu
The optimisation of \f(CW\*(C`for (reverse @a)\*(C' introduced in perl 5.8.6 could
misbehave when the array had undefined elements and was used in \s-1LVALUE\s0
context. Dave Mitchell provided a fix.

- \(bu
Some case insensitive matches between \s-1UTF-8\s0 encoded data and 8 bit regexps,
and vice versa, could give malformed character warnings. These have been
fixed by Dave Mitchell and Yves Orton.

- \(bu
\f(CW\*(C`lcfirst\*(C' and \f(CW\*(C`ucfirst\*(C' could corrupt the string for certain cases where
the length \s-1UTF-8\s0 encoding of the string in lower case, upper case or title
case differed. This was fixed by Nicholas Clark.

- \(bu
Perl will now use the C library calls \f(CW\*(C`unsetenv\*(C' and \f(CW\*(C`clearenv\*(C' if present
to delete keys from \f(CW%ENV and delete \f(CW%ENV entirely, thanks to a patch
from Alan Burlison.

## New or Changed Diagnostics

Header "New or Changed Diagnostics"

### Attempt to set length of freed array

Subsection "Attempt to set length of freed array"
This is a new warning, produced in situations such as this:

.Vb 2
    $r = do \{my @a; \\$#a\};
    $$r = 503;
.Ve

### Non-string passed as bitmask

Subsection "Non-string passed as bitmask"
This is a new warning, produced when number has been passed as an argument to
**select()**, instead of a bitmask.

.Vb 3
    # Wrong, will now warn
    $rin = fileno(STDIN);
    ($nfound,$timeleft) = select($rout=$rin, undef, undef, $timeout);

    # Should be
    $rin = \*(Aq\*(Aq;
    vec($rin,fileno(STDIN),1) = 1;
    ($nfound,$timeleft) = select($rout=$rin, undef, undef, $timeout);
.Ve

### Search pattern not terminated or ternary operator parsed as search pattern

Subsection "Search pattern not terminated or ternary operator parsed as search pattern"
This syntax error indicates that the lexer couldn't find the final
delimiter of a \f(CW\*(C`?PATTERN?\*(C' construct. Mentioning the ternary operator in
this error message makes it easier to diagnose syntax errors.

## Changed Internals

Header "Changed Internals"
There has been a fair amount of refactoring of the \f(CW\*(C`C\*(C' source code, partly to
make it tidier and more maintainable. The resulting object code and the
\f(CW\*(C`perl\*(C' binary may well be smaller than 5.8.7, in particular due to a change
contributed by Dave Mitchell which reworked the warnings code to be
significantly smaller. Apart from being smaller and possibly faster, there
should be no user-detectable changes.

Andy Lester supplied many improvements to determine which function
parameters and local variables could actually be declared \f(CW\*(C`const\*(C' to the C
compiler. Steve Peters provided new \f(CW*_set macros and reworked the core to
use these rather than assigning to macros in \s-1LVALUE\s0 context.

Dave Mitchell improved the lexer debugging output under \f(CW\*(C`-DT\*(C'

Nicholas Clark changed the string buffer allocation so that it is now rounded
up to the next multiple of 4 (or 8 on platforms with 64 bit pointers). This
should reduce the number of calls to \f(CW\*(C`realloc\*(C' without actually using any
extra memory.

The \f(CW\*(C`HV\*(C''s array of \f(CW\*(C`HE*\*(C's is now allocated at the correct (minimal) size,
thanks to another change by Nicholas Clark. Compile with
\f(CW\*(C`-DPERL_USE_LARGE_HV_ALLOC\*(C' to use the old, sloppier, default.

For \s-1XS\s0 or embedding debugging purposes, if perl is compiled with
\f(CW\*(C`-DDEBUG_LEAKING_SCALARS_FORK_DUMP\*(C' in addition to
\f(CW\*(C`-DDEBUG_LEAKING_SCALARS\*(C' then a child process is \f(CW\*(C`fork\*(C'ed just before
global destruction, which is used to display the values of any scalars
found to have leaked at the end of global destruction. Without this, the
scalars have already been freed sufficiently at the point of detection that
it is impossible to produce any meaningful dump of their contents.  This
feature was implemented by the indefatigable Nicholas Clark, based on an idea
by Mike Giroux.

## Platform Specific Problems

Header "Platform Specific Problems"
The optimiser on HP-UX 11.23 (Itanium 2) is currently partly disabled (scaled
down to +O1) when using \s-1HP\s0 C-ANSI-C; the cause of problems at higher
optimisation levels is still unclear.

There are a handful of remaining test failures on \s-1VMS,\s0 mostly due to
test fixes and minor module tweaks with too many dependencies to
integrate into this release from the development stream, where they have
all been corrected.  The following is a list of expected failures with
the patch number of the fix where that is known:

.Vb 6
    ext/Devel/PPPort/t/ppphtest.t  #26913
    ext/List/Util/t/p_tainted.t    #26912
    lib/ExtUtils/t/PL_FILES.t      #26813
    lib/ExtUtils/t/basic.t         #26813
    t/io/fs.t
    t/op/cmp.t
.Ve

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://bugs.perl.org.  There may also be
information at http://www.perl.org, the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.  You can browse and search
the Perl 5 bugs at http://bugs.perl.org/

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for exhaustive details on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
