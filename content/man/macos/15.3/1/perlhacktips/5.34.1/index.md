+++
manpage_section = "1"
manpage_format = "troff"
date = "2022-02-19"
detected_package_version = "5.34.1"
description = "This document will help you learn the best way to go about hacking on the Perl core C code.  It covers common problems, debugging, profiling, and more. If you havent read perlhack and perlhacktut yet, you might want to do that first. Perl source..."
operating_system_version = "15.3"
operating_system = "macos"
title = "perlhacktips(1)"
manpage_name = "perlhacktips"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLHACKTIPS 1"
PERLHACKTIPS 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlhacktips - Tips for Perl core C code hacking

## DESCRIPTION

Header "DESCRIPTION"
This document will help you learn the best way to go about hacking on
the Perl core C code.  It covers common problems, debugging, profiling,
and more.

If you haven't read perlhack and perlhacktut yet, you might want
to do that first.

## COMMON PROBLEMS

Header "COMMON PROBLEMS"
Perl source plays by \s-1ANSI C89\s0 rules: no C99 (or \*(C+) extensions.
You don't care about some particular platform having broken Perl? I
hear there is still a strong demand for J2EE programmers.

### Perl environment problems

Subsection "Perl environment problems"

- \(bu
Not compiling with threading
.Sp
Compiling with threading (-Duseithreads) completely rewrites the
function prototypes of Perl.  You better try your changes with that.
Related to this is the difference between \*(L"Perl_-less\*(R" and \*(L"Perl_-ly\*(R"
APIs, for example:
.Sp
.Vb 2
  Perl_sv_setiv(aTHX_ ...);
  sv_setiv(...);
.Ve
.Sp
The first one explicitly passes in the context, which is needed for
e.g. threaded builds.  The second one does that implicitly; do not get
them mixed.  If you are not passing in a aTHX_, you will need to do a
dTHX as the first thing in the function.
.Sp
See \*(L"How multiple interpreters and concurrency are
supported\*(R" in perlguts for further discussion about context.

- \(bu
Not compiling with -DDEBUGGING
.Sp
The \s-1DEBUGGING\s0 define exposes more code to the compiler, therefore more
ways for things to go wrong.  You should try it.

- \(bu
Introducing (non-read-only) globals
.Sp
Do not introduce any modifiable globals, truly global or file static.
They are bad form and complicate multithreading and other forms of
concurrency.  The right way is to introduce them as new interpreter
variables, see *intrpvar.h* (at the very end for binary
compatibility).
.Sp
Introducing read-only (const) globals is okay, as long as you verify
with e.g. \f(CW\*(C`nm libperl.a|egrep -v \*(Aq [TURtr] \*(Aq\*(C' (if your \f(CW\*(C`nm\*(C' has
BSD-style output) that the data you added really is read-only.  (If it
is, it shouldn't show up in the output of that command.)
.Sp
If you want to have static strings, make them constant:
.Sp
.Vb 1
  static const char etc[] = "...";
.Ve
.Sp
If you want to have arrays of constant strings, note carefully the
right combination of \f(CW\*(C`const\*(C's:
.Sp
.Vb 2
    static const char * const yippee[] =
        \{"hi", "ho", "silver"\};
.Ve

- \(bu
Not exporting your new function
.Sp
Some platforms (Win32, \s-1AIX, VMS, OS/2,\s0 to name a few) require any
function that is part of the public \s-1API\s0 (the shared Perl library) to be
explicitly marked as exported.  See the discussion about *embed.pl* in
perlguts.

- \(bu
Exporting your new function
.Sp
The new shiny result of either genuine new functionality or your
arduous refactoring is now ready and correctly exported.  So what could
possibly go wrong?
.Sp
Maybe simply that your function did not need to be exported in the
first place.  Perl has a long and not so glorious history of exporting
functions that it should not have.
.Sp
If the function is used only inside one source code file, make it
static.  See the discussion about *embed.pl* in perlguts.
.Sp
If the function is used across several files, but intended only for
Perl's internal use (and this should be the common case), do not export
it to the public \s-1API.\s0  See the discussion about *embed.pl* in
perlguts.

### Portability problems

Subsection "Portability problems"
The following are common causes of compilation and/or execution
failures, not common to Perl as such.  The C \s-1FAQ\s0 is good bedtime
reading.  Please test your changes with as many C compilers and
platforms as possible; we will, anyway, and it's nice to save oneself
from public embarrassment.

If using gcc, you can add the \f(CW\*(C`-std=c89\*(C' option which will hopefully
catch most of these unportabilities.  (However it might also catch
incompatibilities in your system's header files.)

Use the Configure \f(CW\*(C`-Dgccansipedantic\*(C' flag to enable the gcc \f(CW\*(C`-ansi
-pedantic\*(C' flags which enforce stricter \s-1ANSI\s0 rules.

If using the \f(CW\*(C`gcc -Wall\*(C' note that not all the possible warnings (like
\f(CW\*(C`-Wuninitialized\*(C') are given unless you also compile with \f(CW\*(C`-O\*(C'.

Note that if using gcc, starting from Perl 5.9.5 the Perl core source
code files (the ones at the top level of the source code distribution,
but not e.g. the extensions under ext/) are automatically compiled with
as many as possible of the \f(CW\*(C`-std=c89\*(C', \f(CW\*(C`-ansi\*(C', \f(CW\*(C`-pedantic\*(C', and a
selection of \f(CW\*(C`-W\*(C' flags (see cflags.SH).

Also study perlport carefully to avoid any bad assumptions about the
operating system, filesystems, character set, and so forth.

You may once in a while try a \*(L"make microperl\*(R" to see whether we can
still compile Perl with just the bare minimum of interfaces.  (See
\s-1README\s0.micro.)

Do not assume an operating system indicates a certain compiler.

- \(bu
Casting pointers to integers or casting integers to pointers
.Sp
.Vb 3
    void castaway(U8* p)
    \{
      IV i = p;
.Ve
.Sp
or
.Sp
.Vb 3
    void castaway(U8* p)
    \{
      IV i = (IV)p;
.Ve
.Sp
Both are bad, and broken, and unportable.  Use the \s-1**PTR2IV\s0()** macro that
does it right.  (Likewise, there are \s-1**PTR2UV\s0()**, \s-1**PTR2NV\s0()**, \s-1**INT2PTR\s0()**, and
\s-1**NUM2PTR\s0()**.)

- \(bu
Casting between function pointers and data pointers
.Sp
Technically speaking casting between function pointers and data
pointers is unportable and undefined, but practically speaking it seems
to work, but you should use the \s-1**FPTR2DPTR\s0()** and \s-1**DPTR2FPTR\s0()** macros.
Sometimes you can also play games with unions.

- \(bu
Assuming sizeof(int) == sizeof(long)
.Sp
There are platforms where longs are 64 bits, and platforms where ints
are 64 bits, and while we are out to shock you, even platforms where
shorts are 64 bits.  This is all legal according to the C standard.  (In
other words, \*(L"long long\*(R" is not a portable way to specify 64 bits, and
\*(L"long long\*(R" is not even guaranteed to be any wider than \*(L"long\*(R".)
.Sp
Instead, use the definitions \s-1IV, UV, IVSIZE, I32SIZE,\s0 and so forth.
Avoid things like I32 because they are **not** guaranteed to be
*exactly* 32 bits, they are *at least* 32 bits, nor are they
guaranteed to be **int** or **long**.  If you really explicitly need
64-bit variables, use I64 and U64, but only if guarded by \s-1HAS_QUAD.\s0

- \(bu
Assuming one can dereference any type of pointer for any type of data
.Sp
.Vb 2
  char *p = ...;
  long pony = *(long *)p;    /* BAD */
.Ve
.Sp
Many platforms, quite rightly so, will give you a core dump instead of
a pony if the p happens not to be correctly aligned.

- \(bu
Lvalue casts
.Sp
.Vb 1
  (int)*p = ...;    /* BAD */
.Ve
.Sp
Simply not portable.  Get your lvalue to be of the right type, or maybe
use temporary variables, or dirty tricks with unions.

- \(bu
Assume **anything** about structs (especially the ones you don't
control, like the ones coming from the system headers)

> 
- \(bu
That a certain field exists in a struct

- \(bu
That no other fields exist besides the ones you know of

- \(bu
That a field is of certain signedness, sizeof, or type

- \(bu
That the fields are in a certain order

> 
- \(bu
While C guarantees the ordering specified in the struct definition,
between different platforms the definitions might differ



> 


- \(bu
That the sizeof(struct) or the alignments are the same everywhere

> 
- \(bu
There might be padding bytes between the fields to align the fields -
the bytes can be anything

- \(bu
Structs are required to be aligned to the maximum alignment required by
the fields - which for native types is for usually equivalent to
**sizeof()** of the field



> 




> 


- \(bu
Assuming the character set is ASCIIish
.Sp
Perl can compile and run under \s-1EBCDIC\s0 platforms.  See perlebcdic.
This is transparent for the most part, but because the character sets
differ, you shouldn't use numeric (decimal, octal, nor hex) constants
to refer to characters.  You can safely say \f(CW\*(AqA\*(Aq, but not \f(CW0x41.
You can safely say \f(CW\*(Aq\\n\*(Aq, but not \f(CW\*(C`\\012\*(C'.  However, you can use
macros defined in *utf8.h* to specify any code point portably.
\f(CW\*(C`LATIN1_TO_NATIVE(0xDF)\*(C' is going to be the code point that means
\s-1LATIN SMALL LETTER SHARP S\s0 on whatever platform you are running on (on
\s-1ASCII\s0 platforms it compiles without adding any extra code, so there is
zero performance hit on those).  The acceptable inputs to
\f(CW\*(C`LATIN1_TO_NATIVE\*(C' are from \f(CW0x00 through \f(CW0xFF.  If your input
isn't guaranteed to be in that range, use \f(CW\*(C`UNICODE_TO_NATIVE\*(C' instead.
\f(CW\*(C`NATIVE_TO_LATIN1\*(C' and \f(CW\*(C`NATIVE_TO_UNICODE\*(C' translate the opposite
direction.
.Sp
If you need the string representation of a character that doesn't have a
mnemonic name in C, you should add it to the list in
*regen/unicode_constants.pl*, and have Perl create \f(CW\*(C`#define\*(C''s for you,
based on the current platform.
.Sp
Note that the \f(CW\*(C`is\f(CIFOO\f(CW\*(C' and \f(CW\*(C`to\f(CIFOO\f(CW\*(C' macros in *handy.h* work
properly on native code points and strings.
.Sp
Also, the range 'A' - 'Z' in \s-1ASCII\s0 is an unbroken sequence of 26 upper
case alphabetic characters.  That is not true in \s-1EBCDIC.\s0  Nor for 'a' to
'z'.  But '0' - '9' is an unbroken range in both systems.  Don't assume
anything about other ranges.  (Note that special handling of ranges in
regular expression patterns and transliterations makes it appear to Perl
code that the aforementioned ranges are all unbroken.)
.Sp
Many of the comments in the existing code ignore the possibility of
\s-1EBCDIC,\s0 and may be wrong therefore, even if the code works.  This is
actually a tribute to the successful transparent insertion of being
able to handle \s-1EBCDIC\s0 without having to change pre-existing code.
.Sp
\s-1UTF-8\s0 and UTF-EBCDIC are two different encodings used to represent
Unicode code points as sequences of bytes.  Macros  with the same names
(but different definitions) in *utf8.h* and *utfebcdic.h* are used to
allow the calling code to think that there is only one such encoding.
This is almost always referred to as \f(CW\*(C`utf8\*(C', but it means the \s-1EBCDIC\s0
version as well.  Again, comments in the code may well be wrong even if
the code itself is right.  For example, the concept of \s-1UTF-8\s0 \f(CW\*(C`invariant
characters\*(C' differs between \s-1ASCII\s0 and \s-1EBCDIC.\s0  On \s-1ASCII\s0 platforms, only
characters that do not have the high-order bit set (i.e.  whose ordinals
are strict \s-1ASCII, 0\s0 - 127) are invariant, and the documentation and
comments in the code may assume that, often referring to something
like, say, \f(CW\*(C`hibit\*(C'.  The situation differs and is not so simple on
\s-1EBCDIC\s0 machines, but as long as the code itself uses the
\f(CW\*(C`NATIVE_IS_INVARIANT()\*(C' macro appropriately, it works, even if the
comments are wrong.
.Sp
As noted in \*(L"\s-1TESTING\*(R"\s0 in perlhack, when writing test scripts, the file
*t/charset_tools.pl* contains some helpful functions for writing tests
valid on both \s-1ASCII\s0 and \s-1EBCDIC\s0 platforms.  Sometimes, though, a test
can't use a function and it's inconvenient to have different test
versions depending on the platform.  There are 20 code points that are
the same in all 4 character sets currently recognized by Perl (the 3
\s-1EBCDIC\s0 code pages plus \s-1ISO 8859-1\s0 (ASCII/Latin1)).  These can be used in
such tests, though there is a small possibility that Perl will become
available in yet another character set, breaking your test.  All but one
of these code points are C0 control characters.  The most significant
controls that are the same are \f(CW\*(C`\\0\*(C', \f(CW\*(C`\\r\*(C', and \f(CW\*(C`\\N\{VT\}\*(C' (also
specifiable as \f(CW\*(C`\\cK\*(C', \f(CW\*(C`\\x0B\*(C', \f(CW\*(C`\\N\{U+0B\}\*(C', or \f(CW\*(C`\\013\*(C').  The single
non-control is U+00B6 \s-1PILCROW SIGN.\s0  The controls that are the same have
the same bit pattern in all 4 character sets, regardless of the UTF8ness
of the string containing them.  The bit pattern for U+B6 is the same in
all 4 for non-UTF8 strings, but differs in each when its containing
string is \s-1UTF-8\s0 encoded.  The only other code points that have some sort
of sameness across all 4 character sets are the pair 0xDC and 0xFC.
Together these represent upper- and lowercase \s-1LATIN LETTER U WITH
DIAERESIS,\s0 but which is upper and which is lower may be reversed: 0xDC
is the capital in Latin1 and 0xFC is the small letter, while 0xFC is the
capital in \s-1EBCDIC\s0 and 0xDC is the small one.  This factoid may be
exploited in writing case insensitive tests that are the same across all
4 character sets.

- \(bu
Assuming the character set is just \s-1ASCII\s0
.Sp
\s-1ASCII\s0 is a 7 bit encoding, but bytes have 8 bits in them.  The 128 extra
characters have different meanings depending on the locale.  Absent a
locale, currently these extra characters are generally considered to be
unassigned, and this has presented some problems.  This has being
changed starting in 5.12 so that these characters can be considered to
be Latin-1 (\s-1ISO-8859-1\s0).

- \(bu
Mixing #define and #ifdef
.Sp
.Vb 6
  #define BURGLE(x) ... \\
  #ifdef BURGLE_OLD_STYLE        /* BAD */
  ... do it the old way ... \\
  #else
  ... do it the new way ... \\
  #endif
.Ve
.Sp
You cannot portably \*(L"stack\*(R" cpp directives.  For example in the above
you need two separate \s-1**BURGLE\s0()** #defines, one for each #ifdef branch.

- \(bu
Adding non-comment stuff after #endif or #else
.Sp
.Vb 5
  #ifdef SNOSH
  ...
  #else !SNOSH    /* BAD */
  ...
  #endif SNOSH    /* BAD */
.Ve
.Sp
The #endif and #else cannot portably have anything non-comment after
them.  If you want to document what is going (which is a good idea
especially if the branches are long), use (C) comments:
.Sp
.Vb 5
  #ifdef SNOSH
  ...
  #else /* !SNOSH */
  ...
  #endif /* SNOSH */
.Ve
.Sp
The gcc option \f(CW\*(C`-Wendif-labels\*(C' warns about the bad variant (by
default on starting from Perl 5.9.4).

- \(bu
Having a comma after the last element of an enum list
.Sp
.Vb 5
  enum color \{
    CERULEAN,
    CHARTREUSE,
    CINNABAR,     /* BAD */
  \};
.Ve
.Sp
is not portable.  Leave out the last comma.
.Sp
Also note that whether enums are implicitly morphable to ints varies
between compilers, you might need to (int).

- \(bu
Using //-comments
.Sp
.Vb 1
  // This function bamfoodles the zorklator.   /* BAD */
.Ve
.Sp
That is C99 or \*(C+.  Perl is C89.  Using the //-comments is silently
allowed by many C compilers but cranking up the \s-1ANSI C89\s0 strictness
(which we like to do) causes the compilation to fail.

- \(bu
Mixing declarations and code
.Sp
.Vb 5
  void zorklator()
  \{
    int n = 3;
    set_zorkmids(n);    /* BAD */
    int q = 4;
.Ve
.Sp
That is C99 or \*(C+.  Some C compilers allow that, but you shouldn't.
.Sp
The gcc option \f(CW\*(C`-Wdeclaration-after-statement\*(C' scans for such
problems (by default on starting from Perl 5.9.4).

- \(bu
Introducing variables inside **for()**
.Sp
.Vb 1
  for(int i = ...; ...; ...) \{    /* BAD */
.Ve
.Sp
That is C99 or \*(C+.  While it would indeed be awfully nice to have that
also in C89, to limit the scope of the loop variable, alas, we cannot.

- \(bu
Mixing signed char pointers with unsigned char pointers
.Sp
.Vb 4
  int foo(char *s) \{ ... \}
  ...
  unsigned char *t = ...; /* Or U8* t = ... */
  foo(t);   /* BAD */
.Ve
.Sp
While this is legal practice, it is certainly dubious, and downright
fatal in at least one platform: for example \s-1VMS\s0 cc considers this a
fatal error.  One cause for people often making this mistake is that a
\*(L"naked char\*(R" and therefore dereferencing a \*(L"naked char pointer\*(R" have an
undefined signedness: it depends on the compiler and the flags of the
compiler and the underlying platform whether the result is signed or
unsigned.  For this very same reason using a 'char' as an array index is
bad.

- \(bu
Macros that have string constants and their arguments as substrings of
the string constants
.Sp
.Vb 2
  #define FOO(n) printf("number = %d\\n", n)    /* BAD */
  FOO(10);
.Ve
.Sp
Pre-ANSI semantics for that was equivalent to
.Sp
.Vb 1
  printf("10umber = %d\\10");
.Ve
.Sp
which is probably not what you were expecting.  Unfortunately at least
one reasonably common and modern C compiler does \*(L"real backward
compatibility\*(R" here, in \s-1AIX\s0 that is what still happens even though the
rest of the \s-1AIX\s0 compiler is very happily C89.

- \(bu
Using printf formats for non-basic C types
.Sp
.Vb 2
   IV i = ...;
   printf("i = %d\\n", i);    /* BAD */
.Ve
.Sp
While this might by accident work in some platform (where \s-1IV\s0 happens to
be an \f(CW\*(C`int\*(C'), in general it cannot.  \s-1IV\s0 might be something larger.  Even
worse the situation is with more specific types (defined by Perl's
configuration step in *config.h*):
.Sp
.Vb 2
   Uid_t who = ...;
   printf("who = %d\\n", who);    /* BAD */
.Ve
.Sp
The problem here is that Uid_t might be not only not \f(CW\*(C`int\*(C'-wide but it
might also be unsigned, in which case large uids would be printed as
negative values.
.Sp
There is no simple solution to this because of **printf()**'s limited
intelligence, but for many types the right format is available as with
either 'f' or '_f' suffix, for example:
.Sp
.Vb 2
   IVdf /* IV in decimal */
   UVxf /* UV is hexadecimal */

   printf("i = %"IVdf"\\n", i); /* The IVdf is a string constant. */

   Uid_t_f /* Uid_t in decimal */

   printf("who = %"Uid_t_f"\\n", who);
.Ve
.Sp
Or you can try casting to a \*(L"wide enough\*(R" type:
.Sp
.Vb 1
   printf("i = %"IVdf"\\n", (IV)something_very_small_and_signed);
.Ve
.Sp
See \*(L"Formatted Printing of Size_t and SSize_t\*(R" in perlguts for how to
print those.
.Sp
Also remember that the \f(CW%p format really does require a void pointer:
.Sp
.Vb 2
   U8* p = ...;
   printf("p = %p\\n", (void*)p);
.Ve
.Sp
The gcc option \f(CW\*(C`-Wformat\*(C' scans for such problems.

- \(bu
Blindly using variadic macros
.Sp
gcc has had them for a while with its own syntax, and C99 brought them
with a standardized syntax.  Don't use the former, and use the latter
only if the \s-1HAS_C99_VARIADIC_MACROS\s0 is defined.

- \(bu
Blindly passing va_list
.Sp
Not all platforms support passing va_list to further varargs (stdarg)
functions.  The right thing to do is to copy the va_list using the
**Perl_va_copy()** if the \s-1NEED_VA_COPY\s0 is defined.

- \(bu
Using gcc statement expressions
.Sp
.Vb 1
   val = (\{...;...;...\});    /* BAD */
.Ve
.Sp
While a nice extension, it's not portable.  The Perl code does
admittedly use them if available to gain some extra speed (essentially
as a funky form of inlining), but you shouldn't.

- \(bu
Binding together several statements in a macro
.Sp
Use the macros \s-1STMT_START\s0 and \s-1STMT_END.\s0
.Sp
.Vb 3
   STMT_START \{
      ...
   \} STMT_END
.Ve

- \(bu
Testing for operating systems or versions when should be testing for
features
.Sp
.Vb 3
  #ifdef _\|_FOONIX_\|_    /* BAD */
  foo = quux();
  #endif
.Ve
.Sp
Unless you know with 100% certainty that **quux()** is only ever available
for the \*(L"Foonix\*(R" operating system **and** that is available **and**
correctly working for **all** past, present, **and** future versions of
\*(L"Foonix\*(R", the above is very wrong.  This is more correct (though still
not perfect, because the below is a compile-time check):
.Sp
.Vb 3
  #ifdef HAS_QUUX
  foo = quux();
  #endif
.Ve
.Sp
How does the \s-1HAS_QUUX\s0 become defined where it needs to be?  Well, if
Foonix happens to be Unixy enough to be able to run the Configure
script, and Configure has been taught about detecting and testing
**quux()**, the \s-1HAS_QUUX\s0 will be correctly defined.  In other platforms, the
corresponding configuration step will hopefully do the same.
.Sp
In a pinch, if you cannot wait for Configure to be educated, or if you
have a good hunch of where **quux()** might be available, you can
temporarily try the following:
.Sp
.Vb 3
  #if (defined(_\|_FOONIX_\|_) || defined(_\|_BARNIX_\|_))
  # define HAS_QUUX
  #endif

  ...

  #ifdef HAS_QUUX
  foo = quux();
  #endif
.Ve
.Sp
But in any case, try to keep the features and operating systems
separate.
.Sp
A good resource on the predefined macros for various operating
systems, compilers, and so forth is
<http://sourceforge.net/p/predef/wiki/Home/>

- \(bu
Assuming the contents of static memory pointed to by the return values
of Perl wrappers for C library functions doesn't change.  Many C library
functions return pointers to static storage that can be overwritten by
subsequent calls to the same or related functions.  Perl has
light-weight wrappers for some of these functions, and which don't make
copies of the static memory.  A good example is the interface to the
environment variables that are in effect for the program.  Perl has
\f(CW\*(C`PerlEnv_getenv\*(C' to get values from the environment.  But the return is
a pointer to static memory in the C library.  If you are using the value
to immediately test for something, that's fine, but if you save the
value and expect it to be unchanged by later processing, you would be
wrong, but perhaps you wouldn't know it because different C library
implementations behave differently, and the one on the platform you're
testing on might work for your situation.  But on some platforms, a
subsequent call to \f(CW\*(C`PerlEnv_getenv\*(C' or related function \s-1WILL\s0 overwrite
the memory that your first call points to.  This has led to some
hard-to-debug problems.  Do a \*(L"savepv\*(R" in perlapi to make a copy, thus
avoiding these problems.  You will have to free the copy when you're
done to avoid memory leaks.  If you don't have control over when it gets
freed, you'll need to make the copy in a mortal scalar, like so:
.Sp
.Vb 6
 if ((s = PerlEnv_getenv("foo") == NULL) \{
    ... /* handle NULL case */
 \}
 else \{
     s = SvPVX(sv_2mortal(newSVpv(s, 0)));
 \}
.Ve
.Sp
The above example works only if \f(CW"s" is \f(CW\*(C`NUL\*(C'-terminated; otherwise
you have to pass its length to \f(CW\*(C`newSVpv\*(C'.

### Problematic System Interfaces

Subsection "Problematic System Interfaces"

- \(bu
Perl strings are \s-1NOT\s0 the same as C strings:  They may contain \f(CW\*(C`NUL\*(C'
characters, whereas a C string is terminated by the first \f(CW\*(C`NUL\*(C'.
That is why Perl \s-1API\s0 functions that deal with strings generally take a
pointer to the first byte and either a length or a pointer to the byte
just beyond the final one.
.Sp
And this is the reason that many of the C library string handling
functions should not be used.  They don't cope with the full generality
of Perl strings.  It may be that your test cases don't have embedded
\f(CW\*(C`NUL\*(C's, and so the tests pass, whereas there may well eventually arise
real-world cases where they fail.  A lesson here is to include \f(CW\*(C`NUL\*(C's
in your tests.  Now it's fairly rare in most real world cases to get
\f(CW\*(C`NUL\*(C's, so your code may seem to work, until one day a \f(CW\*(C`NUL\*(C' comes
along.
.Sp
Here's an example.  It used to be a common paradigm, for decades, in the
perl core to use \f(CW\*(C`strchr("list",\ c)\*(C' to see if the character \f(CW\*(C`c\*(C' is
any of the ones given in \f(CW"list", a double-quote-enclosed string of
the set of characters that we are seeing if \f(CW\*(C`c\*(C' is one of.  As long as
\f(CW\*(C`c\*(C' isn't a \f(CW\*(C`NUL\*(C', it works.  But when \f(CW\*(C`c\*(C' is a \f(CW\*(C`NUL\*(C', \f(CW\*(C`strchr\*(C'
returns a pointer to the terminating \f(CW\*(C`NUL\*(C' in \f(CW"list".   This likely
will result in a segfault or a security issue when the caller uses that
end pointer as the starting point to read from.
.Sp
A solution to this and many similar issues is to use the \f(CW\*(C`mem\*(C'*-foo* C
library functions instead.  In this case \f(CW\*(C`memchr\*(C' can be used to see if
\f(CW\*(C`c\*(C' is in \f(CW"list" and works even if \f(CW\*(C`c\*(C' is \f(CW\*(C`NUL\*(C'.  These functions
need an additional parameter to give the string length.
In the case of literal string parameters, perl has defined macros that
calculate the length for you.  See \*(L"String Handling\*(R" in perlapi.

- \(bu
**malloc**\|(0), **realloc**\|(0), calloc(0, 0) are non-portable.  To be portable
allocate at least one byte.  (In general you should rarely need to work
at this low level, but instead use the various malloc wrappers.)

- \(bu
**snprintf()** - the return type is unportable.  Use **my_snprintf()** instead.

### Security problems

Subsection "Security problems"
Last but not least, here are various tips for safer coding.
See also perlclib for libc/stdio replacements one should use.

- \(bu
Do not use **gets()**
.Sp
Or we will publicly ridicule you.  Seriously.

- \(bu
Do not use **tmpfile()**
.Sp
Use **mkstemp()** instead.

- \(bu
Do not use **strcpy()** or **strcat()** or **strncpy()** or **strncat()**
.Sp
Use **my_strlcpy()** and **my_strlcat()** instead: they either use the native
implementation, or Perl's own implementation (borrowed from the public
domain implementation of \s-1INN\s0).

- \(bu
Do not use **sprintf()** or **vsprintf()**
.Sp
If you really want just plain byte strings, use **my_snprintf()** and
**my_vsnprintf()** instead, which will try to use **snprintf()** and
**vsnprintf()** if those safer APIs are available.  If you want something
fancier than a plain byte string, use
\f(CW\*(C`Perl_form\*(C'() or SVs and
\f(CW\*(C`Perl_sv_catpvf()\*(C'.
.Sp
Note that glibc \f(CW\*(C`printf()\*(C', \f(CW\*(C`sprintf()\*(C', etc. are buggy before glibc
version 2.17.  They won't allow a \f(CW\*(C`%.s\*(C' format with a precision to
create a string that isn't valid \s-1UTF-8\s0 if the current underlying locale
of the program is \s-1UTF-8.\s0  What happens is that the \f(CW%s and its operand are
simply skipped without any notice.
<https://sourceware.org/bugzilla/show_bug.cgi?id=6530>.

- \(bu
Do not use **atoi()**
.Sp
Use **grok_atoUV()** instead.  **atoi()** has ill-defined behavior on overflows,
and cannot be used for incremental parsing.  It is also affected by locale,
which is bad.

- \(bu
Do not use **strtol()** or **strtoul()**
.Sp
Use **grok_atoUV()** instead.  **strtol()** or **strtoul()** (or their IV/UV-friendly
macro disguises, **Strtol()** and **Strtoul()**, or **Atol()** and **Atoul()** are
affected by locale, which is bad.

## DEBUGGING

Header "DEBUGGING"
You can compile a special debugging version of Perl, which allows you
to use the \f(CW\*(C`-D\*(C' option of Perl to tell more about what Perl is doing.
But sometimes there is no alternative than to dive in with a debugger,
either to see the stack trace of a core dump (very useful in a bug
report), or trying to figure out what went wrong before the core dump
happened, or how did we end up having wrong or unexpected results.

### Poking at Perl

Subsection "Poking at Perl"
To really poke around with Perl, you'll probably want to build Perl for
debugging, like this:

.Vb 2
    ./Configure -d -DDEBUGGING
    make
.Ve

\f(CW\*(C`-DDEBUGGING\*(C' turns on the C compiler's \f(CW\*(C`-g\*(C' flag to have it produce
debugging information which will allow us to step through a running
program, and to see in which C function we are at (without the debugging
information we might see only the numerical addresses of the functions,
which is not very helpful). It will also turn on the \f(CW\*(C`DEBUGGING\*(C'
compilation symbol which enables all the internal debugging code in Perl.
There are a whole bunch of things you can debug with this:
perlrun lists them all, and the best way to find out
about them is to play about with them.  The most useful options are
probably

.Vb 5
    l  Context (loop) stack processing
    s  Stack snapshots (with v, displays all stacks)
    t  Trace execution
    o  Method and overloading resolution
    c  String/numeric conversions
.Ve

For example

.Vb 8
    $ perl -Dst -e \*(Aq$a + 1\*(Aq
    ....
    (-e:1)      gvsv(main::a)
        =>  UNDEF
    (-e:1)      const(IV(1))
        =>  UNDEF  IV(1)
    (-e:1)      add
        =>  NV(1)
.Ve

Some of the functionality of the debugging code can be achieved with a
non-debugging perl by using \s-1XS\s0 modules:

.Vb 2
    -Dr => use re \*(Aqdebug\*(Aq
    -Dx => use O \*(AqDebug\*(Aq
.Ve

### Using a source-level debugger

Subsection "Using a source-level debugger"
If the debugging output of \f(CW\*(C`-D\*(C' doesn't help you, it's time to step
through perl's execution with a source-level debugger.

- \(bu
We'll use \f(CW\*(C`gdb\*(C' for our examples here; the principles will apply to
any debugger (many vendors call their debugger \f(CW\*(C`dbx\*(C'), but check the
manual of the one you're using.

To fire up the debugger, type

.Vb 1
    gdb ./perl
.Ve

Or if you have a core dump:

.Vb 1
    gdb ./perl core
.Ve

You'll want to do that in your Perl source tree so the debugger can
read the source code.  You should see the copyright message, followed by
the prompt.

.Vb 1
    (gdb)
.Ve

\f(CW\*(C`help\*(C' will get you into the documentation, but here are the most
useful commands:

- \(bu
run [args]
.Sp
Run the program with the given arguments.

- \(bu
break function_name

- \(bu
break source.c:xxx
.Sp
Tells the debugger that we'll want to pause execution when we reach
either the named function (but see \*(L"Internal Functions\*(R" in perlguts!) or
the given line in the named source file.

- \(bu
step
.Sp
Steps through the program a line at a time.

- \(bu
next
.Sp
Steps through the program a line at a time, without descending into
functions.

- \(bu
continue
.Sp
Run until the next breakpoint.

- \(bu
finish
.Sp
Run until the end of the current function, then stop again.

- \(bu
'enter'
.Sp
Just pressing Enter will do the most recent operation again - it's a
blessing when stepping through miles of source code.

- \(bu
ptype
.Sp
Prints the C definition of the argument given.
.Sp
.Vb 10
  (gdb) ptype PL_op
  type = struct op \{
      OP *op_next;
      OP *op_sibparent;
      OP *(*op_ppaddr)(void);
      PADOFFSET op_targ;
      unsigned int op_type : 9;
      unsigned int op_opt : 1;
      unsigned int op_slabbed : 1;
      unsigned int op_savefree : 1;
      unsigned int op_static : 1;
      unsigned int op_folded : 1;
      unsigned int op_spare : 2;
      U8 op_flags;
      U8 op_private;
  \} *
.Ve

- \(bu
print
.Sp
Execute the given C code and print its results.  **\s-1WARNING\s0**: Perl makes
heavy use of macros, and *gdb* does not necessarily support macros
(see later \*(L"gdb macro support\*(R").  You'll have to substitute them
yourself, or to invoke cpp on the source code files (see \*(L"The .i
Targets\*(R") So, for instance, you can't say
.Sp
.Vb 1
    print SvPV_nolen(sv)
.Ve
.Sp
but you have to say
.Sp
.Vb 1
    print Perl_sv_2pv_nolen(sv)
.Ve

You may find it helpful to have a \*(L"macro dictionary\*(R", which you can
produce by saying \f(CW\*(C`cpp -dM perl.c | sort\*(C'.  Even then, *cpp* won't
recursively apply those macros for you.

### gdb macro support

Subsection "gdb macro support"
Recent versions of *gdb* have fairly good macro support, but in order
to use it you'll need to compile perl with macro definitions included
in the debugging information.  Using *gcc* version 3.1, this means
configuring with \f(CW\*(C`-Doptimize=-g3\*(C'.  Other compilers might use a
different switch (if they support debugging macros at all).

### Dumping Perl Data Structures

Subsection "Dumping Perl Data Structures"
One way to get around this macro hell is to use the dumping functions
in *dump.c*; these work a little like an internal
Devel::Peek, but they also cover OPs and other
structures that you can't get at from Perl.  Let's take an example.
We'll use the \f(CW\*(C`$a = $b + $c\*(C' we used before, but give it a bit of
context: \f(CW\*(C`$b = "6XXXX"; $c = 2.3;\*(C'.  Where's a good place to stop and
poke around?

What about \f(CW\*(C`pp_add\*(C', the function we examined earlier to implement the
\f(CW\*(C`+\*(C' operator:

.Vb 2
    (gdb) break Perl_pp_add
    Breakpoint 1 at 0x46249f: file pp_hot.c, line 309.
.Ve

Notice we use \f(CW\*(C`Perl_pp_add\*(C' and not \f(CW\*(C`pp_add\*(C' - see
\*(L"Internal Functions\*(R" in perlguts.  With the breakpoint in place, we can
run our program:

.Vb 1
    (gdb) run -e \*(Aq$b = "6XXXX"; $c = 2.3; $a = $b + $c\*(Aq
.Ve

Lots of junk will go past as gdb reads in the relevant source files and
libraries, and then:

.Vb 5
    Breakpoint 1, Perl_pp_add () at pp_hot.c:309
    1396    dSP; dATARGET; bool useleft; SV *svl, *svr;
    (gdb) step
    311           dPOPTOPnnrl_ul;
    (gdb)
.Ve

We looked at this bit of code before, and we said that
\f(CW\*(C`dPOPTOPnnrl_ul\*(C' arranges for two \f(CW\*(C`NV\*(C's to be placed into \f(CW\*(C`left\*(C' and
\f(CW\*(C`right\*(C' - let's slightly expand it:

.Vb 3
 #define dPOPTOPnnrl_ul  NV right = POPn; \\
                         SV *leftsv = TOPs; \\
                         NV left = USE_LEFT(leftsv) ? SvNV(leftsv) : 0.0
.Ve

\f(CW\*(C`POPn\*(C' takes the \s-1SV\s0 from the top of the stack and obtains its \s-1NV\s0
either directly (if \f(CW\*(C`SvNOK\*(C' is set) or by calling the \f(CW\*(C`sv_2nv\*(C'
function.  \f(CW\*(C`TOPs\*(C' takes the next \s-1SV\s0 from the top of the stack - yes,
\f(CW\*(C`POPn\*(C' uses \f(CW\*(C`TOPs\*(C' - but doesn't remove it.  We then use \f(CW\*(C`SvNV\*(C' to
get the \s-1NV\s0 from \f(CW\*(C`leftsv\*(C' in the same way as before - yes, \f(CW\*(C`POPn\*(C' uses
\f(CW\*(C`SvNV\*(C'.

Since we don't have an \s-1NV\s0 for \f(CW$b, we'll have to use \f(CW\*(C`sv_2nv\*(C' to
convert it.  If we step again, we'll find ourselves there:

.Vb 4
    (gdb) step
    Perl_sv_2nv (sv=0xa0675d0) at sv.c:1669
    1669        if (!sv)
    (gdb)
.Ve

We can now use \f(CW\*(C`Perl_sv_dump\*(C' to investigate the \s-1SV:\s0

.Vb 8
    (gdb) print Perl_sv_dump(sv)
    SV = PV(0xa057cc0) at 0xa0675d0
    REFCNT = 1
    FLAGS = (POK,pPOK)
    PV = 0xa06a510 "6XXXX"\\0
    CUR = 5
    LEN = 6
    $1 = void
.Ve

We know we're going to get \f(CW6 from this, so let's finish the
subroutine:

.Vb 4
    (gdb) finish
    Run till exit from #0  Perl_sv_2nv (sv=0xa0675d0) at sv.c:1671
    0x462669 in Perl_pp_add () at pp_hot.c:311
    311           dPOPTOPnnrl_ul;
.Ve

We can also dump out this op: the current op is always stored in
\f(CW\*(C`PL_op\*(C', and we can dump it with \f(CW\*(C`Perl_op_dump\*(C'.  This'll give us
similar output to \s-1CPAN\s0 module B::Debug.

.Vb 10
    (gdb) print Perl_op_dump(PL_op)
    \{
    13  TYPE = add  ===> 14
        TARG = 1
        FLAGS = (SCALAR,KIDS)
        \{
            TYPE = null  ===> (12)
              (was rv2sv)
            FLAGS = (SCALAR,KIDS)
            \{
    11          TYPE = gvsv  ===> 12
                FLAGS = (SCALAR)
                GV = main::b
            \}
        \}
.Ve

# finish this later #

### Using gdb to look at specific parts of a program

Subsection "Using gdb to look at specific parts of a program"
With the example above, you knew to look for \f(CW\*(C`Perl_pp_add\*(C', but what if
there were multiple calls to it all over the place, or you didn't know what
the op was you were looking for?

One way to do this is to inject a rare call somewhere near what you're looking
for.  For example, you could add \f(CW\*(C`study\*(C' before your method:

.Vb 1
    study;
.Ve

And in gdb do:

.Vb 1
    (gdb) break Perl_pp_study
.Ve

And then step until you hit what you're
looking for.  This works well in a loop
if you want to only break at certain iterations:

.Vb 3
    for my $c (1..100) \{
        study if $c == 50;
    \}
.Ve

### Using gdb to look at what the parser/lexer are doing

Subsection "Using gdb to look at what the parser/lexer are doing"
If you want to see what perl is doing when parsing/lexing your code, you can
use \f(CW\*(C`BEGIN \{\}\*(C':

.Vb 3
    print "Before\\n";
    BEGIN \{ study; \}
    print "After\\n";
.Ve

And in gdb:

.Vb 1
    (gdb) break Perl_pp_study
.Ve

If you want to see what the parser/lexer is doing inside of \f(CW\*(C`if\*(C' blocks and
the like you need to be a little trickier:

.Vb 1
    if ($a && $b && do \{ BEGIN \{ study \} 1 \} && $c) \{ ... \}
.Ve

## SOURCE CODE STATIC ANALYSIS

Header "SOURCE CODE STATIC ANALYSIS"
Various tools exist for analysing C source code **statically**, as
opposed to **dynamically**, that is, without executing the code.  It is
possible to detect resource leaks, undefined behaviour, type
mismatches, portability problems, code paths that would cause illegal
memory accesses, and other similar problems by just parsing the C code
and looking at the resulting graph, what does it tell about the
execution and data flows.  As a matter of fact, this is exactly how C
compilers know to give warnings about dubious code.

### lint

Subsection "lint"
The good old C code quality inspector, \f(CW\*(C`lint\*(C', is available in several
platforms, but please be aware that there are several different
implementations of it by different vendors, which means that the flags
are not identical across different platforms.

There is a \f(CW\*(C`lint\*(C' target in Makefile, but you may have to
diddle with the flags (see above).

### Coverity

Subsection "Coverity"
Coverity (<http://www.coverity.com/>) is a product similar to lint and as
a testbed for their product they periodically check several open source
projects, and they give out accounts to open source developers to the
defect databases.

There is Coverity setup for the perl5 project:
<https://scan.coverity.com/projects/perl5>

### HP-UX cadvise (Code Advisor)

Subsection "HP-UX cadvise (Code Advisor)"
\s-1HP\s0 has a C/\*(C+ static analyzer product for HP-UX caller Code Advisor.
(Link not given here because the \s-1URL\s0 is horribly long and seems horribly
unstable; use the search engine of your choice to find it.)  The use of
the \f(CW\*(C`cadvise_cc\*(C' recipe with \f(CW\*(C`Configure ... -Dcc=./cadvise_cc\*(C'
(see cadvise \*(L"User Guide\*(R") is recommended; as is the use of \f(CW\*(C`+wall\*(C'.

### cpd (cut-and-paste detector)

Subsection "cpd (cut-and-paste detector)"
The cpd tool detects cut-and-paste coding.  If one instance of the
cut-and-pasted code changes, all the other spots should probably be
changed, too.  Therefore such code should probably be turned into a
subroutine or a macro.

cpd (<http://pmd.sourceforge.net/cpd.html>) is part of the pmd project
(<http://pmd.sourceforge.net/>).  pmd was originally written for static
analysis of Java code, but later the cpd part of it was extended to
parse also C and \*(C+.

Download the pmd-bin-X.Y.zip () from the SourceForge site, extract the
pmd-X.Y.jar from it, and then run that on source code thusly:

.Vb 2
  java -cp pmd-X.Y.jar net.sourceforge.pmd.cpd.CPD \\
   --minimum-tokens 100 --files /some/where/src --language c > cpd.txt
.Ve

You may run into memory limits, in which case you should use the -Xmx
option:

.Vb 1
  java -Xmx512M ...
.Ve

### gcc warnings

Subsection "gcc warnings"
Though much can be written about the inconsistency and coverage
problems of gcc warnings (like \f(CW\*(C`-Wall\*(C' not meaning \*(L"all the warnings\*(R",
or some common portability problems not being covered by \f(CW\*(C`-Wall\*(C', or
\f(CW\*(C`-ansi\*(C' and \f(CW\*(C`-pedantic\*(C' both being a poorly defined collection of
warnings, and so forth), gcc is still a useful tool in keeping our
coding nose clean.

The \f(CW\*(C`-Wall\*(C' is by default on.

The \f(CW\*(C`-ansi\*(C' (and its sidekick, \f(CW\*(C`-pedantic\*(C') would be nice to be on
always, but unfortunately they are not safe on all platforms, they can
for example cause fatal conflicts with the system headers (Solaris
being a prime example).  If Configure \f(CW\*(C`-Dgccansipedantic\*(C' is used, the
\f(CW\*(C`cflags\*(C' frontend selects \f(CW\*(C`-ansi -pedantic\*(C' for the platforms where
they are known to be safe.

The following extra flags are added:

- \(bu
\f(CW\*(C`-Wendif-labels\*(C'

- \(bu
\f(CW\*(C`-Wextra\*(C'

- \(bu
\f(CW\*(C`-Wc++-compat\*(C'

- \(bu
\f(CW\*(C`-Wwrite-strings\*(C'

- \(bu
\f(CW\*(C`-Werror=declaration-after-statement\*(C'

- \(bu
\f(CW\*(C`-Werror=pointer-arith\*(C'

The following flags would be nice to have but they would first need
their own Augean stablemaster:

- \(bu
\f(CW\*(C`-Wshadow\*(C'

- \(bu
\f(CW\*(C`-Wstrict-prototypes\*(C'

The \f(CW\*(C`-Wtraditional\*(C' is another example of the annoying tendency of gcc
to bundle a lot of warnings under one switch (it would be impossible to
deploy in practice because it would complain a lot) but it does contain
some warnings that would be beneficial to have available on their own,
such as the warning about string constants inside macros containing the
macro arguments: this behaved differently pre-ANSI than it does in
\s-1ANSI,\s0 and some C compilers are still in transition, \s-1AIX\s0 being an
example.

### Warnings of other C compilers

Subsection "Warnings of other C compilers"
Other C compilers (yes, there **are** other C compilers than gcc) often
have their \*(L"strict \s-1ANSI\*(R"\s0 or \*(L"strict \s-1ANSI\s0 with some portability
extensions\*(R" modes on, like for example the Sun Workshop has its \f(CW\*(C`-Xa\*(C'
mode on (though implicitly), or the \s-1DEC\s0 (these days, \s-1HP...\s0) has its
\f(CW\*(C`-std1\*(C' mode on.

## MEMORY DEBUGGERS

Header "MEMORY DEBUGGERS"
**\s-1NOTE 1\s0**: Running under older memory debuggers such as Purify,
valgrind or Third Degree greatly slows down the execution: seconds
become minutes, minutes become hours.  For example as of Perl 5.8.1, the
ext/Encode/t/Unicode.t takes extraordinarily long to complete under
e.g. Purify, Third Degree, and valgrind.  Under valgrind it takes more
than six hours, even on a snappy computer.  The said test must be doing
something that is quite unfriendly for memory debuggers.  If you don't
feel like waiting, that you can simply kill away the perl process.
Roughly valgrind slows down execution by factor 10, AddressSanitizer by
factor 2.

**\s-1NOTE 2\s0**: To minimize the number of memory leak false alarms (see
\*(L"\s-1PERL_DESTRUCT_LEVEL\*(R"\s0 for more information), you have to set the
environment variable \s-1PERL_DESTRUCT_LEVEL\s0 to 2.  For example, like this:

.Vb 1
    env PERL_DESTRUCT_LEVEL=2 valgrind ./perl -Ilib ...
.Ve

**\s-1NOTE 3\s0**: There are known memory leaks when there are compile-time
errors within eval or require, seeing \f(CW\*(C`S_doeval\*(C' in the call stack is
a good sign of these.  Fixing these leaks is non-trivial, unfortunately,
but they must be fixed eventually.

**\s-1NOTE 4\s0**: DynaLoader will not clean up after itself completely
unless Perl is built with the Configure option
\f(CW\*(C`-Accflags=-DDL_UNLOAD_ALL_AT_EXIT\*(C'.

### valgrind

Subsection "valgrind"
The valgrind tool can be used to find out both memory leaks and illegal
heap memory accesses.  As of version 3.3.0, Valgrind only supports Linux
on x86, x86-64 and PowerPC and Darwin (\s-1OS X\s0) on x86 and x86-64.  The
special \*(L"test.valgrind\*(R" target can be used to run the tests under
valgrind.  Found errors and memory leaks are logged in files named
*testfile.valgrind* and by default output is displayed inline.

Example usage:

.Vb 1
    make test.valgrind
.Ve

Since valgrind adds significant overhead, tests will take much longer to
run.  The valgrind tests support being run in parallel to help with this:

.Vb 1
    TEST_JOBS=9 make test.valgrind
.Ve

Note that the above two invocations will be very verbose as reachable
memory and leak-checking is enabled by default.  If you want to just see
pure errors, try:

.Vb 2
    VG_OPTS=\*(Aq-q --leak-check=no --show-reachable=no\*(Aq TEST_JOBS=9 \\
        make test.valgrind
.Ve

Valgrind also provides a cachegrind tool, invoked on perl as:

.Vb 1
    VG_OPTS=--tool=cachegrind make test.valgrind
.Ve

As system libraries (most notably glibc) are also triggering errors,
valgrind allows to suppress such errors using suppression files.  The
default suppression file that comes with valgrind already catches a lot
of them.  Some additional suppressions are defined in *t/perl.supp*.

To get valgrind and for more information see

.Vb 1
    http://valgrind.org/
.Ve

### AddressSanitizer

Subsection "AddressSanitizer"
AddressSanitizer (\*(L"ASan\*(R") consists of a compiler instrumentation module
and a run-time \f(CW\*(C`malloc\*(C' library. ASan is available for a variety of
architectures, operating systems, and compilers (see project link below).
It checks for unsafe memory usage, such as use after free and buffer
overflow conditions, and is fast enough that you can easily compile your
debugging or optimized perl with it. Modern versions of ASan check for
memory leaks by default on most platforms, otherwise (e.g. x86_64 \s-1OS X\s0)
this feature can be enabled via \f(CW\*(C`ASAN_OPTIONS=detect_leaks=1\*(C'.

To build perl with AddressSanitizer, your Configure invocation should
look like:

.Vb 4
    sh Configure -des -Dcc=clang \\
       -Accflags=-fsanitize=address -Aldflags=-fsanitize=address \\
       -Alddlflags=-shared\\ -fsanitize=address \\
       -fsanitize-blacklist=\`pwd\`/asan_ignore
.Ve

where these arguments mean:

- \(bu
-Dcc=clang
.Sp
This should be replaced by the full path to your clang executable if it
is not in your path.

- \(bu
-Accflags=-fsanitize=address
.Sp
Compile perl and extensions sources with AddressSanitizer.

- \(bu
-Aldflags=-fsanitize=address
.Sp
Link the perl executable with AddressSanitizer.

- \(bu
-Alddlflags=-shared\\ -fsanitize=address
.Sp
Link dynamic extensions with AddressSanitizer.  You must manually
specify \f(CW\*(C`-shared\*(C' because using \f(CW\*(C`-Alddlflags=-shared\*(C' will prevent
Configure from setting a default value for \f(CW\*(C`lddlflags\*(C', which usually
contains \f(CW\*(C`-shared\*(C' (at least on Linux).

- \(bu
-fsanitize-blacklist=`pwd`/asan_ignore
.Sp
AddressSanitizer will ignore functions listed in the \f(CW\*(C`asan_ignore\*(C'
file. (This file should contain a short explanation of why each of
the functions is listed.)

See also
<https://github.com/google/sanitizers/wiki/AddressSanitizer>.

## PROFILING

Header "PROFILING"
Depending on your platform there are various ways of profiling Perl.

There are two commonly used techniques of profiling executables:
*statistical time-sampling* and *basic-block counting*.

The first method takes periodically samples of the \s-1CPU\s0 program counter,
and since the program counter can be correlated with the code generated
for functions, we get a statistical view of in which functions the
program is spending its time.  The caveats are that very small/fast
functions have lower probability of showing up in the profile, and that
periodically interrupting the program (this is usually done rather
frequently, in the scale of milliseconds) imposes an additional
overhead that may skew the results.  The first problem can be alleviated
by running the code for longer (in general this is a good idea for
profiling), the second problem is usually kept in guard by the
profiling tools themselves.

The second method divides up the generated code into *basic blocks*.
Basic blocks are sections of code that are entered only in the
beginning and exited only at the end.  For example, a conditional jump
starts a basic block.  Basic block profiling usually works by
*instrumenting* the code by adding *enter basic block #nnnn*
book-keeping code to the generated code.  During the execution of the
code the basic block counters are then updated appropriately.  The
caveat is that the added extra code can skew the results: again, the
profiling tools usually try to factor their own effects out of the
results.

### Gprof Profiling

Subsection "Gprof Profiling"
*gprof* is a profiling tool available in many Unix platforms which
uses *statistical time-sampling*.  You can build a profiled version of
*perl* by compiling using gcc with the flag \f(CW\*(C`-pg\*(C'.  Either edit
*config.sh* or re-run *Configure*.  Running the profiled version of
Perl will create an output file called *gmon.out* which contains the
profiling data collected during the execution.

quick hint:

.Vb 6
    $ sh Configure -des -Dusedevel -Accflags=\*(Aq-pg\*(Aq \\
        -Aldflags=\*(Aq-pg\*(Aq -Alddlflags=\*(Aq-pg -shared\*(Aq \\
        && make perl
    $ ./perl ... # creates gmon.out in current directory
    $ gprof ./perl > out
    $ less out
.Ve

(you probably need to add \f(CW\*(C`-shared\*(C' to the <-Alddlflags> line until \s-1RT\s0
#118199 is resolved)

The *gprof* tool can then display the collected data in various ways.
Usually *gprof* understands the following options:

- \(bu
-a
.Sp
Suppress statically defined functions from the profile.

- \(bu
-b
.Sp
Suppress the verbose descriptions in the profile.

- \(bu
-e routine
.Sp
Exclude the given routine and its descendants from the profile.

- \(bu
-f routine
.Sp
Display only the given routine and its descendants in the profile.

- \(bu
-s
.Sp
Generate a summary file called *gmon.sum* which then may be given to
subsequent gprof runs to accumulate data over several runs.

- \(bu
-z
.Sp
Display routines that have zero usage.

For more detailed explanation of the available commands and output
formats, see your own local documentation of *gprof*.

### \s-1GCC\s0 gcov Profiling

Subsection "GCC gcov Profiling"
*basic block profiling* is officially available in gcc 3.0 and later.
You can build a profiled version of *perl* by compiling using gcc with
the flags \f(CW\*(C`-fprofile-arcs -ftest-coverage\*(C'.  Either edit *config.sh*
or re-run *Configure*.

quick hint:

.Vb 9
    $ sh Configure -des -Dusedevel -Doptimize=\*(Aq-g\*(Aq \\
        -Accflags=\*(Aq-fprofile-arcs -ftest-coverage\*(Aq \\
        -Aldflags=\*(Aq-fprofile-arcs -ftest-coverage\*(Aq \\
        -Alddlflags=\*(Aq-fprofile-arcs -ftest-coverage -shared\*(Aq \\
        && make perl
    $ rm -f regexec.c.gcov regexec.gcda
    $ ./perl ...
    $ gcov regexec.c
    $ less regexec.c.gcov
.Ve

(you probably need to add \f(CW\*(C`-shared\*(C' to the <-Alddlflags> line until \s-1RT\s0
#118199 is resolved)

Running the profiled version of Perl will cause profile output to be
generated.  For each source file an accompanying *.gcda* file will be
created.

To display the results you use the *gcov* utility (which should be
installed if you have gcc 3.0 or newer installed).  *gcov* is run on
source code files, like this

.Vb 1
    gcov sv.c
.Ve

which will cause *sv.c.gcov* to be created.  The *.gcov* files contain
the source code annotated with relative frequencies of execution
indicated by \*(L"#\*(R" markers.  If you want to generate *.gcov* files for
all profiled object files, you can run something like this:

.Vb 3
    for file in \`find . -name \\*.gcno\`
    do sh -c "cd \`dirname $file\` && gcov \`basename $file .gcno\`"
    done
.Ve

Useful options of *gcov* include \f(CW\*(C`-b\*(C' which will summarise the basic
block, branch, and function call coverage, and \f(CW\*(C`-c\*(C' which instead of
relative frequencies will use the actual counts.  For more information
on the use of *gcov* and basic block profiling with gcc, see the
latest \s-1GNU CC\s0 manual.  As of gcc 4.8, this is at
<http://gcc.gnu.org/onlinedocs/gcc/Gcov-Intro.html#Gcov-Intro>

## MISCELLANEOUS TRICKS

Header "MISCELLANEOUS TRICKS"

### \s-1PERL_DESTRUCT_LEVEL\s0

Subsection "PERL_DESTRUCT_LEVEL"
If you want to run any of the tests yourself manually using e.g.
valgrind, please note that by default perl **does not** explicitly
cleanup all the memory it has allocated (such as global memory arenas)
but instead lets the **exit()** of the whole program \*(L"take care\*(R" of such
allocations, also known as \*(L"global destruction of objects\*(R".

There is a way to tell perl to do complete cleanup: set the environment
variable \s-1PERL_DESTRUCT_LEVEL\s0 to a non-zero value.  The t/TEST wrapper
does set this to 2, and this is what you need to do too, if you don't
want to see the \*(L"global leaks\*(R": For example, for running under valgrind

.Vb 1
    env PERL_DESTRUCT_LEVEL=2 valgrind ./perl -Ilib t/foo/bar.t
.Ve

(Note: the mod_perl apache module uses also this environment variable
for its own purposes and extended its semantics.  Refer to the mod_perl
documentation for more information.  Also, spawned threads do the
equivalent of setting this variable to the value 1.)

If, at the end of a run you get the message *N scalars leaked*, you
can recompile with \f(CW\*(C`-DDEBUG_LEAKING_SCALARS\*(C',
(\f(CW\*(C`Configure -Accflags=-DDEBUG_LEAKING_SCALARS\*(C'), which will cause the
addresses of all those leaked SVs to be dumped along with details as to
where each \s-1SV\s0 was originally allocated.  This information is also
displayed by Devel::Peek.  Note that the extra details recorded with
each \s-1SV\s0 increases memory usage, so it shouldn't be used in production
environments.  It also converts \f(CW\*(C`new_SV()\*(C' from a macro into a real
function, so you can use your favourite debugger to discover where
those pesky SVs were allocated.

If you see that you're leaking memory at runtime, but neither valgrind
nor \f(CW\*(C`-DDEBUG_LEAKING_SCALARS\*(C' will find anything, you're probably
leaking SVs that are still reachable and will be properly cleaned up
during destruction of the interpreter.  In such cases, using the \f(CW\*(C`-Dm\*(C'
switch can point you to the source of the leak.  If the executable was
built with \f(CW\*(C`-DDEBUG_LEAKING_SCALARS\*(C', \f(CW\*(C`-Dm\*(C' will output \s-1SV\s0
allocations in addition to memory allocations.  Each \s-1SV\s0 allocation has a
distinct serial number that will be written on creation and destruction
of the \s-1SV.\s0  So if you're executing the leaking code in a loop, you need
to look for SVs that are created, but never destroyed between each
cycle.  If such an \s-1SV\s0 is found, set a conditional breakpoint within
\f(CW\*(C`new_SV()\*(C' and make it break only when \f(CW\*(C`PL_sv_serial\*(C' is equal to the
serial number of the leaking \s-1SV.\s0  Then you will catch the interpreter in
exactly the state where the leaking \s-1SV\s0 is allocated, which is
sufficient in many cases to find the source of the leak.

As \f(CW\*(C`-Dm\*(C' is using the PerlIO layer for output, it will by itself
allocate quite a bunch of SVs, which are hidden to avoid recursion.  You
can bypass the PerlIO layer if you use the \s-1SV\s0 logging provided by
\f(CW\*(C`-DPERL_MEM_LOG\*(C' instead.

### \s-1PERL_MEM_LOG\s0

Subsection "PERL_MEM_LOG"
If compiled with \f(CW\*(C`-DPERL_MEM_LOG\*(C' (\f(CW\*(C`-Accflags=-DPERL_MEM_LOG\*(C'), both
memory and \s-1SV\s0 allocations go through logging functions, which is
handy for breakpoint setting.

Unless \f(CW\*(C`-DPERL_MEM_LOG_NOIMPL\*(C' (\f(CW\*(C`-Accflags=-DPERL_MEM_LOG_NOIMPL\*(C') is
also compiled, the logging functions read \f(CW$ENV\{\s-1PERL_MEM_LOG\s0\} to
determine whether to log the event, and if so how:

.Vb 4
    $ENV\{PERL_MEM_LOG\} =~ /m/           Log all memory ops
    $ENV\{PERL_MEM_LOG\} =~ /s/           Log all SV ops
    $ENV\{PERL_MEM_LOG\} =~ /t/           include timestamp in Log
    $ENV\{PERL_MEM_LOG\} =~ /^(\\d+)/      write to FD given (default is 2)
.Ve

Memory logging is somewhat similar to \f(CW\*(C`-Dm\*(C' but is independent of
\f(CW\*(C`-DDEBUGGING\*(C', and at a higher level; all uses of **Newx()**, **Renew()**, and
**Safefree()** are logged with the caller's source code file and line
number (and C function name, if supported by the C compiler).  In
contrast, \f(CW\*(C`-Dm\*(C' is directly at the point of \f(CW\*(C`malloc()\*(C'.  \s-1SV\s0 logging is
similar.

Since the logging doesn't use PerlIO, all \s-1SV\s0 allocations are logged and
no extra \s-1SV\s0 allocations are introduced by enabling the logging.  If
compiled with \f(CW\*(C`-DDEBUG_LEAKING_SCALARS\*(C', the serial number for each \s-1SV\s0
allocation is also logged.

### \s-1DDD\s0 over gdb

Subsection "DDD over gdb"
Those debugging perl with the \s-1DDD\s0 frontend over gdb may find the
following useful:

You can extend the data conversion shortcuts menu, so for example you
can display an \s-1SV\s0's \s-1IV\s0 value with one click, without doing any typing.
To do that simply edit ~/.ddd/init file and add after:

.Vb 6
  ! Display shortcuts.
  Ddd*gdbDisplayShortcuts: \\
  /t ()   // Convert to Bin\\n\\
  /d ()   // Convert to Dec\\n\\
  /x ()   // Convert to Hex\\n\\
  /o ()   // Convert to Oct(\\n\\
.Ve

the following two lines:

.Vb 2
  ((XPV*) (())->sv_any )->xpv_pv  // 2pvx\\n\\
  ((XPVIV*) (())->sv_any )->xiv_iv // 2ivx
.Ve

so now you can do ivx and pvx lookups or you can plug there the sv_peek
\*(L"conversion\*(R":

.Vb 1
  Perl_sv_peek(my_perl, (SV*)()) // sv_peek
.Ve

(The my_perl is for threaded builds.)  Just remember that every line,
but the last one, should end with \\n\\

Alternatively edit the init file interactively via: 3rd mouse button ->
New Display -> Edit Menu

Note: you can define up to 20 conversion shortcuts in the gdb section.

### C backtrace

Subsection "C backtrace"
On some platforms Perl supports retrieving the C level backtrace
(similar to what symbolic debuggers like gdb do).

The backtrace returns the stack trace of the C call frames,
with the symbol names (function names), the object names (like \*(L"perl\*(R"),
and if it can, also the source code locations (file:line).

The supported platforms are Linux, and \s-1OS X\s0 (some *BSD might
work at least partly, but they have not yet been tested).

This feature hasn't been tested with multiple threads, but it will
only show the backtrace of the thread doing the backtracing.

The feature needs to be enabled with \f(CW\*(C`Configure -Dusecbacktrace\*(C'.

The \f(CW\*(C`-Dusecbacktrace\*(C' also enables keeping the debug information when
compiling/linking (often: \f(CW\*(C`-g\*(C').  Many compilers/linkers do support
having both optimization and keeping the debug information.  The debug
information is needed for the symbol names and the source locations.

Static functions might not be visible for the backtrace.

Source code locations, even if available, can often be missing or
misleading if the compiler has e.g. inlined code.  Optimizer can
make matching the source code and the object code quite challenging.

- Linux
Item "Linux"
You **must** have the \s-1BFD\s0 (-lbfd) library installed, otherwise \f(CW\*(C`perl\*(C' will
fail to link.  The \s-1BFD\s0 is usually distributed as part of the \s-1GNU\s0 binutils.
.Sp
Summary: \f(CW\*(C`Configure ... -Dusecbacktrace\*(C'
and you need \f(CW\*(C`-lbfd\*(C'.

- \s-1OS X\s0
Item "OS X"
The source code locations are supported **only** if you have
the Developer Tools installed.  (\s-1BFD\s0 is **not** needed.)
.Sp
Summary: \f(CW\*(C`Configure ... -Dusecbacktrace\*(C'
and installing the Developer Tools would be good.

Optionally, for trying out the feature, you may want to enable
automatic dumping of the backtrace just before a warning or croak (die)
message is emitted, by adding \f(CW\*(C`-Accflags=-DUSE_C_BACKTRACE_ON_ERROR\*(C'
for Configure.

Unless the above additional feature is enabled, nothing about the
backtrace functionality is visible, except for the Perl/XS level.

Furthermore, even if you have enabled this feature to be compiled,
you need to enable it in runtime with an environment variable:
\f(CW\*(C`PERL_C_BACKTRACE_ON_ERROR=10\*(C'.  It must be an integer higher
than zero, telling the desired frame count.

Retrieving the backtrace from Perl level (using for example an \s-1XS\s0
extension) would be much less exciting than one would hope: normally
you would see \f(CW\*(C`runops\*(C', \f(CW\*(C`entersub\*(C', and not much else.  This \s-1API\s0 is
intended to be called **from within** the Perl implementation, not from
Perl level execution.

The C \s-1API\s0 for the backtrace is as follows:

- get_c_backtrace
Item "get_c_backtrace"
0

- free_c_backtrace
Item "free_c_backtrace"

- get_c_backtrace_dump
Item "get_c_backtrace_dump"

- dump_c_backtrace
Item "dump_c_backtrace"
.PD

### Poison

Subsection "Poison"
If you see in a debugger a memory area mysteriously full of 0xABABABAB
or 0xEFEFEFEF, you may be seeing the effect of the **Poison()** macros, see
perlclib.

### Read-only optrees

Subsection "Read-only optrees"
Under ithreads the optree is read only.  If you want to enforce this, to
check for write accesses from buggy code, compile with
\f(CW\*(C`-Accflags=-DPERL_DEBUG_READONLY_OPS\*(C'
to enable code that allocates op memory
via \f(CW\*(C`mmap\*(C', and sets it read-only when it is attached to a subroutine.
Any write access to an op results in a \f(CW\*(C`SIGBUS\*(C' and abort.

This code is intended for development only, and may not be portable
even to all Unix variants.  Also, it is an 80% solution, in that it
isn't able to make all ops read only.  Specifically it does not apply to
op slabs belonging to \f(CW\*(C`BEGIN\*(C' blocks.

However, as an 80% solution it is still effective, as it has caught
bugs in the past.

### When is a bool not a bool?

Subsection "When is a bool not a bool?"
On pre-C99 compilers, \f(CW\*(C`bool\*(C' is defined as equivalent to \f(CW\*(C`char\*(C'.
Consequently assignment of any larger type to a \f(CW\*(C`bool\*(C' is unsafe and may be
truncated.  The \f(CW\*(C`cBOOL\*(C' macro exists to cast it correctly; you may also find
that using it is shorter and clearer than writing out the equivalent
conditional expression longhand.

On those platforms and compilers where \f(CW\*(C`bool\*(C' really is a boolean (\*(C+,
C99), it is easy to forget the cast.  You can force \f(CW\*(C`bool\*(C' to be a \f(CW\*(C`char\*(C'
by compiling with \f(CW\*(C`-Accflags=-DPERL_BOOL_AS_CHAR\*(C'.  You may also wish to
run \f(CW\*(C`Configure\*(C' with something like

.Vb 1
    -Accflags=\*(Aq-Wconversion -Wno-sign-conversion -Wno-shorten-64-to-32\*(Aq
.Ve

or your compiler's equivalent to make it easier to spot any unsafe truncations
that show up.

The \f(CW\*(C`TRUE\*(C' and \f(CW\*(C`FALSE\*(C' macros are available for situations where using them
would clarify intent. (But they always just mean the same as the integers 1 and
0 regardless, so using them isn't compulsory.)

### The .i Targets

Subsection "The .i Targets"
You can expand the macros in a *foo.c* file by saying

.Vb 1
    make foo.i
.Ve

which will expand the macros using cpp.  Don't be scared by the
results.

## AUTHOR

Header "AUTHOR"
This document was originally written by Nathan Torkington, and is
maintained by the perl5-porters mailing list.
