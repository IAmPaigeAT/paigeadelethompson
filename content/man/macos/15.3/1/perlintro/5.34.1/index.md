+++
manpage_name = "perlintro"
operating_system = "macos"
operating_system_version = "15.3"
date = "2022-02-19"
title = "perlintro(1)"
manpage_section = "1"
author = "None Specified"
manpage_format = "troff"
detected_package_version = "5.34.1"
description = "This document is intended to give you a quick overview of the Perl programming language, along with pointers to further documentation.  It is intended as a *(Lbootstrap*(R guide for those who are new to the language, and provides just enough info..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLINTRO 1"
PERLINTRO 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlintro - a brief introduction and overview of Perl

## DESCRIPTION

Header "DESCRIPTION"
This document is intended to give you a quick overview of the Perl
programming language, along with pointers to further documentation.  It
is intended as a \*(L"bootstrap\*(R" guide for those who are new to the
language, and provides just enough information for you to be able to
read other peoples' Perl and understand roughly what it's doing, or
write your own simple scripts.

This introductory document does not aim to be complete.  It does not
even aim to be entirely accurate.  In some cases perfection has been
sacrificed in the goal of getting the general idea across.  You are
*strongly* advised to follow this introduction with more information
from the full Perl manual, the table of contents to which can be found
in perltoc.

Throughout this document you'll see references to other parts of the
Perl documentation.  You can read that documentation using the \f(CW\*(C`perldoc\*(C'
command or whatever method you're using to read this document.

Throughout Perl's documentation, you'll find numerous examples intended
to help explain the discussed features.  Please keep in mind that many
of them are code fragments rather than complete programs.

These examples often reflect the style and preference of the author of
that piece of the documentation, and may be briefer than a corresponding
line of code in a real program.  Except where otherwise noted, you
should assume that \f(CW\*(C`use strict\*(C' and \f(CW\*(C`use warnings\*(C' statements
appear earlier in the \*(L"program\*(R", and that any variables used have
already been declared, even if those declarations have been omitted
to make the example easier to read.

Do note that the examples have been written by many different authors over
a period of several decades.  Styles and techniques will therefore differ,
although some effort has been made to not vary styles too widely in the
same sections.  Do not consider one style to be better than others - \*(L"There's
More Than One Way To Do It\*(R" is one of Perl's mottos.  After all, in your
journey as a programmer, you are likely to encounter different styles.

### What is Perl?

Subsection "What is Perl?"
Perl is a general-purpose programming language originally developed for
text manipulation and now used for a wide range of tasks including
system administration, web development, network programming, \s-1GUI\s0
development, and more.

The language is intended to be practical (easy to use, efficient,
complete) rather than beautiful (tiny, elegant, minimal).  Its major
features are that it's easy to use, supports both procedural and
object-oriented (\s-1OO\s0) programming, has powerful built-in support for text
processing, and has one of the world's most impressive collections of
third-party modules.

Different definitions of Perl are given in perl, perlfaq1 and
no doubt other places.  From this we can determine that Perl is different
things to different people, but that lots of people think it's at least
worth writing about.

### Running Perl programs

Subsection "Running Perl programs"
To run a Perl program from the Unix command line:

.Vb 1
 perl progname.pl
.Ve

Alternatively, put this as the first line of your script:

.Vb 1
 #!/usr/bin/env perl
.Ve

... and run the script as */path/to/script.pl*.  Of course, it'll need
to be executable first, so \f(CW\*(C`chmod 755 script.pl\*(C' (under Unix).

(This start line assumes you have the **env** program.  You can also put
directly the path to your perl executable, like in \f(CW\*(C`#!/usr/bin/perl\*(C').

For more information, including instructions for other platforms such as
Windows and Mac \s-1OS,\s0 read perlrun.

### Safety net

Subsection "Safety net"
Perl by default is very forgiving.  In order to make it more robust
it is recommended to start every program with the following lines:

.Vb 3
 #!/usr/bin/perl
 use strict;
 use warnings;
.Ve

The two additional lines request from perl to catch various common
problems in your code.  They check different things so you need both.  A
potential problem caught by \f(CW\*(C`use strict;\*(C' will cause your code to stop
immediately when it is encountered, while \f(CW\*(C`use warnings;\*(C' will merely
give a warning (like the command-line switch **-w**) and let your code run.
To read more about them check their respective manual pages at strict
and warnings.

### Basic syntax overview

Subsection "Basic syntax overview"
A Perl script or program consists of one or more statements.  These
statements are simply written in the script in a straightforward
fashion.  There is no need to have a \f(CW\*(C`main()\*(C' function or anything of
that kind.

Perl statements end in a semi-colon:

.Vb 1
 print "Hello, world";
.Ve

Comments start with a hash symbol and run to the end of the line

.Vb 1
 # This is a comment
.Ve

Whitespace is irrelevant:

.Vb 3
 print
     "Hello, world"
     ;
.Ve

... except inside quoted strings:

.Vb 3
 # this would print with a linebreak in the middle
 print "Hello
 world";
.Ve

Double quotes or single quotes may be used around literal strings:

.Vb 2
 print "Hello, world";
 print \*(AqHello, world\*(Aq;
.Ve

However, only double quotes \*(L"interpolate\*(R" variables and special
characters such as newlines (\f(CW\*(C`\\n\*(C'):

.Vb 2
 print "Hello, $name\\n";     # works fine
 print \*(AqHello, $name\\n\*(Aq;     # prints $name\\n literally
.Ve

Numbers don't need quotes around them:

.Vb 1
 print 42;
.Ve

You can use parentheses for functions' arguments or omit them
according to your personal taste.  They are only required
occasionally to clarify issues of precedence.

.Vb 2
 print("Hello, world\\n");
 print "Hello, world\\n";
.Ve

More detailed information about Perl syntax can be found in perlsyn.

### Perl variable types

Subsection "Perl variable types"
Perl has three main variable types: scalars, arrays, and hashes.

- Scalars
Item "Scalars"
A scalar represents a single value:
.Sp
.Vb 2
 my $animal = "camel";
 my $answer = 42;
.Ve
.Sp
Scalar values can be strings, integers or floating point numbers, and Perl
will automatically convert between them as required.  There is no need
to pre-declare your variable types, but you have to declare them using
the \f(CW\*(C`my\*(C' keyword the first time you use them.  (This is one of the
requirements of \f(CW\*(C`use strict;\*(C'.)
.Sp
Scalar values can be used in various ways:
.Sp
.Vb 3
 print $animal;
 print "The animal is $animal\\n";
 print "The square of $answer is ", $answer * $answer, "\\n";
.Ve
.Sp
There are a number of \*(L"magic\*(R" scalars with names that look like
punctuation or line noise.  These special variables are used for all
kinds of purposes, and are documented in perlvar.  The only one you
need to know about for now is \f(CW$_ which is the \*(L"default variable\*(R".
It's used as the default argument to a number of functions in Perl, and
it's set implicitly by certain looping constructs.
.Sp
.Vb 1
 print;          # prints contents of $_ by default
.Ve

- Arrays
Item "Arrays"
An array represents a list of values:
.Sp
.Vb 3
 my @animals = ("camel", "llama", "owl");
 my @numbers = (23, 42, 69);
 my @mixed   = ("camel", 42, 1.23);
.Ve
.Sp
Arrays are zero-indexed.  Here's how you get at elements in an array:
.Sp
.Vb 2
 print $animals[0];              # prints "camel"
 print $animals[1];              # prints "llama"
.Ve
.Sp
The special variable \f(CW$#array tells you the index of the last element
of an array:
.Sp
.Vb 1
 print $mixed[$#mixed];       # last element, prints 1.23
.Ve
.Sp
You might be tempted to use \f(CW\*(C`$#array + 1\*(C' to tell you how many items there
are in an array.  Don't bother.  As it happens, using \f(CW@array where Perl
expects to find a scalar value (\*(L"in scalar context\*(R") will give you the number
of elements in the array:
.Sp
.Vb 1
 if (@animals < 5) \{ ... \}
.Ve
.Sp
The elements we're getting from the array start with a \f(CW\*(C`$\*(C' because
we're getting just a single value out of the array; you ask for a scalar,
you get a scalar.
.Sp
To get multiple values from an array:
.Sp
.Vb 3
 @animals[0,1];                 # gives ("camel", "llama");
 @animals[0..2];                # gives ("camel", "llama", "owl");
 @animals[1..$#animals];        # gives all except the first element
.Ve
.Sp
This is called an \*(L"array slice\*(R".
.Sp
You can do various useful things to lists:
.Sp
.Vb 2
 my @sorted    = sort @animals;
 my @backwards = reverse @numbers;
.Ve
.Sp
There are a couple of special arrays too, such as \f(CW@ARGV (the command
line arguments to your script) and \f(CW@_ (the arguments passed to a
subroutine).  These are documented in perlvar.

- Hashes
Item "Hashes"
A hash represents a set of key/value pairs:
.Sp
.Vb 1
 my %fruit_color = ("apple", "red", "banana", "yellow");
.Ve
.Sp
You can use whitespace and the \f(CW\*(C`=>\*(C' operator to lay them out more
nicely:
.Sp
.Vb 4
 my %fruit_color = (
     apple  => "red",
     banana => "yellow",
 );
.Ve
.Sp
To get at hash elements:
.Sp
.Vb 1
 $fruit_color\{"apple"\};           # gives "red"
.Ve
.Sp
You can get at lists of keys and values with \f(CW\*(C`keys()\*(C' and
\f(CW\*(C`values()\*(C'.
.Sp
.Vb 2
 my @fruits = keys %fruit_color;
 my @colors = values %fruit_color;
.Ve
.Sp
Hashes have no particular internal order, though you can sort the keys
and loop through them.
.Sp
Just like special scalars and arrays, there are also special hashes.
The most well known of these is \f(CW%ENV which contains environment
variables.  Read all about it (and other special variables) in
perlvar.

Scalars, arrays and hashes are documented more fully in perldata.

More complex data types can be constructed using references, which allow
you to build lists and hashes within lists and hashes.

A reference is a scalar value and can refer to any other Perl data
type.  So by storing a reference as the value of an array or hash
element, you can easily create lists and hashes within lists and
hashes.  The following example shows a 2 level hash of hash
structure using anonymous hash references.

.Vb 10
 my $variables = \{
     scalar  =>  \{
                  description => "single item",
                  sigil => \*(Aq$\*(Aq,
                 \},
     array   =>  \{
                  description => "ordered list of items",
                  sigil => \*(Aq@\*(Aq,
                 \},
     hash    =>  \{
                  description => "key/value pairs",
                  sigil => \*(Aq%\*(Aq,
                 \},
 \};

 print "Scalars begin with a $variables->\{\*(Aqscalar\*(Aq\}->\{\*(Aqsigil\*(Aq\}\\n";
.Ve

Exhaustive information on the topic of references can be found in
perlreftut, perllol, perlref and perldsc.

### Variable scoping

Subsection "Variable scoping"
Throughout the previous section all the examples have used the syntax:

.Vb 1
 my $var = "value";
.Ve

The \f(CW\*(C`my\*(C' is actually not required; you could just use:

.Vb 1
 $var = "value";
.Ve

However, the above usage will create global variables throughout your
program, which is bad programming practice.  \f(CW\*(C`my\*(C' creates lexically
scoped variables instead.  The variables are scoped to the block
(i.e. a bunch of statements surrounded by curly-braces) in which they
are defined.

.Vb 9
 my $x = "foo";
 my $some_condition = 1;
 if ($some_condition) \{
     my $y = "bar";
     print $x;           # prints "foo"
     print $y;           # prints "bar"
 \}
 print $x;               # prints "foo"
 print $y;               # prints nothing; $y has fallen out of scope
.Ve

Using \f(CW\*(C`my\*(C' in combination with a \f(CW\*(C`use strict;\*(C' at the top of
your Perl scripts means that the interpreter will pick up certain common
programming errors.  For instance, in the example above, the final
\f(CW\*(C`print $y\*(C' would cause a compile-time error and prevent you from
running the program.  Using \f(CW\*(C`strict\*(C' is highly recommended.

### Conditional and looping constructs

Subsection "Conditional and looping constructs"
Perl has most of the usual conditional and looping constructs.  As of Perl
5.10, it even has a case/switch statement (spelled \f(CW\*(C`given\*(C'/\f(CW\*(C`when\*(C').  See
\*(L"Switch Statements\*(R" in perlsyn for more details.

The conditions can be any Perl expression.  See the list of operators in
the next section for information on comparison and boolean logic operators,
which are commonly used in conditional statements.

- if
Item "if"
.Vb 7
 if ( condition ) \{
     ...
 \} elsif ( other condition ) \{
     ...
 \} else \{
     ...
 \}
.Ve
.Sp
There's also a negated version of it:
.Sp
.Vb 3
 unless ( condition ) \{
     ...
 \}
.Ve
.Sp
This is provided as a more readable version of \f(CW\*(C`if (!\f(CIcondition\f(CW)\*(C'.
.Sp
Note that the braces are required in Perl, even if you've only got one
line in the block.  However, there is a clever way of making your one-line
conditional blocks more English like:
.Sp
.Vb 4
 # the traditional way
 if ($zippy) \{
     print "Yow!";
 \}

 # the Perlish post-condition way
 print "Yow!" if $zippy;
 print "We have no bananas" unless $bananas;
.Ve

- while
Item "while"
.Vb 3
 while ( condition ) \{
     ...
 \}
.Ve
.Sp
There's also a negated version, for the same reason we have \f(CW\*(C`unless\*(C':
.Sp
.Vb 3
 until ( condition ) \{
     ...
 \}
.Ve
.Sp
You can also use \f(CW\*(C`while\*(C' in a post-condition:
.Sp
.Vb 1
 print "LA LA LA\\n" while 1;          # loops forever
.Ve

- for
Item "for"
Exactly like C:
.Sp
.Vb 3
 for ($i = 0; $i <= $max; $i++) \{
     ...
 \}
.Ve
.Sp
The C style for loop is rarely needed in Perl since Perl provides
the more friendly list scanning \f(CW\*(C`foreach\*(C' loop.

- foreach
Item "foreach"
.Vb 3
 foreach (@array) \{
     print "This element is $_\\n";
 \}

 print $list[$_] foreach 0 .. $max;

 # you don\*(Aqt have to use the default $_ either...
 foreach my $key (keys %hash) \{
     print "The value of $key is $hash\{$key\}\\n";
 \}
.Ve
.Sp
The \f(CW\*(C`foreach\*(C' keyword is actually a synonym for the \f(CW\*(C`for\*(C'
keyword.  See \f(CW\*(C`"Foreach Loops" in perlsyn\*(C'.

For more detail on looping constructs (and some that weren't mentioned in
this overview) see perlsyn.

### Builtin operators and functions

Subsection "Builtin operators and functions"
Perl comes with a wide selection of builtin functions.  Some of the ones
we've already seen include \f(CW\*(C`print\*(C', \f(CW\*(C`sort\*(C' and \f(CW\*(C`reverse\*(C'.  A list of
them is given at the start of perlfunc and you can easily read
about any given function by using \f(CW\*(C`perldoc -f \f(CIfunctionname\f(CW\*(C'.

Perl operators are documented in full in perlop, but here are a few
of the most common ones:

- Arithmetic
Item "Arithmetic"
.Vb 4
 +   addition
 -   subtraction
 *   multiplication
 /   division
.Ve

- Numeric comparison
Item "Numeric comparison"
.Vb 6
 ==  equality
 !=  inequality
 <   less than
 >   greater than
 <=  less than or equal
 >=  greater than or equal
.Ve

- String comparison
Item "String comparison"
.Vb 6
 eq  equality
 ne  inequality
 lt  less than
 gt  greater than
 le  less than or equal
 ge  greater than or equal
.Ve
.Sp
(Why do we have separate numeric and string comparisons?  Because we don't
have special variable types, and Perl needs to know whether to sort
numerically (where 99 is less than 100) or alphabetically (where 100 comes
before 99).

- Boolean logic
Item "Boolean logic"
.Vb 3
 &&  and
 ||  or
 !   not
.Ve
.Sp
(\f(CW\*(C`and\*(C', \f(CW\*(C`or\*(C' and \f(CW\*(C`not\*(C' aren't just in the above table as descriptions
of the operators.  They're also supported as operators in their own
right.  They're more readable than the C-style operators, but have
different precedence to \f(CW\*(C`&&\*(C' and friends.  Check perlop for more
detail.)

- Miscellaneous
Item "Miscellaneous"
.Vb 4
 =   assignment
 .   string concatenation
 x   string multiplication (repeats strings)
 ..  range operator (creates a list of numbers or strings)
.Ve

Many operators can be combined with a \f(CW\*(C`=\*(C' as follows:

.Vb 3
 $a += 1;        # same as $a = $a + 1
 $a -= 1;        # same as $a = $a - 1
 $a .= "\\n";     # same as $a = $a . "\\n";
.Ve

### Files and I/O

Subsection "Files and I/O"
You can open a file for input or output using the \f(CW\*(C`open()\*(C' function.
It's documented in extravagant detail in perlfunc and perlopentut,
but in short:

.Vb 3
 open(my $in,  "<",  "input.txt")  or die "Can\*(Aqt open input.txt: $!";
 open(my $out, ">",  "output.txt") or die "Can\*(Aqt open output.txt: $!";
 open(my $log, ">>", "my.log")     or die "Can\*(Aqt open my.log: $!";
.Ve

You can read from an open filehandle using the \f(CW\*(C`<>\*(C' operator.  In
scalar context it reads a single line from the filehandle, and in list
context it reads the whole file in, assigning each line to an element of
the list:

.Vb 2
 my $line  = <$in>;
 my @lines = <$in>;
.Ve

Reading in the whole file at one time is called slurping.  It can
be useful but it may be a memory hog.  Most text file processing
can be done a line at a time with Perl's looping constructs.

The \f(CW\*(C`<>\*(C' operator is most often seen in a \f(CW\*(C`while\*(C' loop:

.Vb 3
 while (<$in>) \{     # assigns each line in turn to $_
     print "Just read in this line: $_";
 \}
.Ve

We've already seen how to print to standard output using \f(CW\*(C`print()\*(C'.
However, \f(CW\*(C`print()\*(C' can also take an optional first argument specifying
which filehandle to print to:

.Vb 3
 print STDERR "This is your final warning.\\n";
 print $out $record;
 print $log $logmessage;
.Ve

When you're done with your filehandles, you should \f(CW\*(C`close()\*(C' them
(though to be honest, Perl will clean up after you if you forget):

.Vb 1
 close $in or die "$in: $!";
.Ve

### Regular expressions

Subsection "Regular expressions"
Perl's regular expression support is both broad and deep, and is the
subject of lengthy documentation in perlrequick, perlretut, and
elsewhere.  However, in short:

- Simple matching
Item "Simple matching"
.Vb 2
 if (/foo/)       \{ ... \}  # true if $_ contains "foo"
 if ($a =~ /foo/) \{ ... \}  # true if $a contains "foo"
.Ve
.Sp
The \f(CW\*(C`//\*(C' matching operator is documented in perlop.  It operates on
\f(CW$_ by default, or can be bound to another variable using the \f(CW\*(C`=~\*(C'
binding operator (also documented in perlop).

- Simple substitution
Item "Simple substitution"
.Vb 4
 s/foo/bar/;               # replaces foo with bar in $_
 $a =~ s/foo/bar/;         # replaces foo with bar in $a
 $a =~ s/foo/bar/g;        # replaces ALL INSTANCES of foo with bar
                           # in $a
.Ve
.Sp
The \f(CW\*(C`s///\*(C' substitution operator is documented in perlop.

- More complex regular expressions
Item "More complex regular expressions"
You don't just have to match on fixed strings.  In fact, you can match
on just about anything you could dream of by using more complex regular
expressions.  These are documented at great length in perlre, but for
the meantime, here's a quick cheat sheet:
.Sp
.Vb 12
 .                   a single character
 \\s                  a whitespace character (space, tab, newline,
                     ...)
 \\S                  non-whitespace character
 \\d                  a digit (0-9)
 \\D                  a non-digit
 \\w                  a word character (a-z, A-Z, 0-9, _)
 \\W                  a non-word character
 [aeiou]             matches a single character in the given set
 [^aeiou]            matches a single character outside the given
                     set
 (foo|bar|baz)       matches any of the alternatives specified

 ^                   start of string
 $                   end of string
.Ve
.Sp
Quantifiers can be used to specify how many of the previous thing you
want to match on, where \*(L"thing\*(R" means either a literal character, one
of the metacharacters listed above, or a group of characters or
metacharacters in parentheses.
.Sp
.Vb 6
 *                   zero or more of the previous thing
 +                   one or more of the previous thing
 ?                   zero or one of the previous thing
 \{3\}                 matches exactly 3 of the previous thing
 \{3,6\}               matches between 3 and 6 of the previous thing
 \{3,\}                matches 3 or more of the previous thing
.Ve
.Sp
Some brief examples:
.Sp
.Vb 7
 /^\\d+/              string starts with one or more digits
 /^$/                nothing in the string (start and end are
                     adjacent)
 /(\\d\\s)\{3\}/         three digits, each followed by a whitespace
                     character (eg "3 4 5 ")
 /(a.)+/             matches a string in which every odd-numbered
                     letter is a (eg "abacadaf")

 # This loop reads from STDIN, and prints non-blank lines:
 while (<>) \{
     next if /^$/;
     print;
 \}
.Ve

- Parentheses for capturing
Item "Parentheses for capturing"
As well as grouping, parentheses serve a second purpose.  They can be
used to capture the results of parts of the regexp match for later use.
The results end up in \f(CW$1, \f(CW$2 and so on.
.Sp
.Vb 1
 # a cheap and nasty way to break an email address up into parts

 if ($email =~ /([^@]+)@(.+)/) \{
     print "Username is $1\\n";
     print "Hostname is $2\\n";
 \}
.Ve

- Other regexp features
Item "Other regexp features"
Perl regexps also support backreferences, lookaheads, and all kinds of
other complex details.  Read all about them in perlrequick,
perlretut, and perlre.

### Writing subroutines

Subsection "Writing subroutines"
Writing subroutines is easy:

.Vb 5
 sub logger \{
    my $logmessage = shift;
    open my $logfile, ">>", "my.log" or die "Could not open my.log: $!";
    print $logfile $logmessage;
 \}
.Ve

Now we can use the subroutine just as any other built-in function:

.Vb 1
 logger("We have a logger subroutine!");
.Ve

What's that \f(CW\*(C`shift\*(C'?  Well, the arguments to a subroutine are available
to us as a special array called \f(CW@_ (see perlvar for more on that).
The default argument to the \f(CW\*(C`shift\*(C' function just happens to be \f(CW@_.
So \f(CW\*(C`my $logmessage = shift;\*(C' shifts the first item off the list of
arguments and assigns it to \f(CW$logmessage.

We can manipulate \f(CW@_ in other ways too:

.Vb 2
 my ($logmessage, $priority) = @_;       # common
 my $logmessage = $_[0];                 # uncommon, and ugly
.Ve

Subroutines can also return values:

.Vb 5
 sub square \{
     my $num = shift;
     my $result = $num * $num;
     return $result;
 \}
.Ve

Then use it like:

.Vb 1
 $sq = square(8);
.Ve

For more information on writing subroutines, see perlsub.

### \s-1OO\s0 Perl

Subsection "OO Perl"
\s-1OO\s0 Perl is relatively simple and is implemented using references which
know what sort of object they are based on Perl's concept of packages.
However, \s-1OO\s0 Perl is largely beyond the scope of this document.
Read perlootut and perlobj.

As a beginning Perl programmer, your most common use of \s-1OO\s0 Perl will be
in using third-party modules, which are documented below.

### Using Perl modules

Subsection "Using Perl modules"
Perl modules provide a range of features to help you avoid reinventing
the wheel, and can be downloaded from \s-1CPAN\s0 ( <http://www.cpan.org/> ).  A
number of popular modules are included with the Perl distribution
itself.

Categories of modules range from text manipulation to network protocols
to database integration to graphics.  A categorized list of modules is
also available from \s-1CPAN.\s0

To learn how to install modules you download from \s-1CPAN,\s0 read
perlmodinstall.

To learn how to use a particular module, use \f(CW\*(C`perldoc \f(CIModule::Name\f(CW\*(C'.
Typically you will want to \f(CW\*(C`use \f(CIModule::Name\f(CW\*(C', which will then give
you access to exported functions or an \s-1OO\s0 interface to the module.

perlfaq contains questions and answers related to many common
tasks, and often provides suggestions for good \s-1CPAN\s0 modules to use.

perlmod describes Perl modules in general.  perlmodlib lists the
modules which came with your Perl installation.

If you feel the urge to write Perl modules, perlnewmod will give you
good advice.

## AUTHOR

Header "AUTHOR"
Kirrily \*(L"Skud\*(R" Robert <skud@cpan.org>
