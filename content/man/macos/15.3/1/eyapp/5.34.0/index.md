+++
author = "None Specified"
operating_system = "macos"
title = "eyapp(1)"
manpage_format = "troff"
operating_system_version = "15.3"
date = "2017-06-14"
manpage_section = "1"
keywords = ["header", "see", "also", "bu", "4", "parse", "eyapp", "perldoc", "vgg", "the", "tutorial", "fiparsing", "strings", "and", "trees", "with", "f", "cw", "c", "an", "introduction", "to", "compiler", "construction", "in", "seven", "pages", "pdf", "file", "http", "nereida", "deioc", "ull", "es", "pl", "perlexamples", "section_eyappts", "html", "spanish", "treereg", "yapp", "fbyacc", "1", "fbbison", "classic", "book", "l", "compilers", "principles", "techniques", "tools", "r", "by", "alfred", "v", "aho", "ravi", "sethi", "jeffrey", "d", "ullman", "addison-wesley", "1986", "recdescent", "pod", "errors", "hey", "fbthe", "above", "document", "had", "some", "coding", "which", "are", "explained", "below", "around", "line", "199", "item", "non-ascii", "character", "seen", "before", "encoding", "assuming", "s-1utf-8", "s0"]
description = "The eyapp compiler is a front-end to the Parse::Eyapp module, which lets you compile Parse::Eyapp grammar input files into Perl s-1LALRs0|(1) Object Oriented parser modules. Creates a file grammar.output describing your parser. It will show you a s..."
manpage_name = "eyapp"
detected_package_version = "5.34.0"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "EYAPP 1"
EYAPP 1 "2017-06-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

eyapp - A Perl front-end to the Parse::Eyapp module

## SYNOPSIS

Header "SYNOPSIS"
.Vb 3
    eyapp [options] grammar[.eyp]
    eyapp -V
    eyapp -h

        grammar     The grammar file. If no suffix is given, and the file
                    does not exists, .eyp is added
.Ve

## DESCRIPTION

Header "DESCRIPTION"
The eyapp compiler is a front-end to the Parse::Eyapp module, which lets you compile
Parse::Eyapp grammar input files into Perl \s-1**LALR\s0**\|(1) Object Oriented parser modules.

## OPTIONS IN DETAIL

Header "OPTIONS IN DETAIL"

- \fI-v
Item "-v"
Creates a file *grammar*.output describing your parser. It will
show you a summary of conflicts, rules, the \s-1DFA\s0 (Deterministic
Finite Automaton) states and overall usage of the parser.
.Sp
Implies option \f(CW\*(C`-N\*(C'. To produce a more detailed description of the states,
the \s-1LALR\s0 tables aren't compacted.
Use the combination \f(CW\*(C`-vN\*(C' to produce an  \f(CW\*(C`.output\*(C' file
corresponding to the compacted tables.

- \fI-s
Item "-s"
Create a standalone module in which the parsing driver is included.
The modules including the \s-1LALR\s0 driver (Parse::Eyapp::Driver),
those for \s-1AST\s0 manipulations (Parse::Eyapp::Node and
Parse::Eyapp::YATW)) and Parse::Eyapp::Base
are included - almost verbatim - inside the generated module.
.Sp
Note that if you have more than one parser module called from a program,
to have it standalone, you need this option only for one of your grammars;

- \fI-n
Item "-n"
Disable source file line numbering embedded in your parser module.
I don't know why one should need it, but it's there.

- \fI-m module
Item "-m module"
Gives your parser module the package name (or name space or module name or
class name or whatever-you-call-it) of *module*.  It defaults to *grammar*

- \fI-o outfile
Item "-o outfile"
The compiled output file will be named *outfile* for your parser module.
It defaults to *grammar*.pm or, if you specified the option
*-m A::Module::Name* (see below), to *Name.pm*.

- \fI-c grammar[.eyp]
Item "-c grammar[.eyp]"
Produces as output (\s-1STDOUT\s0) the grammar without the actions. Only the syntactic
parts are displayed. Comments will be also stripped
if the \f(CW\*(C`-v\*(C' option is added.

- \fI-t filename
Item "-t filename"
The *-t filename* option allows you to specify a file which should be
used as template for generating the parser output.  The default is to
use the internal template defined in *Parse::Eyapp::Output.pm*.
For how to write your own template and which substitutions are available,
have a look to the module *Parse::Eyapp::Output.pm* : it should be obvious.

- \fI-b shebang
Item "-b shebang"
If you work on systems that understand so called *shebangs*, and your
generated parser is directly an executable script, you can specify one
with the *-b* option, ie:
.Sp
.Vb 1
    eyapp -b \*(Aq/usr/local/bin/perl -w\*(Aq -o myscript.pl myscript.yp
.Ve
.Sp
This will output a file called *myscript.pl* whose very first line is:
.Sp
.Vb 1
    #!/usr/local/bin/perl -w
.Ve
.Sp
The argument is mandatory, but if you specify an empty string, the value
of *\f(CI$Config\fI\{perlpath\}* will be used instead.

- \fI-B prompt
Item "-B prompt"
Adds a modulino call '_\|_PACKAGE->main(<prompt>) unless **caller()**;'
as the very last line of the output file. The argument is mandatory.

- \fI-C grammar.eyp
Item "-C grammar.eyp"
An abbreviation for the combined use of *-b ''* and  *-B ''*

- \fI-T grammar.eyp
Item "-T grammar.eyp"
Equivalent to \f(CW%tree.

- \fI-N grammar.eyp
Item "-N grammar.eyp"
Equivalent to the directive \f(CW%nocompact. Do not compact \s-1LALR\s0
action tables.

- \fI-l
Item "-l"
Do not provide a default lexical analyzer. By default \f(CW\*(C`eyapp\*(C'
builds a lexical analyzer from your \f(CW\*(C`%token = /regexp/\*(C' definitions

- \fIgrammar
Item "grammar"
The input grammar file. If no suffix is given, and the file does not exists,
an attempt to open the file with a suffix of  *.eyp* is tried before exiting.

- \fI-V
Item "-V"
Display current version of Parse::Eyapp and gracefully exits.

- \fI-h
Item "-h"
Display the usage screen.

## EXAMPLE

Header "EXAMPLE"
The following \f(CW\*(C`eyapp\*(C' program translates an infix expression
like \f(CW\*(C`2+3*4\*(C' to postfix: \f(CW\*(C`2 3 4 * +\*(C'

.Vb 2
    %token NUM = /([0-9]+(?:\\.[0-9]+)?)/
    %token VAR = /([A-Za-z][A-Za-z0-9_]*)/

    %right  \*(Aq=\*(Aq
    %left   \*(Aq-\*(Aq \*(Aq+\*(Aq
    %left   \*(Aq*\*(Aq \*(Aq/\*(Aq
    %left   NEG

    %defaultaction \{ "$left $right $op"; \}

    %%
    line: $exp  \{ print "$exp\\n" \}
    ;

    exp:        $NUM  \{ $NUM \}
            |   $VAR  \{ $VAR \}
            |   VAR.left \*(Aq=\*(Aq.op exp.right
            |   exp.left \*(Aq+\*(Aq.op exp.right
            |   exp.left \*(Aq-\*(Aq.op exp.right
            |   exp.left \*(Aq*\*(Aq.op exp.right
            |   exp.left \*(Aq/\*(Aq.op exp.right
            |   \*(Aq-\*(Aq $exp %prec NEG \{ "$exp NEG" \}
            |   \*(Aq(\*(Aq $exp \*(Aq)\*(Aq \{ $exp \}
    ;

    %%
.Ve

Notice that there is no need to write lexer and error report subroutines.
First, we compile the grammar:

.Vb 1
    pl@nereida:~/LEyapp/examples/eyappintro$ eyapp -o postfix.pl -C Postfix.eyp
.Ve

If we use the \f(CW\*(C`-C\*(C' option and no \f(CW\*(C`main()\*(C' was written one default \f(CW\*(C`main\*(C' sub is provided.
We can now execute the resulting program:

.Vb 2
    pl@nereida:~/LEyapp/examples/eyappintro$ ./postfix.pl -c \*(Aqa = 2*3 +b\*(Aq
    a 2 3 * b + =
.Ve

When a non conformant input is given, it produces an accurate error message:

.Vb 1
    pl@nereida:~/LEyapp/examples/eyappintro$ ./postfix.pl -c \*(Aqa = 2**3 +b\*(Aq

    Syntax error near \*(Aq*\*(Aq.
    Expected one of these terminals: \*(Aq-\*(Aq \*(AqNUM\*(Aq \*(AqVAR\*(Aq \*(Aq(\*(Aq
    There were 1 errors during parsing
.Ve

## AUTHOR

Header "AUTHOR"
Casiano Rodriguez-Leon

## COPYRIGHT

Header "COPYRIGHT"
Copyright © 2006, 2007, 2008, 2009, 2010, 2011, 2012 Casiano Rodriguez-Leon.
Copyright © 2017 William N. Braswell, Jr.
All Rights Reserved.

Parse::Yapp is Copyright © 1998, 1999, 2000, 2001, Francois Desarmenien.
Parse::Yapp is Copyright © 2017 William N. Braswell, Jr.
All Rights Reserved.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.

## SEE ALSO

Header "SEE ALSO"

- \(bu
Parse::Eyapp,

- \(bu
perldoc vgg,

- \(bu
The tutorial *Parsing Strings and Trees with* \f(CW\*(C`Parse::Eyapp\*(C'
(An Introduction to Compiler Construction in seven pages)> in

- \(bu
The pdf file in <http://nereida.deioc.ull.es/~pl/perlexamples/Eyapp.pdf>

- \(bu
<http://nereida.deioc.ull.es/~pl/perlexamples/section_eyappts.html> (Spanish),

- \(bu
eyapp,

- \(bu
treereg,

- \(bu
Parse::yapp,

- \(bu
**yacc**\|(1),

- \(bu
**bison**\|(1),

- \(bu
the classic book \*(L"Compilers: Principles, Techniques, and Tools\*(R" by Alfred V. Aho, Ravi Sethi and

- \(bu
Jeffrey D. Ullman (Addison-Wesley 1986)

- \(bu
Parse::RecDescent.

## POD ERRORS

Header "POD ERRORS"
Hey! **The above document had some coding errors, which are explained below:**

- Around line 199:
Item "Around line 199:"
Non-ASCII character seen before =encoding in '©'. Assuming \s-1UTF-8\s0
