+++
manpage_format = "troff"
description = "ppdpo extracts UI strings from PPDC source files and updates either a GNU gettext or macOS strings format message catalog source file for translation. This program is deprecated and will be removed in a future release of CUPS. ppdpo supports the fol..."
title = "ppdpo(1)"
manpage_name = "ppdpo"
manpage_section = "1"
author = "None Specified"
operating_system = "macos"
keywords = ["ppdc", "1", "ppdhtml", "ppdi", "ppdmerge", "ppdcfile", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
detected_package_version = "2.0"
date = "Sun Feb 16 04:48:18 2025"
operating_system_version = "15.3"
+++

ppdpo 1 "CUPS" "26 April 2019" "Apple Inc."

## NAME

ppdpo - ppd message catalog generator (deprecated)

## SYNOPSIS

ppdpo
[
**-D **name[**=**value]
] [
-I
include-directory
] [
-o
output-file
]
source-file

## DESCRIPTION

**ppdpo** extracts UI strings from PPDC source files and updates either a GNU gettext or macOS strings format message catalog source file for translation.
**This program is deprecated and will be removed in a future release of CUPS.**

## OPTIONS

**ppdpo** supports the following options:

**-D **name[**=**value]
Sets the named variable for use in the source file.
It is equivalent to using the *#define* directive in the source file.

**-I **include-directory
Specifies an alternate include directory.
Multiple *-I* options can be supplied to add additional directories.

**-o **output-file
Specifies the output file.
The supported extensions are *.po* or *.po.gz* for GNU gettext format message catalogs and *.strings* for macOS strings files.

## NOTES

PPD files are deprecated and will no longer be supported in a future feature release of CUPS.
Printers that do not support IPP can be supported using applications such as
ippeveprinter (1).

## SEE ALSO

ppdc (1),
ppdhtml (1),
ppdi (1),
ppdmerge (1),
ppdcfile(5),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
