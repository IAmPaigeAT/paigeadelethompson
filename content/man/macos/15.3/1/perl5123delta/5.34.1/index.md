+++
description = "This document describes differences between the 5.12.2 release and the 5.12.3 release. If you are upgrading from an earlier release such as 5.12.1, first read perl5122delta, which describes differences between 5.12.1 and 5.12.2.  The major changes..."
date = "2022-02-19"
operating_system = "macos"
manpage_format = "troff"
author = "None Specified"
title = "perl5123delta(1)"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_section = "1"
operating_system_version = "15.3"
manpage_name = "perl5123delta"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5123DELTA 1"
PERL5123DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5123delta - what is new for perl v5.12.3

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.12.2 release and
the 5.12.3 release.

If you are upgrading from an earlier release such as 5.12.1, first read
perl5122delta, which describes differences between 5.12.1 and
5.12.2.  The major changes made in 5.12.0 are described in perl5120delta.

## Incompatible Changes

Header "Incompatible Changes"
.Vb 2
    There are no changes intentionally incompatible with 5.12.2. If any
    exist, they are bugs and reports are welcome.
.Ve

## Core Enhancements

Header "Core Enhancements"
.ie n .SS """keys"", ""values"" work on arrays"
.el .SS "\f(CWkeys, \f(CWvalues work on arrays"
Subsection "keys, values work on arrays"
You can now use the \f(CW\*(C`keys\*(C', \f(CW\*(C`values\*(C', \f(CW\*(C`each\*(C' builtin functions on arrays
(previously you could only use them on hashes).  See perlfunc for details.
This is actually a change introduced in perl 5.12.0, but it was missed from
that release's perldelta.

## Bug Fixes

Header "Bug Fixes"
\*(L"no \s-1VERSION\*(R"\s0 will now correctly deparse with B::Deparse, as will certain
constant expressions.

Module::Build should be more reliably pass its tests under cygwin.

Lvalue subroutines are again able to return copy-on-write scalars.  This
had been broken since version 5.10.0.

## Platform Specific Notes

Header "Platform Specific Notes"

- Solaris
Item "Solaris"
A separate DTrace is now build for miniperl, which means that perl can be
compiled with -Dusedtrace on Solaris again.

- \s-1VMS\s0
Item "VMS"
A number of regressions on \s-1VMS\s0 have been fixed.  In addition to minor cleanup
of questionable expressions in *vms.c*, file permissions should no longer be
garbled by the PerlIO layer, and spurious record boundaries should no longer be
introduced by the PerlIO layer during output.
.Sp
For more details and discussion on the latter, see:
.Sp
.Vb 1
    http://www.nntp.perl.org/group/perl.vmsperl/2010/11/msg15419.html
.Ve

- \s-1VOS\s0
Item "VOS"
A few very small changes were made to the build process on \s-1VOS\s0 to better
support the platform.  Longer-than-32-character filenames are now supported on
OpenVOS, and build properly without IPv6 support.

## Acknowledgements

Header "Acknowledgements"
Perl 5.12.3 represents approximately four months of development since
Perl 5.12.2 and contains approximately 2500 lines of changes across
54 files from 16 authors.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers.  The following people are known to
have contributed the improvements that became Perl 5.12.3:

Craig A. Berry, David Golden, David Leadbeater, Father Chrysostomos, Florian
Ragwitz, Jesse Vincent, Karl Williamson, Nick Johnston, Nicolas Kaiser, Paul
Green, Rafael Garcia-Suarez, Rainer Tammer, Ricardo Signes, Steffen Mueller,
Zsba\*'n Ambrus, \*(Aevar Arnfjo\*:r\*(d- Bjarmason

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes
all the core committers, who will be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for
security issues in the Perl core, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
