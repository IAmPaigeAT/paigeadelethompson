+++
date = "2014-03-06"
keywords = ["header", "see", "also", "locale", "maketext", "extract", "4", "item", "lexicon", "gettext", "plugin", "perl", "ppi", "tt2", "yaml", "formfu", "mason", "texttemplate", "generic", "authors", "audrey", "tang", "cpan", "audreyt", "org", "copyright", "2002-2008", "by", "this", "software", "is", "released", "under", "the", "s-1mit", "s0", "license", "cited", "below", "ie", "n", "ss", "el", "subsection", "mit", "permission", "hereby", "granted", "free", "of", "charge", "to", "any", "person", "obtaining", "a", "copy", "and", "associated", "documentation", "files", "l", "r", "deal", "in", "without", "restriction", "including", "limitation", "rights", "use", "modify", "merge", "publish", "distribute", "sublicense", "or", "sell", "copies", "permit", "persons", "whom", "furnished", "do", "so", "subject", "following", "conditions", "above", "notice", "shall", "be", "included", "all", "substantial", "portions", "s-1the", "provided", "as", "warranty", "kind", "express", "implied", "but", "not", "limited", "warranties", "merchantability", "fitness", "for", "particular", "purpose", "noninfringement", "no", "event", "holders", "liable", "claim", "damages", "other", "liability", "whether", "an", "action", "contract", "tort", "otherwise", "arising", "from", "out", "connection", "with", "dealings", "bu", "clinton", "gormley", "drtech", "c", "2014", "licensed", "vb", "1", "x11", "ve"]
description = "This program extracts translatable strings from given input files, or from s-1STDINs0 if none are given. Please see Locale::Maketext::Extract for a list of supported input file formats. Mandatory arguments to long options are mandatory for short o..."
manpage_section = "1"
title = "xgettext(1)"
operating_system_version = "15.3"
operating_system = "macos"
author = "None Specified"
manpage_name = "xgettext"
detected_package_version = "5.34.0"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "XGETTEXT 1"
XGETTEXT 1 "2014-03-06" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

xgettext.pl - Extract translatable strings from source

## VERSION

Header "VERSION"
version 1.00

## SYNOPSIS

Header "SYNOPSIS"
**xgettext.pl** [*\s-1OPTION\s0*] [*\s-1INPUTFILE\s0*]...

## DESCRIPTION

Header "DESCRIPTION"
This program extracts translatable strings from given input files, or
from **\s-1STDIN\s0** if none are given.

Please see Locale::Maketext::Extract for a list of supported input file
formats.

## OPTIONS

Header "OPTIONS"
Mandatory arguments to long options are mandatory for short options too.
Similarly for optional arguments.

### Input file location:

Subsection "Input file location:"

- \fI\s-1INPUTFILE\s0...
Item "INPUTFILE..."
Files to extract messages from.  If not specified, **\s-1STDIN\s0** is assumed.

- \fB-f, \fB--files-from=\fI\s-1FILE\s0
Item "-f, --files-from=FILE"
Get list of input files from *\s-1FILE\s0*.

- \fB-D, \fB--directory=\fI\s-1DIRECTORY\s0
Item "-D, --directory=DIRECTORY"
Add *\s-1DIRECTORY\s0* to list for input files search.

### Input file format:

Subsection "Input file format:"

- \fB-u, \fB--use-gettext-style
Item "-u, --use-gettext-style"
Specifies that the source programs uses the **Gettext** style (e.g.
\f(CW%1) instead of the **Maketext** style (e.g. \f(CW\*(C`[_1]\*(C') in its
localization calls.

### Output file location:

Subsection "Output file location:"

- \fB-d, \fB--default-domain=\fI\s-1NAME\s0
Item "-d, --default-domain=NAME"
Use *\s-1NAME\s0*.po for output, instead of \f(CW\*(C`messages.po\*(C'.

- \fB-o, \fB--output=\fI\s-1FILE\s0
Item "-o, --output=FILE"
\s-1PO\s0 file name to be written or incrementally updated; \f(CW\*(C`-\*(C' means writing
to **\s-1STDOUT\s0**.

- \fB-p, \fB--output-dir=\fI\s-1DIR\s0
Item "-p, --output-dir=DIR"
Output files will be placed in directory *\s-1DIR\s0*.

### Output details:

Subsection "Output details:"

- \fB-g, \fB--gnu-gettext
Item "-g, --gnu-gettext"
Enables \s-1GNU\s0 gettext interoperability by printing \f(CW\*(C`#, perl-maketext-format\*(C'
before each entry that has \f(CW\*(C`%\*(C' variables.

- \fB-W, \fB--wrap
Item "-W, --wrap"
If wrap is enabled, then, for entries with multiple file locations, each
location is listed on a separate line. The default is to put them all
on a single line.
.Sp
Other comments are not affected.

### Plugins:

Subsection "Plugins:"
By default, all builtin parser plugins are enabled for all file types, with
warnings turned off.

If any plugin is specified on the command line, then warnings are turned
on by default - you can turn them off with \f(CW\*(C`-now\*(C'

- \fB-P|\fB--plugin pluginname
Item "-P|--plugin pluginname"
.Vb 2
    Use the specified plugin for the default file types recognised by that
    plugin.
.Ve

- \fB-P|\fB--plugin 'pluginname=*'
Item "-P|--plugin 'pluginname=*'"
.Vb 1
    Use the specified plugin for all file types.
.Ve

- \fB-P|\fB--plugin pluginname=ext,ext2
Item "-P|--plugin pluginname=ext,ext2"
.Vb 1
    Use the specified plugin for any files ending in C<.ext> or C<.ext2>
.Ve

- \fB-P|\fB--plugin My::Module::Name='*'
Item "-P|--plugin My::Module::Name='*'"
.Vb 1
    Use your custom plugin module for all file types
.Ve

Multiple plugins can be specified on the command line.

*Available plugins:*
Subsection "Available plugins:"
.ie n .IP """perl""    : Locale::Maketext::Extract::Plugin::Perl" 4
.el .IP "\f(CWperl    : Locale::Maketext::Extract::Plugin::Perl" 4
Item "perl : Locale::Maketext::Extract::Plugin::Perl"
For a slightly more accurate but much slower Perl parser, you can use
the \s-1PPI\s0 plugin. This does not have a short name, but must be specified in
full, eg:
.Sp
.Vb 1
    xgettext.pl -P Locale::Maketext::Extract::Plugin::PPI
.Ve
.ie n .IP """tt2""     : Locale::Maketext::Extract::Plugin::TT2" 4
.el .IP "\f(CWtt2     : Locale::Maketext::Extract::Plugin::TT2" 4
Item "tt2 : Locale::Maketext::Extract::Plugin::TT2"
0
.ie n .IP """yaml""    : Locale::Maketext::Extract::Plugin::YAML" 4
.el .IP "\f(CWyaml    : Locale::Maketext::Extract::Plugin::YAML" 4
Item "yaml : Locale::Maketext::Extract::Plugin::YAML"
.ie n .IP """formfu""  : Locale::Maketext::Extract::Plugin::FormFu" 4
.el .IP "\f(CWformfu  : Locale::Maketext::Extract::Plugin::FormFu" 4
Item "formfu : Locale::Maketext::Extract::Plugin::FormFu"
.ie n .IP """mason""   : Locale::Maketext::Extract::Plugin::Mason" 4
.el .IP "\f(CWmason   : Locale::Maketext::Extract::Plugin::Mason" 4
Item "mason : Locale::Maketext::Extract::Plugin::Mason"
.ie n .IP """text""    : Locale::Maketext::Extract::Plugin::TextTemplate" 4
.el .IP "\f(CWtext    : Locale::Maketext::Extract::Plugin::TextTemplate" 4
Item "text : Locale::Maketext::Extract::Plugin::TextTemplate"
.ie n .IP """generic"" : Locale::Maketext::Extract::Plugin::Generic" 4
.el .IP "\f(CWgeneric : Locale::Maketext::Extract::Plugin::Generic" 4
Item "generic : Locale::Maketext::Extract::Plugin::Generic"
.PD

### Warnings:

Subsection "Warnings:"
If a parser plugin encounters a syntax error while parsing, it will abort
parsing and hand over to the next parser plugin.  If warnings are turned
on then the error will be echoed to \s-1STDERR.\s0

Off by default, unless any plugin has been specified on the command line.

- \fB-w|\fB--warnings
Item "-w|--warnings"
0

- \fB-now|\fB--nowarnings
Item "-now|--nowarnings"
.PD

### Verbose:

Subsection "Verbose:"
If you would like to see which files have been processed, which plugins were
used, and which strings were extracted, then enable \f(CW\*(C`verbose\*(C'. If no
acceptable plugin was found, or no strings were extracted, then the file
is not listed:

- \fB-v|\fB--verbose
Item "-v|--verbose"
Lists processed files.

- \fB-v -v|\fB--verbose --verbose :
Item "-v -v|--verbose --verbose :"
Lists processed files and which plugins managed to extract strings.

- \fB-v -v|\fB--verbose --verbose :
Item "-v -v|--verbose --verbose :"
Lists processed files, which plugins managed to extract strings, and the
extracted strings, the line where they were found, and any variables.

## SEE ALSO

Header "SEE ALSO"

- Locale::Maketext::Extract
Item "Locale::Maketext::Extract"
0

- Locale::Maketext::Lexicon::Gettext
Item "Locale::Maketext::Lexicon::Gettext"

- Locale::Maketext
Item "Locale::Maketext"

- Locale::Maketext::Extract::Plugin::Perl
Item "Locale::Maketext::Extract::Plugin::Perl"

- Locale::Maketext::Extract::Plugin::PPI
Item "Locale::Maketext::Extract::Plugin::PPI"

- Locale::Maketext::Extract::Plugin::TT2
Item "Locale::Maketext::Extract::Plugin::TT2"

- Locale::Maketext::Extract::Plugin::YAML
Item "Locale::Maketext::Extract::Plugin::YAML"

- Locale::Maketext::Extract::Plugin::FormFu
Item "Locale::Maketext::Extract::Plugin::FormFu"

- Locale::Maketext::Extract::Plugin::Mason
Item "Locale::Maketext::Extract::Plugin::Mason"

- Locale::Maketext::Extract::Plugin::TextTemplate
Item "Locale::Maketext::Extract::Plugin::TextTemplate"

- Locale::Maketext::Extract::Plugin::Generic
Item "Locale::Maketext::Extract::Plugin::Generic"
.PD

## AUTHORS

Header "AUTHORS"
Audrey Tang <cpan@audreyt.org>

## COPYRIGHT

Header "COPYRIGHT"
Copyright 2002-2008 by Audrey Tang <cpan@audreyt.org>.

This software is released under the \s-1MIT\s0 license cited below.
.ie n .SS "The ""\s-1MIT""\s0 License"
.el .SS "The ``\s-1MIT''\s0 License"
Subsection "The MIT License"
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the \*(L"Software\*(R"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

\s-1THE SOFTWARE IS PROVIDED \*(L"AS IS\*(R", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.\s0

## AUTHORS

Header "AUTHORS"

- \(bu
Clinton Gormley <drtech@cpan.org>

- \(bu
Audrey Tang <cpan@audreyt.org>

## COPYRIGHT AND LICENSE

Header "COPYRIGHT AND LICENSE"
This software is Copyright (c) 2014 by Audrey Tang.

This is free software, licensed under:

.Vb 1
  The MIT (X11) License
.Ve
