+++
author = "None Specified"
detected_package_version = "0.7.3"
date = "Sun Feb 16 04:48:26 2025"
operating_system = "macos"
description = "Adds the named gem(s) with their version requirements to the resolved [Gemfile(5)][Gemfile(5)]. This command will add the gem to both your [Gemfile(5)][Gemfile(5)] and Gemfile.lock if it isnt listed yet. Example:..."
manpage_format = "troff"
title = "bundle-inject(1)"
manpage_section = "1"
operating_system_version = "15.3"
manpage_name = "bundle-inject"
+++

.
"BUNDLE-INJECT" "1" "November 2018" "" ""
.

## NAME

**bundle-inject** - Add named gem(s) with version requirements to Gemfile
.

## SYNOPSIS

**bundle inject** [GEM] [VERSION]
.

## DESCRIPTION

Adds the named gem(s) with their version requirements to the resolved [**Gemfile(5)**][Gemfile(5)]\.
.
.P
This command will add the gem to both your [**Gemfile(5)**][Gemfile(5)] and Gemfile\.lock if it isn\'t listed yet\.
.
.P
Example:
.
"" 4
.

```

bundle install
bundle inject \'rack\' \'> 0\'
.
```

.
"" 0
.
.P
This will inject the \'rack\' gem with a version greater than 0 in your [**Gemfile(5)**][Gemfile(5)] and Gemfile\.lock
