+++
detected_package_version = "0"
operating_system = "macos"
manpage_section = "1"
keywords = ["vim", "author", "most", "of", "was", "made", "by", "bram", "moolenaar", "with", "a", "lot", "help", "from", "others", "see", "the", "credits", "menu"]
manpage_name = "evim"
description = "starts and sets options to make it behave like a modeless editor. This is still Vim but used as a point-and-click editor. This feels a lot like using Notepad on MS-Windows. will always run in the GUI, to enable the use of menus and toolbar. Only ..."
manpage_format = "troff"
operating_system_version = "15.3"
title = "evim(1)"
author = "None Specified"
date = "2024-01-01"
+++

EVIM 1 "2024 August 12"

## NAME

evim - easy Vim, edit a file with Vim and setup for modeless editing

## SYNOPSIS

.br
evim
[options] [file ..]
.br
eview

## DESCRIPTION

eVim
starts
Vim
and sets options to make it behave like a modeless editor.
This is still Vim but used as a point-and-click editor.
This feels a lot like using Notepad on MS-Windows.
eVim
will always run in the GUI, to enable the use of menus and toolbar.

Only to be used for people who really can't work with Vim in the normal way.
Editing will be much less efficient.

eview
is the same, but starts in read-only mode.  It works just like evim -R.

See vim(1) for details about Vim, options, etc.

The 'insertmode' option is set to be able to type text directly.
.br
Mappings are setup to make Copy and Paste work with the MS-Windows keys.
CTRL-X cuts text, CTRL-C copies text and CTRL-V pastes text.
Use CTRL-Q to obtain the original meaning of CTRL-V.

## OPTIONS

See vim(1).

## FILES


/usr/local/share/vim/vim??/evim.vim
The script loaded to initialize eVim.
.br
vim??
is short version number, like vim91 for
Vim 9.1

## AKA

Also Known As "Vim for gumbies".
When using evim you are expected to take a handkerchief,
make a knot in each corner and wear it on your head.

## SEE ALSO

vim(1)

## AUTHOR

Most of
Vim
was made by Bram Moolenaar, with a lot of help from others.
See the Help/Credits menu.
