+++
manpage_section = "1"
operating_system_version = "15.3"
detected_package_version = "2.0"
operating_system = "macos"
date = "Sun Feb 16 04:48:23 2025"
title = "lprm(1)"
keywords = ["cancel", "1", "lp", "lpq", "lpr", "lpstat", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
manpage_format = "troff"
description = "cancels print jobs that have been queued for printing. If no arguments are supplied, the current job on the default destination is canceled. You can specify one or more job ID numbers to cancel those jobs or use the - option to cancel all jobs. The..."
author = "None Specified"
manpage_name = "lprm"
+++

lprm 1 "CUPS" "26 April 2019" "Apple Inc."

## NAME

lprm - cancel print jobs

## SYNOPSIS

lprm
[
-E
] [
-U
username
] [
-h
server [ :port ]
] [
-P
destination [ /instance ]
] [
-
] [
job-id(s)
]

## DESCRIPTION

lprm
cancels print jobs that have been queued for printing.
If no arguments are supplied, the current job on the default destination is canceled.
You can specify one or more job ID numbers to cancel those jobs or use the *-* option to cancel all jobs.

## OPTIONS

The
lprm
command supports the following options:

-E
Forces encryption when connecting to the server.

**-P **destination[*/instance*]
Specifies the destination printer or class.

**-U **username
Specifies an alternate username.

**-h **server[*:port*]
Specifies an alternate server.

## CONFORMING TO

The CUPS version of
lprm
is compatible with the standard Berkeley command of the same name.

## EXAMPLES

Cancel the current job on the default printer:

```

    lprm

```

Cancel job 1234:

```

    lprm 1234

```

Cancel all jobs:

```

    lprm \-
```


## SEE ALSO

cancel (1),
lp (1),
lpq (1),
lpr (1),
lpstat (1),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
