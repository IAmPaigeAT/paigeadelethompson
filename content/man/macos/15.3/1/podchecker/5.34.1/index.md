+++
title = "podchecker(1)"
manpage_section = "1"
author = "None Specified"
operating_system = "macos"
detected_package_version = "5.34.1"
date = "2024-12-14"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_name = "podchecker"
description = "podchecker will read the given input files looking for s-1PODs0 syntax errors in the s-1PODs0 documentation and will print any errors it find to s-1STDERR.s0 At the end, it will print a status message indicating the number of errors found. Directo..."
keywords = ["header", "see", "also", "pod", "simple", "and", "checker", "authors", "please", "report", "bugs", "using", "http", "rt", "cpan", "org", "brad", "appleton", "bradapp", "enteract", "com", "marek", "rouchal", "marekr", "based", "on", "code", "for", "fb", "fbpod", "text", "pod2text", "1", "written", "by", "tom", "christiansen", "tchrist", "mox", "perl"]
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PODCHECKER 1"
PODCHECKER 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

podchecker - check the syntax of POD format documentation files

## SYNOPSIS

Header "SYNOPSIS"
**podchecker** [**-help**] [**-man**] [**-(no)warnings**] [*file*\ ...]

## OPTIONS AND ARGUMENTS

Header "OPTIONS AND ARGUMENTS"

- \fB-help
Item "-help"
Print a brief help message and exit.

- \fB-man
Item "-man"
Print the manual page and exit.

- \fB-warnings \fB-nowarnings
Item "-warnings -nowarnings"
Turn on/off printing of warnings. Repeating **-warnings** increases the
warning level, i.e. more warnings are printed. Currently increasing to
level two causes flagging of unescaped \*(L"<,>\*(R" characters.

- \fIfile
Item "file"
The pathname of a \s-1POD\s0 file to syntax-check (defaults to standard input).

## DESCRIPTION

Header "DESCRIPTION"
**podchecker** will read the given input files looking for \s-1POD\s0
syntax errors in the \s-1POD\s0 documentation and will print any errors
it find to \s-1STDERR.\s0 At the end, it will print a status message
indicating the number of errors found.

Directories are ignored, an appropriate warning message is printed.

**podchecker** invokes the ****podchecker()\fB** function exported by \fBPod::Checker**
Please see \*(L"**podchecker()**\*(R" in Pod::Checker for more details.

## RETURN VALUE

Header "RETURN VALUE"
**podchecker** returns a 0 (zero) exit status if all specified
\s-1POD\s0 files are ok.

## ERRORS

Header "ERRORS"
**podchecker** returns the exit status 1 if at least one of
the given \s-1POD\s0 files has syntax errors.

The status 2 indicates that at least one of the specified
files does not contain *any* \s-1POD\s0 commands.

Status 1 overrides status 2. If you want unambiguous
results, call **podchecker** with one single argument only.

## SEE ALSO

Header "SEE ALSO"
Pod::Simple and Pod::Checker

## AUTHORS

Header "AUTHORS"
Please report bugs using <http://rt.cpan.org>.

Brad Appleton <bradapp@enteract.com>,
Marek Rouchal <marekr@cpan.org>

Based on code for **\fBPod::Text::pod2text\fB\|(1)** written by
Tom Christiansen <tchrist@mox.perl.com>
