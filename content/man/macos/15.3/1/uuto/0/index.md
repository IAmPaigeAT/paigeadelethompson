+++
manpage_format = "troff"
manpage_section = "1"
detected_package_version = "0"
operating_system_version = "15.3"
keywords = ["uucp", "uupick", "author", "ian", "lance", "taylor", "airs", "com", "text", "for", "this", "manpage", "comes", "from", "version", "1", "07", "info", "documentation"]
author = "None Specified"
description = "The  program may be used to conveniently send files to a particular  user on a remote system. It will arrange for mail to be sent to the remote user  when the files arrive on the remote system,  and he or she may easily retrieve the files using t..."
title = "uuto(1)"
date = "Sun Feb 16 04:48:24 2025"
operating_system = "macos"
manpage_name = "uuto"
+++

uuto 1 "Taylor UUCP 1.07"

## NAME

uuto - send files to a particular user on a remote system

## SYNOPSIS

uuto
files... system!user

## DESCRIPTION

The
uuto
program may be used to conveniently send files to a particular
user on a remote system.
It will arrange for mail to be sent to the remote user
when the files arrive on the remote system,
and he or she may easily retrieve the files using the
uupick
program.
Note that
uuto
does not provide any security--any user on the remote system can examine the files.

The last argument specifies the system and user name to which to send
the files.  The other arguments are the files or directories to be sent.

The
uuto
program is actually just a trivial shell script which
invokes the
uucp
program with the appropriate arguments.

## OPTIONS

Any option which may be given to
uucp
may also be given to
uuto.

## SEE ALSO

uucp(1), uupick(1)

## AUTHOR

Ian Lance Taylor
<ian@airs.com>.
Text for this Manpage comes from Taylor UUCP, version 1.07 Info documentation.


