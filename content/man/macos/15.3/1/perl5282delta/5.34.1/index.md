+++
author = "None Specified"
manpage_section = "1"
date = "2022-02-19"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
detected_package_version = "5.34.1"
operating_system = "macos"
title = "perl5282delta(1)"
manpage_name = "perl5282delta"
operating_system_version = "15.3"
description = "This document describes differences between the 5.28.1 release and the 5.28.2 release. If you are upgrading from an earlier release such as 5.28.0, first read perl5281delta, which describes differences between 5.28.0 and 5.28.1. There are several ..."
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5282DELTA 1"
PERL5282DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5282delta - what is new for perl v5.28.2

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.28.1 release and the 5.28.2
release.

If you are upgrading from an earlier release such as 5.28.0, first read
perl5281delta, which describes differences between 5.28.0 and 5.28.1.

## Incompatible Changes

Header "Incompatible Changes"

### Any set of digits in the Common script are legal in a script run of another script

Subsection "Any set of digits in the Common script are legal in a script run of another script"
There are several sets of digits in the Common script.  \f(CW\*(C`[0-9]\*(C' is the most
familiar.  But there are also \f(CW\*(C`[\\x\{FF10\}-\\x\{FF19\}]\*(C' (\s-1FULLWIDTH DIGIT ZERO\s0 -
\s-1FULLWIDTH DIGIT NINE\s0), and several sets for use in mathematical notation, such
as the \s-1MATHEMATICAL\s0 DOUBLE-STRUCK DIGITs.  Any of these sets should be able to
appear in script runs of, say, Greek.  But the previous design overlooked all
but the \s-1ASCII\s0 digits \f(CW\*(C`[0-9]\*(C', so the design was flawed.  This has been fixed,
so is both a bug fix and an incompatibility.

All digits in a run still have to come from the same set of ten digits.

[\s-1GH\s0 #16704] <https://github.com/Perl/perl5/issues/16704>

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Module::CoreList has been upgraded from version 5.20181129_28 to 5.20190419.

- \(bu
PerlIO::scalar has been upgraded from version 0.29 to 0.30.

- \(bu
Storable has been upgraded from version 3.08 to 3.08_01.

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- Windows
Item "Windows"
The Windows Server 2003 \s-1SP1\s0 Platform \s-1SDK\s0 build, with its early x64 compiler and
tools, was accidentally broken in Perl 5.27.9.  This has now been fixed.

- Mac \s-1OS X\s0
Item "Mac OS X"
Perl's build and testing process on Mac \s-1OS X\s0 for \f(CW\*(C`-Duseshrplib\*(C' builds is now
compatible with Mac \s-1OS X\s0 System Integrity Protection (\s-1SIP\s0).
.Sp
\s-1SIP\s0 prevents binaries in */bin* (and a few other places) being passed the
\f(CW\*(C`DYLD_LIBRARY_PATH\*(C' environment variable.  For our purposes this prevents
\f(CW\*(C`DYLD_LIBRARY_PATH\*(C' from being passed to the shell, which prevents that
variable being passed to the testing or build process, so running \f(CW\*(C`perl\*(C'
couldn't find *libperl.dylib*.
.Sp
To work around that, the initial build of the *perl* executable expects to
find *libperl.dylib* in the build directory, and the library path is then
adjusted during installation to point to the installed library.
.Sp
[\s-1GH\s0 #15057] <https://github.com/Perl/perl5/issues/15057>

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
If an in-place edit is still in progress during global destruction and the
process exit code (as stored in \f(CW$?) is zero, perl will now treat the
in-place edit as successful, replacing the input file with any output produced.
.Sp
This allows code like:
.Sp
.Vb 1
  perl -i -ne \*(Aqprint "Foo"; last\*(Aq
.Ve
.Sp
to replace the input file, while code like:
.Sp
.Vb 1
  perl -i -ne \*(Aqprint "Foo"; die\*(Aq
.Ve
.Sp
will not.  Partly resolves [perl #133659].
.Sp
[\s-1GH\s0 #16748] <https://github.com/Perl/perl5/issues/16748>

- \(bu
A regression in Perl 5.28 caused the following code to fail
.Sp
.Vb 1
 close(STDIN); open(CHILD, "|wc -l")\*(Aq
.Ve
.Sp
because the child's stdin would be closed on exec.  This has now been fixed.

- \(bu
\f(CW\*(C`pack "u", "invalid uuencoding"\*(C' now properly \s-1NUL\s0 terminates the zero-length
\s-1SV\s0 produced.
.Sp
[\s-1GH\s0 #16343] <https://github.com/Perl/perl5/issues/16343>

- \(bu
Failing to compile a format now aborts compilation.  Like other errors in
sub-parses this could leave the parser in a strange state, possibly crashing
perl if compilation continued.
.Sp
[\s-1GH\s0 #16169] <https://github.com/Perl/perl5/issues/16169>

- \(bu
See \*(L"Any set of digits in the Common script are legal in a script run of
another script\*(R".

## Acknowledgements

Header "Acknowledgements"
Perl 5.28.2 represents approximately 4 months of development since Perl 5.28.1
and contains approximately 2,500 lines of changes across 75 files from 13
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 1,200 lines of changes to 29 .pm, .t, .c and .h files.

Perl continues to flourish into its fourth decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.28.2:

Aaron Crane, Abigail, Andy Dougherty, David Mitchell, Karen Etheridge, Karl
Williamson, Leon Timmermans, Nicolas R., Sawyer X, Steve Hay, Tina Mu\*:ller,
Tony Cook, Zak B. Elep.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database
at <https://rt.perl.org/> .  There may also be information at
<http://www.perl.org/> , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5,
you can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
