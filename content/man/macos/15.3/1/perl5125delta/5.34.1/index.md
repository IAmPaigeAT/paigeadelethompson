+++
date = "2022-02-19"
author = "None Specified"
manpage_name = "perl5125delta"
title = "perl5125delta(1)"
description = "This document describes differences between the 5.12.4 release and the 5.12.5 release. If you are upgrading from an earlier release such as 5.12.3, first read perl5124delta, which describes differences between 5.12.3 and 5.12.4. A bug in f(CW*(C`..."
detected_package_version = "5.34.1"
manpage_format = "troff"
operating_system_version = "15.3"
operating_system = "macos"
manpage_section = "1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5125DELTA 1"
PERL5125DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5125delta - what is new for perl v5.12.5

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.12.4 release and
the 5.12.5 release.

If you are upgrading from an earlier release such as 5.12.3, first read
perl5124delta, which describes differences between 5.12.3 and
5.12.4.

## Security

Header "Security"
.ie n .SS """Encode"" decode_xs n-byte heap-overflow (\s-1CVE-2011-2939\s0)"
.el .SS "\f(CWEncode decode_xs n-byte heap-overflow (\s-1CVE-2011-2939\s0)"
Subsection "Encode decode_xs n-byte heap-overflow (CVE-2011-2939)"
A bug in \f(CW\*(C`Encode\*(C' could, on certain inputs, cause the heap to overflow.
This problem has been corrected.  Bug reported by Robert Zacek.
.ie n .SS """File::Glob::bsd_glob()"" memory error with \s-1GLOB_ALTDIRFUNC\s0 (\s-1CVE-2011-2728\s0)."
.el .SS "\f(CWFile::Glob::bsd_glob() memory error with \s-1GLOB_ALTDIRFUNC\s0 (\s-1CVE-2011-2728\s0)."
Subsection "File::Glob::bsd_glob() memory error with GLOB_ALTDIRFUNC (CVE-2011-2728)."
Calling \f(CW\*(C`File::Glob::bsd_glob\*(C' with the unsupported flag \s-1GLOB_ALTDIRFUNC\s0 would
cause an access violation / segfault.  A Perl program that accepts a flags value from
an external source could expose itself to denial of service or arbitrary code
execution attacks.  There are no known exploits in the wild.  The problem has been
corrected by explicitly disabling all unsupported flags and setting unused function
pointers to null.  Bug reported by Cle\*'ment Lecigne.

### Heap buffer overrun in x string repeat operator (\s-1CVE-2012-5195\s0)

Subsection "Heap buffer overrun in 'x' string repeat operator (CVE-2012-5195)"
Poorly written perl code that allows an attacker to specify the count to
perl's 'x' string repeat operator can already cause a memory exhaustion
denial-of-service attack. A flaw in versions of perl before 5.15.5 can
escalate that into a heap buffer overrun; coupled with versions of glibc
before 2.16, it possibly allows the execution of arbitrary code.

This problem has been fixed.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.12.4. If any
exist, they are bugs and reports are welcome.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules

Subsection "Updated Modules"
*B::Concise*
Subsection "B::Concise"

B::Concise no longer produces mangled output with the **-tree** option
[perl #80632].

*charnames*
Subsection "charnames"

A regression introduced in Perl 5.8.8 has been fixed, that caused
\f(CWcharnames::viacode(0) to return \f(CW\*(C`undef\*(C' instead of the string \*(L"\s-1NULL\*(R"\s0
[perl #72624].

*Encode has been upgraded from version 2.39 to version 2.39_01.*
Subsection "Encode has been upgraded from version 2.39 to version 2.39_01."

See \*(L"Security\*(R".

*File::Glob has been upgraded from version 1.07 to version 1.07_01.*
Subsection "File::Glob has been upgraded from version 1.07 to version 1.07_01."

See \*(L"Security\*(R".

*Unicode::UCD*
Subsection "Unicode::UCD"

The documentation for the \f(CW\*(C`upper\*(C' function now actually says \*(L"upper\*(R", not
\*(L"lower\*(R".

*Module::CoreList*
Subsection "Module::CoreList"

Module::CoreList has been updated to version 2.50_02 to add data for
this release.

## Changes to Existing Documentation

Header "Changes to Existing Documentation"

### perlebcdic

Subsection "perlebcdic"
The perlebcdic document contains a helpful table to use in \f(CW\*(C`tr///\*(C' to
convert between \s-1EBCDIC\s0 and Latin1/ASCII.  Unfortunately, the table was the
inverse of the one it describes.  This has been corrected.

### perlunicode

Subsection "perlunicode"
The section on
User-Defined Case Mappings had
some bad markup and unclear sentences, making parts of it unreadable.  This
has been rectified.

### perluniprops

Subsection "perluniprops"
This document has been corrected to take non-ASCII platforms into account.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"

### Platform Specific Changes

Subsection "Platform Specific Changes"

- Mac \s-1OS X\s0
Item "Mac OS X"
There have been configuration and test fixes to make Perl build cleanly on
Lion and Mountain Lion.

- NetBSD
Item "NetBSD"
The NetBSD hints file was corrected to be compatible with NetBSD 6.*

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
\f(CW\*(C`chop\*(C' now correctly handles characters above \*(L"\\x\{7fffffff\}\*(R"
[perl #73246].

- \(bu
\f(CW\*(C`($<,$>) = (...)\*(C' stopped working properly in 5.12.0.  It is supposed
to make a single \f(CW\*(C`setreuid()\*(C' call, rather than calling \f(CW\*(C`setruid()\*(C' and
\f(CW\*(C`seteuid()\*(C' separately.  Consequently it did not work properly.  This has
been fixed [perl #75212].

- \(bu
Fixed a regression of **kill()** when a match variable is used for the
process \s-1ID\s0 to kill [perl #75812].

- \(bu
\f(CW\*(C`UNIVERSAL::VERSION\*(C' no longer leaks memory.  It started leaking in Perl
5.10.0.

- \(bu
The C-level \f(CW\*(C`my_strftime\*(C' functions no longer leaks memory.  This fixes a
memory leak in \f(CW\*(C`POSIX::strftime\*(C' [perl #73520].

- \(bu
\f(CW\*(C`caller\*(C' no longer leaks memory when called from the \s-1DB\s0 package if
\f(CW@DB::args was assigned to after the first call to \f(CW\*(C`caller\*(C'.  Carp
was triggering this bug [perl #97010].

- \(bu
Passing to \f(CW\*(C`index\*(C' an offset beyond the end of the string when the string
is encoded internally in \s-1UTF8\s0 no longer causes panics [perl #75898].

- \(bu
Syntax errors in \f(CW\*(C`(?\{...\})\*(C' blocks in regular expressions no longer
cause panic messages [perl #2353].

- \(bu
Perl 5.10.0 introduced some faulty logic that made \*(L"U*\*(R" in the middle of
a pack template equivalent to \*(L"U0\*(R" if the input string was empty.  This has
been fixed [perl #90160].

## Errata

Header "Errata"
.ie n .SS "**split()** and @_"
.el .SS "**split()** and \f(CW@_"
Subsection "split() and @_"
**split()** no longer modifies \f(CW@_ when called in scalar or void context.
In void context it now produces a \*(L"Useless use of split\*(R" warning.
This is actually a change introduced in perl 5.12.0, but it was missed from
that release's perl5120delta.

## Acknowledgements

Header "Acknowledgements"
Perl 5.12.5 represents approximately 17 months of development since Perl 5.12.4
and contains approximately 1,900 lines of changes across 64 files from 18
authors.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers. The following people are known to have contributed the
improvements that became Perl 5.12.5:

Andy Dougherty, Chris 'BinGOs' Williams, Craig A. Berry, David Mitchell,
Dominic Hargreaves, Father Chrysostomos, Florian Ragwitz, George Greer, Goro
Fuji, Jesse Vincent, Karl Williamson, Leon Brocard, Nicholas Clark, Rafael
Garcia-Suarez, Reini Urban, Ricardo Signes, Steve Hay, Tony Cook.

The list above is almost certainly incomplete as it is automatically generated
from version control history. In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core. We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes all the core committers, who be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for
security issues in the Perl core, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
