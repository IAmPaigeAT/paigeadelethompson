+++
description = "The perlfaq comprises several documents that answer the most commonly asked questions about Perl and Perl programming. Its divided by topic into nine major sections outlined in this document. The perlfaq is an evolving document.  Read the latest v..."
date = "2022-02-19"
manpage_name = "perlfaq"
manpage_format = "troff"
manpage_section = "1"
title = "perlfaq(1)"
detected_package_version = "5.34.1"
operating_system = "macos"
author = "None Specified"
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLFAQ 1"
PERLFAQ 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlfaq - Frequently asked questions about Perl

## VERSION

Header "VERSION"
version 5.20210411

## DESCRIPTION

Header "DESCRIPTION"
The perlfaq comprises several documents that answer the most commonly
asked questions about Perl and Perl programming. It's divided by topic
into nine major sections outlined in this document.

### Where to find the perlfaq

Subsection "Where to find the perlfaq"
The perlfaq is an evolving document.  Read the latest version at
<https://perldoc.perl.org/perlfaq>.  It is also included in the standard Perl
distribution.

### How to use the perlfaq

Subsection "How to use the perlfaq"
The \f(CW\*(C`perldoc\*(C' command line tool is part of the standard Perl distribution. To
read the perlfaq:

.Vb 1
    $ perldoc perlfaq
.Ve

To search the perlfaq question headings:

.Vb 1
    $ perldoc -q open
.Ve

### How to contribute to the perlfaq

Subsection "How to contribute to the perlfaq"
Review <https://github.com/perl-doc-cats/perlfaq/wiki>.  If you don't find
your suggestion create an issue or pull request against
<https://github.com/perl-doc-cats/perlfaq>.

Once approved, changes will be distributed with the next Perl release and
subsequently appear at <https://perldoc.perl.org/perlfaq>.

### What if my question isnt answered in the \s-1FAQ\s0?

Subsection "What if my question isn't answered in the FAQ?"
Try the resources in perlfaq2.

## TABLE OF CONTENTS

Header "TABLE OF CONTENTS"

- perlfaq1 - General Questions About Perl
Item "perlfaq1 - General Questions About Perl"
0

- perlfaq2 - Obtaining and Learning about Perl
Item "perlfaq2 - Obtaining and Learning about Perl"

- perlfaq3 - Programming Tools
Item "perlfaq3 - Programming Tools"

- perlfaq4 - Data Manipulation
Item "perlfaq4 - Data Manipulation"

- perlfaq5 - Files and Formats
Item "perlfaq5 - Files and Formats"

- perlfaq6 - Regular Expressions
Item "perlfaq6 - Regular Expressions"

- perlfaq7 - General Perl Language Issues
Item "perlfaq7 - General Perl Language Issues"

- perlfaq8 - System Interaction
Item "perlfaq8 - System Interaction"

- perlfaq9 - Web, Email and Networking
Item "perlfaq9 - Web, Email and Networking"
.PD

## THE QUESTIONS

Header "THE QUESTIONS"

### perlfaq1: General Questions About Perl

Subsection "perlfaq1: General Questions About Perl"
This section of the \s-1FAQ\s0 answers very general, high-level questions about Perl.

- \(bu
What is Perl?

- \(bu
Who supports Perl? Who develops it? Why is it free?

- \(bu
Which version of Perl should I use?

- \(bu
What are Perl 4, Perl 5, or Raku (Perl 6)?

- \(bu
What is Raku (Perl 6)?

- \(bu
How stable is Perl?

- \(bu
How often are new versions of Perl released?

- \(bu
Is Perl difficult to learn?

- \(bu
How does Perl compare with other languages like Java, Python, \s-1REXX,\s0 Scheme, or Tcl?

- \(bu
Can I do [task] in Perl?

- \(bu
When shouldn't I program in Perl?

- \(bu
What's the difference between \*(L"perl\*(R" and \*(L"Perl\*(R"?

- \(bu
What is a \s-1JAPH\s0?

- \(bu
How can I convince others to use Perl?

### perlfaq2: Obtaining and Learning about Perl

Subsection "perlfaq2: Obtaining and Learning about Perl"
This section of the \s-1FAQ\s0 answers questions about where to find source and documentation for Perl, support, and related matters.

- \(bu
What machines support Perl? Where do I get it?

- \(bu
How can I get a binary version of Perl?

- \(bu
I don't have a C compiler. How can I build my own Perl interpreter?

- \(bu
I copied the Perl binary from one machine to another, but scripts don't work.

- \(bu
I grabbed the sources and tried to compile but gdbm/dynamic loading/malloc/linking/... failed. How do I make it work?

- \(bu
What modules and extensions are available for Perl? What is \s-1CPAN\s0?

- \(bu
Where can I get information on Perl?

- \(bu
What is perl.com? Perl Mongers? pm.org? perl.org? cpan.org?

- \(bu
Where can I post questions?

- \(bu
Perl Books

- \(bu
Which magazines have Perl content?

- \(bu
Which Perl blogs should I read?

- \(bu
What mailing lists are there for Perl?

- \(bu
Where can I buy a commercial version of Perl?

- \(bu
Where do I send bug reports?

### perlfaq3: Programming Tools

Subsection "perlfaq3: Programming Tools"
This section of the \s-1FAQ\s0 answers questions related to programmer tools and programming support.

- \(bu
How do I do (anything)?

- \(bu
How can I use Perl interactively?

- \(bu
How do I find which modules are installed on my system?

- \(bu
How do I debug my Perl programs?

- \(bu
How do I profile my Perl programs?

- \(bu
How do I cross-reference my Perl programs?

- \(bu
Is there a pretty-printer (formatter) for Perl?

- \(bu
Is there an \s-1IDE\s0 or Windows Perl Editor?

- \(bu
Where can I get Perl macros for vi?

- \(bu
Where can I get perl-mode or cperl-mode for emacs?

- \(bu
How can I use curses with Perl?

- \(bu
How can I write a \s-1GUI\s0 (X, Tk, Gtk, etc.) in Perl?

- \(bu
How can I make my Perl program run faster?

- \(bu
How can I make my Perl program take less memory?

- \(bu
Is it safe to return a reference to local or lexical data?

- \(bu
How can I free an array or hash so my program shrinks?

- \(bu
How can I make my \s-1CGI\s0 script more efficient?

- \(bu
How can I hide the source for my Perl program?

- \(bu
How can I compile my Perl program into byte code or C?

- \(bu
How can I get \f(CW\*(C`#!perl\*(C' to work on [\s-1MS-DOS,NT,...\s0]?

- \(bu
Can I write useful Perl programs on the command line?

- \(bu
Why don't Perl one-liners work on my DOS/Mac/VMS system?

- \(bu
Where can I learn about \s-1CGI\s0 or Web programming in Perl?

- \(bu
Where can I learn about object-oriented Perl programming?

- \(bu
Where can I learn about linking C with Perl?

- \(bu
I've read perlembed, perlguts, etc., but I can't embed perl in my C program; what am I doing wrong?

- \(bu
When I tried to run my script, I got this message. What does it mean?

- \(bu
What's MakeMaker?

### perlfaq4: Data Manipulation

Subsection "perlfaq4: Data Manipulation"
This section of the \s-1FAQ\s0 answers questions related to manipulating numbers, dates, strings, arrays, hashes, and miscellaneous data issues.

- \(bu
Why am I getting long decimals (eg, 19.9499999999999) instead of the numbers I should be getting (eg, 19.95)?

- \(bu
Why is **int()** broken?

- \(bu
Why isn't my octal data interpreted correctly?

- \(bu
Does Perl have a **round()** function? What about **ceil()** and **floor()**? Trig functions?

- \(bu
How do I convert between numeric representations/bases/radixes?

- \(bu
Why doesn't & work the way I want it to?

- \(bu
How do I multiply matrices?

- \(bu
How do I perform an operation on a series of integers?

- \(bu
How can I output Roman numerals?

- \(bu
Why aren't my random numbers random?

- \(bu
How do I get a random number between X and Y?

- \(bu
How do I find the day or week of the year?

- \(bu
How do I find the current century or millennium?

- \(bu
How can I compare two dates and find the difference?

- \(bu
How can I take a string and turn it into epoch seconds?

- \(bu
How can I find the Julian Day?

- \(bu
How do I find yesterday's date?

- \(bu
Does Perl have a Year 2000 or 2038 problem? Is Perl Y2K compliant?

- \(bu
How do I validate input?

- \(bu
How do I unescape a string?

- \(bu
How do I remove consecutive pairs of characters?

- \(bu
How do I expand function calls in a string?

- \(bu
How do I find matching/nesting anything?

- \(bu
How do I reverse a string?

- \(bu
How do I expand tabs in a string?

- \(bu
How do I reformat a paragraph?

- \(bu
How can I access or change N characters of a string?

- \(bu
How do I change the Nth occurrence of something?

- \(bu
How can I count the number of occurrences of a substring within a string?

- \(bu
How do I capitalize all the words on one line?

- \(bu
How can I split a [character]-delimited string except when inside [character]?

- \(bu
How do I strip blank space from the beginning/end of a string?

- \(bu
How do I pad a string with blanks or pad a number with zeroes?

- \(bu
How do I extract selected columns from a string?

- \(bu
How do I find the soundex value of a string?

- \(bu
How can I expand variables in text strings?

- \(bu
Does Perl have anything like Ruby's #\{\} or Python's f string?

- \(bu
What's wrong with always quoting \*(L"$vars\*(R"?

- \(bu
Why don't my <<\s-1HERE\s0 documents work?

- \(bu
What is the difference between a list and an array?

- \(bu
What is the difference between \f(CW$array[1] and \f(CW@array[1]?

- \(bu
How can I remove duplicate elements from a list or array?

- \(bu
How can I tell whether a certain element is contained in a list or array?

- \(bu
How do I compute the difference of two arrays? How do I compute the intersection of two arrays?

- \(bu
How do I test whether two arrays or hashes are equal?

- \(bu
How do I find the first array element for which a condition is true?

- \(bu
How do I handle linked lists?

- \(bu
How do I handle circular lists?

- \(bu
How do I shuffle an array randomly?

- \(bu
How do I process/modify each element of an array?

- \(bu
How do I select a random element from an array?

- \(bu
How do I permute N elements of a list?

- \(bu
How do I sort an array by (anything)?

- \(bu
How do I manipulate arrays of bits?

- \(bu
Why does **defined()** return true on empty arrays and hashes?

- \(bu
How do I process an entire hash?

- \(bu
How do I merge two hashes?

- \(bu
What happens if I add or remove keys from a hash while iterating over it?

- \(bu
How do I look up a hash element by value?

- \(bu
How can I know how many entries are in a hash?

- \(bu
How do I sort a hash (optionally by value instead of key)?

- \(bu
How can I always keep my hash sorted?

- \(bu
What's the difference between \*(L"delete\*(R" and \*(L"undef\*(R" with hashes?

- \(bu
Why don't my tied hashes make the defined/exists distinction?

- \(bu
How do I reset an **each()** operation part-way through?

- \(bu
How can I get the unique keys from two hashes?

- \(bu
How can I store a multidimensional array in a \s-1DBM\s0 file?

- \(bu
How can I make my hash remember the order I put elements into it?

- \(bu
Why does passing a subroutine an undefined element in a hash create it?

- \(bu
How can I make the Perl equivalent of a C structure/\*(C+ class/hash or array of hashes or arrays?

- \(bu
How can I use a reference as a hash key?

- \(bu
How can I check if a key exists in a multilevel hash?

- \(bu
How can I prevent addition of unwanted keys into a hash?

- \(bu
How do I handle binary data correctly?

- \(bu
How do I determine whether a scalar is a number/whole/integer/float?

- \(bu
How do I keep persistent data across program calls?

- \(bu
How do I print out or copy a recursive data structure?

- \(bu
How do I define methods for every class/object?

- \(bu
How do I verify a credit card checksum?

- \(bu
How do I pack arrays of doubles or floats for \s-1XS\s0 code?

### perlfaq5: Files and Formats

Subsection "perlfaq5: Files and Formats"
This section deals with I/O and the \*(L"f\*(R" issues: filehandles, flushing, formats, and footers.

- \(bu
How do I flush/unbuffer an output filehandle? Why must I do this?

- \(bu
How do I change, delete, or insert a line in a file, or append to the beginning of a file?

- \(bu
How do I count the number of lines in a file?

- \(bu
How do I delete the last N lines from a file?

- \(bu
How can I use Perl's \f(CW\*(C`-i\*(C' option from within a program?

- \(bu
How can I copy a file?

- \(bu
How do I make a temporary file name?

- \(bu
How can I manipulate fixed-record-length files?

- \(bu
How can I make a filehandle local to a subroutine? How do I pass filehandles between subroutines? How do I make an array of filehandles?

- \(bu
How can I use a filehandle indirectly?

- \(bu
How can I open a filehandle to a string?

- \(bu
How can I set up a footer format to be used with **write()**?

- \(bu
How can I **write()** into a string?

- \(bu
How can I output my numbers with commas added?

- \(bu
How can I translate tildes (~) in a filename?

- \(bu
How come when I open a file read-write it wipes it out?

- \(bu
Why do I sometimes get an \*(L"Argument list too long\*(R" when I use <*>?

- \(bu
How can I open a file named with a leading \*(L">\*(R" or trailing blanks?

- \(bu
How can I reliably rename a file?

- \(bu
How can I lock a file?

- \(bu
Why can't I just open(\s-1FH, \*(L"\s0>file.lock\*(R")?

- \(bu
I still don't get locking. I just want to increment the number in the file. How can I do this?

- \(bu
All I want to do is append a small amount of text to the end of a file. Do I still have to use locking?

- \(bu
How do I randomly update a binary file?

- \(bu
How do I get a file's timestamp in perl?

- \(bu
How do I set a file's timestamp in perl?

- \(bu
How do I print to more than one file at once?

- \(bu
How can I read in an entire file all at once?

- \(bu
How can I read in a file by paragraphs?

- \(bu
How can I read a single character from a file? From the keyboard?

- \(bu
How can I tell whether there's a character waiting on a filehandle?

- \(bu
How do I do a \f(CW\*(C`tail -f\*(C' in perl?

- \(bu
How do I **dup()** a filehandle in Perl?

- \(bu
How do I close a file descriptor by number?

- \(bu
Why can't I use \*(L"C:\\temp\\foo\*(R" in \s-1DOS\s0 paths? Why doesn't `C:\\temp\\foo.exe` work?

- \(bu
Why doesn't glob(\*(L"*.*\*(R") get all the files?

- \(bu
Why does Perl let me delete read-only files? Why does \f(CW\*(C`-i\*(C' clobber protected files? Isn't this a bug in Perl?

- \(bu
How do I select a random line from a file?

- \(bu
Why do I get weird spaces when I print an array of lines?

- \(bu
How do I traverse a directory tree?

- \(bu
How do I delete a directory tree?

- \(bu
How do I copy an entire directory?

### perlfaq6: Regular Expressions

Subsection "perlfaq6: Regular Expressions"
This section is surprisingly small because the rest of the \s-1FAQ\s0 is littered with answers involving regular expressions. For example, decoding a \s-1URL\s0 and checking whether something is a number can be handled with regular expressions, but those answers are found elsewhere in this document (in perlfaq9 : \*(L"How do I decode or create those %-encodings on the web\*(R" and perlfaq4 : \*(L"How do I determine whether a scalar is a number/whole/integer/float\*(R", to be precise).

- \(bu
How can I hope to use regular expressions without creating illegible and unmaintainable code?

- \(bu
I'm having trouble matching over more than one line. What's wrong?

- \(bu
How can I pull out lines between two patterns that are themselves on different lines?

- \(bu
How do I match \s-1XML, HTML,\s0 or other nasty, ugly things with a regex?

- \(bu
I put a regular expression into $/ but it didn't work. What's wrong?

- \(bu
How do I substitute case-insensitively on the \s-1LHS\s0 while preserving case on the \s-1RHS\s0?

- \(bu
How can I make \f(CW\*(C`\\w\*(C' match national character sets?

- \(bu
How can I match a locale-smart version of \f(CW\*(C`/[a-zA-Z]/\*(C' ?

- \(bu
How can I quote a variable to use in a regex?

- \(bu
What is \f(CW\*(C`/o\*(C' really for?

- \(bu
How do I use a regular expression to strip C-style comments from a file?

- \(bu
Can I use Perl regular expressions to match balanced text?

- \(bu
What does it mean that regexes are greedy? How can I get around it?

- \(bu
How do I process each word on each line?

- \(bu
How can I print out a word-frequency or line-frequency summary?

- \(bu
How can I do approximate matching?

- \(bu
How do I efficiently match many regular expressions at once?

- \(bu
Why don't word-boundary searches with \f(CW\*(C`\\b\*(C' work for me?

- \(bu
Why does using $&, $`, or $' slow my program down?

- \(bu
What good is \f(CW\*(C`\\G\*(C' in a regular expression?

- \(bu
Are Perl regexes DFAs or NFAs? Are they \s-1POSIX\s0 compliant?

- \(bu
What's wrong with using grep in a void context?

- \(bu
How can I match strings with multibyte characters?

- \(bu
How do I match a regular expression that's in a variable?

### perlfaq7: General Perl Language Issues

Subsection "perlfaq7: General Perl Language Issues"
This section deals with general Perl language issues that don't clearly fit into any of the other sections.

- \(bu
Can I get a BNF/yacc/RE for the Perl language?

- \(bu
What are all these $@%&* punctuation signs, and how do I know when to use them?

- \(bu
Do I always/never have to quote my strings or use semicolons and commas?

- \(bu
How do I skip some return values?

- \(bu
How do I temporarily block warnings?

- \(bu
What's an extension?

- \(bu
Why do Perl operators have different precedence than C operators?

- \(bu
How do I declare/create a structure?

- \(bu
How do I create a module?

- \(bu
How do I adopt or take over a module already on \s-1CPAN\s0?

- \(bu
How do I create a class?

- \(bu
How can I tell if a variable is tainted?

- \(bu
What's a closure?

- \(bu
What is variable suicide and how can I prevent it?

- \(bu
How can I pass/return a \{Function, FileHandle, Array, Hash, Method, Regex\}?

- \(bu
How do I create a static variable?

- \(bu
What's the difference between dynamic and lexical (static) scoping? Between **local()** and **my()**?

- \(bu
How can I access a dynamic variable while a similarly named lexical is in scope?

- \(bu
What's the difference between deep and shallow binding?

- \(bu
Why doesn't \*(L"my($foo) = <$fh>;\*(R" work right?

- \(bu
How do I redefine a builtin function, operator, or method?

- \(bu
What's the difference between calling a function as &foo and **foo()**?

- \(bu
How do I create a switch or case statement?

- \(bu
How can I catch accesses to undefined variables, functions, or methods?

- \(bu
Why can't a method included in this same file be found?

- \(bu
How can I find out my current or calling package?

- \(bu
How can I comment out a large block of Perl code?

- \(bu
How do I clear a package?

- \(bu
How can I use a variable as a variable name?

- \(bu
What does \*(L"bad interpreter\*(R" mean?

- \(bu
Do I need to recompile \s-1XS\s0 modules when there is a change in the C library?

### perlfaq8: System Interaction

Subsection "perlfaq8: System Interaction"
This section of the Perl \s-1FAQ\s0 covers questions involving operating system interaction. Topics include interprocess communication (\s-1IPC\s0), control over the user-interface (keyboard, screen and pointing devices), and most anything else not related to data manipulation.

- \(bu
How do I find out which operating system I'm running under?

- \(bu
How come **exec()** doesn't return?

- \(bu
How do I do fancy stuff with the keyboard/screen/mouse?

- \(bu
How do I print something out in color?

- \(bu
How do I read just one key without waiting for a return key?

- \(bu
How do I check whether input is ready on the keyboard?

- \(bu
How do I clear the screen?

- \(bu
How do I get the screen size?

- \(bu
How do I ask the user for a password?

- \(bu
How do I read and write the serial port?

- \(bu
How do I decode encrypted password files?

- \(bu
How do I start a process in the background?

- \(bu
How do I trap control characters/signals?

- \(bu
How do I modify the shadow password file on a Unix system?

- \(bu
How do I set the time and date?

- \(bu
How can I **sleep()** or **alarm()** for under a second?

- \(bu
How can I measure time under a second?

- \(bu
How can I do an **atexit()** or **setjmp()**/**longjmp()**? (Exception handling)

- \(bu
Why doesn't my sockets program work under System V (Solaris)? What does the error message \*(L"Protocol not supported\*(R" mean?

- \(bu
How can I call my system's unique C functions from Perl?

- \(bu
Where do I get the include files to do **ioctl()** or **syscall()**?

- \(bu
Why do setuid perl scripts complain about kernel problems?

- \(bu
How can I open a pipe both to and from a command?

- \(bu
Why can't I get the output of a command with **system()**?

- \(bu
How can I capture \s-1STDERR\s0 from an external command?

- \(bu
Why doesn't **open()** return an error when a pipe open fails?

- \(bu
What's wrong with using backticks in a void context?

- \(bu
How can I call backticks without shell processing?

- \(bu
Why can't my script read from \s-1STDIN\s0 after I gave it \s-1EOF\s0 (^D on Unix, ^Z on MS-DOS)?

- \(bu
How can I convert my shell script to perl?

- \(bu
Can I use perl to run a telnet or ftp session?

- \(bu
How can I write expect in Perl?

- \(bu
Is there a way to hide perl's command line from programs such as \*(L"ps\*(R"?

- \(bu
I \{changed directory, modified my environment\} in a perl script. How come the change disappeared when I exited the script? How do I get my changes to be visible?

- \(bu
How do I close a process's filehandle without waiting for it to complete?

- \(bu
How do I fork a daemon process?

- \(bu
How do I find out if I'm running interactively or not?

- \(bu
How do I timeout a slow event?

- \(bu
How do I set \s-1CPU\s0 limits?

- \(bu
How do I avoid zombies on a Unix system?

- \(bu
How do I use an \s-1SQL\s0 database?

- \(bu
How do I make a **system()** exit on control-C?

- \(bu
How do I open a file without blocking?

- \(bu
How do I tell the difference between errors from the shell and perl?

- \(bu
How do I install a module from \s-1CPAN\s0?

- \(bu
What's the difference between require and use?

- \(bu
How do I keep my own module/library directory?

- \(bu
How do I add the directory my program lives in to the module/library search path?

- \(bu
How do I add a directory to my include path (@INC) at runtime?

- \(bu
Where are modules installed?

- \(bu
What is socket.ph and where do I get it?

### perlfaq9: Web, Email and Networking

Subsection "perlfaq9: Web, Email and Networking"
This section deals with questions related to running web sites, sending and receiving email as well as general networking.

- \(bu
Should I use a web framework?

- \(bu
Which web framework should I use?

- \(bu
What is Plack and \s-1PSGI\s0?

- \(bu
How do I remove \s-1HTML\s0 from a string?

- \(bu
How do I extract URLs?

- \(bu
How do I fetch an \s-1HTML\s0 file?

- \(bu
How do I automate an \s-1HTML\s0 form submission?

- \(bu
How do I decode or create those %-encodings on the web?

- \(bu
How do I redirect to another page?

- \(bu
How do I put a password on my web pages?

- \(bu
How do I make sure users can't enter values into a form that causes my \s-1CGI\s0 script to do bad things?

- \(bu
How do I parse a mail header?

- \(bu
How do I check a valid mail address?

- \(bu
How do I decode a \s-1MIME/BASE64\s0 string?

- \(bu
How do I find the user's mail address?

- \(bu
How do I send email?

- \(bu
How do I use \s-1MIME\s0 to make an attachment to a mail message?

- \(bu
How do I read email?

- \(bu
How do I find out my hostname, domainname, or \s-1IP\s0 address?

- \(bu
How do I fetch/put an (S)FTP file?

- \(bu
How can I do \s-1RPC\s0 in Perl?

## CREDITS

Header "CREDITS"
Tom Christiansen wrote the original perlfaq then expanded it with the
help of Nat Torkington. brian d foy substantially edited and expanded
the perlfaq. perlfaq-workers and others have also supplied feedback,
patches and corrections over the years.

## AUTHOR AND COPYRIGHT

Header "AUTHOR AND COPYRIGHT"
Tom Christiansen wrote the original version of this document.
brian d foy \f(CW\*(C`<bdfoy@cpan.org>\*(C' wrote this version. See the
individual perlfaq documents for additional copyright information.

This document is available under the same terms as Perl itself. Code
examples in all the perlfaq documents are in the public domain. Use
them as you see fit (and at your own risk with no warranty from anyone).
