+++
operating_system = "macos"
description = "This document describes the behavior and implementation of the PerlIO abstraction described in perlapio when f(CW*(C`USE_PERLIO*(C is defined. The PerlIO abstraction was introduced in perl5.003_02 but languished as just an abstraction until perl5...."
author = "None Specified"
manpage_format = "troff"
date = "2022-02-19"
operating_system_version = "15.3"
detected_package_version = "5.34.1"
manpage_section = "1"
manpage_name = "perliol"
title = "perliol(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLIOL 1"
PERLIOL 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perliol - C API for Perl's implementation of IO in Layers.

## SYNOPSIS

Header "SYNOPSIS"
.Vb 2
    /* Defining a layer ... */
    #include <perliol.h>
.Ve

## DESCRIPTION

Header "DESCRIPTION"
This document describes the behavior and implementation of the PerlIO
abstraction described in perlapio when \f(CW\*(C`USE_PERLIO\*(C' is defined.

### History and Background

Subsection "History and Background"
The PerlIO abstraction was introduced in perl5.003_02 but languished as
just an abstraction until perl5.7.0. However during that time a number
of perl extensions switched to using it, so the \s-1API\s0 is mostly fixed to
maintain (source) compatibility.

The aim of the implementation is to provide the PerlIO \s-1API\s0 in a flexible
and platform neutral manner. It is also a trial of an \*(L"Object Oriented
C, with vtables\*(R" approach which may be applied to Raku.

### Basic Structure

Subsection "Basic Structure"
PerlIO is a stack of layers.

The low levels of the stack work with the low-level operating system
calls (file descriptors in C) getting bytes in and out, the higher
layers of the stack buffer, filter, and otherwise manipulate the I/O,
and return characters (or bytes) to Perl.  Terms *above* and *below*
are used to refer to the relative positioning of the stack layers.

A layer contains a \*(L"vtable\*(R", the table of I/O operations (at C level
a table of function pointers), and status flags.  The functions in the
vtable implement operations like \*(L"open\*(R", \*(L"read\*(R", and \*(L"write\*(R".

When I/O, for example \*(L"read\*(R", is requested, the request goes from Perl
first down the stack using \*(L"read\*(R" functions of each layer, then at the
bottom the input is requested from the operating system services, then
the result is returned up the stack, finally being interpreted as Perl
data.

The requests do not necessarily go always all the way down to the
operating system: that's where PerlIO buffering comes into play.

When you do an **open()** and specify extra PerlIO layers to be deployed,
the layers you specify are \*(L"pushed\*(R" on top of the already existing
default stack.  One way to see it is that \*(L"operating system is
on the left\*(R" and \*(L"Perl is on the right\*(R".

What exact layers are in this default stack depends on a lot of
things: your operating system, Perl version, Perl compile time
configuration, and Perl runtime configuration.  See PerlIO,
\*(L"\s-1PERLIO\*(R"\s0 in perlrun, and open for more information.

**binmode()** operates similarly to **open()**: by default the specified
layers are pushed on top of the existing stack.

However, note that even as the specified layers are \*(L"pushed on top\*(R"
for **open()** and **binmode()**, this doesn't mean that the effects are
limited to the \*(L"top\*(R": PerlIO layers can be very 'active' and inspect
and affect layers also deeper in the stack.  As an example there
is a layer called \*(L"raw\*(R" which repeatedly \*(L"pops\*(R" layers until
it reaches the first layer that has declared itself capable of
handling binary data.  The \*(L"pushed\*(R" layers are processed in left-to-right
order.

**sysopen()** operates (unsurprisingly) at a lower level in the stack than
**open()**.  For example in Unix or Unix-like systems **sysopen()** operates
directly at the level of file descriptors: in the terms of PerlIO
layers, it uses only the \*(L"unix\*(R" layer, which is a rather thin wrapper
on top of the Unix file descriptors.

### Layers vs Disciplines

Subsection "Layers vs Disciplines"
Initial discussion of the ability to modify \s-1IO\s0 streams behaviour used
the term \*(L"discipline\*(R" for the entities which were added. This came (I
believe) from the use of the term in \*(L"sfio\*(R", which in turn borrowed it
from \*(L"line disciplines\*(R" on Unix terminals. However, this document (and
the C code) uses the term \*(L"layer\*(R".

This is, I hope, a natural term given the implementation, and should
avoid connotations that are inherent in earlier uses of \*(L"discipline\*(R"
for things which are rather different.

### Data Structures

Subsection "Data Structures"
The basic data structure is a PerlIOl:

.Vb 3
        typedef struct _PerlIO PerlIOl;
        typedef struct _PerlIO_funcs PerlIO_funcs;
        typedef PerlIOl *PerlIO;

        struct _PerlIO
        \{
         PerlIOl *      next;       /* Lower layer */
         PerlIO_funcs * tab;        /* Functions for this layer */
         U32            flags;      /* Various flags for state */
        \};
.Ve

A \f(CW\*(C`PerlIOl *\*(C' is a pointer to the struct, and the *application*
level \f(CW\*(C`PerlIO *\*(C' is a pointer to a \f(CW\*(C`PerlIOl *\*(C' - i.e. a pointer
to a pointer to the struct. This allows the application level \f(CW\*(C`PerlIO *\*(C'
to remain constant while the actual \f(CW\*(C`PerlIOl *\*(C' underneath
changes. (Compare perl's \f(CW\*(C`SV *\*(C' which remains constant while its
\f(CW\*(C`sv_any\*(C' field changes as the scalar's type changes.) An \s-1IO\s0 stream is
then in general represented as a pointer to this linked-list of
\*(L"layers\*(R".

It should be noted that because of the double indirection in a \f(CW\*(C`PerlIO *\*(C',
a \f(CW\*(C`&(perlio->next)\*(C' \*(L"is\*(R" a \f(CW\*(C`PerlIO *\*(C', and so to some degree
at least one layer can use the \*(L"standard\*(R" \s-1API\s0 on the next layer down.

A \*(L"layer\*(R" is composed of two parts:

- 1.
The functions and attributes of the \*(L"layer class\*(R".

- 2.
The per-instance data for a particular handle.

### Functions and Attributes

Subsection "Functions and Attributes"
The functions and attributes are accessed via the \*(L"tab\*(R" (for table)
member of \f(CW\*(C`PerlIOl\*(C'. The functions (methods of the layer \*(L"class\*(R") are
fixed, and are defined by the \f(CW\*(C`PerlIO_funcs\*(C' type. They are broadly the
same as the public \f(CW\*(C`PerlIO_xxxxx\*(C' functions:

.Vb 10
 struct _PerlIO_funcs
 \{
  Size_t     fsize;
  char *     name;
  Size_t     size;
  IV         kind;
  IV         (*Pushed)(pTHX_ PerlIO *f,
                             const char *mode,
                             SV *arg,
                             PerlIO_funcs *tab);
  IV         (*Popped)(pTHX_ PerlIO *f);
  PerlIO *   (*Open)(pTHX_ PerlIO_funcs *tab,
                           PerlIO_list_t *layers, IV n,
                           const char *mode,
                           int fd, int imode, int perm,
                           PerlIO *old,
                           int narg, SV **args);
  IV         (*Binmode)(pTHX_ PerlIO *f);
  SV *       (*Getarg)(pTHX_ PerlIO *f, CLONE_PARAMS *param, int flags)
  IV         (*Fileno)(pTHX_ PerlIO *f);
  PerlIO *   (*Dup)(pTHX_ PerlIO *f,
                          PerlIO *o,
                          CLONE_PARAMS *param,
                          int flags)
  /* Unix-like functions - cf sfio line disciplines */
  SSize_t    (*Read)(pTHX_ PerlIO *f, void *vbuf, Size_t count);
  SSize_t    (*Unread)(pTHX_ PerlIO *f, const void *vbuf, Size_t count);
  SSize_t    (*Write)(pTHX_ PerlIO *f, const void *vbuf, Size_t count);
  IV         (*Seek)(pTHX_ PerlIO *f, Off_t offset, int whence);
  Off_t      (*Tell)(pTHX_ PerlIO *f);
  IV         (*Close)(pTHX_ PerlIO *f);
  /* Stdio-like buffered IO functions */
  IV         (*Flush)(pTHX_ PerlIO *f);
  IV         (*Fill)(pTHX_ PerlIO *f);
  IV         (*Eof)(pTHX_ PerlIO *f);
  IV         (*Error)(pTHX_ PerlIO *f);
  void       (*Clearerr)(pTHX_ PerlIO *f);
  void       (*Setlinebuf)(pTHX_ PerlIO *f);
  /* Perl\*(Aqs snooping functions */
  STDCHAR *  (*Get_base)(pTHX_ PerlIO *f);
  Size_t     (*Get_bufsiz)(pTHX_ PerlIO *f);
  STDCHAR *  (*Get_ptr)(pTHX_ PerlIO *f);
  SSize_t    (*Get_cnt)(pTHX_ PerlIO *f);
  void       (*Set_ptrcnt)(pTHX_ PerlIO *f,STDCHAR *ptr,SSize_t cnt);
 \};
.Ve

The first few members of the struct give a function table size for
compatibility check \*(L"name\*(R" for the layer, the  size to \f(CW\*(C`malloc\*(C' for the per-instance data,
and some flags which are attributes of the class as whole (such as whether it is a buffering
layer), then follow the functions which fall into four basic groups:

- 1.
Opening and setup functions

- 2.
Basic \s-1IO\s0 operations

- 3.
Stdio class buffering options.

- 4.
Functions to support Perl's traditional \*(L"fast\*(R" access to the buffer.

A layer does not have to implement all the functions, but the whole
table has to be present. Unimplemented slots can be \s-1NULL\s0 (which will
result in an error when called) or can be filled in with stubs to
\*(L"inherit\*(R" behaviour from a \*(L"base class\*(R". This \*(L"inheritance\*(R" is fixed
for all instances of the layer, but as the layer chooses which stubs
to populate the table, limited \*(L"multiple inheritance\*(R" is possible.

### Per-instance Data

Subsection "Per-instance Data"
The per-instance data are held in memory beyond the basic PerlIOl
struct, by making a PerlIOl the first member of the layer's struct
thus:

.Vb 10
        typedef struct
        \{
         struct _PerlIO base;       /* Base "class" info */
         STDCHAR *      buf;        /* Start of buffer */
         STDCHAR *      end;        /* End of valid part of buffer */
         STDCHAR *      ptr;        /* Current position in buffer */
         Off_t          posn;       /* Offset of buf into the file */
         Size_t         bufsiz;     /* Real size of buffer */
         IV             oneword;    /* Emergency buffer */
        \} PerlIOBuf;
.Ve

In this way (as for perl's scalars) a pointer to a PerlIOBuf can be
treated as a pointer to a PerlIOl.

### Layers in action.

Subsection "Layers in action."
.Vb 8
                table           perlio          unix
            |           |
            +-----------+    +----------+    +--------+
   PerlIO ->|           |--->|  next    |--->|  NULL  |
            +-----------+    +----------+    +--------+
            |           |    |  buffer  |    |   fd   |
            +-----------+    |          |    +--------+
            |           |    +----------+
.Ve

The above attempts to show how the layer scheme works in a simple case.
The application's \f(CW\*(C`PerlIO *\*(C' points to an entry in the table(s)
representing open (allocated) handles. For example the first three slots
in the table correspond to \f(CW\*(C`stdin\*(C',\f(CW\*(C`stdout\*(C' and \f(CW\*(C`stderr\*(C'. The table
in turn points to the current \*(L"top\*(R" layer for the handle - in this case
an instance of the generic buffering layer \*(L"perlio\*(R". That layer in turn
points to the next layer down - in this case the low-level \*(L"unix\*(R" layer.

The above is roughly equivalent to a \*(L"stdio\*(R" buffered stream, but with
much more flexibility:

- \(bu
If Unix level \f(CW\*(C`read\*(C'/\f(CW\*(C`write\*(C'/\f(CW\*(C`lseek\*(C' is not appropriate for (say)
sockets then the \*(L"unix\*(R" layer can be replaced (at open time or even
dynamically) with a \*(L"socket\*(R" layer.

- \(bu
Different handles can have different buffering schemes. The \*(L"top\*(R"
layer could be the \*(L"mmap\*(R" layer if reading disk files was quicker
using \f(CW\*(C`mmap\*(C' than \f(CW\*(C`read\*(C'. An \*(L"unbuffered\*(R" stream can be implemented
simply by not having a buffer layer.

- \(bu
Extra layers can be inserted to process the data as it flows through.
This was the driving need for including the scheme in perl 5.7.0+ - we
needed a mechanism to allow data to be translated between perl's
internal encoding (conceptually at least Unicode as \s-1UTF-8\s0), and the
\*(L"native\*(R" format used by the system. This is provided by the
\*(L":encoding(xxxx)\*(R" layer which typically sits above the buffering layer.

- \(bu
A layer can be added that does \*(L"\\n\*(R" to \s-1CRLF\s0 translation. This layer
can be used on any platform, not just those that normally do such
things.

### Per-instance flag bits

Subsection "Per-instance flag bits"
The generic flag bits are a hybrid of \f(CW\*(C`O_XXXXX\*(C' style flags deduced
from the mode string passed to \f(CW\*(C`PerlIO_open()\*(C', and state bits for
typical buffer layers.

- \s-1PERLIO_F_EOF\s0
Item "PERLIO_F_EOF"
End of file.

- \s-1PERLIO_F_CANWRITE\s0
Item "PERLIO_F_CANWRITE"
Writes are permitted, i.e. opened as \*(L"w\*(R" or \*(L"r+\*(R" or \*(L"a\*(R", etc.

- \s-1PERLIO_F_CANREAD\s0
Item "PERLIO_F_CANREAD"
Reads are permitted i.e. opened \*(L"r\*(R" or \*(L"w+\*(R" (or even \*(L"a+\*(R" - ick).

- \s-1PERLIO_F_ERROR\s0
Item "PERLIO_F_ERROR"
An error has occurred (for \f(CW\*(C`PerlIO_error()\*(C').

- \s-1PERLIO_F_TRUNCATE\s0
Item "PERLIO_F_TRUNCATE"
Truncate file suggested by open mode.

- \s-1PERLIO_F_APPEND\s0
Item "PERLIO_F_APPEND"
All writes should be appends.

- \s-1PERLIO_F_CRLF\s0
Item "PERLIO_F_CRLF"
Layer is performing Win32-like \*(L"\\n\*(R" mapped to \s-1CR,LF\s0 for output and \s-1CR,LF\s0
mapped to \*(L"\\n\*(R" for input. Normally the provided \*(L"crlf\*(R" layer is the only
layer that need bother about this. \f(CW\*(C`PerlIO_binmode()\*(C' will mess with this
flag rather than add/remove layers if the \f(CW\*(C`PERLIO_K_CANCRLF\*(C' bit is set
for the layers class.

- \s-1PERLIO_F_UTF8\s0
Item "PERLIO_F_UTF8"
Data written to this layer should be \s-1UTF-8\s0 encoded; data provided
by this layer should be considered \s-1UTF-8\s0 encoded. Can be set on any layer
by \*(L":utf8\*(R" dummy layer. Also set on \*(L":encoding\*(R" layer.

- \s-1PERLIO_F_UNBUF\s0
Item "PERLIO_F_UNBUF"
Layer is unbuffered - i.e. write to next layer down should occur for
each write to this layer.

- \s-1PERLIO_F_WRBUF\s0
Item "PERLIO_F_WRBUF"
The buffer for this layer currently holds data written to it but not sent
to next layer.

- \s-1PERLIO_F_RDBUF\s0
Item "PERLIO_F_RDBUF"
The buffer for this layer currently holds unconsumed data read from
layer below.

- \s-1PERLIO_F_LINEBUF\s0
Item "PERLIO_F_LINEBUF"
Layer is line buffered. Write data should be passed to next layer down
whenever a \*(L"\\n\*(R" is seen. Any data beyond the \*(L"\\n\*(R" should then be
processed.

- \s-1PERLIO_F_TEMP\s0
Item "PERLIO_F_TEMP"
File has been \f(CW\*(C`unlink()\*(C'ed, or should be deleted on \f(CW\*(C`close()\*(C'.

- \s-1PERLIO_F_OPEN\s0
Item "PERLIO_F_OPEN"
Handle is open.

- \s-1PERLIO_F_FASTGETS\s0
Item "PERLIO_F_FASTGETS"
This instance of this layer supports the "fast \f(CW\*(C`gets\*(C'" interface.
Normally set based on \f(CW\*(C`PERLIO_K_FASTGETS\*(C' for the class and by the
existence of the function(s) in the table. However a class that
normally provides that interface may need to avoid it on a
particular instance. The \*(L"pending\*(R" layer needs to do this when
it is pushed above a layer which does not support the interface.
(Perl's \f(CW\*(C`sv_gets()\*(C' does not expect the streams fast \f(CW\*(C`gets\*(C' behaviour
to change during one \*(L"get\*(R".)

### Methods in Detail

Subsection "Methods in Detail"

- fsize
Item "fsize"
.Vb 1
        Size_t fsize;
.Ve
.Sp
Size of the function table. This is compared against the value PerlIO
code \*(L"knows\*(R" as a compatibility check. Future versions *may* be able
to tolerate layers compiled against an old version of the headers.

- name
Item "name"
.Vb 1
        char * name;
.Ve
.Sp
The name of the layer whose **open()** method Perl should invoke on
**open()**.  For example if the layer is called \s-1APR,\s0 you will call:
.Sp
.Vb 1
  open $fh, ">:APR", ...
.Ve
.Sp
and Perl knows that it has to invoke the **PerlIOAPR_open()** method
implemented by the \s-1APR\s0 layer.

- size
Item "size"
.Vb 1
        Size_t size;
.Ve
.Sp
The size of the per-instance data structure, e.g.:
.Sp
.Vb 1
  sizeof(PerlIOAPR)
.Ve
.Sp
If this field is zero then \f(CW\*(C`PerlIO_pushed\*(C' does not malloc anything
and assumes layer's Pushed function will do any required layer stack
manipulation - used to avoid malloc/free overhead for dummy layers.
If the field is non-zero it must be at least the size of \f(CW\*(C`PerlIOl\*(C',
\f(CW\*(C`PerlIO_pushed\*(C' will allocate memory for the layer's data structures
and link new layer onto the stream's stack. (If the layer's Pushed
method returns an error indication the layer is popped again.)

- kind
Item "kind"
.Vb 1
        IV kind;
.Ve

> 
- \(bu
\s-1PERLIO_K_BUFFERED\s0
.Sp
The layer is buffered.

- \(bu
\s-1PERLIO_K_RAW\s0
.Sp
The layer is acceptable to have in a binmode(\s-1FH\s0) stack - i.e. it does not
(or will configure itself not to) transform bytes passing through it.

- \(bu
\s-1PERLIO_K_CANCRLF\s0
.Sp
Layer can translate between \*(L"\\n\*(R" and \s-1CRLF\s0 line ends.

- \(bu
\s-1PERLIO_K_FASTGETS\s0
.Sp
Layer allows buffer snooping.

- \(bu
\s-1PERLIO_K_MULTIARG\s0
.Sp
Used when the layer's **open()** accepts more arguments than usual. The
extra arguments should come not before the \f(CW\*(C`MODE\*(C' argument. When this
flag is used it's up to the layer to validate the args.



> 


- Pushed
Item "Pushed"
.Vb 1
 IV     (*Pushed)(pTHX_ PerlIO *f,const char *mode, SV *arg);
.Ve
.Sp
The only absolutely mandatory method. Called when the layer is pushed
onto the stack.  The \f(CW\*(C`mode\*(C' argument may be \s-1NULL\s0 if this occurs
post-open. The \f(CW\*(C`arg\*(C' will be non-\f(CW\*(C`NULL\*(C' if an argument string was
passed. In most cases this should call \f(CW\*(C`PerlIOBase_pushed()\*(C' to
convert \f(CW\*(C`mode\*(C' into the appropriate \f(CW\*(C`PERLIO_F_XXXXX\*(C' flags in
addition to any actions the layer itself takes.  If a layer is not
expecting an argument it need neither save the one passed to it, nor
provide \f(CW\*(C`Getarg()\*(C' (it could perhaps \f(CW\*(C`Perl_warn\*(C' that the argument
was un-expected).
.Sp
Returns 0 on success. On failure returns -1 and should set errno.

- Popped
Item "Popped"
.Vb 1
        IV      (*Popped)(pTHX_ PerlIO *f);
.Ve
.Sp
Called when the layer is popped from the stack. A layer will normally
be popped after \f(CW\*(C`Close()\*(C' is called. But a layer can be popped
without being closed if the program is dynamically managing layers on
the stream. In such cases \f(CW\*(C`Popped()\*(C' should free any resources
(buffers, translation tables, ...) not held directly in the layer's
struct.  It should also \f(CW\*(C`Unread()\*(C' any unconsumed data that has been
read and buffered from the layer below back to that layer, so that it
can be re-provided to what ever is now above.
.Sp
Returns 0 on success and failure.  If \f(CW\*(C`Popped()\*(C' returns *true* then
*perlio.c* assumes that either the layer has popped itself, or the
layer is super special and needs to be retained for other reasons.
In most cases it should return *false*.

- Open
Item "Open"
.Vb 1
        PerlIO *        (*Open)(...);
.Ve
.Sp
The \f(CW\*(C`Open()\*(C' method has lots of arguments because it combines the
functions of perl's \f(CW\*(C`open\*(C', \f(CW\*(C`PerlIO_open\*(C', perl's \f(CW\*(C`sysopen\*(C',
\f(CW\*(C`PerlIO_fdopen\*(C' and \f(CW\*(C`PerlIO_reopen\*(C'.  The full prototype is as
follows:
.Sp
.Vb 6
 PerlIO *       (*Open)(pTHX_ PerlIO_funcs *tab,
                        PerlIO_list_t *layers, IV n,
                        const char *mode,
                        int fd, int imode, int perm,
                        PerlIO *old,
                        int narg, SV **args);
.Ve
.Sp
Open should (perhaps indirectly) call \f(CW\*(C`PerlIO_allocate()\*(C' to allocate
a slot in the table and associate it with the layers information for
the opened file, by calling \f(CW\*(C`PerlIO_push\*(C'.  The *layers* is an
array of all the layers destined for the \f(CW\*(C`PerlIO *\*(C', and any
arguments passed to them, *n* is the index into that array of the
layer being called. The macro \f(CW\*(C`PerlIOArg\*(C' will return a (possibly
\f(CW\*(C`NULL\*(C') \s-1SV\s0 * for the argument passed to the layer.
.Sp
Where a layer opens or takes ownership of a file descriptor, that layer is
responsible for getting the file descriptor's close-on-exec flag into the
correct state.  The flag should be clear for a file descriptor numbered
less than or equal to \f(CW\*(C`PL_maxsysfd\*(C', and set for any file descriptor
numbered higher.  For thread safety, when a layer opens a new file
descriptor it should if possible open it with the close-on-exec flag
initially set.
.Sp
The *mode* string is an "\f(CW\*(C`fopen()\*(C'-like" string which would match
the regular expression \f(CW\*(C`/^[I#]?[rwa]\\+?[bt]?$/\*(C'.
.Sp
The \f(CW\*(AqI\*(Aq prefix is used during creation of \f(CW\*(C`stdin\*(C'..\f(CW\*(C`stderr\*(C' via
special \f(CW\*(C`PerlIO_fdopen\*(C' calls; the \f(CW\*(Aq#\*(Aq prefix means that this is
\f(CW\*(C`sysopen\*(C' and that *imode* and *perm* should be passed to
\f(CW\*(C`PerlLIO_open3\*(C'; \f(CW\*(Aqr\*(Aq means **r**ead, \f(CW\*(Aqw\*(Aq means **w**rite and
\f(CW\*(Aqa\*(Aq means **a**ppend. The \f(CW\*(Aq+\*(Aq suffix means that both reading and
writing/appending are permitted.  The \f(CW\*(Aqb\*(Aq suffix means file should
be binary, and \f(CW\*(Aqt\*(Aq means it is text. (Almost all layers should do
the \s-1IO\s0 in binary mode, and ignore the b/t bits. The \f(CW\*(C`:crlf\*(C' layer
should be pushed to handle the distinction.)
.Sp
If *old* is not \f(CW\*(C`NULL\*(C' then this is a \f(CW\*(C`PerlIO_reopen\*(C'. Perl itself
does not use this (yet?) and semantics are a little vague.
.Sp
If *fd* not negative then it is the numeric file descriptor *fd*,
which will be open in a manner compatible with the supplied mode
string, the call is thus equivalent to \f(CW\*(C`PerlIO_fdopen\*(C'. In this case
*nargs* will be zero.
The file descriptor may have the close-on-exec flag either set or clear;
it is the responsibility of the layer that takes ownership of it to get
the flag into the correct state.
.Sp
If *nargs* is greater than zero then it gives the number of arguments
passed to \f(CW\*(C`open\*(C', otherwise it will be 1 if for example
\f(CW\*(C`PerlIO_open\*(C' was called.  In simple cases SvPV_nolen(*args) is the
pathname to open.
.Sp
If a layer provides \f(CW\*(C`Open()\*(C' it should normally call the \f(CW\*(C`Open()\*(C'
method of next layer down (if any) and then push itself on top if that
succeeds.  \f(CW\*(C`PerlIOBase_open\*(C' is provided to do exactly that, so in
most cases you don't have to write your own \f(CW\*(C`Open()\*(C' method.  If this
method is not defined, other layers may have difficulty pushing
themselves on top of it during open.
.Sp
If \f(CW\*(C`PerlIO_push\*(C' was performed and open has failed, it must
\f(CW\*(C`PerlIO_pop\*(C' itself, since if it's not, the layer won't be removed
and may cause bad problems.
.Sp
Returns \f(CW\*(C`NULL\*(C' on failure.

- Binmode
Item "Binmode"
.Vb 1
        IV        (*Binmode)(pTHX_ PerlIO *f);
.Ve
.Sp
Optional. Used when \f(CW\*(C`:raw\*(C' layer is pushed (explicitly or as a result
of binmode(\s-1FH\s0)). If not present layer will be popped. If present
should configure layer as binary (or pop itself) and return 0.
If it returns -1 for error \f(CW\*(C`binmode\*(C' will fail with layer
still on the stack.

- Getarg
Item "Getarg"
.Vb 2
        SV *      (*Getarg)(pTHX_ PerlIO *f,
                            CLONE_PARAMS *param, int flags);
.Ve
.Sp
Optional. If present should return an \s-1SV\s0 * representing the string
argument passed to the layer when it was
pushed. e.g. \*(L":encoding(ascii)\*(R" would return an SvPV with value
\*(L"ascii\*(R". (*param* and *flags* arguments can be ignored in most
cases)
.Sp
\f(CW\*(C`Dup\*(C' uses \f(CW\*(C`Getarg\*(C' to retrieve the argument originally passed to
\f(CW\*(C`Pushed\*(C', so you must implement this function if your layer has an
extra argument to \f(CW\*(C`Pushed\*(C' and will ever be \f(CW\*(C`Dup\*(C'ed.

- Fileno
Item "Fileno"
.Vb 1
        IV        (*Fileno)(pTHX_ PerlIO *f);
.Ve
.Sp
Returns the Unix/Posix numeric file descriptor for the handle. Normally
\f(CW\*(C`PerlIOBase_fileno()\*(C' (which just asks next layer down) will suffice
for this.
.Sp
Returns -1 on error, which is considered to include the case where the
layer cannot provide such a file descriptor.

- Dup
Item "Dup"
.Vb 2
        PerlIO * (*Dup)(pTHX_ PerlIO *f, PerlIO *o,
                        CLONE_PARAMS *param, int flags);
.Ve
.Sp
\s-1XXX:\s0 Needs more docs.
.Sp
Used as part of the \*(L"clone\*(R" process when a thread is spawned (in which
case param will be non-NULL) and when a stream is being duplicated via
'&' in the \f(CW\*(C`open\*(C'.
.Sp
Similar to \f(CW\*(C`Open\*(C', returns PerlIO* on success, \f(CW\*(C`NULL\*(C' on failure.

- Read
Item "Read"
.Vb 1
        SSize_t (*Read)(pTHX_ PerlIO *f, void *vbuf, Size_t count);
.Ve
.Sp
Basic read operation.
.Sp
Typically will call \f(CW\*(C`Fill\*(C' and manipulate pointers (possibly via the
\s-1API\s0).  \f(CW\*(C`PerlIOBuf_read()\*(C' may be suitable for derived classes which
provide \*(L"fast gets\*(R" methods.
.Sp
Returns actual bytes read, or -1 on an error.

- Unread
Item "Unread"
.Vb 2
        SSize_t (*Unread)(pTHX_ PerlIO *f,
                          const void *vbuf, Size_t count);
.Ve
.Sp
A superset of stdio's \f(CW\*(C`ungetc()\*(C'. Should arrange for future reads to
see the bytes in \f(CW\*(C`vbuf\*(C'. If there is no obviously better implementation
then \f(CW\*(C`PerlIOBase_unread()\*(C' provides the function by pushing a \*(L"fake\*(R"
\*(L"pending\*(R" layer above the calling layer.
.Sp
Returns the number of unread chars.

- Write
Item "Write"
.Vb 1
        SSize_t (*Write)(PerlIO *f, const void *vbuf, Size_t count);
.Ve
.Sp
Basic write operation.
.Sp
Returns bytes written or -1 on an error.

- Seek
Item "Seek"
.Vb 1
        IV      (*Seek)(pTHX_ PerlIO *f, Off_t offset, int whence);
.Ve
.Sp
Position the file pointer. Should normally call its own \f(CW\*(C`Flush\*(C'
method and then the \f(CW\*(C`Seek\*(C' method of next layer down.
.Sp
Returns 0 on success, -1 on failure.

- Tell
Item "Tell"
.Vb 1
        Off_t   (*Tell)(pTHX_ PerlIO *f);
.Ve
.Sp
Return the file pointer. May be based on layers cached concept of
position to avoid overhead.
.Sp
Returns -1 on failure to get the file pointer.

- Close
Item "Close"
.Vb 1
        IV      (*Close)(pTHX_ PerlIO *f);
.Ve
.Sp
Close the stream. Should normally call \f(CW\*(C`PerlIOBase_close()\*(C' to flush
itself and close layers below, and then deallocate any data structures
(buffers, translation tables, ...) not  held directly in the data
structure.
.Sp
Returns 0 on success, -1 on failure.

- Flush
Item "Flush"
.Vb 1
        IV      (*Flush)(pTHX_ PerlIO *f);
.Ve
.Sp
Should make stream's state consistent with layers below. That is, any
buffered write data should be written, and file position of lower layers
adjusted for data read from below but not actually consumed.
(Should perhaps \f(CW\*(C`Unread()\*(C' such data to the lower layer.)
.Sp
Returns 0 on success, -1 on failure.

- Fill
Item "Fill"
.Vb 1
        IV      (*Fill)(pTHX_ PerlIO *f);
.Ve
.Sp
The buffer for this layer should be filled (for read) from layer
below.  When you \*(L"subclass\*(R" PerlIOBuf layer, you want to use its
*_read* method and to supply your own fill method, which fills the
PerlIOBuf's buffer.
.Sp
Returns 0 on success, -1 on failure.

- Eof
Item "Eof"
.Vb 1
        IV      (*Eof)(pTHX_ PerlIO *f);
.Ve
.Sp
Return end-of-file indicator. \f(CW\*(C`PerlIOBase_eof()\*(C' is normally sufficient.
.Sp
Returns 0 on end-of-file, 1 if not end-of-file, -1 on error.

- Error
Item "Error"
.Vb 1
        IV      (*Error)(pTHX_ PerlIO *f);
.Ve
.Sp
Return error indicator. \f(CW\*(C`PerlIOBase_error()\*(C' is normally sufficient.
.Sp
Returns 1 if there is an error (usually when \f(CW\*(C`PERLIO_F_ERROR\*(C' is set),
0 otherwise.

- Clearerr
Item "Clearerr"
.Vb 1
        void    (*Clearerr)(pTHX_ PerlIO *f);
.Ve
.Sp
Clear end-of-file and error indicators. Should call \f(CW\*(C`PerlIOBase_clearerr()\*(C'
to set the \f(CW\*(C`PERLIO_F_XXXXX\*(C' flags, which may suffice.

- Setlinebuf
Item "Setlinebuf"
.Vb 1
        void    (*Setlinebuf)(pTHX_ PerlIO *f);
.Ve
.Sp
Mark the stream as line buffered. \f(CW\*(C`PerlIOBase_setlinebuf()\*(C' sets the
\s-1PERLIO_F_LINEBUF\s0 flag and is normally sufficient.

- Get_base
Item "Get_base"
.Vb 1
        STDCHAR *       (*Get_base)(pTHX_ PerlIO *f);
.Ve
.Sp
Allocate (if not already done so) the read buffer for this layer and
return pointer to it. Return \s-1NULL\s0 on failure.

- Get_bufsiz
Item "Get_bufsiz"
.Vb 1
        Size_t  (*Get_bufsiz)(pTHX_ PerlIO *f);
.Ve
.Sp
Return the number of bytes that last \f(CW\*(C`Fill()\*(C' put in the buffer.

- Get_ptr
Item "Get_ptr"
.Vb 1
        STDCHAR *       (*Get_ptr)(pTHX_ PerlIO *f);
.Ve
.Sp
Return the current read pointer relative to this layer's buffer.

- Get_cnt
Item "Get_cnt"
.Vb 1
        SSize_t (*Get_cnt)(pTHX_ PerlIO *f);
.Ve
.Sp
Return the number of bytes left to be read in the current buffer.

- Set_ptrcnt
Item "Set_ptrcnt"
.Vb 2
        void    (*Set_ptrcnt)(pTHX_ PerlIO *f,
                              STDCHAR *ptr, SSize_t cnt);
.Ve
.Sp
Adjust the read pointer and count of bytes to match \f(CW\*(C`ptr\*(C' and/or \f(CW\*(C`cnt\*(C'.
The application (or layer above) must ensure they are consistent.
(Checking is allowed by the paranoid.)

### Utilities

Subsection "Utilities"
To ask for the next layer down use PerlIONext(PerlIO *f).

To check that a PerlIO* is valid use PerlIOValid(PerlIO *f).  (All
this does is really just to check that the pointer is non-NULL and
that the pointer behind that is non-NULL.)

PerlIOBase(PerlIO *f) returns the \*(L"Base\*(R" pointer, or in other words,
the \f(CW\*(C`PerlIOl*\*(C' pointer.

PerlIOSelf(PerlIO* f, type) return the PerlIOBase cast to a type.

Perl_PerlIO_or_Base(PerlIO* f, callback, base, failure, args) either
calls the *callback* from the functions of the layer *f* (just by
the name of the \s-1IO\s0 function, like \*(L"Read\*(R") with the *args*, or if
there is no such callback, calls the *base* version of the callback
with the same args, or if the f is invalid, set errno to \s-1EBADF\s0 and
return *failure*.

Perl_PerlIO_or_fail(PerlIO* f, callback, failure, args) either calls
the *callback* of the functions of the layer *f* with the *args*,
or if there is no such callback, set errno to \s-1EINVAL.\s0  Or if the f is
invalid, set errno to \s-1EBADF\s0 and return *failure*.

Perl_PerlIO_or_Base_void(PerlIO* f, callback, base, args) either calls
the *callback* of the functions of the layer *f* with the *args*,
or if there is no such callback, calls the *base* version of the
callback with the same args, or if the f is invalid, set errno to
\s-1EBADF.\s0

Perl_PerlIO_or_fail_void(PerlIO* f, callback, args) either calls the
*callback* of the functions of the layer *f* with the *args*, or if
there is no such callback, set errno to \s-1EINVAL.\s0  Or if the f is
invalid, set errno to \s-1EBADF.\s0

### Implementing PerlIO Layers

Subsection "Implementing PerlIO Layers"
If you find the implementation document unclear or not sufficient,
look at the existing PerlIO layer implementations, which include:

- \(bu
C implementations
.Sp
The *perlio.c* and *perliol.h* in the Perl core implement the
\*(L"unix\*(R", \*(L"perlio\*(R", \*(L"stdio\*(R", \*(L"crlf\*(R", \*(L"utf8\*(R", \*(L"byte\*(R", \*(L"raw\*(R", \*(L"pending\*(R"
layers, and also the \*(L"mmap\*(R" and \*(L"win32\*(R" layers if applicable.
(The \*(L"win32\*(R" is currently unfinished and unused, to see what is used
instead in Win32, see \*(L"Querying the layers of filehandles\*(R" in PerlIO .)
.Sp
PerlIO::encoding, PerlIO::scalar, PerlIO::via in the Perl core.
.Sp
PerlIO::gzip and APR::PerlIO (mod_perl 2.0) on \s-1CPAN.\s0

- \(bu
Perl implementations
.Sp
PerlIO::via::QuotedPrint in the Perl core and PerlIO::via::* on \s-1CPAN.\s0

If you are creating a PerlIO layer, you may want to be lazy, in other
words, implement only the methods that interest you.  The other methods
you can either replace with the \*(L"blank\*(R" methods

.Vb 2
    PerlIOBase_noop_ok
    PerlIOBase_noop_fail
.Ve

(which do nothing, and return zero and -1, respectively) or for
certain methods you may assume a default behaviour by using a \s-1NULL\s0
method.  The Open method looks for help in the 'parent' layer.
The following table summarizes the behaviour:

.Vb 1
    method      behaviour with NULL

    Clearerr    PerlIOBase_clearerr
    Close       PerlIOBase_close
    Dup         PerlIOBase_dup
    Eof         PerlIOBase_eof
    Error       PerlIOBase_error
    Fileno      PerlIOBase_fileno
    Fill        FAILURE
    Flush       SUCCESS
    Getarg      SUCCESS
    Get_base    FAILURE
    Get_bufsiz  FAILURE
    Get_cnt     FAILURE
    Get_ptr     FAILURE
    Open        INHERITED
    Popped      SUCCESS
    Pushed      SUCCESS
    Read        PerlIOBase_read
    Seek        FAILURE
    Set_cnt     FAILURE
    Set_ptrcnt  FAILURE
    Setlinebuf  PerlIOBase_setlinebuf
    Tell        FAILURE
    Unread      PerlIOBase_unread
    Write       FAILURE

 FAILURE        Set errno (to EINVAL in Unixish, to LIB$_INVARG in VMS)
                and return -1 (for numeric return values) or NULL (for
                pointers)
 INHERITED      Inherited from the layer below
 SUCCESS        Return 0 (for numeric return values) or a pointer
.Ve

### Core Layers

Subsection "Core Layers"
The file \f(CW\*(C`perlio.c\*(C' provides the following layers:
.ie n .IP """unix""" 4
.el .IP "``unix''" 4
Item "unix"
A basic non-buffered layer which calls Unix/POSIX \f(CW\*(C`read()\*(C', \f(CW\*(C`write()\*(C',
\f(CW\*(C`lseek()\*(C', \f(CW\*(C`close()\*(C'. No buffering. Even on platforms that distinguish
between O_TEXT and O_BINARY this layer is always O_BINARY.
.ie n .IP """perlio""" 4
.el .IP "``perlio''" 4
Item "perlio"
A very complete generic buffering layer which provides the whole of
PerlIO \s-1API.\s0 It is also intended to be used as a \*(L"base class\*(R" for other
layers. (For example its \f(CW\*(C`Read()\*(C' method is implemented in terms of
the \f(CW\*(C`Get_cnt()\*(C'/\f(CW\*(C`Get_ptr()\*(C'/\f(CW\*(C`Set_ptrcnt()\*(C' methods).
.Sp
\*(L"perlio\*(R" over \*(L"unix\*(R" provides a complete replacement for stdio as seen
via PerlIO \s-1API.\s0 This is the default for \s-1USE_PERLIO\s0 when system's stdio
does not permit perl's \*(L"fast gets\*(R" access, and which do not
distinguish between \f(CW\*(C`O_TEXT\*(C' and \f(CW\*(C`O_BINARY\*(C'.
.ie n .IP """stdio""" 4
.el .IP "``stdio''" 4
Item "stdio"
A layer which provides the PerlIO \s-1API\s0 via the layer scheme, but
implements it by calling system's stdio. This is (currently) the default
if system's stdio provides sufficient access to allow perl's \*(L"fast gets\*(R"
access and which do not distinguish between \f(CW\*(C`O_TEXT\*(C' and \f(CW\*(C`O_BINARY\*(C'.
.ie n .IP """crlf""" 4
.el .IP "``crlf''" 4
Item "crlf"
A layer derived using \*(L"perlio\*(R" as a base class. It provides Win32-like
\*(L"\\n\*(R" to \s-1CR,LF\s0 translation. Can either be applied above \*(L"perlio\*(R" or serve
as the buffer layer itself. \*(L"crlf\*(R" over \*(L"unix\*(R" is the default if system
distinguishes between \f(CW\*(C`O_TEXT\*(C' and \f(CW\*(C`O_BINARY\*(C' opens. (At some point
\*(L"unix\*(R" will be replaced by a \*(L"native\*(R" Win32 \s-1IO\s0 layer on that platform,
as Win32's read/write layer has various drawbacks.) The \*(L"crlf\*(R" layer is
a reasonable model for a layer which transforms data in some way.
.ie n .IP """mmap""" 4
.el .IP "``mmap''" 4
Item "mmap"
If Configure detects \f(CW\*(C`mmap()\*(C' functions this layer is provided (with
\*(L"perlio\*(R" as a \*(L"base\*(R") which does \*(L"read\*(R" operations by **mmap()**ing the
file. Performance improvement is marginal on modern systems, so it is
mainly there as a proof of concept. It is likely to be unbundled from
the core at some point. The \*(L"mmap\*(R" layer is a reasonable model for a
minimalist \*(L"derived\*(R" layer.
.ie n .IP """pending""" 4
.el .IP "``pending''" 4
Item "pending"
An \*(L"internal\*(R" derivative of \*(L"perlio\*(R" which can be used to provide
**Unread()** function for layers which have no buffer or cannot be
bothered.  (Basically this layer's \f(CW\*(C`Fill()\*(C' pops itself off the stack
and so resumes reading from layer below.)
.ie n .IP """raw""" 4
.el .IP "``raw''" 4
Item "raw"
A dummy layer which never exists on the layer stack. Instead when
\*(L"pushed\*(R" it actually pops the stack removing itself, it then calls
Binmode function table entry on all the layers in the stack - normally
this (via PerlIOBase_binmode) removes any layers which do not have
\f(CW\*(C`PERLIO_K_RAW\*(C' bit set. Layers can modify that behaviour by defining
their own Binmode entry.
.ie n .IP """utf8""" 4
.el .IP "``utf8''" 4
Item "utf8"
Another dummy layer. When pushed it pops itself and sets the
\f(CW\*(C`PERLIO_F_UTF8\*(C' flag on the layer which was (and now is once more)
the top of the stack.

In addition *perlio.c* also provides a number of \f(CW\*(C`PerlIOBase_xxxx()\*(C'
functions which are intended to be used in the table slots of classes
which do not need to do anything special for a particular method.

### Extension Layers

Subsection "Extension Layers"
Layers can be made available by extension modules. When an unknown layer
is encountered the PerlIO code will perform the equivalent of :

.Vb 1
   use PerlIO \*(Aqlayer\*(Aq;
.Ve

Where *layer* is the unknown layer. *PerlIO.pm* will then attempt to:

.Vb 1
   require PerlIO::layer;
.Ve

If after that process the layer is still not defined then the \f(CW\*(C`open\*(C'
will fail.

The following extension layers are bundled with perl:
.ie n .IP """:encoding""" 4
.el .IP "``:encoding''" 4
Item ":encoding"
.Vb 1
   use Encoding;
.Ve
.Sp
makes this layer available, although *PerlIO.pm* \*(L"knows\*(R" where to
find it.  It is an example of a layer which takes an argument as it is
called thus:
.Sp
.Vb 1
   open( $fh, "<:encoding(iso-8859-7)", $pathname );
.Ve
.ie n .IP """:scalar""" 4
.el .IP "``:scalar''" 4
Item ":scalar"
Provides support for reading data from and writing data to a scalar.
.Sp
.Vb 1
   open( $fh, "+<:scalar", \\$scalar );
.Ve
.Sp
When a handle is so opened, then reads get bytes from the string value
of *\f(CI$scalar\fI*, and writes change the value. In both cases the position
in *\f(CI$scalar** starts as zero but can be altered via \f(CW\*(C`seek\*(C'*, and
determined via \f(CW\*(C`tell\*(C'.
.Sp
Please note that this layer is implied when calling **open()** thus:
.Sp
.Vb 1
   open( $fh, "+<", \\$scalar );
.Ve
.ie n .IP """:via""" 4
.el .IP "``:via''" 4
Item ":via"
Provided to allow layers to be implemented as Perl code.  For instance:
.Sp
.Vb 2
   use PerlIO::via::StripHTML;
   open( my $fh, "<:via(StripHTML)", "index.html" );
.Ve
.Sp
See PerlIO::via for details.

## TODO

Header "TODO"
Things that need to be done to improve this document.

- \(bu
Explain how to make a valid fh without going through **open()**(i.e. apply
a layer). For example if the file is not opened through perl, but we
want to get back a fh, like it was opened by Perl.
.Sp
How PerlIO_apply_layera fits in, where its docs, was it made public?
.Sp
Currently the example could be something like this:
.Sp
.Vb 8
  PerlIO *foo_to_PerlIO(pTHX_ char *mode, ...)
  \{
      char *mode; /* "w", "r", etc */
      const char *layers = ":APR"; /* the layer name */
      PerlIO *f = PerlIO_allocate(aTHX);
      if (!f) \{
          return NULL;
      \}

      PerlIO_apply_layers(aTHX_ f, mode, layers);

      if (f) \{
          PerlIOAPR *st = PerlIOSelf(f, PerlIOAPR);
          /* fill in the st struct, as in _open() */
          st->file = file;
          PerlIOBase(f)->flags |= PERLIO_F_OPEN;

          return f;
      \}
      return NULL;
  \}
.Ve

- \(bu
fix/add the documentation in places marked as \s-1XXX.\s0

- \(bu
The handling of errors by the layer is not specified. e.g. when $!
should be set explicitly, when the error handling should be just
delegated to the top layer.
.Sp
Probably give some hints on using \s-1**SETERRNO\s0()** or pointers to where they
can be found.

- \(bu
I think it would help to give some concrete examples to make it easier
to understand the \s-1API.\s0 Of course I agree that the \s-1API\s0 has to be
concise, but since there is no second document that is more of a
guide, I think that it'd make it easier to start with the doc which is
an \s-1API,\s0 but has examples in it in places where things are unclear, to
a person who is not a PerlIO guru (yet).
