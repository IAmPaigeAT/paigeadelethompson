+++
operating_system = "macos"
operating_system_version = "15.3"
manpage_name = "perl584delta"
description = "This document describes differences between the 5.8.3 release and the 5.8.4 release. Many minor bugs have been fixed. Scripts which happen to rely on previously erroneous behaviour will consider these fixes as incompatible changes :-) You are advi..."
manpage_format = "troff"
title = "perl584delta(1)"
date = "2022-02-19"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "how", "to", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_section = "1"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL584DELTA 1"
PERL584DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl584delta - what is new for perl v5.8.4

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.8.3 release and
the 5.8.4 release.

## Incompatible Changes

Header "Incompatible Changes"
Many minor bugs have been fixed. Scripts which happen to rely on previously
erroneous behaviour will consider these fixes as incompatible changes :-)
You are advised to perform sufficient acceptance testing on this release
to satisfy yourself that this does not affect you, before putting this
release into production.

The diagnostic output of Carp has been changed slightly, to add a space after
the comma between arguments. This makes it much easier for tools such as
web browsers to wrap it, but might confuse any automatic tools which perform
detailed parsing of Carp output.

The internal dump output has been improved, so that non-printable characters
such as newline and backspace are output in \f(CW\*(C`\\x\*(C' notation, rather than
octal. This might just confuse non-robust tools which parse the output of
modules such as Devel::Peek.

## Core Enhancements

Header "Core Enhancements"

### Malloc wrapping

Subsection "Malloc wrapping"
Perl can now be built to detect attempts to assign pathologically large chunks
of memory.  Previously such assignments would suffer from integer wrap-around
during size calculations causing a misallocation, which would crash perl, and
could theoretically be used for \*(L"stack smashing\*(R" attacks.  The wrapping
defaults to enabled on platforms where we know it works (most \s-1AIX\s0
configurations, BSDi, Darwin, \s-1DEC OSF/1,\s0 FreeBSD, \s-1HP/UX, GNU\s0 Linux, OpenBSD,
Solaris, \s-1VMS\s0 and most Win32 compilers) and defaults to disabled on other
platforms.

### Unicode Character Database 4.0.1

Subsection "Unicode Character Database 4.0.1"
The copy of the Unicode Character Database included in Perl 5.8 has
been updated to 4.0.1 from 4.0.0.

### suidperl less insecure

Subsection "suidperl less insecure"
Paul Szabo has analysed and patched \f(CW\*(C`suidperl\*(C' to remove existing known
insecurities. Currently there are no known holes in \f(CW\*(C`suidperl\*(C', but previous
experience shows that we cannot be confident that these were the last. You may
no longer invoke the set uid perl directly, so to preserve backwards
compatibility with scripts that invoke #!/usr/bin/suidperl the only set uid
binary is now \f(CW\*(C`sperl5.8.\*(C'*n* (\f(CW\*(C`sperl5.8.4\*(C' for this release). \f(CW\*(C`suidperl\*(C'
is installed as a hard link to \f(CW\*(C`perl\*(C'; both \f(CW\*(C`suidperl\*(C' and \f(CW\*(C`perl\*(C' will
invoke \f(CW\*(C`sperl5.8.4\*(C' automatically the set uid binary, so this change should
be completely transparent.

For new projects the core perl team would strongly recommend that you use
dedicated, single purpose security tools such as \f(CW\*(C`sudo\*(C' in preference to
\f(CW\*(C`suidperl\*(C'.

### format

Subsection "format"
In addition to bug fixes, \f(CW\*(C`format\*(C''s features have been enhanced. See
perlform

## Modules and Pragmata

Header "Modules and Pragmata"
The (mis)use of \f(CW\*(C`/tmp\*(C' in core modules and documentation has been tidied up.
Some modules available both within the perl core and independently from \s-1CPAN\s0
(\*(L"dual-life modules\*(R") have not yet had these changes applied; the changes
will be integrated into future stable perl releases as the modules are
updated on \s-1CPAN.\s0

### Updated modules

Subsection "Updated modules"

- Attribute::Handlers
Item "Attribute::Handlers"
0

- B
Item "B"

- Benchmark
Item "Benchmark"

- \s-1CGI\s0
Item "CGI"

- Carp
Item "Carp"

- Cwd
Item "Cwd"

- Exporter
Item "Exporter"

- File::Find
Item "File::Find"

- \s-1IO\s0
Item "IO"

- IPC::Open3
Item "IPC::Open3"

- Local::Maketext
Item "Local::Maketext"

- Math::BigFloat
Item "Math::BigFloat"

- Math::BigInt
Item "Math::BigInt"

- Math::BigRat
Item "Math::BigRat"

- MIME::Base64
Item "MIME::Base64"

- ODBM_File
Item "ODBM_File"

- \s-1POSIX\s0
Item "POSIX"

- Shell
Item "Shell"

- Socket
Item "Socket"
.PD
There is experimental support for Linux abstract Unix domain sockets.

- Storable
Item "Storable"
0

- Switch
Item "Switch"
.PD
Synced with its \s-1CPAN\s0 version 2.10

- Sys::Syslog
Item "Sys::Syslog"
\f(CW\*(C`syslog()\*(C' can now use numeric constants for facility names and priorities,
in addition to strings.

- Term::ANSIColor
Item "Term::ANSIColor"
0

- Time::HiRes
Item "Time::HiRes"

- Unicode::UCD
Item "Unicode::UCD"

- Win32
Item "Win32"
.PD
Win32.pm/Win32.xs has moved from the libwin32 module to core Perl

- base
Item "base"
0

- open
Item "open"

- threads
Item "threads"
.PD
Detached threads are now also supported on Windows.

- utf8
Item "utf8"

## Performance Enhancements

Header "Performance Enhancements"
0

- \(bu
.PD
Accelerated Unicode case mappings (\f(CW\*(C`/i\*(C', \f(CW\*(C`lc\*(C', \f(CW\*(C`uc\*(C', etc).

- \(bu
In place sort optimised (eg \f(CW\*(C`@a = sort @a\*(C')

- \(bu
Unnecessary assignment optimised away in
.Sp
.Vb 3
  my $s = undef;
  my @a = ();
  my %h = ();
.Ve

- \(bu
Optimised \f(CW\*(C`map\*(C' in scalar context

## Utility Changes

Header "Utility Changes"
The Perl debugger (*lib/perl5db.pl*) can now save all debugger commands for
sourcing later, and can display the parent inheritance tree of a given class.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"
The build process on both \s-1VMS\s0 and Windows has had several minor improvements
made. On Windows Borland's C compiler can now compile perl with PerlIO and/or
\s-1USE_LARGE_FILES\s0 enabled.

\f(CW\*(C`perl.exe\*(C' on Windows now has a \*(L"Camel\*(R" logo icon. The use of a camel with
the topic of Perl is a trademark of O'Reilly and Associates Inc., and is used
with their permission (ie distribution of the source, compiling a Windows
executable from it, and using that executable locally). Use of the supplied
camel for anything other than a perl executable's icon is specifically not
covered, and anyone wishing to redistribute perl binaries *with* the icon
should check directly with O'Reilly beforehand.

Perl should build cleanly on Stratus \s-1VOS\s0 once more.

## Selected Bug Fixes

Header "Selected Bug Fixes"
More utf8 bugs fixed, notably in how \f(CW\*(C`chomp\*(C', \f(CW\*(C`chop\*(C', \f(CW\*(C`send\*(C', and
\f(CW\*(C`syswrite\*(C' and interact with utf8 data. Concatenation now works correctly
when \f(CW\*(C`use bytes;\*(C' is in scope.

Pragmata are now correctly propagated into (?\{...\}) constructions in regexps.
Code such as

.Vb 1
   my $x = qr\{ ... (??\{ $x \}) ... \};
.Ve

will now (correctly) fail under use strict. (As the inner \f(CW$x is and
has always referred to \f(CW$::x)

The \*(L"const in void context\*(R" warning has been suppressed for a constant in an
optimised-away boolean expression such as \f(CW\*(C`5 || print;\*(C'

\f(CW\*(C`perl -i\*(C' could \f(CW\*(C`fchmod(stdin)\*(C' by mistake. This is serious if stdin is
attached to a terminal, and perl is running as root. Now fixed.

## New or Changed Diagnostics

Header "New or Changed Diagnostics"
\f(CW\*(C`Carp\*(C' and the internal diagnostic routines used by \f(CW\*(C`Devel::Peek\*(C' have been
made clearer, as described in \*(L"Incompatible Changes\*(R"

## Changed Internals

Header "Changed Internals"
Some bugs have been fixed in the hash internals. Restricted hashes and
their place holders are now allocated and deleted at slightly different times,
but this should not be visible to user code.

## Future Directions

Header "Future Directions"
Code freeze for the next maintenance release (5.8.5) will be on 30th June
2004, with release by mid July.

## Platform Specific Problems

Header "Platform Specific Problems"
This release is known not to build on Windows 95.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://bugs.perl.org.  There may also be
information at http://www.perl.org, the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.  You can browse and search
the Perl 5 bugs at http://bugs.perl.org/

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for exhaustive details on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
