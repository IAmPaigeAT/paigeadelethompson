+++
manpage_name = "perltrap"
description = "The biggest trap of all is forgetting to f(CW*(C`use warnings*(C or use the -w switch; see warnings and *(L-w*(R in perlrun. The second biggest trap is not making your entire program runnable under f(CW*(C`use strict*(C.  The third biggest trap..."
manpage_format = "troff"
title = "perltrap(1)"
date = "2022-02-19"
manpage_section = "1"
operating_system = "macos"
operating_system_version = "15.3"
author = "None Specified"
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLTRAP 1"
PERLTRAP 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perltrap - Perl traps for the unwary

## DESCRIPTION

Header "DESCRIPTION"
The biggest trap of all is forgetting to \f(CW\*(C`use warnings\*(C' or use the **-w**
switch; see warnings and \*(L"-w\*(R" in perlrun. The second biggest trap is not
making your entire program runnable under \f(CW\*(C`use strict\*(C'.  The third biggest
trap is not reading the list of changes in this version of Perl; see
perldelta.

### Awk Traps

Subsection "Awk Traps"
Accustomed **awk** users should take special note of the following:

- \(bu
A Perl program executes only once, not once for each input line.  You can
do an implicit loop with \f(CW\*(C`-n\*(C' or \f(CW\*(C`-p\*(C'.

- \(bu
The English module, loaded via
.Sp
.Vb 1
    use English;
.Ve
.Sp
allows you to refer to special variables (like \f(CW$/) with names (like
\f(CW$RS), as though they were in **awk**; see perlvar for details.

- \(bu
Semicolons are required after all simple statements in Perl (except
at the end of a block).  Newline is not a statement delimiter.

- \(bu
Curly brackets are required on \f(CW\*(C`if\*(C's and \f(CW\*(C`while\*(C's.

- \(bu
Variables begin with \*(L"$\*(R", \*(L"@\*(R" or \*(L"%\*(R" in Perl.

- \(bu
Arrays index from 0.  Likewise string positions in **substr()** and
**index()**.

- \(bu
You have to decide whether your array has numeric or string indices.

- \(bu
Hash values do not spring into existence upon mere reference.

- \(bu
You have to decide whether you want to use string or numeric
comparisons.

- \(bu
Reading an input line does not split it for you.  You get to split it
to an array yourself.  And the **split()** operator has different
arguments than **awk**'s.

- \(bu
The current input line is normally in \f(CW$_, not \f(CW$0.  It generally does
not have the newline stripped.  ($0 is the name of the program
executed.)  See perlvar.

- \(bu
$<*digit*> does not refer to fields\*(--it refers to substrings matched
by the last match pattern.

- \(bu
The **print()** statement does not add field and record separators unless
you set \f(CW$, and \f(CW\*(C`$\\\*(C'.  You can set \f(CW$OFS and \f(CW$ORS if you're using
the English module.

- \(bu
You must open your files before you print to them.

- \(bu
The range operator is \*(L"..\*(R", not comma.  The comma operator works as in
C.

- \(bu
The match operator is \*(L"=~\*(R", not \*(L"~\*(R".  (\*(L"~\*(R" is the one's complement
operator, as in C.)

- \(bu
The exponentiation operator is \*(L"**\*(R", not \*(L"^\*(R".  \*(L"^\*(R" is the \s-1XOR\s0
operator, as in C.  (You know, one could get the feeling that **awk** is
basically incompatible with C.)

- \(bu
The concatenation operator is \*(L".\*(R", not the null string.  (Using the
null string would render \f(CW\*(C`/pat/ /pat/\*(C' unparsable, because the third slash
would be interpreted as a division operator\*(--the tokenizer is in fact
slightly context sensitive for operators like \*(L"/\*(R", \*(L"?\*(R", and \*(L">\*(R".
And in fact, \*(L".\*(R" itself can be the beginning of a number.)

- \(bu
The \f(CW\*(C`next\*(C', \f(CW\*(C`exit\*(C', and \f(CW\*(C`continue\*(C' keywords work differently.

- \(bu
The following variables work differently:
.Sp
.Vb 10
      Awk       Perl
      ARGC      scalar @ARGV (compare with $#ARGV)
      ARGV[0]   $0
      FILENAME  $ARGV
      FNR       $. - something
      FS        (whatever you like)
      NF        $#Fld, or some such
      NR        $.
      OFMT      $#
      OFS       $,
      ORS       $\\
      RLENGTH   length($&)
      RS        $/
      RSTART    length($\`)
      SUBSEP    $;
.Ve

- \(bu
You cannot set \f(CW$RS to a pattern, only a string.

- \(bu
When in doubt, run the **awk** construct through **a2p** and see what it
gives you.

### C/\*(C+ Traps

Subsection "C/ Traps"
Cerebral C and \*(C+ programmers should take note of the following:

- \(bu
Curly brackets are required on \f(CW\*(C`if\*(C''s and \f(CW\*(C`while\*(C''s.

- \(bu
You must use \f(CW\*(C`elsif\*(C' rather than \f(CW\*(C`else if\*(C'.

- \(bu
The \f(CW\*(C`break\*(C' and \f(CW\*(C`continue\*(C' keywords from C become in Perl \f(CW\*(C`last\*(C'
and \f(CW\*(C`next\*(C', respectively.  Unlike in C, these do *not* work within a
\f(CW\*(C`do \{ \} while\*(C' construct.  See \*(L"Loop Control\*(R" in perlsyn.

- \(bu
The switch statement is called \f(CW\*(C`given\*(C'/\f(CW\*(C`when\*(C' and only available in
perl 5.10 or newer.  See \*(L"Switch Statements\*(R" in perlsyn.

- \(bu
Variables begin with \*(L"$\*(R", \*(L"@\*(R" or \*(L"%\*(R" in Perl.

- \(bu
Comments begin with \*(L"#\*(R", not \*(L"/*\*(R" or \*(L"//\*(R".  Perl may interpret C/\*(C+
comments as division operators, unterminated regular expressions or
the defined-or operator.

- \(bu
You can't take the address of anything, although a similar operator
in Perl is the backslash, which creates a reference.

- \(bu
\f(CW\*(C`ARGV\*(C' must be capitalized.  \f(CW$ARGV[0] is C's \f(CW\*(C`argv[1]\*(C', and \f(CW\*(C`argv[0]\*(C'
ends up in \f(CW$0.

- \(bu
System calls such as **link()**, **unlink()**, **rename()**, etc. return nonzero for
success, not 0. (**system()**, however, returns zero for success.)

- \(bu
Signal handlers deal with signal names, not numbers.  Use \f(CW\*(C`kill -l\*(C'
to find their names on your system.

### JavaScript Traps

Subsection "JavaScript Traps"
Judicious JavaScript programmers should take note of the following:

- \(bu
In Perl, binary \f(CW\*(C`+\*(C' is always addition.  \f(CW\*(C`$string1 + $string2\*(C' converts
both strings to numbers and then adds them.  To concatenate two strings,
use the \f(CW\*(C`.\*(C' operator.

- \(bu
The \f(CW\*(C`+\*(C' unary operator doesn't do anything in Perl.  It exists to avoid
syntactic ambiguities.

- \(bu
Unlike \f(CW\*(C`for...in\*(C', Perl's \f(CW\*(C`for\*(C' (also spelled \f(CW\*(C`foreach\*(C') does not allow
the left-hand side to be an arbitrary expression.  It must be a variable:
.Sp
.Vb 3
   for my $variable (keys %hash) \{
        ...
   \}
.Ve
.Sp
Furthermore, don't forget the \f(CW\*(C`keys\*(C' in there, as
\f(CW\*(C`foreach my $kv (%hash) \{\}\*(C' iterates over the keys and values, and is
generally not useful ($kv would be a key, then a value, and so on).

- \(bu
To iterate over the indices of an array, use \f(CW\*(C`foreach my $i (0 .. $#array)
\{\}\*(C'.  \f(CW\*(C`foreach my $v (@array) \{\}\*(C' iterates over the values.

- \(bu
Perl requires braces following \f(CW\*(C`if\*(C', \f(CW\*(C`while\*(C', \f(CW\*(C`foreach\*(C', etc.

- \(bu
In Perl, \f(CW\*(C`else if\*(C' is spelled \f(CW\*(C`elsif\*(C'.

- \(bu
\f(CW\*(C`? :\*(C' has higher precedence than assignment.  In JavaScript, one can
write:
.Sp
.Vb 1
    condition ? do_something() : variable = 3
.Ve
.Sp
and the variable is only assigned if the condition is false.  In Perl, you
need parentheses:
.Sp
.Vb 1
    $condition ? do_something() : ($variable = 3);
.Ve
.Sp
Or just use \f(CW\*(C`if\*(C'.

- \(bu
Perl requires semicolons to separate statements.

- \(bu
Variables declared with \f(CW\*(C`my\*(C' only affect code *after* the declaration.
You cannot write \f(CW\*(C`$x = 1; my $x;\*(C' and expect the first assignment to
affect the same variable.  It will instead assign to an \f(CW$x declared
previously in an outer scope, or to a global variable.
.Sp
Note also that the variable is not visible until the following
*statement*.  This means that in \f(CW\*(C`my $x = 1 + $x\*(C' the second \f(CW$x refers
to one declared previously.

- \(bu
\f(CW\*(C`my\*(C' variables are scoped to the current block, not to the current
function.  If you write \f(CW\*(C`\{my $x;\} $x;\*(C', the second \f(CW$x does not refer to
the one declared inside the block.

- \(bu
An object's members cannot be made accessible as variables.  The closest
Perl equivalent to \f(CW\*(C`with(object) \{ method() \}\*(C' is \f(CW\*(C`for\*(C', which can alias
\f(CW$_ to the object:
.Sp
.Vb 3
    for ($object) \{
        $_->method;
    \}
.Ve

- \(bu
The object or class on which a method is called is passed as one of the
method's arguments, not as a separate \f(CW\*(C`this\*(C' value.

### Sed Traps

Subsection "Sed Traps"
Seasoned **sed** programmers should take note of the following:

- \(bu
A Perl program executes only once, not once for each input line.  You can
do an implicit loop with \f(CW\*(C`-n\*(C' or \f(CW\*(C`-p\*(C'.

- \(bu
Backreferences in substitutions use \*(L"$\*(R" rather than \*(L"\\\*(R".

- \(bu
The pattern matching metacharacters \*(L"(\*(R", \*(L")\*(R", and \*(L"|\*(R" do not have backslashes
in front.

- \(bu
The range operator is \f(CW\*(C`...\*(C', rather than comma.

### Shell Traps

Subsection "Shell Traps"
Sharp shell programmers should take note of the following:

- \(bu
The backtick operator does variable interpolation without regard to
the presence of single quotes in the command.

- \(bu
The backtick operator does no translation of the return value, unlike **csh**.

- \(bu
Shells (especially **csh**) do several levels of substitution on each
command line.  Perl does substitution in only certain constructs
such as double quotes, backticks, angle brackets, and search patterns.

- \(bu
Shells interpret scripts a little bit at a time.  Perl compiles the
entire program before executing it (except for \f(CW\*(C`BEGIN\*(C' blocks, which
execute at compile time).

- \(bu
The arguments are available via \f(CW@ARGV, not \f(CW$1, \f(CW$2, etc.

- \(bu
The environment is not automatically made available as separate scalar
variables.

- \(bu
The shell's \f(CW\*(C`test\*(C' uses \*(L"=\*(R", \*(L"!=\*(R", \*(L"<\*(R" etc for string comparisons and \*(L"-eq\*(R",
\*(L"-ne\*(R", \*(L"-lt\*(R" etc for numeric comparisons. This is the reverse of Perl, which
uses \f(CW\*(C`eq\*(C', \f(CW\*(C`ne\*(C', \f(CW\*(C`lt\*(C' for string comparisons, and \f(CW\*(C`==\*(C', \f(CW\*(C`!=\*(C' \f(CW\*(C`<\*(C' etc
for numeric comparisons.

### Perl Traps

Subsection "Perl Traps"
Practicing Perl Programmers should take note of the following:

- \(bu
Remember that many operations behave differently in a list
context than they do in a scalar one.  See perldata for details.

- \(bu
Avoid barewords if you can, especially all lowercase ones.
You can't tell by just looking at it whether a bareword is
a function or a string.  By using quotes on strings and
parentheses on function calls, you won't ever get them confused.

- \(bu
You cannot discern from mere inspection which builtins
are unary operators (like **chop()** and **chdir()**)
and which are list operators (like **print()** and **unlink()**).
(Unless prototyped, user-defined subroutines can **only** be list
operators, never unary ones.)  See perlop and perlsub.

- \(bu
People have a hard time remembering that some functions
default to \f(CW$_, or \f(CW@ARGV, or whatever, but that others which
you might expect to do not.

- \(bu
The <\s-1FH\s0> construct is not the name of the filehandle, it is a readline
operation on that handle.  The data read is assigned to \f(CW$_ only if the
file read is the sole condition in a while loop:
.Sp
.Vb 3
    while (<FH>)      \{ \}
    while (defined($_ = <FH>)) \{ \}..
    <FH>;  # data discarded!
.Ve

- \(bu
Remember not to use \f(CW\*(C`=\*(C' when you need \f(CW\*(C`=~\*(C';
these two constructs are quite different:
.Sp
.Vb 2
    $x =  /foo/;
    $x =~ /foo/;
.Ve

- \(bu
The \f(CW\*(C`do \{\}\*(C' construct isn't a real loop that you can use
loop control on.

- \(bu
Use \f(CW\*(C`my()\*(C' for local variables whenever you can get away with
it (but see perlform for where you can't).
Using \f(CW\*(C`local()\*(C' actually gives a local value to a global
variable, which leaves you open to unforeseen side-effects
of dynamic scoping.

- \(bu
If you localize an exported variable in a module, its exported value will
not change.  The local name becomes an alias to a new value but the
external name is still an alias for the original.

As always, if any of these are ever officially declared as bugs,
they'll be fixed and removed.
