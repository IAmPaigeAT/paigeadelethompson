+++
manpage_section = "1"
operating_system_version = "15.3"
title = "perlobj(1)"
author = "None Specified"
manpage_format = "troff"
description = "This document provides a reference for Perls object orientation features. If youre looking for an introduction to object-oriented programming in Perl, please see perlootut. In order to understand Perl objects, you first need to understand refere..."
detected_package_version = "5.34.1"
operating_system = "macos"
keywords = ["header", "see", "also", "a", "kinder", "gentler", "tutorial", "on", "object-oriented", "programming", "in", "perl", "can", "be", "found", "perlootut", "you", "should", "check", "out", "perlmodlib", "for", "some", "style", "guides", "constructing", "both", "modules", "and", "classes"]
date = "2022-02-19"
manpage_name = "perlobj"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLOBJ 1"
PERLOBJ 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlobj - Perl object reference
Xref "object OOP"

## DESCRIPTION

Header "DESCRIPTION"
This document provides a reference for Perl's object orientation
features. If you're looking for an introduction to object-oriented
programming in Perl, please see perlootut.

In order to understand Perl objects, you first need to understand
references in Perl. See perlreftut for details.

This document describes all of Perl's object-oriented (\s-1OO\s0) features
from the ground up. If you're just looking to write some
object-oriented code of your own, you are probably better served by
using one of the object systems from \s-1CPAN\s0 described in perlootut.

If you're looking to write your own object system, or you need to
maintain code which implements objects from scratch then this document
will help you understand exactly how Perl does object orientation.

There are a few basic principles which define object oriented Perl:

- 1.
An object is simply a data structure that knows to which class it
belongs.

- 2.
A class is simply a package. A class provides methods that expect to
operate on objects.

- 3.
A method is simply a subroutine that expects a reference to an object
(or a package name, for class methods) as the first argument.

Let's look at each of these principles in depth.

### An Object is Simply a Data Structure

Xref "object bless constructor new"
Subsection "An Object is Simply a Data Structure"
Unlike many other languages which support object orientation, Perl does
not provide any special syntax for constructing an object. Objects are
merely Perl data structures (hashes, arrays, scalars, filehandles,
etc.) that have been explicitly associated with a particular class.

That explicit association is created by the built-in \f(CW\*(C`bless\*(C' function,
which is typically used within the *constructor* subroutine of the
class.

Here is a simple constructor:

.Vb 1
  package File;

  sub new \{
      my $class = shift;

      return bless \{\}, $class;
  \}
.Ve

The name \f(CW\*(C`new\*(C' isn't special. We could name our constructor something
else:

.Vb 1
  package File;

  sub load \{
      my $class = shift;

      return bless \{\}, $class;
  \}
.Ve

The modern convention for \s-1OO\s0 modules is to always use \f(CW\*(C`new\*(C' as the
name for the constructor, but there is no requirement to do so. Any
subroutine that blesses a data structure into a class is a valid
constructor in Perl.

In the previous examples, the \f(CW\*(C`\{\}\*(C' code creates a reference to an
empty anonymous hash. The \f(CW\*(C`bless\*(C' function then takes that reference
and associates the hash with the class in \f(CW$class. In the simplest
case, the \f(CW$class variable will end up containing the string \*(L"File\*(R".

We can also use a variable to store a reference to the data structure
that is being blessed as our object:

.Vb 2
  sub new \{
      my $class = shift;

      my $self = \{\};
      bless $self, $class;

      return $self;
  \}
.Ve

Once we've blessed the hash referred to by \f(CW$self we can start
calling methods on it. This is useful if you want to put object
initialization in its own separate method:

.Vb 2
  sub new \{
      my $class = shift;

      my $self = \{\};
      bless $self, $class;

      $self->_initialize();

      return $self;
  \}
.Ve

Since the object is also a hash, you can treat it as one, using it to
store data associated with the object. Typically, code inside the class
can treat the hash as an accessible data structure, while code outside
the class should always treat the object as opaque. This is called
**encapsulation**. Encapsulation means that the user of an object does
not have to know how it is implemented. The user simply calls
documented methods on the object.

Note, however, that (unlike most other \s-1OO\s0 languages) Perl does not
ensure or enforce encapsulation in any way. If you want objects to
actually *be* opaque you need to arrange for that yourself. This can
be done in a variety of ways, including using \*(L"Inside-Out objects\*(R"
or modules from \s-1CPAN.\s0

*Objects Are Blessed; Variables Are Not*
Subsection "Objects Are Blessed; Variables Are Not"

When we bless something, we are not blessing the variable which
contains a reference to that thing, nor are we blessing the reference
that the variable stores; we are blessing the thing that the variable
refers to (sometimes known as the *referent*). This is best
demonstrated with this code:

.Vb 1
  use Scalar::Util \*(Aqblessed\*(Aq;

  my $foo = \{\};
  my $bar = $foo;

  bless $foo, \*(AqClass\*(Aq;
  print blessed( $bar ) // \*(Aqnot blessed\*(Aq;    # prints "Class"

  $bar = "some other value";
  print blessed( $bar ) // \*(Aqnot blessed\*(Aq;    # prints "not blessed"
.Ve

When we call \f(CW\*(C`bless\*(C' on a variable, we are actually blessing the
underlying data structure that the variable refers to. We are not
blessing the reference itself, nor the variable that contains that
reference. That's why the second call to \f(CW\*(C`blessed( $bar )\*(C' returns
false. At that point \f(CW$bar is no longer storing a reference to an
object.

You will sometimes see older books or documentation mention \*(L"blessing a
reference\*(R" or describe an object as a \*(L"blessed reference\*(R", but this is
incorrect. It isn't the reference that is blessed as an object; it's
the thing the reference refers to (i.e. the referent).

### A Class is Simply a Package

Xref "class package @ISA inheritance"
Subsection "A Class is Simply a Package"
Perl does not provide any special syntax for class definitions. A
package is simply a namespace containing variables and subroutines. The
only difference is that in a class, the subroutines may expect a
reference to an object or the name of a class as the first argument.
This is purely a matter of convention, so a class may contain both
methods and subroutines which *don't* operate on an object or class.

Each package contains a special array called \f(CW@ISA. The \f(CW@ISA array
contains a list of that class's parent classes, if any. This array is
examined when Perl does method resolution, which we will cover later.

Calling methods from a package means it must be loaded, of course, so
you will often want to load a module and add it to \f(CW@ISA at the same
time. You can do so in a single step using the parent pragma.
(In older code you may encounter the base pragma, which is nowadays
discouraged except when you have to work with the equally discouraged
fields pragma.)

However the parent classes are set, the package's \f(CW@ISA variable will
contain a list of those parents. This is simply a list of scalars, each
of which is a string that corresponds to a package name.

All classes inherit from the \s-1UNIVERSAL\s0 class implicitly. The
\s-1UNIVERSAL\s0 class is implemented by the Perl core, and provides
several default methods, such as \f(CW\*(C`isa()\*(C', \f(CW\*(C`can()\*(C', and \f(CW\*(C`VERSION()\*(C'.
The \f(CW\*(C`UNIVERSAL\*(C' class will *never* appear in a package's \f(CW@ISA
variable.

Perl *only* provides method inheritance as a built-in feature.
Attribute inheritance is left up the class to implement. See the
\*(L"Writing Accessors\*(R" section for details.

### A Method is Simply a Subroutine

Xref "method"
Subsection "A Method is Simply a Subroutine"
Perl does not provide any special syntax for defining a method. A
method is simply a regular subroutine, and is declared with \f(CW\*(C`sub\*(C'.
What makes a method special is that it expects to receive either an
object or a class name as its first argument.

Perl *does* provide special syntax for method invocation, the \f(CW\*(C`->\*(C' operator. We will cover this in more detail later.

Most methods you write will expect to operate on objects:

.Vb 2
  sub save \{
      my $self = shift;

      open my $fh, \*(Aq>\*(Aq, $self->path() or die $!;
      print \{$fh\} $self->data()       or die $!;
      close $fh                       or die $!;
  \}
.Ve

### Method Invocation

Xref "invocation method arrow ->"
Subsection "Method Invocation"
Calling a method on an object is written as \f(CW\*(C`$object->method\*(C'.

The left hand side of the method invocation (or arrow) operator is the
object (or class name), and the right hand side is the method name.

.Vb 2
  my $pod = File->new( \*(Aqperlobj.pod\*(Aq, $data );
  $pod->save();
.Ve

The \f(CW\*(C`->\*(C' syntax is also used when dereferencing a reference. It
looks like the same operator, but these are two different operations.

When you call a method, the thing on the left side of the arrow is
passed as the first argument to the method. That means when we call \f(CW\*(C`Critter->new()\*(C', the \f(CW\*(C`new()\*(C' method receives the string \f(CW"Critter"
as its first argument. When we call \f(CW\*(C`$fred->speak()\*(C', the \f(CW$fred
variable is passed as the first argument to \f(CW\*(C`speak()\*(C'.

Just as with any Perl subroutine, all of the arguments passed in \f(CW@_
are aliases to the original argument. This includes the object itself.
If you assign directly to \f(CW$_[0] you will change the contents of the
variable that holds the reference to the object. We recommend that you
don't do this unless you know exactly what you're doing.

Perl knows what package the method is in by looking at the left side of
the arrow. If the left hand side is a package name, it looks for the
method in that package. If the left hand side is an object, then Perl
looks for the method in the package that the object has been blessed
into.

If the left hand side is neither a package name nor an object, then the
method call will cause an error, but see the section on \*(L"Method Call
Variations\*(R" for more nuances.

### Inheritance

Xref "inheritance"
Subsection "Inheritance"
We already talked about the special \f(CW@ISA array and the parent
pragma.

When a class inherits from another class, any methods defined in the
parent class are available to the child class. If you attempt to call a
method on an object that isn't defined in its own class, Perl will also
look for that method in any parent classes it may have.

.Vb 2
  package File::MP3;
  use parent \*(AqFile\*(Aq;    # sets @File::MP3::ISA = (\*(AqFile\*(Aq);

  my $mp3 = File::MP3->new( \*(AqAndvari.mp3\*(Aq, $data );
  $mp3->save();
.Ve

Since we didn't define a \f(CW\*(C`save()\*(C' method in the \f(CW\*(C`File::MP3\*(C' class,
Perl will look at the \f(CW\*(C`File::MP3\*(C' class's parent classes to find the
\f(CW\*(C`save()\*(C' method. If Perl cannot find a \f(CW\*(C`save()\*(C' method anywhere in
the inheritance hierarchy, it will die.

In this case, it finds a \f(CW\*(C`save()\*(C' method in the \f(CW\*(C`File\*(C' class. Note
that the object passed to \f(CW\*(C`save()\*(C' in this case is still a
\f(CW\*(C`File::MP3\*(C' object, even though the method is found in the \f(CW\*(C`File\*(C'
class.

We can override a parent's method in a child class. When we do so, we
can still call the parent class's method with the \f(CW\*(C`SUPER\*(C'
pseudo-class.

.Vb 2
  sub save \{
      my $self = shift;

      say \*(AqPrepare to rock\*(Aq;
      $self->SUPER::save();
  \}
.Ve

The \f(CW\*(C`SUPER\*(C' modifier can *only* be used for method calls. You can't
use it for regular subroutine calls or class methods:

.Vb 1
  SUPER::save($thing);     # FAIL: looks for save() sub in package SUPER

  SUPER->save($thing);     # FAIL: looks for save() method in class
                           #       SUPER

  $thing->SUPER::save();   # Okay: looks for save() method in parent
                           #       classes
.Ve

*How \s-1SUPER\s0 is Resolved*
Xref "SUPER"
Subsection "How SUPER is Resolved"

The \f(CW\*(C`SUPER\*(C' pseudo-class is resolved from the package where the call
is made. It is *not* resolved based on the object's class. This is
important, because it lets methods at different levels within a deep
inheritance hierarchy each correctly call their respective parent
methods.

.Vb 1
  package A;

  sub new \{
      return bless \{\}, shift;
  \}

  sub speak \{
      my $self = shift;

      say \*(AqA\*(Aq;
  \}

  package B;

  use parent -norequire, \*(AqA\*(Aq;

  sub speak \{
      my $self = shift;

      $self->SUPER::speak();

      say \*(AqB\*(Aq;
  \}

  package C;

  use parent -norequire, \*(AqB\*(Aq;

  sub speak \{
      my $self = shift;

      $self->SUPER::speak();

      say \*(AqC\*(Aq;
  \}

  my $c = C->new();
  $c->speak();
.Ve

In this example, we will get the following output:

.Vb 3
  A
  B
  C
.Ve

This demonstrates how \f(CW\*(C`SUPER\*(C' is resolved. Even though the object is
blessed into the \f(CW\*(C`C\*(C' class, the \f(CW\*(C`speak()\*(C' method in the \f(CW\*(C`B\*(C' class
can still call \f(CW\*(C`SUPER::speak()\*(C' and expect it to correctly look in the
parent class of \f(CW\*(C`B\*(C' (i.e the class the method call is in), not in the
parent class of \f(CW\*(C`C\*(C' (i.e. the class the object belongs to).

There are rare cases where this package-based resolution can be a
problem. If you copy a subroutine from one package to another, \f(CW\*(C`SUPER\*(C'
resolution will be done based on the original package.

*Multiple Inheritance*
Xref "multiple inheritance"
Subsection "Multiple Inheritance"

Multiple inheritance often indicates a design problem, but Perl always
gives you enough rope to hang yourself with if you ask for it.

To declare multiple parents, you simply need to pass multiple class
names to \f(CW\*(C`use parent\*(C':

.Vb 1
  package MultiChild;

  use parent \*(AqParent1\*(Aq, \*(AqParent2\*(Aq;
.Ve

*Method Resolution Order*
Xref "method resolution order mro"
Subsection "Method Resolution Order"

Method resolution order only matters in the case of multiple
inheritance. In the case of single inheritance, Perl simply looks up
the inheritance chain to find a method:

.Vb 5
  Grandparent
    |
  Parent
    |
  Child
.Ve

If we call a method on a \f(CW\*(C`Child\*(C' object and that method is not defined
in the \f(CW\*(C`Child\*(C' class, Perl will look for that method in the \f(CW\*(C`Parent\*(C'
class and then, if necessary, in the \f(CW\*(C`Grandparent\*(C' class.

If Perl cannot find the method in any of these classes, it will die
with an error message.

When a class has multiple parents, the method lookup order becomes more
complicated.

By default, Perl does a depth-first left-to-right search for a method.
That means it starts with the first parent in the \f(CW@ISA array, and
then searches all of its parents, grandparents, etc. If it fails to
find the method, it then goes to the next parent in the original
class's \f(CW@ISA array and searches from there.

.Vb 7
            SharedGreatGrandParent
            /                    \\
  PaternalGrandparent       MaternalGrandparent
            \\                    /
             Father        Mother
                   \\      /
                    Child
.Ve

So given the diagram above, Perl will search \f(CW\*(C`Child\*(C', \f(CW\*(C`Father\*(C',
\f(CW\*(C`PaternalGrandparent\*(C', \f(CW\*(C`SharedGreatGrandParent\*(C', \f(CW\*(C`Mother\*(C', and
finally \f(CW\*(C`MaternalGrandparent\*(C'. This may be a problem because now we're
looking in \f(CW\*(C`SharedGreatGrandParent\*(C' *before* we've checked all its
derived classes (i.e. before we tried \f(CW\*(C`Mother\*(C' and
\f(CW\*(C`MaternalGrandparent\*(C').

It is possible to ask for a different method resolution order with the
mro pragma.

.Vb 1
  package Child;

  use mro \*(Aqc3\*(Aq;
  use parent \*(AqFather\*(Aq, \*(AqMother\*(Aq;
.Ve

This pragma lets you switch to the \*(L"C3\*(R" resolution order. In simple
terms, \*(L"C3\*(R" order ensures that shared parent classes are never searched
before child classes, so Perl will now search: \f(CW\*(C`Child\*(C', \f(CW\*(C`Father\*(C',
\f(CW\*(C`PaternalGrandparent\*(C', \f(CW\*(C`Mother\*(C' \f(CW\*(C`MaternalGrandparent\*(C', and finally
\f(CW\*(C`SharedGreatGrandParent\*(C'. Note however that this is not
\*(L"breadth-first\*(R" searching: All the \f(CW\*(C`Father\*(C' ancestors (except the
common ancestor) are searched before any of the \f(CW\*(C`Mother\*(C' ancestors are
considered.

The C3 order also lets you call methods in sibling classes with the
\f(CW\*(C`next\*(C' pseudo-class. See the mro documentation for more details on
this feature.

*Method Resolution Caching*
Subsection "Method Resolution Caching"

When Perl searches for a method, it caches the lookup so that future
calls to the method do not need to search for it again. Changing a
class's parent class or adding subroutines to a class will invalidate
the cache for that class.

The mro pragma provides some functions for manipulating the method
cache directly.

### Writing Constructors

Xref "constructor"
Subsection "Writing Constructors"
As we mentioned earlier, Perl provides no special constructor syntax.
This means that a class must implement its own constructor. A
constructor is simply a class method that returns a reference to a new
object.

The constructor can also accept additional parameters that define the
object. Let's write a real constructor for the \f(CW\*(C`File\*(C' class we used
earlier:

.Vb 1
  package File;

  sub new \{
      my $class = shift;
      my ( $path, $data ) = @_;

      my $self = bless \{
          path => $path,
          data => $data,
      \}, $class;

      return $self;
  \}
.Ve

As you can see, we've stored the path and file data in the object
itself. Remember, under the hood, this object is still just a hash.
Later, we'll write accessors to manipulate this data.

For our \f(CW\*(C`File::MP3\*(C' class, we can check to make sure that the path
we're given ends with \*(L".mp3\*(R":

.Vb 1
  package File::MP3;

  sub new \{
      my $class = shift;
      my ( $path, $data ) = @_;

      die "You cannot create a File::MP3 without an mp3 extension\\n"
          unless $path =~ /\\.mp3\\z/;

      return $class->SUPER::new(@_);
  \}
.Ve

This constructor lets its parent class do the actual object
construction.

### Attributes

Xref "attribute"
Subsection "Attributes"
An attribute is a piece of data belonging to a particular object.
Unlike most object-oriented languages, Perl provides no special syntax
or support for declaring and manipulating attributes.

Attributes are often stored in the object itself. For example, if the
object is an anonymous hash, we can store the attribute values in the
hash using the attribute name as the key.

While it's possible to refer directly to these hash keys outside of the
class, it's considered a best practice to wrap all access to the
attribute with accessor methods.

This has several advantages. Accessors make it easier to change the
implementation of an object later while still preserving the original
\s-1API.\s0

An accessor lets you add additional code around attribute access. For
example, you could apply a default to an attribute that wasn't set in
the constructor, or you could validate that a new value for the
attribute is acceptable.

Finally, using accessors makes inheritance much simpler. Subclasses can
use the accessors rather than having to know how a parent class is
implemented internally.

*Writing Accessors*
Xref "accessor"
Subsection "Writing Accessors"

As with constructors, Perl provides no special accessor declaration
syntax, so classes must provide explicitly written accessor methods.
There are two common types of accessors, read-only and read-write.

A simple read-only accessor simply gets the value of a single
attribute:

.Vb 2
  sub path \{
      my $self = shift;

      return $self->\{path\};
  \}
.Ve

A read-write accessor will allow the caller to set the value as well as
get it:

.Vb 2
  sub path \{
      my $self = shift;

      if (@_) \{
          $self->\{path\} = shift;
      \}

      return $self->\{path\};
  \}
.Ve

### An Aside About Smarter and Safer Code

Subsection "An Aside About Smarter and Safer Code"
Our constructor and accessors are not very smart. They don't check that
a \f(CW$path is defined, nor do they check that a \f(CW$path is a valid
filesystem path.

Doing these checks by hand can quickly become tedious. Writing a bunch
of accessors by hand is also incredibly tedious. There are a lot of
modules on \s-1CPAN\s0 that can help you write safer and more concise code,
including the modules we recommend in perlootut.

### Method Call Variations

Xref "method"
Subsection "Method Call Variations"
Perl supports several other ways to call methods besides the \f(CW\*(C`$object->method()\*(C' usage we've seen so far.

*Method Names with a Fully Qualified Name*
Subsection "Method Names with a Fully Qualified Name"

Perl allows you to call methods using their fully qualified name (the
package and method name):

.Vb 2
  my $mp3 = File::MP3->new( \*(AqRegin.mp3\*(Aq, $data );
  $mp3->File::save();
.Ve

When you call a fully qualified method name like \f(CW\*(C`File::save\*(C', the method
resolution search for the \f(CW\*(C`save\*(C' method starts in the \f(CW\*(C`File\*(C' class,
skipping any \f(CW\*(C`save\*(C' method the \f(CW\*(C`File::MP3\*(C' class may have defined. It
still searches the \f(CW\*(C`File\*(C' class's parents if necessary.

While this feature is most commonly used to explicitly call methods
inherited from an ancestor class, there is no technical restriction
that enforces this:

.Vb 2
  my $obj = Tree->new();
  $obj->Dog::bark();
.Ve

This calls the \f(CW\*(C`bark\*(C' method from class \f(CW\*(C`Dog\*(C' on an object of class
\f(CW\*(C`Tree\*(C', even if the two classes are completely unrelated. Use this
with great care.

The \f(CW\*(C`SUPER\*(C' pseudo-class that was described earlier is *not* the same
as calling a method with a fully-qualified name. See the earlier
\*(L"Inheritance\*(R" section for details.

*Method Names as Strings*
Subsection "Method Names as Strings"

Perl lets you use a scalar variable containing a string as a method
name:

.Vb 1
  my $file = File->new( $path, $data );

  my $method = \*(Aqsave\*(Aq;
  $file->$method();
.Ve

This works exactly like calling \f(CW\*(C`$file->save()\*(C'. This can be very
useful for writing dynamic code. For example, it allows you to pass a
method name to be called as a parameter to another method.

*Class Names as Strings*
Subsection "Class Names as Strings"

Perl also lets you use a scalar containing a string as a class name:

.Vb 1
  my $class = \*(AqFile\*(Aq;

  my $file = $class->new( $path, $data );
.Ve

Again, this allows for very dynamic code.

*Subroutine References as Methods*
Subsection "Subroutine References as Methods"

You can also use a subroutine reference as a method:

.Vb 2
  my $sub = sub \{
      my $self = shift;

      $self->save();
  \};

  $file->$sub();
.Ve

This is exactly equivalent to writing \f(CW\*(C`$sub->($file)\*(C'. You may see
this idiom in the wild combined with a call to \f(CW\*(C`can\*(C':

.Vb 3
  if ( my $meth = $object->can(\*(Aqfoo\*(Aq) ) \{
      $object->$meth();
  \}
.Ve

*Dereferencing Method Call*
Subsection "Dereferencing Method Call"

Perl also lets you use a dereferenced scalar reference in a method
call. That's a mouthful, so let's look at some code:

.Vb 4
  $file->$\{ \\\*(Aqsave\*(Aq \};
  $file->$\{ returns_scalar_ref() \};
  $file->$\{ \\( returns_scalar() ) \};
  $file->$\{ returns_ref_to_sub_ref() \};
.Ve

This works if the dereference produces a string *or* a subroutine
reference.

*Method Calls on Filehandles*
Subsection "Method Calls on Filehandles"

Under the hood, Perl filehandles are instances of the \f(CW\*(C`IO::Handle\*(C' or
\f(CW\*(C`IO::File\*(C' class. Once you have an open filehandle, you can call
methods on it. Additionally, you can call methods on the \f(CW\*(C`STDIN\*(C',
\f(CW\*(C`STDOUT\*(C', and \f(CW\*(C`STDERR\*(C' filehandles.

.Vb 3
  open my $fh, \*(Aq>\*(Aq, \*(Aqpath/to/file\*(Aq;
  $fh->autoflush();
  $fh->print(\*(Aqcontent\*(Aq);

  STDOUT->autoflush();
.Ve

### Invoking Class Methods

Xref "invocation"
Subsection "Invoking Class Methods"
Because Perl allows you to use barewords for package names and
subroutine names, it sometimes interprets a bareword's meaning
incorrectly. For example, the construct \f(CW\*(C`Class->new()\*(C' can be
interpreted as either \f(CW\*(C`\*(AqClass\*(Aq->new()\*(C' or \f(CW\*(C`Class()->new()\*(C'.
In English, that second interpretation reads as \*(L"call a subroutine
named **Class()**, then call **new()** as a method on the return value of
**Class()**\*(R". If there is a subroutine named \f(CW\*(C`Class()\*(C' in the current
namespace, Perl will always interpret \f(CW\*(C`Class->new()\*(C' as the second
alternative: a call to \f(CW\*(C`new()\*(C' on the object  returned by a call to
\f(CW\*(C`Class()\*(C'

You can force Perl to use the first interpretation (i.e. as a method
call on the class named \*(L"Class\*(R") in two ways. First, you can append a
\f(CW\*(C`::\*(C' to the class name:

.Vb 1
    Class::->new()
.Ve

Perl will always interpret this as a method call.

Alternatively, you can quote the class name:

.Vb 1
    \*(AqClass\*(Aq->new()
.Ve

Of course, if the class name is in a scalar Perl will do the right
thing as well:

.Vb 2
    my $class = \*(AqClass\*(Aq;
    $class->new();
.Ve

*Indirect Object Syntax*
Xref "indirect object"
Subsection "Indirect Object Syntax"

\fBOutside of the file handle case, use of this syntax is discouraged as
it can confuse the Perl interpreter. See below for more details.

Perl supports another method invocation syntax called \*(L"indirect object\*(R"
notation. This syntax is called \*(L"indirect\*(R" because the method comes
before the object it is being invoked on.

This syntax can be used with any class or object method:

.Vb 2
    my $file = new File $path, $data;
    save $file;
.Ve

We recommend that you avoid this syntax, for several reasons.

First, it can be confusing to read. In the above example, it's not
clear if \f(CW\*(C`save\*(C' is a method provided by the \f(CW\*(C`File\*(C' class or simply a
subroutine that expects a file object as its first argument.

When used with class methods, the problem is even worse. Because Perl
allows subroutine names to be written as barewords, Perl has to guess
whether the bareword after the method is a class name or subroutine
name. In other words, Perl can resolve the syntax as either \f(CW\*(C`File->new( $path, $data )\*(C' **or** \f(CW\*(C`new( File( $path, $data ) )\*(C'.

To parse this code, Perl uses a heuristic based on what package names
it has seen, what subroutines exist in the current package, what
barewords it has previously seen, and other input. Needless to say,
heuristics can produce very surprising results!

Older documentation (and some \s-1CPAN\s0 modules) encouraged this syntax,
particularly for constructors, so you may still find it in the wild.
However, we encourage you to avoid using it in new code.

You can force Perl to interpret the bareword as a class name by
appending \*(L"::\*(R" to it, like we saw earlier:

.Vb 1
  my $file = new File:: $path, $data;
.Ve
.ie n .SS """bless"", ""blessed"", and ""ref"""
.el .SS "\f(CWbless, \f(CWblessed, and \f(CWref"
Subsection "bless, blessed, and ref"
As we saw earlier, an object is simply a data structure that has been
blessed into a class via the \f(CW\*(C`bless\*(C' function. The \f(CW\*(C`bless\*(C' function
can take either one or two arguments:

.Vb 2
  my $object = bless \{\}, $class;
  my $object = bless \{\};
.Ve

In the first form, the anonymous hash is being blessed into the class
in \f(CW$class. In the second form, the anonymous hash is blessed into
the current package.

The second form is strongly discouraged, because it breaks the ability
of a subclass to reuse the parent's constructor, but you may still run
across it in existing code.

If you want to know whether a particular scalar refers to an object,
you can use the \f(CW\*(C`blessed\*(C' function exported by Scalar::Util, which
is shipped with the Perl core.

.Vb 1
  use Scalar::Util \*(Aqblessed\*(Aq;

  if ( defined blessed($thing) ) \{ ... \}
.Ve

If \f(CW$thing refers to an object, then this function returns the name
of the package the object has been blessed into. If \f(CW$thing doesn't
contain a reference to a blessed object, the \f(CW\*(C`blessed\*(C' function
returns \f(CW\*(C`undef\*(C'.

Note that \f(CW\*(C`blessed($thing)\*(C' will also return false if \f(CW$thing has
been blessed into a class named \*(L"0\*(R". This is a possible, but quite
pathological. Don't create a class named \*(L"0\*(R" unless you know what
you're doing.

Similarly, Perl's built-in \f(CW\*(C`ref\*(C' function treats a reference to a
blessed object specially. If you call \f(CW\*(C`ref($thing)\*(C' and \f(CW$thing
holds a reference to an object, it will return the name of the class
that the object has been blessed into.

If you simply want to check that a variable contains an object
reference, we recommend that you use \f(CW\*(C`defined blessed($object)\*(C', since
\f(CW\*(C`ref\*(C' returns true values for all references, not just objects.

### The \s-1UNIVERSAL\s0 Class

Xref "UNIVERSAL"
Subsection "The UNIVERSAL Class"
All classes automatically inherit from the \s-1UNIVERSAL\s0 class, which is
built-in to the Perl core. This class provides a number of methods, all
of which can be called on either a class or an object. You can also
choose to override some of these methods in your class. If you do so,
we recommend that you follow the built-in semantics described below.

- isa($class)
Xref "isa"
Item "isa($class)"
The \f(CW\*(C`isa\*(C' method returns *true* if the object is a member of the
class in \f(CW$class, or a member of a subclass of \f(CW$class.
.Sp
If you override this method, it should never throw an exception.

- \s-1DOES\s0($role)
Xref "DOES"
Item "DOES($role)"
The \f(CW\*(C`DOES\*(C' method returns *true* if its object claims to perform the
role \f(CW$role. By default, this is equivalent to \f(CW\*(C`isa\*(C'. This method is
provided for use by object system extensions that implement roles, like
\f(CW\*(C`Moose\*(C' and \f(CW\*(C`Role::Tiny\*(C'.
.Sp
You can also override \f(CW\*(C`DOES\*(C' directly in your own classes. If you
override this method, it should never throw an exception.

- can($method)
Xref "can"
Item "can($method)"
The \f(CW\*(C`can\*(C' method checks to see if the class or object it was called on
has a method named \f(CW$method. This checks for the method in the class
and all of its parents. If the method exists, then a reference to the
subroutine is returned. If it does not then \f(CW\*(C`undef\*(C' is returned.
.Sp
If your class responds to method calls via \f(CW\*(C`AUTOLOAD\*(C', you may want to
overload \f(CW\*(C`can\*(C' to return a subroutine reference for methods which your
\f(CW\*(C`AUTOLOAD\*(C' method handles.
.Sp
If you override this method, it should never throw an exception.

- \s-1VERSION\s0($need)
Xref "VERSION"
Item "VERSION($need)"
The \f(CW\*(C`VERSION\*(C' method returns the version number of the class
(package).
.Sp
If the \f(CW$need argument is given then it will check that the current
version (as defined by the \f(CW$VERSION variable in the package) is greater
than or equal to \f(CW$need; it will die if this is not the case. This
method is called automatically by the \f(CW\*(C`VERSION\*(C' form of \f(CW\*(C`use\*(C'.
.Sp
.Vb 3
    use Package 1.2 qw(some imported subs);
    # implies:
    Package->VERSION(1.2);
.Ve
.Sp
We recommend that you use this method to access another package's
version, rather than looking directly at \f(CW$Package::VERSION. The
package you are looking at could have overridden the \f(CW\*(C`VERSION\*(C' method.
.Sp
We also recommend using this method to check whether a module has a
sufficient version. The internal implementation uses the version
module to make sure that different types of version numbers are
compared correctly.

### \s-1AUTOLOAD\s0

Xref "AUTOLOAD"
Subsection "AUTOLOAD"
If you call a method that doesn't exist in a class, Perl will throw an
error. However, if that class or any of its parent classes defines an
\f(CW\*(C`AUTOLOAD\*(C' method, that \f(CW\*(C`AUTOLOAD\*(C' method is called instead.

\f(CW\*(C`AUTOLOAD\*(C' is called as a regular method, and the caller will not know
the difference. Whatever value your \f(CW\*(C`AUTOLOAD\*(C' method returns is
returned to the caller.

The fully qualified method name that was called is available in the
\f(CW$AUTOLOAD package global for your class. Since this is a global, if
you want to refer to do it without a package name prefix under \f(CW\*(C`strict
\*(Aqvars\*(Aq\*(C', you need to declare it.

.Vb 5
  # XXX - this is a terrible way to implement accessors, but it makes
  # for a simple example.
  our $AUTOLOAD;
  sub AUTOLOAD \{
      my $self = shift;

      # Remove qualifier from original method name...
      my $called =  $AUTOLOAD =~ s/.*:://r;

      # Is there an attribute of that name?
      die "No such attribute: $called"
          unless exists $self->\{$called\};

      # If so, return it...
      return $self->\{$called\};
  \}

  sub DESTROY \{ \} # see below
.Ve

Without the \f(CW\*(C`our $AUTOLOAD\*(C' declaration, this code will not compile
under the strict pragma.

As the comment says, this is not a good way to implement accessors.
It's slow and too clever by far. However, you may see this as a way to
provide accessors in older Perl code. See perlootut for
recommendations on \s-1OO\s0 coding in Perl.

If your class does have an \f(CW\*(C`AUTOLOAD\*(C' method, we strongly recommend
that you override \f(CW\*(C`can\*(C' in your class as well. Your overridden \f(CW\*(C`can\*(C'
method should return a subroutine reference for any method that your
\f(CW\*(C`AUTOLOAD\*(C' responds to.

### Destructors

Xref "destructor DESTROY"
Subsection "Destructors"
When the last reference to an object goes away, the object is
destroyed. If you only have one reference to an object stored in a
lexical scalar, the object is destroyed when that scalar goes out of
scope. If you store the object in a package global, that object may not
go out of scope until the program exits.

If you want to do something when the object is destroyed, you can
define a \f(CW\*(C`DESTROY\*(C' method in your class. This method will always be
called by Perl at the appropriate time, unless the method is empty.

This is called just like any other method, with the object as the first
argument. It does not receive any additional arguments. However, the
\f(CW$_[0] variable will be read-only in the destructor, so you cannot
assign a value to it.

If your \f(CW\*(C`DESTROY\*(C' method throws an exception, this will not cause
any control transfer beyond exiting the method.  The exception will be
reported to \f(CW\*(C`STDERR\*(C' as a warning, marked \*(L"(in cleanup)\*(R", and Perl will
continue with whatever it was doing before.

Because \f(CW\*(C`DESTROY\*(C' methods can be called at any time, you should localize
any global status variables that might be set by anything you do in
your \f(CW\*(C`DESTROY\*(C' method.  If you are in doubt about a particular status
variable, it doesn't hurt to localize it.  There are five global status
variables, and the safest way is to localize all five of them:

.Vb 5
  sub DESTROY \{
      local($., $@, $!, $^E, $?);
      my $self = shift;
      ...;
  \}
.Ve

If you define an \f(CW\*(C`AUTOLOAD\*(C' in your class, then Perl will call your
\f(CW\*(C`AUTOLOAD\*(C' to handle the \f(CW\*(C`DESTROY\*(C' method. You can prevent this by
defining an empty \f(CW\*(C`DESTROY\*(C', like we did in the autoloading example.
You can also check the value of \f(CW$AUTOLOAD and return without doing
anything when called to handle \f(CW\*(C`DESTROY\*(C'.

*Global Destruction*
Subsection "Global Destruction"

The order in which objects are destroyed during the global destruction
before the program exits is unpredictable. This means that any objects
contained by your object may already have been destroyed. You should
check that a contained object is defined before calling a method on it:

.Vb 2
  sub DESTROY \{
      my $self = shift;

      $self->\{handle\}->close() if $self->\{handle\};
  \}
.Ve

You can use the \f(CW\*(C`$\{^GLOBAL_PHASE\}\*(C' variable to detect if you are
currently in the global destruction phase:

.Vb 2
  sub DESTROY \{
      my $self = shift;

      return if $\{^GLOBAL_PHASE\} eq \*(AqDESTRUCT\*(Aq;

      $self->\{handle\}->close();
  \}
.Ve

Note that this variable was added in Perl 5.14.0. If you want to detect
the global destruction phase on older versions of Perl, you can use the
\f(CW\*(C`Devel::GlobalDestruction\*(C' module on \s-1CPAN.\s0

If your \f(CW\*(C`DESTROY\*(C' method issues a warning during global destruction,
the Perl interpreter will append the string \*(L" during global
destruction\*(R" to the warning.

During global destruction, Perl will always garbage collect objects
before unblessed references. See \*(L"\s-1PERL_DESTRUCT_LEVEL\*(R"\s0 in perlhacktips
for more information about global destruction.

### Non-Hash Objects

Subsection "Non-Hash Objects"
All the examples so far have shown objects based on a blessed hash.
However, it's possible to bless any type of data structure or referent,
including scalars, globs, and subroutines. You may see this sort of
thing when looking at code in the wild.

Here's an example of a module as a blessed scalar:

.Vb 1
  package Time;

  use strict;
  use warnings;

  sub new \{
      my $class = shift;

      my $time = time;
      return bless \\$time, $class;
  \}

  sub epoch \{
      my $self = shift;
      return $$self;
  \}

  my $time = Time->new();
  print $time->epoch();
.Ve

### Inside-Out objects

Subsection "Inside-Out objects"
In the past, the Perl community experimented with a technique called
\*(L"inside-out objects\*(R". An inside-out object stores its data outside of
the object's reference, indexed on a unique property of the object,
such as its memory address, rather than in the object itself. This has
the advantage of enforcing the encapsulation of object attributes,
since their data is not stored in the object itself.

This technique was popular for a while (and was recommended in Damian
Conway's *Perl Best Practices*), but never achieved universal
adoption. The Object::InsideOut module on \s-1CPAN\s0 provides a
comprehensive implementation of this technique, and you may see it or
other inside-out modules in the wild.

Here is a simple example of the technique, using the
Hash::Util::FieldHash core module. This module was added to the core
to support inside-out object implementations.

.Vb 1
  package Time;

  use strict;
  use warnings;

  use Hash::Util::FieldHash \*(Aqfieldhash\*(Aq;

  fieldhash my %time_for;

  sub new \{
      my $class = shift;

      my $self = bless \\( my $object ), $class;

      $time_for\{$self\} = time;

      return $self;
  \}

  sub epoch \{
      my $self = shift;

      return $time_for\{$self\};
  \}

  my $time = Time->new;
  print $time->epoch;
.Ve

### Pseudo-hashes

Subsection "Pseudo-hashes"
The pseudo-hash feature was an experimental feature introduced in
earlier versions of Perl and removed in 5.10.0. A pseudo-hash is an
array reference which can be accessed using named keys like a hash. You
may run in to some code in the wild which uses it. See the fields
pragma for more information.

## SEE ALSO

Header "SEE ALSO"
A kinder, gentler tutorial on object-oriented programming in Perl can
be found in perlootut. You should also check out perlmodlib for
some style guides on constructing both modules and classes.
