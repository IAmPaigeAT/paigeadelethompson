+++
manpage_name = "piconv"
keywords = ["header", "see", "also", "fbiconv", "1", "fblocale", "3", "encode", "supported", "alias", "perlio"]
title = "piconv(1)"
manpage_section = "1"
date = "2024-12-14"
author = "None Specified"
operating_system_version = "15.3"
manpage_format = "troff"
description = "piconv is perl version of iconv, a character encoding converter widely available for various Unixen today.  This script was primarily a technology demonstrator for Perl 5.8.0, but you can use piconv in the place of iconv for virtually any case. pi..."
detected_package_version = "5.34.1"
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PICONV 1"
PICONV 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

piconv -- iconv(1), reinvented in perl

## SYNOPSIS

Header "SYNOPSIS"
.Vb 6
  piconv [-f from_encoding] [-t to_encoding]
         [-p|--perlqq|--htmlcref|--xmlcref] [-C N|-c] [-D] [-S scheme]
         [-s string|file...]
  piconv -l
  piconv -r encoding_alias
  piconv -h
.Ve

## DESCRIPTION

Header "DESCRIPTION"
**piconv** is perl version of **iconv**, a character encoding converter
widely available for various Unixen today.  This script was primarily
a technology demonstrator for Perl 5.8.0, but you can use piconv in the
place of iconv for virtually any case.

piconv converts the character encoding of either \s-1STDIN\s0 or files
specified in the argument and prints out to \s-1STDOUT.\s0

Here is the list of options.  Some options can be in short format (-f)
or long (--from) one.

- -f,--from \fIfrom_encoding
Item "-f,--from from_encoding"
Specifies the encoding you are converting from.  Unlike **iconv**,
this option can be omitted.  In such cases, the current locale is used.

- -t,--to \fIto_encoding
Item "-t,--to to_encoding"
Specifies the encoding you are converting to.  Unlike **iconv**,
this option can be omitted.  In such cases, the current locale is used.
.Sp
Therefore, when both -f and -t are omitted, **piconv** just acts
like **cat**.

- -s,--string \fIstring
Item "-s,--string string"
uses *string* instead of file for the source of text.

- -l,--list
Item "-l,--list"
Lists all available encodings, one per line, in case-insensitive
order.  Note that only the canonical names are listed; many aliases
exist.  For example, the names are case-insensitive, and many standard
and common aliases work, such as \*(L"latin1\*(R" for \*(L"\s-1ISO-8859-1\*(R",\s0 or \*(L"ibm850\*(R"
instead of \*(L"cp850\*(R", or \*(L"winlatin1\*(R" for \*(L"cp1252\*(R".  See Encode::Supported
for a full discussion.

- -r,--resolve \fIencoding_alias
Item "-r,--resolve encoding_alias"
Resolve *encoding_alias* to Encode canonical encoding name.

- -C,--check \fIN
Item "-C,--check N"
Check the validity of the stream if *N* = 1.  When *N* = -1, something
interesting happens when it encounters an invalid character.

- -c
Item "-c"
Same as \f(CW\*(C`-C 1\*(C'.

- -p,--perlqq
Item "-p,--perlqq"
Transliterate characters missing in encoding to \\x\{\s-1HHHH\s0\} where \s-1HHHH\s0 is the
hexadecimal Unicode code point.

- --htmlcref
Item "--htmlcref"
Transliterate characters missing in encoding to &#NNN; where \s-1NNN\s0 is the
decimal Unicode code point.

- --xmlcref
Item "--xmlcref"
Transliterate characters missing in encoding to &#xHHHH; where \s-1HHHH\s0 is the
hexadecimal Unicode code point.

- -h,--help
Item "-h,--help"
Show usage.

- -D,--debug
Item "-D,--debug"
Invokes debugging mode.  Primarily for Encode hackers.

- -S,--scheme \fIscheme
Item "-S,--scheme scheme"
Selects which scheme is to be used for conversion.  Available schemes
are as follows:

> 
- from_to
Item "from_to"
Uses Encode::from_to for conversion.  This is the default.

- decode_encode
Item "decode_encode"
Input strings are **decode()**d then **encode()**d.  A straight two-step
implementation.

- perlio
Item "perlio"
The new perlIO layer is used.  \s-1NI-S\s0' favorite.
.Sp
You should use this option if you are using \s-1UTF-16\s0 and others which
linefeed is not $/.



> .Sp
Like the *-D* option, this is also for Encode hackers.



## SEE ALSO

Header "SEE ALSO"
**iconv**\|(1)
**locale**\|(3)
Encode
Encode::Supported
Encode::Alias
PerlIO
