+++
manpage_section = "1"
operating_system = "macos"
date = "2022-02-19"
detected_package_version = "5.34.1"
operating_system_version = "15.3"
manpage_name = "perl5300delta"
description = "This document describes differences between the 5.28.0 release and the 5.30.0 release. If you are upgrading from an earlier release such as 5.26.0, first read perl5280delta, which describes differences between 5.26.0 and 5.28.0. sv_utf8_(downgrade..."
title = "perl5300delta(1)"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_format = "troff"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5300DELTA 1"
PERL5300DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5300delta - what is new for perl v5.30.0

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.28.0 release and the 5.30.0
release.

If you are upgrading from an earlier release such as 5.26.0, first read
perl5280delta, which describes differences between 5.26.0 and 5.28.0.

## Notice

Header "Notice"
sv_utf8_(downgrade|decode) are no longer marked as experimental.
[\s-1GH\s0 #16822] <https://github.com/Perl/perl5/issues/16822>.

## Core Enhancements

Header "Core Enhancements"

### Limited variable length lookbehind in regular expression pattern matching is now experimentally supported

Subsection "Limited variable length lookbehind in regular expression pattern matching is now experimentally supported"
Using a lookbehind assertion (like \f(CW\*(C`(?<=foo?)\*(C' or \f(CW\*(C`(?<!ba\{1,9\}r)\*(C' previously
would generate an error and refuse to compile.  Now it compiles (if the
maximum lookbehind is at most 255 characters), but raises a warning in
the new \f(CW\*(C`experimental::vlb\*(C' warnings category.  This is to caution you
that the precise behavior is subject to change based on feedback from
use in the field.

See \*(L"(?<=pattern)\*(R" in perlre and \*(L"(?<!pattern)\*(R" in perlre.
.ie n .SS "The upper limit ""n"" specifiable in a regular expression quantifier of the form ""\{m,n\}"" has been doubled to 65534"
.el .SS "The upper limit \f(CW``n'' specifiable in a regular expression quantifier of the form \f(CW``\{m,n\}'' has been doubled to 65534"
Subsection "The upper limit ""n"" specifiable in a regular expression quantifier of the form ""\{m,n\}"" has been doubled to 65534"
The meaning of an unbounded upper quantifier \f(CW"\{m,\}" remains unchanged.
It matches 2**31 - 1 times on most platforms, and more on ones where a C
language short variable is more than 4 bytes long.

### Unicode 12.1 is supported

Subsection "Unicode 12.1 is supported"
Because of a change in Unicode release cycles, Perl jumps from Unicode
10.0 in Perl 5.28 to Unicode 12.1 in Perl 5.30.

For details on the Unicode changes, see
<https://www.unicode.org/versions/Unicode11.0.0/> for 11.0;
<https://www.unicode.org/versions/Unicode12.0.0/> for 12.0;
and
<https://www.unicode.org/versions/Unicode12.1.0/> for 12.1.
(Unicode 12.1 differs from 12.0 only in the addition of a single
character, that for the new Japanese era name.)

The Word_Break property, as in past Perl releases, remains tailored to
behave more in line with expectations of Perl users.  This means that
sequential runs of horizontal white space characters are not broken
apart, but kept as a single run.  Unicode 11 changed from past versions
to be more in line with Perl, but it left several white space characters
as causing breaks: \s-1TAB, NO BREAK SPACE,\s0 and \s-1FIGURE SPACE\s0 (U+2007).  We
have decided to continue to use the previous Perl tailoring with regards
to these.

### Wildcards in Unicode property value specifications are now partially supported

Subsection "Wildcards in Unicode property value specifications are now partially supported"
You can now do something like this in a regular expression pattern

.Vb 1
 qr! \\p\{nv= /(?x) \\A [0-5] \\z / \}!
.Ve

which matches all Unicode code points whose numeric value is
between 0 and 5 inclusive.  So, it could match the Thai or Bengali
digits whose numeric values are 0, 1, 2, 3, 4, or 5.

This marks another step in implementing the regular expression features
the Unicode Consortium suggests.

Most properties are supported, with the remainder planned for 5.32.
Details are in \*(L"Wildcards in Property Values\*(R" in perlunicode.

### qr\\N\{name\} is now supported

Subsection "qr'N\{name\}' is now supported"
Previously it was an error to evaluate a named character \f(CW\*(C`\\N\{...\}\*(C'
within a single quoted regular expression pattern (whose evaluation is
deferred from the normal place).  This restriction is now removed.

### Turkic \s-1UTF-8\s0 locales are now seamlessly supported

Subsection "Turkic UTF-8 locales are now seamlessly supported"
Turkic languages have different casing rules than other languages for
the characters \f(CW"i" and \f(CW"I".  The uppercase of \f(CW"i" is \s-1LATIN
CAPITAL LETTER I WITH DOT ABOVE\s0 (U+0130); and the lowercase of \f(CW"I" is \s-1LATIN
SMALL LETTER DOTLESS I\s0 (U+0131).  Unicode furnishes alternate casing
rules for use with Turkic languages.  Previously, Perl ignored these,
but now, it uses them when it detects that it is operating under a
Turkic \s-1UTF-8\s0 locale.

### It is now possible to compile perl to always use thread-safe locale operations.

Subsection "It is now possible to compile perl to always use thread-safe locale operations."
Previously, these calls were only used when the perl was compiled to be
multi-threaded.  To always enable them, add

.Vb 1
 -Accflags=\*(Aq-DUSE_THREAD_SAFE_LOCALE\*(Aq
.Ve

to your *Configure* flags.

### Eliminate opASSIGN macro usage from core

Subsection "Eliminate opASSIGN macro usage from core"
This macro is still defined but no longer used in core
.ie n .SS """-Drv"" now means something on ""-DDEBUGGING"" builds"
.el .SS "\f(CW-Drv now means something on \f(CW-DDEBUGGING builds"
Subsection "-Drv now means something on -DDEBUGGING builds"
Now, adding the verbose flag (\f(CW\*(C`-Dv\*(C') to the \f(CW\*(C`-Dr\*(C' flag turns on all
possible regular expression debugging.

## Incompatible Changes

Header "Incompatible Changes"
.ie n .SS "Assigning non-zero to $[ is fatal"
.el .SS "Assigning non-zero to \f(CW$[ is fatal"
Subsection "Assigning non-zero to $[ is fatal"
Setting \f(CW$[ to a non-zero value has been deprecated since
Perl 5.12 and now throws a fatal error.
See "Assigning non-zero to \f(CW$[ is fatal" in perldeprecation.

### Delimiters must now be graphemes

Subsection "Delimiters must now be graphemes"
See \*(L"Use of unassigned code point or non-standalone grapheme
for a delimiter.\*(R" in perldeprecation
.ie n .SS "Some formerly deprecated uses of an unescaped left brace ""\{"" in regular expression patterns are now illegal"
.el .SS "Some formerly deprecated uses of an unescaped left brace \f(CW``\{'' in regular expression patterns are now illegal"
Subsection "Some formerly deprecated uses of an unescaped left brace ""\{"" in regular expression patterns are now illegal"
But to avoid breaking code unnecessarily, most instances that issued a
deprecation warning, remain legal and now have a non-deprecation warning
raised.  See \*(L"Unescaped left braces in regular expressions\*(R" in perldeprecation.

### Previously deprecated \fBsysread()/\fBsyswrite() on :utf8 handles is now fatal

Subsection "Previously deprecated sysread()/syswrite() on :utf8 handles is now fatal"
Calling **sysread()**, **syswrite()**, **send()** or **recv()** on a \f(CW\*(C`:utf8\*(C' handle,
whether applied explicitly or implicitly, is now fatal.  This was
deprecated in perl 5.24.

There were two problems with calling these functions on \f(CW\*(C`:utf8\*(C'
handles:

- \(bu
All four functions only paid attention to the \f(CW\*(C`:utf8\*(C' flag.  Other
layers were completely ignored, so a handle with
\f(CW\*(C`:encoding(UTF-16LE)\*(C' layer would be treated as \s-1UTF-8.\s0  Other layers,
such as compression are completely ignored with or without the
\f(CW\*(C`:utf8\*(C' flag.

- \(bu
**sysread()** and **recv()** would read from the handle, skipping any
validation by the layers, and do no validation of their own.  This
could lead to invalidly encoded perl scalars.

[\s-1GH\s0 #14839] <https://github.com/Perl/perl5/issues/14839>.

### \fBmy() in false conditional prohibited

Subsection "my() in false conditional prohibited"
Declarations such as \f(CW\*(C`my $x if 0\*(C' are no longer permitted.

[\s-1GH\s0 #16702] <https://github.com/Perl/perl5/issues/16702>.

### Fatalize $* and $#

Subsection "Fatalize $* and $#"
These special variables, long deprecated, now throw exceptions when used.

[\s-1GH\s0 #16718] <https://github.com/Perl/perl5/issues/16718>.

### Fatalize unqualified use of \fBdump()

Subsection "Fatalize unqualified use of dump()"
The \f(CW\*(C`dump()\*(C' function, long discouraged, may no longer be used unless it is
fully qualified, *i.e.*, \f(CW\*(C`CORE::dump()\*(C'.

[\s-1GH\s0 #16719] <https://github.com/Perl/perl5/issues/16719>.

### Remove \fBFile::Glob::glob()

Subsection "Remove File::Glob::glob()"
The \f(CW\*(C`File::Glob::glob()\*(C' function, long deprecated, has been removed and now
throws an exception which advises use of \f(CW\*(C`File::Glob::bsd_glob()\*(C' instead.

[\s-1GH\s0 #16721] <https://github.com/Perl/perl5/issues/16721>.
.ie n .SS """pack()"" no longer can return malformed \s-1UTF-8\s0"
.el .SS "\f(CWpack() no longer can return malformed \s-1UTF-8\s0"
Subsection "pack() no longer can return malformed UTF-8"
It croaks if it would otherwise return a \s-1UTF-8\s0 string that contains
malformed \s-1UTF-8.\s0  This protects against potential security threats.  This
is considered a bug fix as well.
[\s-1GH\s0 #16035] <https://github.com/Perl/perl5/issues/16035>.

### Any set of digits in the Common script are legal in a script run of another script

Subsection "Any set of digits in the Common script are legal in a script run of another script"
There are several sets of digits in the Common script.  \f(CW\*(C`[0-9]\*(C' is the
most familiar.  But there are also \f(CW\*(C`[\\x\{FF10\}-\\x\{FF19\}]\*(C' (\s-1FULLWIDTH
DIGIT ZERO\s0 - \s-1FULLWIDTH DIGIT NINE\s0), and several sets for use in
mathematical notation, such as the \s-1MATHEMATICAL\s0 DOUBLE-STRUCK DIGITs.
Any of these sets should be able to appear in script runs of, say,
Greek.  But the design of 5.30 overlooked all but the \s-1ASCII\s0 digits
\f(CW\*(C`[0-9]\*(C', so the design was flawed.  This has been fixed, so is both a
bug fix and an incompatibility.
[\s-1GH\s0 #16704] <https://github.com/Perl/perl5/issues/16704>.

All digits in a run still have to come from the same set of ten digits.

### \s-1JSON::PP\s0 enables allow_nonref by default

Subsection "JSON::PP enables allow_nonref by default"
As \s-1JSON::XS 4.0\s0 changed its policy and enabled allow_nonref
by default, \s-1JSON::PP\s0 also enabled allow_nonref by default.

## Deprecations

Header "Deprecations"

### In \s-1XS\s0 code, use of various macros dealing with \s-1UTF-8.\s0

Subsection "In XS code, use of various macros dealing with UTF-8."
This deprecation was scheduled to become fatal in 5.30, but has been
delayed to 5.32 due to problems that showed up with some \s-1CPAN\s0 modules.
For details of what's affected, see perldeprecation.

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
Translating from \s-1UTF-8\s0 into the code point it represents now is done via a
deterministic finite automaton, speeding it up.  As a typical example,
\f(CW\*(C`ord("\\x7fff")\*(C' now requires 12% fewer instructions than before.  The
performance of checking that a sequence of bytes is valid \s-1UTF-8\s0 is similarly
improved, again by using a \s-1DFA.\s0

- \(bu
Eliminate recursion from **finalize_op()**.
[\s-1GH\s0 #11866] <https://github.com/Perl/perl5/issues/11866>.

- \(bu
A handful of small optimizations related to character folding
and character classes in regular expressions.

- \(bu
Optimization of \f(CW\*(C`IV\*(C' to \f(CW\*(C`UV\*(C' conversions.
[\s-1GH\s0 #16761] <https://github.com/Perl/perl5/issues/16761>.

- \(bu
Speed up of the integer stringification algorithm by processing
two digits at a time instead of one.
[\s-1GH\s0 #16769] <https://github.com/Perl/perl5/issues/16769>.

- \(bu
Improvements based on \s-1LGTM\s0 analysis and recommendation.
(<https://lgtm.com/projects/g/Perl/perl5/alerts/?mode=tree>).
[\s-1GH\s0 #16765] <https://github.com/Perl/perl5/issues/16765>.
[\s-1GH\s0 #16773] <https://github.com/Perl/perl5/issues/16773>.

- \(bu
Code optimizations in *regcomp.c*, *regcomp.h*, *regexec.c*.

- \(bu
Regular expression pattern matching of things like \f(CW\*(C`qr/[^\f(CIa\f(CW]/\*(C' is
significantly sped up, where *a* is any \s-1ASCII\s0 character.  Other classes
can get this speed up, but which ones is complicated and depends on the
underlying bit patterns of those characters, so differs between \s-1ASCII\s0
and \s-1EBCDIC\s0 platforms, but all case pairs, like \f(CW\*(C`qr/[Gg]/\*(C' are included,
as is \f(CW\*(C`[^01]\*(C'.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Archive::Tar has been upgraded from version 2.30 to 2.32.

- \(bu
B has been upgraded from version 1.74 to 1.76.

- \(bu
B::Concise has been upgraded from version 1.003 to 1.004.

- \(bu
B::Deparse has been upgraded from version 1.48 to 1.49.

- \(bu
bignum has been upgraded from version 0.49 to 0.51.

- \(bu
bytes has been upgraded from version 1.06 to 1.07.

- \(bu
Carp has been upgraded from version 1.38 to 1.50

- \(bu
Compress::Raw::Bzip2 has been upgraded from version 2.074 to 2.084.

- \(bu
Compress::Raw::Zlib has been upgraded from version 2.076 to 2.084.

- \(bu
Config::Extensions has been upgraded from version 0.02 to 0.03.

- \(bu
Config::Perl::V. has been upgraded from version 0.29 to 0.32. This was due
to a new configuration variable that has influence on binary compatibility:
\f(CW\*(C`USE_THREAD_SAFE_LOCALE\*(C'.

- \(bu
\s-1CPAN\s0 has been upgraded from version 2.20 to 2.22.

- \(bu
Data::Dumper has been upgraded from version 2.170 to 2.174
.Sp
Data::Dumper now avoids leaking when \f(CW\*(C`croak\*(C'ing.

- \(bu
DB_File has been upgraded from version 1.840 to 1.843.

- \(bu
deprecate has been upgraded from version 0.03 to 0.04.

- \(bu
Devel::Peek has been upgraded from version 1.27 to 1.28.

- \(bu
Devel::PPPort has been upgraded from version 3.40 to 3.52.

- \(bu
Digest::SHA has been upgraded from version 6.01 to 6.02.

- \(bu
Encode has been upgraded from version 2.97 to 3.01.

- \(bu
Errno has been upgraded from version 1.29 to 1.30.

- \(bu
experimental has been upgraded from version 0.019 to 0.020.

- \(bu
ExtUtils::CBuilder has been upgraded from version 0.280230 to 0.280231.

- \(bu
ExtUtils::Manifest has been upgraded from version 1.70 to 1.72.

- \(bu
ExtUtils::Miniperl has been upgraded from version 1.08 to 1.09.

- \(bu
ExtUtils::ParseXS has been upgraded from version 3.39 to 3.40.
\f(CW\*(C`OUTLIST\*(C' parameters are no longer incorrectly included in the
automatically generated function prototype.
[\s-1GH\s0 #16746] <https://github.com/Perl/perl5/issues/16746>.

- \(bu
feature has been upgraded from version 1.52 to 1.54.

- \(bu
File::Copy has been upgraded from version 2.33 to 2.34.

- \(bu
File::Find has been upgraded from version 1.34 to 1.36.
.Sp
\f(CW$File::Find::dont_use_nlink now defaults to 1 on all
platforms.
[\s-1GH\s0 #16759] <https://github.com/Perl/perl5/issues/16759>.
.Sp
Variables \f(CW$Is_Win32 and \f(CW$Is_VMS are being initialized.

- \(bu
File::Glob has been upgraded from version 1.31 to 1.32.

- \(bu
File::Path has been upgraded from version 2.15 to 2.16.

- \(bu
File::Spec has been upgraded from version 3.74 to 3.78.
.Sp
Silence Cwd warning on Android builds if \f(CW\*(C`targetsh\*(C' is not defined.

- \(bu
File::Temp has been upgraded from version 0.2304 to 0.2309.

- \(bu
Filter::Util::Call has been upgraded from version 1.58 to 1.59.

- \(bu
GDBM_File has been upgraded from version 1.17 to 1.18.

- \(bu
HTTP::Tiny has been upgraded from version 0.070 to 0.076.

- \(bu
I18N::Langinfo has been upgraded from version 0.17 to 0.18.

- \(bu
\s-1IO\s0 has been upgraded from version 1.39 to 1.40.

- \(bu
IO-Compress has been upgraded from version 2.074 to 2.084.
.Sp
Adds support for \f(CW\*(C`IO::Uncompress::Zstd\*(C' and
\f(CW\*(C`IO::Uncompress::UnLzip\*(C'.
.Sp
The \f(CW\*(C`BinModeIn\*(C' and \f(CW\*(C`BinModeOut\*(C' options are now no-ops.
\s-1ALL\s0 files will be read/written in binmode.

- \(bu
IPC::Cmd has been upgraded from version 1.00 to 1.02.

- \(bu
\s-1JSON::PP\s0 has been upgraded from version 2.97001 to 4.02.
.Sp
\s-1JSON::PP\s0 as \s-1JSON::XS 4.0\s0 enables \f(CW\*(C`allow_nonref\*(C' by default.

- \(bu
lib has been upgraded from version 0.64 to 0.65.

- \(bu
Locale::Codes has been upgraded from version 3.56 to 3.57.

- \(bu
Math::BigInt has been upgraded from version 1.999811 to 1.999816.
.Sp
\f(CW\*(C`bnok()\*(C' now supports the full Kronenburg extension.
[cpan #95628] <https://rt.cpan.org/Ticket/Display.html?id=95628>.

- \(bu
Math::BigInt::FastCalc has been upgraded from version 0.5006 to 0.5008.

- \(bu
Math::BigRat has been upgraded from version 0.2613 to 0.2614.

- \(bu
Module::CoreList has been upgraded from version 5.20180622 to 5.20190520.
.Sp
Changes to B::Op_private and Config

- \(bu
Module::Load has been upgraded from version 0.32 to 0.34.

- \(bu
Module::Metadata has been upgraded from version 1.000033 to 1.000036.
.Sp
Properly clean up temporary directories after testing.

- \(bu
NDBM_File has been upgraded from version 1.14 to 1.15.

- \(bu
Net::Ping has been upgraded from version 2.62 to 2.71.

- \(bu
ODBM_File has been upgraded from version 1.15 to 1.16.

- \(bu
PathTools has been upgraded from version 3.74 to 3.78.

- \(bu
parent has been upgraded from version 0.236 to 0.237.

- \(bu
perl5db.pl has been upgraded from version 1.54 to 1.55.
.Sp
Debugging threaded code no longer deadlocks in \f(CW\*(C`DB::sub\*(C' nor
\f(CW\*(C`DB::lsub\*(C'.

- \(bu
perlfaq has been upgraded from version 5.021011 to 5.20190126.

- \(bu
PerlIO::encoding has been upgraded from version 0.26 to 0.27.
.Sp
Warnings enabled by setting the \f(CW\*(C`WARN_ON_ERR\*(C' flag in
\f(CW$PerlIO::encoding::fallback are now only produced if warnings are
enabled with \f(CW\*(C`use warnings "utf8";\*(C' or setting \f(CW$^W.

- \(bu
PerlIO::scalar has been upgraded from version 0.29 to 0.30.

- \(bu
podlators has been upgraded from version 4.10 to 4.11.

- \(bu
\s-1POSIX\s0 has been upgraded from version 1.84 to 1.88.

- \(bu
re has been upgraded from version 0.36 to 0.37.

- \(bu
SDBM_File has been upgraded from version 1.14 to 1.15.

- \(bu
sigtrap has been upgraded from version 1.08 to 1.09.

- \(bu
Storable has been upgraded from version 3.08 to 3.15.
.Sp
Storable no longer probes for recursion limits at build time.
[\s-1GH\s0 #16780] <https://github.com/Perl/perl5/issues/16780>
and others.
.Sp
Metasploit exploit code was included to test for \s-1CVE-2015-1992\s0
detection, this caused anti-virus detections on at least one \s-1AV\s0 suite.
The exploit code has been removed and replaced with a simple
functional test.
[\s-1GH\s0 #16778] <https://github.com/Perl/perl5/issues/16778>

- \(bu
Test::Simple has been upgraded from version 1.302133 to 1.302162.

- \(bu
Thread::Queue has been upgraded from version 3.12 to 3.13.

- \(bu
threads::shared has been upgraded from version 1.58 to 1.60.
.Sp
Added support for extra tracing of locking, this requires a
\f(CW\*(C`-DDEBUGGING\*(C' and extra compilation flags.

- \(bu
Time::HiRes has been upgraded from version 1.9759 to 1.9760.

- \(bu
Time::Local has been upgraded from version 1.25 to 1.28.

- \(bu
Time::Piece has been upgraded from version 1.3204 to 1.33.

- \(bu
Unicode::Collate has been upgraded from version 1.25 to 1.27.

- \(bu
Unicode::UCD has been upgraded from version 0.70 to 0.72.

- \(bu
User::grent has been upgraded from version 1.02 to 1.03.

- \(bu
utf8 has been upgraded from version 1.21 to 1.22.

- \(bu
vars has been upgraded from version 1.04 to 1.05.
.Sp
\f(CW\*(C`vars.pm\*(C' no longer disables non-vars strict when checking if strict
vars is enabled.
[\s-1GH\s0 #15851] <https://github.com/Perl/perl5/issues/15851>.

- \(bu
version has been upgraded from version 0.9923 to 0.9924.

- \(bu
warnings has been upgraded from version 1.42 to 1.44.

- \(bu
XS::APItest has been upgraded from version 0.98 to 1.00.

- \(bu
XS::Typemap has been upgraded from version 0.16 to 0.17.

### Removed Modules and Pragmata

Subsection "Removed Modules and Pragmata"
The following modules will be removed from the core distribution in a
future release, and will at that time need to be installed from \s-1CPAN.\s0
Distributions on \s-1CPAN\s0 which require these modules will need to list them as
prerequisites.

The core versions of these modules will now issue \f(CW"deprecated"-category
warnings to alert you to this fact.  To silence these deprecation warnings,
install the modules in question from \s-1CPAN.\s0

Note that these are (with rare exceptions) fine modules that you are encouraged
to continue to use.  Their disinclusion from core primarily hinges on their
necessity to bootstrapping a fully functional, CPAN-capable Perl installation,
not usually on concerns over their design.

- \(bu
B::Debug is no longer distributed with the core distribution.  It
continues to be available on \s-1CPAN\s0 as
\f(CW\*(C`B::Debug <https://metacpan.org/pod/B::Debug>\*(C'.

- \(bu
Locale::Codes has been removed at the request of its author.  It
continues to be available on \s-1CPAN\s0 as
\f(CW\*(C`Locale::Codes <https://metacpan.org/pod/Locale::Codes>\*(C'
[\s-1GH\s0 #16660] <https://github.com/Perl/perl5/issues/16660>.

## Documentation

Header "Documentation"

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
We have attempted to update the documentation to reflect the changes
listed in this document.  If you find any we have missed, send email
to perlbug@perl.org <mailto:perlbug@perl.org>.

*perlapi*
Subsection "perlapi"

- \(bu
\f(CW\*(C`AvFILL()\*(C' was wrongly listed as deprecated.  This has been corrected.
[\s-1GH\s0 #16586] <https://github.com/Perl/perl5/issues/16586>

*perlop*
Subsection "perlop"

- \(bu
We no longer have null (empty line) here doc terminators, so
perlop should not refer to them.

- \(bu
The behaviour of \f(CW\*(C`tr\*(C' when the delimiter is an apostrophe has been clarified.
In particular, hyphens aren't special, and \f(CW\*(C`\\x\{\}\*(C' isn't interpolated.
[\s-1GH\s0 #15853] <https://github.com/Perl/perl5/issues/15853>

*perlreapi, perlvar*
Subsection "perlreapi, perlvar"

- \(bu
Improve docs for lastparen, lastcloseparen.

*perlfunc*
Subsection "perlfunc"

- \(bu
The entry for \*(L"-X\*(R" in perlfunc has been clarified to indicate that symbolic
links are followed for most tests.

- \(bu
Clarification of behaviour of \f(CW\*(C`reset EXPR\*(C'.

- \(bu
Try to clarify that \f(CW\*(C`ref(qr/xx/)\*(C' returns \f(CW\*(C`Regexp\*(C' rather than
\f(CW\*(C`REGEXP\*(C' and why.
[\s-1GH\s0 #16801] <https://github.com/Perl/perl5/issues/16801>.

*perlreref*
Subsection "perlreref"

- \(bu
Clarification of the syntax of /(?(cond)yes)/.

*perllocale*
Subsection "perllocale"

- \(bu
There are actually two slightly different types of \s-1UTF-8\s0 locales: one for Turkic
languages and one for everything else. Starting in Perl v5.30, Perl seamlessly
handles both types.

*perlrecharclass*
Subsection "perlrecharclass"

- \(bu
Added a note for the ::xdigit:: character class.

*perlvar*
Subsection "perlvar"

- \(bu
More specific documentation of paragraph mode.
[\s-1GH\s0 #16787] <https://github.com/Perl/perl5/issues/16787>.

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
As noted under \*(L"Incompatible Changes\*(R" above, the deprecation warning
\*(L"Unescaped left brace in regex is deprecated here (and will be fatal in Perl
5.30), passed through in regex; marked by <--\ \s-1HERE\s0 in m/%s/\*(R" has been
changed to the non-deprecation warning \*(L"Unescaped left brace in regex is passed
through in regex; marked by <--\ \s-1HERE\s0 in m/%s/\*(R".

- \(bu
Specifying \f(CW\*(C`\\o\{\}\*(C' without anything between the braces now yields the
fatal error message \*(L"Empty \\o\{\}\*(R".  Previously it was  \*(L"Number with no
digits\*(R".  This means the same wording is used for this kind of error as
with similar constructs such as \f(CW\*(C`\\p\{\}\*(C'.

- \(bu
Within the scope of the experimental feature \f(CW\*(C`use re \*(Aqstrict\*(Aq\*(C',
specifying \f(CW\*(C`\\x\{\}\*(C' without anything between the braces now yields the
fatal error message \*(L"Empty \\x\{\}\*(R".  Previously it was  \*(L"Number with no
digits\*(R".  This means the same wording is used for this kind of error as
with similar constructs such as \f(CW\*(C`\\p\{\}\*(C'.  It is legal, though not wise
to have an empty \f(CW\*(C`\\x\*(C' outside of \f(CW\*(C`re \*(Aqstrict\*(Aq\*(C'; it silently generates
a \s-1NUL\s0 character.

- \(bu
Type of arg \f(CW%d to \f(CW%s must be \f(CW%s (not \f(CW%s)
.Sp
Attempts to push, pop, etc on a hash or glob now produce this message
rather than complaining that they no longer work on scalars.
[\s-1GH\s0 #15774] <https://github.com/Perl/perl5/issues/15774>.

- \(bu
Prototype not terminated
.Sp
The file and line number is now reported for this error.
[\s-1GH\s0 #16697] <https://github.com/Perl/perl5/issues/16697>

- \(bu
Under \f(CW\*(C`-Dr\*(C' (or \f(CW\*(C`use re \*(AqDebug\*(Aq\*(C') the compiled regex engine
program is displayed. It used to use two different spellings for *infinity*,
\f(CW\*(C`INFINITY\*(C', and \f(CW\*(C`INFTY\*(C'. It now uses the latter exclusively,
as that spelling has been around the longest.

## Utility Changes

Header "Utility Changes"

### xsubpp

Subsection "xsubpp"

- \(bu
The generated prototype (with \f(CW\*(C`PROTOTYPES: ENABLE\*(C') would include
\f(CW\*(C`OUTLIST\*(C' parameters, but these aren't arguments to the perl function.
This has been rectified.
[\s-1GH\s0 #16746] <https://github.com/Perl/perl5/issues/16746>.

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
Normally the thread-safe locale functions are used only on threaded
builds.  It is now possible to force their use on unthreaded builds on
systems that have them available, by including the
\f(CW\*(C`-Accflags=\*(Aq-DUSE_THREAD_SAFE_LOCALE\*(Aq\*(C' option to *Configure*.

- \(bu
Improve detection of memrchr, strlcat, and strlcpy

- \(bu
Improve Configure detection of **memmem()**.
[\s-1GH\s0 #16807] <https://github.com/Perl/perl5/issues/16807>.

- \(bu
Multiple improvements and fixes for -DPERL_GLOBAL_STRUCT build option.

- \(bu
Fix -DPERL_GLOBAL_STRUCT_PRIVATE build option.

## Testing

Header "Testing"

- \(bu
*t/lib/croak/op*
[\s-1GH\s0 #15774] <https://github.com/Perl/perl5/issues/15774>.
.Sp
separate error for \f(CW\*(C`push\*(C', etc. on hash/glob.

- \(bu
*t/op/svleak.t*
[\s-1GH\s0 #16749] <https://github.com/Perl/perl5/issues/16749>.
.Sp
Add test for \f(CW\*(C`goto &sub\*(C' in overload leaking.

- \(bu
Split *t/re/fold_grind.t* into multiple test files.

- \(bu
Fix intermittent tests which failed due to race conditions which
surface during parallel testing.
[\s-1GH\s0 #16795] <https://github.com/Perl/perl5/issues/16795>.

- \(bu
Thoroughly test paragraph mode, using a new test file,
*t/io/paragraph_mode.t*.
[\s-1GH\s0 #16787] <https://github.com/Perl/perl5/issues/16787>.

- \(bu
Some tests in *t/io/eintr.t* caused the process to hang on
pre-16 Darwin. These tests are skipped for those version of Darwin.

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- HP-UX 11.11
Item "HP-UX 11.11"
An obscure problem in \f(CW\*(C`pack()\*(C' when compiling with \s-1HP\s0 C-ANSI-C has been fixed
by disabling optimizations in *pp_pack.c*.

- Mac \s-1OS X\s0
Item "Mac OS X"
Perl's build and testing process on Mac \s-1OS X\s0 for \f(CW\*(C`-Duseshrplib\*(C'
builds is now compatible with Mac \s-1OS X\s0 System Integrity Protection
(\s-1SIP\s0).
.Sp
\s-1SIP\s0 prevents binaries in */bin* (and a few other places) being passed
the \f(CW\*(C`DYLD_LIBRARY_PATH\*(C' environment variable.  For our purposes this
prevents \f(CW\*(C`DYLD_LIBRARY_PATH\*(C' from being passed to the shell, which
prevents that variable being passed to the testing or build process,
so running \f(CW\*(C`perl\*(C' couldn't find *libperl.dylib*.
.Sp
To work around that, the initial build of the *perl* executable
expects to find *libperl.dylib* in the build directory, and the
library path is then adjusted during installation to point to the
installed library.
.Sp
[\s-1GH\s0 #15057] <https://github.com/Perl/perl5/issues/15057>.

- Minix3
Item "Minix3"
Some support for Minix3 has been re-added.

- Cygwin
Item "Cygwin"
Cygwin doesn't make \f(CW\*(C`cuserid\*(C' visible.

- Win32 Mingw
Item "Win32 Mingw"
C99 math functions are now available.

- Windows
Item "Windows"

> 0

- \(bu
.PD
The \f(CW\*(C`USE_CPLUSPLUS\*(C' build option which has long been available in
*win32/Makefile* (for **nmake**) and *win32/makefile.mk* (for **dmake**) is now
also available in *win32/GNUmakefile* (for **gmake**).

- \(bu
The **nmake** makefile no longer defaults to Visual \*(C+ 6.0 (a very old version
which is unlikely to be widely used today).  As a result, it is now a
requirement to specify the \f(CW\*(C`CCTYPE\*(C' since there is no obvious choice of which
modern version to default to instead.  Failure to specify \f(CW\*(C`CCTYPE\*(C' will result
in an error being output and the build will stop.
.Sp
(The **dmake** and **gmake** makefiles will automatically detect which compiler
is being used, so do not require \f(CW\*(C`CCTYPE\*(C' to be set.  This feature has not yet
been added to the **nmake** makefile.)

- \(bu
\f(CW\*(C`sleep()\*(C' with warnings enabled for a \f(CW\*(C`USE_IMP_SYS\*(C' build no longer
warns about the sleep timeout being too large.
[\s-1GH\s0 #16631] <https://github.com/Perl/perl5/issues/16631>.

- \(bu
Support for compiling perl on Windows using Microsoft Visual Studio 2019
(containing Visual \*(C+ 14.2) has been added.

- \(bu
**socket()** now sets \f(CW$! if the protocol, address family and socket
type combination is not found.
[\s-1GH\s0 #16849] <https://github.com/Perl/perl5/issues/16849>.

- \(bu
The Windows Server 2003 \s-1SP1\s0 Platform \s-1SDK\s0 build, with its early x64 compiler and
tools, was accidentally broken in Perl 5.27.9.  This has now been fixed.



> 


## Internal Changes

Header "Internal Changes"

- \(bu
The sizing pass has been eliminated from the regular expression
compiler.  An extra pass may instead be needed in some cases to count
the number of parenthetical capture groups.

- \(bu
A new function "\f(CW\*(C`my_strtod\*(C'" in perlapi or its synonym, **Strtod()**, is
now available with the same signature as the libc **strtod()**.  It provides
**strotod()** equivalent behavior on all platforms, using the best available
precision, depending on platform capabilities and *Configure* options,
while handling locale-related issues, such as if the radix character
should be a dot or comma.

- \(bu
Added \f(CW\*(C`newSVsv_nomg()\*(C' to copy a \s-1SV\s0 without processing get magic on
the source.
[\s-1GH\s0 #16461] <https://github.com/Perl/perl5/issues/16461>.

- \(bu
It is now forbidden to malloc more than \f(CW\*(C`PTRDIFF_T_MAX\*(C' bytes.  Much
code (including C optimizers) assumes that all data structures will not
be larger than this, so this catches such attempts before overflow
happens.

- \(bu
Two new regnodes have been introduced \f(CW\*(C`EXACT_ONLY8\*(C', and
\f(CW\*(C`EXACTFU_ONLY8\*(C'. They're equivalent to \f(CW\*(C`EXACT\*(C' and \f(CW\*(C`EXACTFU\*(C',
except that they contain a code point which requires \s-1UTF-8\s0 to
represent/match. Hence, if the target string isn't \s-1UTF-8,\s0 we know
it can't possibly match, without needing to try.

- \(bu
\f(CW\*(C`print_bytes_for_locale()\*(C' is now defined if \f(CW\*(C`DEBUGGING\*(C',
Prior, it didn't get defined unless \f(CW\*(C`LC_COLLATE\*(C' was defined
on the platform.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Compilation under \f(CW\*(C`-DPERL_MEM_LOG\*(C' and \f(CW\*(C`-DNO_LOCALE\*(C' have been fixed.

- \(bu
Perl 5.28 introduced an \f(CW\*(C`index()\*(C' optimization when comparing to -1 (or
indirectly, e.g. >= 0).  When this optimization was triggered inside a \f(CW\*(C`when\*(C'
clause it caused a warning (\*(L"Argument \f(CW%s isn't numeric in smart match\*(R").  This
has now been fixed.
[\s-1GH\s0 #16626] <https://github.com/Perl/perl5/issues/16626>

- \(bu
The new in-place editing code no longer leaks directory handles.
[\s-1GH\s0 #16602] <https://github.com/Perl/perl5/issues/16602>.

- \(bu
Warnings produced from constant folding operations on overloaded
values no longer produce spurious \*(L"Use of uninitialized value\*(R"
warnings.
[\s-1GH\s0 #16349] <https://github.com/Perl/perl5/issues/16349>.

- \(bu
Fix for \*(L"mutator not seen in (lex = ...) .= ...\*(R"
[\s-1GH\s0 #16655] <https://github.com/Perl/perl5/issues/16655>.

- \(bu
\f(CW\*(C`pack "u", "invalid uuencoding"\*(C' now properly \s-1NUL\s0 terminates the
zero-length \s-1SV\s0 produced.
[\s-1GH\s0 #16343] <https://github.com/Perl/perl5/issues/16343>.

- \(bu
Improve the debugging output for **calloc()** calls with \f(CW\*(C`-Dm\*(C'.
[\s-1GH\s0 #16653] <https://github.com/Perl/perl5/issues/16653>.

- \(bu
Regexp script runs were failing to permit \s-1ASCII\s0 digits in some cases.
[\s-1GH\s0 #16704] <https://github.com/Perl/perl5/issues/16704>.

- \(bu
On Unix-like systems supporting a platform-specific technique for
determining \f(CW$^X, Perl failed to fall back to the
generic technique when the platform-specific one fails (for example, a Linux
system with /proc not mounted).  This was a regression in Perl 5.28.0.
[\s-1GH\s0 #16715] <https://github.com/Perl/perl5/issues/16715>.

- \(bu
SDBM_File is now more robust with corrupt database files.  The
improvements do not make \s-1SDBM\s0 files suitable as an interchange format.
[\s-1GH\s0 #16164] <https://github.com/Perl/perl5/issues/16164>.

- \(bu
\f(CW\*(C`binmode($fh);\*(C' or \f(CW\*(C`binmode($fh, \*(Aq:raw\*(Aq);\*(C' now properly removes the
\f(CW\*(C`:utf8\*(C' flag from the default \f(CW\*(C`:crlf\*(C' I/O layer on Win32.
[\s-1GH\s0 #16730] <https://github.com/Perl/perl5/issues/16730>.

- \(bu
The experimental reference aliasing feature was misinterpreting array and
hash slice assignment as being localised, e.g.
.Sp
.Vb 1
    \\(@a[3,5,7]) = \\(....);
.Ve
.Sp
was being interpreted as:
.Sp
.Vb 1
    local \\(@a[3,5,7]) = \\(....);
.Ve
.Sp
[\s-1GH\s0 #16701] <https://github.com/Perl/perl5/issues/16701>.

- \(bu
\f(CW\*(C`sort SUBNAME\*(C' within an \f(CW\*(C`eval EXPR\*(C' when \f(CW\*(C`EXPR\*(C' was \s-1UTF-8\s0 upgraded
could panic if the \f(CW\*(C`SUBNAME\*(C' was non-ASCII.
[\s-1GH\s0 #16979] <https://github.com/Perl/perl5/issues/16979>.

- \(bu
Correctly handle **realloc()** modifying \f(CW\*(C`errno\*(C' on success so that the
modification isn't visible to the perl user, since **realloc()** is called
implicitly by the interpreter.  This modification is permitted by the
C standard, but has only been observed on FreeBSD 13.0-CURRENT.
[\s-1GH\s0 #16907] <https://github.com/Perl/perl5/issues/16907>.

- \(bu
Perl now exposes \s-1POSIX\s0 \f(CW\*(C`getcwd\*(C' as \f(CW\*(C`Internals::getcwd()\*(C' if
available.  This is intended for use by \f(CW\*(C`Cwd.pm\*(C' during bootstrapping
and may be removed or changed without notice.  This fixes some
bootstrapping issues while building perl in a directory where some
ancestor directory isn't readable.
[\s-1GH\s0 #16903] <https://github.com/Perl/perl5/issues/16903>.

- \(bu
\f(CW\*(C`pack()\*(C' no longer can return malformed \s-1UTF-8.\s0  It croaks if it would
otherwise return a \s-1UTF-8\s0 string that contains malformed \s-1UTF-8.\s0  This
protects against potential security threats.
[\s-1GH\s0 #16035] <https://github.com/Perl/perl5/issues/16035>.

- \(bu
See \*(L"Any set of digits in the Common script are legal in a script run
of another script\*(R".

- \(bu
Regular expression matching no longer leaves stale \s-1UTF-8\s0 length magic
when updating \f(CW$^R. This could result in \f(CW\*(C`length($^R)\*(C' returning
an incorrect value.

- \(bu
Reduce recursion on ops
[\s-1GH\s0 #11866] <https://github.com/Perl/perl5/issues/11866>.
.Sp
This can prevent stack overflow when processing extremely deep op
trees.

- \(bu
Avoid leak in multiconcat with overloading.
[\s-1GH\s0 #16823] <https://github.com/Perl/perl5/issues/16823>.

- \(bu
The handling of user-defined \f(CW\*(C`\\p\{\}\*(C' properties (see
\*(L"User-Defined Character Properties\*(R" in perlunicode) has been rewritten to
be in C (instead of Perl).  This speeds things up, but in the process
several inconsistencies and bug fixes are made.

> 
- 1.
A few error messages have minor wording changes.  This is essentially
because the new way is integrated into the regex error handling
mechanism that marks the position in the input at which the error
occurred.  That was not possible previously.  The messages now also
contain additional back-trace-like information in case the error occurs
deep in nested calls.

- 2.
A user-defined property is implemented as a perl subroutine with certain
highly constrained naming conventions.  It was documented previously
that the sub would be in the current package if the package was
unspecified.  This turned out not to be true in all cases, but now it
is.

- 3.
All recursive calls are treated as infinite recursion.  Previously they
would cause the interpreter to panic.  Now, they cause the regex pattern
to fail to compile.

- 4.
Similarly, any other error likely would lead to a panic; now to just the
pattern failing to compile.

- 5.
The old mechanism did not detect illegal ranges in the definition of the
property.  Now, the range max must not be smaller than the range min.
Otherwise, the pattern fails to compile.

- 6.
The intention was to have each sub called only once during the lifetime
of the program, so that a property's definition is immutable.  This was
relaxed so that it could be called once for all /i compilations, and
potentially a second time for non-/i (the sub is passed a parameter
indicating which).  However, in practice there were instances when this
was broken, and multiple calls were possible.  Those have been fixed.
Now (besides the /i,non-/i cases) the only way a sub can be called
multiple times is if some component of it has not been defined yet.  For
example, suppose we have sub **IsA()** whose definition is known at compile
time, and it in turn calls **isB()** whose definition is not yet known.
**isA()** will be called each time a pattern it appears in is compiled.  If
**isA()** also calls **isC()** and that definition is known, **isC()** will be
called just once.

- 7.
There were some races and very long hangs should one thread be compiling
the same property as another simultaneously.  These have now been fixed.



> 


- \(bu
Fixed a failure to match properly.
.Sp
An EXACTFish regnode has a finite length it can hold for the string
being matched.  If that length is exceeded, a second node is used for
the next segment of the string, for as many regnodes as are needed.
Care has to be taken where to break the string, in order to deal
multi-character folds in Unicode correctly. If we want to break a
string at a place which could potentially be in the middle of a
multi-character fold, we back off one (or more) characters, leaving
a shorter EXACTFish regnode. This backing off mechanism contained
an off-by-one error.
[\s-1GH\s0 #16806] <https://github.com/Perl/perl5/issues/16806>.

- \(bu
A bare \f(CW\*(C`eof\*(C' call with no previous file handle now returns true.
[\s-1GH\s0 #16786] <https://github.com/Perl/perl5/issues/16786>

- \(bu
Failing to compile a format now aborts compilation.  Like other errors
in sub-parses this could leave the parser in a strange state, possibly
crashing perl if compilation continued.
[\s-1GH\s0 #16169] <https://github.com/Perl/perl5/issues/16169>

- \(bu
If an in-place edit is still in progress during global destruction and
the process exit code (as stored in \f(CW$?) is zero, perl will now
treat the in-place edit as successful, replacing the input file with
any output produced.
.Sp
This allows code like:
.Sp
.Vb 1
  perl -i -ne \*(Aqprint "Foo"; last\*(Aq
.Ve
.Sp
to replace the input file, while code like:
.Sp
.Vb 1
  perl -i -ne \*(Aqprint "Foo"; die\*(Aq
.Ve
.Sp
will not.  Partly resolves
[\s-1GH\s0 #16748] <https://github.com/Perl/perl5/issues/16748>.

- \(bu
A regression in 5.28 caused the following code to fail
.Sp
.Vb 1
 close(STDIN); open(CHILD, "|wc -l")\*(Aq
.Ve
.Sp
because the child's stdin would be closed on exec. This has now been fixed.

- \(bu
Fixed an issue where compiling a regexp containing both compile-time
and run-time code blocks could lead to trying to compile something
which is invalid syntax.

- \(bu
Fixed build failures with \f(CW\*(C`-DNO_LOCALE_NUMERIC\*(C' and
\f(CW\*(C`-DNO_LOCALE_COLLATE\*(C'.
[\s-1GH\s0 #16771] <https://github.com/Perl/perl5/issues/16771>.

- \(bu
Prevent the tests in *ext/B/t/strict.t* from being skipped.
[\s-1GH\s0 #16783] <https://github.com/Perl/perl5/issues/16783>.

- \(bu
\f(CW\*(C`/di\*(C' nodes ending or beginning in *s* are now \f(CW\*(C`EXACTF\*(C'. We do not
want two \f(CW\*(C`EXACTFU\*(C' to be joined together during optimization,
and to form a \f(CW\*(C`ss\*(C', \f(CW\*(C`sS\*(C', \f(CW\*(C`Ss\*(C' or \f(CW\*(C`SS\*(C' sequence;
they are the only multi-character sequences which may match differently
under \f(CW\*(C`/ui\*(C' and \f(CW\*(C`/di\*(C'.

## Acknowledgements

Header "Acknowledgements"
Perl 5.30.0 represents approximately 11 months of development since Perl
5.28.0 and contains approximately 620,000 lines of changes across 1,300
files from 58 authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 510,000 lines of changes to 750 .pm, .t, .c and .h files.

Perl continues to flourish into its fourth decade thanks to a vibrant
community of users and developers. The following people are known to have
contributed the improvements that became Perl 5.30.0:

Aaron Crane, Abigail, Alberto Simo\*~es, Alexandr Savca, Andreas Ko\*:nig, Andy
Dougherty, Aristotle Pagaltzis, Brian Greenfield, Chad Granum, Chris
'BinGOs' Williams, Craig A. Berry, Dagfinn Ilmari Mannsa\*oker, Dan Book, Dan
Dedrick, Daniel Dragan, Dan Kogai, David Cantrell, David Mitchell, Dominic
Hargreaves, E. Choroba, Ed J, Eugen Konkov, Franc\*,ois Perrad, Graham Knop,
Hauke D, H.Merijn Brand, Hugo van der Sanden, Jakub Wilk, James Clarke,
James E Keenan, Jerry D. Hedden, Jim Cromie, John \s-1SJ\s0 Anderson, Karen
Etheridge, Karl Williamson, Leon Timmermans, Matthias Bethke, Nicholas
Clark, Nicolas R., Niko Tyni, Pali, Petr Pi\*'saX, Phil Pearl (Lobbes),
Richard Leach, Ryan Voots, Sawyer X, Shlomi Fish, Sisyphus, Slaven Rezic,
Steve Hay, Sullivan Beck, Tina Mu\*:ller, Tomasz Konojacki, Tom Wyant, Tony
Cook, Unicode Consortium, Yves Orton, Zak B. Elep.

The list above is almost certainly incomplete as it is automatically
generated from version control history. In particular, it does not include
the names of most of the (very much appreciated) contributors who reported
issues to the Perl bug tracker. Noteworthy in this release were the large
number of bug fixes made possible by Sergey Aleynikov's high quality perlbug
reports for issues he discovered by fuzzing with \s-1AFL.\s0

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core. We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please
see the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database
at <https://rt.perl.org/>.  There may also be information at
<http://www.perl.org/>, the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5,
you can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
