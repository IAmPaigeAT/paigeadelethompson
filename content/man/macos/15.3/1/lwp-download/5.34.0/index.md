+++
date = "2020-04-14"
manpage_name = "lwp-download"
operating_system_version = "15.3"
author = "None Specified"
detected_package_version = "5.34.0"
operating_system = "macos"
description = "The lwp-download program will save the file at url to a local file. If local path is not specified, then the current directory is assumed. If local path is a directory, then the last segment of the path of the url is appended to form a local file..."
title = "lwp-download(1)"
manpage_section = "1"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "LWP-DOWNLOAD 1"
LWP-DOWNLOAD 1 "2020-04-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

lwp-download - Fetch large files from the web

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
 lwp-download [-a] [-s] <url> [<local path>]

 Options:

   -a   save the file in ASCII mode
   -s   use HTTP headers to guess output filename
.Ve

## DESCRIPTION

Header "DESCRIPTION"
The **lwp-download** program will save the file at *url* to a local
file.

If *local path* is not specified, then the current directory is
assumed.

If *local path* is a directory, then the last segment of the path of the
*url* is appended to form a local filename.  If the *url* path ends with
slash the name \*(L"index\*(R" is used.  With the **-s** option pick up the last segment
of the filename from server provided sources like the Content-Disposition
header or any redirect URLs.  A file extension to match the server reported
Content-Type might also be appended.  If a file with the produced filename
already exists, then **lwp-download** will prompt before it overwrites and will
fail if its standard input is not a terminal.  This form of invocation will
also fail is no acceptable filename can be derived from the sources mentioned
above.

If *local path* is not a directory, then it is simply used as the
path to save into.  If the file already exists it's overwritten.

The *lwp-download* program is implemented using the *libwww-perl*
library.  It is better suited to down load big files than the
*lwp-request* program because it does not store the file in memory.
Another benefit is that it will keep you updated about its progress
and that you don't have much options to worry about.

Use the \f(CW\*(C`-a\*(C' option to save the file in text (\s-1ASCII\s0) mode.  Might
make a difference on DOSish systems.

## EXAMPLE

Header "EXAMPLE"
Fetch the newest and greatest perl version:

.Vb 3
 $ lwp-download http://www.perl.com/CPAN/src/latest.tar.gz
 Saving to \*(Aqlatest.tar.gz\*(Aq...
 11.4 MB received in 8 seconds (1.43 MB/sec)
.Ve

## AUTHOR

Header "AUTHOR"
Gisle Aas <gisle@aas.no>
