+++
operating_system = "macos"
description = "xpath uses the XML::XPath perl module to make XPath queries to any s-1XMLs0 document. The XML::XPath module aims to comply exactly to the XPath specification at f(CW*(C`http://www.w3.org/TR/xpath*(C and yet allows extensions to be added in the form..."
detected_package_version = "5.34.0"
operating_system_version = "15.3"
manpage_section = "1"
date = "2017-07-27"
manpage_format = "troff"
keywords = ["header", "see", "also", "xml", "xpath", "license", "and", "copyright", "this", "module", "is", "2000", "axkit", "com", "ltd", "free", "software", "as", "such", "comes", "with", "s-1no", "warranty", "s0", "no", "dates", "are", "used", "in", "you", "may", "distribute", "under", "the", "terms", "of", "either", "gnu", "s-1gpl", "or", "artistic", "same", "perl", "itself", "for", "support", "please", "subscribe", "to", "perl-xml", "http", "listserv", "activestate", "mailman", "listinfo", "mailing", "list", "at", "s-1url"]
manpage_name = "xpath"
author = "None Specified"
title = "xpath(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "XPATH 1"
XPATH 1 "2017-07-27" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

xpath - a script to query XPath statements in XML documents.

## SYNOPSIS

Header "SYNOPSIS"
**xpath [-s suffix] [-p prefix] [-n] [-q] -e query [-e query] ... [file] ...**

## DESCRIPTION

Header "DESCRIPTION"
**xpath** uses the XML::XPath perl module to make XPath queries to any \s-1XML\s0 document.
The XML::XPath module aims to comply exactly to the XPath specification
at \f(CW\*(C`http://www.w3.org/TR/xpath\*(C' and yet allows extensions to be added in the form of
functions.

The script takes any number of XPath pointers and tries to apply them to each \s-1XML\s0 document
given on the command line. If no file arguments are given, the query is done using \f(CW\*(C`STDIN\*(C'
as an \s-1XML\s0 document.

When multiple queries exist, the result of the last query is used as context for the next
query and only the result of the last one is output. The context of the first query is always
the root of the current document.

## OPTIONS

Header "OPTIONS"

### \fB-q

Subsection "-q"
Be quiet. Output only errors (and no separator) on stderr.

### \fB-n

Subsection "-n"
Never use an external \s-1DTD,\s0 ie. instantiate the XML::Parser module with 'ParseParamEnt => 0'.

### \fB-s suffix

Subsection "-s suffix"
Place \f(CW\*(C`suffix\*(C' at the end of each entry. Default is a linefeed.

### \fB-p prefix

Subsection "-p prefix"
Place \f(CW\*(C`prefix\*(C' preceding each entry. Default is nothing.

## BUGS

Header "BUGS"
The author of this man page is not very fluant in english. Please, send him (fabien@tzone.org)
any corrections concerning this text.

## SEE ALSO

Header "SEE ALSO"
XML::XPath

## LICENSE AND COPYRIGHT

Header "LICENSE AND COPYRIGHT"
This module is  copyright  2000 AxKit.com Ltd. This is free software, and as such
comes with \s-1NO WARRANTY.\s0 No dates are used in this module. You may distribute this
module under the terms  of either the Gnu \s-1GPL,\s0  or the Artistic License (the same
terms as Perl itself).

For support, please subscribe to the Perl-XML <http://listserv.activestate.com/mailman/listinfo/perl-xml>
mailing list at the \s-1URL\s0
