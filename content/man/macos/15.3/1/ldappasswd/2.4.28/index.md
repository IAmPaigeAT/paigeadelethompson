+++
manpage_name = "ldappasswd"
manpage_section = "1"
operating_system = "macos"
operating_system_version = "15.3"
date = "2011-01-01"
manpage_format = "troff"
keywords = ["ldap_sasl_bind", "3", "ldap_extended_operation", "ldap_start_tls_s", "author", "the", "openldap", "project", "http", "www", "org", "acknowledgements", "shared", "acknowledgement", "text", "software", "is", "developed", "and", "maintained", "by", "derived", "from", "university", "of", "michigan", "ldap", "release"]
author = "The OpenLDAP Foundation All Rights Reserved."
detected_package_version = "2.4.28"
description = "is a tool to set the password of an LDAP user. uses the LDAPv3 Password Modify (RFC 3062) extended operation. sets the password of associated with the user [or an optionally specified If the new password is not specified on the command line and t..."
title = "ldappasswd(1)"
+++

LDAPPASSWD 1 "2011/11/24" "OpenLDAP 2.4.28"

## NAME

ldappasswd - change the password of an LDAP entry

## SYNOPSIS

ldappasswd
[\c
-A ]
[\c
-a \ oldPasswd]
[\c
-t \ oldpasswdfile]
[\c
-D \ binddn]
[\c
-d \ debuglevel]
[\c
-H \ ldapuri]
[\c
-h \ ldaphost]
[\c
-n ]
[\c
-p \ ldapport]
[\c
-S ]
[\c
-s \ newPasswd]
[\c
-T \ newpasswdfile]
[\c
-v ]
[\c
-W ]
[\c
-w \ passwd]
[\c
-y \ passwdfile]
[\c
-O \ security-properties]
[\c
-I ]
[\c
-Q ]
[\c
-U \ authcid]
[\c
-R \ realm]
[\c
-x ]
[\c
-X \ authzid]
[\c
-Y \ mech]
[\c
-Z [ Z ]]
[\c
user ]

## DESCRIPTION

ldappasswd
is a tool to set the password of an LDAP user.
ldappasswd
uses the LDAPv3 Password Modify (RFC 3062) extended operation.

ldappasswd
sets the password of associated with the user [or an optionally
specified
user ].
If the new
password is not specified on the command line and the user
doesn't enable prompting, the server will be asked to generate
a password for the user.

ldappasswd
is neither designed nor intended to be a replacement for
passwd (1)
and should not be installed as such.

## OPTIONS


-A
Prompt for old password.
This is used instead of specifying the password on the command line.

-a \ oldPasswd
Set the old password to *oldPasswd*.

-t \ oldPasswdFile
Set the old password to the contents of *oldPasswdFile*.

-x
Use simple authentication instead of SASL.

-D \ binddn
Use the Distinguished Name *binddn* to bind to the LDAP directory.
For SASL binds, the server is expected to ignore this value.

-d \ debuglevel
Set the LDAP debugging level to *debuglevel*.
ldappasswd
must be compiled with LDAP_DEBUG defined for this option to have any effect.

-H \ ldapuri
Specify URI(s) referring to the ldap server(s); only the protocol/host/port
fields are allowed; a list of URI, separated by whitespace or commas
is expected.

-h \ ldaphost
Specify an alternate host on which the ldap server is running.
Deprecated in favor of **-H**.

-p \ ldapport
Specify an alternate TCP port where the ldap server is listening.
Deprecated in favor of **-H**.

-n
Do not set password. (Can be useful when used in conjunction with
**-v** or **-d**)

-S
Prompt for new password.
This is used instead of specifying the password on the command line.

-s \ newPasswd
Set the new password to *newPasswd*.

-T \ newPasswdFile
Set the new password to the contents of *newPasswdFile*.

-v
Increase the verbosity of output.  Can be specified multiple times.

-W
Prompt for bind password.
This is used instead of specifying the password on the command line.

-w \ passwd
Use *passwd* as the password to bind with.

-y \ passwdfile
Use complete contents of *passwdfile* as the password for
simple authentication.

-O \ security-properties
Specify SASL security properties.

-I
Enable SASL Interactive mode.  Always prompt.  Default is to prompt
only as needed.

-Q
Enable SASL Quiet mode.  Never prompt.

-U \ authcid
Specify the authentication ID for SASL bind. The form of the ID
depends on the actual SASL mechanism used.

-R \ realm
Specify the realm of authentication ID for SASL bind. The form of the realm
depends on the actual SASL mechanism used.

-X \ authzid
Specify the requested authorization ID for SASL bind.
authzid
must be one of the following formats:
dn: "<distinguished name>"
or
u: <username>.

-Y \ mech
Specify the SASL mechanism to be used for authentication. If it's not
specified, the program will choose the best mechanism the server knows.

-Z [ Z ]
Issue StartTLS (Transport Layer Security) extended operation. If you use
**-ZZ**, the command will require the operation to be successful

## SEE ALSO

ldap_sasl_bind (3),
ldap_extended_operation (3),
ldap_start_tls_s (3)

## AUTHOR

The OpenLDAP Project <http://www.openldap.org/>

## ACKNOWLEDGEMENTS

"OpenLDAP Software"
is developed and maintained by The OpenLDAP Project <http://www.openldap.org/>.
"OpenLDAP Software"
is derived from University of Michigan LDAP 3.3 Release.
