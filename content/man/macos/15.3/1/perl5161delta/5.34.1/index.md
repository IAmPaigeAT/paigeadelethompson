+++
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
operating_system_version = "15.3"
author = "None Specified"
description = "This document describes differences between the 5.16.0 release and the 5.16.1 release. If you are upgrading from an earlier release such as 5.14.0, first read perl5160delta, which describes differences between 5.14.0 and 5.16.0. The bugfix was in..."
operating_system = "macos"
date = "2022-02-19"
detected_package_version = "5.34.1"
manpage_name = "perl5161delta"
title = "perl5161delta(1)"
manpage_section = "1"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5161DELTA 1"
PERL5161DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5161delta - what is new for perl v5.16.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.16.0 release and
the 5.16.1 release.

If you are upgrading from an earlier release such as 5.14.0, first read
perl5160delta, which describes differences between 5.14.0 and
5.16.0.

## Security

Header "Security"

### an off-by-two error in Scalar-List-Util has been fixed

Subsection "an off-by-two error in Scalar-List-Util has been fixed"
The bugfix was in Scalar-List-Util 1.23_04, and perl 5.16.1 includes
Scalar-List-Util 1.25.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.16.0 If any
exist, they are bugs, and we request that you submit a report.  See
\*(L"Reporting Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Scalar::Util and List::Util have been upgraded from version 1.23 to
version 1.25.

- \(bu
B::Deparse has been updated from version 1.14 to 1.14_01.  An
\*(L"uninitialized\*(R" warning emitted by B::Deparse has been squashed
[perl #113464].

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
Building perl with some Windows compilers used to fail due to a problem
with miniperl's \f(CW\*(C`glob\*(C' operator (which uses the \f(CW\*(C`perlglob\*(C' program)
deleting the \s-1PATH\s0 environment variable [perl #113798].

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- \s-1VMS\s0
Item "VMS"
All C header files from the top-level directory of the distribution are now
installed on \s-1VMS,\s0 providing consistency with a long-standing practice on other
platforms. Previously only a subset were installed, which broke non-core extension
builds for extensions that depended on the missing include files.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
A regression introduced in Perl v5.16.0 involving
\f(CW\*(C`tr/\f(CISEARCHLIST\f(CW/\f(CIREPLACEMENTLIST\f(CW/\*(C' has been fixed.  Only the first
instance is supposed to be meaningful if a character appears more than
once in \f(CW\*(C`\f(CISEARCHLIST\f(CW\*(C'.  Under some circumstances, the final instance
was overriding all earlier ones.  [perl #113584]

- \(bu
\f(CW\*(C`B::COP::stashlen\*(C' has been added.   This provides access to an internal
field added in perl 5.16 under threaded builds.  It was broken at the last
minute before 5.16 was released [perl #113034].

- \(bu
The re pragma will no longer clobber \f(CW$_. [perl #113750]

- \(bu
Unicode 6.1 published an incorrect alias for one of the
Canonical_Combining_Class property's values (which range between 0 and
254).  The alias \f(CW\*(C`CCC133\*(C' should have been \f(CW\*(C`CCC132\*(C'.  Perl now
overrides the data file furnished by Unicode to give the correct value.

- \(bu
Duplicating scalar filehandles works again.  [perl #113764]

- \(bu
Under threaded perls, a runtime code block in a regular expression could
corrupt the package name stored in the op tree, resulting in bad reads
in \f(CW\*(C`caller\*(C', and possibly crashes [perl #113060].

- \(bu
For efficiency's sake, many operators and built-in functions return the
same scalar each time.  Lvalue subroutines and subroutines in the \s-1CORE::\s0
namespace were allowing this implementation detail to leak through.
\f(CW\*(C`print &CORE::uc("a"), &CORE::uc("b")\*(C' used to print \*(L"\s-1BB\*(R".\s0  The same thing
would happen with an lvalue subroutine returning the return value of \f(CW\*(C`uc\*(C'.
Now the value is copied in such cases [perl #113044].

- \(bu
\f(CW\*(C`_\|_SUB_\|_\*(C' now works in special blocks (\f(CW\*(C`BEGIN\*(C', \f(CW\*(C`END\*(C', etc.).

- \(bu
Formats that reference lexical variables from outside no longer result
in crashes.

## Known Problems

Header "Known Problems"
There are no new known problems, but consult \*(L"Known
Problems\*(R" in perl5160delta to see those identified in the 5.16.0 release.

## Acknowledgements

Header "Acknowledgements"
Perl 5.16.1 represents approximately 2 months of development since Perl
5.16.0 and contains approximately 14,000 lines of changes across 96
files from 8 authors.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers. The following people are known to
have contributed the improvements that became Perl 5.16.1:

Chris 'BinGOs' Williams, Craig A. Berry, Father Chrysostomos, Karl
Williamson, Paul Johnson, Reini Urban, Ricardo Signes, Tony Cook.

The list above is almost certainly incomplete as it is automatically
generated from version control history. In particular, it does not
include the names of the (very much appreciated) contributors who
reported issues to the Perl bug tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0
modules included in Perl's core. We're grateful to the entire \s-1CPAN\s0
community for helping Perl to flourish.

For a more complete list of all of Perl's historical contributors,
please see the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please
send it to perl5-security-report@perl.org. This points to a closed
subscription unarchived mailing list, which includes all the core
committers, who will be able to help assess the impact of issues, figure
out a resolution, and help co-ordinate the release of patches to
mitigate or fix the problem across all platforms on which Perl is
supported. Please only use this address for security issues in the Perl
core, not for modules independently distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
