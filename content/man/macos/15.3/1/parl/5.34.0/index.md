+++
operating_system_version = "15.3"
keywords = ["header", "see", "also", "s-1par", "s0", "par", "dist", "pl", "pp", "authors", "audrey", "tang", "cpan", "audreyt", "org", "you", "can", "write", "to", "the", "mailing", "list", "at", "perl", "or", "send", "an", "empty", "mail", "par-subscribe", "participate", "in", "discussion", "please", "submit", "bug", "reports", "bug-par-packer", "rt", "copyright", "2002-2009", "by", "neither", "this", "program", "nor", "associated", "impose", "any", "licensing", "restrictions", "on", "files", "generated", "their", "execution", "accordance", "with", "8th", "article", "of", "artistic", "license", "vb", "5", "aggregation", "package", "a", "commercial", "distribution", "is", "always", "permitted", "provided", "that", "use", "embedded", "when", "no", "overt", "attempt", "made", "make", "aqs", "interfaces", "visible", "end", "user", "such", "shall", "not", "be", "construed", "as", "ve", "therefore", "are", "absolutely", "free", "place", "resulting", "executable", "long", "packed", "3rd-party", "libraries", "available", "under", "software", "redistribute", "it", "and", "modify", "same", "terms", "itself", "fi", "s-1license"]
manpage_name = "parl"
description = "This stand-alone command offers roughly the same feature as f(CW*(C`perl -MPAR*(C, except that it takes the pre-loaded .par files via f(CW*(C`-Afoo.par*(C instead of f(CW*(C`-MPAR=foo.par*(C. Additionally, it lets you convert a s-1CPANs0 distrib..."
manpage_format = "troff"
title = "parl(1)"
detected_package_version = "5.34.0"
date = "2020-03-08"
operating_system = "macos"
author = "None Specified"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "PARL 1"
PARL 1 "2020-03-08" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

parl - Binary PAR Loader

## SYNOPSIS

Header "SYNOPSIS"
(Please see pp for convenient ways to make self-contained
executables, scripts or \s-1PAR\s0 archives from perl programs.)

To make a *\s-1PAR\s0 distribution* from a \s-1CPAN\s0 module distribution:

.Vb 2
    % parl -p                 # make a PAR dist under the current path
    % parl -p Foo-0.01        # assume unpacked CPAN dist in Foo-0.01/
.Ve

To manipulate a *\s-1PAR\s0 distribution*:

.Vb 6
    % parl -i Foo-0.01-i386-freebsd-5.8.0.par   # install
    % parl -i http://foo.com/Foo-0.01           # auto-appends archname + perlver
    % parl -i cpan://AUTRIJUS/PAR-0.74          # uses CPAN author directory
    % parl -u Foo-0.01-i386-freebsd-5.8.0.par   # uninstall
    % parl -s Foo-0.01-i386-freebsd-5.8.0.par   # sign
    % parl -v Foo-0.01-i386-freebsd-5.8.0.par   # verify
.Ve

To use *Hello.pm* from *./foo.par*:

.Vb 2
    % parl -A./foo.par -MHello
    % parl -A./foo -MHello      # the .par part is optional
.Ve

Same thing, but search *foo.par* in the *\f(CI@INC\fI*;

.Vb 2
    % parl -Ifoo.par -MHello
    % parl -Ifoo -MHello        # ditto
.Ve

Run *test.pl* or *script/test.pl* from *foo.par*:

.Vb 2
    % parl foo.par test.pl      # looks for \*(Aqmain.pl\*(Aq by default,
                                # otherwise run \*(Aqtest.pl\*(Aq
.Ve

To make a self-containing executable containing a \s-1PAR\s0 file :

.Vb 2
    % parl -O./foo foo.par
    % ./foo test.pl             # same as above
.Ve

To embed the necessary non-core modules and shared objects for \s-1PAR\s0's
execution (like \f(CW\*(C`Zlib\*(C', \f(CW\*(C`IO\*(C', \f(CW\*(C`Cwd\*(C', etc), use the **-b** flag:

.Vb 2
    % parl -b -O./foo foo.par
    % ./foo test.pl             # runs anywhere with core modules installed
.Ve

If you also wish to embed *core* modules along, use the **-B** flag
instead:

.Vb 2
    % parl -B -O./foo foo.par
    % ./foo test.pl             # runs anywhere with the perl interpreter
.Ve

This is particularly useful when making stand-alone binary
executables; see pp for details.

## DESCRIPTION

Header "DESCRIPTION"
This stand-alone command offers roughly the same feature as \f(CW\*(C`perl
-MPAR\*(C', except that it takes the pre-loaded *.par* files via
\f(CW\*(C`-Afoo.par\*(C' instead of \f(CW\*(C`-MPAR=foo.par\*(C'.

Additionally, it lets you convert a \s-1CPAN\s0 distribution to a \s-1PAR\s0
distribution, as well as manipulate such distributions.  For more
information about \s-1PAR\s0 distributions, see PAR::Dist.

You can use it to run *.par* files:

.Vb 3
    # runs script/run.pl in archive, uses its lib/* as libraries
    % parl myapp.par run.pl     # runs run.pl or script/run.pl in myapp.par
    % parl otherapp.pl          # also runs normal perl scripts
.Ve

However, if the *.par* archive contains either *main.pl* or
*script/main.pl*, it is used instead:

.Vb 1
    % parl myapp.par run.pl     # runs main.pl, with \*(Aqrun.pl\*(Aq as @ARGV
.Ve

Finally, the \f(CW\*(C`-O\*(C' option makes a stand-alone binary executable from a
\s-1PAR\s0 file:

.Vb 2
    % parl -B -Omyapp myapp.par
    % ./myapp                   # run it anywhere without perl binaries
.Ve

With the \f(CW\*(C`--par-options\*(C' flag, generated binaries can act as \f(CW\*(C`parl\*(C'
to pack new binaries:

.Vb 2
    % ./myapp --par-options -Omyap2 myapp.par   # identical to ./myapp
    % ./myapp --par-options -Omyap3 myap3.par   # now with different PAR
.Ve

For an explanation of stand-alone executable format, please see par.pl.

## SEE ALSO

Header "SEE ALSO"
\s-1PAR\s0, PAR::Dist, par.pl, pp

## AUTHORS

Header "AUTHORS"
Audrey Tang <cpan@audreyt.org>

You can write
to the mailing list at <par@perl.org>, or send an empty mail to
<par-subscribe@perl.org> to participate in the discussion.

Please submit bug reports to <bug-par-packer@rt.cpan.org>.

## COPYRIGHT

Header "COPYRIGHT"
Copyright 2002-2009 by Audrey Tang
<cpan@audreyt.org>.

Neither this program nor the associated pp program impose any
licensing restrictions on files generated by their execution, in
accordance with the 8th article of the Artistic License:

.Vb 5
    "Aggregation of this Package with a commercial distribution is
    always permitted provided that the use of this Package is embedded;
    that is, when no overt attempt is made to make this Package\*(Aqs
    interfaces visible to the end user of the commercial distribution.
    Such use shall not be construed as a distribution of this Package."
.Ve

Therefore, you are absolutely free to place any license on the resulting
executable, as long as the packed 3rd-party libraries are also available
under the Artistic License.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See *\s-1LICENSE\s0*.
