+++
detected_package_version = "5.34.1"
operating_system = "macos"
title = "perlhurd(1)"
description = "If you want to use Perl on the Hurd, I recommend using the Debian GNU/Hurd distribution ( see <https://www.debian.org/> ), even if an official, stable release has not yet been made.  The old *(Lgnu-0.2*(R binary distribution will most certainly h..."
author = "None Specified"
operating_system_version = "15.3"
manpage_format = "troff"
manpage_name = "perlhurd"
date = "2022-02-19"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLHURD 1"
PERLHURD 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlhurd - Perl version 5 on Hurd

## DESCRIPTION

Header "DESCRIPTION"
If you want to use Perl on the Hurd, I recommend using the Debian
GNU/Hurd distribution ( see <https://www.debian.org/> ), even if an
official, stable release has not yet been made.  The old \*(L"gnu-0.2\*(R"
binary distribution will most certainly have additional problems.

### Known Problems with Perl on Hurd

Subsection "Known Problems with Perl on Hurd"
The Perl test suite may still report some errors on the Hurd.  The
\*(L"lib/anydbm\*(R" and \*(L"pragma/warnings\*(R" tests will almost certainly fail.
Both failures are not really specific to the Hurd, as indicated by the
test suite output.

The socket tests may fail if the network is not configured.  You have
to make \*(L"/hurd/pfinet\*(R" the translator for \*(L"/servers/socket/2\*(R", giving
it the right arguments.  Try \*(L"/hurd/pfinet --help\*(R" for more
information.

Here are the statistics for Perl 5.005_62 on my system:

.Vb 4
 Failed Test  Status Wstat Total Fail  Failed  List of failed
 -----------------------------------------------------------------------
 lib/anydbm.t                 12    1   8.33%  12
 pragma/warnings             333    1   0.30%  215

 8 tests and 24 subtests skipped.
 Failed 2/229 test scripts, 99.13% okay. 2/10850 subtests failed,
     99.98% okay.
.Ve

There are quite a few systems out there that do worse!

However, since I am running a very recent Hurd snapshot, in which a lot of
bugs that were exposed by the Perl test suite have been fixed, you may
encounter more failures.  Likely candidates are: \*(L"op/stat\*(R", \*(L"lib/io_pipe\*(R",
\*(L"lib/io_sock\*(R", \*(L"lib/io_udp\*(R" and \*(L"lib/time\*(R".

In any way, if you're seeing failures beyond those mentioned in this
document, please consider upgrading to the latest Hurd before reporting
the failure as a bug.

## AUTHOR

Header "AUTHOR"
Mark Kettenis <kettenis@gnu.org>

Last Updated: Fri, 29 Oct 1999 22:50:30 +0200
