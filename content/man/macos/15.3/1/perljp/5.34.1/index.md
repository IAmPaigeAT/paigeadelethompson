+++
description = "None Specified"
date = "2022-02-19"
title = "perljp(1)"
operating_system_version = "15.3"
manpage_name = "perljp"
operating_system = "macos"
detected_package_version = "5.34.1"
manpage_section = "1"
author = "None Specified"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLJP 1"
PERLJP 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perljp - XXX Perl XXX

## XX

Header "XX"
Perl \s-1XXXXXXXX\s0!

Perl 5.8.0 XXXUnicodeXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX \s-1CJK\s0 (\s-1XXXXXXXXXXXX\s0)XXXXXXXXXXXUnicodeXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXOSXXXXXXXXXXXXXXX(PCXXXMacintosh)XXXXXXXXXXXX

Perl \s-1XXX\s0 Unicode XXXXXXXPerl \s-1XXXXXXXXXXXXXXXXXXXX\s0 Unicode \s-1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\s0 Encode XXXXXXXXXXXXUnicode \s-1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\s0

\s-1XXXX\s0 Encode \s-1XXXXXXXXXXXXXXXXXXXXXX\s0

.Vb 10
  7bit-jis      AdobeStandardEncoding AdobeSymbol       AdobeZdingbat
  ascii             big5              big5-hkscs        cp1006
  cp1026            cp1047            cp1250            cp1251
  cp1252            cp1253            cp1254            cp1255
  cp1256            cp1257            cp1258            cp37
  cp424             cp437             cp500             cp737
  cp775             cp850             cp852             cp855
  cp856             cp857             cp860             cp861
  cp862             cp863             cp864             cp865
  cp866             cp869             cp874             cp875
  cp932             cp936             cp949             cp950
  dingbats          euc-cn            euc-jp            euc-kr
  gb12345-raw       gb2312-raw        gsm0338           hp-roman8
  hz                iso-2022-jp       iso-2022-jp-1     iso-8859-1
  iso-8859-10       iso-8859-11       iso-8859-13       iso-8859-14
  iso-8859-15       iso-8859-16       iso-8859-2        iso-8859-3
  iso-8859-4        iso-8859-5        iso-8859-6        iso-8859-7
  iso-8859-8        iso-8859-9        iso-ir-165        jis0201-raw
  jis0208-raw       jis0212-raw       johab             koi8-f
  koi8-r            koi8-u            ksc5601-raw       MacArabic
  MacCentralEurRoman  MacChineseSimp    MacChineseTrad    MacCroatian
  MacCyrillic       MacDingbats       MacFarsi          MacGreek
  MacHebrew         MacIcelandic      MacJapanese       MacKorean
  MacRoman          MacRomanian       MacRumanian       MacSami
  MacSymbol         MacThai           MacTurkish        MacUkrainian
  nextstep          posix-bc          shiftjis          symbol
  UCS-2BE           UCS-2LE           UTF-16            UTF-16BE
  UTF-16LE          UTF-32            UTF-32BE          UTF-32LE
  utf8              viscii
.Ve

(X114XX)

\s-1XXXXXXXXXFOOXXXXXXUTF-8XXXXXXXXXXXXXXXXXX\s0

.Vb 1
    perl -Mencoding=FOO,STDOUT,utf8 -pe1 < file.FOO > file.utf8
.Ve

XXXPerlXXXXXXPerlXXXXXXXXXXXXXXXXXXXXpiconvXXXXXXXXXXXXXXXXXXXXXXXXXX

.Vb 2
   piconv -f FOO -t utf8 < file.FOO > file.utf8
   piconv -f utf8 -t FOO < file.utf8 > file.FOO
.Ve

### (jcode.pl|Jcode.pm|JPerl) \s-1XXXXX\s0

Subsection "(jcode.pl|Jcode.pm|JPerl) XXXXX"
5.8XXXXXXXXXXEUC-JPXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXJcode.pmX( <http://openlab.ring.gr.jp/Jcode/> )Xperl4XXXXXXXXXXXXXjcode.plXXXXXXXXXXXXXXXXCGIXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

5.005XXXPerlXXXXXXXXXXXXXXXXXXXJperlXXXXXXX( <http://homepage2.nifty.com/kipp/perl/jperl/index.html> X1)XXXXMac \s-1OS 9\s0.x/ClassicXXPerlXMacPerlXXXXXXMacJPerlXXXXXXXXXXX( <https://habilis.net/macjperl/> ).XXXXXXXXXXXXXEUC-JPXXXShift_JISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Perl5.8XXXXXXXXXXXXXPerlXXXXXXXXXXXXXXXXXXXXXXX114XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXCPANXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

X1: \s-1XXXXXXXXXXXXXXXXXXXXXXXXXXX\s0 Vector( <https://www.vector.co.jp/soft/win95/util/se098198.html> )XXWindowXXXXXXXXCPAN( <https://www.cpan.org/src/unsupported/4.036/jperl/> )XXperl4XXXXXXXXXXXXXXXXXX

- \(bu
\s-1XXX\s0
.Sp
XXXXXXXXXShift_JISXXXXEUC-JPXXXXXXXXXXX
.Sp
.Vb 10
  # jcode.pl
  require "jcode.pl";
  while(<>)\{
    jcode::convert(*_, \*(Aqeuc\*(Aq, \*(Aqsjis\*(Aq);
    print;
  \}
  # Jcode.pm
  use Jcode;
  while(<>)\{
        print Jcode->new($_, \*(Aqsjis\*(Aq)->euc;
  \}
  # Perl 5.8
  use Encode;
  while(<>)\{
    from_to($_, \*(Aqshiftjis\*(Aq, \*(Aqeuc-jp\*(Aq);
    print;
  \}
  # Perl 5.8 - encoding XXXXX
  use encoding \*(Aqeuc-jp\*(Aq, STDIN => \*(Aqshiftjis\*(Aq;
  while(<>)\{
        print;
  \}
.Ve

- \(bu
Jperl \s-1XXXXXXX\s0
.Sp
\s-1XXXX\s0\*(L"shebang\*(R"XXXXXXXXXJperlXXscriptXXXXXXXXXXXXXXXXXXXXXXX
.Sp
.Vb 3
   #!/path/to/jperl
   X
   #!/path/to/perl -Mencoding=euc-jp
.Ve
.Sp
\s-1XXXX\s0 perldoc encoding \s-1XXXXXXXXXX\s0

### \s-1XXXXXX\s0

Subsection "XXXXXX"
PerlXXXXXXXXXXXXXXXPerlXXXXXUnicodeXXXXXXXXEncodeXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

.Vb 3
  perldoc perlunicode # PerlXUnicodeXXXXXX
  perldoc Encode      # EncodeXXXXXXXXX
  perldoc Encode::JP  # XXXXXXXXXXXXXX
.Ve

### PerlXXXXXX \s-1URL\s0

Subsection "PerlXXXXXX URL"

- <https://www.perl.org/>
Item "<https://www.perl.org/>"
Perl \s-1XXXXXX\s0

- <https://www.perl.com/>
Item "<https://www.perl.com/>"
Perl \s-1XXXXXXXXXXXX\s0

- <https://www.cpan.org/>
Item "<https://www.cpan.org/>"
\s-1CPAN\s0 (Comprehensive Perl Archive Network)

- <https://metacpan.org/>
Item "<https://metacpan.org/>"
MetaCPAN \s-1CPANXXXXXXX\s0

- <https://lists.perl.org/>
Item "<https://lists.perl.org/>"
Perl \s-1XXXXXXXXX\s0

- <https://perldoc.jp/>
Item "<https://perldoc.jp/>"
perldoc.jp Perl \s-1XXXXXXXXXXXXXXXXXXXXXXXXXX\s0

### PerlXXXXXXX \s-1URL\s0

Subsection "PerlXXXXXXX URL"

- <http://www.oreilly.com.tw/>
Item "<http://www.oreilly.com.tw/>"
O'Reilly XXPerlXXXX(\s-1XXXXXX\s0)

- <http://www.oreilly.com.cn/>
Item "<http://www.oreilly.com.cn/>"
O'Reilly XXPerlXXXX(\s-1XXXXXX\s0)

- <https://www.oreilly.co.jp/catalog/>
Item "<https://www.oreilly.co.jp/catalog/>"
XXXXXXXPerlXXXX(\s-1XXX\s0)

### Perl \s-1XXXXXX\s0

Subsection "Perl XXXXXX"

- <https://www.pm.org/groups/asia.html>
Item "<https://www.pm.org/groups/asia.html>"
\s-1XXXXXX\s0 Perl Mongers (PerlXXXXXXXXX) \s-1XX\s0

- <https://japan.perlassociation.org>
Item "<https://japan.perlassociation.org>"
XXXXXXJapan Perl Association (\s-1JPA\s0) PerlXXXXXXXXXXXXXXXXXX

### UnicodeXXXURL

Subsection "UnicodeXXXURL"

- <https://www.unicode.org/>
Item "<https://www.unicode.org/>"
Unicode \s-1XXXXXXX\s0 (UnicodeXXXXXXX)

- <https://www.cl.cam.ac.uk/%7Emgk25/unicode.html>
Item "<https://www.cl.cam.ac.uk/%7Emgk25/unicode.html>"
\s-1UTF-8\s0 and Unicode \s-1FAQ\s0 for Unix/Linux

- <https://wiki.kldp.org/Translations/html/UTF8-Unicode-KLDP/UTF8-Unicode-KLDP.html>
Item "<https://wiki.kldp.org/Translations/html/UTF8-Unicode-KLDP/UTF8-Unicode-KLDP.html>"
\s-1UTF-8\s0 and Unicode \s-1FAQ\s0 for Unix/Linux (\s-1XXXXX\s0)

## AUTHORS

Header "AUTHORS"

- \(bu
Jarkko Hietaniemi <jhi@iki.fi>

- \(bu
Dan Kogai (\s-1XXXX\s0) <dankogai@dan.co.jp>

- \(bu
Shogo Ichinose (\s-1XXXXXX\s0) <shogo82148@gmail.com>
