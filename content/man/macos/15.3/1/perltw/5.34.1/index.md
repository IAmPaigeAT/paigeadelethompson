+++
operating_system_version = "15.3"
date = "2022-02-19"
author = "None Specified"
detected_package_version = "5.34.1"
manpage_format = "troff"
manpage_name = "perltw"
description = "s-1XXXXs0 Perl s-1XXXs0! X 5.8.0 s-1XXX,s0 Perl s-1XXXXXXs0 Unicode (s-1XXXs0) s-1XX, XXXXXXXXXXXXXXXXXXXs0; s-1CJKs0 (s-1XXXs0) s-1XXXXXXXX.s0 Unicode s-1XXXXXXX, XXXXXXXXXXXX: XXXX, XXXX, XXXXXXXXs0 (s-1XXX, XXXX, XXXX, XXXX, XXX, XXXX, XXs0). ..."
operating_system = "macos"
keywords = ["header", "see", "also", "encode", "tw", "perluniintro", "perlunicode", "authors", "jarkko", "hietaniemi", "jhi", "iki", "fi", "audrey", "tang", "s-1xx", "s0", "audreyt", "org"]
title = "perltw(1)"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLTW 1"
PERLTW 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"
\s-1XXXXXXXXXXXXXXXXXX, XXXXXXXXXXXX.
XXXXXX POD\s0 (\s-1XXXXXX\s0) \s-1XX\s0; \s-1XXXXXXXXXXXXXX,
XXXXXX. XXXXXXXXXXX, XXX\s0 perlpod \s-1XXXX.\s0

## NAME

perltw - XXXX Perl XX

## DESCRIPTION

Header "DESCRIPTION"
\s-1XXXX\s0 Perl \s-1XXX\s0!

X 5.8.0 \s-1XXX,\s0 Perl \s-1XXXXXX\s0 Unicode (\s-1XXX\s0) \s-1XX,
XXXXXXXXXXXXXXXXXXX\s0; \s-1CJK\s0 (\s-1XXX\s0) \s-1XXXXXXXX.\s0
Unicode \s-1XXXXXXX, XXXXXXXXXXXX: XXXX, XXXX,
XXXXXXXX\s0 (\s-1XXX, XXXX, XXXX, XXXX, XXX,
XXXX, XX\s0). \s-1XXXXXXXXXXXXXX\s0 (X \s-1PC XXXX\s0).

Perl \s-1XXX\s0 Unicode \s-1XXXX. XXX\s0 Perl \s-1XXXXXXXXX\s0 Unicode
\s-1XX\s0; Perl \s-1XXXXXX\s0 (\s-1XXXXXXXXX\s0) \s-1XXX\s0 Unicode \s-1XXXX.
XXXXXXX, XXXXX\s0 Unicode \s-1XXXXXXXXXXXX,\s0 Perl
\s-1XXX\s0 Encode \s-1XXXX, XXXXXXXXXXXXXXXXXXX.\s0

Encode \s-1XXXXXXXXXXXXXXXXX\s0 ('big5' \s-1XX\s0 'big5-eten'):

.Vb 3
    big5-eten   Big5 XX (XXXXXXX)
    big5-hkscs  Big5 + XXXXX, 2001 XX
    cp950       XXX 950 (Big5 + XXXXXXX)
.Ve

\s-1XXXX, X\s0 Big5 \s-1XXXXXXX\s0 Unicode, \s-1XXXXXXXX:\s0

.Vb 2
    perl -MEncode -pe \*(Aq$_= encode( utf8 => decode( big5 => $_ ) )\*(Aq \\
      < file.big5 > file.utf8
.Ve

Perl \s-1XXXX\s0 \*(L"piconv\*(R", \s-1XXXXX\s0 Perl \s-1XXXXXXXXXXX, XXXX:\s0

.Vb 2
    piconv -f big5 -t utf8 < file.big5 > file.utf8
    piconv -f utf8 -t big5 < file.utf8 > file.big5
.Ve

\s-1XXXXXXXXXX\s0 utf8 \s-1XXXXXXXXX\s0 utf8 \s-1XXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXX\s0

.Vb 4
    #!/usr/bin/env perl
    use utf8;
    print length("XX");      #  2 (XX 6)
    print index("XXXX", "XX"); #  2 (X 0 XXX 2 XXX)
.Ve

### \s-1XXXXXXX\s0

Subsection "XXXXXXX"
\s-1XXXXXXXXXXX, XXX CPAN\s0 (<https://www.cpan.org/>) \s-1XX\s0
Encode::HanExtra \s-1XX. XXXXXXXXXXX:\s0

.Vb 4
    cccii       1980 XXXXXXXXXXXX
    euc-tw      Unix XXXXX, XX CNS11643 XX 1-7
    big5plus    XXXXXXXXXXXXX Big5+
    big5ext     XXXXXXXXXXXXX Big5e
.Ve

\s-1XX,\s0 Encode::HanConvert \s-1XXXXXXXXXXXXXXXX:\s0

.Vb 2
    big5-simp   Big5 XXXXX Unicode XXXXXX
    gbk-trad    GBK XXXXX Unicode XXXXXX
.Ve

\s-1XXX GBK X\s0 Big5 \s-1XXXX, XXXXXXXXX\s0 b2g.pl X g2b.pl \s-1XXXX,
XXXXXXXXXXX:\s0

.Vb 3
    use Encode::HanConvert;
    $euc_cn = big5_to_gb($big5); # X Big5 XX GBK
    $big5 = gb_to_big5($euc_cn); # X GBK XX Big5
.Ve

### \s-1XXXXXX\s0

Subsection "XXXXXX"
\s-1XXX\s0 Perl \s-1XXXXXXXXX\s0 (\s-1XXXXXXXXX\s0), \s-1XXXXXXX\s0
Perl \s-1XXX, XX\s0 Unicode \s-1XXXXX. XX, XXXXXXXXX:\s0

### \s-1XX\s0 Perl \s-1XXXXX\s0

Subsection "XX Perl XXXXX"

- <https://www.perl.org/>
Item "<https://www.perl.org/>"
Perl \s-1XXX\s0

- <https://www.perl.com/>
Item "<https://www.perl.com/>"
X Perl \s-1XXXXXXXXXXX\s0

- <https://www.cpan.org/>
Item "<https://www.cpan.org/>"
Perl \s-1XXXXX\s0 (Comprehensive Perl Archive Network)

- <https://lists.perl.org/>
Item "<https://lists.perl.org/>"
Perl \s-1XXXXXX\s0

### \s-1XX\s0 Perl \s-1XXX\s0

Subsection "XX Perl XXX"

- <http://www.oreilly.com.tw/product_perl.php?id=index_perl>
Item "<http://www.oreilly.com.tw/product_perl.php?id=index_perl>"
\s-1XXXXXXXXX\s0 Perl \s-1XX\s0

### Perl \s-1XXXXX\s0

Subsection "Perl XXXXX"

- <https://www.pm.org/groups/taiwan.html>
Item "<https://www.pm.org/groups/taiwan.html>"
\s-1XX\s0 Perl \s-1XXXXX\s0

- <irc://chat.freenode.org/#perl.tw>
Item "<irc://chat.freenode.org/#perl.tw>"
Perl.tw \s-1XXXXX\s0

### Unicode \s-1XXXX\s0

Subsection "Unicode XXXX"

- <https://www.unicode.org/>
Item "<https://www.unicode.org/>"
Unicode \s-1XXXX\s0 (Unicode \s-1XXXXXX\s0)

- <http://www.cl.cam.ac.uk/%7Emgk25/unicode.html>
Item "<http://www.cl.cam.ac.uk/%7Emgk25/unicode.html>"
Unix/Linux \s-1XX UTF-8 X\s0 Unicode \s-1XXX\s0

### \s-1XXXXX\s0

Subsection "XXXXX"

- \s-1XXXXXXX\s0
Item "XXXXXXX"
<http://www.cpatch.org/>

## SEE ALSO

Header "SEE ALSO"
Encode, Encode::TW, perluniintro, perlunicode

## AUTHORS

Header "AUTHORS"
Jarkko Hietaniemi <jhi@iki.fi>

Audrey Tang (\s-1XX\s0) <audreyt@audreyt.org>
