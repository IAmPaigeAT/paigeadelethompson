+++
date = "2022-02-19"
operating_system = "macos"
detected_package_version = "5.34.1"
manpage_name = "perlexperiment"
title = "perlexperiment(1)"
description = "This document lists the current and past experimental features in the perl core. Although all of these are documented with their appropriate topics, this succinct listing gives you an overview and basic facts about their status. So far weve merel..."
author = "None Specified"
manpage_format = "troff"
manpage_section = "1"
keywords = ["header", "see", "also", "for", "a", "complete", "list", "of", "features", "check", "feature", "authors", "brian", "d", "foy", "f", "cw", "c", "gmail", "com", "se", "bastien", "aperghis-tramoni", "saper", "cpan", "org", "copyright", "2010", "license", "you", "can", "use", "and", "redistribute", "this", "document", "under", "the", "same", "terms", "as", "perl", "itself"]
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLEXPERIMENT 1"
PERLEXPERIMENT 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlexperiment - A listing of experimental features in Perl

## DESCRIPTION

Header "DESCRIPTION"
This document lists the current and past experimental features in the perl
core. Although all of these are documented with their appropriate topics,
this succinct listing gives you an overview and basic facts about their
status.

So far we've merely tried to find and list the experimental features and infer
their inception, versions, etc. There's a lot of speculation here.

### Current experiments

Subsection "Current experiments"
.ie n .IP "Smart match (""~~"")" 8
.el .IP "Smart match (\f(CW~~)" 8
Item "Smart match (~~)"
Introduced in Perl 5.10.0
.Sp
Modified in Perl 5.10.1, 5.12.0
.Sp
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::smartmatch\*(C'.
.Sp
The ticket for this experiment is
[perl #13173] <https://github.com/Perl/perl5/issues/13173>.

- Pluggable keywords
Item "Pluggable keywords"
Introduced in Perl 5.11.2
.Sp
See \*(L"PL_keyword_plugin\*(R" in perlapi for the mechanism.
.Sp
The ticket for this experiment is
[perl #13199] <https://github.com/Perl/perl5/issues/13199>.

- Regular Expression Set Operations
Item "Regular Expression Set Operations"
Introduced in Perl 5.18
.Sp
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::regex_sets\*(C'.
.Sp
The ticket for this experiment is
[perl #13197] <https://github.com/Perl/perl5/issues/13197>.
.Sp
See also: \*(L"Extended Bracketed Character Classes\*(R" in perlrecharclass

- Subroutine signatures
Item "Subroutine signatures"
Introduced in Perl 5.20.0
.Sp
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::signatures\*(C'.
.Sp
The ticket for this experiment is
[perl #13681] <https://github.com/Perl/perl5/issues/13681>.

- Aliasing via reference
Item "Aliasing via reference"
Introduced in Perl 5.22.0
.Sp
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::refaliasing\*(C'.
.Sp
The ticket for this experiment is
[perl #14150] <https://github.com/Perl/perl5/issues/14150>.
.Sp
See also: \*(L"Assigning to References\*(R" in perlref
.ie n .IP "The ""const"" attribute" 8
.el .IP "The ``const'' attribute" 8
Item "The const attribute"
Introduced in Perl 5.22.0
.Sp
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::const_attr\*(C'.
.Sp
The ticket for this experiment is
[perl #14428] <https://github.com/Perl/perl5/issues/14428>.
.Sp
See also: \*(L"Constant Functions\*(R" in perlsub

- use re 'strict';
Item "use re 'strict';"
Introduced in Perl 5.22.0
.Sp
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::re_strict\*(C'.
.Sp
The ticket for this experiment is
[perl #18755] <https://github.com/Perl/perl5/issues/18755>
.Sp
See \*(L"'strict' mode\*(R" in re

- The <:win32> \s-1IO\s0 pseudolayer
Item "The <:win32> IO pseudolayer"
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::win32_perlio\*(C'.
.Sp
The ticket for this experiment is
[perl #13198] <https://github.com/Perl/perl5/issues/13198>.
.Sp
See also \*(L"\s-1PERLIO\*(R"\s0 in perlrun

- Declaring a reference to a variable
Item "Declaring a reference to a variable"
Introduced in Perl 5.26.0
.Sp
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::declared_refs\*(C'.
.Sp
The ticket for this experiment is
[perl #15458] <https://github.com/Perl/perl5/issues/15458>.
.Sp
See also: \*(L"Declaring a Reference to a Variable\*(R" in perlref
.ie n .IP "There is an ""installhtml"" target in the Makefile." 8
.el .IP "There is an \f(CWinstallhtml target in the Makefile." 8
Item "There is an installhtml target in the Makefile."
The ticket for this experiment is
[perl #12726] <https://github.com/Perl/perl5/issues/12726>.

- (Limited) Variable-length look-behind
Item "(Limited) Variable-length look-behind"
Introduced in Perl 5.30.0.
.Sp
Variability of up to 255 characters is handled.
.Sp
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::vlb\*(C'.
.Sp
The ticket for this experiment is
[perl #18756] <https://github.com/Perl/perl5/issues/18756>.
.Sp
See also: "(*positive_lookbehind:*pattern*)" in perlre and
"(*negative_lookbehind:*pattern*)" in perlre

- Unicode private use character hooks
Item "Unicode private use character hooks"
Introduced in Perl 5.30.0.
.Sp
This feature is part of an interface intended for internal and experimental
use by the perl5 developers.  You are unlikely to encounter it in the wild.
.Sp
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::private_use\*(C'.
.Sp
The ticket for this experiment is
[perl #18758] <https://github.com/Perl/perl5/issues/18758>.

- Unicode property wildcards
Item "Unicode property wildcards"
Introduced in Perl 5.30.0.
.Sp
This feature allows regular expression matching against Unicode character
properties to be expressed more concisely.
.Sp
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::uniprop_wildcards\*(C'.
.Sp
The ticket for this experiment is
[perl #18759] <https://github.com/Perl/perl5/issues/18759>.

- isa infix operator
Item "isa infix operator"
Introduced in Perl 5.32.0.
.Sp
Using this feature triggers warnings in the category
\f(CW\*(C`experimental::isa\*(C'.
.Sp
The ticket for this experiment is
[perl #18754] <https://github.com/Perl/perl5/issues/18754>

- try/catch control structure
Item "try/catch control structure"
Introduced in Perl 5.34.0.
.Sp
Using this feature triggers warnings in the category \f(CW\*(C`experimental::try\*(C'.
.Sp
The ticket for this experiment is
[perl #18760] <https://github.com/Perl/perl5/issues/18760>

### Accepted features

Subsection "Accepted features"
These features were so wildly successful and played so well with others that
we decided to remove their experimental status and admit them as full, stable
features in the world of Perl, lavishing all the benefits and luxuries thereof.
They are also awarded +5 Stability and +3 Charisma.

- 64-bit support
Item "64-bit support"
Introduced in Perl 5.005

- die accepts a reference
Item "die accepts a reference"
Introduced in Perl 5.005

- \s-1DB\s0 module
Item "DB module"
Introduced in Perl 5.6.0
.Sp
See also perldebug, perldebtut

- Weak references
Item "Weak references"
Introduced in Perl 5.6.0

- Internal file glob
Item "Internal file glob"
Introduced in Perl 5.6.0

- \fBfork() emulation
Item "fork() emulation"
Introduced in Perl 5.6.1
.Sp
See also perlfork

- -Dusemultiplicity -Duseithreads
Item "-Dusemultiplicity -Duseithreads"
Introduced in Perl 5.6.0
.Sp
Accepted in Perl 5.8.0

- Support for long doubles
Item "Support for long doubles"
Introduced in Perl 5.6.0
.Sp
Accepted in Perl 5.8.1
.ie n .IP "The ""\\N"" regex character class" 8
.el .IP "The \f(CW\\N regex character class" 8
Item "The N regex character class"
The \f(CW\*(C`\\N\*(C' character class, not to be confused with the named character
sequence \f(CW\*(C`\\N\{NAME\}\*(C', denotes any non-newline character in a regular
expression.
.Sp
Introduced in Perl 5.12
.Sp
Exact version of acceptance unclear, but no later than Perl 5.18.
.ie n .IP """(?\{code\})"" and ""(??\{ code \})""" 8
.el .IP "\f(CW(?\{code\}) and \f(CW(??\{ code \})" 8
Item "(?\{code\}) and (??\{ code \})"
Introduced in Perl 5.6.0
.Sp
Accepted in Perl 5.20.0
.Sp
See also perlre

- Linux abstract Unix domain sockets
Item "Linux abstract Unix domain sockets"
Introduced in Perl 5.9.2
.Sp
Accepted before Perl 5.20.0.  The Socket library is now primarily maintained
on \s-1CPAN,\s0 rather than in the perl core.
.Sp
See also Socket

- Lvalue subroutines
Item "Lvalue subroutines"
Introduced in Perl 5.6.0
.Sp
Accepted in Perl 5.20.0
.Sp
See also perlsub

- Backtracking control verbs
Item "Backtracking control verbs"
\f(CW\*(C`(*ACCEPT)\*(C'
.Sp
Introduced in Perl 5.10
.Sp
Accepted in Perl 5.20.0

- The <:pop> \s-1IO\s0 pseudolayer
Item "The <:pop> IO pseudolayer"
See also \*(L"\s-1PERLIO\*(R"\s0 in perlrun
.Sp
Accepted in Perl 5.20.0
.ie n .IP """\\s"" in regexp matches vertical tab" 8
.el .IP "\f(CW\\s in regexp matches vertical tab" 8
Item "s in regexp matches vertical tab"
Accepted in Perl 5.22.0

- Postfix dereference syntax
Item "Postfix dereference syntax"
Introduced in Perl 5.20.0
.Sp
Accepted in Perl 5.24.0

- Lexical subroutines
Item "Lexical subroutines"
Introduced in Perl 5.18.0
.Sp
Accepted in Perl 5.26.0

- String- and number-specific bitwise operators
Item "String- and number-specific bitwise operators"
Introduced in Perl 5.22.0
.Sp
Accepted in Perl 5.28.0

- Alphabetic assertions
Item "Alphabetic assertions"
Introduced in Perl 5.28.0
.Sp
Accepted in Perl 5.32.0

- Script runs
Item "Script runs"
Introduced in Perl 5.28.0
.Sp
Accepted in Perl 5.32.0

### Removed features

Subsection "Removed features"
These features are no longer considered experimental and their functionality
has disappeared. It's your own fault if you wrote production programs using
these features after we explicitly told you not to (see perlpolicy).

- 5.005-style threading
Item "5.005-style threading"
Introduced in Perl 5.005
.Sp
Removed in Perl 5.10

- perlcc
Item "perlcc"
Introduced in Perl 5.005
.Sp
Moved from Perl 5.9.0 to \s-1CPAN\s0

- The pseudo-hash data type
Item "The pseudo-hash data type"
Introduced in Perl 5.6.0
.Sp
Removed in Perl 5.9.0

- GetOpt::Long Options can now take multiple values at once (experimental)
Item "GetOpt::Long Options can now take multiple values at once (experimental)"
\f(CW\*(C`Getopt::Long\*(C' upgraded to version 2.35
.Sp
Removed in Perl 5.8.8

- Assertions
Item "Assertions"
The \f(CW\*(C`-A\*(C' command line switch
.Sp
Introduced in Perl 5.9.0
.Sp
Removed in Perl 5.9.5

- Test::Harness::Straps
Item "Test::Harness::Straps"
Moved from Perl 5.10.1 to \s-1CPAN\s0
.ie n .IP """legacy""" 8
.el .IP "\f(CWlegacy" 8
Item "legacy"
The experimental \f(CW\*(C`legacy\*(C' pragma was swallowed by the \f(CW\*(C`feature\*(C' pragma.
.Sp
Introduced in Perl 5.11.2
.Sp
Removed in Perl 5.11.3
.ie n .IP "Lexical $_" 8
.el .IP "Lexical \f(CW$_" 8
Item "Lexical $_"
Using this feature triggered warnings in the category
\f(CW\*(C`experimental::lexical_topic\*(C'.
.Sp
Introduced in Perl 5.10.0
.Sp
Removed in Perl 5.24.0

- Array and hash container functions accept references
Item "Array and hash container functions accept references"
Using this feature triggered warnings in the category
\f(CW\*(C`experimental::autoderef\*(C'.
.Sp
Superseded by \*(L"Postfix dereference syntax\*(R".
.Sp
Introduced in Perl 5.14.0
.Sp
Removed in Perl 5.24.0
.ie n .IP """our"" can have an experimental optional attribute ""unique""" 8
.el .IP "\f(CWour can have an experimental optional attribute \f(CWunique" 8
Item "our can have an experimental optional attribute unique"
Introduced in Perl 5.8.0
.Sp
Deprecated in Perl 5.10.0
.Sp
Removed in Perl 5.28.0

## SEE ALSO

Header "SEE ALSO"
For a complete list of features check feature.

## AUTHORS

Header "AUTHORS"
brian d foy \f(CW\*(C`<brian.d.foy@gmail.com>\*(C'

Se\*'bastien Aperghis-Tramoni \f(CW\*(C`<saper@cpan.org>\*(C'

## COPYRIGHT

Header "COPYRIGHT"
Copyright 2010, brian d foy \f(CW\*(C`<brian.d.foy@gmail.com>\*(C'

## LICENSE

Header "LICENSE"
You can use and redistribute this document under the same terms as Perl
itself.
