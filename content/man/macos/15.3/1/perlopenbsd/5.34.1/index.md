+++
manpage_section = "1"
title = "perlopenbsd(1)"
description = "This document describes various features of OpenBSD that will affect how Perl version 5 (hereafter just Perl) is compiled and/or runs. When Perl is configured to use ithreads, it will use re-entrant library calls in preference to non-re-entrant ver..."
operating_system = "macos"
date = "2022-02-19"
author = "None Specified"
operating_system_version = "15.3"
manpage_name = "perlopenbsd"
detected_package_version = "5.34.1"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLOPENBSD 1"
PERLOPENBSD 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlopenbsd - Perl version 5 on OpenBSD systems

## DESCRIPTION

Header "DESCRIPTION"
This document describes various features of OpenBSD that will affect how Perl
version 5 (hereafter just Perl) is compiled and/or runs.

### OpenBSD core dumps from getprotobyname_r and getservbyname_r with ithreads

Subsection "OpenBSD core dumps from getprotobyname_r and getservbyname_r with ithreads"
When Perl is configured to use ithreads, it will use re-entrant library calls
in preference to non-re-entrant versions.  There is an incompatibility in
OpenBSD's \f(CW\*(C`getprotobyname_r\*(C' and \f(CW\*(C`getservbyname_r\*(C' function in versions 3.7
and later that will cause a \s-1SEGV\s0 when called without doing a \f(CW\*(C`bzero\*(C' on
their return structs prior to calling these functions.  Current Perl's
should handle this problem correctly.  Older threaded Perls (5.8.6 or earlier)
will run into this problem.  If you want to run a threaded Perl on OpenBSD
3.7 or higher, you will need to upgrade to at least Perl 5.8.7.

## AUTHOR

Header "AUTHOR"
Steve Peters <steve@fisharerojo.org>

Please report any errors, updates, or suggestions to
<https://github.com/Perl/perl5/issues>.
