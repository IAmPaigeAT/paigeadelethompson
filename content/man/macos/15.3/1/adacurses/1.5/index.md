+++
operating_system_version = "15.3"
author = ",2011 Free Software Foundation, Inc. *"
title = "adacurses(1)"
description = "This is a shell script which simplifies configuring an application to use the AdaCurses library binding to ncurses. --cflags echos the gnat (Ada compiler) flags needed to compile with AdaCurses. --libs echos the gnat libraries needed to link with..."
manpage_section = "1"
keywords = ["fbcurses", "this", "describes", "fbncurses", "version", "5", "9", "patch", "20110404"]
operating_system = "macos"
detected_package_version = "1.5"
manpage_name = "adacurses"
manpage_format = "troff"
date = "Sun Feb 16 04:48:23 2025"
+++

ADACURSES "1" "" "" "User Commands"

## NAME

adacurses-config - helper script for AdaCurses libraries

## SYNOPSIS

adacurses-config
[*options*]

## DESCRIPTION

This is a shell script which simplifies configuring an application to use
the AdaCurses library binding to ncurses.

## OPTIONS


**--cflags**
echos the gnat (Ada compiler) flags needed to compile with AdaCurses.

**--libs**
echos the gnat libraries needed to link with AdaCurses.

**--version**
echos the release+patchdate version of the ncurses libraries used
to configure and build AdaCurses.

**--help**
prints a list of the **adacurses-config** script's options.

## SEE ALSO

**curses**(3X)

This describes **ncurses**
version 5.9 (patch 20110404).
