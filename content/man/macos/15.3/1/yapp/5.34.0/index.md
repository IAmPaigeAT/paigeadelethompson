+++
manpage_format = "troff"
description = "yapp is a frontend to the Parse::Yapp module, which lets you compile Parse::Yapp grammar input files into Perl s-1LALRs0|(1) s-1OOs0 parser modules. Options, as of today, are all optionals :-) Creates a file grammar.output describing your parser. I..."
keywords = ["header", "see", "also", "fbparse", "yapp", "3", "fbperl", "1", "fbyacc", "fbbison"]
operating_system_version = "15.3"
operating_system = "macos"
manpage_section = "1"
manpage_name = "yapp"
detected_package_version = "5.34.0"
date = "2017-08-04"
title = "yapp(1)"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "YAPP 1"
YAPP 1 "2017-08-04" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

yapp - A perl frontend to the Parse::Yapp module

## SYNOPSYS

Header "SYNOPSYS"
yapp [options] *grammar*[.yp]

yapp *-V*

yapp *-h*

## DESCRIPTION

Header "DESCRIPTION"
yapp is a frontend to the Parse::Yapp module, which lets you compile
Parse::Yapp grammar input files into Perl \s-1**LALR\s0**\|(1) \s-1OO\s0 parser modules.

## OPTIONS

Header "OPTIONS"
Options, as of today, are all optionals :-)

- \fI-v
Item "-v"
Creates a file *grammar*.output describing your parser. It will
show you a summary of conflicts, rules, the \s-1DFA\s0 (Deterministic
Finite Automaton) states and overall usage of the parser.

- \fI-s
Item "-s"
Create a standalone module in which the driver is included.
Note that if you have more than one parser module called from
a program, to have it standalone, you need this option only
for one of your parser module.

- \fI-n
Item "-n"
Disable source file line numbering embedded in your parser module.
I don't know why one should need it, but it's there.

- \fI-m module
Item "-m module"
Gives your parser module the package name (or name space or module name or
class name or whatever-you-call-it) of *module*.  It defaults to *grammar*

- \fI-o outfile
Item "-o outfile"
The compiled output file will be named *outfile* for your parser module.
It defaults to *grammar*.pm or, if you specified the option
*-m A::Module::Name* (see below), to *Name.pm*, in the current
working directory.

- \fI-t filename
Item "-t filename"
The *-t filename* option allows you to specify a file which should be
used as template for generating the parser output.  The default is to
use the internal template defined in *Parse::Yapp::Output.pm*.
For how to write your own template and which substitutions are available,
have a look to the module *Parse::Yapp::Output.pm* : it should be obvious.

- \fI-b shebang
Item "-b shebang"
If you work on systems that understand so called *shebangs*, and your
generated parser is directly an executable script, you can specifie one
with the *-b* option, ie:
.Sp
.Vb 1
    yapp -b \*(Aq/usr/local/bin/perl -w\*(Aq -o myscript.pl myscript.yp
.Ve
.Sp
This will output a file called *myscript.pl* whose very first line is:
.Sp
.Vb 1
    #!/usr/local/bin/perl -w
.Ve
.Sp
The argument is mandatory, but if you specify an empty string, the value
of *\f(CI$Config\fI\{perlpath\}* will be used instead.

- \fIgrammar
Item "grammar"
The input grammar file. If no suffix is given, and the file does not exists,
an attempt to open the file with a suffix of  *.yp* is tried before exiting.

- \fI-V
Item "-V"
Display current version of Parse::Yapp and gracefully exits.

- \fI-h
Item "-h"
Display the usage screen.

## BUGS

Header "BUGS"
None known now :-)

## AUTHOR

Header "AUTHOR"
William N. Braswell, Jr. <wbraswell_cpan@NOSPAM.nym.hush.com>
(Remove \*(L"\s-1NOSPAM\*(R".\s0)

## COPYRIGHT

Header "COPYRIGHT"
Copyright © 1998, 1999, 2000, 2001, Francois Desarmenien.
Copyright © 2017 William N. Braswell, Jr.

See **Parse::Yapp**\|(3) for legal use and distribution rights

## SEE ALSO

Header "SEE ALSO"
**Parse::Yapp**\|(3) **Perl**\|(1) **yacc**\|(1) **bison**\|(1)
