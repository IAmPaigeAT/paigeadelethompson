+++
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
title = "perl5244delta(1)"
date = "2022-02-19"
operating_system = "macos"
operating_system_version = "15.3"
description = "This document describes differences between the 5.24.3 release and the 5.24.4 release. If you are upgrading from an earlier release such as 5.24.2, first read perl5243delta, which describes differences between 5.24.2 and 5.24.3. A crafted regular ..."
manpage_format = "troff"
manpage_name = "perl5244delta"
detected_package_version = "5.34.1"
manpage_section = "1"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5244DELTA 1"
PERL5244DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5244delta - what is new for perl v5.24.4

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.24.3 release and the 5.24.4
release.

If you are upgrading from an earlier release such as 5.24.2, first read
perl5243delta, which describes differences between 5.24.2 and 5.24.3.

## Security

Header "Security"

### [\s-1CVE-2018-6797\s0] heap-buffer-overflow (\s-1WRITE\s0 of size 1) in S_regatom (regcomp.c)

Subsection "[CVE-2018-6797] heap-buffer-overflow (WRITE of size 1) in S_regatom (regcomp.c)"
A crafted regular expression could cause a heap buffer write overflow, with
control over the bytes written.
[\s-1GH\s0 #16185] <https://github.com/Perl/perl5/issues/16185>

### [\s-1CVE-2018-6798\s0] Heap-buffer-overflow in Perl_\|_byte_dump_string (utf8.c)

Subsection "[CVE-2018-6798] Heap-buffer-overflow in Perl__byte_dump_string (utf8.c)"
Matching a crafted locale dependent regular expression could cause a heap
buffer read overflow and potentially information disclosure.
[\s-1GH\s0 #16143] <https://github.com/Perl/perl5/issues/16143>

### [\s-1CVE-2018-6913\s0] heap-buffer-overflow in S_pack_rec

Subsection "[CVE-2018-6913] heap-buffer-overflow in S_pack_rec"
\f(CW\*(C`pack()\*(C' could cause a heap buffer write overflow with a large item count.
[\s-1GH\s0 #16098] <https://github.com/Perl/perl5/issues/16098>

### Assertion failure in Perl_\|_core_swash_init (utf8.c)

Subsection "Assertion failure in Perl__core_swash_init (utf8.c)"
Control characters in a supposed Unicode property name could cause perl to
crash.  This has been fixed.
[perl #132055] <https://rt.perl.org/Public/Bug/Display.html?id=132055>
[perl #132553] <https://rt.perl.org/Public/Bug/Display.html?id=132553>
[perl #132658] <https://rt.perl.org/Public/Bug/Display.html?id=132658>

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.24.3.  If any exist,
they are bugs, and we request that you submit a report.  See \*(L"Reporting
Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Module::CoreList has been upgraded from version 5.20170922_24 to 5.20180414_24.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
The \f(CW\*(C`readpipe()\*(C' built-in function now checks at compile time that it has only
one parameter expression, and puts it in scalar context, thus ensuring that it
doesn't corrupt the stack at runtime.
[\s-1GH\s0 #2793] <https://github.com/Perl/perl5/issues/2793>

## Acknowledgements

Header "Acknowledgements"
Perl 5.24.4 represents approximately 7 months of development since Perl 5.24.3
and contains approximately 2,400 lines of changes across 49 files from 12
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 1,300 lines of changes to 12 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.24.4:

Abigail, Chris 'BinGOs' Williams, John \s-1SJ\s0 Anderson, Karen Etheridge, Karl
Williamson, Renee Baecker, Sawyer X, Steve Hay, Todd Rinaldo, Tony Cook, Yves
Orton, Zefram.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the perl bug database at
<https://rt.perl.org/> .  There may also be information at
<http://www.perl.org/> , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
