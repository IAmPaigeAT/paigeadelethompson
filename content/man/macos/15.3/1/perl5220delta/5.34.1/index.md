+++
manpage_section = "1"
title = "perl5220delta(1)"
author = "None Specified"
description = "This document describes differences between the 5.20.0 release and the 5.22.0 release. If you are upgrading from an earlier release such as 5.18.0, first read perl5200delta, which describes differences between 5.18.0 and 5.20.0. A new experimental..."
operating_system_version = "15.3"
detected_package_version = "5.34.1"
manpage_name = "perl5220delta"
operating_system = "macos"
manpage_format = "troff"
date = "2022-02-19"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5220DELTA 1"
PERL5220DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5220delta - what is new for perl v5.22.0

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.20.0 release and the 5.22.0
release.

If you are upgrading from an earlier release such as 5.18.0, first read
perl5200delta, which describes differences between 5.18.0 and 5.20.0.

## Core Enhancements

Header "Core Enhancements"

### New bitwise operators

Subsection "New bitwise operators"
A new experimental facility has been added that makes the four standard
bitwise operators (\f(CW\*(C`& | ^ ~\*(C') treat their operands consistently as
numbers, and introduces four new dotted operators (\f(CW\*(C`&. |. ^. ~.\*(C') that
treat their operands consistently as strings.  The same applies to the
assignment variants (\f(CW\*(C`&= |= ^= &.= |.= ^.=\*(C').

To use this, enable the \*(L"bitwise\*(R" feature and disable the
\*(L"experimental::bitwise\*(R" warnings category.  See \*(L"Bitwise String
Operators\*(R" in perlop for details.
[\s-1GH\s0 #14348] <https://github.com/Perl/perl5/issues/14348>.

### New double-diamond operator

Subsection "New double-diamond operator"
\f(CW\*(C`<<>>\*(C' is like \f(CW\*(C`<>\*(C' but uses three-argument \f(CW\*(C`open\*(C' to open
each file in \f(CW@ARGV.  This means that each element of \f(CW@ARGV will be treated
as an actual file name, and \f(CW"|foo" won't be treated as a pipe open.
.ie n .SS "New ""\\b"" boundaries in regular expressions"
.el .SS "New \f(CW\\b boundaries in regular expressions"
Subsection "New b boundaries in regular expressions"
*\f(CI\*(C`qr/\\b\{gcb\}/\*(C'\fI*
Subsection "qr/b\{gcb\}/"

\f(CW\*(C`gcb\*(C' stands for Grapheme Cluster Boundary.  It is a Unicode property
that finds the boundary between sequences of characters that look like a
single character to a native speaker of a language.  Perl has long had
the ability to deal with these through the \f(CW\*(C`\\X\*(C' regular escape
sequence.  Now, there is an alternative way of handling these.  See
\*(L"\\b\{\}, \\b, \\B\{\}, \\B\*(R" in perlrebackslash for details.

*\f(CI\*(C`qr/\\b\{wb\}/\*(C'\fI*
Subsection "qr/b\{wb\}/"

\f(CW\*(C`wb\*(C' stands for Word Boundary.  It is a Unicode property
that finds the boundary between words.  This is similar to the plain
\f(CW\*(C`\\b\*(C' (without braces) but is more suitable for natural language
processing.  It knows, for example, that apostrophes can occur in the
middle of words.  See \*(L"\\b\{\}, \\b, \\B\{\}, \\B\*(R" in perlrebackslash for details.

*\f(CI\*(C`qr/\\b\{sb\}/\*(C'\fI*
Subsection "qr/b\{sb\}/"

\f(CW\*(C`sb\*(C' stands for Sentence Boundary.  It is a Unicode property
to aid in parsing natural language sentences.
See \*(L"\\b\{\}, \\b, \\B\{\}, \\B\*(R" in perlrebackslash for details.

### Non-Capturing Regular Expression Flag

Subsection "Non-Capturing Regular Expression Flag"
Regular expressions now support a \f(CW\*(C`/n\*(C' flag that disables capturing
and filling in \f(CW$1, \f(CW$2, etc inside of groups:

.Vb 1
  "hello" =~ /(hi|hello)/n; # $1 is not set
.Ve

This is equivalent to putting \f(CW\*(C`?:\*(C' at the beginning of every capturing group.

See \*(L"n\*(R" in perlre for more information.
.ie n .SS """use re \*(Aqstrict\*(Aq"""
.el .SS "\f(CWuse re \*(Aqstrict\*(Aq"
Subsection "use re strict"
This applies stricter syntax rules to regular expression patterns
compiled within its scope. This will hopefully alert you to typos and
other unintentional behavior that backwards-compatibility issues prevent
us from reporting in normal regular expression compilations.  Because the
behavior of this is subject to change in future Perl releases as we gain
experience, using this pragma will raise a warning of category
\f(CW\*(C`experimental::re_strict\*(C'.
See 'strict' in re.

### Unicode 7.0 (with correction) is now supported

Subsection "Unicode 7.0 (with correction) is now supported"
For details on what is in this release, see
<http://www.unicode.org/versions/Unicode7.0.0/>.
The version of Unicode 7.0 that comes with Perl includes
a correction dealing with glyph shaping in Arabic
(see <http://www.unicode.org/errata/#current_errata>).
.ie n .SS """use\ locale"" can restrict which locale categories are affected"
.el .SS "\f(CWuse\ locale can restrict which locale categories are affected"
Subsection "uselocale can restrict which locale categories are affected"
It is now possible to pass a parameter to \f(CW\*(C`use\ locale\*(C' to specify
a subset of locale categories to be locale-aware, with the remaining
ones unaffected.  See \*(L"The \*(R"use locale\*(L" pragma\*(R" in perllocale for details.

### Perl now supports \s-1POSIX 2008\s0 locale currency additions

Subsection "Perl now supports POSIX 2008 locale currency additions"
On platforms that are able to handle \s-1POSIX.1-2008,\s0 the
hash returned by
\f(CW\*(C`POSIX::localeconv()\*(C'
includes the international currency fields added by that version of the
\s-1POSIX\s0 standard.  These are
\f(CW\*(C`int_n_cs_precedes\*(C',
\f(CW\*(C`int_n_sep_by_space\*(C',
\f(CW\*(C`int_n_sign_posn\*(C',
\f(CW\*(C`int_p_cs_precedes\*(C',
\f(CW\*(C`int_p_sep_by_space\*(C',
and
\f(CW\*(C`int_p_sign_posn\*(C'.

### Better heuristics on older platforms for determining locale UTF-8ness

Subsection "Better heuristics on older platforms for determining locale UTF-8ness"
On platforms that implement neither the C99 standard nor the \s-1POSIX 2001\s0
standard, determining if the current locale is \s-1UTF-8\s0 or not depends on
heuristics.  These are improved in this release.

### Aliasing via reference

Subsection "Aliasing via reference"
Variables and subroutines can now be aliased by assigning to a reference:

.Vb 2
    \\$c = \\$d;
    \x = \y;
.Ve

Aliasing can also be accomplished
by using a backslash before a \f(CW\*(C`foreach\*(C' iterator variable; this is
perhaps the most useful idiom this feature provides:

.Vb 1
    foreach \\%hash (@array_of_hash_refs) \{ ... \}
.Ve

This feature is experimental and must be enabled via \f(CW\*(C`use\ feature\ \*(Aqrefaliasing\*(Aq\*(C'.  It will warn unless the \f(CW\*(C`experimental::refaliasing\*(C'
warnings category is disabled.

See \*(L"Assigning to References\*(R" in perlref
.ie n .SS """prototype"" with no arguments"
.el .SS "\f(CWprototype with no arguments"
Subsection "prototype with no arguments"
\f(CW\*(C`prototype()\*(C' with no arguments now infers \f(CW$_.
[\s-1GH\s0 #14376] <https://github.com/Perl/perl5/issues/14376>.
.ie n .SS "New "":const"" subroutine attribute"
.el .SS "New \f(CW:const subroutine attribute"
Subsection "New :const subroutine attribute"
The \f(CW\*(C`const\*(C' attribute can be applied to an anonymous subroutine.  It
causes the new sub to be executed immediately whenever one is created
(*i.e.* when the \f(CW\*(C`sub\*(C' expression is evaluated).  Its value is captured
and used to create a new constant subroutine that is returned.  This
feature is experimental.  See \*(L"Constant Functions\*(R" in perlsub.
.ie n .SS """fileno"" now works on directory handles"
.el .SS "\f(CWfileno now works on directory handles"
Subsection "fileno now works on directory handles"
When the relevant support is available in the operating system, the
\f(CW\*(C`fileno\*(C' builtin now works on directory handles, yielding the
underlying file descriptor in the same way as for filehandles. On
operating systems without such support, \f(CW\*(C`fileno\*(C' on a directory handle
continues to return the undefined value, as before, but also sets \f(CW$! to
indicate that the operation is not supported.

Currently, this uses either a \f(CW\*(C`dd_fd\*(C' member in the \s-1OS\s0 \f(CW\*(C`DIR\*(C'
structure, or a \f(CWdirfd(3) function as specified by \s-1POSIX.1-2008.\s0

### List form of pipe open implemented for Win32

Subsection "List form of pipe open implemented for Win32"
The list form of pipe:

.Vb 1
  open my $fh, "-|", "program", @arguments;
.Ve

is now implemented on Win32.  It has the same limitations as \f(CW\*(C`system
LIST\*(C' on Win32, since the Win32 \s-1API\s0 doesn't accept program arguments
as a list.

### Assignment to list repetition

Subsection "Assignment to list repetition"
\f(CW\*(C`(...) x ...\*(C' can now be used within a list that is assigned to, as long
as the left-hand side is a valid lvalue.  This allows \f(CW\*(C`(undef,undef,$foo)\ =\ that_function()\*(C' to be written as \f(CW\*(C`((undef)x2,\ $foo)\ =\ that_function()\*(C'.

### Infinity and NaN (not-a-number) handling improved

Subsection "Infinity and NaN (not-a-number) handling improved"
Floating point values are able to hold the special values infinity, negative
infinity, and NaN (not-a-number).  Now we more robustly recognize and
propagate the value in computations, and on output normalize them to the strings
\f(CW\*(C`Inf\*(C', \f(CW\*(C`-Inf\*(C', and \f(CW\*(C`NaN\*(C'.

See also the \s-1POSIX\s0 enhancements.

### Floating point parsing has been improved

Subsection "Floating point parsing has been improved"
Parsing and printing of floating point values has been improved.

As a completely new feature, hexadecimal floating point literals
(like \f(CW\*(C`0x1.23p-4\*(C')  are now supported, and they can be output with
\f(CW\*(C`printf\ "%a"\*(C'. See \*(L"Scalar value constructors\*(R" in perldata for more
details.

### Packing infinity or not-a-number into a character is now fatal

Subsection "Packing infinity or not-a-number into a character is now fatal"
Before, when trying to pack infinity or not-a-number into a
(signed) character, Perl would warn, and assumed you tried to
pack \f(CW0xFF; if you gave it as an argument to \f(CW\*(C`chr\*(C',
\f(CW\*(C`U+FFFD\*(C' was returned.

But now, all such actions (\f(CW\*(C`pack\*(C', \f(CW\*(C`chr\*(C', and \f(CW\*(C`print \*(Aq%c\*(Aq\*(C')
result in a fatal error.

### Experimental C Backtrace \s-1API\s0

Subsection "Experimental C Backtrace API"
Perl now supports (via a C level \s-1API\s0) retrieving
the C level backtrace (similar to what symbolic debuggers like gdb do).

The backtrace returns the stack trace of the C call frames,
with the symbol names (function names), the object names (like \*(L"perl\*(R"),
and if it can, also the source code locations (file:line).

The supported platforms are Linux and \s-1OS X\s0 (some *BSD might work at
least partly, but they have not yet been tested).

The feature needs to be enabled with \f(CW\*(C`Configure -Dusecbacktrace\*(C'.

See \*(L"C backtrace\*(R" in perlhacktips for more information.

## Security

Header "Security"
.ie n .SS "Perl is now compiled with ""-fstack-protector-strong"" if available"
.el .SS "Perl is now compiled with \f(CW-fstack-protector-strong if available"
Subsection "Perl is now compiled with -fstack-protector-strong if available"
Perl has been compiled with the anti-stack-smashing option
\f(CW\*(C`-fstack-protector\*(C' since 5.10.1.  Now Perl uses the newer variant
called \f(CW\*(C`-fstack-protector-strong\*(C', if available.

### The Safe module could allow outside packages to be replaced

Subsection "The Safe module could allow outside packages to be replaced"
Critical bugfix: outside packages could be replaced.  Safe has
been patched to 2.38 to address this.
.ie n .SS "Perl is now always compiled with ""-D_FORTIFY_SOURCE=2"" if available"
.el .SS "Perl is now always compiled with \f(CW-D_FORTIFY_SOURCE=2 if available"
Subsection "Perl is now always compiled with -D_FORTIFY_SOURCE=2 if available"
The 'code hardening' option called \f(CW\*(C`_FORTIFY_SOURCE\*(C', available in
gcc 4.*, is now always used for compiling Perl, if available.

Note that this isn't necessarily a huge step since in many platforms
the step had already been taken several years ago: many Linux
distributions (like Fedora) have been using this option for Perl,
and \s-1OS X\s0 has enforced the same for many years.

## Incompatible Changes

Header "Incompatible Changes"

### Subroutine signatures moved before attributes

Subsection "Subroutine signatures moved before attributes"
The experimental sub signatures feature, as introduced in 5.20, parsed
signatures after attributes. In this release, following feedback from users
of the experimental feature, the positioning has been moved such that
signatures occur after the subroutine name (if any) and before the attribute
list (if any).
.ie n .SS """&"" and ""\"" prototypes accepts only subs"
.el .SS "\f(CW& and \f(CW\ prototypes accepts only subs"
Subsection "& and & prototypes accepts only subs"
The \f(CW\*(C`&\*(C' prototype character now accepts only anonymous subs (\f(CW\*(C`sub
\{...\}\*(C'), things beginning with \f(CW\*(C`\\*(C', or an explicit \f(CW\*(C`undef\*(C'.  Formerly
it erroneously also allowed references to arrays, hashes, and lists.
[\s-1GH\s0 #2776] <https://github.com/Perl/perl5/issues/2776>.
[\s-1GH\s0 #14186] <https://github.com/Perl/perl5/issues/14186>.
[\s-1GH\s0 #14353] <https://github.com/Perl/perl5/issues/14353>.

In addition, the \f(CW\*(C`\\*(C' prototype was allowing subroutine calls, whereas
now it only allows subroutines: \f(CW&foo is still permitted as an argument,
while \f(CW\*(C`&foo()\*(C' and \f(CW\*(C`foo()\*(C' no longer are.
[\s-1GH\s0 #10633] <https://github.com/Perl/perl5/issues/10633>.
.ie n .SS """use encoding"" is now lexical"
.el .SS "\f(CWuse encoding is now lexical"
Subsection "use encoding is now lexical"
The encoding pragma's effect is now limited to lexical scope.  This
pragma is deprecated, but in the meantime, it could adversely affect
unrelated modules that are included in the same program; this change
fixes that.

### List slices returning empty lists

Subsection "List slices returning empty lists"
List slices now return an empty list only if the original list was empty
(or if there are no indices).  Formerly, a list slice would return an empty
list if all indices fell outside the original list; now it returns a list
of \f(CW\*(C`undef\*(C' values in that case.
[\s-1GH\s0 #12335] <https://github.com/Perl/perl5/issues/12335>.
.ie n .SS """\\N\{\}"" with a sequence of multiple spaces is now a fatal error"
.el .SS "\f(CW\\N\{\} with a sequence of multiple spaces is now a fatal error"
Subsection "N\{\} with a sequence of multiple spaces is now a fatal error"
E.g. \f(CW\*(C`\\N\{TOO\ \ MANY\ SPACES\}\*(C' or \f(CW\*(C`\\N\{TRAILING\ SPACE\ \}\*(C'.
This has been deprecated since v5.18.
.ie n .SS """use\ UNIVERSAL\ \*(Aq...\*(Aq"" is now a fatal error"
.el .SS "\f(CWuse\ UNIVERSAL\ \*(Aq...\*(Aq is now a fatal error"
Subsection "useUNIVERSAL... is now a fatal error"
Importing functions from \f(CW\*(C`UNIVERSAL\*(C' has been deprecated since v5.12, and
is now a fatal error.  \f(CW\*(C`use\ UNIVERSAL\*(C' without any arguments is still
allowed.
.ie n .SS "In double-quotish ""\\c\f(CIX"", *X* must now be a printable \s-1ASCII\s0 character"
.el .SS "In double-quotish \f(CW\\c\f(CIX\f(CW, *X* must now be a printable \s-1ASCII\s0 character"
Subsection "In double-quotish cX, X must now be a printable ASCII character"
In prior releases, failure to do this raised a deprecation warning.
.ie n .SS "Splitting the tokens ""(?"" and ""(*"" in regular expressions is now a fatal compilation error."
.el .SS "Splitting the tokens \f(CW(? and \f(CW(* in regular expressions is now a fatal compilation error."
Subsection "Splitting the tokens (? and (* in regular expressions is now a fatal compilation error."
These had been deprecated since v5.18.
.ie n .SS """qr/foo/x"" now ignores all Unicode pattern white space"
.el .SS "\f(CWqr/foo/x now ignores all Unicode pattern white space"
Subsection "qr/foo/x now ignores all Unicode pattern white space"
The \f(CW\*(C`/x\*(C' regular expression modifier allows the pattern to contain
white space and comments (both of which are ignored) for improved
readability.  Until now, not all the white space characters that Unicode
designates for this purpose were handled.  The additional ones now
recognized are:

.Vb 5
    U+0085 NEXT LINE
    U+200E LEFT-TO-RIGHT MARK
    U+200F RIGHT-TO-LEFT MARK
    U+2028 LINE SEPARATOR
    U+2029 PARAGRAPH SEPARATOR
.Ve

The use of these characters with \f(CW\*(C`/x\*(C' outside bracketed character
classes and when not preceded by a backslash has raised a deprecation
warning since v5.18.  Now they will be ignored.
.ie n .SS "Comment lines within ""(?[\ ])"" are now ended only by a ""\\n"""
.el .SS "Comment lines within \f(CW(?[\ ]) are now ended only by a \f(CW\\n"
Subsection "Comment lines within (?[]) are now ended only by a n"
\f(CW\*(C`(?[\ ])\*(C'  is an experimental feature, introduced in v5.18.  It operates
as if \f(CW\*(C`/x\*(C' is always enabled.  But there was a difference: comment
lines (following a \f(CW\*(C`#\*(C' character) were terminated by anything matching
\f(CW\*(C`\\R\*(C' which includes all vertical whitespace, such as form feeds.  For
consistency, this is now changed to match what terminates comment lines
outside \f(CW\*(C`(?[\ ])\*(C', namely a \f(CW\*(C`\\n\*(C' (even if escaped), which is the
same as what terminates a heredoc string and formats.
.ie n .SS """(?[...])"" operators now follow standard Perl precedence"
.el .SS "\f(CW(?[...]) operators now follow standard Perl precedence"
Subsection "(?[...]) operators now follow standard Perl precedence"
This experimental feature allows set operations in regular expression patterns.
Prior to this, the intersection operator had the same precedence as the other
binary operators.  Now it has higher precedence.  This could lead to different
outcomes than existing code expects (though the documentation has always noted
that this change might happen, recommending fully parenthesizing the
expressions).  See \*(L"Extended Bracketed Character Classes\*(R" in perlrecharclass.
.ie n .SS "Omitting ""%"" and ""@"" on hash and array names is no longer permitted"
.el .SS "Omitting \f(CW% and \f(CW@ on hash and array names is no longer permitted"
Subsection "Omitting % and @ on hash and array names is no longer permitted"
Really old Perl let you omit the \f(CW\*(C`@\*(C' on array names and the \f(CW\*(C`%\*(C' on hash
names in some spots.  This has issued a deprecation warning since Perl
5.000, and is no longer permitted.
.ie n .SS """$!"" text is now in English outside the scope of ""use locale"""
.el .SS "\f(CW``$!'' text is now in English outside the scope of \f(CWuse locale"
Subsection """$!"" text is now in English outside the scope of use locale"
Previously, the text, unlike almost everything else, always came out
based on the current underlying locale of the program.  (Also affected
on some systems is \f(CW"$^E".)  For programs that are unprepared to
handle locale differences, this can cause garbage text to be displayed.
It's better to display text that is translatable via some tool than
garbage text which is much harder to figure out.
.ie n .SS """$!"" text will be returned in \s-1UTF-8\s0 when appropriate"
.el .SS "\f(CW``$!'' text will be returned in \s-1UTF-8\s0 when appropriate"
Subsection """$!"" text will be returned in UTF-8 when appropriate"
The stringification of \f(CW$! and \f(CW$^E will have the \s-1UTF-8\s0 flag set
when the text is actually non-ASCII \s-1UTF-8.\s0  This will enable programs
that are set up to be locale-aware to properly output messages in the
user's native language.  Code that needs to continue the 5.20 and
earlier behavior can do the stringification within the scopes of both
\f(CW\*(C`use\ bytes\*(C' and \f(CW\*(C`use\ locale\ ":messages"\*(C'.  Within these two
scopes, no other Perl operations will
be affected by locale; only \f(CW$! and \f(CW$^E stringification.  The
\f(CW\*(C`bytes\*(C' pragma causes the \s-1UTF-8\s0 flag to not be set, just as in previous
Perl releases.  This resolves
[\s-1GH\s0 #12035] <https://github.com/Perl/perl5/issues/12035>.
.ie n .SS "Support for ""?PATTERN?"" without explicit operator has been removed"
.el .SS "Support for \f(CW?PATTERN? without explicit operator has been removed"
Subsection "Support for ?PATTERN? without explicit operator has been removed"
The \f(CW\*(C`m?PATTERN?\*(C' construct, which allows matching a regex only once,
previously had an alternative form that was written directly with a question
mark delimiter, omitting the explicit \f(CW\*(C`m\*(C' operator.  This usage has produced
a deprecation warning since 5.14.0.  It is now a syntax error, so that the
question mark can be available for use in new operators.
.ie n .SS """defined(@array)"" and ""defined(%hash)"" are now fatal errors"
.el .SS "\f(CWdefined(@array) and \f(CWdefined(%hash) are now fatal errors"
Subsection "defined(@array) and defined(%hash) are now fatal errors"
These have been deprecated since v5.6.1 and have raised deprecation
warnings since v5.16.

### Using a hash or an array as a reference are now fatal errors

Subsection "Using a hash or an array as a reference are now fatal errors"
For example, \f(CW\*(C`%foo->\{"bar"\}\*(C' now causes a fatal compilation
error.  These have been deprecated since before v5.8, and have raised
deprecation warnings since then.
.ie n .SS "Changes to the ""*"" prototype"
.el .SS "Changes to the \f(CW* prototype"
Subsection "Changes to the * prototype"
The \f(CW\*(C`*\*(C' character in a subroutine's prototype used to allow barewords to take
precedence over most, but not all, subroutine names.  It was never
consistent and exhibited buggy behavior.

Now it has been changed, so subroutines always take precedence over barewords,
which brings it into conformity with similarly prototyped built-in functions:

.Vb 6
    sub splat(*) \{ ... \}
    sub foo \{ ... \}
    splat(foo); # now always splat(foo())
    splat(bar); # still splat(\*(Aqbar\*(Aq) as before
    close(foo); # close(foo())
    close(bar); # close(\*(Aqbar\*(Aq)
.Ve

## Deprecations

Header "Deprecations"
.ie n .SS "Setting ""$\{^ENCODING\}"" to anything but ""undef"""
.el .SS "Setting \f(CW$\{^ENCODING\} to anything but \f(CWundef"
Subsection "Setting $\{^ENCODING\} to anything but undef"
This variable allows Perl scripts to be written in an encoding other than
\s-1ASCII\s0 or \s-1UTF-8.\s0  However, it affects all modules globally, leading
to wrong answers and segmentation faults.  New scripts should be written
in \s-1UTF-8\s0; old scripts should be converted to \s-1UTF-8,\s0 which is easily done
with the piconv utility.

### Use of non-graphic characters in single-character variable names

Subsection "Use of non-graphic characters in single-character variable names"
The syntax for single-character variable names is more lenient than
for longer variable names, allowing the one-character name to be a
punctuation character or even invisible (a non-graphic).  Perl v5.20
deprecated the ASCII-range controls as such a name.  Now, all
non-graphic characters that formerly were allowed are deprecated.
The practical effect of this occurs only when not under \f(CW\*(C`use\ utf8\*(C', and affects just the C1 controls (code points 0x80 through
0xFF), NO-BREAK \s-1SPACE,\s0 and \s-1SOFT HYPHEN.\s0
.ie n .SS "Inlining of ""sub () \{ $var \}"" with observable side-effects"
.el .SS "Inlining of \f(CWsub () \{ $var \} with observable side-effects"
Subsection "Inlining of sub () \{ $var \} with observable side-effects"
In many cases Perl makes \f(CW\*(C`sub\ ()\ \{\ $var\ \}\*(C' into an inlinable constant
subroutine, capturing the value of \f(CW$var at the time the \f(CW\*(C`sub\*(C' expression
is evaluated.  This can break the closure behavior in those cases where
\f(CW$var is subsequently modified, since the subroutine won't return the
changed value. (Note that this all only applies to anonymous subroutines
with an empty prototype (\f(CW\*(C`sub\ ()\*(C').)

This usage is now deprecated in those cases where the variable could be
modified elsewhere.  Perl detects those cases and emits a deprecation
warning.  Such code will likely change in the future and stop producing a
constant.

If your variable is only modified in the place where it is declared, then
Perl will continue to make the sub inlinable with no warnings.

.Vb 4
    sub make_constant \{
        my $var = shift;
        return sub () \{ $var \}; # fine
    \}

    sub make_constant_deprecated \{
        my $var;
        $var = shift;
        return sub () \{ $var \}; # deprecated
    \}

    sub make_constant_deprecated2 \{
        my $var = shift;
        log_that_value($var); # could modify $var
        return sub () \{ $var \}; # deprecated
    \}
.Ve

In the second example above, detecting that \f(CW$var is assigned to only once
is too hard to detect.  That it happens in a spot other than the \f(CW\*(C`my\*(C'
declaration is enough for Perl to find it suspicious.

This deprecation warning happens only for a simple variable for the body of
the sub.  (A \f(CW\*(C`BEGIN\*(C' block or \f(CW\*(C`use\*(C' statement inside the sub is ignored,
because it does not become part of the sub's body.)  For more complex
cases, such as \f(CW\*(C`sub\ ()\ \{\ do_something()\ if\ 0;\ $var\ \}\*(C' the behavior has
changed such that inlining does not happen if the variable is modifiable
elsewhere.  Such cases should be rare.
.ie n .SS "Use of multiple ""/x"" regexp modifiers"
.el .SS "Use of multiple \f(CW/x regexp modifiers"
Subsection "Use of multiple /x regexp modifiers"
It is now deprecated to say something like any of the following:

.Vb 3
    qr/foo/xx;
    /(?xax:foo)/;
    use re qw(/amxx);
.Ve

That is, now \f(CW\*(C`x\*(C' should only occur once in any string of contiguous
regular expression pattern modifiers.  We do not believe there are any
occurrences of this in all of \s-1CPAN.\s0  This is in preparation for a future
Perl release having \f(CW\*(C`/xx\*(C' permit white-space for readability in
bracketed character classes (those enclosed in square brackets:
\f(CW\*(C`[...]\*(C').
.ie n .SS "Using a NO-BREAK space in a character alias for ""\\N\{...\}"" is now deprecated"
.el .SS "Using a NO-BREAK space in a character alias for \f(CW\\N\{...\} is now deprecated"
Subsection "Using a NO-BREAK space in a character alias for N\{...\} is now deprecated"
This non-graphic character is essentially indistinguishable from a
regular space, and so should not be allowed.  See
\*(L"\s-1CUSTOM ALIASES\*(R"\s0 in charnames.
.ie n .SS "A literal ""\{"" should now be escaped in a pattern"
.el .SS "A literal \f(CW``\{'' should now be escaped in a pattern"
Subsection "A literal ""\{"" should now be escaped in a pattern"
If you want a literal left curly bracket (also called a left brace) in a
regular expression pattern, you should now escape it by either
preceding it with a backslash (\f(CW"\\\{") or enclosing it within square
brackets \f(CW"[\{]", or by using \f(CW\*(C`\\Q\*(C'; otherwise a deprecation warning
will be raised.  This was first announced as forthcoming in the v5.16
release; it will allow future extensions to the language to happen.

### Making all warnings fatal is discouraged

Subsection "Making all warnings fatal is discouraged"
The documentation for fatal warnings notes that
\f(CW\*(C`use warnings FATAL => \*(Aqall\*(Aq\*(C' is discouraged, and provides stronger
language about the risks of fatal warnings in general.

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
If a method or class name is known at compile time, a hash is precomputed
to speed up run-time method lookup.  Also, compound method names like
\f(CW\*(C`SUPER::new\*(C' are parsed at compile time, to save having to parse them at
run time.

- \(bu
Array and hash lookups (especially nested ones) that use only constants
or simple variables as keys, are now considerably faster. See
\*(L"Internal Changes\*(R" for more details.

- \(bu
\f(CW\*(C`(...)x1\*(C', \f(CW\*(C`("constant")x0\*(C' and \f(CW\*(C`($scalar)x0\*(C' are now optimised in list
context.  If the right-hand argument is a constant 1, the repetition
operator disappears.  If the right-hand argument is a constant 0, the whole
expression is optimised to the empty list, so long as the left-hand
argument is a simple scalar or constant.  (That is, \f(CW\*(C`(foo())x0\*(C' is not
subject to this optimisation.)

- \(bu
\f(CW\*(C`substr\*(C' assignment is now optimised into 4-argument \f(CW\*(C`substr\*(C' at the end
of a subroutine (or as the argument to \f(CW\*(C`return\*(C').  Previously, this
optimisation only happened in void context.

- \(bu
In \f(CW"\\L...", \f(CW"\\Q...", etc., the extra \*(L"stringify\*(R" op is now optimised
away, making these just as fast as \f(CW\*(C`lcfirst\*(C', \f(CW\*(C`quotemeta\*(C', etc.

- \(bu
Assignment to an empty list is now sometimes faster.  In particular, it
never calls \f(CW\*(C`FETCH\*(C' on tied arguments on the right-hand side, whereas it
used to sometimes.

- \(bu
There is a performance improvement of up to 20% when \f(CW\*(C`length\*(C' is applied to
a non-magical, non-tied string, and either \f(CW\*(C`use bytes\*(C' is in scope or the
string doesn't use \s-1UTF-8\s0 internally.

- \(bu
On most perl builds with 64-bit integers, memory usage for non-magical,
non-tied scalars containing only a floating point value has been reduced
by between 8 and 32 bytes, depending on \s-1OS.\s0

- \(bu
In \f(CW\*(C`@array = split\*(C', the assignment can be optimized away, so that \f(CW\*(C`split\*(C'
writes directly to the array.  This optimisation was happening only for
package arrays other than \f(CW@_, and only sometimes.  Now this
optimisation happens almost all the time.

- \(bu
\f(CW\*(C`join\*(C' is now subject to constant folding.  So for example
\f(CW\*(C`join\ "-",\ "a",\ "b"\*(C' is converted at compile-time to \f(CW"a-b".
Moreover, \f(CW\*(C`join\*(C' with a scalar or constant for the separator and a
single-item list to join is simplified to a stringification, and the
separator doesn't even get evaluated.

- \(bu
\f(CW\*(C`qq(@array)\*(C' is implemented using two ops: a stringify op and a join op.
If the \f(CW\*(C`qq\*(C' contains nothing but a single array, the stringification is
optimized away.

- \(bu
\f(CW\*(C`our\ $var\*(C' and \f(CW\*(C`our($s,@a,%h)\*(C' in void context are no longer evaluated at
run time.  Even a whole sequence of \f(CW\*(C`our\ $foo;\*(C' statements will simply be
skipped over.  The same applies to \f(CW\*(C`state\*(C' variables.

- \(bu
Many internal functions have been refactored to improve performance and reduce
their memory footprints.
[\s-1GH\s0 #13659] <https://github.com/Perl/perl5/issues/13659>
[\s-1GH\s0 #13856] <https://github.com/Perl/perl5/issues/13856>
[\s-1GH\s0 #13874] <https://github.com/Perl/perl5/issues/13874>

- \(bu
\f(CW\*(C`-T\*(C' and \f(CW\*(C`-B\*(C' filetests will return sooner when an empty file is detected.
[\s-1GH\s0 #13686] <https://github.com/Perl/perl5/issues/13686>

- \(bu
Hash lookups where the key is a constant are faster.

- \(bu
Subroutines with an empty prototype and a body containing just \f(CW\*(C`undef\*(C' are now
eligible for inlining.
[\s-1GH\s0 #14077] <https://github.com/Perl/perl5/issues/14077>

- \(bu
Subroutines in packages no longer need to be stored in typeglobs:
declaring a subroutine will now put a simple sub reference directly in the
stash if possible, saving memory.  The typeglob still notionally exists,
so accessing it will cause the stash entry to be upgraded to a typeglob
(*i.e.* this is just an internal implementation detail).
This optimization does not currently apply to XSUBs or exported
subroutines, and method calls will undo it, since they cache things in
typeglobs.
[\s-1GH\s0 #13392] <https://github.com/Perl/perl5/issues/13392>

- \(bu
The functions \f(CW\*(C`utf8::native_to_unicode()\*(C' and \f(CW\*(C`utf8::unicode_to_native()\*(C'
(see utf8) are now optimized out on \s-1ASCII\s0 platforms.  There is now not even
a minimal performance hit in writing code portable between \s-1ASCII\s0 and \s-1EBCDIC\s0
platforms.

- \(bu
Win32 Perl uses 8 \s-1KB\s0 less of per-process memory than before for every perl
process, because some data is now memory mapped from disk and shared
between processes from the same perl binary.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"
Many of the libraries distributed with perl have been upgraded since v5.20.0.
For a complete list of changes, run:

.Vb 1
  corelist --diff 5.20.0 5.22.0
.Ve

You can substitute your favorite version in place of 5.20.0, too.

Some notable changes include:

- \(bu
Archive::Tar has been upgraded to version 2.04.
.Sp
Tests can now be run in parallel.

- \(bu
attributes has been upgraded to version 0.27.
.Sp
The usage of \f(CW\*(C`memEQs\*(C' in the \s-1XS\s0 has been corrected.
[\s-1GH\s0 #14072] <https://github.com/Perl/perl5/issues/14072>
.Sp
Avoid reading beyond the end of a buffer. [perl #122629]

- \(bu
B has been upgraded to version 1.58.
.Sp
It provides a new \f(CW\*(C`B::safename\*(C' function, based on the existing
\f(CW\*(C`B::GV->SAFENAME\*(C', that converts \f(CW\*(C`\\cOPEN\*(C' to \f(CW\*(C`^OPEN\*(C'.
.Sp
Nulled COPs are now of class \f(CW\*(C`B::COP\*(C', rather than \f(CW\*(C`B::OP\*(C'.
.Sp
\f(CW\*(C`B::REGEXP\*(C' objects now provide a \f(CW\*(C`qr_anoncv\*(C' method for accessing the
implicit \s-1CV\s0 associated with \f(CW\*(C`qr//\*(C' things containing code blocks, and a
\f(CW\*(C`compflags\*(C' method that returns the pertinent flags originating from the
\f(CW\*(C`qr//blahblah\*(C' op.
.Sp
\f(CW\*(C`B::PMOP\*(C' now provides a \f(CW\*(C`pmregexp\*(C' method returning a \f(CW\*(C`B::REGEXP\*(C' object.
Two new classes, \f(CW\*(C`B::PADNAME\*(C' and \f(CW\*(C`B::PADNAMELIST\*(C', have been introduced.
.Sp
A bug where, after an ithread creation or pseudofork, special/immortal SVs in
the child ithread/pseudoprocess did not have the correct class of
\f(CW\*(C`B::SPECIAL\*(C', has been fixed.
The \f(CW\*(C`id\*(C' and \f(CW\*(C`outid\*(C' \s-1PADLIST\s0 methods have been added.

- \(bu
B::Concise has been upgraded to version 0.996.
.Sp
Null ops that are part of the execution chain are now given sequence
numbers.
.Sp
Private flags for nulled ops are now dumped with mnemonics as they would be
for the non-nulled counterparts.

- \(bu
B::Deparse has been upgraded to version 1.35.
.Sp
It now deparses \f(CW\*(C`+sub : attr \{ ... \}\*(C' correctly at the start of a
statement.  Without the initial \f(CW\*(C`+\*(C', \f(CW\*(C`sub\*(C' would be a statement label.
.Sp
\f(CW\*(C`BEGIN\*(C' blocks are now emitted in the right place most of the time, but
the change unfortunately introduced a regression, in that \f(CW\*(C`BEGIN\*(C' blocks
occurring just before the end of the enclosing block may appear below it
instead.
.Sp
\f(CW\*(C`B::Deparse\*(C' no longer puts erroneous \f(CW\*(C`local\*(C' here and there, such as for
\f(CW\*(C`LIST = tr/a//d\*(C'.  [perl #119815]
.Sp
Adjacent \f(CW\*(C`use\*(C' statements are no longer accidentally nested if one
contains a \f(CW\*(C`do\*(C' block.  [perl #115066]
.Sp
Parenthesised arrays in lists passed to \f(CW\*(C`\\\*(C' are now correctly deparsed
with parentheses (*e.g.*, \f(CW\*(C`\\(@a, (@b), @c)\*(C' now retains the parentheses
around \f(CW@b), thus preserving the flattening behavior of referenced
parenthesised arrays.  Formerly, it only worked for one array: \f(CW\*(C`\\(@a)\*(C'.
.Sp
\f(CW\*(C`local our\*(C' is now deparsed correctly, with the \f(CW\*(C`our\*(C' included.
.Sp
\f(CW\*(C`for($foo; !$bar; $baz) \{...\}\*(C' was deparsed without the \f(CW\*(C`!\*(C' (or \f(CW\*(C`not\*(C').
This has been fixed.
.Sp
Core keywords that conflict with lexical subroutines are now deparsed with
the \f(CW\*(C`CORE::\*(C' prefix.
.Sp
\f(CW\*(C`foreach state $x (...) \{...\}\*(C' now deparses correctly with \f(CW\*(C`state\*(C' and
not \f(CW\*(C`my\*(C'.
.Sp
\f(CW\*(C`our @array = split(...)\*(C' now deparses correctly with \f(CW\*(C`our\*(C' in those
cases where the assignment is optimized away.
.Sp
It now deparses \f(CW\*(C`our(\f(CILIST\f(CW)\*(C' and typed lexical (\f(CW\*(C`my Dog $spot\*(C') correctly.
.Sp
Deparse \f(CW$#_ as that instead of as \f(CW$#\{_\}.
[\s-1GH\s0 #14545] <https://github.com/Perl/perl5/issues/14545>
.Sp
\s-1BEGIN\s0 blocks at the end of the enclosing scope are now deparsed in the
right place.  [perl #77452]
.Sp
\s-1BEGIN\s0 blocks were sometimes deparsed as _\|_ANON_\|_, but are now always called
\s-1BEGIN.\s0
.Sp
Lexical subroutines are now fully deparsed.  [perl #116553]
.Sp
\f(CW\*(C`Anything =~ y///r\*(C' with \f(CW\*(C`/r\*(C' no longer omits the left-hand operand.
.Sp
The op trees that make up regexp code blocks are now deparsed for real.
Formerly, the original string that made up the regular expression was used.
That caused problems with \f(CW\*(C`qr/(?\{<<heredoc\})/\*(C' and multiline code blocks,
which were deparsed incorrectly.  [perl #123217] [perl #115256]
.Sp
\f(CW$; at the end of a statement no longer loses its semicolon.
[perl #123357]
.Sp
Some cases of subroutine declarations stored in the stash in shorthand form
were being omitted.
.Sp
Non-ASCII characters are now consistently escaped in strings, instead of
some of the time.  (There are still outstanding problems with regular
expressions and identifiers that have not been fixed.)
.Sp
When prototype sub calls are deparsed with \f(CW\*(C`&\*(C' (*e.g.*, under the **-P**
option), \f(CW\*(C`scalar\*(C' is now added where appropriate, to force the scalar
context implied by the prototype.
.Sp
\f(CW\*(C`require(foo())\*(C', \f(CW\*(C`do(foo())\*(C', \f(CW\*(C`goto(foo())\*(C' and similar constructs with
loop controls are now deparsed correctly.  The outer parentheses are not
optional.
.Sp
Whitespace is no longer escaped in regular expressions, because it was
getting erroneously escaped within \f(CW\*(C`(?x:...)\*(C' sections.
.Sp
\f(CW\*(C`sub foo \{ foo() \}\*(C' is now deparsed with those mandatory parentheses.
.Sp
\f(CW\*(C`/@array/\*(C' is now deparsed as a regular expression, and not just
\f(CW@array.
.Sp
\f(CW\*(C`/@\{-\}/\*(C', \f(CW\*(C`/@\{+\}/\*(C' and \f(CW$#\{1\} are now deparsed with the braces, which
are mandatory in these cases.
.Sp
In deparsing feature bundles, \f(CW\*(C`B::Deparse\*(C' was emitting \f(CW\*(C`no feature;\*(C' first
instead of \f(CW\*(C`no feature \*(Aq:all\*(Aq;\*(C'.  This has been fixed.
.Sp
\f(CW\*(C`chdir FH\*(C' is now deparsed without quotation marks.
.Sp
\f(CW\*(C`\\my @a\*(C' is now deparsed without parentheses.  (Parenthese would flatten
the array.)
.Sp
\f(CW\*(C`system\*(C' and \f(CW\*(C`exec\*(C' followed by a block are now deparsed correctly.
Formerly there was an erroneous \f(CW\*(C`do\*(C' before the block.
.Sp
\f(CW\*(C`use constant QR => qr/.../flags\*(C' followed by \f(CW\*(C`"" =~ QR\*(C' is no longer
without the flags.
.Sp
Deparsing \f(CW\*(C`BEGIN \{ undef &foo \}\*(C' with the **-w** switch enabled started to
emit 'uninitialized' warnings in Perl 5.14.  This has been fixed.
.Sp
Deparsing calls to subs with a \f(CW\*(C`(;+)\*(C' prototype resulted in an infinite
loop.  The \f(CW\*(C`(;$\*(C') \f(CW\*(C`(_)\*(C' and \f(CW\*(C`(;_)\*(C' prototypes were given the wrong
precedence, causing \f(CW\*(C`foo($a<$b)\*(C' to be deparsed without the parentheses.
.Sp
Deparse now provides a defined state sub in inner subs.

- \(bu
B::Op_private has been added.
.Sp
B::Op_private provides detailed information about the flags used in the
\f(CW\*(C`op_private\*(C' field of perl opcodes.

- \(bu
bigint, bignum, bigrat have been upgraded to version 0.39.
.Sp
Document in \s-1CAVEATS\s0 that using strings as numbers won't always invoke
the big number overloading, and how to invoke it.  [rt.perl.org #123064]

- \(bu
Carp has been upgraded to version 1.36.
.Sp
\f(CW\*(C`Carp::Heavy\*(C' now ignores version mismatches with Carp if Carp is newer
than 1.12, since \f(CW\*(C`Carp::Heavy\*(C''s guts were merged into Carp at that
point.
[\s-1GH\s0 #13708] <https://github.com/Perl/perl5/issues/13708>
.Sp
Carp now handles non-ASCII platforms better.
.Sp
Off-by-one error fix for Perl < 5.14.

- \(bu
constant has been upgraded to version 1.33.
.Sp
It now accepts fully-qualified constant names, allowing constants to be defined
in packages other than the caller.

- \(bu
\s-1CPAN\s0 has been upgraded to version 2.11.
.Sp
Add support for \f(CW\*(C`Cwd::getdcwd()\*(C' and introduce workaround for a misbehavior
seen on Strawberry Perl 5.20.1.
.Sp
Fix \f(CW\*(C`chdir()\*(C' after building dependencies bug.
.Sp
Introduce experimental support for plugins/hooks.
.Sp
Integrate the \f(CW\*(C`App::Cpan\*(C' sources.
.Sp
Do not check recursion on optional dependencies.
.Sp
Sanity check *\s-1META\s0.yml* to contain a hash.
[cpan #95271] <https://rt.cpan.org/Ticket/Display.html?id=95271>

- \(bu
CPAN::Meta::Requirements has been upgraded to version 2.132.
.Sp
Works around limitations in \f(CW\*(C`version::vpp\*(C' detecting v-string magic and adds
support for forthcoming ExtUtils::MakeMaker bootstrap *version.pm* for
Perls older than 5.10.0.

- \(bu
Data::Dumper has been upgraded to version 2.158.
.Sp
Fixes \s-1CVE-2014-4330\s0 by adding a configuration variable/option to limit
recursion when dumping deep data structures.
.Sp
Changes to resolve Coverity issues.
\s-1XS\s0 dumps incorrectly stored the name of code references stored in a
\s-1GLOB.\s0
[\s-1GH\s0 #13911] <https://github.com/Perl/perl5/issues/13911>

- \(bu
DynaLoader has been upgraded to version 1.32.
.Sp
Remove \f(CW\*(C`dl_nonlazy\*(C' global if unused in Dynaloader. [perl #122926]

- \(bu
Encode has been upgraded to version 2.72.
.Sp
\f(CW\*(C`piconv\*(C' now has better error handling when the encoding name is nonexistent,
and a build breakage when upgrading Encode in perl-5.8.2 and earlier has
been fixed.
.Sp
Building in \*(C+ mode on Windows now works.

- \(bu
Errno has been upgraded to version 1.23.
.Sp
Add \f(CW\*(C`-P\*(C' to the preprocessor command-line on \s-1GCC 5.\s0  \s-1GCC\s0 added extra
line directives, breaking parsing of error code definitions.  [rt.perl.org
#123784]

- \(bu
experimental has been upgraded to version 0.013.
.Sp
Hardcodes features for Perls older than 5.15.7.

- \(bu
ExtUtils::CBuilder has been upgraded to version 0.280221.
.Sp
Fixes a regression on Android.
[\s-1GH\s0 #14064] <https://github.com/Perl/perl5/issues/14064>

- \(bu
ExtUtils::Manifest has been upgraded to version 1.70.
.Sp
Fixes a bug with \f(CW\*(C`maniread()\*(C''s handling of quoted filenames and improves
\f(CW\*(C`manifind()\*(C' to follow symlinks.
[\s-1GH\s0 #14003] <https://github.com/Perl/perl5/issues/14003>

- \(bu
ExtUtils::ParseXS has been upgraded to version 3.28.
.Sp
Only declare \f(CW\*(C`file\*(C' unused if we actually define it.
Improve generated \f(CW\*(C`RETVAL\*(C' code generation to avoid repeated
references to \f(CWST(0).  [perl #123278]
Broaden and document the \f(CW\*(C`/OBJ$/\*(C' to \f(CW\*(C`/REF$/\*(C' typemap optimization
for the \f(CW\*(C`DESTROY\*(C' method.  [perl #123418]

- \(bu
Fcntl has been upgraded to version 1.13.
.Sp
Add support for the Linux pipe buffer size \f(CW\*(C`fcntl()\*(C' commands.

- \(bu
File::Find has been upgraded to version 1.29.
.Sp
\f(CW\*(C`find()\*(C' and \f(CW\*(C`finddepth()\*(C' will now warn if passed inappropriate or
misspelled options.

- \(bu
File::Glob has been upgraded to version 1.24.
.Sp
Avoid \f(CW\*(C`SvIV()\*(C' expanding to call \f(CW\*(C`get_sv()\*(C' three times in a few
places. [perl #123606]

- \(bu
HTTP::Tiny has been upgraded to version 0.054.
.Sp
\f(CW\*(C`keep_alive\*(C' is now fork-safe and thread-safe.

- \(bu
\s-1IO\s0 has been upgraded to version 1.35.
.Sp
The \s-1XS\s0 implementation has been fixed for the sake of older Perls.

- \(bu
IO::Socket has been upgraded to version 1.38.
.Sp
Document the limitations of the \f(CW\*(C`connected()\*(C' method.  [perl #123096]

- \(bu
IO::Socket::IP has been upgraded to version 0.37.
.Sp
A better fix for subclassing \f(CW\*(C`connect()\*(C'.
[cpan #95983] <https://rt.cpan.org/Ticket/Display.html?id=95983>
[cpan #97050] <https://rt.cpan.org/Ticket/Display.html?id=97050>
.Sp
Implements Timeout for \f(CW\*(C`connect()\*(C'.
[cpan #92075] <https://rt.cpan.org/Ticket/Display.html?id=92075>

- \(bu
The libnet collection of modules has been upgraded to version 3.05.
.Sp
Support for IPv6 and \s-1SSL\s0 to \f(CW\*(C`Net::FTP\*(C', \f(CW\*(C`Net::NNTP\*(C', \f(CW\*(C`Net::POP3\*(C' and \f(CW\*(C`Net::SMTP\*(C'.
Improvements in \f(CW\*(C`Net::SMTP\*(C' authentication.

- \(bu
Locale::Codes has been upgraded to version 3.34.
.Sp
Fixed a bug in the scripts used to extract data from spreadsheets that
prevented the \s-1SHP\s0 currency code from being found.
[cpan #94229] <https://rt.cpan.org/Ticket/Display.html?id=94229>
.Sp
New codes have been added.

- \(bu
Math::BigInt has been upgraded to version 1.9997.
.Sp
Synchronize \s-1POD\s0 changes from the \s-1CPAN\s0 release.
\f(CW\*(C`Math::BigFloat->blog(x)\*(C' would sometimes return \f(CW\*(C`blog(2*x)\*(C' when
the accuracy was greater than 70 digits.
The result of \f(CW\*(C`Math::BigFloat->bdiv()\*(C' in list context now
satisfies \f(CW\*(C`x = quotient * divisor + remainder\*(C'.
.Sp
Correct handling of subclasses.
[cpan #96254] <https://rt.cpan.org/Ticket/Display.html?id=96254>
[cpan #96329] <https://rt.cpan.org/Ticket/Display.html?id=96329>

- \(bu
Module::Metadata has been upgraded to version 1.000026.
.Sp
Support installations on older perls with an ExtUtils::MakeMaker earlier
than 6.63_03

- \(bu
overload has been upgraded to version 1.26.
.Sp
A redundant \f(CW\*(C`ref $sub\*(C' check has been removed.

- \(bu
The PathTools module collection has been upgraded to version 3.56.
.Sp
A warning from the **gcc** compiler is now avoided when building the \s-1XS.\s0
.Sp
Don't turn leading \f(CW\*(C`//\*(C' into \f(CW\*(C`/\*(C' on Cygwin. [perl #122635]

- \(bu
perl5db.pl has been upgraded to version 1.49.
.Sp
The debugger would cause an assertion failure.
[\s-1GH\s0 #14605] <https://github.com/Perl/perl5/issues/14605>
.Sp
\f(CW\*(C`fork()\*(C' in the debugger under \f(CW\*(C`tmux\*(C' will now create a new window for
the forked process. [\s-1GH\s0 #13602] <https://github.com/Perl/perl5/issues/13602>
.Sp
The debugger now saves the current working directory on startup and
restores it when you restart your program with \f(CW\*(C`R\*(C' or \f(CW\*(C`rerun\*(C'.
[\s-1GH\s0 #13691] <https://github.com/Perl/perl5/issues/13691>

- \(bu
PerlIO::scalar has been upgraded to version 0.22.
.Sp
Reading from a position well past the end of the scalar now correctly
returns end of file.  [perl #123443]
.Sp
Seeking to a negative position still fails, but no longer leaves the
file position set to a negation location.
.Sp
\f(CW\*(C`eof()\*(C' on a \f(CW\*(C`PerlIO::scalar\*(C' handle now properly returns true when
the file position is past the 2GB mark on 32-bit systems.
.Sp
Attempting to write at file positions impossible for the platform now
fail early rather than wrapping at 4GB.

- \(bu
Pod::Perldoc has been upgraded to version 3.25.
.Sp
Filehandles opened for reading or writing now have \f(CW\*(C`:encoding(UTF-8)\*(C' set.
[cpan #98019] <https://rt.cpan.org/Ticket/Display.html?id=98019>

- \(bu
\s-1POSIX\s0 has been upgraded to version 1.53.
.Sp
The C99 math functions and constants (for example \f(CW\*(C`acosh\*(C', \f(CW\*(C`isinf\*(C', \f(CW\*(C`isnan\*(C', \f(CW\*(C`round\*(C',
\f(CW\*(C`trunc\*(C'; \f(CW\*(C`M_E\*(C', \f(CW\*(C`M_SQRT2\*(C', \f(CW\*(C`M_PI\*(C') have been added.
.Sp
\f(CW\*(C`POSIX::tmpnam()\*(C' now produces a deprecation warning.  [perl #122005]

- \(bu
Safe has been upgraded to version 2.39.
.Sp
\f(CW\*(C`reval\*(C' was not propagating void context properly.

- \(bu
Scalar-List-Utils has been upgraded to version 1.41.
.Sp
A new module, Sub::Util, has been added, containing functions related to
\s-1CODE\s0 refs, including \f(CW\*(C`subname\*(C' (inspired by \f(CW\*(C`Sub::Identity\*(C') and \f(CW\*(C`set_subname\*(C'
(copied and renamed from \f(CW\*(C`Sub::Name\*(C').
The use of \f(CW\*(C`GetMagic\*(C' in \f(CW\*(C`List::Util::reduce()\*(C' has also been fixed.
[cpan #63211] <https://rt.cpan.org/Ticket/Display.html?id=63211>

- \(bu
SDBM_File has been upgraded to version 1.13.
.Sp
Simplified the build process.  [perl #123413]

- \(bu
Time::Piece has been upgraded to version 1.29.
.Sp
When pretty printing negative \f(CW\*(C`Time::Seconds\*(C', the \*(L"minus\*(R" is no longer lost.

- \(bu
Unicode::Collate has been upgraded to version 1.12.
.Sp
Version 0.67's improved discontiguous contractions is invalidated by default
and is supported as a parameter \f(CW\*(C`long_contraction\*(C'.

- \(bu
Unicode::Normalize has been upgraded to version 1.18.
.Sp
The \s-1XSUB\s0 implementation has been removed in favor of pure Perl.

- \(bu
Unicode::UCD has been upgraded to version 0.61.
.Sp
A new function **property_values()**
has been added to return a given property's possible values.
.Sp
A new function **charprop()**
has been added to return the value of a given property for a given code
point.
.Sp
A new function **charprops_all()**
has been added to return the values of all Unicode properties for a
given code point.
.Sp
A bug has been fixed so that **propaliases()**
returns the correct short and long names for the Perl extensions where
it was incorrect.
.Sp
A bug has been fixed so that
**prop_value_aliases()**
returns \f(CW\*(C`undef\*(C' instead of a wrong result for properties that are Perl
extensions.
.Sp
This module now works on \s-1EBCDIC\s0 platforms.

- \(bu
utf8 has been upgraded to version 1.17
.Sp
A mismatch between the documentation and the code in \f(CW\*(C`utf8::downgrade()\*(C'
was fixed in favor of the documentation. The optional second argument
is now correctly treated as a perl boolean (true/false semantics) and
not as an integer.

- \(bu
version has been upgraded to version 0.9909.
.Sp
Numerous changes.  See the *Changes* file in the \s-1CPAN\s0 distribution for
details.

- \(bu
Win32 has been upgraded to version 0.51.
.Sp
\f(CW\*(C`GetOSName()\*(C' now supports Windows 8.1, and building in \*(C+ mode now works.

- \(bu
Win32API::File has been upgraded to version 0.1202
.Sp
Building in \*(C+ mode now works.

- \(bu
XSLoader has been upgraded to version 0.20.
.Sp
Allow XSLoader to load modules from a different namespace.
[perl #122455]

### Removed Modules and Pragmata

Subsection "Removed Modules and Pragmata"
The following modules (and associated modules) have been removed from the core
perl distribution:

- \(bu
\s-1CGI\s0

- \(bu
Module::Build

## Documentation

Header "Documentation"

### New Documentation

Subsection "New Documentation"
*perlunicook*
Subsection "perlunicook"

This document, by Tom Christiansen, provides examples of handling Unicode in
Perl.

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perlaix*
Subsection "perlaix"

- \(bu
A note on long doubles has been added.

*perlapi*
Subsection "perlapi"

- \(bu
Note that \f(CW\*(C`SvSetSV\*(C' doesn't do set magic.

- \(bu
\f(CW\*(C`sv_usepvn_flags\*(C' - fix documentation to mention the use of \f(CW\*(C`Newx\*(C' instead of
\f(CW\*(C`malloc\*(C'.
.Sp
[\s-1GH\s0 #13835] <https://github.com/Perl/perl5/issues/13835>

- \(bu
Clarify where \f(CW\*(C`NUL\*(C' may be embedded or is required to terminate a string.

- \(bu
Some documentation that was previously missing due to formatting errors is
now included.

- \(bu
Entries are now organized into groups rather than by the file where they
are found.

- \(bu
Alphabetical sorting of entries is now done consistently (automatically
by the \s-1POD\s0 generator) to make entries easier to find when scanning.

*perldata*
Subsection "perldata"

- \(bu
The syntax of single-character variable names has been brought
up-to-date and more fully explained.

- \(bu
Hexadecimal floating point numbers are described, as are infinity and
NaN.

*perlebcdic*
Subsection "perlebcdic"

- \(bu
This document has been significantly updated in the light of recent
improvements to \s-1EBCDIC\s0 support.

*perlfilter*
Subsection "perlfilter"

- \(bu
Added a \s-1LIMITATIONS\s0 section.

*perlfunc*
Subsection "perlfunc"

- \(bu
Mention that \f(CW\*(C`study()\*(C' is currently a no-op.

- \(bu
Calling \f(CW\*(C`delete\*(C' or \f(CW\*(C`exists\*(C' on array values is now described as \*(L"strongly
discouraged\*(R" rather than \*(L"deprecated\*(R".

- \(bu
Improve documentation of \f(CW\*(C`our\*(C'.

- \(bu
\f(CW\*(C`-l\*(C' now notes that it will return false if symlinks aren't supported by the
file system.
[\s-1GH\s0 #13695] <https://github.com/Perl/perl5/issues/13695>

- \(bu
Note that \f(CW\*(C`exec LIST\*(C' and \f(CW\*(C`system LIST\*(C' may fall back to the shell on
Win32. Only the indirect-object syntax \f(CW\*(C`exec PROGRAM LIST\*(C' and
\f(CW\*(C`system PROGRAM LIST\*(C' will reliably avoid using the shell.
.Sp
This has also been noted in perlport.
.Sp
[\s-1GH\s0 #13907] <https://github.com/Perl/perl5/issues/13907>

*perlguts*
Subsection "perlguts"

- \(bu
The \s-1OOK\s0 example has been updated to account for \s-1COW\s0 changes and a change in the
storage of the offset.

- \(bu
Details on C level symbols and libperl.t added.

- \(bu
Information on Unicode handling has been added

- \(bu
Information on \s-1EBCDIC\s0 handling has been added

*perlhack*
Subsection "perlhack"

- \(bu
A note has been added about running on platforms with non-ASCII
character sets

- \(bu
A note has been added about performance testing

*perlhacktips*
Subsection "perlhacktips"

- \(bu
Documentation has been added illustrating the perils of assuming that
there is no change to the contents of static memory pointed to by the
return values of Perl's wrappers for C library functions.

- \(bu
Replacements for \f(CW\*(C`tmpfile\*(C', \f(CW\*(C`atoi\*(C', \f(CW\*(C`strtol\*(C', and \f(CW\*(C`strtoul\*(C' are now
recommended.

- \(bu
Updated documentation for the \f(CW\*(C`test.valgrind\*(C' \f(CW\*(C`make\*(C' target.
[\s-1GH\s0 #13658] <https://github.com/Perl/perl5/issues/13658>

- \(bu
Information is given about writing test files portably to non-ASCII
platforms.

- \(bu
A note has been added about how to get a C language stack backtrace.

*perlhpux*
Subsection "perlhpux"

- \(bu
Note that the message \*(L"Redeclaration of \*(R"sendpath\*(L" with a different
storage class specifier\*(R" is harmless.

*perllocale*
Subsection "perllocale"

- \(bu
Updated for the enhancements in v5.22, along with some clarifications.

*perlmodstyle*
Subsection "perlmodstyle"

- \(bu
Instead of pointing to the module list, we are now pointing to
PrePAN <http://prepan.org/>.

*perlop*
Subsection "perlop"

- \(bu
Updated for the enhancements in v5.22, along with some clarifications.

*perlpodspec*
Subsection "perlpodspec"

- \(bu
The specification of the pod language is changing so that the default
encoding of pods that aren't in \s-1UTF-8\s0 (unless otherwise indicated) is
\s-1CP1252\s0 instead of \s-1ISO 8859-1\s0 (Latin1).

*perlpolicy*
Subsection "perlpolicy"

- \(bu
We now have a code of conduct for the *p5p* mailing list, as documented
in \*(L"\s-1STANDARDS OF CONDUCT\*(R"\s0 in perlpolicy.

- \(bu
The conditions for marking an experimental feature as non-experimental are now
set out.

- \(bu
Clarification has been made as to what sorts of changes are permissible in
maintenance releases.

*perlport*
Subsection "perlport"

- \(bu
Out-of-date VMS-specific information has been fixed and/or simplified.

- \(bu
Notes about \s-1EBCDIC\s0 have been added.

*perlre*
Subsection "perlre"

- \(bu
The description of the \f(CW\*(C`/x\*(C' modifier has been clarified to note that
comments cannot be continued onto the next line by escaping them; and
there is now a list of all the characters that are considered whitespace
by this modifier.

- \(bu
The new \f(CW\*(C`/n\*(C' modifier is described.

- \(bu
A note has been added on how to make bracketed character class ranges
portable to non-ASCII machines.

*perlrebackslash*
Subsection "perlrebackslash"

- \(bu
Added documentation of \f(CW\*(C`\\b\{sb\}\*(C', \f(CW\*(C`\\b\{wb\}\*(C', \f(CW\*(C`\\b\{gcb\}\*(C', and \f(CW\*(C`\\b\{g\}\*(C'.

*perlrecharclass*
Subsection "perlrecharclass"

- \(bu
Clarifications have been added to \*(L"Character Ranges\*(R" in perlrecharclass
to the effect \f(CW\*(C`[A-Z]\*(C', \f(CW\*(C`[a-z]\*(C', \f(CW\*(C`[0-9]\*(C' and
any subranges thereof in regular expression bracketed character classes
are guaranteed to match exactly what a naive English speaker would
expect them to match, even on platforms (such as \s-1EBCDIC\s0) where perl
has to do extra work to accomplish this.

- \(bu
The documentation of Bracketed Character Classes has been expanded to cover the
improvements in \f(CW\*(C`qr/[\\N\{named sequence\}]/\*(C' (see under \*(L"Selected Bug Fixes\*(R").

*perlref*
Subsection "perlref"

- \(bu
A new section has been added
Assigning to References

*perlsec*
Subsection "perlsec"

- \(bu
Comments added on algorithmic complexity and tied hashes.

*perlsyn*
Subsection "perlsyn"

- \(bu
An ambiguity in the documentation of the \f(CW\*(C`...\*(C' statement has been corrected.
[\s-1GH\s0 #14054] <https://github.com/Perl/perl5/issues/14054>

- \(bu
The empty conditional in \f(CW\*(C`for\*(C' and \f(CW\*(C`while\*(C' is now documented
in perlsyn.

*perlunicode*
Subsection "perlunicode"

- \(bu
This has had extensive revisions to bring it up-to-date with current
Unicode support and to make it more readable.  Notable is that Unicode
7.0 changed what it should do with non-characters.  Perl retains the old
way of handling for reasons of backward compatibility.  See
\*(L"Noncharacter code points\*(R" in perlunicode.

*perluniintro*
Subsection "perluniintro"

- \(bu
Advice for how to make sure your strings and regular expression patterns are
interpreted as Unicode has been updated.

*perlvar*
Subsection "perlvar"

- \(bu
\f(CW$] is no longer listed as being deprecated.  Instead, discussion has
been added on the advantages and disadvantages of using it versus
\f(CW$^V.  \f(CW$OLD_PERL_VERSION was re-added to the documentation as the long
form of \f(CW$].

- \(bu
\f(CW\*(C`$\{^ENCODING\}\*(C' is now marked as deprecated.

- \(bu
The entry for \f(CW\*(C`%^H\*(C' has been clarified to indicate it can only handle
simple values.

*perlvms*
Subsection "perlvms"

- \(bu
Out-of-date and/or incorrect material has been removed.

- \(bu
Updated documentation on environment and shell interaction in \s-1VMS.\s0

*perlxs*
Subsection "perlxs"

- \(bu
Added a discussion of locale issues in \s-1XS\s0 code.

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### New Diagnostics

Subsection "New Diagnostics"
*New Errors*
Subsection "New Errors"

- \(bu
Bad symbol for scalar
.Sp
(P) An internal request asked to add a scalar entry to something that
wasn't a symbol table entry.

- \(bu
Can't use a hash as a reference
.Sp
(F) You tried to use a hash as a reference, as in
\f(CW\*(C`%foo->\{"bar"\}\*(C' or \f(CW\*(C`%$ref->\{"hello"\}\*(C'.  Versions of perl <= 5.6.1
used to allow this syntax, but shouldn't have.

- \(bu
Can't use an array as a reference
.Sp
(F) You tried to use an array as a reference, as in
\f(CW\*(C`@foo->[23]\*(C' or \f(CW\*(C`@$ref->[99]\*(C'.  Versions of perl <= 5.6.1 used to
allow this syntax, but shouldn't have.

- \(bu
Can't use 'defined(@array)' (Maybe you should just omit the **defined()**?)
.Sp
(F) \f(CW\*(C`defined()\*(C' is not useful on arrays because it
checks for an undefined *scalar* value.  If you want to see if the
array is empty, just use \f(CW\*(C`if\ (@array)\ \{\ #\ not\ empty\ \}\*(C' for example.

- \(bu
Can't use 'defined(%hash)' (Maybe you should just omit the **defined()**?)
.Sp
(F) \f(CW\*(C`defined()\*(C' is not usually right on hashes.
.Sp
Although \f(CW\*(C`defined\ %hash\*(C' is false on a plain not-yet-used hash, it
becomes true in several non-obvious circumstances, including iterators,
weak references, stash names, even remaining true after \f(CW\*(C`undef\ %hash\*(C'.
These things make \f(CW\*(C`defined\ %hash\*(C' fairly useless in practice, so it now
generates a fatal error.
.Sp
If a check for non-empty is what you wanted then just put it in boolean
context (see \*(L"Scalar values\*(R" in perldata):
.Sp
.Vb 3
    if (%hash) \{
       # not empty
    \}
.Ve
.Sp
If you had \f(CW\*(C`defined\ %Foo::Bar::QUUX\*(C' to check whether such a package
variable exists then that's never really been reliable, and isn't
a good way to enquire about the features of a package, or whether
it's loaded, etc.

- \(bu
Cannot chr \f(CW%f
.Sp
(F) You passed an invalid number (like an infinity or not-a-number) to
\f(CW\*(C`chr\*(C'.

- \(bu
Cannot compress \f(CW%f in pack
.Sp
(F) You tried converting an infinity or not-a-number to an unsigned
character, which makes no sense.

- \(bu
Cannot pack \f(CW%f with '%c'
.Sp
(F) You tried converting an infinity or not-a-number to a character,
which makes no sense.

- \(bu
Cannot print \f(CW%f with '%c'
.Sp
(F) You tried printing an infinity or not-a-number as a character (\f(CW%c),
which makes no sense.  Maybe you meant \f(CW\*(Aq%s\*(Aq, or just stringifying it?

- \(bu
charnames alias definitions may not contain a sequence of multiple spaces
.Sp
(F) You defined a character name which had multiple space
characters in a row.  Change them to single spaces.  Usually these
names are defined in the \f(CW\*(C`:alias\*(C' import argument to \f(CW\*(C`use charnames\*(C', but
they could be defined by a translator installed into \f(CW$^H\{charnames\}.
See \*(L"\s-1CUSTOM ALIASES\*(R"\s0 in charnames.

- \(bu
charnames alias definitions may not contain trailing white-space
.Sp
(F) You defined a character name which ended in a space
character.  Remove the trailing space(s).  Usually these names are
defined in the \f(CW\*(C`:alias\*(C' import argument to \f(CW\*(C`use charnames\*(C', but they
could be defined by a translator installed into \f(CW$^H\{charnames\}.
See \*(L"\s-1CUSTOM ALIASES\*(R"\s0 in charnames.

- \(bu
:const is not permitted on named subroutines
.Sp
(F) The \f(CW\*(C`const\*(C' attribute causes an anonymous subroutine to be run and
its value captured at the time that it is cloned.  Named subroutines are
not cloned like this, so the attribute does not make sense on them.

- \(bu
Hexadecimal float: internal error
.Sp
(F) Something went horribly bad in hexadecimal float handling.

- \(bu
Hexadecimal float: unsupported long double format
.Sp
(F) You have configured Perl to use long doubles but
the internals of the long double format are unknown,
therefore the hexadecimal float output is impossible.

- \(bu
Illegal suidscript
.Sp
(F) The script run under suidperl was somehow illegal.

- \(bu
In '(?...)', the '(' and '?' must be adjacent in regex; marked by <--\ \s-1HERE\s0 in m/%s/
.Sp
(F) The two-character sequence \f(CW"(?" in
this context in a regular expression pattern should be an
indivisible token, with nothing intervening between the \f(CW"("
and the \f(CW"?", but you separated them.

- \(bu
In '(*VERB...)', the '(' and '*' must be adjacent in regex; marked by <--\ \s-1HERE\s0 in m/%s/
.Sp
(F) The two-character sequence \f(CW"(*" in
this context in a regular expression pattern should be an
indivisible token, with nothing intervening between the \f(CW"("
and the \f(CW"*", but you separated them.

- \(bu
Invalid quantifier in \{,\} in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
(F) The pattern looks like a \{min,max\} quantifier, but the min or max could not
be parsed as a valid number: either it has leading zeroes, or it represents
too big a number to cope with.  The <--\ \s-1HERE\s0 shows where in the regular
expression the problem was discovered.  See perlre.

- \(bu
'%s' is an unknown bound type in regex
.Sp
(F) You used \f(CW\*(C`\\b\{...\}\*(C' or \f(CW\*(C`\\B\{...\}\*(C' and the \f(CW\*(C`...\*(C' is not known to
Perl.  The current valid ones are given in
\*(L"\\b\{\}, \\b, \\B\{\}, \\B\*(R" in perlrebackslash.

- \(bu
Missing or undefined argument to require
.Sp
(F) You tried to call \f(CW\*(C`require\*(C' with no argument or with an undefined
value as an argument.  \f(CW\*(C`require\*(C' expects either a package name or a
file-specification as an argument.  See \*(L"require\*(R" in perlfunc.
.Sp
Formerly, \f(CW\*(C`require\*(C' with no argument or \f(CW\*(C`undef\*(C' warned about a Null filename.

*New Warnings*
Subsection "New Warnings"

- \(bu
\\C is deprecated in regex
.Sp
(D deprecated) The \f(CW\*(C`/\\C/\*(C' character class was deprecated in v5.20, and
now emits a warning. It is intended that it will become an error in v5.24.
This character class matches a single byte even if it appears within a
multi-byte character, breaks encapsulation, and can corrupt \s-1UTF-8\s0
strings.

- \(bu
\*(L"%s\*(R" is more clearly written simply as \*(L"%s\*(R" in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
(W regexp) (only under \f(CW\*(C`use\ re\ \*(Aqstrict\*(Aq\*(C' or within \f(CW\*(C`(?[...])\*(C')
.Sp
You specified a character that has the given plainer way of writing it,
and which is also portable to platforms running with different character
sets.

- \(bu
Argument \*(L"%s\*(R" treated as 0 in increment (++)
.Sp
(W numeric) The indicated string was fed as an argument to the \f(CW\*(C`++\*(C' operator
which expects either a number or a string matching \f(CW\*(C`/^[a-zA-Z]*[0-9]*\\z/\*(C'.
See \*(L"Auto-increment and Auto-decrement\*(R" in perlop for details.

- \(bu
Both or neither range ends should be Unicode in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
(W regexp) (only under \f(CW\*(C`use\ re\ \*(Aqstrict\*(Aq\*(C' or within \f(CW\*(C`(?[...])\*(C')
.Sp
In a bracketed character class in a regular expression pattern, you
had a range which has exactly one end of it specified using \f(CW\*(C`\\N\{\}\*(C', and
the other end is specified using a non-portable mechanism.  Perl treats
the range as a Unicode range, that is, all the characters in it are
considered to be the Unicode characters, and which may be different code
points on some platforms Perl runs on.  For example, \f(CW\*(C`[\\N\{U+06\}-\\x08]\*(C'
is treated as if you had instead said \f(CW\*(C`[\\N\{U+06\}-\\N\{U+08\}]\*(C', that is it
matches the characters whose code points in Unicode are 6, 7, and 8.
But that \f(CW\*(C`\\x08\*(C' might indicate that you meant something different, so
the warning gets raised.

- \(bu
Can't do %s(\*(L"%s\*(R") on non-UTF-8 locale; resolved to \*(L"%s\*(R".
.Sp
(W locale) You are 1) running under "\f(CW\*(C`use locale\*(C'"; 2) the current
locale is not a \s-1UTF-8\s0 one; 3) you tried to do the designated case-change
operation on the specified Unicode character; and 4) the result of this
operation would mix Unicode and locale rules, which likely conflict.
.Sp
The warnings category \f(CW\*(C`locale\*(C' is new.

- \(bu
:const is experimental
.Sp
(S experimental::const_attr) The \f(CW\*(C`const\*(C' attribute is experimental.
If you want to use the feature, disable the warning with \f(CW\*(C`no warnings
\*(Aqexperimental::const_attr\*(Aq\*(C', but know that in doing so you are taking
the risk that your code may break in a future Perl version.

- \(bu
gmtime(%f) failed
.Sp
(W overflow) You called \f(CW\*(C`gmtime\*(C' with a number that it could not handle:
too large, too small, or NaN.  The returned value is \f(CW\*(C`undef\*(C'.

- \(bu
Hexadecimal float: exponent overflow
.Sp
(W overflow) The hexadecimal floating point has larger exponent
than the floating point supports.

- \(bu
Hexadecimal float: exponent underflow
.Sp
(W overflow) The hexadecimal floating point has smaller exponent
than the floating point supports.

- \(bu
Hexadecimal float: mantissa overflow
.Sp
(W overflow) The hexadecimal floating point literal had more bits in
the mantissa (the part between the \f(CW\*(C`0x\*(C' and the exponent, also known as
the fraction or the significand) than the floating point supports.

- \(bu
Hexadecimal float: precision loss
.Sp
(W overflow) The hexadecimal floating point had internally more
digits than could be output.  This can be caused by unsupported
long double formats, or by 64-bit integers not being available
(needed to retrieve the digits under some configurations).

- \(bu
Locale '%s' may not work well.%s
.Sp
(W locale) You are using the named locale, which is a non-UTF-8 one, and
which perl has determined is not fully compatible with what it can
handle.  The second \f(CW%s gives a reason.
.Sp
The warnings category \f(CW\*(C`locale\*(C' is new.

- \(bu
localtime(%f) failed
.Sp
(W overflow) You called \f(CW\*(C`localtime\*(C' with a number that it could not handle:
too large, too small, or NaN.  The returned value is \f(CW\*(C`undef\*(C'.

- \(bu
Negative repeat count does nothing
.Sp
(W numeric) You tried to execute the
\f(CW\*(C`x\*(C' repetition operator fewer than 0
times, which doesn't make sense.

- \(bu
NO-BREAK \s-1SPACE\s0 in a charnames alias definition is deprecated
.Sp
(D deprecated) You defined a character name which contained a no-break
space character.  Change it to a regular space.  Usually these names are
defined in the \f(CW\*(C`:alias\*(C' import argument to \f(CW\*(C`use charnames\*(C', but they
could be defined by a translator installed into \f(CW$^H\{charnames\}.  See
\*(L"\s-1CUSTOM ALIASES\*(R"\s0 in charnames.

- \(bu
Non-finite repeat count does nothing
.Sp
(W numeric) You tried to execute the
\f(CW\*(C`x\*(C' repetition operator \f(CW\*(C`Inf\*(C' (or
\f(CW\*(C`-Inf\*(C') or NaN times, which doesn't make sense.

- \(bu
PerlIO layer ':win32' is experimental
.Sp
(S experimental::win32_perlio) The \f(CW\*(C`:win32\*(C' PerlIO layer is
experimental.  If you want to take the risk of using this layer,
simply disable this warning:
.Sp
.Vb 1
    no warnings "experimental::win32_perlio";
.Ve

- \(bu
Ranges of \s-1ASCII\s0 printables should be some subset of \*(L"0-9\*(R", \*(L"A-Z\*(R", or \*(L"a-z\*(R" in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
(W regexp) (only under \f(CW\*(C`use\ re\ \*(Aqstrict\*(Aq\*(C' or within \f(CW\*(C`(?[...])\*(C')
.Sp
Stricter rules help to find typos and other errors.  Perhaps you didn't
even intend a range here, if the \f(CW"-" was meant to be some other
character, or should have been escaped (like \f(CW"\\-").  If you did
intend a range, the one that was used is not portable between \s-1ASCII\s0 and
\s-1EBCDIC\s0 platforms, and doesn't have an obvious meaning to a casual
reader.
.Sp
.Vb 7
 [3-7]    # OK; Obvious and portable
 [d-g]    # OK; Obvious and portable
 [A-Y]    # OK; Obvious and portable
 [A-z]    # WRONG; Not portable; not clear what is meant
 [a-Z]    # WRONG; Not portable; not clear what is meant
 [%-.]    # WRONG; Not portable; not clear what is meant
 [\\x41-Z] # WRONG; Not portable; not obvious to non-geek
.Ve
.Sp
(You can force portability by specifying a Unicode range, which means that
the endpoints are specified by
\f(CW\*(C`\\N\{...\}\*(C', but the meaning may
still not be obvious.)
The stricter rules require that ranges that start or stop with an \s-1ASCII\s0
character that is not a control have all their endpoints be a literal
character, and not some escape sequence (like \f(CW"\\x41"), and the ranges
must be all digits, or all uppercase letters, or all lowercase letters.

- \(bu
Ranges of digits should be from the same group in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
(W regexp) (only under \f(CW\*(C`use\ re\ \*(Aqstrict\*(Aq\*(C' or within \f(CW\*(C`(?[...])\*(C')
.Sp
Stricter rules help to find typos and other errors.  You included a
range, and at least one of the end points is a decimal digit.  Under the
stricter rules, when this happens, both end points should be digits in
the same group of 10 consecutive digits.

- \(bu
Redundant argument in \f(CW%s
.Sp
(W redundant) You called a function with more arguments than were
needed, as indicated by information within other arguments you supplied
(*e.g*. a printf format). Currently only emitted when a printf-type format
required fewer arguments than were supplied, but might be used in the
future for *e.g.* \*(L"pack\*(R" in perlfunc.
.Sp
The warnings category \f(CW\*(C`redundant\*(C' is new. See also
[\s-1GH\s0 #13534] <https://github.com/Perl/perl5/issues/13534>.

- \(bu
Replacement list is longer than search list
.Sp
This is not a new diagnostic, but in earlier releases was accidentally
not displayed if the transliteration contained wide characters.  This is
now fixed, so that you may see this diagnostic in places where you
previously didn't (but should have).

- \(bu
Use of \\b\{\} for non-UTF-8 locale is wrong.  Assuming a \s-1UTF-8\s0 locale
.Sp
(W locale) You are matching a regular expression using locale rules,
and a Unicode boundary is being matched, but the locale is not a Unicode
one.  This doesn't make sense.  Perl will continue, assuming a Unicode
(\s-1UTF-8\s0) locale, but the results could well be wrong except if the locale
happens to be \s-1ISO-8859-1\s0 (Latin1) where this message is spurious and can
be ignored.
.Sp
The warnings category \f(CW\*(C`locale\*(C' is new.

- \(bu
Using /u for '%s' instead of /%s in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
(W regexp) You used a Unicode boundary (\f(CW\*(C`\\b\{...\}\*(C' or \f(CW\*(C`\\B\{...\}\*(C') in a
portion of a regular expression where the character set modifiers \f(CW\*(C`/a\*(C'
or \f(CW\*(C`/aa\*(C' are in effect.  These two modifiers indicate an \s-1ASCII\s0
interpretation, and this doesn't make sense for a Unicode definition.
The generated regular expression will compile so that the boundary uses
all of Unicode.  No other portion of the regular expression is affected.

- \(bu
The bitwise feature is experimental
.Sp
(S experimental::bitwise) This warning is emitted if you use bitwise
operators (\f(CW\*(C`& | ^ ~ &. |. ^. ~.\*(C') with the \*(L"bitwise\*(R" feature enabled.
Simply suppress the warning if you want to use the feature, but know
that in doing so you are taking the risk of using an experimental
feature which may change or be removed in a future Perl version:
.Sp
.Vb 3
    no warnings "experimental::bitwise";
    use feature "bitwise";
    $x |.= $y;
.Ve

- \(bu
Unescaped left brace in regex is deprecated, passed through in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
(D deprecated, regexp) You used a literal \f(CW"\{" character in a regular
expression pattern. You should change to use \f(CW"\\\{" instead, because a future
version of Perl (tentatively v5.26) will consider this to be a syntax error.  If
the pattern delimiters are also braces, any matching right brace
(\f(CW"\}") should also be escaped to avoid confusing the parser, for
example,
.Sp
.Vb 1
    qr\{abc\\\{def\\\}ghi\}
.Ve

- \(bu
Use of literal non-graphic characters in variable names is deprecated
.Sp
(D deprecated) Using literal non-graphic (including control)
characters in the source to refer to the *^FOO* variables, like \f(CW$^X and
\f(CW\*(C`$\{^GLOBAL_PHASE\}\*(C' is now deprecated.

- \(bu
Useless use of attribute \*(L"const\*(R"
.Sp
(W misc) The \f(CW\*(C`const\*(C' attribute has no effect except
on anonymous closure prototypes.  You applied it to
a subroutine via attributes.pm.  This is only useful
inside an attribute handler for an anonymous subroutine.

- \(bu
Useless use of /d modifier in transliteration operator
.Sp
This is not a new diagnostic, but in earlier releases was accidentally
not displayed if the transliteration contained wide characters.  This is
now fixed, so that you may see this diagnostic in places where you
previously didn't (but should have).

- \(bu
\*(L"use re 'strict'\*(R" is experimental
.Sp
(S experimental::re_strict) The things that are different when a regular
expression pattern is compiled under \f(CW\*(Aqstrict\*(Aq are subject to change
in future Perl releases in incompatible ways; there are also proposals
to change how to enable strict checking instead of using this subpragma.
This means that a pattern that compiles today may not in a future Perl
release.  This warning is to alert you to that risk.

- \(bu
Warning: unable to close filehandle properly: \f(CW%s
.Sp
Warning: unable to close filehandle \f(CW%s properly: \f(CW%s
.Sp
(S io) Previously, perl silently ignored any errors when doing an implicit
close of a filehandle, *i.e.* where the reference count of the filehandle
reached zero and the user's code hadn't already called \f(CW\*(C`close()\*(C'; *e.g.*
.Sp
.Vb 4
    \{
        open my $fh, \*(Aq>\*(Aq, $file  or die "open: \*(Aq$file\*(Aq: $!\\n";
        print $fh, $data  or die;
    \} # implicit close here
.Ve
.Sp
In a situation such as disk full, due to buffering, the error may only be
detected during the final close, so not checking the result of the close is
dangerous.
.Sp
So perl now warns in such situations.

- \(bu
Wide character (U+%X) in \f(CW%s
.Sp
(W locale) While in a single-byte locale (*i.e.*, a non-UTF-8
one), a multi-byte character was encountered.   Perl considers this
character to be the specified Unicode code point.  Combining non-UTF-8
locales and Unicode is dangerous.  Almost certainly some characters
will have two different representations.  For example, in the \s-1ISO 8859-7\s0
(Greek) locale, the code point 0xC3 represents a Capital Gamma.  But so
also does 0x393.  This will make string comparisons unreliable.
.Sp
You likely need to figure out how this multi-byte character got mixed up
with your single-byte locale (or perhaps you thought you had a \s-1UTF-8\s0
locale, but Perl disagrees).
.Sp
The warnings category \f(CW\*(C`locale\*(C' is new.

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
<> should be quotes
.Sp
This warning has been changed to
<> at require-statement should be quotes
to make the issue more identifiable.

- \(bu
Argument \*(L"%s\*(R" isn't numeric%s
.Sp
The perldiag entry for this warning has added this clarifying note:
.Sp
.Vb 4
 Note that for the Inf and NaN (infinity and not-a-number) the
 definition of "numeric" is somewhat unusual: the strings themselves
 (like "Inf") are considered numeric, and anything following them is
 considered non-numeric.
.Ve

- \(bu
Global symbol \*(L"%s\*(R" requires explicit package name
.Sp
This message has had '(did you forget to declare \*(L"my \f(CW%s\*(R"?)' appended to it, to
make it more helpful to new Perl programmers.
[\s-1GH\s0 #13732] <https://github.com/Perl/perl5/issues/13732>

- \(bu
'\*(L"my\*(R" variable &foo::bar can't be in a package' has been reworded to say
'subroutine' instead of 'variable'.

- \(bu
\\N\{\} in character class restricted to one character in regex; marked by
<--\ \s-1HERE\s0 in m/%s/
.Sp
This message has had *character class* changed to \fIinverted character
class or as a range end-point is to reflect improvements in
\f(CW\*(C`qr/[\\N\{named sequence\}]/\*(C' (see under \*(L"Selected Bug Fixes\*(R").

- \(bu
panic: frexp
.Sp
This message has had ': \f(CW%f' appended to it, to show what the offending
floating point number is.

- \(bu
*Possible precedence problem on bitwise \f(CI%c\fI operator* reworded as
Possible precedence problem on bitwise \f(CW%s operator.

- \(bu
Unsuccessful \f(CW%s on filename containing newline
.Sp
This warning is now only produced when the newline is at the end of
the filename.

- \(bu
"Variable \f(CW%s will not stay shared\*(L" has been changed to say \*(R"Subroutine"
when it is actually a lexical sub that will not stay shared.

- \(bu
Variable length lookbehind not implemented in regex m/%s/
.Sp
The perldiag entry for this warning has had information about Unicode
behavior added.

### Diagnostic Removals

Subsection "Diagnostic Removals"

- \(bu
\*(L"Ambiguous use of -foo resolved as -&**foo()**\*(R"
.Sp
There is actually no ambiguity here, and this impedes the use of negated
constants; *e.g.*, \f(CW\*(C`-Inf\*(C'.

- \(bu
\*(L"Constant is not a \s-1FOO\s0 reference\*(R"
.Sp
Compile-time checking of constant dereferencing (*e.g.*, \f(CW\*(C`my_constant->()\*(C')
has been removed, since it was not taking overloading into account.
[\s-1GH\s0 #9891] <https://github.com/Perl/perl5/issues/9891>
[\s-1GH\s0 #14044] <https://github.com/Perl/perl5/issues/14044>

## Utility Changes

Header "Utility Changes"

### \fIfind2perl, \fIs2p and \fIa2p removal

Subsection "find2perl, s2p and a2p removal"

- \(bu
The *x2p/* directory has been removed from the Perl core.
.Sp
This removes find2perl, s2p and a2p. They have all been released to \s-1CPAN\s0 as
separate distributions (\f(CW\*(C`App::find2perl\*(C', \f(CW\*(C`App::s2p\*(C', \f(CW\*(C`App::a2p\*(C').

### h2ph

Subsection "h2ph"

- \(bu
*h2ph* now handles hexadecimal constants in the compiler's predefined
macro definitions, as visible in \f(CW$Config\{cppsymbols\}.
[\s-1GH\s0 #14491] <https://github.com/Perl/perl5/issues/14491>.

### encguess

Subsection "encguess"

- \(bu
No longer depends on non-core modules.

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
*Configure* now checks for \f(CW\*(C`lrintl()\*(C', \f(CW\*(C`lroundl()\*(C', \f(CW\*(C`llrintl()\*(C', and
\f(CW\*(C`llroundl()\*(C'.

- \(bu
*Configure* with \f(CW\*(C`-Dmksymlinks\*(C' should now be faster.
[\s-1GH\s0 #13890] <https://github.com/Perl/perl5/issues/13890>.

- \(bu
The \f(CW\*(C`pthreads\*(C' and \f(CW\*(C`cl\*(C' libraries will be linked by default if present.
This allows \s-1XS\s0 modules that require threading to work on non-threaded
perls. Note that you must still pass \f(CW\*(C`-Dusethreads\*(C' if you want a
threaded perl.

- \(bu
To get more precision and range for floating point numbers one can now
use the \s-1GCC\s0 quadmath library which implements the quadruple precision
floating point numbers on x86 and \s-1IA-64\s0 platforms.  See *\s-1INSTALL\s0* for
details.

- \(bu
MurmurHash64A and MurmurHash64B can now be configured as the internal hash
function.

- \(bu
\f(CW\*(C`make test.valgrind\*(C' now supports parallel testing.
.Sp
For example:
.Sp
.Vb 1
    TEST_JOBS=9 make test.valgrind
.Ve
.Sp
See \*(L"valgrind\*(R" in perlhacktips for more information.
.Sp
[\s-1GH\s0 #13658] <https://github.com/Perl/perl5/issues/13658>

- \(bu
The \s-1MAD\s0 (Misc Attribute Decoration) build option has been removed
.Sp
This was an unmaintained attempt at preserving
the Perl parse tree more faithfully so that automatic conversion of
Perl 5 to Perl 6 would have been easier.
.Sp
This build-time configuration option had been unmaintained for years,
and had probably seriously diverged on both Perl 5 and Perl 6 sides.

- \(bu
A new compilation flag, \f(CW\*(C`-DPERL_OP_PARENT\*(C' is available. For details,
see the discussion below at \*(L"Internal Changes\*(R".

- \(bu
Pathtools no longer tries to load \s-1XS\s0 on miniperl. This speeds up building perl
slightly.

## Testing

Header "Testing"

- \(bu
*t/porting/re_context.t* has been added to test that utf8 and its
dependencies only use the subset of the \f(CW\*(C`$1..$n\*(C' capture vars that
\f(CW\*(C`Perl_save_re_context()\*(C' is hard-coded to localize, because that function
has no efficient way of determining at runtime what vars to localize.

- \(bu
Tests for performance issues have been added in the file *t/perf/taint.t*.

- \(bu
Some regular expression tests are written in such a way that they will
run very slowly if certain optimizations break. These tests have been
moved into new files, *t/re/speed.t* and *t/re/speed_thr.t*,
and are run with a \f(CW\*(C`watchdog()\*(C'.

- \(bu
\f(CW\*(C`test.pl\*(C' now allows \f(CW\*(C`plan skip_all => $reason\*(C', to make it
more compatible with \f(CW\*(C`Test::More\*(C'.

- \(bu
A new test script, *op/infnan.t*, has been added to test if infinity and NaN are
working correctly.  See \*(L"Infinity and NaN (not-a-number) handling improved\*(R".

## Platform Support

Header "Platform Support"

### Regained Platforms

Subsection "Regained Platforms"

- \s-1IRIX\s0 and Tru64 platforms are working again.
Item "IRIX and Tru64 platforms are working again."
Some \f(CW\*(C`make test\*(C' failures remain:
[\s-1GH\s0 #14557] <https://github.com/Perl/perl5/issues/14557>
and [\s-1GH\s0 #14727] <https://github.com/Perl/perl5/issues/14727>
for \s-1IRIX\s0; [\s-1GH\s0 #14629] <https://github.com/Perl/perl5/issues/14629>,
[cpan #99605] <https://rt.cpan.org/Public/Bug/Display.html?id=99605>, and
[cpan #104836] <https://rt.cpan.org/Ticket/Display.html?id=104836> for Tru64.

- z/OS running \s-1EBCDIC\s0 Code Page 1047
Item "z/OS running EBCDIC Code Page 1047"
Core perl now works on this \s-1EBCDIC\s0 platform.  Earlier perls also worked, but,
even though support wasn't officially withdrawn, recent perls would not compile
and run well.  Perl 5.20 would work, but had many bugs which have now been
fixed.  Many \s-1CPAN\s0 modules that ship with Perl still fail tests, including
\f(CW\*(C`Pod::Simple\*(C'.  However the version of \f(CW\*(C`Pod::Simple\*(C' currently on \s-1CPAN\s0 should work;
it was fixed too late to include in Perl 5.22.  Work is under way to fix many
of the still-broken \s-1CPAN\s0 modules, which likely will be installed on \s-1CPAN\s0 when
completed, so that you may not have to wait until Perl 5.24 to get a working
version.

### Discontinued Platforms

Subsection "Discontinued Platforms"

- NeXTSTEP/OPENSTEP
Item "NeXTSTEP/OPENSTEP"
NeXTSTEP was a proprietary operating system bundled with NeXT's
workstations in the early to mid 90s; \s-1OPENSTEP\s0 was an \s-1API\s0 specification
that provided a NeXTSTEP-like environment on a non-NeXTSTEP system.  Both
are now long dead, so support for building Perl on them has been removed.

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- \s-1EBCDIC\s0
Item "EBCDIC"
Special handling is required of the perl interpreter on \s-1EBCDIC\s0 platforms
to get \f(CW\*(C`qr/[i-j]/\*(C' to match only \f(CW"i" and \f(CW"j", since there are 7
characters between the
code points for \f(CW"i" and \f(CW"j".  This special handling had only been
invoked when both ends of the range are literals.  Now it is also
invoked if any of the \f(CW\*(C`\\N\{...\}\*(C' forms for specifying a character by
name or Unicode code point is used instead of a literal.  See
\*(L"Character Ranges\*(R" in perlrecharclass.

- HP-UX
Item "HP-UX"
The archname now distinguishes use64bitint from use64bitall.

- Android
Item "Android"
Build support has been improved for cross-compiling in general and for
Android in particular.

- \s-1VMS\s0
Item "VMS"

> 0

- \(bu
.PD
When spawning a subprocess without waiting, the return value is now
the correct \s-1PID.\s0

- \(bu
Fix a prototype so linking doesn't fail under the \s-1VMS \*(C+\s0 compiler.

- \(bu
\f(CW\*(C`finite\*(C', \f(CW\*(C`finitel\*(C', and \f(CW\*(C`isfinite\*(C' detection has been added to
\f(CW\*(C`configure.com\*(C', environment handling has had some minor changes, and
a fix for legacy feature checking status.



> 


- Win32
Item "Win32"

> 0

- \(bu
.PD
*miniperl.exe* is now built with \f(CW\*(C`-fno-strict-aliasing\*(C', allowing 64-bit
builds to complete on \s-1GCC 4.8.\s0
[\s-1GH\s0 #14556] <https://github.com/Perl/perl5/issues/14556>

- \(bu
\f(CW\*(C`nmake minitest\*(C' now works on Win32.  Due to dependency issues you
need to build \f(CW\*(C`nmake test-prep\*(C' first, and a small number of the
tests fail.
[\s-1GH\s0 #14318] <https://github.com/Perl/perl5/issues/14318>

- \(bu
Perl can now be built in \*(C+ mode on Windows by setting the makefile macro
\f(CW\*(C`USE_CPLUSPLUS\*(C' to the value \*(L"define\*(R".

- \(bu
The list form of piped open has been implemented for Win32.  Note: unlike
\f(CW\*(C`system LIST\*(C' this does not fall back to the shell.
[\s-1GH\s0 #13574] <https://github.com/Perl/perl5/issues/13574>

- \(bu
New \f(CW\*(C`DebugSymbols\*(C' and \f(CW\*(C`DebugFull\*(C' configuration options added to
Windows makefiles.

- \(bu
Previously, compiling \s-1XS\s0 modules (including \s-1CPAN\s0 ones) using Visual \*(C+ for
Win64 resulted in around a dozen warnings per file from *hv_func.h*.  These
warnings have been silenced.

- \(bu
Support for building without PerlIO has been removed from the Windows
makefiles.  Non-PerlIO builds were all but deprecated in Perl 5.18.0 and are
already not supported by *Configure* on \s-1POSIX\s0 systems.

- \(bu
Between 2 and 6 milliseconds and seven I/O calls have been saved per attempt
to open a perl module for each path in \f(CW@INC.

- \(bu
Intel C builds are now always built with C99 mode on.

- \(bu
\f(CW%I64d is now being used instead of \f(CW%lld for MinGW.

- \(bu
In the experimental \f(CW\*(C`:win32\*(C' layer, a crash in \f(CW\*(C`open\*(C' was fixed. Also
opening */dev/null* (which works under Win32 Perl's default \f(CW\*(C`:unix\*(C'
layer) was implemented for \f(CW\*(C`:win32\*(C'.
[\s-1GH\s0 #13968] <https://github.com/Perl/perl5/issues/13968>

- \(bu
A new makefile option, \f(CW\*(C`USE_LONG_DOUBLE\*(C', has been added to the Windows
dmake makefile for gcc builds only.  Set this to \*(L"define\*(R" if you want perl to
use long doubles to give more accuracy and range for floating point numbers.



> 


- OpenBSD
Item "OpenBSD"
On OpenBSD, Perl will now default to using the system \f(CW\*(C`malloc\*(C' due to the
security features it provides. Perl's own malloc wrapper has been in use
since v5.14 due to performance reasons, but the OpenBSD project believes
the tradeoff is worth it and would prefer that users who need the speed
specifically ask for it.
.Sp
[\s-1GH\s0 #13888] <https://github.com/Perl/perl5/issues/13888>.

- Solaris
Item "Solaris"

> 0

- \(bu
.PD
We now look for the Sun Studio compiler in both */opt/solstudio** and
*/opt/solarisstudio**.

- \(bu
Builds on Solaris 10 with \f(CW\*(C`-Dusedtrace\*(C' would fail early since make
didn't follow implied dependencies to build \f(CW\*(C`perldtrace.h\*(C'.  Added an
explicit dependency to \f(CW\*(C`depend\*(C'.
[\s-1GH\s0 #13334] <https://github.com/Perl/perl5/issues/13334>

- \(bu
C99 options have been cleaned up; hints look for \f(CW\*(C`solstudio\*(C'
as well as \f(CW\*(C`SUNWspro\*(C'; and support for native \f(CW\*(C`setenv\*(C' has been added.



> 


## Internal Changes

Header "Internal Changes"

- \(bu
Experimental support has been added to allow ops in the optree to locate
their parent, if any. This is enabled by the non-default build option
\f(CW\*(C`-DPERL_OP_PARENT\*(C'. It is envisaged that this will eventually become
enabled by default, so \s-1XS\s0 code which directly accesses the \f(CW\*(C`op_sibling\*(C'
field of ops should be updated to be future-proofed.
.Sp
On \f(CW\*(C`PERL_OP_PARENT\*(C' builds, the \f(CW\*(C`op_sibling\*(C' field has been renamed
\f(CW\*(C`op_sibparent\*(C' and a new flag, \f(CW\*(C`op_moresib\*(C', added. On the last op in a
sibling chain, \f(CW\*(C`op_moresib\*(C' is false and \f(CW\*(C`op_sibparent\*(C' points to the
parent (if any) rather than being \f(CW\*(C`NULL\*(C'.
.Sp
To make existing code work transparently whether using \f(CW\*(C`PERL_OP_PARENT\*(C'
or not, a number of new macros and functions have been added that should
be used, rather than directly manipulating \f(CW\*(C`op_sibling\*(C'.
.Sp
For the case of just reading \f(CW\*(C`op_sibling\*(C' to determine the next sibling,
two new macros have been added. A simple scan through a sibling chain
like this:
.Sp
.Vb 1
    for (; kid->op_sibling; kid = kid->op_sibling) \{ ... \}
.Ve
.Sp
should now be written as:
.Sp
.Vb 1
    for (; OpHAS_SIBLING(kid); kid = OpSIBLING(kid)) \{ ... \}
.Ve
.Sp
For altering optrees, a general-purpose function \f(CW\*(C`op_sibling_splice()\*(C'
has been added, which allows for manipulation of a chain of sibling ops.
By analogy with the Perl function \f(CW\*(C`splice()\*(C', it allows you to cut out
zero or more ops from a sibling chain and replace them with zero or more
new ops.  It transparently handles all the updating of sibling, parent,
op_last pointers etc.
.Sp
If you need to manipulate ops at a lower level, then three new macros,
\f(CW\*(C`OpMORESIB_set\*(C', \f(CW\*(C`OpLASTSIB_set\*(C' and \f(CW\*(C`OpMAYBESIB_set\*(C' are intended to
be a low-level portable way to set \f(CW\*(C`op_sibling\*(C' / \f(CW\*(C`op_sibparent\*(C' while
also updating \f(CW\*(C`op_moresib\*(C'.  The first sets the sibling pointer to a new
sibling, the second makes the op the last sibling, and the third
conditionally does the first or second action.  Note that unlike
\f(CW\*(C`op_sibling_splice()\*(C' these macros won't maintain consistency in the
parent at the same time (*e.g.* by updating \f(CW\*(C`op_first\*(C' and \f(CW\*(C`op_last\*(C' where
appropriate).
.Sp
A C-level \f(CW\*(C`Perl_op_parent()\*(C' function and a Perl-level \f(CW\*(C`B::OP::parent()\*(C'
method have been added. The C function only exists under
\f(CW\*(C`PERL_OP_PARENT\*(C' builds (using it is build-time error on vanilla
perls).  \f(CW\*(C`B::OP::parent()\*(C' exists always, but on a vanilla build it
always returns \f(CW\*(C`NULL\*(C'. Under \f(CW\*(C`PERL_OP_PARENT\*(C', they return the parent
of the current op, if any. The variable \f(CW$B::OP::does_parent allows you
to determine whether \f(CW\*(C`B\*(C' supports retrieving an op's parent.
.Sp
\f(CW\*(C`PERL_OP_PARENT\*(C' was introduced in 5.21.2, but the interface was
changed considerably in 5.21.11. If you updated your code before the
5.21.11 changes, it may require further revision. The main changes after
5.21.2 were:

> 
- \(bu
The \f(CW\*(C`OP_SIBLING\*(C' and \f(CW\*(C`OP_HAS_SIBLING\*(C' macros have been renamed
\f(CW\*(C`OpSIBLING\*(C' and \f(CW\*(C`OpHAS_SIBLING\*(C' for consistency with other
op-manipulating macros.

- \(bu
The \f(CW\*(C`op_lastsib\*(C' field has been renamed \f(CW\*(C`op_moresib\*(C', and its meaning
inverted.

- \(bu
The macro \f(CW\*(C`OpSIBLING_set\*(C' has been removed, and has been superseded by
\f(CW\*(C`OpMORESIB_set\*(C' *et al*.

- \(bu
The \f(CW\*(C`op_sibling_splice()\*(C' function now accepts a null \f(CW\*(C`parent\*(C' argument
where the splicing doesn't affect the first or last ops in the sibling
chain



> 


- \(bu
Macros have been created to allow \s-1XS\s0 code to better manipulate the \s-1POSIX\s0 locale
category \f(CW\*(C`LC_NUMERIC\*(C'.  See \*(L"Locale-related functions and macros\*(R" in perlapi.

- \(bu
The previous \f(CW\*(C`atoi\*(C' *et al* replacement function, \f(CW\*(C`grok_atou\*(C', has now been
superseded by \f(CW\*(C`grok_atoUV\*(C'.  See perlclib for details.

- \(bu
A new function, \f(CW\*(C`Perl_sv_get_backrefs()\*(C', has been added which allows you
retrieve the weak references, if any, which point at an \s-1SV.\s0

- \(bu
The \f(CW\*(C`screaminstr()\*(C' function has been removed. Although marked as
public \s-1API,\s0 it was undocumented and had no usage in \s-1CPAN\s0 modules. Calling
it has been fatal since 5.17.0.

- \(bu
The \f(CW\*(C`newDEFSVOP()\*(C', \f(CW\*(C`block_start()\*(C', \f(CW\*(C`block_end()\*(C' and \f(CW\*(C`intro_my()\*(C'
functions have been added to the \s-1API.\s0

- \(bu
The internal \f(CW\*(C`convert\*(C' function in *op.c* has been renamed
\f(CW\*(C`op_convert_list\*(C' and added to the \s-1API.\s0

- \(bu
The \f(CW\*(C`sv_magic()\*(C' function no longer forbids \*(L"ext\*(R" magic on read-only
values.  After all, perl can't know whether the custom magic will modify
the \s-1SV\s0 or not.
[\s-1GH\s0 #14202] <https://github.com/Perl/perl5/issues/14202>.

- \(bu
Accessing \*(L"CvPADLIST\*(R" in perlapi on an \s-1XSUB\s0 is now forbidden.
.Sp
The \f(CW\*(C`CvPADLIST\*(C' field has been reused for a different internal purpose
for XSUBs. So in particular, you can no longer rely on it being \s-1NULL\s0 as a
test of whether a \s-1CV\s0 is an \s-1XSUB.\s0 Use \f(CW\*(C`CvISXSUB()\*(C' instead.

- \(bu
SVs of type \f(CW\*(C`SVt_NV\*(C' are now sometimes bodiless when the build
configuration and platform allow it: specifically, when \f(CW\*(C`sizeof(NV) <=
sizeof(IV)\*(C'. \*(L"Bodiless\*(R" means that the \s-1NV\s0 value is stored directly in
the head of an \s-1SV,\s0 without requiring a separate body to be allocated. This
trick has already been used for IVs since 5.9.2 (though in the case of
IVs, it is always used, regardless of platform and build configuration).

- \(bu
The \f(CW$DB::single, \f(CW$DB::signal and \f(CW$DB::trace variables now have set- and
get-magic that stores their values as IVs, and those IVs are used when
testing their values in \f(CW\*(C`pp_dbstate()\*(C'.  This prevents perl from
recursing infinitely if an overloaded object is assigned to any of those
variables.
[\s-1GH\s0 #14013] <https://github.com/Perl/perl5/issues/14013>.

- \(bu
\f(CW\*(C`Perl_tmps_grow()\*(C', which is marked as public \s-1API\s0 but is undocumented, has
been removed from the public \s-1API.\s0 This change does not affect \s-1XS\s0 code that
uses the \f(CW\*(C`EXTEND_MORTAL\*(C' macro to pre-extend the mortal stack.

- \(bu
Perl's internals no longer sets or uses the \f(CW\*(C`SVs_PADMY\*(C' flag.
\f(CW\*(C`SvPADMY()\*(C' now returns a true value for anything not marked \f(CW\*(C`PADTMP\*(C'
and \f(CW\*(C`SVs_PADMY\*(C' is now defined as 0.

- \(bu
The macros \f(CW\*(C`SETsv\*(C' and \f(CW\*(C`SETsvUN\*(C' have been removed. They were no longer used
in the core since commit 6f1401dc2a five years ago, and have not been
found present on \s-1CPAN.\s0

- \(bu
The \f(CW\*(C`SvFAKE\*(C' bit (unused on HVs) got informally reserved by
David Mitchell for future work on vtables.

- \(bu
The \f(CW\*(C`sv_catpvn_flags()\*(C' function accepts \f(CW\*(C`SV_CATBYTES\*(C' and \f(CW\*(C`SV_CATUTF8\*(C'
flags, which specify whether the appended string is bytes or \s-1UTF-8,\s0
respectively. (These flags have in fact been present since 5.16.0, but
were formerly not regarded as part of the \s-1API.\s0)

- \(bu
A new opcode class, \f(CW\*(C`METHOP\*(C', has been introduced. It holds
information used at runtime to improve the performance
of class/object method calls.
.Sp
\f(CW\*(C`OP_METHOD\*(C' and \f(CW\*(C`OP_METHOD_NAMED\*(C' have changed from being
\f(CW\*(C`UNOP/SVOP\*(C' to being \f(CW\*(C`METHOP\*(C'.

- \(bu
\f(CW\*(C`cv_name()\*(C' is a new \s-1API\s0 function that can be passed a \s-1CV\s0 or \s-1GV.\s0  It
returns an \s-1SV\s0 containing the name of the subroutine, for use in
diagnostics.
.Sp
[\s-1GH\s0 #12767] <https://github.com/Perl/perl5/issues/12767>
[\s-1GH\s0 #13392] <https://github.com/Perl/perl5/issues/13392>

- \(bu
\f(CW\*(C`cv_set_call_checker_flags()\*(C' is a new \s-1API\s0 function that works like
\f(CW\*(C`cv_set_call_checker()\*(C', except that it allows the caller to specify
whether the call checker requires a full \s-1GV\s0 for reporting the subroutine's
name, or whether it could be passed a \s-1CV\s0 instead.  Whatever value is
passed will be acceptable to \f(CW\*(C`cv_name()\*(C'.  \f(CW\*(C`cv_set_call_checker()\*(C'
guarantees there will be a \s-1GV,\s0 but it may have to create one on the fly,
which is inefficient.
[\s-1GH\s0 #12767] <https://github.com/Perl/perl5/issues/12767>

- \(bu
\f(CW\*(C`CvGV\*(C' (which is not part of the \s-1API\s0) is now a more complex macro, which may
call a function and reify a \s-1GV.\s0  For those cases where it has been used as a
boolean, \f(CW\*(C`CvHASGV\*(C' has been added, which will return true for CVs that
notionally have GVs, but without reifying the \s-1GV.\s0  \f(CW\*(C`CvGV\*(C' also returns a \s-1GV\s0
now for lexical subs.
[\s-1GH\s0 #13392] <https://github.com/Perl/perl5/issues/13392>

- \(bu
The \*(L"sync_locale\*(R" in perlapi function has been added to the public \s-1API.\s0
Changing the program's locale should be avoided by \s-1XS\s0 code. Nevertheless,
certain non-Perl libraries called from \s-1XS\s0 need to do so, such as \f(CW\*(C`Gtk\*(C'.
When this happens, Perl needs to be told that the locale has
changed.  Use this function to do so, before returning to Perl.

- \(bu
The defines and labels for the flags in the \f(CW\*(C`op_private\*(C' field of OPs are now
auto-generated from data in *regen/op_private*.  The noticeable effect of this
is that some of the flag output of \f(CW\*(C`Concise\*(C' might differ slightly, and the
flag output of \f(CW\*(C`perl\ -Dx\*(C' may differ considerably (they both use the same set
of labels now).  Also, debugging builds now have a new assertion in
\f(CW\*(C`op_free()\*(C' to ensure that the op doesn't have any unrecognized flags set in
\f(CW\*(C`op_private\*(C'.

- \(bu
The deprecated variable \f(CW\*(C`PL_sv_objcount\*(C' has been removed.

- \(bu
Perl now tries to keep the locale category \f(CW\*(C`LC_NUMERIC\*(C' set to \*(L"C\*(R"
except around operations that need it to be set to the program's
underlying locale.  This protects the many \s-1XS\s0 modules that cannot cope
with the decimal radix character not being a dot.  Prior to this
release, Perl initialized this category to \*(L"C\*(R", but a call to
\f(CW\*(C`POSIX::setlocale()\*(C' would change it.  Now such a call will change the
underlying locale of the \f(CW\*(C`LC_NUMERIC\*(C' category for the program, but the
locale exposed to \s-1XS\s0 code will remain \*(L"C\*(R".  There are new macros
to manipulate the \s-1LC_NUMERIC\s0 locale, including
\f(CW\*(C`STORE_LC_NUMERIC_SET_TO_NEEDED\*(C' and
\f(CW\*(C`STORE_LC_NUMERIC_FORCE_TO_UNDERLYING\*(C'.
See \*(L"Locale-related functions and macros\*(R" in perlapi.

- \(bu
A new macro \f(CW\*(C`isUTF8_CHAR\*(C' has been written which
efficiently determines if the string given by its parameters begins
with a well-formed \s-1UTF-8\s0 encoded character.

- \(bu
The following private \s-1API\s0 functions had their context parameter removed:
\f(CW\*(C`Perl_cast_ulong\*(C',  \f(CW\*(C`Perl_cast_i32\*(C', \f(CW\*(C`Perl_cast_iv\*(C',    \f(CW\*(C`Perl_cast_uv\*(C',
\f(CW\*(C`Perl_cv_const_sv\*(C', \f(CW\*(C`Perl_mg_find\*(C',  \f(CW\*(C`Perl_mg_findext\*(C', \f(CW\*(C`Perl_mg_magical\*(C',
\f(CW\*(C`Perl_mini_mktime\*(C', \f(CW\*(C`Perl_my_dirfd\*(C', \f(CW\*(C`Perl_sv_backoff\*(C', \f(CW\*(C`Perl_utf8_hop\*(C'.
.Sp
Note that the prefix-less versions of those functions that are part of the
public \s-1API,\s0 such as \f(CW\*(C`cast_i32()\*(C', remain unaffected.

- \(bu
The \f(CW\*(C`PADNAME\*(C' and \f(CW\*(C`PADNAMELIST\*(C' types are now separate types, and no
longer simply aliases for \s-1SV\s0 and \s-1AV.\s0
[\s-1GH\s0 #14250] <https://github.com/Perl/perl5/issues/14250>.

- \(bu
Pad names are now always \s-1UTF-8.\s0  The \f(CW\*(C`PadnameUTF8\*(C' macro always returns
true.  Previously, this was effectively the case already, but any support
for two different internal representations of pad names has now been
removed.

- \(bu
A new op class, \f(CW\*(C`UNOP_AUX\*(C', has been added. This is a subclass of
\f(CW\*(C`UNOP\*(C' with an \f(CW\*(C`op_aux\*(C' field added, which points to an array of unions
of \s-1UV,\s0 SV* etc. It is intended for where an op needs to store more data
than a simple \f(CW\*(C`op_sv\*(C' or whatever. Currently the only op of this type is
\f(CW\*(C`OP_MULTIDEREF\*(C' (see next item).

- \(bu
A new op has been added, \f(CW\*(C`OP_MULTIDEREF\*(C', which performs one or more
nested array and hash lookups where the key is a constant or simple
variable. For example the expression \f(CW$a[0]\{$k\}[$i], which previously
involved ten \f(CW\*(C`rv2Xv\*(C', \f(CW\*(C`Xelem\*(C', \f(CW\*(C`gvsv\*(C' and \f(CW\*(C`const\*(C' ops is now performed
by a single \f(CW\*(C`multideref\*(C' op. It can also handle \f(CW\*(C`local\*(C', \f(CW\*(C`exists\*(C' and
\f(CW\*(C`delete\*(C'. A non-simple index expression, such as \f(CW\*(C`[$i+1]\*(C' is still done
using \f(CW\*(C`aelem\*(C'/\f(CW\*(C`helem\*(C', and single-level array lookup with a small constant
index is still done using \f(CW\*(C`aelemfast\*(C'.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
\f(CW\*(C`close\*(C' now sets \f(CW$!
.Sp
When an I/O error occurs, the fact that there has been an error is recorded
in the handle.  \f(CW\*(C`close\*(C' returns false for such a handle.  Previously, the
value of \f(CW$! would be untouched by \f(CW\*(C`close\*(C', so the common convention of
writing \f(CW\*(C`close\ $fh\ or\ die\ $!\*(C' did not work reliably.  Now the handle
records the value of \f(CW$!, too, and \f(CW\*(C`close\*(C' restores it.

- \(bu
\f(CW\*(C`no re\*(C' now can turn off everything that \f(CW\*(C`use re\*(C' enables
.Sp
Previously, running \f(CW\*(C`no re\*(C' would turn off only a few things. Now it
can turn off all the enabled things. For example, the only way to
stop debugging, once enabled, was to exit the enclosing block; that is
now fixed.

- \(bu
\f(CW\*(C`pack("D", $x)\*(C' and \f(CW\*(C`pack("F", $x)\*(C' now zero the padding on x86 long
double builds.  Under some build options on \s-1GCC 4.8\s0 and later, they used
to either overwrite the zero-initialized padding, or bypass the
initialized buffer entirely.  This caused *op/pack.t* to fail.
[\s-1GH\s0 #14554] <https://github.com/Perl/perl5/issues/14554>

- \(bu
Extending an array cloned from a parent thread could result in \*(L"Modification of
a read-only value attempted\*(R" errors when attempting to modify the new elements.
[\s-1GH\s0 #14605] <https://github.com/Perl/perl5/issues/14605>

- \(bu
An assertion failure and subsequent crash with \f(CW\*(C`*x=<y>\*(C' has been fixed.
[\s-1GH\s0 #14493] <https://github.com/Perl/perl5/issues/14493>

- \(bu
A possible crashing/looping bug related to compiling lexical subs has been
fixed.
[\s-1GH\s0 #14596] <https://github.com/Perl/perl5/issues/14596>

- \(bu
\s-1UTF-8\s0 now works correctly in function names, in unquoted HERE-document
terminators, and in variable names used as array indexes.
[\s-1GH\s0 #14601] <https://github.com/Perl/perl5/issues/14601>

- \(bu
Repeated global pattern matches in scalar context on large tainted strings were
exponentially slow depending on the current match position in the string.
[\s-1GH\s0 #14238] <https://github.com/Perl/perl5/issues/14238>

- \(bu
Various crashes due to the parser getting confused by syntax errors have been
fixed.
[\s-1GH\s0 #14496] <https://github.com/Perl/perl5/issues/14496>
[\s-1GH\s0 #14497] <https://github.com/Perl/perl5/issues/14497>
[\s-1GH\s0 #14548] <https://github.com/Perl/perl5/issues/14548>
[\s-1GH\s0 #14564] <https://github.com/Perl/perl5/issues/14564>

- \(bu
\f(CW\*(C`split\*(C' in the scope of lexical \f(CW$_ has been fixed not to fail assertions.
[\s-1GH\s0 #14483] <https://github.com/Perl/perl5/issues/14483>

- \(bu
\f(CW\*(C`my $x : attr\*(C' syntax inside various list operators no longer fails
assertions.
[\s-1GH\s0 #14500] <https://github.com/Perl/perl5/issues/14500>

- \(bu
An \f(CW\*(C`@\*(C' sign in quotes followed by a non-ASCII digit (which is not a valid
identifier) would cause the parser to crash, instead of simply trying the
\f(CW\*(C`@\*(C' as literal.  This has been fixed.
[\s-1GH\s0 #14553] <https://github.com/Perl/perl5/issues/14553>

- \(bu
\f(CW\*(C`*bar::=*foo::=*glob_with_hash\*(C' has been crashing since Perl 5.14, but no
longer does.
[\s-1GH\s0 #14512] <https://github.com/Perl/perl5/issues/14512>

- \(bu
\f(CW\*(C`foreach\*(C' in scalar context was not pushing an item on to the stack, resulting
in bugs.  (\f(CW\*(C`print\ 4,\ scalar\ do\ \{\ foreach(@x)\{\}\ \}\ +\ 1\*(C' would print 5.)
It has been fixed to return \f(CW\*(C`undef\*(C'.
[\s-1GH\s0 #14569] <https://github.com/Perl/perl5/issues/14569>

- \(bu
Several cases of data used to store environment variable contents in core C
code being potentially overwritten before being used have been fixed.
[\s-1GH\s0 #14476] <https://github.com/Perl/perl5/issues/14476>

- \(bu
Some patterns starting with \f(CW\*(C`/.*..../\*(C' matched against long strings have
been slow since v5.8, and some of the form \f(CW\*(C`/.*..../i\*(C' have been slow
since v5.18. They are now all fast again.
[\s-1GH\s0 #14475] <https://github.com/Perl/perl5/issues/14475>.

- \(bu
The original visible value of \f(CW$/ is now preserved when it is set to
an invalid value.  Previously if you set \f(CW$/ to a reference to an
array, for example, perl would produce a runtime error and not set
\f(CW\*(C`PL_rs\*(C', but Perl code that checked \f(CW$/ would see the array
reference.
[\s-1GH\s0 #14245] <https://github.com/Perl/perl5/issues/14245>.

- \(bu
In a regular expression pattern, a \s-1POSIX\s0 class, like \f(CW\*(C`[:ascii:]\*(C', must
be inside a bracketed character class, like \f(CW\*(C`qr/[[:ascii:]]/\*(C'.  A
warning is issued when something looking like a \s-1POSIX\s0 class is not
inside a bracketed class.  That warning wasn't getting generated when
the \s-1POSIX\s0 class was negated: \f(CW\*(C`[:^ascii:]\*(C'.  This is now fixed.

- \(bu
Perl 5.14.0 introduced a bug whereby \f(CW\*(C`eval\ \{\ LABEL:\ \}\*(C' would crash.  This
has been fixed.
[\s-1GH\s0 #14438] <https://github.com/Perl/perl5/issues/14438>.

- \(bu
Various crashes due to the parser getting confused by syntax errors have
been fixed.
[\s-1GH\s0 #14421] <https://github.com/Perl/perl5/issues/14421>.
[\s-1GH\s0 #14472] <https://github.com/Perl/perl5/issues/14472>.
[\s-1GH\s0 #14480] <https://github.com/Perl/perl5/issues/14480>.
[\s-1GH\s0 #14447] <https://github.com/Perl/perl5/issues/14447>.

- \(bu
Code like \f(CW\*(C`/$a[/\*(C' used to read the next line of input and treat it as
though it came immediately after the opening bracket.  Some invalid code
consequently would parse and run, but some code caused crashes, so this is
now disallowed.
[\s-1GH\s0 #14462] <https://github.com/Perl/perl5/issues/14462>.

- \(bu
Fix argument underflow for \f(CW\*(C`pack\*(C'.
[\s-1GH\s0 #14525] <https://github.com/Perl/perl5/issues/14525>.

- \(bu
Fix handling of non-strict \f(CW\*(C`\\x\{\}\*(C'. Now \f(CW\*(C`\\x\{\}\*(C' is equivalent to \f(CW\*(C`\\x\{0\}\*(C'
instead of faulting.

- \(bu
\f(CW\*(C`stat -t\*(C' is now no longer treated as stackable, just like \f(CW\*(C`-t stat\*(C'.
[\s-1GH\s0 #14499] <https://github.com/Perl/perl5/issues/14499>.

- \(bu
The following no longer causes a \s-1SEGV:\s0 \f(CW\*(C`qr\{x+(y(?0))*\}\*(C'.

- \(bu
Fixed infinite loop in parsing backrefs in regexp patterns.

- \(bu
Several minor bug fixes in behavior of Infinity and NaN, including
warnings when stringifying Infinity-like or NaN-like strings. For example,
\*(L"NaNcy\*(R" doesn't numify to NaN anymore.

- \(bu
A bug in regular expression patterns that could lead to segfaults and
other crashes has been fixed.  This occurred only in patterns compiled
with \f(CW\*(C`/i\*(C' while taking into account the current \s-1POSIX\s0 locale (which usually
means they have to be compiled within the scope of \f(CW\*(C`use\ locale\*(C'),
and there must be a string of at least 128 consecutive bytes to match.
[\s-1GH\s0 #14389] <https://github.com/Perl/perl5/issues/14389>.

- \(bu
\f(CW\*(C`s///g\*(C' now works on very long strings (where there are more than 2
billion iterations) instead of dying with 'Substitution loop'.
[\s-1GH\s0 #11742] <https://github.com/Perl/perl5/issues/11742>.
[\s-1GH\s0 #14190] <https://github.com/Perl/perl5/issues/14190>.

- \(bu
\f(CW\*(C`gmtime\*(C' no longer crashes with not-a-number values.
[\s-1GH\s0 #14365] <https://github.com/Perl/perl5/issues/14365>.

- \(bu
\f(CW\*(C`\\()\*(C' (a reference to an empty list), and \f(CW\*(C`y///\*(C' with lexical \f(CW$_ in
scope, could both do a bad write past the end of the stack.  They have
both been fixed to extend the stack first.

- \(bu
\f(CW\*(C`prototype()\*(C' with no arguments used to read the previous item on the
stack, so \f(CW\*(C`print\ "foo",\ prototype()\*(C' would print foo's prototype.
It has been fixed to infer \f(CW$_ instead.
[\s-1GH\s0 #14376] <https://github.com/Perl/perl5/issues/14376>.

- \(bu
Some cases of lexical state subs declared inside predeclared subs could
crash, for example when evalling a string including the name of an outer
variable, but no longer do.

- \(bu
Some cases of nested lexical state subs inside anonymous subs could cause
'Bizarre copy' errors or possibly even crashes.

- \(bu
When trying to emit warnings, perl's default debugger (*perl5db.pl*) was
sometimes giving 'Undefined subroutine &DB::db_warn called' instead.  This
bug, which started to occur in Perl 5.18, has been fixed.
[\s-1GH\s0 #14400] <https://github.com/Perl/perl5/issues/14400>.

- \(bu
Certain syntax errors in substitutions, such as \f(CW\*(C`s/$\{<>\{\})//\*(C', would
crash, and had done so since Perl 5.10.  (In some cases the crash did not
start happening till 5.16.)  The crash has, of course, been fixed.
[\s-1GH\s0 #14391] <https://github.com/Perl/perl5/issues/14391>.

- \(bu
Fix a couple of string grow size calculation overflows; in particular,
a repeat expression like \f(CW\*(C`33\ x\ ~3\*(C' could cause a large buffer
overflow since the new output buffer size was not correctly handled by
\f(CW\*(C`SvGROW()\*(C'.  An expression like this now properly produces a memory wrap
panic.
[\s-1GH\s0 #14401] <https://github.com/Perl/perl5/issues/14401>.

- \(bu
\f(CW\*(C`formline("@...", "a");\*(C' would crash.  The \f(CW\*(C`FF_CHECKNL\*(C' case in
\f(CW\*(C`pp_formline()\*(C' didn't set the pointer used to mark the chop position,
which led to the \f(CW\*(C`FF_MORE\*(C' case crashing with a segmentation fault.
This has been fixed.
[\s-1GH\s0 #14388] <https://github.com/Perl/perl5/issues/14388>.

- \(bu
A possible buffer overrun and crash when parsing a literal pattern during
regular expression compilation has been fixed.
[\s-1GH\s0 #14416] <https://github.com/Perl/perl5/issues/14416>.

- \(bu
\f(CW\*(C`fchmod()\*(C' and \f(CW\*(C`futimes()\*(C' now set \f(CW$! when they fail due to being
passed a closed file handle.
[\s-1GH\s0 #14073] <https://github.com/Perl/perl5/issues/14073>.

- \(bu
\f(CW\*(C`op_free()\*(C' and \f(CW\*(C`scalarvoid()\*(C' no longer crash due to a stack overflow
when freeing a deeply recursive op tree.
[\s-1GH\s0 #11866] <https://github.com/Perl/perl5/issues/11866>.

- \(bu
In Perl 5.20.0, \f(CW$^N accidentally had the internal \s-1UTF-8\s0 flag turned off
if accessed from a code block within a regular expression, effectively
UTF-8-encoding the value.  This has been fixed.
[\s-1GH\s0 #14211] <https://github.com/Perl/perl5/issues/14211>.

- \(bu
A failed \f(CW\*(C`semctl\*(C' call no longer overwrites existing items on the stack,
which means that \f(CW\*(C`(semctl(-1,0,0,0))[0]\*(C' no longer gives an
\*(L"uninitialized\*(R" warning.

- \(bu
\f(CW\*(C`else\{foo()\}\*(C' with no space before \f(CW\*(C`foo\*(C' is now better at assigning the
right line number to that statement.
[\s-1GH\s0 #14070] <https://github.com/Perl/perl5/issues/14070>.

- \(bu
Sometimes the assignment in \f(CW\*(C`@array = split\*(C' gets optimised so that \f(CW\*(C`split\*(C'
itself writes directly to the array.  This caused a bug, preventing this
assignment from being used in lvalue context.  So
\f(CW\*(C`(@a=split//,"foo")=bar()\*(C' was an error.  (This bug probably goes back to
Perl 3, when the optimisation was added.) It has now been fixed.
[\s-1GH\s0 #14183] <https://github.com/Perl/perl5/issues/14183>.

- \(bu
When an argument list fails the checks specified by a subroutine
signature (which is still an experimental feature), the resulting error
messages now give the file and line number of the caller, not of the
called subroutine.
[\s-1GH\s0 #13643] <https://github.com/Perl/perl5/issues/13643>.

- \(bu
The flip-flop operators (\f(CW\*(C`..\*(C' and \f(CW\*(C`...\*(C' in scalar context) used to maintain
a separate state for each recursion level (the number of times the
enclosing sub was called recursively), contrary to the documentation.  Now
each closure has one internal state for each flip-flop.
[\s-1GH\s0 #14110] <https://github.com/Perl/perl5/issues/14110>.

- \(bu
The flip-flop operator (\f(CW\*(C`..\*(C' in scalar context) would return the same
scalar each time, unless the containing subroutine was called recursively.
Now it always returns a new scalar.
[\s-1GH\s0 #14110] <https://github.com/Perl/perl5/issues/14110>.

- \(bu
\f(CW\*(C`use\*(C', \f(CW\*(C`no\*(C', statement labels, special blocks (\f(CW\*(C`BEGIN\*(C') and pod are now
permitted as the first thing in a \f(CW\*(C`map\*(C' or \f(CW\*(C`grep\*(C' block, the block after
\f(CW\*(C`print\*(C' or \f(CW\*(C`say\*(C' (or other functions) returning a handle, and within
\f(CW\*(C`$\{...\}\*(C', \f(CW\*(C`@\{...\}\*(C', etc.
[\s-1GH\s0 #14088] <https://github.com/Perl/perl5/issues/14088>.

- \(bu
The repetition operator \f(CW\*(C`x\*(C' now propagates lvalue context to its left-hand
argument when used in contexts like \f(CW\*(C`foreach\*(C'.  That allows
\f(CW\*(C`for(($#that_array)x2)\ \{\ ...\ \}\*(C' to work as expected if the loop modifies
\f(CW$_.

- \(bu
\f(CW\*(C`(...) x ...\*(C' in scalar context used to corrupt the stack if one operand
was an object with \*(L"x\*(R" overloading, causing erratic behavior.
[\s-1GH\s0 #13811] <https://github.com/Perl/perl5/issues/13811>.

- \(bu
Assignment to a lexical scalar is often optimised away; for example in
\f(CW\*(C`my $x; $x = $y + $z\*(C', the assign operator is optimised away and the add
operator writes its result directly to \f(CW$x.  Various bugs related to
this optimisation have been fixed.  Certain operators on the right-hand
side would sometimes fail to assign the value at all or assign the wrong
value, or would call \s-1STORE\s0 twice or not at all on tied variables.  The
operators affected were \f(CW\*(C`$foo++\*(C', \f(CW\*(C`$foo--\*(C', and \f(CW\*(C`-$foo\*(C' under \f(CW\*(C`use
integer\*(C', \f(CW\*(C`chomp\*(C', \f(CW\*(C`chr\*(C' and \f(CW\*(C`setpgrp\*(C'.

- \(bu
List assignments were sometimes buggy if the same scalar ended up on both
sides of the assignment due to use of \f(CW\*(C`tied\*(C', \f(CW\*(C`values\*(C' or \f(CW\*(C`each\*(C'.  The
result would be the wrong value getting assigned.

- \(bu
\f(CW\*(C`setpgrp($nonzero)\*(C' (with one argument) was accidentally changed in 5.16
to mean \f(CWsetpgrp(0).  This has been fixed.

- \(bu
\f(CW\*(C`_\|_SUB_\|_\*(C' could return the wrong value or even corrupt memory under the
debugger (the \f(CW\*(C`-d\*(C' switch) and in subs containing \f(CW\*(C`eval $string\*(C'.

- \(bu
When \f(CW\*(C`sub\ ()\ \{\ $var\ \}\*(C' becomes inlinable, it now returns a different
scalar each time, just as a non-inlinable sub would, though Perl still
optimises the copy away in cases where it would make no observable
difference.

- \(bu
\f(CW\*(C`my\ sub\ f\ ()\ \{\ $var\ \}\*(C' and \f(CW\*(C`sub\ ()\ :\ attr\ \{\ $var\ \}\*(C' are no longer
eligible for inlining.  The former would crash; the latter would just
throw the attributes away.  An exception is made for the little-known
\f(CW\*(C`:method\*(C' attribute, which does nothing much.

- \(bu
Inlining of subs with an empty prototype is now more consistent than
before. Previously, a sub with multiple statements, of which all but the last
were optimised away, would be inlinable only if it were an anonymous sub
containing a string \f(CW\*(C`eval\*(C' or \f(CW\*(C`state\*(C' declaration or closing over an
outer lexical variable (or any anonymous sub under the debugger).  Now any
sub that gets folded to a single constant after statements have been
optimised away is eligible for inlining.  This applies to things like \f(CW\*(C`sub
() \{ jabber() if DEBUG; 42 \}\*(C'.
.Sp
Some subroutines with an explicit \f(CW\*(C`return\*(C' were being made inlinable,
contrary to the documentation,  Now \f(CW\*(C`return\*(C' always prevents inlining.

- \(bu
On some systems, such as \s-1VMS,\s0 \f(CW\*(C`crypt\*(C' can return a non-ASCII string.  If a
scalar assigned to had contained a \s-1UTF-8\s0 string previously, then \f(CW\*(C`crypt\*(C'
would not turn off the \s-1UTF-8\s0 flag, thus corrupting the return value.  This
would happen with \f(CW\*(C`$lexical\ =\ crypt\ ...\*(C'.

- \(bu
\f(CW\*(C`crypt\*(C' no longer calls \f(CW\*(C`FETCH\*(C' twice on a tied first argument.

- \(bu
An unterminated here-doc on the last line of a quote-like operator
(\f(CW\*(C`qq[$\{ <<END \}]\*(C', \f(CW\*(C`/(?\{ <<END \})/\*(C') no longer causes a double free.  It
started doing so in 5.18.

- \(bu
\f(CW\*(C`index()\*(C' and \f(CW\*(C`rindex()\*(C' no longer crash when used on strings over 2GB in
size.
[\s-1GH\s0 #13700] <https://github.com/Perl/perl5/issues/13700>.

- \(bu
A small, previously intentional, memory leak in
\f(CW\*(C`PERL_SYS_INIT\*(C'/\f(CW\*(C`PERL_SYS_INIT3\*(C' on Win32 builds was fixed. This might
affect embedders who repeatedly create and destroy perl engines within
the same process.

- \(bu
\f(CW\*(C`POSIX::localeconv()\*(C' now returns the data for the program's underlying
locale even when called from outside the scope of \f(CW\*(C`use\ locale\*(C'.

- \(bu
\f(CW\*(C`POSIX::localeconv()\*(C' now works properly on platforms which don't have
\f(CW\*(C`LC_NUMERIC\*(C' and/or \f(CW\*(C`LC_MONETARY\*(C', or for which Perl has been compiled
to disregard either or both of these locale categories.  In such
circumstances, there are now no entries for the corresponding values in
the hash returned by \f(CW\*(C`localeconv()\*(C'.

- \(bu
\f(CW\*(C`POSIX::localeconv()\*(C' now marks appropriately the values it returns as
\s-1UTF-8\s0 or not.  Previously they were always returned as bytes, even if
they were supposed to be encoded as \s-1UTF-8.\s0

- \(bu
On Microsoft Windows, within the scope of \f(CW\*(C`use\ locale\*(C', the following
\s-1POSIX\s0 character classes gave results for many locales that did not
conform to the \s-1POSIX\s0 standard:
\f(CW\*(C`[[:alnum:]]\*(C',
\f(CW\*(C`[[:alpha:]]\*(C',
\f(CW\*(C`[[:blank:]]\*(C',
\f(CW\*(C`[[:digit:]]\*(C',
\f(CW\*(C`[[:graph:]]\*(C',
\f(CW\*(C`[[:lower:]]\*(C',
\f(CW\*(C`[[:print:]]\*(C',
\f(CW\*(C`[[:punct:]]\*(C',
\f(CW\*(C`[[:upper:]]\*(C',
\f(CW\*(C`[[:word:]]\*(C',
and
\f(CW\*(C`[[:xdigit:]]\*(C'.
This was because the underlying Microsoft implementation does not
follow the standard.  Perl now takes special precautions to correct for
this.

- \(bu
Many issues have been detected by Coverity <http://www.coverity.com/> and
fixed.

- \(bu
\f(CW\*(C`system()\*(C' and friends should now work properly on more Android builds.
.Sp
Due to an oversight, the value specified through \f(CW\*(C`-Dtargetsh\*(C' to *Configure*
would end up being ignored by some of the build process.  This caused perls
cross-compiled for Android to end up with defective versions of \f(CW\*(C`system()\*(C',
\f(CW\*(C`exec()\*(C' and backticks: the commands would end up looking for \f(CW\*(C`/bin/sh\*(C'
instead of \f(CW\*(C`/system/bin/sh\*(C', and so would fail for the vast majority
of devices, leaving \f(CW$! as \f(CW\*(C`ENOENT\*(C'.

- \(bu
\f(CW\*(C`qr(...\\(...\\)...)\*(C',
\f(CW\*(C`qr[...\\[...\\]...]\*(C',
and
\f(CW\*(C`qr\{...\\\{...\\\}...\}\*(C'
now work.  Previously it was impossible to escape these three
left-characters with a backslash within a regular expression pattern
where otherwise they would be considered metacharacters, and the pattern
opening delimiter was the character, and the closing delimiter was its
mirror character.

- \(bu
\f(CW\*(C`s///e\*(C' on tainted \s-1UTF-8\s0 strings corrupted \f(CW\*(C`pos()\*(C'. This bug,
introduced in 5.20, is now fixed.
[\s-1GH\s0 #13948] <https://github.com/Perl/perl5/issues/13948>.

- \(bu
A non-word boundary in a regular expression (\f(CW\*(C`\\B\*(C') did not always
match the end of the string; in particular \f(CW\*(C`q\{\} =~ /\\B/\*(C' did not
match. This bug, introduced in perl 5.14, is now fixed.
[\s-1GH\s0 #13917] <https://github.com/Perl/perl5/issues/13917>.

- \(bu
\f(CW\*(C`" P" =~ /(?=.*P)P/\*(C' should match, but did not. This is now fixed.
[\s-1GH\s0 #13954] <https://github.com/Perl/perl5/issues/13954>.

- \(bu
Failing to compile \f(CW\*(C`use Foo\*(C' in an \f(CW\*(C`eval\*(C' could leave a spurious
\f(CW\*(C`BEGIN\*(C' subroutine definition, which would produce a \*(L"Subroutine
\s-1BEGIN\s0 redefined\*(R" warning on the next use of \f(CW\*(C`use\*(C', or other \f(CW\*(C`BEGIN\*(C'
block.
[\s-1GH\s0 #13926] <https://github.com/Perl/perl5/issues/13926>.

- \(bu
\f(CW\*(C`method \{ BLOCK \} ARGS\*(C' syntax now correctly parses the arguments if they
begin with an opening brace.
[\s-1GH\s0 #9085] <https://github.com/Perl/perl5/issues/9085>.

- \(bu
External libraries and Perl may have different ideas of what the locale is.
This is problematic when parsing version strings if the locale's numeric
separator has been changed.  Version parsing has been patched to ensure
it handles the locales correctly.
[\s-1GH\s0 #13863] <https://github.com/Perl/perl5/issues/13863>.

- \(bu
A bug has been fixed where zero-length assertions and code blocks inside of a
regex could cause \f(CW\*(C`pos\*(C' to see an incorrect value.
[\s-1GH\s0 #14016] <https://github.com/Perl/perl5/issues/14016>.

- \(bu
Dereferencing of constants now works correctly for typeglob constants.  Previously
the glob was stringified and its name looked up.  Now the glob itself is used.
[\s-1GH\s0 #9891] <https://github.com/Perl/perl5/issues/9891>

- \(bu
When parsing a sigil (\f(CW\*(C`$\*(C' \f(CW\*(C`@\*(C' \f(CW\*(C`%\*(C' \f(CW\*(C`&)\*(C' followed by braces,
the parser no
longer tries to guess whether it is a block or a hash constructor (causing a
syntax error when it guesses the latter), since it can only be a block.

- \(bu
\f(CW\*(C`undef\ $reference\*(C' now frees the referent immediately, instead of hanging on
to it until the next statement.
[\s-1GH\s0 #14032] <https://github.com/Perl/perl5/issues/14032>

- \(bu
Various cases where the name of a sub is used (autoload, overloading, error
messages) used to crash for lexical subs, but have been fixed.

- \(bu
Bareword lookup now tries to avoid vivifying packages if it turns out the
bareword is not going to be a subroutine name.

- \(bu
Compilation of anonymous constants (*e.g.*, \f(CW\*(C`sub () \{ 3 \}\*(C') no longer deletes
any subroutine named \f(CW\*(C`_\|_ANON_\|_\*(C' in the current package.  Not only was
\f(CW\*(C`*_\|_ANON_\|_\{CODE\}\*(C' cleared, but there was a memory leak, too.  This bug goes
back to Perl 5.8.0.

- \(bu
Stub declarations like \f(CW\*(C`sub f;\*(C' and \f(CW\*(C`sub f ();\*(C' no longer wipe out constants
of the same name declared by \f(CW\*(C`use constant\*(C'.  This bug was introduced in Perl
5.10.0.

- \(bu
\f(CW\*(C`qr/[\\N\{named sequence\}]/\*(C' now works properly in many instances.
.Sp
Some names
known to \f(CW\*(C`\\N\{...\}\*(C' refer to a sequence of multiple characters, instead of the
usual single character.  Bracketed character classes generally only match
single characters, but now special handling has been added so that they can
match named sequences, but not if the class is inverted or the sequence is
specified as the beginning or end of a range.  In these cases, the only
behavior change from before is a slight rewording of the fatal error message
given when this class is part of a \f(CW\*(C`?[...])\*(C' construct.  When the \f(CW\*(C`[...]\*(C'
stands alone, the same non-fatal warning as before is raised, and only the
first character in the sequence is used, again just as before.

- \(bu
Tainted constants evaluated at compile time no longer cause unrelated
statements to become tainted.
[\s-1GH\s0 #14059] <https://github.com/Perl/perl5/issues/14059>

- \(bu
\f(CW\*(C`open\ $$fh,\ ...\*(C', which vivifies a handle with a name like
\f(CW"main::_GEN_0", was not giving the handle the right reference count, so
a double free could happen.

- \(bu
When deciding that a bareword was a method name, the parser would get confused
if an \f(CW\*(C`our\*(C' sub with the same name existed, and look up the method in the
package of the \f(CW\*(C`our\*(C' sub, instead of the package of the invocant.

- \(bu
The parser no longer gets confused by \f(CW\*(C`\\U=\*(C' within a double-quoted string.  It
used to produce a syntax error, but now compiles it correctly.
[\s-1GH\s0 #10882] <https://github.com/Perl/perl5/issues/10882>

- \(bu
It has always been the intention for the \f(CW\*(C`-B\*(C' and \f(CW\*(C`-T\*(C' file test operators to
treat \s-1UTF-8\s0 encoded files as text.  (perlfunc has
been updated to say this.)  Previously, it was possible for some files to be
considered \s-1UTF-8\s0 that actually weren't valid \s-1UTF-8.\s0  This is now fixed.  The
operators now work on \s-1EBCDIC\s0 platforms as well.

- \(bu
Under some conditions warning messages raised during regular expression pattern
compilation were being output more than once.  This has now been fixed.

- \(bu
Perl 5.20.0 introduced a regression in which a \s-1UTF-8\s0 encoded regular
expression pattern that contains a single \s-1ASCII\s0 lowercase letter did not
match its uppercase counterpart. That has been fixed in both 5.20.1 and
5.22.0.
[\s-1GH\s0 #14051] <https://github.com/Perl/perl5/issues/14051>

- \(bu
Constant folding could incorrectly suppress warnings if lexical warnings
(\f(CW\*(C`use warnings\*(C' or \f(CW\*(C`no warnings\*(C') were not in effect and \f(CW$^W were
false at compile time and true at run time.

- \(bu
Loading Unicode tables during a regular expression match could cause assertion
failures under debugging builds if the previous match used the very same
regular expression.
[\s-1GH\s0 #14081] <https://github.com/Perl/perl5/issues/14081>

- \(bu
Thread cloning used to work incorrectly for lexical subs, possibly causing
crashes or double frees on exit.

- \(bu
Since Perl 5.14.0, deleting \f(CW$SomePackage::\{_\|_ANON_\|_\} and then undefining an
anonymous subroutine could corrupt things internally, resulting in
Devel::Peek crashing or B.pm giving nonsensical data.  This has been
fixed.

- \(bu
\f(CW\*(C`(caller\ $n)[3]\*(C' now reports names of lexical subs, instead of
treating them as \f(CW"(unknown)".

- \(bu
\f(CW\*(C`sort subname LIST\*(C' now supports using a lexical sub as the comparison
routine.

- \(bu
Aliasing (*e.g.*, via \f(CW\*(C`*x\ =\ *y\*(C') could confuse list assignments that mention the
two names for the same variable on either side, causing wrong values to be
assigned.
[\s-1GH\s0 #5788] <https://github.com/Perl/perl5/issues/5788>

- \(bu
Long here-doc terminators could cause a bad read on short lines of input.  This
has been fixed.  It is doubtful that any crash could have occurred.  This bug
goes back to when here-docs were introduced in Perl 3.000 twenty-five years
ago.

- \(bu
An optimization in \f(CW\*(C`split\*(C' to treat \f(CW\*(C`split\ /^/\*(C' like \f(CW\*(C`split\ /^/m\*(C' had the
unfortunate side-effect of also treating \f(CW\*(C`split\ /\\A/\*(C' like \f(CW\*(C`split\ /^/m\*(C',
which it should not.  This has been fixed.  (Note, however, that \f(CW\*(C`split\ /^x/\*(C'
does not behave like \f(CW\*(C`split\ /^x/m\*(C', which is also considered to be a bug and
will be fixed in a future version.)
[\s-1GH\s0 #14086] <https://github.com/Perl/perl5/issues/14086>

- \(bu
The little-known \f(CW\*(C`my\ Class\ $var\*(C' syntax (see fields and attributes)
could get confused in the scope of \f(CW\*(C`use utf8\*(C' if \f(CW\*(C`Class\*(C' were a constant
whose value contained Latin-1 characters.

- \(bu
Locking and unlocking values via Hash::Util or \f(CW\*(C`Internals::SvREADONLY\*(C'
no longer has any effect on values that were read-only to begin with.
Previously, unlocking such values could result in crashes, hangs or
other erratic behavior.

- \(bu
Some unterminated \f(CW\*(C`(?(...)...)\*(C' constructs in regular expressions would
either crash or give erroneous error messages.  \f(CW\*(C`/(?(1)/\*(C' is one such
example.

- \(bu
\f(CW\*(C`pack\ "w",\ $tied\*(C' no longer calls \s-1FETCH\s0 twice.

- \(bu
List assignments like \f(CW\*(C`($x,\ $z)\ =\ (1,\ $y)\*(C' now work correctly if \f(CW$x and
\f(CW$y have been aliased by \f(CW\*(C`foreach\*(C'.

- \(bu
Some patterns including code blocks with syntax errors, such as
\f(CW\*(C`/\ (?\{(^\{\})/\*(C', would hang or fail assertions on debugging builds.  Now
they produce errors.

- \(bu
An assertion failure when parsing \f(CW\*(C`sort\*(C' with debugging enabled has been
fixed.
[\s-1GH\s0 #14087] <https://github.com/Perl/perl5/issues/14087>.

- \(bu
\f(CW\*(C`*a\ =\ *b;\ @a\ =\ split\ //,\ $b[1]\*(C' could do a bad read and produce junk
results.

- \(bu
In \f(CW\*(C`()\ =\ @array\ =\ split\*(C', the \f(CW\*(C`()\ =\*(C' at the beginning no longer confuses
the optimizer into assuming a limit of 1.

- \(bu
Fatal warnings no longer prevent the output of syntax errors.
[\s-1GH\s0 #14155] <https://github.com/Perl/perl5/issues/14155>.

- \(bu
Fixed a NaN double-to-long-double conversion error on \s-1VMS.\s0 For quiet NaNs
(and only on Itanium, not Alpha) negative infinity instead of NaN was
produced.

- \(bu
Fixed the issue that caused \f(CW\*(C`make distclean\*(C' to incorrectly leave some
files behind.
[\s-1GH\s0 #14108] <https://github.com/Perl/perl5/issues/14108>.

- \(bu
\s-1AIX\s0 now sets the length in \f(CW\*(C`getsockopt\*(C' correctly.
[\s-1GH\s0 #13484] <https://github.com/Perl/perl5/issues/13484>.
[cpan #91183] <https://rt.cpan.org/Ticket/Display.html?id=91183>.
[cpan #85570] <https://rt.cpan.org/Ticket/Display.html?id=85570>.

- \(bu
The optimization phase of a regexp compilation could run \*(L"forever\*(R" and
exhaust all memory under certain circumstances; now fixed.
[\s-1GH\s0 #13984] <https://github.com/Perl/perl5/issues/13984>.

- \(bu
The test script *t/op/crypt.t* now uses the \s-1SHA-256\s0 algorithm if the
default one is disabled, rather than giving failures.
[\s-1GH\s0 #13715] <https://github.com/Perl/perl5/issues/13715>.

- \(bu
Fixed an off-by-one error when setting the size of a shared array.
[\s-1GH\s0 #14151] <https://github.com/Perl/perl5/issues/14151>.

- \(bu
Fixed a bug that could cause perl to enter an infinite loop during
compilation. In particular, a \f(CWwhile(1) within a sublist, *e.g.*
.Sp
.Vb 1
    sub foo \{ () = ($a, my $b, ($c, do \{ while(1) \{\} \})) \}
.Ve
.Sp
The bug was introduced in 5.20.0
[\s-1GH\s0 #14165] <https://github.com/Perl/perl5/issues/14165>.

- \(bu
On Win32, if a variable was \f(CW\*(C`local\*(C'-ized in a pseudo-process that later
forked, restoring the original value in the child pseudo-process caused
memory corruption and a crash in the child pseudo-process (and therefore the
\s-1OS\s0 process).
[\s-1GH\s0 #8641] <https://github.com/Perl/perl5/issues/8641>.

- \(bu
Calling \f(CW\*(C`write\*(C' on a format with a \f(CW\*(C`^**\*(C' field could produce a panic
in \f(CW\*(C`sv_chop()\*(C' if there were insufficient arguments or if the variable
used to fill the field was empty.
[\s-1GH\s0 #14255] <https://github.com/Perl/perl5/issues/14255>.

- \(bu
Non-ASCII lexical sub names now appear without trailing junk when they
appear in error messages.

- \(bu
The \f(CW\*(C`\\@\*(C' subroutine prototype no longer flattens parenthesized arrays
(taking a reference to each element), but takes a reference to the array
itself.
[\s-1GH\s0 #9111] <https://github.com/Perl/perl5/issues/9111>.

- \(bu
A block containing nothing except a C-style \f(CW\*(C`for\*(C' loop could corrupt the
stack, causing lists outside the block to lose elements or have elements
overwritten.  This could happen with \f(CW\*(C`map \{ for(...)\{...\} \} ...\*(C' and with
lists containing \f(CW\*(C`do \{ for(...)\{...\} \}\*(C'.
[\s-1GH\s0 #14269] <https://github.com/Perl/perl5/issues/14269>.

- \(bu
\f(CW\*(C`scalar()\*(C' now propagates lvalue context, so that
\f(CW\*(C`for(scalar($#foo))\ \{\ ...\ \}\*(C' can modify \f(CW$#foo through \f(CW$_.

- \(bu
\f(CW\*(C`qr/@array(?\{block\})/\*(C' no longer dies with \*(L"Bizarre copy of \s-1ARRAY\*(R".\s0
[\s-1GH\s0 #14292] <https://github.com/Perl/perl5/issues/14292>.

- \(bu
\f(CW\*(C`eval\ \*(Aq$variable\*(Aq\*(C' in nested named subroutines would sometimes look up a
global variable even with a lexical variable in scope.

- \(bu
In perl 5.20.0, \f(CW\*(C`sort CORE::fake\*(C' where 'fake' is anything other than a
keyword, started chopping off the last 6 characters and treating the result
as a sort sub name.  The previous behavior of treating \f(CW\*(C`CORE::fake\*(C' as a
sort sub name has been restored.
[\s-1GH\s0 #14323] <https://github.com/Perl/perl5/issues/14323>.

- \(bu
Outside of \f(CW\*(C`use utf8\*(C', a single-character Latin-1 lexical variable is
disallowed.  The error message for it, "Can't use global \f(CW$foo...", was
giving garbage instead of the variable name.

- \(bu
\f(CW\*(C`readline\*(C' on a nonexistent handle was causing \f(CW\*(C`$\{^LAST_FH\}\*(C' to produce a
reference to an undefined scalar (or fail an assertion).  Now
\f(CW\*(C`$\{^LAST_FH\}\*(C' ends up undefined.

- \(bu
\f(CW\*(C`(...) x ...\*(C' in void context now applies scalar context to the left-hand
argument, instead of the context the current sub was called in.
[\s-1GH\s0 #14174] <https://github.com/Perl/perl5/issues/14174>.

## Known Problems

Header "Known Problems"

- \(bu
\f(CW\*(C`pack\*(C'-ing a NaN on a perl compiled with Visual C 6 does not behave properly,
leading to a test failure in *t/op/infnan.t*.
[\s-1GH\s0 #14705] <https://github.com/Perl/perl5/issues/14705>

- \(bu
A goal is for Perl to be able to be recompiled to work reasonably well on any
Unicode version.  In Perl 5.22, though, the earliest such version is Unicode
5.1 (current is 7.0).

- \(bu
\s-1EBCDIC\s0 platforms

> 
- \(bu
The \f(CW\*(C`cmp\*(C' (and hence \f(CW\*(C`sort\*(C') operators do not necessarily give the
correct results when both operands are UTF-EBCDIC encoded strings and
there is a mixture of \s-1ASCII\s0 and/or control characters, along with other
characters.

- \(bu
Ranges containing \f(CW\*(C`\\N\{...\}\*(C' in the \f(CW\*(C`tr///\*(C' (and \f(CW\*(C`y///\*(C')
transliteration operators are treated differently than the equivalent
ranges in regular expression patterns.  They should, but don't, cause
the values in the ranges to all be treated as Unicode code points, and
not native ones.  (\*(L"Version 8 Regular Expressions\*(R" in perlre gives
details as to how it should work.)

- \(bu
Encode and encoding are mostly broken.

- \(bu
Many \s-1CPAN\s0 modules that are shipped with core show failing tests.

- \(bu
\f(CW\*(C`pack\*(C'/\f(CW\*(C`unpack\*(C' with \f(CW"U0" format may not work properly.



> 


- \(bu
The following modules are known to have test failures with this version of
Perl.  In many cases, patches have been submitted, so there will hopefully be
new releases soon:

> 
- \(bu
B::Generate version 1.50

- \(bu
B::Utils version 0.25

- \(bu
Coro version 6.42

- \(bu
Dancer version 1.3130

- \(bu
Data::Alias version 1.18

- \(bu
Data::Dump::Streamer version 2.38

- \(bu
Data::Util version 0.63

- \(bu
Devel::Spy version 0.07

- \(bu
invoker version 0.34

- \(bu
Lexical::Var version 0.009

- \(bu
LWP::ConsoleLogger version 0.000018

- \(bu
Mason version 2.22

- \(bu
NgxQueue version 0.02

- \(bu
Padre version 1.00

- \(bu
Parse::Keyword 0.08



> 


## Obituary

Header "Obituary"
Brian McCauley died on May 8, 2015.  He was a frequent poster to Usenet, Perl
Monks, and other Perl forums, and made several \s-1CPAN\s0 contributions under the
nick \s-1NOBULL,\s0 including to the Perl \s-1FAQ.\s0  He attended almost every
YAPC::Europe, and indeed, helped organise YAPC::Europe 2006 and the \s-1QA\s0
Hackathon 2009.  His wit and his delight in intricate systems were
particularly apparent in his love of board games; many Perl mongers will
have fond memories of playing Fluxx and other games with Brian.  He will be
missed.

## Acknowledgements

Header "Acknowledgements"
Perl 5.22.0 represents approximately 12 months of development since Perl 5.20.0
and contains approximately 590,000 lines of changes across 2,400 files from 94
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 370,000 lines of changes to 1,500 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers. The following people are known to have contributed the
improvements that became Perl 5.22.0:

Aaron Crane, Abhijit Menon-Sen, Abigail, Alberto Simo\*~es, Alex Solovey, Alex
Vandiver, Alexandr Ciornii, Alexandre (Midnite) Jousset, Andreas Ko\*:nig,
Andreas Voegele, Andrew Fresh, Andy Dougherty, Anthony Heading, Aristotle
Pagaltzis, brian d foy, Brian Fraser, Chad Granum, Chris 'BinGOs' Williams,
Craig A. Berry, Dagfinn Ilmari Mannsa\*oker, Daniel Dragan, Darin McBride, Dave
Rolsky, David Golden, David Mitchell, David Wheeler, Dmitri Tikhonov, Doug
Bell, E. Choroba, Ed J, Eric Herman, Father Chrysostomos, George Greer, Glenn
D. Golden, Graham Knop, H.Merijn Brand, Herbert Breunung, Hugo van der Sanden,
James E Keenan, James McCoy, James Raspass, Jan Dubois, Jarkko Hietaniemi,
Jasmine Ngan, Jerry D. Hedden, Jim Cromie, John Goodyear, kafka, Karen
Etheridge, Karl Williamson, Kent Fredric, kmx, Lajos Veres, Leon Timmermans,
Lukas Mai, Mathieu Arnold, Matthew Horsfall, Max Maischein, Michael Bunk,
Nicholas Clark, Niels Thykier, Niko Tyni, Norman Koch, Olivier Mengue\*', Peter
John Acklam, Peter Martini, Petr Pi\*'saX, Philippe Bruhat (BooK), Pierre
Bogossian, Rafael Garcia-Suarez, Randy Stauner, Reini Urban, Ricardo Signes,
Rob Hoelz, Rostislav Skudnov, Sawyer X, Shirakata Kentaro, Shlomi Fish,
Sisyphus, Slaven Rezic, Smylers, Steffen Mu\*:ller, Steve Hay, Sullivan Beck,
syber, Tadeusz SoXnierz, Thomas Sibley, Todd Rinaldo, Tony Cook, Vincent Pit,
Vladimir Marek, Yaroslav Kuzmin, Yves Orton, \*(Aevar Arnfjo\*:r\*(d- Bjarmason.

The list above is almost certainly incomplete as it is automatically generated
from version control history. In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core. We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the perl bug database at
<https://rt.perl.org/>.  There may also be information at
<http://www.perl.org/>, the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send it
to perl5-security-report@perl.org.  This points to a closed subscription
unarchived mailing list, which includes all the core committers, who will be
able to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported.  Please only use this address for
security issues in the Perl core, not for modules independently distributed on
\s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
