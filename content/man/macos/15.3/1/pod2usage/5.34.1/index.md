+++
operating_system = "macos"
operating_system_version = "15.3"
manpage_section = "1"
author = "None Specified"
description = "pod2usage will read the given input file looking for pod documentation and will print the corresponding usage message. If no input file is specified then standard input is read. pod2usage invokes the  module. Please see *(Lpod2usage()*(R in Pod:..."
keywords = ["header", "see", "also", "pod", "usage", "pod2text", "text", "termcap", "perldoc", "author", "please", "report", "bugs", "using", "http", "rt", "cpan", "org", "brad", "appleton", "bradapp", "enteract", "com", "based", "on", "code", "for", "fb", "fbpod2text", "1", "written", "by", "tom", "christiansen", "tchrist", "mox", "perl"]
title = "pod2usage(1)"
manpage_name = "pod2usage"
detected_package_version = "5.34.1"
date = "2024-12-14"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "POD2USAGE 1"
POD2USAGE 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

pod2usage - print usage messages from embedded pod docs in files

## SYNOPSIS

Header "SYNOPSIS"

- \fBpod2usage
Item "pod2usage"
[**-help**]
[**-man**]
[**-exit**\ *exitval*]
[**-output**\ *outfile*]
[**-verbose** *level*]
[**-pathlist** *dirlist*]
[**-formatter** *module*]
[**-utf8**]
*file*

## OPTIONS AND ARGUMENTS

Header "OPTIONS AND ARGUMENTS"

- \fB-help
Item "-help"
Print a brief help message and exit.

- \fB-man
Item "-man"
Print this command's manual page and exit.

- \fB-exit \fIexitval
Item "-exit exitval"
The exit status value to return.

- \fB-output \fIoutfile
Item "-output outfile"
The output file to print to. If the special names \*(L"-\*(R" or \*(L">&1\*(R" or \*(L">&STDOUT\*(R"
are used then standard output is used. If \*(L">&2\*(R" or \*(L">&STDERR\*(R" is used then
standard error is used.

- \fB-verbose \fIlevel
Item "-verbose level"
The desired level of verbosity to use:
.Sp
.Vb 3
    1 : print SYNOPSIS only
    2 : print SYNOPSIS sections and any OPTIONS/ARGUMENTS sections
    3 : print the entire manpage (similar to running pod2text)
.Ve

- \fB-pathlist \fIdirlist
Item "-pathlist dirlist"
Specifies one or more directories to search for the input file if it
was not supplied with an absolute path. Each directory path in the given
list should be separated by a ':' on Unix (';' on MSWin32 and \s-1DOS\s0).

- \fB-formatter \fImodule
Item "-formatter module"
Which text formatter to use. Default is Pod::Text, or for very old
Perl versions Pod::PlainText. An alternative would be e.g.
Pod::Text::Termcap.

- \fB-utf8
Item "-utf8"
This option assumes that the formatter (see above) understands the option
\*(L"utf8\*(R". It turns on generation of utf8 output.

- \fIfile
Item "file"
The pathname of a file containing pod documentation to be output in
usage message format. If omitted, standard input is read - but the
output is then formatted with Pod::Text only - unless a specific
formatter has been specified with **-formatter**.

## DESCRIPTION

Header "DESCRIPTION"
**pod2usage** will read the given input file looking for pod
documentation and will print the corresponding usage message.
If no input file is specified then standard input is read.

**pod2usage** invokes the ****pod2usage()\fB** function in the \fBPod::Usage**
module. Please see \*(L"**pod2usage()**\*(R" in Pod::Usage.

## SEE ALSO

Header "SEE ALSO"
Pod::Usage, pod2text, Pod::Text, Pod::Text::Termcap,
perldoc

## AUTHOR

Header "AUTHOR"
Please report bugs using <http://rt.cpan.org>.

Brad Appleton <bradapp@enteract.com>

Based on code for **\fBpod2text\fB\|(1)** written by
Tom Christiansen <tchrist@mox.perl.com>
