+++
operating_system = "macos"
date = "2022-02-19"
title = "perl5243delta(1)"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
operating_system_version = "15.3"
detected_package_version = "5.34.1"
manpage_section = "1"
author = "None Specified"
description = "This document describes differences between the 5.24.2 release and the 5.24.3 release. If you are upgrading from an earlier release such as 5.24.1, first read perl5242delta, which describes differences between 5.24.1 and 5.24.2. Compiling certain ..."
manpage_name = "perl5243delta"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5243DELTA 1"
PERL5243DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5243delta - what is new for perl v5.24.3

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.24.2 release and the 5.24.3
release.

If you are upgrading from an earlier release such as 5.24.1, first read
perl5242delta, which describes differences between 5.24.1 and 5.24.2.

## Security

Header "Security"

### [\s-1CVE-2017-12837\s0] Heap buffer overflow in regular expression compiler

Subsection "[CVE-2017-12837] Heap buffer overflow in regular expression compiler"
Compiling certain regular expression patterns with the case-insensitive
modifier could cause a heap buffer overflow and crash perl.  This has now been
fixed.
[\s-1GH\s0 #16021] <https://github.com/Perl/perl5/issues/16021>

### [\s-1CVE-2017-12883\s0] Buffer over-read in regular expression parser

Subsection "[CVE-2017-12883] Buffer over-read in regular expression parser"
For certain types of syntax error in a regular expression pattern, the error
message could either contain the contents of a random, possibly large, chunk of
memory, or could crash perl.  This has now been fixed.
[\s-1GH\s0 #16025] <https://github.com/Perl/perl5/issues/16025>
.ie n .SS "[\s-1CVE-2017-12814\s0] $ENV\{$key\} stack buffer overflow on Windows"
.el .SS "[\s-1CVE-2017-12814\s0] \f(CW$ENV\{$key\} stack buffer overflow on Windows"
Subsection "[CVE-2017-12814] $ENV\{$key\} stack buffer overflow on Windows"
A possible stack buffer overflow in the \f(CW%ENV code on Windows has been fixed
by removing the buffer completely since it was superfluous anyway.
[\s-1GH\s0 #16051] <https://github.com/Perl/perl5/issues/16051>

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.24.2.  If any exist,
they are bugs, and we request that you submit a report.  See \*(L"Reporting
Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Module::CoreList has been upgraded from version 5.20170715_24 to
5.20170922_24.

- \(bu
\s-1POSIX\s0 has been upgraded from version 1.65 to 1.65_01.

- \(bu
Time::HiRes has been upgraded from version 1.9733 to 1.9741.
.Sp
[\s-1GH\s0 #15396] <https://github.com/Perl/perl5/issues/15396>
[\s-1GH\s0 #15401] <https://github.com/Perl/perl5/issues/15401>
[\s-1GH\s0 #15524] <https://github.com/Perl/perl5/issues/15524>
[cpan #120032] <https://rt.cpan.org/Public/Bug/Display.html?id=120032>

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
When building with \s-1GCC 6\s0 and link-time optimization (the **-flto** option to
**gcc**), *Configure* was treating all probed symbols as present on the system,
regardless of whether they actually exist.  This has been fixed.
[\s-1GH\s0 #15322] <https://github.com/Perl/perl5/issues/15322>

- \(bu
*Configure* now aborts if both \f(CW\*(C`-Duselongdouble\*(C' and \f(CW\*(C`-Dusequadmath\*(C' are
requested.
[\s-1GH\s0 #14944] <https://github.com/Perl/perl5/issues/14944>

- \(bu
Fixed a bug in which *Configure* could append \f(CW\*(C`-quadmath\*(C' to the archname
even if it was already present.
[\s-1GH\s0 #15423] <https://github.com/Perl/perl5/issues/15423>

- \(bu
Clang builds with \f(CW\*(C`-DPERL_GLOBAL_STRUCT\*(C' or \f(CW\*(C`-DPERL_GLOBAL_STRUCT_PRIVATE\*(C'
have been fixed (by disabling Thread Safety Analysis for these configurations).

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- \s-1VMS\s0
Item "VMS"

> 0

- \(bu
.PD
\f(CW\*(C`configure.com\*(C' now recognizes the VSI-branded C compiler.



> 


- Windows
Item "Windows"

> 0

- \(bu
.PD
Building \s-1XS\s0 modules with \s-1GCC 6\s0 in a 64-bit build of Perl failed due to
incorrect mapping of \f(CW\*(C`strtoll\*(C' and \f(CW\*(C`strtoull\*(C'.  This has now been fixed.
[\s-1GH\s0 #16074] <https://github.com/Perl/perl5/issues/16074>
[cpan #121683] <https://rt.cpan.org/Public/Bug/Display.html?id=121683>
[cpan #122353] <https://rt.cpan.org/Public/Bug/Display.html?id=122353>



> 


## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
\f(CW\*(C`/@0\{0*->@*/*0\*(C' and similar contortions used to crash, but no longer
do, but merely produce a syntax error.
[\s-1GH\s0 #15333] <https://github.com/Perl/perl5/issues/15333>

- \(bu
\f(CW\*(C`do\*(C' or \f(CW\*(C`require\*(C' with an argument which is a reference or typeglob which,
when stringified, contains a null character, started crashing in Perl 5.20, but
has now been fixed.
[\s-1GH\s0 #15337] <https://github.com/Perl/perl5/issues/15337>

- \(bu
Expressions containing an \f(CW\*(C`&&\*(C' or \f(CW\*(C`||\*(C' operator (or their synonyms \f(CW\*(C`and\*(C' and
\f(CW\*(C`or\*(C') were being compiled incorrectly in some cases.  If the left-hand side
consisted of either a negated bareword constant or a negated \f(CW\*(C`do \{\}\*(C' block
containing a constant expression, and the right-hand side consisted of a
negated non-foldable expression, one of the negations was effectively ignored.
The same was true of \f(CW\*(C`if\*(C' and \f(CW\*(C`unless\*(C' statement modifiers, though with the
left-hand and right-hand sides swapped.  This long-standing bug has now been
fixed.
[\s-1GH\s0 #15285] <https://github.com/Perl/perl5/issues/15285>

- \(bu
\f(CW\*(C`reset\*(C' with an argument no longer crashes when encountering stash entries
other than globs.
[\s-1GH\s0 #15314] <https://github.com/Perl/perl5/issues/15314>

- \(bu
Assignment of hashes to, and deletion of, typeglobs named \f(CW*:::::: no longer
causes crashes.
[\s-1GH\s0 #15307] <https://github.com/Perl/perl5/issues/15307>

- \(bu
Assignment variants of any bitwise ops under the \f(CW\*(C`bitwise\*(C' feature would crash
if the left-hand side was an array or hash.
[\s-1GH\s0 #15346] <https://github.com/Perl/perl5/issues/15346>

- \(bu
\f(CW\*(C`socket\*(C' now leaves the error code returned by the system in \f(CW$! on failure.
[\s-1GH\s0 #15383] <https://github.com/Perl/perl5/issues/15383>

- \(bu
Parsing bad \s-1POSIX\s0 charclasses no longer leaks memory.
[\s-1GH\s0 #15382] <https://github.com/Perl/perl5/issues/15382>

- \(bu
Since Perl 5.20, line numbers have been off by one when perl is invoked with
the **-x** switch.  This has been fixed.
[\s-1GH\s0 #15413] <https://github.com/Perl/perl5/issues/15413>

- \(bu
Some obscure cases of subroutines and file handles being freed at the same time
could result in crashes, but have been fixed.  The crash was introduced in Perl
5.22.
[\s-1GH\s0 #15435] <https://github.com/Perl/perl5/issues/15435>

- \(bu
Some regular expression parsing glitches could lead to assertion failures with
regular expressions such as \f(CW\*(C`/(?<=/\*(C' and \f(CW\*(C`/(?<!/\*(C'.  This has now been
fixed.
[\s-1GH\s0 #15332] <https://github.com/Perl/perl5/issues/15332>

- \(bu
\f(CW\*(C`gethostent\*(C' and similar functions now perform a null check internally, to
avoid crashing with the torsocks library.  This was a regression from Perl
5.22.
[\s-1GH\s0 #15478] <https://github.com/Perl/perl5/issues/15478>

- \(bu
Mentioning the same constant twice in a row (which is a syntax error) no longer
fails an assertion under debugging builds.  This was a regression from Perl
5.20.
[\s-1GH\s0 #15017] <https://github.com/Perl/perl5/issues/15017>

- \(bu
In Perl 5.24 \f(CW\*(C`fchown\*(C' was changed not to accept negative one as an argument
because in some platforms that is an error.  However, in some other platforms
that is an acceptable argument.  This change has been reverted.
[\s-1GH\s0 #15523] <https://github.com/Perl/perl5/issues/15523>.

- \(bu
\f(CW\*(C`@\{x\*(C' followed by a newline where \f(CW"x" represents a control or non-ASCII
character no longer produces a garbled syntax error message or a crash.
[\s-1GH\s0 #15518] <https://github.com/Perl/perl5/issues/15518>

- \(bu
A regression in Perl 5.24 with \f(CW\*(C`tr/\\N\{U+...\}/foo/\*(C' when the code point was
between 128 and 255 has been fixed.
[\s-1GH\s0 #15475] <https://github.com/Perl/perl5/issues/15475>.

- \(bu
Many issues relating to \f(CW\*(C`printf "%a"\*(C' of hexadecimal floating point were
fixed.  In addition, the \*(L"subnormals\*(R" (formerly known as \*(L"denormals\*(R") floating
point numbers are now supported both with the plain \s-1IEEE 754\s0 floating point
numbers (64-bit or 128-bit) and the x86 80-bit \*(L"extended precision\*(R".  Note that
subnormal hexadecimal floating point literals will give a warning about
\*(L"exponent underflow\*(R".
[\s-1GH\s0 #15495] <https://github.com/Perl/perl5/issues/15495>
[\s-1GH\s0 #15502] <https://github.com/Perl/perl5/issues/15502>
[\s-1GH\s0 #15503] <https://github.com/Perl/perl5/issues/15503>
[\s-1GH\s0 #15504] <https://github.com/Perl/perl5/issues/15504>
[\s-1GH\s0 #15505] <https://github.com/Perl/perl5/issues/15505>
[\s-1GH\s0 #15510] <https://github.com/Perl/perl5/issues/15510>
[\s-1GH\s0 #15512] <https://github.com/Perl/perl5/issues/15512>

- \(bu
The parser could sometimes crash if a bareword came after \f(CW\*(C`evalbytes\*(C'.
[\s-1GH\s0 #15586] <https://github.com/Perl/perl5/issues/15586>

- \(bu
Fixed a place where the regex parser was not setting the syntax error correctly
on a syntactically incorrect pattern.
[\s-1GH\s0 #15565] <https://github.com/Perl/perl5/issues/15565>

- \(bu
A vulnerability in Perl's \f(CW\*(C`sprintf\*(C' implementation has been fixed by avoiding
a possible memory wrap.
[\s-1GH\s0 #15970] <https://github.com/Perl/perl5/issues/15970>

## Acknowledgements

Header "Acknowledgements"
Perl 5.24.3 represents approximately 2 months of development since Perl 5.24.2
and contains approximately 3,200 lines of changes across 120 files from 23
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 1,600 lines of changes to 56 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.24.3:

Aaron Crane, Craig A. Berry, Dagfinn Ilmari Mannsa\*oker, Dan Collins, Daniel
Dragan, Dave Cross, David Mitchell, Eric Herman, Father Chrysostomos, H.Merijn
Brand, Hugo van der Sanden, James E Keenan, Jarkko Hietaniemi, John \s-1SJ\s0
Anderson, Karl Williamson, Ken Brown, Lukas Mai, Matthew Horsfall, Stevan
Little, Steve Hay, Steven Humphrey, Tony Cook, Yves Orton.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the perl bug database at
<https://rt.perl.org/> .  There may also be information at
<http://www.perl.org/> , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec for details of how to
report the issue.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
