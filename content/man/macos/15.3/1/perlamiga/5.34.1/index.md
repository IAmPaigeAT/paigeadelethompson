+++
detected_package_version = "5.34.1"
description = "The most important of which is: Perl installs into the s-1SDKs0 directory structure and expects many of the build tools present in the s-1SDKs0 to be available. So for the best results install the s-1SDKs0 first. If you do not have the s-1SDKs0 in..."
operating_system_version = "15.3"
manpage_section = "1"
title = "perlamiga(1)"
manpage_format = "troff"
keywords = ["header", "see", "also", "you", "like", "this", "port", "http", "www", "broad", "ology", "org", "uk", "amiga", "for", "how", "can", "help"]
operating_system = "macos"
date = "2022-02-19"
author = "None Specified"
manpage_name = "perlamiga"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLAMIGA 1"
PERLAMIGA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlamiga - Perl under AmigaOS 4.1

## NOTE

Header "NOTE"
This is a port of Perl 5.22.1, it is a fresh port and not in any way
compatible with my previous ports of Perl 5.8 and 5.16.3. This means
you will need to reinstall / rebuild any third party modules you have
installed.

newlib.library version 53.28 or greater is required.

## SYNOPSIS

Header "SYNOPSIS"
Once perl is installed you can read this document in the following way

.Vb 1
        sh -c "perldoc perlamiga"
.Ve

or you may read *as is*: either as *\s-1README\s0.amiga*, or *pod/perlamiga.pod*.

## DESCRIPTION

Header "DESCRIPTION"

### Prerequisites for running Perl 5.22.1 under AmigaOS 4.1

Subsection "Prerequisites for running Perl 5.22.1 under AmigaOS 4.1"

- \fBAmigaOS 4.1 update 6 with all updates applied as of 9th October 2013
Item "AmigaOS 4.1 update 6 with all updates applied as of 9th October 2013"
The most important of which is:

- \fBnewlib.library version 53.28 or greater
Item "newlib.library version 53.28 or greater"
0

- \fBAmigaOS \s-1SDK\s0
Item "AmigaOS SDK"
.PD
Perl installs into the \s-1SDK\s0 directory structure and expects many of the
build tools present in the \s-1SDK\s0 to be available. So for the best results
install the \s-1SDK\s0 first.

- \fBabc-shell
Item "abc-shell"
If you do not have the \s-1SDK\s0 installed you must at least have abc-shell
installed or some other suitable sh port. This is required to run
external commands and should be available as 'sh' in your path.

### Starting Perl programs under AmigaOS 4.1

Subsection "Starting Perl programs under AmigaOS 4.1"
Perl may be run from the AmigaOS shell but for best results should be
run under abc-shell.  (abc-shell handles file globbing, pattern
expansion, and sets up environment variables in the UN*Xy way that
Perl expects.)

For example:

.Vb 3
        New Shell process 10
        10.AmigaOS4:> sh
        /AmigaOS4>perl path:to/myprog arg1 arrg2 arg3
.Ve

Abc-shell can also launch programs via the #! syntax at the start of
the program file, it's best use the form #!SDK:Local/C/perl so that
the AmigaOS shell may also find perl in the same way. AmigaOS requires
the script bit to be set for this to work

.Vb 2
        10.AmigaOS4:> sh
        /AmigaOS4>myprog arg1 arrg2 arg3
.Ve

### Limitations of Perl under AmigaOS 4.1

Subsection "Limitations of Perl under AmigaOS 4.1"

- \fBNested Piped programs can crash when run from older abc-shells
Item "Nested Piped programs can crash when run from older abc-shells"
abc-shell version 53.2 has a bug that can cause crashes in the
subprocesses used to run piped programs, if a later version is
available you should install it instead.

- \fBIncorrect or unexpected command line unescaping
Item "Incorrect or unexpected command line unescaping"
newlib.library 53.30 and earlier incorrectly unescape slashed escape
sequences e.g. \\" \\n \\t etc requiring unusual extra escaping.

- \fBStarting subprocesses via open has limitations
Item "Starting subprocesses via open has limitations"
.Vb 1
        open FH, "command |"
.Ve
.Sp
Subprocesses started with open use a minimal **popen()** routine and
therefore they do not return pids usable with waitpid etc.

- If you find any other limitations or bugs then let me know.
Item "If you find any other limitations or bugs then let me know."
Please report bugs in this version of perl to andy@broad.ology.org.uk
in the first instance.

## INSTALLATION

Header "INSTALLATION"
This guide assumes you have obtained a prebuilt archive from os4depot.net.

Unpack the main archive to a temporary location (\s-1RAM:\s0 is fine).

Execute the provided install script from shell or via its icon.

You **must not** attempt to install by hand.

Once installed you may delete the temporary archive.

This approach will preserve links in the installation without creating
duplicate binaries.

If you have the earlier ports perl 5.16 or 5.8 installed you may like
to rename your perl executable to perl516 or perl58 or something
similar before the installation of 5.22.1, this will allow you to use
both versions at the same time.

## Amiga Specific Modules

Header "Amiga Specific Modules"

### Amiga::ARexx

Subsection "Amiga::ARexx"
The Amiga::ARexx module allows you to easily create a perl based ARexx
host or to send ARexx commands to other programs.

Try \f(CW\*(C`perldoc Amiga::ARexx\*(C' for more info.

### Amiga::Exec

Subsection "Amiga::Exec"
The Amiga::Exec module introduces support for **Wait()**.

Try \f(CW\*(C`perldoc Amiga::Exec\*(C' for more info.

## BUILDING

Header "BUILDING"
To build perl under AmigaOS from the patched sources you will need to
have a recent version of the \s-1SDK.\s0 Version 53.29 is recommended,
earlier versions will probably work too.

With the help of Jarkko Hietaniemi the Configure system has been tweaked to
run under abc-shell so the recommend build process is as follows.

.Vb 3
        stack 2000000
        sh Configure -de
        gmake
.Ve

This will build the default setup that installs under SDK:local/newlib/lib/

## CHANGES

Header "CHANGES"

- \fBAugust 2015
Item "August 2015"

> 0

- Port to Perl 5.22
Item "Port to Perl 5.22"

- Add handling of \s-1NIL:\s0 to \fBafstat()
Item "Add handling of NIL: to afstat()"

- Fix inheritance of environment variables by subprocesses.
Item "Fix inheritance of environment variables by subprocesses."
.ie n .IP "Fix exec, and exit in ""forked"" subprocesses." 2
.el .IP "Fix exec, and exit in ``forked'' subprocesses." 2
Item "Fix exec, and exit in forked subprocesses."

- Fix issue with newlib's unlink, which could cause infinite loops.
Item "Fix issue with newlib's unlink, which could cause infinite loops."

- Add \fBflock() emulation using \s-1IDOS-\s0>LockRecord thanks to Tony Cook for the suggestion.
Item "Add flock() emulation using IDOS->LockRecord thanks to Tony Cook for the suggestion."

- Fix issue where kill was using the wrong kind of process \s-1ID\s0
Item "Fix issue where kill was using the wrong kind of process ID"



> 


- \fB27th November 2013
Item "27th November 2013"

> 
- Create new installation system based on installperl links and Amiga protection bits now set correctly.
Item "Create new installation system based on installperl links and Amiga protection bits now set correctly."

- Pod now defaults to text.
Item "Pod now defaults to text."

- File::Spec should now recognise an Amiga style absolute path as well as an Unix style one. Relative paths must always be Unix style.
Item "File::Spec should now recognise an Amiga style absolute path as well as an Unix style one. Relative paths must always be Unix style."



> 


- \fB20th November 2013
Item "20th November 2013"

> 
- Configured to use SDK:Local/C/perl to start standard scripts
Item "Configured to use SDK:Local/C/perl to start standard scripts"

- Added Amiga::Exec module with support for \fBWait() and AmigaOS signal numbers.
Item "Added Amiga::Exec module with support for Wait() and AmigaOS signal numbers."



> 


- \fB10th October 13
Item "10th October 13"
.PD
First release of port to 5.16.3.

## SEE ALSO

Header "SEE ALSO"
You like this port?  See <http://www.broad.ology.org.uk/amiga/>
for how you can help.
