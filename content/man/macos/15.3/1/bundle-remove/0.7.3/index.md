+++
manpage_name = "bundle-remove"
operating_system_version = "15.3"
title = "bundle-remove(1)"
manpage_section = "1"
author = "None Specified"
description = "Removes the given gems from the Gemfile while ensuring that the resulting Gemfile is still valid. If a gem cannot be removed, a warning is printed. If a gem is already absent from the Gemfile, and error is raised. --install Runs bundle install after..."
manpage_format = "troff"
detected_package_version = "0.7.3"
date = "Sun Feb 16 04:48:23 2025"
operating_system = "macos"
+++

.
"BUNDLE-REMOVE" "1" "November 2018" "" ""
.

## NAME

**bundle-remove** - Removes gems from the Gemfile
.

## SYNOPSIS

**bundle remove [GEM [GEM \.\.\.]] [--install]**
.

## DESCRIPTION

Removes the given gems from the Gemfile while ensuring that the resulting Gemfile is still valid\. If a gem cannot be removed, a warning is printed\. If a gem is already absent from the Gemfile, and error is raised\.
.

## OPTIONS

.

**--install**
Runs **bundle install** after the given gems have been removed from the Gemfile, which ensures that both the lockfile and the installed gems on disk are also updated to remove the given gem(s)\.
.
.P
Example:
.
.P
bundle remove rails
.
.P
bundle remove rails rack
.
.P
bundle remove rails rack --install
