+++
operating_system = "macos"
manpage_format = "troff"
keywords = ["header", "see", "also", "besides", "the", "obvious", "documents", "source", "code", "can", "be", "instructive", "some", "pathological", "examples", "of", "use", "references", "found", "in", "fit", "op", "ref", "t", "regression", "test", "perl", "directory", "perldsc", "and", "perllol", "for", "how", "to", "create", "complex", "data", "structures", "perlootut", "perlobj", "them", "objects"]
author = "None Specified"
manpage_name = "perlref"
manpage_section = "1"
date = "2022-02-19"
operating_system_version = "15.3"
title = "perlref(1)"
detected_package_version = "5.34.1"
description = "Before release 5 of Perl it was difficult to represent complex data structures, because all references had to be symbolic*(--and even then it was difficult to refer to a variable instead of a symbol table entry. Perl now not only makes it easier to..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLREF 1"
PERLREF 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlref - Perl references and nested data structures
Xref "reference pointer data structure structure struct"

## NOTE

Header "NOTE"
This is complete documentation about all aspects of references.
For a shorter, tutorial introduction to just the essential features,
see perlreftut.

## DESCRIPTION

Header "DESCRIPTION"
Before release 5 of Perl it was difficult to represent complex data
structures, because all references had to be symbolic\*(--and even then
it was difficult to refer to a variable instead of a symbol table entry.
Perl now not only makes it easier to use symbolic references to variables,
but also lets you have \*(L"hard\*(R" references to any piece of data or code.
Any scalar may hold a hard reference.  Because arrays and hashes contain
scalars, you can now easily build arrays of arrays, arrays of hashes,
hashes of arrays, arrays of hashes of functions, and so on.

Hard references are smart\*(--they keep track of reference counts for you,
automatically freeing the thing referred to when its reference count goes
to zero.  (Reference counts for values in self-referential or
cyclic data structures may not go to zero without a little help; see
\*(L"Circular References\*(R" for a detailed explanation.)
If that thing happens to be an object, the object is destructed.  See
perlobj for more about objects.  (In a sense, everything in Perl is an
object, but we usually reserve the word for references to objects that
have been officially \*(L"blessed\*(R" into a class package.)

Symbolic references are names of variables or other objects, just as a
symbolic link in a Unix filesystem contains merely the name of a file.
The \f(CW*glob notation is something of a symbolic reference.  (Symbolic
references are sometimes called \*(L"soft references\*(R", but please don't call
them that; references are confusing enough without useless synonyms.)
Xref "reference, symbolic reference, soft symbolic reference soft reference"

In contrast, hard references are more like hard links in a Unix file
system: They are used to access an underlying object without concern for
what its (other) name is.  When the word \*(L"reference\*(R" is used without an
adjective, as in the following paragraph, it is usually talking about a
hard reference.
Xref "reference, hard hard reference"

References are easy to use in Perl.  There is just one overriding
principle: in general, Perl does no implicit referencing or dereferencing.
When a scalar is holding a reference, it always behaves as a simple scalar.
It doesn't magically start being an array or hash or subroutine; you have to
tell it explicitly to do so, by dereferencing it.

### Making References

Xref "reference, creation referencing"
Subsection "Making References"
References can be created in several ways.

*Backslash Operator*
Xref "\ backslash"
Subsection "Backslash Operator"

By using the backslash operator on a variable, subroutine, or value.
(This works much like the & (address-of) operator in C.)
This typically creates *another* reference to a variable, because
there's already a reference to the variable in the symbol table.  But
the symbol table reference might go away, and you'll still have the
reference that the backslash returned.  Here are some examples:

.Vb 5
    $scalarref = \\$foo;
    $arrayref  = \\@ARGV;
    $hashref   = \\%ENV;
    $coderef   = \handler;
    $globref   = \\*foo;
.Ve

It isn't possible to create a true reference to an \s-1IO\s0 handle (filehandle
or dirhandle) using the backslash operator.  The most you can get is a
reference to a typeglob, which is actually a complete symbol table entry.
But see the explanation of the \f(CW*foo\{THING\} syntax below.  However,
you can still use type globs and globrefs as though they were \s-1IO\s0 handles.

*Square Brackets*
Xref "array, anonymous [ [] square bracket bracket, square arrayref array reference reference, array"
Subsection "Square Brackets"

A reference to an anonymous array can be created using square
brackets:

.Vb 1
    $arrayref = [1, 2, [\*(Aqa\*(Aq, \*(Aqb\*(Aq, \*(Aqc\*(Aq]];
.Ve

Here we've created a reference to an anonymous array of three elements
whose final element is itself a reference to another anonymous array of three
elements.  (The multidimensional syntax described later can be used to
access this.  For example, after the above, \f(CW\*(C`$arrayref->[2][1]\*(C' would have
the value \*(L"b\*(R".)

Taking a reference to an enumerated list is not the same
as using square brackets\*(--instead it's the same as creating
a list of references!

.Vb 2
    @list = (\\$a, \\@b, \\%c);
    @list = \\($a, @b, %c);      # same thing!
.Ve

As a special case, \f(CW\*(C`\\(@foo)\*(C' returns a list of references to the contents
of \f(CW@foo, not a reference to \f(CW@foo itself.  Likewise for \f(CW%foo,
except that the key references are to copies (since the keys are just
strings rather than full-fledged scalars).

*Curly Brackets*
Xref "hash, anonymous \{ \{\} curly bracket bracket, curly brace hashref hash reference reference, hash"
Subsection "Curly Brackets"

A reference to an anonymous hash can be created using curly
brackets:

.Vb 4
    $hashref = \{
        \*(AqAdam\*(Aq  => \*(AqEve\*(Aq,
        \*(AqClyde\*(Aq => \*(AqBonnie\*(Aq,
    \};
.Ve

Anonymous hash and array composers like these can be intermixed freely to
produce as complicated a structure as you want.  The multidimensional
syntax described below works for these too.  The values above are
literals, but variables and expressions would work just as well, because
assignment operators in Perl (even within **local()** or **my()**) are executable
statements, not compile-time declarations.

Because curly brackets (braces) are used for several other things
including BLOCKs, you may occasionally have to disambiguate braces at the
beginning of a statement by putting a \f(CW\*(C`+\*(C' or a \f(CW\*(C`return\*(C' in front so
that Perl realizes the opening brace isn't starting a \s-1BLOCK.\s0  The economy and
mnemonic value of using curlies is deemed worth this occasional extra
hassle.

For example, if you wanted a function to make a new hash and return a
reference to it, you have these options:

.Vb 3
    sub hashem \{        \{ @_ \} \}   # silently wrong
    sub hashem \{       +\{ @_ \} \}   # ok
    sub hashem \{ return \{ @_ \} \}   # ok
.Ve

On the other hand, if you want the other meaning, you can do this:

.Vb 4
    sub showem \{        \{ @_ \} \}   # ambiguous (currently ok,
                                   # but may change)
    sub showem \{       \{; @_ \} \}   # ok
    sub showem \{ \{ return @_ \} \}   # ok
.Ve

The leading \f(CW\*(C`+\{\*(C' and \f(CW\*(C`\{;\*(C' always serve to disambiguate
the expression to mean either the \s-1HASH\s0 reference, or the \s-1BLOCK.\s0

*Anonymous Subroutines*
Xref "subroutine, anonymous subroutine, reference reference, subroutine scope, lexical closure lexical lexical scope"
Subsection "Anonymous Subroutines"

A reference to an anonymous subroutine can be created by using
\f(CW\*(C`sub\*(C' without a subname:

.Vb 1
    $coderef = sub \{ print "Boink!\\n" \};
.Ve

Note the semicolon.  Except for the code
inside not being immediately executed, a \f(CW\*(C`sub \{\}\*(C' is not so much a
declaration as it is an operator, like \f(CW\*(C`do\{\}\*(C' or \f(CW\*(C`eval\{\}\*(C'.  (However, no
matter how many times you execute that particular line (unless you're in an
\f(CW\*(C`eval("...")\*(C'), \f(CW$coderef will still have a reference to the *same*
anonymous subroutine.)

Anonymous subroutines act as closures with respect to **my()** variables,
that is, variables lexically visible within the current scope.  Closure
is a notion out of the Lisp world that says if you define an anonymous
function in a particular lexical context, it pretends to run in that
context even when it's called outside the context.

In human terms, it's a funny way of passing arguments to a subroutine when
you define it as well as when you call it.  It's useful for setting up
little bits of code to run later, such as callbacks.  You can even
do object-oriented stuff with it, though Perl already provides a different
mechanism to do that\*(--see perlobj.

You might also think of closure as a way to write a subroutine
template without using **eval()**.  Here's a small example of how
closures work:

.Vb 6
    sub newprint \{
        my $x = shift;
        return sub \{ my $y = shift; print "$x, $y!\\n"; \};
    \}
    $h = newprint("Howdy");
    $g = newprint("Greetings");

    # Time passes...

    &$h("world");
    &$g("earthlings");
.Ve

This prints

.Vb 2
    Howdy, world!
    Greetings, earthlings!
.Ve

Note particularly that \f(CW$x continues to refer to the value passed
into **newprint()** *despite* \*(L"my \f(CW$x\*(R" having gone out of scope by the
time the anonymous subroutine runs.  That's what a closure is all
about.

This applies only to lexical variables, by the way.  Dynamic variables
continue to work as they have always worked.  Closure is not something
that most Perl programmers need trouble themselves about to begin with.

*Constructors*
Xref "constructor new"
Subsection "Constructors"

References are often returned by special subroutines called constructors.  Perl
objects are just references to a special type of object that happens to know
which package it's associated with.  Constructors are just special subroutines
that know how to create that association.  They do so by starting with an
ordinary reference, and it remains an ordinary reference even while it's also
being an object.  Constructors are often named \f(CW\*(C`new()\*(C'.  You *can* call them
indirectly:

.Vb 1
    $objref = new Doggie( Tail => \*(Aqshort\*(Aq, Ears => \*(Aqlong\*(Aq );
.Ve

But that can produce ambiguous syntax in certain cases, so it's often
better to use the direct method invocation approach:

.Vb 1
    $objref   = Doggie->new(Tail => \*(Aqshort\*(Aq, Ears => \*(Aqlong\*(Aq);

    use Term::Cap;
    $terminal = Term::Cap->Tgetent( \{ OSPEED => 9600 \});

    use Tk;
    $main    = MainWindow->new();
    $menubar = $main->Frame(-relief              => "raised",
                            -borderwidth         => 2)
.Ve

*Autovivification*
Xref "autovivification"
Subsection "Autovivification"

References of the appropriate type can spring into existence if you
dereference them in a context that assumes they exist.  Because we haven't
talked about dereferencing yet, we can't show you any examples yet.

*Typeglob Slots*
Xref "*foo\{THING\} *"
Subsection "Typeglob Slots"

A reference can be created by using a special syntax, lovingly known as
the *foo\{\s-1THING\s0\} syntax.  *foo\{\s-1THING\s0\} returns a reference to the \s-1THING\s0
slot in *foo (which is the symbol table entry which holds everything
known as foo).

.Vb 9
    $scalarref = *foo\{SCALAR\};
    $arrayref  = *ARGV\{ARRAY\};
    $hashref   = *ENV\{HASH\};
    $coderef   = *handler\{CODE\};
    $ioref     = *STDIN\{IO\};
    $globref   = *foo\{GLOB\};
    $formatref = *foo\{FORMAT\};
    $globname  = *foo\{NAME\};    # "foo"
    $pkgname   = *foo\{PACKAGE\}; # "main"
.Ve

Most of these are self-explanatory, but \f(CW*foo\{IO\}
deserves special attention.  It returns
the \s-1IO\s0 handle, used for file handles (\*(L"open\*(R" in perlfunc), sockets
(\*(L"socket\*(R" in perlfunc and \*(L"socketpair\*(R" in perlfunc), and directory
handles (\*(L"opendir\*(R" in perlfunc).  For compatibility with previous
versions of Perl, \f(CW*foo\{FILEHANDLE\} is a synonym for \f(CW*foo\{IO\}, though it
is discouraged, to encourage a consistent use of one name: \s-1IO.\s0  On perls
between v5.8 and v5.22, it will issue a deprecation warning, but this
deprecation has since been rescinded.

\f(CW*foo\{THING\} returns undef if that particular \s-1THING\s0 hasn't been used yet,
except in the case of scalars.  \f(CW*foo\{SCALAR\} returns a reference to an
anonymous scalar if \f(CW$foo hasn't been used yet.  This might change in a
future release.

\f(CW*foo\{NAME\} and \f(CW*foo\{PACKAGE\} are the exception, in that they return
strings, rather than references.  These return the package and name of the
typeglob itself, rather than one that has been assigned to it.  So, after
\f(CW\*(C`*foo=*Foo::bar\*(C', \f(CW*foo will become \*(L"*Foo::bar\*(R" when used as a string,
but \f(CW*foo\{PACKAGE\} and \f(CW*foo\{NAME\} will continue to produce \*(L"main\*(R" and
\*(L"foo\*(R", respectively.

\f(CW*foo\{IO\} is an alternative to the \f(CW*HANDLE mechanism given in
\*(L"Typeglobs and Filehandles\*(R" in perldata for passing filehandles
into or out of subroutines, or storing into larger data structures.
Its disadvantage is that it won't create a new filehandle for you.
Its advantage is that you have less risk of clobbering more than
you want to with a typeglob assignment.  (It still conflates file
and directory handles, though.)  However, if you assign the incoming
value to a scalar instead of a typeglob as we do in the examples
below, there's no risk of that happening.

.Vb 2
    splutter(*STDOUT);          # pass the whole glob
    splutter(*STDOUT\{IO\});      # pass both file and dir handles

    sub splutter \{
        my $fh = shift;
        print $fh "her um well a hmmm\\n";
    \}

    $rec = get_rec(*STDIN);     # pass the whole glob
    $rec = get_rec(*STDIN\{IO\}); # pass both file and dir handles

    sub get_rec \{
        my $fh = shift;
        return scalar <$fh>;
    \}
.Ve

### Using References

Xref "reference, use dereferencing dereference"
Subsection "Using References"
That's it for creating references.  By now you're probably dying to
know how to use references to get back to your long-lost data.  There
are several basic methods.

*Simple Scalar*
Subsection "Simple Scalar"

Anywhere you'd put an identifier (or chain of identifiers) as part
of a variable or subroutine name, you can replace the identifier with
a simple scalar variable containing a reference of the correct type:

.Vb 6
    $bar = $$scalarref;
    push(@$arrayref, $filename);
    $$arrayref[0] = "January";
    $$hashref\{"KEY"\} = "VALUE";
    &$coderef(1,2,3);
    print $globref "output\\n";
.Ve

It's important to understand that we are specifically *not* dereferencing
\f(CW$arrayref[0] or \f(CW$hashref\{"KEY"\} there.  The dereference of the
scalar variable happens *before* it does any key lookups.  Anything more
complicated than a simple scalar variable must use methods 2 or 3 below.
However, a \*(L"simple scalar\*(R" includes an identifier that itself uses method
1 recursively.  Therefore, the following prints \*(L"howdy\*(R".

.Vb 2
    $refrefref = \\\\\\"howdy";
    print $$$$refrefref;
.Ve

*Block*
Subsection "Block"

Anywhere you'd put an identifier (or chain of identifiers) as part of a
variable or subroutine name, you can replace the identifier with a
\s-1BLOCK\s0 returning a reference of the correct type.  In other words, the
previous examples could be written like this:

.Vb 6
    $bar = $\{$scalarref\};
    push(@\{$arrayref\}, $filename);
    $\{$arrayref\}[0] = "January";
    $\{$hashref\}\{"KEY"\} = "VALUE";
    &\{$coderef\}(1,2,3);
    $globref->print("output\\n");  # iff IO::Handle is loaded
.Ve

Admittedly, it's a little silly to use the curlies in this case, but
the \s-1BLOCK\s0 can contain any arbitrary expression, in particular,
subscripted expressions:

.Vb 1
    &\{ $dispatch\{$index\} \}(1,2,3);      # call correct routine
.Ve

Because of being able to omit the curlies for the simple case of \f(CW$$x,
people often make the mistake of viewing the dereferencing symbols as
proper operators, and wonder about their precedence.  If they were,
though, you could use parentheses instead of braces.  That's not the case.
Consider the difference below; case 0 is a short-hand version of case 1,
*not* case 2:

.Vb 4
    $$hashref\{"KEY"\}   = "VALUE";       # CASE 0
    $\{$hashref\}\{"KEY"\} = "VALUE";       # CASE 1
    $\{$hashref\{"KEY"\}\} = "VALUE";       # CASE 2
    $\{$hashref->\{"KEY"\}\} = "VALUE";     # CASE 3
.Ve

Case 2 is also deceptive in that you're accessing a variable
called \f(CW%hashref, not dereferencing through \f(CW$hashref to the hash
it's presumably referencing.  That would be case 3.

*Arrow Notation*
Subsection "Arrow Notation"

Subroutine calls and lookups of individual array elements arise often
enough that it gets cumbersome to use method 2.  As a form of
syntactic sugar, the examples for method 2 may be written:

.Vb 3
    $arrayref->[0] = "January";   # Array element
    $hashref->\{"KEY"\} = "VALUE";  # Hash element
    $coderef->(1,2,3);            # Subroutine call
.Ve

The left side of the arrow can be any expression returning a reference,
including a previous dereference.  Note that \f(CW$array[$x] is *not* the
same thing as \f(CW\*(C`$array->[$x]\*(C' here:

.Vb 1
    $array[$x]->\{"foo"\}->[0] = "January";
.Ve

This is one of the cases we mentioned earlier in which references could
spring into existence when in an lvalue context.  Before this
statement, \f(CW$array[$x] may have been undefined.  If so, it's
automatically defined with a hash reference so that we can look up
\f(CW\*(C`\{"foo"\}\*(C' in it.  Likewise \f(CW\*(C`$array[$x]->\{"foo"\}\*(C' will automatically get
defined with an array reference so that we can look up \f(CW\*(C`[0]\*(C' in it.
This process is called *autovivification*.

One more thing here.  The arrow is optional *between* brackets
subscripts, so you can shrink the above down to

.Vb 1
    $array[$x]\{"foo"\}[0] = "January";
.Ve

Which, in the degenerate case of using only ordinary arrays, gives you
multidimensional arrays just like C's:

.Vb 1
    $score[$x][$y][$z] += 42;
.Ve

Well, okay, not entirely like C's arrays, actually.  C doesn't know how
to grow its arrays on demand.  Perl does.

*Objects*
Subsection "Objects"

If a reference happens to be a reference to an object, then there are
probably methods to access the things referred to, and you should probably
stick to those methods unless you're in the class package that defines the
object's methods.  In other words, be nice, and don't violate the object's
encapsulation without a very good reason.  Perl does not enforce
encapsulation.  We are not totalitarians here.  We do expect some basic
civility though.

*Miscellaneous Usage*
Subsection "Miscellaneous Usage"

Using a string or number as a reference produces a symbolic reference,
as explained above.  Using a reference as a number produces an
integer representing its storage location in memory.  The only
useful thing to be done with this is to compare two references
numerically to see whether they refer to the same location.
Xref "reference, numeric context"

.Vb 3
    if ($ref1 == $ref2) \{  # cheap numeric compare of references
        print "refs 1 and 2 refer to the same thing\\n";
    \}
.Ve

Using a reference as a string produces both its referent's type,
including any package blessing as described in perlobj, as well
as the numeric address expressed in hex.  The **ref()** operator returns
just the type of thing the reference is pointing to, without the
address.  See \*(L"ref\*(R" in perlfunc for details and examples of its use.
Xref "reference, string context"

The **bless()** operator may be used to associate the object a reference
points to with a package functioning as an object class.  See perlobj.

A typeglob may be dereferenced the same way a reference can, because
the dereference syntax always indicates the type of reference desired.
So \f(CW\*(C`$\{*foo\}\*(C' and \f(CW\*(C`$\{\\$foo\}\*(C' both indicate the same scalar variable.

Here's a trick for interpolating a subroutine call into a string:

.Vb 1
    print "My sub returned @\{[mysub(1,2,3)]\} that time.\\n";
.Ve

The way it works is that when the \f(CW\*(C`@\{...\}\*(C' is seen in the double-quoted
string, it's evaluated as a block.  The block creates a reference to an
anonymous array containing the results of the call to \f(CW\*(C`mysub(1,2,3)\*(C'.  So
the whole block returns a reference to an array, which is then
dereferenced by \f(CW\*(C`@\{...\}\*(C' and stuck into the double-quoted string. This
chicanery is also useful for arbitrary expressions:

.Vb 1
    print "That yields @\{[$n + 5]\} widgets\\n";
.Ve

Similarly, an expression that returns a reference to a scalar can be
dereferenced via \f(CW\*(C`$\{...\}\*(C'. Thus, the above expression may be written
as:

.Vb 1
    print "That yields $\{\\($n + 5)\} widgets\\n";
.Ve

### Circular References

Xref "circular reference reference, circular"
Subsection "Circular References"
It is possible to create a \*(L"circular reference\*(R" in Perl, which can lead
to memory leaks. A circular reference occurs when two references
contain a reference to each other, like this:

.Vb 3
    my $foo = \{\};
    my $bar = \{ foo => $foo \};
    $foo->\{bar\} = $bar;
.Ve

You can also create a circular reference with a single variable:

.Vb 2
    my $foo;
    $foo = \\$foo;
.Ve

In this case, the reference count for the variables will never reach 0,
and the references will never be garbage-collected. This can lead to
memory leaks.

Because objects in Perl are implemented as references, it's possible to
have circular references with objects as well. Imagine a TreeNode class
where each node references its parent and child nodes. Any node with a
parent will be part of a circular reference.

You can break circular references by creating a \*(L"weak reference\*(R". A
weak reference does not increment the reference count for a variable,
which means that the object can go out of scope and be destroyed. You
can weaken a reference with the \f(CW\*(C`weaken\*(C' function exported by the
Scalar::Util module.

Here's how we can make the first example safer:

.Vb 1
    use Scalar::Util \*(Aqweaken\*(Aq;

    my $foo = \{\};
    my $bar = \{ foo => $foo \};
    $foo->\{bar\} = $bar;

    weaken $foo->\{bar\};
.Ve

The reference from \f(CW$foo to \f(CW$bar has been weakened. When the
\f(CW$bar variable goes out of scope, it will be garbage-collected. The
next time you look at the value of the \f(CW\*(C`$foo->\{bar\}\*(C' key, it will
be \f(CW\*(C`undef\*(C'.

This action at a distance can be confusing, so you should be careful
with your use of weaken. You should weaken the reference in the
variable that will go out of scope *first*. That way, the longer-lived
variable will contain the expected reference until it goes out of
scope.

### Symbolic references

Xref "reference, symbolic reference, soft symbolic reference soft reference"
Subsection "Symbolic references"
We said that references spring into existence as necessary if they are
undefined, but we didn't say what happens if a value used as a
reference is already defined, but *isn't* a hard reference.  If you
use it as a reference, it'll be treated as a symbolic
reference.  That is, the value of the scalar is taken to be the *name*
of a variable, rather than a direct link to a (possibly) anonymous
value.

People frequently expect it to work like this.  So it does.

.Vb 9
    $name = "foo";
    $$name = 1;                 # Sets $foo
    $\{$name\} = 2;               # Sets $foo
    $\{$name x 2\} = 3;           # Sets $foofoo
    $name->[0] = 4;             # Sets $foo[0]
    @$name = ();                # Clears @foo
    &$name();                   # Calls &foo()
    $pack = "THAT";
    $\{"$\{pack\}::$name"\} = 5;    # Sets $THAT::foo without eval
.Ve

This is powerful, and slightly dangerous, in that it's possible
to intend (with the utmost sincerity) to use a hard reference, and
accidentally use a symbolic reference instead.  To protect against
that, you can say

.Vb 1
    use strict \*(Aqrefs\*(Aq;
.Ve

and then only hard references will be allowed for the rest of the enclosing
block.  An inner block may countermand that with

.Vb 1
    no strict \*(Aqrefs\*(Aq;
.Ve

Only package variables (globals, even if localized) are visible to
symbolic references.  Lexical variables (declared with **my()**) aren't in
a symbol table, and thus are invisible to this mechanism.  For example:

.Vb 6
    local $value = 10;
    $ref = "value";
    \{
        my $value = 20;
        print $$ref;
    \}
.Ve

This will still print 10, not 20.  Remember that **local()** affects package
variables, which are all \*(L"global\*(R" to the package.

### Not-so-symbolic references

Subsection "Not-so-symbolic references"
Brackets around a symbolic reference can simply
serve to isolate an identifier or variable name from the rest of an
expression, just as they always have within a string.  For example,

.Vb 2
    $push = "pop on ";
    print "$\{push\}over";
.Ve

has always meant to print \*(L"pop on over\*(R", even though push is
a reserved word.  This is generalized to work the same
without the enclosing double quotes, so that

.Vb 1
    print $\{push\} . "over";
.Ve

and even

.Vb 1
    print $\{ push \} . "over";
.Ve

will have the same effect.  This
construct is *not* considered to be a symbolic reference when you're
using strict refs:

.Vb 3
    use strict \*(Aqrefs\*(Aq;
    $\{ bareword \};      # Okay, means $bareword.
    $\{ "bareword" \};    # Error, symbolic reference.
.Ve

Similarly, because of all the subscripting that is done using single words,
the same rule applies to any bareword that is used for subscripting a hash.
So now, instead of writing

.Vb 1
    $hash\{ "aaa" \}\{ "bbb" \}\{ "ccc" \}
.Ve

you can write just

.Vb 1
    $hash\{ aaa \}\{ bbb \}\{ ccc \}
.Ve

and not worry about whether the subscripts are reserved words.  In the
rare event that you do wish to do something like

.Vb 1
    $hash\{ shift \}
.Ve

you can force interpretation as a reserved word by adding anything that
makes it more than a bareword:

.Vb 3
    $hash\{ shift() \}
    $hash\{ +shift \}
    $hash\{ shift @_ \}
.Ve

The \f(CW\*(C`use warnings\*(C' pragma or the **-w** switch will warn you if it
interprets a reserved word as a string.
But it will no longer warn you about using lowercase words, because the
string is effectively quoted.

### Pseudo-hashes: Using an array as a hash

Xref "pseudo-hash pseudo hash pseudohash"
Subsection "Pseudo-hashes: Using an array as a hash"
Pseudo-hashes have been removed from Perl.  The 'fields' pragma
remains available.

### Function Templates

Xref "scope, lexical closure lexical lexical scope subroutine, nested sub, nested subroutine, local sub, local"
Subsection "Function Templates"
As explained above, an anonymous function with access to the lexical
variables visible when that function was compiled, creates a closure.  It
retains access to those variables even though it doesn't get run until
later, such as in a signal handler or a Tk callback.

Using a closure as a function template allows us to generate many functions
that act similarly.  Suppose you wanted functions named after the colors
that generated \s-1HTML\s0 font changes for the various colors:

.Vb 1
    print "Be ", red("careful"), "with that ", green("light");
.Ve

The **red()** and **green()** functions would be similar.  To create these,
we'll assign a closure to a typeglob of the name of the function we're
trying to build.

.Vb 5
    @colors = qw(red blue green yellow orange purple violet);
    for my $name (@colors) \{
        no strict \*(Aqrefs\*(Aq;       # allow symbol table manipulation
        *$name = *\{uc $name\} = sub \{ "<FONT COLOR=\*(Aq$name\*(Aq>@_</FONT>" \};
    \}
.Ve

Now all those different functions appear to exist independently.  You can
call **red()**, \s-1**RED\s0()**, **blue()**, \s-1**BLUE\s0()**, **green()**, etc.  This technique saves on
both compile time and memory use, and is less error-prone as well, since
syntax checks happen at compile time.  It's critical that any variables in
the anonymous subroutine be lexicals in order to create a proper closure.
That's the reasons for the \f(CW\*(C`my\*(C' on the loop iteration variable.

This is one of the only places where giving a prototype to a closure makes
much sense.  If you wanted to impose scalar context on the arguments of
these functions (probably not a wise idea for this particular example),
you could have written it this way instead:

.Vb 1
    *$name = sub ($) \{ "<FONT COLOR=\*(Aq$name\*(Aq>$_[0]</FONT>" \};
.Ve

However, since prototype checking happens at compile time, the assignment
above happens too late to be of much use.  You could address this by
putting the whole loop of assignments within a \s-1BEGIN\s0 block, forcing it
to occur during compilation.

Access to lexicals that change over time\*(--like those in the \f(CW\*(C`for\*(C' loop
above, basically aliases to elements from the surrounding lexical scopes\*(--
only works with anonymous subs, not with named subroutines. Generally
said, named subroutines do not nest properly and should only be declared
in the main package scope.

This is because named subroutines are created at compile time so their
lexical variables get assigned to the parent lexicals from the first
execution of the parent block. If a parent scope is entered a second
time, its lexicals are created again, while the nested subs still
reference the old ones.

Anonymous subroutines get to capture each time you execute the \f(CW\*(C`sub\*(C'
operator, as they are created on the fly. If you are accustomed to using
nested subroutines in other programming languages with their own private
variables, you'll have to work at it a bit in Perl.  The intuitive coding
of this type of thing incurs mysterious warnings about \*(L"will not stay
shared\*(R" due to the reasons explained above.
For example, this won't work:

.Vb 5
    sub outer \{
        my $x = $_[0] + 35;
        sub inner \{ return $x * 19 \}   # WRONG
        return $x + inner();
    \}
.Ve

A work-around is the following:

.Vb 5
    sub outer \{
        my $x = $_[0] + 35;
        local *inner = sub \{ return $x * 19 \};
        return $x + inner();
    \}
.Ve

Now **inner()** can only be called from within **outer()**, because of the
temporary assignments of the anonymous subroutine. But when it does,
it has normal access to the lexical variable \f(CW$x from the scope of
**outer()** at the time outer is invoked.

This has the interesting effect of creating a function local to another
function, something not normally supported in Perl.

### Postfix Dereference Syntax

Subsection "Postfix Dereference Syntax"
Beginning in v5.20.0, a postfix syntax for using references is
available.  It behaves as described in \*(L"Using References\*(R", but instead
of a prefixed sigil, a postfixed sigil-and-star is used.

For example:

.Vb 2
    $r = \\@a;
    @b = $r->@*; # equivalent to @$r or @\{ $r \}

    $r = [ 1, [ 2, 3 ], 4 ];
    $r->[1]->@*;  # equivalent to @\{ $r->[1] \}
.Ve

In Perl 5.20 and 5.22, this syntax must be enabled with \f(CW\*(C`use feature
\*(Aqpostderef\*(Aq\*(C'. As of Perl 5.24, no feature declarations are required to make
it available.

Postfix dereference should work in all circumstances where block
(circumfix) dereference worked, and should be entirely equivalent.  This
syntax allows dereferencing to be written and read entirely
left-to-right.  The following equivalencies are defined:

.Vb 6
  $sref->$*;  # same as  $\{ $sref \}
  $aref->@*;  # same as  @\{ $aref \}
  $aref->$#*; # same as $#\{ $aref \}
  $href->%*;  # same as  %\{ $href \}
  $cref->&*;  # same as  &\{ $cref \}
  $gref->**;  # same as  *\{ $gref \}
.Ve

Note especially that \f(CW\*(C`$cref->&*\*(C' is *not* equivalent to \f(CW\*(C`$cref->()\*(C', and can serve different purposes.

Glob elements can be extracted through the postfix dereferencing feature:

.Vb 1
  $gref->*\{SCALAR\}; # same as *\{ $gref \}\{SCALAR\}
.Ve

Postfix array and scalar dereferencing *can* be used in interpolating
strings (double quotes or the \f(CW\*(C`qq\*(C' operator), but only if the
\f(CW\*(C`postderef_qq\*(C' feature is enabled.

### Postfix Reference Slicing

Subsection "Postfix Reference Slicing"
Value slices of arrays and hashes may also be taken with postfix
dereferencing notation, with the following equivalencies:

.Vb 2
  $aref->@[ ... ];  # same as @$aref[ ... ]
  $href->@\{ ... \};  # same as @$href\{ ... \}
.Ve

Postfix key/value pair slicing, added in 5.20.0 and documented in
the Key/Value Hash Slices section of perldata, also behaves as expected:

.Vb 2
  $aref->%[ ... ];  # same as %$aref[ ... ]
  $href->%\{ ... \};  # same as %$href\{ ... \}
.Ve

As with postfix array, postfix value slice dereferencing *can* be used
in interpolating strings (double quotes or the \f(CW\*(C`qq\*(C' operator), but only
if the \f(CW\*(C`postderef_qq\*(C' feature is enabled.

### Assigning to References

Subsection "Assigning to References"
Beginning in v5.22.0, the referencing operator can be assigned to.  It
performs an aliasing operation, so that the variable name referenced on the
left-hand side becomes an alias for the thing referenced on the right-hand
side:

.Vb 2
    \\$a = \\$b; # $a and $b now point to the same scalar
    \foo = \bar; # foo() now means bar()
.Ve

This syntax must be enabled with \f(CW\*(C`use feature \*(Aqrefaliasing\*(Aq\*(C'.  It is
experimental, and will warn by default unless \f(CW\*(C`no warnings
\*(Aqexperimental::refaliasing\*(Aq\*(C' is in effect.

These forms may be assigned to, and cause the right-hand side to be
evaluated in scalar context:

.Vb 10
    \\$scalar
    \\@array
    \\%hash
    \sub
    \\my $scalar
    \\my @array
    \\my %hash
    \\state $scalar # or @array, etc.
    \\our $scalar   # etc.
    \\local $scalar # etc.
    \\local our $scalar # etc.
    \\$some_array[$index]
    \\$some_hash\{$key\}
    \\local $some_array[$index]
    \\local $some_hash\{$key\}
    condition ? \\$this : \\$that[0] # etc.
.Ve

Slicing operations and parentheses cause
the right-hand side to be evaluated in
list context:

.Vb 10
    \\@array[5..7]
    (\\@array[5..7])
    \\(@array[5..7])
    \\@hash\{\*(Aqfoo\*(Aq,\*(Aqbar\*(Aq\}
    (\\@hash\{\*(Aqfoo\*(Aq,\*(Aqbar\*(Aq\})
    \\(@hash\{\*(Aqfoo\*(Aq,\*(Aqbar\*(Aq\})
    (\\$scalar)
    \\($scalar)
    \\(my $scalar)
    \\my($scalar)
    (\\@array)
    (\\%hash)
    (\sub)
    \\(&sub)
    \\($foo, @bar, %baz)
    (\\$foo, \\@bar, \\%baz)
.Ve

Each element on the right-hand side must be a reference to a datum of the
right type.  Parentheses immediately surrounding an array (and possibly
also \f(CW\*(C`my\*(C'/\f(CW\*(C`state\*(C'/\f(CW\*(C`our\*(C'/\f(CW\*(C`local\*(C') will make each element of the array an
alias to the corresponding scalar referenced on the right-hand side:

.Vb 5
    \\(@a) = \\(@b); # @a and @b now have the same elements
    \\my(@a) = \\(@b); # likewise
    \\(my @a) = \\(@b); # likewise
    push @a, 3; # but now @a has an extra element that @b lacks
    \\(@a) = (\\$a, \\$b, \\$c); # @a now contains $a, $b, and $c
.Ve

Combining that form with \f(CW\*(C`local\*(C' and putting parentheses immediately
around a hash are forbidden (because it is not clear what they should do):

.Vb 2
    \\local(@array) = foo(); # WRONG
    \\(%hash)       = bar(); # WRONG
.Ve

Assignment to references and non-references may be combined in lists and
conditional ternary expressions, as long as the values on the right-hand
side are the right type for each element on the left, though this may make
for obfuscated code:

.Vb 4
    (my $tom, \\my $dick, \\my @harry) = (\\1, \\2, [1..3]);
    # $tom is now \\1
    # $dick is now 2 (read-only)
    # @harry is (1,2,3)

    my $type = ref $thingy;
    ($type ? $type eq \*(AqARRAY\*(Aq ? \\@foo : \\$bar : $baz) = $thingy;
.Ve

The \f(CW\*(C`foreach\*(C' loop can also take a reference constructor for its loop
variable, though the syntax is limited to one of the following, with an
optional \f(CW\*(C`my\*(C', \f(CW\*(C`state\*(C', or \f(CW\*(C`our\*(C' after the backslash:

.Vb 4
    \\$s
    \\@a
    \\%h
    \c
.Ve

No parentheses are permitted.  This feature is particularly useful for
arrays-of-arrays, or arrays-of-hashes:

.Vb 3
    foreach \\my @a (@array_of_arrays) \{
        frobnicate($a[0], $a[-1]);
    \}

    foreach \\my %h (@array_of_hashes) \{
        $h\{gelastic\}++ if $h\{type\} eq \*(Aqfunny\*(Aq;
    \}
.Ve

**\s-1CAVEAT:\s0** Aliasing does not work correctly with closures.  If you try to
alias lexical variables from an inner subroutine or \f(CW\*(C`eval\*(C', the aliasing
will only be visible within that inner sub, and will not affect the outer
subroutine where the variables are declared.  This bizarre behavior is
subject to change.

### Declaring a Reference to a Variable

Subsection "Declaring a Reference to a Variable"
Beginning in v5.26.0, the referencing operator can come after \f(CW\*(C`my\*(C',
\f(CW\*(C`state\*(C', \f(CW\*(C`our\*(C', or \f(CW\*(C`local\*(C'.  This syntax must be enabled with \f(CW\*(C`use
feature \*(Aqdeclared_refs\*(Aq\*(C'.  It is experimental, and will warn by default
unless \f(CW\*(C`no warnings \*(Aqexperimental::refaliasing\*(Aq\*(C' is in effect.

This feature makes these:

.Vb 2
    my \\$x;
    our \\$y;
.Ve

equivalent to:

.Vb 2
    \\my $x;
    \\our $x;
.Ve

It is intended mainly for use in assignments to references (see
\*(L"Assigning to References\*(R", above).  It also allows the backslash to be
used on just some items in a list of declared variables:

.Vb 1
    my ($foo, \\@bar, \\%baz); # equivalent to:  my $foo, \\my(@bar, %baz);
.Ve

## WARNING: Dont use references as hash keys

Xref "reference, string context reference, use as hash key"
Header "WARNING: Don't use references as hash keys"
You may not (usefully) use a reference as the key to a hash.  It will be
converted into a string:

.Vb 1
    $x\{ \\$a \} = $a;
.Ve

If you try to dereference the key, it won't do a hard dereference, and
you won't accomplish what you're attempting.  You might want to do something
more like

.Vb 2
    $r = \\@a;
    $x\{ $r \} = $r;
.Ve

And then at least you can use the **values()**, which will be
real refs, instead of the **keys()**, which won't.

The standard Tie::RefHash module provides a convenient workaround to this.

## SEE ALSO

Header "SEE ALSO"
Besides the obvious documents, source code can be instructive.
Some pathological examples of the use of references can be found
in the *t/op/ref.t* regression test in the Perl source directory.

See also perldsc and perllol for how to use references to create
complex data structures, and perlootut and perlobj
for how to use them to create objects.
