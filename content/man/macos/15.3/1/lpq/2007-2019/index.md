+++
date = "Sun Feb 16 04:48:20 2025"
manpage_format = "troff"
description = "lpq shows the current print queue status on the named printer. Jobs queued on the default destination will be shown if no printer or class is specified on the command-line. The +interval option allows you to continuously report the jobs in the queue..."
title = "lpq(1)"
operating_system_version = "15.3"
manpage_section = "1"
detected_package_version = "2007-2019"
author = "Apple"
keywords = ["cancel", "1", "lp", "lpr", "lprm", "lpstat", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
manpage_name = "lpq"
operating_system = "macos"
+++

lpq 1 "CUPS" "26 April 2019" "Apple Inc."

## NAME

lpq - show printer queue status

## SYNOPSIS

lpq
[
-E
] [
-U
username
] [
**-h **server[**:**port]
] [
**-P **destination[**/**instance]
] [
-a
] [
-l
] [
+ interval
]

## DESCRIPTION

**lpq** shows the current print queue status on the named printer.
Jobs queued on the default destination will be shown if no printer or class is specified on the command-line.

The *+interval* option allows you to continuously report the jobs in the queue until the queue is empty; the list of jobs is shown once every *interval* seconds.

## OPTIONS

**lpq** supports the following options:

-E
Forces encryption when connecting to the server.

**-P **destination[**/**instance]
Specifies an alternate printer or class name.

**-U **username
Specifies an alternate username.

-a
Reports jobs on all printers.

**-h **server[**:**port]
Specifies an alternate server.

-l
Requests a more verbose (long) reporting format.

## SEE ALSO

cancel (1),
lp (1),
lpr (1),
lprm (1),
lpstat (1),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
