+++
description = "The normal way to run a Perl program is by making it directly executable, or else by passing the name of the source file as an argument on the command line.  (An interactive Perl environment is also possible*(--see perldebug for details on how to d..."
date = "2022-02-19"
author = "None Specified"
manpage_name = "perlrun"
detected_package_version = "5.34.1"
manpage_format = "troff"
operating_system = "macos"
operating_system_version = "15.3"
title = "perlrun(1)"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLRUN 1"
PERLRUN 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlrun - how to execute the Perl interpreter

## SYNOPSIS

Header "SYNOPSIS"
**perl**	[\ **-sTtuUWX**\ ]
	[\ **-hv**\ ]\ [\ **-V**[:*configvar*]\ ]
	[\ **-cw**\ ]\ [\ **-d**[**t**][:*debugger*]\ ]\ [\ **-D**[*number/list*]\ ]
	[\ **-pna**\ ]\ [\ **-F***pattern*\ ]\ [\ **-l**[*octal*]\ ]\ [\ **-0**[*octal/hexadecimal*]\ ]
	[\ **-I***dir*\ ]\ [\ **-m**[**-**]*module*\ ]\ [\ **-M**[**-**]*'module...'*\ ]\ [\ **-f**\ ]
	[\ **-C\ [\f(BInumber/list\fB]\ **]
	[\ **-S**\ ]
	[\ **-x**[*dir*]\ ]
	[\ **-i**[*extension*]\ ]
	[\ [**-e**|**-E**]\ *'command'*\ ]\ [\ **--**\ ]\ [\ *programfile*\ ]\ [\ *argument*\ ]...

## DESCRIPTION

Header "DESCRIPTION"
The normal way to run a Perl program is by making it directly
executable, or else by passing the name of the source file as an
argument on the command line.  (An interactive Perl environment
is also possible\*(--see perldebug for details on how to do that.)
Upon startup, Perl looks for your program in one of the following
places:

- 1.
Specified line by line via -e or -E
switches on the command line.

- 2.
Contained in the file specified by the first filename on the command line.
(Note that systems supporting the \f(CW\*(C`#!\*(C' notation invoke interpreters this
way. See \*(L"Location of Perl\*(R".)

- 3.
Passed in implicitly via standard input.  This works only if there are
no filename arguments\*(--to pass arguments to a STDIN-read program you
must explicitly specify a \*(L"-\*(R" for the program name.

With methods 2 and 3, Perl starts parsing the input file from the
beginning, unless you've specified a \*(L"-x\*(R" switch, in which case it
scans for the first line starting with \f(CW\*(C`#!\*(C' and containing the word
\*(L"perl\*(R", and starts there instead.  This is useful for running a program
embedded in a larger message.  (In this case you would indicate the end
of the program using the \f(CW\*(C`_\|_END_\|_\*(C' token.)

The \f(CW\*(C`#!\*(C' line is always examined for switches as the line is being
parsed.  Thus, if you're on a machine that allows only one argument
with the \f(CW\*(C`#!\*(C' line, or worse, doesn't even recognize the \f(CW\*(C`#!\*(C' line, you
still can get consistent switch behaviour regardless of how Perl was
invoked, even if \*(L"-x\*(R" was used to find the beginning of the program.

Because historically some operating systems silently chopped off
kernel interpretation of the \f(CW\*(C`#!\*(C' line after 32 characters, some
switches may be passed in on the command line, and some may not;
you could even get a \*(L"-\*(R" without its letter, if you're not careful.
You probably want to make sure that all your switches fall either
before or after that 32-character boundary.  Most switches don't
actually care if they're processed redundantly, but getting a \*(L"-\*(R"
instead of a complete switch could cause Perl to try to execute
standard input instead of your program.  And a partial -I
switch could also cause odd results.

Some switches do care if they are processed twice, for instance
combinations of -l and -0.
Either put all the switches after the 32-character boundary (if
applicable), or replace the use of **-0***digits* by
\f(CW\*(C`BEGIN\{ $/ = "\\0digits"; \}\*(C'.

Parsing of the \f(CW\*(C`#!\*(C' switches starts wherever \*(L"perl\*(R" is mentioned in the line.
The sequences \*(L"-*\*(R" and \*(L"- \*(R" are specifically ignored so that you could,
if you were so inclined, say

.Vb 4
    #!/bin/sh
    #! -*- perl -*- -p
    eval \*(Aqexec perl -x -wS $0 $\{1+"$@"\}\*(Aq
        if 0;
.Ve

to let Perl see the \*(L"-p\*(R" switch.

A similar trick involves the *env* program, if you have it.

.Vb 1
    #!/usr/bin/env perl
.Ve

The examples above use a relative path to the perl interpreter,
getting whatever version is first in the user's path.  If you want
a specific version of Perl, say, perl5.14.1, you should place
that directly in the \f(CW\*(C`#!\*(C' line's path.

If the \f(CW\*(C`#!\*(C' line does not contain the word \*(L"perl\*(R" nor the word \*(L"indir\*(R",
the program named after the \f(CW\*(C`#!\*(C' is executed instead of the Perl
interpreter.  This is slightly bizarre, but it helps people on machines
that don't do \f(CW\*(C`#!\*(C', because they can tell a program that their \s-1SHELL\s0 is
*/usr/bin/perl*, and Perl will then dispatch the program to the correct
interpreter for them.

After locating your program, Perl compiles the entire program to an
internal form.  If there are any compilation errors, execution of the
program is not attempted.  (This is unlike the typical shell script,
which might run part-way through before finding a syntax error.)

If the program is syntactically correct, it is executed.  If the program
runs off the end without hitting an **exit()** or **die()** operator, an implicit
\f(CWexit(0) is provided to indicate successful completion.

### #! and quoting on non-Unix systems

Xref "hashbang #!"
Subsection "#! and quoting on non-Unix systems"
Unix's \f(CW\*(C`#!\*(C' technique can be simulated on other systems:

- \s-1OS/2\s0
Item "OS/2"
Put
.Sp
.Vb 1
    extproc perl -S -your_switches
.Ve
.Sp
as the first line in \f(CW\*(C`*.cmd\*(C' file (\*(L"-S\*(R" due to a bug in cmd.exe's
`extproc' handling).

- MS-DOS
Item "MS-DOS"
Create a batch file to run your program, and codify it in
\f(CW\*(C`ALTERNATE_SHEBANG\*(C' (see the *dosish.h* file in the source
distribution for more information).

- Win95/NT
Item "Win95/NT"
The Win95/NT installation, when using the ActiveState installer for Perl,
will modify the Registry to associate the *.pl* extension with the perl
interpreter.  If you install Perl by other means (including building from
the sources), you may have to modify the Registry yourself.  Note that
this means you can no longer tell the difference between an executable
Perl program and a Perl library file.

- \s-1VMS\s0
Item "VMS"
Put
.Sp
.Vb 2
 $ perl -mysw \*(Aqf$env("procedure")\*(Aq \*(Aqp1\*(Aq \*(Aqp2\*(Aq \*(Aqp3\*(Aq \*(Aqp4\*(Aq \*(Aqp5\*(Aq \*(Aqp6\*(Aq \*(Aqp7\*(Aq \*(Aqp8\*(Aq !
 $ exit++ + ++$status != 0 and $exit = $status = undef;
.Ve
.Sp
at the top of your program, where **-mysw** are any command line switches you
want to pass to Perl.  You can now invoke the program directly, by saying
\f(CW\*(C`perl program\*(C', or as a \s-1DCL\s0 procedure, by saying \f(CW@program (or implicitly
via *\s-1DCL$PATH\s0* by just using the name of the program).
.Sp
This incantation is a bit much to remember, but Perl will display it for
you if you say \f(CW\*(C`perl "-V:startperl"\*(C'.

Command-interpreters on non-Unix systems have rather different ideas
on quoting than Unix shells.  You'll need to learn the special
characters in your command-interpreter (\f(CW\*(C`*\*(C', \f(CW\*(C`\\\*(C' and \f(CW\*(C`"\*(C' are
common) and how to protect whitespace and these characters to run
one-liners (see -e below).

On some systems, you may have to change single-quotes to double ones,
which you must *not* do on Unix or Plan 9 systems.  You might also
have to change a single % to a %%.

For example:

.Vb 2
    # Unix
    perl -e \*(Aqprint "Hello world\\n"\*(Aq

    # MS-DOS, etc.
    perl -e "print \\"Hello world\\n\\""

    # VMS
    perl -e "print ""Hello world\\n"""
.Ve

The problem is that none of this is reliable: it depends on the
command and it is entirely possible neither works.  If *4DOS* were
the command shell, this would probably work better:

.Vb 1
    perl -e "print <Ctrl-x>"Hello world\\n<Ctrl-x>""
.Ve

**\s-1CMD.EXE\s0** in Windows \s-1NT\s0 slipped a lot of standard Unix functionality in
when nobody was looking, but just try to find documentation for its
quoting rules.

There is no general solution to all of this.  It's just a mess.

### Location of Perl

Xref "perl, location of interpreter"
Subsection "Location of Perl"
It may seem obvious to say, but Perl is useful only when users can
easily find it.  When possible, it's good for both */usr/bin/perl*
and */usr/local/bin/perl* to be symlinks to the actual binary.  If
that can't be done, system administrators are strongly encouraged
to put (symlinks to) perl and its accompanying utilities into a
directory typically found along a user's \s-1PATH,\s0 or in some other
obvious and convenient place.

In this documentation, \f(CW\*(C`#!/usr/bin/perl\*(C' on the first line of the program
will stand in for whatever method works on your system.  You are
advised to use a specific path if you care about a specific version.

.Vb 1
    #!/usr/local/bin/perl5.14
.Ve

or if you just want to be running at least version, place a statement
like this at the top of your program:

.Vb 1
    use 5.014;
.Ve

### Command Switches

Xref "perl, command switches command switches"
Subsection "Command Switches"
As with all standard commands, a single-character switch may be
clustered with the following switch, if any.

.Vb 1
    #!/usr/bin/perl -spi.orig   # same as -s -p -i.orig
.Ve

A \f(CW\*(C`--\*(C' signals the end of options and disables further option processing. Any
arguments after the \f(CW\*(C`--\*(C' are treated as filenames and arguments.

Switches include:

- \fB-0[\fIoctal/hexadecimal]
Xref "-0 $"
Item "-0[octal/hexadecimal]"
specifies the input record separator (\f(CW$/) as an octal or
hexadecimal number.  If there are no digits, the null character is the
separator.  Other switches may precede or follow the digits.  For
example, if you have a version of *find* which can print filenames
terminated by the null character, you can say this:
.Sp
.Vb 1
    find . -name \*(Aq*.orig\*(Aq -print0 | perl -n0e unlink
.Ve
.Sp
The special value 00 will cause Perl to slurp files in paragraph mode.
Any value 0400 or above will cause Perl to slurp files whole, but by convention
the value 0777 is the one normally used for this purpose.
.Sp
You can also specify the separator character using hexadecimal notation:
**-0x\f(BI\s-1HHH...\s0****, where the \f(CW\*(C`\f(CIH\f(CW\*(C'** are valid hexadecimal digits.  Unlike
the octal form, this one may be used to specify any Unicode character, even
those beyond 0xFF.  So if you *really* want a record separator of 0777,
specify it as **-0x1FF**.  (This means that you cannot use the \*(L"-x\*(R" option
with a directory name that consists of hexadecimal digits, or else Perl
will think you have specified a hex number to **-0**.)

- \fB-a
Xref "-a autosplit"
Item "-a"
turns on autosplit mode when used with a \*(L"-n\*(R" or \*(L"-p\*(R".  An implicit
split command to the \f(CW@F array is done as the first thing inside the
implicit while loop produced by the \*(L"-n\*(R" or \*(L"-p\*(R".
.Sp
.Vb 1
    perl -ane \*(Aqprint pop(@F), "\\n";\*(Aq
.Ve
.Sp
is equivalent to
.Sp
.Vb 4
    while (<>) \{
        @F = split(\*(Aq \*(Aq);
        print pop(@F), "\\n";
    \}
.Ve
.Sp
An alternate delimiter may be specified using -F.
.Sp
**-a** implicitly sets \*(L"-n\*(R".

- \fB-C [\f(BInumber/list\fB]
Xref "-C"
Item "-C [number/list]"
The **-C** flag controls some of the Perl Unicode features.
.Sp
As of 5.8.1, the **-C** can be followed either by a number or a list
of option letters.  The letters, their numeric values, and effects
are as follows; listing the letters is equal to summing the numbers.
.Sp
.Vb 10
    I     1   STDIN is assumed to be in UTF-8
    O     2   STDOUT will be in UTF-8
    E     4   STDERR will be in UTF-8
    S     7   I + O + E
    i     8   UTF-8 is the default PerlIO layer for input streams
    o    16   UTF-8 is the default PerlIO layer for output streams
    D    24   i + o
    A    32   the @ARGV elements are expected to be strings encoded
              in UTF-8
    L    64   normally the "IOEioA" are unconditional, the L makes
              them conditional on the locale environment variables
              (the LC_ALL, LC_CTYPE, and LANG, in the order of
              decreasing precedence) -- if the variables indicate
              UTF-8, then the selected "IOEioA" are in effect
    a   256   Set $\{^UTF8CACHE\} to -1, to run the UTF-8 caching
              code in debugging mode.
.Ve
.Sp
For example, **-COE** and **-C6** will both turn on UTF-8-ness on both
\s-1STDOUT\s0 and \s-1STDERR.\s0  Repeating letters is just redundant, not cumulative
nor toggling.
.Sp
The \f(CW\*(C`io\*(C' options mean that any subsequent **open()** (or similar I/O
operations) in main program scope will have the \f(CW\*(C`:utf8\*(C' PerlIO layer
implicitly applied to them, in other words, \s-1UTF-8\s0 is expected from any
input stream, and \s-1UTF-8\s0 is produced to any output stream.  This is just
the default set via \f(CW\*(C`$\{^OPEN\}\*(C',
with explicit layers in **open()** and with **binmode()** one can
manipulate streams as usual.  This has no effect on code run in modules.
.Sp
**-C** on its own (not followed by any number or option list), or the
empty string \f(CW"" for the \*(L"\s-1PERL_UNICODE\*(R"\s0 environment variable, has the
same effect as **-CSDL**.  In other words, the standard I/O handles and
the default \f(CW\*(C`open()\*(C' layer are UTF-8-fied *but* only if the locale
environment variables indicate a \s-1UTF-8\s0 locale.  This behaviour follows
the *implicit* (and problematic) \s-1UTF-8\s0 behaviour of Perl 5.8.0.
(See \*(L"\s-1UTF-8\s0 no longer default under \s-1UTF-8\s0 locales\*(R" in perl581delta.)
.Sp
You can use **-C0** (or \f(CW"0" for \f(CW\*(C`PERL_UNICODE\*(C') to explicitly
disable all the above Unicode features.
.Sp
The read-only magic variable \f(CW\*(C`$\{^UNICODE\}\*(C' reflects the numeric value
of this setting.  This variable is set during Perl startup and is
thereafter read-only.  If you want runtime effects, use the three-arg
**open()** (see \*(L"open\*(R" in perlfunc), the two-arg **binmode()** (see \*(L"binmode\*(R" in perlfunc),
and the \f(CW\*(C`open\*(C' pragma (see open).
.Sp
(In Perls earlier than 5.8.1 the **-C** switch was a Win32-only switch
that enabled the use of Unicode-aware \*(L"wide system call\*(R" Win32 APIs.
This feature was practically unused, however, and the command line
switch was therefore \*(L"recycled\*(R".)
.Sp
**Note:** Since perl 5.10.1, if the **-C** option is used on the \f(CW\*(C`#!\*(C' line,
it must be specified on the command line as well, since the standard streams
are already set up at this point in the execution of the perl interpreter.
You can also use **binmode()** to set the encoding of an I/O stream.

- \fB-c
Xref "-c"
Item "-c"
causes Perl to check the syntax of the program and then exit without
executing it.  Actually, it *will* execute any \f(CW\*(C`BEGIN\*(C', \f(CW\*(C`UNITCHECK\*(C',
or \f(CW\*(C`CHECK\*(C' blocks and any \f(CW\*(C`use\*(C' statements: these are considered as
occurring outside the execution of your program.  \f(CW\*(C`INIT\*(C' and \f(CW\*(C`END\*(C'
blocks, however, will be skipped.

- \fB-d
Xref "-d -dt"
Item "-d"
0

- \fB-dt
Item "-dt"
.PD
runs the program under the Perl debugger.  See perldebug.
If **t** is specified, it indicates to the debugger that threads
will be used in the code being debugged.

- \fB-d:\fIMOD[=bar,baz]
Xref "-d -dt"
Item "-d:MOD[=bar,baz]"
0

- \fB-dt:\fIMOD[=bar,baz]
Item "-dt:MOD[=bar,baz]"
.PD
runs the program under the control of a debugging, profiling, or tracing
module installed as \f(CW\*(C`Devel::\f(CIMOD\f(CW\*(C'. E.g., **-d:DProf** executes the
program using the \f(CW\*(C`Devel::DProf\*(C' profiler.  As with the -M
flag, options may be passed to the \f(CW\*(C`Devel::\f(CIMOD\f(CW\*(C' package where they will
be received and interpreted by the \f(CW\*(C`Devel::\f(CIMOD\f(CW::import\*(C' routine.  Again,
like **-M**, use -**-d:-\f(BI\s-1MOD\s0**** to call \f(CW\*(C`Devel::\f(CIMOD\f(CW::unimport\*(C'** instead of
import.  The comma-separated list of options must follow a \f(CW\*(C`=\*(C' character.
If **t** is specified, it indicates to the debugger that threads will be used
in the code being debugged.  See perldebug.

- \fB-D\fIletters
Xref "-D DEBUGGING -DDEBUGGING"
Item "-Dletters"
0

- \fB-D\fInumber
Item "-Dnumber"
.PD
sets debugging flags. This switch is enabled only if your perl binary has
been built with debugging enabled: normal production perls won't have
been.
.Sp
For example, to watch how perl executes your program, use **-Dtls**.
Another nice value is **-Dx**, which lists your compiled syntax tree, and
**-Dr** displays compiled regular expressions; the format of the output is
explained in perldebguts.
.Sp
As an alternative, specify a number instead of list of letters (e.g.,
**-D14** is equivalent to **-Dtls**):
.Sp
.Vb 10
         1  p  Tokenizing and parsing (with v, displays parse
               stack)
         2  s  Stack snapshots (with v, displays all stacks)
         4  l  Context (loop) stack processing
         8  t  Trace execution
        16  o  Method and overloading resolution
        32  c  String/numeric conversions
        64  P  Print profiling info, source file input state
       128  m  Memory and SV allocation
       256  f  Format processing
       512  r  Regular expression parsing and execution
      1024  x  Syntax tree dump
      2048  u  Tainting checks
      4096  U  Unofficial, User hacking (reserved for private,
               unreleased use)
     16384  X  Scratchpad allocation
     32768  D  Cleaning up
     65536  S  Op slab allocation
    131072  T  Tokenizing
    262144  R  Include reference counts of dumped variables
               (eg when using -Ds)
    524288  J  show s,t,P-debug (don\*(Aqt Jump over) on opcodes within
               package DB
   1048576  v  Verbose: use in conjunction with other flags to
               increase the verbosity of the output.  Is a no-op on
               many of the other flags
   2097152  C  Copy On Write
   4194304  A  Consistency checks on internal structures
   8388608  q  quiet - currently only suppresses the "EXECUTING"
               message
  16777216  M  trace smart match resolution
  33554432  B  dump suBroutine definitions, including special
               Blocks like BEGIN
  67108864  L  trace Locale-related info; what gets output is very
               subject to change
 134217728  i  trace PerlIO layer processing.  Set PERLIO_DEBUG to
               the filename to trace to.
 268435456  y  trace y///, tr/// compilation and execution
.Ve
.Sp
All these flags require **-DDEBUGGING** when you compile the Perl
executable (but see \f(CW\*(C`:opd\*(C' in Devel::Peek or \*(L"'debug' mode\*(R" in re
which may change this).
See the *\s-1INSTALL\s0* file in the Perl source distribution
for how to do this.
.Sp
If you're just trying to get a print out of each line of Perl code
as it executes, the way that \f(CW\*(C`sh -x\*(C' provides for shell scripts,
you can't use Perl's **-D** switch.  Instead do this
.Sp
.Vb 2
  # If you have "env" utility
  env PERLDB_OPTS="NonStop=1 AutoTrace=1 frame=2" perl -dS program

  # Bourne shell syntax
  $ PERLDB_OPTS="NonStop=1 AutoTrace=1 frame=2" perl -dS program

  # csh syntax
  % (setenv PERLDB_OPTS "NonStop=1 AutoTrace=1 frame=2"; perl -dS program)
.Ve
.Sp
See perldebug for details and variations.

- \fB-e \fIcommandline
Xref "-e"
Item "-e commandline"
may be used to enter one line of program.  If **-e** is given, Perl
will not look for a filename in the argument list.  Multiple **-e**
commands may be given to build up a multi-line script.  Make sure
to use semicolons where you would in a normal program.

- \fB-E \fIcommandline
Xref "-E"
Item "-E commandline"
behaves just like -e, except that it implicitly
enables all optional features (in the main compilation unit). See
feature.

- \fB-f
Xref "-f sitecustomize sitecustomize.pl"
Item "-f"
Disable executing *\f(CI$Config\fI\{sitelib\}/sitecustomize.pl* at startup.
.Sp
Perl can be built so that it by default will try to execute
*\f(CI$Config\fI\{sitelib\}/sitecustomize.pl* at startup (in a \s-1BEGIN\s0 block).
This is a hook that allows the sysadmin to customize how Perl behaves.
It can for instance be used to add entries to the \f(CW@INC array to make Perl
find modules in non-standard locations.
.Sp
Perl actually inserts the following code:
.Sp
.Vb 4
    BEGIN \{
        do \{ local $!; -f "$Config\{sitelib\}/sitecustomize.pl"; \}
            && do "$Config\{sitelib\}/sitecustomize.pl";
    \}
.Ve
.Sp
Since it is an actual \f(CW\*(C`do\*(C' (not a \f(CW\*(C`require\*(C'), *sitecustomize.pl*
doesn't need to return a true value. The code is run in package \f(CW\*(C`main\*(C',
in its own lexical scope. However, if the script dies, \f(CW$@ will not
be set.
.Sp
The value of \f(CW$Config\{sitelib\} is also determined in C code and not
read from \f(CW\*(C`Config.pm\*(C', which is not loaded.
.Sp
The code is executed *very* early. For example, any changes made to
\f(CW@INC will show up in the output of `perl -V`. Of course, \f(CW\*(C`END\*(C'
blocks will be likewise executed very late.
.Sp
To determine at runtime if this capability has been compiled in your
perl, you can check the value of \f(CW$Config\{usesitecustomize\}.

- \fB-F\fIpattern
Xref "-F"
Item "-Fpattern"
specifies the pattern to split on for \*(L"-a\*(R". The pattern may be
surrounded by \f(CW\*(C`//\*(C', \f(CW"", or \f(CW\*(Aq\*(Aq, otherwise it will be put in single
quotes. You can't use literal whitespace or \s-1NUL\s0 characters in the pattern.
.Sp
**-F** implicitly sets both \*(L"-a\*(R" and \*(L"-n\*(R".

- \fB-h
Xref "-h"
Item "-h"
prints a summary of the options.

- \fB-i[\fIextension]
Xref "-i in-place"
Item "-i[extension]"
specifies that files processed by the \f(CW\*(C`<>\*(C' construct are to be
edited in-place.  It does this by renaming the input file, opening the
output file by the original name, and selecting that output file as the
default for **print()** statements.  The extension, if supplied, is used to
modify the name of the old file to make a backup copy, following these
rules:
.Sp
If no extension is supplied, and your system supports it, the original
*file* is kept open without a name while the output is redirected to
a new file with the original *filename*.  When perl exits, cleanly or not,
the original *file* is unlinked.
.Sp
If the extension doesn't contain a \f(CW\*(C`*\*(C', then it is appended to the
end of the current filename as a suffix.  If the extension does
contain one or more \f(CW\*(C`*\*(C' characters, then each \f(CW\*(C`*\*(C' is replaced
with the current filename.  In Perl terms, you could think of this
as:
.Sp
.Vb 1
    ($backup = $extension) =~ s/\\*/$file_name/g;
.Ve
.Sp
This allows you to add a prefix to the backup file, instead of (or in
addition to) a suffix:
.Sp
.Vb 2
 $ perl -pi\*(Aqorig_*\*(Aq -e \*(Aqs/bar/baz/\*(Aq fileA  # backup to
                                           # \*(Aqorig_fileA\*(Aq
.Ve
.Sp
Or even to place backup copies of the original files into another
directory (provided the directory already exists):
.Sp
.Vb 2
 $ perl -pi\*(Aqold/*.orig\*(Aq -e \*(Aqs/bar/baz/\*(Aq fileA  # backup to
                                               # \*(Aqold/fileA.orig\*(Aq
.Ve
.Sp
These sets of one-liners are equivalent:
.Sp
.Vb 2
 $ perl -pi -e \*(Aqs/bar/baz/\*(Aq fileA          # overwrite current file
 $ perl -pi\*(Aq*\*(Aq -e \*(Aqs/bar/baz/\*(Aq fileA       # overwrite current file

 $ perl -pi\*(Aq.orig\*(Aq -e \*(Aqs/bar/baz/\*(Aq fileA   # backup to \*(AqfileA.orig\*(Aq
 $ perl -pi\*(Aq*.orig\*(Aq -e \*(Aqs/bar/baz/\*(Aq fileA  # backup to \*(AqfileA.orig\*(Aq
.Ve
.Sp
From the shell, saying
.Sp
.Vb 1
    $ perl -p -i.orig -e "s/foo/bar/; ... "
.Ve
.Sp
is the same as using the program:
.Sp
.Vb 2
    #!/usr/bin/perl -pi.orig
    s/foo/bar/;
.Ve
.Sp
which is equivalent to
.Sp
.Vb 10
    #!/usr/bin/perl
    $extension = \*(Aq.orig\*(Aq;
    LINE: while (<>) \{
        if ($ARGV ne $oldargv) \{
            if ($extension !~ /\\*/) \{
                $backup = $ARGV . $extension;
            \}
            else \{
                ($backup = $extension) =~ s/\\*/$ARGV/g;
            \}
            rename($ARGV, $backup);
            open(ARGVOUT, ">$ARGV");
            select(ARGVOUT);
            $oldargv = $ARGV;
        \}
        s/foo/bar/;
    \}
    continue \{
        print;  # this prints to original filename
    \}
    select(STDOUT);
.Ve
.Sp
except that the **-i** form doesn't need to compare \f(CW$ARGV to \f(CW$oldargv to
know when the filename has changed.  It does, however, use \s-1ARGVOUT\s0 for
the selected filehandle.  Note that \s-1STDOUT\s0 is restored as the default
output filehandle after the loop.
.Sp
As shown above, Perl creates the backup file whether or not any output
is actually changed.  So this is just a fancy way to copy files:
.Sp
.Vb 3
    $ perl -p -i\*(Aq/some/file/path/*\*(Aq -e 1 file1 file2 file3...
or
    $ perl -p -i\*(Aq.orig\*(Aq -e 1 file1 file2 file3...
.Ve
.Sp
You can use \f(CW\*(C`eof\*(C' without parentheses to locate the end of each input
file, in case you want to append to each file, or reset line numbering
(see example in \*(L"eof\*(R" in perlfunc).
.Sp
If, for a given file, Perl is unable to create the backup file as
specified in the extension then it will skip that file and continue on
with the next one (if it exists).
.Sp
For a discussion of issues surrounding file permissions and **-i**, see
\*(L"Why does Perl let me delete read-only files?  Why does -i clobber
protected files?  Isn't this a bug in Perl?\*(R" in perlfaq5.
.Sp
You cannot use **-i** to create directories or to strip extensions from
files.
.Sp
Perl does not expand \f(CW\*(C`~\*(C' in filenames, which is good, since some
folks use it for their backup files:
.Sp
.Vb 1
    $ perl -pi~ -e \*(Aqs/foo/bar/\*(Aq file1 file2 file3...
.Ve
.Sp
Note that because **-i** renames or deletes the original file before
creating a new file of the same name, Unix-style soft and hard links will
not be preserved.
.Sp
Finally, the **-i** switch does not impede execution when no
files are given on the command line.  In this case, no backup is made
(the original file cannot, of course, be determined) and processing
proceeds from \s-1STDIN\s0 to \s-1STDOUT\s0 as might be expected.

- \fB-I\fIdirectory
Xref "-I @INC"
Item "-Idirectory"
Directories specified by **-I** are prepended to the search path for
modules (\f(CW@INC).

- \fB-l[\fIoctnum]
Xref "-l $ $\"
Item "-l[octnum]"
enables automatic line-ending processing.  It has two separate
effects.  First, it automatically chomps \f(CW$/ (the input record
separator) when used with \*(L"-n\*(R" or \*(L"-p\*(R".  Second, it assigns \f(CW\*(C`$\\\*(C'
(the output record separator) to have the value of *octnum* so
that any print statements will have that separator added back on.
If *octnum* is omitted, sets \f(CW\*(C`$\\\*(C' to the current value of
\f(CW$/.  For instance, to trim lines to 80 columns:
.Sp
.Vb 1
    perl -lpe \*(Aqsubstr($_, 80) = ""\*(Aq
.Ve
.Sp
Note that the assignment \f(CW\*(C`$\\ = $/\*(C' is done when the switch is processed,
so the input record separator can be different than the output record
separator if the **-l** switch is followed by a
-0 switch:
.Sp
.Vb 1
    gnufind / -print0 | perl -ln0e \*(Aqprint "found $_" if -p\*(Aq
.Ve
.Sp
This sets \f(CW\*(C`$\\\*(C' to newline and then sets \f(CW$/ to the null character.

- \fB-m[\fB-]\fImodule
Xref "-m -M"
Item "-m[-]module"
0

- \fB-M[\fB-]\fImodule
Item "-M[-]module"

- \fB-M[\fB-]\fI'module ...'
Item "-M[-]'module ...'"

- \fB-[mM][\fB-]\fImodule=arg[,arg]...
Item "-[mM][-]module=arg[,arg]..."
.PD
**-m***module* executes \f(CW\*(C`use\*(C' *module* \f(CW\*(C`();\*(C' before executing your
program.  This loads the module, but does not call its \f(CW\*(C`import\*(C' method,
so does not import subroutines and does not give effect to a pragma.
.Sp
**-M***module* executes \f(CW\*(C`use\*(C' *module* \f(CW\*(C`;\*(C' before executing your
program.  This loads the module and calls its \f(CW\*(C`import\*(C' method, causing
the module to have its default effect, typically importing subroutines
or giving effect to a pragma.
You can use quotes to add extra code after the module name,
e.g., \f(CW\*(Aq-M\f(CIMODULE\f(CW qw(foo bar)\*(Aq.
.Sp
If the first character after the **-M** or **-m** is a dash (**-**)
then the 'use' is replaced with 'no'.
This makes no difference for **-m**.
.Sp
A little builtin syntactic sugar means you can also say
**-m\f(BI\s-1MODULE\s0**=foo,bar** or \fB-M\f(BI\s-1MODULE\s0\fB=foo,bar** as a shortcut for
**'-M\f(BI\s-1MODULE\s0\fB qw(foo bar)'**.  This avoids the need to use quotes when
importing symbols.  The actual code generated by **-M\f(BI\s-1MODULE\s0\fB=foo,bar** is
\f(CW\*(C`use module split(/,/,q\{foo,bar\})\*(C'.  Note that the \f(CW\*(C`=\*(C' form
removes the distinction between **-m** and **-M**; that is,
**-m\f(BI\s-1MODULE\s0**=foo,bar** is the same as \fB-M\f(BI\s-1MODULE\s0\fB=foo,bar**.
.Sp
A consequence of the \f(CW\*(C`split\*(C' formulation
is that **-M\f(BI\s-1MODULE\s0\fB=number** never does a version check,
unless \f(CW\*(C`\f(CIMODULE\f(CW::import()\*(C' itself is set up to do a version check, which
could happen for example if *\s-1MODULE\s0* inherits from Exporter.

- \fB-n
Xref "-n"
Item "-n"
causes Perl to assume the following loop around your program, which
makes it iterate over filename arguments somewhat like *sed -n* or
*awk*:
.Sp
.Vb 4
  LINE:
    while (<>) \{
        ...             # your program goes here
    \}
.Ve
.Sp
Note that the lines are not printed by default.  See \*(L"-p\*(R" to have
lines printed.  If a file named by an argument cannot be opened for
some reason, Perl warns you about it and moves on to the next file.
.Sp
Also note that \f(CW\*(C`<>\*(C' passes command line arguments to
\*(L"open\*(R" in perlfunc, which doesn't necessarily interpret them as file names.
See  perlop for possible security implications.
.Sp
Here is an efficient way to delete all files that haven't been modified for
at least a week:
.Sp
.Vb 1
    find . -mtime +7 -print | perl -nle unlink
.Ve
.Sp
This is faster than using the **-exec** switch of *find* because you don't
have to start a process on every filename found (but it's not faster
than using the **-delete** switch available in newer versions of *find*.
It does suffer from the bug of mishandling newlines in pathnames, which
you can fix if you follow the example under
-0.
.Sp
\f(CW\*(C`BEGIN\*(C' and \f(CW\*(C`END\*(C' blocks may be used to capture control before or after
the implicit program loop, just as in *awk*.

- \fB-p
Xref "-p"
Item "-p"
causes Perl to assume the following loop around your program, which
makes it iterate over filename arguments somewhat like *sed*:
.Sp
.Vb 6
  LINE:
    while (<>) \{
        ...             # your program goes here
    \} continue \{
        print or die "-p destination: $!\\n";
    \}
.Ve
.Sp
If a file named by an argument cannot be opened for some reason, Perl
warns you about it, and moves on to the next file.  Note that the
lines are printed automatically.  An error occurring during printing is
treated as fatal.  To suppress printing use the \*(L"-n\*(R" switch.  A **-p**
overrides a **-n** switch.
.Sp
\f(CW\*(C`BEGIN\*(C' and \f(CW\*(C`END\*(C' blocks may be used to capture control before or after
the implicit loop, just as in *awk*.

- \fB-s
Xref "-s"
Item "-s"
enables rudimentary switch parsing for switches on the command
line after the program name but before any filename arguments (or before
an argument of **--**).  Any switch found there is removed from \f(CW@ARGV and sets the
corresponding variable in the Perl program.  The following program
prints \*(L"1\*(R" if the program is invoked with a **-xyz** switch, and \*(L"abc\*(R"
if it is invoked with **-xyz=abc**.
.Sp
.Vb 2
    #!/usr/bin/perl -s
    if ($xyz) \{ print "$xyz\\n" \}
.Ve
.Sp
Do note that a switch like **--help** creates the variable \f(CW\*(C`$\{-help\}\*(C', which is
not compliant with \f(CW\*(C`use strict "refs"\*(C'.  Also, when using this option on a
script with warnings enabled you may get a lot of spurious \*(L"used only once\*(R"
warnings.

- \fB-S
Xref "-S"
Item "-S"
makes Perl use the \*(L"\s-1PATH\*(R"\s0 environment variable to search for the
program unless the name of the program contains path separators.
.Sp
On some platforms, this also makes Perl append suffixes to the
filename while searching for it.  For example, on Win32 platforms,
the \*(L".bat\*(R" and \*(L".cmd\*(R" suffixes are appended if a lookup for the
original name fails, and if the name does not already end in one
of those suffixes.  If your Perl was compiled with \f(CW\*(C`DEBUGGING\*(C' turned
on, using the -Dp switch to Perl shows how the search
progresses.
.Sp
Typically this is used to emulate \f(CW\*(C`#!\*(C' startup on platforms that don't
support \f(CW\*(C`#!\*(C'.  It's also convenient when debugging a script that uses \f(CW\*(C`#!\*(C',
and is thus normally found by the shell's \f(CW$PATH search mechanism.
.Sp
This example works on many platforms that have a shell compatible with
Bourne shell:
.Sp
.Vb 3
    #!/usr/bin/perl
    eval \*(Aqexec /usr/bin/perl -wS $0 $\{1+"$@"\}\*(Aq
            if 0; # ^ Run only under a shell
.Ve
.Sp
The system ignores the first line and feeds the program to */bin/sh*,
which proceeds to try to execute the Perl program as a shell script.
The shell executes the second line as a normal shell command, and thus
starts up the Perl interpreter.  On some systems \f(CW$0 doesn't always
contain the full pathname, so the \*(L"-S\*(R" tells Perl to search for the
program if necessary.  After Perl locates the program, it parses the
lines and ignores them because the check 'if 0' is never true.
If the program will be interpreted by csh, you will need
to replace \f(CW\*(C`$\{1+"$@"\}\*(C' with \f(CW$*, even though that doesn't understand
embedded spaces (and such) in the argument list.  To start up *sh* rather
than *csh*, some systems may have to replace the \f(CW\*(C`#!\*(C' line with a line
containing just a colon, which will be politely ignored by Perl.  Other
systems can't control that, and need a totally devious construct that
will work under any of *csh*, *sh*, or Perl, such as the following:
.Sp
.Vb 3
        eval \*(Aq(exit $?0)\*(Aq && eval \*(Aqexec perl -wS $0 $\{1+"$@"\}\*(Aq
        & eval \*(Aqexec /usr/bin/perl -wS $0 $argv:q\*(Aq
                if 0; # ^ Run only under a shell
.Ve
.Sp
If the filename supplied contains directory separators (and so is an
absolute or relative pathname), and if that file is not found,
platforms that append file extensions will do so and try to look
for the file with those extensions added, one by one.
.Sp
On DOS-like platforms, if the program does not contain directory
separators, it will first be searched for in the current directory
before being searched for on the \s-1PATH.\s0  On Unix platforms, the
program will be searched for strictly on the \s-1PATH.\s0

- \fB-t
Xref "-t"
Item "-t"
Like \*(L"-T\*(R", but taint checks will issue warnings rather than fatal
errors.  These warnings can now be controlled normally with \f(CW\*(C`no warnings
qw(taint)\*(C'.
.Sp
**Note: This is not a substitute for \f(CB\*(C`-T\*(C'\fB!** This is meant to be
used *only* as a temporary development aid while securing legacy code:
for real production code and for new secure code written from scratch,
always use the real \*(L"-T\*(R".

- \fB-T
Xref "-T"
Item "-T"
turns on \*(L"taint\*(R" so you can test them.  Ordinarily
these checks are done only when running setuid or setgid.  It's a
good idea to turn them on explicitly for programs that run on behalf
of someone else whom you might not necessarily trust, such as \s-1CGI\s0
programs or any internet servers you might write in Perl.  See
perlsec for details.  For security reasons, this option must be
seen by Perl quite early; usually this means it must appear early
on the command line or in the \f(CW\*(C`#!\*(C' line for systems which support
that construct.

- \fB-u
Xref "-u"
Item "-u"
This switch causes Perl to dump core after compiling your
program.  You can then in theory take this core dump and turn it
into an executable file by using the *undump* program (not supplied).
This speeds startup at the expense of some disk space (which you
can minimize by stripping the executable).  (Still, a \*(L"hello world\*(R"
executable comes out to about 200K on my machine.)  If you want to
execute a portion of your program before dumping, use the \f(CW\*(C`CORE::dump()\*(C'
function instead.  Note: availability of *undump* is platform
specific and may not be available for a specific port of Perl.

- \fB-U
Xref "-U"
Item "-U"
allows Perl to do unsafe operations.  Currently the only \*(L"unsafe\*(R"
operations are attempting to unlink directories while running as superuser
and running setuid programs with fatal taint checks turned into warnings.
Note that warnings must be enabled along with this option to actually
*generate* the taint-check warnings.

- \fB-v
Xref "-v"
Item "-v"
prints the version and patchlevel of your perl executable.

- \fB-V
Xref "-V"
Item "-V"
prints summary of the major perl configuration values and the current
values of \f(CW@INC.

- \fB-V:\fIconfigvar
Item "-V:configvar"
Prints to \s-1STDOUT\s0 the value of the named configuration variable(s),
with multiples when your \f(CW\*(C`\f(CIconfigvar\f(CW\*(C' argument looks like a regex (has
non-letters).  For example:
.Sp
.Vb 12
    $ perl -V:libc
        libc=\*(Aq/lib/libc-2.2.4.so\*(Aq;
    $ perl -V:lib.
        libs=\*(Aq-lnsl -lgdbm -ldb -ldl -lm -lcrypt -lutil -lc\*(Aq;
        libc=\*(Aq/lib/libc-2.2.4.so\*(Aq;
    $ perl -V:lib.*
        libpth=\*(Aq/usr/local/lib /lib /usr/lib\*(Aq;
        libs=\*(Aq-lnsl -lgdbm -ldb -ldl -lm -lcrypt -lutil -lc\*(Aq;
        lib_ext=\*(Aq.a\*(Aq;
        libc=\*(Aq/lib/libc-2.2.4.so\*(Aq;
        libperl=\*(Aqlibperl.a\*(Aq;
        ....
.Ve
.Sp
Additionally, extra colons can be used to control formatting.  A
trailing colon suppresses the linefeed and terminator \*(L";\*(R", allowing
you to embed queries into shell commands.  (mnemonic: \s-1PATH\s0 separator
\*(L":\*(R".)
.Sp
.Vb 2
    $ echo "compression-vars: " \`perl -V:z.*: \` " are here !"
    compression-vars:  zcat=\*(Aq\*(Aq zip=\*(Aqzip\*(Aq  are here !
.Ve
.Sp
A leading colon removes the \*(L"name=\*(R" part of the response, this allows
you to map to the name you need.  (mnemonic: empty label)
.Sp
.Vb 2
    $ echo "goodvfork="\`./perl -Ilib -V::usevfork\`
    goodvfork=false;
.Ve
.Sp
Leading and trailing colons can be used together if you need
positional parameter values without the names.  Note that in the case
below, the \f(CW\*(C`PERL_API\*(C' params are returned in alphabetical order.
.Sp
.Vb 2
    $ echo building_on \`perl -V::osname: -V::PERL_API_.*:\` now
    building_on \*(Aqlinux\*(Aq \*(Aq5\*(Aq \*(Aq1\*(Aq \*(Aq9\*(Aq now
.Ve

- \fB-w
Xref "-w"
Item "-w"
prints warnings about dubious constructs, such as variable names
mentioned only once and scalar variables used
before being set; redefined subroutines; references to undefined
filehandles; filehandles opened read-only that you are attempting
to write on; values used as a number that don't *look* like numbers;
using an array as though it were a scalar; if your subroutines
recurse more than 100 deep; and innumerable other things.
.Sp
This switch really just enables the global \f(CW$^W variable; normally,
the lexically scoped \f(CW\*(C`use warnings\*(C' pragma is preferred. You
can disable or promote into fatal errors specific warnings using
\f(CW\*(C`_\|_WARN_\|_\*(C' hooks, as described in perlvar and \*(L"warn\*(R" in perlfunc.
See also perldiag and perltrap.  A fine-grained warning
facility is also available if you want to manipulate entire classes
of warnings; see warnings.

- \fB-W
Xref "-W"
Item "-W"
Enables all warnings regardless of \f(CW\*(C`no warnings\*(C' or \f(CW$^W.
See warnings.

- \fB-X
Xref "-X"
Item "-X"
Disables all warnings regardless of \f(CW\*(C`use warnings\*(C' or \f(CW$^W.
See warnings.
.Sp
Forbidden in \f(CW"PERL5OPT".

- \fB-x
Xref "-x"
Item "-x"
0

- \fB-x\fIdirectory
Item "-xdirectory"
.PD
tells Perl that the program is embedded in a larger chunk of unrelated
text, such as in a mail message.  Leading garbage will be
discarded until the first line that starts with \f(CW\*(C`#!\*(C' and contains the
string \*(L"perl\*(R".  Any meaningful switches on that line will be applied.
.Sp
All references to line numbers by the program (warnings, errors, ...)
will treat the \f(CW\*(C`#!\*(C' line as the first line.
Thus a warning on the 2nd line of the program, which is on the 100th
line in the file will be reported as line 2, not as line 100.
This can be overridden by using the \f(CW\*(C`#line\*(C' directive.
(See \*(L"Plain Old Comments (Not!)\*(R" in perlsyn)
.Sp
If a directory name is specified, Perl will switch to that directory
before running the program.  The **-x** switch controls only the
disposal of leading garbage.  The program must be terminated with
\f(CW\*(C`_\|_END_\|_\*(C' if there is trailing garbage to be ignored;  the program
can process any or all of the trailing garbage via the \f(CW\*(C`DATA\*(C' filehandle
if desired.
.Sp
The directory, if specified, must appear immediately following the **-x**
with no intervening whitespace.

## ENVIRONMENT

Xref "perl, environment variables"
Header "ENVIRONMENT"

- \s-1HOME\s0
Xref "HOME"
Item "HOME"
Used if \f(CW\*(C`chdir\*(C' has no argument.

- \s-1LOGDIR\s0
Xref "LOGDIR"
Item "LOGDIR"
Used if \f(CW\*(C`chdir\*(C' has no argument and \*(L"\s-1HOME\*(R"\s0 is not set.

- \s-1PATH\s0
Xref "PATH"
Item "PATH"
Used in executing subprocesses, and in finding the program if \*(L"-S\*(R" is
used.

- \s-1PERL5LIB\s0
Xref "PERL5LIB"
Item "PERL5LIB"
A list of directories in which to look for Perl library files before
looking in the standard library.
Any architecture-specific and version-specific directories,
such as *version/archname/*, *version/*, or *archname/* under the
specified locations are automatically included if they exist, with this
lookup done at interpreter startup time.  In addition, any directories
matching the entries in \f(CW$Config\{inc_version_list\} are added.
(These typically would be for older compatible perl versions installed
in the same directory tree.)
.Sp
If \s-1PERL5LIB\s0 is not defined, \*(L"\s-1PERLLIB\*(R"\s0 is used.  Directories are separated
(like in \s-1PATH\s0) by a colon on Unixish platforms and by a semicolon on
Windows (the proper path separator being given by the command \f(CW\*(C`perl
-V:\f(CIpath_sep\f(CW\*(C').
.Sp
When running taint checks, either because the program was running setuid or
setgid, or the \*(L"-T\*(R" or \*(L"-t\*(R" switch was specified, neither \s-1PERL5LIB\s0 nor
\*(L"\s-1PERLLIB\*(R"\s0 is consulted. The program should instead say:
.Sp
.Vb 1
    use lib "/my/directory";
.Ve

- \s-1PERL5OPT\s0
Xref "PERL5OPT"
Item "PERL5OPT"
Command-line options (switches).  Switches in this variable are treated
as if they were on every Perl command line.  Only the **-[CDIMTUWdmtw]**
switches are allowed.  When running taint checks (either because the
program was running setuid or setgid, or because the \*(L"-T\*(R" or \*(L"-t\*(R"
switch was used), this variable is ignored.  If \s-1PERL5OPT\s0 begins with
**-T**, tainting will be enabled and subsequent options ignored.  If
\s-1PERL5OPT\s0 begins with **-t**, tainting will be enabled, a writable dot
removed from \f(CW@INC, and subsequent options honored.

- \s-1PERLIO\s0
Xref "PERLIO"
Item "PERLIO"
A space (or colon) separated list of PerlIO layers. If perl is built
to use PerlIO system for \s-1IO\s0 (the default) these layers affect Perl's \s-1IO.\s0
.Sp
It is conventional to start layer names with a colon (for example, \f(CW\*(C`:perlio\*(C') to
emphasize their similarity to variable \*(L"attributes\*(R". But the code that parses
layer specification strings, which is also used to decode the \s-1PERLIO\s0
environment variable, treats the colon as a separator.
.Sp
An unset or empty \s-1PERLIO\s0 is equivalent to the default set of layers for
your platform; for example, \f(CW\*(C`:unix:perlio\*(C' on Unix-like systems
and \f(CW\*(C`:unix:crlf\*(C' on Windows and other DOS-like systems.
.Sp
The list becomes the default for *all* Perl's \s-1IO.\s0 Consequently only built-in
layers can appear in this list, as external layers (such as \f(CW\*(C`:encoding()\*(C') need
\s-1IO\s0 in order to load them!  See \*(L"open pragma\*(R" for how to add external
encodings as defaults.
.Sp
Layers it makes sense to include in the \s-1PERLIO\s0 environment
variable are briefly summarized below. For more details see PerlIO.

> 
- :crlf
Xref ":crlf"
Item ":crlf"
A layer which does \s-1CRLF\s0 to \f(CW"\\n" translation distinguishing \*(L"text\*(R" and
\*(L"binary\*(R" files in the manner of MS-DOS and similar operating systems,
and also provides buffering similar to \f(CW\*(C`:perlio\*(C' on these architectures.

- :perlio
Xref ":perlio"
Item ":perlio"
This is a re-implementation of stdio-like buffering written as a
PerlIO layer.  As such it will call whatever layer is below it for
its operations, typically \f(CW\*(C`:unix\*(C'.

- :stdio
Xref ":stdio"
Item ":stdio"
This layer provides a PerlIO interface by wrapping system's \s-1ANSI C\s0 \*(L"stdio\*(R"
library calls. The layer provides both buffering and \s-1IO.\s0
Note that the \f(CW\*(C`:stdio\*(C' layer does *not* do \s-1CRLF\s0 translation even if that
is the platform's normal behaviour. You will need a \f(CW\*(C`:crlf\*(C' layer above it
to do that.

- :unix
Xref ":unix"
Item ":unix"
Low-level layer that calls \f(CW\*(C`read\*(C', \f(CW\*(C`write\*(C', \f(CW\*(C`lseek\*(C', etc.

- :win32
Xref ":win32"
Item ":win32"
On Win32 platforms this *experimental* layer uses native \*(L"handle\*(R" \s-1IO\s0
rather than a Unix-like numeric file descriptor layer. Known to be
buggy in this release (5.30).



> .Sp
The default set of layers should give acceptable results on all platforms.
.Sp
For Unix platforms that will be the equivalent of \*(L":unix:perlio\*(R" or \*(L":stdio\*(R".
Configure is set up to prefer the \*(L":stdio\*(R" implementation if the system's library
provides for fast access to the buffer (not common on modern architectures);
otherwise, it uses the \*(L":unix:perlio\*(R" implementation.
.Sp
On Win32 the default in this release (5.30) is \*(L":unix:crlf\*(R". Win32's \*(L":stdio\*(R"
has a number of bugs/mis-features for Perl \s-1IO\s0 which are somewhat depending
on the version and vendor of the C compiler. Using our own \f(CW\*(C`:crlf\*(C' layer as
the buffer avoids those issues and makes things more uniform.
.Sp
This release (5.30) uses \f(CW\*(C`:unix\*(C' as the bottom layer on Win32, and so still
uses the C compiler's numeric file descriptor routines. There is an
experimental native \f(CW\*(C`:win32\*(C' layer, which is expected to be enhanced and
may eventually become the default under Win32.
.Sp
The \s-1PERLIO\s0 environment variable is completely ignored when Perl
is run in taint mode.



- \s-1PERLIO_DEBUG\s0
Xref "PERLIO_DEBUG"
Item "PERLIO_DEBUG"
If set to the name of a file or device when Perl is run with the
-Di command-line switch, the logging of certain operations
of the PerlIO subsystem will be redirected to the specified file rather
than going to stderr, which is the default. The file is opened in append
mode. Typical uses are in Unix:
.Sp
.Vb 1
   % env PERLIO_DEBUG=/tmp/perlio.log perl -Di script ...
.Ve
.Sp
and under Win32, the approximately equivalent:
.Sp
.Vb 2
   > set PERLIO_DEBUG=CON
   perl -Di script ...
.Ve
.Sp
This functionality is disabled for setuid scripts, for scripts run
with \*(L"-T\*(R", and for scripts run on a Perl built without \f(CW\*(C`-DDEBUGGING\*(C'
support.

- \s-1PERLLIB\s0
Xref "PERLLIB"
Item "PERLLIB"
A list of directories in which to look for Perl library
files before looking in the standard library.
If \*(L"\s-1PERL5LIB\*(R"\s0 is defined, \s-1PERLLIB\s0 is not used.
.Sp
The \s-1PERLLIB\s0 environment variable is completely ignored when Perl
is run in taint mode.

- \s-1PERL5DB\s0
Xref "PERL5DB"
Item "PERL5DB"
The command used to load the debugger code.  The default is:
.Sp
.Vb 1
        BEGIN \{ require "perl5db.pl" \}
.Ve
.Sp
The \s-1PERL5DB\s0 environment variable is only used when Perl is started with
a bare \*(L"-d\*(R" switch.

- \s-1PERL5DB_THREADED\s0
Xref "PERL5DB_THREADED"
Item "PERL5DB_THREADED"
If set to a true value, indicates to the debugger that the code being
debugged uses threads.

- \s-1PERL5SHELL\s0 (specific to the Win32 port)
Xref "PERL5SHELL"
Item "PERL5SHELL (specific to the Win32 port)"
On Win32 ports only, may be set to an alternative shell that Perl must use
internally for executing \*(L"backtick\*(R" commands or **system()**.  Default is
\f(CW\*(C`cmd.exe /x/d/c\*(C' on WindowsNT and \f(CW\*(C`command.com /c\*(C' on Windows95.  The
value is considered space-separated.  Precede any character that
needs to be protected, like a space or backslash, with another backslash.
.Sp
Note that Perl doesn't use \s-1COMSPEC\s0 for this purpose because
\s-1COMSPEC\s0 has a high degree of variability among users, leading to
portability concerns.  Besides, Perl can use a shell that may not be
fit for interactive use, and setting \s-1COMSPEC\s0 to such a shell may
interfere with the proper functioning of other programs (which usually
look in \s-1COMSPEC\s0 to find a shell fit for interactive use).
.Sp
Before Perl 5.10.0 and 5.8.8, \s-1PERL5SHELL\s0 was not taint checked
when running external commands.  It is recommended that
you explicitly set (or delete) \f(CW$ENV\{PERL5SHELL\} when running
in taint mode under Windows.

- \s-1PERL_ALLOW_NON_IFS_LSP\s0 (specific to the Win32 port)
Xref "PERL_ALLOW_NON_IFS_LSP"
Item "PERL_ALLOW_NON_IFS_LSP (specific to the Win32 port)"
Set to 1 to allow the use of non-IFS compatible LSPs (Layered Service Providers).
Perl normally searches for an IFS-compatible \s-1LSP\s0 because this is required
for its emulation of Windows sockets as real filehandles.  However, this may
cause problems if you have a firewall such as *McAfee Guardian*, which requires
that all applications use its \s-1LSP\s0 but which is not IFS-compatible, because clearly
Perl will normally avoid using such an \s-1LSP.\s0
.Sp
Setting this environment variable to 1 means that Perl will simply use the
first suitable \s-1LSP\s0 enumerated in the catalog, which keeps *McAfee Guardian*
happy\*(--and in that particular case Perl still works too because \fIMcAfee
Guardian's \s-1LSP\s0 actually plays other games which allow applications
requiring \s-1IFS\s0 compatibility to work.

- \s-1PERL_DEBUG_MSTATS\s0
Xref "PERL_DEBUG_MSTATS"
Item "PERL_DEBUG_MSTATS"
Relevant only if Perl is compiled with the \f(CW\*(C`malloc\*(C' included with the Perl
distribution; that is, if \f(CW\*(C`perl -V:d_mymalloc\*(C' is \*(L"define\*(R".
.Sp
If set, this dumps out memory statistics after execution.  If set
to an integer greater than one, also dumps out memory statistics
after compilation.

- \s-1PERL_DESTRUCT_LEVEL\s0
Xref "PERL_DESTRUCT_LEVEL"
Item "PERL_DESTRUCT_LEVEL"
Controls the behaviour of global destruction of objects and other
references.  See \*(L"\s-1PERL_DESTRUCT_LEVEL\*(R"\s0 in perlhacktips for more information.

- \s-1PERL_DL_NONLAZY\s0
Xref "PERL_DL_NONLAZY"
Item "PERL_DL_NONLAZY"
Set to \f(CW"1" to have Perl resolve *all* undefined symbols when it loads
a dynamic library.  The default behaviour is to resolve symbols when
they are used.  Setting this variable is useful during testing of
extensions, as it ensures that you get an error on misspelled function
names even if the test suite doesn't call them.

- \s-1PERL_ENCODING\s0
Xref "PERL_ENCODING"
Item "PERL_ENCODING"
If using the \f(CW\*(C`use encoding\*(C' pragma without an explicit encoding name, the
\s-1PERL_ENCODING\s0 environment variable is consulted for an encoding name.

- \s-1PERL_HASH_SEED\s0
Xref "PERL_HASH_SEED"
Item "PERL_HASH_SEED"
(Since Perl 5.8.1, new semantics in Perl 5.18.0)  Used to override
the randomization of Perl's internal hash function. The value is expressed
in hexadecimal, and may include a leading 0x. Truncated patterns
are treated as though they are suffixed with sufficient 0's as required.
.Sp
If the option is provided, and \f(CW\*(C`PERL_PERTURB_KEYS\*(C' is \s-1NOT\s0 set, then
a value of '0' implies \f(CW\*(C`PERL_PERTURB_KEYS=0\*(C' and any other value
implies \f(CW\*(C`PERL_PERTURB_KEYS=2\*(C'.
.Sp
**\s-1PLEASE NOTE:\s0 The hash seed is sensitive information**. Hashes are
randomized to protect against local and remote attacks against Perl
code. By manually setting a seed, this protection may be partially or
completely lost.
.Sp
See \*(L"Algorithmic Complexity Attacks\*(R" in perlsec, \*(L"\s-1PERL_PERTURB_KEYS\*(R"\s0, and
\*(L"\s-1PERL_HASH_SEED_DEBUG\*(R"\s0 for more information.

- \s-1PERL_PERTURB_KEYS\s0
Xref "PERL_PERTURB_KEYS"
Item "PERL_PERTURB_KEYS"
(Since Perl 5.18.0)  Set to \f(CW"0" or \f(CW"NO" then traversing keys
will be repeatable from run to run for the same \f(CW\*(C`PERL_HASH_SEED\*(C'.
Insertion into a hash will not change the order, except to provide
for more space in the hash. When combined with setting \s-1PERL_HASH_SEED\s0
this mode is as close to pre 5.18 behavior as you can get.
.Sp
When set to \f(CW"1" or \f(CW"RANDOM" then traversing keys will be randomized.
Every time a hash is inserted into the key order will change in a random
fashion. The order may not be repeatable in a following program run
even if the \s-1PERL_HASH_SEED\s0 has been specified. This is the default
mode for perl.
.Sp
When set to \f(CW"2" or \f(CW"DETERMINISTIC" then inserting keys into a hash
will cause the key order to change, but in a way that is repeatable
from program run to program run.
.Sp
**\s-1NOTE:\s0** Use of this option is considered insecure, and is intended only
for debugging non-deterministic behavior in Perl's hash function. Do
not use it in production.
.Sp
See \*(L"Algorithmic Complexity Attacks\*(R" in perlsec and \*(L"\s-1PERL_HASH_SEED\*(R"\s0
and \*(L"\s-1PERL_HASH_SEED_DEBUG\*(R"\s0 for more information. You can get and set the
key traversal mask for a specific hash by using the \f(CW\*(C`hash_traversal_mask()\*(C'
function from Hash::Util.

- \s-1PERL_HASH_SEED_DEBUG\s0
Xref "PERL_HASH_SEED_DEBUG"
Item "PERL_HASH_SEED_DEBUG"
(Since Perl 5.8.1.)  Set to \f(CW"1" to display (to \s-1STDERR\s0) information
about the hash function, seed, and what type of key traversal
randomization is in effect at the beginning of execution.  This, combined
with \*(L"\s-1PERL_HASH_SEED\*(R"\s0 and \*(L"\s-1PERL_PERTURB_KEYS\*(R"\s0 is intended to aid in
debugging nondeterministic behaviour caused by hash randomization.
.Sp
**Note** that any information about the hash function, especially the hash
seed is **sensitive information**: by knowing it, one can craft a denial-of-service
attack against Perl code, even remotely; see \*(L"Algorithmic Complexity Attacks\*(R" in perlsec
for more information. **Do not disclose the hash seed** to people who
don't need to know it. See also \f(CW\*(C`hash_seed()\*(C' and
\f(CW\*(C`hash_traversal_mask()\*(C'.
.Sp
An example output might be:
.Sp
.Vb 1
 HASH_FUNCTION = ONE_AT_A_TIME_HARD HASH_SEED = 0x652e9b9349a7a032 PERTURB_KEYS = 1 (RANDOM)
.Ve

- \s-1PERL_MEM_LOG\s0
Xref "PERL_MEM_LOG"
Item "PERL_MEM_LOG"
If your Perl was configured with **-Accflags=-DPERL_MEM_LOG**, setting
the environment variable \f(CW\*(C`PERL_MEM_LOG\*(C' enables logging debug
messages. The value has the form \f(CW\*(C`<\f(CInumber\f(CW>[m][s][t]\*(C', where
\f(CW\*(C`\f(CInumber\f(CW\*(C' is the file descriptor number you want to write to (2 is
default), and the combination of letters specifies that you want
information about (m)emory and/or (s)v, optionally with
(t)imestamps. For example, \f(CW\*(C`PERL_MEM_LOG=1mst\*(C' logs all
information to stdout. You can write to other opened file descriptors
in a variety of ways:
.Sp
.Vb 1
  $ 3>foo3 PERL_MEM_LOG=3m perl ...
.Ve

- \s-1PERL_ROOT\s0 (specific to the \s-1VMS\s0 port)
Xref "PERL_ROOT"
Item "PERL_ROOT (specific to the VMS port)"
A translation-concealed rooted logical name that contains Perl and the
logical device for the \f(CW@INC path on \s-1VMS\s0 only.  Other logical names that
affect Perl on \s-1VMS\s0 include \s-1PERLSHR, PERL_ENV_TABLES,\s0 and
\s-1SYS$TIMEZONE_DIFFERENTIAL,\s0 but are optional and discussed further in
perlvms and in *\s-1README\s0.vms* in the Perl source distribution.

- \s-1PERL_SIGNALS\s0
Xref "PERL_SIGNALS"
Item "PERL_SIGNALS"
Available in Perls 5.8.1 and later.  If set to \f(CW"unsafe", the pre-Perl-5.8.0
signal behaviour (which is immediate but unsafe) is restored.  If set
to \f(CW\*(C`safe\*(C', then safe (but deferred) signals are used.  See
\*(L"Deferred Signals (Safe Signals)\*(R" in perlipc.

- \s-1PERL_UNICODE\s0
Xref "PERL_UNICODE"
Item "PERL_UNICODE"
Equivalent to the -C command-line switch.  Note
that this is not a boolean variable. Setting this to \f(CW"1" is not the
right way to \*(L"enable Unicode\*(R" (whatever that would mean).  You can use
\f(CW"0" to \*(L"disable Unicode\*(R", though (or alternatively unset \s-1PERL_UNICODE\s0
in your shell before starting Perl).  See the description of the
-C switch for more information.

- \s-1PERL_USE_UNSAFE_INC\s0
Xref "PERL_USE_UNSAFE_INC"
Item "PERL_USE_UNSAFE_INC"
If perl has been configured to not have the current directory in
\f(CW@INC by default, this variable can be set to \f(CW"1"
to reinstate it.  It's primarily intended for use while building and
testing modules that have not been updated to deal with \*(L".\*(R" not being in
\f(CW@INC and should not be set in the environment for day-to-day use.

- \s-1SYS$LOGIN\s0 (specific to the \s-1VMS\s0 port)
Xref "SYS$LOGIN"
Item "SYS$LOGIN (specific to the VMS port)"
Used if chdir has no argument and \*(L"\s-1HOME\*(R"\s0 and \*(L"\s-1LOGDIR\*(R"\s0 are not set.

- \s-1PERL_INTERNAL_RAND_SEED\s0
Xref "PERL_INTERNAL_RAND_SEED"
Item "PERL_INTERNAL_RAND_SEED"
Set to a non-negative integer to seed the random number generator used
internally by perl for a variety of purposes.
.Sp
Ignored if perl is run setuid or setgid.  Used only for some limited
startup randomization (hash keys) if \f(CW\*(C`-T\*(C' or \f(CW\*(C`-t\*(C' perl is started
with tainting enabled.
.Sp
Perl may be built to ignore this variable.

Perl also has environment variables that control how Perl handles data
specific to particular natural languages; see perllocale.

Perl and its various modules and components, including its test frameworks,
may sometimes make use of certain other environment variables.  Some of
these are specific to a particular platform.  Please consult the
appropriate module documentation and any documentation for your platform
(like perlsolaris, perllinux, perlmacosx, perlwin32, etc) for
variables peculiar to those specific situations.

Perl makes all environment variables available to the program being
executed, and passes these along to any child processes it starts.
However, programs running setuid would do well to execute the following
lines before doing anything else, just to keep people honest:

.Vb 3
    $ENV\{PATH\}  = "/bin:/usr/bin";    # or whatever you need
    $ENV\{SHELL\} = "/bin/sh" if exists $ENV\{SHELL\};
    delete @ENV\{qw(IFS CDPATH ENV BASH_ENV)\};
.Ve

## ORDER OF APPLICATION

Header "ORDER OF APPLICATION"
Some options, in particular \f(CW\*(C`-I\*(C', \f(CW\*(C`-M\*(C', \f(CW\*(C`PERL5LIB\*(C' and \f(CW\*(C`PERL5OPT\*(C' can
interact, and the order in which they are applied is important.

Note that this section does not document what *actually* happens inside the
perl interpreter, it documents what *effectively* happens.

- -I
Item "-I"
The effect of multiple \f(CW\*(C`-I\*(C' options is to \f(CW\*(C`unshift\*(C' them onto \f(CW@INC
from right to left. So for example:
.Sp
.Vb 1
    perl -I 1 -I 2 -I 3
.Ve
.Sp
will first prepend \f(CW3 onto the front of \f(CW@INC, then prepend \f(CW2, and
then prepend \f(CW1. The result is that \f(CW@INC begins with:
.Sp
.Vb 1
    qw(1 2 3)
.Ve

- -M
Item "-M"
Multiple \f(CW\*(C`-M\*(C' options are processed from left to right. So this:
.Sp
.Vb 1
    perl -Mlib=1 -Mlib=2 -Mlib=3
.Ve
.Sp
will first use the lib pragma to prepend \f(CW1 to \f(CW@INC, then
it will prepend \f(CW2, then it will prepend \f(CW3, resulting in an \f(CW@INC
that begins with:
.Sp
.Vb 1
    qw(3 2 1)
.Ve

- the \s-1PERL5LIB\s0 environment variable
Item "the PERL5LIB environment variable"
This contains a list of directories, separated by colons. The entire list
is prepended to \f(CW@INC in one go. This:
.Sp
.Vb 1
    PERL5LIB=1:2:3 perl
.Ve
.Sp
will result in an \f(CW@INC that begins with:
.Sp
.Vb 1
    qw(1 2 3)
.Ve

- combinations of -I, -M and \s-1PERL5LIB\s0
Item "combinations of -I, -M and PERL5LIB"
\f(CW\*(C`PERL5LIB\*(C' is applied first, then all the \f(CW\*(C`-I\*(C' arguments, then all the
\f(CW\*(C`-M\*(C' arguments. This:
.Sp
.Vb 1
    PERL5LIB=e1:e2 perl -I i1 -Mlib=m1 -I i2 -Mlib=m2
.Ve
.Sp
will result in an \f(CW@INC that begins with:
.Sp
.Vb 1
    qw(m2 m1 i1 i2 e1 e2)
.Ve

- the \s-1PERL5OPT\s0 environment variable
Item "the PERL5OPT environment variable"
This contains a space separated list of switches. We only consider the
effects of \f(CW\*(C`-M\*(C' and \f(CW\*(C`-I\*(C' in this section.
.Sp
After normal processing of \f(CW\*(C`-I\*(C' switches from the command line, all
the \f(CW\*(C`-I\*(C' switches in \f(CW\*(C`PERL5OPT\*(C' are extracted. They are processed from
left to right instead of from right to left. Also note that while
whitespace is allowed between a \f(CW\*(C`-I\*(C' and its directory on the command
line, it is not allowed in \f(CW\*(C`PERL5OPT\*(C'.
.Sp
After normal processing of \f(CW\*(C`-M\*(C' switches from the command line, all
the \f(CW\*(C`-M\*(C' switches in \f(CW\*(C`PERL5OPT\*(C' are extracted. They are processed from
left to right, *i.e.* the same as those on the command line.
.Sp
An example may make this clearer:
.Sp
.Vb 3
    export PERL5OPT="-Mlib=optm1 -Iopti1 -Mlib=optm2 -Iopti2"
    export PERL5LIB=e1:e2
    perl -I i1 -Mlib=m1 -I i2 -Mlib=m2
.Ve
.Sp
will result in an \f(CW@INC that begins with:
.Sp
.Vb 3
    qw(
        optm2
        optm1

        m2
        m1

        opti2
        opti1

        i1
        i2

        e1
        e2
    )
.Ve

- Other complications
Item "Other complications"
There are some complications that are ignored in the examples above:

> 
- arch and version subdirs
Item "arch and version subdirs"
All of \f(CW\*(C`-I\*(C', \f(CW\*(C`PERL5LIB\*(C' and \f(CW\*(C`use lib\*(C' will also prepend arch and version
subdirs if they are present

- sitecustomize.pl
Item "sitecustomize.pl"



> 

