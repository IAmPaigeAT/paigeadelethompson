+++
date = "2022-02-19"
operating_system = "macos"
description = "This document describes various features of FreeBSD that will affect how Perl version 5 (hereafter just Perl) is compiled and/or runs. When perl is configured to use ithreads, it will use re-entrant library calls in preference to non-re-entrant ver..."
manpage_section = "1"
title = "perlfreebsd(1)"
manpage_name = "perlfreebsd"
author = "None Specified"
manpage_format = "troff"
detected_package_version = "5.34.1"
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLFREEBSD 1"
PERLFREEBSD 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlfreebsd - Perl version 5 on FreeBSD systems

## DESCRIPTION

Header "DESCRIPTION"
This document describes various features of FreeBSD that will affect how Perl
version 5 (hereafter just Perl) is compiled and/or runs.

### FreeBSD core dumps from readdir_r with ithreads

Subsection "FreeBSD core dumps from readdir_r with ithreads"
When perl is configured to use ithreads, it will use re-entrant library calls
in preference to non-re-entrant versions.  There is a bug in FreeBSD's
\f(CW\*(C`readdir_r\*(C' function in versions 4.5 and earlier that can cause a \s-1SEGV\s0 when
reading large directories. A patch for FreeBSD libc is available
(see <http://www.freebsd.org/cgi/query-pr.cgi?pr=misc/30631> )
which has been integrated into FreeBSD 4.6.
.ie n .SS "$^X doesn't always contain a full path in FreeBSD"
.el .SS "\f(CW$^X doesn't always contain a full path in FreeBSD"
Subsection "$^X doesn't always contain a full path in FreeBSD"
perl sets \f(CW$^X where possible to a full path by asking the operating
system. On FreeBSD the full path of the perl interpreter is found by using
\f(CW\*(C`sysctl\*(C' with \f(CW\*(C`KERN_PROC_PATHNAME\*(C' if that is supported, else by reading
the symlink */proc/curproc/file*. FreeBSD 7 and earlier has a bug where
either approach sometimes returns an incorrect value
(see <http://www.freebsd.org/cgi/query-pr.cgi?pr=35703> ).
In these cases perl will fall back to the old behaviour of using C's
\f(CW\*(C`argv[0]\*(C' value for \f(CW$^X.

## AUTHOR

Header "AUTHOR"
Nicholas Clark <nick@ccl4.org>, collating wisdom supplied by Slaven Rezic
and Tim Bunce.

Please report any errors, updates, or suggestions to
<https://github.com/Perl/perl5/issues>.
