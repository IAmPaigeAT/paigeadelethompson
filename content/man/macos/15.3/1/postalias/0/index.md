+++
author = "None Specified"
operating_system_version = "15.3"
manpage_format = "troff"
description = "The postalias(1) command creates or queries one or more Postfix alias databases, or updates an existing one. The input and output file formats are expected to be compatible with Sendmail version 8, and are expected to be suitable for the use as NIS..."
manpage_section = "1"
operating_system = "macos"
manpage_name = "postalias"
date = "Sun Feb 16 04:48:18 2025"
detected_package_version = "0"
title = "postalias(1)"
keywords = ["na", "aliases", "format", "of", "alias", "database", "input", "file", "local", "postfix", "delivery", "agent", "postconf", "supported", "types", "configuration", "parameters", "postmap", "create", "update", "query", "lookup", "tables", "newaliases", "sendmail", "compatibility", "interface", "syslogd", "system", "logging", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "database_readme", "table", "overview", "license", "the", "secure", "mailer", "must", "be", "distributed", "with", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
+++

POSTALIAS 1


## NAME

postalias
-
Postfix alias database maintenance

## SYNOPSIS

.na

```
```

**postalias** [**-Nfinoprsuvw**] [**-c **config_dir]
[**-d **key] [**-q **key]
        [*file_type*:]*file_name* ...

## DESCRIPTION


The **postalias**(1) command creates or queries one or more Postfix
alias databases, or updates an existing one. The input and output
file formats are expected to be compatible with Sendmail version 8,
and are expected to be suitable for the use as NIS alias maps.

If the result files do not exist they will be created with the
same group and other read permissions as their source file.

While a database update is in progress, signal delivery is
postponed, and an exclusive, advisory, lock is placed on the
entire database, in order to avoid surprises in spectator
processes.

The format of Postfix alias input files is described in
**aliases**(5).

By default the lookup key is mapped to lowercase to make
the lookups case insensitive; as of Postfix 2.3 this case
folding happens only with tables whose lookup keys are
fixed-case strings such as btree:, dbm: or hash:. With
earlier versions, the lookup key is folded even with tables
where a lookup field can match both upper and lower case
text, such as regexp: and pcre:. This resulted in loss of
information with $*number* substitutions.

Options:

- \fB-c
Read the **main.cf** configuration file in the named directory
instead of the default configuration directory.

- \fB-d
Search the specified maps for *key* and remove one entry per map.
The exit status is zero when the requested information was found.

If a key value of **-** is specified, the program reads key
values from the standard input stream. The exit status is zero
when at least one of the requested keys was found.
**-f**
Do not fold the lookup key to lower case while creating or querying
a table.

With Postfix version 2.3 and later, this option has no
effect for regular expression tables. There, case folding
is controlled by appending a flag to a pattern.
**-i**
Incremental mode. Read entries from standard input and do not
truncate an existing database. By default, **postalias**(1) creates
a new database from the entries in *file_name*.
**-N**
Include the terminating null character that terminates lookup keys
and values. By default, **postalias**(1) does whatever
is the default for
the host operating system.
**-n**
Don't include the terminating null character that terminates lookup
keys and values. By default, **postalias**(1) does whatever
is the default for
the host operating system.
**-o**
Do not release root privileges when processing a non-root
input file. By default, **postalias**(1) drops root privileges
and runs as the source file owner instead.
**-p**
Do not inherit the file access permissions from the input file
when creating a new file.  Instead, create a new file with default
access permissions (mode 0644).

- \fB-q
Search the specified maps for *key* and write the first value
found to the standard output stream. The exit status is zero
when the requested information was found.

If a key value of **-** is specified, the program reads key
values from the standard input stream and writes one line of
*key: value* output for each key that was found. The exit
status is zero when at least one of the requested keys was found.
**-r**
When updating a table, do not complain about attempts to update
existing entries, and make those updates anyway.
**-s**
Retrieve all database elements, and write one line of
*key: value* output for each element. The elements are
printed in database order, which is not necessarily the same
as the original input order.
This feature is available in Postfix version 2.2 and later,
and is not available for all database types.
**-u**
Disable UTF-8 support. UTF-8 support is enabled by default
when "smtputf8_enable = yes". It requires that keys and
values are valid UTF-8 strings.
**-v**
Enable verbose logging for debugging purposes. Multiple **-v**
options make the software increasingly verbose.
**-w**
When updating a table, do not complain about attempts to update
existing entries, and ignore those attempts.

Arguments:
*file_type*
The database type. To find out what types are supported, use
the "**postconf -m**" command.

The **postalias**(1) command can query any supported file type,
but it can create only the following file types:

> **btree**
The output is a btree file, named \fIfile_name**.db**.
This is available on systems with support for **db** databases.
**cdb**
The output is one file named \fIfile_name**.cdb**.
This is available on systems with support for **cdb** databases.
**dbm**
The output consists of two files, named \fIfile_name**.pag** and
\fIfile_name**.dir**.
This is available on systems with support for **dbm** databases.
**hash**
The output is a hashed file, named \fIfile_name**.db**.
This is available on systems with support for **db** databases.
**fail**
A table that reliably fails all requests. The lookup table
name is used for logging only. This table exists to simplify
Postfix error tests.
**sdbm**
The output consists of two files, named \fIfile_name**.pag** and
\fIfile_name**.dir**.
This is available on systems with support for **sdbm** databases.

When no *file_type* is specified, the software uses the database
type specified via the **default_database_type** configuration
parameter.
The default value for this parameter depends on the host environment.


*file_name*
The name of the alias database source file when creating a database.

## DIAGNOSTICS


Problems are logged to the standard error stream and to
**syslogd**(8).  No output means that
no problems were detected. Duplicate entries are skipped and are
flagged with a warning.

**postalias**(1) terminates with zero exit status in case of success
(including successful "**postalias -q**" lookup) and terminates
with non-zero exit status in case of failure.

## ENVIRONMENT

.na

```
.ad
```

**MAIL_CONFIG**
Directory with Postfix configuration files.
**MAIL_VERBOSE**
Enable verbose logging for debugging purposes.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

The following **main.cf** parameters are especially relevant to
this program.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

- \fBalias_database (see 'postconf -d'
The alias databases for **local**(8) delivery that are updated with
"**newaliases**" or with "**sendmail -bi**".

- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBberkeley_db_create_buffer_size
The per-table I/O buffer size for programs that create Berkeley DB
hash or btree tables.

- \fBberkeley_db_read_buffer_size
The per-table I/O buffer size for programs that read Berkeley DB
hash or btree tables.

- \fBdefault_database_type (see 'postconf -d'
The default database type for use in **newaliases**(1), **postalias**(1)
and **postmap**(1) commands.

- \fBimport_environment (see 'postconf -d'
The list of environment parameters that a privileged Postfix
process will import from a non-Postfix parent process, or name=value
environment overrides.

- \fBsmtputf8_enable
Enable preliminary SMTPUTF8 support for the protocols described
in RFC 6531..6533.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## STANDARDS

.na

```
RFC 822 (ARPA Internet Text Messages)
.SH "SEE ALSO"
.na

```
aliases(5), format of alias database input file.
local(8), Postfix local delivery agent.
postconf(1), supported database types
postconf(5), configuration parameters
postmap(1), create/update/query lookup tables
newaliases(1), Sendmail compatibility interface.
syslogd(8), system logging
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
DATABASE_README, Postfix lookup table overview
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
