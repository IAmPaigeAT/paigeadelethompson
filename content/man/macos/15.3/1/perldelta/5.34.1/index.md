+++
operating_system = "macos"
manpage_name = "perldelta"
author = "None Specified"
date = "2022-03-06"
description = "This document describes differences between the 5.34.0 release and the 5.34.1 release. If you are upgrading from an earlier release such as 5.33.0, first read perl5340delta, which describes differences between 5.33.0 and 5.34.0. There are no chang..."
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
title = "perldelta(1)"
detected_package_version = "5.34.1"
manpage_section = "1"
operating_system_version = "15.3"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLDELTA 1"
PERLDELTA 1 "2022-03-06" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perldelta - what is new for perl v5.34.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.34.0 release and the 5.34.1
release.

If you are upgrading from an earlier release such as 5.33.0, first read
perl5340delta, which describes differences between 5.33.0 and 5.34.0.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.34.0.  If any exist,
they are bugs, and we request that you submit a report.  See
\*(L"Reporting Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
B::Deparse has been upgraded from version 1.56 to 1.57.

- \(bu
Encode has been upgraded from version 3.08 to 3.08_01.

- \(bu
GDBM_File has been upgraded from version 1.19 to 1.19_01.

- \(bu
Module::CoreList has been upgraded from version 5.20210520 to 5.20220313.

- \(bu
perl5db.pl has been upgraded from version 1.60 to 1.60_01.

## Testing

Header "Testing"
Tests were added and changed to reflect the other additions and changes in this
release.

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- Windows
Item "Windows"

> 0

- \(bu
.PD
Support for compiling perl on Windows using Microsoft Visual Studio 2022
(containing Visual \*(C+ 14.3) has been added.



> 


## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
B::Deparse now correctly handles try/catch blocks with more complex scopes.
[\s-1GH\s0 #18874 <https://github.com/Perl/perl5/issues/18874>]

## Acknowledgements

Header "Acknowledgements"
Perl 5.34.1 represents approximately 10 months of development since Perl 5.34.0
and contains approximately 4,600 lines of changes across 60 files from 23
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 1,100 lines of changes to 18 .pm, .t, .c and .h files.

Perl continues to flourish into its fourth decade thanks to a vibrant community
of users and developers. The following people are known to have contributed the
improvements that became Perl 5.34.1:

Andrew Fresh, Atsushi Sugawara, Chris 'BinGOs' Williams, Dan Book, Hugo van der
Sanden, James E Keenan, Karen Etheridge, Leon Timmermans, Matthew Horsfall, Max
Maischein, Michiel Beijen, Neil Bowers, Nicolas R., Paul Evans, Renee Baecker,
Ricardo Signes, Richard Leach, Sawyer X, Sergey Poznyakoff, Steve Hay, Tomasz
Konojacki, Tony Cook, Yves Orton.

The list above is almost certainly incomplete as it is automatically generated
from version control history. In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core. We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database
at <https://github.com/Perl/perl5/issues>.  There may also be information at
<http://www.perl.org/>, the Perl Home Page.

If you believe you have an unreported bug, please open an issue at
<https://github.com/Perl/perl5/issues>.  Be sure to trim your bug down to a
tiny but sufficient test case.

If the bug you are reporting has security implications which make it
inappropriate to send to a public issue tracker, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5,
you can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
