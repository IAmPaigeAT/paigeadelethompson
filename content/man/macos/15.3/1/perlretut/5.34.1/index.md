+++
manpage_section = "1"
description = "This page provides a basic tutorial on understanding, creating and using regular expressions in Perl.  It serves as a complement to the reference page on regular expressions perlre.  Regular expressions are an integral part of the f(CW*(C`m//*(C, ..."
title = "perlretut(1)"
detected_package_version = "5.34.1"
date = "2022-02-19"
manpage_name = "perlretut"
author = "None Specified"
operating_system = "macos"
operating_system_version = "15.3"
keywords = ["header", "see", "also", "this", "is", "just", "a", "tutorial", "for", "the", "full", "story", "on", "perl", "regular", "expressions", "perlre", "reference", "page", "more", "information", "matching", "f", "cw", "c", "m", "and", "substitution", "s", "operators", "l", "regexp", "quote-like", "r", "in", "perlop", "split", "operation", "perlfunc", "an", "excellent", "all-around", "resource", "care", "feeding", "of", "book", "fimastering", "by", "jeffrey", "friedl", "published", "o", "reilly", "s-1isbn", "1556592-257-3", "s0", "author", "copyright", "2000", "mark", "kvale", "all", "rights", "reserved", "now", "maintained", "porters", "document", "may", "be", "distributed", "under", "same", "terms", "as", "itself", "acknowledgments", "subsection", "inspiration", "stop", "codon", "s-1dna", "example", "came", "from", "s-1zip", "code", "chapter", "7", "would", "like", "to", "thank", "jeff", "pinyan", "andrew", "johnson", "peter", "haworth", "ronald", "j", "kimball", "joe", "smith", "their", "helpful", "comments"]
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLRETUT 1"
PERLRETUT 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlretut - Perl regular expressions tutorial

## DESCRIPTION

Header "DESCRIPTION"
This page provides a basic tutorial on understanding, creating and
using regular expressions in Perl.  It serves as a complement to the
reference page on regular expressions perlre.  Regular expressions
are an integral part of the \f(CW\*(C`m//\*(C', \f(CW\*(C`s///\*(C', \f(CW\*(C`qr//\*(C' and \f(CW\*(C`split\*(C'
operators and so this tutorial also overlaps with
\*(L"Regexp Quote-Like Operators\*(R" in perlop and \*(L"split\*(R" in perlfunc.

Perl is widely renowned for excellence in text processing, and regular
expressions are one of the big factors behind this fame.  Perl regular
expressions display an efficiency and flexibility unknown in most
other computer languages.  Mastering even the basics of regular
expressions will allow you to manipulate text with surprising ease.

What is a regular expression?  At its most basic, a regular expression
is a template that is used to determine if a string has certain
characteristics.  The string is most often some text, such as a line,
sentence, web page, or even a whole book, but it doesn't have to be.  It
could be binary data, for example.  Biologists often use Perl to look
for patterns in long \s-1DNA\s0 sequences.

Suppose we want to determine if the text in variable, \f(CW$var contains
the sequence of characters \f(CW\*(C`m\ u\ s\ h\ r\ o\ o\ m\*(C'
(blanks added for legibility).  We can write in Perl

.Vb 1
 $var =~ m/mushroom/
.Ve

The value of this expression will be \s-1TRUE\s0 if \f(CW$var contains that
sequence of characters anywhere within it, and \s-1FALSE\s0 otherwise.  The
portion enclosed in \f(CW\*(Aq/\*(Aq characters denotes the characteristic we
are looking for.
We use the term *pattern* for it.  The process of looking to see if the
pattern occurs in the string is called *matching*, and the \f(CW"=~"
operator along with the \f(CW\*(C`m//\*(C' tell Perl to try to match the pattern
against the string.  Note that the pattern is also a string, but a very
special kind of one, as we will see.  Patterns are in common use these
days;
examples are the patterns typed into a search engine to find web pages
and the patterns used to list files in a directory, *e.g.*, "\f(CW\*(C`ls *.txt\*(C'\*(L"
or \*(R"\f(CW\*(C`dir *.*\*(C'".  In Perl, the patterns described by regular expressions
are used not only to search strings, but to also extract desired parts
of strings, and to do search and replace operations.

Regular expressions have the undeserved reputation of being abstract
and difficult to understand.  This really stems simply because the
notation used to express them tends to be terse and dense, and not
because of inherent complexity.  We recommend using the \f(CW\*(C`/x\*(C' regular
expression modifier (described below) along with plenty of white space
to make them less dense, and easier to read.  Regular expressions are
constructed using
simple concepts like conditionals and loops and are no more difficult
to understand than the corresponding \f(CW\*(C`if\*(C' conditionals and \f(CW\*(C`while\*(C'
loops in the Perl language itself.

This tutorial flattens the learning curve by discussing regular
expression concepts, along with their notation, one at a time and with
many examples.  The first part of the tutorial will progress from the
simplest word searches to the basic regular expression concepts.  If
you master the first part, you will have all the tools needed to solve
about 98% of your needs.  The second part of the tutorial is for those
comfortable with the basics and hungry for more power tools.  It
discusses the more advanced regular expression operators and
introduces the latest cutting-edge innovations.

A note: to save time, \*(L"regular expression\*(R" is often abbreviated as
regexp or regex.  Regexp is a more natural abbreviation than regex, but
is harder to pronounce.  The Perl pod documentation is evenly split on
regexp vs regex; in Perl, there is more than one way to abbreviate it.
We'll use regexp in this tutorial.

New in v5.22, \f(CW\*(C`use re \*(Aqstrict\*(Aq\*(C' applies stricter
rules than otherwise when compiling regular expression patterns.  It can
find things that, while legal, may not be what you intended.

## Part 1: The basics

Header "Part 1: The basics"

### Simple word matching

Subsection "Simple word matching"
The simplest regexp is simply a word, or more generally, a string of
characters.  A regexp consisting of just a word matches any string that
contains that word:

.Vb 1
    "Hello World" =~ /World/;  # matches
.Ve

What is this Perl statement all about? \f(CW"Hello World" is a simple
double-quoted string.  \f(CW\*(C`World\*(C' is the regular expression and the
\f(CW\*(C`//\*(C' enclosing \f(CW\*(C`/World/\*(C' tells Perl to search a string for a match.
The operator \f(CW\*(C`=~\*(C' associates the string with the regexp match and
produces a true value if the regexp matched, or false if the regexp
did not match.  In our case, \f(CW\*(C`World\*(C' matches the second word in
\f(CW"Hello World", so the expression is true.  Expressions like this
are useful in conditionals:

.Vb 6
    if ("Hello World" =~ /World/) \{
        print "It matches\\n";
    \}
    else \{
        print "It doesn\*(Aqt match\\n";
    \}
.Ve

There are useful variations on this theme.  The sense of the match can
be reversed by using the \f(CW\*(C`!~\*(C' operator:

.Vb 6
    if ("Hello World" !~ /World/) \{
        print "It doesn\*(Aqt match\\n";
    \}
    else \{
        print "It matches\\n";
    \}
.Ve

The literal string in the regexp can be replaced by a variable:

.Vb 7
    my $greeting = "World";
    if ("Hello World" =~ /$greeting/) \{
        print "It matches\\n";
    \}
    else \{
        print "It doesn\*(Aqt match\\n";
    \}
.Ve

If you're matching against the special default variable \f(CW$_, the
\f(CW\*(C`$_ =~\*(C' part can be omitted:

.Vb 7
    $_ = "Hello World";
    if (/World/) \{
        print "It matches\\n";
    \}
    else \{
        print "It doesn\*(Aqt match\\n";
    \}
.Ve

And finally, the \f(CW\*(C`//\*(C' default delimiters for a match can be changed
to arbitrary delimiters by putting an \f(CW\*(Aqm\*(Aq out front:

.Vb 4
    "Hello World" =~ m!World!;   # matches, delimited by \*(Aq!\*(Aq
    "Hello World" =~ m\{World\};   # matches, note the paired \*(Aq\{\}\*(Aq
    "/usr/bin/perl" =~ m"/perl"; # matches after \*(Aq/usr/bin\*(Aq,
                                 # \*(Aq/\*(Aq becomes an ordinary char
.Ve

\f(CW\*(C`/World/\*(C', \f(CW\*(C`m!World!\*(C', and \f(CW\*(C`m\{World\}\*(C' all represent the
same thing.  When, *e.g.*, the quote (\f(CW\*(Aq"\*(Aq) is used as a delimiter, the forward
slash \f(CW\*(Aq/\*(Aq becomes an ordinary character and can be used in this regexp
without trouble.

Let's consider how different regexps would match \f(CW"Hello World":

.Vb 4
    "Hello World" =~ /world/;  # doesn\*(Aqt match
    "Hello World" =~ /o W/;    # matches
    "Hello World" =~ /oW/;     # doesn\*(Aqt match
    "Hello World" =~ /World /; # doesn\*(Aqt match
.Ve

The first regexp \f(CW\*(C`world\*(C' doesn't match because regexps are by default
case-sensitive.  The second regexp matches because the substring
\f(CW\*(Aqo\ W\*(Aq occurs in the string \f(CW"Hello\ World".  The space
character \f(CW\*(Aq \*(Aq is treated like any other character in a regexp and is
needed to match in this case.  The lack of a space character is the
reason the third regexp \f(CW\*(AqoW\*(Aq doesn't match.  The fourth regexp
"\f(CW\*(C`World \*(C'" doesn't match because there is a space at the end of the
regexp, but not at the end of the string.  The lesson here is that
regexps must match a part of the string *exactly* in order for the
statement to be true.

If a regexp matches in more than one place in the string, Perl will
always match at the earliest possible point in the string:

.Vb 2
    "Hello World" =~ /o/;       # matches \*(Aqo\*(Aq in \*(AqHello\*(Aq
    "That hat is red" =~ /hat/; # matches \*(Aqhat\*(Aq in \*(AqThat\*(Aq
.Ve

With respect to character matching, there are a few more points you
need to know about.   First of all, not all characters can be used
\*(L"as-is\*(R" in a match.  Some characters, called *metacharacters*, are
generally reserved for use in regexp notation.  The metacharacters are

.Vb 1
    \{\}[]()^$.|*+?-#\\
.Ve

This list is not as definitive as it may appear (or be claimed to be in
other documentation).  For example, \f(CW"#" is a metacharacter only when
the \f(CW\*(C`/x\*(C' pattern modifier (described below) is used, and both \f(CW"\}"
and \f(CW"]" are metacharacters only when paired with opening \f(CW"\{" or
\f(CW"[" respectively; other gotchas apply.

The significance of each of these will be explained
in the rest of the tutorial, but for now, it is important only to know
that a metacharacter can be matched as-is by putting a backslash before
it:

.Vb 5
    "2+2=4" =~ /2+2/;    # doesn\*(Aqt match, + is a metacharacter
    "2+2=4" =~ /2\\+2/;   # matches, \\+ is treated like an ordinary +
    "The interval is [0,1)." =~ /[0,1)./     # is a syntax error!
    "The interval is [0,1)." =~ /\\[0,1\\)\\./  # matches
    "#!/usr/bin/perl" =~ /#!\\/usr\\/bin\\/perl/;  # matches
.Ve

In the last regexp, the forward slash \f(CW\*(Aq/\*(Aq is also backslashed,
because it is used to delimit the regexp.  This can lead to \s-1LTS\s0
(leaning toothpick syndrome), however, and it is often more readable
to change delimiters.

.Vb 1
    "#!/usr/bin/perl" =~ m!#\\!/usr/bin/perl!;  # easier to read
.Ve

The backslash character \f(CW\*(Aq\\\*(Aq is a metacharacter itself and needs to
be backslashed:

.Vb 1
    \*(AqC:\\WIN32\*(Aq =~ /C:\\\\WIN/;   # matches
.Ve

In situations where it doesn't make sense for a particular metacharacter
to mean what it normally does, it automatically loses its
metacharacter-ness and becomes an ordinary character that is to be
matched literally.  For example, the \f(CW\*(Aq\}\*(Aq is a metacharacter only when
it is the mate of a \f(CW\*(Aq\{\*(Aq metacharacter.  Otherwise it is treated as a
literal \s-1RIGHT CURLY BRACKET.\s0  This may lead to unexpected results.
\f(CW\*(C`use re \*(Aqstrict\*(Aq\*(C' can catch some of these.

In addition to the metacharacters, there are some \s-1ASCII\s0 characters
which don't have printable character equivalents and are instead
represented by *escape sequences*.  Common examples are \f(CW\*(C`\\t\*(C' for a
tab, \f(CW\*(C`\\n\*(C' for a newline, \f(CW\*(C`\\r\*(C' for a carriage return and \f(CW\*(C`\\a\*(C' for a
bell (or alert).  If your string is better thought of as a sequence of arbitrary
bytes, the octal escape sequence, *e.g.*, \f(CW\*(C`\\033\*(C', or hexadecimal escape
sequence, *e.g.*, \f(CW\*(C`\\x1B\*(C' may be a more natural representation for your
bytes.  Here are some examples of escapes:

.Vb 5
    "1000\\t2000" =~ m(0\\t2)   # matches
    "1000\\n2000" =~ /0\\n20/   # matches
    "1000\\t2000" =~ /\\000\\t2/ # doesn\*(Aqt match, "0" ne "\\000"
    "cat"   =~ /\\o\{143\}\\x61\\x74/ # matches in ASCII, but a weird way
                                 # to spell cat
.Ve

If you've been around Perl a while, all this talk of escape sequences
may seem familiar.  Similar escape sequences are used in double-quoted
strings and in fact the regexps in Perl are mostly treated as
double-quoted strings.  This means that variables can be used in
regexps as well.  Just like double-quoted strings, the values of the
variables in the regexp will be substituted in before the regexp is
evaluated for matching purposes.  So we have:

.Vb 4
    $foo = \*(Aqhouse\*(Aq;
    \*(Aqhousecat\*(Aq =~ /$foo/;      # matches
    \*(Aqcathouse\*(Aq =~ /cat$foo/;   # matches
    \*(Aqhousecat\*(Aq =~ /$\{foo\}cat/; # matches
.Ve

So far, so good.  With the knowledge above you can already perform
searches with just about any literal string regexp you can dream up.
Here is a *very simple* emulation of the Unix grep program:

.Vb 7
    % cat > simple_grep
    #!/usr/bin/perl
    $regexp = shift;
    while (<>) \{
        print if /$regexp/;
    \}
    ^D

    % chmod +x simple_grep

    % simple_grep abba /usr/dict/words
    Babbage
    cabbage
    cabbages
    sabbath
    Sabbathize
    Sabbathizes
    sabbatical
    scabbard
    scabbards
.Ve

This program is easy to understand.  \f(CW\*(C`#!/usr/bin/perl\*(C' is the standard
way to invoke a perl program from the shell.
\f(CW\*(C`$regexp\ =\ shift;\*(C' saves the first command line argument as the
regexp to be used, leaving the rest of the command line arguments to
be treated as files.  \f(CW\*(C`while\ (<>)\*(C' loops over all the lines in
all the files.  For each line, \f(CW\*(C`print\ if\ /$regexp/;\*(C' prints the
line if the regexp matches the line.  In this line, both \f(CW\*(C`print\*(C' and
\f(CW\*(C`/$regexp/\*(C' use the default variable \f(CW$_ implicitly.

With all of the regexps above, if the regexp matched anywhere in the
string, it was considered a match.  Sometimes, however, we'd like to
specify *where* in the string the regexp should try to match.  To do
this, we would use the *anchor* metacharacters \f(CW\*(Aq^\*(Aq and \f(CW\*(Aq$\*(Aq.  The
anchor \f(CW\*(Aq^\*(Aq means match at the beginning of the string and the anchor
\f(CW\*(Aq$\*(Aq means match at the end of the string, or before a newline at the
end of the string.  Here is how they are used:

.Vb 4
    "housekeeper" =~ /keeper/;    # matches
    "housekeeper" =~ /^keeper/;   # doesn\*(Aqt match
    "housekeeper" =~ /keeper$/;   # matches
    "housekeeper\\n" =~ /keeper$/; # matches
.Ve

The second regexp doesn't match because \f(CW\*(Aq^\*(Aq constrains \f(CW\*(C`keeper\*(C' to
match only at the beginning of the string, but \f(CW"housekeeper" has
keeper starting in the middle.  The third regexp does match, since the
\f(CW\*(Aq$\*(Aq constrains \f(CW\*(C`keeper\*(C' to match only at the end of the string.

When both \f(CW\*(Aq^\*(Aq and \f(CW\*(Aq$\*(Aq are used at the same time, the regexp has to
match both the beginning and the end of the string, *i.e.*, the regexp
matches the whole string.  Consider

.Vb 3
    "keeper" =~ /^keep$/;      # doesn\*(Aqt match
    "keeper" =~ /^keeper$/;    # matches
    ""       =~ /^$/;          # ^$ matches an empty string
.Ve

The first regexp doesn't match because the string has more to it than
\f(CW\*(C`keep\*(C'.  Since the second regexp is exactly the string, it
matches.  Using both \f(CW\*(Aq^\*(Aq and \f(CW\*(Aq$\*(Aq in a regexp forces the complete
string to match, so it gives you complete control over which strings
match and which don't.  Suppose you are looking for a fellow named
bert, off in a string by himself:

.Vb 1
    "dogbert" =~ /bert/;   # matches, but not what you want

    "dilbert" =~ /^bert/;  # doesn\*(Aqt match, but ..
    "bertram" =~ /^bert/;  # matches, so still not good enough

    "bertram" =~ /^bert$/; # doesn\*(Aqt match, good
    "dilbert" =~ /^bert$/; # doesn\*(Aqt match, good
    "bert"    =~ /^bert$/; # matches, perfect
.Ve

Of course, in the case of a literal string, one could just as easily
use the string comparison \f(CW\*(C`$string\ eq\ \*(Aqbert\*(Aq\*(C' and it would be
more efficient.   The  \f(CW\*(C`^...$\*(C' regexp really becomes useful when we
add in the more powerful regexp tools below.

### Using character classes

Subsection "Using character classes"
Although one can already do quite a lot with the literal string
regexps above, we've only scratched the surface of regular expression
technology.  In this and subsequent sections we will introduce regexp
concepts (and associated metacharacter notations) that will allow a
regexp to represent not just a single character sequence, but a \fIwhole
class of them.

One such concept is that of a *character class*.  A character class
allows a set of possible characters, rather than just a single
character, to match at a particular point in a regexp.  You can define
your own custom character classes.  These
are denoted by brackets \f(CW\*(C`[...]\*(C', with the set of characters
to be possibly matched inside.  Here are some examples:

.Vb 4
    /cat/;       # matches \*(Aqcat\*(Aq
    /[bcr]at/;   # matches \*(Aqbat, \*(Aqcat\*(Aq, or \*(Aqrat\*(Aq
    /item[0123456789]/;  # matches \*(Aqitem0\*(Aq or ... or \*(Aqitem9\*(Aq
    "abc" =~ /[cab]/;    # matches \*(Aqa\*(Aq
.Ve

In the last statement, even though \f(CW\*(Aqc\*(Aq is the first character in
the class, \f(CW\*(Aqa\*(Aq matches because the first character position in the
string is the earliest point at which the regexp can match.

.Vb 2
    /[yY][eE][sS]/;      # match \*(Aqyes\*(Aq in a case-insensitive way
                         # \*(Aqyes\*(Aq, \*(AqYes\*(Aq, \*(AqYES\*(Aq, etc.
.Ve

This regexp displays a common task: perform a case-insensitive
match.  Perl provides a way of avoiding all those brackets by simply
appending an \f(CW\*(Aqi\*(Aq to the end of the match.  Then \f(CW\*(C`/[yY][eE][sS]/;\*(C'
can be rewritten as \f(CW\*(C`/yes/i;\*(C'.  The \f(CW\*(Aqi\*(Aq stands for
case-insensitive and is an example of a *modifier* of the matching
operation.  We will meet other modifiers later in the tutorial.

We saw in the section above that there were ordinary characters, which
represented themselves, and special characters, which needed a
backslash \f(CW\*(Aq\\\*(Aq to represent themselves.  The same is true in a
character class, but the sets of ordinary and special characters
inside a character class are different than those outside a character
class.  The special characters for a character class are \f(CW\*(C`-]\\^$\*(C' (and
the pattern delimiter, whatever it is).
\f(CW\*(Aq]\*(Aq is special because it denotes the end of a character class.  \f(CW\*(Aq$\*(Aq is
special because it denotes a scalar variable.  \f(CW\*(Aq\\\*(Aq is special because
it is used in escape sequences, just like above.  Here is how the
special characters \f(CW\*(C`]$\\\*(C' are handled:

.Vb 5
   /[\\]c]def/; # matches \*(Aq]def\*(Aq or \*(Aqcdef\*(Aq
   $x = \*(Aqbcr\*(Aq;
   /[$x]at/;   # matches \*(Aqbat\*(Aq, \*(Aqcat\*(Aq, or \*(Aqrat\*(Aq
   /[\\$x]at/;  # matches \*(Aq$at\*(Aq or \*(Aqxat\*(Aq
   /[\\\\$x]at/; # matches \*(Aq\\at\*(Aq, \*(Aqbat, \*(Aqcat\*(Aq, or \*(Aqrat\*(Aq
.Ve

The last two are a little tricky.  In \f(CW\*(C`[\\$x]\*(C', the backslash protects
the dollar sign, so the character class has two members \f(CW\*(Aq$\*(Aq and \f(CW\*(Aqx\*(Aq.
In \f(CW\*(C`[\\\\$x]\*(C', the backslash is protected, so \f(CW$x is treated as a
variable and substituted in double quote fashion.

The special character \f(CW\*(Aq-\*(Aq acts as a range operator within character
classes, so that a contiguous set of characters can be written as a
range.  With ranges, the unwieldy \f(CW\*(C`[0123456789]\*(C' and \f(CW\*(C`[abc...xyz]\*(C'
become the svelte \f(CW\*(C`[0-9]\*(C' and \f(CW\*(C`[a-z]\*(C'.  Some examples are

.Vb 6
    /item[0-9]/;  # matches \*(Aqitem0\*(Aq or ... or \*(Aqitem9\*(Aq
    /[0-9bx-z]aa/;  # matches \*(Aq0aa\*(Aq, ..., \*(Aq9aa\*(Aq,
                    # \*(Aqbaa\*(Aq, \*(Aqxaa\*(Aq, \*(Aqyaa\*(Aq, or \*(Aqzaa\*(Aq
    /[0-9a-fA-F]/;  # matches a hexadecimal digit
    /[0-9a-zA-Z_]/; # matches a "word" character,
                    # like those in a Perl variable name
.Ve

If \f(CW\*(Aq-\*(Aq is the first or last character in a character class, it is
treated as an ordinary character; \f(CW\*(C`[-ab]\*(C', \f(CW\*(C`[ab-]\*(C' and \f(CW\*(C`[a\\-b]\*(C' are
all equivalent.

The special character \f(CW\*(Aq^\*(Aq in the first position of a character class
denotes a *negated character class*, which matches any character but
those in the brackets.  Both \f(CW\*(C`[...]\*(C' and \f(CW\*(C`[^...]\*(C' must match a
character, or the match fails.  Then

.Vb 4
    /[^a]at/;  # doesn\*(Aqt match \*(Aqaat\*(Aq or \*(Aqat\*(Aq, but matches
               # all other \*(Aqbat\*(Aq, \*(Aqcat, \*(Aq0at\*(Aq, \*(Aq%at\*(Aq, etc.
    /[^0-9]/;  # matches a non-numeric character
    /[a^]at/;  # matches \*(Aqaat\*(Aq or \*(Aq^at\*(Aq; here \*(Aq^\*(Aq is ordinary
.Ve

Now, even \f(CW\*(C`[0-9]\*(C' can be a bother to write multiple times, so in the
interest of saving keystrokes and making regexps more readable, Perl
has several abbreviations for common character classes, as shown below.
Since the introduction of Unicode, unless the \f(CW\*(C`/a\*(C' modifier is in
effect, these character classes match more than just a few characters in
the \s-1ASCII\s0 range.

- \(bu
\f(CW\*(C`\\d\*(C' matches a digit, not just \f(CW\*(C`[0-9]\*(C' but also digits from non-roman scripts

- \(bu
\f(CW\*(C`\\s\*(C' matches a whitespace character, the set \f(CW\*(C`[\\ \\t\\r\\n\\f]\*(C' and others

- \(bu
\f(CW\*(C`\\w\*(C' matches a word character (alphanumeric or \f(CW\*(Aq_\*(Aq), not just \f(CW\*(C`[0-9a-zA-Z_]\*(C'
but also digits and characters from non-roman scripts

- \(bu
\f(CW\*(C`\\D\*(C' is a negated \f(CW\*(C`\\d\*(C'; it represents any other character than a digit, or \f(CW\*(C`[^\\d]\*(C'

- \(bu
\f(CW\*(C`\\S\*(C' is a negated \f(CW\*(C`\\s\*(C'; it represents any non-whitespace character \f(CW\*(C`[^\\s]\*(C'

- \(bu
\f(CW\*(C`\\W\*(C' is a negated \f(CW\*(C`\\w\*(C'; it represents any non-word character \f(CW\*(C`[^\\w]\*(C'

- \(bu
The period \f(CW\*(Aq.\*(Aq matches any character but \f(CW"\\n" (unless the modifier \f(CW\*(C`/s\*(C' is
in effect, as explained below).

- \(bu
\f(CW\*(C`\\N\*(C', like the period, matches any character but \f(CW"\\n", but it does so
regardless of whether the modifier \f(CW\*(C`/s\*(C' is in effect.

The \f(CW\*(C`/a\*(C' modifier, available starting in Perl 5.14,  is used to
restrict the matches of \f(CW\*(C`\\d\*(C', \f(CW\*(C`\\s\*(C', and \f(CW\*(C`\\w\*(C' to just those in the \s-1ASCII\s0 range.
It is useful to keep your program from being needlessly exposed to full
Unicode (and its accompanying security considerations) when all you want
is to process English-like text.  (The \*(L"a\*(R" may be doubled, \f(CW\*(C`/aa\*(C', to
provide even more restrictions, preventing case-insensitive matching of
\s-1ASCII\s0 with non-ASCII characters; otherwise a Unicode \*(L"Kelvin Sign\*(R"
would caselessly match a \*(L"k\*(R" or \*(L"K\*(R".)

The \f(CW\*(C`\\d\\s\\w\\D\\S\\W\*(C' abbreviations can be used both inside and outside
of bracketed character classes.  Here are some in use:

.Vb 7
    /\\d\\d:\\d\\d:\\d\\d/; # matches a hh:mm:ss time format
    /[\\d\\s]/;         # matches any digit or whitespace character
    /\\w\\W\\w/;         # matches a word char, followed by a
                      # non-word char, followed by a word char
    /..rt/;           # matches any two chars, followed by \*(Aqrt\*(Aq
    /end\\./;          # matches \*(Aqend.\*(Aq
    /end[.]/;         # same thing, matches \*(Aqend.\*(Aq
.Ve

Because a period is a metacharacter, it needs to be escaped to match
as an ordinary period. Because, for example, \f(CW\*(C`\\d\*(C' and \f(CW\*(C`\\w\*(C' are sets
of characters, it is incorrect to think of \f(CW\*(C`[^\\d\\w]\*(C' as \f(CW\*(C`[\\D\\W]\*(C'; in
fact \f(CW\*(C`[^\\d\\w]\*(C' is the same as \f(CW\*(C`[^\\w]\*(C', which is the same as
\f(CW\*(C`[\\W]\*(C'. Think DeMorgan's laws.

In actuality, the period and \f(CW\*(C`\\d\\s\\w\\D\\S\\W\*(C' abbreviations are
themselves types of character classes, so the ones surrounded by
brackets are just one type of character class.  When we need to make a
distinction, we refer to them as \*(L"bracketed character classes.\*(R"

An anchor useful in basic regexps is the *word anchor*
\f(CW\*(C`\\b\*(C'.  This matches a boundary between a word character and a non-word
character \f(CW\*(C`\\w\\W\*(C' or \f(CW\*(C`\\W\\w\*(C':

.Vb 5
    $x = "Housecat catenates house and cat";
    $x =~ /cat/;    # matches cat in \*(Aqhousecat\*(Aq
    $x =~ /\\bcat/;  # matches cat in \*(Aqcatenates\*(Aq
    $x =~ /cat\\b/;  # matches cat in \*(Aqhousecat\*(Aq
    $x =~ /\\bcat\\b/;  # matches \*(Aqcat\*(Aq at end of string
.Ve

Note in the last example, the end of the string is considered a word
boundary.

For natural language processing (so that, for example, apostrophes are
included in words), use instead \f(CW\*(C`\\b\{wb\}\*(C'

.Vb 1
    "don\*(Aqt" =~ / .+? \\b\{wb\} /x;  # matches the whole string
.Ve

You might wonder why \f(CW\*(Aq.\*(Aq matches everything but \f(CW"\\n" - why not
every character? The reason is that often one is matching against
lines and would like to ignore the newline characters.  For instance,
while the string \f(CW"\\n" represents one line, we would like to think
of it as empty.  Then

.Vb 2
    ""   =~ /^$/;    # matches
    "\\n" =~ /^$/;    # matches, $ anchors before "\\n"

    ""   =~ /./;      # doesn\*(Aqt match; it needs a char
    ""   =~ /^.$/;    # doesn\*(Aqt match; it needs a char
    "\\n" =~ /^.$/;    # doesn\*(Aqt match; it needs a char other than "\\n"
    "a"  =~ /^.$/;    # matches
    "a\\n"  =~ /^.$/;  # matches, $ anchors before "\\n"
.Ve

This behavior is convenient, because we usually want to ignore
newlines when we count and match characters in a line.  Sometimes,
however, we want to keep track of newlines.  We might even want \f(CW\*(Aq^\*(Aq
and \f(CW\*(Aq$\*(Aq to anchor at the beginning and end of lines within the
string, rather than just the beginning and end of the string.  Perl
allows us to choose between ignoring and paying attention to newlines
by using the \f(CW\*(C`/s\*(C' and \f(CW\*(C`/m\*(C' modifiers.  \f(CW\*(C`/s\*(C' and \f(CW\*(C`/m\*(C' stand for
single line and multi-line and they determine whether a string is to
be treated as one continuous string, or as a set of lines.  The two
modifiers affect two aspects of how the regexp is interpreted: 1) how
the \f(CW\*(Aq.\*(Aq character class is defined, and 2) where the anchors \f(CW\*(Aq^\*(Aq
and \f(CW\*(Aq$\*(Aq are able to match.  Here are the four possible combinations:

- \(bu
no modifiers: Default behavior.  \f(CW\*(Aq.\*(Aq matches any character
except \f(CW"\\n".  \f(CW\*(Aq^\*(Aq matches only at the beginning of the string and
\f(CW\*(Aq$\*(Aq matches only at the end or before a newline at the end.

- \(bu
s modifier (\f(CW\*(C`/s\*(C'): Treat string as a single long line.  \f(CW\*(Aq.\*(Aq matches
any character, even \f(CW"\\n".  \f(CW\*(Aq^\*(Aq matches only at the beginning of
the string and \f(CW\*(Aq$\*(Aq matches only at the end or before a newline at the
end.

- \(bu
m modifier (\f(CW\*(C`/m\*(C'): Treat string as a set of multiple lines.  \f(CW\*(Aq.\*(Aq
matches any character except \f(CW"\\n".  \f(CW\*(Aq^\*(Aq and \f(CW\*(Aq$\*(Aq are able to match
at the start or end of *any* line within the string.

- \(bu
both s and m modifiers (\f(CW\*(C`/sm\*(C'): Treat string as a single long line, but
detect multiple lines.  \f(CW\*(Aq.\*(Aq matches any character, even
\f(CW"\\n".  \f(CW\*(Aq^\*(Aq and \f(CW\*(Aq$\*(Aq, however, are able to match at the start or end
of *any* line within the string.

Here are examples of \f(CW\*(C`/s\*(C' and \f(CW\*(C`/m\*(C' in action:

.Vb 1
    $x = "There once was a girl\\nWho programmed in Perl\\n";

    $x =~ /^Who/;   # doesn\*(Aqt match, "Who" not at start of string
    $x =~ /^Who/s;  # doesn\*(Aqt match, "Who" not at start of string
    $x =~ /^Who/m;  # matches, "Who" at start of second line
    $x =~ /^Who/sm; # matches, "Who" at start of second line

    $x =~ /girl.Who/;   # doesn\*(Aqt match, "." doesn\*(Aqt match "\\n"
    $x =~ /girl.Who/s;  # matches, "." matches "\\n"
    $x =~ /girl.Who/m;  # doesn\*(Aqt match, "." doesn\*(Aqt match "\\n"
    $x =~ /girl.Who/sm; # matches, "." matches "\\n"
.Ve

Most of the time, the default behavior is what is wanted, but \f(CW\*(C`/s\*(C' and
\f(CW\*(C`/m\*(C' are occasionally very useful.  If \f(CW\*(C`/m\*(C' is being used, the start
of the string can still be matched with \f(CW\*(C`\\A\*(C' and the end of the string
can still be matched with the anchors \f(CW\*(C`\\Z\*(C' (matches both the end and
the newline before, like \f(CW\*(Aq$\*(Aq), and \f(CW\*(C`\\z\*(C' (matches only the end):

.Vb 2
    $x =~ /^Who/m;   # matches, "Who" at start of second line
    $x =~ /\\AWho/m;  # doesn\*(Aqt match, "Who" is not at start of string

    $x =~ /girl$/m;  # matches, "girl" at end of first line
    $x =~ /girl\\Z/m; # doesn\*(Aqt match, "girl" is not at end of string

    $x =~ /Perl\\Z/m; # matches, "Perl" is at newline before end
    $x =~ /Perl\\z/m; # doesn\*(Aqt match, "Perl" is not at end of string
.Ve

We now know how to create choices among classes of characters in a
regexp.  What about choices among words or character strings? Such
choices are described in the next section.

### Matching this or that

Subsection "Matching this or that"
Sometimes we would like our regexp to be able to match different
possible words or character strings.  This is accomplished by using
the *alternation* metacharacter \f(CW\*(Aq|\*(Aq.  To match \f(CW\*(C`dog\*(C' or \f(CW\*(C`cat\*(C', we
form the regexp \f(CW\*(C`dog|cat\*(C'.  As before, Perl will try to match the
regexp at the earliest possible point in the string.  At each
character position, Perl will first try to match the first
alternative, \f(CW\*(C`dog\*(C'.  If \f(CW\*(C`dog\*(C' doesn't match, Perl will then try the
next alternative, \f(CW\*(C`cat\*(C'.  If \f(CW\*(C`cat\*(C' doesn't match either, then the
match fails and Perl moves to the next position in the string.  Some
examples:

.Vb 2
    "cats and dogs" =~ /cat|dog|bird/;  # matches "cat"
    "cats and dogs" =~ /dog|cat|bird/;  # matches "cat"
.Ve

Even though \f(CW\*(C`dog\*(C' is the first alternative in the second regexp,
\f(CW\*(C`cat\*(C' is able to match earlier in the string.

.Vb 2
    "cats"          =~ /c|ca|cat|cats/; # matches "c"
    "cats"          =~ /cats|cat|ca|c/; # matches "cats"
.Ve

Here, all the alternatives match at the first string position, so the
first alternative is the one that matches.  If some of the
alternatives are truncations of the others, put the longest ones first
to give them a chance to match.

.Vb 2
    "cab" =~ /a|b|c/ # matches "c"
                     # /a|b|c/ == /[abc]/
.Ve

The last example points out that character classes are like
alternations of characters.  At a given character position, the first
alternative that allows the regexp match to succeed will be the one
that matches.

### Grouping things and hierarchical matching

Subsection "Grouping things and hierarchical matching"
Alternation allows a regexp to choose among alternatives, but by
itself it is unsatisfying.  The reason is that each alternative is a whole
regexp, but sometime we want alternatives for just part of a
regexp.  For instance, suppose we want to search for housecats or
housekeepers.  The regexp \f(CW\*(C`housecat|housekeeper\*(C' fits the bill, but is
inefficient because we had to type \f(CW\*(C`house\*(C' twice.  It would be nice to
have parts of the regexp be constant, like \f(CW\*(C`house\*(C', and some
parts have alternatives, like \f(CW\*(C`cat|keeper\*(C'.

The *grouping* metacharacters \f(CW\*(C`()\*(C' solve this problem.  Grouping
allows parts of a regexp to be treated as a single unit.  Parts of a
regexp are grouped by enclosing them in parentheses.  Thus we could solve
the \f(CW\*(C`housecat|housekeeper\*(C' by forming the regexp as
\f(CW\*(C`house(cat|keeper)\*(C'.  The regexp \f(CW\*(C`house(cat|keeper)\*(C' means match
\f(CW\*(C`house\*(C' followed by either \f(CW\*(C`cat\*(C' or \f(CW\*(C`keeper\*(C'.  Some more examples
are

.Vb 4
    /(a|b)b/;    # matches \*(Aqab\*(Aq or \*(Aqbb\*(Aq
    /(ac|b)b/;   # matches \*(Aqacb\*(Aq or \*(Aqbb\*(Aq
    /(^a|b)c/;   # matches \*(Aqac\*(Aq at start of string or \*(Aqbc\*(Aq anywhere
    /(a|[bc])d/; # matches \*(Aqad\*(Aq, \*(Aqbd\*(Aq, or \*(Aqcd\*(Aq

    /house(cat|)/;  # matches either \*(Aqhousecat\*(Aq or \*(Aqhouse\*(Aq
    /house(cat(s|)|)/;  # matches either \*(Aqhousecats\*(Aq or \*(Aqhousecat\*(Aq or
                        # \*(Aqhouse\*(Aq.  Note groups can be nested.

    /(19|20|)\\d\\d/;  # match years 19xx, 20xx, or the Y2K problem, xx
    "20" =~ /(19|20|)\\d\\d/;  # matches the null alternative \*(Aq()\\d\\d\*(Aq,
                             # because \*(Aq20\\d\\d\*(Aq can\*(Aqt match
.Ve

Alternations behave the same way in groups as out of them: at a given
string position, the leftmost alternative that allows the regexp to
match is taken.  So in the last example at the first string position,
\f(CW"20" matches the second alternative, but there is nothing left over
to match the next two digits \f(CW\*(C`\\d\\d\*(C'.  So Perl moves on to the next
alternative, which is the null alternative and that works, since
\f(CW"20" is two digits.

The process of trying one alternative, seeing if it matches, and
moving on to the next alternative, while going back in the string
from where the previous alternative was tried, if it doesn't, is called
*backtracking*.  The term \*(L"backtracking\*(R" comes from the idea that
matching a regexp is like a walk in the woods.  Successfully matching
a regexp is like arriving at a destination.  There are many possible
trailheads, one for each string position, and each one is tried in
order, left to right.  From each trailhead there may be many paths,
some of which get you there, and some which are dead ends.  When you
walk along a trail and hit a dead end, you have to backtrack along the
trail to an earlier point to try another trail.  If you hit your
destination, you stop immediately and forget about trying all the
other trails.  You are persistent, and only if you have tried all the
trails from all the trailheads and not arrived at your destination, do
you declare failure.  To be concrete, here is a step-by-step analysis
of what Perl does when it tries to match the regexp

.Vb 1
    "abcde" =~ /(abd|abc)(df|d|de)/;
.Ve
.ie n .IP "0. Start with the first letter in the string \*(Aqa\*(Aq." 4
.el .IP "0. Start with the first letter in the string \f(CW\*(Aqa\*(Aq." 4
Item "0. Start with the first letter in the string a."
\
.ie n .IP "1. Try the first alternative in the first group \*(Aqabd\*(Aq." 4
.el .IP "1. Try the first alternative in the first group \f(CW\*(Aqabd\*(Aq." 4
Item "1. Try the first alternative in the first group abd."
\
.ie n .IP "2.  Match \*(Aqa\*(Aq followed by \*(Aqb\*(Aq. So far so good." 4
.el .IP "2.  Match \f(CW\*(Aqa\*(Aq followed by \f(CW\*(Aqb\*(Aq. So far so good." 4
Item "2. Match a followed by b. So far so good."
\
.ie n .IP "3.  \*(Aqd\*(Aq in the regexp doesn't match \*(Aqc\*(Aq in the string - a dead end.  So backtrack two characters and pick the second alternative in the first group \*(Aqabc\*(Aq." 4
.el .IP "3.  \f(CW\*(Aqd\*(Aq in the regexp doesn't match \f(CW\*(Aqc\*(Aq in the string - a dead end.  So backtrack two characters and pick the second alternative in the first group \f(CW\*(Aqabc\*(Aq." 4
Item "3. d in the regexp doesn't match c in the string - a dead end. So backtrack two characters and pick the second alternative in the first group abc."
\
.ie n .IP "4.  Match \*(Aqa\*(Aq followed by \*(Aqb\*(Aq followed by \*(Aqc\*(Aq.  We are on a roll and have satisfied the first group. Set $1 to \*(Aqabc\*(Aq." 4
.el .IP "4.  Match \f(CW\*(Aqa\*(Aq followed by \f(CW\*(Aqb\*(Aq followed by \f(CW\*(Aqc\*(Aq.  We are on a roll and have satisfied the first group. Set \f(CW$1 to \f(CW\*(Aqabc\*(Aq." 4
Item "4. Match a followed by b followed by c. We are on a roll and have satisfied the first group. Set $1 to abc."
\
.ie n .IP "5 Move on to the second group and pick the first alternative \*(Aqdf\*(Aq." 4
.el .IP "5 Move on to the second group and pick the first alternative \f(CW\*(Aqdf\*(Aq." 4
Item "5 Move on to the second group and pick the first alternative df."
\
.ie n .IP "6 Match the \*(Aqd\*(Aq." 4
.el .IP "6 Match the \f(CW\*(Aqd\*(Aq." 4
Item "6 Match the d."
\
.ie n .IP "7.  \*(Aqf\*(Aq in the regexp doesn't match \*(Aqe\*(Aq in the string, so a dead end.  Backtrack one character and pick the second alternative in the second group \*(Aqd\*(Aq." 4
.el .IP "7.  \f(CW\*(Aqf\*(Aq in the regexp doesn't match \f(CW\*(Aqe\*(Aq in the string, so a dead end.  Backtrack one character and pick the second alternative in the second group \f(CW\*(Aqd\*(Aq." 4
Item "7. f in the regexp doesn't match e in the string, so a dead end. Backtrack one character and pick the second alternative in the second group d."
\
.ie n .IP "8.  \*(Aqd\*(Aq matches. The second grouping is satisfied, so set $2 to \*(Aqd\*(Aq." 4
.el .IP "8.  \f(CW\*(Aqd\*(Aq matches. The second grouping is satisfied, so set \f(CW$2 to \f(CW\*(Aqd\*(Aq." 4
Item "8. d matches. The second grouping is satisfied, so set $2 to d."
\
.ie n .IP "9.  We are at the end of the regexp, so we are done! We have matched \*(Aqabcd\*(Aq out of the string ""abcde""." 4
.el .IP "9.  We are at the end of the regexp, so we are done! We have matched \f(CW\*(Aqabcd\*(Aq out of the string \f(CW``abcde''." 4
Item "9. We are at the end of the regexp, so we are done! We have matched abcd out of the string ""abcde""."

There are a couple of things to note about this analysis.  First, the
third alternative in the second group \f(CW\*(Aqde\*(Aq also allows a match, but we
stopped before we got to it - at a given character position, leftmost
wins.  Second, we were able to get a match at the first character
position of the string \f(CW\*(Aqa\*(Aq.  If there were no matches at the first
position, Perl would move to the second character position \f(CW\*(Aqb\*(Aq and
attempt the match all over again.  Only when all possible paths at all
possible character positions have been exhausted does Perl give
up and declare \f(CW\*(C`$string\ =~\ /(abd|abc)(df|d|de)/;\*(C' to be false.

Even with all this work, regexp matching happens remarkably fast.  To
speed things up, Perl compiles the regexp into a compact sequence of
opcodes that can often fit inside a processor cache.  When the code is
executed, these opcodes can then run at full throttle and search very
quickly.

### Extracting matches

Subsection "Extracting matches"
The grouping metacharacters \f(CW\*(C`()\*(C' also serve another completely
different function: they allow the extraction of the parts of a string
that matched.  This is very useful to find out what matched and for
text processing in general.  For each grouping, the part that matched
inside goes into the special variables \f(CW$1, \f(CW$2, *etc*.  They can be
used just as ordinary variables:

.Vb 6
    # extract hours, minutes, seconds
    if ($time =~ /(\\d\\d):(\\d\\d):(\\d\\d)/) \{    # match hh:mm:ss format
        $hours = $1;
        $minutes = $2;
        $seconds = $3;
    \}
.Ve

Now, we know that in scalar context,
\f(CW\*(C`$time\ =~\ /(\\d\\d):(\\d\\d):(\\d\\d)/\*(C' returns a true or false
value.  In list context, however, it returns the list of matched values
\f(CW\*(C`($1,$2,$3)\*(C'.  So we could write the code more compactly as

.Vb 2
    # extract hours, minutes, seconds
    ($hours, $minutes, $second) = ($time =~ /(\\d\\d):(\\d\\d):(\\d\\d)/);
.Ve

If the groupings in a regexp are nested, \f(CW$1 gets the group with the
leftmost opening parenthesis, \f(CW$2 the next opening parenthesis,
*etc*.  Here is a regexp with nested groups:

.Vb 2
    /(ab(cd|ef)((gi)|j))/;
     1  2      34
.Ve

If this regexp matches, \f(CW$1 contains a string starting with
\f(CW\*(Aqab\*(Aq, \f(CW$2 is either set to \f(CW\*(Aqcd\*(Aq or \f(CW\*(Aqef\*(Aq, \f(CW$3 equals either
\f(CW\*(Aqgi\*(Aq or \f(CW\*(Aqj\*(Aq, and \f(CW$4 is either set to \f(CW\*(Aqgi\*(Aq, just like \f(CW$3,
or it remains undefined.

For convenience, Perl sets \f(CW$+ to the string held by the highest numbered
\f(CW$1, \f(CW$2,... that got assigned (and, somewhat related, \f(CW$^N to the
value of the \f(CW$1, \f(CW$2,... most-recently assigned; *i.e.* the \f(CW$1,
\f(CW$2,... associated with the rightmost closing parenthesis used in the
match).

### Backreferences

Subsection "Backreferences"
Closely associated with the matching variables \f(CW$1, \f(CW$2, ... are
the *backreferences* \f(CW\*(C`\\g1\*(C', \f(CW\*(C`\\g2\*(C',...  Backreferences are simply
matching variables that can be used *inside* a regexp.  This is a
really nice feature; what matches later in a regexp is made to depend on
what matched earlier in the regexp.  Suppose we wanted to look
for doubled words in a text, like \*(L"the the\*(R".  The following regexp finds
all 3-letter doubles with a space in between:

.Vb 1
    /\\b(\\w\\w\\w)\\s\\g1\\b/;
.Ve

The grouping assigns a value to \f(CW\*(C`\\g1\*(C', so that the same 3-letter sequence
is used for both parts.

A similar task is to find words consisting of two identical parts:

.Vb 7
    % simple_grep \*(Aq^(\\w\\w\\w\\w|\\w\\w\\w|\\w\\w|\\w)\\g1$\*(Aq /usr/dict/words
    beriberi
    booboo
    coco
    mama
    murmur
    papa
.Ve

The regexp has a single grouping which considers 4-letter
combinations, then 3-letter combinations, *etc*., and uses \f(CW\*(C`\\g1\*(C' to look for
a repeat.  Although \f(CW$1 and \f(CW\*(C`\\g1\*(C' represent the same thing, care should be
taken to use matched variables \f(CW$1, \f(CW$2,... only *outside* a regexp
and backreferences \f(CW\*(C`\\g1\*(C', \f(CW\*(C`\\g2\*(C',... only *inside* a regexp; not doing
so may lead to surprising and unsatisfactory results.

### Relative backreferences

Subsection "Relative backreferences"
Counting the opening parentheses to get the correct number for a
backreference is error-prone as soon as there is more than one
capturing group.  A more convenient technique became available
with Perl 5.10: relative backreferences. To refer to the immediately
preceding capture group one now may write \f(CW\*(C`\\g-1\*(C' or \f(CW\*(C`\\g\{-1\}\*(C', the next but
last is available via \f(CW\*(C`\\g-2\*(C' or \f(CW\*(C`\\g\{-2\}\*(C', and so on.

Another good reason in addition to readability and maintainability
for using relative backreferences is illustrated by the following example,
where a simple pattern for matching peculiar strings is used:

.Vb 1
    $a99a = \*(Aq([a-z])(\\d)\\g2\\g1\*(Aq;   # matches a11a, g22g, x33x, etc.
.Ve

Now that we have this pattern stored as a handy string, we might feel
tempted to use it as a part of some other pattern:

.Vb 6
    $line = "code=e99e";
    if ($line =~ /^(\\w+)=$a99a$/)\{   # unexpected behavior!
        print "$1 is valid\\n";
    \} else \{
        print "bad line: \*(Aq$line\*(Aq\\n";
    \}
.Ve

But this doesn't match, at least not the way one might expect. Only
after inserting the interpolated \f(CW$a99a and looking at the resulting
full text of the regexp is it obvious that the backreferences have
backfired. The subexpression \f(CW\*(C`(\\w+)\*(C' has snatched number 1 and
demoted the groups in \f(CW$a99a by one rank. This can be avoided by
using relative backreferences:

.Vb 1
    $a99a = \*(Aq([a-z])(\\d)\\g\{-1\}\\g\{-2\}\*(Aq;  # safe for being interpolated
.Ve

### Named backreferences

Subsection "Named backreferences"
Perl 5.10 also introduced named capture groups and named backreferences.
To attach a name to a capturing group, you write either
\f(CW\*(C`(?<name>...)\*(C' or \f(CW\*(C`(?\*(Aqname\*(Aq...)\*(C'.  The backreference may
then be written as \f(CW\*(C`\\g\{name\}\*(C'.  It is permissible to attach the
same name to more than one group, but then only the leftmost one of the
eponymous set can be referenced.  Outside of the pattern a named
capture group is accessible through the \f(CW\*(C`%+\*(C' hash.

Assuming that we have to match calendar dates which may be given in one
of the three formats yyyy-mm-dd, mm/dd/yyyy or dd.mm.yyyy, we can write
three suitable patterns where we use \f(CW\*(Aqd\*(Aq, \f(CW\*(Aqm\*(Aq and \f(CW\*(Aqy\*(Aq respectively as the
names of the groups capturing the pertaining components of a date. The
matching operation combines the three patterns as alternatives:

.Vb 8
    $fmt1 = \*(Aq(?<y>\\d\\d\\d\\d)-(?<m>\\d\\d)-(?<d>\\d\\d)\*(Aq;
    $fmt2 = \*(Aq(?<m>\\d\\d)/(?<d>\\d\\d)/(?<y>\\d\\d\\d\\d)\*(Aq;
    $fmt3 = \*(Aq(?<d>\\d\\d)\\.(?<m>\\d\\d)\\.(?<y>\\d\\d\\d\\d)\*(Aq;
    for my $d (qw(2006-10-21 15.01.2007 10/31/2005)) \{
        if ( $d =~ m\{$fmt1|$fmt2|$fmt3\} )\{
            print "day=$+\{d\} month=$+\{m\} year=$+\{y\}\\n";
        \}
    \}
.Ve

If any of the alternatives matches, the hash \f(CW\*(C`%+\*(C' is bound to contain the
three key-value pairs.

### Alternative capture group numbering

Subsection "Alternative capture group numbering"
Yet another capturing group numbering technique (also as from Perl 5.10)
deals with the problem of referring to groups within a set of alternatives.
Consider a pattern for matching a time of the day, civil or military style:

.Vb 3
    if ( $time =~ /(\\d\\d|\\d):(\\d\\d)|(\\d\\d)(\\d\\d)/ )\{
        # process hour and minute
    \}
.Ve

Processing the results requires an additional if statement to determine
whether \f(CW$1 and \f(CW$2 or \f(CW$3 and \f(CW$4 contain the goodies. It would
be easier if we could use group numbers 1 and 2 in second alternative as
well, and this is exactly what the parenthesized construct \f(CW\*(C`(?|...)\*(C',
set around an alternative achieves. Here is an extended version of the
previous pattern:

.Vb 3
  if($time =~ /(?|(\\d\\d|\\d):(\\d\\d)|(\\d\\d)(\\d\\d))\\s+([A-Z][A-Z][A-Z])/)\{
      print "hour=$1 minute=$2 zone=$3\\n";
  \}
.Ve

Within the alternative numbering group, group numbers start at the same
position for each alternative. After the group, numbering continues
with one higher than the maximum reached across all the alternatives.

### Position information

Subsection "Position information"
In addition to what was matched, Perl also provides the
positions of what was matched as contents of the \f(CW\*(C`@-\*(C' and \f(CW\*(C`@+\*(C'
arrays. \f(CW\*(C`$-[0]\*(C' is the position of the start of the entire match and
\f(CW$+[0] is the position of the end. Similarly, \f(CW\*(C`$-[n]\*(C' is the
position of the start of the \f(CW$n match and \f(CW$+[n] is the position
of the end. If \f(CW$n is undefined, so are \f(CW\*(C`$-[n]\*(C' and \f(CW$+[n]. Then
this code

.Vb 6
    $x = "Mmm...donut, thought Homer";
    $x =~ /^(Mmm|Yech)\\.\\.\\.(donut|peas)/; # matches
    foreach $exp (1..$#-) \{
        no strict \*(Aqrefs\*(Aq;
        print "Match $exp: \*(Aq$$exp\*(Aq at position ($-[$exp],$+[$exp])\\n";
    \}
.Ve

prints

.Vb 2
    Match 1: \*(AqMmm\*(Aq at position (0,3)
    Match 2: \*(Aqdonut\*(Aq at position (6,11)
.Ve

Even if there are no groupings in a regexp, it is still possible to
find out what exactly matched in a string.  If you use them, Perl
will set \f(CW\*(C`$\`\*(C' to the part of the string before the match, will set \f(CW$&
to the part of the string that matched, and will set \f(CW\*(C`$\*(Aq\*(C' to the part
of the string after the match.  An example:

.Vb 3
    $x = "the cat caught the mouse";
    $x =~ /cat/;  # $\` = \*(Aqthe \*(Aq, $& = \*(Aqcat\*(Aq, $\*(Aq = \*(Aq caught the mouse\*(Aq
    $x =~ /the/;  # $\` = \*(Aq\*(Aq, $& = \*(Aqthe\*(Aq, $\*(Aq = \*(Aq cat caught the mouse\*(Aq
.Ve

In the second match, \f(CW\*(C`$\`\*(C' equals \f(CW\*(Aq\*(Aq because the regexp matched at the
first character position in the string and stopped; it never saw the
second \*(L"the\*(R".

If your code is to run on Perl versions earlier than
5.20, it is worthwhile to note that using \f(CW\*(C`$\`\*(C' and \f(CW\*(C`$\*(Aq\*(C'
slows down regexp matching quite a bit, while \f(CW$& slows it down to a
lesser extent, because if they are used in one regexp in a program,
they are generated for *all* regexps in the program.  So if raw
performance is a goal of your application, they should be avoided.
If you need to extract the corresponding substrings, use \f(CW\*(C`@-\*(C' and
\f(CW\*(C`@+\*(C' instead:

.Vb 3
    $\` is the same as substr( $x, 0, $-[0] )
    $& is the same as substr( $x, $-[0], $+[0]-$-[0] )
    $\*(Aq is the same as substr( $x, $+[0] )
.Ve

As of Perl 5.10, the \f(CW\*(C`$\{^PREMATCH\}\*(C', \f(CW\*(C`$\{^MATCH\}\*(C' and \f(CW\*(C`$\{^POSTMATCH\}\*(C'
variables may be used.  These are only set if the \f(CW\*(C`/p\*(C' modifier is
present.  Consequently they do not penalize the rest of the program.  In
Perl 5.20, \f(CW\*(C`$\{^PREMATCH\}\*(C', \f(CW\*(C`$\{^MATCH\}\*(C' and \f(CW\*(C`$\{^POSTMATCH\}\*(C' are available
whether the \f(CW\*(C`/p\*(C' has been used or not (the modifier is ignored), and
\f(CW\*(C`$\`\*(C', \f(CW\*(C`$\*(Aq\*(C' and \f(CW$& do not cause any speed difference.

### Non-capturing groupings

Subsection "Non-capturing groupings"
A group that is required to bundle a set of alternatives may or may not be
useful as a capturing group.  If it isn't, it just creates a superfluous
addition to the set of available capture group values, inside as well as
outside the regexp.  Non-capturing groupings, denoted by \f(CW\*(C`(?:regexp)\*(C',
still allow the regexp to be treated as a single unit, but don't establish
a capturing group at the same time.  Both capturing and non-capturing
groupings are allowed to co-exist in the same regexp.  Because there is
no extraction, non-capturing groupings are faster than capturing
groupings.  Non-capturing groupings are also handy for choosing exactly
which parts of a regexp are to be extracted to matching variables:

.Vb 2
    # match a number, $1-$4 are set, but we only want $1
    /([+-]?\\ *(\\d+(\\.\\d*)?|\\.\\d+)([eE][+-]?\\d+)?)/;

    # match a number faster , only $1 is set
    /([+-]?\\ *(?:\\d+(?:\\.\\d*)?|\\.\\d+)(?:[eE][+-]?\\d+)?)/;

    # match a number, get $1 = whole number, $2 = exponent
    /([+-]?\\ *(?:\\d+(?:\\.\\d*)?|\\.\\d+)(?:[eE]([+-]?\\d+))?)/;
.Ve

Non-capturing groupings are also useful for removing nuisance
elements gathered from a split operation where parentheses are
required for some reason:

.Vb 3
    $x = \*(Aq12aba34ba5\*(Aq;
    @num = split /(a|b)+/, $x;    # @num = (\*(Aq12\*(Aq,\*(Aqa\*(Aq,\*(Aq34\*(Aq,\*(Aqa\*(Aq,\*(Aq5\*(Aq)
    @num = split /(?:a|b)+/, $x;  # @num = (\*(Aq12\*(Aq,\*(Aq34\*(Aq,\*(Aq5\*(Aq)
.Ve

In Perl 5.22 and later, all groups within a regexp can be set to
non-capturing by using the new \f(CW\*(C`/n\*(C' flag:

.Vb 1
    "hello" =~ /(hi|hello)/n; # $1 is not set!
.Ve

See \*(L"n\*(R" in perlre for more information.

### Matching repetitions

Subsection "Matching repetitions"
The examples in the previous section display an annoying weakness.  We
were only matching 3-letter words, or chunks of words of 4 letters or
less.  We'd like to be able to match words or, more generally, strings
of any length, without writing out tedious alternatives like
\f(CW\*(C`\\w\\w\\w\\w|\\w\\w\\w|\\w\\w|\\w\*(C'.

This is exactly the problem the *quantifier* metacharacters \f(CW\*(Aq?\*(Aq,
\f(CW\*(Aq*\*(Aq, \f(CW\*(Aq+\*(Aq, and \f(CW\*(C`\{\}\*(C' were created for.  They allow us to delimit the
number of repeats for a portion of a regexp we consider to be a
match.  Quantifiers are put immediately after the character, character
class, or grouping that we want to specify.  They have the following
meanings:

- \(bu
\f(CW\*(C`a?\*(C' means: match \f(CW\*(Aqa\*(Aq 1 or 0 times

- \(bu
\f(CW\*(C`a*\*(C' means: match \f(CW\*(Aqa\*(Aq 0 or more times, *i.e.*, any number of times

- \(bu
\f(CW\*(C`a+\*(C' means: match \f(CW\*(Aqa\*(Aq 1 or more times, *i.e.*, at least once

- \(bu
\f(CW\*(C`a\{n,m\}\*(C' means: match at least \f(CW\*(C`n\*(C' times, but not more than \f(CW\*(C`m\*(C'
times.

- \(bu
\f(CW\*(C`a\{n,\}\*(C' means: match at least \f(CW\*(C`n\*(C' or more times

- \(bu
\f(CW\*(C`a\{,n\}\*(C' means: match at most \f(CW\*(C`n\*(C' times, or fewer

- \(bu
\f(CW\*(C`a\{n\}\*(C' means: match exactly \f(CW\*(C`n\*(C' times

If you like, you can add blanks (tab or space characters) within the
braces, but adjacent to them, and/or next to the comma (if any).

Here are some examples:

.Vb 10
    /[a-z]+\\s+\\d*/;  # match a lowercase word, at least one space, and
                     # any number of digits
    /(\\w+)\\s+\\g1/;    # match doubled words of arbitrary length
    /y(es)?/i;       # matches \*(Aqy\*(Aq, \*(AqY\*(Aq, or a case-insensitive \*(Aqyes\*(Aq
    $year =~ /^\\d\{2,4\}$/;  # make sure year is at least 2 but not more
                           # than 4 digits
    $year =~ /^\\d\{ 2, 4 \}$/;    # Same; for those who like wide open
                                # spaces.
    $year =~ /^\\d\{2, 4\}$/;      # Same.
    $year =~ /^\\d\{4\}$|^\\d\{2\}$/; # better match; throw out 3-digit dates
    $year =~ /^\\d\{2\}(\\d\{2\})?$/; # same thing written differently.
                                # However, this captures the last two
                                # digits in $1 and the other does not.

    % simple_grep \*(Aq^(\\w+)\\g1$\*(Aq /usr/dict/words   # isn\*(Aqt this easier?
    beriberi
    booboo
    coco
    mama
    murmur
    papa
.Ve

For all of these quantifiers, Perl will try to match as much of the
string as possible, while still allowing the regexp to succeed.  Thus
with \f(CW\*(C`/a?.../\*(C', Perl will first try to match the regexp with the \f(CW\*(Aqa\*(Aq
present; if that fails, Perl will try to match the regexp without the
\f(CW\*(Aqa\*(Aq present.  For the quantifier \f(CW\*(Aq*\*(Aq, we get the following:

.Vb 5
    $x = "the cat in the hat";
    $x =~ /^(.*)(cat)(.*)$/; # matches,
                             # $1 = \*(Aqthe \*(Aq
                             # $2 = \*(Aqcat\*(Aq
                             # $3 = \*(Aq in the hat\*(Aq
.Ve

Which is what we might expect, the match finds the only \f(CW\*(C`cat\*(C' in the
string and locks onto it.  Consider, however, this regexp:

.Vb 4
    $x =~ /^(.*)(at)(.*)$/; # matches,
                            # $1 = \*(Aqthe cat in the h\*(Aq
                            # $2 = \*(Aqat\*(Aq
                            # $3 = \*(Aq\*(Aq   (0 characters match)
.Ve

One might initially guess that Perl would find the \f(CW\*(C`at\*(C' in \f(CW\*(C`cat\*(C' and
stop there, but that wouldn't give the longest possible string to the
first quantifier \f(CW\*(C`.*\*(C'.  Instead, the first quantifier \f(CW\*(C`.*\*(C' grabs as
much of the string as possible while still having the regexp match.  In
this example, that means having the \f(CW\*(C`at\*(C' sequence with the final \f(CW\*(C`at\*(C'
in the string.  The other important principle illustrated here is that,
when there are two or more elements in a regexp, the *leftmost*
quantifier, if there is one, gets to grab as much of the string as
possible, leaving the rest of the regexp to fight over scraps.  Thus in
our example, the first quantifier \f(CW\*(C`.*\*(C' grabs most of the string, while
the second quantifier \f(CW\*(C`.*\*(C' gets the empty string.   Quantifiers that
grab as much of the string as possible are called *maximal match* or
*greedy* quantifiers.

When a regexp can match a string in several different ways, we can use
the principles above to predict which way the regexp will match:

- \(bu
Principle 0: Taken as a whole, any regexp will be matched at the
earliest possible position in the string.

- \(bu
Principle 1: In an alternation \f(CW\*(C`a|b|c...\*(C', the leftmost alternative
that allows a match for the whole regexp will be the one used.

- \(bu
Principle 2: The maximal matching quantifiers \f(CW\*(Aq?\*(Aq, \f(CW\*(Aq*\*(Aq, \f(CW\*(Aq+\*(Aq and
\f(CW\*(C`\{n,m\}\*(C' will in general match as much of the string as possible while
still allowing the whole regexp to match.

- \(bu
Principle 3: If there are two or more elements in a regexp, the
leftmost greedy quantifier, if any, will match as much of the string
as possible while still allowing the whole regexp to match.  The next
leftmost greedy quantifier, if any, will try to match as much of the
string remaining available to it as possible, while still allowing the
whole regexp to match.  And so on, until all the regexp elements are
satisfied.

As we have seen above, Principle 0 overrides the others. The regexp
will be matched as early as possible, with the other principles
determining how the regexp matches at that earliest character
position.

Here is an example of these principles in action:

.Vb 5
    $x = "The programming republic of Perl";
    $x =~ /^(.+)(e|r)(.*)$/;  # matches,
                              # $1 = \*(AqThe programming republic of Pe\*(Aq
                              # $2 = \*(Aqr\*(Aq
                              # $3 = \*(Aql\*(Aq
.Ve

This regexp matches at the earliest string position, \f(CW\*(AqT\*(Aq.  One
might think that \f(CW\*(Aqe\*(Aq, being leftmost in the alternation, would be
matched, but \f(CW\*(Aqr\*(Aq produces the longest string in the first quantifier.

.Vb 3
    $x =~ /(m\{1,2\})(.*)$/;  # matches,
                            # $1 = \*(Aqmm\*(Aq
                            # $2 = \*(Aqing republic of Perl\*(Aq
.Ve

Here, The earliest possible match is at the first \f(CW\*(Aqm\*(Aq in
\f(CW\*(C`programming\*(C'. \f(CW\*(C`m\{1,2\}\*(C' is the first quantifier, so it gets to match
a maximal \f(CW\*(C`mm\*(C'.

.Vb 3
    $x =~ /.*(m\{1,2\})(.*)$/;  # matches,
                              # $1 = \*(Aqm\*(Aq
                              # $2 = \*(Aqing republic of Perl\*(Aq
.Ve

Here, the regexp matches at the start of the string. The first
quantifier \f(CW\*(C`.*\*(C' grabs as much as possible, leaving just a single
\f(CW\*(Aqm\*(Aq for the second quantifier \f(CW\*(C`m\{1,2\}\*(C'.

.Vb 4
    $x =~ /(.?)(m\{1,2\})(.*)$/;  # matches,
                                # $1 = \*(Aqa\*(Aq
                                # $2 = \*(Aqmm\*(Aq
                                # $3 = \*(Aqing republic of Perl\*(Aq
.Ve

Here, \f(CW\*(C`.?\*(C' eats its maximal one character at the earliest possible
position in the string, \f(CW\*(Aqa\*(Aq in \f(CW\*(C`programming\*(C', leaving \f(CW\*(C`m\{1,2\}\*(C'
the opportunity to match both \f(CW\*(Aqm\*(Aq's. Finally,

.Vb 1
    "aXXXb" =~ /(X*)/; # matches with $1 = \*(Aq\*(Aq
.Ve

because it can match zero copies of \f(CW\*(AqX\*(Aq at the beginning of the
string.  If you definitely want to match at least one \f(CW\*(AqX\*(Aq, use
\f(CW\*(C`X+\*(C', not \f(CW\*(C`X*\*(C'.

Sometimes greed is not good.  At times, we would like quantifiers to
match a *minimal* piece of string, rather than a maximal piece.  For
this purpose, Larry Wall created the *minimal match* or
*non-greedy* quantifiers \f(CW\*(C`??\*(C', \f(CW\*(C`*?\*(C', \f(CW\*(C`+?\*(C', and \f(CW\*(C`\{\}?\*(C'.  These are
the usual quantifiers with a \f(CW\*(Aq?\*(Aq appended to them.  They have the
following meanings:

- \(bu
\f(CW\*(C`a??\*(C' means: match \f(CW\*(Aqa\*(Aq 0 or 1 times. Try 0 first, then 1.

- \(bu
\f(CW\*(C`a*?\*(C' means: match \f(CW\*(Aqa\*(Aq 0 or more times, *i.e.*, any number of times,
but as few times as possible

- \(bu
\f(CW\*(C`a+?\*(C' means: match \f(CW\*(Aqa\*(Aq 1 or more times, *i.e.*, at least once, but
as few times as possible

- \(bu
\f(CW\*(C`a\{n,m\}?\*(C' means: match at least \f(CW\*(C`n\*(C' times, not more than \f(CW\*(C`m\*(C'
times, as few times as possible

- \(bu
\f(CW\*(C`a\{n,\}?\*(C' means: match at least \f(CW\*(C`n\*(C' times, but as few times as
possible

- \(bu
\f(CW\*(C`a\{,n\}?\*(C' means: match at most \f(CW\*(C`n\*(C' times, but as few times as
possible

- \(bu
\f(CW\*(C`a\{n\}?\*(C' means: match exactly \f(CW\*(C`n\*(C' times.  Because we match exactly
\f(CW\*(C`n\*(C' times, \f(CW\*(C`a\{n\}?\*(C' is equivalent to \f(CW\*(C`a\{n\}\*(C' and is just there for
notational consistency.

Let's look at the example above, but with minimal quantifiers:

.Vb 5
    $x = "The programming republic of Perl";
    $x =~ /^(.+?)(e|r)(.*)$/; # matches,
                              # $1 = \*(AqTh\*(Aq
                              # $2 = \*(Aqe\*(Aq
                              # $3 = \*(Aq programming republic of Perl\*(Aq
.Ve

The minimal string that will allow both the start of the string \f(CW\*(Aq^\*(Aq
and the alternation to match is \f(CW\*(C`Th\*(C', with the alternation \f(CW\*(C`e|r\*(C'
matching \f(CW\*(Aqe\*(Aq.  The second quantifier \f(CW\*(C`.*\*(C' is free to gobble up the
rest of the string.

.Vb 3
    $x =~ /(m\{1,2\}?)(.*?)$/;  # matches,
                              # $1 = \*(Aqm\*(Aq
                              # $2 = \*(Aqming republic of Perl\*(Aq
.Ve

The first string position that this regexp can match is at the first
\f(CW\*(Aqm\*(Aq in \f(CW\*(C`programming\*(C'. At this position, the minimal \f(CW\*(C`m\{1,2\}?\*(C'
matches just one \f(CW\*(Aqm\*(Aq.  Although the second quantifier \f(CW\*(C`.*?\*(C' would
prefer to match no characters, it is constrained by the end-of-string
anchor \f(CW\*(Aq$\*(Aq to match the rest of the string.

.Vb 4
    $x =~ /(.*?)(m\{1,2\}?)(.*)$/;  # matches,
                                  # $1 = \*(AqThe progra\*(Aq
                                  # $2 = \*(Aqm\*(Aq
                                  # $3 = \*(Aqming republic of Perl\*(Aq
.Ve

In this regexp, you might expect the first minimal quantifier \f(CW\*(C`.*?\*(C'
to match the empty string, because it is not constrained by a \f(CW\*(Aq^\*(Aq
anchor to match the beginning of the word.  Principle 0 applies here,
however.  Because it is possible for the whole regexp to match at the
start of the string, it *will* match at the start of the string.  Thus
the first quantifier has to match everything up to the first \f(CW\*(Aqm\*(Aq.  The
second minimal quantifier matches just one \f(CW\*(Aqm\*(Aq and the third
quantifier matches the rest of the string.

.Vb 4
    $x =~ /(.??)(m\{1,2\})(.*)$/;  # matches,
                                 # $1 = \*(Aqa\*(Aq
                                 # $2 = \*(Aqmm\*(Aq
                                 # $3 = \*(Aqing republic of Perl\*(Aq
.Ve

Just as in the previous regexp, the first quantifier \f(CW\*(C`.??\*(C' can match
earliest at position \f(CW\*(Aqa\*(Aq, so it does.  The second quantifier is
greedy, so it matches \f(CW\*(C`mm\*(C', and the third matches the rest of the
string.

We can modify principle 3 above to take into account non-greedy
quantifiers:

- \(bu
Principle 3: If there are two or more elements in a regexp, the
leftmost greedy (non-greedy) quantifier, if any, will match as much
(little) of the string as possible while still allowing the whole
regexp to match.  The next leftmost greedy (non-greedy) quantifier, if
any, will try to match as much (little) of the string remaining
available to it as possible, while still allowing the whole regexp to
match.  And so on, until all the regexp elements are satisfied.

Just like alternation, quantifiers are also susceptible to
backtracking.  Here is a step-by-step analysis of the example

.Vb 5
    $x = "the cat in the hat";
    $x =~ /^(.*)(at)(.*)$/; # matches,
                            # $1 = \*(Aqthe cat in the h\*(Aq
                            # $2 = \*(Aqat\*(Aq
                            # $3 = \*(Aq\*(Aq   (0 matches)
.Ve
.ie n .IP "0.  Start with the first letter in the string \*(Aqt\*(Aq." 4
.el .IP "0.  Start with the first letter in the string \f(CW\*(Aqt\*(Aq." 4
Item "0. Start with the first letter in the string t."
\
.ie n .IP "1.  The first quantifier \*(Aq.*\*(Aq starts out by matching the whole string """"the cat in the hat""""." 4
.el .IP "1.  The first quantifier \f(CW\*(Aq.*\*(Aq starts out by matching the whole string ``\f(CWthe cat in the hat''." 4
Item "1. The first quantifier .* starts out by matching the whole string ""the cat in the hat""."
\
.ie n .IP "2.  \*(Aqa\*(Aq in the regexp element \*(Aqat\*(Aq doesn't match the end of the string.  Backtrack one character." 4
.el .IP "2.  \f(CW\*(Aqa\*(Aq in the regexp element \f(CW\*(Aqat\*(Aq doesn't match the end of the string.  Backtrack one character." 4
Item "2. a in the regexp element at doesn't match the end of the string. Backtrack one character."
\
.ie n .IP "3.  \*(Aqa\*(Aq in the regexp element \*(Aqat\*(Aq still doesn't match the last letter of the string \*(Aqt\*(Aq, so backtrack one more character." 4
.el .IP "3.  \f(CW\*(Aqa\*(Aq in the regexp element \f(CW\*(Aqat\*(Aq still doesn't match the last letter of the string \f(CW\*(Aqt\*(Aq, so backtrack one more character." 4
Item "3. a in the regexp element at still doesn't match the last letter of the string t, so backtrack one more character."
\
.ie n .IP "4.  Now we can match the \*(Aqa\*(Aq and the \*(Aqt\*(Aq." 4
.el .IP "4.  Now we can match the \f(CW\*(Aqa\*(Aq and the \f(CW\*(Aqt\*(Aq." 4
Item "4. Now we can match the a and the t."
\
.ie n .IP "5.  Move on to the third element \*(Aq.*\*(Aq.  Since we are at the end of the string and \*(Aq.*\*(Aq can match 0 times, assign it the empty string." 4
.el .IP "5.  Move on to the third element \f(CW\*(Aq.*\*(Aq.  Since we are at the end of the string and \f(CW\*(Aq.*\*(Aq can match 0 times, assign it the empty string." 4
Item "5. Move on to the third element .*. Since we are at the end of the string and .* can match 0 times, assign it the empty string."
\

- 6.  We are done!
Item "6. We are done!"

Most of the time, all this moving forward and backtracking happens
quickly and searching is fast. There are some pathological regexps,
however, whose execution time exponentially grows with the size of the
string.  A typical structure that blows up in your face is of the form

.Vb 1
    /(a|b+)*/;
.Ve

The problem is the nested indeterminate quantifiers.  There are many
different ways of partitioning a string of length n between the \f(CW\*(Aq+\*(Aq
and \f(CW\*(Aq*\*(Aq: one repetition with \f(CW\*(C`b+\*(C' of length n, two repetitions with
the first \f(CW\*(C`b+\*(C' length k and the second with length n-k, m repetitions
whose bits add up to length n, *etc*.  In fact there are an exponential
number of ways to partition a string as a function of its length.  A
regexp may get lucky and match early in the process, but if there is
no match, Perl will try *every* possibility before giving up.  So be
careful with nested \f(CW\*(Aq*\*(Aq's, \f(CW\*(C`\{n,m\}\*(C''s, and \f(CW\*(Aq+\*(Aq's.  The book
*Mastering Regular Expressions* by Jeffrey Friedl gives a wonderful
discussion of this and other efficiency issues.

### Possessive quantifiers

Subsection "Possessive quantifiers"
Backtracking during the relentless search for a match may be a waste
of time, particularly when the match is bound to fail.  Consider
the simple pattern

.Vb 1
    /^\\w+\\s+\\w+$/; # a word, spaces, a word
.Ve

Whenever this is applied to a string which doesn't quite meet the
pattern's expectations such as \f(CW"abc\ \ " or \f(CW"abc\ \ def\ ",
the regexp engine will backtrack, approximately once for each character
in the string.  But we know that there is no way around taking *all*
of the initial word characters to match the first repetition, that *all*
spaces must be eaten by the middle part, and the same goes for the second
word.

With the introduction of the *possessive quantifiers* in Perl 5.10, we
have a way of instructing the regexp engine not to backtrack, with the
usual quantifiers with a \f(CW\*(Aq+\*(Aq appended to them.  This makes them greedy as
well as stingy; once they succeed they won't give anything back to permit
another solution. They have the following meanings:

- \(bu
\f(CW\*(C`a\{n,m\}+\*(C' means: match at least \f(CW\*(C`n\*(C' times, not more than \f(CW\*(C`m\*(C' times,
as many times as possible, and don't give anything up. \f(CW\*(C`a?+\*(C' is short
for \f(CW\*(C`a\{0,1\}+\*(C'

- \(bu
\f(CW\*(C`a\{n,\}+\*(C' means: match at least \f(CW\*(C`n\*(C' times, but as many times as possible,
and don't give anything up. \f(CW\*(C`a++\*(C' is short for \f(CW\*(C`a\{1,\}+\*(C'.

- \(bu
\f(CW\*(C`a\{,n\}+\*(C' means: match as many times as possible up to at most \f(CW\*(C`n\*(C'
times, and don't give anything up. \f(CW\*(C`a*+\*(C' is short for \f(CW\*(C`a\{0,\}+\*(C'.

- \(bu
\f(CW\*(C`a\{n\}+\*(C' means: match exactly \f(CW\*(C`n\*(C' times.  It is just there for
notational consistency.

These possessive quantifiers represent a special case of a more general
concept, the *independent subexpression*, see below.

As an example where a possessive quantifier is suitable we consider
matching a quoted string, as it appears in several programming languages.
The backslash is used as an escape character that indicates that the
next character is to be taken literally, as another character for the
string.  Therefore, after the opening quote, we expect a (possibly
empty) sequence of alternatives: either some character except an
unescaped quote or backslash or an escaped character.

.Vb 1
    /"(?:[^"\\\\]++|\\\\.)*+"/;
.Ve

### Building a regexp

Subsection "Building a regexp"
At this point, we have all the basic regexp concepts covered, so let's
give a more involved example of a regular expression.  We will build a
regexp that matches numbers.

The first task in building a regexp is to decide what we want to match
and what we want to exclude.  In our case, we want to match both
integers and floating point numbers and we want to reject any string
that isn't a number.

The next task is to break the problem down into smaller problems that
are easily converted into a regexp.

The simplest case is integers.  These consist of a sequence of digits,
with an optional sign in front.  The digits we can represent with
\f(CW\*(C`\\d+\*(C' and the sign can be matched with \f(CW\*(C`[+-]\*(C'.  Thus the integer
regexp is

.Vb 1
    /[+-]?\\d+/;  # matches integers
.Ve

A floating point number potentially has a sign, an integral part, a
decimal point, a fractional part, and an exponent.  One or more of these
parts is optional, so we need to check out the different
possibilities.  Floating point numbers which are in proper form include
123., 0.345, .34, -1e6, and 25.4E-72.  As with integers, the sign out
front is completely optional and can be matched by \f(CW\*(C`[+-]?\*(C'.  We can
see that if there is no exponent, floating point numbers must have a
decimal point, otherwise they are integers.  We might be tempted to
model these with \f(CW\*(C`\\d*\\.\\d*\*(C', but this would also match just a single
decimal point, which is not a number.  So the three cases of floating
point number without exponent are

.Vb 3
   /[+-]?\\d+\\./;  # 1., 321., etc.
   /[+-]?\\.\\d+/;  # .1, .234, etc.
   /[+-]?\\d+\\.\\d+/;  # 1.0, 30.56, etc.
.Ve

These can be combined into a single regexp with a three-way alternation:

.Vb 1
   /[+-]?(\\d+\\.\\d+|\\d+\\.|\\.\\d+)/;  # floating point, no exponent
.Ve

In this alternation, it is important to put \f(CW\*(Aq\\d+\\.\\d+\*(Aq before
\f(CW\*(Aq\\d+\\.\*(Aq.  If \f(CW\*(Aq\\d+\\.\*(Aq were first, the regexp would happily match that
and ignore the fractional part of the number.

Now consider floating point numbers with exponents.  The key
observation here is that *both* integers and numbers with decimal
points are allowed in front of an exponent.  Then exponents, like the
overall sign, are independent of whether we are matching numbers with
or without decimal points, and can be \*(L"decoupled\*(R" from the
mantissa.  The overall form of the regexp now becomes clear:

.Vb 1
    /^(optional sign)(integer | f.p. mantissa)(optional exponent)$/;
.Ve

The exponent is an \f(CW\*(Aqe\*(Aq or \f(CW\*(AqE\*(Aq, followed by an integer.  So the
exponent regexp is

.Vb 1
   /[eE][+-]?\\d+/;  # exponent
.Ve

Putting all the parts together, we get a regexp that matches numbers:

.Vb 1
   /^[+-]?(\\d+\\.\\d+|\\d+\\.|\\.\\d+|\\d+)([eE][+-]?\\d+)?$/;  # Ta da!
.Ve

Long regexps like this may impress your friends, but can be hard to
decipher.  In complex situations like this, the \f(CW\*(C`/x\*(C' modifier for a
match is invaluable.  It allows one to put nearly arbitrary whitespace
and comments into a regexp without affecting their meaning.  Using it,
we can rewrite our \*(L"extended\*(R" regexp in the more pleasing form

.Vb 10
   /^
      [+-]?         # first, match an optional sign
      (             # then match integers or f.p. mantissas:
          \\d+\\.\\d+  # mantissa of the form a.b
         |\\d+\\.     # mantissa of the form a.
         |\\.\\d+     # mantissa of the form .b
         |\\d+       # integer of the form a
      )
      ( [eE] [+-]? \\d+ )?  # finally, optionally match an exponent
   $/x;
.Ve

If whitespace is mostly irrelevant, how does one include space
characters in an extended regexp? The answer is to backslash it
\f(CW\*(Aq\\\ \*(Aq or put it in a character class \f(CW\*(C`[\ ]\*(C'.  The same thing
goes for pound signs: use \f(CW\*(C`\\#\*(C' or \f(CW\*(C`[#]\*(C'.  For instance, Perl allows
a space between the sign and the mantissa or integer, and we could add
this to our regexp as follows:

.Vb 10
   /^
      [+-]?\\ *      # first, match an optional sign *and space*
      (             # then match integers or f.p. mantissas:
          \\d+\\.\\d+  # mantissa of the form a.b
         |\\d+\\.     # mantissa of the form a.
         |\\.\\d+     # mantissa of the form .b
         |\\d+       # integer of the form a
      )
      ( [eE] [+-]? \\d+ )?  # finally, optionally match an exponent
   $/x;
.Ve

In this form, it is easier to see a way to simplify the
alternation.  Alternatives 1, 2, and 4 all start with \f(CW\*(C`\\d+\*(C', so it
could be factored out:

.Vb 11
   /^
      [+-]?\\ *      # first, match an optional sign
      (             # then match integers or f.p. mantissas:
          \\d+       # start out with a ...
          (
              \\.\\d* # mantissa of the form a.b or a.
          )?        # ? takes care of integers of the form a
         |\\.\\d+     # mantissa of the form .b
      )
      ( [eE] [+-]? \\d+ )?  # finally, optionally match an exponent
   $/x;
.Ve

Starting in Perl v5.26, specifying \f(CW\*(C`/xx\*(C' changes the square-bracketed
portions of a pattern to ignore tabs and space characters unless they
are escaped by preceding them with a backslash.  So, we could write

.Vb 11
   /^
      [ + - ]?\\ *   # first, match an optional sign
      (             # then match integers or f.p. mantissas:
          \\d+       # start out with a ...
          (
              \\.\\d* # mantissa of the form a.b or a.
          )?        # ? takes care of integers of the form a
         |\\.\\d+     # mantissa of the form .b
      )
      ( [ e E ] [ + - ]? \\d+ )?  # finally, optionally match an exponent
   $/xx;
.Ve

This doesn't really improve the legibility of this example, but it's
available in case you want it.  Squashing the pattern down to the
compact form, we have

.Vb 1
    /^[+-]?\\ *(\\d+(\\.\\d*)?|\\.\\d+)([eE][+-]?\\d+)?$/;
.Ve

This is our final regexp.  To recap, we built a regexp by

- \(bu
specifying the task in detail,

- \(bu
breaking down the problem into smaller parts,

- \(bu
translating the small parts into regexps,

- \(bu
combining the regexps,

- \(bu
and optimizing the final combined regexp.

These are also the typical steps involved in writing a computer
program.  This makes perfect sense, because regular expressions are
essentially programs written in a little computer language that specifies
patterns.

### Using regular expressions in Perl

Subsection "Using regular expressions in Perl"
The last topic of Part 1 briefly covers how regexps are used in Perl
programs.  Where do they fit into Perl syntax?

We have already introduced the matching operator in its default
\f(CW\*(C`/regexp/\*(C' and arbitrary delimiter \f(CW\*(C`m!regexp!\*(C' forms.  We have used
the binding operator \f(CW\*(C`=~\*(C' and its negation \f(CW\*(C`!~\*(C' to test for string
matches.  Associated with the matching operator, we have discussed the
single line \f(CW\*(C`/s\*(C', multi-line \f(CW\*(C`/m\*(C', case-insensitive \f(CW\*(C`/i\*(C' and
extended \f(CW\*(C`/x\*(C' modifiers.  There are a few more things you might
want to know about matching operators.

*Prohibiting substitution*
Subsection "Prohibiting substitution"

If you change \f(CW$pattern after the first substitution happens, Perl
will ignore it.  If you don't want any substitutions at all, use the
special delimiter \f(CW\*(C`m\*(Aq\*(Aq\*(C':

.Vb 4
    @pattern = (\*(AqSeuss\*(Aq);
    while (<>) \{
        print if m\*(Aq@pattern\*(Aq;  # matches literal \*(Aq@pattern\*(Aq, not \*(AqSeuss\*(Aq
    \}
.Ve

Similar to strings, \f(CW\*(C`m\*(Aq\*(Aq\*(C' acts like apostrophes on a regexp; all other
\f(CW\*(Aqm\*(Aq delimiters act like quotes.  If the regexp evaluates to the empty string,
the regexp in the *last successful match* is used instead.  So we have

.Vb 2
    "dog" =~ /d/;  # \*(Aqd\*(Aq matches
    "dogbert" =~ //;  # this matches the \*(Aqd\*(Aq regexp used before
.Ve

*Global matching*
Subsection "Global matching"

The final two modifiers we will discuss here,
\f(CW\*(C`/g\*(C' and \f(CW\*(C`/c\*(C', concern multiple matches.
The modifier \f(CW\*(C`/g\*(C' stands for global matching and allows the
matching operator to match within a string as many times as possible.
In scalar context, successive invocations against a string will have
\f(CW\*(C`/g\*(C' jump from match to match, keeping track of position in the
string as it goes along.  You can get or set the position with the
\f(CW\*(C`pos()\*(C' function.

The use of \f(CW\*(C`/g\*(C' is shown in the following example.  Suppose we have
a string that consists of words separated by spaces.  If we know how
many words there are in advance, we could extract the words using
groupings:

.Vb 5
    $x = "cat dog house"; # 3 words
    $x =~ /^\\s*(\\w+)\\s+(\\w+)\\s+(\\w+)\\s*$/; # matches,
                                           # $1 = \*(Aqcat\*(Aq
                                           # $2 = \*(Aqdog\*(Aq
                                           # $3 = \*(Aqhouse\*(Aq
.Ve

But what if we had an indeterminate number of words? This is the sort
of task \f(CW\*(C`/g\*(C' was made for.  To extract all words, form the simple
regexp \f(CW\*(C`(\\w+)\*(C' and loop over all matches with \f(CW\*(C`/(\\w+)/g\*(C':

.Vb 3
    while ($x =~ /(\\w+)/g) \{
        print "Word is $1, ends at position ", pos $x, "\\n";
    \}
.Ve

prints

.Vb 3
    Word is cat, ends at position 3
    Word is dog, ends at position 7
    Word is house, ends at position 13
.Ve

A failed match or changing the target string resets the position.  If
you don't want the position reset after failure to match, add the
\f(CW\*(C`/c\*(C', as in \f(CW\*(C`/regexp/gc\*(C'.  The current position in the string is
associated with the string, not the regexp.  This means that different
strings have different positions and their respective positions can be
set or read independently.

In list context, \f(CW\*(C`/g\*(C' returns a list of matched groupings, or if
there are no groupings, a list of matches to the whole regexp.  So if
we wanted just the words, we could use

.Vb 4
    @words = ($x =~ /(\\w+)/g);  # matches,
                                # $words[0] = \*(Aqcat\*(Aq
                                # $words[1] = \*(Aqdog\*(Aq
                                # $words[2] = \*(Aqhouse\*(Aq
.Ve

Closely associated with the \f(CW\*(C`/g\*(C' modifier is the \f(CW\*(C`\\G\*(C' anchor.  The
\f(CW\*(C`\\G\*(C' anchor matches at the point where the previous \f(CW\*(C`/g\*(C' match left
off.  \f(CW\*(C`\\G\*(C' allows us to easily do context-sensitive matching:

.Vb 12
    $metric = 1;  # use metric units
    ...
    $x = <FILE>;  # read in measurement
    $x =~ /^([+-]?\\d+)\\s*/g;  # get magnitude
    $weight = $1;
    if ($metric) \{ # error checking
        print "Units error!" unless $x =~ /\\Gkg\\./g;
    \}
    else \{
        print "Units error!" unless $x =~ /\\Glbs\\./g;
    \}
    $x =~ /\\G\\s+(widget|sprocket)/g;  # continue processing
.Ve

The combination of \f(CW\*(C`/g\*(C' and \f(CW\*(C`\\G\*(C' allows us to process the string a
bit at a time and use arbitrary Perl logic to decide what to do next.
Currently, the \f(CW\*(C`\\G\*(C' anchor is only fully supported when used to anchor
to the start of the pattern.

\f(CW\*(C`\\G\*(C' is also invaluable in processing fixed-length records with
regexps.  Suppose we have a snippet of coding region \s-1DNA,\s0 encoded as
base pair letters \f(CW\*(C`ATCGTTGAAT...\*(C' and we want to find all the stop
codons \f(CW\*(C`TGA\*(C'.  In a coding region, codons are 3-letter sequences, so
we can think of the \s-1DNA\s0 snippet as a sequence of 3-letter records.  The
naive regexp

.Vb 3
    # expanded, this is "ATC GTT GAA TGC AAA TGA CAT GAC"
    $dna = "ATCGTTGAATGCAAATGACATGAC";
    $dna =~ /TGA/;
.Ve

doesn't work; it may match a \f(CW\*(C`TGA\*(C', but there is no guarantee that
the match is aligned with codon boundaries, *e.g.*, the substring
\f(CW\*(C`GTT\ GAA\*(C' gives a match.  A better solution is

.Vb 3
    while ($dna =~ /(\\w\\w\\w)*?TGA/g) \{  # note the minimal *?
        print "Got a TGA stop codon at position ", pos $dna, "\\n";
    \}
.Ve

which prints

.Vb 2
    Got a TGA stop codon at position 18
    Got a TGA stop codon at position 23
.Ve

Position 18 is good, but position 23 is bogus.  What happened?

The answer is that our regexp works well until we get past the last
real match.  Then the regexp will fail to match a synchronized \f(CW\*(C`TGA\*(C'
and start stepping ahead one character position at a time, not what we
want.  The solution is to use \f(CW\*(C`\\G\*(C' to anchor the match to the codon
alignment:

.Vb 3
    while ($dna =~ /\\G(\\w\\w\\w)*?TGA/g) \{
        print "Got a TGA stop codon at position ", pos $dna, "\\n";
    \}
.Ve

This prints

.Vb 1
    Got a TGA stop codon at position 18
.Ve

which is the correct answer.  This example illustrates that it is
important not only to match what is desired, but to reject what is not
desired.

(There are other regexp modifiers that are available, such as
\f(CW\*(C`/o\*(C', but their specialized uses are beyond the
scope of this introduction.  )

*Search and replace*
Subsection "Search and replace"

Regular expressions also play a big role in *search and replace*
operations in Perl.  Search and replace is accomplished with the
\f(CW\*(C`s///\*(C' operator.  The general form is
\f(CW\*(C`s/regexp/replacement/modifiers\*(C', with everything we know about
regexps and modifiers applying in this case as well.  The
*replacement* is a Perl double-quoted string that replaces in the
string whatever is matched with the \f(CW\*(C`regexp\*(C'.  The operator \f(CW\*(C`=~\*(C' is
also used here to associate a string with \f(CW\*(C`s///\*(C'.  If matching
against \f(CW$_, the \f(CW\*(C`$_\ =~\*(C' can be dropped.  If there is a match,
\f(CW\*(C`s///\*(C' returns the number of substitutions made; otherwise it returns
false.  Here are a few examples:

.Vb 8
    $x = "Time to feed the cat!";
    $x =~ s/cat/hacker/;   # $x contains "Time to feed the hacker!"
    if ($x =~ s/^(Time.*hacker)!$/$1 now!/) \{
        $more_insistent = 1;
    \}
    $y = "\*(Aqquoted words\*(Aq";
    $y =~ s/^\*(Aq(.*)\*(Aq$/$1/;  # strip single quotes,
                           # $y contains "quoted words"
.Ve

In the last example, the whole string was matched, but only the part
inside the single quotes was grouped.  With the \f(CW\*(C`s///\*(C' operator, the
matched variables \f(CW$1, \f(CW$2, *etc*. are immediately available for use
in the replacement expression, so we use \f(CW$1 to replace the quoted
string with just what was quoted.  With the global modifier, \f(CW\*(C`s///g\*(C'
will search and replace all occurrences of the regexp in the string:

.Vb 6
    $x = "I batted 4 for 4";
    $x =~ s/4/four/;   # doesn\*(Aqt do it all:
                       # $x contains "I batted four for 4"
    $x = "I batted 4 for 4";
    $x =~ s/4/four/g;  # does it all:
                       # $x contains "I batted four for four"
.Ve

If you prefer \*(L"regex\*(R" over \*(L"regexp\*(R" in this tutorial, you could use
the following program to replace it:

.Vb 9
    % cat > simple_replace
    #!/usr/bin/perl
    $regexp = shift;
    $replacement = shift;
    while (<>) \{
        s/$regexp/$replacement/g;
        print;
    \}
    ^D

    % simple_replace regexp regex perlretut.pod
.Ve

In \f(CW\*(C`simple_replace\*(C' we used the \f(CW\*(C`s///g\*(C' modifier to replace all
occurrences of the regexp on each line.  (Even though the regular
expression appears in a loop, Perl is smart enough to compile it
only once.)  As with \f(CW\*(C`simple_grep\*(C', both the
\f(CW\*(C`print\*(C' and the \f(CW\*(C`s/$regexp/$replacement/g\*(C' use \f(CW$_ implicitly.

If you don't want \f(CW\*(C`s///\*(C' to change your original variable you can use
the non-destructive substitute modifier, \f(CW\*(C`s///r\*(C'.  This changes the
behavior so that \f(CW\*(C`s///r\*(C' returns the final substituted string
(instead of the number of substitutions):

.Vb 3
    $x = "I like dogs.";
    $y = $x =~ s/dogs/cats/r;
    print "$x $y\\n";
.Ve

That example will print \*(L"I like dogs. I like cats\*(R". Notice the original
\f(CW$x variable has not been affected. The overall
result of the substitution is instead stored in \f(CW$y. If the
substitution doesn't affect anything then the original string is
returned:

.Vb 3
    $x = "I like dogs.";
    $y = $x =~ s/elephants/cougars/r;
    print "$x $y\\n"; # prints "I like dogs. I like dogs."
.Ve

One other interesting thing that the \f(CW\*(C`s///r\*(C' flag allows is chaining
substitutions:

.Vb 4
    $x = "Cats are great.";
    print $x =~ s/Cats/Dogs/r =~ s/Dogs/Frogs/r =~
        s/Frogs/Hedgehogs/r, "\\n";
    # prints "Hedgehogs are great."
.Ve

A modifier available specifically to search and replace is the
\f(CW\*(C`s///e\*(C' evaluation modifier.  \f(CW\*(C`s///e\*(C' treats the
replacement text as Perl code, rather than a double-quoted
string.  The value that the code returns is substituted for the
matched substring.  \f(CW\*(C`s///e\*(C' is useful if you need to do a bit of
computation in the process of replacing text.  This example counts
character frequencies in a line:

.Vb 4
    $x = "Bill the cat";
    $x =~ s/(.)/$chars\{$1\}++;$1/eg; # final $1 replaces char with itself
    print "frequency of \*(Aq$_\*(Aq is $chars\{$_\}\\n"
        foreach (sort \{$chars\{$b\} <=> $chars\{$a\}\} keys %chars);
.Ve

This prints

.Vb 9
    frequency of \*(Aq \*(Aq is 2
    frequency of \*(Aqt\*(Aq is 2
    frequency of \*(Aql\*(Aq is 2
    frequency of \*(AqB\*(Aq is 1
    frequency of \*(Aqc\*(Aq is 1
    frequency of \*(Aqe\*(Aq is 1
    frequency of \*(Aqh\*(Aq is 1
    frequency of \*(Aqi\*(Aq is 1
    frequency of \*(Aqa\*(Aq is 1
.Ve

As with the match \f(CW\*(C`m//\*(C' operator, \f(CW\*(C`s///\*(C' can use other delimiters,
such as \f(CW\*(C`s!!!\*(C' and \f(CW\*(C`s\{\}\{\}\*(C', and even \f(CW\*(C`s\{\}//\*(C'.  If single quotes are
used \f(CW\*(C`s\*(Aq\*(Aq\*(Aq\*(C', then the regexp and replacement are
treated as single-quoted strings and there are no
variable substitutions.  \f(CW\*(C`s///\*(C' in list context
returns the same thing as in scalar context, *i.e.*, the number of
matches.

*The split function*
Subsection "The split function"

The \f(CW\*(C`split()\*(C' function is another place where a regexp is used.
\f(CW\*(C`split /regexp/, string, limit\*(C' separates the \f(CW\*(C`string\*(C' operand into
a list of substrings and returns that list.  The regexp must be designed
to match whatever constitutes the separators for the desired substrings.
The \f(CW\*(C`limit\*(C', if present, constrains splitting into no more than \f(CW\*(C`limit\*(C'
number of strings.  For example, to split a string into words, use

.Vb 4
    $x = "Calvin and Hobbes";
    @words = split /\\s+/, $x;  # $word[0] = \*(AqCalvin\*(Aq
                               # $word[1] = \*(Aqand\*(Aq
                               # $word[2] = \*(AqHobbes\*(Aq
.Ve

If the empty regexp \f(CW\*(C`//\*(C' is used, the regexp always matches and
the string is split into individual characters.  If the regexp has
groupings, then the resulting list contains the matched substrings from the
groupings as well.  For instance,

.Vb 12
    $x = "/usr/bin/perl";
    @dirs = split m!/!, $x;  # $dirs[0] = \*(Aq\*(Aq
                             # $dirs[1] = \*(Aqusr\*(Aq
                             # $dirs[2] = \*(Aqbin\*(Aq
                             # $dirs[3] = \*(Aqperl\*(Aq
    @parts = split m!(/)!, $x;  # $parts[0] = \*(Aq\*(Aq
                                # $parts[1] = \*(Aq/\*(Aq
                                # $parts[2] = \*(Aqusr\*(Aq
                                # $parts[3] = \*(Aq/\*(Aq
                                # $parts[4] = \*(Aqbin\*(Aq
                                # $parts[5] = \*(Aq/\*(Aq
                                # $parts[6] = \*(Aqperl\*(Aq
.Ve

Since the first character of \f(CW$x matched the regexp, \f(CW\*(C`split\*(C' prepended
an empty initial element to the list.

If you have read this far, congratulations! You now have all the basic
tools needed to use regular expressions to solve a wide range of text
processing problems.  If this is your first time through the tutorial,
why not stop here and play around with regexps a while....  Part\ 2
concerns the more esoteric aspects of regular expressions and those
concepts certainly aren't needed right at the start.

## Part 2: Power tools

Header "Part 2: Power tools"
\s-1OK,\s0 you know the basics of regexps and you want to know more.  If
matching regular expressions is analogous to a walk in the woods, then
the tools discussed in Part 1 are analogous to topo maps and a
compass, basic tools we use all the time.  Most of the tools in part 2
are analogous to flare guns and satellite phones.  They aren't used
too often on a hike, but when we are stuck, they can be invaluable.

What follows are the more advanced, less used, or sometimes esoteric
capabilities of Perl regexps.  In Part 2, we will assume you are
comfortable with the basics and concentrate on the advanced features.

### More on characters, strings, and character classes

Subsection "More on characters, strings, and character classes"
There are a number of escape sequences and character classes that we
haven't covered yet.

There are several escape sequences that convert characters or strings
between upper and lower case, and they are also available within
patterns.  \f(CW\*(C`\\l\*(C' and \f(CW\*(C`\\u\*(C' convert the next character to lower or
upper case, respectively:

.Vb 4
    $x = "perl";
    $string =~ /\\u$x/;  # matches \*(AqPerl\*(Aq in $string
    $x = "M(rs?|s)\\\\."; # note the double backslash
    $string =~ /\\l$x/;  # matches \*(Aqmr.\*(Aq, \*(Aqmrs.\*(Aq, and \*(Aqms.\*(Aq,
.Ve

A \f(CW\*(C`\\L\*(C' or \f(CW\*(C`\\U\*(C' indicates a lasting conversion of case, until
terminated by \f(CW\*(C`\\E\*(C' or thrown over by another \f(CW\*(C`\\U\*(C' or \f(CW\*(C`\\L\*(C':

.Vb 4
    $x = "This word is in lower case:\\L SHOUT\\E";
    $x =~ /shout/;       # matches
    $x = "I STILL KEYPUNCH CARDS FOR MY 360";
    $x =~ /\\Ukeypunch/;  # matches punch card string
.Ve

If there is no \f(CW\*(C`\\E\*(C', case is converted until the end of the
string. The regexps \f(CW\*(C`\\L\\u$word\*(C' or \f(CW\*(C`\\u\\L$word\*(C' convert the first
character of \f(CW$word to uppercase and the rest of the characters to
lowercase.

Control characters can be escaped with \f(CW\*(C`\\c\*(C', so that a control-Z
character would be matched with \f(CW\*(C`\\cZ\*(C'.  The escape sequence
\f(CW\*(C`\\Q\*(C'...\f(CW\*(C`\\E\*(C' quotes, or protects most non-alphabetic characters.   For
instance,

.Vb 2
    $x = "\\QThat !^*&%~& cat!";
    $x =~ /\\Q!^*&%~&\\E/;  # check for rough language
.Ve

It does not protect \f(CW\*(Aq$\*(Aq or \f(CW\*(Aq@\*(Aq, so that variables can still be
substituted.

\f(CW\*(C`\\Q\*(C', \f(CW\*(C`\\L\*(C', \f(CW\*(C`\\l\*(C', \f(CW\*(C`\\U\*(C', \f(CW\*(C`\\u\*(C' and \f(CW\*(C`\\E\*(C' are actually part of
double-quotish syntax, and not part of regexp syntax proper.  They will
work if they appear in a regular expression embedded directly in a
program, but not when contained in a string that is interpolated in a
pattern.

Perl regexps can handle more than just the
standard \s-1ASCII\s0 character set.  Perl supports *Unicode*, a standard
for representing the alphabets from virtually all of the world's written
languages, and a host of symbols.  Perl's text strings are Unicode strings, so
they can contain characters with a value (codepoint or character number) higher
than 255.

What does this mean for regexps? Well, regexp users don't need to know
much about Perl's internal representation of strings.  But they do need
to know 1) how to represent Unicode characters in a regexp and 2) that
a matching operation will treat the string to be searched as a sequence
of characters, not bytes.  The answer to 1) is that Unicode characters
greater than \f(CW\*(C`chr(255)\*(C' are represented using the \f(CW\*(C`\\x\{hex\}\*(C' notation, because
\f(CW\*(C`\\x\*(C'*\s-1XY\s0* (without curly braces and *\s-1XY\s0* are two hex digits) doesn't
go further than 255.  (Starting in Perl 5.14, if you're an octal fan,
you can also use \f(CW\*(C`\\o\{oct\}\*(C'.)

.Vb 2
    /\\x\{263a\}/;   # match a Unicode smiley face :)
    /\\x\{ 263a \}/; # Same
.Ve

**\s-1NOTE\s0**: In Perl 5.6.0 it used to be that one needed to say \f(CW\*(C`use
utf8\*(C' to use any Unicode features.  This is no longer the case: for
almost all Unicode processing, the explicit \f(CW\*(C`utf8\*(C' pragma is not
needed.  (The only case where it matters is if your Perl script is in
Unicode and encoded in \s-1UTF-8,\s0 then an explicit \f(CW\*(C`use utf8\*(C' is needed.)

Figuring out the hexadecimal sequence of a Unicode character you want
or deciphering someone else's hexadecimal Unicode regexp is about as
much fun as programming in machine code.  So another way to specify
Unicode characters is to use the *named character* escape
sequence \f(CW\*(C`\\N\{\f(CIname\f(CW\}\*(C'.  *name* is a name for the Unicode character, as
specified in the Unicode standard.  For instance, if we wanted to
represent or match the astrological sign for the planet Mercury, we
could use

.Vb 3
    $x = "abc\\N\{MERCURY\}def";
    $x =~ /\\N\{MERCURY\}/;   # matches
    $x =~ /\\N\{ MERCURY \}/; # Also matches
.Ve

One can also use \*(L"short\*(R" names:

.Vb 2
    print "\\N\{GREEK SMALL LETTER SIGMA\} is called sigma.\\n";
    print "\\N\{greek:Sigma\} is an upper-case sigma.\\n";
.Ve

You can also restrict names to a certain alphabet by specifying the
charnames pragma:

.Vb 2
    use charnames qw(greek);
    print "\\N\{sigma\} is Greek sigma\\n";
.Ve

An index of character names is available on-line from the Unicode
Consortium, <https://www.unicode.org/charts/charindex.html>; explanatory
material with links to other resources at
<https://www.unicode.org/standard/where>.

Starting in Perl v5.32, an alternative to \f(CW\*(C`\\N\{...\}\*(C' for full names is
available, and that is to say

.Vb 1
 /\\p\{Name=greek small letter sigma\}/
.Ve

The casing of the character name is irrelevant when used in \f(CW\*(C`\\p\{\}\*(C', as
are most spaces, underscores and hyphens.  (A few outlier characters
cause problems with ignoring all of them always.  The details (which you
can look up when you get more proficient, and if ever needed) are in
<https://www.unicode.org/reports/tr44/tr44-24.html#UAX44-LM2>).

The answer to requirement 2) is that a regexp (mostly)
uses Unicode characters.  The \*(L"mostly\*(R" is for messy backward
compatibility reasons, but starting in Perl 5.14, any regexp compiled in
the scope of a \f(CW\*(C`use feature \*(Aqunicode_strings\*(Aq\*(C' (which is automatically
turned on within the scope of a \f(CW\*(C`use 5.012\*(C' or higher) will turn that
\*(L"mostly\*(R" into \*(L"always\*(R".  If you want to handle Unicode properly, you
should ensure that \f(CW\*(Aqunicode_strings\*(Aq is turned on.
Internally, this is encoded to bytes using either \s-1UTF-8\s0 or a native 8
bit encoding, depending on the history of the string, but conceptually
it is a sequence of characters, not bytes. See perlunitut for a
tutorial about that.

Let us now discuss Unicode character classes, most usually called
\*(L"character properties\*(R".  These are represented by the \f(CW\*(C`\\p\{\f(CIname\f(CW\}\*(C'
escape sequence.  The negation of this is \f(CW\*(C`\\P\{\f(CIname\f(CW\}\*(C'.  For example,
to match lower and uppercase characters,

.Vb 5
    $x = "BOB";
    $x =~ /^\\p\{IsUpper\}/;   # matches, uppercase char class
    $x =~ /^\\P\{IsUpper\}/;   # doesn\*(Aqt match, char class sans uppercase
    $x =~ /^\\p\{IsLower\}/;   # doesn\*(Aqt match, lowercase char class
    $x =~ /^\\P\{IsLower\}/;   # matches, char class sans lowercase
.Ve

(The "\f(CW\*(C`Is\*(C'" is optional.)

There are many, many Unicode character properties.  For the full list
see perluniprops.  Most of them have synonyms with shorter names,
also listed there.  Some synonyms are a single character.  For these,
you can drop the braces.  For instance, \f(CW\*(C`\\pM\*(C' is the same thing as
\f(CW\*(C`\\p\{Mark\}\*(C', meaning things like accent marks.

The Unicode \f(CW\*(C`\\p\{Script\}\*(C' and \f(CW\*(C`\\p\{Script_Extensions\}\*(C' properties are
used to categorize every Unicode character into the language script it
is written in.  For example,
English, French, and a bunch of other European languages are written in
the Latin script.  But there is also the Greek script, the Thai script,
the Katakana script, *etc*.  (\f(CW\*(C`Script\*(C' is an older, less advanced,
form of \f(CW\*(C`Script_Extensions\*(C', retained only for backwards
compatibility.)  You can test whether a character is in a particular
script  with, for example \f(CW\*(C`\\p\{Latin\}\*(C', \f(CW\*(C`\\p\{Greek\}\*(C', or
\f(CW\*(C`\\p\{Katakana\}\*(C'.  To test if it isn't in the Balinese script, you would
use \f(CW\*(C`\\P\{Balinese\}\*(C'.  (These all use \f(CW\*(C`Script_Extensions\*(C' under the
hood, as that gives better results.)

What we have described so far is the single form of the \f(CW\*(C`\\p\{...\}\*(C' character
classes.  There is also a compound form which you may run into.  These
look like \f(CW\*(C`\\p\{\f(CIname\f(CW=\f(CIvalue\f(CW\}\*(C' or \f(CW\*(C`\\p\{\f(CIname\f(CW:\f(CIvalue\f(CW\}\*(C' (the equals sign and colon
can be used interchangeably).  These are more general than the single form,
and in fact most of the single forms are just Perl-defined shortcuts for common
compound forms.  For example, the script examples in the previous paragraph
could be written equivalently as \f(CW\*(C`\\p\{Script_Extensions=Latin\}\*(C', \f(CW\*(C`\\p\{Script_Extensions:Greek\}\*(C',
\f(CW\*(C`\\p\{script_extensions=katakana\}\*(C', and \f(CW\*(C`\\P\{script_extensions=balinese\}\*(C' (case is irrelevant
between the \f(CW\*(C`\{\}\*(C' braces).  You may
never have to use the compound forms, but sometimes it is necessary, and their
use can make your code easier to understand.

\f(CW\*(C`\\X\*(C' is an abbreviation for a character class that comprises
a Unicode *extended grapheme cluster*.  This represents a \*(L"logical character\*(R":
what appears to be a single character, but may be represented internally by more
than one.  As an example, using the Unicode full names, *e.g.*, \*(L"A\ +\ \s-1COMBINING\s0\ \s-1RING\*(R"\s0 is a grapheme cluster with base character \*(L"A\*(R" and combining character
\*(L"\s-1COMBINING\s0\ \s-1RING,\s0 which translates in Danish to \*(R"A" with the circle atop it,
as in the word A\*ongstrom.

For the full and latest information about Unicode see the latest
Unicode standard, or the Unicode Consortium's website <https://www.unicode.org>

As if all those classes weren't enough, Perl also defines POSIX-style
character classes.  These have the form \f(CW\*(C`[:\f(CIname\f(CW:]\*(C', with *name* the
name of the \s-1POSIX\s0 class.  The \s-1POSIX\s0 classes are \f(CW\*(C`alpha\*(C', \f(CW\*(C`alnum\*(C',
\f(CW\*(C`ascii\*(C', \f(CW\*(C`cntrl\*(C', \f(CW\*(C`digit\*(C', \f(CW\*(C`graph\*(C', \f(CW\*(C`lower\*(C', \f(CW\*(C`print\*(C', \f(CW\*(C`punct\*(C',
\f(CW\*(C`space\*(C', \f(CW\*(C`upper\*(C', and \f(CW\*(C`xdigit\*(C', and two extensions, \f(CW\*(C`word\*(C' (a Perl
extension to match \f(CW\*(C`\\w\*(C'), and \f(CW\*(C`blank\*(C' (a \s-1GNU\s0 extension).  The \f(CW\*(C`/a\*(C'
modifier restricts these to matching just in the \s-1ASCII\s0 range; otherwise
they can match the same as their corresponding Perl Unicode classes:
\f(CW\*(C`[:upper:]\*(C' is the same as \f(CW\*(C`\\p\{IsUpper\}\*(C', *etc*.  (There are some
exceptions and gotchas with this; see perlrecharclass for a full
discussion.) The \f(CW\*(C`[:digit:]\*(C', \f(CW\*(C`[:word:]\*(C', and
\f(CW\*(C`[:space:]\*(C' correspond to the familiar \f(CW\*(C`\\d\*(C', \f(CW\*(C`\\w\*(C', and \f(CW\*(C`\\s\*(C'
character classes.  To negate a \s-1POSIX\s0 class, put a \f(CW\*(Aq^\*(Aq in front of
the name, so that, *e.g.*, \f(CW\*(C`[:^digit:]\*(C' corresponds to \f(CW\*(C`\\D\*(C' and, under
Unicode, \f(CW\*(C`\\P\{IsDigit\}\*(C'.  The Unicode and \s-1POSIX\s0 character classes can
be used just like \f(CW\*(C`\\d\*(C', with the exception that \s-1POSIX\s0 character
classes can only be used inside of a character class:

.Vb 6
    /\\s+[abc[:digit:]xyz]\\s*/;  # match a,b,c,x,y,z, or a digit
    /^=item\\s[[:digit:]]/;      # match \*(Aq=item\*(Aq,
                                # followed by a space and a digit
    /\\s+[abc\\p\{IsDigit\}xyz]\\s+/;  # match a,b,c,x,y,z, or a digit
    /^=item\\s\\p\{IsDigit\}/;        # match \*(Aq=item\*(Aq,
                                  # followed by a space and a digit
.Ve

Whew! That is all the rest of the characters and character classes.

### Compiling and saving regular expressions

Subsection "Compiling and saving regular expressions"
In Part 1 we mentioned that Perl compiles a regexp into a compact
sequence of opcodes.  Thus, a compiled regexp is a data structure
that can be stored once and used again and again.  The regexp quote
\f(CW\*(C`qr//\*(C' does exactly that: \f(CW\*(C`qr/string/\*(C' compiles the \f(CW\*(C`string\*(C' as a
regexp and transforms the result into a form that can be assigned to a
variable:

.Vb 1
    $reg = qr/foo+bar?/;  # reg contains a compiled regexp
.Ve

Then \f(CW$reg can be used as a regexp:

.Vb 3
    $x = "fooooba";
    $x =~ $reg;     # matches, just like /foo+bar?/
    $x =~ /$reg/;   # same thing, alternate form
.Ve

\f(CW$reg can also be interpolated into a larger regexp:

.Vb 1
    $x =~ /(abc)?$reg/;  # still matches
.Ve

As with the matching operator, the regexp quote can use different
delimiters, *e.g.*, \f(CW\*(C`qr!!\*(C', \f(CW\*(C`qr\{\}\*(C' or \f(CW\*(C`qr~~\*(C'.  Apostrophes
as delimiters (\f(CW\*(C`qr\*(Aq\*(Aq\*(C') inhibit any interpolation.

Pre-compiled regexps are useful for creating dynamic matches that
don't need to be recompiled each time they are encountered.  Using
pre-compiled regexps, we write a \f(CW\*(C`grep_step\*(C' program which greps
for a sequence of patterns, advancing to the next pattern as soon
as one has been satisfied.

.Vb 4
    % cat > grep_step
    #!/usr/bin/perl
    # grep_step - match <number> regexps, one after the other
    # usage: multi_grep <number> regexp1 regexp2 ... file1 file2 ...

    $number = shift;
    $regexp[$_] = shift foreach (0..$number-1);
    @compiled = map qr/$_/, @regexp;
    while ($line = <>) \{
        if ($line =~ /$compiled[0]/) \{
            print $line;
            shift @compiled;
            last unless @compiled;
        \}
    \}
    ^D

    % grep_step 3 shift print last grep_step
    $number = shift;
            print $line;
            last unless @compiled;
.Ve

Storing pre-compiled regexps in an array \f(CW@compiled allows us to
simply loop through the regexps without any recompilation, thus gaining
flexibility without sacrificing speed.

### Composing regular expressions at runtime

Subsection "Composing regular expressions at runtime"
Backtracking is more efficient than repeated tries with different regular
expressions.  If there are several regular expressions and a match with
any of them is acceptable, then it is possible to combine them into a set
of alternatives.  If the individual expressions are input data, this
can be done by programming a join operation.  We'll exploit this idea in
an improved version of the \f(CW\*(C`simple_grep\*(C' program: a program that matches
multiple patterns:

.Vb 4
    % cat > multi_grep
    #!/usr/bin/perl
    # multi_grep - match any of <number> regexps
    # usage: multi_grep <number> regexp1 regexp2 ... file1 file2 ...

    $number = shift;
    $regexp[$_] = shift foreach (0..$number-1);
    $pattern = join \*(Aq|\*(Aq, @regexp;

    while ($line = <>) \{
        print $line if $line =~ /$pattern/;
    \}
    ^D

    % multi_grep 2 shift for multi_grep
    $number = shift;
    $regexp[$_] = shift foreach (0..$number-1);
.Ve

Sometimes it is advantageous to construct a pattern from the *input*
that is to be analyzed and use the permissible values on the left
hand side of the matching operations.  As an example for this somewhat
paradoxical situation, let's assume that our input contains a command
verb which should match one out of a set of available command verbs,
with the additional twist that commands may be abbreviated as long as
the given string is unique. The program below demonstrates the basic
algorithm.

.Vb 10
    % cat > keymatch
    #!/usr/bin/perl
    $kwds = \*(Aqcopy compare list print\*(Aq;
    while( $cmd = <> )\{
        $cmd =~ s/^\\s+|\\s+$//g;  # trim leading and trailing spaces
        if( ( @matches = $kwds =~ /\\b$cmd\\w*/g ) == 1 )\{
            print "command: \*(Aq@matches\*(Aq\\n";
        \} elsif( @matches == 0 )\{
            print "no such command: \*(Aq$cmd\*(Aq\\n";
        \} else \{
            print "not unique: \*(Aq$cmd\*(Aq (could be one of: @matches)\\n";
        \}
    \}
    ^D

    % keymatch
    li
    command: \*(Aqlist\*(Aq
    co
    not unique: \*(Aqco\*(Aq (could be one of: copy compare)
    printer
    no such command: \*(Aqprinter\*(Aq
.Ve

Rather than trying to match the input against the keywords, we match the
combined set of keywords against the input.  The pattern matching
operation \f(CW\*(C`$kwds\ =~\ /\\b($cmd\\w*)/g\*(C' does several things at the
same time. It makes sure that the given command begins where a keyword
begins (\f(CW\*(C`\\b\*(C'). It tolerates abbreviations due to the added \f(CW\*(C`\\w*\*(C'. It
tells us the number of matches (\f(CW\*(C`scalar @matches\*(C') and all the keywords
that were actually matched.  You could hardly ask for more.

### Embedding comments and modifiers in a regular expression

Subsection "Embedding comments and modifiers in a regular expression"
Starting with this section, we will be discussing Perl's set of
*extended patterns*.  These are extensions to the traditional regular
expression syntax that provide powerful new tools for pattern
matching.  We have already seen extensions in the form of the minimal
matching constructs \f(CW\*(C`??\*(C', \f(CW\*(C`*?\*(C', \f(CW\*(C`+?\*(C', \f(CW\*(C`\{n,m\}?\*(C', \f(CW\*(C`\{n,\}?\*(C', and
\f(CW\*(C`\{,n\}?\*(C'.  Most of the extensions below have the form \f(CW\*(C`(?char...)\*(C',
where the \f(CW\*(C`char\*(C' is a character that determines the type of extension.

The first extension is an embedded comment \f(CW\*(C`(?#text)\*(C'.  This embeds a
comment into the regular expression without affecting its meaning.  The
comment should not have any closing parentheses in the text.  An
example is

.Vb 1
    /(?# Match an integer:)[+-]?\\d+/;
.Ve

This style of commenting has been largely superseded by the raw,
freeform commenting that is allowed with the \f(CW\*(C`/x\*(C' modifier.

Most modifiers, such as \f(CW\*(C`/i\*(C', \f(CW\*(C`/m\*(C', \f(CW\*(C`/s\*(C' and \f(CW\*(C`/x\*(C' (or any
combination thereof) can also be embedded in
a regexp using \f(CW\*(C`(?i)\*(C', \f(CW\*(C`(?m)\*(C', \f(CW\*(C`(?s)\*(C', and \f(CW\*(C`(?x)\*(C'.  For instance,

.Vb 7
    /(?i)yes/;  # match \*(Aqyes\*(Aq case insensitively
    /yes/i;     # same thing
    /(?x)(          # freeform version of an integer regexp
             [+-]?  # match an optional sign
             \\d+    # match a sequence of digits
         )
    /x;
.Ve

Embedded modifiers can have two important advantages over the usual
modifiers.  Embedded modifiers allow a custom set of modifiers for
*each* regexp pattern.  This is great for matching an array of regexps
that must have different modifiers:

.Vb 8
    $pattern[0] = \*(Aq(?i)doctor\*(Aq;
    $pattern[1] = \*(AqJohnson\*(Aq;
    ...
    while (<>) \{
        foreach $patt (@pattern) \{
            print if /$patt/;
        \}
    \}
.Ve

The second advantage is that embedded modifiers (except \f(CW\*(C`/p\*(C', which
modifies the entire regexp) only affect the regexp
inside the group the embedded modifier is contained in.  So grouping
can be used to localize the modifier's effects:

.Vb 1
    /Answer: ((?i)yes)/;  # matches \*(AqAnswer: yes\*(Aq, \*(AqAnswer: YES\*(Aq, etc.
.Ve

Embedded modifiers can also turn off any modifiers already present
by using, *e.g.*, \f(CW\*(C`(?-i)\*(C'.  Modifiers can also be combined into
a single expression, *e.g.*, \f(CW\*(C`(?s-i)\*(C' turns on single line mode and
turns off case insensitivity.

Embedded modifiers may also be added to a non-capturing grouping.
\f(CW\*(C`(?i-m:regexp)\*(C' is a non-capturing grouping that matches \f(CW\*(C`regexp\*(C'
case insensitively and turns off multi-line mode.

### Looking ahead and looking behind

Subsection "Looking ahead and looking behind"
This section concerns the lookahead and lookbehind assertions.  First,
a little background.

In Perl regular expressions, most regexp elements \*(L"eat up\*(R" a certain
amount of string when they match.  For instance, the regexp element
\f(CW\*(C`[abc]\*(C' eats up one character of the string when it matches, in the
sense that Perl moves to the next character position in the string
after the match.  There are some elements, however, that don't eat up
characters (advance the character position) if they match.  The examples
we have seen so far are the anchors.  The anchor \f(CW\*(Aq^\*(Aq matches the
beginning of the line, but doesn't eat any characters.  Similarly, the
word boundary anchor \f(CW\*(C`\\b\*(C' matches wherever a character matching \f(CW\*(C`\\w\*(C'
is next to a character that doesn't, but it doesn't eat up any
characters itself.  Anchors are examples of *zero-width assertions*:
zero-width, because they consume
no characters, and assertions, because they test some property of the
string.  In the context of our walk in the woods analogy to regexp
matching, most regexp elements move us along a trail, but anchors have
us stop a moment and check our surroundings.  If the local environment
checks out, we can proceed forward.  But if the local environment
doesn't satisfy us, we must backtrack.

Checking the environment entails either looking ahead on the trail,
looking behind, or both.  \f(CW\*(Aq^\*(Aq looks behind, to see that there are no
characters before.  \f(CW\*(Aq$\*(Aq looks ahead, to see that there are no
characters after.  \f(CW\*(C`\\b\*(C' looks both ahead and behind, to see if the
characters on either side differ in their \*(L"word-ness\*(R".

The lookahead and lookbehind assertions are generalizations of the
anchor concept.  Lookahead and lookbehind are zero-width assertions
that let us specify which characters we want to test for.  The
lookahead assertion is denoted by \f(CW\*(C`(?=regexp)\*(C' or (starting in 5.32,
experimentally in 5.28) \f(CW\*(C`(*pla:regexp)\*(C' or
\f(CW\*(C`(*positive_lookahead:regexp)\*(C'; and the lookbehind assertion is denoted
by \f(CW\*(C`(?<=fixed-regexp)\*(C' or (starting in 5.32, experimentally in
5.28) \f(CW\*(C`(*plb:fixed-regexp)\*(C' or \f(CW\*(C`(*positive_lookbehind:fixed-regexp)\*(C'.
Some examples are

.Vb 8
    $x = "I catch the housecat \*(AqTom-cat\*(Aq with catnip";
    $x =~ /cat(*pla:\\s)/;   # matches \*(Aqcat\*(Aq in \*(Aqhousecat\*(Aq
    @catwords = ($x =~ /(?<=\\s)cat\\w+/g);  # matches,
                                           # $catwords[0] = \*(Aqcatch\*(Aq
                                           # $catwords[1] = \*(Aqcatnip\*(Aq
    $x =~ /\\bcat\\b/;  # matches \*(Aqcat\*(Aq in \*(AqTom-cat\*(Aq
    $x =~ /(?<=\\s)cat(?=\\s)/; # doesn\*(Aqt match; no isolated \*(Aqcat\*(Aq in
                              # middle of $x
.Ve

Note that the parentheses in these are
non-capturing, since these are zero-width assertions.  Thus in the
second regexp, the substrings captured are those of the whole regexp
itself.  Lookahead can match arbitrary regexps, but
lookbehind prior to 5.30 \f(CW\*(C`(?<=fixed-regexp)\*(C' only works for regexps
of fixed width, *i.e.*, a fixed number of characters long.  Thus
\f(CW\*(C`(?<=(ab|bc))\*(C' is fine, but \f(CW\*(C`(?<=(ab)*)\*(C' prior to 5.30 is not.

The negated versions of the lookahead and lookbehind assertions are
denoted by \f(CW\*(C`(?!regexp)\*(C' and \f(CW\*(C`(?<!fixed-regexp)\*(C' respectively.
Or, starting in 5.32 (experimentally in 5.28), \f(CW\*(C`(*nla:regexp)\*(C',
\f(CW\*(C`(*negative_lookahead:regexp)\*(C', \f(CW\*(C`(*nlb:regexp)\*(C', or
\f(CW\*(C`(*negative_lookbehind:regexp)\*(C'.
They evaluate true if the regexps do *not* match:

.Vb 4
    $x = "foobar";
    $x =~ /foo(?!bar)/;  # doesn\*(Aqt match, \*(Aqbar\*(Aq follows \*(Aqfoo\*(Aq
    $x =~ /foo(?!baz)/;  # matches, \*(Aqbaz\*(Aq doesn\*(Aqt follow \*(Aqfoo\*(Aq
    $x =~ /(?<!\\s)foo/;  # matches, there is no \\s before \*(Aqfoo\*(Aq
.Ve

Here is an example where a string containing blank-separated words,
numbers and single dashes is to be split into its components.
Using \f(CW\*(C`/\\s+/\*(C' alone won't work, because spaces are not required between
dashes, or a word or a dash. Additional places for a split are established
by looking ahead and behind:

.Vb 5
    $str = "one two - --6-8";
    @toks = split / \\s+              # a run of spaces
                  | (?<=\\S) (?=-)    # any non-space followed by \*(Aq-\*(Aq
                  | (?<=-)  (?=\\S)   # a \*(Aq-\*(Aq followed by any non-space
                  /x, $str;          # @toks = qw(one two - - - 6 - 8)
.Ve

### Using independent subexpressions to prevent backtracking

Subsection "Using independent subexpressions to prevent backtracking"
*Independent subexpressions* (or atomic subexpressions) are regular
expressions, in the context of a larger regular expression, that
function independently of the larger regular expression.  That is, they
consume as much or as little of the string as they wish without regard
for the ability of the larger regexp to match.  Independent
subexpressions are represented by
\f(CW\*(C`(?>regexp)\*(C' or (starting in 5.32, experimentally in 5.28)
\f(CW\*(C`(*atomic:regexp)\*(C'.  We can illustrate their behavior by first
considering an ordinary regexp:

.Vb 2
    $x = "ab";
    $x =~ /a*ab/;  # matches
.Ve

This obviously matches, but in the process of matching, the
subexpression \f(CW\*(C`a*\*(C' first grabbed the \f(CW\*(Aqa\*(Aq.  Doing so, however,
wouldn't allow the whole regexp to match, so after backtracking, \f(CW\*(C`a*\*(C'
eventually gave back the \f(CW\*(Aqa\*(Aq and matched the empty string.  Here, what
\f(CW\*(C`a*\*(C' matched was *dependent* on what the rest of the regexp matched.

Contrast that with an independent subexpression:

.Vb 1
    $x =~ /(?>a*)ab/;  # doesn\*(Aqt match!
.Ve

The independent subexpression \f(CW\*(C`(?>a*)\*(C' doesn't care about the rest
of the regexp, so it sees an \f(CW\*(Aqa\*(Aq and grabs it.  Then the rest of the
regexp \f(CW\*(C`ab\*(C' cannot match.  Because \f(CW\*(C`(?>a*)\*(C' is independent, there
is no backtracking and the independent subexpression does not give
up its \f(CW\*(Aqa\*(Aq.  Thus the match of the regexp as a whole fails.  A similar
behavior occurs with completely independent regexps:

.Vb 3
    $x = "ab";
    $x =~ /a*/g;   # matches, eats an \*(Aqa\*(Aq
    $x =~ /\\Gab/g; # doesn\*(Aqt match, no \*(Aqa\*(Aq available
.Ve

Here \f(CW\*(C`/g\*(C' and \f(CW\*(C`\\G\*(C' create a \*(L"tag team\*(R" handoff of the string from
one regexp to the other.  Regexps with an independent subexpression are
much like this, with a handoff of the string to the independent
subexpression, and a handoff of the string back to the enclosing
regexp.

The ability of an independent subexpression to prevent backtracking
can be quite useful.  Suppose we want to match a non-empty string
enclosed in parentheses up to two levels deep.  Then the following
regexp matches:

.Vb 2
    $x = "abc(de(fg)h";  # unbalanced parentheses
    $x =~ /\\( ( [ ^ () ]+ | \\( [ ^ () ]* \\) )+ \\)/xx;
.Ve

The regexp matches an open parenthesis, one or more copies of an
alternation, and a close parenthesis.  The alternation is two-way, with
the first alternative \f(CW\*(C`[^()]+\*(C' matching a substring with no
parentheses and the second alternative \f(CW\*(C`\\([^()]*\\)\*(C'  matching a
substring delimited by parentheses.  The problem with this regexp is
that it is pathological: it has nested indeterminate quantifiers
of the form \f(CW\*(C`(a+|b)+\*(C'.  We discussed in Part 1 how nested quantifiers
like this could take an exponentially long time to execute if no match
were possible.  To prevent the exponential blowup, we need to prevent
useless backtracking at some point.  This can be done by enclosing the
inner quantifier as an independent subexpression:

.Vb 1
    $x =~ /\\( ( (?> [ ^ () ]+ ) | \\([ ^ () ]* \\) )+ \\)/xx;
.Ve

Here, \f(CW\*(C`(?>[^()]+)\*(C' breaks the degeneracy of string partitioning
by gobbling up as much of the string as possible and keeping it.   Then
match failures fail much more quickly.

### Conditional expressions

Subsection "Conditional expressions"
A *conditional expression* is a form of if-then-else statement
that allows one to choose which patterns are to be matched, based on
some condition.  There are two types of conditional expression:
\f(CW\*(C`(?(\f(CIcondition\f(CW)\f(CIyes-regexp\f(CW)\*(C' and
\f(CW\*(C`(?(condition)\f(CIyes-regexp\f(CW|\f(CIno-regexp\f(CW)\*(C'.
\f(CW\*(C`(?(\f(CIcondition\f(CW)\f(CIyes-regexp\f(CW)\*(C' is
like an \f(CW\*(Aqif\ ()\ \{\}\*(Aq statement in Perl.  If the *condition* is true,
the *yes-regexp* will be matched.  If the *condition* is false, the
*yes-regexp* will be skipped and Perl will move onto the next regexp
element.  The second form is like an \f(CW\*(Aqif\ ()\ \{\}\ else\ \{\}\*(Aq statement
in Perl.  If the *condition* is true, the *yes-regexp* will be
matched, otherwise the *no-regexp* will be matched.

The *condition* can have several forms.  The first form is simply an
integer in parentheses \f(CW\*(C`(\f(CIinteger\f(CW)\*(C'.  It is true if the corresponding
backreference \f(CW\*(C`\\\f(CIinteger\f(CW\*(C' matched earlier in the regexp.  The same
thing can be done with a name associated with a capture group, written
as \f(CW\*(C`(<\f(CIname\f(CW>)\*(C' or \f(CW\*(C`(\*(Aq\f(CIname\f(CW\*(Aq)\*(C'.  The second form is a bare
zero-width assertion \f(CW\*(C`(?...)\*(C', either a lookahead, a lookbehind, or a
code assertion (discussed in the next section).  The third set of forms
provides tests that return true if the expression is executed within
a recursion (\f(CW\*(C`(R)\*(C') or is being called from some capturing group,
referenced either by number (\f(CW\*(C`(R1)\*(C', \f(CW\*(C`(R2)\*(C',...) or by name
(\f(CW\*(C`(R&\f(CIname\f(CW)\*(C').

The integer or name form of the \f(CW\*(C`condition\*(C' allows us to choose,
with more flexibility, what to match based on what matched earlier in the
regexp. This searches for words of the form \f(CW"$x$x" or \f(CW"$x$y$y$x":

.Vb 9
    % simple_grep \*(Aq^(\\w+)(\\w+)?(?(2)\\g2\\g1|\\g1)$\*(Aq /usr/dict/words
    beriberi
    coco
    couscous
    deed
    ...
    toot
    toto
    tutu
.Ve

The lookbehind \f(CW\*(C`condition\*(C' allows, along with backreferences,
an earlier part of the match to influence a later part of the
match.  For instance,

.Vb 1
    /[ATGC]+(?(?<=AA)G|C)$/;
.Ve

matches a \s-1DNA\s0 sequence such that it either ends in \f(CW\*(C`AAG\*(C', or some
other base pair combination and \f(CW\*(AqC\*(Aq.  Note that the form is
\f(CW\*(C`(?(?<=AA)G|C)\*(C' and not \f(CW\*(C`(?((?<=AA))G|C)\*(C'; for the
lookahead, lookbehind or code assertions, the parentheses around the
conditional are not needed.

### Defining named patterns

Subsection "Defining named patterns"
Some regular expressions use identical subpatterns in several places.
Starting with Perl 5.10, it is possible to define named subpatterns in
a section of the pattern so that they can be called up by name
anywhere in the pattern.  This syntactic pattern for this definition
group is \f(CW\*(C`(?(DEFINE)(?<\f(CIname\f(CW>\f(CIpattern\f(CW)...)\*(C'.  An insertion
of a named pattern is written as \f(CW\*(C`(?&\f(CIname\f(CW)\*(C'.

The example below illustrates this feature using the pattern for
floating point numbers that was presented earlier on.  The three
subpatterns that are used more than once are the optional sign, the
digit sequence for an integer and the decimal fraction.  The \f(CW\*(C`DEFINE\*(C'
group at the end of the pattern contains their definition.  Notice
that the decimal fraction pattern is the first place where we can
reuse the integer pattern.

.Vb 8
   /^ (?&osg)\\ * ( (?&int)(?&dec)? | (?&dec) )
      (?: [eE](?&osg)(?&int) )?
    $
    (?(DEFINE)
      (?<osg>[-+]?)         # optional sign
      (?<int>\\d++)          # integer
      (?<dec>\\.(?&int))     # decimal fraction
    )/x
.Ve

### Recursive patterns

Subsection "Recursive patterns"
This feature (introduced in Perl 5.10) significantly extends the
power of Perl's pattern matching.  By referring to some other
capture group anywhere in the pattern with the construct
\f(CW\*(C`(?\f(CIgroup-ref\f(CW)\*(C', the *pattern* within the referenced group is used
as an independent subpattern in place of the group reference itself.
Because the group reference may be contained *within* the group it
refers to, it is now possible to apply pattern matching to tasks that
hitherto required a recursive parser.

To illustrate this feature, we'll design a pattern that matches if
a string contains a palindrome. (This is a word or a sentence that,
while ignoring spaces, interpunctuation and case, reads the same backwards
as forwards. We begin by observing that the empty string or a string
containing just one word character is a palindrome. Otherwise it must
have a word character up front and the same at its end, with another
palindrome in between.

.Vb 1
 /(?: (\\w) (?...Here be a palindrome...) \\g\{ -1 \} | \\w? )/x
.Ve

Adding \f(CW\*(C`\\W*\*(C' at either end to eliminate what is to be ignored, we already
have the full pattern:

.Vb 4
    my $pp = qr/^(\\W* (?: (\\w) (?1) \\g\{-1\} | \\w? ) \\W*)$/ix;
    for $s ( "saippuakauppias", "A man, a plan, a canal: Panama!" )\{
        print "\*(Aq$s\*(Aq is a palindrome\\n" if $s =~ /$pp/;
    \}
.Ve

In \f(CW\*(C`(?...)\*(C' both absolute and relative backreferences may be used.
The entire pattern can be reinserted with \f(CW\*(C`(?R)\*(C' or \f(CW\*(C`(?0)\*(C'.
If you prefer to name your groups, you can use \f(CW\*(C`(?&\f(CIname\f(CW)\*(C' to
recurse into that group.

### A bit of magic: executing Perl code in a regular expression

Subsection "A bit of magic: executing Perl code in a regular expression"
Normally, regexps are a part of Perl expressions.
*Code evaluation* expressions turn that around by allowing
arbitrary Perl code to be a part of a regexp.  A code evaluation
expression is denoted \f(CW\*(C`(?\{\f(CIcode\f(CW\})\*(C', with *code* a string of Perl
statements.

Code expressions are zero-width assertions, and the value they return
depends on their environment.  There are two possibilities: either the
code expression is used as a conditional in a conditional expression
\f(CW\*(C`(?(\f(CIcondition\f(CW)...)\*(C', or it is not.  If the code expression is a
conditional, the code is evaluated and the result (*i.e.*, the result of
the last statement) is used to determine truth or falsehood.  If the
code expression is not used as a conditional, the assertion always
evaluates true and the result is put into the special variable
\f(CW$^R.  The variable \f(CW$^R can then be used in code expressions later
in the regexp.  Here are some silly examples:

.Vb 5
    $x = "abcdef";
    $x =~ /abc(?\{print "Hi Mom!";\})def/; # matches,
                                         # prints \*(AqHi Mom!\*(Aq
    $x =~ /aaa(?\{print "Hi Mom!";\})def/; # doesn\*(Aqt match,
                                         # no \*(AqHi Mom!\*(Aq
.Ve

Pay careful attention to the next example:

.Vb 3
    $x =~ /abc(?\{print "Hi Mom!";\})ddd/; # doesn\*(Aqt match,
                                         # no \*(AqHi Mom!\*(Aq
                                         # but why not?
.Ve

At first glance, you'd think that it shouldn't print, because obviously
the \f(CW\*(C`ddd\*(C' isn't going to match the target string. But look at this
example:

.Vb 2
    $x =~ /abc(?\{print "Hi Mom!";\})[dD]dd/; # doesn\*(Aqt match,
                                            # but _does_ print
.Ve

Hmm. What happened here? If you've been following along, you know that
the above pattern should be effectively (almost) the same as the last one;
enclosing the \f(CW\*(Aqd\*(Aq in a character class isn't going to change what it
matches. So why does the first not print while the second one does?

The answer lies in the optimizations the regexp engine makes. In the first
case, all the engine sees are plain old characters (aside from the
\f(CW\*(C`?\{\}\*(C' construct). It's smart enough to realize that the string \f(CW\*(Aqddd\*(Aq
doesn't occur in our target string before actually running the pattern
through. But in the second case, we've tricked it into thinking that our
pattern is more complicated. It takes a look, sees our
character class, and decides that it will have to actually run the
pattern to determine whether or not it matches, and in the process of
running it hits the print statement before it discovers that we don't
have a match.

To take a closer look at how the engine does optimizations, see the
section \*(L"Pragmas and debugging\*(R" below.

More fun with \f(CW\*(C`?\{\}\*(C':

.Vb 6
    $x =~ /(?\{print "Hi Mom!";\})/;         # matches,
                                           # prints \*(AqHi Mom!\*(Aq
    $x =~ /(?\{$c = 1;\})(?\{print "$c";\})/;  # matches,
                                           # prints \*(Aq1\*(Aq
    $x =~ /(?\{$c = 1;\})(?\{print "$^R";\})/; # matches,
                                           # prints \*(Aq1\*(Aq
.Ve

The bit of magic mentioned in the section title occurs when the regexp
backtracks in the process of searching for a match.  If the regexp
backtracks over a code expression and if the variables used within are
localized using \f(CW\*(C`local\*(C', the changes in the variables produced by the
code expression are undone! Thus, if we wanted to count how many times
a character got matched inside a group, we could use, *e.g.*,

.Vb 11
    $x = "aaaa";
    $count = 0;  # initialize \*(Aqa\*(Aq count
    $c = "bob";  # test if $c gets clobbered
    $x =~ /(?\{local $c = 0;\})         # initialize count
           ( a                        # match \*(Aqa\*(Aq
             (?\{local $c = $c + 1;\})  # increment count
           )*                         # do this any number of times,
           aa                         # but match \*(Aqaa\*(Aq at the end
           (?\{$count = $c;\})          # copy local $c var into $count
          /x;
    print "\*(Aqa\*(Aq count is $count, \\$c variable is \*(Aq$c\*(Aq\\n";
.Ve

This prints

.Vb 1
    \*(Aqa\*(Aq count is 2, $c variable is \*(Aqbob\*(Aq
.Ve

If we replace the \f(CW\*(C`\ (?\{local\ $c\ =\ $c\ +\ 1;\})\*(C' with
\f(CW\*(C`\ (?\{$c\ =\ $c\ +\ 1;\})\*(C', the variable changes are *not* undone
during backtracking, and we get

.Vb 1
    \*(Aqa\*(Aq count is 4, $c variable is \*(Aqbob\*(Aq
.Ve

Note that only localized variable changes are undone.  Other side
effects of code expression execution are permanent.  Thus

.Vb 2
    $x = "aaaa";
    $x =~ /(a(?\{print "Yow\\n";\}))*aa/;
.Ve

produces

.Vb 4
   Yow
   Yow
   Yow
   Yow
.Ve

The result \f(CW$^R is automatically localized, so that it will behave
properly in the presence of backtracking.

This example uses a code expression in a conditional to match a
definite article, either \f(CW\*(Aqthe\*(Aq in English or \f(CW\*(Aqder|die|das\*(Aq in
German:

.Vb 11
    $lang = \*(AqDE\*(Aq;  # use German
    ...
    $text = "das";
    print "matched\\n"
        if $text =~ /(?(?\{
                          $lang eq \*(AqEN\*(Aq; # is the language English?
                         \})
                       the |             # if so, then match \*(Aqthe\*(Aq
                       (der|die|das)     # else, match \*(Aqder|die|das\*(Aq
                     )
                    /xi;
.Ve

Note that the syntax here is \f(CW\*(C`(?(?\{...\})\f(CIyes-regexp\f(CW|\f(CIno-regexp\f(CW)\*(C', not
\f(CW\*(C`(?((?\{...\}))\f(CIyes-regexp\f(CW|\f(CIno-regexp\f(CW)\*(C'.  In other words, in the case of a
code expression, we don't need the extra parentheses around the
conditional.

If you try to use code expressions where the code text is contained within
an interpolated variable, rather than appearing literally in the pattern,
Perl may surprise you:

.Vb 5
    $bar = 5;
    $pat = \*(Aq(?\{ 1 \})\*(Aq;
    /foo(?\{ $bar \})bar/; # compiles ok, $bar not interpolated
    /foo(?\{ 1 \})$bar/;   # compiles ok, $bar interpolated
    /foo$\{pat\}bar/;      # compile error!

    $pat = qr/(?\{ $foo = 1 \})/;  # precompile code regexp
    /foo$\{pat\}bar/;      # compiles ok
.Ve

If a regexp has a variable that interpolates a code expression, Perl
treats the regexp as an error. If the code expression is precompiled into
a variable, however, interpolating is ok. The question is, why is this an
error?

The reason is that variable interpolation and code expressions
together pose a security risk.  The combination is dangerous because
many programmers who write search engines often take user input and
plug it directly into a regexp:

.Vb 3
    $regexp = <>;       # read user-supplied regexp
    $chomp $regexp;     # get rid of possible newline
    $text =~ /$regexp/; # search $text for the $regexp
.Ve

If the \f(CW$regexp variable contains a code expression, the user could
then execute arbitrary Perl code.  For instance, some joker could
search for \f(CW\*(C`system(\*(Aqrm\ -rf\ *\*(Aq);\*(C' to erase your files.  In this
sense, the combination of interpolation and code expressions *taints*
your regexp.  So by default, using both interpolation and code
expressions in the same regexp is not allowed.  If you're not
concerned about malicious users, it is possible to bypass this
security check by invoking \f(CW\*(C`use\ re\ \*(Aqeval\*(Aq\*(C':

.Vb 4
    use re \*(Aqeval\*(Aq;       # throw caution out the door
    $bar = 5;
    $pat = \*(Aq(?\{ 1 \})\*(Aq;
    /foo$\{pat\}bar/;      # compiles ok
.Ve

Another form of code expression is the *pattern code expression*.
The pattern code expression is like a regular code expression, except
that the result of the code evaluation is treated as a regular
expression and matched immediately.  A simple example is

.Vb 4
    $length = 5;
    $char = \*(Aqa\*(Aq;
    $x = \*(Aqaaaaabb\*(Aq;
    $x =~ /(??\{$char x $length\})/x; # matches, there are 5 of \*(Aqa\*(Aq
.Ve

This final example contains both ordinary and pattern code
expressions.  It detects whether a binary string \f(CW1101010010001... has a
Fibonacci spacing 0,1,1,2,3,5,...  of the \f(CW\*(Aq1\*(Aq's:

.Vb 12
    $x = "1101010010001000001";
    $z0 = \*(Aq\*(Aq; $z1 = \*(Aq0\*(Aq;   # initial conditions
    print "It is a Fibonacci sequence\\n"
        if $x =~ /^1         # match an initial \*(Aq1\*(Aq
                    (?:
                       ((??\{ $z0 \})) # match some \*(Aq0\*(Aq
                       1             # and then a \*(Aq1\*(Aq
                       (?\{ $z0 = $z1; $z1 .= $^N; \})
                    )+   # repeat as needed
                  $      # that is all there is
                 /x;
    printf "Largest sequence matched was %d\\n", length($z1)-length($z0);
.Ve

Remember that \f(CW$^N is set to whatever was matched by the last
completed capture group. This prints

.Vb 2
    It is a Fibonacci sequence
    Largest sequence matched was 5
.Ve

Ha! Try that with your garden variety regexp package...

Note that the variables \f(CW$z0 and \f(CW$z1 are not substituted when the
regexp is compiled, as happens for ordinary variables outside a code
expression.  Rather, the whole code block is parsed as perl code at the
same time as perl is compiling the code containing the literal regexp
pattern.

This regexp without the \f(CW\*(C`/x\*(C' modifier is

.Vb 1
    /^1(?:((??\{ $z0 \}))1(?\{ $z0 = $z1; $z1 .= $^N; \}))+$/
.Ve

which shows that spaces are still possible in the code parts. Nevertheless,
when working with code and conditional expressions, the extended form of
regexps is almost necessary in creating and debugging regexps.

### Backtracking control verbs

Subsection "Backtracking control verbs"
Perl 5.10 introduced a number of control verbs intended to provide
detailed control over the backtracking process, by directly influencing
the regexp engine and by providing monitoring techniques.  See
\*(L"Special Backtracking Control Verbs\*(R" in perlre for a detailed
description.

Below is just one example, illustrating the control verb \f(CW\*(C`(*FAIL)\*(C',
which may be abbreviated as \f(CW\*(C`(*F)\*(C'. If this is inserted in a regexp
it will cause it to fail, just as it would at some
mismatch between the pattern and the string. Processing
of the regexp continues as it would after any \*(L"normal\*(R"
failure, so that, for instance, the next position in the string or another
alternative will be tried. As failing to match doesn't preserve capture
groups or produce results, it may be necessary to use this in
combination with embedded code.

.Vb 4
   %count = ();
   "supercalifragilisticexpialidocious" =~
       /([aeiou])(?\{ $count\{$1\}++; \})(*FAIL)/i;
   printf "%3d \*(Aq%s\*(Aq\\n", $count\{$_\}, $_ for (sort keys %count);
.Ve

The pattern begins with a class matching a subset of letters.  Whenever
this matches, a statement like \f(CW\*(C`$count\{\*(Aqa\*(Aq\}++;\*(C' is executed, incrementing
the letter's counter. Then \f(CW\*(C`(*FAIL)\*(C' does what it says, and
the regexp engine proceeds according to the book: as long as the end of
the string hasn't been reached, the position is advanced before looking
for another vowel. Thus, match or no match makes no difference, and the
regexp engine proceeds until the entire string has been inspected.
(It's remarkable that an alternative solution using something like

.Vb 2
   $count\{lc($_)\}++ for split(\*(Aq\*(Aq, "supercalifragilisticexpialidocious");
   printf "%3d \*(Aq%s\*(Aq\\n", $count2\{$_\}, $_ for ( qw\{ a e i o u \} );
.Ve

is considerably slower.)

### Pragmas and debugging

Subsection "Pragmas and debugging"
Speaking of debugging, there are several pragmas available to control
and debug regexps in Perl.  We have already encountered one pragma in
the previous section, \f(CW\*(C`use\ re\ \*(Aqeval\*(Aq;\*(C', that allows variable
interpolation and code expressions to coexist in a regexp.  The other
pragmas are

.Vb 3
    use re \*(Aqtaint\*(Aq;
    $tainted = <>;
    @parts = ($tainted =~ /(\\w+)\\s+(\\w+)/; # @parts is now tainted
.Ve

The \f(CW\*(C`taint\*(C' pragma causes any substrings from a match with a tainted
variable to be tainted as well.  This is not normally the case, as
regexps are often used to extract the safe bits from a tainted
variable.  Use \f(CW\*(C`taint\*(C' when you are not extracting safe bits, but are
performing some other processing.  Both \f(CW\*(C`taint\*(C' and \f(CW\*(C`eval\*(C' pragmas
are lexically scoped, which means they are in effect only until
the end of the block enclosing the pragmas.

.Vb 2
    use re \*(Aq/m\*(Aq;  # or any other flags
    $multiline_string =~ /^foo/; # /m is implied
.Ve

The \f(CW\*(C`re \*(Aq/flags\*(Aq\*(C' pragma (introduced in Perl
5.14) turns on the given regular expression flags
until the end of the lexical scope.  See
\*(L"'/flags' mode\*(R" in re for more
detail.

.Vb 2
    use re \*(Aqdebug\*(Aq;
    /^(.*)$/s;       # output debugging info

    use re \*(Aqdebugcolor\*(Aq;
    /^(.*)$/s;       # output debugging info in living color
.Ve

The global \f(CW\*(C`debug\*(C' and \f(CW\*(C`debugcolor\*(C' pragmas allow one to get
detailed debugging info about regexp compilation and
execution.  \f(CW\*(C`debugcolor\*(C' is the same as debug, except the debugging
information is displayed in color on terminals that can display
termcap color sequences.  Here is example output:

.Vb 10
    % perl -e \*(Aquse re "debug"; "abc" =~ /a*b+c/;\*(Aq
    Compiling REx \*(Aqa*b+c\*(Aq
    size 9 first at 1
       1: STAR(4)
       2:   EXACT <a>(0)
       4: PLUS(7)
       5:   EXACT <b>(0)
       7: EXACT <c>(9)
       9: END(0)
    floating \*(Aqbc\*(Aq at 0..2147483647 (checking floating) minlen 2
    Guessing start of match, REx \*(Aqa*b+c\*(Aq against \*(Aqabc\*(Aq...
    Found floating substr \*(Aqbc\*(Aq at offset 1...
    Guessed: match at offset 0
    Matching REx \*(Aqa*b+c\*(Aq against \*(Aqabc\*(Aq
      Setting an EVAL scope, savestack=3
       0 <> <abc>           |  1:  STAR
                             EXACT <a> can match 1 times out of 32767...
      Setting an EVAL scope, savestack=3
       1 <a> <bc>           |  4:    PLUS
                             EXACT <b> can match 1 times out of 32767...
      Setting an EVAL scope, savestack=3
       2 <ab> <c>           |  7:      EXACT <c>
       3 <abc> <>           |  9:      END
    Match successful!
    Freeing REx: \*(Aqa*b+c\*(Aq
.Ve

If you have gotten this far into the tutorial, you can probably guess
what the different parts of the debugging output tell you.  The first
part

.Vb 8
    Compiling REx \*(Aqa*b+c\*(Aq
    size 9 first at 1
       1: STAR(4)
       2:   EXACT <a>(0)
       4: PLUS(7)
       5:   EXACT <b>(0)
       7: EXACT <c>(9)
       9: END(0)
.Ve

describes the compilation stage.  \f(CWSTAR(4) means that there is a
starred object, in this case \f(CW\*(Aqa\*(Aq, and if it matches, goto line 4,
*i.e.*, \f(CWPLUS(7).  The middle lines describe some heuristics and
optimizations performed before a match:

.Vb 4
    floating \*(Aqbc\*(Aq at 0..2147483647 (checking floating) minlen 2
    Guessing start of match, REx \*(Aqa*b+c\*(Aq against \*(Aqabc\*(Aq...
    Found floating substr \*(Aqbc\*(Aq at offset 1...
    Guessed: match at offset 0
.Ve

Then the match is executed and the remaining lines describe the
process:

.Vb 12
    Matching REx \*(Aqa*b+c\*(Aq against \*(Aqabc\*(Aq
      Setting an EVAL scope, savestack=3
       0 <> <abc>           |  1:  STAR
                             EXACT <a> can match 1 times out of 32767...
      Setting an EVAL scope, savestack=3
       1 <a> <bc>           |  4:    PLUS
                             EXACT <b> can match 1 times out of 32767...
      Setting an EVAL scope, savestack=3
       2 <ab> <c>           |  7:      EXACT <c>
       3 <abc> <>           |  9:      END
    Match successful!
    Freeing REx: \*(Aqa*b+c\*(Aq
.Ve

Each step is of the form \f(CW\*(C`n\ <x>\ <y>\*(C', with \f(CW\*(C`<x>\*(C' the
part of the string matched and \f(CW\*(C`<y>\*(C' the part not yet
matched.  The \f(CW\*(C`|\ \ 1:\ \ STAR\*(C' says that Perl is at line number 1
in the compilation list above.  See
\*(L"Debugging Regular Expressions\*(R" in perldebguts for much more detail.

An alternative method of debugging regexps is to embed \f(CW\*(C`print\*(C'
statements within the regexp.  This provides a blow-by-blow account of
the backtracking in an alternation:

.Vb 12
    "that this" =~ m@(?\{print "Start at position ", pos, "\\n";\})
                     t(?\{print "t1\\n";\})
                     h(?\{print "h1\\n";\})
                     i(?\{print "i1\\n";\})
                     s(?\{print "s1\\n";\})
                         |
                     t(?\{print "t2\\n";\})
                     h(?\{print "h2\\n";\})
                     a(?\{print "a2\\n";\})
                     t(?\{print "t2\\n";\})
                     (?\{print "Done at position ", pos, "\\n";\})
                    @x;
.Ve

prints

.Vb 8
    Start at position 0
    t1
    h1
    t2
    h2
    a2
    t2
    Done at position 4
.Ve

## SEE ALSO

Header "SEE ALSO"
This is just a tutorial.  For the full story on Perl regular
expressions, see the perlre regular expressions reference page.

For more information on the matching \f(CW\*(C`m//\*(C' and substitution \f(CW\*(C`s///\*(C'
operators, see \*(L"Regexp Quote-Like Operators\*(R" in perlop.  For
information on the \f(CW\*(C`split\*(C' operation, see \*(L"split\*(R" in perlfunc.

For an excellent all-around resource on the care and feeding of
regular expressions, see the book *Mastering Regular Expressions* by
Jeffrey Friedl (published by O'Reilly, \s-1ISBN 1556592-257-3\s0).

## AUTHOR AND COPYRIGHT

Header "AUTHOR AND COPYRIGHT"
Copyright (c) 2000 Mark Kvale.
All rights reserved.
Now maintained by Perl porters.

This document may be distributed under the same terms as Perl itself.

### Acknowledgments

Subsection "Acknowledgments"
The inspiration for the stop codon \s-1DNA\s0 example came from the \s-1ZIP\s0
code example in chapter 7 of *Mastering Regular Expressions*.

The author would like to thank Jeff Pinyan, Andrew Johnson, Peter
Haworth, Ronald J Kimball, and Joe Smith for all their helpful
comments.
