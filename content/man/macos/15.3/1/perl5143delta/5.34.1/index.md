+++
title = "perl5143delta(1)"
detected_package_version = "5.34.1"
description = "This document describes differences between the 5.14.2 release and the 5.14.3 release. If you are upgrading from an earlier release such as 5.12.0, first read perl5140delta, which describes differences between 5.12.0 and 5.14.0. No changes since ..."
operating_system_version = "15.3"
manpage_section = "1"
manpage_name = "perl5143delta"
manpage_format = "troff"
operating_system = "macos"
author = "None Specified"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
date = "2022-02-19"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5143DELTA 1"
PERL5143DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5143delta - what is new for perl v5.14.3

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.14.2 release and
the 5.14.3 release.

If you are upgrading from an earlier release such as 5.12.0, first read
perl5140delta, which describes differences between 5.12.0 and
5.14.0.

## Core Enhancements

Header "Core Enhancements"
No changes since 5.14.0.

## Security

Header "Security"
.ie n .SS """Digest"" unsafe use of eval (\s-1CVE-2011-3597\s0)"
.el .SS "\f(CWDigest unsafe use of eval (\s-1CVE-2011-3597\s0)"
Subsection "Digest unsafe use of eval (CVE-2011-3597)"
The \f(CW\*(C`Digest->new()\*(C' function did not properly sanitize input before
using it in an **eval()** call, which could lead to the injection of arbitrary
Perl code.

In order to exploit this flaw, the attacker would need to be able to set
the algorithm name used, or be able to execute arbitrary Perl code already.

This problem has been fixed.

### Heap buffer overrun in x string repeat operator (\s-1CVE-2012-5195\s0)

Subsection "Heap buffer overrun in 'x' string repeat operator (CVE-2012-5195)"
Poorly written perl code that allows an attacker to specify the count to
perl's 'x' string repeat operator can already cause a memory exhaustion
denial-of-service attack. A flaw in versions of perl before 5.15.5 can
escalate that into a heap buffer overrun; coupled with versions of glibc
before 2.16, it possibly allows the execution of arbitrary code.

This problem has been fixed.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.14.0. If any
exist, they are bugs and reports are welcome.

## Deprecations

Header "Deprecations"
There have been no deprecations since 5.14.0.

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules and Pragmata

Subsection "New Modules and Pragmata"
None

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
PerlIO::scalar was updated to fix a bug in which opening a filehandle to
a glob copy caused assertion failures (under debugging) or hangs or other
erratic behaviour without debugging.

- \(bu
ODBM_File and NDBM_File were updated to allow building on GNU/Hurd.

- \(bu
IPC::Open3 has been updated to fix a regression introduced in perl
5.12, which broke \f(CW\*(C`IPC::Open3::open3($in, $out, $err, \*(Aq-\*(Aq)\*(C'.
[perl #95748]

- \(bu
Digest has been upgraded from version 1.16 to 1.16_01.
.Sp
See \*(L"Security\*(R".

- \(bu
Module::CoreList has been updated to version 2.49_04 to add data for
this release.

### Removed Modules and Pragmata

Subsection "Removed Modules and Pragmata"
None

## Documentation

Header "Documentation"

### New Documentation

Subsection "New Documentation"
None

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perlcheat*
Subsection "perlcheat"

- \(bu
perlcheat was updated to 5.14.

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
h2ph was updated to search correctly gcc include directories on platforms
such as Debian with multi-architecture support.

- \(bu
In Configure, the test for procselfexe was refactored into a loop.

## Platform Support

Header "Platform Support"

### New Platforms

Subsection "New Platforms"
None

### Discontinued Platforms

Subsection "Discontinued Platforms"
None

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- FreeBSD
Item "FreeBSD"
The FreeBSD hints file was corrected to be compatible with FreeBSD 10.0.

- Solaris and NetBSD
Item "Solaris and NetBSD"
Configure was updated for \*(L"procselfexe\*(R" support on Solaris and NetBSD.

- HP-UX
Item "HP-UX"
\s-1README\s0.hpux was updated to note the existence of a broken header in
HP-UX 11.00.

- Linux
Item "Linux"
libutil is no longer used when compiling on Linux platforms, which avoids
warnings being emitted.
.Sp
The system gcc (rather than any other gcc which might be in the compiling
user's path) is now used when searching for libraries such as \f(CW\*(C`-lm\*(C'.

- Mac \s-1OS X\s0
Item "Mac OS X"
The locale tests were updated to reflect the behaviour of locales in
Mountain Lion.

- GNU/Hurd
Item "GNU/Hurd"
Various build and test fixes were included for GNU/Hurd.
.Sp
\s-1LFS\s0 support was enabled in GNU/Hurd.

- NetBSD
Item "NetBSD"
The NetBSD hints file was corrected to be compatible with NetBSD 6.*

## Bug Fixes

Header "Bug Fixes"

- \(bu
A regression has been fixed that was introduced in 5.14, in \f(CW\*(C`/i\*(C'
regular expression matching, in which a match improperly fails if the
pattern is in \s-1UTF-8,\s0 the target string is not, and a Latin-1 character
precedes a character in the string that should match the pattern.  [perl
#101710]

- \(bu
In case-insensitive regular expression pattern matching, no longer on
\s-1UTF-8\s0 encoded strings does the scan for the start of match only look at
the first possible position.  This caused matches such as
\f(CW\*(C`"f\\x\{FB00\}" =~ /ff/i\*(C' to fail.

- \(bu
The sitecustomize support was made relocatableinc aware, so that
-Dusesitecustomize and -Duserelocatableinc may be used together.

- \(bu
The smartmatch operator (\f(CW\*(C`~~\*(C') was changed so that the right-hand side
takes precedence during \f(CW\*(C`Any ~~ Object\*(C' operations.

- \(bu
A bug has been fixed in the tainting support, in which an \f(CW\*(C`index()\*(C'
operation on a tainted constant would cause all other constants to become
tainted.  [perl #64804]

- \(bu
A regression has been fixed that was introduced in perl 5.12, whereby
tainting errors were not correctly propagated through \f(CW\*(C`die()\*(C'.
[perl #111654]

- \(bu
A regression has been fixed that was introduced in perl 5.14, in which
\f(CW\*(C`/[[:lower:]]/i\*(C' and \f(CW\*(C`/[[:upper:]]/i\*(C' no longer matched the opposite case.
[perl #101970]

## Acknowledgements

Header "Acknowledgements"
Perl 5.14.3 represents approximately 12 months of development since Perl 5.14.2
and contains approximately 2,300 lines of changes across 64 files from 22
authors.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers. The following people are known to have contributed the
improvements that became Perl 5.14.3:

Abigail, Andy Dougherty, Carl Hayter, Chris 'BinGOs' Williams, Dave Rolsky,
David Mitchell, Dominic Hargreaves, Father Chrysostomos, Florian Ragwitz,
H.Merijn Brand, Jilles Tjoelker, Karl Williamson, Leon Timmermans, Michael G
Schwern, Nicholas Clark, Niko Tyni, Pino Toscano, Ricardo Signes, Salvador
Fandin\*~o, Samuel Thibault, Steve Hay, Tony Cook.

The list above is almost certainly incomplete as it is automatically generated
from version control history. In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core. We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes all the core committers, who be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for
security issues in the Perl core, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
