+++
manpage_format = "troff"
operating_system = "macos"
operating_system_version = "15.3"
keywords = ["ppdc", "1", "ppdhtml", "ppdmerge", "ppdpo", "ppdcfile", "5", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
manpage_name = "ppdi"
date = "Sun Feb 16 04:48:19 2025"
author = "None Specified"
title = "ppdi(1)"
description = "ppdi imports one or more PPD files into a PPD compiler source file. Multiple languages of the same PPD file are merged into a single printer definition to facilitate accurate changes for all localizations. This program is deprecated and will be remo..."
manpage_section = "1"
detected_package_version = "2.0"
+++

ppdi 1 "CUPS" "26 April 2019" "Apple Inc."

## NAME

ppdi - import ppd files (deprecated)

## SYNOPSIS

ppdi
[
-I
include-directory
] [
-o
source-file
]
ppd-file
[ ...
ppd-file
]

## DESCRIPTION

**ppdi** imports one or more PPD files into a PPD compiler source file.
Multiple languages of the same PPD file are merged into a single printer definition to facilitate accurate changes for all localizations.
**This program is deprecated and will be removed in a future release of CUPS.**

## OPTIONS

**ppdi** supports the following options:

**-I **include-directory
Specifies an alternate include directory.
Multiple *-I* options can be supplied to add additional directories.

**-o **source-file
Specifies the PPD source file to update.
If the source file does not exist, a new source file is created.
Otherwise the existing file is merged with the new PPD file(s) on the command-line.
If no source file is specified, the filename *ppdi.drv* is used.

## NOTES

PPD files are deprecated and will no longer be supported in a future feature release of CUPS.
Printers that do not support IPP can be supported using applications such as
ippeveprinter (1).

## SEE ALSO

ppdc (1),
ppdhtml (1),
ppdmerge (1),
ppdpo (1),
ppdcfile (5),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
