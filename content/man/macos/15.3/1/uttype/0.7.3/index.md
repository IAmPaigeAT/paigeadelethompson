+++
keywords = ["lsregister", "history", "first", "appeared", "in", "macos", "11"]
description = "This command can be used to query the system programmatically about Uniform Type Identifiers (UTTypes for short) known to it. List all declared UTTypes uttype --all List all UTTypes that claim the filename extension xyz and which conform to publ..."
manpage_name = "uttype"
manpage_section = "1"
date = "Sun Feb 16 04:48:22 2025"
author = "None Specified"
operating_system_version = "15.3"
title = "uttype(1)"
operating_system = "macos"
manpage_format = "troff"
detected_package_version = "0.7.3"
+++

.
"UTTYPE" "1" "August 2020" "macOS" "BSD General Commands Manual"
.

## NAME

**uttype** - Information about Uniform Type Identifiers
.

## SYNOPSIS

**uttype** [**flags**] [**options**] [identifier1 [identifier2 [\.\.\.]]]
.

## DESCRIPTION

This command can be used to query the system programmatically about Uniform Type Identifiers (*UTTypes* for short) known to it\.
.

## EXAMPLE

.

List all declared UTTypes
**uttype** --all
.

List all UTTypes that claim the filename extension "xyz" and which conform to "public\.data"
**uttype** --extension \'xyz\' --conformsto \'public\.data\'
.

List extended information about the UTType "com\.apple\.package"
**uttype** --verbose "com\.apple\.package"
.

List all supported flags and options
**uttype** --help
.

## SEE ALSO

lsregister(1)
.

## HISTORY

First appeared in macOS 11\.
