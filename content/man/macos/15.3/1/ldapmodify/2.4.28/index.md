+++
date = "2011-01-01"
operating_system = "macos"
manpage_format = "troff"
manpage_section = "1"
operating_system_version = "15.3"
detected_package_version = "2.4.28"
keywords = ["ldapadd", "1", "ldapdelete", "ldapmodrdn", "ldapsearch", "ldap", "conf", "5", "3", "ldap_add_ext", "ldap_delete_ext", "ldap_modify_ext", "ldap_modrdn_ext", "ldif", "slapd", "replog", "author", "the", "openldap", "project", "http", "www", "org", "acknowledgements", "shared", "acknowledgement", "text", "software", "is", "developed", "and", "maintained", "by", "derived", "from", "university", "of", "michigan", "release"]
description = "is a shell-accessible interface to the and library calls. is implemented as a hard link to the ldapmodify tool.  When invoked as the -a (add new entry) flag is turned on automatically. opens a connection to an LDAP server, binds, and modifies or ..."
author = "The OpenLDAP Foundation All Rights Reserved."
manpage_name = "ldapmodify"
title = "ldapmodify(1)"
+++

LDAPMODIFY 1 "2011/11/24" "OpenLDAP 2.4.28"

## NAME

ldapmodify, ldapadd - LDAP modify entry and LDAP add entry tools

## SYNOPSIS

ldapmodify
[\c
-a ]
[\c
-c ]
[\c
-S \ file]
[\c
-n ]
[\c
-v ]
[\c
-M [ M ]]
[\c
-d \ debuglevel]
[\c
-D \ binddn]
[\c
-W ]
[\c
-w \ passwd]
[\c
-y \ passwdfile]
[\c
-H \ ldapuri]
[\c
-h \ ldaphost]
[\c
-p \ ldapport]
[\c
-P \ \{ 2 \||\| 3 \}]
[\c
-e \ [ ! ] *ext* [ =*extparam* ]]
[\c
-E \ [ ! ] *ext* [ =*extparam* ]]
[\c
-O \ security-properties]
[\c
-I ]
[\c
-Q ]
[\c
-U \ authcid]
[\c
-R \ realm]
[\c
-x ]
[\c
-X \ authzid]
[\c
-Y \ mech]
[\c
-Z [ Z ]]
[\c
-f \ file]

ldapadd
[\c
-c ]
[\c
-S \ file]
[\c
-n ]
[\c
-v ]
[\c
-M [ M ]]
[\c
-d \ debuglevel]
[\c
-D \ binddn]
[\c
-W ]
[\c
-w \ passwd]
[\c
-y \ passwdfile]
[\c
-H \ ldapuri]
[\c
-h \ ldaphost]
[\c
-p \ ldapport]
[\c
-P \ \{ 2 \||\| 3 \}]
[\c
-O \ security-properties]
[\c
-I ]
[\c
-Q ]
[\c
-U \ authcid]
[\c
-R \ realm]
[\c
-x ]
[\c
-X \ authzid]
[\c
-Y \ mech]
[\c
-Z [ Z ]]
[\c
-f \ file]

## DESCRIPTION

ldapmodify
is a shell-accessible interface to the
ldap_add_ext (3),
ldap_modify_ext (3),
ldap_delete_ext (3)
and
ldap_rename (3).
library calls.
ldapadd
is implemented as a hard link to the ldapmodify tool.  When invoked as
ldapadd
the **-a** (add new entry) flag is turned on automatically.

ldapmodify
opens a connection to an LDAP server, binds, and modifies or adds entries.
The entry information is read from standard input or from *file* through
the use of the **-f** option.

## OPTIONS


-a
Add new entries.  The default for
ldapmodify
is to modify existing entries.  If invoked as
ldapadd ,
this flag is always set.

-c
Continuous operation mode.  Errors are reported, but
ldapmodify
will continue with modifications.  The default is to exit after
reporting an error.

-S \ file
Add or change records which were skipped due to an error are written to *file*
and the error message returned by the server is added as a comment. Most useful in
conjunction with **-c**.

-n
Show what would be done, but don't actually modify entries.  Useful for
debugging in conjunction with **-v**.

-v
Use verbose mode, with many diagnostics written to standard output.

-M [ M ]
Enable manage DSA IT control.
-MM
makes control critical.

-d \ debuglevel
Set the LDAP debugging level to *debuglevel*.
ldapmodify
must be compiled with LDAP_DEBUG defined for this option to have any effect.

-f \ file
Read the entry modification information from *file* instead of from
standard input.

-x
Use simple authentication instead of SASL.

-D \ binddn
Use the Distinguished Name *binddn* to bind to the LDAP directory.
For SASL binds, the server is expected to ignore this value.

-W
Prompt for simple authentication.
This is used instead of specifying the password on the command line.

-w \ passwd
Use *passwd* as the password for simple authentication.

-y \ passwdfile
Use complete contents of *passwdfile* as the password for
simple authentication.

-H \ ldapuri
Specify URI(s) referring to the ldap server(s); only the protocol/host/port
fields are allowed; a list of URI, separated by whitespace or commas
is expected.

-h \ ldaphost
Specify an alternate host on which the ldap server is running.
Deprecated in favor of **-H**.

-p \ ldapport
Specify an alternate TCP port where the ldap server is listening.
Deprecated in favor of **-H**.

-P \ \{ 2 \||\| 3 \}
Specify the LDAP protocol version to use.

-O \ security-properties
Specify SASL security properties.

-e \ [ ! ] *ext* [ =*extparam* ]

-E \ [ ! ] *ext* [ =*extparam* ]

Specify general extensions with **-e** and search extensions with **-E**.
\'**!**\' indicates criticality.

General extensions:

```
  [!]assert=<filter>   (an RFC 4515 Filter)
  [!]authzid=<authzid> ("dn:<dn>" or "u:<user>")
  [!]manageDSAit
  [!]noop
  ppolicy
  [!]postread[=<attrs>]        (a comma-separated attribute list)
  [!]preread[=<attrs>] (a comma-separated attribute list)
  abandon, cancel (SIGINT sends abandon/cancel; not really controls)
```


Search extensions:

```
  [!]domainScope                               (domain scope)
  [!]mv=<filter>                               (matched values filter)
  [!]pr=<size>[/prompt|noprompt]       (paged results/prompt)
  [!]sss=[\-]<attr[:OID]>[/[\-]<attr[:OID]>...]  (server side sorting)
  [!]subentries[=true|false]           (subentries)
  [!]sync=ro[/<cookie>]                        (LDAP Sync refreshOnly)
          rp[/<cookie>][/<slimit>]     (LDAP Sync refreshAndPersist)
```


-I
Enable SASL Interactive mode.  Always prompt.  Default is to prompt
only as needed.

-Q
Enable SASL Quiet mode.  Never prompt.

-U \ authcid
Specify the authentication ID for SASL bind. The form of the ID
depends on the actual SASL mechanism used.

-R \ realm
Specify the realm of authentication ID for SASL bind. The form of the realm
depends on the actual SASL mechanism used.

-X \ authzid
Specify the requested authorization ID for SASL bind.
authzid
must be one of the following formats:
dn: "<distinguished name>"
or
u: <username>

-Y \ mech
Specify the SASL mechanism to be used for authentication. If it's not
specified, the program will choose the best mechanism the server knows.

-Z [ Z ]
Issue StartTLS (Transport Layer Security) extended operation. If you use
-ZZ\c
, the command will require the operation to be successful.

## INPUT FORMAT

The contents of *file* (or standard input if no **-f** flag is given on
the command line) must conform to the format defined in
ldif (5)
(LDIF as defined in RFC 2849).

## EXAMPLES

Assuming that the file
/tmp/entrymods
exists and has the contents:


```
    dn: cn=Modify Me,dc=example,dc=com
    changetype: modify
    replace: mail
    mail: modme@example.com
    \-
    add: title
    title: Grand Poobah
    \-
    add: jpegPhoto
    jpegPhoto:< file:///tmp/modme.jpeg
    \-
    delete: description
    \-
```


the command:


```
    ldapmodify \-f /tmp/entrymods
```


will replace the contents of the "Modify Me" entry's
mail
attribute with the value "modme@example.com", add a
title
of "Grand Poobah", and the contents of the file "/tmp/modme.jpeg"
as a
jpegPhoto ,
and completely remove the
description
attribute.

Assuming that the file
/tmp/newentry
exists and has the contents:


```
    dn: cn=Barbara Jensen,dc=example,dc=com
    objectClass: person
    cn: Barbara Jensen
    cn: Babs Jensen
    sn: Jensen
    title: the world's most famous mythical manager
    mail: bjensen@example.com
    uid: bjensen
```


the command:


```
    ldapadd \-f /tmp/newentry
```


will add a new entry for Babs Jensen, using the values from the
file
/tmp/newentry.

Assuming that the file
/tmp/entrymods
exists and has the contents:


```
    dn: cn=Barbara Jensen,dc=example,dc=com
    changetype: delete
```


the command:


```
    ldapmodify \-f /tmp/entrymods
```


will remove Babs Jensen's entry.

## DIAGNOSTICS

Exit status is zero if no errors occur.  Errors result in a non-zero
exit status and a diagnostic message being written to standard error.

## SEE ALSO

ldapadd (1),
ldapdelete (1),
ldapmodrdn (1),
ldapsearch (1),
ldap.conf (5),
ldap (3),
ldap_add_ext (3),
ldap_delete_ext (3),
ldap_modify_ext (3),
ldap_modrdn_ext (3),
ldif (5),
slapd.replog (5)

## AUTHOR

The OpenLDAP Project <http://www.openldap.org/>

## ACKNOWLEDGEMENTS

"OpenLDAP Software"
is developed and maintained by The OpenLDAP Project <http://www.openldap.org/>.
"OpenLDAP Software"
is derived from University of Michigan LDAP 3.3 Release.
