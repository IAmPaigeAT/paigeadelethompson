+++
description = "and  are used to invoke the or the program on bzip2 compressed files.  All options specified are passed directly to or If only 1 file is specified, then the files compared are and an uncompressed If two files are specified, then they are uncom..."
title = "bzdiff(1)"
date = "Sun Feb 16 04:48:19 2025"
operating_system_version = "15.3"
detected_package_version = "0"
manpage_name = "bzdiff"
manpage_format = "troff"
operating_system = "macos"
author = "None Specified"
keywords = ["cmp", "diff", "bzmore", "bzless", "bzgrep", "bzip2", "bugs", "messages", "from", "the", "or", "programs", "refer", "to", "temporary", "filenames", "instead", "of", "those", "specified"]
manpage_section = "1"
+++

\"Shamelessly copied from zmore.1 by Philippe Troin <phil@fifi.org>
\"for Debian GNU/Linux
BZDIFF 1

## NAME

bzcmp, bzdiff - compare bzip2 compressed files

## SYNOPSIS

bzcmp
[ cmp_options ] file1
[ file2 ]
.br
bzdiff
[ diff_options ] file1
[ file2 ]

## DESCRIPTION

Bzcmp
and
bzdiff
are used to invoke the
cmp
or the
diff
program on bzip2 compressed files.  All options specified are passed
directly to
cmp
or
diff "."
If only 1 file is specified, then the files compared are
file1
and an uncompressed
file1 ".bz2."
If two files are specified, then they are uncompressed if necessary and fed to
cmp
or
diff "."
The exit status from
cmp
or
diff
is preserved.

## SEE ALSO

cmp(1), diff(1), bzmore(1), bzless(1), bzgrep(1), bzip2(1)

## BUGS

Messages from the
cmp
or
diff
programs refer to temporary filenames instead of those specified.
