+++
date = "Sun Feb 16 04:48:22 2025"
title = "xcode-select(1)"
manpage_section = "1"
detected_package_version = "0"
manpage_name = "xcode-select"
description = "xcode-select controls the location of the developer directory used by xcrun(1), xcodebuild(1), cc(1), and other Xcode and BSD development tools. This also controls the locations that are searched for by man(1) for developer tool manpages. This all..."
manpage_format = "troff"
operating_system = "macos"
operating_system_version = "15.3"
author = "None Specified"
keywords = ["fbxcrun", "fbxcodebuild", "history", "the", "fbxcode-select", "command", "first", "appeared", "in", "xcode", "3", "generated", "by", "docutils", "manpage", "writer"]
+++


## SYNOPSIS


**xcode-select** [-h|--help] [-s|--switch <path>] [-p|--print-path]
[-v|--version]

## DESCRIPTION


**xcode-select** controls the location of the developer directory used by
**xcrun(1)**, **xcodebuild(1)**, **cc(1)**, and other Xcode
and BSD development tools. This also controls the locations that are searched
for by **man(1)** for developer tool manpages.

This allows you to easily switch between different versions of the Xcode tools
and can be used to update the path to the Xcode if it is moved after
installation.

### Usage


When multiple Xcode applications are installed on a system
(e.g. **/Applications/Xcode.app**, containing the latest Xcode, and
**/Applications/Xcode-beta.app** containing a beta) use \fBxcode-select
--switch path/to/Xcode.app to specify the Xcode that you wish to use for
command line developer tools.

After setting a developer directory, all of the **xcode-select** provided
developer tool shims (see *\%FILES*) will automatically invoke the version of
the tool inside the selected developer directory. Your own scripts, makefiles,
and other tools can also use **xcrun(1)** to easily lookup tools inside
the active developer directory, making it easy to switch them between different
versions of the Xcode tools and allowing them to function properly on systems
where the Xcode application has been installed to a non-default location.

## OPTIONS

0.0

-h, --help
Prints the usage message.
.UNINDENT
0.0

-s <path>, --switch <path>
Sets the active developer directory to the given path, for example
**/Applications/Xcode-beta.app**. This command must be run with superuser
permissions (see **sudo(8)**), and will affect all users on the
system. To set the path without superuser permissions or only for the current
shell session, use the DEVELOPER_DIR environment variable instead (see
*\%ENVIRONMENT*).
.UNINDENT
0.0

-p, --print-path
Prints the path to the currently selected developer directory. This is useful
for inspection, but scripts and other tools should use **xcrun(1)** to
locate tool inside the active developer directory.
.UNINDENT
0.0

-r, --reset
Unsets any user-specified developer directory, so that the developer directory
will be found via the default search mechanism. This command must be run with
superuser permissions (see **sudo(8)**), and will affect all users on
the system.
.UNINDENT
0.0

-v, --version
Prints **xcode-select** version information.
.UNINDENT
0.0

--install
Opens a user interface dialog to request automatic installation of the command
line developer tools.
.UNINDENT

## ENVIRONMENT


DEVELOPER_DIR
0.0
3.5
Overrides the active developer directory. When DEVELOPER_DIR is set, its value
will be used instead of the system-wide active developer directory.

Note that for historical reason, the developer directory is considered to be
the Developer content directory inside the Xcode application (for example
**/Applications/Xcode.app/Contents/Developer**). You can set the environment
variable to either the actual Developer contents directory, or the Xcode
application directory -- the **xcode-select** provided shims will
automatically convert the environment variable into the full Developer content
path.
.UNINDENT
.UNINDENT

## EXAMPLES


**xcode-select --switch /Applications/Xcode.app/Contents/Developer**
0.0
3.5
Select **/Applications/Xcode.app/Contents/Developer** as the active developer
directory.
.UNINDENT
.UNINDENT

**xcode-select --switch /Applications/Xcode.app**
0.0
3.5
As above, selects **/Applications/Xcode.app/Contents/Developer** as the active
developer directory. The Developer content directory is automatically inferred
by **xcode-select**.
.UNINDENT
.UNINDENT

**/usr/bin/xcodebuild**
0.0
3.5
Runs **xcodebuild** out of the active developer directory.
.UNINDENT
.UNINDENT

**/usr/bin/xcrun --find xcodebuild**
0.0
3.5
Use **xcrun** to locate **xcodebuild** inside the active
developer directory.
.UNINDENT
.UNINDENT

**env DEVELOPER_DIR="/Applications/Xcode-beta.app" /usr/bin/xcodebuild**
0.0
3.5
Execute **xcodebuild** using an alternate developer directory.
.UNINDENT
.UNINDENT

## FILES


**/usr/bin/xcrun**
0.0
3.5
Used to find or run arbitrary commands from the active developer directory. See
**xcrun(1)** for more information.
.UNINDENT
.UNINDENT

**/usr/bin/actool**
**/usr/bin/agvtool**
**/usr/bin/desdp**
**/usr/bin/genstrings**
**/usr/bin/ibtool**
**/usr/bin/ictool**
**/usr/bin/opendiff**
**/usr/bin/pip3**
**/usr/bin/python3**
**/usr/bin/sdef**
**/usr/bin/sdp**
**/usr/bin/stapler**
**/usr/bin/xcodebuild**
**/usr/bin/xcscontrol**
**/usr/bin/xcsdiagnose**
**/usr/bin/xctrace**
**/usr/bin/xed**
0.0
3.5
Runs the matching Xcode tool from with the active developer directory.
.UNINDENT
.UNINDENT

**/usr/bin/DeRez**
**/usr/bin/GetFileInfo**
**/usr/bin/ResMerger**
**/usr/bin/Rez**
**/usr/bin/SetFile**
**/usr/bin/SplitForks**
**/usr/bin/ar**
**/usr/bin/as**
**/usr/bin/asa**
**/usr/bin/bm4**
**/usr/bin/bison**
**/usr/bin/c89**
**/usr/bin/c99**
**/usr/bin/clang++**
**/usr/bin/clang**
**/usr/bin/clangd**
**/usr/bin/cmpdylib**
**/usr/bin/codesign_allocate**
**/usr/bin/cpp**
**/usr/bin/ctags**
**/usr/bin/ctf_insert**
**/usr/bin/dsymutil**
**/usr/bin/dwarfdump**
**/usr/bin/dyld_info**
**/usr/bin/flex++**
**/usr/bin/flex**
**/usr/bin/g++**
**/usr/bin/gatherheaderdoc**
**/usr/bin/gcc**
**/usr/bin/gcov**
**/usr/bin/git-receive-pack**
**/usr/bin/git-shell**
**/usr/bin/git-upload-archive**
**/usr/bin/git-upload-pack**
**/usr/bin/git**
**/usr/bin/gm4**
**/usr/bin/gnumake**
**/usr/bin/gperf**
**/usr/bin/hdxml2manxml**
**/usr/bin/headerdoc2html**
**/usr/bin/indent**
**/usr/bin/install_name_tool**
**/usr/bin/ld**
**/usr/bin/lex**
**/usr/bin/libtool**
**/usr/bin/lipo**
**/usr/bin/lldb**
**/usr/bin/lorder**
**/usr/bin/m4**
**/usr/bin/make**
**/usr/bin/mig**
**/usr/bin/nm**
**/usr/bin/nmedit**
**/usr/bin/objdump**
**/usr/bin/otool**
**/usr/bin/pagestuff**
**/usr/bin/ranlib**
**/usr/bin/resolveLinks**
**/usr/bin/rpcgen**
**/usr/bin/segedit**
**/usr/bin/size**
**/usr/bin/strings**
**/usr/bin/strip**
**/usr/bin/swift**
**/usr/bin/swiftc**
**/usr/bin/unifdef**
**/usr/bin/unifdefall**
**/usr/bin/vtool**
**/usr/bin/xml2man**
**/usr/bin/yacc**
0.0
3.5
Runs the matching BSD tool from with the active developer directory.
.UNINDENT
.UNINDENT

## SEE ALSO


**xcrun(1)**, **xcodebuild(1)**

## HISTORY


The **xcode-select** command first appeared in Xcode 3.0.
.
