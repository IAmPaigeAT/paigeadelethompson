+++
operating_system = "macos"
manpage_format = "troff"
title = "pod2html(1)"
operating_system_version = "15.3"
author = "None Specified"
manpage_name = "pod2html"
description = "Converts files from pod format (see perlpod) to s-1HTMLs0 format. pod2html takes the following arguments:   --help Displays the usage message.   --htmldir=name Sets the directory to which all cross references in the resulting s-1HTMLs0 file will..."
manpage_section = "1"
detected_package_version = "5.34.1"
date = "2024-12-14"
keywords = ["header", "see", "also", "perlpod", "pod", "html", "copyright", "this", "program", "is", "distributed", "under", "the", "artistic", "license"]
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "POD2HTML 1"
POD2HTML 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

pod2html - convert .pod files to .html files

## SYNOPSIS

Header "SYNOPSIS"
.Vb 8
    pod2html --help --htmldir=<name> --htmlroot=<URL>
             --infile=<name> --outfile=<name>
             --podpath=<name>:...:<name> --podroot=<name>
             --cachedir=<name> --flush --recurse --norecurse
             --quiet --noquiet --verbose --noverbose
             --index --noindex --backlink --nobacklink
             --header --noheader --poderrors --nopoderrors
             --css=<URL> --title=<name>
.Ve

## DESCRIPTION

Header "DESCRIPTION"
Converts files from pod format (see perlpod) to \s-1HTML\s0 format.

## ARGUMENTS

Header "ARGUMENTS"
pod2html takes the following arguments:

- help
Item "help"
.Vb 1
  --help
.Ve
.Sp
Displays the usage message.

- htmldir
Item "htmldir"
.Vb 1
  --htmldir=name
.Ve
.Sp
Sets the directory to which all cross references in the resulting \s-1HTML\s0 file
will be relative. Not passing this causes all links to be absolute since this
is the value that tells Pod::Html the root of the documentation tree.
.Sp
Do not use this and --htmlroot in the same call to pod2html; they are mutually
exclusive.

- htmlroot
Item "htmlroot"
.Vb 1
  --htmlroot=URL
.Ve
.Sp
Sets the base \s-1URL\s0 for the \s-1HTML\s0 files.  When cross-references are made, the
\s-1HTML\s0 root is prepended to the \s-1URL.\s0
.Sp
Do not use this if relative links are desired: use --htmldir instead.
.Sp
Do not pass both this and --htmldir to pod2html; they are mutually exclusive.

- infile
Item "infile"
.Vb 1
  --infile=name
.Ve
.Sp
Specify the pod file to convert.  Input is taken from \s-1STDIN\s0 if no
infile is specified.

- outfile
Item "outfile"
.Vb 1
  --outfile=name
.Ve
.Sp
Specify the \s-1HTML\s0 file to create.  Output goes to \s-1STDOUT\s0 if no outfile
is specified.

- podroot
Item "podroot"
.Vb 1
  --podroot=name
.Ve
.Sp
Specify the base directory for finding library pods.

- podpath
Item "podpath"
.Vb 1
  --podpath=name:...:name
.Ve
.Sp
Specify which subdirectories of the podroot contain pod files whose
\s-1HTML\s0 converted forms can be linked-to in cross-references.

- cachedir
Item "cachedir"
.Vb 1
  --cachedir=name
.Ve
.Sp
Specify which directory is used for storing cache. Default directory is the
current working directory.

- flush
Item "flush"
.Vb 1
  --flush
.Ve
.Sp
Flush the cache.

- backlink
Item "backlink"
.Vb 1
  --backlink
.Ve
.Sp
Turn =head1 directives into links pointing to the top of the \s-1HTML\s0 file.

- nobacklink
Item "nobacklink"
.Vb 1
  --nobacklink
.Ve
.Sp
Do not turn =head1 directives into links pointing to the top of the \s-1HTML\s0 file
(default behaviour).

- header
Item "header"
.Vb 1
  --header
.Ve
.Sp
Create header and footer blocks containing the text of the \*(L"\s-1NAME\*(R"\s0 section.

- noheader
Item "noheader"
.Vb 1
  --noheader
.Ve
.Sp
Do not create header and footer blocks containing the text of the \*(L"\s-1NAME\*(R"\s0
section (default behaviour).

- poderrors
Item "poderrors"
.Vb 1
  --poderrors
.Ve
.Sp
Include a \*(L"\s-1POD ERRORS\*(R"\s0 section in the outfile if there were any \s-1POD\s0 errors in
the infile (default behaviour).

- nopoderrors
Item "nopoderrors"
.Vb 1
  --nopoderrors
.Ve
.Sp
Do not include a \*(L"\s-1POD ERRORS\*(R"\s0 section in the outfile if there were any \s-1POD\s0
errors in the infile.

- index
Item "index"
.Vb 1
  --index
.Ve
.Sp
Generate an index at the top of the \s-1HTML\s0 file (default behaviour).

- noindex
Item "noindex"
.Vb 1
  --noindex
.Ve
.Sp
Do not generate an index at the top of the \s-1HTML\s0 file.

- recurse
Item "recurse"
.Vb 1
  --recurse
.Ve
.Sp
Recurse into subdirectories specified in podpath (default behaviour).

- norecurse
Item "norecurse"
.Vb 1
  --norecurse
.Ve
.Sp
Do not recurse into subdirectories specified in podpath.

- css
Item "css"
.Vb 1
  --css=URL
.Ve
.Sp
Specify the \s-1URL\s0 of cascading style sheet to link from resulting \s-1HTML\s0 file.
Default is none style sheet.

- title
Item "title"
.Vb 1
  --title=title
.Ve
.Sp
Specify the title of the resulting \s-1HTML\s0 file.

- quiet
Item "quiet"
.Vb 1
  --quiet
.Ve
.Sp
Don't display mostly harmless warning messages.

- noquiet
Item "noquiet"
.Vb 1
  --noquiet
.Ve
.Sp
Display mostly harmless warning messages (default behaviour). But this is not
the same as \*(L"verbose\*(R" mode.

- verbose
Item "verbose"
.Vb 1
  --verbose
.Ve
.Sp
Display progress messages.

- noverbose
Item "noverbose"
.Vb 1
  --noverbose
.Ve
.Sp
Do not display progress messages (default behaviour).

## AUTHOR

Header "AUTHOR"
Tom Christiansen, <tchrist@perl.com>.

## BUGS

Header "BUGS"
See Pod::Html for a list of known bugs in the translator.

## SEE ALSO

Header "SEE ALSO"
perlpod, Pod::Html

## COPYRIGHT

Header "COPYRIGHT"
This program is distributed under the Artistic License.
