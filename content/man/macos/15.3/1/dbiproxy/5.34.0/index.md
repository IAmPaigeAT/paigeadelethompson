+++
title = "dbiproxy(1)"
operating_system = "macos"
manpage_format = "troff"
manpage_name = "dbiproxy"
date = "2024-12-14"
manpage_section = "1"
detected_package_version = "5.34.0"
author = "None Specified"
keywords = ["header", "see", "also", "dbi", "proxyserver", "dbd", "proxy", "s-1dbi", "s0"]
description = "This tool is just a front end for the DBI::ProxyServer package. All it does is picking options from the command line and calling DBI::ProxyServer::main(). See DBI::ProxyServer for details. Available options include: (s-1UNIXs0 only)  After doing a..."
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "DBIPROXY 1"
DBIPROXY 1 "2024-12-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

dbiproxy - A proxy server for the DBD::Proxy driver

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
    dbiproxy <options> --localport=<port>
.Ve

## DESCRIPTION

Header "DESCRIPTION"
This tool is just a front end for the DBI::ProxyServer package. All it
does is picking options from the command line and calling
**DBI::ProxyServer::main()**. See DBI::ProxyServer for details.

Available options include:

- \fB--chroot=dir
Item "--chroot=dir"
(\s-1UNIX\s0 only)  After doing a **bind()**, change root directory to the given
directory by doing a **chroot()**. This is useful for security, but it
restricts the environment a lot. For example, you need to load \s-1DBI\s0
drivers in the config file or you have to create hard links to Unix
sockets, if your drivers are using them. For example, with MySQL, a
config file might contain the following lines:
.Sp
.Vb 9
    my $rootdir = \*(Aq/var/dbiproxy\*(Aq;
    my $unixsockdir = \*(Aq/tmp\*(Aq;
    my $unixsockfile = \*(Aqmysql.sock\*(Aq;
    foreach $dir ($rootdir, "$rootdir$unixsockdir") \{
        mkdir 0755, $dir;
    \}
    link("$unixsockdir/$unixsockfile",
         "$rootdir$unixsockdir/$unixsockfile");
    require DBD::mysql;

    \{
        \*(Aqchroot\*(Aq => $rootdir,
        ...
    \}
.Ve
.Sp
If you don't know **chroot()**, think of an \s-1FTP\s0 server where you can see a
certain directory tree only after logging in. See also the --group and
--user options.

- \fB--configfile=file
Item "--configfile=file"
Config files are assumed to return a single hash ref that overrides the
arguments of the new method. However, command line arguments in turn take
precedence over the config file. See the \*(L"\s-1CONFIGURATION FILE\*(R"\s0 section
in the DBI::ProxyServer documentation for details on the config file.

- \fB--debug
Item "--debug"
Turn debugging mode on. Mainly this asserts that logging messages of
level \*(L"debug\*(R" are created.

- \fB--facility=mode
Item "--facility=mode"
(\s-1UNIX\s0 only) Facility to use for Sys::Syslog. The default is
**daemon**.

- \fB--group=gid
Item "--group=gid"
After doing a **bind()**, change the real and effective \s-1GID\s0 to the given.
This is useful, if you want your server to bind to a privileged port
(<1024), but don't want the server to execute as root. See also
the --user option.
.Sp
\s-1GID\s0's can be passed as group names or numeric values.

- \fB--localaddr=ip
Item "--localaddr=ip"
By default a daemon is listening to any \s-1IP\s0 number that a machine
has. This attribute allows one to restrict the server to the given
\s-1IP\s0 number.

- \fB--localport=port
Item "--localport=port"
This attribute sets the port on which the daemon is listening. It
must be given somehow, as there's no default.

- \fB--logfile=file
Item "--logfile=file"
Be default logging messages will be written to the syslog (Unix) or
to the event log (Windows \s-1NT\s0). On other operating systems you need to
specify a log file. The special value \*(L"\s-1STDERR\*(R"\s0 forces logging to
stderr. See Net::Daemon::Log for details.

- \fB--mode=modename
Item "--mode=modename"
The server can run in three different modes, depending on the environment.
.Sp
If you are running Perl 5.005 and did compile it for threads, then the
server will create a new thread for each connection. The thread will
execute the server's **Run()** method and then terminate. This mode is the
default, you can force it with \*(L"--mode=threads\*(R".
.Sp
If threads are not available, but you have a working **fork()**, then the
server will behave similar by creating a new process for each connection.
This mode will be used automatically in the absence of threads or if
you use the \*(L"--mode=fork\*(R" option.
.Sp
Finally there's a single-connection mode: If the server has accepted a
connection, he will enter the **Run()** method. No other connections are
accepted until the **Run()** method returns (if the client disconnects).
This operation mode is useful if you have neither threads nor **fork()**,
for example on the Macintosh. For debugging purposes you can force this
mode with \*(L"--mode=single\*(R".

- \fB--pidfile=file
Item "--pidfile=file"
(\s-1UNIX\s0 only) If this option is present, a \s-1PID\s0 file will be created at the
given location. Default is to not create a pidfile.

- \fB--user=uid
Item "--user=uid"
After doing a **bind()**, change the real and effective \s-1UID\s0 to the given.
This is useful, if you want your server to bind to a privileged port
(<1024), but don't want the server to execute as root. See also
the --group and the --chroot options.
.Sp
\s-1UID\s0's can be passed as group names or numeric values.

- \fB--version
Item "--version"
Suppresses startup of the server; instead the version string will
be printed and the program exits immediately.

## AUTHOR

Header "AUTHOR"
.Vb 4
    Copyright (c) 1997    Jochen Wiedmann
                          Am Eisteich 9
                          72555 Metzingen
                          Germany

                          Email: joe@ispsoft.de
                          Phone: +49 7123 14881
.Ve

The DBI::ProxyServer module is free software; you can redistribute it
and/or modify it under the same terms as Perl itself. In particular
permission is granted to Tim Bunce for distributing this as a part of
the \s-1DBI.\s0

## SEE ALSO

Header "SEE ALSO"
DBI::ProxyServer, DBD::Proxy, \s-1DBI\s0
