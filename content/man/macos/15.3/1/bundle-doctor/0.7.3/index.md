+++
author = "None Specified"
description = "Checks your Gemfile and gem environment for common problems. If issues are detected, Bundler prints them and exits status 1. Otherwise, Bundler prints a success message and exits status 0. Examples of common problems caught by bundle-doctor include:..."
title = "bundle-doctor(1)"
operating_system = "macos"
date = "Sun Feb 16 04:48:24 2025"
manpage_format = "troff"
operating_system_version = "15.3"
detected_package_version = "0.7.3"
manpage_section = "1"
manpage_name = "bundle-doctor"
+++

.
"BUNDLE-DOCTOR" "1" "November 2018" "" ""
.

## NAME

**bundle-doctor** - Checks the bundle for common problems
.

## SYNOPSIS

**bundle doctor** [--quiet] [--gemfile=GEMFILE]
.

## DESCRIPTION

Checks your Gemfile and gem environment for common problems\. If issues are detected, Bundler prints them and exits status 1\. Otherwise, Bundler prints a success message and exits status 0\.
.
.P
Examples of common problems caught by bundle-doctor include:
.

- \(bu
Invalid Bundler settings
.

- \(bu
Mismatched Ruby versions
.

- \(bu
Mismatched platforms
.

- \(bu
Uninstalled gems
.

- \(bu
Missing dependencies
.
"" 0
.

## OPTIONS

.

**--quiet**
Only output warnings and errors\.
.

**--gemfile=<gemfile>**
The location of the Gemfile(5) which Bundler should use\. This defaults to a Gemfile(5) in the current working directory\. In general, Bundler will assume that the location of the Gemfile(5) is also the project\'s root and will try to find **Gemfile\.lock** and **vendor/cache** relative to this location\.

