+++
operating_system_version = "15.3"
date = "2006-01-01"
detected_package_version = "0"
operating_system = "macos"
manpage_name = "tkmib"
author = "None Specified"
description = "Simple Network Management Protocol (SNMP) provides a framework for exchange of the management information between the agents (servers) and clients. The Management Information Bases (MIBs) contain a formal description of a set of network objects tha..."
title = "tkmib(1)"
manpage_section = "1"
manpage_format = "troff"
+++

tkmib 1 "16 Nov 2006" V5.6.2.1 "Net-SNMP"

## NAME

tkmib - an interactive graphical MIB browser for SNMP

## SYNOPSIS


tkmib

## DESCRIPTION


Simple Network Management Protocol (SNMP) provides a framework for
exchange of the management information between the agents (servers)
and clients. The Management Information Bases (MIBs) contain a formal
description of a set of network objects that can be managed using the
SNMP for a particular agent. **tkmib** is a graphical user interface
for browsing the MIBs. It is also capable of sending or retrieving the
SNMP management information to/from the remote agents interactively.
