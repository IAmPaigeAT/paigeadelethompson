+++
date = "Sun Feb 16 04:48:20 2025"
author = "None Specified"
operating_system_version = "15.3"
operating_system = "macos"
detected_package_version = "0.7.3"
manpage_name = "bundle-list"
manpage_section = "1"
description = "Prints a list of all the gems in the bundle including their version. Example: bundle list --name-only bundle list --paths bundle list --without-group test bundle list --only-group dev bundle list --only-group dev --paths --name-only Print only..."
title = "bundle-list(1)"
manpage_format = "troff"
+++

.
"BUNDLE-LIST" "1" "November 2018" "" ""
.

## NAME

**bundle-list** - List all the gems in the bundle
.

## SYNOPSIS

**bundle list** [--name-only] [--paths] [--without-group=GROUP] [--only-group=GROUP]
.

## DESCRIPTION

Prints a list of all the gems in the bundle including their version\.
.
.P
Example:
.
.P
bundle list --name-only
.
.P
bundle list --paths
.
.P
bundle list --without-group test
.
.P
bundle list --only-group dev
.
.P
bundle list --only-group dev --paths
.

## OPTIONS

.

**--name-only**
Print only the name of each gem\.
.

**--paths**
Print the path to each gem in the bundle\.
.

**--without-group**
Print all gems expect from a group\.
.

**--only-group**
Print gems from a particular group\.

