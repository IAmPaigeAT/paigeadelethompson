+++
operating_system = "macos"
description = "You can think of a module as the fundamental unit of reusable Perl code; see perlmod for details.  Whenever anyone creates a chunk of Perl code that they think will be useful to the world, they register as a Perl developer at <https://www.cpan.org/..."
detected_package_version = "5.34.1"
date = "2022-02-19"
title = "perlmodinstall(1)"
author = "None Specified"
manpage_section = "1"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_name = "perlmodinstall"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLMODINSTALL 1"
PERLMODINSTALL 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlmodinstall - Installing CPAN Modules

## DESCRIPTION

Header "DESCRIPTION"
You can think of a module as the fundamental unit of reusable Perl
code; see perlmod for details.  Whenever anyone creates a chunk of
Perl code that they think will be useful to the world, they register
as a Perl developer at <https://www.cpan.org/modules/04pause.html>
so that they can then upload their code to the \s-1CPAN.\s0  The \s-1CPAN\s0 is the
Comprehensive Perl Archive Network and can be accessed at
<https://www.cpan.org/> , and searched at <https://metacpan.org/> .

This documentation is for people who want to download \s-1CPAN\s0 modules
and install them on their own computer.

### \s-1PREAMBLE\s0

Subsection "PREAMBLE"
First, are you sure that the module isn't already on your system?  Try
\f(CW\*(C`perl -MFoo -e 1\*(C'.  (Replace \*(L"Foo\*(R" with the name of the module; for
instance, \f(CW\*(C`perl -MCGI::Carp -e 1\*(C'.)

If you don't see an error message, you have the module.  (If you do
see an error message, it's still possible you have the module, but
that it's not in your path, which you can display with \f(CW\*(C`perl -e
"print qq(@INC)"\*(C'.)  For the remainder of this document, we'll assume
that you really honestly truly lack an installed module, but have
found it on the \s-1CPAN.\s0

So now you have a file ending in .tar.gz (or, less often, .zip).  You
know there's a tasty module inside.  There are four steps you must now
take:

- \fB\s-1DECOMPRESS\s0 the file
Item "DECOMPRESS the file"
0

- \fB\s-1UNPACK\s0 the file into a directory
Item "UNPACK the file into a directory"

- \fB\s-1BUILD\s0 the module (sometimes unnecessary)
Item "BUILD the module (sometimes unnecessary)"

- \fB\s-1INSTALL\s0 the module.
Item "INSTALL the module."
.PD

Here's how to perform each step for each operating system.  This is
<not> a substitute for reading the \s-1README\s0 and \s-1INSTALL\s0 files that
might have come with your module!

Also note that these instructions are tailored for installing the
module into your system's repository of Perl modules, but you can
install modules into any directory you wish.  For instance, where I
say \f(CW\*(C`perl Makefile.PL\*(C', you can substitute \f(CW\*(C`perl Makefile.PL
PREFIX=/my/perl_directory\*(C' to install the modules into
*/my/perl_directory*.  Then you can use the modules from your Perl
programs with \f(CW\*(C`use lib "/my/perl_directory/lib/site_perl";\*(C' or
sometimes just \f(CW\*(C`use "/my/perl_directory";\*(C'.  If you're on a system
that requires superuser/root access to install modules into the
directories you see when you type \f(CW\*(C`perl -e "print qq(@INC)"\*(C', you'll
want to install them into a local directory (such as your home
directory) and use this approach.

- \(bu
**If you're on a Unix or Unix-like system,**
.Sp
You can use Andreas Koenig's \s-1CPAN\s0 module
( <https://metacpan.org/release/CPAN> )
to automate the following steps, from \s-1DECOMPRESS\s0 through \s-1INSTALL.\s0
.Sp
A. \s-1DECOMPRESS\s0
.Sp
Decompress the file with \f(CW\*(C`gzip -d yourmodule.tar.gz\*(C'
.Sp
You can get gzip from <ftp://prep.ai.mit.edu/pub/gnu/>
.Sp
Or, you can combine this step with the next to save disk space:
.Sp
.Vb 1
     gzip -dc yourmodule.tar.gz | tar -xof -
.Ve
.Sp
B. \s-1UNPACK\s0
.Sp
Unpack the result with \f(CW\*(C`tar -xof yourmodule.tar\*(C'
.Sp
C. \s-1BUILD\s0
.Sp
Go into the newly-created directory and type:
.Sp
.Vb 2
      perl Makefile.PL
      make test
.Ve
.Sp
or
.Sp
.Vb 1
      perl Makefile.PL PREFIX=/my/perl_directory
.Ve
.Sp
to install it locally.  (Remember that if you do this, you'll have to
put \f(CW\*(C`use lib "/my/perl_directory";\*(C' near the top of the program that
is to use this module.
.Sp
D. \s-1INSTALL\s0
.Sp
While still in that directory, type:
.Sp
.Vb 1
      make install
.Ve
.Sp
Make sure you have the appropriate permissions to install the module
in your Perl 5 library directory.  Often, you'll need to be root.
.Sp
That's all you need to do on Unix systems with dynamic linking.
Most Unix systems have dynamic linking. If yours doesn't, or if for
another reason you have a statically-linked perl, **and** the
module requires compilation, you'll need to build a new Perl binary
that includes the module.  Again, you'll probably need to be root.

- \(bu
**If you're running ActivePerl (Win95/98/2K/NT/XP, Linux, Solaris),**
.Sp
First, type \f(CW\*(C`ppm\*(C' from a shell and see whether ActiveState's \s-1PPM\s0
repository has your module.  If so, you can install it with \f(CW\*(C`ppm\*(C' and
you won't have to bother with any of the other steps here.  You might
be able to use the \s-1CPAN\s0 instructions from the \*(L"Unix or Linux\*(R" section
above as well; give it a try.  Otherwise, you'll have to follow the
steps below.
.Sp
.Vb 1
   A. DECOMPRESS
.Ve
.Sp
You can use the
open source 7-zip ( <https://www.7-zip.org/> )
or the shareware Winzip ( <https://www.winzip.com> ) to
decompress and unpack modules.
.Sp
.Vb 1
   B. UNPACK
.Ve
.Sp
If you used WinZip, this was already done for you.
.Sp
.Vb 1
   C. BUILD
.Ve
.Sp
You'll need either \f(CW\*(C`nmake\*(C' or \f(CW\*(C`gmake\*(C'.
.Sp
Does the module require compilation (i.e. does it have files that end
in .xs, .c, .h, .y, .cc, .cxx, or .C)?  If it does, life is now
officially tough for you, because you have to compile the module
yourself (no easy feat on Windows).  You'll need a compiler such as
Visual \*(C+.  Alternatively, you can download a pre-built \s-1PPM\s0 package
from ActiveState.
<http://aspn.activestate.com/ASPN/Downloads/ActivePerl/PPM/>
.Sp
Go into the newly-created directory and type:
.Sp
.Vb 2
      perl Makefile.PL
      nmake test


   D. INSTALL
.Ve
.Sp
While still in that directory, type:
.Sp
.Vb 1
      nmake install
.Ve

- \(bu
**If you're on the \s-1DJGPP\s0 port of \s-1DOS,\s0**
.Sp
.Vb 1
   A. DECOMPRESS
.Ve
.Sp
djtarx ( <ftp://ftp.delorie.com/pub/djgpp/current/v2/> )
will both uncompress and unpack.
.Sp
.Vb 1
   B. UNPACK
.Ve
.Sp
See above.
.Sp
.Vb 1
   C. BUILD
.Ve
.Sp
Go into the newly-created directory and type:
.Sp
.Vb 2
      perl Makefile.PL
      make test
.Ve
.Sp
You will need the packages mentioned in *\s-1README\s0.dos*
in the Perl distribution.
.Sp
.Vb 1
   D. INSTALL
.Ve
.Sp
While still in that directory, type:
.Sp
.Vb 1
     make install
.Ve
.Sp
You will need the packages mentioned in *\s-1README\s0.dos* in the Perl distribution.

- \(bu
**If you're on \s-1OS/2,\s0**
.Sp
Get the \s-1EMX\s0 development suite and gzip/tar from Hobbes (
<http://hobbes.nmsu.edu/h-browse.php?dir=/pub/os2/dev/emx/v0.9d> ), and then follow
the instructions for Unix.

- \(bu
**If you're on \s-1VMS,\s0**
.Sp
When downloading from \s-1CPAN,\s0 save your file with a \f(CW\*(C`.tgz\*(C'
extension instead of \f(CW\*(C`.tar.gz\*(C'.  All other periods in the
filename should be replaced with underscores.  For example,
\f(CW\*(C`Your-Module-1.33.tar.gz\*(C' should be downloaded as
\f(CW\*(C`Your-Module-1_33.tgz\*(C'.
.Sp
A. \s-1DECOMPRESS\s0
.Sp
Type
.Sp
.Vb 1
    gzip -d Your-Module.tgz
.Ve
.Sp
or, for zipped modules, type
.Sp
.Vb 1
    unzip Your-Module.zip
.Ve
.Sp
Executables for gzip, zip, and VMStar:
.Sp
.Vb 1
    http://www.hp.com/go/openvms/freeware/
.Ve
.Sp
and their source code:
.Sp
.Vb 1
    http://www.fsf.org/order/ftp.html
.Ve
.Sp
Note that \s-1GNU\s0's gzip/gunzip is not the same as Info-ZIP's zip/unzip
package.  The former is a simple compression tool; the latter permits
creation of multi-file archives.
.Sp
B. \s-1UNPACK\s0
.Sp
If you're using VMStar:
.Sp
.Vb 1
     VMStar xf Your-Module.tar
.Ve
.Sp
Or, if you're fond of \s-1VMS\s0 command syntax:
.Sp
.Vb 1
     tar/extract/verbose Your_Module.tar
.Ve
.Sp
C. \s-1BUILD\s0
.Sp
Make sure you have \s-1MMS\s0 (from Digital) or the freeware \s-1MMK\s0 ( available
from MadGoat at <http://www.madgoat.com> ).  Then type this to create
the \s-1DESCRIP.MMS\s0 for the module:
.Sp
.Vb 1
    perl Makefile.PL
.Ve
.Sp
Now you're ready to build:
.Sp
.Vb 1
    mms test
.Ve
.Sp
Substitute \f(CW\*(C`mmk\*(C' for \f(CW\*(C`mms\*(C' above if you're using \s-1MMK.\s0
.Sp
D. \s-1INSTALL\s0
.Sp
Type
.Sp
.Vb 1
    mms install
.Ve
.Sp
Substitute \f(CW\*(C`mmk\*(C' for \f(CW\*(C`mms\*(C' above if you're using \s-1MMK.\s0

- \(bu
**If you're on \s-1MVS\s0**,
.Sp
Introduce the *.tar.gz* file into an \s-1HFS\s0 as binary; don't translate from
\s-1ASCII\s0 to \s-1EBCDIC.\s0
.Sp
A. \s-1DECOMPRESS\s0
.Sp
Decompress the file with \f(CW\*(C`gzip -d yourmodule.tar.gz\*(C'
.Sp
You can get gzip from
<http://www.s390.ibm.com/products/oe/bpxqp1.html>
.Sp
B. \s-1UNPACK\s0
.Sp
Unpack the result with
.Sp
.Vb 1
     pax -o to=IBM-1047,from=ISO8859-1 -r < yourmodule.tar
.Ve
.Sp
The \s-1BUILD\s0 and \s-1INSTALL\s0 steps are identical to those for Unix.  Some
modules generate Makefiles that work better with \s-1GNU\s0 make, which is
available from <http://www.mks.com/s390/gnu/>

## PORTABILITY

Header "PORTABILITY"
Note that not all modules will work with on all platforms.
See perlport for more information on portability issues.
Read the documentation to see if the module will work on your
system.  There are basically three categories
of modules that will not work \*(L"out of the box\*(R" with all
platforms (with some possibility of overlap):

- \(bu
**Those that should, but don't.**  These need to be fixed; consider
contacting the author and possibly writing a patch.

- \(bu
\fBThose that need to be compiled, where the target platform
doesn't have compilers readily available.  (These modules contain
*.xs* or *.c* files, usually.)  You might be able to find
existing binaries on the \s-1CPAN\s0 or elsewhere, or you might
want to try getting compilers and building it yourself, and then
release the binary for other poor souls to use.

- \(bu
**Those that are targeted at a specific platform.**
(Such as the Win32:: modules.)  If the module is targeted
specifically at a platform other than yours, you're out
of luck, most likely.

Check the \s-1CPAN\s0 Testers if a module should work with your platform
but it doesn't behave as you'd expect, or you aren't sure whether or
not a module will work under your platform.  If the module you want
isn't listed there, you can test it yourself and let \s-1CPAN\s0 Testers know,
you can join \s-1CPAN\s0 Testers, or you can request it be tested.

.Vb 1
    https://cpantesters.org/
.Ve

## HEY

Header "HEY"
If you have any suggested changes for this page, let me know.  Please
don't send me mail asking for help on how to install your modules.
There are too many modules, and too few Orwants, for me to be able to
answer or even acknowledge all your questions.  Contact the module
author instead, ask someone familiar with Perl on your operating
system, or if all else fails, file a ticket at <https://rt.cpan.org/>.

## AUTHOR

Header "AUTHOR"
Jon Orwant

orwant@medita.mit.edu

with invaluable help from Chris Nandor, and valuable help from Brandon
Allbery, Charles Bailey, Graham Barr, Dominic Dunlop, Jarkko
Hietaniemi, Ben Holzman, Tom Horsley, Nick Ing-Simmons, Tuomas
J. Lukka, Laszlo Molnar, Alan Olsen, Peter Prymmer, Gurusamy Sarathy,
Christoph Spalinger, Dan Sugalski, Larry Virden, and Ilya Zakharevich.

First version July 22, 1998; last revised November 21, 2001.

## COPYRIGHT

Header "COPYRIGHT"
Copyright (C) 1998, 2002, 2003 Jon Orwant.  All Rights Reserved.

This document may be distributed under the same terms as Perl itself.
