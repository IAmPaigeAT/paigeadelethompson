+++
operating_system = "macos"
operating_system_version = "15.3"
keywords = ["header", "see", "also", "lwp-mirror", "s-1lwp", "s0", "copyright", "1995-1999", "gisle", "aas", "this", "library", "is", "free", "software", "you", "can", "redistribute", "it", "and", "or", "modify", "under", "the", "same", "terms", "as", "perl", "itself", "author", "no"]
title = "lwp-request(1)"
author = "None Specified"
description = "This program can be used to send requests to s-1WWWs0 servers and your local file system. The request content for s-1POSTs0 and s-1PUTs0 methods is read from stdin.  The content of the response is printed on stdout.  Error messages are printed on s..."
detected_package_version = "5.34.0"
manpage_format = "troff"
manpage_name = "lwp-request"
manpage_section = "1"
date = "2020-04-14"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "LWP-REQUEST 1"
LWP-REQUEST 1 "2020-04-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

lwp-request - Simple command line user agent

## SYNOPSIS

Header "SYNOPSIS"
**lwp-request** [**-afPuUsSedvhx**] [**-m** *method*] [**-b** *base \s-1URL\s0*] [**-t** *timeout*]
            [**-i** *if-modified-since*] [**-c** *content-type*]
            [**-C** *credentials*] [**-p** *proxy-url*] [**-o** *format*] *url*...

## DESCRIPTION

Header "DESCRIPTION"
This program can be used to send requests to \s-1WWW\s0 servers and your
local file system. The request content for \s-1POST\s0 and \s-1PUT\s0
methods is read from stdin.  The content of the response is printed on
stdout.  Error messages are printed on stderr.  The program returns a
status value indicating the number of URLs that failed.

The options are:

- -m <method>
Item "-m <method>"
Set which method to use for the request.  If this option is not used,
then the method is derived from the name of the program.

- -f
Item "-f"
Force request through, even if the program believes that the method is
illegal.  The server might reject the request eventually.

- -b <uri>
Item "-b <uri>"
This \s-1URI\s0 will be used as the base \s-1URI\s0 for resolving all relative URIs
given as argument.

- -t <timeout>
Item "-t <timeout>"
Set the timeout value for the requests.  The timeout is the amount of
time that the program will wait for a response from the remote server
before it fails.  The default unit for the timeout value is seconds.
You might append \*(L"m\*(R" or \*(L"h\*(R" to the timeout value to make it minutes or
hours, respectively.  The default timeout is '3m', i.e. 3 minutes.

- -i <time>
Item "-i <time>"
Set the If-Modified-Since header in the request. If *time* is the
name of a file, use the modification timestamp for this file. If
*time* is not a file, it is parsed as a literal date. Take a look at
HTTP::Date for recognized formats.

- -c <content-type>
Item "-c <content-type>"
Set the Content-Type for the request.  This option is only allowed for
requests that take a content, i.e. \s-1POST\s0 and \s-1PUT.\s0  You can
force methods to take content by using the \f(CW\*(C`-f\*(C' option together with
\f(CW\*(C`-c\*(C'.  The default Content-Type for \s-1POST\s0 is
\f(CW\*(C`application/x-www-form-urlencoded\*(C'.  The default Content-type for
the others is \f(CW\*(C`text/plain\*(C'.

- -p <proxy-url>
Item "-p <proxy-url>"
Set the proxy to be used for the requests.  The program also loads
proxy settings from the environment.  You can disable this with the
\f(CW\*(C`-P\*(C' option.

- -P
Item "-P"
Don't load proxy settings from environment.

- -H <header>
Item "-H <header>"
Send this \s-1HTTP\s0 header with each request. You can specify several, e.g.:
.Sp
.Vb 4
    lwp-request \\
        -H \*(AqReferer: http://other.url/\*(Aq \\
        -H \*(AqHost: somehost\*(Aq \\
        http://this.url/
.Ve

- -C <username>:<password>
Item "-C <username>:<password>"
Provide credentials for documents that are protected by Basic
Authentication.  If the document is protected and you did not specify
the username and password with this option, then you will be prompted
to provide these values.

The following options controls what is displayed by the program:

- -u
Item "-u"
Print request method and absolute \s-1URL\s0 as requests are made.

- -U
Item "-U"
Print request headers in addition to request method and absolute \s-1URL.\s0

- -s
Item "-s"
Print response status code.  This option is always on for \s-1HEAD\s0 requests.

- -S
Item "-S"
Print response status chain. This shows redirect and authorization
requests that are handled by the library.

- -e
Item "-e"
Print response headers.  This option is always on for \s-1HEAD\s0 requests.

- -E
Item "-E"
Print response status chain with full response headers.

- -d
Item "-d"
Do **not** print the content of the response.

- -o <format>
Item "-o <format>"
Process \s-1HTML\s0 content in various ways before printing it.  If the
content type of the response is not \s-1HTML,\s0 then this option has no
effect.  The legal format values are; \f(CW\*(C`text\*(C', \f(CW\*(C`ps\*(C', \f(CW\*(C`links\*(C',
\f(CW\*(C`html\*(C' and \f(CW\*(C`dump\*(C'.
.Sp
If you specify the \f(CW\*(C`text\*(C' format then the \s-1HTML\s0 will be formatted as
plain \f(CW\*(C`latin1\*(C' text.  If you specify the \f(CW\*(C`ps\*(C' format then it will be
formatted as Postscript.
.Sp
The \f(CW\*(C`links\*(C' format will output all links found in the \s-1HTML\s0 document.
Relative links will be expanded to absolute ones.
.Sp
The \f(CW\*(C`html\*(C' format will reformat the \s-1HTML\s0 code and the \f(CW\*(C`dump\*(C' format
will just dump the \s-1HTML\s0 syntax tree.
.Sp
Note that the \f(CW\*(C`HTML-Tree\*(C' distribution needs to be installed for this
option to work.  In addition the \f(CW\*(C`HTML-Format\*(C' distribution needs to
be installed for \f(CW\*(C`-o text\*(C' or \f(CW\*(C`-o ps\*(C' to work.

- -v
Item "-v"
Print the version number of the program and quit.

- -h
Item "-h"
Print usage message and quit.

- -a
Item "-a"
Set text(ascii) mode for content input and output.  If this option is not
used, content input and output is done in binary mode.

Because this program is implemented using the \s-1LWP\s0 library, it will
only support the protocols that \s-1LWP\s0 supports.

## SEE ALSO

Header "SEE ALSO"
lwp-mirror, \s-1LWP\s0

## COPYRIGHT

Header "COPYRIGHT"
Copyright 1995-1999 Gisle Aas.

This library is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

## AUTHOR

Header "AUTHOR"
Gisle Aas <gisle@aas.no>
