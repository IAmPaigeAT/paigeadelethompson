+++
manpage_name = "perlhacktut"
detected_package_version = "5.34.1"
operating_system = "macos"
author = "None Specified"
manpage_format = "troff"
title = "perlhacktut(1)"
date = "2022-02-19"
operating_system_version = "15.3"
manpage_section = "1"
description = "This document takes you through a simple patch example. If you havent read perlhack yet, go do that first! You might also want to read through perlsource too. Once youre done here, check out perlhacktips next. Lets take a simple patch from star..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLHACKTUT 1"
PERLHACKTUT 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlhacktut - Walk through the creation of a simple C code patch

## DESCRIPTION

Header "DESCRIPTION"
This document takes you through a simple patch example.

If you haven't read perlhack yet, go do that first! You might also
want to read through perlsource too.

Once you're done here, check out perlhacktips next.

## EXAMPLE OF A SIMPLE PATCH

Header "EXAMPLE OF A SIMPLE PATCH"
Let's take a simple patch from start to finish.

Here's something Larry suggested: if a \f(CW\*(C`U\*(C' is the first active format
during a \f(CW\*(C`pack\*(C', (for example, \f(CW\*(C`pack "U3C8", @stuff\*(C') then the
resulting string should be treated as \s-1UTF-8\s0 encoded.

If you are working with a git clone of the Perl repository, you will
want to create a branch for your changes. This will make creating a
proper patch much simpler. See the perlgit for details on how to do
this.

### Writing the patch

Subsection "Writing the patch"
How do we prepare to fix this up? First we locate the code in question
- the \f(CW\*(C`pack\*(C' happens at runtime, so it's going to be in one of the
*pp* files. Sure enough, \f(CW\*(C`pp_pack\*(C' is in *pp.c*. Since we're going
to be altering this file, let's copy it to *pp.c~*.

[Well, it was in *pp.c* when this tutorial was written. It has now
been split off with \f(CW\*(C`pp_unpack\*(C' to its own file, *pp_pack.c*]

Now let's look over \f(CW\*(C`pp_pack\*(C': we take a pattern into \f(CW\*(C`pat\*(C', and then
loop over the pattern, taking each format character in turn into
\f(CW\*(C`datum_type\*(C'. Then for each possible format character, we swallow up
the other arguments in the pattern (a field width, an asterisk, and so
on) and convert the next chunk input into the specified format, adding
it onto the output \s-1SV\s0 \f(CW\*(C`cat\*(C'.

How do we know if the \f(CW\*(C`U\*(C' is the first format in the \f(CW\*(C`pat\*(C'? Well, if
we have a pointer to the start of \f(CW\*(C`pat\*(C' then, if we see a \f(CW\*(C`U\*(C' we can
test whether we're still at the start of the string. So, here's where
\f(CW\*(C`pat\*(C' is set up:

.Vb 6
    STRLEN fromlen;
    char *pat = SvPVx(*++MARK, fromlen);
    char *patend = pat + fromlen;
    I32 len;
    I32 datumtype;
    SV *fromstr;
.Ve

We'll have another string pointer in there:

.Vb 7
    STRLEN fromlen;
    char *pat = SvPVx(*++MARK, fromlen);
    char *patend = pat + fromlen;
 +  char *patcopy;
    I32 len;
    I32 datumtype;
    SV *fromstr;
.Ve

And just before we start the loop, we'll set \f(CW\*(C`patcopy\*(C' to be the start
of \f(CW\*(C`pat\*(C':

.Vb 5
    items = SP - MARK;
    MARK++;
    SvPVCLEAR(cat);
 +  patcopy = pat;
    while (pat < patend) \{
.Ve

Now if we see a \f(CW\*(C`U\*(C' which was at the start of the string, we turn on
the \f(CW\*(C`UTF8\*(C' flag for the output \s-1SV,\s0 \f(CW\*(C`cat\*(C':

.Vb 5
 +  if (datumtype == \*(AqU\*(Aq && pat==patcopy+1)
 +      SvUTF8_on(cat);
    if (datumtype == \*(Aq#\*(Aq) \{
        while (pat < patend && *pat != \*(Aq\\n\*(Aq)
            pat++;
.Ve

Remember that it has to be \f(CW\*(C`patcopy+1\*(C' because the first character of
the string is the \f(CW\*(C`U\*(C' which has been swallowed into \f(CW\*(C`datumtype!\*(C'

Oops, we forgot one thing: what if there are spaces at the start of the
pattern? \f(CW\*(C`pack("  U*", @stuff)\*(C' will have \f(CW\*(C`U\*(C' as the first active
character, even though it's not the first thing in the pattern. In this
case, we have to advance \f(CW\*(C`patcopy\*(C' along with \f(CW\*(C`pat\*(C' when we see
spaces:

.Vb 2
    if (isSPACE(datumtype))
        continue;
.Ve

needs to become

.Vb 4
    if (isSPACE(datumtype)) \{
        patcopy++;
        continue;
    \}
.Ve

\s-1OK.\s0 That's the C part done. Now we must do two additional things before
this patch is ready to go: we've changed the behaviour of Perl, and so
we must document that change. We must also provide some more regression
tests to make sure our patch works and doesn't create a bug somewhere
else along the line.

### Testing the patch

Subsection "Testing the patch"
The regression tests for each operator live in *t/op/*, and so we make
a copy of *t/op/pack.t* to *t/op/pack.t~*. Now we can add our tests
to the end. First, we'll test that the \f(CW\*(C`U\*(C' does indeed create Unicode
strings.

t/op/pack.t has a sensible **ok()** function, but if it didn't we could use
the one from t/test.pl.

.Vb 2
 require \*(Aq./test.pl\*(Aq;
 plan( tests => 159 );
.Ve

so instead of this:

.Vb 3
 print \*(Aqnot \*(Aq unless "1.20.300.4000" eq sprintf "%vd",
                                               pack("U*",1,20,300,4000);
 print "ok $test\\n"; $test++;
.Ve

we can write the more sensible (see Test::More for a full
explanation of **is()** and other testing functions).

.Vb 2
 is( "1.20.300.4000", sprintf "%vd", pack("U*",1,20,300,4000),
                                       "U* produces Unicode" );
.Ve

Now we'll test that we got that space-at-the-beginning business right:

.Vb 2
 is( "1.20.300.4000", sprintf "%vd", pack("  U*",1,20,300,4000),
                                     "  with spaces at the beginning" );
.Ve

And finally we'll test that we don't make Unicode strings if \f(CW\*(C`U\*(C' is
**not** the first active format:

.Vb 2
 isnt( v1.20.300.4000, sprintf "%vd", pack("C0U*",1,20,300,4000),
                                       "U* not first isn\*(Aqt Unicode" );
.Ve

Mustn't forget to change the number of tests which appears at the top,
or else the automated tester will get confused. This will either look
like this:

.Vb 1
 print "1..156\\n";
.Ve

or this:

.Vb 1
 plan( tests => 156 );
.Ve

We now compile up Perl, and run it through the test suite. Our new
tests pass, hooray!

### Documenting the patch

Subsection "Documenting the patch"
Finally, the documentation. The job is never done until the paperwork
is over, so let's describe the change we've just made. The relevant
place is *pod/perlfunc.pod*; again, we make a copy, and then we'll
insert this text in the description of \f(CW\*(C`pack\*(C':

.Vb 1
 =item *

 If the pattern begins with a C<U>, the resulting string will be treated
 as UTF-8-encoded Unicode. You can force UTF-8 encoding on in a string
 with an initial C<U0>, and the bytes that follow will be interpreted as
 Unicode characters. If you don\*(Aqt want this to happen, you can begin
 your pattern with C<C0> (or anything else) to force Perl not to UTF-8
 encode your string, and then follow this with a C<U*> somewhere in your
 pattern.
.Ve

### Submit

Subsection "Submit"
See perlhack for details on how to submit this patch.

## AUTHOR

Header "AUTHOR"
This document was originally written by Nathan Torkington, and is
maintained by the perl5-porters mailing list.
