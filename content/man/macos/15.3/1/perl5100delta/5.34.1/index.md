+++
keywords = ["header", "see", "also", "the", "fichanges", "file", "and", "perl590delta", "to", "perl595delta", "man", "pages", "for", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "how", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "ficopying", "files", "copyright", "information"]
manpage_name = "perl5100delta"
operating_system = "macos"
manpage_section = "1"
operating_system_version = "15.3"
description = "This document describes the differences between the 5.8.8 release and the 5.10.0 release. Many of the bug fixes in 5.10.0 were already seen in the 5.8.X maintenance releases; they are not duplicated here and are documented in the set of man pages ..."
author = "None Specified"
manpage_format = "troff"
date = "2022-02-19"
title = "perl5100delta(1)"
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5100DELTA 1"
PERL5100DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5100delta - what is new for perl 5.10.0

## DESCRIPTION

Header "DESCRIPTION"
This document describes the differences between the 5.8.8 release and
the 5.10.0 release.

Many of the bug fixes in 5.10.0 were already seen in the 5.8.X maintenance
releases; they are not duplicated here and are documented in the set of
man pages named perl58[1-8]?delta.

## Core Enhancements

Header "Core Enhancements"
.ie n .SS "The ""feature"" pragma"
.el .SS "The \f(CWfeature pragma"
Subsection "The feature pragma"
The \f(CW\*(C`feature\*(C' pragma is used to enable new syntax that would break Perl's
backwards-compatibility with older releases of the language. It's a lexical
pragma, like \f(CW\*(C`strict\*(C' or \f(CW\*(C`warnings\*(C'.

Currently the following new features are available: \f(CW\*(C`switch\*(C' (adds a
switch statement), \f(CW\*(C`say\*(C' (adds a \f(CW\*(C`say\*(C' built-in function), and \f(CW\*(C`state\*(C'
(adds a \f(CW\*(C`state\*(C' keyword for declaring \*(L"static\*(R" variables). Those
features are described in their own sections of this document.

The \f(CW\*(C`feature\*(C' pragma is also implicitly loaded when you require a minimal
perl version (with the \f(CW\*(C`use VERSION\*(C' construct) greater than, or equal
to, 5.9.5. See feature for details.

### New \fB-E command-line switch

Subsection "New -E command-line switch"
**-E** is equivalent to **-e**, but it implicitly enables all
optional features (like \f(CW\*(C`use feature ":5.10"\*(C').

### Defined-or operator

Subsection "Defined-or operator"
A new operator \f(CW\*(C`//\*(C' (defined-or) has been implemented.
The following expression:

.Vb 1
    $a // $b
.Ve

is merely equivalent to

.Vb 1
   defined $a ? $a : $b
.Ve

and the statement

.Vb 1
   $c //= $d;
.Ve

can now be used instead of

.Vb 1
   $c = $d unless defined $c;
.Ve

The \f(CW\*(C`//\*(C' operator has the same precedence and associativity as \f(CW\*(C`||\*(C'.
Special care has been taken to ensure that this operator Do What You Mean
while not breaking old code, but some edge cases involving the empty
regular expression may now parse differently.  See perlop for
details.

### Switch and Smart Match operator

Subsection "Switch and Smart Match operator"
Perl 5 now has a switch statement. It's available when \f(CW\*(C`use feature
\*(Aqswitch\*(Aq\*(C' is in effect. This feature introduces three new keywords,
\f(CW\*(C`given\*(C', \f(CW\*(C`when\*(C', and \f(CW\*(C`default\*(C':

.Vb 6
    given ($foo) \{
        when (/^abc/) \{ $abc = 1; \}
        when (/^def/) \{ $def = 1; \}
        when (/^xyz/) \{ $xyz = 1; \}
        default \{ $nothing = 1; \}
    \}
.Ve

A more complete description of how Perl matches the switch variable
against the \f(CW\*(C`when\*(C' conditions is given in \*(L"Switch statements\*(R" in perlsyn.

This kind of match is called *smart match*, and it's also possible to use
it outside of switch statements, via the new \f(CW\*(C`~~\*(C' operator. See
\*(L"Smart matching in detail\*(R" in perlsyn.

This feature was contributed by Robin Houston.

### Regular expressions

Subsection "Regular expressions"

- Recursive Patterns
Item "Recursive Patterns"
It is now possible to write recursive patterns without using the \f(CW\*(C`(??\{\})\*(C'
construct. This new way is more efficient, and in many cases easier to
read.
.Sp
Each capturing parenthesis can now be treated as an independent pattern
that can be entered by using the \f(CW\*(C`(?PARNO)\*(C' syntax (\f(CW\*(C`PARNO\*(C' standing for
\*(L"parenthesis number\*(R"). For example, the following pattern will match
nested balanced angle brackets:
.Sp
.Vb 10
    /
     ^                      # start of line
     (                      # start capture buffer 1
        <                   #   match an opening angle bracket
        (?:                 #   match one of:
            (?>             #     don\*(Aqt backtrack over the inside of this group
                [^<>]+      #       one or more non angle brackets
            )               #     end non backtracking group
        |                   #     ... or ...
            (?1)            #     recurse to bracket 1 and try it again
        )*                  #   0 or more times.
        >                   #   match a closing angle bracket
     )                      # end capture buffer one
     $                      # end of line
    /x
.Ve
.Sp
\s-1PCRE\s0 users should note that Perl's recursive regex feature allows
backtracking into a recursed pattern, whereas in \s-1PCRE\s0 the recursion is
atomic or \*(L"possessive\*(R" in nature.  As in the example above, you can
add (?>) to control this selectively.  (Yves Orton)

- Named Capture Buffers
Item "Named Capture Buffers"
It is now possible to name capturing parenthesis in a pattern and refer to
the captured contents by name. The naming syntax is \f(CW\*(C`(?<NAME>....)\*(C'.
It's possible to backreference to a named buffer with the \f(CW\*(C`\\k<NAME>\*(C'
syntax. In code, the new magical hashes \f(CW\*(C`%+\*(C' and \f(CW\*(C`%-\*(C' can be used to
access the contents of the capture buffers.
.Sp
Thus, to replace all doubled chars with a single copy, one could write
.Sp
.Vb 1
    s/(?<letter>.)\\k<letter>/$+\{letter\}/g
.Ve
.Sp
Only buffers with defined contents will be \*(L"visible\*(R" in the \f(CW\*(C`%+\*(C' hash, so
it's possible to do something like
.Sp
.Vb 3
    foreach my $name (keys %+) \{
        print "content of buffer \*(Aq$name\*(Aq is $+\{$name\}\\n";
    \}
.Ve
.Sp
The \f(CW\*(C`%-\*(C' hash is a bit more complete, since it will contain array refs
holding values from all capture buffers similarly named, if there should
be many of them.
.Sp
\f(CW\*(C`%+\*(C' and \f(CW\*(C`%-\*(C' are implemented as tied hashes through the new module
\f(CW\*(C`Tie::Hash::NamedCapture\*(C'.
.Sp
Users exposed to the .NET regex engine will find that the perl
implementation differs in that the numerical ordering of the buffers
is sequential, and not \*(L"unnamed first, then named\*(R". Thus in the pattern
.Sp
.Vb 1
   /(A)(?<B>B)(C)(?<D>D)/
.Ve
.Sp
\f(CW$1 will be 'A', \f(CW$2 will be 'B', \f(CW$3 will be 'C' and \f(CW$4 will be 'D' and not
\f(CW$1 is 'A', \f(CW$2 is 'C' and \f(CW$3 is 'B' and \f(CW$4 is 'D' that a .NET programmer
would expect. This is considered a feature. :-) (Yves Orton)

- Possessive Quantifiers
Item "Possessive Quantifiers"
Perl now supports the \*(L"possessive quantifier\*(R" syntax of the \*(L"atomic match\*(R"
pattern. Basically a possessive quantifier matches as much as it can and never
gives any back. Thus it can be used to control backtracking. The syntax is
similar to non-greedy matching, except instead of using a '?' as the modifier
the '+' is used. Thus \f(CW\*(C`?+\*(C', \f(CW\*(C`*+\*(C', \f(CW\*(C`++\*(C', \f(CW\*(C`\{min,max\}+\*(C' are now legal
quantifiers. (Yves Orton)

- Backtracking control verbs
Item "Backtracking control verbs"
The regex engine now supports a number of special-purpose backtrack
control verbs: (*THEN), (*PRUNE), (*MARK), (*SKIP), (*COMMIT), (*FAIL)
and (*ACCEPT). See perlre for their descriptions. (Yves Orton)

- Relative backreferences
Item "Relative backreferences"
A new syntax \f(CW\*(C`\\g\{N\}\*(C' or \f(CW\*(C`\\gN\*(C' where \*(L"N\*(R" is a decimal integer allows a
safer form of back-reference notation as well as allowing relative
backreferences. This should make it easier to generate and embed patterns
that contain backreferences. See \*(L"Capture buffers\*(R" in perlre. (Yves Orton)
.ie n .IP """\\K"" escape" 4
.el .IP "\f(CW\\K escape" 4
Item "K escape"
The functionality of Jeff Pinyan's module Regexp::Keep has been added to
the core. In regular expressions you can now use the special escape \f(CW\*(C`\\K\*(C'
as a way to do something like floating length positive lookbehind. It is
also useful in substitutions like:
.Sp
.Vb 1
  s/(foo)bar/$1/g
.Ve
.Sp
that can now be converted to
.Sp
.Vb 1
  s/foo\\Kbar//g
.Ve
.Sp
which is much more efficient. (Yves Orton)

- Vertical and horizontal whitespace, and linebreak
Item "Vertical and horizontal whitespace, and linebreak"
Regular expressions now recognize the \f(CW\*(C`\\v\*(C' and \f(CW\*(C`\\h\*(C' escapes that match
vertical and horizontal whitespace, respectively. \f(CW\*(C`\\V\*(C' and \f(CW\*(C`\\H\*(C'
logically match their complements.
.Sp
\f(CW\*(C`\\R\*(C' matches a generic linebreak, that is, vertical whitespace, plus
the multi-character sequence \f(CW"\\x0D\\x0A".

- Optional pre-match and post-match captures with the /p flag
Item "Optional pre-match and post-match captures with the /p flag"
There is a new flag \f(CW\*(C`/p\*(C' for regular expressions.  Using this
makes the engine preserve a copy of the part of the matched string before
the matching substring to the new special variable \f(CW\*(C`$\{^PREMATCH\}\*(C', the
part after the matching substring to \f(CW\*(C`$\{^POSTMATCH\}\*(C', and the matched
substring itself to \f(CW\*(C`$\{^MATCH\}\*(C'.
.Sp
Perl is still able to store these substrings to the special variables
\f(CW\*(C`$\`\*(C', \f(CW\*(C`$\*(Aq\*(C', \f(CW$&, but using these variables anywhere in the program
adds a penalty to all regular expression matches, whereas if you use
the \f(CW\*(C`/p\*(C' flag and the new special variables instead, you pay only for
the regular expressions where the flag is used.
.Sp
For more detail on the new variables, see perlvar; for the use of
the regular expression flag, see perlop and perlre.
.ie n .SS """say()"""
.el .SS "\f(CWsay()"
Subsection "say()"
**say()** is a new built-in, only available when \f(CW\*(C`use feature \*(Aqsay\*(Aq\*(C' is in
effect, that is similar to **print()**, but that implicitly appends a newline
to the printed string. See \*(L"say\*(R" in perlfunc. (Robin Houston)
.ie n .SS "Lexical $_"
.el .SS "Lexical \f(CW$_"
Subsection "Lexical $_"
The default variable \f(CW$_ can now be lexicalized, by declaring it like
any other lexical variable, with a simple

.Vb 1
    my $_;
.Ve

The operations that default on \f(CW$_ will use the lexically-scoped
version of \f(CW$_ when it exists, instead of the global \f(CW$_.

In a \f(CW\*(C`map\*(C' or a \f(CW\*(C`grep\*(C' block, if \f(CW$_ was previously my'ed, then the
\f(CW$_ inside the block is lexical as well (and scoped to the block).

In a scope where \f(CW$_ has been lexicalized, you can still have access to
the global version of \f(CW$_ by using \f(CW$::_, or, more simply, by
overriding the lexical declaration with \f(CW\*(C`our $_\*(C'. (Rafael Garcia-Suarez)
.ie n .SS "The ""_"" prototype"
.el .SS "The \f(CW_ prototype"
Subsection "The _ prototype"
A new prototype character has been added. \f(CW\*(C`_\*(C' is equivalent to \f(CW\*(C`$\*(C' but
defaults to \f(CW$_ if the corresponding argument isn't supplied (both \f(CW\*(C`$\*(C'
and \f(CW\*(C`_\*(C' denote a scalar). Due to the optional nature of the argument,
you can only use it at the end of a prototype, or before a semicolon.

This has a small incompatible consequence: the **prototype()** function has
been adjusted to return \f(CW\*(C`_\*(C' for some built-ins in appropriate cases (for
example, \f(CW\*(C`prototype(\*(AqCORE::rmdir\*(Aq)\*(C'). (Rafael Garcia-Suarez)

### \s-1UNITCHECK\s0 blocks

Subsection "UNITCHECK blocks"
\f(CW\*(C`UNITCHECK\*(C', a new special code block has been introduced, in addition to
\f(CW\*(C`BEGIN\*(C', \f(CW\*(C`CHECK\*(C', \f(CW\*(C`INIT\*(C' and \f(CW\*(C`END\*(C'.

\f(CW\*(C`CHECK\*(C' and \f(CW\*(C`INIT\*(C' blocks, while useful for some specialized purposes,
are always executed at the transition between the compilation and the
execution of the main program, and thus are useless whenever code is
loaded at runtime. On the other hand, \f(CW\*(C`UNITCHECK\*(C' blocks are executed
just after the unit which defined them has been compiled. See perlmod
for more information. (Alex Gough)
.ie n .SS "New Pragma, ""mro"""
.el .SS "New Pragma, \f(CWmro"
Subsection "New Pragma, mro"
A new pragma, \f(CW\*(C`mro\*(C' (for Method Resolution Order) has been added. It
permits to switch, on a per-class basis, the algorithm that perl uses to
find inherited methods in case of a multiple inheritance hierarchy. The
default \s-1MRO\s0 hasn't changed (\s-1DFS,\s0 for Depth First Search). Another \s-1MRO\s0 is
available: the C3 algorithm. See mro for more information.
(Brandon Black)

Note that, due to changes in the implementation of class hierarchy search,
code that used to undef the \f(CW*ISA glob will most probably break. Anyway,
undef'ing \f(CW*ISA had the side-effect of removing the magic on the \f(CW@ISA
array and should not have been done in the first place. Also, the
cache \f(CW*::ISA::CACHE:: no longer exists; to force reset the \f(CW@ISA cache,
you now need to use the \f(CW\*(C`mro\*(C' \s-1API,\s0 or more simply to assign to \f(CW@ISA
(e.g. with \f(CW\*(C`@ISA = @ISA\*(C').
.ie n .SS "**readdir()** may return a ""short filename"" on Windows"
.el .SS "**readdir()** may return a ``short filename'' on Windows"
Subsection "readdir() may return a short filename on Windows"
The **readdir()** function may return a \*(L"short filename\*(R" when the long
filename contains characters outside the \s-1ANSI\s0 codepage.  Similarly
**Cwd::cwd()** may return a short directory name, and **glob()** may return short
names as well.  On the \s-1NTFS\s0 file system these short names can always be
represented in the \s-1ANSI\s0 codepage.  This will not be true for all other file
system drivers; e.g. the \s-1FAT\s0 filesystem stores short filenames in the \s-1OEM\s0
codepage, so some files on \s-1FAT\s0 volumes remain inaccessible through the
\s-1ANSI\s0 APIs.

Similarly, $^X, \f(CW@INC, and \f(CW$ENV\{\s-1PATH\s0\} are preprocessed at startup to make
sure all paths are valid in the \s-1ANSI\s0 codepage (if possible).

The **Win32::GetLongPathName()** function now returns the \s-1UTF-8\s0 encoded
correct long file name instead of using replacement characters to force
the name into the \s-1ANSI\s0 codepage.  The new **Win32::GetANSIPathName()**
function can be used to turn a long pathname into a short one only if the
long one cannot be represented in the \s-1ANSI\s0 codepage.

Many other functions in the \f(CW\*(C`Win32\*(C' module have been improved to accept
\s-1UTF-8\s0 encoded arguments.  Please see Win32 for details.

### \fBreadpipe() is now overridable

Subsection "readpipe() is now overridable"
The built-in function **readpipe()** is now overridable. Overriding it permits
also to override its operator counterpart, \f(CW\*(C`qx//\*(C' (a.k.a. \f(CW\*(C`\`\`\*(C').
Moreover, it now defaults to \f(CW$_ if no argument is provided. (Rafael
Garcia-Suarez)

### Default argument for \fBreadline()

Subsection "Default argument for readline()"
**readline()** now defaults to \f(CW*ARGV if no argument is provided. (Rafael
Garcia-Suarez)

### \fBstate() variables

Subsection "state() variables"
A new class of variables has been introduced. State variables are similar
to \f(CW\*(C`my\*(C' variables, but are declared with the \f(CW\*(C`state\*(C' keyword in place of
\f(CW\*(C`my\*(C'. They're visible only in their lexical scope, but their value is
persistent: unlike \f(CW\*(C`my\*(C' variables, they're not undefined at scope entry,
but retain their previous value. (Rafael Garcia-Suarez, Nicholas Clark)

To use state variables, one needs to enable them by using

.Vb 1
    use feature \*(Aqstate\*(Aq;
.Ve

or by using the \f(CW\*(C`-E\*(C' command-line switch in one-liners.
See \*(L"Persistent Private Variables\*(R" in perlsub.

### Stacked filetest operators

Subsection "Stacked filetest operators"
As a new form of syntactic sugar, it's now possible to stack up filetest
operators. You can now write \f(CW\*(C`-f -w -x $file\*(C' in a row to mean
\f(CW\*(C`-x $file && -w _ && -f _\*(C'. See \*(L"-X\*(R" in perlfunc.

### \s-1\fBUNIVERSAL::DOES\s0()

Subsection "UNIVERSAL::DOES()"
The \f(CW\*(C`UNIVERSAL\*(C' class has a new method, \f(CW\*(C`DOES()\*(C'. It has been added to
solve semantic problems with the \f(CW\*(C`isa()\*(C' method. \f(CW\*(C`isa()\*(C' checks for
inheritance, while \f(CW\*(C`DOES()\*(C' has been designed to be overridden when
module authors use other types of relations between classes (in addition
to inheritance). (chromatic)

See \*(L"$obj->\s-1DOES\s0( \s-1ROLE\s0 )\*(R" in \s-1UNIVERSAL\s0.

### Formats

Subsection "Formats"
Formats were improved in several ways. A new field, \f(CW\*(C`^*\*(C', can be used for
variable-width, one-line-at-a-time text. Null characters are now handled
correctly in picture lines. Using \f(CW\*(C`@#\*(C' and \f(CW\*(C`~~\*(C' together will now
produce a compile-time error, as those format fields are incompatible.
perlform has been improved, and miscellaneous bugs fixed.

### Byte-order modifiers for \fBpack() and \fBunpack()

Subsection "Byte-order modifiers for pack() and unpack()"
There are two new byte-order modifiers, \f(CW\*(C`>\*(C' (big-endian) and \f(CW\*(C`<\*(C'
(little-endian), that can be appended to most **pack()** and **unpack()** template
characters and groups to force a certain byte-order for that type or group.
See \*(L"pack\*(R" in perlfunc and perlpacktut for details.
.ie n .SS """no VERSION"""
.el .SS "\f(CWno VERSION"
Subsection "no VERSION"
You can now use \f(CW\*(C`no\*(C' followed by a version number to specify that you
want to use a version of perl older than the specified one.
.ie n .SS """chdir"", ""chmod"" and ""chown"" on filehandles"
.el .SS "\f(CWchdir, \f(CWchmod and \f(CWchown on filehandles"
Subsection "chdir, chmod and chown on filehandles"
\f(CW\*(C`chdir\*(C', \f(CW\*(C`chmod\*(C' and \f(CW\*(C`chown\*(C' can now work on filehandles as well as
filenames, if the system supports respectively \f(CW\*(C`fchdir\*(C', \f(CW\*(C`fchmod\*(C' and
\f(CW\*(C`fchown\*(C', thanks to a patch provided by Gisle Aas.

### \s-1OS\s0 groups

Subsection "OS groups"
\f(CW$( and \f(CW$) now return groups in the order where the \s-1OS\s0 returns them,
thanks to Gisle Aas. This wasn't previously the case.

### Recursive sort subs

Subsection "Recursive sort subs"
You can now use recursive subroutines with **sort()**, thanks to Robin Houston.

### Exceptions in constant folding

Subsection "Exceptions in constant folding"
The constant folding routine is now wrapped in an exception handler, and
if folding throws an exception (such as attempting to evaluate 0/0), perl
now retains the current optree, rather than aborting the whole program.
Without this change, programs would not compile if they had expressions that
happened to generate exceptions, even though those expressions were in code
that could never be reached at runtime. (Nicholas Clark, Dave Mitchell)
.ie n .SS "Source filters in @INC"
.el .SS "Source filters in \f(CW@INC"
Subsection "Source filters in @INC"
It's possible to enhance the mechanism of subroutine hooks in \f(CW@INC by
adding a source filter on top of the filehandle opened and returned by the
hook. This feature was planned a long time ago, but wasn't quite working
until now. See \*(L"require\*(R" in perlfunc for details. (Nicholas Clark)

### New internal variables

Subsection "New internal variables"
.ie n .IP """$\{^RE_DEBUG_FLAGS\}""" 4
.el .IP "\f(CW$\{^RE_DEBUG_FLAGS\}" 4
Item "$\{^RE_DEBUG_FLAGS\}"
This variable controls what debug flags are in effect for the regular
expression engine when running under \f(CW\*(C`use re "debug"\*(C'. See re for
details.
.ie n .IP """$\{^CHILD_ERROR_NATIVE\}""" 4
.el .IP "\f(CW$\{^CHILD_ERROR_NATIVE\}" 4
Item "$\{^CHILD_ERROR_NATIVE\}"
This variable gives the native status returned by the last pipe close,
backtick command, successful call to **wait()** or **waitpid()**, or from the
**system()** operator. See perlvar for details. (Contributed by Gisle Aas.)
.ie n .IP """$\{^RE_TRIE_MAXBUF\}""" 4
.el .IP "\f(CW$\{^RE_TRIE_MAXBUF\}" 4
Item "$\{^RE_TRIE_MAXBUF\}"
See \*(L"Trie optimisation of literal string alternations\*(R".
.ie n .IP """$\{^WIN32_SLOPPY_STAT\}""" 4
.el .IP "\f(CW$\{^WIN32_SLOPPY_STAT\}" 4
Item "$\{^WIN32_SLOPPY_STAT\}"
See \*(L"Sloppy stat on Windows\*(R".

### Miscellaneous

Subsection "Miscellaneous"
\f(CW\*(C`unpack()\*(C' now defaults to unpacking the \f(CW$_ variable.

\f(CW\*(C`mkdir()\*(C' without arguments now defaults to \f(CW$_.

The internal dump output has been improved, so that non-printable characters
such as newline and backspace are output in \f(CW\*(C`\\x\*(C' notation, rather than
octal.

The **-C** option can no longer be used on the \f(CW\*(C`#!\*(C' line. It wasn't
working there anyway, since the standard streams are already set up
at this point in the execution of the perl interpreter. You can use
**binmode()** instead to get the desired behaviour.

### \s-1UCD 5.0.0\s0

Subsection "UCD 5.0.0"
The copy of the Unicode Character Database included in Perl 5 has
been updated to version 5.0.0.

### \s-1MAD\s0

Subsection "MAD"
\s-1MAD,\s0 which stands for *Miscellaneous Attribute Decoration*, is a
still-in-development work leading to a Perl 5 to Perl 6 converter. To
enable it, it's necessary to pass the argument \f(CW\*(C`-Dmad\*(C' to Configure. The
obtained perl isn't binary compatible with a regular perl 5.10, and has
space and speed penalties; moreover not all regression tests still pass
with it. (Larry Wall, Nicholas Clark)

### \fBkill() on Windows

Subsection "kill() on Windows"
On Windows platforms, \f(CW\*(C`kill(-9, $pid)\*(C' now kills a process tree.
(On Unix, this delivers the signal to all processes in the same process
group.)

## Incompatible Changes

Header "Incompatible Changes"

### Packing and \s-1UTF-8\s0 strings

Subsection "Packing and UTF-8 strings"
The semantics of **pack()** and **unpack()** regarding UTF-8-encoded data has been
changed. Processing is now by default character per character instead of
byte per byte on the underlying encoding. Notably, code that used things
like \f(CW\*(C`pack("a*", $string)\*(C' to see through the encoding of string will now
simply get back the original \f(CW$string. Packed strings can also get upgraded
during processing when you store upgraded characters. You can get the old
behaviour by using \f(CW\*(C`use bytes\*(C'.

To be consistent with **pack()**, the \f(CW\*(C`C0\*(C' in **unpack()** templates indicates
that the data is to be processed in character mode, i.e. character by
character; on the contrary, \f(CW\*(C`U0\*(C' in **unpack()** indicates \s-1UTF-8\s0 mode, where
the packed string is processed in its UTF-8-encoded Unicode form on a byte
by byte basis. This is reversed with regard to perl 5.8.X, but now consistent
between **pack()** and **unpack()**.

Moreover, \f(CW\*(C`C0\*(C' and \f(CW\*(C`U0\*(C' can also be used in **pack()** templates to specify
respectively character and byte modes.

\f(CW\*(C`C0\*(C' and \f(CW\*(C`U0\*(C' in the middle of a pack or unpack format now switch to the
specified encoding mode, honoring parens grouping. Previously, parens were
ignored.

Also, there is a new **pack()** character format, \f(CW\*(C`W\*(C', which is intended to
replace the old \f(CW\*(C`C\*(C'. \f(CW\*(C`C\*(C' is kept for unsigned chars coded as bytes in
the strings internal representation. \f(CW\*(C`W\*(C' represents unsigned (logical)
character values, which can be greater than 255. It is therefore more
robust when dealing with potentially UTF-8-encoded data (as \f(CW\*(C`C\*(C' will wrap
values outside the range 0..255, and not respect the string encoding).

In practice, that means that pack formats are now encoding-neutral, except
\f(CW\*(C`C\*(C'.

For consistency, \f(CW\*(C`A\*(C' in **unpack()** format now trims all Unicode whitespace
from the end of the string. Before perl 5.9.2, it used to strip only the
classical \s-1ASCII\s0 space characters.

### Byte/character count feature in \fBunpack()

Subsection "Byte/character count feature in unpack()"
A new **unpack()** template character, \f(CW".", returns the number of bytes or
characters (depending on the selected encoding mode, see above) read so far.
.ie n .SS "The $* and $# variables have been removed"
.el .SS "The \f(CW$* and \f(CW$# variables have been removed"
Subsection "The $* and $# variables have been removed"
\f(CW$*, which was deprecated in favor of the \f(CW\*(C`/s\*(C' and \f(CW\*(C`/m\*(C' regexp
modifiers, has been removed.

The deprecated \f(CW$# variable (output format for numbers) has been
removed.

Two new severe warnings, \f(CW\*(C`$#/$* is no longer supported\*(C', have been added.

### \fBsubstr() lvalues are no longer fixed-length

Subsection "substr() lvalues are no longer fixed-length"
The lvalues returned by the three argument form of **substr()** used to be a
\*(L"fixed length window\*(R" on the original string. In some cases this could
cause surprising action at distance or other undefined behaviour. Now the
length of the window adjusts itself to the length of the string assigned to
it.
.ie n .SS "Parsing of ""-f _"""
.el .SS "Parsing of \f(CW-f _"
Subsection "Parsing of -f _"
The identifier \f(CW\*(C`_\*(C' is now forced to be a bareword after a filetest
operator. This solves a number of misparsing issues when a global \f(CW\*(C`_\*(C'
subroutine is defined.
.ie n .SS """:unique"""
.el .SS "\f(CW:unique"
Subsection ":unique"
The \f(CW\*(C`:unique\*(C' attribute has been made a no-op, since its current
implementation was fundamentally flawed and not threadsafe.

### Effect of pragmas in eval

Subsection "Effect of pragmas in eval"
The compile-time value of the \f(CW\*(C`%^H\*(C' hint variable can now propagate into
eval("")uated code. This makes it more useful to implement lexical
pragmas.

As a side-effect of this, the overloaded-ness of constants now propagates
into eval("").

### chdir \s-1FOO\s0

Subsection "chdir FOO"
A bareword argument to **chdir()** is now recognized as a file handle.
Earlier releases interpreted the bareword as a directory name.
(Gisle Aas)

### Handling of .pmc files

Subsection "Handling of .pmc files"
An old feature of perl was that before \f(CW\*(C`require\*(C' or \f(CW\*(C`use\*(C' look for a
file with a *.pm* extension, they will first look for a similar filename
with a *.pmc* extension. If this file is found, it will be loaded in
place of any potentially existing file ending in a *.pm* extension.

Previously, *.pmc* files were loaded only if more recent than the
matching *.pm* file. Starting with 5.9.4, they'll be always loaded if
they exist.
.ie n .SS "$^V is now a ""version"" object instead of a v-string"
.el .SS "$^V is now a \f(CWversion object instead of a v-string"
Subsection "$^V is now a version object instead of a v-string"
$^V can still be used with the \f(CW%vd format in printf, but any
character-level operations will now access the string representation
of the \f(CW\*(C`version\*(C' object and not the ordinals of a v-string.
Expressions like \f(CW\*(C`substr($^V, 0, 2)\*(C' or \f(CW\*(C`split //, $^V\*(C'
no longer work and must be rewritten.

### @- and @+ in patterns

Subsection "@- and @+ in patterns"
The special arrays \f(CW\*(C`@-\*(C' and \f(CW\*(C`@+\*(C' are no longer interpolated in regular
expressions. (Sadahiro Tomoyuki)
.ie n .SS "$AUTOLOAD can now be tainted"
.el .SS "\f(CW$AUTOLOAD can now be tainted"
Subsection "$AUTOLOAD can now be tainted"
If you call a subroutine by a tainted name, and if it defers to an
\s-1AUTOLOAD\s0 function, then \f(CW$AUTOLOAD will be (correctly) tainted.
(Rick Delaney)

### Tainting and printf

Subsection "Tainting and printf"
When perl is run under taint mode, \f(CW\*(C`printf()\*(C' and \f(CW\*(C`sprintf()\*(C' will now
reject any tainted format argument. (Rafael Garcia-Suarez)

### undef and signal handlers

Subsection "undef and signal handlers"
Undefining or deleting a signal handler via \f(CW\*(C`undef $SIG\{FOO\}\*(C' is now
equivalent to setting it to \f(CW\*(AqDEFAULT\*(Aq. (Rafael Garcia-Suarez)

### strictures and dereferencing in \fBdefined()

Subsection "strictures and dereferencing in defined()"
\f(CW\*(C`use strict \*(Aqrefs\*(Aq\*(C' was ignoring taking a hard reference in an argument
to **defined()**, as in :

.Vb 3
    use strict \*(Aqrefs\*(Aq;
    my $x = \*(Aqfoo\*(Aq;
    if (defined $$x) \{...\}
.Ve

This now correctly produces the run-time error \f(CW\*(C`Can\*(Aqt use string as a
SCALAR ref while "strict refs" in use\*(C'.

\f(CW\*(C`defined @$foo\*(C' and \f(CW\*(C`defined %$bar\*(C' are now also subject to \f(CW\*(C`strict
\*(Aqrefs\*(Aq\*(C' (that is, \f(CW$foo and \f(CW$bar shall be proper references there.)
(\f(CW\*(C`defined(@foo)\*(C' and \f(CW\*(C`defined(%bar)\*(C' are discouraged constructs anyway.)
(Nicholas Clark)
.ie n .SS """(?p\{\})"" has been removed"
.el .SS "\f(CW(?p\{\}) has been removed"
Subsection "(?p\{\}) has been removed"
The regular expression construct \f(CW\*(C`(?p\{\})\*(C', which was deprecated in perl
5.8, has been removed. Use \f(CW\*(C`(??\{\})\*(C' instead. (Rafael Garcia-Suarez)

### Pseudo-hashes have been removed

Subsection "Pseudo-hashes have been removed"
Support for pseudo-hashes has been removed from Perl 5.9. (The \f(CW\*(C`fields\*(C'
pragma remains here, but uses an alternate implementation.)

### Removal of the bytecode compiler and of perlcc

Subsection "Removal of the bytecode compiler and of perlcc"
\f(CW\*(C`perlcc\*(C', the byteloader and the supporting modules (B::C, B::CC,
B::Bytecode, etc.) are no longer distributed with the perl sources. Those
experimental tools have never worked reliably, and, due to the lack of
volunteers to keep them in line with the perl interpreter developments, it
was decided to remove them instead of shipping a broken version of those.
The last version of those modules can be found with perl 5.9.4.

However the B compiler framework stays supported in the perl core, as with
the more useful modules it has permitted (among others, B::Deparse and
B::Concise).

### Removal of the \s-1JPL\s0

Subsection "Removal of the JPL"
The \s-1JPL\s0 (Java-Perl Lingo) has been removed from the perl sources tarball.

### Recursive inheritance detected earlier

Subsection "Recursive inheritance detected earlier"
Perl will now immediately throw an exception if you modify any package's
\f(CW@ISA in such a way that it would cause recursive inheritance.

Previously, the exception would not occur until Perl attempted to make
use of the recursive inheritance while resolving a method or doing a
\f(CW\*(C`$foo->isa($bar)\*(C' lookup.

### warnings::enabled and warnings::warnif changed to favor users of modules

Subsection "warnings::enabled and warnings::warnif changed to favor users of modules"
The behaviour in 5.10.x favors the person using the module;
The behaviour in 5.8.x favors the module writer;

Assume the following code:

.Vb 5
  main calls Foo::Bar::baz()
  Foo::Bar inherits from Foo::Base
  Foo::Bar::baz() calls Foo::Base::_bazbaz()
  Foo::Base::_bazbaz() calls: warnings::warnif(\*(Aqsubstr\*(Aq, \*(Aqsome warning
message\*(Aq);
.Ve

On 5.8.x, the code warns when Foo::Bar contains \f(CW\*(C`use warnings;\*(C'
It does not matter if Foo::Base or main have warnings enabled
to disable the warning one has to modify Foo::Bar.

On 5.10.0 and newer, the code warns when main contains \f(CW\*(C`use warnings;\*(C'
It does not matter if Foo::Base or Foo::Bar have warnings enabled
to disable the warning one has to modify main.

## Modules and Pragmata

Header "Modules and Pragmata"

### Upgrading individual core modules

Subsection "Upgrading individual core modules"
Even more core modules are now also available separately through the
\s-1CPAN.\s0  If you wish to update one of these modules, you don't need to
wait for a new perl release.  From within the cpan shell, running the
'r' command will report on modules with upgrades available.  See
\f(CW\*(C`perldoc CPAN\*(C' for more information.

### Pragmata Changes

Subsection "Pragmata Changes"
.ie n .IP """feature""" 4
.el .IP "\f(CWfeature" 4
Item "feature"
The new pragma \f(CW\*(C`feature\*(C' is used to enable new features that might break
old code. See "The \f(CW\*(C`feature\*(C' pragma" above.
.ie n .IP """mro""" 4
.el .IP "\f(CWmro" 4
Item "mro"
This new pragma enables to change the algorithm used to resolve inherited
methods. See "New Pragma, \f(CW\*(C`mro\*(C'" above.
.ie n .IP "Scoping of the ""sort"" pragma" 4
.el .IP "Scoping of the \f(CWsort pragma" 4
Item "Scoping of the sort pragma"
The \f(CW\*(C`sort\*(C' pragma is now lexically scoped. Its effect used to be global.
.ie n .IP "Scoping of ""bignum"", ""bigint"", ""bigrat""" 4
.el .IP "Scoping of \f(CWbignum, \f(CWbigint, \f(CWbigrat" 4
Item "Scoping of bignum, bigint, bigrat"
The three numeric pragmas \f(CW\*(C`bignum\*(C', \f(CW\*(C`bigint\*(C' and \f(CW\*(C`bigrat\*(C' are now
lexically scoped. (Tels)
.ie n .IP """base""" 4
.el .IP "\f(CWbase" 4
Item "base"
The \f(CW\*(C`base\*(C' pragma now warns if a class tries to inherit from itself.
(Curtis \*(L"Ovid\*(R" Poe)
.ie n .IP """strict"" and ""warnings""" 4
.el .IP "\f(CWstrict and \f(CWwarnings" 4
Item "strict and warnings"
\f(CW\*(C`strict\*(C' and \f(CW\*(C`warnings\*(C' will now complain loudly if they are loaded via
incorrect casing (as in \f(CW\*(C`use Strict;\*(C'). (Johan Vromans)
.ie n .IP """version""" 4
.el .IP "\f(CWversion" 4
Item "version"
The \f(CW\*(C`version\*(C' module provides support for version objects.
.ie n .IP """warnings""" 4
.el .IP "\f(CWwarnings" 4
Item "warnings"
The \f(CW\*(C`warnings\*(C' pragma doesn't load \f(CW\*(C`Carp\*(C' anymore. That means that code
that used \f(CW\*(C`Carp\*(C' routines without having loaded it at compile time might
need to be adjusted; typically, the following (faulty) code won't work
anymore, and will require parentheses to be added after the function name:
.Sp
.Vb 3
    use warnings;
    require Carp;
    Carp::confess \*(Aqargh\*(Aq;
.Ve
.ie n .IP """less""" 4
.el .IP "\f(CWless" 4
Item "less"
\f(CW\*(C`less\*(C' now does something useful (or at least it tries to). In fact, it
has been turned into a lexical pragma. So, in your modules, you can now
test whether your users have requested to use less \s-1CPU,\s0 or less memory,
less magic, or maybe even less fat. See less for more. (Joshua ben
Jore)

### New modules

Subsection "New modules"

- \(bu
\f(CW\*(C`encoding::warnings\*(C', by Audrey Tang, is a module to emit warnings
whenever an \s-1ASCII\s0 character string containing high-bit bytes is implicitly
converted into \s-1UTF-8.\s0 It's a lexical pragma since Perl 5.9.4; on older
perls, its effect is global.

- \(bu
\f(CW\*(C`Module::CoreList\*(C', by Richard Clamp, is a small handy module that tells
you what versions of core modules ship with any versions of Perl 5. It
comes with a command-line frontend, \f(CW\*(C`corelist\*(C'.

- \(bu
\f(CW\*(C`Math::BigInt::FastCalc\*(C' is an XS-enabled, and thus faster, version of
\f(CW\*(C`Math::BigInt::Calc\*(C'.

- \(bu
\f(CW\*(C`Compress::Zlib\*(C' is an interface to the zlib compression library. It
comes with a bundled version of zlib, so having a working zlib is not a
prerequisite to install it. It's used by \f(CW\*(C`Archive::Tar\*(C' (see below).

- \(bu
\f(CW\*(C`IO::Zlib\*(C' is an \f(CW\*(C`IO::\*(C'-style interface to \f(CW\*(C`Compress::Zlib\*(C'.

- \(bu
\f(CW\*(C`Archive::Tar\*(C' is a module to manipulate \f(CW\*(C`tar\*(C' archives.

- \(bu
\f(CW\*(C`Digest::SHA\*(C' is a module used to calculate many types of \s-1SHA\s0 digests,
has been included for \s-1SHA\s0 support in the \s-1CPAN\s0 module.

- \(bu
\f(CW\*(C`ExtUtils::CBuilder\*(C' and \f(CW\*(C`ExtUtils::ParseXS\*(C' have been added.

- \(bu
\f(CW\*(C`Hash::Util::FieldHash\*(C', by Anno Siegel, has been added. This module
provides support for *field hashes*: hashes that maintain an association
of a reference with a value, in a thread-safe garbage-collected way.
Such hashes are useful to implement inside-out objects.

- \(bu
\f(CW\*(C`Module::Build\*(C', by Ken Williams, has been added. It's an alternative to
\f(CW\*(C`ExtUtils::MakeMaker\*(C' to build and install perl modules.

- \(bu
\f(CW\*(C`Module::Load\*(C', by Jos Boumans, has been added. It provides a single
interface to load Perl modules and *.pl* files.

- \(bu
\f(CW\*(C`Module::Loaded\*(C', by Jos Boumans, has been added. It's used to mark
modules as loaded or unloaded.

- \(bu
\f(CW\*(C`Package::Constants\*(C', by Jos Boumans, has been added. It's a simple
helper to list all constants declared in a given package.

- \(bu
\f(CW\*(C`Win32API::File\*(C', by Tye McQueen, has been added (for Windows builds).
This module provides low-level access to Win32 system \s-1API\s0 calls for
files/dirs.

- \(bu
\f(CW\*(C`Locale::Maketext::Simple\*(C', needed by \s-1CPANPLUS,\s0 is a simple wrapper around
\f(CW\*(C`Locale::Maketext::Lexicon\*(C'. Note that \f(CW\*(C`Locale::Maketext::Lexicon\*(C' isn't
included in the perl core; the behaviour of \f(CW\*(C`Locale::Maketext::Simple\*(C'
gracefully degrades when the later isn't present.

- \(bu
\f(CW\*(C`Params::Check\*(C' implements a generic input parsing/checking mechanism. It
is used by \s-1CPANPLUS.\s0

- \(bu
\f(CW\*(C`Term::UI\*(C' simplifies the task to ask questions at a terminal prompt.

- \(bu
\f(CW\*(C`Object::Accessor\*(C' provides an interface to create per-object accessors.

- \(bu
\f(CW\*(C`Module::Pluggable\*(C' is a simple framework to create modules that accept
pluggable sub-modules.

- \(bu
\f(CW\*(C`Module::Load::Conditional\*(C' provides simple ways to query and possibly
load installed modules.

- \(bu
\f(CW\*(C`Time::Piece\*(C' provides an object oriented interface to time functions,
overriding the built-ins **localtime()** and **gmtime()**.

- \(bu
\f(CW\*(C`IPC::Cmd\*(C' helps to find and run external commands, possibly
interactively.

- \(bu
\f(CW\*(C`File::Fetch\*(C' provide a simple generic file fetching mechanism.

- \(bu
\f(CW\*(C`Log::Message\*(C' and \f(CW\*(C`Log::Message::Simple\*(C' are used by the log facility
of \f(CW\*(C`CPANPLUS\*(C'.

- \(bu
\f(CW\*(C`Archive::Extract\*(C' is a generic archive extraction mechanism
for *.tar* (plain, gzipped or bzipped) or *.zip* files.

- \(bu
\f(CW\*(C`CPANPLUS\*(C' provides an \s-1API\s0 and a command-line tool to access the \s-1CPAN\s0
mirrors.

- \(bu
\f(CW\*(C`Pod::Escapes\*(C' provides utilities that are useful in decoding Pod
E<...> sequences.

- \(bu
\f(CW\*(C`Pod::Simple\*(C' is now the backend for several of the Pod-related modules
included with Perl.

### Selected Changes to Core Modules

Subsection "Selected Changes to Core Modules"
.ie n .IP """Attribute::Handlers""" 4
.el .IP "\f(CWAttribute::Handlers" 4
Item "Attribute::Handlers"
\f(CW\*(C`Attribute::Handlers\*(C' can now report the caller's file and line number.
(David Feldman)
.Sp
All interpreted attributes are now passed as array references. (Damian
Conway)
.ie n .IP """B::Lint""" 4
.el .IP "\f(CWB::Lint" 4
Item "B::Lint"
\f(CW\*(C`B::Lint\*(C' is now based on \f(CW\*(C`Module::Pluggable\*(C', and so can be extended
with plugins. (Joshua ben Jore)
.ie n .IP """B""" 4
.el .IP "\f(CWB" 4
Item "B"
It's now possible to access the lexical pragma hints (\f(CW\*(C`%^H\*(C') by using the
method **B::COP::hints_hash()**. It returns a \f(CW\*(C`B::RHE\*(C' object, which in turn
can be used to get a hash reference via the method **B::RHE::HASH()**. (Joshua
ben Jore)
.ie n .IP """Thread""" 4
.el .IP "\f(CWThread" 4
Item "Thread"
As the old 5005thread threading model has been removed, in favor of the
ithreads scheme, the \f(CW\*(C`Thread\*(C' module is now a compatibility wrapper, to
be used in old code only. It has been removed from the default list of
dynamic extensions.

## Utility Changes

Header "Utility Changes"

- perl -d
Item "perl -d"
The Perl debugger can now save all debugger commands for sourcing later;
notably, it can now emulate stepping backwards, by restarting and
rerunning all bar the last command from a saved command history.
.Sp
It can also display the parent inheritance tree of a given class, with the
\f(CW\*(C`i\*(C' command.

- ptar
Item "ptar"
\f(CW\*(C`ptar\*(C' is a pure perl implementation of \f(CW\*(C`tar\*(C' that comes with
\f(CW\*(C`Archive::Tar\*(C'.

- ptardiff
Item "ptardiff"
\f(CW\*(C`ptardiff\*(C' is a small utility used to generate a diff between the contents
of a tar archive and a directory tree. Like \f(CW\*(C`ptar\*(C', it comes with
\f(CW\*(C`Archive::Tar\*(C'.

- shasum
Item "shasum"
\f(CW\*(C`shasum\*(C' is a command-line utility, used to print or to check \s-1SHA\s0
digests. It comes with the new \f(CW\*(C`Digest::SHA\*(C' module.

- corelist
Item "corelist"
The \f(CW\*(C`corelist\*(C' utility is now installed with perl (see \*(L"New modules\*(R"
above).

- h2ph and h2xs
Item "h2ph and h2xs"
\f(CW\*(C`h2ph\*(C' and \f(CW\*(C`h2xs\*(C' have been made more robust with regard to
\*(L"modern\*(R" C code.
.Sp
\f(CW\*(C`h2xs\*(C' implements a new option \f(CW\*(C`--use-xsloader\*(C' to force use of
\f(CW\*(C`XSLoader\*(C' even in backwards compatible modules.
.Sp
The handling of authors' names that had apostrophes has been fixed.
.Sp
Any enums with negative values are now skipped.

- perlivp
Item "perlivp"
\f(CW\*(C`perlivp\*(C' no longer checks for **.ph* files by default.  Use the new \f(CW\*(C`-a\*(C'
option to run *all* tests.

- find2perl
Item "find2perl"
\f(CW\*(C`find2perl\*(C' now assumes \f(CW\*(C`-print\*(C' as a default action. Previously, it
needed to be specified explicitly.
.Sp
Several bugs have been fixed in \f(CW\*(C`find2perl\*(C', regarding \f(CW\*(C`-exec\*(C' and
\f(CW\*(C`-eval\*(C'. Also the options \f(CW\*(C`-path\*(C', \f(CW\*(C`-ipath\*(C' and \f(CW\*(C`-iname\*(C' have been
added.

- config_data
Item "config_data"
\f(CW\*(C`config_data\*(C' is a new utility that comes with \f(CW\*(C`Module::Build\*(C'. It
provides a command-line interface to the configuration of Perl modules
that use Module::Build's framework of configurability (that is,
\f(CW*::ConfigData modules that contain local configuration information for
their parent modules.)

- cpanp
Item "cpanp"
\f(CW\*(C`cpanp\*(C', the \s-1CPANPLUS\s0 shell, has been added. (\f(CW\*(C`cpanp-run-perl\*(C', a
helper for \s-1CPANPLUS\s0 operation, has been added too, but isn't intended for
direct use).

- cpan2dist
Item "cpan2dist"
\f(CW\*(C`cpan2dist\*(C' is a new utility that comes with \s-1CPANPLUS.\s0 It's a tool to
create distributions (or packages) from \s-1CPAN\s0 modules.

- pod2html
Item "pod2html"
The output of \f(CW\*(C`pod2html\*(C' has been enhanced to be more customizable via
\s-1CSS.\s0 Some formatting problems were also corrected. (Jari Aalto)

## New Documentation

Header "New Documentation"
The perlpragma manpage documents how to write one's own lexical
pragmas in pure Perl (something that is possible starting with 5.9.4).

The new perlglossary manpage is a glossary of terms used in the Perl
documentation, technical and otherwise, kindly provided by O'Reilly Media,
Inc.

The perlreguts manpage, courtesy of Yves Orton, describes internals of the
Perl regular expression engine.

The perlreapi manpage describes the interface to the perl interpreter
used to write pluggable regular expression engines (by \*(Aevar Arnfjo\*:r\*(d-
Bjarmason).

The perlunitut manpage is a tutorial for programming with Unicode and
string encodings in Perl, courtesy of Juerd Waalboer.

A new manual page, perlunifaq (the Perl Unicode \s-1FAQ\s0), has been added
(Juerd Waalboer).

The perlcommunity manpage gives a description of the Perl community
on the Internet and in real life. (Edgar \*(L"Trizor\*(R" Bering)

The \s-1CORE\s0 manual page documents the \f(CW\*(C`CORE::\*(C' namespace. (Tels)

The long-existing feature of \f(CW\*(C`/(?\{...\})/\*(C' regexps setting \f(CW$_ and **pos()**
is now documented.

## Performance Enhancements

Header "Performance Enhancements"

### In-place sorting

Subsection "In-place sorting"
Sorting arrays in place (\f(CW\*(C`@a = sort @a\*(C') is now optimized to avoid
making a temporary copy of the array.

Likewise, \f(CW\*(C`reverse sort ...\*(C' is now optimized to sort in reverse,
avoiding the generation of a temporary intermediate list.

### Lexical array access

Subsection "Lexical array access"
Access to elements of lexical arrays via a numeric constant between 0 and
255 is now faster. (This used to be only the case for global arrays.)

### XS-assisted \s-1SWASHGET\s0

Subsection "XS-assisted SWASHGET"
Some pure-perl code that perl was using to retrieve Unicode properties and
transliteration mappings has been reimplemented in \s-1XS.\s0

### Constant subroutines

Subsection "Constant subroutines"
The interpreter internals now support a far more memory efficient form of
inlineable constants. Storing a reference to a constant value in a symbol
table is equivalent to a full typeglob referencing a constant subroutine,
but using about 400 bytes less memory. This proxy constant subroutine is
automatically upgraded to a real typeglob with subroutine if necessary.
The approach taken is analogous to the existing space optimisation for
subroutine stub declarations, which are stored as plain scalars in place
of the full typeglob.

Several of the core modules have been converted to use this feature for
their system dependent constants - as a result \f(CW\*(C`use POSIX;\*(C' now takes about
200K less memory.
.ie n .SS """PERL_DONT_CREATE_GVSV"""
.el .SS "\f(CWPERL_DONT_CREATE_GVSV"
Subsection "PERL_DONT_CREATE_GVSV"
The new compilation flag \f(CW\*(C`PERL_DONT_CREATE_GVSV\*(C', introduced as an option
in perl 5.8.8, is turned on by default in perl 5.9.3. It prevents perl
from creating an empty scalar with every new typeglob. See perl589delta
for details.

### Weak references are cheaper

Subsection "Weak references are cheaper"
Weak reference creation is now *O(1)* rather than *O(n)*, courtesy of
Nicholas Clark. Weak reference deletion remains *O(n)*, but if deletion only
happens at program exit, it may be skipped completely.

### \fBsort() enhancements

Subsection "sort() enhancements"
Salvador Fandin\*~o provided improvements to reduce the memory usage of \f(CW\*(C`sort\*(C'
and to speed up some cases.

### Memory optimisations

Subsection "Memory optimisations"
Several internal data structures (typeglobs, GVs, CVs, formats) have been
restructured to use less memory. (Nicholas Clark)

### \s-1UTF-8\s0 cache optimisation

Subsection "UTF-8 cache optimisation"
The \s-1UTF-8\s0 caching code is now more efficient, and used more often.
(Nicholas Clark)

### Sloppy stat on Windows

Subsection "Sloppy stat on Windows"
On Windows, perl's **stat()** function normally opens the file to determine
the link count and update attributes that may have been changed through
hard links. Setting $\{^WIN32_SLOPPY_STAT\} to a true value speeds up
**stat()** by not performing this operation. (Jan Dubois)

### Regular expressions optimisations

Subsection "Regular expressions optimisations"

- Engine de-recursivised
Item "Engine de-recursivised"
The regular expression engine is no longer recursive, meaning that
patterns that used to overflow the stack will either die with useful
explanations, or run to completion, which, since they were able to blow
the stack before, will likely take a very long time to happen. If you were
experiencing the occasional stack overflow (or segfault) and upgrade to
discover that now perl apparently hangs instead, look for a degenerate
regex. (Dave Mitchell)

- Single char char-classes treated as literals
Item "Single char char-classes treated as literals"
Classes of a single character are now treated the same as if the character
had been used as a literal, meaning that code that uses char-classes as an
escaping mechanism will see a speedup. (Yves Orton)

- Trie optimisation of literal string alternations
Item "Trie optimisation of literal string alternations"
Alternations, where possible, are optimised into more efficient matching
structures. String literal alternations are merged into a trie and are
matched simultaneously.  This means that instead of O(N) time for matching
N alternations at a given point, the new code performs in O(1) time.
A new special variable, $\{^RE_TRIE_MAXBUF\}, has been added to fine-tune
this optimization. (Yves Orton)
.Sp
**Note:** Much code exists that works around perl's historic poor
performance on alternations. Often the tricks used to do so will disable
the new optimisations. Hopefully the utility modules used for this purpose
will be educated about these new optimisations.

- Aho-Corasick start-point optimisation
Item "Aho-Corasick start-point optimisation"
When a pattern starts with a trie-able alternation and there aren't
better optimisations available, the regex engine will use Aho-Corasick
matching to find the start point. (Yves Orton)

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"

### Configuration improvements

Subsection "Configuration improvements"
.ie n .IP """-Dusesitecustomize""" 4
.el .IP "\f(CW-Dusesitecustomize" 4
Item "-Dusesitecustomize"
Run-time customization of \f(CW@INC can be enabled by passing the
\f(CW\*(C`-Dusesitecustomize\*(C' flag to Configure. When enabled, this will make perl
run *\f(CI$sitelibexp\fI/sitecustomize.pl* before anything else.  This script can
then be set up to add additional entries to \f(CW@INC.

- Relocatable installations
Item "Relocatable installations"
There is now Configure support for creating a relocatable perl tree. If
you Configure with \f(CW\*(C`-Duserelocatableinc\*(C', then the paths in \f(CW@INC (and
everything else in \f(CW%Config) can be optionally located via the path of the
perl executable.
.Sp
That means that, if the string \f(CW".../" is found at the start of any
path, it's substituted with the directory of $^X. So, the relocation can
be configured on a per-directory basis, although the default with
\f(CW\*(C`-Duserelocatableinc\*(C' is that everything is relocated. The initial
install is done to the original configured prefix.

- \fBstrlcat() and \fBstrlcpy()
Item "strlcat() and strlcpy()"
The configuration process now detects whether **strlcat()** and **strlcpy()** are
available.  When they are not available, perl's own version is used (from
Russ Allbery's public domain implementation).  Various places in the perl
interpreter now use them. (Steve Peters)
.ie n .IP """d_pseudofork"" and ""d_printf_format_null""" 4
.el .IP "\f(CWd_pseudofork and \f(CWd_printf_format_null" 4
Item "d_pseudofork and d_printf_format_null"
A new configuration variable, available as \f(CW$Config\{d_pseudofork\} in
the Config module, has been added, to distinguish real **fork()** support
from fake pseudofork used on Windows platforms.
.Sp
A new configuration variable, \f(CW\*(C`d_printf_format_null\*(C', has been added,
to see if printf-like formats are allowed to be \s-1NULL.\s0

- Configure help
Item "Configure help"
\f(CW\*(C`Configure -h\*(C' has been extended with the most commonly used options.

### Compilation improvements

Subsection "Compilation improvements"

- Parallel build
Item "Parallel build"
Parallel makes should work properly now, although there may still be problems
if \f(CW\*(C`make test\*(C' is instructed to run in parallel.

- Borland's compilers support
Item "Borland's compilers support"
Building with Borland's compilers on Win32 should work more smoothly. In
particular Steve Hay has worked to side step many warnings emitted by their
compilers and at least one C compiler internal error.

- Static build on Windows
Item "Static build on Windows"
Perl extensions on Windows now can be statically built into the Perl \s-1DLL.\s0
.Sp
Also, it's now possible to build a \f(CW\*(C`perl-static.exe\*(C' that doesn't depend
on the Perl \s-1DLL\s0 on Win32. See the Win32 makefiles for details.
(Vadim Konovalov)

- ppport.h files
Item "ppport.h files"
All *ppport.h* files in the \s-1XS\s0 modules bundled with perl are now
autogenerated at build time. (Marcus Holland-Moritz)

- \*(C+ compatibility
Item " compatibility"
Efforts have been made to make perl and the core \s-1XS\s0 modules compilable
with various \*(C+ compilers (although the situation is not perfect with
some of the compilers on some of the platforms tested.)

- Support for Microsoft 64-bit compiler
Item "Support for Microsoft 64-bit compiler"
Support for building perl with Microsoft's 64-bit compiler has been
improved. (ActiveState)

- Visual \*(C+
Item "Visual "
Perl can now be compiled with Microsoft Visual \*(C+ 2005 (and 2008 Beta 2).

- Win32 builds
Item "Win32 builds"
All win32 builds (MS-Win, WinCE) have been merged and cleaned up.

### Installation improvements

Subsection "Installation improvements"

- Module auxiliary files
Item "Module auxiliary files"
\s-1README\s0 files and changelogs for \s-1CPAN\s0 modules bundled with perl are no
longer installed.

### New Or Improved Platforms

Subsection "New Or Improved Platforms"
Perl has been reported to work on Symbian \s-1OS.\s0 See perlsymbian for more
information.

Many improvements have been made towards making Perl work correctly on
z/OS.

Perl has been reported to work on DragonFlyBSD and MidnightBSD.

Perl has also been reported to work on NexentaOS
( http://www.gnusolaris.org/ ).

The \s-1VMS\s0 port has been improved. See perlvms.

Support for Cray \s-1XT4\s0 Catamount/Qk has been added. See
*hints/catamount.sh* in the source code distribution for more
information.

Vendor patches have been merged for RedHat and Gentoo.

**DynaLoader::dl_unload_file()** now works on Windows.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- strictures in regexp-eval blocks
Item "strictures in regexp-eval blocks"
\f(CW\*(C`strict\*(C' wasn't in effect in regexp-eval blocks (\f(CW\*(C`/(?\{...\})/\*(C').

- Calling \fBCORE::require()
Item "Calling CORE::require()"
**CORE::require()** and **CORE::do()** were always parsed as **require()** and **do()**
when they were overridden. This is now fixed.

- Subscripts of slices
Item "Subscripts of slices"
You can now use a non-arrowed form for chained subscripts after a list
slice, like in:
.Sp
.Vb 1
    (\{foo => "bar"\})[0]\{foo\}
.Ve
.Sp
This used to be a syntax error; a \f(CW\*(C`->\*(C' was required.
.ie n .IP """no warnings \*(Aqcategory\*(Aq"" works correctly with -w" 4
.el .IP "\f(CWno warnings \*(Aqcategory\*(Aq works correctly with -w" 4
Item "no warnings category works correctly with -w"
Previously when running with warnings enabled globally via \f(CW\*(C`-w\*(C', selective
disabling of specific warning categories would actually turn off all warnings.
This is now fixed; now \f(CW\*(C`no warnings \*(Aqio\*(Aq;\*(C' will only turn off warnings in the
\f(CW\*(C`io\*(C' class. Previously it would erroneously turn off all warnings.

- threads improvements
Item "threads improvements"
Several memory leaks in ithreads were closed. Also, ithreads were made
less memory-intensive.
.Sp
\f(CW\*(C`threads\*(C' is now a dual-life module, also available on \s-1CPAN.\s0 It has been
expanded in many ways. A **kill()** method is available for thread signalling.
One can get thread status, or the list of running or joinable threads.
.Sp
A new \f(CW\*(C`threads->exit()\*(C' method is used to exit from the application
(this is the default for the main thread) or from the current thread only
(this is the default for all other threads). On the other hand, the **exit()**
built-in now always causes the whole application to terminate. (Jerry
D. Hedden)

- \fBchr() and negative values
Item "chr() and negative values"
**chr()** on a negative value now gives \f(CW\*(C`\\x\{FFFD\}\*(C', the Unicode replacement
character, unless when the \f(CW\*(C`bytes\*(C' pragma is in effect, where the low
eight bits of the value are used.

- \s-1PERL5SHELL\s0 and tainting
Item "PERL5SHELL and tainting"
On Windows, the \s-1PERL5SHELL\s0 environment variable is now checked for
taintedness. (Rafael Garcia-Suarez)

- Using *FILE\{\s-1IO\s0\}
Item "Using *FILE\{IO\}"
\f(CW\*(C`stat()\*(C' and \f(CW\*(C`-X\*(C' filetests now treat *FILE\{\s-1IO\s0\} filehandles like *FILE
filehandles. (Steve Peters)

- Overloading and reblessing
Item "Overloading and reblessing"
Overloading now works when references are reblessed into another class.
Internally, this has been implemented by moving the flag for \*(L"overloading\*(R"
from the reference to the referent, which logically is where it should
always have been. (Nicholas Clark)

- Overloading and \s-1UTF-8\s0
Item "Overloading and UTF-8"
A few bugs related to \s-1UTF-8\s0 handling with objects that have
stringification overloaded have been fixed. (Nicholas Clark)

- eval memory leaks fixed
Item "eval memory leaks fixed"
Traditionally, \f(CW\*(C`eval \*(Aqsyntax error\*(Aq\*(C' has leaked badly. Many (but not all)
of these leaks have now been eliminated or reduced. (Dave Mitchell)

- Random device on Windows
Item "Random device on Windows"
In previous versions, perl would read the file */dev/urandom* if it
existed when seeding its random number generator.  That file is unlikely
to exist on Windows, and if it did would probably not contain appropriate
data, so perl no longer tries to read it on Windows. (Alex Davies)

- \s-1PERLIO_DEBUG\s0
Item "PERLIO_DEBUG"
The \f(CW\*(C`PERLIO_DEBUG\*(C' environment variable no longer has any effect for
setuid scripts and for scripts run with **-T**.
.Sp
Moreover, with a thread-enabled perl, using \f(CW\*(C`PERLIO_DEBUG\*(C' could lead to
an internal buffer overflow. This has been fixed.

- PerlIO::scalar and read-only scalars
Item "PerlIO::scalar and read-only scalars"
PerlIO::scalar will now prevent writing to read-only scalars. Moreover,
**seek()** is now supported with PerlIO::scalar-based filehandles, the
underlying string being zero-filled as needed. (Rafael, Jarkko Hietaniemi)

- \fBstudy() and \s-1UTF-8\s0
Item "study() and UTF-8"
**study()** never worked for \s-1UTF-8\s0 strings, but could lead to false results.
It's now a no-op on \s-1UTF-8\s0 data. (Yves Orton)

- Critical signals
Item "Critical signals"
The signals \s-1SIGILL, SIGBUS\s0 and \s-1SIGSEGV\s0 are now always delivered in an
\*(L"unsafe\*(R" manner (contrary to other signals, that are deferred until the
perl interpreter reaches a reasonably stable state; see
\*(L"Deferred Signals (Safe Signals)\*(R" in perlipc). (Rafael)
.ie n .IP "@INC-hook fix" 4
.el .IP "\f(CW@INC-hook fix" 4
Item "@INC-hook fix"
When a module or a file is loaded through an \f(CW@INC-hook, and when this hook
has set a filename entry in \f(CW%INC, _\|_FILE_\|_ is now set for this module
accordingly to the contents of that \f(CW%INC entry. (Rafael)
.ie n .IP """-t"" switch fix" 4
.el .IP "\f(CW-t switch fix" 4
Item "-t switch fix"
The \f(CW\*(C`-w\*(C' and \f(CW\*(C`-t\*(C' switches can now be used together without messing
up which categories of warnings are activated. (Rafael)

- Duping \s-1UTF-8\s0 filehandles
Item "Duping UTF-8 filehandles"
Duping a filehandle which has the \f(CW\*(C`:utf8\*(C' PerlIO layer set will now
properly carry that layer on the duped filehandle. (Rafael)

- Localisation of hash elements
Item "Localisation of hash elements"
Localizing a hash element whose key was given as a variable didn't work
correctly if the variable was changed while the **local()** was in effect (as
in \f(CW\*(C`local $h\{$x\}; ++$x\*(C'). (Bo Lindbergh)

## New or Changed Diagnostics

Header "New or Changed Diagnostics"

- Use of uninitialized value
Item "Use of uninitialized value"
Perl will now try to tell you the name of the variable (if any) that was
undefined.

- Deprecated use of \fBmy() in false conditional
Item "Deprecated use of my() in false conditional"
A new deprecation warning, *Deprecated use of \f(BImy()\fI in false conditional*,
has been added, to warn against the use of the dubious and deprecated
construct
.Sp
.Vb 1
    my $x if 0;
.Ve
.Sp
See perldiag. Use \f(CW\*(C`state\*(C' variables instead.

- !=~ should be !~
Item "!=~ should be !~"
A new warning, \f(CW\*(C`!=~ should be !~\*(C', is emitted to prevent this misspelling
of the non-matching operator.

- Newline in left-justified string
Item "Newline in left-justified string"
The warning *Newline in left-justified string* has been removed.
.ie n .IP "Too late for ""-T"" option" 4
.el .IP "Too late for ``-T'' option" 4
Item "Too late for -T option"
The error *Too late for \*(L"-T\*(R" option* has been reformulated to be more
descriptive.
.ie n .IP """%s"" variable %s masks earlier declaration" 4
.el .IP "``%s'' variable \f(CW%s masks earlier declaration" 4
Item "%s variable %s masks earlier declaration"
This warning is now emitted in more consistent cases; in short, when one
of the declarations involved is a \f(CW\*(C`my\*(C' variable:
.Sp
.Vb 3
    my $x;   my $x;     # warns
    my $x;  our $x;     # warns
    our $x;  my $x;     # warns
.Ve
.Sp
On the other hand, the following:
.Sp
.Vb 1
    our $x; our $x;
.Ve
.Sp
now gives a \f(CW\*(C`"our" variable %s redeclared\*(C' warning.

- \fBreaddir()/\fBclosedir()/etc. attempted on invalid dirhandle
Item "readdir()/closedir()/etc. attempted on invalid dirhandle"
These new warnings are now emitted when a dirhandle is used but is
either closed or not really a dirhandle.
.ie n .IP "Opening dirhandle/filehandle %s also as a file/directory" 4
.el .IP "Opening dirhandle/filehandle \f(CW%s also as a file/directory" 4
Item "Opening dirhandle/filehandle %s also as a file/directory"
Two deprecation warnings have been added: (Rafael)
.Sp
.Vb 2
    Opening dirhandle %s also as a file
    Opening filehandle %s also as a directory
.Ve

- Use of -P is deprecated
Item "Use of -P is deprecated"
Perl's command-line switch \f(CW\*(C`-P\*(C' is now deprecated.

- v-string in use/require is non-portable
Item "v-string in use/require is non-portable"
Perl will warn you against potential backwards compatibility problems with
the \f(CW\*(C`use VERSION\*(C' syntax.

- perl -V
Item "perl -V"
\f(CW\*(C`perl -V\*(C' has several improvements, making it more useable from shell
scripts to get the value of configuration variables. See perlrun for
details.

## Changed Internals

Header "Changed Internals"
In general, the source code of perl has been refactored, tidied up,
and optimized in many places. Also, memory management and allocation
has been improved in several points.

When compiling the perl core with gcc, as many gcc warning flags are
turned on as is possible on the platform.  (This quest for cleanliness
doesn't extend to \s-1XS\s0 code because we cannot guarantee the tidiness of
code we didn't write.)  Similar strictness flags have been added or
tightened for various other C compilers.

### Reordering of SVt_* constants

Subsection "Reordering of SVt_* constants"
The relative ordering of constants that define the various types of \f(CW\*(C`SV\*(C'
have changed; in particular, \f(CW\*(C`SVt_PVGV\*(C' has been moved before \f(CW\*(C`SVt_PVLV\*(C',
\f(CW\*(C`SVt_PVAV\*(C', \f(CW\*(C`SVt_PVHV\*(C' and \f(CW\*(C`SVt_PVCV\*(C'.  This is unlikely to make any
difference unless you have code that explicitly makes assumptions about that
ordering. (The inheritance hierarchy of \f(CW\*(C`B::*\*(C' objects has been changed
to reflect this.)

### Elimination of SVt_PVBM

Subsection "Elimination of SVt_PVBM"
Related to this, the internal type \f(CW\*(C`SVt_PVBM\*(C' has been removed. This
dedicated type of \f(CW\*(C`SV\*(C' was used by the \f(CW\*(C`index\*(C' operator and parts of the
regexp engine to facilitate fast Boyer-Moore matches. Its use internally has
been replaced by \f(CW\*(C`SV\*(C's of type \f(CW\*(C`SVt_PVGV\*(C'.

### New type SVt_BIND

Subsection "New type SVt_BIND"
A new type \f(CW\*(C`SVt_BIND\*(C' has been added, in readiness for the project to
implement Perl 6 on 5. There deliberately is no implementation yet, and
they cannot yet be created or destroyed.

### Removal of \s-1CPP\s0 symbols

Subsection "Removal of CPP symbols"
The C preprocessor symbols \f(CW\*(C`PERL_PM_APIVERSION\*(C' and
\f(CW\*(C`PERL_XS_APIVERSION\*(C', which were supposed to give the version number of
the oldest perl binary-compatible (resp. source-compatible) with the
present one, were not used, and sometimes had misleading values. They have
been removed.

### Less space is used by ops

Subsection "Less space is used by ops"
The \f(CW\*(C`BASEOP\*(C' structure now uses less space. The \f(CW\*(C`op_seq\*(C' field has been
removed and replaced by a single bit bit-field \f(CW\*(C`op_opt\*(C'. \f(CW\*(C`op_type\*(C' is now 9
bits long. (Consequently, the \f(CW\*(C`B::OP\*(C' class doesn't provide an \f(CW\*(C`seq\*(C'
method anymore.)

### New parser

Subsection "New parser"
perl's parser is now generated by bison (it used to be generated by
byacc.) As a result, it seems to be a bit more robust.

Also, Dave Mitchell improved the lexer debugging output under \f(CW\*(C`-DT\*(C'.
.ie n .SS "Use of ""const"""
.el .SS "Use of \f(CWconst"
Subsection "Use of const"
Andy Lester supplied many improvements to determine which function
parameters and local variables could actually be declared \f(CW\*(C`const\*(C' to the C
compiler. Steve Peters provided new \f(CW*_set macros and reworked the core to
use these rather than assigning to macros in \s-1LVALUE\s0 context.

### Mathoms

Subsection "Mathoms"
A new file, *mathoms.c*, has been added. It contains functions that are
no longer used in the perl core, but that remain available for binary or
source compatibility reasons. However, those functions will not be
compiled in if you add \f(CW\*(C`-DNO_MATHOMS\*(C' in the compiler flags.
.ie n .SS """AvFLAGS"" has been removed"
.el .SS "\f(CWAvFLAGS has been removed"
Subsection "AvFLAGS has been removed"
The \f(CW\*(C`AvFLAGS\*(C' macro has been removed.
.ie n .SS """av_*"" changes"
.el .SS "\f(CWav_* changes"
Subsection "av_* changes"
The \f(CW\*(C`av_*()\*(C' functions, used to manipulate arrays, no longer accept null
\f(CW\*(C`AV*\*(C' parameters.

### $^H and %^H

Subsection "$^H and %^H"
The implementation of the special variables $^H and %^H has changed, to
allow implementing lexical pragmas in pure Perl.

### B:: modules inheritance changed

Subsection "B:: modules inheritance changed"
The inheritance hierarchy of \f(CW\*(C`B::\*(C' modules has changed; \f(CW\*(C`B::NV\*(C' now
inherits from \f(CW\*(C`B::SV\*(C' (it used to inherit from \f(CW\*(C`B::IV\*(C').

### Anonymous hash and array constructors

Subsection "Anonymous hash and array constructors"
The anonymous hash and array constructors now take 1 op in the optree
instead of 3, now that pp_anonhash and pp_anonlist return a reference to
a hash/array when the op is flagged with OPf_SPECIAL. (Nicholas Clark)

## Known Problems

Header "Known Problems"
There's still a remaining problem in the implementation of the lexical
\f(CW$_: it doesn't work inside \f(CW\*(C`/(?\{...\})/\*(C' blocks. (See the \s-1TODO\s0 test in
*t/op/mydef.t*.)

Stacked filetest operators won't work when the \f(CW\*(C`filetest\*(C' pragma is in
effect, because they rely on the **stat()** buffer \f(CW\*(C`_\*(C' being populated, and
filetest bypasses **stat()**.

### \s-1UTF-8\s0 problems

Subsection "UTF-8 problems"
The handling of Unicode still is unclean in several places, where it's
dependent on whether a string is internally flagged as \s-1UTF-8.\s0 This will
be made more consistent in perl 5.12, but that won't be possible without
a certain amount of backwards incompatibility.

## Platform Specific Problems

Header "Platform Specific Problems"
When compiled with g++ and thread support on Linux, it's reported that the
\f(CW$! stops working correctly. This is related to the fact that the glibc
provides two **strerror_r**\|(3) implementation, and perl selects the wrong
one.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/rt3/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file and the perl590delta to perl595delta man pages for
exhaustive details on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
