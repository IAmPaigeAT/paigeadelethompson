+++
operating_system_version = "15.3"
author = "None Specified"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "how", "to", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
operating_system = "macos"
manpage_section = "1"
detected_package_version = "5.34.1"
manpage_name = "perl587delta"
date = "2022-02-19"
manpage_format = "troff"
description = "This document describes differences between the 5.8.6 release and the 5.8.7 release. There are no changes incompatible with 5.8.6. The copy of the Unicode Character Database included in Perl 5.8 has been updated to 4.1.0 from 4.0.1. See <http://w..."
title = "perl587delta(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL587DELTA 1"
PERL587DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl587delta - what is new for perl v5.8.7

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.8.6 release and
the 5.8.7 release.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes incompatible with 5.8.6.

## Core Enhancements

Header "Core Enhancements"

### Unicode Character Database 4.1.0

Subsection "Unicode Character Database 4.1.0"
The copy of the Unicode Character Database included in Perl 5.8 has
been updated to 4.1.0 from 4.0.1. See
<http://www.unicode.org/versions/Unicode4.1.0/#NotableChanges> for the
notable changes.

### suidperl less insecure

Subsection "suidperl less insecure"
A pair of exploits in \f(CW\*(C`suidperl\*(C' involving debugging code have been closed.

For new projects the core perl team strongly recommends that you use
dedicated, single purpose security tools such as \f(CW\*(C`sudo\*(C' in preference to
\f(CW\*(C`suidperl\*(C'.

### Optional site customization script

Subsection "Optional site customization script"
The perl interpreter can be built to allow the use of a site customization
script. By default this is not enabled, to be consistent with previous perl
releases. To use this, add \f(CW\*(C`-Dusesitecustomize\*(C' to the command line flags
when running the \f(CW\*(C`Configure\*(C' script. See also \*(L"-f\*(R" in perlrun.
.ie n .SS """Config.pm"" is now much smaller."
.el .SS "\f(CWConfig.pm is now much smaller."
Subsection "Config.pm is now much smaller."
\f(CW\*(C`Config.pm\*(C' is now about 3K rather than 32K, with the infrequently used
code and \f(CW%Config values loaded on demand. This is transparent to the
programmer, but means that most code will save parsing and loading 29K of
script (for example, code that uses \f(CW\*(C`File::Find\*(C').

## Modules and Pragmata

Header "Modules and Pragmata"

- \(bu
B upgraded to version 1.09

- \(bu
base upgraded to version 2.07

- \(bu
bignum upgraded to version 0.17

- \(bu
bytes upgraded to version 1.02

- \(bu
Carp upgraded to version 1.04

- \(bu
\s-1CGI\s0 upgraded to version 3.10

- \(bu
Class::ISA upgraded to version 0.33

- \(bu
Data::Dumper upgraded to version 2.121_02

- \(bu
DB_File upgraded to version 1.811

- \(bu
Devel::PPPort upgraded to version 3.06

- \(bu
Digest upgraded to version 1.10

- \(bu
Encode upgraded to version 2.10

- \(bu
FileCache upgraded to version 1.05

- \(bu
File::Path upgraded to version 1.07

- \(bu
File::Temp upgraded to version 0.16

- \(bu
IO::File upgraded to version 1.11

- \(bu
IO::Socket upgraded to version 1.28

- \(bu
Math::BigInt upgraded to version 1.77

- \(bu
Math::BigRat upgraded to version 0.15

- \(bu
overload upgraded to version 1.03

- \(bu
PathTools upgraded to version 3.05

- \(bu
Pod::HTML upgraded to version 1.0503

- \(bu
Pod::Perldoc upgraded to version 3.14

- \(bu
Pod::LaTeX upgraded to version 0.58

- \(bu
Pod::Parser upgraded to version 1.30

- \(bu
Symbol upgraded to version 1.06

- \(bu
Term::ANSIColor upgraded to version 1.09

- \(bu
Test::Harness upgraded to version 2.48

- \(bu
Test::Simple upgraded to version 0.54

- \(bu
Text::Wrap upgraded to version 2001.09293, to fix a bug when **wrap()** was
called with a non-space separator.

- \(bu
threads::shared upgraded to version 0.93

- \(bu
Time::HiRes upgraded to version 1.66

- \(bu
Time::Local upgraded to version 1.11

- \(bu
Unicode::Normalize upgraded to version 0.32

- \(bu
utf8 upgraded to version 1.05

- \(bu
Win32 upgraded to version 0.24, which provides Win32::GetFileVersion

## Utility Changes

Header "Utility Changes"

### find2perl enhancements

Subsection "find2perl enhancements"
\f(CW\*(C`find2perl\*(C' has new options \f(CW\*(C`-iname\*(C', \f(CW\*(C`-path\*(C' and \f(CW\*(C`-ipath\*(C'.

## Performance Enhancements

Header "Performance Enhancements"
The internal pointer mapping hash used during ithreads cloning now uses an
arena for memory allocation. In tests this reduced ithreads cloning time by
about 10%.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"

- \(bu
The Win32 \*(L"dmake\*(R" makefile.mk has been updated to make it compatible
with the latest versions of dmake.

- \(bu
\f(CW\*(C`PERL_MALLOC\*(C', \f(CW\*(C`DEBUG_MSTATS\*(C', \f(CW\*(C`PERL_HASH_SEED_EXPLICIT\*(C' and \f(CW\*(C`NO_HASH_SEED\*(C'
should now work in Win32 makefiles.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
The **socket()** function on Win32 has been fixed so that it is able to use
transport providers which specify a protocol of 0 (meaning any protocol
is allowed) once more.  (This was broken in 5.8.6, and typically caused
the use of \s-1ICMP\s0 sockets to fail.)

- \(bu
Another obscure bug involving \f(CW\*(C`substr\*(C' and \s-1UTF-8\s0 caused by bad internal
offset caching has been identified and fixed.

- \(bu
A bug involving the loading of \s-1UTF-8\s0 tables by the regexp engine has been
fixed - code such as \f(CW\*(C`"\\x\{100\}" =~ /[[:print:]]/\*(C' will no longer give
corrupt results.

- \(bu
Case conversion operations such as \f(CW\*(C`uc\*(C' on a long Unicode string could
exhaust memory. This has been fixed.

- \(bu
\f(CW\*(C`index\*(C'/\f(CW\*(C`rindex\*(C' were buggy for some combinations of Unicode and
non-Unicode data. This has been fixed.

- \(bu
\f(CW\*(C`read\*(C' (and presumably \f(CW\*(C`sysread\*(C') would expose the \s-1UTF-8\s0 internals when
reading from a byte oriented file handle into a \s-1UTF-8\s0 scalar. This has
been fixed.

- \(bu
Several \f(CW\*(C`pack\*(C'/\f(CW\*(C`unpack\*(C' bug fixes:

> 
- \(bu
Checksums with \f(CW\*(C`b\*(C' or \f(CW\*(C`B\*(C' formats were broken.

- \(bu
\f(CW\*(C`unpack\*(C' checksums could overflow with the \f(CW\*(C`C\*(C' format.

- \(bu
\f(CW\*(C`U0\*(C' and \f(CW\*(C`C0\*(C' are now scoped to \f(CW\*(C`()\*(C' \f(CW\*(C`pack\*(C' sub-templates.

- \(bu
Counted length prefixes now don't change \f(CW\*(C`C0\*(C'/\f(CW\*(C`U0\*(C' mode.

- \(bu
\f(CW\*(C`pack\*(C' \f(CW\*(C`Z0\*(C' used to destroy the preceding character.

- \(bu
\f(CW\*(C`P\*(C'/\f(CW\*(C`p\*(C' \f(CW\*(C`pack\*(C' formats used to only recognise literal \f(CW\*(C`undef\*(C'



> 


- \(bu
Using closures with ithreads could cause perl to crash. This was due to
failure to correctly lock internal \s-1OP\s0 structures, and has been fixed.

- \(bu
The return value of \f(CW\*(C`close\*(C' now correctly reflects any file errors that
occur while flushing the handle's data, instead of just giving failure if
the actual underlying file close operation failed.

- \(bu
\f(CW\*(C`not() || 1\*(C' used to segfault. \f(CW\*(C`not()\*(C' now behaves like \f(CWnot(0), which was
the pre 5.6.0 behaviour.

- \(bu
\f(CW\*(C`h2ph\*(C' has various enhancements to cope with constructs in header files that
used to result in incorrect or invalid output.

## New or Changed Diagnostics

Header "New or Changed Diagnostics"
There is a new taint error, \*(L"%ENV is aliased to \f(CW%s\*(R". This error is thrown
when taint checks are enabled and when \f(CW*ENV has been aliased, so that
\f(CW%ENV has no env-magic anymore and hence the environment cannot be verified
as taint-free.

The internals of \f(CW\*(C`pack\*(C' and \f(CW\*(C`unpack\*(C' have been updated. All legitimate
templates should work as before, but there may be some changes in the error
reported for complex failure cases. Any behaviour changes for non-error cases
are bugs, and should be reported.

## Changed Internals

Header "Changed Internals"
There has been a fair amount of refactoring of the \f(CW\*(C`C\*(C' source code, partly to
make it tidier and more maintainable. The resulting object code and the
\f(CW\*(C`perl\*(C' binary may well be smaller than 5.8.6, and hopefully faster in some
cases, but apart from this there should be no user-detectable changes.

\f(CW\*(C`$\{^UTF8LOCALE\}\*(C' has been added to give perl space access to \f(CW\*(C`PL_utf8locale\*(C'.

The size of the arenas used to allocate \s-1SV\s0 heads and most \s-1SV\s0 bodies can now
be changed at compile time. The old size was 1008 bytes, the new default size
is 4080 bytes.

## Known Problems

Header "Known Problems"
Unicode strings returned from overloaded operators can be buggy. This is a
long standing bug reported since 5.8.6 was released, but we do not yet have
a suitable fix for it.

## Platform Specific Problems

Header "Platform Specific Problems"
On \s-1UNICOS,\s0 lib/Math/BigInt/t/bigintc.t hangs burning \s-1CPU.\s0
ext/B/t/bytecode.t and ext/Socket/t/socketpair.t both fail tests.
These are unlikely to be resolved, as our valiant \s-1UNICOS\s0 porter's last
Cray is being decommissioned.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://bugs.perl.org.  There may also be
information at http://www.perl.org, the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.  You can browse and search
the Perl 5 bugs at http://bugs.perl.org/

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for exhaustive details on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
