+++
date = "2022-02-19"
keywords = ["header", "see", "also", "perlmodlib", "for", "general", "style", "issues", "related", "to", "building", "perl", "modules", "and", "classes", "as", "well", "descriptions", "of", "the", "standard", "library", "s-1cpan", "s0", "exporter", "how", "s", "import", "export", "mechanism", "works", "perlootut", "perlobj", "in-depth", "information", "on", "creating", "a", "hard-core", "reference", "document", "objects", "perlsub", "an", "explanation", "functions", "scoping", "perlxstut", "perlguts", "more", "writing", "extension"]
manpage_name = "perlmod"
manpage_section = "1"
operating_system = "macos"
manpage_format = "troff"
operating_system_version = "15.3"
author = "None Specified"
title = "perlmod(1)"
description = "There are other documents which might contain the information that youre looking for: Perls packages, namespaces, and some info on classes. Tutorial on making a new module. Best practices for making a new module. Unlike Perl 4, in which all the..."
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLMOD 1"
PERLMOD 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlmod - Perl modules (packages and symbol tables)

## DESCRIPTION

Header "DESCRIPTION"

### Is this the document you were after?

Subsection "Is this the document you were after?"
There are other documents which might contain the information that you're
looking for:

- This doc
Item "This doc"
Perl's packages, namespaces, and some info on classes.

- perlnewmod
Item "perlnewmod"
Tutorial on making a new module.

- perlmodstyle
Item "perlmodstyle"
Best practices for making a new module.

### Packages

Xref "package namespace variable, global global variable global"
Subsection "Packages"
Unlike Perl 4, in which all the variables were dynamic and shared one
global name space, causing maintainability problems, Perl 5 provides two
mechanisms for protecting code from having its variables stomped on by
other code: lexically scoped variables created with \f(CW\*(C`my\*(C' or \f(CW\*(C`state\*(C' and
namespaced global variables, which are exposed via the \f(CW\*(C`vars\*(C' pragma,
or the \f(CW\*(C`our\*(C' keyword. Any global variable is considered to
be part of a namespace and can be accessed via a \*(L"fully qualified form\*(R".
Conversely, any lexically scoped variable is considered to be part of
that lexical-scope, and does not have a \*(L"fully qualified form\*(R".

In perl namespaces are called \*(L"packages\*(R" and
the \f(CW\*(C`package\*(C' declaration tells the compiler which
namespace to prefix to \f(CW\*(C`our\*(C' variables and unqualified dynamic names.
This both protects
against accidental stomping and provides an interface for deliberately
clobbering global dynamic variables declared and used in other scopes or
packages, when that is what you want to do.

The scope of the \f(CW\*(C`package\*(C' declaration is from the
declaration itself through the end of the enclosing block, \f(CW\*(C`eval\*(C',
or file, whichever comes first (the same scope as the **my()**, **our()**, **state()**, and
**local()** operators, and also the effect
of the experimental \*(L"reference aliasing,\*(R" which may change), or until
the next \f(CW\*(C`package\*(C' declaration.  Unqualified dynamic identifiers will be in
this namespace, except for those few identifiers that, if unqualified,
default to the main package instead of the current one as described
below.  A \f(CW\*(C`package\*(C' statement affects only dynamic global
symbols, including subroutine names, and variables you've used **local()**
on, but *not* lexical variables created with **my()**, **our()** or **state()**.

Typically, a \f(CW\*(C`package\*(C' statement is the first declaration in a file
included in a program by one of the \f(CW\*(C`do\*(C', \f(CW\*(C`require\*(C', or \f(CW\*(C`use\*(C' operators.  You can
switch into a package in more than one place: \f(CW\*(C`package\*(C' has no
effect beyond specifying which symbol table the compiler will use for
dynamic symbols for the rest of that block or until the next \f(CW\*(C`package\*(C' statement.
You can refer to variables and filehandles in other packages
by prefixing the identifier with the package name and a double
colon: \f(CW$Package::Variable.  If the package name is null, the
\f(CW\*(C`main\*(C' package is assumed.  That is, \f(CW$::sail is equivalent to
\f(CW$main::sail.

The old package delimiter was a single quote, but double colon is now the
preferred delimiter, in part because it's more readable to humans, and
in part because it's more readable to **emacs** macros.  It also makes \*(C+
programmers feel like they know what's going on\*(--as opposed to using the
single quote as separator, which was there to make Ada programmers feel
like they knew what was going on.  Because the old-fashioned syntax is still
supported for backwards compatibility, if you try to use a string like
\f(CW"This is $owner\*(Aqs house", you'll be accessing \f(CW$owner::s; that is,
the \f(CW$s variable in package \f(CW\*(C`owner\*(C', which is probably not what you meant.
Use braces to disambiguate, as in \f(CW"This is $\{owner\}\*(Aqs house".
Xref ":: '"

Packages may themselves contain package separators, as in
\f(CW$OUTER::INNER::var.  This implies nothing about the order of
name lookups, however.  There are no relative packages: all symbols
are either local to the current package, or must be fully qualified
from the outer package name down.  For instance, there is nowhere
within package \f(CW\*(C`OUTER\*(C' that \f(CW$INNER::var refers to
\f(CW$OUTER::INNER::var.  \f(CW\*(C`INNER\*(C' refers to a totally
separate global package. The custom of treating package names as a
hierarchy is very strong, but the language in no way enforces it.

Only identifiers starting with letters (or underscore) are stored
in a package's symbol table.  All other symbols are kept in package
\f(CW\*(C`main\*(C', including all punctuation variables, like \f(CW$_.  In addition,
when unqualified, the identifiers \s-1STDIN, STDOUT, STDERR, ARGV,
ARGVOUT, ENV, INC,\s0 and \s-1SIG\s0 are forced to be in package \f(CW\*(C`main\*(C',
even when used for other purposes than their built-in ones.  If you
have a package called \f(CW\*(C`m\*(C', \f(CW\*(C`s\*(C', or \f(CW\*(C`y\*(C', then you can't use the
qualified form of an identifier because it would be instead interpreted
as a pattern match, a substitution, or a transliteration.
Xref "variable, punctuation"

Variables beginning with underscore used to be forced into package
main, but we decided it was more useful for package writers to be able
to use leading underscore to indicate private variables and method names.
However, variables and functions named with a single \f(CW\*(C`_\*(C', such as
\f(CW$_ and \f(CW\*(C`sub _\*(C', are still forced into the package \f(CW\*(C`main\*(C'.  See also
\*(L"The Syntax of Variable Names\*(R" in perlvar.

\f(CW\*(C`eval\*(C'ed strings are compiled in the package in which the **eval()** was
compiled.  (Assignments to \f(CW$SIG\{\}, however, assume the signal
handler specified is in the \f(CW\*(C`main\*(C' package.  Qualify the signal handler
name if you wish to have a signal handler in a package.)  For an
example, examine *perldb.pl* in the Perl library.  It initially switches
to the \f(CW\*(C`DB\*(C' package so that the debugger doesn't interfere with variables
in the program you are trying to debug.  At various points, however, it
temporarily switches back to the \f(CW\*(C`main\*(C' package to evaluate various
expressions in the context of the \f(CW\*(C`main\*(C' package (or wherever you came
from).  See perldebug.

The special symbol \f(CW\*(C`_\|_PACKAGE_\|_\*(C' contains the current package, but cannot
(easily) be used to construct variable names. After \f(CW\*(C`my($foo)\*(C' has hidden
package variable \f(CW$foo, it can still be accessed, without knowing what
package you are in, as \f(CW\*(C`$\{_\|_PACKAGE_\|_.\*(Aq::foo\*(Aq\}\*(C'.

See perlsub for other scoping issues related to **my()** and **local()**,
and perlref regarding closures.

### Symbol Tables

Xref "symbol table stash %:: %main:: typeglob glob alias"
Subsection "Symbol Tables"
The symbol table for a package happens to be stored in the hash of that
name with two colons appended.  The main symbol table's name is thus
\f(CW%main::, or \f(CW%:: for short.  Likewise the symbol table for the nested
package mentioned earlier is named \f(CW%OUTER::INNER::.

The value in each entry of the hash is what you are referring to when you
use the \f(CW*name typeglob notation.

.Vb 1
    local *main::foo    = *main::bar;
.Ve

You can use this to print out all the variables in a package, for
instance.  The standard but antiquated *dumpvar.pl* library and
the \s-1CPAN\s0 module Devel::Symdump make use of this.

The results of creating new symbol table entries directly or modifying any
entries that are not already typeglobs are undefined and subject to change
between releases of perl.

Assignment to a typeglob performs an aliasing operation, i.e.,

.Vb 1
    *dick = *richard;
.Ve

causes variables, subroutines, formats, and file and directory handles
accessible via the identifier \f(CW\*(C`richard\*(C' also to be accessible via the
identifier \f(CW\*(C`dick\*(C'.  If you want to alias only a particular variable or
subroutine, assign a reference instead:

.Vb 1
    *dick = \\$richard;
.Ve

Which makes \f(CW$richard and \f(CW$dick the same variable, but leaves
\f(CW@richard and \f(CW@dick as separate arrays.  Tricky, eh?

There is one subtle difference between the following statements:

.Vb 2
    *foo = *bar;
    *foo = \\$bar;
.Ve

\f(CW\*(C`*foo = *bar\*(C' makes the typeglobs themselves synonymous while
\f(CW\*(C`*foo = \\$bar\*(C' makes the \s-1SCALAR\s0 portions of two distinct typeglobs
refer to the same scalar value. This means that the following code:

.Vb 2
    $bar = 1;
    *foo = \\$bar;       # Make $foo an alias for $bar

    \{
        local $bar = 2; # Restrict changes to block
        print $foo;     # Prints \*(Aq1\*(Aq!
    \}
.Ve

Would print '1', because \f(CW$foo holds a reference to the *original*
\f(CW$bar. The one that was stuffed away by \f(CW\*(C`local()\*(C' and which will be
restored when the block ends. Because variables are accessed through the
typeglob, you can use \f(CW\*(C`*foo = *bar\*(C' to create an alias which can be
localized. (But be aware that this means you can't have a separate
\f(CW@foo and \f(CW@bar, etc.)

What makes all of this important is that the Exporter module uses glob
aliasing as the import/export mechanism. Whether or not you can properly
localize a variable that has been exported from a module depends on how
it was exported:

.Vb 2
    @EXPORT = qw($FOO); # Usual form, can\*(Aqt be localized
    @EXPORT = qw(*FOO); # Can be localized
.Ve

You can work around the first case by using the fully qualified name
(\f(CW$Package::FOO) where you need a local value, or by overriding it
by saying \f(CW\*(C`*FOO = *Package::FOO\*(C' in your script.

The \f(CW\*(C`*x = \\$y\*(C' mechanism may be used to pass and return cheap references
into or from subroutines if you don't want to copy the whole
thing.  It only works when assigning to dynamic variables, not
lexicals.

.Vb 9
    %some_hash = ();                    # can\*(Aqt be my()
    *some_hash = fn( \\%another_hash );
    sub fn \{
        local *hashsym = shift;
        # now use %hashsym normally, and you
        # will affect the caller\*(Aqs %another_hash
        my %nhash = (); # do what you want
        return \\%nhash;
    \}
.Ve

On return, the reference will overwrite the hash slot in the
symbol table specified by the *some_hash typeglob.  This
is a somewhat tricky way of passing around references cheaply
when you don't want to have to remember to dereference variables
explicitly.

Another use of symbol tables is for making \*(L"constant\*(R" scalars.
Xref "constant scalar, constant"

.Vb 1
    *PI = \\3.14159265358979;
.Ve

Now you cannot alter \f(CW$PI, which is probably a good thing all in all.
This isn't the same as a constant subroutine, which is subject to
optimization at compile-time.  A constant subroutine is one prototyped
to take no arguments and to return a constant expression.  See
perlsub for details on these.  The \f(CW\*(C`use constant\*(C' pragma is a
convenient shorthand for these.

You can say \f(CW*foo\{PACKAGE\} and \f(CW*foo\{NAME\} to find out what name and
package the *foo symbol table entry comes from.  This may be useful
in a subroutine that gets passed typeglobs as arguments:

.Vb 7
    sub identify_typeglob \{
        my $glob = shift;
        print \*(AqYou gave me \*(Aq, *\{$glob\}\{PACKAGE\},
            \*(Aq::\*(Aq, *\{$glob\}\{NAME\}, "\\n";
    \}
    identify_typeglob *foo;
    identify_typeglob *bar::baz;
.Ve

This prints

.Vb 2
    You gave me main::foo
    You gave me bar::baz
.Ve

The \f(CW*foo\{THING\} notation can also be used to obtain references to the
individual elements of *foo.  See perlref.

Subroutine definitions (and declarations, for that matter) need
not necessarily be situated in the package whose symbol table they
occupy.  You can define a subroutine outside its package by
explicitly qualifying the name of the subroutine:

.Vb 2
    package main;
    sub Some_package::foo \{ ... \}   # &foo defined in Some_package
.Ve

This is just a shorthand for a typeglob assignment at compile time:

.Vb 1
    BEGIN \{ *Some_package::foo = sub \{ ... \} \}
.Ve

and is *not* the same as writing:

.Vb 4
    \{
        package Some_package;
        sub foo \{ ... \}
    \}
.Ve

In the first two versions, the body of the subroutine is
lexically in the main package, *not* in Some_package. So
something like this:

.Vb 1
    package main;

    $Some_package::name = "fred";
    $main::name = "barney";

    sub Some_package::foo \{
        print "in ", _\|_PACKAGE_\|_, ": \\$name is \*(Aq$name\*(Aq\\n";
    \}

    Some_package::foo();
.Ve

prints:

.Vb 1
    in main: $name is \*(Aqbarney\*(Aq
.Ve

rather than:

.Vb 1
    in Some_package: $name is \*(Aqfred\*(Aq
.Ve

This also has implications for the use of the \s-1SUPER::\s0 qualifier
(see perlobj).

### \s-1BEGIN, UNITCHECK, CHECK, INIT\s0 and \s-1END\s0

Xref "BEGIN UNITCHECK CHECK INIT END"
Subsection "BEGIN, UNITCHECK, CHECK, INIT and END"
Five specially named code blocks are executed at the beginning and at
the end of a running Perl program.  These are the \f(CW\*(C`BEGIN\*(C',
\f(CW\*(C`UNITCHECK\*(C', \f(CW\*(C`CHECK\*(C', \f(CW\*(C`INIT\*(C', and \f(CW\*(C`END\*(C' blocks.

These code blocks can be prefixed with \f(CW\*(C`sub\*(C' to give the appearance of a
subroutine (although this is not considered good style).  One should note
that these code blocks don't really exist as named subroutines (despite
their appearance). The thing that gives this away is the fact that you can
have **more than one** of these code blocks in a program, and they will get
**all** executed at the appropriate moment.  So you can't execute any of
these code blocks by name.

A \f(CW\*(C`BEGIN\*(C' code block is executed as soon as possible, that is, the moment
it is completely defined, even before the rest of the containing file (or
string) is parsed.  You may have multiple \f(CW\*(C`BEGIN\*(C' blocks within a file (or
eval'ed string); they will execute in order of definition.  Because a \f(CW\*(C`BEGIN\*(C'
code block executes immediately, it can pull in definitions of subroutines
and such from other files in time to be visible to the rest of the compile
and run time.  Once a \f(CW\*(C`BEGIN\*(C' has run, it is immediately undefined and any
code it used is returned to Perl's memory pool.

An \f(CW\*(C`END\*(C' code block is executed as late as possible, that is, after
perl has finished running the program and just before the interpreter
is being exited, even if it is exiting as a result of a **die()** function.
(But not if it's morphing into another program via \f(CW\*(C`exec\*(C', or
being blown out of the water by a signal\*(--you have to trap that yourself
(if you can).)  You may have multiple \f(CW\*(C`END\*(C' blocks within a file\*(--they
will execute in reverse order of definition; that is: last in, first
out (\s-1LIFO\s0).  \f(CW\*(C`END\*(C' blocks are not executed when you run perl with the
\f(CW\*(C`-c\*(C' switch, or if compilation fails.

Note that \f(CW\*(C`END\*(C' code blocks are **not** executed at the end of a string
\f(CW\*(C`eval()\*(C': if any \f(CW\*(C`END\*(C' code blocks are created in a string \f(CW\*(C`eval()\*(C',
they will be executed just as any other \f(CW\*(C`END\*(C' code block of that package
in \s-1LIFO\s0 order just before the interpreter is being exited.

Inside an \f(CW\*(C`END\*(C' code block, \f(CW$? contains the value that the program is
going to pass to \f(CW\*(C`exit()\*(C'.  You can modify \f(CW$? to change the exit
value of the program.  Beware of changing \f(CW$? by accident (e.g. by
running something via \f(CW\*(C`system\*(C').
Xref "$?"

Inside of a \f(CW\*(C`END\*(C' block, the value of \f(CW\*(C`$\{^GLOBAL_PHASE\}\*(C' will be
\f(CW"END".

\f(CW\*(C`UNITCHECK\*(C', \f(CW\*(C`CHECK\*(C' and \f(CW\*(C`INIT\*(C' code blocks are useful to catch the
transition between the compilation phase and the execution phase of
the main program.

\f(CW\*(C`UNITCHECK\*(C' blocks are run just after the unit which defined them has
been compiled.  The main program file and each module it loads are
compilation units, as are string \f(CW\*(C`eval\*(C's, run-time code compiled using the
\f(CW\*(C`(?\{ \})\*(C' construct in a regex, calls to \f(CW\*(C`do FILE\*(C', \f(CW\*(C`require FILE\*(C',
and code after the \f(CW\*(C`-e\*(C' switch on the command line.

\f(CW\*(C`BEGIN\*(C' and \f(CW\*(C`UNITCHECK\*(C' blocks are not directly related to the phase of
the interpreter.  They can be created and executed during any phase.

\f(CW\*(C`CHECK\*(C' code blocks are run just after the **initial** Perl compile phase ends
and before the run time begins, in \s-1LIFO\s0 order.  \f(CW\*(C`CHECK\*(C' code blocks are used
in the Perl compiler suite to save the compiled state of the program.

Inside of a \f(CW\*(C`CHECK\*(C' block, the value of \f(CW\*(C`$\{^GLOBAL_PHASE\}\*(C' will be
\f(CW"CHECK".

\f(CW\*(C`INIT\*(C' blocks are run just before the Perl runtime begins execution, in
\*(L"first in, first out\*(R" (\s-1FIFO\s0) order.

Inside of an \f(CW\*(C`INIT\*(C' block, the value of \f(CW\*(C`$\{^GLOBAL_PHASE\}\*(C' will be \f(CW"INIT".

The \f(CW\*(C`CHECK\*(C' and \f(CW\*(C`INIT\*(C' blocks in code compiled by \f(CW\*(C`require\*(C', string \f(CW\*(C`do\*(C',
or string \f(CW\*(C`eval\*(C' will not be executed if they occur after the end of the
main compilation phase; that can be a problem in mod_perl and other persistent
environments which use those functions to load code at runtime.

When you use the **-n** and **-p** switches to Perl, \f(CW\*(C`BEGIN\*(C' and
\f(CW\*(C`END\*(C' work just as they do in **awk**, as a degenerate case.
Both \f(CW\*(C`BEGIN\*(C' and \f(CW\*(C`CHECK\*(C' blocks are run when you use the **-c**
switch for a compile-only syntax check, although your main code
is not.

The **begincheck** program makes it all clear, eventually:

.Vb 1
  #!/usr/bin/perl

  # begincheck

  print         "10. Ordinary code runs at runtime.\\n";

  END \{ print   "16.   So this is the end of the tale.\\n" \}
  INIT \{ print  " 7. INIT blocks run FIFO just before runtime.\\n" \}
  UNITCHECK \{
    print       " 4.   And therefore before any CHECK blocks.\\n"
  \}
  CHECK \{ print " 6.   So this is the sixth line.\\n" \}

  print         "11.   It runs in order, of course.\\n";

  BEGIN \{ print " 1. BEGIN blocks run FIFO during compilation.\\n" \}
  END \{ print   "15.   Read perlmod for the rest of the story.\\n" \}
  CHECK \{ print " 5. CHECK blocks run LIFO after all compilation.\\n" \}
  INIT \{ print  " 8.   Run this again, using Perl\*(Aqs -c switch.\\n" \}

  print         "12.   This is anti-obfuscated code.\\n";

  END \{ print   "14. END blocks run LIFO at quitting time.\\n" \}
  BEGIN \{ print " 2.   So this line comes out second.\\n" \}
  UNITCHECK \{
   print " 3. UNITCHECK blocks run LIFO after each file is compiled.\\n"
  \}
  INIT \{ print  " 9.   You\*(Aqll see the difference right away.\\n" \}

  print         "13.   It only _looks_ like it should be confusing.\\n";

  _\|_END_\|_
.Ve

### Perl Classes

Xref "class @ISA"
Subsection "Perl Classes"
There is no special class syntax in Perl, but a package may act
as a class if it provides subroutines to act as methods.  Such a
package may also derive some of its methods from another class (package)
by listing the other package name(s) in its global \f(CW@ISA array (which
must be a package global, not a lexical).

For more on this, see perlootut and perlobj.

### Perl Modules

Xref "module"
Subsection "Perl Modules"
A module is just a set of related functions in a library file, i.e.,
a Perl package with the same name as the file.  It is specifically
designed to be reusable by other modules or programs.  It may do this
by providing a mechanism for exporting some of its symbols into the
symbol table of any package using it, or it may function as a class
definition and make its semantics available implicitly through
method calls on the class and its objects, without explicitly
exporting anything.  Or it can do a little of both.

For example, to start a traditional, non-OO module called Some::Module,
create a file called *Some/Module.pm* and start with this template:

.Vb 1
    package Some::Module;  # assumes Some/Module.pm

    use strict;
    use warnings;

    # Get the import method from Exporter to export functions and
    # variables
    use Exporter 5.57 \*(Aqimport\*(Aq;

    # set the version for version checking
    our $VERSION     = \*(Aq1.00\*(Aq;

    # Functions and variables which are exported by default
    our @EXPORT      = qw(func1 func2);

    # Functions and variables which can be optionally exported
    our @EXPORT_OK   = qw($Var1 %Hashit func3);

    # exported package globals go here
    our $Var1    = \*(Aq\*(Aq;
    our %Hashit  = ();

    # non-exported package globals go here
    # (they are still accessible as $Some::Module::stuff)
    our @more    = ();
    our $stuff   = \*(Aq\*(Aq;

    # file-private lexicals go here, before any functions which use them
    my $priv_var    = \*(Aq\*(Aq;
    my %secret_hash = ();

    # here\*(Aqs a file-private function as a closure,
    # callable as $priv_func->();
    my $priv_func = sub \{
        ...
    \};

    # make all your functions, whether exported or not;
    # remember to put something interesting in the \{\} stubs
    sub func1      \{ ... \}
    sub func2      \{ ... \}

    # this one isn\*(Aqt always exported, but could be called directly
    # as Some::Module::func3()
    sub func3      \{ ... \}

    END \{ ... \}       # module clean-up code here (global destructor)

    1;  # don\*(Aqt forget to return a true value from the file
.Ve

Then go on to declare and use your variables in functions without
any qualifications.  See Exporter and the perlmodlib for
details on mechanics and style issues in module creation.

Perl modules are included into your program by saying

.Vb 1
    use Module;
.Ve

or

.Vb 1
    use Module LIST;
.Ve

This is exactly equivalent to

.Vb 1
    BEGIN \{ require \*(AqModule.pm\*(Aq; \*(AqModule\*(Aq->import; \}
.Ve

or

.Vb 1
    BEGIN \{ require \*(AqModule.pm\*(Aq; \*(AqModule\*(Aq->import( LIST ); \}
.Ve

As a special case

.Vb 1
    use Module ();
.Ve

is exactly equivalent to

.Vb 1
    BEGIN \{ require \*(AqModule.pm\*(Aq; \}
.Ve

All Perl module files have the extension *.pm*.  The \f(CW\*(C`use\*(C' operator
assumes this so you don't have to spell out "*Module.pm*" in quotes.
This also helps to differentiate new modules from old *.pl* and
*.ph* files.  Module names are also capitalized unless they're
functioning as pragmas; pragmas are in effect compiler directives,
and are sometimes called \*(L"pragmatic modules\*(R" (or even \*(L"pragmata\*(R"
if you're a classicist).

The two statements:

.Vb 2
    require SomeModule;
    require "SomeModule.pm";
.Ve

differ from each other in two ways.  In the first case, any double
colons in the module name, such as \f(CW\*(C`Some::Module\*(C', are translated
into your system's directory separator, usually \*(L"/\*(R".   The second
case does not, and would have to be specified literally.  The other
difference is that seeing the first \f(CW\*(C`require\*(C' clues in the compiler
that uses of indirect object notation involving \*(L"SomeModule\*(R", as
in \f(CW\*(C`$ob = purge SomeModule\*(C', are method calls, not function calls.
(Yes, this really can make a difference.)

Because the \f(CW\*(C`use\*(C' statement implies a \f(CW\*(C`BEGIN\*(C' block, the importing
of semantics happens as soon as the \f(CW\*(C`use\*(C' statement is compiled,
before the rest of the file is compiled.  This is how it is able
to function as a pragma mechanism, and also how modules are able to
declare subroutines that are then visible as list or unary operators for
the rest of the current file.  This will not work if you use \f(CW\*(C`require\*(C'
instead of \f(CW\*(C`use\*(C'.  With \f(CW\*(C`require\*(C' you can get into this problem:

.Vb 2
    require Cwd;                # make Cwd:: accessible
    $here = Cwd::getcwd();

    use Cwd;                    # import names from Cwd::
    $here = getcwd();

    require Cwd;                # make Cwd:: accessible
    $here = getcwd();           # oops! no main::getcwd()
.Ve

In general, \f(CW\*(C`use Module ()\*(C' is recommended over \f(CW\*(C`require Module\*(C',
because it determines module availability at compile time, not in the
middle of your program's execution.  An exception would be if two modules
each tried to \f(CW\*(C`use\*(C' each other, and each also called a function from
that other module.  In that case, it's easy to use \f(CW\*(C`require\*(C' instead.

Perl packages may be nested inside other package names, so we can have
package names containing \f(CW\*(C`::\*(C'.  But if we used that package name
directly as a filename it would make for unwieldy or impossible
filenames on some systems.  Therefore, if a module's name is, say,
\f(CW\*(C`Text::Soundex\*(C', then its definition is actually found in the library
file *Text/Soundex.pm*.

Perl modules always have a *.pm* file, but there may also be
dynamically linked executables (often ending in *.so*) or autoloaded
subroutine definitions (often ending in *.al*) associated with the
module.  If so, these will be entirely transparent to the user of
the module.  It is the responsibility of the *.pm* file to load
(or arrange to autoload) any additional functionality.  For example,
although the \s-1POSIX\s0 module happens to do both dynamic loading and
autoloading, the user can say just \f(CW\*(C`use POSIX\*(C' to get it all.

### Making your module threadsafe

Xref "threadsafe thread safe module, threadsafe module, thread safe CLONE CLONE_SKIP thread threads ithread"
Subsection "Making your module threadsafe"
Perl supports a type of threads called interpreter threads (ithreads).
These threads can be used explicitly and implicitly.

Ithreads work by cloning the data tree so that no data is shared
between different threads. These threads can be used by using the \f(CW\*(C`threads\*(C'
module or by doing **fork()** on win32 (fake **fork()** support). When a
thread is cloned all Perl data is cloned, however non-Perl data cannot
be cloned automatically.  Perl after 5.8.0 has support for the \f(CW\*(C`CLONE\*(C'
special subroutine.  In \f(CW\*(C`CLONE\*(C' you can do whatever
you need to do,
like for example handle the cloning of non-Perl data, if necessary.
\f(CW\*(C`CLONE\*(C' will be called once as a class method for every package that has it
defined (or inherits it).  It will be called in the context of the new thread,
so all modifications are made in the new area.  Currently \s-1CLONE\s0 is called with
no parameters other than the invocant package name, but code should not assume
that this will remain unchanged, as it is likely that in future extra parameters
will be passed in to give more information about the state of cloning.

If you want to \s-1CLONE\s0 all objects you will need to keep track of them per
package. This is simply done using a hash and **Scalar::Util::weaken()**.

Perl after 5.8.7 has support for the \f(CW\*(C`CLONE_SKIP\*(C' special subroutine.
Like \f(CW\*(C`CLONE\*(C', \f(CW\*(C`CLONE_SKIP\*(C' is called once per package; however, it is
called just before cloning starts, and in the context of the parent
thread. If it returns a true value, then no objects of that class will
be cloned; or rather, they will be copied as unblessed, undef values.
For example: if in the parent there are two references to a single blessed
hash, then in the child there will be two references to a single undefined
scalar value instead.
This provides a simple mechanism for making a module threadsafe; just add
\f(CW\*(C`sub CLONE_SKIP \{ 1 \}\*(C' at the top of the class, and \f(CW\*(C`DESTROY()\*(C' will
now only be called once per object. Of course, if the child thread needs
to make use of the objects, then a more sophisticated approach is
needed.

Like \f(CW\*(C`CLONE\*(C', \f(CW\*(C`CLONE_SKIP\*(C' is currently called with no parameters other
than the invocant package name, although that may change. Similarly, to
allow for future expansion, the return value should be a single \f(CW0 or
\f(CW1 value.

## SEE ALSO

Header "SEE ALSO"
See perlmodlib for general style issues related to building Perl
modules and classes, as well as descriptions of the standard library
and \s-1CPAN,\s0 Exporter for how Perl's standard import/export mechanism
works, perlootut and perlobj for in-depth information on
creating classes, perlobj for a hard-core reference document on
objects, perlsub for an explanation of functions and scoping,
and perlxstut and perlguts for more information on writing
extension modules.
