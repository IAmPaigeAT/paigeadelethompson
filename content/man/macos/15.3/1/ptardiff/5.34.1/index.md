+++
manpage_format = "troff"
author = "None Specified"
manpage_name = "ptardiff"
date = "2024-12-14"
operating_system = "macos"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "fbtar", "1", "archive", "tar"]
title = "ptardiff(1)"
manpage_section = "1"
description = "    ptardiff is a small program that diffs an extracted archive     against an unextracted one, using the perl module Archive::Tar.      This effectively lets you view changes made to an archives contents.      Provide the progam with an ARCHIVE_..."
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PTARDIFF 1"
PTARDIFF 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

ptardiff - program that diffs an extracted archive against an unextracted one

## DESCRIPTION

Header "DESCRIPTION"
.Vb 2
    ptardiff is a small program that diffs an extracted archive
    against an unextracted one, using the perl module Archive::Tar.

    This effectively lets you view changes made to an archives contents.

    Provide the progam with an ARCHIVE_FILE and it will look up all
    the files with in the archive, scan the current working directory
    for a file with the name and diff it against the contents of the
    archive.
.Ve

## SYNOPSIS

Header "SYNOPSIS"
.Vb 2
    ptardiff ARCHIVE_FILE
    ptardiff -h

    $ tar -xzf Acme-Buffy-1.3.tar.gz
    $ vi Acme-Buffy-1.3/README
    [...]
    $ ptardiff Acme-Buffy-1.3.tar.gz > README.patch
.Ve

## OPTIONS

Header "OPTIONS"
.Vb 1
    h   Prints this help message
.Ve

## SEE ALSO

Header "SEE ALSO"
**tar**\|(1), Archive::Tar.
