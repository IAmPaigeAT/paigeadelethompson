+++
manpage_name = "treereg"
description = "f(CW*(C`Treereg*(C translates a tree grammar specification file (default extension f(CW*(C`.trg*(C describing  a set of tree patterns and the actions to modify them  using tree-terms like:   TIMES(NUM, $x) and { $NUM->{VAL} == 0) => { $NUM } w..."
operating_system = "macos"
keywords = ["header", "see", "also", "bu", "4", "parse", "eyapp", "eyapptut", "the", "pdf", "file", "in", "http", "nereida", "deioc", "ull", "es", "pl", "perlexamples", "section_eyappts", "html", "spanish", "treereg", "yapp", "fbyacc", "1", "fbbison", "classic", "book", "l", "compilers", "principles", "techniques", "and", "tools", "r", "by", "alfred", "v", "aho", "ravi", "sethi", "jeffrey", "d", "ullman", "addison-wesley", "1986", "recdescent", "author", "casiano", "rodriguez-leon", "license", "copyright", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2017", "william", "n", "braswell", "jr", "all", "rights", "reserved", "is", "1998", "1999", "2000", "2001", "francois", "desarmenien", "this", "library", "free", "software", "you", "can", "redistribute", "it", "or", "modify", "under", "same", "terms", "as", "perl", "itself", "either", "version", "5", "8", "at", "your", "option", "any", "later", "of", "may", "have", "available", "pod", "errors", "hey", "fbthe", "above", "document", "had", "some", "coding", "which", "are", "explained", "below", "around", "line", "416", "item", "non-ascii", "character", "seen", "before", "encoding", "assuming", "s-1utf-8", "s0"]
date = "2017-06-14"
detected_package_version = "5.34.0"
author = "None Specified"
manpage_format = "troff"
manpage_section = "1"
operating_system_version = "15.3"
title = "treereg(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "TREEREG 1"
TREEREG 1 "2017-06-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

treereg - Compiler for Tree Regular Expressions

## SYNOPSIS

Header "SYNOPSIS"
.Vb 6
  treereg [-m packagename] [[no]syntax] [[no]numbers] [-severity 0|1|2|3] \\
          [-p treeprefix] [-o outputfile] [-lib /path/to/library/] -i filename[.trg]
  treereg [-m packagename] [[no]syntax] [[no]numbers] [-severity 0|1|2|3] \\
          [-p treeprefix] [-lib /path/to/library/] [-o outputfile] filename[.trg]
  treereg -v
  treereg -h
.Ve

## OPTIONS

Header "OPTIONS"
Options can be used both with one dash and double dash.
It is not necessary to write the full name of the option.
A disambiguation prefix suffices.

- \(bu
\f(CW\*(C`-i[n] filename\*(C'
.Sp
Input file. Extension \f(CW\*(C`.trg\*(C' is assumed if no extension is provided.

- \(bu
\f(CW\*(C`-o[ut] filename\*(C'
.Sp
Output file. By default is the name of the input file (concatenated with .pm)

- \(bu
\f(CW\*(C`-m[od] packagename\*(C'
.Sp
Name of the package containing the generated subroutines.
By default is the longest prefix of the input file name that
conforms to the classic definition of integer \f(CW\*(C`[a-z_A-Z]\\w*\*(C'.

- \(bu
\f(CW\*(C`-l[ib] /path/to/library/\*(C'
.Sp
Specifies that \f(CW\*(C`/path/to/library/\*(C' will be included in \f(CW@INC.
Useful when the \f(CW\*(C`syntax\*(C' option is on. Can be inserted as many times as necessary.

- \(bu
\f(CW\*(C`-p[refix] treeprefix\*(C'
.Sp
Tree nodes automatically generated using \f(CW\*(C`Parse::Eyapp\*(C' are objects blessed
into the name of the production. To avoid crashes the programmer may prefix
the class names with a given prefix when calling the parser; for example:
.Sp
.Vb 1
  $self->YYParse( yylex => \_Lexer, yyerror => \_Error, yyprefix => _\|_PACKAGE_\|_."::")
.Ve
.Sp
The \f(CW\*(C`-prefix treeprefix\*(C' option simplifies the process of writing the tree
grammar so that instead of writing with the full names
.Sp
.Vb 1
 CLASS::TIMES(CLASS::NUM, $x) and \{ $NUM->\{VAL\} == 0) => \{ $NUM \}
.Ve
.Sp
it can be written:
.Sp
.Vb 1
 TIMES(NUM, $x) and \{ $NUM->\{VAL\} == 0) => \{ $NUM \}
.Ve

- \(bu
\f(CW\*(C`-n[umbers]\*(C'
.Sp
Produces \f(CW\*(C`#line\*(C' directives.

- \(bu
\f(CW\*(C`-non[umbers]\*(C'
.Sp
Disable source file line numbering embedded in your parser

- \(bu
\f(CW\*(C`-sy[ntax]\*(C'
.Sp
Checks that Perl code is syntactically correct.

- \(bu
\f(CW\*(C`-nosy[ntax]\*(C'
.Sp
Does not check the syntax of Perl code

- \(bu
\f(CW\*(C`-se[verity] number\*(C'

> 
- - 0 = Don't  check arity (default). Matching does not check the arity. The actual node being visited may have more children.
Item "- 0 = Don't check arity (default). Matching does not check the arity. The actual node being visited may have more children."
0

- - 1 = Check arity. Matching requires the equality of the number of children and the actual node and the pattern.
Item "- 1 = Check arity. Matching requires the equality of the number of children and the actual node and the pattern."

- - 2 = Check arity and give a warning
Item "- 2 = Check arity and give a warning"

- - 3 = Check arity, give a warning and exit
Item "- 3 = Check arity, give a warning and exit"



> 


- \(bu
.PD
\f(CW\*(C`-v[ersion]\*(C'
.Sp
Gives the version

- \(bu
\f(CW\*(C`-u[sage]\*(C'
.Sp
Prints the usage info

- \(bu
\f(CW\*(C`-h[elp]\*(C'
.Sp
Print this help

## DESCRIPTION

Header "DESCRIPTION"
\f(CW\*(C`Treereg\*(C' translates a tree grammar specification
file (default extension \f(CW\*(C`.trg\*(C' describing
a set of tree patterns
and the actions to modify them
using tree-terms like:

.Vb 1
  TIMES(NUM, $x) and \{ $NUM->\{VAL\} == 0) => \{ $NUM \}
.Ve

which says that wherever an abstract syntax tree representing
the product of a numeric expression with value 0 times
any other kind of expression, the \f(CW\*(C`TIMES\*(C' tree can be substituted by
its left child.

The compiler produces a Perl module containing the subroutines
implementing those sets of pattern-actions.

## EXAMPLE

Header "EXAMPLE"
Consider the following \f(CW\*(C`eyapp\*(C' grammar (see the \f(CW\*(C`Parse::Eyapp\*(C' documentation
to know more about \f(CW\*(C`Parse::Eyapp\*(C' grammars):

.Vb 10
  ----------------------------------------------------------
  nereida:~/LEyapp/examples> cat Rule6.yp
  %\{
  use Data::Dumper;
  %\}
  %right  \*(Aq=\*(Aq
  %left   \*(Aq-\*(Aq \*(Aq+\*(Aq
  %left   \*(Aq*\*(Aq \*(Aq/\*(Aq
  %left   NEG
  %tree

  %%
  line: exp  \{ $_[1] \}
  ;

  exp:      %name NUM
              NUM
          | %name VAR
            VAR
          | %name ASSIGN
            VAR \*(Aq=\*(Aq exp
          | %name PLUS
            exp \*(Aq+\*(Aq exp
          | %name MINUS
            exp \*(Aq-\*(Aq exp
          | %name TIMES
            exp \*(Aq*\*(Aq exp
          | %name DIV
            exp \*(Aq/\*(Aq exp
          | %name UMINUS
            \*(Aq-\*(Aq exp %prec NEG
          |   \*(Aq(\*(Aq exp \*(Aq)\*(Aq  \{ $_[2] \} /* Let us simplify a bit the tree */
  ;

  %%

  sub _Error \{
      die  "Syntax error.\\n";
  \}

  sub _Lexer \{
      my($parser)=shift;

          $parser->YYData->\{INPUT\}
      or  $parser->YYData->\{INPUT\} = <STDIN>
      or  return(\*(Aq\*(Aq,undef);

      $parser->YYData->\{INPUT\}=~s/^\\s+//;

      for ($parser->YYData->\{INPUT\}) \{
          s/^([0-9]+(?:\\.[0-9]+)?)// and return(\*(AqNUM\*(Aq,$1);
          s/^([A-Za-z][A-Za-z0-9_]*)// and return(\*(AqVAR\*(Aq,$1);
          s/^(.)//s and return($1,$1);
      \}
  \}

  sub Run \{
      my($self)=shift;
      $self->YYParse( yylex => \_Lexer, yyerror => \_Error );
  \}
  ----------------------------------------------------------
.Ve

Compile it using \f(CW\*(C`eyapp\*(C':

.Vb 5
  ----------------------------------------------------------
  nereida:~/LEyapp/examples> eyapp Rule6.yp
  nereida:~/LEyapp/examples> ls -ltr | tail -1
  -rw-rw----  1 pl users  4976 2006-09-15 19:56 Rule6.pm
  ----------------------------------------------------------
.Ve

Now consider this tree grammar:

.Vb 5
  ----------------------------------------------------------
  nereida:~/LEyapp/examples> cat Transform2.trg
  %\{
  my %Op = (PLUS=>\*(Aq+\*(Aq, MINUS => \*(Aq-\*(Aq, TIMES=>\*(Aq*\*(Aq, DIV => \*(Aq/\*(Aq);
  %\}

  fold: \*(AqTIMES|PLUS|DIV|MINUS\*(Aq:bin(NUM($n), NUM($m))
    => \{
      my $op = $Op\{ref($bin)\};
      $n->\{attr\} = eval  "$n->\{attr\} $op $m->\{attr\}";
      $_[0] = $NUM[0];
    \}
  zero_times_whatever: TIMES(NUM($x), .) and \{ $x->\{attr\} == 0 \} => \{ $_[0] = $NUM \}
  whatever_times_zero: TIMES(., NUM($x)) and \{ $x->\{attr\} == 0 \} => \{ $_[0] = $NUM \}

  /* rules related with times */
  times_zero = zero_times_whatever whatever_times_zero;
  ----------------------------------------------------------
.Ve

Compile it with \f(CW\*(C`treereg\*(C':

.Vb 5
  ----------------------------------------------------------
  nereida:~/LEyapp/examples> treereg Transform2.trg
  nereida:~/LEyapp/examples> ls -ltr | tail -1
  -rw-rw----  1 pl users  1948 2006-09-15 19:57 Transform2.pm
  ----------------------------------------------------------
.Ve

The following program makes use of both modules \f(CW\*(C`Rule6.pm\*(C'
and \f(CW\*(C`Transform2.pm\*(C':

.Vb 8
  ----------------------------------------------------------
  nereida:~/LEyapp/examples> cat foldand0rule6_3.pl
  #!/usr/bin/perl -w
  use strict;
  use Rule6;
  use Parse::Eyapp::YATW;
  use Data::Dumper;
  use Transform2;

  $Data::Dumper::Indent = 1;
  my $parser = new Rule6();
  my $t = $parser->Run;
  print "\\n***** Before ******\\n";
  print Dumper($t);
  $t->s(@Transform2::all);
  print "\\n***** After ******\\n";
  print Dumper($t);
  ----------------------------------------------------------
.Ve

When the program runs with input \f(CW\*(C`b*(2-2)\*(C' produces the following output:

.Vb 3
  ----------------------------------------------------------
  nereida:~/LEyapp/examples> foldand0rule6_3.pl
  b*(2-2)

  ***** Before ******
  $VAR1 = bless( \{
    \*(Aqchildren\*(Aq => [
      bless( \{
        \*(Aqchildren\*(Aq => [
          bless( \{ \*(Aqchildren\*(Aq => [], \*(Aqattr\*(Aq => \*(Aqb\*(Aq, \*(Aqtoken\*(Aq => \*(AqVAR\*(Aq \}, \*(AqTERMINAL\*(Aq )
        ]
      \}, \*(AqVAR\*(Aq ),
      bless( \{
        \*(Aqchildren\*(Aq => [
          bless( \{ \*(Aqchildren\*(Aq => [
              bless( \{ \*(Aqchildren\*(Aq => [], \*(Aqattr\*(Aq => \*(Aq2\*(Aq, \*(Aqtoken\*(Aq => \*(AqNUM\*(Aq \}, \*(AqTERMINAL\*(Aq )
            ]
          \}, \*(AqNUM\*(Aq ),
          bless( \{
            \*(Aqchildren\*(Aq => [
              bless( \{ \*(Aqchildren\*(Aq => [], \*(Aqattr\*(Aq => \*(Aq2\*(Aq, \*(Aqtoken\*(Aq => \*(AqNUM\*(Aq \}, \*(AqTERMINAL\*(Aq )
            ]
          \}, \*(AqNUM\*(Aq )
        ]
      \}, \*(AqMINUS\*(Aq )
    ]
  \}, \*(AqTIMES\*(Aq );

  ***** After ******
  $VAR1 = bless( \{
    \*(Aqchildren\*(Aq => [
      bless( \{ \*(Aqchildren\*(Aq => [], \*(Aqattr\*(Aq => 0, \*(Aqtoken\*(Aq => \*(AqNUM\*(Aq \}, \*(AqTERMINAL\*(Aq )
    ]
  \}, \*(AqNUM\*(Aq );
  ----------------------------------------------------------
.Ve

See also the section \*(L"Compiling: More Options\*(R" in Parse::Eyapp  for a more
contrived example.

## SEE ALSO

Header "SEE ALSO"

- \(bu
Parse::Eyapp,

- \(bu
eyapptut

- \(bu
The pdf file in <http://nereida.deioc.ull.es/~pl/perlexamples/Eyapp.pdf>

- \(bu
<http://nereida.deioc.ull.es/~pl/perlexamples/section_eyappts.html> (Spanish),

- \(bu
eyapp,

- \(bu
treereg,

- \(bu
Parse::yapp,

- \(bu
**yacc**\|(1),

- \(bu
**bison**\|(1),

- \(bu
the classic book \*(L"Compilers: Principles, Techniques, and Tools\*(R" by Alfred V. Aho, Ravi Sethi and

- \(bu
Jeffrey D. Ullman (Addison-Wesley 1986)

- \(bu
Parse::RecDescent.

## AUTHOR

Header "AUTHOR"
Casiano Rodriguez-Leon

## LICENSE AND COPYRIGHT

Header "LICENSE AND COPYRIGHT"
Copyright © 2006, 2007, 2008, 2009, 2010, 2011, 2012 Casiano Rodriguez-Leon.
Copyright © 2017 William N. Braswell, Jr.
All Rights Reserved.

Parse::Yapp is Copyright © 1998, 1999, 2000, 2001, Francois Desarmenien.
Parse::Yapp is Copyright © 2017 William N. Braswell, Jr.
All Rights Reserved.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.

## POD ERRORS

Header "POD ERRORS"
Hey! **The above document had some coding errors, which are explained below:**

- Around line 416:
Item "Around line 416:"
Non-ASCII character seen before =encoding in '©'. Assuming \s-1UTF-8\s0
