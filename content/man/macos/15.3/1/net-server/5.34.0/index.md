+++
date = "2017-08-10"
manpage_section = "1"
detected_package_version = "5.34.0"
title = "net-server(1)"
manpage_format = "troff"
manpage_name = "net-server"
description = "The net-server program gives a simple way to test out code and try port connection parameters.  Though the running server can be robust enough for full tim use, it is anticipated that this binary will just be used for basic testing of net-server po..."
operating_system = "macos"
author = "None Specified"
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "NET-SERVER 1"
NET-SERVER 1 "2017-08-10" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

net-server - Base Net::Server starting module

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
    net-server [base type] [net server arguments]

    net-server PreFork ipv \*(Aq*\*(Aq

    net-server HTTP

    net-server HTTP app foo.cgi

    net-server HTTP app foo.cgi app /=bar.cgi

    net-server HTTP port 8080 port 8443/ssl ipv \*(Aq*\*(Aq server_type PreFork --SSL_key_file=my.key --SSL_cert_file=my.crt access_log_file STDERR
.Ve

## DESCRIPTION

Header "DESCRIPTION"
The net-server program gives a simple way to test out code and try
port connection parameters.  Though the running server can be robust
enough for full tim use, it is anticipated that this binary will just
be used for basic testing of net-server ports, acting as a simple echo
server, or for running development scripts as \s-1CGI.\s0

## OPTIONS

Header "OPTIONS"
.ie n .IP """base type""" 4
.el .IP "\f(CWbase type" 4
Item "base type"
The very first argument may be a Net::Server flavor.  This is given as
shorthand for writing out server_type \*(L"ServerFlavor\*(R".  Additionally,
this allows types such as \s-1HTTP\s0 and \s-1PSGI,\s0 which are not true
Net::Server base types, to subclass other server types via an
additional server_type argument.
.Sp
.Vb 1
    net-server PreFork

    net-server HTTP  # becomes a HTTP server in the Fork flavor

    net-server HTTP server_type PreFork  # preforking HTTP server
.Ve
.ie n .IP """port""" 4
.el .IP "\f(CWport" 4
Item "port"
Port to bind upon.  Default is 80 if running a \s-1HTTP\s0 server as root,
8080 if running a \s-1HTTP\s0 server as non-root, or 20203 otherwise.
.Sp
Multiple value can be given for binding to multiple ports.  All of the
methods for specifying port attributes enumerated in Net::Server
and Net::Server::Proto are available here.
.Sp
.Vb 1
    net-server port 20201

    net-server port 20202

    net-server port 20203/IPv6
.Ve
.ie n .IP """host""" 4
.el .IP "\f(CWhost" 4
Item "host"
Host to bind to.  Default is *.  Will bind to an IPv4 socket if an
IPv4 address is given.  Will bind to an IPv6 socket if an IPv6 address
is given (requires installation of IO::Socket::INET6).
.Sp
If a hostname is given and \f(CW\*(C`ipv\*(C' is still set to 4, an IPv4 socket
will be created.  If a hostname is given and \f(CW\*(C`ipv\*(C' is set to 6, an
IPv6 socket will be created.  If a hostname is given and \f(CW\*(C`ipv\*(C' is set
to * (default), a lookup will be performed and any available IPv4 or
IPv6 addresses will be bound.  The \f(CW\*(C`ipv\*(C' parameter can be set
directly, or passed along in the port, or additionally can be passed
as part of the hostname.
.Sp
.Vb 1
    net-server host localhost

    net-server host localhost/IPv4
.Ve

There are many more options available.  Please see the Net::Server
documentation.

## AUTHOR

Header "AUTHOR"
.Vb 1
    Paul Seamons <paul@seamons.com>
.Ve

## LICENSE

Header "LICENSE"
This package may be distributed under the terms of either the

.Vb 3
  GNU General Public License
    or the
  Perl Artistic License
.Ve
