+++
description = "DTrace is a framework for comprehensive system- and application-level tracing. Perl is a DTrace provider, meaning it exposes several probes for instrumentation. You can use these in conjunction with kernel-level probes, as well as probes from other..."
keywords = ["header", "see", "also", "devel", "dtrace", "provider", "4", "item", "this", "s-1cpan", "s0", "module", "lets", "you", "create", "application-level", "probes", "written", "in", "perl", "authors", "shawn", "m", "moore", "f", "cw", "c", "sartak", "gmail", "com"]
manpage_name = "perldtrace"
operating_system_version = "15.3"
date = "2022-02-19"
author = "None Specified"
detected_package_version = "5.34.1"
manpage_format = "troff"
title = "perldtrace(1)"
operating_system = "macos"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLDTRACE 1"
PERLDTRACE 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perldtrace - Perl's support for DTrace

## SYNOPSIS

Header "SYNOPSIS"
.Vb 2
 # dtrace -Zn \*(Aqperl::sub-entry, perl::sub-return \{ trace(copyinstr(arg0)) \}\*(Aq
 dtrace: description \*(Aqperl::sub-entry, perl::sub-return \*(Aq matched 10 probes

 # perl -E \*(Aqsub outer \{ inner(@_) \} sub inner \{ say shift \} outer("hello")\*(Aq
 hello

 (dtrace output)
 CPU     ID                    FUNCTION:NAME
   0  75915       Perl_pp_entersub:sub-entry   BEGIN
   0  75915       Perl_pp_entersub:sub-entry   import
   0  75922      Perl_pp_leavesub:sub-return   import
   0  75922      Perl_pp_leavesub:sub-return   BEGIN
   0  75915       Perl_pp_entersub:sub-entry   outer
   0  75915       Perl_pp_entersub:sub-entry   inner
   0  75922      Perl_pp_leavesub:sub-return   inner
   0  75922      Perl_pp_leavesub:sub-return   outer
.Ve

## DESCRIPTION

Header "DESCRIPTION"
DTrace is a framework for comprehensive system- and application-level
tracing. Perl is a DTrace *provider*, meaning it exposes several
*probes* for instrumentation. You can use these in conjunction
with kernel-level probes, as well as probes from other providers
such as MySQL, in order to diagnose software defects, or even just
your application's bottlenecks.

Perl must be compiled with the \f(CW\*(C`-Dusedtrace\*(C' option in order to
make use of the provided probes. While DTrace aims to have no
overhead when its instrumentation is not active, Perl's support
itself cannot uphold that guarantee, so it is built without DTrace
probes under most systems. One notable exception is that Mac \s-1OS X\s0
ships a */usr/bin/perl* with DTrace support enabled.

## HISTORY

Header "HISTORY"

- 5.10.1
Item "5.10.1"
Perl's initial DTrace support was added, providing \f(CW\*(C`sub-entry\*(C' and
\f(CW\*(C`sub-return\*(C' probes.

- 5.14.0
Item "5.14.0"
The \f(CW\*(C`sub-entry\*(C' and \f(CW\*(C`sub-return\*(C' probes gain a fourth argument: the
package name of the function.

- 5.16.0
Item "5.16.0"
The \f(CW\*(C`phase-change\*(C' probe was added.

- 5.18.0
Item "5.18.0"
The \f(CW\*(C`op-entry\*(C', \f(CW\*(C`loading-file\*(C', and \f(CW\*(C`loaded-file\*(C' probes were added.

## PROBES

Header "PROBES"

- sub-entry(\s-1SUBNAME, FILE, LINE, PACKAGE\s0)
Item "sub-entry(SUBNAME, FILE, LINE, PACKAGE)"
Traces the entry of any subroutine. Note that all of the variables
refer to the subroutine that is being invoked; there is currently
no way to get ahold of any information about the subroutine's
*caller* from a DTrace action.
.Sp
.Vb 4
 :*perl*::sub-entry \{
     printf("%s::%s entered at %s line %d\\n",
           copyinstr(arg3), copyinstr(arg0), copyinstr(arg1), arg2);
 \}
.Ve

- sub-return(\s-1SUBNAME, FILE, LINE, PACKAGE\s0)
Item "sub-return(SUBNAME, FILE, LINE, PACKAGE)"
Traces the exit of any subroutine. Note that all of the variables
refer to the subroutine that is returning; there is currently no
way to get ahold of any information about the subroutine's *caller*
from a DTrace action.
.Sp
.Vb 4
 :*perl*::sub-return \{
     printf("%s::%s returned at %s line %d\\n",
           copyinstr(arg3), copyinstr(arg0), copyinstr(arg1), arg2);
 \}
.Ve

- phase-change(\s-1NEWPHASE, OLDPHASE\s0)
Item "phase-change(NEWPHASE, OLDPHASE)"
Traces changes to Perl's interpreter state. You can internalize this
as tracing changes to Perl's \f(CW\*(C`$\{^GLOBAL_PHASE\}\*(C' variable, especially
since the values for \f(CW\*(C`NEWPHASE\*(C' and \f(CW\*(C`OLDPHASE\*(C' are the strings that
\f(CW\*(C`$\{^GLOBAL_PHASE\}\*(C' reports.
.Sp
.Vb 4
 :*perl*::phase-change \{
     printf("Phase changed from %s to %s\\n",
         copyinstr(arg1), copyinstr(arg0));
 \}
.Ve

- op-entry(\s-1OPNAME\s0)
Item "op-entry(OPNAME)"
Traces the execution of each opcode in the Perl runloop. This probe
is fired before the opcode is executed. When the Perl debugger is
enabled, the DTrace probe is fired *after* the debugger hooks (but
still before the opcode itself is executed).
.Sp
.Vb 3
 :*perl*::op-entry \{
     printf("About to execute opcode %s\\n", copyinstr(arg0));
 \}
.Ve

- loading-file(\s-1FILENAME\s0)
Item "loading-file(FILENAME)"
Fires when Perl is about to load an individual file, whether from
\f(CW\*(C`use\*(C', \f(CW\*(C`require\*(C', or \f(CW\*(C`do\*(C'. This probe fires before the file is
read from disk. The filename argument is converted to local filesystem
paths instead of providing \f(CW\*(C`Module::Name\*(C'-style names.
.Sp
.Vb 3
 :*perl*:loading-file \{
     printf("About to load %s\\n", copyinstr(arg0));
 \}
.Ve

- loaded-file(\s-1FILENAME\s0)
Item "loaded-file(FILENAME)"
Fires when Perl has successfully loaded an individual file, whether
from \f(CW\*(C`use\*(C', \f(CW\*(C`require\*(C', or \f(CW\*(C`do\*(C'. This probe fires after the file
is read from disk and its contents evaluated. The filename argument
is converted to local filesystem paths instead of providing
\f(CW\*(C`Module::Name\*(C'-style names.
.Sp
.Vb 3
 :*perl*:loaded-file \{
     printf("Successfully loaded %s\\n", copyinstr(arg0));
 \}
.Ve

## EXAMPLES

Header "EXAMPLES"

- Most frequently called functions
Item "Most frequently called functions"
.Vb 1
 # dtrace -qZn \*(Aqsub-entry \{ @[strjoin(strjoin(copyinstr(arg3),"::"),copyinstr(arg0))] = count() \} END \{trunc(@, 10)\}\*(Aq

 Class::MOP::Attribute::slots                                    400
 Try::Tiny::catch                                                411
 Try::Tiny::try                                                  411
 Class::MOP::Instance::inline_slot_access                        451
 Class::MOP::Class::Immutable::Trait:::around                    472
 Class::MOP::Mixin::AttributeCore::has_initializer               496
 Class::MOP::Method::Wrapped::_\|_ANON_\|_                           544
 Class::MOP::Package::_package_stash                             737
 Class::MOP::Class::initialize                                  1128
 Class::MOP::get_metaclass_by_name                              1204
.Ve

- Trace function calls
Item "Trace function calls"
.Vb 1
 # dtrace -qFZn \*(Aqsub-entry, sub-return \{ trace(copyinstr(arg0)) \}\*(Aq

 0  -> Perl_pp_entersub                        BEGIN
 0  <- Perl_pp_leavesub                        BEGIN
 0  -> Perl_pp_entersub                        BEGIN
 0    -> Perl_pp_entersub                      import
 0    <- Perl_pp_leavesub                      import
 0  <- Perl_pp_leavesub                        BEGIN
 0  -> Perl_pp_entersub                        BEGIN
 0    -> Perl_pp_entersub                      dress
 0    <- Perl_pp_leavesub                      dress
 0    -> Perl_pp_entersub                      dirty
 0    <- Perl_pp_leavesub                      dirty
 0    -> Perl_pp_entersub                      whiten
 0    <- Perl_pp_leavesub                      whiten
 0  <- Perl_dounwind                           BEGIN
.Ve

- Function calls during interpreter cleanup
Item "Function calls during interpreter cleanup"
.Vb 1
 # dtrace -Zn \*(Aqphase-change /copyinstr(arg0) == "END"/ \{ self->ending = 1 \} sub-entry /self->ending/ \{ trace(copyinstr(arg0)) \}\*(Aq

 CPU     ID                    FUNCTION:NAME
   1  77214       Perl_pp_entersub:sub-entry   END
   1  77214       Perl_pp_entersub:sub-entry   END
   1  77214       Perl_pp_entersub:sub-entry   cleanup
   1  77214       Perl_pp_entersub:sub-entry   _force_writable
   1  77214       Perl_pp_entersub:sub-entry   _force_writable
.Ve

- System calls at compile time
Item "System calls at compile time"
.Vb 1
 # dtrace -qZn \*(Aqphase-change /copyinstr(arg0) == "START"/ \{ self->interesting = 1 \} phase-change /copyinstr(arg0) == "RUN"/ \{ self->interesting = 0 \} syscall::: /self->interesting/ \{ @[probefunc] = count() \} END \{ trunc(@, 3) \}\*(Aq

 lseek                                                           310
 read                                                            374
 stat64                                                         1056
.Ve

- Perl functions that execute the most opcodes
Item "Perl functions that execute the most opcodes"
.Vb 1
 # dtrace -qZn \*(Aqsub-entry \{ self->fqn = strjoin(copyinstr(arg3), strjoin("::", copyinstr(arg0))) \} op-entry /self->fqn != ""/ \{ @[self->fqn] = count() \} END \{ trunc(@, 3) \}\*(Aq

 warnings::unimport                                             4589
 Exporter::Heavy::_rebuild_cache                                5039
 Exporter::import                                              14578
.Ve

## REFERENCES

Header "REFERENCES"

- DTrace Dynamic Tracing Guide
Item "DTrace Dynamic Tracing Guide"
<http://dtrace.org/guide/preface.html>

- DTrace: Dynamic Tracing in Oracle Solaris, Mac \s-1OS X\s0 and FreeBSD
Item "DTrace: Dynamic Tracing in Oracle Solaris, Mac OS X and FreeBSD"
<https://www.amazon.com/DTrace-Dynamic-Tracing-Solaris-FreeBSD/dp/0132091518/>

## SEE ALSO

Header "SEE ALSO"

- Devel::DTrace::Provider
Item "Devel::DTrace::Provider"
This \s-1CPAN\s0 module lets you create application-level DTrace probes written in
Perl.

## AUTHORS

Header "AUTHORS"
Shawn M Moore \f(CW\*(C`sartak@gmail.com\*(C'
