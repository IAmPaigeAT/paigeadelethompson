+++
author = "None Specified"
operating_system_version = "15.3"
operating_system = "macos"
detected_package_version = "5.34.1"
manpage_name = "perlop"
description = "In Perl, the operator determines what operation is performed, independent of the type of the operands.  For example f(CW*(C`$x + $y*(C is always a numeric addition, and if f(CW$x or f(CW$y do not contain numbers, an attempt is made to convert them..."
date = "2022-02-19"
manpage_section = "1"
title = "perlop(1)"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLOP 1"
PERLOP 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlop - Perl operators and precedence
Xref "operator"

## DESCRIPTION

Header "DESCRIPTION"
In Perl, the operator determines what operation is performed,
independent of the type of the operands.  For example \f(CW\*(C`$x\ +\ $y\*(C'
is always a numeric addition, and if \f(CW$x or \f(CW$y do not contain
numbers, an attempt is made to convert them to numbers first.

This is in contrast to many other dynamic languages, where the
operation is determined by the type of the first argument.  It also
means that Perl has two versions of some operators, one for numeric
and one for string comparison.  For example \f(CW\*(C`$x\ ==\ $y\*(C' compares
two numbers for equality, and \f(CW\*(C`$x\ eq\ $y\*(C' compares two strings.

There are a few exceptions though: \f(CW\*(C`x\*(C' can be either string
repetition or list repetition, depending on the type of the left
operand, and \f(CW\*(C`&\*(C', \f(CW\*(C`|\*(C', \f(CW\*(C`^\*(C' and \f(CW\*(C`~\*(C' can be either string or numeric bit
operations.

### Operator Precedence and Associativity

Xref "operator, precedence precedence associativity"
Subsection "Operator Precedence and Associativity"
Operator precedence and associativity work in Perl more or less like
they do in mathematics.

*Operator precedence* means some operators group more tightly than others.
For example, in \f(CW\*(C`2 + 4 * 5\*(C', the multiplication has higher precedence, so \f(CW\*(C`4
* 5\*(C' is grouped together as the right-hand operand of the addition, rather
than \f(CW\*(C`2 + 4\*(C' being grouped together as the left-hand operand of the
multiplication. It is as if the expression were written \f(CW\*(C`2 + (4 * 5)\*(C', not
\f(CW\*(C`(2 + 4) * 5\*(C'. So the expression yields \f(CW\*(C`2 + 20 == 22\*(C', rather than
\f(CW\*(C`6 * 5 == 30\*(C'.

*Operator associativity* defines what happens if a sequence of the same
operators is used one after another:
usually that they will be grouped at the left
or the right. For example, in \f(CW\*(C`9 - 3 - 2\*(C', subtraction is left associative,
so \f(CW\*(C`9 - 3\*(C' is grouped together as the left-hand operand of the second
subtraction, rather than \f(CW\*(C`3 - 2\*(C' being grouped together as the right-hand
operand of the first subtraction. It is as if the expression were written
\f(CW\*(C`(9 - 3) - 2\*(C', not \f(CW\*(C`9 - (3 - 2)\*(C'. So the expression yields \f(CW\*(C`6 - 2 == 4\*(C',
rather than \f(CW\*(C`9 - 1 == 8\*(C'.

For simple operators that evaluate all their operands and then combine the
values in some way, precedence and associativity (and parentheses) imply some
ordering requirements on those combining operations. For example, in \f(CW\*(C`2 + 4 *
5\*(C', the grouping implied by precedence means that the multiplication of 4 and
5 must be performed before the addition of 2 and 20, simply because the result
of that multiplication is required as one of the operands of the addition. But
the order of operations is not fully determined by this: in \f(CW\*(C`2 * 2 + 4 * 5\*(C'
both multiplications must be performed before the addition, but the grouping
does not say anything about the order in which the two multiplications are
performed. In fact Perl has a general rule that the operands of an operator
are evaluated in left-to-right order. A few operators such as \f(CW\*(C`&&=\*(C' have
special evaluation rules that can result in an operand not being evaluated at
all; in general, the top-level operator in an expression has control of
operand evaluation.

Some comparison operators, as their associativity, *chain* with some
operators of the same precedence (but never with operators of different
precedence).  This chaining means that each comparison is performed
on the two arguments surrounding it, with each interior argument taking
part in two comparisons, and the comparison results are implicitly ANDed.
Thus \f(CW"$x\ <\ $y\ <=\ $z" behaves exactly like \f(CW"$x\ <\ $y\ &&\ $y\ <=\ $z", assuming that \f(CW"$y" is as simple a scalar as
it looks.  The ANDing short-circuits just like \f(CW"&&" does, stopping
the sequence of comparisons as soon as one yields false.

In a chained comparison, each argument expression is evaluated at most
once, even if it takes part in two comparisons, but the result of the
evaluation is fetched for each comparison.  (It is not evaluated
at all if the short-circuiting means that it's not required for any
comparisons.)  This matters if the computation of an interior argument
is expensive or non-deterministic.  For example,

.Vb 1
    if($x < expensive_sub() <= $z) \{ ...
.Ve

is not entirely like

.Vb 1
    if($x < expensive_sub() && expensive_sub() <= $z) \{ ...
.Ve

but instead closer to

.Vb 2
    my $tmp = expensive_sub();
    if($x < $tmp && $tmp <= $z) \{ ...
.Ve

in that the subroutine is only called once.  However, it's not exactly
like this latter code either, because the chained comparison doesn't
actually involve any temporary variable (named or otherwise): there is
no assignment.  This doesn't make much difference where the expression
is a call to an ordinary subroutine, but matters more with an lvalue
subroutine, or if the argument expression yields some unusual kind of
scalar by other means.  For example, if the argument expression yields
a tied scalar, then the expression is evaluated to produce that scalar
at most once, but the value of that scalar may be fetched up to twice,
once for each comparison in which it is actually used.

In this example, the expression is evaluated only once, and the tied
scalar (the result of the expression) is fetched for each comparison that
uses it.

.Vb 1
    if ($x < $tied_scalar < $z) \{ ...
.Ve

In the next example, the expression is evaluated only once, and the tied
scalar is fetched once as part of the operation within the expression.
The result of that operation is fetched for each comparison, which
normally doesn't matter unless that expression result is also magical due
to operator overloading.

.Vb 1
    if ($x < $tied_scalar + 42 < $z) \{ ...
.Ve

Some operators are instead non-associative, meaning that it is a syntax
error to use a sequence of those operators of the same precedence.
For example, \f(CW"$x\ ..\ $y\ ..\ $z" is an error.

Perl operators have the following associativity and precedence,
listed from highest precedence to lowest.  Operators borrowed from
C keep the same precedence relationship with each other, even where
C's precedence is slightly screwy.  (This makes learning Perl easier
for C folks.)  With very few exceptions, these all operate on scalar
values only, not array values.

.Vb 10
    left        terms and list operators (leftward)
    left        ->
    nonassoc    ++ --
    right       **
    right       ! ~ ~. \\ and unary + and -
    left        =~ !~
    left        * / % x
    left        + - .
    left        << >>
    nonassoc    named unary operators
    nonassoc    isa
    chained     < > <= >= lt gt le ge
    chain/na    == != eq ne <=> cmp ~~
    left        & &.
    left        | |. ^ ^.
    left        &&
    left        || //
    nonassoc    ..  ...
    right       ?:
    right       = += -= *= etc. goto last next redo dump
    left        , =>
    nonassoc    list operators (rightward)
    right       not
    left        and
    left        or xor
.Ve

In the following sections, these operators are covered in detail, in the
same order in which they appear in the table above.

Many operators can be overloaded for objects.  See overload.

### Terms and List Operators (Leftward)

Xref "list operator operator, list term"
Subsection "Terms and List Operators (Leftward)"
A \s-1TERM\s0 has the highest precedence in Perl.  They include variables,
quote and quote-like operators, any expression in parentheses,
and any function whose arguments are parenthesized.  Actually, there
aren't really functions in this sense, just list operators and unary
operators behaving as functions because you put parentheses around
the arguments.  These are all documented in perlfunc.

If any list operator (\f(CW\*(C`print()\*(C', etc.) or any unary operator (\f(CW\*(C`chdir()\*(C', etc.)
is followed by a left parenthesis as the next token, the operator and
arguments within parentheses are taken to be of highest precedence,
just like a normal function call.

In the absence of parentheses, the precedence of list operators such as
\f(CW\*(C`print\*(C', \f(CW\*(C`sort\*(C', or \f(CW\*(C`chmod\*(C' is either very high or very low depending on
whether you are looking at the left side or the right side of the operator.
For example, in

.Vb 2
    @ary = (1, 3, sort 4, 2);
    print @ary;         # prints 1324
.Ve

the commas on the right of the \f(CW\*(C`sort\*(C' are evaluated before the \f(CW\*(C`sort\*(C',
but the commas on the left are evaluated after.  In other words,
list operators tend to gobble up all arguments that follow, and
then act like a simple \s-1TERM\s0 with regard to the preceding expression.
Be careful with parentheses:

.Vb 3
    # These evaluate exit before doing the print:
    print($foo, exit);  # Obviously not what you want.
    print $foo, exit;   # Nor is this.

    # These do the print before evaluating exit:
    (print $foo), exit; # This is what you want.
    print($foo), exit;  # Or this.
    print ($foo), exit; # Or even this.
.Ve

Also note that

.Vb 1
    print ($foo & 255) + 1, "\\n";
.Ve

probably doesn't do what you expect at first glance.  The parentheses
enclose the argument list for \f(CW\*(C`print\*(C' which is evaluated (printing
the result of \f(CW\*(C`$foo\ &\ 255\*(C').  Then one is added to the return value
of \f(CW\*(C`print\*(C' (usually 1).  The result is something like this:

.Vb 1
    1 + 1, "\\n";    # Obviously not what you meant.
.Ve

To do what you meant properly, you must write:

.Vb 1
    print(($foo & 255) + 1, "\\n");
.Ve

See \*(L"Named Unary Operators\*(R" for more discussion of this.

Also parsed as terms are the \f(CW\*(C`do\ \{\}\*(C' and \f(CW\*(C`eval\ \{\}\*(C' constructs, as
well as subroutine and method calls, and the anonymous
constructors \f(CW\*(C`[]\*(C' and \f(CW\*(C`\{\}\*(C'.

See also \*(L"Quote and Quote-like Operators\*(R" toward the end of this section,
as well as \*(L"I/O Operators\*(R".

### The Arrow Operator

Xref "arrow dereference ->"
Subsection "The Arrow Operator"
"\f(CW\*(C`->\*(C'" is an infix dereference operator, just as it is in C
and \*(C+.  If the right side is either a \f(CW\*(C`[...]\*(C', \f(CW\*(C`\{...\}\*(C', or a
\f(CW\*(C`(...)\*(C' subscript, then the left side must be either a hard or
symbolic reference to an array, a hash, or a subroutine respectively.
(Or technically speaking, a location capable of holding a hard
reference, if it's an array or hash reference being used for
assignment.)  See perlreftut and perlref.

Otherwise, the right side is a method name or a simple scalar
variable containing either the method name or a subroutine reference,
and the left side must be either an object (a blessed reference)
or a class name (that is, a package name).  See perlobj.

The dereferencing cases (as opposed to method-calling cases) are
somewhat extended by the \f(CW\*(C`postderef\*(C' feature.  For the
details of that feature, consult \*(L"Postfix Dereference Syntax\*(R" in perlref.

### Auto-increment and Auto-decrement

Xref "increment auto-increment ++ decrement auto-decrement --"
Subsection "Auto-increment and Auto-decrement"
\f(CW"++" and \f(CW"--" work as in C.  That is, if placed before a variable,
they increment or decrement the variable by one before returning the
value, and if placed after, increment or decrement after returning the
value.

.Vb 3
    $i = 0;  $j = 0;
    print $i++;  # prints 0
    print ++$j;  # prints 1
.Ve

Note that just as in C, Perl doesn't define **when** the variable is
incremented or decremented.  You just know it will be done sometime
before or after the value is returned.  This also means that modifying
a variable twice in the same statement will lead to undefined behavior.
Avoid statements like:

.Vb 2
    $i = $i ++;
    print ++ $i + $i ++;
.Ve

Perl will not guarantee what the result of the above statements is.

The auto-increment operator has a little extra builtin magic to it.  If
you increment a variable that is numeric, or that has ever been used in
a numeric context, you get a normal increment.  If, however, the
variable has been used in only string contexts since it was set, and
has a value that is not the empty string and matches the pattern
\f(CW\*(C`/^[a-zA-Z]*[0-9]*\\z/\*(C', the increment is done as a string, preserving each
character within its range, with carry:

.Vb 4
    print ++($foo = "99");      # prints "100"
    print ++($foo = "a0");      # prints "a1"
    print ++($foo = "Az");      # prints "Ba"
    print ++($foo = "zz");      # prints "aaa"
.Ve

\f(CW\*(C`undef\*(C' is always treated as numeric, and in particular is changed
to \f(CW0 before incrementing (so that a post-increment of an undef value
will return \f(CW0 rather than \f(CW\*(C`undef\*(C').

The auto-decrement operator is not magical.

### Exponentiation

Xref "** exponentiation power"
Subsection "Exponentiation"
Binary \f(CW"**" is the exponentiation operator.  It binds even more
tightly than unary minus, so \f(CW\*(C`-2**4\*(C' is \f(CW\*(C`-(2**4)\*(C', not \f(CW\*(C`(-2)**4\*(C'.
(This is
implemented using C's \f(CWpow(3) function, which actually works on doubles
internally.)

Note that certain exponentiation expressions are ill-defined:
these include \f(CW\*(C`0**0\*(C', \f(CW\*(C`1**Inf\*(C', and \f(CW\*(C`Inf**0\*(C'.  Do not expect
any particular results from these special cases, the results
are platform-dependent.

### Symbolic Unary Operators

Xref "unary operator operator, unary"
Subsection "Symbolic Unary Operators"
Unary \f(CW"!" performs logical negation, that is, \*(L"not\*(R".  See also
\f(CW\*(C`not\*(C' for a lower precedence version of this.
Xref "!"

Unary \f(CW"-" performs arithmetic negation if the operand is numeric,
including any string that looks like a number.  If the operand is
an identifier, a string consisting of a minus sign concatenated
with the identifier is returned.  Otherwise, if the string starts
with a plus or minus, a string starting with the opposite sign is
returned.  One effect of these rules is that \f(CW\*(C`-bareword\*(C' is equivalent
to the string \f(CW"-bareword".  If, however, the string begins with a
non-alphabetic character (excluding \f(CW"+" or \f(CW"-"), Perl will attempt
to convert
the string to a numeric, and the arithmetic negation is performed.  If the
string cannot be cleanly converted to a numeric, Perl will give the warning
**Argument \*(L"the string\*(R" isn't numeric in negation (-) at ...**.
Xref "- negation, arithmetic"

Unary \f(CW"~" performs bitwise negation, that is, 1's complement.  For
example, \f(CW\*(C`0666\ &\ ~027\*(C' is 0640.  (See also \*(L"Integer Arithmetic\*(R" and
\*(L"Bitwise String Operators\*(R".)  Note that the width of the result is
platform-dependent: \f(CW\*(C`~0\*(C' is 32 bits wide on a 32-bit platform, but 64
bits wide on a 64-bit platform, so if you are expecting a certain bit
width, remember to use the \f(CW"&" operator to mask off the excess bits.
Xref "~ negation, binary"

Starting in Perl 5.28, it is a fatal error to try to complement a string
containing a character with an ordinal value above 255.

If the \*(L"bitwise\*(R" feature is enabled via \f(CW\*(C`use\ feature\ \*(Aqbitwise\*(Aq\*(C' or \f(CW\*(C`use v5.28\*(C', then unary
\f(CW"~" always treats its argument as a number, and an
alternate form of the operator, \f(CW"~.", always treats its argument as a
string.  So \f(CW\*(C`~0\*(C' and \f(CW\*(C`~"0"\*(C' will both give 2**32-1 on 32-bit platforms,
whereas \f(CW\*(C`~.0\*(C' and \f(CW\*(C`~."0"\*(C' will both yield \f(CW"\\xff".  Until Perl 5.28,
this feature produced a warning in the \f(CW"experimental::bitwise" category.

Unary \f(CW"+" has no effect whatsoever, even on strings.  It is useful
syntactically for separating a function name from a parenthesized expression
that would otherwise be interpreted as the complete list of function
arguments.  (See examples above under \*(L"Terms and List Operators (Leftward)\*(R".)
Xref "+"

Unary \f(CW"\\" creates references.  If its operand is a single sigilled
thing, it creates a reference to that object.  If its operand is a
parenthesised list, then it creates references to the things mentioned
in the list.  Otherwise it puts its operand in list context, and creates
a list of references to the scalars in the list provided by the operand.
See perlreftut
and perlref.  Do not confuse this behavior with the behavior of
backslash within a string, although both forms do convey the notion
of protecting the next thing from interpolation.
Xref "\ reference backslash"

### Binding Operators

Xref "binding operator, binding =~ !~"
Subsection "Binding Operators"
Binary \f(CW"=~" binds a scalar expression to a pattern match.  Certain operations
search or modify the string \f(CW$_ by default.  This operator makes that kind
of operation work on some other string.  The right argument is a search
pattern, substitution, or transliteration.  The left argument is what is
supposed to be searched, substituted, or transliterated instead of the default
\f(CW$_.  When used in scalar context, the return value generally indicates the
success of the operation.  The exceptions are substitution (\f(CW\*(C`s///\*(C')
and transliteration (\f(CW\*(C`y///\*(C') with the \f(CW\*(C`/r\*(C' (non-destructive) option,
which cause the **r**eturn value to be the result of the substitution.
Behavior in list context depends on the particular operator.
See \*(L"Regexp Quote-Like Operators\*(R" for details and perlretut for
examples using these operators.

If the right argument is an expression rather than a search pattern,
substitution, or transliteration, it is interpreted as a search pattern at run
time.  Note that this means that its
contents will be interpolated twice, so

.Vb 1
    \*(Aq\\\\\*(Aq =~ q\*(Aq\\\\\*(Aq;
.Ve

is not ok, as the regex engine will end up trying to compile the
pattern \f(CW\*(C`\\\*(C', which it will consider a syntax error.

Binary \f(CW"!~" is just like \f(CW"=~" except the return value is negated in
the logical sense.

Binary \f(CW"!~" with a non-destructive substitution (\f(CW\*(C`s///r\*(C') or transliteration
(\f(CW\*(C`y///r\*(C') is a syntax error.

### Multiplicative Operators

Xref "operator, multiplicative"
Subsection "Multiplicative Operators"
Binary \f(CW"*" multiplies two numbers.
Xref "*"

Binary \f(CW"/" divides two numbers.
Xref "slash"

Binary \f(CW"%" is the modulo operator, which computes the division
remainder of its first argument with respect to its second argument.
Given integer
operands \f(CW$m and \f(CW$n: If \f(CW$n is positive, then \f(CW\*(C`$m\ %\ $n\*(C' is
\f(CW$m minus the largest multiple of \f(CW$n less than or equal to
\f(CW$m.  If \f(CW$n is negative, then \f(CW\*(C`$m\ %\ $n\*(C' is \f(CW$m minus the
smallest multiple of \f(CW$n that is not less than \f(CW$m (that is, the
result will be less than or equal to zero).  If the operands
\f(CW$m and \f(CW$n are floating point values and the absolute value of
\f(CW$n (that is \f(CW\*(C`abs($n)\*(C') is less than \f(CW\*(C`(UV_MAX\ +\ 1)\*(C', only
the integer portion of \f(CW$m and \f(CW$n will be used in the operation
(Note: here \f(CW\*(C`UV_MAX\*(C' means the maximum of the unsigned integer type).
If the absolute value of the right operand (\f(CW\*(C`abs($n)\*(C') is greater than
or equal to \f(CW\*(C`(UV_MAX\ +\ 1)\*(C', \f(CW"%" computes the floating-point remainder
\f(CW$r in the equation \f(CW\*(C`($r\ =\ $m\ -\ $i*$n)\*(C' where \f(CW$i is a certain
integer that makes \f(CW$r have the same sign as the right operand
\f(CW$n (**not** as the left operand \f(CW$m like C function \f(CW\*(C`fmod()\*(C')
and the absolute value less than that of \f(CW$n.
Note that when \f(CW\*(C`use\ integer\*(C' is in scope, \f(CW"%" gives you direct access
to the modulo operator as implemented by your C compiler.  This
operator is not as well defined for negative operands, but it will
execute faster.
Xref "% remainder modulo mod"

Binary \f(CW\*(C`x\*(C' is the repetition operator.  In scalar context, or if the
left operand is neither enclosed in parentheses nor a \f(CW\*(C`qw//\*(C' list,
it performs a string repetition.  In that case it supplies scalar
context to the left operand, and returns a string consisting of the
left operand string repeated the number of times specified by the right
operand.  If the \f(CW\*(C`x\*(C' is in list context, and the left operand is either
enclosed in parentheses or a \f(CW\*(C`qw//\*(C' list, it performs a list repetition.
In that case it supplies list context to the left operand, and returns
a list consisting of the left operand list repeated the number of times
specified by the right operand.
If the right operand is zero or negative (raising a warning on
negative), it returns an empty string
or an empty list, depending on the context.
Xref "x"

.Vb 1
    print \*(Aq-\*(Aq x 80;             # print row of dashes

    print "\\t" x ($tab/8), \*(Aq \*(Aq x ($tab%8);      # tab over

    @ones = (1) x 80;           # a list of 80 1\*(Aqs
    @ones = (5) x @ones;        # set all elements to 5
.Ve

### Additive Operators

Xref "operator, additive"
Subsection "Additive Operators"
Binary \f(CW"+" returns the sum of two numbers.
Xref "+"

Binary \f(CW"-" returns the difference of two numbers.
Xref "-"

Binary \f(CW"." concatenates two strings.
Xref "string, concatenation concatenation cat concat concatenate ."

### Shift Operators

Xref "shift operator operator, shift << >> right shift left shift bitwise shift shl shr shift, right shift, left"
Subsection "Shift Operators"
Binary \f(CW"<<" returns the value of its left argument shifted left by the
number of bits specified by the right argument.  Arguments should be
integers.  (See also \*(L"Integer Arithmetic\*(R".)

Binary \f(CW">>" returns the value of its left argument shifted right by
the number of bits specified by the right argument.  Arguments should
be integers.  (See also \*(L"Integer Arithmetic\*(R".)

If \f(CW\*(C`use\ integer\*(C' (see \*(L"Integer Arithmetic\*(R") is in force then
signed C integers are used (*arithmetic shift*), otherwise unsigned C
integers are used (*logical shift*), even for negative shiftees.
In arithmetic right shift the sign bit is replicated on the left,
in logical shift zero bits come in from the left.

Either way, the implementation isn't going to generate results larger
than the size of the integer type Perl was built with (32 bits or 64 bits).

Shifting by negative number of bits means the reverse shift: left
shift becomes right shift, right shift becomes left shift.  This is
unlike in C, where negative shift is undefined.

Shifting by more bits than the size of the integers means most of the
time zero (all bits fall off), except that under \f(CW\*(C`use\ integer\*(C'
right overshifting a negative shiftee results in -1.  This is unlike
in C, where shifting by too many bits is undefined.  A common C
behavior is \*(L"shift by modulo wordbits\*(R", so that for example

.Vb 1
    1 >> 64 == 1 >> (64 % 64) == 1 >> 0 == 1  # Common C behavior.
.Ve

but that is completely accidental.

If you get tired of being subject to your platform's native integers,
the \f(CW\*(C`use\ bigint\*(C' pragma neatly sidesteps the issue altogether:

.Vb 5
    print 20 << 20;  # 20971520
    print 20 << 40;  # 5120 on 32-bit machines,
                     # 21990232555520 on 64-bit machines
    use bigint;
    print 20 << 100; # 25353012004564588029934064107520
.Ve

### Named Unary Operators

Xref "operator, named unary"
Subsection "Named Unary Operators"
The various named unary operators are treated as functions with one
argument, with optional parentheses.

If any list operator (\f(CW\*(C`print()\*(C', etc.) or any unary operator (\f(CW\*(C`chdir()\*(C', etc.)
is followed by a left parenthesis as the next token, the operator and
arguments within parentheses are taken to be of highest precedence,
just like a normal function call.  For example,
because named unary operators are higher precedence than \f(CW\*(C`||\*(C':

.Vb 4
    chdir $foo    || die;       # (chdir $foo) || die
    chdir($foo)   || die;       # (chdir $foo) || die
    chdir ($foo)  || die;       # (chdir $foo) || die
    chdir +($foo) || die;       # (chdir $foo) || die
.Ve

but, because \f(CW"*" is higher precedence than named operators:

.Vb 4
    chdir $foo * 20;    # chdir ($foo * 20)
    chdir($foo) * 20;   # (chdir $foo) * 20
    chdir ($foo) * 20;  # (chdir $foo) * 20
    chdir +($foo) * 20; # chdir ($foo * 20)

    rand 10 * 20;       # rand (10 * 20)
    rand(10) * 20;      # (rand 10) * 20
    rand (10) * 20;     # (rand 10) * 20
    rand +(10) * 20;    # rand (10 * 20)
.Ve

Regarding precedence, the filetest operators, like \f(CW\*(C`-f\*(C', \f(CW\*(C`-M\*(C', etc. are
treated like named unary operators, but they don't follow this functional
parenthesis rule.  That means, for example, that \f(CW\*(C`-f($file).".bak"\*(C' is
equivalent to \f(CW\*(C`-f\ "$file.bak"\*(C'.
Xref "-X filetest operator, filetest"

See also \*(L"Terms and List Operators (Leftward)\*(R".

### Relational Operators

Xref "relational operator operator, relational"
Subsection "Relational Operators"
Perl operators that return true or false generally return values
that can be safely used as numbers.  For example, the relational
operators in this section and the equality operators in the next
one return \f(CW1 for true and a special version of the defined empty
string, \f(CW"", which counts as a zero but is exempt from warnings
about improper numeric conversions, just as \f(CW"0\ but\ true" is.

Binary \f(CW"<" returns true if the left argument is numerically less than
the right argument.
Xref "<"

Binary \f(CW">" returns true if the left argument is numerically greater
than the right argument.
Xref ">"

Binary \f(CW"<=" returns true if the left argument is numerically less than
or equal to the right argument.
Xref "<="

Binary \f(CW">=" returns true if the left argument is numerically greater
than or equal to the right argument.
Xref ">="

Binary \f(CW"lt" returns true if the left argument is stringwise less than
the right argument.
Xref "lt"

Binary \f(CW"gt" returns true if the left argument is stringwise greater
than the right argument.
Xref "gt"

Binary \f(CW"le" returns true if the left argument is stringwise less than
or equal to the right argument.
Xref "le"

Binary \f(CW"ge" returns true if the left argument is stringwise greater
than or equal to the right argument.
Xref "ge"

A sequence of relational operators, such as \f(CW"$x\ <\ $y\ <=\ $z", performs chained comparisons, in the manner described above in
the section \*(L"Operator Precedence and Associativity\*(R".
Beware that they do not chain with equality operators, which have lower
precedence.

### Equality Operators

Xref "equality equal equals operator, equality"
Subsection "Equality Operators"
Binary \f(CW"==" returns true if the left argument is numerically equal to
the right argument.
Xref "=="

Binary \f(CW"!=" returns true if the left argument is numerically not equal
to the right argument.
Xref "!="

Binary \f(CW"eq" returns true if the left argument is stringwise equal to
the right argument.
Xref "eq"

Binary \f(CW"ne" returns true if the left argument is stringwise not equal
to the right argument.
Xref "ne"

A sequence of the above equality operators, such as \f(CW"$x\ ==\ $y\ ==\ $z", performs chained comparisons, in the manner described above in
the section \*(L"Operator Precedence and Associativity\*(R".
Beware that they do not chain with relational operators, which have
higher precedence.

Binary \f(CW"<=>" returns -1, 0, or 1 depending on whether the left
argument is numerically less than, equal to, or greater than the right
argument.  If your platform supports \f(CW\*(C`NaN\*(C''s (not-a-numbers) as numeric
values, using them with \f(CW"<=>" returns undef.  \f(CW\*(C`NaN\*(C' is not
\f(CW"<", \f(CW"==", \f(CW">", \f(CW"<=" or \f(CW">=" anything
(even \f(CW\*(C`NaN\*(C'), so those 5 return false.  \f(CW\*(C`NaN\ !=\ NaN\*(C' returns
true, as does \f(CW\*(C`NaN\ !=\*(C'\ *anything\ else*.  If your platform doesn't
support \f(CW\*(C`NaN\*(C''s then \f(CW\*(C`NaN\*(C' is just a string with numeric value 0.
Xref "<=> spaceship"

.Vb 2
    $ perl -le \*(Aq$x = "NaN"; print "No NaN support here" if $x == $x\*(Aq
    $ perl -le \*(Aq$x = "NaN"; print "NaN support here" if $x != $x\*(Aq
.Ve

(Note that the bigint, bigrat, and bignum pragmas all
support \f(CW"NaN".)

Binary \f(CW"cmp" returns -1, 0, or 1 depending on whether the left
argument is stringwise less than, equal to, or greater than the right
argument.
Xref "cmp"

Binary \f(CW"~~" does a smartmatch between its arguments.  Smart matching
is described in the next section.
Xref "~~"

The two-sided ordering operators \f(CW"<=>" and \f(CW"cmp", and the
smartmatch operator \f(CW"~~", are non-associative with respect to each
other and with respect to the equality operators of the same precedence.

\f(CW"lt", \f(CW"le", \f(CW"ge", \f(CW"gt" and \f(CW"cmp" use the collation (sort)
order specified by the current \f(CW\*(C`LC_COLLATE\*(C' locale if a \f(CW\*(C`use\ locale\*(C' form that includes collation is in effect.  See perllocale.
Do not mix these with Unicode,
only use them with legacy 8-bit locale encodings.
The standard \f(CW\*(C`Unicode::Collate\*(C' and
\f(CW\*(C`Unicode::Collate::Locale\*(C' modules offer much more powerful
solutions to collation issues.

For case-insensitive comparisons, look at the \*(L"fc\*(R" in perlfunc case-folding
function, available in Perl v5.16 or later:

.Vb 1
    if ( fc($x) eq fc($y) ) \{ ... \}
.Ve

### Class Instance Operator

Xref "isa operator"
Subsection "Class Instance Operator"
Binary \f(CW\*(C`isa\*(C' evaluates to true when the left argument is an object instance of
the class (or a subclass derived from that class) given by the right argument.
If the left argument is not defined, not a blessed object instance, nor does
not derive from the class given by the right argument, the operator evaluates
as false. The right argument may give the class either as a bareword or a
scalar expression that yields a string class name:

.Vb 1
    if( $obj isa Some::Class ) \{ ... \}

    if( $obj isa "Different::Class" ) \{ ... \}
    if( $obj isa $name_of_class ) \{ ... \}
.Ve

This is an experimental feature and is available from Perl 5.31.6 when enabled
by \f(CW\*(C`use feature \*(Aqisa\*(Aq\*(C'. It emits a warning in the \f(CW\*(C`experimental::isa\*(C'
category.

### Smartmatch Operator

Subsection "Smartmatch Operator"
First available in Perl 5.10.1 (the 5.10.0 version behaved differently),
binary \f(CW\*(C`~~\*(C' does a \*(L"smartmatch\*(R" between its arguments.  This is mostly
used implicitly in the \f(CW\*(C`when\*(C' construct described in perlsyn, although
not all \f(CW\*(C`when\*(C' clauses call the smartmatch operator.  Unique among all of
Perl's operators, the smartmatch operator can recurse.  The smartmatch
operator is experimental and its behavior is
subject to change.

It is also unique in that all other Perl operators impose a context
(usually string or numeric context) on their operands, autoconverting
those operands to those imposed contexts.  In contrast, smartmatch
*infers* contexts from the actual types of its operands and uses that
type information to select a suitable comparison mechanism.

The \f(CW\*(C`~~\*(C' operator compares its operands \*(L"polymorphically\*(R", determining how
to compare them according to their actual types (numeric, string, array,
hash, etc.).  Like the equality operators with which it shares the same
precedence, \f(CW\*(C`~~\*(C' returns 1 for true and \f(CW"" for false.  It is often best
read aloud as \*(L"in\*(R", \*(L"inside of\*(R", or \*(L"is contained in\*(R", because the left
operand is often looked for *inside* the right operand.  That makes the
order of the operands to the smartmatch operand often opposite that of
the regular match operator.  In other words, the \*(L"smaller\*(R" thing is usually
placed in the left operand and the larger one in the right.

The behavior of a smartmatch depends on what type of things its arguments
are, as determined by the following table.  The first row of the table
whose types apply determines the smartmatch behavior.  Because what
actually happens is mostly determined by the type of the second operand,
the table is sorted on the right operand instead of on the left.

.Vb 4
 Left      Right      Description and pseudocode
 ===============================================================
 Any       undef      check whether Any is undefined
                like: !defined Any

 Any       Object     invoke ~~ overloading on Object, or die

 Right operand is an ARRAY:

 Left      Right      Description and pseudocode
 ===============================================================
 ARRAY1    ARRAY2     recurse on paired elements of ARRAY1 and ARRAY2[2]
                like: (ARRAY1[0] ~~ ARRAY2[0])
                        && (ARRAY1[1] ~~ ARRAY2[1]) && ...
 HASH      ARRAY      any ARRAY elements exist as HASH keys
                like: grep \{ exists HASH->\{$_\} \} ARRAY
 Regexp    ARRAY      any ARRAY elements pattern match Regexp
                like: grep \{ /Regexp/ \} ARRAY
 undef     ARRAY      undef in ARRAY
                like: grep \{ !defined \} ARRAY
 Any       ARRAY      smartmatch each ARRAY element[3]
                like: grep \{ Any ~~ $_ \} ARRAY

 Right operand is a HASH:

 Left      Right      Description and pseudocode
 ===============================================================
 HASH1     HASH2      all same keys in both HASHes
                like: keys HASH1 ==
                         grep \{ exists HASH2->\{$_\} \} keys HASH1
 ARRAY     HASH       any ARRAY elements exist as HASH keys
                like: grep \{ exists HASH->\{$_\} \} ARRAY
 Regexp    HASH       any HASH keys pattern match Regexp
                like: grep \{ /Regexp/ \} keys HASH
 undef     HASH       always false (undef can\*(Aqt be a key)
                like: 0 == 1
 Any       HASH       HASH key existence
                like: exists HASH->\{Any\}

 Right operand is CODE:

 Left      Right      Description and pseudocode
 ===============================================================
 ARRAY     CODE       sub returns true on all ARRAY elements[1]
                like: !grep \{ !CODE->($_) \} ARRAY
 HASH      CODE       sub returns true on all HASH keys[1]
                like: !grep \{ !CODE->($_) \} keys HASH
 Any       CODE       sub passed Any returns true
                like: CODE->(Any)
.Ve

Right operand is a Regexp:

.Vb 8
 Left      Right      Description and pseudocode
 ===============================================================
 ARRAY     Regexp     any ARRAY elements match Regexp
                like: grep \{ /Regexp/ \} ARRAY
 HASH      Regexp     any HASH keys match Regexp
                like: grep \{ /Regexp/ \} keys HASH
 Any       Regexp     pattern match
                like: Any =~ /Regexp/

 Other:

 Left      Right      Description and pseudocode
 ===============================================================
 Object    Any        invoke ~~ overloading on Object,
                      or fall back to...

 Any       Num        numeric equality
                 like: Any == Num
 Num       nummy[4]    numeric equality
                 like: Num == nummy
 undef     Any        check whether undefined
                 like: !defined(Any)
 Any       Any        string equality
                 like: Any eq Any
.Ve

Notes:

- 1. Empty hashes or arrays match.
Item "1. Empty hashes or arrays match."
0

- 2. That is, each element smartmatches the element of the same index in the other array.[3]
Item "2. That is, each element smartmatches the element of the same index in the other array.[3]"

- 3. If a circular reference is found, fall back to referential equality.
Item "3. If a circular reference is found, fall back to referential equality."

- 4. Either an actual number, or a string that looks like one.
Item "4. Either an actual number, or a string that looks like one."
.PD

The smartmatch implicitly dereferences any non-blessed hash or array
reference, so the \f(CW\*(C`\f(CIHASH\f(CW\*(C' and \f(CW\*(C`\f(CIARRAY\f(CW\*(C' entries apply in those cases.
For blessed references, the \f(CW\*(C`\f(CIObject\f(CW\*(C' entries apply.  Smartmatches
involving hashes only consider hash keys, never hash values.

The \*(L"like\*(R" code entry is not always an exact rendition.  For example, the
smartmatch operator short-circuits whenever possible, but \f(CW\*(C`grep\*(C' does
not.  Also, \f(CW\*(C`grep\*(C' in scalar context returns the number of matches, but
\f(CW\*(C`~~\*(C' returns only true or false.

Unlike most operators, the smartmatch operator knows to treat \f(CW\*(C`undef\*(C'
specially:

.Vb 3
    use v5.10.1;
    @array = (1, 2, 3, undef, 4, 5);
    say "some elements undefined" if undef ~~ @array;
.Ve

Each operand is considered in a modified scalar context, the modification
being that array and hash variables are passed by reference to the
operator, which implicitly dereferences them.  Both elements
of each pair are the same:

.Vb 1
    use v5.10.1;

    my %hash = (red    => 1, blue   => 2, green  => 3,
                orange => 4, yellow => 5, purple => 6,
                black  => 7, grey   => 8, white  => 9);

    my @array = qw(red blue green);

    say "some array elements in hash keys" if  @array ~~  %hash;
    say "some array elements in hash keys" if \\@array ~~ \\%hash;

    say "red in array" if "red" ~~  @array;
    say "red in array" if "red" ~~ \\@array;

    say "some keys end in e" if /e$/ ~~  %hash;
    say "some keys end in e" if /e$/ ~~ \\%hash;
.Ve

Two arrays smartmatch if each element in the first array smartmatches
(that is, is \*(L"in\*(R") the corresponding element in the second array,
recursively.

.Vb 6
    use v5.10.1;
    my @little = qw(red blue green);
    my @bigger = ("red", "blue", [ "orange", "green" ] );
    if (@little ~~ @bigger) \{  # true!
        say "little is contained in bigger";
    \}
.Ve

Because the smartmatch operator recurses on nested arrays, this
will still report that \*(L"red\*(R" is in the array.

.Vb 4
    use v5.10.1;
    my @array = qw(red blue green);
    my $nested_array = [[[[[[[ @array ]]]]]]];
    say "red in array" if "red" ~~ $nested_array;
.Ve

If two arrays smartmatch each other, then they are deep
copies of each others' values, as this example reports:

.Vb 3
    use v5.12.0;
    my @a = (0, 1, 2, [3, [4, 5], 6], 7);
    my @b = (0, 1, 2, [3, [4, 5], 6], 7);

    if (@a ~~ @b && @b ~~ @a) \{
        say "a and b are deep copies of each other";
    \}
    elsif (@a ~~ @b) \{
        say "a smartmatches in b";
    \}
    elsif (@b ~~ @a) \{
        say "b smartmatches in a";
    \}
    else \{
        say "a and b don\*(Aqt smartmatch each other at all";
    \}
.Ve

If you were to set \f(CW\*(C`$b[3]\ =\ 4\*(C', then instead of reporting that \*(L"a and b
are deep copies of each other\*(R", it now reports that \f(CW"b smartmatches in a".
That's because the corresponding position in \f(CW@a contains an array that
(eventually) has a 4 in it.

Smartmatching one hash against another reports whether both contain the
same keys, no more and no less.  This could be used to see whether two
records have the same field names, without caring what values those fields
might have.  For example:

.Vb 3
    use v5.10.1;
    sub make_dogtag \{
        state $REQUIRED_FIELDS = \{ name=>1, rank=>1, serial_num=>1 \};

        my ($class, $init_fields) = @_;

        die "Must supply (only) name, rank, and serial number"
            unless $init_fields ~~ $REQUIRED_FIELDS;

        ...
    \}
.Ve

However, this only does what you mean if \f(CW$init_fields is indeed a hash
reference. The condition \f(CW\*(C`$init_fields ~~ $REQUIRED_FIELDS\*(C' also allows the
strings \f(CW"name", \f(CW"rank", \f(CW"serial_num" as well as any array reference
that contains \f(CW"name" or \f(CW"rank" or \f(CW"serial_num" anywhere to pass
through.

The smartmatch operator is most often used as the implicit operator of a
\f(CW\*(C`when\*(C' clause.  See the section on \*(L"Switch Statements\*(R" in perlsyn.

*Smartmatching of Objects*
Subsection "Smartmatching of Objects"

To avoid relying on an object's underlying representation, if the
smartmatch's right operand is an object that doesn't overload \f(CW\*(C`~~\*(C',
it raises the exception "\f(CW\*(C`Smartmatching a non-overloaded object
breaks encapsulation\*(C'\*(L".  That's because one has no business digging
around to see whether something is \*(R"in" an object.  These are all
illegal on objects without a \f(CW\*(C`~~\*(C' overload:

.Vb 3
    %hash ~~ $object
       42 ~~ $object
   "fred" ~~ $object
.Ve

However, you can change the way an object is smartmatched by overloading
the \f(CW\*(C`~~\*(C' operator.  This is allowed to
extend the usual smartmatch semantics.
For objects that do have an \f(CW\*(C`~~\*(C' overload, see overload.

Using an object as the left operand is allowed, although not very useful.
Smartmatching rules take precedence over overloading, so even if the
object in the left operand has smartmatch overloading, this will be
ignored.  A left operand that is a non-overloaded object falls back on a
string or numeric comparison of whatever the \f(CW\*(C`ref\*(C' operator returns.  That
means that

.Vb 1
    $object ~~ X
.Ve

does *not* invoke the overload method with \f(CW\*(C`\f(CIX\f(CW\*(C' as an argument.
Instead the above table is consulted as normal, and based on the type of
\f(CW\*(C`\f(CIX\f(CW\*(C', overloading may or may not be invoked.  For simple strings or
numbers, \*(L"in\*(R" becomes equivalent to this:

.Vb 2
    $object ~~ $number          ref($object) == $number
    $object ~~ $string          ref($object) eq $string
.Ve

For example, this reports that the handle smells IOish
(but please don't really do this!):

.Vb 5
    use IO::Handle;
    my $fh = IO::Handle->new();
    if ($fh ~~ /\\bIO\\b/) \{
        say "handle smells IOish";
    \}
.Ve

That's because it treats \f(CW$fh as a string like
\f(CW"IO::Handle=GLOB(0x8039e0)", then pattern matches against that.

### Bitwise And

Xref "operator, bitwise, and bitwise and &"
Subsection "Bitwise And"
Binary \f(CW"&" returns its operands ANDed together bit by bit.  Although no
warning is currently raised, the result is not well defined when this operation
is performed on operands that aren't either numbers (see
\*(L"Integer Arithmetic\*(R") nor bitstrings (see \*(L"Bitwise String Operators\*(R").

Note that \f(CW"&" has lower priority than relational operators, so for example
the parentheses are essential in a test like

.Vb 1
    print "Even\\n" if ($x & 1) == 0;
.Ve

If the \*(L"bitwise\*(R" feature is enabled via \f(CW\*(C`use\ feature\ \*(Aqbitwise\*(Aq\*(C' or
\f(CW\*(C`use v5.28\*(C', then this operator always treats its operands as numbers.
Before Perl 5.28 this feature produced a warning in the
\f(CW"experimental::bitwise" category.

### Bitwise Or and Exclusive Or

Xref "operator, bitwise, or bitwise or | operator, bitwise, xor bitwise xor ^"
Subsection "Bitwise Or and Exclusive Or"
Binary \f(CW"|" returns its operands ORed together bit by bit.

Binary \f(CW"^" returns its operands XORed together bit by bit.

Although no warning is currently raised, the results are not well
defined when these operations are performed on operands that aren't either
numbers (see \*(L"Integer Arithmetic\*(R") nor bitstrings (see \*(L"Bitwise String
Operators\*(R").

Note that \f(CW"|" and \f(CW"^" have lower priority than relational operators, so
for example the parentheses are essential in a test like

.Vb 1
    print "false\\n" if (8 | 2) != 10;
.Ve

If the \*(L"bitwise\*(R" feature is enabled via \f(CW\*(C`use\ feature\ \*(Aqbitwise\*(Aq\*(C' or
\f(CW\*(C`use v5.28\*(C', then this operator always treats its operands as numbers.
Before Perl 5.28. this feature produced a warning in the
\f(CW"experimental::bitwise" category.

### C-style Logical And

Xref "&& logical and operator, logical, and"
Subsection "C-style Logical And"
Binary \f(CW"&&" performs a short-circuit logical \s-1AND\s0 operation.  That is,
if the left operand is false, the right operand is not even evaluated.
Scalar or list context propagates down to the right operand if it
is evaluated.

### C-style Logical Or

Xref "|| operator, logical, or"
Subsection "C-style Logical Or"
Binary \f(CW"||" performs a short-circuit logical \s-1OR\s0 operation.  That is,
if the left operand is true, the right operand is not even evaluated.
Scalar or list context propagates down to the right operand if it
is evaluated.

### Logical Defined-Or

Xref "operator, logical, defined-or"
Subsection "Logical Defined-Or"
Although it has no direct equivalent in C, Perl's \f(CW\*(C`//\*(C' operator is related
to its C-style \*(L"or\*(R".  In fact, it's exactly the same as \f(CW\*(C`||\*(C', except that it
tests the left hand side's definedness instead of its truth.  Thus,
\f(CW\*(C`EXPR1\ //\ EXPR2\*(C' returns the value of \f(CW\*(C`EXPR1\*(C' if it's defined,
otherwise, the value of \f(CW\*(C`EXPR2\*(C' is returned.
(\f(CW\*(C`EXPR1\*(C' is evaluated in scalar context, \f(CW\*(C`EXPR2\*(C'
in the context of \f(CW\*(C`//\*(C' itself).  Usually,
this is the same result as \f(CW\*(C`defined(EXPR1)\ ?\ EXPR1\ :\ EXPR2\*(C' (except that
the ternary-operator form can be used as a lvalue, while \f(CW\*(C`EXPR1\ //\ EXPR2\*(C'
cannot).  This is very useful for
providing default values for variables.  If you actually want to test if
at least one of \f(CW$x and \f(CW$y is defined, use \f(CW\*(C`defined($x\ //\ $y)\*(C'.

The \f(CW\*(C`||\*(C', \f(CW\*(C`//\*(C' and \f(CW\*(C`&&\*(C' operators return the last value evaluated
(unlike C's \f(CW\*(C`||\*(C' and \f(CW\*(C`&&\*(C', which return 0 or 1).  Thus, a reasonably
portable way to find out the home directory might be:

.Vb 4
    $home =  $ENV\{HOME\}
          // $ENV\{LOGDIR\}
          // (getpwuid($<))[7]
          // die "You\*(Aqre homeless!\\n";
.Ve

In particular, this means that you shouldn't use this
for selecting between two aggregates for assignment:

.Vb 3
    @a = @b || @c;            # This doesn\*(Aqt do the right thing
    @a = scalar(@b) || @c;    # because it really means this.
    @a = @b ? @b : @c;        # This works fine, though.
.Ve

As alternatives to \f(CW\*(C`&&\*(C' and \f(CW\*(C`||\*(C' when used for
control flow, Perl provides the \f(CW\*(C`and\*(C' and \f(CW\*(C`or\*(C' operators (see below).
The short-circuit behavior is identical.  The precedence of \f(CW"and"
and \f(CW"or" is much lower, however, so that you can safely use them after a
list operator without the need for parentheses:

.Vb 2
    unlink "alpha", "beta", "gamma"
            or gripe(), next LINE;
.Ve

With the C-style operators that would have been written like this:

.Vb 2
    unlink("alpha", "beta", "gamma")
            || (gripe(), next LINE);
.Ve

It would be even more readable to write that this way:

.Vb 4
    unless(unlink("alpha", "beta", "gamma")) \{
        gripe();
        next LINE;
    \}
.Ve

Using \f(CW"or" for assignment is unlikely to do what you want; see below.

### Range Operators

Xref "operator, range range .. ..."
Subsection "Range Operators"
Binary \f(CW".." is the range operator, which is really two different
operators depending on the context.  In list context, it returns a
list of values counting (up by ones) from the left value to the right
value.  If the left value is greater than the right value then it
returns the empty list.  The range operator is useful for writing
\f(CW\*(C`foreach\ (1..10)\*(C' loops and for doing slice operations on arrays.  In
the current implementation, no temporary array is created when the
range operator is used as the expression in \f(CW\*(C`foreach\*(C' loops, but older
versions of Perl might burn a lot of memory when you write something
like this:

.Vb 3
    for (1 .. 1_000_000) \{
        # code
    \}
.Ve

The range operator also works on strings, using the magical
auto-increment, see below.

In scalar context, \f(CW".." returns a boolean value.  The operator is
bistable, like a flip-flop, and emulates the line-range (comma)
operator of **sed**, **awk**, and various editors.  Each \f(CW".." operator
maintains its own boolean state, even across calls to a subroutine
that contains it.  It is false as long as its left operand is false.
Once the left operand is true, the range operator stays true until the
right operand is true, *\s-1AFTER\s0* which the range operator becomes false
again.  It doesn't become false till the next time the range operator
is evaluated.  It can test the right operand and become false on the
same evaluation it became true (as in **awk**), but it still returns
true once.  If you don't want it to test the right operand until the
next evaluation, as in **sed**, just use three dots (\f(CW"...") instead of
two.  In all other regards, \f(CW"..." behaves just like \f(CW".." does.

The right operand is not evaluated while the operator is in the
\*(L"false\*(R" state, and the left operand is not evaluated while the
operator is in the \*(L"true\*(R" state.  The precedence is a little lower
than || and &&.  The value returned is either the empty string for
false, or a sequence number (beginning with 1) for true.  The sequence
number is reset for each range encountered.  The final sequence number
in a range has the string \f(CW"E0" appended to it, which doesn't affect
its numeric value, but gives you something to search for if you want
to exclude the endpoint.  You can exclude the beginning point by
waiting for the sequence number to be greater than 1.

If either operand of scalar \f(CW".." is a constant expression,
that operand is considered true if it is equal (\f(CW\*(C`==\*(C') to the current
input line number (the \f(CW$. variable).

To be pedantic, the comparison is actually \f(CW\*(C`int(EXPR)\ ==\ int(EXPR)\*(C',
but that is only an issue if you use a floating point expression; when
implicitly using \f(CW$. as described in the previous paragraph, the
comparison is \f(CW\*(C`int(EXPR)\ ==\ int($.)\*(C' which is only an issue when \f(CW$.
is set to a floating point value and you are not reading from a file.
Furthermore, \f(CW"span"\ ..\ "spat" or \f(CW\*(C`2.18\ ..\ 3.14\*(C' will not do what
you want in scalar context because each of the operands are evaluated
using their integer representation.

Examples:

As a scalar operator:

.Vb 2
    if (101 .. 200) \{ print; \} # print 2nd hundred lines, short for
                               #  if ($. == 101 .. $. == 200) \{ print; \}

    next LINE if (1 .. /^$/);  # skip header lines, short for
                               #   next LINE if ($. == 1 .. /^$/);
                               # (typically in a loop labeled LINE)

    s/^/> / if (/^$/ .. eof());  # quote body

    # parse mail messages
    while (<>) \{
        $in_header =   1  .. /^$/;
        $in_body   = /^$/ .. eof;
        if ($in_header) \{
            # do something
        \} else \{ # in body
            # do something else
        \}
    \} continue \{
        close ARGV if eof;             # reset $. each file
    \}
.Ve

Here's a simple example to illustrate the difference between
the two range operators:

.Vb 4
    @lines = ("   - Foo",
              "01 - Bar",
              "1  - Baz",
              "   - Quux");

    foreach (@lines) \{
        if (/0/ .. /1/) \{
            print "$_\\n";
        \}
    \}
.Ve

This program will print only the line containing \*(L"Bar\*(R".  If
the range operator is changed to \f(CW\*(C`...\*(C', it will also print the
\*(L"Baz\*(R" line.

And now some examples as a list operator:

.Vb 3
    for (101 .. 200) \{ print \}      # print $_ 100 times
    @foo = @foo[0 .. $#foo];        # an expensive no-op
    @foo = @foo[$#foo-4 .. $#foo];  # slice last 5 items
.Ve

Because each operand is evaluated in integer form, \f(CW\*(C`2.18\ ..\ 3.14\*(C' will
return two elements in list context.

.Vb 1
    @list = (2.18 .. 3.14); # same as @list = (2 .. 3);
.Ve

The range operator in list context can make use of the magical
auto-increment algorithm if both operands are strings, subject to the
following rules:

- \(bu
With one exception (below), if both strings look like numbers to Perl,
the magic increment will not be applied, and the strings will be treated
as numbers (more specifically, integers) instead.
.Sp
For example, \f(CW"-2".."2" is the same as \f(CW\*(C`-2..2\*(C', and
\f(CW"2.18".."3.14" produces \f(CW\*(C`2, 3\*(C'.

- \(bu
The exception to the above rule is when the left-hand string begins with
\f(CW0 and is longer than one character, in this case the magic increment
*will* be applied, even though strings like \f(CW"01" would normally look
like a number to Perl.
.Sp
For example, \f(CW"01".."04" produces \f(CW"01", "02", "03", "04", and
\f(CW"00".."-1" produces \f(CW"00" through \f(CW"99" - this may seem
surprising, but see the following rules for why it works this way.
To get dates with leading zeros, you can say:
.Sp
.Vb 2
    @z2 = ("01" .. "31");
    print $z2[$mday];
.Ve
.Sp
If you want to force strings to be interpreted as numbers, you could say
.Sp
.Vb 1
    @numbers = ( 0+$first .. 0+$last );
.Ve
.Sp
**Note:** In Perl versions 5.30 and below, *any* string on the left-hand
side beginning with \f(CW"0", including the string \f(CW"0" itself, would
cause the magic string increment behavior. This means that on these Perl
versions, \f(CW"0".."-1" would produce \f(CW"0" through \f(CW"99", which was
inconsistent with \f(CW\*(C`0..-1\*(C', which produces the empty list. This also means
that \f(CW"0".."9" now produces a list of integers instead of a list of
strings.

- \(bu
If the initial value specified isn't part of a magical increment
sequence (that is, a non-empty string matching \f(CW\*(C`/^[a-zA-Z]*[0-9]*\\z/\*(C'),
only the initial value will be returned.
.Sp
For example, \f(CW"ax".."az" produces \f(CW"ax", "ay", "az", but
\f(CW"*x".."az" produces only \f(CW"*x".

- \(bu
For other initial values that are strings that do follow the rules of the
magical increment, the corresponding sequence will be returned.
.Sp
For example, you can say
.Sp
.Vb 1
    @alphabet = ("A" .. "Z");
.Ve
.Sp
to get all normal letters of the English alphabet, or
.Sp
.Vb 1
    $hexdigit = (0 .. 9, "a" .. "f")[$num & 15];
.Ve
.Sp
to get a hexadecimal digit.

- \(bu
If the final value specified is not in the sequence that the magical
increment would produce, the sequence goes until the next value would
be longer than the final value specified. If the length of the final
string is shorter than the first, the empty list is returned.
.Sp
For example, \f(CW"a".."--" is the same as \f(CW"a".."zz", \f(CW"0".."xx"
produces \f(CW"0" through \f(CW"99", and \f(CW"aaa".."--" returns the empty
list.

As of Perl 5.26, the list-context range operator on strings works as expected
in the scope of \f(CW"use\ feature\ \*(Aqunicode_strings". In previous versions, and outside the scope of
that feature, it exhibits \*(L"The \*(R"Unicode Bug"" in perlunicode: its behavior
depends on the internal encoding of the range endpoint.

Because the magical increment only works on non-empty strings matching
\f(CW\*(C`/^[a-zA-Z]*[0-9]*\\z/\*(C', the following will only return an alpha:

.Vb 2
    use charnames "greek";
    my @greek_small =  ("\\N\{alpha\}" .. "\\N\{omega\}");
.Ve

To get the 25 traditional lowercase Greek letters, including both sigmas,
you could use this instead:

.Vb 5
    use charnames "greek";
    my @greek_small =  map \{ chr \} ( ord("\\N\{alpha\}")
                                        ..
                                     ord("\\N\{omega\}")
                                   );
.Ve

However, because there are *many* other lowercase Greek characters than
just those, to match lowercase Greek characters in a regular expression,
you could use the pattern \f(CW\*(C`/(?:(?=\\p\{Greek\})\\p\{Lower\})+/\*(C' (or the
experimental feature \f(CW\*(C`/(?[\ \\p\{Greek\}\ &\ \\p\{Lower\}\ ])+/\*(C').

### Conditional Operator

Xref "operator, conditional operator, ternary ternary ?:"
Subsection "Conditional Operator"
Ternary \f(CW"?:" is the conditional operator, just as in C.  It works much
like an if-then-else.  If the argument before the \f(CW\*(C`?\*(C' is true, the
argument before the \f(CW\*(C`:\*(C' is returned, otherwise the argument after the
\f(CW\*(C`:\*(C' is returned.  For example:

.Vb 2
    printf "I have %d dog%s.\\n", $n,
            ($n == 1) ? "" : "s";
.Ve

Scalar or list context propagates downward into the 2nd
or 3rd argument, whichever is selected.

.Vb 3
    $x = $ok ? $y : $z;  # get a scalar
    @x = $ok ? @y : @z;  # get an array
    $x = $ok ? @y : @z;  # oops, that\*(Aqs just a count!
.Ve

The operator may be assigned to if both the 2nd and 3rd arguments are
legal lvalues (meaning that you can assign to them):

.Vb 1
    ($x_or_y ? $x : $y) = $z;
.Ve

Because this operator produces an assignable result, using assignments
without parentheses will get you in trouble.  For example, this:

.Vb 1
    $x % 2 ? $x += 10 : $x += 2
.Ve

Really means this:

.Vb 1
    (($x % 2) ? ($x += 10) : $x) += 2
.Ve

Rather than this:

.Vb 1
    ($x % 2) ? ($x += 10) : ($x += 2)
.Ve

That should probably be written more simply as:

.Vb 1
    $x += ($x % 2) ? 10 : 2;
.Ve

### Assignment Operators

Xref "assignment operator, assignment = **= += *= &= <<= &&= -= = |= >>= ||= = .= %= ^= x= &.= |.= ^.="
Subsection "Assignment Operators"
\f(CW"=" is the ordinary assignment operator.

Assignment operators work as in C.  That is,

.Vb 1
    $x += 2;
.Ve

is equivalent to

.Vb 1
    $x = $x + 2;
.Ve

although without duplicating any side effects that dereferencing the lvalue
might trigger, such as from \f(CW\*(C`tie()\*(C'.  Other assignment operators work similarly.
The following are recognized:

.Vb 4
    **=    +=    *=    &=    &.=    <<=    &&=
           -=    /=    |=    |.=    >>=    ||=
           .=    %=    ^=    ^.=           //=
                 x=
.Ve

Although these are grouped by family, they all have the precedence
of assignment.  These combined assignment operators can only operate on
scalars, whereas the ordinary assignment operator can assign to arrays,
hashes, lists and even references.  (See \*(L"Context\*(R"
and \*(L"List value constructors\*(R" in perldata, and \*(L"Assigning to
References\*(R" in perlref.)

Unlike in C, the scalar assignment operator produces a valid lvalue.
Modifying an assignment is equivalent to doing the assignment and
then modifying the variable that was assigned to.  This is useful
for modifying a copy of something, like this:

.Vb 1
    ($tmp = $global) =~ tr/13579/24680/;
.Ve

Although as of 5.14, that can be also be accomplished this way:

.Vb 2
    use v5.14;
    $tmp = ($global =~  tr/13579/24680/r);
.Ve

Likewise,

.Vb 1
    ($x += 2) *= 3;
.Ve

is equivalent to

.Vb 2
    $x += 2;
    $x *= 3;
.Ve

Similarly, a list assignment in list context produces the list of
lvalues assigned to, and a list assignment in scalar context returns
the number of elements produced by the expression on the right hand
side of the assignment.

The three dotted bitwise assignment operators (\f(CW\*(C`&.=\*(C' \f(CW\*(C`|.=\*(C' \f(CW\*(C`^.=\*(C') are new in
Perl 5.22.  See \*(L"Bitwise String Operators\*(R".

### Comma Operator

Xref "comma operator, comma ,"
Subsection "Comma Operator"
Binary \f(CW"," is the comma operator.  In scalar context it evaluates
its left argument, throws that value away, then evaluates its right
argument and returns that value.  This is just like C's comma operator.

In list context, it's just the list argument separator, and inserts
both its arguments into the list.  These arguments are also evaluated
from left to right.

The \f(CW\*(C`=>\*(C' operator (sometimes pronounced \*(L"fat comma\*(R") is a synonym
for the comma except that it causes a
word on its left to be interpreted as a string if it begins with a letter
or underscore and is composed only of letters, digits and underscores.
This includes operands that might otherwise be interpreted as operators,
constants, single number v-strings or function calls.  If in doubt about
this behavior, the left operand can be quoted explicitly.

Otherwise, the \f(CW\*(C`=>\*(C' operator behaves exactly as the comma operator
or list argument separator, according to context.

For example:

.Vb 1
    use constant FOO => "something";

    my %h = ( FOO => 23 );
.Ve

is equivalent to:

.Vb 1
    my %h = ("FOO", 23);
.Ve

It is *\s-1NOT\s0*:

.Vb 1
    my %h = ("something", 23);
.Ve

The \f(CW\*(C`=>\*(C' operator is helpful in documenting the correspondence
between keys and values in hashes, and other paired elements in lists.

.Vb 2
    %hash = ( $key => $value );
    login( $username => $password );
.Ve

The special quoting behavior ignores precedence, and hence may apply to
*part* of the left operand:

.Vb 1
    print time.shift => "bbb";
.Ve

That example prints something like \f(CW"1314363215shiftbbb", because the
\f(CW\*(C`=>\*(C' implicitly quotes the \f(CW\*(C`shift\*(C' immediately on its left, ignoring
the fact that \f(CW\*(C`time.shift\*(C' is the entire left operand.

### List Operators (Rightward)

Xref "operator, list, rightward list operator"
Subsection "List Operators (Rightward)"
On the right side of a list operator, the comma has very low precedence,
such that it controls all comma-separated expressions found there.
The only operators with lower precedence are the logical operators
\f(CW"and", \f(CW"or", and \f(CW"not", which may be used to evaluate calls to list
operators without the need for parentheses:

.Vb 2
    open HANDLE, "< :encoding(UTF-8)", "filename"
        or die "Can\*(Aqt open: $!\\n";
.Ve

However, some people find that code harder to read than writing
it with parentheses:

.Vb 2
    open(HANDLE, "< :encoding(UTF-8)", "filename")
        or die "Can\*(Aqt open: $!\\n";
.Ve

in which case you might as well just use the more customary \f(CW"||" operator:

.Vb 2
    open(HANDLE, "< :encoding(UTF-8)", "filename")
        || die "Can\*(Aqt open: $!\\n";
.Ve

See also discussion of list operators in \*(L"Terms and List Operators (Leftward)\*(R".

### Logical Not

Xref "operator, logical, not not"
Subsection "Logical Not"
Unary \f(CW"not" returns the logical negation of the expression to its right.
It's the equivalent of \f(CW"!" except for the very low precedence.

### Logical And

Xref "operator, logical, and and"
Subsection "Logical And"
Binary \f(CW"and" returns the logical conjunction of the two surrounding
expressions.  It's equivalent to \f(CW\*(C`&&\*(C' except for the very low
precedence.  This means that it short-circuits: the right
expression is evaluated only if the left expression is true.

### Logical or and Exclusive Or

Xref "operator, logical, or operator, logical, xor operator, logical, exclusive or or xor"
Subsection "Logical or and Exclusive Or"
Binary \f(CW"or" returns the logical disjunction of the two surrounding
expressions.  It's equivalent to \f(CW\*(C`||\*(C' except for the very low precedence.
This makes it useful for control flow:

.Vb 1
    print FH $data              or die "Can\*(Aqt write to FH: $!";
.Ve

This means that it short-circuits: the right expression is evaluated
only if the left expression is false.  Due to its precedence, you must
be careful to avoid using it as replacement for the \f(CW\*(C`||\*(C' operator.
It usually works out better for flow control than in assignments:

.Vb 3
    $x = $y or $z;              # bug: this is wrong
    ($x = $y) or $z;            # really means this
    $x = $y || $z;              # better written this way
.Ve

However, when it's a list-context assignment and you're trying to use
\f(CW\*(C`||\*(C' for control flow, you probably need \f(CW"or" so that the assignment
takes higher precedence.

.Vb 2
    @info = stat($file) || die;     # oops, scalar sense of stat!
    @info = stat($file) or die;     # better, now @info gets its due
.Ve

Then again, you could always use parentheses.

Binary \f(CW"xor" returns the exclusive-OR of the two surrounding expressions.
It cannot short-circuit (of course).

There is no low precedence operator for defined-OR.

### C Operators Missing From Perl

Xref "operator, missing from perl & * typecasting (TYPE)"
Subsection "C Operators Missing From Perl"
Here is what C has that Perl doesn't:

- unary &
Item "unary &"
Address-of operator.  (But see the \f(CW"\\" operator for taking a reference.)

- unary *
Item "unary *"
Dereference-address operator.  (Perl's prefix dereferencing
operators are typed: \f(CW\*(C`$\*(C', \f(CW\*(C`@\*(C', \f(CW\*(C`%\*(C', and \f(CW\*(C`&\*(C'.)

- (\s-1TYPE\s0)
Item "(TYPE)"
Type-casting operator.

### Quote and Quote-like Operators

Xref "operator, quote operator, quote-like q qq qx qw m qr s tr ' '' "" """" ` `` << escape sequence escape"
Subsection "Quote and Quote-like Operators"
While we usually think of quotes as literal values, in Perl they
function as operators, providing various kinds of interpolating and
pattern matching capabilities.  Perl provides customary quote characters
for these behaviors, but also provides a way for you to choose your
quote character for any of them.  In the following table, a \f(CW\*(C`\{\}\*(C' represents
any pair of delimiters you choose.

.Vb 11
    Customary  Generic        Meaning        Interpolates
        \*(Aq\*(Aq       q\{\}          Literal             no
        ""      qq\{\}          Literal             yes
        \`\`      qx\{\}          Command             yes*
                qw\{\}         Word list            no
        //       m\{\}       Pattern match          yes*
                qr\{\}          Pattern             yes*
                 s\{\}\{\}      Substitution          yes*
                tr\{\}\{\}    Transliteration         no (but see below)
                 y\{\}\{\}    Transliteration         no (but see below)
        <<EOF                 here-doc            yes*

        * unless the delimiter is \*(Aq\*(Aq.
.Ve

Non-bracketing delimiters use the same character fore and aft, but the four
sorts of \s-1ASCII\s0 brackets (round, angle, square, curly) all nest, which means
that

.Vb 1
    q\{foo\{bar\}baz\}
.Ve

is the same as

.Vb 1
    \*(Aqfoo\{bar\}baz\*(Aq
.Ve

Note, however, that this does not always work for quoting Perl code:

.Vb 1
    $s = q\{ if($x eq "\}") ... \}; # WRONG
.Ve

is a syntax error.  The \f(CW\*(C`Text::Balanced\*(C' module (standard as of v5.8,
and from \s-1CPAN\s0 before then) is able to do this properly.

There can (and in some cases, must) be whitespace between the operator
and the quoting
characters, except when \f(CW\*(C`#\*(C' is being used as the quoting character.
\f(CW\*(C`q#foo#\*(C' is parsed as the string \f(CW\*(C`foo\*(C', while \f(CW\*(C`q\ #foo#\*(C' is the
operator \f(CW\*(C`q\*(C' followed by a comment.  Its argument will be taken
from the next line.  This allows you to write:

.Vb 2
    s \{foo\}  # Replace foo
      \{bar\}  # with bar.
.Ve

The cases where whitespace must be used are when the quoting character
is a word character (meaning it matches \f(CW\*(C`/\\w/\*(C'):

.Vb 2
    q XfooX # Works: means the string \*(Aqfoo\*(Aq
    qXfooX  # WRONG!
.Ve

The following escape sequences are available in constructs that interpolate,
and in transliterations whose delimiters aren't single quotes (\f(CW"\*(Aq").
In all the ones with braces, any number of blanks and/or tabs adjoining
and within the braces are allowed (and ignored).
Xref "\t \n \r \f \b \a \\\ \x \0 \c \N \N\{\} \o\{\}"

.Vb 10
    Sequence     Note  Description
    \\t                  tab               (HT, TAB)
    \\n                  newline           (NL)
    \\r                  return            (CR)
    \\f                  form feed         (FF)
    \\b                  backspace         (BS)
    \\a                  alarm (bell)      (BEL)
    \\e                  escape            (ESC)
    \\x\{263A\}     [1,8]  hex char          (example shown: SMILEY)
    \\x\{ 263A \}          Same, but shows optional blanks inside and
                        adjoining the braces
    \\x1b         [2,8]  restricted range hex char (example: ESC)
    \\N\{name\}     [3]    named Unicode character or character sequence
    \\N\{U+263D\}   [4,8]  Unicode character (example: FIRST QUARTER MOON)
    \\c[          [5]    control char      (example: chr(27))
    \\o\{23072\}    [6,8]  octal char        (example: SMILEY)
    \\033         [7,8]  restricted range octal char  (example: ESC)
.Ve

Note that any escape sequence using braces inside interpolated
constructs may have optional blanks (tab or space characters) adjoining
with and inside of the braces, as illustrated above by the second
\f(CW\*(C`\\x\{\ \}\*(C' example.

- [1]
Item "[1]"
The result is the character specified by the hexadecimal number between
the braces.  See \*(L"[8]\*(R" below for details on which character.
.Sp
Blanks (tab or space characters) may separate the number from either or
both of the braces.
.Sp
Otherwise, only hexadecimal digits are valid between the braces.  If an
invalid character is encountered, a warning will be issued and the
invalid character and all subsequent characters (valid or invalid)
within the braces will be discarded.
.Sp
If there are no valid digits between the braces, the generated character is
the \s-1NULL\s0 character (\f(CW\*(C`\\x\{00\}\*(C').  However, an explicit empty brace (\f(CW\*(C`\\x\{\}\*(C')
will not cause a warning (currently).

- [2]
Item "[2]"
The result is the character specified by the hexadecimal number in the range
0x00 to 0xFF.  See \*(L"[8]\*(R" below for details on which character.
.Sp
Only hexadecimal digits are valid following \f(CW\*(C`\\x\*(C'.  When \f(CW\*(C`\\x\*(C' is followed
by fewer than two valid digits, any valid digits will be zero-padded.  This
means that \f(CW\*(C`\\x7\*(C' will be interpreted as \f(CW\*(C`\\x07\*(C', and a lone \f(CW"\\x" will be
interpreted as \f(CW\*(C`\\x00\*(C'.  Except at the end of a string, having fewer than
two valid digits will result in a warning.  Note that although the warning
says the illegal character is ignored, it is only ignored as part of the
escape and will still be used as the subsequent character in the string.
For example:
.Sp
.Vb 5
  Original    Result    Warns?
  "\\x7"       "\\x07"    no
  "\\x"        "\\x00"    no
  "\\x7q"      "\\x07q"   yes
  "\\xq"       "\\x00q"   yes
.Ve

- [3]
Item "[3]"
The result is the Unicode character or character sequence given by *name*.
See charnames.

- [4]
Item "[4]"
\f(CW\*(C`\\N\{U+\f(CIhexadecimal\ number\f(CW\}\*(C' means the Unicode character whose Unicode code
point is *hexadecimal number*.

- [5]
Item "[5]"
The character following \f(CW\*(C`\\c\*(C' is mapped to some other character as shown in the
table:
.Sp
.Vb 10
 Sequence   Value
   \\c@      chr(0)
   \\cA      chr(1)
   \\ca      chr(1)
   \\cB      chr(2)
   \\cb      chr(2)
   ...
   \\cZ      chr(26)
   \\cz      chr(26)
   \\c[      chr(27)
                     # See below for chr(28)
   \\c]      chr(29)
   \\c^      chr(30)
   \\c_      chr(31)
   \\c?      chr(127) # (on ASCII platforms; see below for link to
                     #  EBCDIC discussion)
.Ve
.Sp
In other words, it's the character whose code point has had 64 xor'd with
its uppercase.  \f(CW\*(C`\\c?\*(C' is \s-1DELETE\s0 on \s-1ASCII\s0 platforms because
\f(CW\*(C`ord("?")\ ^\ 64\*(C' is 127, and
\f(CW\*(C`\\c@\*(C' is \s-1NULL\s0 because the ord of \f(CW"@" is 64, so xor'ing 64 itself produces 0.
.Sp
Also, \f(CW\*(C`\\c\\\f(CIX\f(CW\*(C' yields \f(CW\*(C`\ chr(28)\ .\ "\f(CIX\f(CW"\*(C' for any *X*, but cannot come at the
end of a string, because the backslash would be parsed as escaping the end
quote.
.Sp
On \s-1ASCII\s0 platforms, the resulting characters from the list above are the
complete set of \s-1ASCII\s0 controls.  This isn't the case on \s-1EBCDIC\s0 platforms; see
\*(L"\s-1OPERATOR DIFFERENCES\*(R"\s0 in perlebcdic for a full discussion of the
differences between these for \s-1ASCII\s0 versus \s-1EBCDIC\s0 platforms.
.Sp
Use of any other character following the \f(CW"c" besides those listed above is
discouraged, and as of Perl v5.20, the only characters actually allowed
are the printable \s-1ASCII\s0 ones, minus the left brace \f(CW"\{".  What happens
for any of the allowed other characters is that the value is derived by
xor'ing with the seventh bit, which is 64, and a warning raised if
enabled.  Using the non-allowed characters generates a fatal error.
.Sp
To get platform independent controls, you can use \f(CW\*(C`\\N\{...\}\*(C'.

- [6]
Item "[6]"
The result is the character specified by the octal number between the braces.
See \*(L"[8]\*(R" below for details on which character.
.Sp
Blanks (tab or space characters) may separate the number from either or
both of the braces.
.Sp
Otherwise, if a character that isn't an octal digit is encountered, a
warning is raised, and the value is based on the octal digits before it,
discarding it and all following characters up to the closing brace.  It
is a fatal error if there are no octal digits at all.

- [7]
Item "[7]"
The result is the character specified by the three-digit octal number in the
range 000 to 777 (but best to not use above 077, see next paragraph).  See
\*(L"[8]\*(R" below for details on which character.
.Sp
Some contexts allow 2 or even 1 digit, but any usage without exactly
three digits, the first being a zero, may give unintended results.  (For
example, in a regular expression it may be confused with a backreference;
see \*(L"Octal escapes\*(R" in perlrebackslash.)  Starting in Perl 5.14, you may
use \f(CW\*(C`\\o\{\}\*(C' instead, which avoids all these problems.  Otherwise, it is best to
use this construct only for ordinals \f(CW\*(C`\\077\*(C' and below, remembering to pad to
the left with zeros to make three digits.  For larger ordinals, either use
\f(CW\*(C`\\o\{\}\*(C', or convert to something else, such as to hex and use \f(CW\*(C`\\N\{U+\}\*(C'
(which is portable between platforms with different character sets) or
\f(CW\*(C`\\x\{\}\*(C' instead.

- [8]
Item "[8]"
Several constructs above specify a character by a number.  That number
gives the character's position in the character set encoding (indexed from 0).
This is called synonymously its ordinal, code position, or code point.  Perl
works on platforms that have a native encoding currently of either ASCII/Latin1
or \s-1EBCDIC,\s0 each of which allow specification of 256 characters.  In general, if
the number is 255 (0xFF, 0377) or below, Perl interprets this in the platform's
native encoding.  If the number is 256 (0x100, 0400) or above, Perl interprets
it as a Unicode code point and the result is the corresponding Unicode
character.  For example \f(CW\*(C`\\x\{50\}\*(C' and \f(CW\*(C`\\o\{120\}\*(C' both are the number 80 in
decimal, which is less than 256, so the number is interpreted in the native
character set encoding.  In \s-1ASCII\s0 the character in the 80th position (indexed
from 0) is the letter \f(CW"P", and in \s-1EBCDIC\s0 it is the ampersand symbol \f(CW"&".
\f(CW\*(C`\\x\{100\}\*(C' and \f(CW\*(C`\\o\{400\}\*(C' are both 256 in decimal, so the number is interpreted
as a Unicode code point no matter what the native encoding is.  The name of the
character in the 256th position (indexed by 0) in Unicode is
\f(CW\*(C`LATIN CAPITAL LETTER A WITH MACRON\*(C'.
.Sp
An exception to the above rule is that \f(CW\*(C`\\N\{U+\f(CIhex\ number\f(CW\}\*(C' is
always interpreted as a Unicode code point, so that \f(CW\*(C`\\N\{U+0050\}\*(C' is \f(CW"P" even
on \s-1EBCDIC\s0 platforms.

**\s-1NOTE\s0**: Unlike C and other languages, Perl has no \f(CW\*(C`\\v\*(C' escape sequence for
the vertical tab (\s-1VT,\s0 which is 11 in both \s-1ASCII\s0 and \s-1EBCDIC\s0), but you may
use \f(CW\*(C`\\N\{VT\}\*(C', \f(CW\*(C`\\ck\*(C', \f(CW\*(C`\\N\{U+0b\}\*(C', or \f(CW\*(C`\\x0b\*(C'.  (\f(CW\*(C`\\v\*(C'
does have meaning in regular expression patterns in Perl, see perlre.)

The following escape sequences are available in constructs that interpolate,
but not in transliterations.
Xref "\l \u \L \U \E \Q \F"

.Vb 9
    \\l          lowercase next character only
    \\u          titlecase (not uppercase!) next character only
    \\L          lowercase all characters till \\E or end of string
    \\U          uppercase all characters till \\E or end of string
    \\F          foldcase all characters till \\E or end of string
    \\Q          quote (disable) pattern metacharacters till \\E or
                end of string
    \\E          end either case modification or quoted section
                (whichever was last seen)
.Ve

See \*(L"quotemeta\*(R" in perlfunc for the exact definition of characters that
are quoted by \f(CW\*(C`\\Q\*(C'.

\f(CW\*(C`\\L\*(C', \f(CW\*(C`\\U\*(C', \f(CW\*(C`\\F\*(C', and \f(CW\*(C`\\Q\*(C' can stack, in which case you need one
\f(CW\*(C`\\E\*(C' for each.  For example:

.Vb 2
 say"This \\Qquoting \\ubusiness \\Uhere isn\*(Aqt quite\\E done yet,\\E is it?";
 This quoting\\ Business\\ HERE\\ ISN\\\*(AqT\\ QUITE\\ done\\ yet\\, is it?
.Ve

If a \f(CW\*(C`use\ locale\*(C' form that includes \f(CW\*(C`LC_CTYPE\*(C' is in effect (see
perllocale), the case map used by \f(CW\*(C`\\l\*(C', \f(CW\*(C`\\L\*(C', \f(CW\*(C`\\u\*(C', and \f(CW\*(C`\\U\*(C' is
taken from the current locale.  If Unicode (for example, \f(CW\*(C`\\N\{\}\*(C' or code
points of 0x100 or beyond) is being used, the case map used by \f(CW\*(C`\\l\*(C',
\f(CW\*(C`\\L\*(C', \f(CW\*(C`\\u\*(C', and \f(CW\*(C`\\U\*(C' is as defined by Unicode.  That means that
case-mapping a single character can sometimes produce a sequence of
several characters.
Under \f(CW\*(C`use\ locale\*(C', \f(CW\*(C`\\F\*(C' produces the same results as \f(CW\*(C`\\L\*(C'
for all locales but a \s-1UTF-8\s0 one, where it instead uses the Unicode
definition.

All systems use the virtual \f(CW"\\n" to represent a line terminator,
called a \*(L"newline\*(R".  There is no such thing as an unvarying, physical
newline character.  It is only an illusion that the operating system,
device drivers, C libraries, and Perl all conspire to preserve.  Not all
systems read \f(CW"\\r" as \s-1ASCII CR\s0 and \f(CW"\\n" as \s-1ASCII LF.\s0  For example,
on the ancient Macs (pre-MacOS X) of yesteryear, these used to be reversed,
and on systems without a line terminator,
printing \f(CW"\\n" might emit no actual data.  In general, use \f(CW"\\n" when
you mean a \*(L"newline\*(R" for your system, but use the literal \s-1ASCII\s0 when you
need an exact character.  For example, most networking protocols expect
and prefer a \s-1CR+LF\s0 (\f(CW"\\015\\012" or \f(CW"\\cM\\cJ") for line terminators,
and although they often accept just \f(CW"\\012", they seldom tolerate just
\f(CW"\\015".  If you get in the habit of using \f(CW"\\n" for networking,
you may be burned some day.
Xref "newline line terminator eol end of line \n \r \r\n"

For constructs that do interpolate, variables beginning with "\f(CW\*(C`$\*(C'\*(L"
or \*(R"\f(CW\*(C`@\*(C'" are interpolated.  Subscripted variables such as \f(CW$a[3] or
\f(CW\*(C`$href->\{key\}[0]\*(C' are also interpolated, as are array and hash slices.
But method calls such as \f(CW\*(C`$obj->meth\*(C' are not.

Interpolating an array or slice interpolates the elements in order,
separated by the value of \f(CW$", so is equivalent to interpolating
\f(CW\*(C`join\ $",\ @array\*(C'.  \*(L"Punctuation\*(R" arrays such as \f(CW\*(C`@*\*(C' are usually
interpolated only if the name is enclosed in braces \f(CW\*(C`@\{*\}\*(C', but the
arrays \f(CW@_, \f(CW\*(C`@+\*(C', and \f(CW\*(C`@-\*(C' are interpolated even without braces.

For double-quoted strings, the quoting from \f(CW\*(C`\\Q\*(C' is applied after
interpolation and escapes are processed.

.Vb 1
    "abc\\Qfoo\\tbar$s\\Exyz"
.Ve

is equivalent to

.Vb 1
    "abc" . quotemeta("foo\\tbar$s") . "xyz"
.Ve

For the pattern of regex operators (\f(CW\*(C`qr//\*(C', \f(CW\*(C`m//\*(C' and \f(CW\*(C`s///\*(C'),
the quoting from \f(CW\*(C`\\Q\*(C' is applied after interpolation is processed,
but before escapes are processed.  This allows the pattern to match
literally (except for \f(CW\*(C`$\*(C' and \f(CW\*(C`@\*(C').  For example, the following matches:

.Vb 1
    \*(Aq\\s\\t\*(Aq =~ /\\Q\\s\\t/
.Ve

Because \f(CW\*(C`$\*(C' or \f(CW\*(C`@\*(C' trigger interpolation, you'll need to use something
like \f(CW\*(C`/\\Quser\\E\\@\\Qhost/\*(C' to match them literally.

Patterns are subject to an additional level of interpretation as a
regular expression.  This is done as a second pass, after variables are
interpolated, so that regular expressions may be incorporated into the
pattern from the variables.  If this is not what you want, use \f(CW\*(C`\\Q\*(C' to
interpolate a variable literally.

Apart from the behavior described above, Perl does not expand
multiple levels of interpolation.  In particular, contrary to the
expectations of shell programmers, back-quotes do *\s-1NOT\s0* interpolate
within double quotes, nor do single quotes impede evaluation of
variables when used within double quotes.

### Regexp Quote-Like Operators

Xref "operator, regexp"
Subsection "Regexp Quote-Like Operators"
Here are the quote-like operators that apply to pattern
matching and related activities.
.ie n .IP """qr/*STRING*/msixpodualn""" 8
.el .IP "\f(CWqr/\f(CISTRING\f(CW/msixpodualn" 8
Xref "qr i m o s x p"
Item "qr/STRING/msixpodualn"
This operator quotes (and possibly compiles) its *\s-1STRING\s0* as a regular
expression.  *\s-1STRING\s0* is interpolated the same way as *\s-1PATTERN\s0*
in \f(CW\*(C`m/\f(CIPATTERN\f(CW/\*(C'.  If \f(CW"\*(Aq" is used as the delimiter, no variable
interpolation is done.  Returns a Perl value which may be used instead of the
corresponding \f(CW\*(C`/\f(CISTRING\f(CW/msixpodualn\*(C' expression.  The returned value is a
normalized version of the original pattern.  It magically differs from
a string containing the same characters: \f(CW\*(C`ref(qr/x/)\*(C' returns \*(L"Regexp\*(R";
however, dereferencing it is not well defined (you currently get the
normalized version of the original pattern, but this may change).
.Sp
For example,
.Sp
.Vb 3
    $rex = qr/my.STRING/is;
    print $rex;                 # prints (?si-xm:my.STRING)
    s/$rex/foo/;
.Ve
.Sp
is equivalent to
.Sp
.Vb 1
    s/my.STRING/foo/is;
.Ve
.Sp
The result may be used as a subpattern in a match:
.Sp
.Vb 5
    $re = qr/$pattern/;
    $string =~ /foo$\{re\}bar/;   # can be interpolated in other
                                # patterns
    $string =~ $re;             # or used standalone
    $string =~ /$re/;           # or this way
.Ve
.Sp
Since Perl may compile the pattern at the moment of execution of the \f(CW\*(C`qr()\*(C'
operator, using \f(CW\*(C`qr()\*(C' may have speed advantages in some situations,
notably if the result of \f(CW\*(C`qr()\*(C' is used standalone:
.Sp
.Vb 11
    sub match \{
        my $patterns = shift;
        my @compiled = map qr/$_/i, @$patterns;
        grep \{
            my $success = 0;
            foreach my $pat (@compiled) \{
                $success = 1, last if /$pat/;
            \}
            $success;
        \} @_;
    \}
.Ve
.Sp
Precompilation of the pattern into an internal representation at
the moment of \f(CW\*(C`qr()\*(C' avoids the need to recompile the pattern every
time a match \f(CW\*(C`/$pat/\*(C' is attempted.  (Perl has many other internal
optimizations, but none would be triggered in the above example if
we did not use \f(CW\*(C`qr()\*(C' operator.)
.Sp
Options (specified by the following modifiers) are:
.Sp
.Vb 10
    m   Treat string as multiple lines.
    s   Treat string as single line. (Make . match a newline)
    i   Do case-insensitive pattern matching.
    x   Use extended regular expressions; specifying two
        x\*(Aqs means \\t and the SPACE character are ignored within
        square-bracketed character classes
    p   When matching preserve a copy of the matched string so
        that $\{^PREMATCH\}, $\{^MATCH\}, $\{^POSTMATCH\} will be
        defined (ignored starting in v5.20) as these are always
        defined starting in that release
    o   Compile pattern only once.
    a   ASCII-restrict: Use ASCII for \\d, \\s, \\w and [[:posix:]]
        character classes; specifying two a\*(Aqs adds the further
        restriction that no ASCII character will match a
        non-ASCII one under /i.
    l   Use the current run-time locale\*(Aqs rules.
    u   Use Unicode rules.
    d   Use Unicode or native charset, as in 5.12 and earlier.
    n   Non-capture mode. Don\*(Aqt let () fill in $1, $2, etc...
.Ve
.Sp
If a precompiled pattern is embedded in a larger pattern then the effect
of \f(CW"msixpluadn" will be propagated appropriately.  The effect that the
\f(CW\*(C`/o\*(C' modifier has is not propagated, being restricted to those patterns
explicitly using it.
.Sp
The \f(CW\*(C`/a\*(C', \f(CW\*(C`/d\*(C', \f(CW\*(C`/l\*(C', and \f(CW\*(C`/u\*(C' modifiers (added in Perl 5.14)
control the character set rules, but \f(CW\*(C`/a\*(C' is the only one you are likely
to want to specify explicitly; the other three are selected
automatically by various pragmas.
.Sp
See perlre for additional information on valid syntax for *\s-1STRING\s0*, and
for a detailed look at the semantics of regular expressions.  In
particular, all modifiers except the largely obsolete \f(CW\*(C`/o\*(C' are further
explained in \*(L"Modifiers\*(R" in perlre.  \f(CW\*(C`/o\*(C' is described in the next section.
.ie n .IP """m/*PATTERN*/msixpodualngc""" 8
.el .IP "\f(CWm/\f(CIPATTERN\f(CW/msixpodualngc" 8
Xref "m operator, match regexp, options regexp regex, options regex m s i x p o g c"
Item "m/PATTERN/msixpodualngc"
0
.ie n .IP """/*PATTERN*/msixpodualngc""" 8
.el .IP "\f(CW/\f(CIPATTERN\f(CW/msixpodualngc" 8
Item "/PATTERN/msixpodualngc"
.PD
Searches a string for a pattern match, and in scalar context returns
true if it succeeds, false if it fails.  If no string is specified
via the \f(CW\*(C`=~\*(C' or \f(CW\*(C`!~\*(C' operator, the \f(CW$_ string is searched.  (The
string specified with \f(CW\*(C`=~\*(C' need not be an lvalue\*(--it may be the
result of an expression evaluation, but remember the \f(CW\*(C`=~\*(C' binds
rather tightly.)  See also perlre.
.Sp
Options are as described in \f(CW\*(C`qr//\*(C' above; in addition, the following match
process modifiers are available:
.Sp
.Vb 3
 g  Match globally, i.e., find all occurrences.
 c  Do not reset search position on a failed match when /g is
    in effect.
.Ve
.Sp
If \f(CW"/" is the delimiter then the initial \f(CW\*(C`m\*(C' is optional.  With the \f(CW\*(C`m\*(C'
you can use any pair of non-whitespace (\s-1ASCII\s0) characters
as delimiters.  This is particularly useful for matching path names
that contain \f(CW"/", to avoid \s-1LTS\s0 (leaning toothpick syndrome).  If \f(CW"?" is
the delimiter, then a match-only-once rule applies,
described in \f(CW\*(C`m?\f(CIPATTERN\f(CW?\*(C' below.  If \f(CW"\*(Aq" (single quote) is the delimiter,
no variable interpolation is performed on the *\s-1PATTERN\s0*.
When using a delimiter character valid in an identifier, whitespace is required
after the \f(CW\*(C`m\*(C'.
.Sp
*\s-1PATTERN\s0* may contain variables, which will be interpolated
every time the pattern search is evaluated, except
for when the delimiter is a single quote.  (Note that \f(CW$(, \f(CW$), and
\f(CW$| are not interpolated because they look like end-of-string tests.)
Perl will not recompile the pattern unless an interpolated
variable that it contains changes.  You can force Perl to skip the
test and never recompile by adding a \f(CW\*(C`/o\*(C' (which stands for \*(L"once\*(R")
after the trailing delimiter.
Once upon a time, Perl would recompile regular expressions
unnecessarily, and this modifier was useful to tell it not to do so, in the
interests of speed.  But now, the only reasons to use \f(CW\*(C`/o\*(C' are one of:

> 
- 1.
The variables are thousands of characters long and you know that they
don't change, and you need to wring out the last little bit of speed by
having Perl skip testing for that.  (There is a maintenance penalty for
doing this, as mentioning \f(CW\*(C`/o\*(C' constitutes a promise that you won't
change the variables in the pattern.  If you do change them, Perl won't
even notice.)

- 2.
you want the pattern to use the initial values of the variables
regardless of whether they change or not.  (But there are saner ways
of accomplishing this than using \f(CW\*(C`/o\*(C'.)

- 3.
If the pattern contains embedded code, such as
.Sp
.Vb 3
    use re \*(Aqeval\*(Aq;
    $code = \*(Aqfoo(?\{ $x \})\*(Aq;
    /$code/
.Ve
.Sp
then perl will recompile each time, even though the pattern string hasn't
changed, to ensure that the current value of \f(CW$x is seen each time.
Use \f(CW\*(C`/o\*(C' if you want to avoid this.



> .Sp
The bottom line is that using \f(CW\*(C`/o\*(C' is almost never a good idea.


.ie n .IP "The empty pattern ""//""" 8
.el .IP "The empty pattern \f(CW//" 8
Item "The empty pattern //"
If the *\s-1PATTERN\s0* evaluates to the empty string, the last
*successfully* matched regular expression is used instead.  In this
case, only the \f(CW\*(C`g\*(C' and \f(CW\*(C`c\*(C' flags on the empty pattern are honored;
the other flags are taken from the original pattern.  If no match has
previously succeeded, this will (silently) act instead as a genuine
empty pattern (which will always match).
.Sp
Note that it's possible to confuse Perl into thinking \f(CW\*(C`//\*(C' (the empty
regex) is really \f(CW\*(C`//\*(C' (the defined-or operator).  Perl is usually pretty
good about this, but some pathological cases might trigger this, such as
\f(CW\*(C`$x///\*(C' (is that \f(CW\*(C`($x)\ /\ (//)\*(C' or \f(CW\*(C`$x\ //\ /\*(C'?) and \f(CW\*(C`print\ $fh\ //\*(C'
(\f(CW\*(C`print\ $fh(//\*(C' or \f(CW\*(C`print($fh\ //\*(C'?).  In all of these examples, Perl
will assume you meant defined-or.  If you meant the empty regex, just
use parentheses or spaces to disambiguate, or even prefix the empty
regex with an \f(CW\*(C`m\*(C' (so \f(CW\*(C`//\*(C' becomes \f(CW\*(C`m//\*(C').

- Matching in list context
Item "Matching in list context"
If the \f(CW\*(C`/g\*(C' option is not used, \f(CW\*(C`m//\*(C' in list context returns a
list consisting of the subexpressions matched by the parentheses in the
pattern, that is, (\f(CW$1, \f(CW$2, \f(CW$3...)  (Note that here \f(CW$1 etc. are
also set).  When there are no parentheses in the pattern, the return
value is the list \f(CW\*(C`(1)\*(C' for success.
With or without parentheses, an empty list is returned upon failure.
.Sp
Examples:
.Sp
.Vb 2
 open(TTY, "+</dev/tty")
    || die "can\*(Aqt access /dev/tty: $!";

 <TTY> =~ /^y/i && foo();       # do foo if desired

 if (/Version: *([0-9.]*)/) \{ $version = $1; \}

 next if m#^/usr/spool/uucp#;

 # poor man\*(Aqs grep
 $arg = shift;
 while (<>) \{
    print if /$arg/o; # compile only once (no longer needed!)
 \}

 if (($F1, $F2, $Etc) = ($foo =~ /^(\\S+)\\s+(\\S+)\\s*(.*)/))
.Ve
.Sp
This last example splits \f(CW$foo into the first two words and the
remainder of the line, and assigns those three fields to \f(CW$F1, \f(CW$F2, and
\f(CW$Etc.  The conditional is true if any variables were assigned; that is,
if the pattern matched.
.Sp
The \f(CW\*(C`/g\*(C' modifier specifies global pattern matching\*(--that is,
matching as many times as possible within the string.  How it behaves
depends on the context.  In list context, it returns a list of the
substrings matched by any capturing parentheses in the regular
expression.  If there are no parentheses, it returns a list of all
the matched strings, as if there were parentheses around the whole
pattern.
.Sp
In scalar context, each execution of \f(CW\*(C`m//g\*(C' finds the next match,
returning true if it matches, and false if there is no further match.
The position after the last match can be read or set using the \f(CW\*(C`pos()\*(C'
function; see \*(L"pos\*(R" in perlfunc.  A failed match normally resets the
search position to the beginning of the string, but you can avoid that
by adding the \f(CW\*(C`/c\*(C' modifier (for example, \f(CW\*(C`m//gc\*(C').  Modifying the target
string also resets the search position.
.ie n .IP """\\G *assertion*""" 8
.el .IP "\f(CW\\G \f(CIassertion\f(CW" 8
Item "G assertion"
You can intermix \f(CW\*(C`m//g\*(C' matches with \f(CW\*(C`m/\\G.../g\*(C', where \f(CW\*(C`\\G\*(C' is a
zero-width assertion that matches the exact position where the
previous \f(CW\*(C`m//g\*(C', if any, left off.  Without the \f(CW\*(C`/g\*(C' modifier, the
\f(CW\*(C`\\G\*(C' assertion still anchors at \f(CW\*(C`pos()\*(C' as it was at the start of
the operation (see \*(L"pos\*(R" in perlfunc), but the match is of course only
attempted once.  Using \f(CW\*(C`\\G\*(C' without \f(CW\*(C`/g\*(C' on a target string that has
not previously had a \f(CW\*(C`/g\*(C' match applied to it is the same as using
the \f(CW\*(C`\\A\*(C' assertion to match the beginning of the string.  Note also
that, currently, \f(CW\*(C`\\G\*(C' is only properly supported when anchored at the
very beginning of the pattern.
.Sp
Examples:
.Sp
.Vb 2
    # list context
    ($one,$five,$fifteen) = (\`uptime\` =~ /(\\d+\\.\\d+)/g);

    # scalar context
    local $/ = "";
    while ($paragraph = <>) \{
        while ($paragraph =~ /\\p\{Ll\}[\*(Aq")]*[.!?]+[\*(Aq")]*\\s/g) \{
            $sentences++;
        \}
    \}
    say $sentences;
.Ve
.Sp
Here's another way to check for sentences in a paragraph:
.Sp
.Vb 10
 my $sentence_rx = qr\{
    (?: (?<= ^ ) | (?<= \\s ) )  # after start-of-string or
                                # whitespace
    \\p\{Lu\}                      # capital letter
    .*?                         # a bunch of anything
    (?<= \\S )                   # that ends in non-
                                # whitespace
    (?<! \\b [DMS]r  )           # but isn\*(Aqt a common abbr.
    (?<! \\b Mrs )
    (?<! \\b Sra )
    (?<! \\b St  )
    [.?!]                       # followed by a sentence
                                # ender
    (?= $ | \\s )                # in front of end-of-string
                                # or whitespace
 \}sx;
 local $/ = "";
 while (my $paragraph = <>) \{
    say "NEW PARAGRAPH";
    my $count = 0;
    while ($paragraph =~ /($sentence_rx)/g) \{
        printf "\\tgot sentence %d: <%s>\\n", ++$count, $1;
    \}
 \}
.Ve
.Sp
Here's how to use \f(CW\*(C`m//gc\*(C' with \f(CW\*(C`\\G\*(C':
.Sp
.Vb 10
    $_ = "ppooqppqq";
    while ($i++ < 2) \{
        print "1: \*(Aq";
        print $1 while /(o)/gc; print "\*(Aq, pos=", pos, "\\n";
        print "2: \*(Aq";
        print $1 if /\\G(q)/gc;  print "\*(Aq, pos=", pos, "\\n";
        print "3: \*(Aq";
        print $1 while /(p)/gc; print "\*(Aq, pos=", pos, "\\n";
    \}
    print "Final: \*(Aq$1\*(Aq, pos=",pos,"\\n" if /\\G(.)/;
.Ve
.Sp
The last example should print:
.Sp
.Vb 7
    1: \*(Aqoo\*(Aq, pos=4
    2: \*(Aqq\*(Aq, pos=5
    3: \*(Aqpp\*(Aq, pos=7
    1: \*(Aq\*(Aq, pos=7
    2: \*(Aqq\*(Aq, pos=8
    3: \*(Aq\*(Aq, pos=8
    Final: \*(Aqq\*(Aq, pos=8
.Ve
.Sp
Notice that the final match matched \f(CW\*(C`q\*(C' instead of \f(CW\*(C`p\*(C', which a match
without the \f(CW\*(C`\\G\*(C' anchor would have done.  Also note that the final match
did not update \f(CW\*(C`pos\*(C'.  \f(CW\*(C`pos\*(C' is only updated on a \f(CW\*(C`/g\*(C' match.  If the
final match did indeed match \f(CW\*(C`p\*(C', it's a good bet that you're running an
ancient (pre-5.6.0) version of Perl.
.Sp
A useful idiom for \f(CW\*(C`lex\*(C'-like scanners is \f(CW\*(C`/\\G.../gc\*(C'.  You can
combine several regexps like this to process a string part-by-part,
doing different actions depending on which regexp matched.  Each
regexp tries to match where the previous one leaves off.
.Sp
.Vb 4
 $_ = <<\*(AqEOL\*(Aq;
    $url = URI::URL->new( "http://example.com/" );
    die if $url eq "xXx";
 EOL

 LOOP: \{
     print(" digits"),       redo LOOP if /\\G\\d+\\b[,.;]?\\s*/gc;
     print(" lowercase"),    redo LOOP
                                    if /\\G\\p\{Ll\}+\\b[,.;]?\\s*/gc;
     print(" UPPERCASE"),    redo LOOP
                                    if /\\G\\p\{Lu\}+\\b[,.;]?\\s*/gc;
     print(" Capitalized"),  redo LOOP
                              if /\\G\\p\{Lu\}\\p\{Ll\}+\\b[,.;]?\\s*/gc;
     print(" MiXeD"),        redo LOOP if /\\G\\pL+\\b[,.;]?\\s*/gc;
     print(" alphanumeric"), redo LOOP
                            if /\\G[\\p\{Alpha\}\\pN]+\\b[,.;]?\\s*/gc;
     print(" line-noise"),   redo LOOP if /\\G\\W+/gc;
     print ". That\*(Aqs all!\\n";
 \}
.Ve
.Sp
Here is the output (split into several lines):
.Sp
.Vb 4
 line-noise lowercase line-noise UPPERCASE line-noise UPPERCASE
 line-noise lowercase line-noise lowercase line-noise lowercase
 lowercase line-noise lowercase lowercase line-noise lowercase
 lowercase line-noise MiXeD line-noise. That\*(Aqs all!
.Ve
.ie n .IP """m?*PATTERN*?msixpodualngc""" 8
.el .IP "\f(CWm?\f(CIPATTERN\f(CW?msixpodualngc" 8
Xref "? operator, match-once"
Item "m?PATTERN?msixpodualngc"
This is just like the \f(CW\*(C`m/\f(CIPATTERN\f(CW/\*(C' search, except that it matches
only once between calls to the \f(CW\*(C`reset()\*(C' operator.  This is a useful
optimization when you want to see only the first occurrence of
something in each file of a set of files, for instance.  Only \f(CW\*(C`m??\*(C'
patterns local to the current package are reset.
.Sp
.Vb 7
    while (<>) \{
        if (m?^$?) \{
                            # blank line between header and body
        \}
    \} continue \{
        reset if eof;       # clear m?? status for next file
    \}
.Ve
.Sp
Another example switched the first \*(L"latin1\*(R" encoding it finds
to \*(L"utf8\*(R" in a pod file:
.Sp
.Vb 1
    s//utf8/ if m? ^ =encoding \\h+ \\K latin1 ?x;
.Ve
.Sp
The match-once behavior is controlled by the match delimiter being
\f(CW\*(C`?\*(C'; with any other delimiter this is the normal \f(CW\*(C`m//\*(C' operator.
.Sp
In the past, the leading \f(CW\*(C`m\*(C' in \f(CW\*(C`m?\f(CIPATTERN\f(CW?\*(C' was optional, but omitting it
would produce a deprecation warning.  As of v5.22.0, omitting it produces a
syntax error.  If you encounter this construct in older code, you can just add
\f(CW\*(C`m\*(C'.
.ie n .IP """s/*PATTERN*/*REPLACEMENT*/msixpodualngcer""" 8
.el .IP "\f(CWs/\f(CIPATTERN\f(CW/\f(CIREPLACEMENT\f(CW/msixpodualngcer" 8
Xref "s substitute substitution replace regexp, replace regexp, substitute m s i x p o g c e r"
Item "s/PATTERN/REPLACEMENT/msixpodualngcer"
Searches a string for a pattern, and if found, replaces that pattern
with the replacement text and returns the number of substitutions
made.  Otherwise it returns false (a value that is both an empty string (\f(CW"")
and numeric zero (\f(CW0) as described in \*(L"Relational Operators\*(R").
.Sp
If the \f(CW\*(C`/r\*(C' (non-destructive) option is used then it runs the
substitution on a copy of the string and instead of returning the
number of substitutions, it returns the copy whether or not a
substitution occurred.  The original string is never changed when
\f(CW\*(C`/r\*(C' is used.  The copy will always be a plain string, even if the
input is an object or a tied variable.
.Sp
If no string is specified via the \f(CW\*(C`=~\*(C' or \f(CW\*(C`!~\*(C' operator, the \f(CW$_
variable is searched and modified.  Unless the \f(CW\*(C`/r\*(C' option is used,
the string specified must be a scalar variable, an array element, a
hash element, or an assignment to one of those; that is, some sort of
scalar lvalue.
.Sp
If the delimiter chosen is a single quote, no variable interpolation is
done on either the *\s-1PATTERN\s0* or the *\s-1REPLACEMENT\s0*.  Otherwise, if the
*\s-1PATTERN\s0* contains a \f(CW\*(C`$\*(C' that looks like a variable rather than an
end-of-string test, the variable will be interpolated into the pattern
at run-time.  If you want the pattern compiled only once the first time
the variable is interpolated, use the \f(CW\*(C`/o\*(C' option.  If the pattern
evaluates to the empty string, the last successfully executed regular
expression is used instead.  See perlre for further explanation on these.
.Sp
Options are as with \f(CW\*(C`m//\*(C' with the addition of the following replacement
specific options:
.Sp
.Vb 5
    e   Evaluate the right side as an expression.
    ee  Evaluate the right side as a string then eval the
        result.
    r   Return substitution and leave the original string
        untouched.
.Ve
.Sp
Any non-whitespace delimiter may replace the slashes.  Add space after
the \f(CW\*(C`s\*(C' when using a character allowed in identifiers.  If single quotes
are used, no interpretation is done on the replacement string (the \f(CW\*(C`/e\*(C'
modifier overrides this, however).  Note that Perl treats backticks
as normal delimiters; the replacement text is not evaluated as a command.
If the *\s-1PATTERN\s0* is delimited by bracketing quotes, the *\s-1REPLACEMENT\s0* has
its own pair of quotes, which may or may not be bracketing quotes, for example,
\f(CW\*(C`s(foo)(bar)\*(C' or \f(CW\*(C`s<foo>/bar/\*(C'.  A \f(CW\*(C`/e\*(C' will cause the
replacement portion to be treated as a full-fledged Perl expression
and evaluated right then and there.  It is, however, syntax checked at
compile-time.  A second \f(CW\*(C`e\*(C' modifier will cause the replacement portion
to be \f(CW\*(C`eval\*(C'ed before being run as a Perl expression.
.Sp
Examples:
.Sp
.Vb 1
    s/\\bgreen\\b/mauve/g;              # don\*(Aqt change wintergreen

    $path =~ s|/usr/bin|/usr/local/bin|;

    s/Login: $foo/Login: $bar/; # run-time pattern

    ($foo = $bar) =~ s/this/that/;      # copy first, then
                                        # change
    ($foo = "$bar") =~ s/this/that/;    # convert to string,
                                        # copy, then change
    $foo = $bar =~ s/this/that/r;       # Same as above using /r
    $foo = $bar =~ s/this/that/r
                =~ s/that/the other/r;  # Chained substitutes
                                        # using /r
    @foo = map \{ s/this/that/r \} @bar   # /r is very useful in
                                        # maps

    $count = ($paragraph =~ s/Mister\\b/Mr./g);  # get change-cnt

    $_ = \*(Aqabc123xyz\*(Aq;
    s/\\d+/$&*2/e;               # yields \*(Aqabc246xyz\*(Aq
    s/\\d+/sprintf("%5d",$&)/e;  # yields \*(Aqabc  246xyz\*(Aq
    s/\\w/$& x 2/eg;             # yields \*(Aqaabbcc  224466xxyyzz\*(Aq

    s/%(.)/$percent\{$1\}/g;      # change percent escapes; no /e
    s/%(.)/$percent\{$1\} || $&/ge;       # expr now, so /e
    s/^=(\\w+)/pod($1)/ge;       # use function call

    $_ = \*(Aqabc123xyz\*(Aq;
    $x = s/abc/def/r;           # $x is \*(Aqdef123xyz\*(Aq and
                                # $_ remains \*(Aqabc123xyz\*(Aq.

    # expand variables in $_, but dynamics only, using
    # symbolic dereferencing
    s/\\$(\\w+)/$\{$1\}/g;

    # Add one to the value of any numbers in the string
    s/(\\d+)/1 + $1/eg;

    # Titlecase words in the last 30 characters only
    substr($str, -30) =~ s/\\b(\\p\{Alpha\}+)\\b/\\u\\L$1/g;

    # This will expand any embedded scalar variable
    # (including lexicals) in $_ : First $1 is interpolated
    # to the variable name, and then evaluated
    s/(\\$\\w+)/$1/eeg;

    # Delete (most) C comments.
    $program =~ s \{
        /\\*     # Match the opening delimiter.
        .*?     # Match a minimal number of characters.
        \\*/     # Match the closing delimiter.
    \} []gsx;

    s/^\\s*(.*?)\\s*$/$1/;        # trim whitespace in $_,
                                # expensively

    for ($variable) \{           # trim whitespace in $variable,
                                # cheap
        s/^\\s+//;
        s/\\s+$//;
    \}

    s/([^ ]*) *([^ ]*)/$2 $1/;  # reverse 1st two fields

    $foo !~ s/A/a/g;    # Lowercase all A\*(Aqs in $foo; return
                        # 0 if any were found and changed;
                        # otherwise return 1
.Ve
.Sp
Note the use of \f(CW\*(C`$\*(C' instead of \f(CW\*(C`\\\*(C' in the last example.  Unlike
**sed**, we use the \\<*digit*> form only in the left hand side.
Anywhere else it's $<*digit*>.
.Sp
Occasionally, you can't use just a \f(CW\*(C`/g\*(C' to get all the changes
to occur that you might want.  Here are two common cases:
.Sp
.Vb 2
    # put commas in the right places in an integer
    1 while s/(\\d)(\\d\\d\\d)(?!\\d)/$1,$2/g;

    # expand tabs to 8-column spacing
    1 while s/\\t+/\*(Aq \*(Aq x (length($&)*8 - length($\`)%8)/e;
.Ve
.Sp
While \f(CW\*(C`s///\*(C' accepts the \f(CW\*(C`/c\*(C' flag, it has no effect beyond
producing a warning if warnings are enabled.
Xref " c"

### Quote-Like Operators

Xref "operator, quote-like"
Subsection "Quote-Like Operators"
.ie n .IP """q/*STRING*/""" 4
.el .IP "\f(CWq/\f(CISTRING\f(CW/" 4
Xref "q quote, single ' ''"
Item "q/STRING/"
0
.ie n .IP "\*(Aq*STRING*\*(Aq" 4
.el .IP "\f(CW\*(Aq\f(CISTRING\f(CW\*(Aq" 4
Item "STRING"
.PD
A single-quoted, literal string.  A backslash represents a backslash
unless followed by the delimiter or another backslash, in which case
the delimiter or backslash is interpolated.
.Sp
.Vb 3
    $foo = q!I said, "You said, \*(AqShe said it.\*(Aq"!;
    $bar = q(\*(AqThis is it.\*(Aq);
    $baz = \*(Aq\\n\*(Aq;                # a two-character string
.Ve
.ie n .IP """qq/*STRING*/""" 4
.el .IP "\f(CWqq/\f(CISTRING\f(CW/" 4
Xref "qq quote, double "" """""
Item "qq/STRING/"
0
.ie n .IP """*STRING*""" 4
.el .IP "\f(CW``\f(CISTRING\f(CW''" 4
Item """STRING"""
.PD
A double-quoted, interpolated string.
.Sp
.Vb 4
    $_ .= qq
     (*** The previous line contains the naughty word "$1".\\n)
                if /\\b(tcl|java|python)\\b/i;      # :-)
    $baz = "\\n";                # a one-character string
.Ve
.ie n .IP """qx/*STRING*/""" 4
.el .IP "\f(CWqx/\f(CISTRING\f(CW/" 4
Xref "qx ` `` backtick"
Item "qx/STRING/"
0
.ie n .IP """\`*STRING*\`""" 4
.el .IP "\f(CW\`\f(CISTRING\f(CW\`" 4
Item "STRING"
.PD
A string which is (possibly) interpolated and then executed as a
system command, via */bin/sh* or its equivalent if required.  Shell
wildcards, pipes, and redirections will be honored.  Similarly to
\f(CW\*(C`system\*(C', if the string contains no shell metacharacters then it will
executed directly.  The collected standard output of the command is
returned; standard error is unaffected.  In scalar context, it comes
back as a single (potentially multi-line) string, or \f(CW\*(C`undef\*(C' if the
shell (or command) could not be started.  In list context, returns a
list of lines (however you've defined lines with \f(CW$/ or
\f(CW$INPUT_RECORD_SEPARATOR), or an empty list if the shell (or command)
could not be started.
.Sp
Because backticks do not affect standard error, use shell file descriptor
syntax (assuming the shell supports this) if you care to address this.
To capture a command's \s-1STDERR\s0 and \s-1STDOUT\s0 together:
.Sp
.Vb 1
    $output = \`cmd 2>&1\`;
.Ve
.Sp
To capture a command's \s-1STDOUT\s0 but discard its \s-1STDERR:\s0
.Sp
.Vb 1
    $output = \`cmd 2>/dev/null\`;
.Ve
.Sp
To capture a command's \s-1STDERR\s0 but discard its \s-1STDOUT\s0 (ordering is
important here):
.Sp
.Vb 1
    $output = \`cmd 2>&1 1>/dev/null\`;
.Ve
.Sp
To exchange a command's \s-1STDOUT\s0 and \s-1STDERR\s0 in order to capture the \s-1STDERR\s0
but leave its \s-1STDOUT\s0 to come out the old \s-1STDERR:\s0
.Sp
.Vb 1
    $output = \`cmd 3>&1 1>&2 2>&3 3>&-\`;
.Ve
.Sp
To read both a command's \s-1STDOUT\s0 and its \s-1STDERR\s0 separately, it's easiest
to redirect them separately to files, and then read from those files
when the program is done:
.Sp
.Vb 1
    system("program args 1>program.stdout 2>program.stderr");
.Ve
.Sp
The \s-1STDIN\s0 filehandle used by the command is inherited from Perl's \s-1STDIN.\s0
For example:
.Sp
.Vb 3
    open(SPLAT, "stuff")   || die "can\*(Aqt open stuff: $!";
    open(STDIN, "<&SPLAT") || die "can\*(Aqt dupe SPLAT: $!";
    print STDOUT \`sort\`;
.Ve
.Sp
will print the sorted contents of the file named *\*(L"stuff\*(R"*.
.Sp
Using single-quote as a delimiter protects the command from Perl's
double-quote interpolation, passing it on to the shell instead:
.Sp
.Vb 2
    $perl_info  = qx(ps $$);            # that\*(Aqs Perl\*(Aqs $$
    $shell_info = qx\*(Aqps $$\*(Aq;            # that\*(Aqs the new shell\*(Aqs $$
.Ve
.Sp
How that string gets evaluated is entirely subject to the command
interpreter on your system.  On most platforms, you will have to protect
shell metacharacters if you want them treated literally.  This is in
practice difficult to do, as it's unclear how to escape which characters.
See perlsec for a clean and safe example of a manual \f(CW\*(C`fork()\*(C' and \f(CW\*(C`exec()\*(C'
to emulate backticks safely.
.Sp
On some platforms (notably DOS-like ones), the shell may not be
capable of dealing with multiline commands, so putting newlines in
the string may not get you what you want.  You may be able to evaluate
multiple commands in a single line by separating them with the command
separator character, if your shell supports that (for example, \f(CW\*(C`;\*(C' on
many Unix shells and \f(CW\*(C`&\*(C' on the Windows \s-1NT\s0 \f(CW\*(C`cmd\*(C' shell).
.Sp
Perl will attempt to flush all files opened for
output before starting the child process, but this may not be supported
on some platforms (see perlport).  To be safe, you may need to set
\f(CW$| (\f(CW$AUTOFLUSH in \f(CW\*(C`English\*(C') or call the \f(CW\*(C`autoflush()\*(C' method of
\f(CW\*(C`IO::Handle\*(C' on any open handles.
.Sp
Beware that some command shells may place restrictions on the length
of the command line.  You must ensure your strings don't exceed this
limit after any necessary interpolations.  See the platform-specific
release notes for more details about your particular environment.
.Sp
Using this operator can lead to programs that are difficult to port,
because the shell commands called vary between systems, and may in
fact not be present at all.  As one example, the \f(CW\*(C`type\*(C' command under
the \s-1POSIX\s0 shell is very different from the \f(CW\*(C`type\*(C' command under \s-1DOS.\s0
That doesn't mean you should go out of your way to avoid backticks
when they're the right way to get something done.  Perl was made to be
a glue language, and one of the things it glues together is commands.
Just understand what you're getting yourself into.
.Sp
Like \f(CW\*(C`system\*(C', backticks put the child process exit code in \f(CW$?.
If you'd like to manually inspect failure, you can check all possible
failure modes by inspecting \f(CW$? like this:
.Sp
.Vb 10
    if ($? == -1) \{
        print "failed to execute: $!\\n";
    \}
    elsif ($? & 127) \{
        printf "child died with signal %d, %s coredump\\n",
            ($? & 127),  ($? & 128) ? \*(Aqwith\*(Aq : \*(Aqwithout\*(Aq;
    \}
    else \{
        printf "child exited with value %d\\n", $? >> 8;
    \}
.Ve
.Sp
Use the open pragma to control the I/O layers used when reading the
output of the command, for example:
.Sp
.Vb 2
  use open IN => ":encoding(UTF-8)";
  my $x = \`cmd-producing-utf-8\`;
.Ve
.Sp
\f(CW\*(C`qx//\*(C' can also be called like a function with \*(L"readpipe\*(R" in perlfunc.
.Sp
See \*(L"I/O Operators\*(R" for more discussion.
.ie n .IP """qw/*STRING*/""" 4
.el .IP "\f(CWqw/\f(CISTRING\f(CW/" 4
Xref "qw quote, list quote, words"
Item "qw/STRING/"
Evaluates to a list of the words extracted out of *\s-1STRING\s0*, using embedded
whitespace as the word delimiters.  It can be understood as being roughly
equivalent to:
.Sp
.Vb 1
    split(" ", q/STRING/);
.Ve
.Sp
the differences being that it only splits on \s-1ASCII\s0 whitespace,
generates a real list at compile time, and
in scalar context it returns the last element in the list.  So
this expression:
.Sp
.Vb 1
    qw(foo bar baz)
.Ve
.Sp
is semantically equivalent to the list:
.Sp
.Vb 1
    "foo", "bar", "baz"
.Ve
.Sp
Some frequently seen examples:
.Sp
.Vb 2
    use POSIX qw( setlocale localeconv )
    @EXPORT = qw( foo bar baz );
.Ve
.Sp
A common mistake is to try to separate the words with commas or to
put comments into a multi-line \f(CW\*(C`qw\*(C'-string.  For this reason, the
\f(CW\*(C`use\ warnings\*(C' pragma and the **-w** switch (that is, the \f(CW$^W variable)
produces warnings if the *\s-1STRING\s0* contains the \f(CW"," or the \f(CW"#" character.
.ie n .IP """tr/*SEARCHLIST*/*REPLACEMENTLIST*/cdsr""" 4
.el .IP "\f(CWtr/\f(CISEARCHLIST\f(CW/\f(CIREPLACEMENTLIST\f(CW/cdsr" 4
Xref "tr y transliterate c d s"
Item "tr/SEARCHLIST/REPLACEMENTLIST/cdsr"
0
.ie n .IP """y/*SEARCHLIST*/*REPLACEMENTLIST*/cdsr""" 4
.el .IP "\f(CWy/\f(CISEARCHLIST\f(CW/\f(CIREPLACEMENTLIST\f(CW/cdsr" 4
Item "y/SEARCHLIST/REPLACEMENTLIST/cdsr"
.PD
Transliterates all occurrences of the characters found (or not found
if the \f(CW\*(C`/c\*(C' modifier is specified) in the search list with the
positionally corresponding character in the replacement list, possibly
deleting some, depending on the modifiers specified.  It returns the
number of characters replaced or deleted.  If no string is specified via
the \f(CW\*(C`=~\*(C' or \f(CW\*(C`!~\*(C' operator, the \f(CW$_ string is transliterated.
.Sp
For **sed** devotees, \f(CW\*(C`y\*(C' is provided as a synonym for \f(CW\*(C`tr\*(C'.
.Sp
If the \f(CW\*(C`/r\*(C' (non-destructive) option is present, a new copy of the string
is made and its characters transliterated, and this copy is returned no
matter whether it was modified or not: the original string is always
left unchanged.  The new copy is always a plain string, even if the input
string is an object or a tied variable.
.Sp
Unless the \f(CW\*(C`/r\*(C' option is used, the string specified with \f(CW\*(C`=~\*(C' must be a
scalar variable, an array element, a hash element, or an assignment to one
of those; in other words, an lvalue.
.Sp
The characters delimitting *\s-1SEARCHLIST\s0* and *\s-1REPLACEMENTLIST\s0*
can be any printable character, not just forward slashes.  If they
are single quotes (\f(CW\*(C`tr\*(Aq\f(CISEARCHLIST\f(CW\*(Aq\f(CIREPLACEMENTLIST\f(CW\*(Aq\*(C'), the only
interpolation is removal of \f(CW\*(C`\\\*(C' from pairs of \f(CW\*(C`\\\\\*(C'.
.Sp
Otherwise, a character range may be specified with a hyphen, so
\f(CW\*(C`tr/A-J/0-9/\*(C' does the same replacement as
\f(CW\*(C`tr/ACEGIBDFHJ/0246813579/\*(C'.
.Sp
If the *\s-1SEARCHLIST\s0* is delimited by bracketing quotes, the
*\s-1REPLACEMENTLIST\s0* must have its own pair of quotes, which may or may
not be bracketing quotes; for example, \f(CW\*(C`tr[aeiouy][yuoiea]\*(C' or
\f(CW\*(C`tr(+\\-*/)/ABCD/\*(C'.
.Sp
Characters may be literals, or (if the delimiters aren't single quotes)
any of the escape sequences accepted in double-quoted strings.  But
there is never any variable interpolation, so \f(CW"$" and \f(CW"@" are
always treated as literals.  A hyphen at the beginning or end, or
preceded by a backslash is also always considered a literal.  Escape
sequence details are in the table near the beginning of this
section.
.Sp
Note that \f(CW\*(C`tr\*(C' does **not** do regular expression character classes such as
\f(CW\*(C`\\d\*(C' or \f(CW\*(C`\\pL\*(C'.  The \f(CW\*(C`tr\*(C' operator is not equivalent to the \f(CWtr(1)
utility.  \f(CW\*(C`tr[a-z][A-Z]\*(C' will uppercase the 26 letters \*(L"a\*(R" through \*(L"z\*(R",
but for case changing not confined to \s-1ASCII,\s0 use
\f(CW\*(C`lc\*(C', \f(CW\*(C`uc\*(C',
\f(CW\*(C`lcfirst\*(C', \f(CW\*(C`ucfirst\*(C'
(all documented in perlfunc), or the
substitution operator \f(CW\*(C`s/\f(CIPATTERN\f(CW/\f(CIREPLACEMENT\f(CW/\*(C'
(with \f(CW\*(C`\\U\*(C', \f(CW\*(C`\\u\*(C', \f(CW\*(C`\\L\*(C', and \f(CW\*(C`\\l\*(C' string-interpolation escapes in the
*\s-1REPLACEMENT\s0* portion).
.Sp
Most ranges are unportable between character sets, but certain ones
signal Perl to do special handling to make them portable.  There are two
classes of portable ranges.  The first are any subsets of the ranges
\f(CW\*(C`A-Z\*(C', \f(CW\*(C`a-z\*(C', and \f(CW\*(C`0-9\*(C', when expressed as literal characters.
.Sp
.Vb 1
  tr/h-k/H-K/
.Ve
.Sp
capitalizes the letters \f(CW"h", \f(CW"i", \f(CW"j", and \f(CW"k" and nothing
else, no matter what the platform's character set is.  In contrast, all
of
.Sp
.Vb 3
  tr/\\x68-\\x6B/\\x48-\\x4B/
  tr/h-\\x6B/H-\\x4B/
  tr/\\x68-k/\\x48-K/
.Ve
.Sp
do the same capitalizations as the previous example when run on \s-1ASCII\s0
platforms, but something completely different on \s-1EBCDIC\s0 ones.
.Sp
The second class of portable ranges is invoked when one or both of the
range's end points are expressed as \f(CW\*(C`\\N\{...\}\*(C'
.Sp
.Vb 1
 $string =~ tr/\\N\{U+20\}-\\N\{U+7E\}//d;
.Ve
.Sp
removes from \f(CW$string all the platform's characters which are
equivalent to any of Unicode U+0020, U+0021, ... U+007D, U+007E.  This
is a portable range, and has the same effect on every platform it is
run on.  In this example, these are the \s-1ASCII\s0
printable characters.  So after this is run, \f(CW$string has only
controls and characters which have no \s-1ASCII\s0 equivalents.
.Sp
But, even for portable ranges, it is not generally obvious what is
included without having to look things up in the manual.  A sound
principle is to use only ranges that both begin from, and end at, either
\s-1ASCII\s0 alphabetics of equal case (\f(CW\*(C`b-e\*(C', \f(CW\*(C`B-E\*(C'), or digits (\f(CW\*(C`1-4\*(C').
Anything else is unclear (and unportable unless \f(CW\*(C`\\N\{...\}\*(C' is used).  If
in doubt, spell out the character sets in full.
.Sp
Options:
.Sp
.Vb 5
    c   Complement the SEARCHLIST.
    d   Delete found but unreplaced characters.
    r   Return the modified string and leave the original string
        untouched.
    s   Squash duplicate replaced characters.
.Ve
.Sp
If the \f(CW\*(C`/d\*(C' modifier is specified, any characters specified by
*\s-1SEARCHLIST\s0*  not found in *\s-1REPLACEMENTLIST\s0* are deleted.  (Note that
this is slightly more flexible than the behavior of some **tr** programs,
which delete anything they find in the *\s-1SEARCHLIST\s0*, period.)
.Sp
If the \f(CW\*(C`/s\*(C' modifier is specified, sequences of characters, all in a
row, that were transliterated to the same character are squashed down to
a single instance of that character.
.Sp
.Vb 2
 my $a = "aaabbbca";
 $a =~ tr/ab/dd/s;     # $a now is "dcd"
.Ve
.Sp
If the \f(CW\*(C`/d\*(C' modifier is used, the *\s-1REPLACEMENTLIST\s0* is always interpreted
exactly as specified.  Otherwise, if the *\s-1REPLACEMENTLIST\s0* is shorter
than the *\s-1SEARCHLIST\s0*, the final character, if any, is replicated until
it is long enough.  There won't be a final character if and only if the
*\s-1REPLACEMENTLIST\s0* is empty, in which case *\s-1REPLACEMENTLIST\s0* is
copied from *\s-1SEARCHLIST\s0*.    An empty *\s-1REPLACEMENTLIST\s0* is useful
for counting characters in a class, or for squashing character sequences
in a class.
.Sp
.Vb 4
    tr/abcd//            tr/abcd/abcd/
    tr/abcd/AB/          tr/abcd/ABBB/
    tr/abcd//d           s/[abcd]//g
    tr/abcd/AB/d         (tr/ab/AB/ + s/[cd]//g)  - but run together
.Ve
.Sp
If the \f(CW\*(C`/c\*(C' modifier is specified, the characters to be transliterated
are the ones \s-1NOT\s0 in *\s-1SEARCHLIST\s0*, that is, it is complemented.  If
\f(CW\*(C`/d\*(C' and/or \f(CW\*(C`/s\*(C' are also specified, they apply to the complemented
*\s-1SEARCHLIST\s0*.  Recall, that if *\s-1REPLACEMENTLIST\s0* is empty (except
under \f(CW\*(C`/d\*(C') a copy of *\s-1SEARCHLIST\s0* is used instead.  That copy is made
after complementing under \f(CW\*(C`/c\*(C'.  *\s-1SEARCHLIST\s0* is sorted by code point
order after complementing, and any *\s-1REPLACEMENTLIST\s0*  is applied to
that sorted result.  This means that under \f(CW\*(C`/c\*(C', the order of the
characters specified in *\s-1SEARCHLIST\s0* is irrelevant.  This can
lead to different results on \s-1EBCDIC\s0 systems if *\s-1REPLACEMENTLIST\s0*
contains more than one character, hence it is generally non-portable to
use \f(CW\*(C`/c\*(C' with such a *\s-1REPLACEMENTLIST\s0*.
.Sp
Another way of describing the operation is this:
If \f(CW\*(C`/c\*(C' is specified, the *\s-1SEARCHLIST\s0* is sorted by code point order,
then complemented.  If *\s-1REPLACEMENTLIST\s0* is empty and \f(CW\*(C`/d\*(C' is not
specified, *\s-1REPLACEMENTLIST\s0* is replaced by a copy of *\s-1SEARCHLIST\s0* (as
modified under \f(CW\*(C`/c\*(C'), and these potentially modified lists are used as
the basis for what follows.  Any character in the target string that
isn't in *\s-1SEARCHLIST\s0* is passed through unchanged.  Every other
character in the target string is replaced by the character in
*\s-1REPLACEMENTLIST\s0* that positionally corresponds to its mate in
*\s-1SEARCHLIST\s0*, except that under \f(CW\*(C`/s\*(C', the 2nd and following characters
are squeezed out in a sequence of characters in a row that all translate
to the same character.  If *\s-1SEARCHLIST\s0* is longer than
*\s-1REPLACEMENTLIST\s0*, characters in the target string that match a
character in *\s-1SEARCHLIST\s0* that doesn't have a correspondence in
*\s-1REPLACEMENTLIST\s0* are either deleted from the target string if \f(CW\*(C`/d\*(C' is
specified; or replaced by the final character in *\s-1REPLACEMENTLIST\s0* if
\f(CW\*(C`/d\*(C' isn't specified.
.Sp
Some examples:
.Sp
.Vb 1
 $ARGV[1] =~ tr/A-Z/a-z/;   # canonicalize to lower case ASCII

 $cnt = tr/*/*/;            # count the stars in $_
 $cnt = tr/*//;             # same thing

 $cnt = $sky =~ tr/*/*/;    # count the stars in $sky
 $cnt = $sky =~ tr/*//;     # same thing

 $cnt = $sky =~ tr/*//c;    # count all the non-stars in $sky
 $cnt = $sky =~ tr/*/*/c;   # same, but transliterate each non-star
                            # into a star, leaving the already-stars
                            # alone.  Afterwards, everything in $sky
                            # is a star.

 $cnt = tr/0-9//;           # count the ASCII digits in $_

 tr/a-zA-Z//s;              # bookkeeper -> bokeper
 tr/o/o/s;                  # bookkeeper -> bokkeeper
 tr/oe/oe/s;                # bookkeeper -> bokkeper
 tr/oe//s;                  # bookkeeper -> bokkeper
 tr/oe/o/s;                 # bookkeeper -> bokkopor

 ($HOST = $host) =~ tr/a-z/A-Z/;
  $HOST = $host  =~ tr/a-z/A-Z/r; # same thing

 $HOST = $host =~ tr/a-z/A-Z/r   # chained with s///r
               =~ s/:/ -p/r;

 tr/a-zA-Z/ /cs;                 # change non-alphas to single space

 @stripped = map tr/a-zA-Z/ /csr, @original;
                                 # /r with map

 tr [\\200-\\377]
    [\\000-\\177];                 # wickedly delete 8th bit

 $foo !~ tr/A/a/    # transliterate all the A\*(Aqs in $foo to \*(Aqa\*(Aq,
                    # return 0 if any were found and changed.
                    # Otherwise return 1
.Ve
.Sp
If multiple transliterations are given for a character, only the
first one is used:
.Sp
.Vb 1
 tr/AAA/XYZ/
.Ve
.Sp
will transliterate any A to X.
.Sp
Because the transliteration table is built at compile time, neither
the *\s-1SEARCHLIST\s0* nor the *\s-1REPLACEMENTLIST\s0* are subjected to double quote
interpolation.  That means that if you want to use variables, you
must use an \f(CW\*(C`eval()\*(C':
.Sp
.Vb 2
 eval "tr/$oldlist/$newlist/";
 die $@ if $@;

 eval "tr/$oldlist/$newlist/, 1" or die $@;
.Ve
.ie n .IP """<<*EOF*""" 4
.el .IP "\f(CW<<\f(CIEOF\f(CW" 4
Xref "here-doc heredoc here-document <<"
Item "<<EOF"
A line-oriented form of quoting is based on the shell \*(L"here-document\*(R"
syntax.  Following a \f(CW\*(C`<<\*(C' you specify a string to terminate
the quoted material, and all lines following the current line down to
the terminating string are the value of the item.
.Sp
Prefixing the terminating string with a \f(CW\*(C`~\*(C' specifies that you
want to use \*(L"Indented Here-docs\*(R" (see below).
.Sp
The terminating string may be either an identifier (a word), or some
quoted text.  An unquoted identifier works like double quotes.
There may not be a space between the \f(CW\*(C`<<\*(C' and the identifier,
unless the identifier is explicitly quoted.  The terminating string
must appear by itself (unquoted and with no surrounding whitespace)
on the terminating line.
.Sp
If the terminating string is quoted, the type of quotes used determine
the treatment of the text.

> 
- Double Quotes
Item "Double Quotes"
Double quotes indicate that the text will be interpolated using exactly
the same rules as normal double quoted strings.
.Sp
.Vb 3
       print <<EOF;
    The price is $Price.
    EOF

       print << "EOF"; # same as above
    The price is $Price.
    EOF
.Ve

- Single Quotes
Item "Single Quotes"
Single quotes indicate the text is to be treated literally with no
interpolation of its content.  This is similar to single quoted
strings except that backslashes have no special meaning, with \f(CW\*(C`\\\\\*(C'
being treated as two backslashes and not one as they would in every
other quoting construct.
.Sp
Just as in the shell, a backslashed bareword following the \f(CW\*(C`<<\*(C'
means the same thing as a single-quoted string does:
.Sp
.Vb 3
        $cost = <<\*(AqVISTA\*(Aq;  # hasta la ...
    That\*(Aqll be $10 please, ma\*(Aqam.
    VISTA

        $cost = <<\\VISTA;   # Same thing!
    That\*(Aqll be $10 please, ma\*(Aqam.
    VISTA
.Ve
.Sp
This is the only form of quoting in perl where there is no need
to worry about escaping content, something that code generators
can and do make good use of.

- Backticks
Item "Backticks"
The content of the here doc is treated just as it would be if the
string were embedded in backticks.  Thus the content is interpolated
as though it were double quoted and then executed via the shell, with
the results of the execution returned.
.Sp
.Vb 3
       print << \`EOC\`; # execute command and get results
    echo hi there
    EOC
.Ve



> 
- Indented Here-docs
Item "Indented Here-docs"
The here-doc modifier \f(CW\*(C`~\*(C' allows you to indent your here-docs to make
the code more readable:
.Sp
.Vb 5
    if ($some_var) \{
      print <<~EOF;
        This is a here-doc
        EOF
    \}
.Ve
.Sp
This will print...
.Sp
.Vb 1
    This is a here-doc
.Ve
.Sp
...with no leading whitespace.
.Sp
The delimiter is used to determine the **exact** whitespace to
remove from the beginning of each line.  All lines **must** have
at least the same starting whitespace (except lines only
containing a newline) or perl will croak.  Tabs and spaces can
be mixed, but are matched exactly.  One tab will not be equal to
8 spaces!
.Sp
Additional beginning whitespace (beyond what preceded the
delimiter) will be preserved:
.Sp
.Vb 5
    print <<~EOF;
      This text is not indented
        This text is indented with two spaces
                This text is indented with two tabs
      EOF
.Ve
.Sp
Finally, the modifier may be used with all of the forms
mentioned above:
.Sp
.Vb 4
    <<~\\EOF;
    <<~\*(AqEOF\*(Aq
    <<~"EOF"
    <<~\`EOF\`
.Ve
.Sp
And whitespace may be used between the \f(CW\*(C`~\*(C' and quoted delimiters:
.Sp
.Vb 1
    <<~ \*(AqEOF\*(Aq; # ... "EOF", \`EOF\`
.Ve



> .Sp
It is possible to stack multiple here-docs in a row:
.Sp
.Vb 5
       print <<"foo", <<"bar"; # you can stack them
    I said foo.
    foo
    I said bar.
    bar

       myfunc(<< "THIS", 23, <<\*(AqTHAT\*(Aq);
    Here\*(Aqs a line
    or two.
    THIS
    and here\*(Aqs another.
    THAT
.Ve
.Sp
Just don't forget that you have to put a semicolon on the end
to finish the statement, as Perl doesn't know you're not going to
try to do this:
.Sp
.Vb 4
       print <<ABC
    179231
    ABC
       + 20;
.Ve
.Sp
If you want to remove the line terminator from your here-docs,
use \f(CW\*(C`chomp()\*(C'.
.Sp
.Vb 3
    chomp($string = <<\*(AqEND\*(Aq);
    This is a string.
    END
.Ve
.Sp
If you want your here-docs to be indented with the rest of the code,
use the \f(CW\*(C`<<~FOO\*(C' construct described under \*(L"Indented Here-docs\*(R":
.Sp
.Vb 4
    $quote = <<~\*(AqFINIS\*(Aq;
       The Road goes ever on and on,
       down from the door where it began.
       FINIS
.Ve
.Sp
If you use a here-doc within a delimited construct, such as in \f(CW\*(C`s///eg\*(C',
the quoted material must still come on the line following the
\f(CW\*(C`<<FOO\*(C' marker, which means it may be inside the delimited
construct:
.Sp
.Vb 4
    s/this/<<E . \*(Aqthat\*(Aq
    the other
    E
     . \*(Aqmore \*(Aq/eg;
.Ve
.Sp
It works this way as of Perl 5.18.  Historically, it was inconsistent, and
you would have to write
.Sp
.Vb 4
    s/this/<<E . \*(Aqthat\*(Aq
     . \*(Aqmore \*(Aq/eg;
    the other
    E
.Ve
.Sp
outside of string evals.
.Sp
Additionally, quoting rules for the end-of-string identifier are
unrelated to Perl's quoting rules.  \f(CW\*(C`q()\*(C', \f(CW\*(C`qq()\*(C', and the like are not
supported in place of \f(CW\*(Aq\*(Aq and \f(CW"", and the only interpolation is for
backslashing the quoting character:
.Sp
.Vb 3
    print << "abc\\"def";
    testing...
    abc"def
.Ve
.Sp
Finally, quoted strings cannot span multiple lines.  The general rule is
that the identifier must be a string literal.  Stick with that, and you
should be safe.



### Gory details of parsing quoted constructs

Xref "quote, gory details"
Subsection "Gory details of parsing quoted constructs"
When presented with something that might have several different
interpretations, Perl uses the **\s-1DWIM\s0** (that's \*(L"Do What I Mean\*(R")
principle to pick the most probable interpretation.  This strategy
is so successful that Perl programmers often do not suspect the
ambivalence of what they write.  But from time to time, Perl's
notions differ substantially from what the author honestly meant.

This section hopes to clarify how Perl handles quoted constructs.
Although the most common reason to learn this is to unravel labyrinthine
regular expressions, because the initial steps of parsing are the
same for all quoting operators, they are all discussed together.

The most important Perl parsing rule is the first one discussed
below: when processing a quoted construct, Perl first finds the end
of that construct, then interprets its contents.  If you understand
this rule, you may skip the rest of this section on the first
reading.  The other rules are likely to contradict the user's
expectations much less frequently than this first one.

Some passes discussed below are performed concurrently, but because
their results are the same, we consider them individually.  For different
quoting constructs, Perl performs different numbers of passes, from
one to four, but these passes are always performed in the same order.

- Finding the end
Item "Finding the end"
The first pass is finding the end of the quoted construct.  This results
in saving to a safe location a copy of the text (between the starting
and ending delimiters), normalized as necessary to avoid needing to know
what the original delimiters were.
.Sp
If the construct is a here-doc, the ending delimiter is a line
that has a terminating string as the content.  Therefore \f(CW\*(C`<<EOF\*(C' is
terminated by \f(CW\*(C`EOF\*(C' immediately followed by \f(CW"\\n" and starting
from the first column of the terminating line.
When searching for the terminating line of a here-doc, nothing
is skipped.  In other words, lines after the here-doc syntax
are compared with the terminating string line by line.
.Sp
For the constructs except here-docs, single characters are used as starting
and ending delimiters.  If the starting delimiter is an opening punctuation
(that is \f(CW\*(C`(\*(C', \f(CW\*(C`[\*(C', \f(CW\*(C`\{\*(C', or \f(CW\*(C`<\*(C'), the ending delimiter is the
corresponding closing punctuation (that is \f(CW\*(C`)\*(C', \f(CW\*(C`]\*(C', \f(CW\*(C`\}\*(C', or \f(CW\*(C`>\*(C').
If the starting delimiter is an unpaired character like \f(CW\*(C`/\*(C' or a closing
punctuation, the ending delimiter is the same as the starting delimiter.
Therefore a \f(CW\*(C`/\*(C' terminates a \f(CW\*(C`qq//\*(C' construct, while a \f(CW\*(C`]\*(C' terminates
both \f(CW\*(C`qq[]\*(C' and \f(CW\*(C`qq]]\*(C' constructs.
.Sp
When searching for single-character delimiters, escaped delimiters
and \f(CW\*(C`\\\\\*(C' are skipped.  For example, while searching for terminating \f(CW\*(C`/\*(C',
combinations of \f(CW\*(C`\\\\\*(C' and \f(CW\*(C`\\/\*(C' are skipped.  If the delimiters are
bracketing, nested pairs are also skipped.  For example, while searching
for a closing \f(CW\*(C`]\*(C' paired with the opening \f(CW\*(C`[\*(C', combinations of \f(CW\*(C`\\\\\*(C', \f(CW\*(C`\\]\*(C',
and \f(CW\*(C`\\[\*(C' are all skipped, and nested \f(CW\*(C`[\*(C' and \f(CW\*(C`]\*(C' are skipped as well.
However, when backslashes are used as the delimiters (like \f(CW\*(C`qq\\\\\*(C' and
\f(CW\*(C`tr\\\\\\\*(C'), nothing is skipped.
During the search for the end, backslashes that escape delimiters or
other backslashes are removed (exactly speaking, they are not copied to the
safe location).
.Sp
For constructs with three-part delimiters (\f(CW\*(C`s///\*(C', \f(CW\*(C`y///\*(C', and
\f(CW\*(C`tr///\*(C'), the search is repeated once more.
If the first delimiter is not an opening punctuation, the three delimiters must
be the same, such as \f(CW\*(C`s!!!\*(C' and \f(CW\*(C`tr)))\*(C',
in which case the second delimiter
terminates the left part and starts the right part at once.
If the left part is delimited by bracketing punctuation (that is \f(CW\*(C`()\*(C',
\f(CW\*(C`[]\*(C', \f(CW\*(C`\{\}\*(C', or \f(CW\*(C`<>\*(C'), the right part needs another pair of
delimiters such as \f(CW\*(C`s()\{\}\*(C' and \f(CW\*(C`tr[]//\*(C'.  In these cases, whitespace
and comments are allowed between the two parts, although the comment must follow
at least one whitespace character; otherwise a character expected as the
start of the comment may be regarded as the starting delimiter of the right part.
.Sp
During this search no attention is paid to the semantics of the construct.
Thus:
.Sp
.Vb 1
    "$hash\{"$foo/$bar"\}"
.Ve
.Sp
or:
.Sp
.Vb 3
    m/
      bar       # NOT a comment, this slash / terminated m//!
     /x
.Ve
.Sp
do not form legal quoted expressions.   The quoted part ends on the
first \f(CW\*(C`"\*(C' and \f(CW\*(C`/\*(C', and the rest happens to be a syntax error.
Because the slash that terminated \f(CW\*(C`m//\*(C' was followed by a \f(CW\*(C`SPACE\*(C',
the example above is not \f(CW\*(C`m//x\*(C', but rather \f(CW\*(C`m//\*(C' with no \f(CW\*(C`/x\*(C'
modifier.  So the embedded \f(CW\*(C`#\*(C' is interpreted as a literal \f(CW\*(C`#\*(C'.
.Sp
Also no attention is paid to \f(CW\*(C`\\c\\\*(C' (multichar control char syntax) during
this search.  Thus the second \f(CW\*(C`\\\*(C' in \f(CW\*(C`qq/\\c\\/\*(C' is interpreted as a part
of \f(CW\*(C`\\/\*(C', and the following \f(CW\*(C`/\*(C' is not recognized as a delimiter.
Instead, use \f(CW\*(C`\\034\*(C' or \f(CW\*(C`\\x1c\*(C' at the end of quoted constructs.

- Interpolation
Xref "interpolation"
Item "Interpolation"
The next step is interpolation in the text obtained, which is now
delimiter-independent.  There are multiple cases.

> .ie n .IP """<<\*(AqEOF\*(Aq""" 4
.el .IP "\f(CW<<\*(AqEOF\*(Aq" 4
Item "<<EOF"
No interpolation is performed.
Note that the combination \f(CW\*(C`\\\\\*(C' is left intact, since escaped delimiters
are not available for here-docs.
.ie n .IP """m\*(Aq\*(Aq"", the pattern of ""s\*(Aq\*(Aq\*(Aq""" 4
.el .IP "\f(CWm\*(Aq\*(Aq, the pattern of \f(CWs\*(Aq\*(Aq\*(Aq" 4
Item "m, the pattern of s"
No interpolation is performed at this stage.
Any backslashed sequences including \f(CW\*(C`\\\\\*(C' are treated at the stage
to \*(L"parsing regular expressions\*(R".
.ie n .IP "\*(Aq\*(Aq, ""q//"", ""tr\*(Aq\*(Aq\*(Aq"", ""y\*(Aq\*(Aq\*(Aq"", the replacement of ""s\*(Aq\*(Aq\*(Aq""" 4
.el .IP "\f(CW\*(Aq\*(Aq, \f(CWq//, \f(CWtr\*(Aq\*(Aq\*(Aq, \f(CWy\*(Aq\*(Aq\*(Aq, the replacement of \f(CWs\*(Aq\*(Aq\*(Aq" 4
Item ", q//, tr, y, the replacement of s"
The only interpolation is removal of \f(CW\*(C`\\\*(C' from pairs of \f(CW\*(C`\\\\\*(C'.
Therefore \f(CW"-" in \f(CW\*(C`tr\*(Aq\*(Aq\*(Aq\*(C' and \f(CW\*(C`y\*(Aq\*(Aq\*(Aq\*(C' is treated literally
as a hyphen and no character range is available.
\f(CW\*(C`\\1\*(C' in the replacement of \f(CW\*(C`s\*(Aq\*(Aq\*(Aq\*(C' does not work as \f(CW$1.
.ie n .IP """tr///"", ""y///""" 4
.el .IP "\f(CWtr///, \f(CWy///" 4
Item "tr///, y///"
No variable interpolation occurs.  String modifying combinations for
case and quoting such as \f(CW\*(C`\\Q\*(C', \f(CW\*(C`\\U\*(C', and \f(CW\*(C`\\E\*(C' are not recognized.
The other escape sequences such as \f(CW\*(C`\\200\*(C' and \f(CW\*(C`\\t\*(C' and backslashed
characters such as \f(CW\*(C`\\\\\*(C' and \f(CW\*(C`\\-\*(C' are converted to appropriate literals.
The character \f(CW"-" is treated specially and therefore \f(CW\*(C`\\-\*(C' is treated
as a literal \f(CW"-".
.ie n .IP """"", ""\`\`"", ""qq//"", ""qx//"", ""<file*glob>"", ""<<""EOF""""" 4
.el .IP "\f(CW``'', \f(CW\`\`, \f(CWqq//, \f(CWqx//, \f(CW<file*glob>, \f(CW<<``EOF''" 4
Item """"", , qq//, qx//, <file*glob>, <<""EOF"""
\f(CW\*(C`\\Q\*(C', \f(CW\*(C`\\U\*(C', \f(CW\*(C`\\u\*(C', \f(CW\*(C`\\L\*(C', \f(CW\*(C`\\l\*(C', \f(CW\*(C`\\F\*(C' (possibly paired with \f(CW\*(C`\\E\*(C') are
converted to corresponding Perl constructs.  Thus, \f(CW"$foo\\Qbaz$bar"
is converted to \f(CW\*(C`$foo\ .\ (quotemeta("baz"\ .\ $bar))\*(C' internally.
The other escape sequences such as \f(CW\*(C`\\200\*(C' and \f(CW\*(C`\\t\*(C' and backslashed
characters such as \f(CW\*(C`\\\\\*(C' and \f(CW\*(C`\\-\*(C' are replaced with appropriate
expansions.
.Sp
Let it be stressed that *whatever falls between \f(CI\*(C`\\Q\*(C'\fI and \f(CI\*(C`\\E\*(C'\fI*
is interpolated in the usual way.  Something like \f(CW"\\Q\\\\E" has
no \f(CW\*(C`\\E\*(C' inside.  Instead, it has \f(CW\*(C`\\Q\*(C', \f(CW\*(C`\\\\\*(C', and \f(CW\*(C`E\*(C', so the
result is the same as for \f(CW"\\\\\\\\E".  As a general rule, backslashes
between \f(CW\*(C`\\Q\*(C' and \f(CW\*(C`\\E\*(C' may lead to counterintuitive results.  So,
\f(CW"\\Q\\t\\E" is converted to \f(CW\*(C`quotemeta("\\t")\*(C', which is the same
as \f(CW"\\\\\\t" (since \s-1TAB\s0 is not alphanumeric).  Note also that:
.Sp
.Vb 2
  $str = \*(Aq\\t\*(Aq;
  return "\\Q$str";
.Ve
.Sp
may be closer to the conjectural *intention* of the writer of \f(CW"\\Q\\t\\E".
.Sp
Interpolated scalars and arrays are converted internally to the \f(CW\*(C`join\*(C' and
\f(CW"." catenation operations.  Thus, \f(CW"$foo\ XXX\ \*(Aq@arr\*(Aq" becomes:
.Sp
.Vb 1
  $foo . " XXX \*(Aq" . (join $", @arr) . "\*(Aq";
.Ve
.Sp
All operations above are performed simultaneously, left to right.
.Sp
Because the result of \f(CW"\\Q\ \f(CISTRING\f(CW\ \\E" has all metacharacters
quoted, there is no way to insert a literal \f(CW\*(C`$\*(C' or \f(CW\*(C`@\*(C' inside a
\f(CW\*(C`\\Q\\E\*(C' pair.  If protected by \f(CW\*(C`\\\*(C', \f(CW\*(C`$\*(C' will be quoted to become
\f(CW"\\\\\\$"; if not, it is interpreted as the start of an interpolated
scalar.
.Sp
Note also that the interpolation code needs to make a decision on
where the interpolated scalar ends.  For instance, whether
\f(CW"a\ $x\ ->\ \{c\}" really means:
.Sp
.Vb 1
  "a " . $x . " -> \{c\}";
.Ve
.Sp
or:
.Sp
.Vb 1
  "a " . $x -> \{c\};
.Ve
.Sp
Most of the time, the longest possible text that does not include
spaces between components and which contains matching braces or
brackets.  because the outcome may be determined by voting based
on heuristic estimators, the result is not strictly predictable.
Fortunately, it's usually correct for ambiguous cases.
.ie n .IP "the replacement of ""s///""" 4
.el .IP "the replacement of \f(CWs///" 4
Item "the replacement of s///"
Processing of \f(CW\*(C`\\Q\*(C', \f(CW\*(C`\\U\*(C', \f(CW\*(C`\\u\*(C', \f(CW\*(C`\\L\*(C', \f(CW\*(C`\\l\*(C', \f(CW\*(C`\\F\*(C' and interpolation
happens as with \f(CW\*(C`qq//\*(C' constructs.
.Sp
It is at this step that \f(CW\*(C`\\1\*(C' is begrudgingly converted to \f(CW$1 in
the replacement text of \f(CW\*(C`s///\*(C', in order to correct the incorrigible
*sed* hackers who haven't picked up the saner idiom yet.  A warning
is emitted if the \f(CW\*(C`use\ warnings\*(C' pragma or the **-w** command-line flag
(that is, the \f(CW$^W variable) was set.
.ie n .IP """RE"" in ""m?RE?"", ""/RE/"", ""m/RE/"", ""s/RE/foo/""," 4
.el .IP "\f(CWRE in \f(CWm?RE?, \f(CW/RE/, \f(CWm/RE/, \f(CWs/RE/foo/," 4
Item "RE in m?RE?, /RE/, m/RE/, s/RE/foo/,"
Processing of \f(CW\*(C`\\Q\*(C', \f(CW\*(C`\\U\*(C', \f(CW\*(C`\\u\*(C', \f(CW\*(C`\\L\*(C', \f(CW\*(C`\\l\*(C', \f(CW\*(C`\\F\*(C', \f(CW\*(C`\\E\*(C',
and interpolation happens (almost) as with \f(CW\*(C`qq//\*(C' constructs.
.Sp
Processing of \f(CW\*(C`\\N\{...\}\*(C' is also done here, and compiled into an intermediate
form for the regex compiler.  (This is because, as mentioned below, the regex
compilation may be done at execution time, and \f(CW\*(C`\\N\{...\}\*(C' is a compile-time
construct.)
.Sp
However any other combinations of \f(CW\*(C`\\\*(C' followed by a character
are not substituted but only skipped, in order to parse them
as regular expressions at the following step.
As \f(CW\*(C`\\c\*(C' is skipped at this step, \f(CW\*(C`@\*(C' of \f(CW\*(C`\\c@\*(C' in \s-1RE\s0 is possibly
treated as an array symbol (for example \f(CW@foo),
even though the same text in \f(CW\*(C`qq//\*(C' gives interpolation of \f(CW\*(C`\\c@\*(C'.
.Sp
Code blocks such as \f(CW\*(C`(?\{BLOCK\})\*(C' are handled by temporarily passing control
back to the perl parser, in a similar way that an interpolated array
subscript expression such as \f(CW"foo$array[1+f("[xyz")]bar" would be.
.Sp
Moreover, inside \f(CW\*(C`(?\{BLOCK\})\*(C', \f(CW\*(C`(?#\ comment\ )\*(C', and
a \f(CW\*(C`#\*(C'-comment in a \f(CW\*(C`/x\*(C'-regular expression, no processing is
performed whatsoever.  This is the first step at which the presence
of the \f(CW\*(C`/x\*(C' modifier is relevant.
.Sp
Interpolation in patterns has several quirks: \f(CW$|, \f(CW$(, \f(CW$), \f(CW\*(C`@+\*(C'
and \f(CW\*(C`@-\*(C' are not interpolated, and constructs \f(CW$var[SOMETHING] are
voted (by several different estimators) to be either an array element
or \f(CW$var followed by an \s-1RE\s0 alternative.  This is where the notation
\f(CW\*(C`$\{arr[$bar]\}\*(C' comes handy: \f(CW\*(C`/$\{arr[0-9]\}/\*(C' is interpreted as
array element \f(CW\*(C`-9\*(C', not as a regular expression from the variable
\f(CW$arr followed by a digit, which would be the interpretation of
\f(CW\*(C`/$arr[0-9]/\*(C'.  Since voting among different estimators may occur,
the result is not predictable.
.Sp
The lack of processing of \f(CW\*(C`\\\\\*(C' creates specific restrictions on
the post-processed text.  If the delimiter is \f(CW\*(C`/\*(C', one cannot get
the combination \f(CW\*(C`\\/\*(C' into the result of this step.  \f(CW\*(C`/\*(C' will
finish the regular expression, \f(CW\*(C`\\/\*(C' will be stripped to \f(CW\*(C`/\*(C' on
the previous step, and \f(CW\*(C`\\\\/\*(C' will be left as is.  Because \f(CW\*(C`/\*(C' is
equivalent to \f(CW\*(C`\\/\*(C' inside a regular expression, this does not
matter unless the delimiter happens to be character special to the
\s-1RE\s0 engine, such as in \f(CW\*(C`s*foo*bar*\*(C', \f(CW\*(C`m[foo]\*(C', or \f(CW\*(C`m?foo?\*(C'; or an
alphanumeric char, as in:
.Sp
.Vb 1
  m m ^ a \\s* b mmx;
.Ve
.Sp
In the \s-1RE\s0 above, which is intentionally obfuscated for illustration, the
delimiter is \f(CW\*(C`m\*(C', the modifier is \f(CW\*(C`mx\*(C', and after delimiter-removal the
\s-1RE\s0 is the same as for \f(CW\*(C`m/\ ^\ a\ \\s*\ b\ /mx\*(C'.  There's more than one
reason you're encouraged to restrict your delimiters to non-alphanumeric,
non-whitespace choices.



> .Sp
This step is the last one for all constructs except regular expressions,
which are processed further.



- parsing regular expressions
Xref "regexp, parse"
Item "parsing regular expressions"
Previous steps were performed during the compilation of Perl code,
but this one happens at run time, although it may be optimized to
be calculated at compile time if appropriate.  After preprocessing
described above, and possibly after evaluation if concatenation,
joining, casing translation, or metaquoting are involved, the
resulting *string* is passed to the \s-1RE\s0 engine for compilation.
.Sp
Whatever happens in the \s-1RE\s0 engine might be better discussed in perlre,
but for the sake of continuity, we shall do so here.
.Sp
This is another step where the presence of the \f(CW\*(C`/x\*(C' modifier is
relevant.  The \s-1RE\s0 engine scans the string from left to right and
converts it into a finite automaton.
.Sp
Backslashed characters are either replaced with corresponding
literal strings (as with \f(CW\*(C`\\\{\*(C'), or else they generate special nodes
in the finite automaton (as with \f(CW\*(C`\\b\*(C').  Characters special to the
\s-1RE\s0 engine (such as \f(CW\*(C`|\*(C') generate corresponding nodes or groups of
nodes.  \f(CW\*(C`(?#...)\*(C' comments are ignored.  All the rest is either
converted to literal strings to match, or else is ignored (as is
whitespace and \f(CW\*(C`#\*(C'-style comments if \f(CW\*(C`/x\*(C' is present).
.Sp
Parsing of the bracketed character class construct, \f(CW\*(C`[...]\*(C', is
rather different than the rule used for the rest of the pattern.
The terminator of this construct is found using the same rules as
for finding the terminator of a \f(CW\*(C`\{\}\*(C'-delimited construct, the only
exception being that \f(CW\*(C`]\*(C' immediately following \f(CW\*(C`[\*(C' is treated as
though preceded by a backslash.
.Sp
The terminator of runtime \f(CW\*(C`(?\{...\})\*(C' is found by temporarily switching
control to the perl parser, which should stop at the point where the
logically balancing terminating \f(CW\*(C`\}\*(C' is found.
.Sp
It is possible to inspect both the string given to \s-1RE\s0 engine and the
resulting finite automaton.  See the arguments \f(CW\*(C`debug\*(C'/\f(CW\*(C`debugcolor\*(C'
in the \f(CW\*(C`use\ re\*(C' pragma, as well as Perl's **-Dr** command-line
switch documented in \*(L"Command Switches\*(R" in perlrun.

- Optimization of regular expressions
Xref "regexp, optimization"
Item "Optimization of regular expressions"
This step is listed for completeness only.  Since it does not change
semantics, details of this step are not documented and are subject
to change without notice.  This step is performed over the finite
automaton that was generated during the previous pass.
.Sp
It is at this stage that \f(CW\*(C`split()\*(C' silently optimizes \f(CW\*(C`/^/\*(C' to
mean \f(CW\*(C`/^/m\*(C'.

### I/O Operators

Xref "operator, i o operator, io io while filehandle <> <<>> @ARGV"
Subsection "I/O Operators"
There are several I/O operators you should know about.

A string enclosed by backticks (grave accents) first undergoes
double-quote interpolation.  It is then interpreted as an external
command, and the output of that command is the value of the
backtick string, like in a shell.  In scalar context, a single string
consisting of all output is returned.  In list context, a list of
values is returned, one per line of output.  (You can set \f(CW$/ to use
a different line terminator.)  The command is executed each time the
pseudo-literal is evaluated.  The status value of the command is
returned in \f(CW$? (see perlvar for the interpretation of \f(CW$?).
Unlike in **csh**, no translation is done on the return data\*(--newlines
remain newlines.  Unlike in any of the shells, single quotes do not
hide variable names in the command from interpretation.  To pass a
literal dollar-sign through to the shell you need to hide it with a
backslash.  The generalized form of backticks is \f(CW\*(C`qx//\*(C', or you can
call the \*(L"readpipe\*(R" in perlfunc function.  (Because
backticks always undergo shell expansion as well, see perlsec for
security concerns.)
Xref "qx ` `` backtick glob"

In scalar context, evaluating a filehandle in angle brackets yields
the next line from that file (the newline, if any, included), or
\f(CW\*(C`undef\*(C' at end-of-file or on error.  When \f(CW$/ is set to \f(CW\*(C`undef\*(C'
(sometimes known as file-slurp mode) and the file is empty, it
returns \f(CW\*(Aq\*(Aq the first time, followed by \f(CW\*(C`undef\*(C' subsequently.

Ordinarily you must assign the returned value to a variable, but
there is one situation where an automatic assignment happens.  If
and only if the input symbol is the only thing inside the conditional
of a \f(CW\*(C`while\*(C' statement (even if disguised as a \f(CW\*(C`for(;;)\*(C' loop),
the value is automatically assigned to the global variable \f(CW$_,
destroying whatever was there previously.  (This may seem like an
odd thing to you, but you'll use the construct in almost every Perl
script you write.)  The \f(CW$_ variable is not implicitly localized.
You'll have to put a \f(CW\*(C`local\ $_;\*(C' before the loop if you want that
to happen.  Furthermore, if the input symbol or an explicit assignment
of the input symbol to a scalar is used as a \f(CW\*(C`while\*(C'/\f(CW\*(C`for\*(C' condition,
then the condition actually tests for definedness of the expression's
value, not for its regular truth value.

Thus the following lines are equivalent:

.Vb 7
    while (defined($_ = <STDIN>)) \{ print; \}
    while ($_ = <STDIN>) \{ print; \}
    while (<STDIN>) \{ print; \}
    for (;<STDIN>;) \{ print; \}
    print while defined($_ = <STDIN>);
    print while ($_ = <STDIN>);
    print while <STDIN>;
.Ve

This also behaves similarly, but assigns to a lexical variable
instead of to \f(CW$_:

.Vb 1
    while (my $line = <STDIN>) \{ print $line \}
.Ve

In these loop constructs, the assigned value (whether assignment
is automatic or explicit) is then tested to see whether it is
defined.  The defined test avoids problems where the line has a string
value that would be treated as false by Perl; for example a "" or
a \f(CW"0" with no trailing newline.  If you really mean for such values
to terminate the loop, they should be tested for explicitly:

.Vb 2
    while (($_ = <STDIN>) ne \*(Aq0\*(Aq) \{ ... \}
    while (<STDIN>) \{ last unless $_; ... \}
.Ve

In other boolean contexts, \f(CW\*(C`<\f(CIFILEHANDLE\f(CW>\*(C' without an
explicit \f(CW\*(C`defined\*(C' test or comparison elicits a warning if the
\f(CW\*(C`use\ warnings\*(C' pragma or the **-w**
command-line switch (the \f(CW$^W variable) is in effect.

The filehandles \s-1STDIN, STDOUT,\s0 and \s-1STDERR\s0 are predefined.  (The
filehandles \f(CW\*(C`stdin\*(C', \f(CW\*(C`stdout\*(C', and \f(CW\*(C`stderr\*(C' will also work except
in packages, where they would be interpreted as local identifiers
rather than global.)  Additional filehandles may be created with
the \f(CW\*(C`open()\*(C' function, amongst others.  See perlopentut and
\*(L"open\*(R" in perlfunc for details on this.
Xref "stdin stdout sterr"

If a \f(CW\*(C`<\f(CIFILEHANDLE\f(CW>\*(C' is used in a context that is looking for
a list, a list comprising all input lines is returned, one line per
list element.  It's easy to grow to a rather large data space this
way, so use with care.

\f(CW\*(C`<\f(CIFILEHANDLE\f(CW>\*(C'  may also be spelled \f(CW\*(C`readline(*\f(CIFILEHANDLE\f(CW)\*(C'.
See \*(L"readline\*(R" in perlfunc.

The null filehandle \f(CW\*(C`<>\*(C' (sometimes called the diamond operator) is
special: it can be used to emulate the
behavior of **sed** and **awk**, and any other Unix filter program
that takes a list of filenames, doing the same to each line
of input from all of them.  Input from \f(CW\*(C`<>\*(C' comes either from
standard input, or from each file listed on the command line.  Here's
how it works: the first time \f(CW\*(C`<>\*(C' is evaluated, the \f(CW@ARGV array is
checked, and if it is empty, \f(CW$ARGV[0] is set to \f(CW"-", which when opened
gives you standard input.  The \f(CW@ARGV array is then processed as a list
of filenames.  The loop

.Vb 3
    while (<>) \{
        ...                     # code for each line
    \}
.Ve

is equivalent to the following Perl-like pseudo code:

.Vb 7
    unshift(@ARGV, \*(Aq-\*(Aq) unless @ARGV;
    while ($ARGV = shift) \{
        open(ARGV, $ARGV);
        while (<ARGV>) \{
            ...         # code for each line
        \}
    \}
.Ve

except that it isn't so cumbersome to say, and will actually work.
It really does shift the \f(CW@ARGV array and put the current filename
into the \f(CW$ARGV variable.  It also uses filehandle *\s-1ARGV\s0*
internally.  \f(CW\*(C`<>\*(C' is just a synonym for \f(CW\*(C`<ARGV>\*(C', which
is magical.  (The pseudo code above doesn't work because it treats
\f(CW\*(C`<ARGV>\*(C' as non-magical.)

Since the null filehandle uses the two argument form of \*(L"open\*(R" in perlfunc
it interprets special characters, so if you have a script like this:

.Vb 3
    while (<>) \{
        print;
    \}
.Ve

and call it with \f(CW\*(C`perl\ dangerous.pl\ \*(Aqrm\ -rfv\ *|\*(Aq\*(C', it actually opens a
pipe, executes the \f(CW\*(C`rm\*(C' command and reads \f(CW\*(C`rm\*(C''s output from that pipe.
If you want all items in \f(CW@ARGV to be interpreted as file names, you
can use the module \f(CW\*(C`ARGV::readonly\*(C' from \s-1CPAN,\s0 or use the double
diamond bracket:

.Vb 3
    while (<<>>) \{
        print;
    \}
.Ve

Using double angle brackets inside of a while causes the open to use the
three argument form (with the second argument being \f(CW\*(C`<\*(C'), so all
arguments in \f(CW\*(C`ARGV\*(C' are treated as literal filenames (including \f(CW"-").
(Note that for convenience, if you use \f(CW\*(C`<<>>\*(C' and if \f(CW@ARGV is
empty, it will still read from the standard input.)

You can modify \f(CW@ARGV before the first \f(CW\*(C`<>\*(C' as long as the array ends up
containing the list of filenames you really want.  Line numbers (\f(CW$.)
continue as though the input were one big happy file.  See the example
in \*(L"eof\*(R" in perlfunc for how to reset line numbers on each file.

If you want to set \f(CW@ARGV to your own list of files, go right ahead.
This sets \f(CW@ARGV to all plain text files if no \f(CW@ARGV was given:

.Vb 1
    @ARGV = grep \{ -f && -T \} glob(\*(Aq*\*(Aq) unless @ARGV;
.Ve

You can even set them to pipe commands.  For example, this automatically
filters compressed arguments through **gzip**:

.Vb 1
    @ARGV = map \{ /\\.(gz|Z)$/ ? "gzip -dc < $_ |" : $_ \} @ARGV;
.Ve

If you want to pass switches into your script, you can use one of the
\f(CW\*(C`Getopts\*(C' modules or put a loop on the front like this:

.Vb 7
    while ($_ = $ARGV[0], /^-/) \{
        shift;
        last if /^--$/;
        if (/^-D(.*)/) \{ $debug = $1 \}
        if (/^-v/)     \{ $verbose++  \}
        # ...           # other switches
    \}

    while (<>) \{
        # ...           # code for each line
    \}
.Ve

The \f(CW\*(C`<>\*(C' symbol will return \f(CW\*(C`undef\*(C' for end-of-file only once.
If you call it again after this, it will assume you are processing another
\f(CW@ARGV list, and if you haven't set \f(CW@ARGV, will read input from \s-1STDIN.\s0

If what the angle brackets contain is a simple scalar variable (for example,
\f(CW$foo), then that variable contains the name of the
filehandle to input from, or its typeglob, or a reference to the
same.  For example:

.Vb 2
    $fh = \\*STDIN;
    $line = <$fh>;
.Ve

If what's within the angle brackets is neither a filehandle nor a simple
scalar variable containing a filehandle name, typeglob, or typeglob
reference, it is interpreted as a filename pattern to be globbed, and
either a list of filenames or the next filename in the list is returned,
depending on context.  This distinction is determined on syntactic
grounds alone.  That means \f(CW\*(C`<$x>\*(C' is always a \f(CW\*(C`readline()\*(C' from
an indirect handle, but \f(CW\*(C`<$hash\{key\}>\*(C' is always a \f(CW\*(C`glob()\*(C'.
That's because \f(CW$x is a simple scalar variable, but \f(CW$hash\{key\} is
not\*(--it's a hash element.  Even \f(CW\*(C`<$x >\*(C' (note the extra space)
is treated as \f(CW\*(C`glob("$x ")\*(C', not \f(CW\*(C`readline($x)\*(C'.

One level of double-quote interpretation is done first, but you can't
say \f(CW\*(C`<$foo>\*(C' because that's an indirect filehandle as explained
in the previous paragraph.  (In older versions of Perl, programmers
would insert curly brackets to force interpretation as a filename glob:
\f(CW\*(C`<$\{foo\}>\*(C'.  These days, it's considered cleaner to call the
internal function directly as \f(CW\*(C`glob($foo)\*(C', which is probably the right
way to have done it in the first place.)  For example:

.Vb 3
    while (<*.c>) \{
        chmod 0644, $_;
    \}
.Ve

is roughly equivalent to:

.Vb 5
    open(FOO, "echo *.c | tr -s \*(Aq \\t\\r\\f\*(Aq \*(Aq\\\\012\\\\012\\\\012\\\\012\*(Aq|");
    while (<FOO>) \{
        chomp;
        chmod 0644, $_;
    \}
.Ve

except that the globbing is actually done internally using the standard
\f(CW\*(C`File::Glob\*(C' extension.  Of course, the shortest way to do the above is:

.Vb 1
    chmod 0644, <*.c>;
.Ve

A (file)glob evaluates its (embedded) argument only when it is
starting a new list.  All values must be read before it will start
over.  In list context, this isn't important because you automatically
get them all anyway.  However, in scalar context the operator returns
the next value each time it's called, or \f(CW\*(C`undef\*(C' when the list has
run out.  As with filehandle reads, an automatic \f(CW\*(C`defined\*(C' is
generated when the glob occurs in the test part of a \f(CW\*(C`while\*(C',
because legal glob returns (for example,
a file called *0*) would otherwise
terminate the loop.  Again, \f(CW\*(C`undef\*(C' is returned only once.  So if
you're expecting a single value from a glob, it is much better to
say

.Vb 1
    ($file) = <blurch*>;
.Ve

than

.Vb 1
    $file = <blurch*>;
.Ve

because the latter will alternate between returning a filename and
returning false.

If you're trying to do variable interpolation, it's definitely better
to use the \f(CW\*(C`glob()\*(C' function, because the older notation can cause people
to become confused with the indirect filehandle notation.

.Vb 2
    @files = glob("$dir/*.[ch]");
    @files = glob($files[$i]);
.Ve

If an angle-bracket-based globbing expression is used as the condition of
a \f(CW\*(C`while\*(C' or \f(CW\*(C`for\*(C' loop, then it will be implicitly assigned to \f(CW$_.
If either a globbing expression or an explicit assignment of a globbing
expression to a scalar is used as a \f(CW\*(C`while\*(C'/\f(CW\*(C`for\*(C' condition, then
the condition actually tests for definedness of the expression's value,
not for its regular truth value.

### Constant Folding

Xref "constant folding folding"
Subsection "Constant Folding"
Like C, Perl does a certain amount of expression evaluation at
compile time whenever it determines that all arguments to an
operator are static and have no side effects.  In particular, string
concatenation happens at compile time between literals that don't do
variable substitution.  Backslash interpolation also happens at
compile time.  You can say

.Vb 3
      \*(AqNow is the time for all\*(Aq
    . "\\n"
    .  \*(Aqgood men to come to.\*(Aq
.Ve

and this all reduces to one string internally.  Likewise, if
you say

.Vb 3
    foreach $file (@filenames) \{
        if (-s $file > 5 + 100 * 2**16) \{  \}
    \}
.Ve

the compiler precomputes the number which that expression
represents so that the interpreter won't have to.

### No-ops

Xref "no-op nop"
Subsection "No-ops"
Perl doesn't officially have a no-op operator, but the bare constants
\f(CW0 and \f(CW1 are special-cased not to produce a warning in void
context, so you can for example safely do

.Vb 1
    1 while foo();
.Ve

### Bitwise String Operators

Xref "operator, bitwise, string &. |. ^. ~."
Subsection "Bitwise String Operators"
Bitstrings of any size may be manipulated by the bitwise operators
(\f(CW\*(C`~ | & ^\*(C').

If the operands to a binary bitwise op are strings of different
sizes, **|** and **^** ops act as though the shorter operand had
additional zero bits on the right, while the **&** op acts as though
the longer operand were truncated to the length of the shorter.
The granularity for such extension or truncation is one or more
bytes.

.Vb 5
    # ASCII-based examples
    print "j p \\n" ^ " a h";            # prints "JAPH\\n"
    print "JA" | "  ph\\n";              # prints "japh\\n"
    print "japh\\nJunk" & \*(Aq_\|_\|_\|_\|_\*(Aq;       # prints "JAPH\\n";
    print \*(Aqp N$\*(Aq ^ " E<H\\n";            # prints "Perl\\n";
.Ve

If you are intending to manipulate bitstrings, be certain that
you're supplying bitstrings: If an operand is a number, that will imply
a **numeric** bitwise operation.  You may explicitly show which type of
operation you intend by using \f(CW"" or \f(CW\*(C`0+\*(C', as in the examples below.

.Vb 4
    $foo =  150  |  105;        # yields 255  (0x96 | 0x69 is 0xFF)
    $foo = \*(Aq150\*(Aq |  105;        # yields 255
    $foo =  150  | \*(Aq105\*(Aq;       # yields 255
    $foo = \*(Aq150\*(Aq | \*(Aq105\*(Aq;       # yields string \*(Aq155\*(Aq (under ASCII)

    $baz = 0+$foo & 0+$bar;     # both ops explicitly numeric
    $biz = "$foo" ^ "$bar";     # both ops explicitly stringy
.Ve

This somewhat unpredictable behavior can be avoided with the \*(L"bitwise\*(R"
feature, new in Perl 5.22.  You can enable it via \f(CW\*(C`use\ feature\ \*(Aqbitwise\*(Aq\*(C' or \f(CW\*(C`use v5.28\*(C'.  Before Perl 5.28, it used to emit a warning
in the \f(CW"experimental::bitwise" category.  Under this feature, the four
standard bitwise operators (\f(CW\*(C`~ | & ^\*(C') are always numeric.  Adding a dot
after each operator (\f(CW\*(C`~. |. &. ^.\*(C') forces it to treat its operands as
strings:

.Vb 9
    use feature "bitwise";
    $foo =  150  |  105;        # yields 255  (0x96 | 0x69 is 0xFF)
    $foo = \*(Aq150\*(Aq |  105;        # yields 255
    $foo =  150  | \*(Aq105\*(Aq;       # yields 255
    $foo = \*(Aq150\*(Aq | \*(Aq105\*(Aq;       # yields 255
    $foo =  150  |. 105;        # yields string \*(Aq155\*(Aq
    $foo = \*(Aq150\*(Aq |. 105;        # yields string \*(Aq155\*(Aq
    $foo =  150  |.\*(Aq105\*(Aq;       # yields string \*(Aq155\*(Aq
    $foo = \*(Aq150\*(Aq |.\*(Aq105\*(Aq;       # yields string \*(Aq155\*(Aq

    $baz = $foo &  $bar;        # both operands numeric
    $biz = $foo ^. $bar;        # both operands stringy
.Ve

The assignment variants of these operators (\f(CW\*(C`&= |= ^= &.= |.= ^.=\*(C')
behave likewise under the feature.

It is a fatal error if an operand contains a character whose ordinal
value is above 0xFF, and hence not expressible except in \s-1UTF-8.\s0  The
operation is performed on a non-UTF-8 copy for other operands encoded in
\s-1UTF-8.\s0  See \*(L"Byte and Character Semantics\*(R" in perlunicode.

See \*(L"vec\*(R" in perlfunc for information on how to manipulate individual bits
in a bit vector.

### Integer Arithmetic

Xref "integer"
Subsection "Integer Arithmetic"
By default, Perl assumes that it must do most of its arithmetic in
floating point.  But by saying

.Vb 1
    use integer;
.Ve

you may tell the compiler to use integer operations
(see integer for a detailed explanation) from here to the end of
the enclosing \s-1BLOCK.\s0  An inner \s-1BLOCK\s0 may countermand this by saying

.Vb 1
    no integer;
.Ve

which lasts until the end of that \s-1BLOCK.\s0  Note that this doesn't
mean everything is an integer, merely that Perl will use integer
operations for arithmetic, comparison, and bitwise operators.  For
example, even under \f(CW\*(C`use\ integer\*(C', if you take the \f(CWsqrt(2), you'll
still get \f(CW1.4142135623731 or so.

Used on numbers, the bitwise operators (\f(CW\*(C`&\*(C' \f(CW\*(C`|\*(C' \f(CW\*(C`^\*(C' \f(CW\*(C`~\*(C' \f(CW\*(C`<<\*(C'
\f(CW\*(C`>>\*(C') always produce integral results.  (But see also
\*(L"Bitwise String Operators\*(R".)  However, \f(CW\*(C`use\ integer\*(C' still has meaning for
them.  By default, their results are interpreted as unsigned integers, but
if \f(CW\*(C`use\ integer\*(C' is in effect, their results are interpreted
as signed integers.  For example, \f(CW\*(C`~0\*(C' usually evaluates to a large
integral value.  However, \f(CW\*(C`use\ integer;\ ~0\*(C' is \f(CW\*(C`-1\*(C' on two's-complement
machines.

### Floating-point Arithmetic

Subsection "Floating-point Arithmetic"

Xref "floating-point floating point float real"

While \f(CW\*(C`use\ integer\*(C' provides integer-only arithmetic, there is no
analogous mechanism to provide automatic rounding or truncation to a
certain number of decimal places.  For rounding to a certain number
of digits, \f(CW\*(C`sprintf()\*(C' or \f(CW\*(C`printf()\*(C' is usually the easiest route.
See perlfaq4.

Floating-point numbers are only approximations to what a mathematician
would call real numbers.  There are infinitely more reals than floats,
so some corners must be cut.  For example:

.Vb 2
    printf "%.20g\\n", 123456789123456789;
    #        produces 123456789123456784
.Ve

Testing for exact floating-point equality or inequality is not a
good idea.  Here's a (relatively expensive) work-around to compare
whether two floating-point numbers are equal to a particular number of
decimal places.  See Knuth, volume \s-1II,\s0 for a more robust treatment of
this topic.

.Vb 7
    sub fp_equal \{
        my ($X, $Y, $POINTS) = @_;
        my ($tX, $tY);
        $tX = sprintf("%.$\{POINTS\}g", $X);
        $tY = sprintf("%.$\{POINTS\}g", $Y);
        return $tX eq $tY;
    \}
.Ve

The \s-1POSIX\s0 module (part of the standard perl distribution) implements
\f(CW\*(C`ceil()\*(C', \f(CW\*(C`floor()\*(C', and other mathematical and trigonometric functions.
The \f(CW\*(C`Math::Complex\*(C' module (part of the standard perl distribution)
defines mathematical functions that work on both the reals and the
imaginary numbers.  \f(CW\*(C`Math::Complex\*(C' is not as efficient as \s-1POSIX,\s0 but
\s-1POSIX\s0 can't work with complex numbers.

Rounding in financial applications can have serious implications, and
the rounding method used should be specified precisely.  In these
cases, it probably pays not to trust whichever system rounding is
being used by Perl, but to instead implement the rounding function you
need yourself.

### Bigger Numbers

Xref "number, arbitrary precision"
Subsection "Bigger Numbers"
The standard \f(CW\*(C`Math::BigInt\*(C', \f(CW\*(C`Math::BigRat\*(C', and
\f(CW\*(C`Math::BigFloat\*(C' modules,
along with the \f(CW\*(C`bignum\*(C', \f(CW\*(C`bigint\*(C', and \f(CW\*(C`bigrat\*(C' pragmas, provide
variable-precision arithmetic and overloaded operators, although
they're currently pretty slow.  At the cost of some space and
considerable speed, they avoid the normal pitfalls associated with
limited-precision representations.

.Vb 5
        use 5.010;
        use bigint;  # easy interface to Math::BigInt
        $x = 123456789123456789;
        say $x * $x;
    +15241578780673678515622620750190521
.Ve

Or with rationals:

.Vb 8
        use 5.010;
        use bigrat;
        $x = 3/22;
        $y = 4/6;
        say "x/y is ", $x/$y;
        say "x*y is ", $x*$y;
        x/y is 9/44
        x*y is 1/11
.Ve

Several modules let you calculate with unlimited or fixed precision
(bound only by memory and \s-1CPU\s0 time).  There
are also some non-standard modules that
provide faster implementations via external C libraries.

Here is a short, but incomplete summary:

.Vb 10
  Math::String           treat string sequences like numbers
  Math::FixedPrecision   calculate with a fixed precision
  Math::Currency         for currency calculations
  Bit::Vector            manipulate bit vectors fast (uses C)
  Math::BigIntFast       Bit::Vector wrapper for big numbers
  Math::Pari             provides access to the Pari C library
  Math::Cephes           uses the external Cephes C library (no
                         big numbers)
  Math::Cephes::Fraction fractions via the Cephes library
  Math::GMP              another one using an external C library
  Math::GMPz             an alternative interface to libgmp\*(Aqs big ints
  Math::GMPq             an interface to libgmp\*(Aqs fraction numbers
  Math::GMPf             an interface to libgmp\*(Aqs floating point numbers
.Ve

Choose wisely.
