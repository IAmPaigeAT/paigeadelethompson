+++
description = "Perls source code, and extensions that want maximum portability, should use the above functions instead of those defined in s-1ANSI Cs0s stdio.h.  The perl headers (in particular *(Lperlio.h*(R) will f(CW*(C`#define*(C them to the I/O mechanis..."
operating_system_version = "15.3"
manpage_format = "troff"
date = "2022-02-19"
manpage_name = "perlapio"
author = "None Specified"
manpage_section = "1"
detected_package_version = "5.34.1"
title = "perlapio(1)"
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLAPIO 1"
PERLAPIO 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlapio - perl's IO abstraction interface.

## SYNOPSIS

Header "SYNOPSIS"
.Vb 2
  #define PERLIO_NOT_STDIO 0    /* For co-existence with stdio only */
  #include <perlio.h>           /* Usually via #include <perl.h> */

  PerlIO *PerlIO_stdin(void);
  PerlIO *PerlIO_stdout(void);
  PerlIO *PerlIO_stderr(void);

  PerlIO *PerlIO_open(const char *path,const char *mode);
  PerlIO *PerlIO_fdopen(int fd, const char *mode);
  PerlIO *PerlIO_reopen(const char *path, /* deprecated */
          const char *mode, PerlIO *old);
  int     PerlIO_close(PerlIO *f);

  int     PerlIO_stdoutf(const char *fmt,...)
  int     PerlIO_puts(PerlIO *f,const char *string);
  int     PerlIO_putc(PerlIO *f,int ch);
  SSize_t PerlIO_write(PerlIO *f,const void *buf,size_t numbytes);
  int     PerlIO_printf(PerlIO *f, const char *fmt,...);
  int     PerlIO_vprintf(PerlIO *f, const char *fmt, va_list args);
  int     PerlIO_flush(PerlIO *f);

  int     PerlIO_eof(PerlIO *f);
  int     PerlIO_error(PerlIO *f);
  void    PerlIO_clearerr(PerlIO *f);

  int     PerlIO_getc(PerlIO *d);
  int     PerlIO_ungetc(PerlIO *f,int ch);
  SSize_t PerlIO_read(PerlIO *f, void *buf, size_t numbytes);

  int     PerlIO_fileno(PerlIO *f);

  void    PerlIO_setlinebuf(PerlIO *f);

  Off_t   PerlIO_tell(PerlIO *f);
  int     PerlIO_seek(PerlIO *f, Off_t offset, int whence);
  void    PerlIO_rewind(PerlIO *f);

  int     PerlIO_getpos(PerlIO *f, SV *save);    /* prototype changed */
  int     PerlIO_setpos(PerlIO *f, SV *saved);   /* prototype changed */

  int     PerlIO_fast_gets(PerlIO *f);
  int     PerlIO_has_cntptr(PerlIO *f);
  SSize_t PerlIO_get_cnt(PerlIO *f);
  char   *PerlIO_get_ptr(PerlIO *f);
  void    PerlIO_set_ptrcnt(PerlIO *f, char *ptr, SSize_t count);

  int     PerlIO_canset_cnt(PerlIO *f);              /* deprecated */
  void    PerlIO_set_cnt(PerlIO *f, int count);      /* deprecated */

  int     PerlIO_has_base(PerlIO *f);
  char   *PerlIO_get_base(PerlIO *f);
  SSize_t PerlIO_get_bufsiz(PerlIO *f);

  PerlIO *PerlIO_importFILE(FILE *stdio, const char *mode);
  FILE   *PerlIO_exportFILE(PerlIO *f, const char *mode);
  FILE   *PerlIO_findFILE(PerlIO *f);
  void    PerlIO_releaseFILE(PerlIO *f,FILE *stdio);

  int     PerlIO_apply_layers(PerlIO *f, const char *mode,
                                                    const char *layers);
  int     PerlIO_binmode(PerlIO *f, int ptype, int imode,
                                                    const char *layers);
  void    PerlIO_debug(const char *fmt,...);
.Ve

## DESCRIPTION

Header "DESCRIPTION"
Perl's source code, and extensions that want maximum portability,
should use the above functions instead of those defined in \s-1ANSI C\s0's
*stdio.h*.  The perl headers (in particular \*(L"perlio.h\*(R") will
\f(CW\*(C`#define\*(C' them to the I/O mechanism selected at Configure time.

The functions are modeled on those in *stdio.h*, but parameter order
has been \*(L"tidied up a little\*(R".

\f(CW\*(C`PerlIO *\*(C' takes the place of \s-1FILE\s0 *. Like \s-1FILE\s0 * it should be
treated as opaque (it is probably safe to assume it is a pointer to
something).

There are currently two implementations:

- 1. \s-1USE_STDIO\s0
Item "1. USE_STDIO"
All above are #define'd to stdio functions or are trivial wrapper
functions which call stdio. In this case *only* PerlIO * is a \s-1FILE\s0 *.
This has been the default implementation since the abstraction was
introduced in perl5.003_02.

- 2. \s-1USE_PERLIO\s0
Item "2. USE_PERLIO"
Introduced just after perl5.7.0, this is a re-implementation of the
above abstraction which allows perl more control over how \s-1IO\s0 is done
as it decouples \s-1IO\s0 from the way the operating system and C library
choose to do things. For \s-1USE_PERLIO\s0 PerlIO * has an extra layer of
indirection - it is a pointer-to-a-pointer.  This allows the PerlIO *
to remain with a known value while swapping the implementation around
underneath *at run time*. In this case all the above are true (but
very simple) functions which call the underlying implementation.
.Sp
This is the only implementation for which \f(CW\*(C`PerlIO_apply_layers()\*(C'
does anything \*(L"interesting\*(R".
.Sp
The \s-1USE_PERLIO\s0 implementation is described in perliol.

Because \*(L"perlio.h\*(R" is a thin layer (for efficiency) the semantics of
these functions are somewhat dependent on the underlying implementation.
Where these variations are understood they are noted below.

Unless otherwise noted, functions return 0 on success, or a negative
value (usually \f(CW\*(C`EOF\*(C' which is usually -1) and set \f(CW\*(C`errno\*(C' on error.

- \fB\fBPerlIO_stdin()\fB, \fB\fBPerlIO_stdout()\fB, \fB\fBPerlIO_stderr()\fB
Item "PerlIO_stdin(), PerlIO_stdout(), PerlIO_stderr()"
Use these rather than \f(CW\*(C`stdin\*(C', \f(CW\*(C`stdout\*(C', \f(CW\*(C`stderr\*(C'. They are written
to look like \*(L"function calls\*(R" rather than variables because this makes
it easier to *make them* function calls if platform cannot export data
to loaded modules, or if (say) different \*(L"threads\*(R" might have different
values.

- \fBPerlIO_open(path, mode), \fBPerlIO_fdopen(fd,mode)
Item "PerlIO_open(path, mode), PerlIO_fdopen(fd,mode)"
These correspond to **fopen()**/**fdopen()** and the arguments are the same.
Return \f(CW\*(C`NULL\*(C' and set \f(CW\*(C`errno\*(C' if there is an error.  There may be an
implementation limit on the number of open handles, which may be lower
than the limit on the number of open files - \f(CW\*(C`errno\*(C' may not be set
when \f(CW\*(C`NULL\*(C' is returned if this limit is exceeded.

- \fBPerlIO_reopen(path,mode,f)
Item "PerlIO_reopen(path,mode,f)"
While this currently exists in both implementations, perl itself
does not use it. *As perl does not use it, it is not well tested.*
.Sp
Perl prefers to \f(CW\*(C`dup\*(C' the new low-level descriptor to the descriptor
used by the existing PerlIO. This may become the behaviour of this
function in the future.

- \fBPerlIO_printf(f,fmt,...), \fBPerlIO_vprintf(f,fmt,a)
Item "PerlIO_printf(f,fmt,...), PerlIO_vprintf(f,fmt,a)"
These are **fprintf()**/**vfprintf()** equivalents.

- \fBPerlIO_stdoutf(fmt,...)
Item "PerlIO_stdoutf(fmt,...)"
This is **printf()** equivalent. printf is #defined to this function,
so it is (currently) legal to use \f(CW\*(C`printf(fmt,...)\*(C' in perl sources.

- \fBPerlIO_read(f,buf,count), \fBPerlIO_write(f,buf,count)
Item "PerlIO_read(f,buf,count), PerlIO_write(f,buf,count)"
These correspond functionally to **fread()** and **fwrite()** but the
arguments and return values are different.  The **PerlIO_read()** and
**PerlIO_write()** signatures have been modeled on the more sane low level
**read()** and **write()** functions instead: The \*(L"file\*(R" argument is passed
first, there is only one \*(L"count\*(R", and the return value can distinguish
between error and \f(CW\*(C`EOF\*(C'.
.Sp
Returns a byte count if successful (which may be zero or
positive), returns negative value and sets \f(CW\*(C`errno\*(C' on error.
Depending on implementation \f(CW\*(C`errno\*(C' may be \f(CW\*(C`EINTR\*(C' if operation was
interrupted by a signal.

- \fBPerlIO_close(f)
Item "PerlIO_close(f)"
Depending on implementation \f(CW\*(C`errno\*(C' may be \f(CW\*(C`EINTR\*(C' if operation was
interrupted by a signal.

- \fBPerlIO_puts(f,s), \fBPerlIO_putc(f,c)
Item "PerlIO_puts(f,s), PerlIO_putc(f,c)"
These correspond to **fputs()** and **fputc()**.
Note that arguments have been revised to have \*(L"file\*(R" first.

- \fBPerlIO_ungetc(f,c)
Item "PerlIO_ungetc(f,c)"
This corresponds to **ungetc()**.  Note that arguments have been revised
to have \*(L"file\*(R" first.  Arranges that next read operation will return
the byte **c**.  Despite the implied \*(L"character\*(R" in the name only
values in the range 0..0xFF are defined. Returns the byte **c** on
success or -1 (\f(CW\*(C`EOF\*(C') on error.  The number of bytes that can be
\*(L"pushed back\*(R" may vary, only 1 character is certain, and then only if
it is the last character that was read from the handle.

- \fBPerlIO_getc(f)
Item "PerlIO_getc(f)"
This corresponds to **getc()**.
Despite the c in the name only byte range 0..0xFF is supported.
Returns the character read or -1 (\f(CW\*(C`EOF\*(C') on error.

- \fBPerlIO_eof(f)
Item "PerlIO_eof(f)"
This corresponds to **feof()**.  Returns a true/false indication of
whether the handle is at end of file.  For terminal devices this may
or may not be \*(L"sticky\*(R" depending on the implementation.  The flag is
cleared by **PerlIO_seek()**, or **PerlIO_rewind()**.

- \fBPerlIO_error(f)
Item "PerlIO_error(f)"
This corresponds to **ferror()**.  Returns a true/false indication of
whether there has been an \s-1IO\s0 error on the handle.

- \fBPerlIO_fileno(f)
Item "PerlIO_fileno(f)"
This corresponds to **fileno()**, note that on some platforms, the meaning
of \*(L"fileno\*(R" may not match Unix. Returns -1 if the handle has no open
descriptor associated with it.

- \fBPerlIO_clearerr(f)
Item "PerlIO_clearerr(f)"
This corresponds to **clearerr()**, i.e., clears 'error' and (usually)
'eof' flags for the \*(L"stream\*(R". Does not return a value.

- \fBPerlIO_flush(f)
Item "PerlIO_flush(f)"
This corresponds to **fflush()**.  Sends any buffered write data to the
underlying file.  If called with \f(CW\*(C`NULL\*(C' this may flush all open
streams (or core dump with some \s-1USE_STDIO\s0 implementations).  Calling
on a handle open for read only, or on which last operation was a read
of some kind may lead to undefined behaviour on some \s-1USE_STDIO\s0
implementations.  The \s-1USE_PERLIO\s0 (layers) implementation tries to
behave better: it flushes all open streams when passed \f(CW\*(C`NULL\*(C', and
attempts to retain data on read streams either in the buffer or by
seeking the handle to the current logical position.

- \fBPerlIO_seek(f,offset,whence)
Item "PerlIO_seek(f,offset,whence)"
This corresponds to **fseek()**.  Sends buffered write data to the
underlying file, or discards any buffered read data, then positions
the file descriptor as specified by **offset** and **whence** (sic).
This is the correct thing to do when switching between read and write
on the same handle (see issues with **PerlIO_flush()** above).  Offset is
of type \f(CW\*(C`Off_t\*(C' which is a perl Configure value which may not be same
as stdio's \f(CW\*(C`off_t\*(C'.

- \fBPerlIO_tell(f)
Item "PerlIO_tell(f)"
This corresponds to **ftell()**.  Returns the current file position, or
(Off_t) -1 on error.  May just return value system \*(L"knows\*(R" without
making a system call or checking the underlying file descriptor (so
use on shared file descriptors is not safe without a
**PerlIO_seek()**). Return value is of type \f(CW\*(C`Off_t\*(C' which is a perl
Configure value which may not be same as stdio's \f(CW\*(C`off_t\*(C'.

- \fBPerlIO_getpos(f,p), \fBPerlIO_setpos(f,p)
Item "PerlIO_getpos(f,p), PerlIO_setpos(f,p)"
These correspond (loosely) to **fgetpos()** and **fsetpos()**. Rather than
stdio's Fpos_t they expect a \*(L"Perl Scalar Value\*(R" to be passed. What is
stored there should be considered opaque. The layout of the data may
vary from handle to handle.  When not using stdio or if platform does
not have the stdio calls then they are implemented in terms of
**PerlIO_tell()** and **PerlIO_seek()**.

- \fBPerlIO_rewind(f)
Item "PerlIO_rewind(f)"
This corresponds to **rewind()**. It is usually defined as being
.Sp
.Vb 2
    PerlIO_seek(f,(Off_t)0L, SEEK_SET);
    PerlIO_clearerr(f);
.Ve

- \fB\fBPerlIO_tmpfile()\fB
Item "PerlIO_tmpfile()"
This corresponds to **tmpfile()**, i.e., returns an anonymous PerlIO or
\s-1NULL\s0 on error.  The system will attempt to automatically delete the
file when closed.  On Unix the file is usually \f(CW\*(C`unlink\*(C'-ed just after
it is created so it does not matter how it gets closed. On other
systems the file may only be deleted if closed via **PerlIO_close()**
and/or the program exits via \f(CW\*(C`exit\*(C'.  Depending on the implementation
there may be \*(L"race conditions\*(R" which allow other processes access to
the file, though in general it will be safer in this regard than
ad. hoc. schemes.

- \fBPerlIO_setlinebuf(f)
Item "PerlIO_setlinebuf(f)"
This corresponds to **setlinebuf()**.  Does not return a value. What
constitutes a \*(L"line\*(R" is implementation dependent but usually means
that writing \*(L"\\n\*(R" flushes the buffer.  What happens with things like
\*(L"this\\nthat\*(R" is uncertain.  (Perl core uses it *only* when \*(L"dumping\*(R";
it has nothing to do with $| auto-flush.)

### Co-existence with stdio

Subsection "Co-existence with stdio"
There is outline support for co-existence of PerlIO with stdio.
Obviously if PerlIO is implemented in terms of stdio there is no
problem. However in other cases then mechanisms must exist to create a
\s-1FILE\s0 * which can be passed to library code which is going to use stdio
calls.

The first step is to add this line:

.Vb 1
   #define PERLIO_NOT_STDIO 0
.Ve

*before* including any perl header files. (This will probably become
the default at some point).  That prevents \*(L"perlio.h\*(R" from attempting
to #define stdio functions onto PerlIO functions.

\s-1XS\s0 code is probably better using \*(L"typemap\*(R" if it expects \s-1FILE\s0 *
arguments.  The standard typemap will be adjusted to comprehend any
changes in this area.

- \fBPerlIO_importFILE(f,mode)
Item "PerlIO_importFILE(f,mode)"
Used to get a PerlIO * from a \s-1FILE\s0 *.
.Sp
The mode argument should be a string as would be passed to
fopen/PerlIO_open.  If it is \s-1NULL\s0 then - for legacy support - the code
will (depending upon the platform and the implementation) either
attempt to empirically determine the mode in which *f* is open, or
use \*(L"r+\*(R" to indicate a read/write stream.
.Sp
Once called the \s-1FILE\s0 * should *\s-1ONLY\s0* be closed by calling
\f(CW\*(C`PerlIO_close()\*(C' on the returned PerlIO *.
.Sp
The PerlIO is set to textmode. Use PerlIO_binmode if this is
not the desired mode.
.Sp
This is **not** the reverse of **PerlIO_exportFILE()**.

- \fBPerlIO_exportFILE(f,mode)
Item "PerlIO_exportFILE(f,mode)"
Given a PerlIO * create a 'native' \s-1FILE\s0 * suitable for passing to code
expecting to be compiled and linked with \s-1ANSI C\s0 *stdio.h*.  The mode
argument should be a string as would be passed to fopen/PerlIO_open.
If it is \s-1NULL\s0 then - for legacy support - the \s-1FILE\s0 * is opened in same
mode as the PerlIO *.
.Sp
The fact that such a \s-1FILE\s0 * has been 'exported' is recorded, (normally
by pushing a new :stdio \*(L"layer\*(R" onto the PerlIO *), which may affect
future PerlIO operations on the original PerlIO *.  You should not
call \f(CW\*(C`fclose()\*(C' on the file unless you call \f(CW\*(C`PerlIO_releaseFILE()\*(C'
to disassociate it from the PerlIO *.  (Do not use **PerlIO_importFILE()**
for doing the disassociation.)
.Sp
Calling this function repeatedly will create a \s-1FILE\s0 * on each call
(and will push an :stdio layer each time as well).

- \fBPerlIO_releaseFILE(p,f)
Item "PerlIO_releaseFILE(p,f)"
Calling PerlIO_releaseFILE informs PerlIO that all use of \s-1FILE\s0 * is
complete. It is removed from the list of 'exported' \s-1FILE\s0 *s, and the
associated PerlIO * should revert to its original behaviour.
.Sp
Use this to disassociate a file from a PerlIO * that was associated
using **PerlIO_exportFILE()**.

- \fBPerlIO_findFILE(f)
Item "PerlIO_findFILE(f)"
Returns a native \s-1FILE\s0 * used by a stdio layer. If there is none, it
will create one with PerlIO_exportFILE. In either case the \s-1FILE\s0 *
should be considered as belonging to PerlIO subsystem and should
only be closed by calling \f(CW\*(C`PerlIO_close()\*(C'.
.ie n .SS """Fast gets"" Functions"
.el .SS "``Fast gets'' Functions"
Subsection "Fast gets Functions"
In addition to standard-like \s-1API\s0 defined so far above there is an
\*(L"implementation\*(R" interface which allows perl to get at internals of
PerlIO.  The following calls correspond to the various FILE_xxx macros
determined by Configure - or their equivalent in other
implementations. This section is really of interest to only those
concerned with detailed perl-core behaviour, implementing a PerlIO
mapping or writing code which can make use of the \*(L"read ahead\*(R" that
has been done by the \s-1IO\s0 system in the same way perl does. Note that
any code that uses these interfaces must be prepared to do things the
traditional way if a handle does not support them.

- \fBPerlIO_fast_gets(f)
Item "PerlIO_fast_gets(f)"
Returns true if implementation has all the interfaces required to
allow perl's \f(CW\*(C`sv_gets\*(C' to \*(L"bypass\*(R" normal \s-1IO\s0 mechanism.  This can
vary from handle to handle.
.Sp
.Vb 3
  PerlIO_fast_gets(f) = PerlIO_has_cntptr(f) && \\
                        PerlIO_canset_cnt(f) && \\
                        \*(AqCan set pointer into buffer\*(Aq
.Ve

- \fBPerlIO_has_cntptr(f)
Item "PerlIO_has_cntptr(f)"
Implementation can return pointer to current position in the \*(L"buffer\*(R"
and a count of bytes available in the buffer.  Do not use this - use
PerlIO_fast_gets.

- \fBPerlIO_get_cnt(f)
Item "PerlIO_get_cnt(f)"
Return count of readable bytes in the buffer. Zero or negative return
means no more bytes available.

- \fBPerlIO_get_ptr(f)
Item "PerlIO_get_ptr(f)"
Return pointer to next readable byte in buffer, accessing via the
pointer (dereferencing) is only safe if **PerlIO_get_cnt()** has returned
a positive value.  Only positive offsets up to value returned by
**PerlIO_get_cnt()** are allowed.

- \fBPerlIO_set_ptrcnt(f,p,c)
Item "PerlIO_set_ptrcnt(f,p,c)"
Set pointer into buffer, and a count of bytes still in the
buffer. Should be used only to set pointer to within range implied by
previous calls to \f(CW\*(C`PerlIO_get_ptr\*(C' and \f(CW\*(C`PerlIO_get_cnt\*(C'. The two
values *must* be consistent with each other (implementation may only
use one or the other or may require both).

- \fBPerlIO_canset_cnt(f)
Item "PerlIO_canset_cnt(f)"
Implementation can adjust its idea of number of bytes in the buffer.
Do not use this - use PerlIO_fast_gets.

- \fBPerlIO_set_cnt(f,c)
Item "PerlIO_set_cnt(f,c)"
Obscure - set count of bytes in the buffer. Deprecated.  Only usable
if **PerlIO_canset_cnt()** returns true.  Currently used in only doio.c to
force count less than -1 to -1.  Perhaps should be PerlIO_set_empty or
similar.  This call may actually do nothing if \*(L"count\*(R" is deduced from
pointer and a \*(L"limit\*(R".  Do not use this - use **PerlIO_set_ptrcnt()**.

- \fBPerlIO_has_base(f)
Item "PerlIO_has_base(f)"
Returns true if implementation has a buffer, and can return pointer
to whole buffer and its size. Used by perl for **-T** / **-B** tests.
Other uses would be very obscure...

- \fBPerlIO_get_base(f)
Item "PerlIO_get_base(f)"
Return *start* of buffer. Access only positive offsets in the buffer
up to the value returned by **PerlIO_get_bufsiz()**.

- \fBPerlIO_get_bufsiz(f)
Item "PerlIO_get_bufsiz(f)"
Return the *total number of bytes* in the buffer, this is neither the
number that can be read, nor the amount of memory allocated to the
buffer. Rather it is what the operating system and/or implementation
happened to \f(CW\*(C`read()\*(C' (or whatever) last time \s-1IO\s0 was requested.

### Other Functions

Subsection "Other Functions"

- PerlIO_apply_layers(f,mode,layers)
Item "PerlIO_apply_layers(f,mode,layers)"
The new interface to the \s-1USE_PERLIO\s0 implementation. The layers \*(L":crlf\*(R"
and \*(L":raw\*(R" are only ones allowed for other implementations and those
are silently ignored. (As of perl5.8 \*(L":raw\*(R" is deprecated.)  Use
**PerlIO_binmode()** below for the portable case.

- PerlIO_binmode(f,ptype,imode,layers)
Item "PerlIO_binmode(f,ptype,imode,layers)"
The hook used by perl's \f(CW\*(C`binmode\*(C' operator.
**ptype** is perl's character for the kind of \s-1IO:\s0

> 
- '<' read
Item "'<' read"
0

- '>' write
Item "'>' write"

- '+' read/write
Item "'+' read/write"



> .PD
.Sp
**imode** is \f(CW\*(C`O_BINARY\*(C' or \f(CW\*(C`O_TEXT\*(C'.
.Sp
**layers** is a string of layers to apply, only \*(L":crlf\*(R" makes sense in
the non \s-1USE_PERLIO\s0 case. (As of perl5.8 \*(L":raw\*(R" is deprecated in favour
of passing \s-1NULL.\s0)
.Sp
Portable cases are:
.Sp
.Vb 3
    PerlIO_binmode(f,ptype,O_BINARY,NULL);
and
    PerlIO_binmode(f,ptype,O_TEXT,":crlf");
.Ve
.Sp
On Unix these calls probably have no effect whatsoever.  Elsewhere
they alter \*(L"\\n\*(R" to \s-1CR,LF\s0 translation and possibly cause a special text
\*(L"end of file\*(R" indicator to be written or honoured on read. The effect
of making the call after doing any \s-1IO\s0 to the handle depends on the
implementation. (It may be ignored, affect any data which is already
buffered as well, or only apply to subsequent data.)



- PerlIO_debug(fmt,...)
Item "PerlIO_debug(fmt,...)"
PerlIO_debug is a **printf()**-like function which can be used for
debugging.  No return value. Its main use is inside PerlIO where using
real printf, **warn()** etc. would recursively call PerlIO and be a
problem.
.Sp
PerlIO_debug writes to the file named by \f(CW$ENV\{'\s-1PERLIO_DEBUG\s0'\} or defaults
to stderr if the environment variable is not defined. Typical
use might be
.Sp
.Vb 2
  Bourne shells (sh, ksh, bash, zsh, ash, ...):
   PERLIO_DEBUG=/tmp/perliodebug.log ./perl -Di somescript some args

  Csh/Tcsh:
   setenv PERLIO_DEBUG /tmp/perliodebug.log
   ./perl -Di somescript some args

  If you have the "env" utility:
   env PERLIO_DEBUG=/tmp/perliodebug.log ./perl -Di somescript args

  Win32:
   set PERLIO_DEBUG=perliodebug.log
   perl -Di somescript some args
.Ve
.Sp
On a Perl built without \f(CW\*(C`-DDEBUGGING\*(C', or when the \f(CW\*(C`-Di\*(C' command-line switch
is not specified, or under taint, **PerlIO_debug()** is a no-op.
