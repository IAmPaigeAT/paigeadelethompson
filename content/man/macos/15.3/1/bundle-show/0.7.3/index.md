+++
description = "Without the [GEM] option, show will print a list of the names and versions of all gems that are required by your [Gemfile(5)][Gemfile(5)], sorted by name. Calling show with [GEM] will list the exact location of that gem on your machine. --paths Lis..."
detected_package_version = "0.7.3"
title = "bundle-show(1)"
manpage_format = "troff"
author = "None Specified"
operating_system = "macos"
operating_system_version = "15.3"
manpage_name = "bundle-show"
date = "Sun Feb 16 04:48:21 2025"
manpage_section = "1"
+++

.
"BUNDLE-SHOW" "1" "November 2018" "" ""
.

## NAME

**bundle-show** - Shows all the gems in your bundle, or the path to a gem
.

## SYNOPSIS

**bundle show** [GEM] [--paths]
.

## DESCRIPTION

Without the [GEM] option, **show** will print a list of the names and versions of all gems that are required by your [**Gemfile(5)**][Gemfile(5)], sorted by name\.
.
.P
Calling show with [GEM] will list the exact location of that gem on your machine\.
.

## OPTIONS

.

**--paths**
List the paths of all gems that are required by your [**Gemfile(5)**][Gemfile(5)], sorted by gem name\.

