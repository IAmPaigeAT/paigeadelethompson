+++
operating_system_version = "15.3"
description = "The Latent Semantic Mapping framework is a language independent, Unicode based technology that builds maps and uses them to classify texts into one of a number of categories. lsm is a tool to create, manipulate, test, and dump Latent Semantic Mapp..."
operating_system = "macos"
date = "2024-05-10"
detected_package_version = "1.0"
manpage_name = "lsm"
author = "None Specified"
title = "lsm(1)"
manpage_section = "1"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "LSM 1"
LSM 1 "2024-05-10" "1.0" "Latent Semantic Mapping"

## NAME

lsm - Latent Semantic Mapping tool

## SYNOPSIS

Header "SYNOPSIS"
lsm *lsm_command* [*command_options*] *map_file* [*input_files*]

## DESCRIPTION

Header "DESCRIPTION"
The Latent Semantic Mapping framework is a language independent,
Unicode based technology that builds *maps* and uses them to classify
*texts* into one of a number of *categories*.

**lsm** is a tool to create, manipulate, test, and dump Latent Semantic
Mapping maps. It is designed to provide access to a large subset of
the functionality of the Latent Semantic Mapping \s-1API,\s0 mainly for rapid
prototyping and diagnostic purposes, but possibly also for simple
shell script based applications of Latent Semantic Mapping.

## COMMANDS

Header "COMMANDS"
**lsm** provides a variety of commands (*lsm_command* in the
Synopsis), each of which often has a wealth of options (see the
Command Options below). Command names may be abbreviated to
unambiguous prefixes.

- \fBlsm create \fImap_file \fIinput_files
Item "lsm create map_file input_files"
Create a new \s-1LSM\s0 map from the specified *input_files*.

- \fBlsm update \fImap_file \fIinput_files
Item "lsm update map_file input_files"
Add the specified *input_files* to an existing \s-1LSM\s0 map.

- \fBlsm evaluate \fImap_file \fIinput_files
Item "lsm evaluate map_file input_files"
Classify the specified *input_files* into the categories of the \s-1LSM\s0 map.

- \fBlsm cluster \fB[--k-means=N | --agglomerative=N] [--apply]
Item "lsm cluster [--k-means=N | --agglomerative=N] [--apply]"
Compute clusters for the map, and, if the **--apply** option is specified,
transform the map accordingly. Multiple levels of clustering may be applied for
faster performance on large maps, e.g.
.Sp
.Vb 1
   lsm cluster --k-means=100 --each --agglomerative=100 --agglomerative=1000 my.map
.Ve
.Sp
first computes 100 clusters using (fast) k-means clustering, computes 100 subclusters
for each first stage cluster using agglomerative clustering, and finally reduces those
10000 clusters to 1000 using agglomerative clustering.

- \fBlsm dump \fImap_file [\fIinput_files]
Item "lsm dump map_file [input_files]"
Without *input_files*, dumps all words in the map with their
counts. With *input_files*, dump, for each file, the words that
appear in the map, their counts in the map, and their relative
frequencies in the input file.

- \fBlsm info \fImap_file
Item "lsm info map_file"
Bypass the Latent Semantic Mapping framework to extract and print
information about the file and perform a number of consistency checks
on it. **(\s-1NOT IMPLEMENTED YET\s0)**

## COMMAND OPTIONS

Header "COMMAND OPTIONS"
This section describes the *command_options* that are available for
the **lsm** commands. Not all commands support all of these options;
each option is only supported for commands where it makes sense.
However, when a command has one of these options you can count on the
same meaning for the option as in other commands.

- \fB--append-categories
Item "--append-categories"
Directs the **update** command to put the data into new categories appended
after the existing ones, instead of adding the data to the existing categories.

- \fB--categories \fIcount
Item "--categories count"
Directs the **evaluate** command to only list the top *count* categories.

- \fB--category-delimiter \fIdelimiter
Item "--category-delimiter delimiter"
Specify the delimiter to be used to between categories in the
*input_files* passed to the **create** and **update** commands.

> 
- \fBgroup
Item "group"
Categories are separated by a `;' argument.

- \fBfile
Item "file"
Each *input_file* represents a separate category. This is the default
if the **--category-delimiter** option is not given.

- \fBline
Item "line"
Each line represents a separate category.

- \fIstring
Item "string"
Categories are separated by the specified *string*.



> 


- \fB--clobber
Item "--clobber"
When creating a map, overwrite an existing file at the path, even if it's not an \s-1LSM\s0 map.
By default, **create** will only overwrite an existing file if it's believed to be an \s-1LSM\s0
map, which guards against frequent operator errors such as:
.Sp
.Vb 1
   lsm create /usr/include/*.h
.Ve

- \fB--dimensions \fIdim
Item "--dimensions dim"
Direct the **create** and **update** commands to use the given number of
dimensions for computing the map (Defaults to the number of
categories). This option is useful to manage the size and
computational overhead of maps with large number of categories.

- \fB--discard-counts
Item "--discard-counts"
Direct the **create** and **update** commands to omit the raw word / token
counts when writing the map. This results in a map that is more compact,
but cannot be updated any further.

- \fB--hash
Item "--hash"
Direct the **create** and **update** commands to write the map in a
format that is not human readable with default file manipulation tools
like **cat** or **hexdump**. This is useful in applications such as junk
mail filtering, where input data may contain naughty words and where
the contents of the map may tip off spammers what words to avoid.

- \fB--help
Item "--help"
List an overview of the options available for a command. Available for
all commands.

- \fB--html
Item "--html"
Strip \s-1HTML\s0 codes from the *input_files*. Useful for mail and web
input. Available for the **create**, **update**, **evaluate**, and
**dump** commands.

- \fB--junk-mail
Item "--junk-mail"
When parsing the input files, apply heuristics to counteract common
methods used by spammers to disguise incriminating words such as:
.Sp
.Vb 3
   Zer0 1nt3rest l0ans     Substituting letters with digits
   W E A L T H             Adding spaces between letters
   m.o.r.t.g.a.g.e         Adding punctuation between letters
.Ve
.Sp
Available for the **create**, **update**, **evaluate**, and
**dump** commands.

- \fB--pairs
Item "--pairs"
If specified with the **create** command when building the map, store
counts for pairs of words as well as the words themselves. This can
increase accuracy for certain classes of problems, but will generate
unreasonably large maps unless the vocabulary is fairly limited.

- \fB--stop-words \fIstop_word_file
Item "--stop-words stop_word_file"
If specified with the **create** command, *stop_word_file* is parsed
and all words found are excluded from texts evaluated against the
map. This is useful for excluding frequent, semantically meaningless
words.

- \fB--sweep-cutoff \fIthreshold
Item "--sweep-cutoff threshold"
0

- \fB--sweep-frequency \fIdays
Item "--sweep-frequency days"
.PD
Available for the **create** and **update** commands. Every specified
number of *days* (by default 7), scan the map and remove from it any
entries that have been in the map for at least 2 previous scans and
whose total counts are smaller than *threshold*.  *threshold*
defaults to 0, so by default the map is not scanned.

- \fB--text-delimiter \fIdelimiter
Item "--text-delimiter delimiter"
Specify the delimiter to be used to between texts in the
*input_files* passed to the **create**, **update**, **evaluate**, and
**dump** commands.

> 
- \fBfile
Item "file"
Each *input_file* represents a separate text. This is the default
if the **--text-delimiter** option is not given.

- \fBline
Item "line"
Each line represents a separate text.

- \fIstring
Item "string"
Texts are separated by the specified *string*.



> 


- \fB--triplets
Item "--triplets"
If specified with the **create** command when building the map, store
counts for triplets and pairs of words as well as the words
themselves. This can increase accuracy for certain classes of
problems, but will generate unreasonably large maps unless the
vocabulary is fairly limited.

- \fB--weight \fIweight
Item "--weight weight"
Scale counts of input words for the **create** and **update** commands
by the specified *weight*, which may be a positive or negative
floating point number.

- \fB--words
Item "--words"
Directs the **evaluate** or **cluster** commands to apply to words, instead of categories.

- \fB--words=\fIcount
Item "--words=count"
Directs the **evaluate** command to list the top *count* words, instead of categories.

## EXAMPLES

Header "EXAMPLES"
.ie n .IP """lsm evaluate --html --junk-mail ~/Library/Mail/V2/MailData/LSMMap2 msg*.txt""" 4
.el .IP "\f(CWlsm evaluate --html --junk-mail ~/Library/Mail/V2/MailData/LSMMap2 msg*.txt" 4
Item "lsm evaluate --html --junk-mail ~/Library/Mail/V2/MailData/LSMMap2 msg*.txt"
Simulate the **Mail.app** junk mail filter by evaluating the specified
files (assumed to each hold the raw text of one mail message) against
the user's junk mail map.
.ie n .IP """lsm dump ~/Library/Mail/V2/MailData/LSMMap2""" 4
.el .IP "\f(CWlsm dump ~/Library/Mail/V2/MailData/LSMMap2" 4
Item "lsm dump ~/Library/Mail/V2/MailData/LSMMap2"
Dump the words accumulated in the junk mail map and their counts.
.ie n .IP """lsm create --category-delimiter=group c_vs_h *.c \*(Aq;\*(Aq *.h""" 4
.el .IP "\f(CWlsm create --category-delimiter=group c_vs_h *.c \*(Aq;\*(Aq *.h" 4
Item "lsm create --category-delimiter=group c_vs_h *.c ; *.h"
Create an \s-1LSM\s0 map trained to distinguish C header files from C source
files.
.ie n .IP """lsm update --weight 2.0 --cat=group c_vs_h \*(Aq;\*(Aq ../xy/*.h""" 4
.el .IP "\f(CWlsm update --weight 2.0 --cat=group c_vs_h \*(Aq;\*(Aq ../xy/*.h" 4
Item "lsm update --weight 2.0 --cat=group c_vs_h ; ../xy/*.h"
Add some additional header files with an increased weight to the training.
.ie n .IP """lsm create --help""" 4
.el .IP "\f(CWlsm create --help" 4
Item "lsm create --help"
List the options available for the **lsm create** command.
