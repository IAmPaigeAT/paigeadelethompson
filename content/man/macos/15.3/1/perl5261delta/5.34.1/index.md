+++
operating_system_version = "15.3"
title = "perl5261delta(1)"
operating_system = "macos"
detected_package_version = "5.34.1"
manpage_name = "perl5261delta"
date = "2022-02-19"
manpage_format = "troff"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_section = "1"
description = "This document describes differences between the 5.26.0 release and the 5.26.1 release. If you are upgrading from an earlier release such as 5.24.0, first read perl5260delta, which describes differences between 5.24.0 and 5.26.0. Compiling certain ..."
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5261DELTA 1"
PERL5261DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5261delta - what is new for perl v5.26.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.26.0 release and the 5.26.1
release.

If you are upgrading from an earlier release such as 5.24.0, first read
perl5260delta, which describes differences between 5.24.0 and 5.26.0.

## Security

Header "Security"

### [\s-1CVE-2017-12837\s0] Heap buffer overflow in regular expression compiler

Subsection "[CVE-2017-12837] Heap buffer overflow in regular expression compiler"
Compiling certain regular expression patterns with the case-insensitive
modifier could cause a heap buffer overflow and crash perl.  This has now been
fixed.
[\s-1GH\s0 #16021] <https://github.com/Perl/perl5/issues/16021>

### [\s-1CVE-2017-12883\s0] Buffer over-read in regular expression parser

Subsection "[CVE-2017-12883] Buffer over-read in regular expression parser"
For certain types of syntax error in a regular expression pattern, the error
message could either contain the contents of a random, possibly large, chunk of
memory, or could crash perl.  This has now been fixed.
[\s-1GH\s0 #16025] <https://github.com/Perl/perl5/issues/16025>
.ie n .SS "[\s-1CVE-2017-12814\s0] $ENV\{$key\} stack buffer overflow on Windows"
.el .SS "[\s-1CVE-2017-12814\s0] \f(CW$ENV\{$key\} stack buffer overflow on Windows"
Subsection "[CVE-2017-12814] $ENV\{$key\} stack buffer overflow on Windows"
A possible stack buffer overflow in the \f(CW%ENV code on Windows has been fixed
by removing the buffer completely since it was superfluous anyway.
[\s-1GH\s0 #16051] <https://github.com/Perl/perl5/issues/16051>

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.26.0.  If any exist,
they are bugs, and we request that you submit a report.  See \*(L"Reporting
Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
base has been upgraded from version 2.25 to 2.26.
.Sp
The effects of dotless \f(CW@INC on this module have been limited by the
introduction of a more refined and accurate solution for removing \f(CW\*(Aq.\*(Aq from
\f(CW@INC while reducing the false positives.

- \(bu
charnames has been upgraded from version 1.44 to 1.45.

- \(bu
Module::CoreList has been upgraded from version 5.20170530 to 5.20170922_26.

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- FreeBSD
Item "FreeBSD"

> 0

- \(bu
.PD
Building with **g++** on FreeBSD-11.0 has been fixed.
[\s-1GH\s0 #15984] <https://github.com/Perl/perl5/issues/15984>



> 


- Windows
Item "Windows"

> 0

- \(bu
.PD
Support for compiling perl on Windows using Microsoft Visual Studio 2017
(containing Visual \*(C+ 14.1) has been added.

- \(bu
Building \s-1XS\s0 modules with \s-1GCC 6\s0 in a 64-bit build of Perl failed due to
incorrect mapping of \f(CW\*(C`strtoll\*(C' and \f(CW\*(C`strtoull\*(C'.  This has now been fixed.
[\s-1GH\s0 #16074] <https://github.com/Perl/perl5/issues/16074>
[cpan #121683] <https://rt.cpan.org/Public/Bug/Display.html?id=121683>
[cpan #122353] <https://rt.cpan.org/Public/Bug/Display.html?id=122353>



> 


## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Several built-in functions previously had bugs that could cause them to write
to the internal stack without allocating room for the item being written.  In
rare situations, this could have led to a crash.  These bugs have now been
fixed, and if any similar bugs are introduced in future, they will be detected
automatically in debugging builds.
[\s-1GH\s0 #16076] <https://github.com/Perl/perl5/issues/16076>

- \(bu
Using a symbolic ref with postderef syntax as the key in a hash lookup was
yielding an assertion failure on debugging builds.
[\s-1GH\s0 #16029] <https://github.com/Perl/perl5/issues/16029>

- \(bu
List assignment (\f(CW\*(C`aassign\*(C') could in some rare cases allocate an entry on the
mortal stack and leave the entry uninitialized.
[\s-1GH\s0 #16017] <https://github.com/Perl/perl5/issues/16017>

- \(bu
Attempting to apply an attribute to an \f(CW\*(C`our\*(C' variable where a function of that
name already exists could result in a \s-1NULL\s0 pointer being supplied where an \s-1SV\s0
was expected, crashing perl.
[perl #131597] <https://rt.perl.org/Public/Bug/Display.html?id=131597>

- \(bu
The code that vivifies a typeglob out of a code ref made some false assumptions
that could lead to a crash in cases such as \f(CW$::\{"A"\} = sub \{\}; \\{"A"\}.
This has now been fixed.
[\s-1GH\s0 #15937] <https://github.com/Perl/perl5/issues/15937>

- \(bu
\f(CW\*(C`my_atof2\*(C' no longer reads beyond the terminating \s-1NUL,\s0 which previously
occurred if the decimal point is immediately before the \s-1NUL.\s0
[\s-1GH\s0 #16002] <https://github.com/Perl/perl5/issues/16002>

- \(bu
Occasional \*(L"Malformed \s-1UTF-8\s0 character\*(R" crashes in \f(CW\*(C`s//\*(C' on utf8 strings have
been fixed.
[\s-1GH\s0 #16019] <https://github.com/Perl/perl5/issues/16019>

- \(bu
\f(CW\*(C`perldoc -f s\*(C' now finds \f(CW\*(C`s///\*(C'.
[\s-1GH\s0 #15989] <https://github.com/Perl/perl5/issues/15989>

- \(bu
Some erroneous warnings after utf8 conversion have been fixed.
[\s-1GH\s0 #15958] <https://github.com/Perl/perl5/issues/15958>

- \(bu
The \f(CW\*(C`jmpenv\*(C' frame to catch Perl exceptions is set up lazily, and this used to
be a bit too lazy.  The catcher is now set up earlier, preventing some possible
crashes.
[\s-1GH\s0 #11804] <https://github.com/Perl/perl5/issues/11804>

- \(bu
Spurious \*(L"Assuming \s-1NOT\s0 a \s-1POSIX\s0 class\*(R" warnings have been removed.
[\s-1GH\s0 #16001] <https://github.com/Perl/perl5/issues/16001>

## Acknowledgements

Header "Acknowledgements"
Perl 5.26.1 represents approximately 4 months of development since Perl 5.26.0
and contains approximately 8,900 lines of changes across 85 files from 23
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 990 lines of changes to 38 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.26.1:

Aaron Crane, Andy Dougherty, Aristotle Pagaltzis, Chris 'BinGOs' Williams,
Craig A. Berry, Dagfinn Ilmari Mannsa\*oker, David Mitchell, E. Choroba, Eric
Herman, Father Chrysostomos, Jacques Germishuys, James E Keenan, John \s-1SJ\s0
Anderson, Karl Williamson, Ken Brown, Lukas Mai, Matthew Horsfall, Ricardo
Signes, Sawyer X, Steve Hay, Tony Cook, Yves Orton, Zefram.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database
at <https://rt.perl.org/> .  There may also be information at
<http://www.perl.org/> , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec for details of how to
report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5, you
can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
