+++
detected_package_version = "5.34.1"
manpage_name = "perlunicook"
author = "None Specified"
operating_system_version = "15.3"
keywords = ["header", "see", "also", "these", "manpages", "some", "of", "which", "are", "s-1cpan", "s0", "modules", "perlunicode", "perluniprops", "perlre", "perlrecharclass", "perluniintro", "perlunitut", "perlunifaq", "perlio", "db_file", "dbm_filter", "utf8", "encode", "locale", "unicode", "ucd", "normalize", "gcstring", "linebreak", "collate", "unihan", "casefold", "tussle", "lingua", "ja", "romanize", "japanese", "zh", "pinyin", "ko", "hangul", "the", "module", "includes", "many", "programs", "to", "help", "with", "working", "including", "fully", "or", "partly", "replace", "standard", "utilities", "fitcgrep", "instead", "fiegrep", "fiuniquote", "ficat", "v", "fihexdump", "fiuniwc", "fiwc", "fiunilook", "filook", "fiunifmt", "fifmt", "and", "fiucsort", "fisort", "for", "exploring", "character", "names", "properties", "its", "fiuniprops", "fiunichars", "fiuninames", "it", "supplies", "all", "general", "filters", "that", "do", "unicode-y", "things", "fiunititle", "fiunicaps", "fiuniwide", "fiuninarrow", "fiunisupers", "fiunisubs", "finfd", "finfc", "finfkd", "finfkc", "fiuc", "filc", "fitc", "finally", "published", "page", "numbers", "from", "version", "6", "specific", "annexes", "technical", "reports", "x3", "13", "default", "case", "algorithms", "113", "x4", "2", "pages", "120x122", "mappings", "166x172", "especially", "caseless", "matching", "starting", "on", "170", "4", "item", "s-1uax", "44", "database", "uax", "s-1uts", "18", "regular", "expressions", "uts", "15", "normalization", "forms", "10", "collation", "algorithm", "29", "text", "segmentation", "14", "line", "breaking", "11", "east", "asian", "width", "author", "tom", "christiansen", "tchrist", "perl", "com", "wrote", "this", "occasional", "kibbitzing", "larry", "wall", "jeffrey", "friedl", "in", "background", "copyright", "licence", "x", "2012", "program", "is", "free", "software", "you", "may", "redistribute", "modify", "under", "same", "terms", "as", "itself", "most", "examples", "taken", "current", "edition", "xcamel", "bookx", "4xx", "fiprogramming", "et", "al", "2012-02-13", "by", "oxreilly", "media", "code", "freely", "redistributable", "encouraged", "transplant", "fold", "spindle", "mutilate", "any", "manpage", "however", "please", "inclusion", "into", "your", "own", "without", "encumbrance", "whatsoever", "acknowledgement", "via", "comment", "polite", "but", "not", "required", "revision", "history"]
description = "This manpage contains short recipes demonstrating how to handle common Unicode operations in Perl, plus one complete program at the end. Any undeclared variables in individual recipes are assumed to have a previous appropriate value in them. Unles..."
date = "2022-02-19"
operating_system = "macos"
manpage_format = "troff"
title = "perlunicook(1)"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLUNICOOK 1"
PERLUNICOOK 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlunicook - cookbookish examples of handling Unicode in Perl

## DESCRIPTION

Header "DESCRIPTION"
This manpage contains short recipes demonstrating how to handle common Unicode
operations in Perl, plus one complete program at the end. Any undeclared
variables in individual recipes are assumed to have a previous appropriate
value in them.

## EXAMPLES

Header "EXAMPLES"

### X 0: Standard preamble

Subsection "X 0: Standard preamble"
Unless otherwise notes, all examples below require this standard preamble
to work correctly, with the \f(CW\*(C`#!\*(C' adjusted to work on your system:

.Vb 1
 #!/usr/bin/env perl

 use utf8;      # so literals and identifiers can be in UTF-8
 use v5.12;     # or later to get "unicode_strings" feature
 use strict;    # quote strings, declare variables
 use warnings;  # on by default
 use warnings  qw(FATAL utf8);    # fatalize encoding glitches
 use open      qw(:std :encoding(UTF-8)); # undeclared streams in UTF-8
 use charnames qw(:full :short);  # unneeded in v5.16
.Ve

This *does* make even Unix programmers \f(CW\*(C`binmode\*(C' your binary streams,
or open them with \f(CW\*(C`:raw\*(C', but that's the only way to get at them
portably anyway.

**\s-1WARNING\s0**: \f(CW\*(C`use autodie\*(C' (pre 2.26) and \f(CW\*(C`use open\*(C' do not get along with each
other.

### X 1: Generic Unicode-savvy filter

Subsection "X 1: Generic Unicode-savvy filter"
Always decompose on the way in, then recompose on the way out.

.Vb 1
 use Unicode::Normalize;

 while (<>) \{
     $_ = NFD($_);   # decompose + reorder canonically
     ...
 \} continue \{
     print NFC($_);  # recompose (where possible) + reorder canonically
 \}
.Ve

### X 2: Fine-tuning Unicode warnings

Subsection "X 2: Fine-tuning Unicode warnings"
As of v5.14, Perl distinguishes three subclasses of \s-1UTFX8\s0 warnings.

.Vb 4
 use v5.14;                  # subwarnings unavailable any earlier
 no warnings "nonchar";      # the 66 forbidden non-characters
 no warnings "surrogate";    # UTF-16/CESU-8 nonsense
 no warnings "non_unicode";  # for codepoints over 0x10_FFFF
.Ve

### X 3: Declare source in utf8 for identifiers and literals

Subsection "X 3: Declare source in utf8 for identifiers and literals"
Without the all-critical \f(CW\*(C`use utf8\*(C' declaration, putting \s-1UTFX8\s0 in your
literals and identifiers wonXt work right.  If you used the standard
preamble just given above, this already happened.  If you did, you can
do things like this:

.Vb 1
 use utf8;

 my $measure   = "A\*ongstro\*:m";
 my @Xsoft     = qw( cp852 cp1251 cp1252 );
 my @XXXXXXXXX = qw( XXXX  XXXXX );
 my @X        = qw( koi8-f koi8-u koi8-r );
 my $motto     = "X X X"; # FAMILY, GROWING HEART, DROMEDARY CAMEL
.Ve

If you forget \f(CW\*(C`use utf8\*(C', high bytes will be misunderstood as
separate characters, and nothing will work right.

### X 4: Characters and their numbers

Subsection "X 4: Characters and their numbers"
The \f(CW\*(C`ord\*(C' and \f(CW\*(C`chr\*(C' functions work transparently on all codepoints,
not just on \s-1ASCII\s0 alone X nor in fact, not even just on Unicode alone.

.Vb 3
 # ASCII characters
 ord("A")
 chr(65)

 # characters from the Basic Multilingual Plane
 ord("X")
 chr(0x3A3)

 # beyond the BMP
 ord("X")               # MATHEMATICAL ITALIC SMALL N
 chr(0x1D45B)

 # beyond Unicode! (up to MAXINT)
 ord("\\x\{20_0000\}")
 chr(0x20_0000)
.Ve

### X 5: Unicode literals by character number

Subsection "X 5: Unicode literals by character number"
In an interpolated literal, whether a double-quoted string or a
regex, you may specify a character by its number using the
\f(CW\*(C`\\x\{\f(CIHHHHHH\f(CW\}\*(C' escape.

.Vb 2
 String: "\\x\{3a3\}"
 Regex:  /\\x\{3a3\}/

 String: "\\x\{1d45b\}"
 Regex:  /\\x\{1d45b\}/

 # even non-BMP ranges in regex work fine
 /[\\x\{1D434\}-\\x\{1D467\}]/
.Ve

### X 6: Get character name by number

Subsection "X 6: Get character name by number"
.Vb 2
 use charnames ();
 my $name = charnames::viacode(0x03A3);
.Ve

### X 7: Get character number by name

Subsection "X 7: Get character number by name"
.Vb 2
 use charnames ();
 my $number = charnames::vianame("GREEK CAPITAL LETTER SIGMA");
.Ve

### X 8: Unicode named characters

Subsection "X 8: Unicode named characters"
Use the \f(CW\*(C`\\N\{\f(CIcharname\f(CW\}\*(C' notation to get the character
by that name for use in interpolated literals (double-quoted
strings and regexes).  In v5.16, there is an implicit

.Vb 1
 use charnames qw(:full :short);
.Ve

But prior to v5.16, you must be explicit about which set of charnames you
want.  The \f(CW\*(C`:full\*(C' names are the official Unicode character name, alias, or
sequence, which all share a namespace.

.Vb 1
 use charnames qw(:full :short latin greek);

 "\\N\{MATHEMATICAL ITALIC SMALL N\}"      # :full
 "\\N\{GREEK CAPITAL LETTER SIGMA\}"       # :full
.Ve

Anything else is a Perl-specific convenience abbreviation.  Specify one or
more scripts by names if you want short names that are script-specific.

.Vb 3
 "\\N\{Greek:Sigma\}"                      # :short
 "\\N\{ae\}"                               #  latin
 "\\N\{epsilon\}"                          #  greek
.Ve

The v5.16 release also supports a \f(CW\*(C`:loose\*(C' import for loose matching of
character names, which works just like loose matching of property names:
that is, it disregards case, whitespace, and underscores:

.Vb 1
 "\\N\{euro sign\}"                        # :loose (from v5.16)
.Ve

Starting in v5.32, you can also use

.Vb 1
 qr/\\p\{name=euro sign\}/
.Ve

to get official Unicode named characters in regular expressions.  Loose
matching is always done for these.

### X 9: Unicode named sequences

Subsection "X 9: Unicode named sequences"
These look just like character names but return multiple codepoints.
Notice the \f(CW%vx vector-print functionality in \f(CW\*(C`printf\*(C'.

.Vb 4
 use charnames qw(:full);
 my $seq = "\\N\{LATIN CAPITAL LETTER A WITH MACRON AND GRAVE\}";
 printf "U+%v04X\\n", $seq;
 U+0100.0300
.Ve

### X 10: Custom named characters

Subsection "X 10: Custom named characters"
Use \f(CW\*(C`:alias\*(C' to give your own lexically scoped nicknames to existing
characters, or even to give unnamed private-use characters useful names.

.Vb 4
 use charnames ":full", ":alias" => \{
     ecute => "LATIN SMALL LETTER E WITH ACUTE",
     "APPLE LOGO" => 0xF8FF, # private use character
 \};

 "\\N\{ecute\}"
 "\\N\{APPLE LOGO\}"
.Ve

### X 11: Names of \s-1CJK\s0 codepoints

Subsection "X 11: Names of CJK codepoints"
Sinograms like \s-1XXXX\s0 come back with character names of
\f(CW\*(C`CJK UNIFIED IDEOGRAPH-6771\*(C' and \f(CW\*(C`CJK UNIFIED IDEOGRAPH-4EAC\*(C',
because their XnamesX vary.  The \s-1CPAN\s0 \f(CW\*(C`Unicode::Unihan\*(C' module
has a large database for decoding these (and a whole lot more), provided you
know how to understand its output.

.Vb 8
 # cpan -i Unicode::Unihan
 use Unicode::Unihan;
 my $str = "XX";
 my $unhan = Unicode::Unihan->new;
 for my $lang (qw(Mandarin Cantonese Korean JapaneseOn JapaneseKun)) \{
     printf "CJK $str in %-12s is ", $lang;
     say $unhan->$lang($str);
 \}
.Ve

prints:

.Vb 5
 CJK XX in Mandarin     is DONG1JING1
 CJK XX in Cantonese    is dung1ging1
 CJK XX in Korean       is TONGKYENG
 CJK XX in JapaneseOn   is TOUKYOU KEI KIN
 CJK XX in JapaneseKun  is HIGASHI AZUMAMIYAKO
.Ve

If you have a specific romanization scheme in mind,
use the specific module:

.Vb 5
 # cpan -i Lingua::JA::Romanize::Japanese
 use Lingua::JA::Romanize::Japanese;
 my $k2r = Lingua::JA::Romanize::Japanese->new;
 my $str = "XX";
 say "Japanese for $str is ", $k2r->chars($str);
.Ve

prints

.Vb 1
 Japanese for XX is toukyou
.Ve

### X 12: Explicit encode/decode

Subsection "X 12: Explicit encode/decode"
On rare occasion, such as a database read, you may be
given encoded text you need to decode.

.Vb 1
  use Encode qw(encode decode);

  my $chars = decode("shiftjis", $bytes, 1);
 # OR
  my $bytes = encode("MIME-Header-ISO_2022_JP", $chars, 1);
.Ve

For streams all in the same encoding, don't use encode/decode; instead
set the file encoding when you open the file or immediately after with
\f(CW\*(C`binmode\*(C' as described later below.

### X 13: Decode program arguments as utf8

Subsection "X 13: Decode program arguments as utf8"
.Vb 6
     $ perl -CA ...
 or
     $ export PERL_UNICODE=A
 or
    use Encode qw(decode);
    @ARGV = map \{ decode(\*(AqUTF-8\*(Aq, $_, 1) \} @ARGV;
.Ve

### X 14: Decode program arguments as locale encoding

Subsection "X 14: Decode program arguments as locale encoding"
.Vb 3
    # cpan -i Encode::Locale
    use Encode qw(locale);
    use Encode::Locale;

    # use "locale" as an arg to encode/decode
    @ARGV = map \{ decode(locale => $_, 1) \} @ARGV;
.Ve

### X 15: Declare STD\{\s-1IN,OUT,ERR\s0\} to be utf8

Subsection "X 15: Declare STD\{IN,OUT,ERR\} to be utf8"
Use a command-line option, an environment variable, or else
call \f(CW\*(C`binmode\*(C' explicitly:

.Vb 9
     $ perl -CS ...
 or
     $ export PERL_UNICODE=S
 or
     use open qw(:std :encoding(UTF-8));
 or
     binmode(STDIN,  ":encoding(UTF-8)");
     binmode(STDOUT, ":utf8");
     binmode(STDERR, ":utf8");
.Ve

### X 16: Declare STD\{\s-1IN,OUT,ERR\s0\} to be in locale encoding

Subsection "X 16: Declare STD\{IN,OUT,ERR\} to be in locale encoding"
.Vb 3
    # cpan -i Encode::Locale
    use Encode;
    use Encode::Locale;

    # or as a stream for binmode or open
    binmode STDIN,  ":encoding(console_in)"  if -t STDIN;
    binmode STDOUT, ":encoding(console_out)" if -t STDOUT;
    binmode STDERR, ":encoding(console_out)" if -t STDERR;
.Ve

### X 17: Make file I/O default to utf8

Subsection "X 17: Make file I/O default to utf8"
Files opened without an encoding argument will be in \s-1UTF-8:\s0

.Vb 5
     $ perl -CD ...
 or
     $ export PERL_UNICODE=D
 or
     use open qw(:encoding(UTF-8));
.Ve

### X 18: Make all I/O and args default to utf8

Subsection "X 18: Make all I/O and args default to utf8"
.Vb 7
     $ perl -CSDA ...
 or
     $ export PERL_UNICODE=SDA
 or
     use open qw(:std :encoding(UTF-8));
     use Encode qw(decode);
     @ARGV = map \{ decode(\*(AqUTF-8\*(Aq, $_, 1) \} @ARGV;
.Ve

### X 19: Open file with specific encoding

Subsection "X 19: Open file with specific encoding"
Specify stream encoding.  This is the normal way
to deal with encoded text, not by calling low-level
functions.

.Vb 7
 # input file
     open(my $in_file, "< :encoding(UTF-16)", "wintext");
 OR
     open(my $in_file, "<", "wintext");
     binmode($in_file, ":encoding(UTF-16)");
 THEN
     my $line = <$in_file>;

 # output file
     open($out_file, "> :encoding(cp1252)", "wintext");
 OR
     open(my $out_file, ">", "wintext");
     binmode($out_file, ":encoding(cp1252)");
 THEN
     print $out_file "some text\\n";
.Ve

More layers than just the encoding can be specified here. For example,
the incantation \f(CW":raw :encoding(UTF-16LE) :crlf" includes implicit
\s-1CRLF\s0 handling.

### X 20: Unicode casing

Subsection "X 20: Unicode casing"
Unicode casing is very different from \s-1ASCII\s0 casing.

.Vb 2
 uc("henry X")  # "HENRY X"
 uc("tschu\*:\*8")   # "TSCHU\*:SS"  notice \*8 => SS

 # both are true:
 "tschu\*:\*8"  =~ /TSCHU\*:SS/i   # notice \*8 => SS
 "XXXXXXX" =~ /XXXXXXX/i   # notice X,X,X sameness
.Ve

### X 21: Unicode case-insensitive comparisons

Subsection "X 21: Unicode case-insensitive comparisons"
Also available in the \s-1CPAN\s0 Unicode::CaseFold module,
the new \f(CW\*(C`fc\*(C' XfoldcaseX function from v5.16 grants
access to the same Unicode casefolding as the \f(CW\*(C`/i\*(C'
pattern modifier has always used:

.Vb 1
 use feature "fc"; # fc() function is from v5.16

 # sort case-insensitively
 my @sorted = sort \{ fc($a) cmp fc($b) \} @list;

 # both are true:
 fc("tschu\*:\*8")  eq fc("TSCHU\*:SS")
 fc("XXXXXXX") eq fc("XXXXXXX")
.Ve

### X 22: Match Unicode linebreak sequence in regex

Subsection "X 22: Match Unicode linebreak sequence in regex"
A Unicode linebreak matches the two-character \s-1CRLF\s0
grapheme or any of seven vertical whitespace characters.
Good for dealing with textfiles coming from different
operating systems.

.Vb 1
 \\R

 s/\\R/\\n/g;  # normalize all linebreaks to \\n
.Ve

### X 23: Get character category

Subsection "X 23: Get character category"
Find the general category of a numeric codepoint.

.Vb 2
 use Unicode::UCD qw(charinfo);
 my $cat = charinfo(0x3A3)->\{category\};  # "Lu"
.Ve

### X 24: Disabling Unicode-awareness in builtin charclasses

Subsection "X 24: Disabling Unicode-awareness in builtin charclasses"
Disable \f(CW\*(C`\\w\*(C', \f(CW\*(C`\\b\*(C', \f(CW\*(C`\\s\*(C', \f(CW\*(C`\\d\*(C', and the \s-1POSIX\s0
classes from working correctly on Unicode either in this
scope, or in just one regex.

.Vb 2
 use v5.14;
 use re "/a";

 # OR

 my($num) = $str =~ /(\\d+)/a;
.Ve

Or use specific un-Unicode properties, like \f(CW\*(C`\\p\{ahex\}\*(C'
and \f(CW\*(C`\\p\{POSIX_Digit\*(C'\}.  Properties still work normally
no matter what charset modifiers (\f(CW\*(C`/d /u /l /a /aa\*(C')
should be effect.

### X 25: Match Unicode properties in regex with \\p, \\P

Subsection "X 25: Match Unicode properties in regex with p, P"
These all match a single codepoint with the given
property.  Use \f(CW\*(C`\\P\*(C' in place of \f(CW\*(C`\\p\*(C' to match
one codepoint lacking that property.

.Vb 8
 \\pL, \\pN, \\pS, \\pP, \\pM, \\pZ, \\pC
 \\p\{Sk\}, \\p\{Ps\}, \\p\{Lt\}
 \\p\{alpha\}, \\p\{upper\}, \\p\{lower\}
 \\p\{Latin\}, \\p\{Greek\}
 \\p\{script_extensions=Latin\}, \\p\{scx=Greek\}
 \\p\{East_Asian_Width=Wide\}, \\p\{EA=W\}
 \\p\{Line_Break=Hyphen\}, \\p\{LB=HY\}
 \\p\{Numeric_Value=4\}, \\p\{NV=4\}
.Ve

### X 26: Custom character properties

Subsection "X 26: Custom character properties"
Define at compile-time your own custom character
properties for use in regexes.

.Vb 2
 # using private-use characters
 sub In_Tengwar \{ "E000\\tE07F\\n" \}

 if (/\\p\{In_Tengwar\}/) \{ ... \}

 # blending existing properties
 sub Is_GraecoRoman_Title \{<<\*(AqEND_OF_SET\*(Aq\}
 +utf8::IsLatin
 +utf8::IsGreek
 &utf8::IsTitle
 END_OF_SET

 if (/\\p\{Is_GraecoRoman_Title\}/ \{ ... \}
.Ve

### X 27: Unicode normalization

Subsection "X 27: Unicode normalization"
Typically render into \s-1NFD\s0 on input and \s-1NFC\s0 on output. Using \s-1NFKC\s0 or \s-1NFKD\s0
functions improves recall on searches, assuming you've already done to the
same text to be searched. Note that this is about much more than just pre-
combined compatibility glyphs; it also reorders marks according to their
canonical combining classes and weeds out singletons.

.Vb 5
 use Unicode::Normalize;
 my $nfd  = NFD($orig);
 my $nfc  = NFC($orig);
 my $nfkd = NFKD($orig);
 my $nfkc = NFKC($orig);
.Ve

### X 28: Convert non-ASCII Unicode numerics

Subsection "X 28: Convert non-ASCII Unicode numerics"
Unless youXve used \f(CW\*(C`/a\*(C' or \f(CW\*(C`/aa\*(C', \f(CW\*(C`\\d\*(C' matches more than
\s-1ASCII\s0 digits only, but PerlXs implicit string-to-number
conversion does not current recognize these.  HereXs how to
convert such strings manually.

.Vb 8
 use v5.14;  # needed for num() function
 use Unicode::UCD qw(num);
 my $str = "got X and XXXX and X and here";
 my @nums = ();
 while ($str =~ /(\\d+|\\N)/g) \{  # not just ASCII!
    push @nums, num($1);
 \}
 say "@nums";   #     12      4567      0.875

 use charnames qw(:full);
 my $nv = num("\\N\{RUMI DIGIT ONE\}\\N\{RUMI DIGIT TWO\}");
.Ve

### X 29: Match Unicode grapheme cluster in regex

Subsection "X 29: Match Unicode grapheme cluster in regex"
Programmer-visible XcharactersX are codepoints matched by \f(CW\*(C`/./s\*(C',
but user-visible XcharactersX are graphemes matched by \f(CW\*(C`/\\X/\*(C'.

.Vb 3
 # Find vowel *plus* any combining diacritics,underlining,etc.
 my $nfd = NFD($orig);
 $nfd =~ / (?=[aeiou]) \\X /xi
.Ve

### X 30: Extract by grapheme instead of by codepoint (regex)

Subsection "X 30: Extract by grapheme instead of by codepoint (regex)"
.Vb 2
 # match and grab five first graphemes
 my($first_five) = $str =~ /^ ( \\X\{5\} ) /x;
.Ve

### X 31: Extract by grapheme instead of by codepoint (substr)

Subsection "X 31: Extract by grapheme instead of by codepoint (substr)"
.Vb 4
 # cpan -i Unicode::GCString
 use Unicode::GCString;
 my $gcs = Unicode::GCString->new($str);
 my $first_five = $gcs->substr(0, 5);
.Ve

### X 32: Reverse string by grapheme

Subsection "X 32: Reverse string by grapheme"
Reversing by codepoint messes up diacritics, mistakenly converting
\f(CW\*(C`cre\*`me bru\*^le\*'e\*(C' into \f(CW\*(C`e\*'elXurb emXerc\*(C' instead of into \f(CW\*(C`ee\*'lu\*^rb eme\*`rc\*(C';
so reverse by grapheme instead.  Both these approaches work
right no matter what normalization the string is in:

.Vb 1
 $str = join("", reverse $str =~ /\\X/g);

 # OR: cpan -i Unicode::GCString
 use Unicode::GCString;
 $str = reverse Unicode::GCString->new($str);
.Ve

### X 33: String length in graphemes

Subsection "X 33: String length in graphemes"
The string \f(CW\*(C`bru\*^le\*'e\*(C' has six graphemes but up to eight codepoints.
This counts by grapheme, not by codepoint:

.Vb 3
 my $str = "bru\*^le\*'e";
 my $count = 0;
 while ($str =~ /\\X/g) \{ $count++ \}

  # OR: cpan -i Unicode::GCString
 use Unicode::GCString;
 my $gcs = Unicode::GCString->new($str);
 my $count = $gcs->length;
.Ve

### X 34: Unicode column-width for printing

Subsection "X 34: Unicode column-width for printing"
PerlXs \f(CW\*(C`printf\*(C', \f(CW\*(C`sprintf\*(C', and \f(CW\*(C`format\*(C' think all
codepoints take up 1 print column, but many take 0 or 2.
Here to show that normalization makes no difference,
we print out both forms:

.Vb 2
 use Unicode::GCString;
 use Unicode::Normalize;

 my @words = qw/cre\*`me bru\*^le\*'e/;
 @words = map \{ NFC($_), NFD($_) \} @words;

 for my $str (@words) \{
     my $gcs = Unicode::GCString->new($str);
     my $cols = $gcs->columns;
     my $pad = " " x (10 - $cols);
     say str, $pad, " |";
 \}
.Ve

generates this to show that it pads correctly no matter
the normalization:

.Vb 4
 cre\*`me      |
 creXme      |
 bru\*^le\*'e     |
 bruXleXe     |
.Ve

### X 35: Unicode collation

Subsection "X 35: Unicode collation"
Text sorted by numeric codepoint follows no reasonable alphabetic order;
use the \s-1UCA\s0 for sorting text.

.Vb 3
 use Unicode::Collate;
 my $col = Unicode::Collate->new();
 my @list = $col->sort(@old_list);
.Ve

See the *ucsort* program from the Unicode::Tussle \s-1CPAN\s0 module
for a convenient command-line interface to this module.

### X 36: Case- \fIand accent-insensitive Unicode sort

Subsection "X 36: Case- and accent-insensitive Unicode sort"
Specify a collation strength of level 1 to ignore case and
diacritics, only looking at the basic character.

.Vb 3
 use Unicode::Collate;
 my $col = Unicode::Collate->new(level => 1);
 my @list = $col->sort(@old_list);
.Ve

### X 37: Unicode locale collation

Subsection "X 37: Unicode locale collation"
Some locales have special sorting rules.

.Vb 4
 # either use v5.12, OR: cpan -i Unicode::Collate::Locale
 use Unicode::Collate::Locale;
 my $col = Unicode::Collate::Locale->new(locale => "de_\|_phonebook");
 my @list = $col->sort(@old_list);
.Ve

The *ucsort* program mentioned above accepts a \f(CW\*(C`--locale\*(C' parameter.
.ie n .SS "X 38: Making ""cmp"" work on text instead of codepoints"
.el .SS "X 38: Making \f(CWcmp work on text instead of codepoints"
Subsection "X 38: Making cmp work on text instead of codepoints"
Instead of this:

.Vb 5
 @srecs = sort \{
     $b->\{AGE\}   <=>  $a->\{AGE\}
                 ||
     $a->\{NAME\}  cmp  $b->\{NAME\}
 \} @recs;
.Ve

Use this:

.Vb 9
 my $coll = Unicode::Collate->new();
 for my $rec (@recs) \{
     $rec->\{NAME_key\} = $coll->getSortKey( $rec->\{NAME\} );
 \}
 @srecs = sort \{
     $b->\{AGE\}       <=>  $a->\{AGE\}
                     ||
     $a->\{NAME_key\}  cmp  $b->\{NAME_key\}
 \} @recs;
.Ve

### X 39: Case- \fIand accent-insensitive comparisons

Subsection "X 39: Case- and accent-insensitive comparisons"
Use a collator object to compare Unicode text by character
instead of by codepoint.

.Vb 5
 use Unicode::Collate;
 my $es = Unicode::Collate->new(
     level => 1,
     normalization => undef
 );

  # now both are true:
 $es->eq("Garci\*'a",  "GARCIA" );
 $es->eq("Ma\*'rquez", "MARQUEZ");
.Ve

### X 40: Case- \fIand accent-insensitive locale comparisons

Subsection "X 40: Case- and accent-insensitive locale comparisons"
Same, but in a specific locale.

.Vb 3
 my $de = Unicode::Collate::Locale->new(
            locale => "de_\|_phonebook",
          );

 # now this is true:
 $de->eq("tschu\*:\*8", "TSCHUESS");  # notice u\*: => UE, \*8 => SS
.Ve

### X 41: Unicode linebreaking

Subsection "X 41: Unicode linebreaking"
Break up text into lines according to Unicode rules.

.Vb 3
 # cpan -i Unicode::LineBreak
 use Unicode::LineBreak;
 use charnames qw(:full);

 my $para = "This is a super\\N\{HYPHEN\}long string. " x 20;
 my $fmt = Unicode::LineBreak->new;
 print $fmt->break($para), "\\n";
.Ve

### X 42: Unicode text in \s-1DBM\s0 hashes, the tedious way

Subsection "X 42: Unicode text in DBM hashes, the tedious way"
Using a regular Perl string as a key or value for a \s-1DBM\s0
hash will trigger a wide character exception if any codepoints
wonXt fit into a byte.  HereXs how to manually manage the translation:

.Vb 3
    use DB_File;
    use Encode qw(encode decode);
    tie %dbhash, "DB_File", "pathname";

 # STORE

    # assume $uni_key and $uni_value are abstract Unicode strings
    my $enc_key   = encode("UTF-8", $uni_key, 1);
    my $enc_value = encode("UTF-8", $uni_value, 1);
    $dbhash\{$enc_key\} = $enc_value;

 # FETCH

    # assume $uni_key holds a normal Perl string (abstract Unicode)
    my $enc_key   = encode("UTF-8", $uni_key, 1);
    my $enc_value = $dbhash\{$enc_key\};
    my $uni_value = decode("UTF-8", $enc_value, 1);
.Ve

### X 43: Unicode text in \s-1DBM\s0 hashes, the easy way

Subsection "X 43: Unicode text in DBM hashes, the easy way"
HereXs how to implicitly manage the translation; all encoding
and decoding is done automatically, just as with streams that
have a particular encoding attached to them:

.Vb 2
    use DB_File;
    use DBM_Filter;

    my $dbobj = tie %dbhash, "DB_File", "pathname";
    $dbobj->Filter_Value("utf8");  # this is the magic bit

 # STORE

    # assume $uni_key and $uni_value are abstract Unicode strings
    $dbhash\{$uni_key\} = $uni_value;

  # FETCH

    # $uni_key holds a normal Perl string (abstract Unicode)
    my $uni_value = $dbhash\{$uni_key\};
.Ve

### X 44: \s-1PROGRAM:\s0 Demo of Unicode collation and printing

Subsection "X 44: PROGRAM: Demo of Unicode collation and printing"
HereXs a full program showing how to make use of locale-sensitive
sorting, Unicode casing, and managing print widths when some of the
characters take up zero or two columns, not just one column each time.
When run, the following program produces this nicely aligned output:

.Vb 10
    Cre\*`me Bru\*^le\*'e....... X2.00
    E\*'clair............. X1.60
    Fideua\*`............. X4.20
    Hamburger.......... X6.00
    Jamo\*'n Serrano...... X4.45
    Linguic\*,a........... X7.00
    Pa\*^te\*'............... X4.15
    Pears.............. X2.00
    Pe\*^ches............. X2.25
    Smo\*/rbro\*/d........... X5.75
    Spa\*:tzle............ X5.50
    Xoric\*,o............. X3.00
    XXXXX.............. X6.50
    XXX............. X4.00
    XXX............. X2.65
    XXXXX......... X8.00
    XXXXXXX..... X1.85
    XX............... X9.99
    XX............... X7.50
.Ve

Here's that program; tested on v5.14.

.Vb 12
 #!/usr/bin/env perl
 # umenu - demo sorting and printing of Unicode food
 #
 # (obligatory and increasingly long preamble)
 #
 use utf8;
 use v5.14;                       # for locale sorting
 use strict;
 use warnings;
 use warnings  qw(FATAL utf8);    # fatalize encoding faults
 use open      qw(:std :encoding(UTF-8)); # undeclared streams in UTF-8
 use charnames qw(:full :short);  # unneeded in v5.16

 # std modules
 use Unicode::Normalize;          # std perl distro as of v5.8
 use List::Util qw(max);          # std perl distro as of v5.10
 use Unicode::Collate::Locale;    # std perl distro as of v5.14

 # cpan modules
 use Unicode::GCString;           # from CPAN

 # forward defs
 sub pad($$$);
 sub colwidth(_);
 sub entitle(_);

 my %price = (
     "XXXXX"             => 6.50, # gyros
     "pears"             => 2.00, # like um, pears
     "linguic\*,a"          => 7.00, # spicy sausage, Portuguese
     "xoric\*,o"            => 3.00, # chorizo sausage, Catalan
     "hamburger"         => 6.00, # burgermeister meisterburger
     "e\*'clair"            => 1.60, # dessert, French
     "smo\*/rbro\*/d"          => 5.75, # sandwiches, Norwegian
     "spa\*:tzle"           => 5.50, # Bayerisch noodles, little sparrows
     "XX"              => 7.50, # bao1 zi5, steamed pork buns, Mandarin
     "jamo\*'n serrano"     => 4.45, # country ham, Spanish
     "pe\*^ches"            => 2.25, # peaches, French
     "XXXXXXX"    => 1.85, # cream-filled pastry like eclair
     "XXX"            => 4.00, # makgeolli, Korean rice wine
     "XX"              => 9.99, # sushi, Japanese
     "XXX"            => 2.65, # omochi, rice cakes, Japanese
     "cre\*`me bru\*^le\*'e"      => 2.00, # crema catalana
     "fideua\*`"            => 4.20, # more noodles, Valencian
                                  # (Catalan=fideuada)
     "pa\*^te\*'"              => 4.15, # gooseliver paste, French
     "XXXXX"        => 8.00, # okonomiyaki, Japanese
 );

 my $width = 5 + max map \{ colwidth \} keys %price;

 # So the Asian stuff comes out in an order that someone
 # who reads those scripts won\*(Aqt freak out over; the
 # CJK stuff will be in JIS X 0208 order that way.
 my $coll  = Unicode::Collate::Locale->new(locale => "ja");

 for my $item ($coll->sort(keys %price)) \{
     print pad(entitle($item), $width, ".");
     printf " X%.2f\\n", $price\{$item\};
 \}

 sub pad($$$) \{
     my($str, $width, $padchar) = @_;
     return $str . ($padchar x ($width - colwidth($str)));
 \}

 sub colwidth(_) \{
     my($str) = @_;
     return Unicode::GCString->new($str)->columns;
 \}

 sub entitle(_) \{
     my($str) = @_;
     $str =~ s\{ (?=\\pL)(\\S)     (\\S*) \}
              \{ ucfirst($1) . lc($2)  \}xge;
     return $str;
 \}
.Ve

## SEE ALSO

Header "SEE ALSO"
See these manpages, some of which are \s-1CPAN\s0 modules:
perlunicode, perluniprops,
perlre, perlrecharclass,
perluniintro, perlunitut, perlunifaq,
PerlIO, DB_File, DBM_Filter, DBM_Filter::utf8,
Encode, Encode::Locale,
Unicode::UCD,
Unicode::Normalize,
Unicode::GCString, Unicode::LineBreak,
Unicode::Collate, Unicode::Collate::Locale,
Unicode::Unihan,
Unicode::CaseFold,
Unicode::Tussle,
Lingua::JA::Romanize::Japanese,
Lingua::ZH::Romanize::Pinyin,
Lingua::KO::Romanize::Hangul.

The Unicode::Tussle \s-1CPAN\s0 module includes many programs
to help with working with Unicode, including
these programs to fully or partly replace standard utilities:
*tcgrep* instead of *egrep*,
*uniquote* instead of *cat -v* or *hexdump*,
*uniwc* instead of *wc*,
*unilook* instead of *look*,
*unifmt* instead of *fmt*,
and
*ucsort* instead of *sort*.
For exploring Unicode character names and character properties,
see its *uniprops*, *unichars*, and *uninames* programs.
It also supplies these programs, all of which are general filters that do Unicode-y things:
*unititle* and *unicaps*;
*uniwide* and *uninarrow*;
*unisupers* and *unisubs*;
*nfd*, *nfc*, *nfkd*, and *nfkc*;
and *uc*, *lc*, and *tc*.

Finally, see the published Unicode Standard (page numbers are from version
6.0.0), including these specific annexes and technical reports:

- X3.13 Default Case Algorithms, page 113; X4.2  Case, pages 120X122; Case Mappings, page 166X172, especially Caseless Matching starting on page 170.
Item "X3.13 Default Case Algorithms, page 113; X4.2 Case, pages 120X122; Case Mappings, page 166X172, especially Caseless Matching starting on page 170."
0

- \s-1UAX\s0 #44: Unicode Character Database
Item "UAX #44: Unicode Character Database"

- \s-1UTS\s0 #18: Unicode Regular Expressions
Item "UTS #18: Unicode Regular Expressions"

- \s-1UAX\s0 #15: Unicode Normalization Forms
Item "UAX #15: Unicode Normalization Forms"

- \s-1UTS\s0 #10: Unicode Collation Algorithm
Item "UTS #10: Unicode Collation Algorithm"

- \s-1UAX\s0 #29: Unicode Text Segmentation
Item "UAX #29: Unicode Text Segmentation"

- \s-1UAX\s0 #14: Unicode Line Breaking Algorithm
Item "UAX #14: Unicode Line Breaking Algorithm"

- \s-1UAX\s0 #11: East Asian Width
Item "UAX #11: East Asian Width"
.PD

## AUTHOR

Header "AUTHOR"
Tom Christiansen <tchrist@perl.com> wrote this, with occasional
kibbitzing from Larry Wall and Jeffrey Friedl in the background.

## COPYRIGHT AND LICENCE

Header "COPYRIGHT AND LICENCE"
Copyright X 2012 Tom Christiansen.

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

Most of these examples taken from the current edition of the XCamel BookX;
that is, from the 4XX Edition of *Programming Perl*, Copyright X 2012 Tom
Christiansen <et al.>, 2012-02-13 by OXReilly Media.  The code itself is
freely redistributable, and you are encouraged to transplant, fold,
spindle, and mutilate any of the examples in this manpage however you please
for inclusion into your own programs without any encumbrance whatsoever.
Acknowledgement via code comment is polite but not required.

## REVISION HISTORY

Header "REVISION HISTORY"
v1.0.0 X first public release, 2012-02-27
