+++
author = "None Specified"
manpage_name = "postqueue"
manpage_section = "1"
detected_package_version = "0"
description = "The postqueue(1) command implements the Postfix user interface for queue management. It implements operations that are traditionally available via the sendmail(1) command. See the postsuper(1) command for queue operations that require super-user p..."
operating_system = "macos"
title = "postqueue(1)"
keywords = ["na", "qmgr", "queue", "manager", "showq", "list", "mail", "flush", "fast", "service", "sendmail", "sendmail-compatible", "user", "interface", "postsuper", "privileged", "operations", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "etrn_readme", "postfix", "etrn", "howto", "license", "the", "secure", "mailer", "must", "be", "distributed", "with", "software", "history", "postqueue", "command", "was", "introduced", "version", "1", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
date = "Sun Feb 16 04:48:24 2025"
operating_system_version = "15.3"
manpage_format = "troff"
+++

POSTQUEUE 1


## NAME

postqueue
-
Postfix queue control

## SYNOPSIS

.na

```
.ti -4
To flush the mail queue:

postqueue [\-v] [\-c config_dir] \-f

postqueue [\-v] [\-c config_dir] \-i queue_id

postqueue [\-v] [\-c config_dir] \-s site

.ti -4
To list the mail queue:

postqueue [\-v] [\-c config_dir] \-j

postqueue [\-v] [\-c config_dir] \-p
.SH DESCRIPTION
.ad
```

The **postqueue**(1) command implements the Postfix user interface
for queue management. It implements operations that are
traditionally available via the **sendmail**(1) command.
See the **postsuper**(1) command for queue operations
that require super-user privileges such as deleting a message
from the queue or changing the status of a message.

The following options are recognized:

- \fB-c
The **main.cf** configuration file is in the named directory
instead of the default configuration directory. See also the
MAIL_CONFIG environment setting below.
**-f**
Flush the queue: attempt to deliver all queued mail.

This option implements the traditional "**sendmail -q**" command,
by contacting the Postfix **qmgr**(8) daemon.

Warning: flushing undeliverable mail frequently will result in
poor delivery performance of all other mail.

- \fB-i
Schedule immediate delivery of deferred mail with the
specified queue ID.

This option implements the traditional **sendmail -qI**
command, by contacting the **flush**(8) server.

This feature is available with Postfix version 2.4 and later.
"**-j**"
Produce a queue listing in JSON format, based on output
from the showq(8) daemon.  The result is a stream of zero
or more JSON objects, one per queue file.  Each object is
followed by a newline character to support simple streaming
parsers. See "**JSON OBJECT FORMAT**" below for details.

This feature is available in Postfix 3.1 and later.
**-p**
Produce a traditional sendmail-style queue listing.
This option implements the traditional **mailq** command,
by contacting the Postfix **showq**(8) daemon.

Each queue entry shows the queue file ID, message
size, arrival time, sender, and the recipients that still need to
be delivered.  If mail could not be delivered upon the last attempt,
the reason for failure is shown. The queue ID string
is followed by an optional status character:

> *****
The message is in the **active** queue, i.e. the message is
selected for delivery.
**!**
The message is in the **hold** queue, i.e. no further delivery
attempt will be made until the mail is taken off hold.



- \fB-s
Schedule immediate delivery of all mail that is queued for the named
*site*. A numerical site must be specified as a valid RFC 5321
address literal enclosed in [], just like in email addresses.
The site must be eligible for the "fast flush" service.
See **flush**(8) for more information about the "fast flush"
service.

This option implements the traditional "**sendmail -qR**site"
command, by contacting the Postfix **flush**(8) daemon.
**-v**
Enable verbose logging for debugging purposes. Multiple **-v**
options make the software increasingly verbose. As of Postfix 2.3,
this option is available for the super-user only.

## JSON OBJECT FORMAT

.na

```
.ad
```

Each JSON object represents one queue file; it is emitted
as a single text line followed by a newline character.

Object members have string values unless indicated otherwise.
Programs should ignore object members that are not listed
here; the list of members is expected to grow over time.
**queue_name**
The name of the queue where the message was found.  Note
that the contents of the mail queue may change while it is
being listed; some messages may appear more than once, and
some messages may be missed.
**queue_id**
The queue file name. The queue_id may be reused within a
Postfix instance unless "enable_long_queue_ids = true" and
time is monotonic.  Even then, the queue_id is not expected
to be unique between different Postfix instances.  Management
tools that require a unique name should combine the queue_id
with the myhostname setting of the Postfix instance.
**arrival_time**
The number of seconds since the start of the UNIX epoch.
**message_size**
The number of bytes in the message header and body. This
number does not include message envelope information. It
is approximately equal to the number of bytes that would
be transmitted via SMTP including the <CR><LF> line endings.
**sender**
The envelope sender address.
**recipients**
An array containing zero or more objects with members:

> **address**
One recipient address.
**delay_reason**
If present, the reason for delayed delivery.  Delayed
recipients may have no delay reason, for example, while
delivery is in progress, or after the system was stopped
before it could record the reason.



## SECURITY

.na

```
.ad
```

This program is designed to run with set-group ID privileges, so
that it can connect to Postfix daemon processes.

## STANDARDS

.na

```
RFC 7159 (JSON notation)
.SH DIAGNOSTICS
.ad
```

Problems are logged to **syslogd**(8) and to the standard error
stream.

## ENVIRONMENT

.na

```
.ad
```

MAIL_CONFIG
Directory with the **main.cf** file. In order to avoid exploitation
of set-group ID privileges, a non-standard directory is allowed only
if:

> \(bu
The name is listed in the standard **main.cf** file with the
**alternate_config_directories** configuration parameter.
\(bu
The command is invoked by the super-user.



## CONFIGURATION PARAMETERS

.na

```
.ad
```

The following **main.cf** parameters are especially relevant to
this program.
The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

- \fBalternate_config_directories
A list of non-default Postfix configuration directories that may
be specified with "-c config_directory" on the command line (in the
case of **sendmail**(1), with "-C config_directory"), or via the MAIL_CONFIG
environment parameter.

- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBcommand_directory (see 'postconf -d'
The location of all postfix administrative commands.

- \fBfast_flush_domains
Optional list of destinations that are eligible for per-destination
logfiles with mail that is queued to those destinations.

- \fBimport_environment (see 'postconf -d'
The list of environment parameters that a Postfix process will
import from a non-Postfix parent process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

- \fBtrigger_timeout
The time limit for sending a trigger to a Postfix daemon (for
example, the **pickup**(8) or **qmgr**(8) daemon).

Available in Postfix version 2.2 and later:

- \fBauthorized_flush_users
List of users who are authorized to flush the queue.

- \fBauthorized_mailq_users
List of users who are authorized to view the queue.

## FILES

.na

```
/var/spool/postfix, mail queue
.SH "SEE ALSO"
.na

```
qmgr(8), queue manager
showq(8), list mail queue
flush(8), fast flush service
sendmail(1), Sendmail\-compatible user interface
postsuper(1), privileged queue operations
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
ETRN_README, Postfix ETRN howto
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## HISTORY



The postqueue command was introduced with Postfix version 1.1.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
