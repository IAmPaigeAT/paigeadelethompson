+++
manpage_name = "perlre"
author = "None Specified"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "the", "syntax", "of", "patterns", "used", "in", "perl", "pattern", "matching", "evolved", "from", "those", "supplied", "bell", "labs", "research", "unix", "8th", "edition", "version", "8", "regex", "routines", "code", "is", "actually", "derived", "distantly", "henry", "spencer", "s", "freely", "redistributable", "reimplementation", "v8", "perlrequick", "perlretut", "l", "regexp", "quote-like", "operators", "r", "perlop", "gory", "details", "parsing", "quoted", "constructs", "perlfaq6", "pos", "perlfunc", "perllocale", "perlebcdic", "fimastering", "regular", "expressions", "by", "jeffrey", "friedl", "published", "o", "reilly", "and", "associates"]
manpage_section = "1"
operating_system_version = "15.3"
manpage_format = "troff"
description = "This page describes the syntax of regular expressions in Perl. If you havent used regular expressions before, a tutorial introduction is available in perlretut.  If you know just a little about them, a quick-start introduction is available in perl..."
title = "perlre(1)"
operating_system = "macos"
date = "2022-02-19"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLRE 1"
PERLRE 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlre - Perl regular expressions
Xref "regular expression regex regexp"

## DESCRIPTION

Header "DESCRIPTION"
This page describes the syntax of regular expressions in Perl.

If you haven't used regular expressions before, a tutorial introduction
is available in perlretut.  If you know just a little about them,
a quick-start introduction is available in perlrequick.

Except for \*(L"The Basics\*(R" section, this page assumes you are familiar
with regular expression basics, like what is a \*(L"pattern\*(R", what does it
look like, and how it is basically used.  For a reference on how they
are used, plus various examples of the same, see discussions of \f(CW\*(C`m//\*(C',
\f(CW\*(C`s///\*(C', \f(CW\*(C`qr//\*(C' and \f(CW"??" in \*(L"Regexp Quote-Like Operators\*(R" in perlop.

New in v5.22, \f(CW\*(C`use re \*(Aqstrict\*(Aq\*(C' applies stricter
rules than otherwise when compiling regular expression patterns.  It can
find things that, while legal, may not be what you intended.

### The Basics

Xref "regular expression, version 8 regex, version 8 regexp, version 8"
Subsection "The Basics"
Regular expressions are strings with the very particular syntax and
meaning described in this document and auxiliary documents referred to
by this one.  The strings are called \*(L"patterns\*(R".  Patterns are used to
determine if some other string, called the \*(L"target\*(R", has (or doesn't
have) the characteristics specified by the pattern.  We call this
\*(L"matching\*(R" the target string against the pattern.  Usually the match is
done by having the target be the first operand, and the pattern be the
second operand, of one of the two binary operators \f(CW\*(C`=~\*(C' and \f(CW\*(C`!~\*(C',
listed in \*(L"Binding Operators\*(R" in perlop; and the pattern will have been
converted from an ordinary string by one of the operators in
\*(L"Regexp Quote-Like Operators\*(R" in perlop, like so:

.Vb 1
 $foo =~ m/abc/
.Ve

This evaluates to true if and only if the string in the variable \f(CW$foo
contains somewhere in it, the sequence of characters \*(L"a\*(R", \*(L"b\*(R", then \*(L"c\*(R".
(The \f(CW\*(C`=~ m\*(C', or match operator, is described in
\*(L"m/PATTERN/msixpodualngc\*(R" in perlop.)

Patterns that aren't already stored in some variable must be delimitted,
at both ends, by delimitter characters.  These are often, as in the
example above, forward slashes, and the typical way a pattern is written
in documentation is with those slashes.  In most cases, the delimitter
is the same character, fore and aft, but there are a few cases where a
character looks like it has a mirror-image mate, where the opening
version is the beginning delimiter, and the closing one is the ending
delimiter, like

.Vb 1
 $foo =~ m<abc>
.Ve

Most times, the pattern is evaluated in double-quotish context, but it
is possible to choose delimiters to force single-quotish, like

.Vb 1
 $foo =~ m\*(Aqabc\*(Aq
.Ve

If the pattern contains its delimiter within it, that delimiter must be
escaped.  Prefixing it with a backslash (*e.g.*, \f(CW"/foo\\/bar/")
serves this purpose.

Any single character in a pattern matches that same character in the
target string, unless the character is a *metacharacter* with a special
meaning described in this document.  A sequence of non-metacharacters
matches the same sequence in the target string, as we saw above with
\f(CW\*(C`m/abc/\*(C'.

Only a few characters (all of them being \s-1ASCII\s0 punctuation characters)
are metacharacters.  The most commonly used one is a dot \f(CW".", which
normally matches almost any character (including a dot itself).

You can cause characters that normally function as metacharacters to be
interpreted literally by prefixing them with a \f(CW"\\", just like the
pattern's delimiter must be escaped if it also occurs within the
pattern.  Thus, \f(CW"\\." matches just a literal dot, \f(CW"." instead of
its normal meaning.  This means that the backslash is also a
metacharacter, so \f(CW"\\\\" matches a single \f(CW"\\".  And a sequence that
contains an escaped metacharacter matches the same sequence (but without
the escape) in the target string.  So, the pattern \f(CW\*(C`/blur\\\\fl/\*(C' would
match any target string that contains the sequence \f(CW"blur\\fl".

The metacharacter \f(CW"|" is used to match one thing or another.  Thus

.Vb 1
 $foo =~ m/this|that/
.Ve

is \s-1TRUE\s0 if and only if \f(CW$foo contains either the sequence \f(CW"this" or
the sequence \f(CW"that".  Like all metacharacters, prefixing the \f(CW"|"
with a backslash makes it match the plain punctuation character; in its
case, the \s-1VERTICAL LINE.\s0

.Vb 1
 $foo =~ m/this\\|that/
.Ve

is \s-1TRUE\s0 if and only if \f(CW$foo contains the sequence \f(CW"this|that".

You aren't limited to just a single \f(CW"|".

.Vb 1
 $foo =~ m/fee|fie|foe|fum/
.Ve

is \s-1TRUE\s0 if and only if \f(CW$foo contains any of those 4 sequences from
the children's story \*(L"Jack and the Beanstalk\*(R".

As you can see, the \f(CW"|" binds less tightly than a sequence of
ordinary characters.  We can override this by using the grouping
metacharacters, the parentheses \f(CW"(" and \f(CW")".

.Vb 1
 $foo =~ m/th(is|at) thing/
.Ve

is \s-1TRUE\s0 if and only if \f(CW$foo contains either the sequence \f(CW"this\ thing" or the sequence \f(CW"that\ thing".  The portions of the string
that match the portions of the pattern enclosed in parentheses are
normally made available separately for use later in the pattern,
substitution, or program.  This is called \*(L"capturing\*(R", and it can get
complicated.  See \*(L"Capture groups\*(R".

The first alternative includes everything from the last pattern
delimiter (\f(CW"(", \f(CW"(?:" (described later), *etc*. or the beginning
of the pattern) up to the first \f(CW"|", and the last alternative
contains everything from the last \f(CW"|" to the next closing pattern
delimiter.  That's why it's common practice to include alternatives in
parentheses: to minimize confusion about where they start and end.

Alternatives are tried from left to right, so the first
alternative found for which the entire expression matches, is the one that
is chosen. This means that alternatives are not necessarily greedy. For
example: when matching \f(CW\*(C`foo|foot\*(C' against \f(CW"barefoot", only the \f(CW"foo"
part will match, as that is the first alternative tried, and it successfully
matches the target string. (This might not seem important, but it is
important when you are capturing matched text using parentheses.)

Besides taking away the special meaning of a metacharacter, a prefixed
backslash changes some letter and digit characters away from matching
just themselves to instead have special meaning.  These are called
\*(L"escape sequences\*(R", and all such are described in perlrebackslash.  A
backslash sequence (of a letter or digit) that doesn't currently have
special meaning to Perl will raise a warning if warnings are enabled,
as those are reserved for potential future use.

One such sequence is \f(CW\*(C`\\b\*(C', which matches a boundary of some sort.
\f(CW\*(C`\\b\{wb\}\*(C' and a few others give specialized types of boundaries.
(They are all described in detail starting at
\*(L"\\b\{\}, \\b, \\B\{\}, \\B\*(R" in perlrebackslash.)  Note that these don't match
characters, but the zero-width spaces between characters.  They are an
example of a zero-width assertion.  Consider again,

.Vb 1
 $foo =~ m/fee|fie|foe|fum/
.Ve

It evaluates to \s-1TRUE\s0 if, besides those 4 words, any of the sequences
\*(L"feed\*(R", \*(L"field\*(R", \*(L"Defoe\*(R", \*(L"fume\*(R", and many others are in \f(CW$foo.  By
judicious use of \f(CW\*(C`\\b\*(C' (or better (because it is designed to handle
natural language) \f(CW\*(C`\\b\{wb\}\*(C'), we can make sure that only the Giant's
words are matched:

.Vb 2
 $foo =~ m/\\b(fee|fie|foe|fum)\\b/
 $foo =~ m/\\b\{wb\}(fee|fie|foe|fum)\\b\{wb\}/
.Ve

The final example shows that the characters \f(CW"\{" and \f(CW"\}" are
metacharacters.

Another use for escape sequences is to specify characters that cannot
(or which you prefer not to) be written literally.  These are described
in detail in \*(L"Character Escapes\*(R" in perlrebackslash, but the next three
paragraphs briefly describe some of them.

Various control characters can be written in C language style: \f(CW"\\n"
matches a newline, \f(CW"\\t" a tab, \f(CW"\\r" a carriage return, \f(CW"\\f" a
form feed, *etc*.

More generally, \f(CW\*(C`\\\f(CInnn\f(CW\*(C', where *nnn* is a string of three octal
digits, matches the character whose native code point is *nnn*.  You
can easily run into trouble if you don't have exactly three digits.  So
always use three, or since Perl 5.14, you can use \f(CW\*(C`\\o\{...\}\*(C' to specify
any number of octal digits.

Similarly, \f(CW\*(C`\\x\f(CInn\f(CW\*(C', where *nn* are hexadecimal digits, matches the
character whose native ordinal is *nn*.  Again, not using exactly two
digits is a recipe for disaster, but you can use \f(CW\*(C`\\x\{...\}\*(C' to specify
any number of hex digits.

Besides being a metacharacter, the \f(CW"." is an example of a \*(L"character
class\*(R", something that can match any single character of a given set of
them.  In its case, the set is just about all possible characters.  Perl
predefines several character classes besides the \f(CW"."; there is a
separate reference page about just these, perlrecharclass.

You can define your own custom character classes, by putting into your
pattern in the appropriate place(s), a list of all the characters you
want in the set.  You do this by enclosing the list within \f(CW\*(C`[]\*(C' bracket
characters.  These are called \*(L"bracketed character classes\*(R" when we are
being precise, but often the word \*(L"bracketed\*(R" is dropped.  (Dropping it
usually doesn't cause confusion.)  This means that the \f(CW"[" character
is another metacharacter.  It doesn't match anything just by itself; it
is used only to tell Perl that what follows it is a bracketed character
class.  If you want to match a literal left square bracket, you must
escape it, like \f(CW"\\[".  The matching \f(CW"]" is also a metacharacter;
again it doesn't match anything by itself, but just marks the end of
your custom class to Perl.  It is an example of a \*(L"sometimes
metacharacter\*(R".  It isn't a metacharacter if there is no corresponding
\f(CW"[", and matches its literal self:

.Vb 1
 print "]" =~ /]/;  # prints 1
.Ve

The list of characters within the character class gives the set of
characters matched by the class.  \f(CW"[abc]" matches a single \*(L"a\*(R" or \*(L"b\*(R"
or \*(L"c\*(R".  But if the first character after the \f(CW"[" is \f(CW"^", the
class instead matches any character not in the list.  Within a list, the
\f(CW"-" character specifies a range of characters, so that \f(CW\*(C`a-z\*(C'
represents all characters between \*(L"a\*(R" and \*(L"z\*(R", inclusive.  If you want
either \f(CW"-" or \f(CW"]" itself to be a member of a class, put it at the
start of the list (possibly after a \f(CW"^"), or escape it with a
backslash.  \f(CW"-" is also taken literally when it is at the end of the
list, just before the closing \f(CW"]".  (The following all specify the
same class of three characters: \f(CW\*(C`[-az]\*(C', \f(CW\*(C`[az-]\*(C', and \f(CW\*(C`[a\\-z]\*(C'.  All
are different from \f(CW\*(C`[a-z]\*(C', which specifies a class containing
twenty-six characters, even on EBCDIC-based character sets.)

There is lots more to bracketed character classes; full details are in
\*(L"Bracketed Character Classes\*(R" in perlrecharclass.

*Metacharacters*
Xref "metacharacter \ ^ . $ | ( () [ []"
Subsection "Metacharacters"

\*(L"The Basics\*(R" introduced some of the metacharacters.  This section
gives them all.  Most of them have the same meaning as in the *egrep*
command.

Only the \f(CW"\\" is always a metacharacter.  The others are metacharacters
just sometimes.  The following tables lists all of them, summarizes
their use, and gives the contexts where they are metacharacters.
Outside those contexts or if prefixed by a \f(CW"\\", they match their
corresponding punctuation character.  In some cases, their meaning
varies depending on various pattern modifiers that alter the default
behaviors.  See \*(L"Modifiers\*(R".

.Vb 10
            PURPOSE                                  WHERE
 \\   Escape the next character                    Always, except when
                                                  escaped by another \\
 ^   Match the beginning of the string            Not in []
       (or line, if /m is used)
 ^   Complement the [] class                      At the beginning of []
 .   Match any single character except newline    Not in []
       (under /s, includes newline)
 $   Match the end of the string                  Not in [], but can
       (or before newline at the end of the       mean interpolate a
       string; or before any newline if /m is     scalar
       used)
 |   Alternation                                  Not in []
 ()  Grouping                                     Not in []
 [   Start Bracketed Character class              Not in []
 ]   End Bracketed Character class                Only in [], and
                                                    not first
 *   Matches the preceding element 0 or more      Not in []
       times
 +   Matches the preceding element 1 or more      Not in []
       times
 ?   Matches the preceding element 0 or 1         Not in []
       times
 \{   Starts a sequence that gives number(s)       Not in []
       of times the preceding element can be
       matched
 \{   when following certain escape sequences
       starts a modifier to the meaning of the
       sequence
 \}   End sequence started by \{
 -   Indicates a range                            Only in [] interior
 #   Beginning of comment, extends to line end    Only with /x modifier
.Ve

Notice that most of the metacharacters lose their special meaning when
they occur in a bracketed character class, except \f(CW"^" has a different
meaning when it is at the beginning of such a class.  And \f(CW"-" and \f(CW"]"
are metacharacters only at restricted positions within bracketed
character classes; while \f(CW"\}" is a metacharacter only when closing a
special construct started by \f(CW"\{".

In double-quotish context, as is usually the case,  you need to be
careful about \f(CW"$" and the non-metacharacter \f(CW"@".  Those could
interpolate variables, which may or may not be what you intended.

These rules were designed for compactness of expression, rather than
legibility and maintainability.  The \*(L"/x and /xx\*(R" pattern
modifiers allow you to insert white space to improve readability.  And
use of \f(CW\*(C`re\ \*(Aqstrict\*(Aq\*(C' adds extra checking to
catch some typos that might silently compile into something unintended.

By default, the \f(CW"^" character is guaranteed to match only the
beginning of the string, the \f(CW"$" character only the end (or before the
newline at the end), and Perl does certain optimizations with the
assumption that the string contains only one line.  Embedded newlines
will not be matched by \f(CW"^" or \f(CW"$".  You may, however, wish to treat a
string as a multi-line buffer, such that the \f(CW"^" will match after any
newline within the string (except if the newline is the last character in
the string), and \f(CW"$" will match before any newline.  At the
cost of a little more overhead, you can do this by using the
\f(CW"/m" modifier on the pattern match operator.  (Older programs
did this by setting \f(CW$*, but this option was removed in perl 5.10.)
Xref "^ $ m"

To simplify multi-line substitutions, the \f(CW"." character never matches a
newline unless you use the \f(CW\*(C`/s\*(C' modifier, which in effect tells
Perl to pretend the string is a single line\*(--even if it isn't.
Xref ". s"

### Modifiers

Subsection "Modifiers"
*Overview*
Subsection "Overview"

The default behavior for matching can be changed, using various
modifiers.  Modifiers that relate to the interpretation of the pattern
are listed just below.  Modifiers that alter the way a pattern is used
by Perl are detailed in \*(L"Regexp Quote-Like Operators\*(R" in perlop and
\*(L"Gory details of parsing quoted constructs\*(R" in perlop.  Modifiers can be added
dynamically; see \*(L"Extended Patterns\*(R" below.
.ie n .IP "**\f(CB""m""\fB**" 4
.el .IP "**\f(CBm\fB**" 4
Xref " m regex, multiline regexp, multiline regular expression, multiline"
Item "m"
Treat the string being matched against as multiple lines.  That is, change \f(CW"^" and \f(CW"$" from matching
the start of the string's first line and the end of its last line to
matching the start and end of each line within the string.
.ie n .IP "**\f(CB""s""\fB**" 4
.el .IP "**\f(CBs\fB**" 4
Xref " s regex, single-line regexp, single-line regular expression, single-line"
Item "s"
Treat the string as single line.  That is, change \f(CW"." to match any character
whatsoever, even a newline, which normally it would not match.
.Sp
Used together, as \f(CW\*(C`/ms\*(C', they let the \f(CW"." match any character whatsoever,
while still allowing \f(CW"^" and \f(CW"$" to match, respectively, just after
and just before newlines within the string.
.ie n .IP "**\f(CB""i""\fB**" 4
.el .IP "**\f(CBi\fB**" 4
Xref " i regex, case-insensitive regexp, case-insensitive regular expression, case-insensitive"
Item "i"
Do case-insensitive pattern matching.  For example, \*(L"A\*(R" will match \*(L"a\*(R"
under \f(CW\*(C`/i\*(C'.
.Sp
If locale matching rules are in effect, the case map is taken from the
current
locale for code points less than 255, and from Unicode rules for larger
code points.  However, matches that would cross the Unicode
rules/non-Unicode rules boundary (ords 255/256) will not succeed, unless
the locale is a \s-1UTF-8\s0 one.  See perllocale.
.Sp
There are a number of Unicode characters that match a sequence of
multiple characters under \f(CW\*(C`/i\*(C'.  For example,
\f(CW\*(C`LATIN SMALL LIGATURE FI\*(C' should match the sequence \f(CW\*(C`fi\*(C'.  Perl is not
currently able to do this when the multiple characters are in the pattern and
are split between groupings, or when one or more are quantified.  Thus
.Sp
.Vb 3
 "\\N\{LATIN SMALL LIGATURE FI\}" =~ /fi/i;          # Matches
 "\\N\{LATIN SMALL LIGATURE FI\}" =~ /[fi][fi]/i;    # Doesn\*(Aqt match!
 "\\N\{LATIN SMALL LIGATURE FI\}" =~ /fi*/i;         # Doesn\*(Aqt match!

 # The below doesn\*(Aqt match, and it isn\*(Aqt clear what $1 and $2 would
 # be even if it did!!
 "\\N\{LATIN SMALL LIGATURE FI\}" =~ /(f)(i)/i;      # Doesn\*(Aqt match!
.Ve
.Sp
Perl doesn't match multiple characters in a bracketed
character class unless the character that maps to them is explicitly
mentioned, and it doesn't match them at all if the character class is
inverted, which otherwise could be highly confusing.  See
\*(L"Bracketed Character Classes\*(R" in perlrecharclass, and
\*(L"Negation\*(R" in perlrecharclass.
.ie n .IP "**\f(CB""x""**** and \fB\f(CB""xx""\fB**" 4
.el .IP "**\f(CBx**** and \fB\f(CBxx\fB**" 4
Xref " x"
Item "x and xx"
Extend your pattern's legibility by permitting whitespace and comments.
Details in \*(L"/x and  /xx\*(R"
.ie n .IP "**\f(CB""p""\fB**" 4
.el .IP "**\f(CBp\fB**" 4
Xref " p regex, preserve regexp, preserve"
Item "p"
Preserve the string matched such that \f(CW\*(C`$\{^PREMATCH\}\*(C', \f(CW\*(C`$\{^MATCH\}\*(C', and
\f(CW\*(C`$\{^POSTMATCH\}\*(C' are available for use after matching.
.Sp
In Perl 5.20 and higher this is ignored. Due to a new copy-on-write
mechanism, \f(CW\*(C`$\{^PREMATCH\}\*(C', \f(CW\*(C`$\{^MATCH\}\*(C', and \f(CW\*(C`$\{^POSTMATCH\}\*(C' will be available
after the match regardless of the modifier.
.ie n .IP "**\f(CB""a""****, **\f(CB""d""****, \fB\f(CB""l""\fB**, and \fB\f(CB""u""\fB**" 4
.el .IP "**\f(CBa****, **\f(CBd****, \fB\f(CBl\fB**, and \fB\f(CBu\fB**" 4
Xref " a d l u"
Item "a, d, l, and u"
These modifiers, all new in 5.14, affect which character-set rules
(Unicode, *etc*.) are used, as described below in
\*(L"Character set modifiers\*(R".
.ie n .IP "**\f(CB""n""\fB**" 4
.el .IP "**\f(CBn\fB**" 4
Xref " n regex, non-capture regexp, non-capture regular expression, non-capture"
Item "n"
Prevent the grouping metacharacters \f(CW\*(C`()\*(C' from capturing. This modifier,
new in 5.22, will stop \f(CW$1, \f(CW$2, *etc*... from being filled in.
.Sp
.Vb 2
  "hello" =~ /(hi|hello)/;   # $1 is "hello"
  "hello" =~ /(hi|hello)/n;  # $1 is undef
.Ve
.Sp
This is equivalent to putting \f(CW\*(C`?:\*(C' at the beginning of every capturing group:
.Sp
.Vb 1
  "hello" =~ /(?:hi|hello)/; # $1 is undef
.Ve
.Sp
\f(CW\*(C`/n\*(C' can be negated on a per-group basis. Alternatively, named captures
may still be used.
.Sp
.Vb 3
  "hello" =~ /(?-n:(hi|hello))/n;   # $1 is "hello"
  "hello" =~ /(?<greet>hi|hello)/n; # $1 is "hello", $+\{greet\} is
                                    # "hello"
.Ve

- Other Modifiers
Item "Other Modifiers"
There are a number of flags that can be found at the end of regular
expression constructs that are *not* generic regular expression flags, but
apply to the operation being performed, like matching or substitution (\f(CW\*(C`m//\*(C'
or \f(CW\*(C`s///\*(C' respectively).
.Sp
Flags described further in
\*(L"Using regular expressions in Perl\*(R" in perlretut are:
.Sp
.Vb 2
  c  - keep the current position during repeated matching
  g  - globally match the pattern repeatedly in the string
.Ve
.Sp
Substitution-specific modifiers described in
\*(L"s/PATTERN/REPLACEMENT/msixpodualngcer\*(R" in perlop are:
.Sp
.Vb 4
  e  - evaluate the right-hand side as an expression
  ee - evaluate the right side as a string then eval the result
  o  - pretend to optimize your code, but actually introduce bugs
  r  - perform non-destructive substitution and return the new value
.Ve

Regular expression modifiers are usually written in documentation
as *e.g.*, "the \f(CW\*(C`/x\*(C' modifier", even though the delimiter
in question might not really be a slash.  The modifiers \f(CW\*(C`/imnsxadlup\*(C'
may also be embedded within the regular expression itself using
the \f(CW\*(C`(?...)\*(C' construct, see \*(L"Extended Patterns\*(R" below.

*Details on some modifiers*
Subsection "Details on some modifiers"

Some of the modifiers require more explanation than given in the
\*(L"Overview\*(R" above.

\f(CW\*(C`/x\*(C' and  \f(CW\*(C`/xx\*(C'
Subsection "/x and /xx"

A single \f(CW\*(C`/x\*(C' tells
the regular expression parser to ignore most whitespace that is neither
backslashed nor within a bracketed character class.  You can use this to
break up your regular expression into more readable parts.
Also, the \f(CW"#" character is treated as a metacharacter introducing a
comment that runs up to the pattern's closing delimiter, or to the end
of the current line if the pattern extends onto the next line.  Hence,
this is very much like an ordinary Perl code comment.  (You can include
the closing delimiter within the comment only if you precede it with a
backslash, so be careful!)

Use of \f(CW\*(C`/x\*(C' means that if you want real
whitespace or \f(CW"#" characters in the pattern (outside a bracketed character
class, which is unaffected by \f(CW\*(C`/x\*(C'), then you'll either have to
escape them (using backslashes or \f(CW\*(C`\\Q...\\E\*(C') or encode them using octal,
hex, or \f(CW\*(C`\\N\{\}\*(C' or \f(CW\*(C`\\p\{name=...\}\*(C' escapes.
It is ineffective to try to continue a comment onto the next line by
escaping the \f(CW\*(C`\\n\*(C' with a backslash or \f(CW\*(C`\\Q\*(C'.

You can use \*(L"(?#text)\*(R" to create a comment that ends earlier than the
end of the current line, but \f(CW\*(C`text\*(C' also can't contain the closing
delimiter unless escaped with a backslash.

A common pitfall is to forget that \f(CW"#" characters begin a comment under
\f(CW\*(C`/x\*(C' and are not matched literally.  Just keep that in mind when trying
to puzzle out why a particular \f(CW\*(C`/x\*(C' pattern isn't working as expected.

Starting in Perl v5.26, if the modifier has a second \f(CW"x" within it,
it does everything that a single \f(CW\*(C`/x\*(C' does, but additionally
non-backslashed \s-1SPACE\s0 and \s-1TAB\s0 characters within bracketed character
classes are also generally ignored, and hence can be added to make the
classes more readable.

.Vb 2
    / [d-e g-i 3-7]/xx
    /[ ! @ " # $ % ^ & * () = ? <> \*(Aq ]/xx
.Ve

may be easier to grasp than the squashed equivalents

.Vb 2
    /[d-eg-i3-7]/
    /[!@"#$%^&*()=?<>\*(Aq]/
.Ve

Taken together, these features go a long way towards
making Perl's regular expressions more readable.  Here's an example:

.Vb 6
    # Delete (most) C comments.
    $program =~ s \{
        /\\*     # Match the opening delimiter.
        .*?     # Match a minimal number of characters.
        \\*/     # Match the closing delimiter.
    \} []gsx;
.Ve

Note that anything inside
a \f(CW\*(C`\\Q...\\E\*(C' stays unaffected by \f(CW\*(C`/x\*(C'.  And note that \f(CW\*(C`/x\*(C' doesn't affect
space interpretation within a single multi-character construct.  For
example \f(CW\*(C`(?:...)\*(C' can't have a space between the \f(CW"(",
\f(CW"?", and \f(CW":".  Within any delimiters for such a construct, allowed
spaces are not affected by \f(CW\*(C`/x\*(C', and depend on the construct.  For
example, all constructs using curly braces as delimiters, such as
\f(CW\*(C`\\x\{...\}\*(C' can have blanks within but adjacent to the braces, but not
elsewhere, and no non-blank space characters.  An exception are Unicode
properties which follow Unicode rules, for which see
\*(L"Properties accessible through \\p\{\} and \\P\{\}\*(R" in perluniprops.
Xref " x"

The set of characters that are deemed whitespace are those that Unicode
calls \*(L"Pattern White Space\*(R", namely:

.Vb 11
 U+0009 CHARACTER TABULATION
 U+000A LINE FEED
 U+000B LINE TABULATION
 U+000C FORM FEED
 U+000D CARRIAGE RETURN
 U+0020 SPACE
 U+0085 NEXT LINE
 U+200E LEFT-TO-RIGHT MARK
 U+200F RIGHT-TO-LEFT MARK
 U+2028 LINE SEPARATOR
 U+2029 PARAGRAPH SEPARATOR
.Ve

Character set modifiers
Subsection "Character set modifiers"

\f(CW\*(C`/d\*(C', \f(CW\*(C`/u\*(C', \f(CW\*(C`/a\*(C', and \f(CW\*(C`/l\*(C', available starting in 5.14, are called
the character set modifiers; they affect the character set rules
used for the regular expression.

The \f(CW\*(C`/d\*(C', \f(CW\*(C`/u\*(C', and \f(CW\*(C`/l\*(C' modifiers are not likely to be of much use
to you, and so you need not worry about them very much.  They exist for
Perl's internal use, so that complex regular expression data structures
can be automatically serialized and later exactly reconstituted,
including all their nuances.  But, since Perl can't keep a secret, and
there may be rare instances where they are useful, they are documented
here.

The \f(CW\*(C`/a\*(C' modifier, on the other hand, may be useful.  Its purpose is to
allow code that is to work mostly on \s-1ASCII\s0 data to not have to concern
itself with Unicode.

Briefly, \f(CW\*(C`/l\*(C' sets the character set to that of whatever **L**ocale is in
effect at the time of the execution of the pattern match.

\f(CW\*(C`/u\*(C' sets the character set to **U**nicode.

\f(CW\*(C`/a\*(C' also sets the character set to Unicode, \s-1BUT\s0 adds several
restrictions for **A**SCII-safe matching.

\f(CW\*(C`/d\*(C' is the old, problematic, pre-5.14 **D**efault character set
behavior.  Its only use is to force that old behavior.

At any given time, exactly one of these modifiers is in effect.  Their
existence allows Perl to keep the originally compiled behavior of a
regular expression, regardless of what rules are in effect when it is
actually executed.  And if it is interpolated into a larger regex, the
original's rules continue to apply to it, and don't affect the other
parts.

The \f(CW\*(C`/l\*(C' and \f(CW\*(C`/u\*(C' modifiers are automatically selected for
regular expressions compiled within the scope of various pragmas,
and we recommend that in general, you use those pragmas instead of
specifying these modifiers explicitly.  For one thing, the modifiers
affect only pattern matching, and do not extend to even any replacement
done, whereas using the pragmas gives consistent results for all
appropriate operations within their scopes.  For example,

.Vb 1
 s/foo/\\Ubar/il
.Ve

will match \*(L"foo\*(R" using the locale's rules for case-insensitive matching,
but the \f(CW\*(C`/l\*(C' does not affect how the \f(CW\*(C`\\U\*(C' operates.  Most likely you
want both of them to use locale rules.  To do this, instead compile the
regular expression within the scope of \f(CW\*(C`use locale\*(C'.  This both
implicitly adds the \f(CW\*(C`/l\*(C', and applies locale rules to the \f(CW\*(C`\\U\*(C'.   The
lesson is to \f(CW\*(C`use locale\*(C', and not \f(CW\*(C`/l\*(C' explicitly.

Similarly, it would be better to use \f(CW\*(C`use feature \*(Aqunicode_strings\*(Aq\*(C'
instead of,

.Vb 1
 s/foo/\\Lbar/iu
.Ve

to get Unicode rules, as the \f(CW\*(C`\\L\*(C' in the former (but not necessarily
the latter) would also use Unicode rules.

More detail on each of the modifiers follows.  Most likely you don't
need to know this detail for \f(CW\*(C`/l\*(C', \f(CW\*(C`/u\*(C', and \f(CW\*(C`/d\*(C', and can skip ahead
to /a.

/l
Subsection "/l"

means to use the current locale's rules (see perllocale) when pattern
matching.  For example, \f(CW\*(C`\\w\*(C' will match the \*(L"word\*(R" characters of that
locale, and \f(CW"/i" case-insensitive matching will match according to
the locale's case folding rules.  The locale used will be the one in
effect at the time of execution of the pattern match.  This may not be
the same as the compilation-time locale, and can differ from one match
to another if there is an intervening call of the
**setlocale()** function.

Prior to v5.20, Perl did not support multi-byte locales.  Starting then,
\s-1UTF-8\s0 locales are supported.  No other multi byte locales are ever
likely to be supported.  However, in all locales, one can have code
points above 255 and these will always be treated as Unicode no matter
what locale is in effect.

Under Unicode rules, there are a few case-insensitive matches that cross
the 255/256 boundary.  Except for \s-1UTF-8\s0 locales in Perls v5.20 and
later, these are disallowed under \f(CW\*(C`/l\*(C'.  For example, 0xFF (on \s-1ASCII\s0
platforms) does not caselessly match the character at 0x178, \f(CW\*(C`LATIN
CAPITAL LETTER Y WITH DIAERESIS\*(C', because 0xFF may not be \f(CW\*(C`LATIN SMALL
LETTER Y WITH DIAERESIS\*(C' in the current locale, and Perl has no way of
knowing if that character even exists in the locale, much less what code
point it is.

In a \s-1UTF-8\s0 locale in v5.20 and later, the only visible difference
between locale and non-locale in regular expressions should be tainting
(see perlsec).

This modifier may be specified to be the default by \f(CW\*(C`use locale\*(C', but
see \*(L"Which character set modifier is in effect?\*(R".
Xref " l"

/u
Subsection "/u"

means to use Unicode rules when pattern matching.  On \s-1ASCII\s0 platforms,
this means that the code points between 128 and 255 take on their
Latin-1 (\s-1ISO-8859-1\s0) meanings (which are the same as Unicode's).
(Otherwise Perl considers their meanings to be undefined.)  Thus,
under this modifier, the \s-1ASCII\s0 platform effectively becomes a Unicode
platform; and hence, for example, \f(CW\*(C`\\w\*(C' will match any of the more than
100_000 word characters in Unicode.

Unlike most locales, which are specific to a language and country pair,
Unicode classifies all the characters that are letters *somewhere* in
the world as
\f(CW\*(C`\\w\*(C'.  For example, your locale might not think that \f(CW\*(C`LATIN SMALL
LETTER ETH\*(C' is a letter (unless you happen to speak Icelandic), but
Unicode does.  Similarly, all the characters that are decimal digits
somewhere in the world will match \f(CW\*(C`\\d\*(C'; this is hundreds, not 10,
possible matches.  And some of those digits look like some of the 10
\s-1ASCII\s0 digits, but mean a different number, so a human could easily think
a number is a different quantity than it really is.  For example,
\f(CW\*(C`BENGALI DIGIT FOUR\*(C' (U+09EA) looks very much like an
\f(CW\*(C`ASCII DIGIT EIGHT\*(C' (U+0038), and \f(CW\*(C`LEPCHA DIGIT SIX\*(C' (U+1C46) looks
very much like an \f(CW\*(C`ASCII DIGIT FIVE\*(C' (U+0035).  And, \f(CW\*(C`\\d+\*(C', may match
strings of digits that are a mixture from different writing systems,
creating a security issue.  A fraudulent website, for example, could
display the price of something using U+1C46, and it would appear to the
user that something cost 500 units, but it really costs 600.  A browser
that enforced script runs (\*(L"Script Runs\*(R") would prevent that
fraudulent display.  \*(L"**num()**\*(R" in Unicode::UCD can also be used to sort this
out.  Or the \f(CW\*(C`/a\*(C' modifier can be used to force \f(CW\*(C`\\d\*(C' to match just the
\s-1ASCII 0\s0 through 9.

Also, under this modifier, case-insensitive matching works on the full
set of Unicode
characters.  The \f(CW\*(C`KELVIN SIGN\*(C', for example matches the letters \*(L"k\*(R" and
\*(L"K\*(R"; and \f(CW\*(C`LATIN SMALL LIGATURE FF\*(C' matches the sequence \*(L"ff\*(R", which,
if you're not prepared, might make it look like a hexadecimal constant,
presenting another potential security issue.  See
<https://unicode.org/reports/tr36> for a detailed discussion of Unicode
security issues.

This modifier may be specified to be the default by \f(CW\*(C`use feature
\*(Aqunicode_strings\*(C', \f(CW\*(C`use locale \*(Aq:not_characters\*(Aq\*(C', or
\f(CW\*(C`use 5.012\*(C' (or higher),
but see \*(L"Which character set modifier is in effect?\*(R".
Xref " u"

/d
Subsection "/d"

This modifier means to use the \*(L"Default\*(R" native rules of the platform
except when there is cause to use Unicode rules instead, as follows:

- 1.
the target string is encoded in \s-1UTF-8\s0; or

- 2.
the pattern is encoded in \s-1UTF-8\s0; or

- 3.
the pattern explicitly mentions a code point that is above 255 (say by
\f(CW\*(C`\\x\{100\}\*(C'); or

- 4.
the pattern uses a Unicode name (\f(CW\*(C`\\N\{...\}\*(C');  or

- 5.
the pattern uses a Unicode property (\f(CW\*(C`\\p\{...\}\*(C' or \f(CW\*(C`\\P\{...\}\*(C'); or

- 6.
the pattern uses a Unicode break (\f(CW\*(C`\\b\{...\}\*(C' or \f(CW\*(C`\\B\{...\}\*(C'); or

- 7.
the pattern uses \f(CW"(?[ ])"

- 8.
the pattern uses \f(CW\*(C`(*script_run: ...)\*(C'

Another mnemonic for this modifier is \*(L"Depends\*(R", as the rules actually
used depend on various things, and as a result you can get unexpected
results.  See \*(L"The \*(R"Unicode Bug"" in perlunicode.  The Unicode Bug has
become rather infamous, leading to yet other (without swearing) names
for this modifier, \*(L"Dicey\*(R" and \*(L"Dodgy\*(R".

Unless the pattern or string are encoded in \s-1UTF-8,\s0 only \s-1ASCII\s0 characters
can match positively.

Here are some examples of how that works on an \s-1ASCII\s0 platform:

.Vb 6
 $str =  "\\xDF";      # $str is not in UTF-8 format.
 $str =~ /^\\w/;       # No match, as $str isn\*(Aqt in UTF-8 format.
 $str .= "\\x\{0e0b\}";  # Now $str is in UTF-8 format.
 $str =~ /^\\w/;       # Match! $str is now in UTF-8 format.
 chop $str;
 $str =~ /^\\w/;       # Still a match! $str remains in UTF-8 format.
.Ve

This modifier is automatically selected by default when none of the
others are, so yet another name for it is \*(L"Default\*(R".

Because of the unexpected behaviors associated with this modifier, you
probably should only explicitly use it to maintain weird backward
compatibilities.

/a (and /aa)
Subsection "/a (and /aa)"

This modifier stands for ASCII-restrict (or ASCII-safe).  This modifier
may be doubled-up to increase its effect.

When it appears singly, it causes the sequences \f(CW\*(C`\\d\*(C', \f(CW\*(C`\\s\*(C', \f(CW\*(C`\\w\*(C', and
the Posix character classes to match only in the \s-1ASCII\s0 range.  They thus
revert to their pre-5.6, pre-Unicode meanings.  Under \f(CW\*(C`/a\*(C',  \f(CW\*(C`\\d\*(C'
always means precisely the digits \f(CW"0" to \f(CW"9"; \f(CW\*(C`\\s\*(C' means the five
characters \f(CW\*(C`[ \\f\\n\\r\\t]\*(C', and starting in Perl v5.18, the vertical tab;
\f(CW\*(C`\\w\*(C' means the 63 characters
\f(CW\*(C`[A-Za-z0-9_]\*(C'; and likewise, all the Posix classes such as
\f(CW\*(C`[[:print:]]\*(C' match only the appropriate ASCII-range characters.

This modifier is useful for people who only incidentally use Unicode,
and who do not wish to be burdened with its complexities and security
concerns.

With \f(CW\*(C`/a\*(C', one can write \f(CW\*(C`\\d\*(C' with confidence that it will only match
\s-1ASCII\s0 characters, and should the need arise to match beyond \s-1ASCII,\s0 you
can instead use \f(CW\*(C`\\p\{Digit\}\*(C' (or \f(CW\*(C`\\p\{Word\}\*(C' for \f(CW\*(C`\\w\*(C').  There are
similar \f(CW\*(C`\\p\{...\}\*(C' constructs that can match beyond \s-1ASCII\s0 both white
space (see \*(L"Whitespace\*(R" in perlrecharclass), and Posix classes (see
\*(L"\s-1POSIX\s0 Character Classes\*(R" in perlrecharclass).  Thus, this modifier
doesn't mean you can't use Unicode, it means that to get Unicode
matching you must explicitly use a construct (\f(CW\*(C`\\p\{\}\*(C', \f(CW\*(C`\\P\{\}\*(C') that
signals Unicode.

As you would expect, this modifier causes, for example, \f(CW\*(C`\\D\*(C' to mean
the same thing as \f(CW\*(C`[^0-9]\*(C'; in fact, all non-ASCII characters match
\f(CW\*(C`\\D\*(C', \f(CW\*(C`\\S\*(C', and \f(CW\*(C`\\W\*(C'.  \f(CW\*(C`\\b\*(C' still means to match at the boundary
between \f(CW\*(C`\\w\*(C' and \f(CW\*(C`\\W\*(C', using the \f(CW\*(C`/a\*(C' definitions of them (similarly
for \f(CW\*(C`\\B\*(C').

Otherwise, \f(CW\*(C`/a\*(C' behaves like the \f(CW\*(C`/u\*(C' modifier, in that
case-insensitive matching uses Unicode rules; for example, \*(L"k\*(R" will
match the Unicode \f(CW\*(C`\\N\{KELVIN SIGN\}\*(C' under \f(CW\*(C`/i\*(C' matching, and code
points in the Latin1 range, above \s-1ASCII\s0 will have Unicode rules when it
comes to case-insensitive matching.

To forbid ASCII/non-ASCII matches (like \*(L"k\*(R" with \f(CW\*(C`\\N\{KELVIN SIGN\}\*(C'),
specify the \f(CW"a" twice, for example \f(CW\*(C`/aai\*(C' or \f(CW\*(C`/aia\*(C'.  (The first
occurrence of \f(CW"a" restricts the \f(CW\*(C`\\d\*(C', *etc*., and the second occurrence
adds the \f(CW\*(C`/i\*(C' restrictions.)  But, note that code points outside the
\s-1ASCII\s0 range will use Unicode rules for \f(CW\*(C`/i\*(C' matching, so the modifier
doesn't really restrict things to just \s-1ASCII\s0; it just forbids the
intermixing of \s-1ASCII\s0 and non-ASCII.

To summarize, this modifier provides protection for applications that
don't wish to be exposed to all of Unicode.  Specifying it twice
gives added protection.

This modifier may be specified to be the default by \f(CW\*(C`use re \*(Aq/a\*(Aq\*(C'
or \f(CW\*(C`use re \*(Aq/aa\*(Aq\*(C'.  If you do so, you may actually have occasion to use
the \f(CW\*(C`/u\*(C' modifier explicitly if there are a few regular expressions
where you do want full Unicode rules (but even here, it's best if
everything were under feature \f(CW"unicode_strings", along with the
\f(CW\*(C`use re \*(Aq/aa\*(Aq\*(C').  Also see \*(L"Which character set modifier is in
effect?\*(R".
Xref " a aa"

Which character set modifier is in effect?
Subsection "Which character set modifier is in effect?"

Which of these modifiers is in effect at any given point in a regular
expression depends on a fairly complex set of interactions.  These have
been designed so that in general you don't have to worry about it, but
this section gives the gory details.  As
explained below in \*(L"Extended Patterns\*(R" it is possible to explicitly
specify modifiers that apply only to portions of a regular expression.
The innermost always has priority over any outer ones, and one applying
to the whole expression has priority over any of the default settings that are
described in the remainder of this section.

The \f(CW\*(C`use re \*(Aq/foo\*(Aq\*(C' pragma can be used to set
default modifiers (including these) for regular expressions compiled
within its scope.  This pragma has precedence over the other pragmas
listed below that also change the defaults.

Otherwise, \f(CW\*(C`use locale\*(C' sets the default modifier to \f(CW\*(C`/l\*(C';
and \f(CW\*(C`use feature \*(Aqunicode_strings\*(C', or
\f(CW\*(C`use 5.012\*(C' (or higher) set the default to
\f(CW\*(C`/u\*(C' when not in the same scope as either \f(CW\*(C`use locale\*(C'
or \f(CW\*(C`use bytes\*(C'.
(\f(CW\*(C`use locale \*(Aq:not_characters\*(Aq\*(C' also
sets the default to \f(CW\*(C`/u\*(C', overriding any plain \f(CW\*(C`use locale\*(C'.)
Unlike the mechanisms mentioned above, these
affect operations besides regular expressions pattern matching, and so
give more consistent results with other operators, including using
\f(CW\*(C`\\U\*(C', \f(CW\*(C`\\l\*(C', *etc*. in substitution replacements.

If none of the above apply, for backwards compatibility reasons, the
\f(CW\*(C`/d\*(C' modifier is the one in effect by default.  As this can lead to
unexpected results, it is best to specify which other rule set should be
used.

Character set modifier behavior prior to Perl 5.14
Subsection "Character set modifier behavior prior to Perl 5.14"

Prior to 5.14, there were no explicit modifiers, but \f(CW\*(C`/l\*(C' was implied
for regexes compiled within the scope of \f(CW\*(C`use locale\*(C', and \f(CW\*(C`/d\*(C' was
implied otherwise.  However, interpolating a regex into a larger regex
would ignore the original compilation in favor of whatever was in effect
at the time of the second compilation.  There were a number of
inconsistencies (bugs) with the \f(CW\*(C`/d\*(C' modifier, where Unicode rules
would be used when inappropriate, and vice versa.  \f(CW\*(C`\\p\{\}\*(C' did not imply
Unicode rules, and neither did all occurrences of \f(CW\*(C`\\N\{\}\*(C', until 5.12.

### Regular Expressions

Subsection "Regular Expressions"
*Quantifiers*
Subsection "Quantifiers"

Quantifiers are used when a particular portion of a pattern needs to
match a certain number (or numbers) of times.  If there isn't a
quantifier the number of times to match is exactly one.  The following
standard quantifiers are recognized:
Xref "metacharacter quantifier * + ? \{n\} \{n,\} \{n,m\}"

.Vb 7
    *           Match 0 or more times
    +           Match 1 or more times
    ?           Match 1 or 0 times
    \{n\}         Match exactly n times
    \{n,\}        Match at least n times
    \{,n\}        Match at most n times
    \{n,m\}       Match at least n but not more than m times
.Ve

(If a non-escaped curly bracket occurs in a context other than one of
the quantifiers listed above, where it does not form part of a
backslashed sequence like \f(CW\*(C`\\x\{...\}\*(C', it is either a fatal syntax error,
or treated as a regular character, generally with a deprecation warning
raised.  To escape it, you can precede it with a backslash (\f(CW"\\\{") or
enclose it within square brackets  (\f(CW"[\{]").
This change will allow for future syntax extensions (like making the
lower bound of a quantifier optional), and better error checking of
quantifiers).

The \f(CW"*" quantifier is equivalent to \f(CW\*(C`\{0,\}\*(C', the \f(CW"+"
quantifier to \f(CW\*(C`\{1,\}\*(C', and the \f(CW"?" quantifier to \f(CW\*(C`\{0,1\}\*(C'.  *n* and *m* are limited
to non-negative integral values less than a preset limit defined when perl is built.
This is usually 65534 on the most common platforms.  The actual limit can
be seen in the error message generated by code such as this:

.Vb 1
    $_ **= $_ , / \{$_\} / for 2 .. 42;
.Ve

By default, a quantified subpattern is \*(L"greedy\*(R", that is, it will match as
many times as possible (given a particular starting location) while still
allowing the rest of the pattern to match.  If you want it to match the
minimum number of times possible, follow the quantifier with a \f(CW"?".  Note
that the meanings don't change, just the \*(L"greediness\*(R":
Xref "metacharacter greedy greediness ? *? +? ?? \{n\}? \{n,\}? \{,n\}? \{n,m\}?"

.Vb 7
    *?        Match 0 or more times, not greedily
    +?        Match 1 or more times, not greedily
    ??        Match 0 or 1 time, not greedily
    \{n\}?      Match exactly n times, not greedily (redundant)
    \{n,\}?     Match at least n times, not greedily
    \{,n\}?     Match at most n times, not greedily
    \{n,m\}?    Match at least n but not more than m times, not greedily
.Ve

Normally when a quantified subpattern does not allow the rest of the
overall pattern to match, Perl will backtrack. However, this behaviour is
sometimes undesirable. Thus Perl provides the \*(L"possessive\*(R" quantifier form
as well.

.Vb 7
 *+     Match 0 or more times and give nothing back
 ++     Match 1 or more times and give nothing back
 ?+     Match 0 or 1 time and give nothing back
 \{n\}+   Match exactly n times and give nothing back (redundant)
 \{n,\}+  Match at least n times and give nothing back
 \{,n\}+  Match at most n times and give nothing back
 \{n,m\}+ Match at least n but not more than m times and give nothing back
.Ve

For instance,

.Vb 1
   \*(Aqaaaa\*(Aq =~ /a++a/
.Ve

will never match, as the \f(CW\*(C`a++\*(C' will gobble up all the \f(CW"a"'s in the
string and won't leave any for the remaining part of the pattern. This
feature can be extremely useful to give perl hints about where it
shouldn't backtrack. For instance, the typical \*(L"match a double-quoted
string\*(R" problem can be most efficiently performed when written as:

.Vb 1
   /"(?:[^"\\\\]++|\\\\.)*+"/
.Ve

as we know that if the final quote does not match, backtracking will not
help. See the independent subexpression
\f(CW"(?>\f(CIpattern\f(CW)" for more details;
possessive quantifiers are just syntactic sugar for that construct. For
instance the above example could also be written as follows:

.Vb 1
   /"(?>(?:(?>[^"\\\\]+)|\\\\.)*)"/
.Ve

Note that the possessive quantifier modifier can not be combined
with the non-greedy modifier. This is because it would make no sense.
Consider the follow equivalency table:

.Vb 5
    Illegal         Legal
    ------------    ------
    X??+            X\{0\}
    X+?+            X\{1\}
    X\{min,max\}?+    X\{min\}
.Ve

*Escape sequences*
Subsection "Escape sequences"

Because patterns are processed as double-quoted strings, the following
also work:

.Vb 10
 \\t          tab                   (HT, TAB)
 \\n          newline               (LF, NL)
 \\r          return                (CR)
 \\f          form feed             (FF)
 \\a          alarm (bell)          (BEL)
 \\e          escape (think troff)  (ESC)
 \\cK         control char          (example: VT)
 \\x\{\}, \\x00  character whose ordinal is the given hexadecimal number
 \\N\{name\}    named Unicode character or character sequence
 \\N\{U+263D\}  Unicode character     (example: FIRST QUARTER MOON)
 \\o\{\}, \\000  character whose ordinal is the given octal number
 \\l          lowercase next char (think vi)
 \\u          uppercase next char (think vi)
 \\L          lowercase until \\E (think vi)
 \\U          uppercase until \\E (think vi)
 \\Q          quote (disable) pattern metacharacters until \\E
 \\E          end either case modification or quoted section, think vi
.Ve

Details are in \*(L"Quote and Quote-like Operators\*(R" in perlop.

*Character Classes and other Special Escapes*
Subsection "Character Classes and other Special Escapes"

In addition, Perl defines the following:
Xref "\g \k \K backreference"

.Vb 10
 Sequence   Note    Description
  [...]     [1]  Match a character according to the rules of the
                   bracketed character class defined by the "...".
                   Example: [a-z] matches "a" or "b" or "c" ... or "z"
  [[:...:]] [2]  Match a character according to the rules of the POSIX
                   character class "..." within the outer bracketed
                   character class.  Example: [[:upper:]] matches any
                   uppercase character.
  (?[...])  [8]  Extended bracketed character class
  \\w        [3]  Match a "word" character (alphanumeric plus "_", plus
                   other connector punctuation chars plus Unicode
                   marks)
  \\W        [3]  Match a non-"word" character
  \\s        [3]  Match a whitespace character
  \\S        [3]  Match a non-whitespace character
  \\d        [3]  Match a decimal digit character
  \\D        [3]  Match a non-digit character
  \\pP       [3]  Match P, named property.  Use \\p\{Prop\} for longer names
  \\PP       [3]  Match non-P
  \\X        [4]  Match Unicode "eXtended grapheme cluster"
  \\1        [5]  Backreference to a specific capture group or buffer.
                   \*(Aq1\*(Aq may actually be any positive integer.
  \\g1       [5]  Backreference to a specific or previous group,
  \\g\{-1\}    [5]  The number may be negative indicating a relative
                   previous group and may optionally be wrapped in
                   curly brackets for safer parsing.
  \\g\{name\}  [5]  Named backreference
  \\k<name>  [5]  Named backreference
  \\k\*(Aqname\*(Aq  [5]  Named backreference
  \\k\{name\}  [5]  Named backreference
  \\K        [6]  Keep the stuff left of the \\K, don\*(Aqt include it in $&
  \\N        [7]  Any character but \\n.  Not affected by /s modifier
  \\v        [3]  Vertical whitespace
  \\V        [3]  Not vertical whitespace
  \\h        [3]  Horizontal whitespace
  \\H        [3]  Not horizontal whitespace
  \\R        [4]  Linebreak
.Ve

- [1]
Item "[1]"
See \*(L"Bracketed Character Classes\*(R" in perlrecharclass for details.

- [2]
Item "[2]"
See \*(L"\s-1POSIX\s0 Character Classes\*(R" in perlrecharclass for details.

- [3]
Item "[3]"
See \*(L"Unicode Character Properties\*(R" in perlunicode for details

- [4]
Item "[4]"
See \*(L"Misc\*(R" in perlrebackslash for details.

- [5]
Item "[5]"
See \*(L"Capture groups\*(R" below for details.

- [6]
Item "[6]"
See \*(L"Extended Patterns\*(R" below for details.

- [7]
Item "[7]"
Note that \f(CW\*(C`\\N\*(C' has two meanings.  When of the form \f(CW\*(C`\\N\{\f(CINAME\f(CW\}\*(C', it
matches the character or character sequence whose name is *\s-1NAME\s0*; and
similarly
when of the form \f(CW\*(C`\\N\{U+\f(CIhex\f(CW\}\*(C', it matches the character whose Unicode
code point is *hex*.  Otherwise it matches any character but \f(CW\*(C`\\n\*(C'.

- [8]
Item "[8]"
See \*(L"Extended Bracketed Character Classes\*(R" in perlrecharclass for details.

*Assertions*
Subsection "Assertions"

Besides \f(CW"^" and \f(CW"$", Perl defines the following
zero-width assertions:
Xref "zero-width assertion assertion regex, zero-width assertion regexp, zero-width assertion regular expression, zero-width assertion \b \B \A \Z \z \G"

.Vb 9
 \\b\{\}   Match at Unicode boundary of specified type
 \\B\{\}   Match where corresponding \\b\{\} doesn\*(Aqt match
 \\b     Match a \\w\\W or \\W\\w boundary
 \\B     Match except at a \\w\\W or \\W\\w boundary
 \\A     Match only at beginning of string
 \\Z     Match only at end of string, or before newline at the end
 \\z     Match only at end of string
 \\G     Match only at pos() (e.g. at the end-of-match position
        of prior m//g)
.Ve

A Unicode boundary (\f(CW\*(C`\\b\{\}\*(C'), available starting in v5.22, is a spot
between two characters, or before the first character in the string, or
after the final character in the string where certain criteria defined
by Unicode are met.  See \*(L"\\b\{\}, \\b, \\B\{\}, \\B\*(R" in perlrebackslash for
details.

A word boundary (\f(CW\*(C`\\b\*(C') is a spot between two characters
that has a \f(CW\*(C`\\w\*(C' on one side of it and a \f(CW\*(C`\\W\*(C' on the other side
of it (in either order), counting the imaginary characters off the
beginning and end of the string as matching a \f(CW\*(C`\\W\*(C'.  (Within
character classes \f(CW\*(C`\\b\*(C' represents backspace rather than a word
boundary, just as it normally does in any double-quoted string.)
The \f(CW\*(C`\\A\*(C' and \f(CW\*(C`\\Z\*(C' are just like \f(CW"^" and \f(CW"$", except that they
won't match multiple times when the \f(CW\*(C`/m\*(C' modifier is used, while
\f(CW"^" and \f(CW"$" will match at every internal line boundary.  To match
the actual end of the string and not ignore an optional trailing
newline, use \f(CW\*(C`\\z\*(C'.
Xref "\b \A \Z \z m"

The \f(CW\*(C`\\G\*(C' assertion can be used to chain global matches (using
\f(CW\*(C`m//g\*(C'), as described in \*(L"Regexp Quote-Like Operators\*(R" in perlop.
It is also useful when writing \f(CW\*(C`lex\*(C'-like scanners, when you have
several patterns that you want to match against consequent substrings
of your string; see the previous reference.  The actual location
where \f(CW\*(C`\\G\*(C' will match can also be influenced by using \f(CW\*(C`pos()\*(C' as
an lvalue: see \*(L"pos\*(R" in perlfunc. Note that the rule for zero-length
matches (see \*(L"Repeated Patterns Matching a Zero-length Substring\*(R")
is modified somewhat, in that contents to the left of \f(CW\*(C`\\G\*(C' are
not counted when determining the length of the match. Thus the following
will not match forever:
Xref "\G"

.Vb 5
     my $string = \*(AqABC\*(Aq;
     pos($string) = 1;
     while ($string =~ /(.\\G)/g) \{
         print $1;
     \}
.Ve

It will print 'A' and then terminate, as it considers the match to
be zero-width, and thus will not match at the same position twice in a
row.

It is worth noting that \f(CW\*(C`\\G\*(C' improperly used can result in an infinite
loop. Take care when using patterns that include \f(CW\*(C`\\G\*(C' in an alternation.

Note also that \f(CW\*(C`s///\*(C' will refuse to overwrite part of a substitution
that has already been replaced; so for example this will stop after the
first iteration, rather than iterating its way backwards through the
string:

.Vb 4
    $_ = "123456789";
    pos = 6;
    s/.(?=.\\G)/X/g;
    print;      # prints 1234X6789, not XXXXX6789
.Ve

*Capture groups*
Subsection "Capture groups"

The grouping construct \f(CW\*(C`( ... )\*(C' creates capture groups (also referred to as
capture buffers). To refer to the current contents of a group later on, within
the same pattern, use \f(CW\*(C`\\g1\*(C' (or \f(CW\*(C`\\g\{1\}\*(C') for the first, \f(CW\*(C`\\g2\*(C' (or \f(CW\*(C`\\g\{2\}\*(C')
for the second, and so on.
This is called a *backreference*.








There is no limit to the number of captured substrings that you may use.
Groups are numbered with the leftmost open parenthesis being number 1, *etc*.  If
a group did not match, the associated backreference won't match either. (This
can happen if the group is optional, or in a different branch of an
alternation.)
You can omit the \f(CW"g", and write \f(CW"\\1", *etc*, but there are some issues with
this form, described below.
Xref "regex, capture buffer regexp, capture buffer regex, capture group regexp, capture group regular expression, capture buffer backreference regular expression, capture group backreference \g\{1\} \g\{-1\} \g\{name\} relative backreference named backreference named capture buffer regular expression, named capture buffer named capture group regular expression, named capture group %+ $+\{name\} \k<name>"

You can also refer to capture groups relatively, by using a negative number, so
that \f(CW\*(C`\\g-1\*(C' and \f(CW\*(C`\\g\{-1\}\*(C' both refer to the immediately preceding capture
group, and \f(CW\*(C`\\g-2\*(C' and \f(CW\*(C`\\g\{-2\}\*(C' both refer to the group before it.  For
example:

.Vb 8
        /
         (Y)            # group 1
         (              # group 2
            (X)         # group 3
            \\g\{-1\}      # backref to group 3
            \\g\{-3\}      # backref to group 1
         )
        /x
.Ve

would match the same as \f(CW\*(C`/(Y) ( (X) \\g3 \\g1 )/x\*(C'.  This allows you to
interpolate regexes into larger regexes and not have to worry about the
capture groups being renumbered.

You can dispense with numbers altogether and create named capture groups.
The notation is \f(CW\*(C`(?<\f(CIname\f(CW>...)\*(C' to declare and \f(CW\*(C`\\g\{\f(CIname\f(CW\}\*(C' to
reference.  (To be compatible with .Net regular expressions, \f(CW\*(C`\\g\{\f(CIname\f(CW\}\*(C' may
also be written as \f(CW\*(C`\\k\{\f(CIname\f(CW\}\*(C', \f(CW\*(C`\\k<\f(CIname\f(CW>\*(C' or \f(CW\*(C`\\k\*(Aq\f(CIname\f(CW\*(Aq\*(C'.)
*name* must not begin with a number, nor contain hyphens.
When different groups within the same pattern have the same name, any reference
to that name assumes the leftmost defined group.  Named groups count in
absolute and relative numbering, and so can also be referred to by those
numbers.
(It's possible to do things with named capture groups that would otherwise
require \f(CW\*(C`(??\{\})\*(C'.)

Capture group contents are dynamically scoped and available to you outside the
pattern until the end of the enclosing block or until the next successful
match, whichever comes first.  (See \*(L"Compound Statements\*(R" in perlsyn.)
You can refer to them by absolute number (using \f(CW"$1" instead of \f(CW"\\g1",
*etc*); or by name via the \f(CW\*(C`%+\*(C' hash, using \f(CW"$+\{\f(CIname\f(CW\}".

Braces are required in referring to named capture groups, but are optional for
absolute or relative numbered ones.  Braces are safer when creating a regex by
concatenating smaller strings.  For example if you have \f(CW\*(C`qr/$a$b/\*(C', and \f(CW$a
contained \f(CW"\\g1", and \f(CW$b contained \f(CW"37", you would get \f(CW\*(C`/\\g137/\*(C' which
is probably not what you intended.

If you use braces, you may also optionally add any number of blank
(space or tab) characters within but adjacent to the braces, like
\f(CW\*(C`\\g\{\ -1\ \}\*(C', or \f(CW\*(C`\\k\{\ \f(CIname\f(CW\ \}\*(C'.

The \f(CW\*(C`\\g\*(C' and \f(CW\*(C`\\k\*(C' notations were introduced in Perl 5.10.0.  Prior to that
there were no named nor relative numbered capture groups.  Absolute numbered
groups were referred to using \f(CW\*(C`\\1\*(C',
\f(CW\*(C`\\2\*(C', *etc*., and this notation is still
accepted (and likely always will be).  But it leads to some ambiguities if
there are more than 9 capture groups, as \f(CW\*(C`\\10\*(C' could mean either the tenth
capture group, or the character whose ordinal in octal is 010 (a backspace in
\s-1ASCII\s0).  Perl resolves this ambiguity by interpreting \f(CW\*(C`\\10\*(C' as a backreference
only if at least 10 left parentheses have opened before it.  Likewise \f(CW\*(C`\\11\*(C' is
a backreference only if at least 11 left parentheses have opened before it.
And so on.  \f(CW\*(C`\\1\*(C' through \f(CW\*(C`\\9\*(C' are always interpreted as backreferences.
There are several examples below that illustrate these perils.  You can avoid
the ambiguity by always using \f(CW\*(C`\\g\{\}\*(C' or \f(CW\*(C`\\g\*(C' if you mean capturing groups;
and for octal constants always using \f(CW\*(C`\\o\{\}\*(C', or for \f(CW\*(C`\\077\*(C' and below, using 3
digits padded with leading zeros, since a leading zero implies an octal
constant.

The \f(CW\*(C`\\\f(CIdigit\f(CW\*(C' notation also works in certain circumstances outside
the pattern.  See \*(L"Warning on \\1 Instead of \f(CW$1\*(R" below for details.

Examples:

.Vb 1
    s/^([^ ]*) *([^ ]*)/$2 $1/;     # swap first two words

    /(.)\\g1/                        # find first doubled char
         and print "\*(Aq$1\*(Aq is the first doubled character\\n";

    /(?<char>.)\\k<char>/            # ... a different way
         and print "\*(Aq$+\{char\}\*(Aq is the first doubled character\\n";

    /(?\*(Aqchar\*(Aq.)\\g1/                 # ... mix and match
         and print "\*(Aq$1\*(Aq is the first doubled character\\n";

    if (/Time: (..):(..):(..)/) \{   # parse out values
        $hours = $1;
        $minutes = $2;
        $seconds = $3;
    \}

    /(.)(.)(.)(.)(.)(.)(.)(.)(.)\\g10/   # \\g10 is a backreference
    /(.)(.)(.)(.)(.)(.)(.)(.)(.)\\10/    # \\10 is octal
    /((.)(.)(.)(.)(.)(.)(.)(.)(.))\\10/  # \\10 is a backreference
    /((.)(.)(.)(.)(.)(.)(.)(.)(.))\\010/ # \\010 is octal

    $a = \*(Aq(.)\\1\*(Aq;        # Creates problems when concatenated.
    $b = \*(Aq(.)\\g\{1\}\*(Aq;     # Avoids the problems.
    "aa" =~ /$\{a\}/;      # True
    "aa" =~ /$\{b\}/;      # True
    "aa0" =~ /$\{a\}0/;    # False!
    "aa0" =~ /$\{b\}0/;    # True
    "aa\\x08" =~ /$\{a\}0/;  # True!
    "aa\\x08" =~ /$\{b\}0/;  # False
.Ve

Several special variables also refer back to portions of the previous
match.  \f(CW$+ returns whatever the last bracket match matched.
\f(CW$& returns the entire matched string.  (At one point \f(CW$0 did
also, but now it returns the name of the program.)  \f(CW\*(C`$\`\*(C' returns
everything before the matched string.  \f(CW\*(C`$\*(Aq\*(C' returns everything
after the matched string. And \f(CW$^N contains whatever was matched by
the most-recently closed group (submatch). \f(CW$^N can be used in
extended patterns (see below), for example to assign a submatch to a
variable.
Xref "$+ $^N $& $` $'"

These special variables, like the \f(CW\*(C`%+\*(C' hash and the numbered match variables
(\f(CW$1, \f(CW$2, \f(CW$3, *etc*.) are dynamically scoped
until the end of the enclosing block or until the next successful
match, whichever comes first.  (See \*(L"Compound Statements\*(R" in perlsyn.)
Xref "$+ $^N $& $` $' $1 $2 $3 $4 $5 $6 $7 $8 $9"

**\s-1NOTE\s0**: Failed matches in Perl do not reset the match variables,
which makes it easier to write code that tests for a series of more
specific cases and remembers the best match.

**\s-1WARNING\s0**: If your code is to run on Perl 5.16 or earlier,
beware that once Perl sees that you need one of \f(CW$&, \f(CW\*(C`$\`\*(C', or
\f(CW\*(C`$\*(Aq\*(C' anywhere in the program, it has to provide them for every
pattern match.  This may substantially slow your program.

Perl uses the same mechanism to produce \f(CW$1, \f(CW$2, *etc*, so you also
pay a price for each pattern that contains capturing parentheses.
(To avoid this cost while retaining the grouping behaviour, use the
extended regular expression \f(CW\*(C`(?: ... )\*(C' instead.)  But if you never
use \f(CW$&, \f(CW\*(C`$\`\*(C' or \f(CW\*(C`$\*(Aq\*(C', then patterns *without* capturing
parentheses will not be penalized.  So avoid \f(CW$&, \f(CW\*(C`$\*(Aq\*(C', and \f(CW\*(C`$\`\*(C'
if you can, but if you can't (and some algorithms really appreciate
them), once you've used them once, use them at will, because you've
already paid the price.
Xref "$& $` $'"

Perl 5.16 introduced a slightly more efficient mechanism that notes
separately whether each of \f(CW\*(C`$\`\*(C', \f(CW$&, and \f(CW\*(C`$\*(Aq\*(C' have been seen, and
thus may only need to copy part of the string.  Perl 5.20 introduced a
much more efficient copy-on-write mechanism which eliminates any slowdown.

As another workaround for this problem, Perl 5.10.0 introduced \f(CW\*(C`$\{^PREMATCH\}\*(C',
\f(CW\*(C`$\{^MATCH\}\*(C' and \f(CW\*(C`$\{^POSTMATCH\}\*(C', which are equivalent to \f(CW\*(C`$\`\*(C', \f(CW$&
and \f(CW\*(C`$\*(Aq\*(C', **except** that they are only guaranteed to be defined after a
successful match that was executed with the \f(CW\*(C`/p\*(C' (preserve) modifier.
The use of these variables incurs no global performance penalty, unlike
their punctuation character equivalents, however at the trade-off that you
have to tell perl when you want to use them.  As of Perl 5.20, these three
variables are equivalent to \f(CW\*(C`$\`\*(C', \f(CW$& and \f(CW\*(C`$\*(Aq\*(C', and \f(CW\*(C`/p\*(C' is ignored.
Xref " p p modifier"

### Quoting metacharacters

Subsection "Quoting metacharacters"
Backslashed metacharacters in Perl are alphanumeric, such as \f(CW\*(C`\\b\*(C',
\f(CW\*(C`\\w\*(C', \f(CW\*(C`\\n\*(C'.  Unlike some other regular expression languages, there
are no backslashed symbols that aren't alphanumeric.  So anything
that looks like \f(CW\*(C`\\\\\*(C', \f(CW\*(C`\\(\*(C', \f(CW\*(C`\\)\*(C', \f(CW\*(C`\\[\*(C', \f(CW\*(C`\\]\*(C', \f(CW\*(C`\\\{\*(C', or \f(CW\*(C`\\\}\*(C' is
always
interpreted as a literal character, not a metacharacter.  This was
once used in a common idiom to disable or quote the special meanings
of regular expression metacharacters in a string that you want to
use for a pattern. Simply quote all non-\*(L"word\*(R" characters:

.Vb 1
    $pattern =~ s/(\\W)/\\\\$1/g;
.Ve

(If \f(CW\*(C`use locale\*(C' is set, then this depends on the current locale.)
Today it is more common to use the \f(CW\*(C`quotemeta()\*(C'
function or the \f(CW\*(C`\\Q\*(C' metaquoting escape sequence to disable all
metacharacters' special meanings like this:

.Vb 1
    /$unquoted\\Q$quoted\\E$unquoted/
.Ve

Beware that if you put literal backslashes (those not inside
interpolated variables) between \f(CW\*(C`\\Q\*(C' and \f(CW\*(C`\\E\*(C', double-quotish
backslash interpolation may lead to confusing results.  If you
*need* to use literal backslashes within \f(CW\*(C`\\Q...\\E\*(C',
consult \*(L"Gory details of parsing quoted constructs\*(R" in perlop.

\f(CW\*(C`quotemeta()\*(C' and \f(CW\*(C`\\Q\*(C' are fully described in \*(L"quotemeta\*(R" in perlfunc.

### Extended Patterns

Subsection "Extended Patterns"
Perl also defines a consistent extension syntax for features not
found in standard tools like **awk** and
**lex**.  The syntax for most of these is a
pair of parentheses with a question mark as the first thing within
the parentheses.  The character after the question mark indicates
the extension.

A question mark was chosen for this and for the minimal-matching
construct because 1) question marks are rare in older regular
expressions, and 2) whenever you see one, you should stop and
\*(L"question\*(R" exactly what is going on.  That's psychology....
.ie n .IP """(?#*text*)""" 4
.el .IP "\f(CW(?#\f(CItext\f(CW)" 4
Xref "(?#)"
Item "(?#text)"
A comment.  The *text* is ignored.
Note that Perl closes
the comment as soon as it sees a \f(CW")", so there is no way to put a literal
\f(CW")" in the comment.  The pattern's closing delimiter must be escaped by
a backslash if it appears in the comment.
.Sp
See \*(L"/x\*(R" for another way to have comments in patterns.
.Sp
Note that a comment can go just about anywhere, except in the middle of
an escape sequence.   Examples:
.Sp
.Vb 1
 qr/foo(?#comment)bar/\*(Aq  # Matches \*(Aqfoobar\*(Aq

 # The pattern below matches \*(Aqabcd\*(Aq, \*(Aqabccd\*(Aq, or \*(Aqabcccd\*(Aq
 qr/abc(?#comment between literal and its quantifier)\{1,3\}d/

 # The pattern below generates a syntax error, because the \*(Aq\\p\*(Aq must
 # be followed immediately by a \*(Aq\{\*(Aq.
 qr/\\p(?#comment between \\p and its property name)\{Any\}/

 # The pattern below generates a syntax error, because the initial
 # \*(Aq\\(\*(Aq is a literal opening parenthesis, and so there is nothing
 # for the  closing \*(Aq)\*(Aq to match
 qr/\\(?#the backslash means this isn\*(Aqt a comment)p\{Any\}/

 # Comments can be used to fold long patterns into multiple lines
 qr/First part of a long regex(?#
   )remaining part/
.Ve
.ie n .IP """(?adlupimnsx-imnsx)""" 4
.el .IP "\f(CW(?adlupimnsx-imnsx)" 4
Item "(?adlupimnsx-imnsx)"
0
.ie n .IP """(?^alupimnsx)""" 4
.el .IP "\f(CW(?^alupimnsx)" 4
Xref "(?) (?^)"
Item "(?^alupimnsx)"
.PD
Zero or more embedded pattern-match modifiers, to be turned on (or
turned off if preceded by \f(CW"-") for the remainder of the pattern or
the remainder of the enclosing pattern group (if any).
.Sp
This is particularly useful for dynamically-generated patterns,
such as those read in from a
configuration file, taken from an argument, or specified in a table
somewhere.  Consider the case where some patterns want to be
case-sensitive and some do not:  The case-insensitive ones merely need to
include \f(CW\*(C`(?i)\*(C' at the front of the pattern.  For example:
.Sp
.Vb 2
    $pattern = "foobar";
    if ( /$pattern/i ) \{ \}

    # more flexible:

    $pattern = "(?i)foobar";
    if ( /$pattern/ ) \{ \}
.Ve
.Sp
These modifiers are restored at the end of the enclosing group. For example,
.Sp
.Vb 1
    ( (?i) blah ) \\s+ \\g1
.Ve
.Sp
will match \f(CW\*(C`blah\*(C' in any case, some spaces, and an exact (*including the case*!)
repetition of the previous word, assuming the \f(CW\*(C`/x\*(C' modifier, and no \f(CW\*(C`/i\*(C'
modifier outside this group.
.Sp
These modifiers do not carry over into named subpatterns called in the
enclosing group. In other words, a pattern such as \f(CW\*(C`((?i)(?&\f(CINAME\f(CW))\*(C' does not
change the case-sensitivity of the *\s-1NAME\s0* pattern.
.Sp
A modifier is overridden by later occurrences of this construct in the
same scope containing the same modifier, so that
.Sp
.Vb 1
    /((?im)foo(?-m)bar)/
.Ve
.Sp
matches all of \f(CW\*(C`foobar\*(C' case insensitively, but uses \f(CW\*(C`/m\*(C' rules for
only the \f(CW\*(C`foo\*(C' portion.  The \f(CW"a" flag overrides \f(CW\*(C`aa\*(C' as well;
likewise \f(CW\*(C`aa\*(C' overrides \f(CW"a".  The same goes for \f(CW"x" and \f(CW\*(C`xx\*(C'.
Hence, in
.Sp
.Vb 1
    /(?-x)foo/xx
.Ve
.Sp
both \f(CW\*(C`/x\*(C' and \f(CW\*(C`/xx\*(C' are turned off during matching \f(CW\*(C`foo\*(C'.  And in
.Sp
.Vb 1
    /(?x)foo/x
.Ve
.Sp
\f(CW\*(C`/x\*(C' but \s-1NOT\s0 \f(CW\*(C`/xx\*(C' is turned on for matching \f(CW\*(C`foo\*(C'.  (One might
mistakenly think that since the inner \f(CW\*(C`(?x)\*(C' is already in the scope of
\f(CW\*(C`/x\*(C', that the result would effectively be the sum of them, yielding
\f(CW\*(C`/xx\*(C'.  It doesn't work that way.)  Similarly, doing something like
\f(CW\*(C`(?xx-x)foo\*(C' turns off all \f(CW"x" behavior for matching \f(CW\*(C`foo\*(C', it is not
that you subtract 1 \f(CW"x" from 2 to get 1 \f(CW"x" remaining.
.Sp
Any of these modifiers can be set to apply globally to all regular
expressions compiled within the scope of a \f(CW\*(C`use re\*(C'.  See
\*(L"'/flags' mode\*(R" in re.
.Sp
Starting in Perl 5.14, a \f(CW"^" (caret or circumflex accent) immediately
after the \f(CW"?" is a shorthand equivalent to \f(CW\*(C`d-imnsx\*(C'.  Flags (except
\f(CW"d") may follow the caret to override it.
But a minus sign is not legal with it.
.Sp
Note that the \f(CW"a", \f(CW"d", \f(CW"l", \f(CW"p", and \f(CW"u" modifiers are special in
that they can only be enabled, not disabled, and the \f(CW"a", \f(CW"d", \f(CW"l", and
\f(CW"u" modifiers are mutually exclusive: specifying one de-specifies the
others, and a maximum of one (or two \f(CW"a"'s) may appear in the
construct.  Thus, for
example, \f(CW\*(C`(?-p)\*(C' will warn when compiled under \f(CW\*(C`use warnings\*(C';
\f(CW\*(C`(?-d:...)\*(C' and \f(CW\*(C`(?dl:...)\*(C' are fatal errors.
.Sp
Note also that the \f(CW"p" modifier is special in that its presence
anywhere in a pattern has a global effect.
.Sp
Having zero modifiers makes this a no-op (so why did you specify it,
unless it's generated code), and starting in v5.30, warns under \f(CW\*(C`use
re \*(Aqstrict\*(Aq\*(C'.
.ie n .IP """(?:*pattern*)""" 4
.el .IP "\f(CW(?:\f(CIpattern\f(CW)" 4
Xref "(?:)"
Item "(?:pattern)"
0
.ie n .IP """(?adluimnsx-imnsx:*pattern*)""" 4
.el .IP "\f(CW(?adluimnsx-imnsx:\f(CIpattern\f(CW)" 4
Item "(?adluimnsx-imnsx:pattern)"
.ie n .IP """(?^aluimnsx:*pattern*)""" 4
.el .IP "\f(CW(?^aluimnsx:\f(CIpattern\f(CW)" 4
Xref "(?^:)"
Item "(?^aluimnsx:pattern)"
.PD
This is for clustering, not capturing; it groups subexpressions like
\f(CW"()", but doesn't make backreferences as \f(CW"()" does.  So
.Sp
.Vb 1
    @fields = split(/\\b(?:a|b|c)\\b/)
.Ve
.Sp
matches the same field delimiters as
.Sp
.Vb 1
    @fields = split(/\\b(a|b|c)\\b/)
.Ve
.Sp
but doesn't spit out the delimiters themselves as extra fields (even though
that's the behaviour of \*(L"split\*(R" in perlfunc when its pattern contains capturing
groups).  It's also cheaper not to capture
characters if you don't need to.
.Sp
Any letters between \f(CW"?" and \f(CW":" act as flags modifiers as with
\f(CW\*(C`(?adluimnsx-imnsx)\*(C'.  For example,
.Sp
.Vb 1
    /(?s-i:more.*than).*million/i
.Ve
.Sp
is equivalent to the more verbose
.Sp
.Vb 1
    /(?:(?s-i)more.*than).*million/i
.Ve
.Sp
Note that any \f(CW\*(C`()\*(C' constructs enclosed within this one will still
capture unless the \f(CW\*(C`/n\*(C' modifier is in effect.
.Sp
Like the \*(L"(?adlupimnsx-imnsx)\*(R" construct, \f(CW\*(C`aa\*(C' and \f(CW"a" override each
other, as do \f(CW\*(C`xx\*(C' and \f(CW"x".  They are not additive.  So, doing
something like \f(CW\*(C`(?xx-x:foo)\*(C' turns off all \f(CW"x" behavior for matching
\f(CW\*(C`foo\*(C'.
.Sp
Starting in Perl 5.14, a \f(CW"^" (caret or circumflex accent) immediately
after the \f(CW"?" is a shorthand equivalent to \f(CW\*(C`d-imnsx\*(C'.  Any positive
flags (except \f(CW"d") may follow the caret, so
.Sp
.Vb 1
    (?^x:foo)
.Ve
.Sp
is equivalent to
.Sp
.Vb 1
    (?x-imns:foo)
.Ve
.Sp
The caret tells Perl that this cluster doesn't inherit the flags of any
surrounding pattern, but uses the system defaults (\f(CW\*(C`d-imnsx\*(C'),
modified by any flags specified.
.Sp
The caret allows for simpler stringification of compiled regular
expressions.  These look like
.Sp
.Vb 1
    (?^:pattern)
.Ve
.Sp
with any non-default flags appearing between the caret and the colon.
A test that looks at such stringification thus doesn't need to have the
system default flags hard-coded in it, just the caret.  If new flags are
added to Perl, the meaning of the caret's expansion will change to include
the default for those flags, so the test will still work, unchanged.
.Sp
Specifying a negative flag after the caret is an error, as the flag is
redundant.
.Sp
Mnemonic for \f(CW\*(C`(?^...)\*(C':  A fresh beginning since the usual use of a caret is
to match at the beginning.
.ie n .IP """(?|*pattern*)""" 4
.el .IP "\f(CW(?|\f(CIpattern\f(CW)" 4
Xref "(?|) Branch reset"
Item "(?|pattern)"
This is the \*(L"branch reset\*(R" pattern, which has the special property
that the capture groups are numbered from the same starting point
in each alternation branch. It is available starting from perl 5.10.0.
.Sp
Capture groups are numbered from left to right, but inside this
construct the numbering is restarted for each branch.
.Sp
The numbering within each branch will be as normal, and any groups
following this construct will be numbered as though the construct
contained only one branch, that being the one with the most capture
groups in it.
.Sp
This construct is useful when you want to capture one of a
number of alternative matches.
.Sp
Consider the following pattern.  The numbers underneath show in
which group the captured content will be stored.
.Sp
.Vb 3
    # before  ---------------branch-reset----------- after
    / ( a )  (?| x ( y ) z | (p (q) r) | (t) u (v) ) ( z ) /x
    # 1            2         2  3        2     3     4
.Ve
.Sp
Be careful when using the branch reset pattern in combination with
named captures. Named captures are implemented as being aliases to
numbered groups holding the captures, and that interferes with the
implementation of the branch reset pattern. If you are using named
captures in a branch reset pattern, it's best to use the same names,
in the same order, in each of the alternations:
.Sp
.Vb 2
   /(?|  (?<a> x ) (?<b> y )
      |  (?<a> z ) (?<b> w )) /x
.Ve
.Sp
Not doing so may lead to surprises:
.Sp
.Vb 3
  "12" =~ /(?| (?<a> \\d+ ) | (?<b> \\D+))/x;
  say $+\{a\};    # Prints \*(Aq12\*(Aq
  say $+\{b\};    # *Also* prints \*(Aq12\*(Aq.
.Ve
.Sp
The problem here is that both the group named \f(CW\*(C`a\*(C' and the group
named \f(CW\*(C`b\*(C' are aliases for the group belonging to \f(CW$1.

- Lookaround Assertions
Xref "look-around assertion lookaround assertion look-around lookaround"
Item "Lookaround Assertions"
Lookaround assertions are zero-width patterns which match a specific
pattern without including it in \f(CW$&. Positive assertions match when
their subpattern matches, negative assertions match when their subpattern
fails. Lookbehind matches text up to the current match position,
lookahead matches text following the current match position.

> .ie n .IP """(?=*pattern*)""" 4
.el .IP "\f(CW(?=\f(CIpattern\f(CW)" 4
Item "(?=pattern)"
0
.ie n .IP """(*pla:*pattern*)""" 4
.el .IP "\f(CW(*pla:\f(CIpattern\f(CW)" 4
Item "(*pla:pattern)"
.ie n .IP """(*positive_lookahead:*pattern*)""" 4
.el .IP "\f(CW(*positive_lookahead:\f(CIpattern\f(CW)" 4
Xref "(?=) (*pla (*positive_lookahead look-ahead, positive lookahead, positive"
Item "(*positive_lookahead:pattern)"
.PD
A zero-width positive lookahead assertion.  For example, \f(CW\*(C`/\\w+(?=\\t)/\*(C'
matches a word followed by a tab, without including the tab in \f(CW$&.
.ie n .IP """(?!*pattern*)""" 4
.el .IP "\f(CW(?!\f(CIpattern\f(CW)" 4
Item "(?!pattern)"
0
.ie n .IP """(*nla:*pattern*)""" 4
.el .IP "\f(CW(*nla:\f(CIpattern\f(CW)" 4
Item "(*nla:pattern)"
.ie n .IP """(*negative_lookahead:*pattern*)""" 4
.el .IP "\f(CW(*negative_lookahead:\f(CIpattern\f(CW)" 4
Xref "(?!) (*nla (*negative_lookahead look-ahead, negative lookahead, negative"
Item "(*negative_lookahead:pattern)"
.PD
A zero-width negative lookahead assertion.  For example \f(CW\*(C`/foo(?!bar)/\*(C'
matches any occurrence of \*(L"foo\*(R" that isn't followed by \*(L"bar\*(R".  Note
however that lookahead and lookbehind are \s-1NOT\s0 the same thing.  You cannot
use this for lookbehind.
.Sp
If you are looking for a \*(L"bar\*(R" that isn't preceded by a \*(L"foo\*(R", \f(CW\*(C`/(?!foo)bar/\*(C'
will not do what you want.  That's because the \f(CW\*(C`(?!foo)\*(C' is just saying that
the next thing cannot be \*(L"foo\*(R"--and it's not, it's a \*(L"bar\*(R", so \*(L"foobar\*(R" will
match.  Use lookbehind instead (see below).
.ie n .IP """(?<=*pattern*)""" 4
.el .IP "\f(CW(?<=\f(CIpattern\f(CW)" 4
Item "(?<=pattern)"
0
.ie n .IP """\\K""" 4
.el .IP "\f(CW\\K" 4
Item "K"
.ie n .IP """(*plb:*pattern*)""" 4
.el .IP "\f(CW(*plb:\f(CIpattern\f(CW)" 4
Item "(*plb:pattern)"
.ie n .IP """(*positive_lookbehind:*pattern*)""" 4
.el .IP "\f(CW(*positive_lookbehind:\f(CIpattern\f(CW)" 4
Xref "(?<=) (*plb (*positive_lookbehind look-behind, positive lookbehind, positive \K"
Item "(*positive_lookbehind:pattern)"
.PD
A zero-width positive lookbehind assertion.  For example, \f(CW\*(C`/(?<=\\t)\\w+/\*(C'
matches a word that follows a tab, without including the tab in \f(CW$&.
.Sp
Prior to Perl 5.30, it worked only for fixed-width lookbehind, but
starting in that release, it can handle variable lengths from 1 to 255
characters as an experimental feature.  The feature is enabled
automatically if you use a variable length lookbehind assertion, but
will raise a warning at pattern compilation time, unless turned off, in
the \f(CW\*(C`experimental::vlb\*(C' category.  This is to warn you that the exact
behavior is subject to change should feedback from actual use in the
field indicate to do so; or even complete removal if the problems found
are not practically surmountable.  You can achieve close to pre-5.30
behavior by fatalizing warnings in this category.
.Sp
There is a special form of this construct, called \f(CW\*(C`\\K\*(C'
(available since Perl 5.10.0), which causes the
regex engine to \*(L"keep\*(R" everything it had matched prior to the \f(CW\*(C`\\K\*(C' and
not include it in \f(CW$&. This effectively provides non-experimental
variable-length lookbehind of any length.
.Sp
And, there is a technique that can be used to handle variable length
lookbehinds on earlier releases, and longer than 255 characters.  It is
described in
<http://www.drregex.com/2019/02/variable-length-lookbehinds-actually.html>.
.Sp
Note that under \f(CW\*(C`/i\*(C', a few single characters match two or three other
characters.  This makes them variable length, and the 255 length applies
to the maximum number of characters in the match.  For
example \f(CW\*(C`qr/\\N\{LATIN SMALL LETTER SHARP S\}/i\*(C' matches the sequence
\f(CW"ss".  Your lookbehind assertion could contain 127 Sharp S
characters under \f(CW\*(C`/i\*(C', but adding a 128th would generate a compilation
error, as that could match 256 \f(CW"s" characters in a row.
.Sp
The use of \f(CW\*(C`\\K\*(C' inside of another lookaround assertion
is allowed, but the behaviour is currently not well defined.
.Sp
For various reasons \f(CW\*(C`\\K\*(C' may be significantly more efficient than the
equivalent \f(CW\*(C`(?<=...)\*(C' construct, and it is especially useful in
situations where you want to efficiently remove something following
something else in a string. For instance
.Sp
.Vb 1
  s/(foo)bar/$1/g;
.Ve
.Sp
can be rewritten as the much more efficient
.Sp
.Vb 1
  s/foo\\Kbar//g;
.Ve
.Sp
Use of the non-greedy modifier \f(CW"?" may not give you the expected
results if it is within a capturing group within the construct.
.ie n .IP """(?<!*pattern*)""" 4
.el .IP "\f(CW(?<!\f(CIpattern\f(CW)" 4
Item "(?<!pattern)"
0
.ie n .IP """(*nlb:*pattern*)""" 4
.el .IP "\f(CW(*nlb:\f(CIpattern\f(CW)" 4
Item "(*nlb:pattern)"
.ie n .IP """(*negative_lookbehind:*pattern*)""" 4
.el .IP "\f(CW(*negative_lookbehind:\f(CIpattern\f(CW)" 4
Xref "(?<!) (*nlb (*negative_lookbehind look-behind, negative lookbehind, negative"
Item "(*negative_lookbehind:pattern)"
.PD
A zero-width negative lookbehind assertion.  For example \f(CW\*(C`/(?<!bar)foo/\*(C'
matches any occurrence of \*(L"foo\*(R" that does not follow \*(L"bar\*(R".
.Sp
Prior to Perl 5.30, it worked only for fixed-width lookbehind, but
starting in that release, it can handle variable lengths from 1 to 255
characters as an experimental feature.  The feature is enabled
automatically if you use a variable length lookbehind assertion, but
will raise a warning at pattern compilation time, unless turned off, in
the \f(CW\*(C`experimental::vlb\*(C' category.  This is to warn you that the exact
behavior is subject to change should feedback from actual use in the
field indicate to do so; or even complete removal if the problems found
are not practically surmountable.  You can achieve close to pre-5.30
behavior by fatalizing warnings in this category.
.Sp
There is a technique that can be used to handle variable length
lookbehinds on earlier releases, and longer than 255 characters.  It is
described in
<http://www.drregex.com/2019/02/variable-length-lookbehinds-actually.html>.
.Sp
Note that under \f(CW\*(C`/i\*(C', a few single characters match two or three other
characters.  This makes them variable length, and the 255 length applies
to the maximum number of characters in the match.  For
example \f(CW\*(C`qr/\\N\{LATIN SMALL LETTER SHARP S\}/i\*(C' matches the sequence
\f(CW"ss".  Your lookbehind assertion could contain 127 Sharp S
characters under \f(CW\*(C`/i\*(C', but adding a 128th would generate a compilation
error, as that could match 256 \f(CW"s" characters in a row.
.Sp
Use of the non-greedy modifier \f(CW"?" may not give you the expected
results if it is within a capturing group within the construct.



> 

.ie n .IP """(?<*NAME*>*pattern*)""" 4
.el .IP "\f(CW(?<\f(CINAME\f(CW>\f(CIpattern\f(CW)" 4
Item "(?<NAME>pattern)"
0
.ie n .IP """(?\*(Aq*NAME*\*(Aq*pattern*)""" 4
.el .IP "\f(CW(?\*(Aq\f(CINAME\f(CW\*(Aq\f(CIpattern\f(CW)" 4
Xref "(?<NAME>) (?'NAME') named capture capture"
Item "(?NAMEpattern)"
.PD
A named capture group. Identical in every respect to normal capturing
parentheses \f(CW\*(C`()\*(C' but for the additional fact that the group
can be referred to by name in various regular expression
constructs (like \f(CW\*(C`\\g\{\f(CINAME\f(CW\}\*(C') and can be accessed by name
after a successful match via \f(CW\*(C`%+\*(C' or \f(CW\*(C`%-\*(C'. See perlvar
for more details on the \f(CW\*(C`%+\*(C' and \f(CW\*(C`%-\*(C' hashes.
.Sp
If multiple distinct capture groups have the same name, then
\f(CW$+\{\f(CINAME\f(CW\} will refer to the leftmost defined group in the match.
.Sp
The forms \f(CW\*(C`(?\*(Aq\f(CINAME\f(CW\*(Aq\f(CIpattern\f(CW)\*(C' and \f(CW\*(C`(?<\f(CINAME\f(CW>\f(CIpattern\f(CW)\*(C'
are equivalent.
.Sp
**\s-1NOTE:\s0** While the notation of this construct is the same as the similar
function in .NET regexes, the behavior is not. In Perl the groups are
numbered sequentially regardless of being named or not. Thus in the
pattern
.Sp
.Vb 1
  /(x)(?<foo>y)(z)/
.Ve
.Sp
\f(CW$+\{foo\} will be the same as \f(CW$2, and \f(CW$3 will contain 'z' instead of
the opposite which is what a .NET regex hacker might expect.
.Sp
Currently *\s-1NAME\s0* is restricted to simple identifiers only.
In other words, it must match \f(CW\*(C`/^[_A-Za-z][_A-Za-z0-9]*\\z/\*(C' or
its Unicode extension (see utf8),
though it isn't extended by the locale (see perllocale).
.Sp
**\s-1NOTE:\s0** In order to make things easier for programmers with experience
with the Python or \s-1PCRE\s0 regex engines, the pattern \f(CW\*(C`(?P<\f(CINAME\f(CW>\f(CIpattern\f(CW)\*(C'
may be used instead of \f(CW\*(C`(?<\f(CINAME\f(CW>\f(CIpattern\f(CW)\*(C'; however this form does not
support the use of single quotes as a delimiter for the name.
.ie n .IP """\\k<*NAME*>""" 4
.el .IP "\f(CW\\k<\f(CINAME\f(CW>" 4
Item "k<NAME>"
0
.ie n .IP """\\k\*(Aq*NAME*\*(Aq""" 4
.el .IP "\f(CW\\k\*(Aq\f(CINAME\f(CW\*(Aq" 4
Item "kNAME"
.ie n .IP """\\k\{*NAME*\}""" 4
.el .IP "\f(CW\\k\{\f(CINAME\f(CW\}" 4
Item "k\{NAME\}"
.PD
Named backreference. Similar to numeric backreferences, except that
the group is designated by name and not number. If multiple groups
have the same name then it refers to the leftmost defined group in
the current match.
.Sp
It is an error to refer to a name not defined by a \f(CW\*(C`(?<\f(CINAME\f(CW>)\*(C'
earlier in the pattern.
.Sp
All three forms are equivalent, although with \f(CW\*(C`\\k\{ \f(CINAME\f(CW \}\*(C',
you may optionally have blanks within but adjacent to the braces, as
shown.
.Sp
**\s-1NOTE:\s0** In order to make things easier for programmers with experience
with the Python or \s-1PCRE\s0 regex engines, the pattern \f(CW\*(C`(?P=\f(CINAME\f(CW)\*(C'
may be used instead of \f(CW\*(C`\\k<\f(CINAME\f(CW>\*(C'.
.ie n .IP """(?\{ *code* \})""" 4
.el .IP "\f(CW(?\{ \f(CIcode\f(CW \})" 4
Xref "(?\{\}) regex, code in regexp, code in regular expression, code in"
Item "(?\{ code \})"
**\s-1WARNING\s0**: Using this feature safely requires that you understand its
limitations.  Code executed that has side effects may not perform identically
from version to version due to the effect of future optimisations in the regex
engine.  For more information on this, see \*(L"Embedded Code Execution
Frequency\*(R".
.Sp
This zero-width assertion executes any embedded Perl code.  It always
succeeds, and its return value is set as \f(CW$^R.
.Sp
In literal patterns, the code is parsed at the same time as the
surrounding code. While within the pattern, control is passed temporarily
back to the perl parser, until the logically-balancing closing brace is
encountered. This is similar to the way that an array index expression in
a literal string is handled, for example
.Sp
.Vb 1
    "abc$array[ 1 + f(\*(Aq[\*(Aq) + g()]def"
.Ve
.Sp
In particular, braces do not need to be balanced:
.Sp
.Vb 1
    s/abc(?\{ f(\*(Aq\{\*(Aq); \})/def/
.Ve
.Sp
Even in a pattern that is interpolated and compiled at run-time, literal
code blocks will be compiled once, at perl compile time; the following
prints \*(L"\s-1ABCD\*(R":\s0
.Sp
.Vb 5
    print "D";
    my $qr = qr/(?\{ BEGIN \{ print "A" \} \})/;
    my $foo = "foo";
    /$foo$qr(?\{ BEGIN \{ print "B" \} \})/;
    BEGIN \{ print "C" \}
.Ve
.Sp
In patterns where the text of the code is derived from run-time
information rather than appearing literally in a source code /pattern/,
the code is compiled at the same time that the pattern is compiled, and
for reasons of security, \f(CW\*(C`use re \*(Aqeval\*(Aq\*(C' must be in scope. This is to
stop user-supplied patterns containing code snippets from being
executable.
.Sp
In situations where you need to enable this with \f(CW\*(C`use re \*(Aqeval\*(Aq\*(C', you should
also have taint checking enabled.  Better yet, use the carefully
constrained evaluation within a Safe compartment.  See perlsec for
details about both these mechanisms.
.Sp
From the viewpoint of parsing, lexical variable scope and closures,
.Sp
.Vb 1
    /AAA(?\{ BBB \})CCC/
.Ve
.Sp
behaves approximately like
.Sp
.Vb 1
    /AAA/ && do \{ BBB \} && /CCC/
.Ve
.Sp
Similarly,
.Sp
.Vb 1
    qr/AAA(?\{ BBB \})CCC/
.Ve
.Sp
behaves approximately like
.Sp
.Vb 1
    sub \{ /AAA/ && do \{ BBB \} && /CCC/ \}
.Ve
.Sp
In particular:
.Sp
.Vb 3
    \{ my $i = 1; $r = qr/(?\{ print $i \})/ \}
    my $i = 2;
    /$r/; # prints "1"
.Ve
.Sp
Inside a \f(CW\*(C`(?\{...\})\*(C' block, \f(CW$_ refers to the string the regular
expression is matching against. You can also use \f(CW\*(C`pos()\*(C' to know what is
the current position of matching within this string.
.Sp
The code block introduces a new scope from the perspective of lexical
variable declarations, but **not** from the perspective of \f(CW\*(C`local\*(C' and
similar localizing behaviours. So later code blocks within the same
pattern will still see the values which were localized in earlier blocks.
These accumulated localizations are undone either at the end of a
successful match, or if the assertion is backtracked (compare
\*(L"Backtracking\*(R"). For example,
.Sp
.Vb 10
  $_ = \*(Aqa\*(Aq x 8;
  m<
     (?\{ $cnt = 0 \})               # Initialize $cnt.
     (
       a
       (?\{
           local $cnt = $cnt + 1;  # Update $cnt,
                                   # backtracking-safe.
       \})
     )*
     aaaa
     (?\{ $res = $cnt \})            # On success copy to
                                   # non-localized location.
   >x;
.Ve
.Sp
will initially increment \f(CW$cnt up to 8; then during backtracking, its
value will be unwound back to 4, which is the value assigned to \f(CW$res.
At the end of the regex execution, \f(CW$cnt will be wound back to its initial
value of 0.
.Sp
This assertion may be used as the condition in a
.Sp
.Vb 1
    (?(condition)yes-pattern|no-pattern)
.Ve
.Sp
switch.  If *not* used in this way, the result of evaluation of *code*
is put into the special variable \f(CW$^R.  This happens immediately, so
\f(CW$^R can be used from other \f(CW\*(C`(?\{ \f(CIcode\f(CW \})\*(C' assertions inside the same
regular expression.
.Sp
The assignment to \f(CW$^R above is properly localized, so the old
value of \f(CW$^R is restored if the assertion is backtracked; compare
\*(L"Backtracking\*(R".
.Sp
Note that the special variable \f(CW$^N  is particularly useful with code
blocks to capture the results of submatches in variables without having to
keep track of the number of nested parentheses. For example:
.Sp
.Vb 3
  $_ = "The brown fox jumps over the lazy dog";
  /the (\\S+)(?\{ $color = $^N \}) (\\S+)(?\{ $animal = $^N \})/i;
  print "color = $color, animal = $animal\\n";
.Ve
.ie n .IP """(??\{ *code* \})""" 4
.el .IP "\f(CW(??\{ \f(CIcode\f(CW \})" 4
Xref "(??\{\}) regex, postponed regexp, postponed regular expression, postponed"
Item "(??\{ code \})"
**\s-1WARNING\s0**: Using this feature safely requires that you understand its
limitations.  Code executed that has side effects may not perform
identically from version to version due to the effect of future
optimisations in the regex engine.  For more information on this, see
\*(L"Embedded Code Execution Frequency\*(R".
.Sp
This is a \*(L"postponed\*(R" regular subexpression.  It behaves in *exactly* the
same way as a \f(CW\*(C`(?\{ \f(CIcode\f(CW \})\*(C' code block as described above, except that
its return value, rather than being assigned to \f(CW$^R, is treated as a
pattern, compiled if it's a string (or used as-is if its a qr// object),
then matched as if it were inserted instead of this construct.
.Sp
During the matching of this sub-pattern, it has its own set of
captures which are valid during the sub-match, but are discarded once
control returns to the main pattern. For example, the following matches,
with the inner pattern capturing \*(L"B\*(R" and matching \*(L"\s-1BB\*(R",\s0 while the outer
pattern captures \*(L"A\*(R";
.Sp
.Vb 3
    my $inner = \*(Aq(.)\\1\*(Aq;
    "ABBA" =~ /^(.)(??\{ $inner \})\\1/;
    print $1; # prints "A";
.Ve
.Sp
Note that this means that  there is no way for the inner pattern to refer
to a capture group defined outside.  (The code block itself can use \f(CW$1,
*etc*., to refer to the enclosing pattern's capture groups.)  Thus, although
.Sp
.Vb 1
    (\*(Aqa\*(Aq x 100)=~/(??\{\*(Aq(.)\*(Aq x 100\})/
.Ve
.Sp
*will* match, it will *not* set \f(CW$1 on exit.
.Sp
The following pattern matches a parenthesized group:
.Sp
.Vb 9
 $re = qr\{
            \\(
            (?:
               (?> [^()]+ )  # Non-parens without backtracking
             |
               (??\{ $re \})   # Group with matching parens
            )*
            \\)
         \}x;
.Ve
.Sp
See also
\f(CW\*(C`(?\f(CIPARNO\f(CW)\*(C'
for a different, more efficient way to accomplish
the same task.
.Sp
Executing a postponed regular expression too many times without
consuming any input string will also result in a fatal error.  The depth
at which that happens is compiled into perl, so it can be changed with a
custom build.
.ie n .IP """(?*PARNO*)"" ""(?-*PARNO*)"" ""(?+*PARNO*)"" ""(?R)"" ""(?0)""" 4
.el .IP "\f(CW(?\f(CIPARNO\f(CW) \f(CW(?-\f(CIPARNO\f(CW) \f(CW(?+\f(CIPARNO\f(CW) \f(CW(?R) \f(CW(?0)" 4
Xref "(?PARNO) (?1) (?R) (?0) (?-1) (?+1) (?-PARNO) (?+PARNO) regex, recursive regexp, recursive regular expression, recursive regex, relative recursion GOSUB GOSTART"
Item "(?PARNO) (?-PARNO) (?+PARNO) (?R) (?0)"
Recursive subpattern. Treat the contents of a given capture buffer in the
current pattern as an independent subpattern and attempt to match it at
the current position in the string. Information about capture state from
the caller for things like backreferences is available to the subpattern,
but capture buffers set by the subpattern are not visible to the caller.
.Sp
Similar to \f(CW\*(C`(??\{ \f(CIcode\f(CW \})\*(C' except that it does not involve executing any
code or potentially compiling a returned pattern string; instead it treats
the part of the current pattern contained within a specified capture group
as an independent pattern that must match at the current position. Also
different is the treatment of capture buffers, unlike \f(CW\*(C`(??\{ \f(CIcode\f(CW \})\*(C'
recursive patterns have access to their caller's match state, so one can
use backreferences safely.
.Sp
*\s-1PARNO\s0* is a sequence of digits (not starting with 0) whose value reflects
the paren-number of the capture group to recurse to. \f(CW\*(C`(?R)\*(C' recurses to
the beginning of the whole pattern. \f(CW\*(C`(?0)\*(C' is an alternate syntax for
\f(CW\*(C`(?R)\*(C'. If *\s-1PARNO\s0* is preceded by a plus or minus sign then it is assumed
to be relative, with negative numbers indicating preceding capture groups
and positive ones following. Thus \f(CW\*(C`(?-1)\*(C' refers to the most recently
declared group, and \f(CW\*(C`(?+1)\*(C' indicates the next group to be declared.
Note that the counting for relative recursion differs from that of
relative backreferences, in that with recursion unclosed groups **are**
included.
.Sp
The following pattern matches a function \f(CW\*(C`foo()\*(C' which may contain
balanced parentheses as the argument.
.Sp
.Vb 10
  $re = qr\{ (                   # paren group 1 (full function)
              foo
              (                 # paren group 2 (parens)
                \\(
                  (             # paren group 3 (contents of parens)
                  (?:
                   (?> [^()]+ ) # Non-parens without backtracking
                  |
                   (?2)         # Recurse to start of paren group 2
                  )*
                  )
                \\)
              )
            )
          \}x;
.Ve
.Sp
If the pattern was used as follows
.Sp
.Vb 4
    \*(Aqfoo(bar(baz)+baz(bop))\*(Aq=~/$re/
        and print "\\$1 = $1\\n",
                  "\\$2 = $2\\n",
                  "\\$3 = $3\\n";
.Ve
.Sp
the output produced should be the following:
.Sp
.Vb 3
    $1 = foo(bar(baz)+baz(bop))
    $2 = (bar(baz)+baz(bop))
    $3 = bar(baz)+baz(bop)
.Ve
.Sp
If there is no corresponding capture group defined, then it is a
fatal error.  Recursing deeply without consuming any input string will
also result in a fatal error.  The depth at which that happens is
compiled into perl, so it can be changed with a custom build.
.Sp
The following shows how using negative indexing can make it
easier to embed recursive patterns inside of a \f(CW\*(C`qr//\*(C' construct
for later use:
.Sp
.Vb 4
    my $parens = qr/(\\((?:[^()]++|(?-1))*+\\))/;
    if (/foo $parens \\s+ \\+ \\s+ bar $parens/x) \{
       # do something here...
    \}
.Ve
.Sp
**Note** that this pattern does not behave the same way as the equivalent
\s-1PCRE\s0 or Python construct of the same form. In Perl you can backtrack into
a recursed group, in \s-1PCRE\s0 and Python the recursed into group is treated
as atomic. Also, modifiers are resolved at compile time, so constructs
like \f(CW\*(C`(?i:(?1))\*(C' or \f(CW\*(C`(?:(?i)(?1))\*(C' do not affect how the sub-pattern will
be processed.
.ie n .IP """(?&*NAME*)""" 4
.el .IP "\f(CW(?&\f(CINAME\f(CW)" 4
Xref "(?&NAME)"
Item "(?&NAME)"
Recurse to a named subpattern. Identical to \f(CW\*(C`(?\f(CIPARNO\f(CW)\*(C' except that the
parenthesis to recurse to is determined by name. If multiple parentheses have
the same name, then it recurses to the leftmost.
.Sp
It is an error to refer to a name that is not declared somewhere in the
pattern.
.Sp
**\s-1NOTE:\s0** In order to make things easier for programmers with experience
with the Python or \s-1PCRE\s0 regex engines the pattern \f(CW\*(C`(?P>\f(CINAME\f(CW)\*(C'
may be used instead of \f(CW\*(C`(?&\f(CINAME\f(CW)\*(C'.
.ie n .IP """(?(*condition*)*yes-pattern*|*no-pattern*)""" 4
.el .IP "\f(CW(?(\f(CIcondition\f(CW)\f(CIyes-pattern\f(CW|\f(CIno-pattern\f(CW)" 4
Xref "(?()"
Item "(?(condition)yes-pattern|no-pattern)"
0
.ie n .IP """(?(*condition*)*yes-pattern*)""" 4
.el .IP "\f(CW(?(\f(CIcondition\f(CW)\f(CIyes-pattern\f(CW)" 4
Item "(?(condition)yes-pattern)"
.PD
Conditional expression. Matches *yes-pattern* if *condition* yields
a true value, matches *no-pattern* otherwise. A missing pattern always
matches.
.Sp
\f(CW\*(C`(\f(CIcondition\f(CW)\*(C' should be one of:

> 
- an integer in parentheses
Item "an integer in parentheses"
(which is valid if the corresponding pair of parentheses
matched);

- a lookahead/lookbehind/evaluate zero-width assertion;
Item "a lookahead/lookbehind/evaluate zero-width assertion;"
0

- a name in angle brackets or single quotes
Item "a name in angle brackets or single quotes"
.PD
(which is valid if a group with the given name matched);
.ie n .IP "the special symbol ""(R)""" 4
.el .IP "the special symbol \f(CW(R)" 4
Item "the special symbol (R)"
(true when evaluated inside of recursion or eval).  Additionally the
\f(CW"R" may be
followed by a number, (which will be true when evaluated when recursing
inside of the appropriate group), or by \f(CW\*(C`&\f(CINAME\f(CW\*(C', in which case it will
be true only when evaluated during recursion in the named group.



> .Sp
Here's a summary of the possible predicates:
.ie n .IP """(1)"" ""(2)"" ..." 4
.el .IP "\f(CW(1) \f(CW(2) ..." 4
Item "(1) (2) ..."
Checks if the numbered capturing group has matched something.
Full syntax: \f(CW\*(C`(?(1)then|else)\*(C'
.ie n .IP """(<*NAME*>)"" ""(\*(Aq*NAME*\*(Aq)""" 4
.el .IP "\f(CW(<\f(CINAME\f(CW>) \f(CW(\*(Aq\f(CINAME\f(CW\*(Aq)" 4
Item "(<NAME>) (NAME)"
Checks if a group with the given name has matched something.
Full syntax: \f(CW\*(C`(?(<name>)then|else)\*(C'
.ie n .IP """(?=...)"" ""(?!...)"" ""(?<=...)"" ""(?<!...)""" 4
.el .IP "\f(CW(?=...) \f(CW(?!...) \f(CW(?<=...) \f(CW(?<!...)" 4
Item "(?=...) (?!...) (?<=...) (?<!...)"
Checks whether the pattern matches (or does not match, for the \f(CW"!"
variants).
Full syntax: \f(CW\*(C`(?(?=\f(CIlookahead\f(CW)\f(CIthen\f(CW|\f(CIelse\f(CW)\*(C'
.ie n .IP """(?\{ *CODE* \})""" 4
.el .IP "\f(CW(?\{ \f(CICODE\f(CW \})" 4
Item "(?\{ CODE \})"
Treats the return value of the code block as the condition.
Full syntax: \f(CW\*(C`(?(?\{ \f(CIcode\f(CW \})\f(CIthen\f(CW|\f(CIelse\f(CW)\*(C'
.ie n .IP """(R)""" 4
.el .IP "\f(CW(R)" 4
Item "(R)"
Checks if the expression has been evaluated inside of recursion.
Full syntax: \f(CW\*(C`(?(R)\f(CIthen\f(CW|\f(CIelse\f(CW)\*(C'
.ie n .IP """(R1)"" ""(R2)"" ..." 4
.el .IP "\f(CW(R1) \f(CW(R2) ..." 4
Item "(R1) (R2) ..."
Checks if the expression has been evaluated while executing directly
inside of the n-th capture group. This check is the regex equivalent of
.Sp
.Vb 1
  if ((caller(0))[3] eq \*(Aqsubname\*(Aq) \{ ... \}
.Ve
.Sp
In other words, it does not check the full recursion stack.
.Sp
Full syntax: \f(CW\*(C`(?(R1)\f(CIthen\f(CW|\f(CIelse\f(CW)\*(C'
.ie n .IP """(R&*NAME*)""" 4
.el .IP "\f(CW(R&\f(CINAME\f(CW)" 4
Item "(R&NAME)"
Similar to \f(CW\*(C`(R1)\*(C', this predicate checks to see if we're executing
directly inside of the leftmost group with a given name (this is the same
logic used by \f(CW\*(C`(?&\f(CINAME\f(CW)\*(C' to disambiguate). It does not check the full
stack, but only the name of the innermost active recursion.
Full syntax: \f(CW\*(C`(?(R&\f(CIname\f(CW)\f(CIthen\f(CW|\f(CIelse\f(CW)\*(C'
.ie n .IP """(DEFINE)""" 4
.el .IP "\f(CW(DEFINE)" 4
Item "(DEFINE)"
In this case, the yes-pattern is never directly executed, and no
no-pattern is allowed. Similar in spirit to \f(CW\*(C`(?\{0\})\*(C' but more efficient.
See below for details.
Full syntax: \f(CW\*(C`(?(DEFINE)\f(CIdefinitions\f(CW...)\*(C'



> .Sp
For example:
.Sp
.Vb 4
    m\{ ( \\( )?
       [^()]+
       (?(1) \\) )
     \}x
.Ve
.Sp
matches a chunk of non-parentheses, possibly included in parentheses
themselves.
.Sp
A special form is the \f(CW\*(C`(DEFINE)\*(C' predicate, which never executes its
yes-pattern directly, and does not allow a no-pattern. This allows one to
define subpatterns which will be executed only by the recursion mechanism.
This way, you can define a set of regular expression rules that can be
bundled into any pattern you choose.
.Sp
It is recommended that for this usage you put the \s-1DEFINE\s0 block at the
end of the pattern, and that you name any subpatterns defined within it.
.Sp
Also, it's worth noting that patterns defined this way probably will
not be as efficient, as the optimizer is not very clever about
handling them.
.Sp
An example of how this might be used is as follows:
.Sp
.Vb 5
  /(?<NAME>(?&NAME_PAT))(?<ADDR>(?&ADDRESS_PAT))
   (?(DEFINE)
     (?<NAME_PAT>....)
     (?<ADDRESS_PAT>....)
   )/x
.Ve
.Sp
Note that capture groups matched inside of recursion are not accessible
after the recursion returns, so the extra layer of capturing groups is
necessary. Thus \f(CW$+\{NAME_PAT\} would not be defined even though
\f(CW$+\{NAME\} would be.
.Sp
Finally, keep in mind that subpatterns created inside a \s-1DEFINE\s0 block
count towards the absolute and relative number of captures, so this:
.Sp
.Vb 5
    my @captures = "a" =~ /(.)                  # First capture
                           (?(DEFINE)
                               (?<EXAMPLE> 1 )  # Second capture
                           )/x;
    say scalar @captures;
.Ve
.Sp
Will output 2, not 1. This is particularly important if you intend to
compile the definitions with the \f(CW\*(C`qr//\*(C' operator, and later
interpolate them in another pattern.


.ie n .IP """(?>*pattern*)""" 4
.el .IP "\f(CW(?>\f(CIpattern\f(CW)" 4
Item "(?>pattern)"
0
.ie n .IP """(*atomic:*pattern*)""" 4
.el .IP "\f(CW(*atomic:\f(CIpattern\f(CW)" 4
Xref "(?>pattern) (*atomic backtrack backtracking atomic possessive"
Item "(*atomic:pattern)"
.PD
An \*(L"independent\*(R" subexpression, one which matches the substring
that a standalone *pattern* would match if anchored at the given
position, and it matches *nothing other than this substring*.  This
construct is useful for optimizations of what would otherwise be
\*(L"eternal\*(R" matches, because it will not backtrack (see \*(L"Backtracking\*(R").
It may also be useful in places where the \*(L"grab all you can, and do not
give anything back\*(R" semantic is desirable.
.Sp
For example: \f(CW\*(C`^(?>a*)ab\*(C' will never match, since \f(CW\*(C`(?>a*)\*(C'
(anchored at the beginning of string, as above) will match *all*
characters \f(CW"a" at the beginning of string, leaving no \f(CW"a" for
\f(CW\*(C`ab\*(C' to match.  In contrast, \f(CW\*(C`a*ab\*(C' will match the same as \f(CW\*(C`a+b\*(C',
since the match of the subgroup \f(CW\*(C`a*\*(C' is influenced by the following
group \f(CW\*(C`ab\*(C' (see \*(L"Backtracking\*(R").  In particular, \f(CW\*(C`a*\*(C' inside
\f(CW\*(C`a*ab\*(C' will match fewer characters than a standalone \f(CW\*(C`a*\*(C', since
this makes the tail match.
.Sp
\f(CW\*(C`(?>\f(CIpattern\f(CW)\*(C' does not disable backtracking altogether once it has
matched. It is still possible to backtrack past the construct, but not
into it. So \f(CW\*(C`((?>a*)|(?>b*))ar\*(C' will still match \*(L"bar\*(R".
.Sp
An effect similar to \f(CW\*(C`(?>\f(CIpattern\f(CW)\*(C' may be achieved by writing
\f(CW\*(C`(?=(\f(CIpattern\f(CW))\\g\{-1\}\*(C'.  This matches the same substring as a standalone
\f(CW\*(C`a+\*(C', and the following \f(CW\*(C`\\g\{-1\}\*(C' eats the matched string; it therefore
makes a zero-length assertion into an analogue of \f(CW\*(C`(?>...)\*(C'.
(The difference between these two constructs is that the second one
uses a capturing group, thus shifting ordinals of backreferences
in the rest of a regular expression.)
.Sp
Consider this pattern:
.Sp
.Vb 8
    m\{ \\(
          (
            [^()]+           # x+
          |
            \\( [^()]* \\)
          )+
       \\)
     \}x
.Ve
.Sp
That will efficiently match a nonempty group with matching parentheses
two levels deep or less.  However, if there is no such group, it
will take virtually forever on a long string.  That's because there
are so many different ways to split a long string into several
substrings.  This is what \f(CW\*(C`(.+)+\*(C' is doing, and \f(CW\*(C`(.+)+\*(C' is similar
to a subpattern of the above pattern.  Consider how the pattern
above detects no-match on \f(CW\*(C`((()aaaaaaaaaaaaaaaaaa\*(C' in several
seconds, but that each extra letter doubles this time.  This
exponential performance will make it appear that your program has
hung.  However, a tiny change to this pattern
.Sp
.Vb 8
    m\{ \\(
          (
            (?> [^()]+ )        # change x+ above to (?> x+ )
          |
            \\( [^()]* \\)
          )+
       \\)
     \}x
.Ve
.Sp
which uses \f(CW\*(C`(?>...)\*(C' matches exactly when the one above does (verifying
this yourself would be a productive exercise), but finishes in a fourth
the time when used on a similar string with 1000000 \f(CW"a"s.  Be aware,
however, that, when this construct is followed by a
quantifier, it currently triggers a warning message under
the \f(CW\*(C`use warnings\*(C' pragma or **-w** switch saying it
\f(CW"matches null string many times in regex".
.Sp
On simple groups, such as the pattern \f(CW\*(C`(?> [^()]+ )\*(C', a comparable
effect may be achieved by negative lookahead, as in \f(CW\*(C`[^()]+ (?! [^()] )\*(C'.
This was only 4 times slower on a string with 1000000 \f(CW"a"s.
.Sp
The \*(L"grab all you can, and do not give anything back\*(R" semantic is desirable
in many situations where on the first sight a simple \f(CW\*(C`()*\*(C' looks like
the correct solution.  Suppose we parse text with comments being delimited
by \f(CW"#" followed by some optional (horizontal) whitespace.  Contrary to
its appearance, \f(CW\*(C`#[ \\t]*\*(C' *is not* the correct subexpression to match
the comment delimiter, because it may \*(L"give up\*(R" some whitespace if
the remainder of the pattern can be made to match that way.  The correct
answer is either one of these:
.Sp
.Vb 2
    (?>#[ \\t]*)
    #[ \\t]*(?![ \\t])
.Ve
.Sp
For example, to grab non-empty comments into \f(CW$1, one should use either
one of these:
.Sp
.Vb 2
    / (?> \\# [ \\t]* ) (        .+ ) /x;
    /     \\# [ \\t]*   ( [^ \\t] .* ) /x;
.Ve
.Sp
Which one you pick depends on which of these expressions better reflects
the above specification of comments.
.Sp
In some literature this construct is called \*(L"atomic matching\*(R" or
\*(L"possessive matching\*(R".
.Sp
Possessive quantifiers are equivalent to putting the item they are applied
to inside of one of these constructs. The following equivalences apply:
.Sp
.Vb 6
    Quantifier Form     Bracketing Form
    ---------------     ---------------
    PAT*+               (?>PAT*)
    PAT++               (?>PAT+)
    PAT?+               (?>PAT?)
    PAT\{min,max\}+       (?>PAT\{min,max\})
.Ve
.Sp
Nested \f(CW\*(C`(?>...)\*(C' constructs are not no-ops, even if at first glance
they might seem to be.  This is because the nested \f(CW\*(C`(?>...)\*(C' can
restrict internal backtracking that otherwise might occur.  For example,
.Sp
.Vb 1
 "abc" =~ /(?>a[bc]*c)/
.Ve
.Sp
matches, but
.Sp
.Vb 1
 "abc" =~ /(?>a(?>[bc]*)c)/
.Ve
.Sp
does not.
.ie n .IP """(?[ ])""" 4
.el .IP "\f(CW(?[ ])" 4
Item "(?[ ])"
See \*(L"Extended Bracketed Character Classes\*(R" in perlrecharclass.
.Sp
Note that this feature is currently experimental;
using it yields a warning in the \f(CW\*(C`experimental::regex_sets\*(C' category.

### Backtracking

Xref "backtrack backtracking"
Subsection "Backtracking"
\s-1NOTE:\s0 This section presents an abstract approximation of regular
expression behavior.  For a more rigorous (and complicated) view of
the rules involved in selecting a match among possible alternatives,
see \*(L"Combining \s-1RE\s0 Pieces\*(R".

A fundamental feature of regular expression matching involves the
notion called *backtracking*, which is currently used (when needed)
by all regular non-possessive expression quantifiers, namely \f(CW"*",
\f(CW\*(C`*?\*(C', \f(CW"+", \f(CW\*(C`+?\*(C', \f(CW\*(C`\{n,m\}\*(C', and \f(CW\*(C`\{n,m\}?\*(C'.  Backtracking is often
optimized internally, but the general principle outlined here is valid.

For a regular expression to match, the *entire* regular expression must
match, not just part of it.  So if the beginning of a pattern containing a
quantifier succeeds in a way that causes later parts in the pattern to
fail, the matching engine backs up and recalculates the beginning
part\*(--that's why it's called backtracking.

Here is an example of backtracking:  Let's say you want to find the
word following \*(L"foo\*(R" in the string \*(L"Food is on the foo table.\*(R":

.Vb 4
    $_ = "Food is on the foo table.";
    if ( /\\b(foo)\\s+(\\w+)/i ) \{
        print "$2 follows $1.\\n";
    \}
.Ve

When the match runs, the first part of the regular expression (\f(CW\*(C`\\b(foo)\*(C')
finds a possible match right at the beginning of the string, and loads up
\f(CW$1 with \*(L"Foo\*(R".  However, as soon as the matching engine sees that there's
no whitespace following the \*(L"Foo\*(R" that it had saved in \f(CW$1, it realizes its
mistake and starts over again one character after where it had the
tentative match.  This time it goes all the way until the next occurrence
of \*(L"foo\*(R". The complete regular expression matches this time, and you get
the expected output of \*(L"table follows foo.\*(R"

Sometimes minimal matching can help a lot.  Imagine you'd like to match
everything between \*(L"foo\*(R" and \*(L"bar\*(R".  Initially, you write something
like this:

.Vb 4
    $_ =  "The food is under the bar in the barn.";
    if ( /foo(.*)bar/ ) \{
        print "got <$1>\\n";
    \}
.Ve

Which perhaps unexpectedly yields:

.Vb 1
  got <d is under the bar in the >
.Ve

That's because \f(CW\*(C`.*\*(C' was greedy, so you get everything between the
*first* \*(L"foo\*(R" and the *last* \*(L"bar\*(R".  Here it's more effective
to use minimal matching to make sure you get the text between a \*(L"foo\*(R"
and the first \*(L"bar\*(R" thereafter.

.Vb 2
    if ( /foo(.*?)bar/ ) \{ print "got <$1>\\n" \}
  got <d is under the >
.Ve

Here's another example. Let's say you'd like to match a number at the end
of a string, and you also want to keep the preceding part of the match.
So you write this:

.Vb 4
    $_ = "I have 2 numbers: 53147";
    if ( /(.*)(\\d*)/ ) \{                                # Wrong!
        print "Beginning is <$1>, number is <$2>.\\n";
    \}
.Ve

That won't work at all, because \f(CW\*(C`.*\*(C' was greedy and gobbled up the
whole string. As \f(CW\*(C`\\d*\*(C' can match on an empty string the complete
regular expression matched successfully.

.Vb 1
    Beginning is <I have 2 numbers: 53147>, number is <>.
.Ve

Here are some variants, most of which don't work:

.Vb 11
    $_ = "I have 2 numbers: 53147";
    @pats = qw\{
        (.*)(\\d*)
        (.*)(\\d+)
        (.*?)(\\d*)
        (.*?)(\\d+)
        (.*)(\\d+)$
        (.*?)(\\d+)$
        (.*)\\b(\\d+)$
        (.*\\D)(\\d+)$
    \};

    for $pat (@pats) \{
        printf "%-12s ", $pat;
        if ( /$pat/ ) \{
            print "<$1> <$2>\\n";
        \} else \{
            print "FAIL\\n";
        \}
    \}
.Ve

That will print out:

.Vb 8
    (.*)(\\d*)    <I have 2 numbers: 53147> <>
    (.*)(\\d+)    <I have 2 numbers: 5314> <7>
    (.*?)(\\d*)   <> <>
    (.*?)(\\d+)   <I have > <2>
    (.*)(\\d+)$   <I have 2 numbers: 5314> <7>
    (.*?)(\\d+)$  <I have 2 numbers: > <53147>
    (.*)\\b(\\d+)$ <I have 2 numbers: > <53147>
    (.*\\D)(\\d+)$ <I have 2 numbers: > <53147>
.Ve

As you see, this can be a bit tricky.  It's important to realize that a
regular expression is merely a set of assertions that gives a definition
of success.  There may be 0, 1, or several different ways that the
definition might succeed against a particular string.  And if there are
multiple ways it might succeed, you need to understand backtracking to
know which variety of success you will achieve.

When using lookahead assertions and negations, this can all get even
trickier.  Imagine you'd like to find a sequence of non-digits not
followed by \*(L"123\*(R".  You might try to write that as

.Vb 4
    $_ = "ABC123";
    if ( /^\\D*(?!123)/ ) \{                # Wrong!
        print "Yup, no 123 in $_\\n";
    \}
.Ve

But that isn't going to match; at least, not the way you're hoping.  It
claims that there is no 123 in the string.  Here's a clearer picture of
why that pattern matches, contrary to popular expectations:

.Vb 2
    $x = \*(AqABC123\*(Aq;
    $y = \*(AqABC445\*(Aq;

    print "1: got $1\\n" if $x =~ /^(ABC)(?!123)/;
    print "2: got $1\\n" if $y =~ /^(ABC)(?!123)/;

    print "3: got $1\\n" if $x =~ /^(\\D*)(?!123)/;
    print "4: got $1\\n" if $y =~ /^(\\D*)(?!123)/;
.Ve

This prints

.Vb 3
    2: got ABC
    3: got AB
    4: got ABC
.Ve

You might have expected test 3 to fail because it seems to a more
general purpose version of test 1.  The important difference between
them is that test 3 contains a quantifier (\f(CW\*(C`\\D*\*(C') and so can use
backtracking, whereas test 1 will not.  What's happening is
that you've asked "Is it true that at the start of \f(CW$x, following 0 or more
non-digits, you have something that's not 123?"  If the pattern matcher had
let \f(CW\*(C`\\D*\*(C' expand to \*(L"\s-1ABC\*(R",\s0 this would have caused the whole pattern to
fail.

The search engine will initially match \f(CW\*(C`\\D*\*(C' with \*(L"\s-1ABC\*(R".\s0  Then it will
try to match \f(CW\*(C`(?!123)\*(C' with \*(L"123\*(R", which fails.  But because
a quantifier (\f(CW\*(C`\\D*\*(C') has been used in the regular expression, the
search engine can backtrack and retry the match differently
in the hope of matching the complete regular expression.

The pattern really, *really* wants to succeed, so it uses the
standard pattern back-off-and-retry and lets \f(CW\*(C`\\D*\*(C' expand to just \*(L"\s-1AB\*(R"\s0 this
time.  Now there's indeed something following \*(L"\s-1AB\*(R"\s0 that is not
\*(L"123\*(R".  It's \*(L"C123\*(R", which suffices.

We can deal with this by using both an assertion and a negation.
We'll say that the first part in \f(CW$1 must be followed both by a digit
and by something that's not \*(L"123\*(R".  Remember that the lookaheads
are zero-width expressions\*(--they only look, but don't consume any
of the string in their match.  So rewriting this way produces what
you'd expect; that is, case 5 will fail, but case 6 succeeds:

.Vb 2
    print "5: got $1\\n" if $x =~ /^(\\D*)(?=\\d)(?!123)/;
    print "6: got $1\\n" if $y =~ /^(\\D*)(?=\\d)(?!123)/;

    6: got ABC
.Ve

In other words, the two zero-width assertions next to each other work as though
they're ANDed together, just as you'd use any built-in assertions:  \f(CW\*(C`/^$/\*(C'
matches only if you're at the beginning of the line \s-1AND\s0 the end of the
line simultaneously.  The deeper underlying truth is that juxtaposition in
regular expressions always means \s-1AND,\s0 except when you write an explicit \s-1OR\s0
using the vertical bar.  \f(CW\*(C`/ab/\*(C' means match \*(L"a\*(R" \s-1AND\s0 (then) match \*(L"b\*(R",
although the attempted matches are made at different positions because \*(L"a\*(R"
is not a zero-width assertion, but a one-width assertion.

**\s-1WARNING\s0**: Particularly complicated regular expressions can take
exponential time to solve because of the immense number of possible
ways they can use backtracking to try for a match.  For example, without
internal optimizations done by the regular expression engine, this will
take a painfully long time to run:

.Vb 1
    \*(Aqaaaaaaaaaaaa\*(Aq =~ /((a\{0,5\})\{0,5\})*[c]/
.Ve

And if you used \f(CW"*"'s in the internal groups instead of limiting them
to 0 through 5 matches, then it would take forever\*(--or until you ran
out of stack space.  Moreover, these internal optimizations are not
always applicable.  For example, if you put \f(CW\*(C`\{0,5\}\*(C' instead of \f(CW"*"
on the external group, no current optimization is applicable, and the
match takes a long time to finish.

A powerful tool for optimizing such beasts is what is known as an
\*(L"independent group\*(R",
which does not backtrack (see \f(CW"(?>pattern)").  Note also that
zero-length lookahead/lookbehind assertions will not backtrack to make
the tail match, since they are in \*(L"logical\*(R" context: only
whether they match is considered relevant.  For an example
where side-effects of lookahead *might* have influenced the
following match, see \f(CW"(?>pattern)".

### Script Runs

Xref "(*script_run:...) (sr:...) (*atomic_script_run:...) (asr:...)"
Subsection "Script Runs"
A script run is basically a sequence of characters, all from the same
Unicode script (see \*(L"Scripts\*(R" in perlunicode), such as Latin or Greek.  In
most places a single word would never be written in multiple scripts,
unless it is a spoofing attack.  An infamous example, is

.Vb 1
 paypal.com
.Ve

Those letters could all be Latin (as in the example just above), or they
could be all Cyrillic (except for the dot), or they could be a mixture
of the two.  In the case of an internet address the \f(CW\*(C`.com\*(C' would be in
Latin, And any Cyrillic ones would cause it to be a mixture, not a
script run.  Someone clicking on such a link would not be directed to
the real Paypal website, but an attacker would craft a look-alike one to
attempt to gather sensitive information from the person.

Starting in Perl 5.28, it is now easy to detect strings that aren't
script runs.  Simply enclose just about any pattern like either of
these:

.Vb 2
 (*script_run:pattern)
 (*sr:pattern)
.Ve

What happens is that after *pattern* succeeds in matching, it is
subjected to the additional criterion that every character in it must be
from the same script (see exceptions below).  If this isn't true,
backtracking occurs until something all in the same script is found that
matches, or all possibilities are exhausted.  This can cause a lot of
backtracking, but generally, only malicious input will result in this,
though the slow down could cause a denial of service attack.  If your
needs permit, it is best to make the pattern atomic to cut down on the
amount of backtracking.  This is so likely to be what you want, that
instead of writing this:

.Vb 1
 (*script_run:(?>pattern))
.Ve

you can write either of these:

.Vb 2
 (*atomic_script_run:pattern)
 (*asr:pattern)
.Ve

(See \f(CW"(?>\f(CIpattern\f(CW)".)

In Taiwan, Japan, and Korea, it is common for text to have a mixture of
characters from their native scripts and base Chinese.  Perl follows
Unicode's \s-1UTS 39\s0 (<https://unicode.org/reports/tr39/>) Unicode Security
Mechanisms in allowing such mixtures.  For example, the Japanese scripts
Katakana and Hiragana are commonly mixed together in practice, along
with some Chinese characters, and hence are treated as being in a single
script run by Perl.

The rules used for matching decimal digits are slightly stricter.  Many
scripts have their own sets of digits equivalent to the Western \f(CW0
through \f(CW9 ones.  A few, such as Arabic, have more than one set.  For
a string to be considered a script run, all digits in it must come from
the same set of ten, as determined by the first digit encountered.
As an example,

.Vb 1
 qr/(*script_run: \\d+ \\b )/x
.Ve

guarantees that the digits matched will all be from the same set of 10.
You won't get a look-alike digit from a different script that has a
different value than what it appears to be.

Unicode has three pseudo scripts that are handled specially.

\*(L"Unknown\*(R" is applied to code points whose meaning has yet to be
determined.  Perl currently will match as a script run, any single
character string consisting of one of these code points.  But any string
longer than one code point containing one of these will not be
considered a script run.

\*(L"Inherited\*(R" is applied to characters that modify another, such as an
accent of some type.  These are considered to be in the script of the
master character, and so never cause a script run to not match.

The other one is \*(L"Common\*(R".  This consists of mostly punctuation, emoji,
and characters used in mathematics and music, the \s-1ASCII\s0 digits \f(CW0
through \f(CW9, and full-width forms of these digits.  These characters
can appear intermixed in text in many of the world's scripts.  These
also don't cause a script run to not match.  But like other scripts, all
digits in a run must come from the same set of 10.

This construct is non-capturing.  You can add parentheses to *pattern*
to capture, if desired.  You will have to do this if you plan to use
\*(L"(*ACCEPT) (*ACCEPT:arg)\*(R" and not have it bypass the script run
checking.

The \f(CW\*(C`Script_Extensions\*(C' property as modified by \s-1UTS 39\s0
(<https://unicode.org/reports/tr39/>) is used as the basis for this
feature.

To summarize,

- \(bu
All length 0 or length 1 sequences are script runs.

- \(bu
A longer sequence is a script run if and only if **all** of the following
conditions are met:
.Sp


> 
- 1.
No code point in the sequence has the \f(CW\*(C`Script_Extension\*(C' property of
\f(CW\*(C`Unknown\*(C'.
.Sp
This currently means that all code points in the sequence have been
assigned by Unicode to be characters that aren't private use nor
surrogate code points.

- 2.
All characters in the sequence come from the Common script and/or the
Inherited script and/or a single other script.
.Sp
The script of a character is determined by the \f(CW\*(C`Script_Extensions\*(C'
property as modified by \s-1UTS 39\s0 (<https://unicode.org/reports/tr39/>), as
described above.

- 3.
All decimal digits in the sequence come from the same block of 10
consecutive digits.



> 


### Special Backtracking Control Verbs

Subsection "Special Backtracking Control Verbs"
These special patterns are generally of the form \f(CW\*(C`(*\f(CIVERB\f(CW:\f(CIarg\f(CW)\*(C'. Unless
otherwise stated the *arg* argument is optional; in some cases, it is
mandatory.

Any pattern containing a special backtracking verb that allows an argument
has the special behaviour that when executed it sets the current package's
\f(CW$REGERROR and \f(CW$REGMARK variables. When doing so the following
rules apply:

On failure, the \f(CW$REGERROR variable will be set to the *arg* value of the
verb pattern, if the verb was involved in the failure of the match. If the
*arg* part of the pattern was omitted, then \f(CW$REGERROR will be set to the
name of the last \f(CW\*(C`(*MARK:\f(CINAME\f(CW)\*(C' pattern executed, or to \s-1TRUE\s0 if there was
none. Also, the \f(CW$REGMARK variable will be set to \s-1FALSE.\s0

On a successful match, the \f(CW$REGERROR variable will be set to \s-1FALSE,\s0 and
the \f(CW$REGMARK variable will be set to the name of the last
\f(CW\*(C`(*MARK:\f(CINAME\f(CW)\*(C' pattern executed.  See the explanation for the
\f(CW\*(C`(*MARK:\f(CINAME\f(CW)\*(C' verb below for more details.

**\s-1NOTE:\s0** \f(CW$REGERROR and \f(CW$REGMARK are not magic variables like \f(CW$1
and most other regex-related variables. They are not local to a scope, nor
readonly, but instead are volatile package variables similar to \f(CW$AUTOLOAD.
They are set in the package containing the code that *executed* the regex
(rather than the one that compiled it, where those differ).  If necessary, you
can use \f(CW\*(C`local\*(C' to localize changes to these variables to a specific scope
before executing a regex.

If a pattern does not contain a special backtracking verb that allows an
argument, then \f(CW$REGERROR and \f(CW$REGMARK are not touched at all.

- Verbs
Item "Verbs"

> 0
.ie n .IP """(*PRUNE)"" ""(*PRUNE:*NAME*)""" 4
.el .IP "\f(CW(*PRUNE) \f(CW(*PRUNE:\f(CINAME\f(CW)" 4
Xref "(*PRUNE) (*PRUNE:NAME)"
Item "(*PRUNE) (*PRUNE:NAME)"
.PD
This zero-width pattern prunes the backtracking tree at the current point
when backtracked into on failure. Consider the pattern \f(CW\*(C`/\f(CIA\f(CW (*PRUNE) \f(CIB\f(CW/\*(C',
where *A* and *B* are complex patterns. Until the \f(CW\*(C`(*PRUNE)\*(C' verb is reached,
*A* may backtrack as necessary to match. Once it is reached, matching
continues in *B*, which may also backtrack as necessary; however, should B
not match, then no further backtracking will take place, and the pattern
will fail outright at the current starting position.
.Sp
The following example counts all the possible matching strings in a
pattern (without actually matching any of them).
.Sp
.Vb 2
    \*(Aqaaab\*(Aq =~ /a+b?(?\{print "$&\\n"; $count++\})(*FAIL)/;
    print "Count=$count\\n";
.Ve
.Sp
which produces:
.Sp
.Vb 10
    aaab
    aaa
    aa
    a
    aab
    aa
    a
    ab
    a
    Count=9
.Ve
.Sp
If we add a \f(CW\*(C`(*PRUNE)\*(C' before the count like the following
.Sp
.Vb 2
    \*(Aqaaab\*(Aq =~ /a+b?(*PRUNE)(?\{print "$&\\n"; $count++\})(*FAIL)/;
    print "Count=$count\\n";
.Ve
.Sp
we prevent backtracking and find the count of the longest matching string
at each matching starting point like so:
.Sp
.Vb 4
    aaab
    aab
    ab
    Count=3
.Ve
.Sp
Any number of \f(CW\*(C`(*PRUNE)\*(C' assertions may be used in a pattern.
.Sp
See also \f(CW"(?>\f(CIpattern\f(CW)" and possessive quantifiers for
other ways to
control backtracking. In some cases, the use of \f(CW\*(C`(*PRUNE)\*(C' can be
replaced with a \f(CW\*(C`(?>pattern)\*(C' with no functional difference; however,
\f(CW\*(C`(*PRUNE)\*(C' can be used to handle cases that cannot be expressed using a
\f(CW\*(C`(?>pattern)\*(C' alone.
.ie n .IP """(*SKIP)"" ""(*SKIP:*NAME*)""" 4
.el .IP "\f(CW(*SKIP) \f(CW(*SKIP:\f(CINAME\f(CW)" 4
Xref "(*SKIP)"
Item "(*SKIP) (*SKIP:NAME)"
This zero-width pattern is similar to \f(CW\*(C`(*PRUNE)\*(C', except that on
failure it also signifies that whatever text that was matched leading up
to the \f(CW\*(C`(*SKIP)\*(C' pattern being executed cannot be part of *any* match
of this pattern. This effectively means that the regex engine \*(L"skips\*(R" forward
to this position on failure and tries to match again, (assuming that
there is sufficient room to match).
.Sp
The name of the \f(CW\*(C`(*SKIP:\f(CINAME\f(CW)\*(C' pattern has special significance. If a
\f(CW\*(C`(*MARK:\f(CINAME\f(CW)\*(C' was encountered while matching, then it is that position
which is used as the \*(L"skip point\*(R". If no \f(CW\*(C`(*MARK)\*(C' of that name was
encountered, then the \f(CW\*(C`(*SKIP)\*(C' operator has no effect. When used
without a name the \*(L"skip point\*(R" is where the match point was when
executing the \f(CW\*(C`(*SKIP)\*(C' pattern.
.Sp
Compare the following to the examples in \f(CW\*(C`(*PRUNE)\*(C'; note the string
is twice as long:
.Sp
.Vb 2
 \*(Aqaaabaaab\*(Aq =~ /a+b?(*SKIP)(?\{print "$&\\n"; $count++\})(*FAIL)/;
 print "Count=$count\\n";
.Ve
.Sp
outputs
.Sp
.Vb 3
    aaab
    aaab
    Count=2
.Ve
.Sp
Once the 'aaab' at the start of the string has matched, and the \f(CW\*(C`(*SKIP)\*(C'
executed, the next starting point will be where the cursor was when the
\f(CW\*(C`(*SKIP)\*(C' was executed.
.ie n .IP """(*MARK:*NAME*)"" ""(*:*NAME*)""" 4
.el .IP "\f(CW(*MARK:\f(CINAME\f(CW) \f(CW(*:\f(CINAME\f(CW)" 4
Xref "(*MARK) (*MARK:NAME) (*:NAME)"
Item "(*MARK:NAME) (*:NAME)"
This zero-width pattern can be used to mark the point reached in a string
when a certain part of the pattern has been successfully matched. This
mark may be given a name. A later \f(CW\*(C`(*SKIP)\*(C' pattern will then skip
forward to that point if backtracked into on failure. Any number of
\f(CW\*(C`(*MARK)\*(C' patterns are allowed, and the *\s-1NAME\s0* portion may be duplicated.
.Sp
In addition to interacting with the \f(CW\*(C`(*SKIP)\*(C' pattern, \f(CW\*(C`(*MARK:\f(CINAME\f(CW)\*(C'
can be used to \*(L"label\*(R" a pattern branch, so that after matching, the
program can determine which branches of the pattern were involved in the
match.
.Sp
When a match is successful, the \f(CW$REGMARK variable will be set to the
name of the most recently executed \f(CW\*(C`(*MARK:\f(CINAME\f(CW)\*(C' that was involved
in the match.
.Sp
This can be used to determine which branch of a pattern was matched
without using a separate capture group for each branch, which in turn
can result in a performance improvement, as perl cannot optimize
\f(CW\*(C`/(?:(x)|(y)|(z))/\*(C' as efficiently as something like
\f(CW\*(C`/(?:x(*MARK:x)|y(*MARK:y)|z(*MARK:z))/\*(C'.
.Sp
When a match has failed, and unless another verb has been involved in
failing the match and has provided its own name to use, the \f(CW$REGERROR
variable will be set to the name of the most recently executed
\f(CW\*(C`(*MARK:\f(CINAME\f(CW)\*(C'.
.Sp
See \*(L"(*SKIP)\*(R" for more details.
.Sp
As a shortcut \f(CW\*(C`(*MARK:\f(CINAME\f(CW)\*(C' can be written \f(CW\*(C`(*:\f(CINAME\f(CW)\*(C'.
.ie n .IP """(*THEN)"" ""(*THEN:*NAME*)""" 4
.el .IP "\f(CW(*THEN) \f(CW(*THEN:\f(CINAME\f(CW)" 4
Item "(*THEN) (*THEN:NAME)"
This is similar to the \*(L"cut group\*(R" operator \f(CW\*(C`::\*(C' from Raku.  Like
\f(CW\*(C`(*PRUNE)\*(C', this verb always matches, and when backtracked into on
failure, it causes the regex engine to try the next alternation in the
innermost enclosing group (capturing or otherwise) that has alternations.
The two branches of a \f(CW\*(C`(?(\f(CIcondition\f(CW)\f(CIyes-pattern\f(CW|\f(CIno-pattern\f(CW)\*(C' do not
count as an alternation, as far as \f(CW\*(C`(*THEN)\*(C' is concerned.
.Sp
Its name comes from the observation that this operation combined with the
alternation operator (\f(CW"|") can be used to create what is essentially a
pattern-based if/then/else block:
.Sp
.Vb 1
  ( COND (*THEN) FOO | COND2 (*THEN) BAR | COND3 (*THEN) BAZ )
.Ve
.Sp
Note that if this operator is used and \s-1NOT\s0 inside of an alternation then
it acts exactly like the \f(CW\*(C`(*PRUNE)\*(C' operator.
.Sp
.Vb 1
  / A (*PRUNE) B /
.Ve
.Sp
is the same as
.Sp
.Vb 1
  / A (*THEN) B /
.Ve
.Sp
but
.Sp
.Vb 1
  / ( A (*THEN) B | C ) /
.Ve
.Sp
is not the same as
.Sp
.Vb 1
  / ( A (*PRUNE) B | C ) /
.Ve
.Sp
as after matching the *A* but failing on the *B* the \f(CW\*(C`(*THEN)\*(C' verb will
backtrack and try *C*; but the \f(CW\*(C`(*PRUNE)\*(C' verb will simply fail.
.ie n .IP """(*COMMIT)"" ""(*COMMIT:*arg*)""" 4
.el .IP "\f(CW(*COMMIT) \f(CW(*COMMIT:\f(CIarg\f(CW)" 4
Xref "(*COMMIT)"
Item "(*COMMIT) (*COMMIT:arg)"
This is the Raku \*(L"commit pattern\*(R" \f(CW\*(C`<commit>\*(C' or \f(CW\*(C`:::\*(C'. It's a
zero-width pattern similar to \f(CW\*(C`(*SKIP)\*(C', except that when backtracked
into on failure it causes the match to fail outright. No further attempts
to find a valid match by advancing the start pointer will occur again.
For example,
.Sp
.Vb 2
 \*(Aqaaabaaab\*(Aq =~ /a+b?(*COMMIT)(?\{print "$&\\n"; $count++\})(*FAIL)/;
 print "Count=$count\\n";
.Ve
.Sp
outputs
.Sp
.Vb 2
    aaab
    Count=1
.Ve
.Sp
In other words, once the \f(CW\*(C`(*COMMIT)\*(C' has been entered, and if the pattern
does not match, the regex engine will not try any further matching on the
rest of the string.
.ie n .IP """(*FAIL)"" ""(*F)"" ""(*FAIL:*arg*)""" 4
.el .IP "\f(CW(*FAIL) \f(CW(*F) \f(CW(*FAIL:\f(CIarg\f(CW)" 4
Xref "(*FAIL) (*F)"
Item "(*FAIL) (*F) (*FAIL:arg)"
This pattern matches nothing and always fails. It can be used to force the
engine to backtrack. It is equivalent to \f(CW\*(C`(?!)\*(C', but easier to read. In
fact, \f(CW\*(C`(?!)\*(C' gets optimised into \f(CW\*(C`(*FAIL)\*(C' internally. You can provide
an argument so that if the match fails because of this \f(CW\*(C`FAIL\*(C' directive
the argument can be obtained from \f(CW$REGERROR.
.Sp
It is probably useful only when combined with \f(CW\*(C`(?\{\})\*(C' or \f(CW\*(C`(??\{\})\*(C'.
.ie n .IP """(*ACCEPT)"" ""(*ACCEPT:*arg*)""" 4
.el .IP "\f(CW(*ACCEPT) \f(CW(*ACCEPT:\f(CIarg\f(CW)" 4
Xref "(*ACCEPT)"
Item "(*ACCEPT) (*ACCEPT:arg)"
This pattern matches nothing and causes the end of successful matching at
the point at which the \f(CW\*(C`(*ACCEPT)\*(C' pattern was encountered, regardless of
whether there is actually more to match in the string. When inside of a
nested pattern, such as recursion, or in a subpattern dynamically generated
via \f(CW\*(C`(??\{\})\*(C', only the innermost pattern is ended immediately.
.Sp
If the \f(CW\*(C`(*ACCEPT)\*(C' is inside of capturing groups then the groups are
marked as ended at the point at which the \f(CW\*(C`(*ACCEPT)\*(C' was encountered.
For instance:
.Sp
.Vb 1
  \*(AqAB\*(Aq =~ /(A (A|B(*ACCEPT)|C) D)(E)/x;
.Ve
.Sp
will match, and \f(CW$1 will be \f(CW\*(C`AB\*(C' and \f(CW$2 will be \f(CW"B", \f(CW$3 will not
be set. If another branch in the inner parentheses was matched, such as in the
string '\s-1ACDE\s0', then the \f(CW"D" and \f(CW"E" would have to be matched as well.
.Sp
You can provide an argument, which will be available in the var
\f(CW$REGMARK after the match completes.



> 

.ie n .SS "Warning on ""\\1"" Instead of $1"
.el .SS "Warning on \f(CW\\1 Instead of \f(CW$1"
Subsection "Warning on 1 Instead of $1"
Some people get too used to writing things like:

.Vb 1
    $pattern =~ s/(\\W)/\\\\\\1/g;
.Ve

This is grandfathered (for \\1 to \\9) for the \s-1RHS\s0 of a substitute to avoid
shocking the
**sed** addicts, but it's a dirty habit to get into.  That's because in
PerlThink, the righthand side of an \f(CW\*(C`s///\*(C' is a double-quoted string.  \f(CW\*(C`\\1\*(C' in
the usual double-quoted string means a control-A.  The customary Unix
meaning of \f(CW\*(C`\\1\*(C' is kludged in for \f(CW\*(C`s///\*(C'.  However, if you get into the habit
of doing that, you get yourself into trouble if you then add an \f(CW\*(C`/e\*(C'
modifier.

.Vb 1
    s/(\\d+)/ \\1 + 1 /eg;            # causes warning under -w
.Ve

Or if you try to do

.Vb 1
    s/(\\d+)/\\1000/;
.Ve

You can't disambiguate that by saying \f(CW\*(C`\\\{1\}000\*(C', whereas you can fix it with
\f(CW\*(C`$\{1\}000\*(C'.  The operation of interpolation should not be confused
with the operation of matching a backreference.  Certainly they mean two
different things on the *left* side of the \f(CW\*(C`s///\*(C'.

### Repeated Patterns Matching a Zero-length Substring

Subsection "Repeated Patterns Matching a Zero-length Substring"
**\s-1WARNING\s0**: Difficult material (and prose) ahead.  This section needs a rewrite.

Regular expressions provide a terse and powerful programming language.  As
with most other power tools, power comes together with the ability
to wreak havoc.

A common abuse of this power stems from the ability to make infinite
loops using regular expressions, with something as innocuous as:

.Vb 1
    \*(Aqfoo\*(Aq =~ m\{ ( o? )* \}x;
.Ve

The \f(CW\*(C`o?\*(C' matches at the beginning of "\f(CW\*(C`foo\*(C'", and since the position
in the string is not moved by the match, \f(CW\*(C`o?\*(C' would match again and again
because of the \f(CW"*" quantifier.  Another common way to create a similar cycle
is with the looping modifier \f(CW\*(C`/g\*(C':

.Vb 1
    @matches = ( \*(Aqfoo\*(Aq =~ m\{ o? \}xg );
.Ve

or

.Vb 1
    print "match: <$&>\\n" while \*(Aqfoo\*(Aq =~ m\{ o? \}xg;
.Ve

or the loop implied by \f(CW\*(C`split()\*(C'.

However, long experience has shown that many programming tasks may
be significantly simplified by using repeated subexpressions that
may match zero-length substrings.  Here's a simple example being:

.Vb 2
    @chars = split //, $string;           # // is not magic in split
    ($whitewashed = $string) =~ s/()/ /g; # parens avoid magic s// /
.Ve

Thus Perl allows such constructs, by \fIforcefully breaking
the infinite loop.  The rules for this are different for lower-level
loops given by the greedy quantifiers \f(CW\*(C`*+\{\}\*(C', and for higher-level
ones like the \f(CW\*(C`/g\*(C' modifier or \f(CW\*(C`split()\*(C' operator.

The lower-level loops are *interrupted* (that is, the loop is
broken) when Perl detects that a repeated expression matched a
zero-length substring.   Thus

.Vb 1
   m\{ (?: NON_ZERO_LENGTH | ZERO_LENGTH )* \}x;
.Ve

is made equivalent to

.Vb 1
   m\{ (?: NON_ZERO_LENGTH )* (?: ZERO_LENGTH )? \}x;
.Ve

For example, this program

.Vb 12
   #!perl -l
   "aaaaab" =~ /
     (?:
        a                 # non-zero
        |                 # or
       (?\{print "hello"\}) # print hello whenever this
                          #    branch is tried
       (?=(b))            # zero-width assertion
     )*  # any number of times
    /x;
   print $&;
   print $1;
.Ve

prints

.Vb 3
   hello
   aaaaa
   b
.Ve

Notice that \*(L"hello\*(R" is only printed once, as when Perl sees that the sixth
iteration of the outermost \f(CW\*(C`(?:)*\*(C' matches a zero-length string, it stops
the \f(CW"*".

The higher-level loops preserve an additional state between iterations:
whether the last match was zero-length.  To break the loop, the following
match after a zero-length match is prohibited to have a length of zero.
This prohibition interacts with backtracking (see \*(L"Backtracking\*(R"),
and so the *second best* match is chosen if the *best* match is of
zero length.

For example:

.Vb 2
    $_ = \*(Aqbar\*(Aq;
    s/\\w??/<$&>/g;
.Ve

results in \f(CW\*(C`<><b><><a><><r><>\*(C'.  At each position of the string the best
match given by non-greedy \f(CW\*(C`??\*(C' is the zero-length match, and the \fIsecond
best match is what is matched by \f(CW\*(C`\\w\*(C'.  Thus zero-length matches
alternate with one-character-long matches.

Similarly, for repeated \f(CW\*(C`m/()/g\*(C' the second-best match is the match at the
position one notch further in the string.

The additional state of being *matched with zero-length* is associated with
the matched string, and is reset by each assignment to \f(CW\*(C`pos()\*(C'.
Zero-length matches at the end of the previous match are ignored
during \f(CW\*(C`split\*(C'.

### Combining \s-1RE\s0 Pieces

Subsection "Combining RE Pieces"
Each of the elementary pieces of regular expressions which were described
before (such as \f(CW\*(C`ab\*(C' or \f(CW\*(C`\\Z\*(C') could match at most one substring
at the given position of the input string.  However, in a typical regular
expression these elementary pieces are combined into more complicated
patterns using combining operators \f(CW\*(C`ST\*(C', \f(CW\*(C`S|T\*(C', \f(CW\*(C`S*\*(C' *etc*.
(in these examples \f(CW"S" and \f(CW"T" are regular subexpressions).

Such combinations can include alternatives, leading to a problem of choice:
if we match a regular expression \f(CW\*(C`a|ab\*(C' against \f(CW"abc", will it match
substring \f(CW"a" or \f(CW"ab"?  One way to describe which substring is
actually matched is the concept of backtracking (see \*(L"Backtracking\*(R").
However, this description is too low-level and makes you think
in terms of a particular implementation.

Another description starts with notions of \*(L"better\*(R"/\*(L"worse\*(R".  All the
substrings which may be matched by the given regular expression can be
sorted from the \*(L"best\*(R" match to the \*(L"worst\*(R" match, and it is the \*(L"best\*(R"
match which is chosen.  This substitutes the question of \*(L"what is chosen?\*(R"
by the question of \*(L"which matches are better, and which are worse?\*(R".

Again, for elementary pieces there is no such question, since at most
one match at a given position is possible.  This section describes the
notion of better/worse for combining operators.  In the description
below \f(CW"S" and \f(CW"T" are regular subexpressions.
.ie n .IP """ST""" 4
.el .IP "\f(CWST" 4
Item "ST"
Consider two possible matches, \f(CW\*(C`AB\*(C' and \f(CW\*(C`A\*(AqB\*(Aq\*(C', \f(CW"A" and \f(CW\*(C`A\*(Aq\*(C' are
substrings which can be matched by \f(CW"S", \f(CW"B" and \f(CW\*(C`B\*(Aq\*(C' are substrings
which can be matched by \f(CW"T".
.Sp
If \f(CW"A" is a better match for \f(CW"S" than \f(CW\*(C`A\*(Aq\*(C', \f(CW\*(C`AB\*(C' is a better
match than \f(CW\*(C`A\*(AqB\*(Aq\*(C'.
.Sp
If \f(CW"A" and \f(CW\*(C`A\*(Aq\*(C' coincide: \f(CW\*(C`AB\*(C' is a better match than \f(CW\*(C`AB\*(Aq\*(C' if
\f(CW"B" is a better match for \f(CW"T" than \f(CW\*(C`B\*(Aq\*(C'.
.ie n .IP """S|T""" 4
.el .IP "\f(CWS|T" 4
Item "S|T"
When \f(CW"S" can match, it is a better match than when only \f(CW"T" can match.
.Sp
Ordering of two matches for \f(CW"S" is the same as for \f(CW"S".  Similar for
two matches for \f(CW"T".
.ie n .IP """S\{REPEAT_COUNT\}""" 4
.el .IP "\f(CWS\{REPEAT_COUNT\}" 4
Item "S\{REPEAT_COUNT\}"
Matches as \f(CW\*(C`SSS...S\*(C' (repeated as many times as necessary).
.ie n .IP """S\{min,max\}""" 4
.el .IP "\f(CWS\{min,max\}" 4
Item "S\{min,max\}"
Matches as \f(CW\*(C`S\{max\}|S\{max-1\}|...|S\{min+1\}|S\{min\}\*(C'.
.ie n .IP """S\{min,max\}?""" 4
.el .IP "\f(CWS\{min,max\}?" 4
Item "S\{min,max\}?"
Matches as \f(CW\*(C`S\{min\}|S\{min+1\}|...|S\{max-1\}|S\{max\}\*(C'.
.ie n .IP """S?"", ""S*"", ""S+""" 4
.el .IP "\f(CWS?, \f(CWS*, \f(CWS+" 4
Item "S?, S*, S+"
Same as \f(CW\*(C`S\{0,1\}\*(C', \f(CW\*(C`S\{0,BIG_NUMBER\}\*(C', \f(CW\*(C`S\{1,BIG_NUMBER\}\*(C' respectively.
.ie n .IP """S??"", ""S*?"", ""S+?""" 4
.el .IP "\f(CWS??, \f(CWS*?, \f(CWS+?" 4
Item "S??, S*?, S+?"
Same as \f(CW\*(C`S\{0,1\}?\*(C', \f(CW\*(C`S\{0,BIG_NUMBER\}?\*(C', \f(CW\*(C`S\{1,BIG_NUMBER\}?\*(C' respectively.
.ie n .IP """(?>S)""" 4
.el .IP "\f(CW(?>S)" 4
Item "(?>S)"
Matches the best match for \f(CW"S" and only that.
.ie n .IP """(?=S)"", ""(?<=S)""" 4
.el .IP "\f(CW(?=S), \f(CW(?<=S)" 4
Item "(?=S), (?<=S)"
Only the best match for \f(CW"S" is considered.  (This is important only if
\f(CW"S" has capturing parentheses, and backreferences are used somewhere
else in the whole regular expression.)
.ie n .IP """(?!S)"", ""(?<!S)""" 4
.el .IP "\f(CW(?!S), \f(CW(?<!S)" 4
Item "(?!S), (?<!S)"
For this grouping operator there is no need to describe the ordering, since
only whether or not \f(CW"S" can match is important.
.ie n .IP """(??\{ *EXPR* \})"", ""(?*PARNO*)""" 4
.el .IP "\f(CW(??\{ \f(CIEXPR\f(CW \}), \f(CW(?\f(CIPARNO\f(CW)" 4
Item "(??\{ EXPR \}), (?PARNO)"
The ordering is the same as for the regular expression which is
the result of *\s-1EXPR\s0*, or the pattern contained by capture group *\s-1PARNO\s0*.
.ie n .IP """(?(*condition*)*yes-pattern*|*no-pattern*)""" 4
.el .IP "\f(CW(?(\f(CIcondition\f(CW)\f(CIyes-pattern\f(CW|\f(CIno-pattern\f(CW)" 4
Item "(?(condition)yes-pattern|no-pattern)"
Recall that which of *yes-pattern* or *no-pattern* actually matches is
already determined.  The ordering of the matches is the same as for the
chosen subexpression.

The above recipes describe the ordering of matches *at a given position*.
One more rule is needed to understand how a match is determined for the
whole regular expression: a match at an earlier position is always better
than a match at a later position.

### Creating Custom \s-1RE\s0 Engines

Subsection "Creating Custom RE Engines"
As of Perl 5.10.0, one can create custom regular expression engines.  This
is not for the faint of heart, as they have to plug in at the C level.  See
perlreapi for more details.

As an alternative, overloaded constants (see overload) provide a simple
way to extend the functionality of the \s-1RE\s0 engine, by substituting one
pattern for another.

Suppose that we want to enable a new \s-1RE\s0 escape-sequence \f(CW\*(C`\\Y|\*(C' which
matches at a boundary between whitespace characters and non-whitespace
characters.  Note that \f(CW\*(C`(?=\\S)(?<!\\S)|(?!\\S)(?<=\\S)\*(C' matches exactly
at these positions, so we want to have each \f(CW\*(C`\\Y|\*(C' in the place of the
more complicated version.  We can create a module \f(CW\*(C`customre\*(C' to do
this:

.Vb 2
    package customre;
    use overload;

    sub import \{
      shift;
      die "No argument to customre::import allowed" if @_;
      overload::constant \*(Aqqr\*(Aq => \convert;
    \}

    sub invalid \{ die "/$_[0]/: invalid escape \*(Aq\\\\$_[1]\*(Aq"\}

    # We must also take care of not escaping the legitimate \\\\Y|
    # sequence, hence the presence of \*(Aq\\\\\*(Aq in the conversion rules.
    my %rules = ( \*(Aq\\\\\*(Aq => \*(Aq\\\\\\\\\*(Aq,
                  \*(AqY|\*(Aq => qr/(?=\\S)(?<!\\S)|(?!\\S)(?<=\\S)/ );
    sub convert \{
      my $re = shift;
      $re =~ s\{
                \\\\ ( \\\\ | Y . )
              \}
              \{ $rules\{$1\} or invalid($re,$1) \}sgex;
      return $re;
    \}
.Ve

Now \f(CW\*(C`use customre\*(C' enables the new escape in constant regular
expressions, *i.e.*, those without any runtime variable interpolations.
As documented in overload, this conversion will work only over
literal parts of regular expressions.  For \f(CW\*(C`\\Y|$re\\Y|\*(C' the variable
part of this regular expression needs to be converted explicitly
(but only if the special meaning of \f(CW\*(C`\\Y|\*(C' should be enabled inside \f(CW$re):

.Vb 5
    use customre;
    $re = <>;
    chomp $re;
    $re = customre::convert $re;
    /\\Y|$re\\Y|/;
.Ve

### Embedded Code Execution Frequency

Subsection "Embedded Code Execution Frequency"
The exact rules for how often \f(CW\*(C`(??\{\})\*(C' and \f(CW\*(C`(?\{\})\*(C' are executed in a pattern
are unspecified.  In the case of a successful match you can assume that
they \s-1DWIM\s0 and will be executed in left to right order the appropriate
number of times in the accepting path of the pattern as would any other
meta-pattern.  How non-accepting pathways and match failures affect the
number of times a pattern is executed is specifically unspecified and
may vary depending on what optimizations can be applied to the pattern
and is likely to change from version to version.

For instance in

.Vb 1
  "aaabcdeeeee"=~/a(?\{print "a"\})b(?\{print "b"\})cde/;
.Ve

the exact number of times \*(L"a\*(R" or \*(L"b\*(R" are printed out is unspecified for
failure, but you may assume they will be printed at least once during
a successful match, additionally you may assume that if \*(L"b\*(R" is printed,
it will be preceded by at least one \*(L"a\*(R".

In the case of branching constructs like the following:

.Vb 1
  /a(b|(?\{ print "a" \}))c(?\{ print "c" \})/;
.Ve

you can assume that the input \*(L"ac\*(R" will output \*(L"ac\*(R", and that \*(L"abc\*(R"
will output only \*(L"c\*(R".

When embedded code is quantified, successful matches will call the
code once for each matched iteration of the quantifier.  For
example:

.Vb 1
  "good" =~ /g(?:o(?\{print "o"\}))*d/;
.Ve

will output \*(L"o\*(R" twice.

### PCRE/Python Support

Subsection "PCRE/Python Support"
As of Perl 5.10.0, Perl supports several Python/PCRE-specific extensions
to the regex syntax. While Perl programmers are encouraged to use the
Perl-specific syntax, the following are also accepted:
.ie n .IP """(?P<*NAME*>*pattern*)""" 4
.el .IP "\f(CW(?P<\f(CINAME\f(CW>\f(CIpattern\f(CW)" 4
Item "(?P<NAME>pattern)"
Define a named capture group. Equivalent to \f(CW\*(C`(?<\f(CINAME\f(CW>\f(CIpattern\f(CW)\*(C'.
.ie n .IP """(?P=*NAME*)""" 4
.el .IP "\f(CW(?P=\f(CINAME\f(CW)" 4
Item "(?P=NAME)"
Backreference to a named capture group. Equivalent to \f(CW\*(C`\\g\{\f(CINAME\f(CW\}\*(C'.
.ie n .IP """(?P>*NAME*)""" 4
.el .IP "\f(CW(?P>\f(CINAME\f(CW)" 4
Item "(?P>NAME)"
Subroutine call to a named capture group. Equivalent to \f(CW\*(C`(?&\f(CINAME\f(CW)\*(C'.

## BUGS

Header "BUGS"
There are a number of issues with regard to case-insensitive matching
in Unicode rules.  See \f(CW"i" under \*(L"Modifiers\*(R" above.

This document varies from difficult to understand to completely
and utterly opaque.  The wandering prose riddled with jargon is
hard to fathom in several places.

This document needs a rewrite that separates the tutorial content
from the reference content.

## SEE ALSO

Header "SEE ALSO"
The syntax of patterns used in Perl pattern matching evolved from those
supplied in the Bell Labs Research Unix 8th Edition (Version 8) regex
routines.  (The code is actually derived (distantly) from Henry
Spencer's freely redistributable reimplementation of those V8 routines.)

perlrequick.

perlretut.

\*(L"Regexp Quote-Like Operators\*(R" in perlop.

\*(L"Gory details of parsing quoted constructs\*(R" in perlop.

perlfaq6.

\*(L"pos\*(R" in perlfunc.

perllocale.

perlebcdic.

*Mastering Regular Expressions* by Jeffrey Friedl, published
by O'Reilly and Associates.
