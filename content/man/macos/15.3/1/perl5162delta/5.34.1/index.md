+++
manpage_section = "1"
operating_system = "macos"
description = "This document describes differences between the 5.16.1 release and the 5.16.2 release. If you are upgrading from an earlier release such as 5.16.0, first read perl5161delta, which describes differences between 5.16.0 and 5.16.1. There are no chan..."
manpage_format = "troff"
operating_system_version = "15.3"
manpage_name = "perl5162delta"
date = "2022-02-19"
title = "perl5162delta(1)"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5162DELTA 1"
PERL5162DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5162delta - what is new for perl v5.16.2

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.16.1 release and
the 5.16.2 release.

If you are upgrading from an earlier release such as 5.16.0, first read
perl5161delta, which describes differences between 5.16.0 and
5.16.1.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.16.0
If any exist, they are bugs, and we request that you submit a
report.  See \*(L"Reporting Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Module::CoreList has been upgraded from version 2.70 to version 2.76.

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
configuration should no longer be confused by ls colorization

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- \s-1AIX\s0
Item "AIX"
Configure now always adds -qlanglvl=extc99 to the \s-1CC\s0 flags on \s-1AIX\s0 when
using xlC.  This will make it easier to compile a number of XS-based modules
that assume C99 [perl #113778].

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
fix /\\h/ equivalence with /[\\h]/
.Sp
see [perl #114220]

## Known Problems

Header "Known Problems"
There are no new known problems.

## Acknowledgements

Header "Acknowledgements"
Perl 5.16.2 represents approximately 2 months of development since Perl
5.16.1 and contains approximately 740 lines of changes across 20 files
from 9 authors.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers. The following people are known to
have contributed the improvements that became Perl 5.16.2:

Andy Dougherty, Craig A. Berry, Darin McBride, Dominic Hargreaves, Karen
Etheridge, Karl Williamson, Peter Martini, Ricardo Signes, Tony Cook.

The list above is almost certainly incomplete as it is automatically
generated from version control history. In particular, it does not
include the names of the (very much appreciated) contributors who
reported issues to the Perl bug tracker.

For a more complete list of all of Perl's historical contributors,
please see the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please
send it to perl5-security-report@perl.org. This points to a closed
subscription unarchived mailing list, which includes all the core
committers, who will be able to help assess the impact of issues, figure
out a resolution, and help co-ordinate the release of patches to
mitigate or fix the problem across all platforms on which Perl is
supported. Please only use this address for security issues in the Perl
core, not for modules independently distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
