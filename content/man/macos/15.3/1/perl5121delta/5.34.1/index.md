+++
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
description = "This document describes differences between the 5.12.0 release and the 5.12.1 release. If you are upgrading from an earlier release such as 5.10.1, first read perl5120delta, which describes differences between 5.10.0 and 5.12.0. There are no chan..."
manpage_section = "1"
operating_system = "macos"
operating_system_version = "15.3"
manpage_format = "troff"
detected_package_version = "5.34.1"
manpage_name = "perl5121delta"
date = "2022-02-19"
title = "perl5121delta(1)"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5121DELTA 1"
PERL5121DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5121delta - what is new for perl v5.12.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.12.0 release and
the 5.12.1 release.

If you are upgrading from an earlier release such as 5.10.1, first read
perl5120delta, which describes differences between 5.10.0 and
5.12.0.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.12.0. If any
incompatibilities with 5.12.0 exist, they are bugs. Please report them.

## Core Enhancements

Header "Core Enhancements"
Other than the bug fixes listed below, there should be no user-visible
changes to the core language in this release.

## Modules and Pragmata

Header "Modules and Pragmata"

### Pragmata Changes

Subsection "Pragmata Changes"

- \(bu
We fixed exporting of \f(CW\*(C`is_strict\*(C' and \f(CW\*(C`is_lax\*(C' from version.
.Sp
These were being exported with a wrapper that treated them as method
calls, which caused them to fail.  They are just functions, are
documented as such, and should never be subclassed, so this patch
just exports them directly as functions without the wrapper.

### Updated Modules

Subsection "Updated Modules"

- \(bu
We upgraded \s-1CGI\s0 to version 3.49 to incorporate fixes for regressions
introduced in the release we shipped with Perl 5.12.0.

- \(bu
We upgraded Pod::Simple to version 3.14 to get an improvement to \\C\\<\\< \\>\\>
parsing.

- \(bu
We made a small fix to the \s-1CPANPLUS\s0 test suite to fix an occasional spurious test failure.

- \(bu
We upgraded Safe to version 2.27 to wrap coderefs returned by \f(CW\*(C`reval()\*(C' and \f(CW\*(C`rdo()\*(C'.

## Changes to Existing Documentation

Header "Changes to Existing Documentation"

- \(bu
We added the new maintenance release policy to perlpolicy

- \(bu
We've clarified the multiple-angle-bracket construct in the spec for \s-1POD\s0
in perlpodspec

- \(bu
We added a missing explanation for a warning about \f(CW\*(C`:=\*(C' to perldiag

- \(bu
We removed a false claim in perlunitut that all text strings are Unicode strings in Perl.

- \(bu
We updated the GitHub mirror link in perlrepository to mirrors/perl, not github/perl

- \(bu
We fixed a minor error in perl5114delta.

- \(bu
We replaced a mention of the now-obsolete Switch with *given*/*when*.

- \(bu
We improved documentation about *\f(CI$sitelibexp\fI/sitecustomize.pl* in perlrun.

- \(bu
We corrected perlmodlib which had unintentionally omitted a number of modules.

- \(bu
We updated the documentation for 'require' in perlfunc relating to putting Perl code in \f(CW@INC.

- \(bu
We reinstated some erroneously-removed documentation about quotemeta in perlfunc.

- \(bu
We fixed an *a2p* example in perlutil.

- \(bu
We filled in a blank in perlport with the release date of Perl 5.12.

- \(bu
We fixed broken links in a number of perldelta files.

- \(bu
The documentation for Carp incorrectly stated that the \f(CW$Carp::Verbose
variable makes cluck generate stack backtraces.

- \(bu
We fixed a number of typos in Pod::Functions

- \(bu
We improved documentation of case-changing functions in perlfunc

- \(bu
We corrected perlgpl to contain the correct version of the \s-1GNU\s0
General Public License.

## Testing

Header "Testing"

### Testing Improvements

Subsection "Testing Improvements"

- \(bu
*t/op/sselect.t* is now less prone to clock jitter during timing checks
on Windows.
.Sp
**sleep()** time on Win32 may be rounded down to multiple of
the clock tick interval.

- \(bu
*lib/blib.t* and *lib/locale.t*: Fixes for test failures on Darwin/PPC

- \(bu
*perl5db.t*: Fix for test failures when \f(CW\*(C`Term::ReadLine::Gnu\*(C' is installed.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"

### Configuration improvements

Subsection "Configuration improvements"

- \(bu
We updated *\s-1INSTALL\s0* with notes about how to deal with broken *dbm.h*
on OpenSUSE (and possibly other platforms)

## Bug Fixes

Header "Bug Fixes"

- \(bu
A bug in how we process filetest operations could cause a segfault.
Filetests don't always expect an op on the stack, so we now use
TOPs only if we're sure that we're not stat'ing the _ filehandle.
This is indicated by OPf_KIDS (as checked in ck_ftst).
.Sp
See also: <https://github.com/Perl/perl5/issues/10335>

- \(bu
When deparsing a nextstate op that has both a change of package (relative
to the previous nextstate) and a label, the package declaration is now
emitted first, because it is syntactically impermissible for a label to
prefix a package declaration.

- \(bu
\s-1XSUB\s0.h now correctly redefines fgets under \s-1PERL_IMPLICIT_SYS\s0
.Sp
See also: <http://rt.cpan.org/Public/Bug/Display.html?id=55049>

- \(bu
utf8::is_utf8 now respects \s-1GMAGIC\s0 (e.g. \f(CW$1)

- \(bu
\s-1XS\s0 code using \f(CW\*(C`fputc()\*(C' or \f(CW\*(C`fputs()\*(C': on Windows could cause an error
due to their arguments being swapped.
.Sp
See also: <https://github.com/Perl/perl5/issues/10156>

- \(bu
We fixed a small bug in **lex_stuff_pvn()** that caused spurious syntax errors
in an obscure situation.  It happened when stuffing was performed on the
last line of a file and the line ended with a statement that lacked a
terminating semicolon.
.Sp
See also: <https://github.com/Perl/perl5/issues/10273>

- \(bu
We fixed a bug that could cause \\N\{\} constructs followed by a single . to
be parsed incorrectly.
.Sp
See also: <https://github.com/Perl/perl5/issues/10367>

- \(bu
We fixed a bug that caused when(scalar) without an argument not to be
treated as a syntax error.
.Sp
See also: <https://github.com/Perl/perl5/issues/10287>

- \(bu
We fixed a regression in the handling of labels immediately before string
evals that was introduced in Perl 5.12.0.
.Sp
See also: <https://github.com/Perl/perl5/issues/10301>

- \(bu
We fixed a regression in case-insensitive matching of folded characters
in regular expressions introduced in Perl 5.10.1.
.Sp
See also: <https://github.com/Perl/perl5/issues/10193>

## Platform Specific Notes

Header "Platform Specific Notes"

### HP-UX

Subsection "HP-UX"

- \(bu
Perl now allows -Duse64bitint without promoting to use64bitall on HP-UX

### \s-1AIX\s0

Subsection "AIX"

- \(bu
Perl now builds on \s-1AIX 4.2\s0
.Sp
The changes required work around \s-1AIX 4\s0.2s' lack of support for IPv6,
and limited support for \s-1POSIX\s0 \f(CW\*(C`sigaction()\*(C'.

### FreeBSD 7

Subsection "FreeBSD 7"

- \(bu
FreeBSD 7 no longer contains */usr/bin/objformat*. At build time,
Perl now skips the *objformat* check for versions 7 and higher and
assumes \s-1ELF.\s0

### \s-1VMS\s0

Subsection "VMS"

- \(bu
It's now possible to build extensions on older (pre 7.3-2) \s-1VMS\s0 systems.
.Sp
\s-1DCL\s0 symbol length was limited to 1K up until about seven years or
so ago, but there was no particularly deep reason to prevent those
older systems from configuring and building Perl.

- \(bu
We fixed the previously-broken \f(CW\*(C`-Uuseperlio\*(C' build on \s-1VMS.\s0
.Sp
We were checking a variable that doesn't exist in the non-default
case of disabling perlio.  Now we only look at it when it exists.

- \(bu
We fixed the -Uuseperlio command-line option in configure.com.
.Sp
Formerly it only worked if you went through all the questions
interactively and explicitly answered no.

## Known Problems

Header "Known Problems"

- \(bu
\f(CW\*(C`List::Util::first\*(C' misbehaves in the presence of a lexical \f(CW$_
(typically introduced by \f(CW\*(C`my $_\*(C' or implicitly by \f(CW\*(C`given\*(C'). The variable
which gets set for each iteration is the package variable \f(CW$_, not the
lexical \f(CW$_.
.Sp
A similar issue may occur in other modules that provide functions which
take a block as their first argument, like
.Sp
.Vb 1
    foo \{ ... $_ ...\} list
.Ve
.Sp
See also: <https://github.com/Perl/perl5/issues/9798>

- \(bu
\f(CW\*(C`Module::Load::Conditional\*(C' and \f(CW\*(C`version\*(C' have an unfortunate
interaction which can cause \f(CW\*(C`CPANPLUS\*(C' to crash when it encounters
an unparseable version string.  Upgrading to \f(CW\*(C`CPANPLUS\*(C' 0.9004 or
\f(CW\*(C`Module::Load::Conditional\*(C' 0.38 from \s-1CPAN\s0 will resolve this issue.

## Acknowledgements

Header "Acknowledgements"
Perl 5.12.1 represents approximately four weeks of development since
Perl 5.12.0 and contains approximately 4,000 lines of changes
across 142 files from 28 authors.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers.  The following people are known to
have contributed the improvements that became Perl 5.12.1:

\*(Aevar Arnfjo\*:r\*(d- Bjarmason, Chris Williams, chromatic, Craig A. Berry,
David Golden, Father Chrysostomos, Florian Ragwitz, Frank Wiegand,
Gene Sullivan, Goro Fuji, H.Merijn Brand, James E Keenan, Jan Dubois,
Jesse Vincent, Josh ben Jore, Karl Williamson, Leon Brocard, Michael
Schwern, Nga Tang Chan, Nicholas Clark, Niko Tyni, Philippe Bruhat,
Rafael Garcia-Suarez, Ricardo Signes, Steffen Mueller, Todd Rinaldo,
Vincent Pit and Zefram.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes
all the core committers, who will be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for
security issues in the Perl core, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
