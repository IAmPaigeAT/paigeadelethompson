+++
operating_system_version = "15.3"
title = "ldapexop(1)"
manpage_section = "1"
detected_package_version = "0"
date = "Sun Feb 16 04:48:20 2025"
keywords = ["ldap_extended_operation_s", "3", "author", "this", "manual", "page", "was", "written", "by", "peter", "marschall", "based", "on", "fbldapexop", "s", "usage", "message", "and", "a", "few", "tests", "with", "do", "not", "expect", "it", "to", "be", "complete", "or", "absolutely", "correct", "acknowledgements", "shared", "project", "acknowledgement", "text", "openldap", "software", "is", "developed", "maintained", "the", "http", "www", "org", "derived", "from", "university", "of", "michigan", "ldap", "release"]
author = "Peter Marschall"
description = "ldapexop issues the LDAP extended operation specified by oid or one of the special keywords whoami, cancel, or refresh."
manpage_name = "ldapexop"
operating_system = "macos"
manpage_format = "troff"
+++


LDAPEXOP 1


## NAME

ldapexop - issue LDAP extended operations


## SYNOPSIS

ldapexop
[\c
-d \ level]
[\c
-D \ binddn]
[\c
-e \ [ ! ] *ext* [ =*extparam* ]]
[\c
-f \ file]
[\c
-h \ host]
[\c
-H \ URI]
[\c
-I ]
[\c
-n ]
[\c
-N ]
[\c
-O \ security-properties]
[\c
-o \ opt[ = optparam]]
[\c
-p \ port]
[\c
-Q ]
[\c
-R \ realm]
[\c
-U \ authcid]
[\c
-v ]
[\c
-V ]
[\c
-w \ passwd]
[\c
-W ]
[\c
-x ]
[\c
-X \ authzid]
[\c
-y \ file]
[\c
-Y \ mech]
[\c
-Z [ Z ]]
\{\c
oid
|
oid: data
|
oid:: b64data
|
whoami
|
cancel \ cancel-id
|
refresh \ DN \ [*ttl*]\}


## DESCRIPTION

ldapexop issues the LDAP extended operation specified by **oid**
or one of the special keywords **whoami**, **cancel**, or **refresh**.

Additional data for the extended operation can be passed to the server using
*data* or base-64 encoded as *b64data* in the case of **oid**,
or using the additional parameters in the case of the specially named extended
operations above.

Please note that ldapexop behaves differently for the same extended operation
when it was given as an OID or as a specialliy named operation:

Calling ldapexop with the OID of the **whoami** (RFC 4532) extended operation

```

  ldapexop [<options>] 1.3.6.1.4.1.4203.1.11.3

```

yields

```

  # extended operation response
  data:: <base64 encoded response data>

```

while calling it with the keyword **whoami**

```

  ldapexop [<options>] whoami

```

results in

```

  dn:<client's identity>

```




## OPTIONS


-d \ level
Set the LDAP debugging level to *level*.

-D \ binddn
Use the Distinguished Name *binddn* to bind to the LDAP directory.

-e \ [ ! ] *ext* [ =*extparam* ]
Specify general extensions.  \'!\' indicates criticality.

```
  [!]assert=<filter>     (RFC 4528; a RFC 4515 Filter string)
  [!]authzid=<authzid>   (RFC 4370; "dn:<dn>" or "u:<user>")
  [!]chaining[=<resolveBehavior>[/<continuationBehavior>]]
     one of "chainingPreferred", "chainingRequired",
     "referralsPreferred", "referralsRequired"
  [!]manageDSAit         (RFC 3296)
  [!]noop
  ppolicy
  [!]postread[=<attrs>]  (RFC 4527; comma-separated attr list)
  [!]preread[=<attrs>]   (RFC 4527; comma-separated attr list)
  [!]relax
  abandon, cancel, ignore (SIGINT sends abandon/cancel,
  or ignores response; if critical, doesn't wait for SIGINT.
  not really controls)
```


-f \ file
Read operations from *file*.

-h \ host
Specify the host on which the ldap server is running.
Deprecated in favor of **-H**.

-H \ URI
Specify URI(s) referring to the ldap server(s); only the protocol/host/port
fields are allowed; a list of URI, separated by whitespace or commas
is expected.

-I
Enable SASL Interactive mode.  Always prompt.  Default is to prompt
only as needed.

-n
Show what would be done but don't actually do it.
Useful for debugging in conjunction with **-v**.

-N
Do not use reverse DNS to canonicalize SASL host name.

-O \ security-properties
Specify SASL security properties.

-o \ opt[ = optparam]
Specify general options:

```
  nettimeout=<timeout> (in seconds, or "none" or "max")
```


-p \ port
Specify the TCP port where the ldap server is listening.
Deprecated in favor of **-H**.

-Q
Enable SASL Quiet mode.  Never prompt.

-R \ realm
Specify the realm of authentication ID for SASL bind. The form of the realm
depends on the actual SASL mechanism used.

-U \ authcid
Specify the authentication ID for SASL bind. The form of the ID
depends on the actual SASL mechanism used.

-v
Run in verbose mode, with many diagnostics written to standard output.

-V
Print version info and usage message.
If**-VV** is given, only the version information is printed.

-w \ passwd
Use *passwd* as the password for simple authentication.

-W
Prompt for simple authentication.
This is used instead of specifying the password on the command line.

-x
Use simple authentication instead of SASL.

-X \ authzid
Specify the requested authorization ID for SASL bind.
authzid
must be one of the following formats:
dn: "<distinguished name>"
or
u: <username>

-y \ file
Use complete contents of *file* as the password for
simple authentication.

-Y \ mech
Specify the SASL mechanism to be used for authentication.
Without this option, the program will choose the best mechanism the server knows.

-Z [ Z ]
Issue StartTLS (Transport Layer Security) extended operation.
Giving it twice (**-ZZ**) will require the operation to be successful.


## DIAGNOSTICS

Exit status is zero if no errors occur.
Errors result in a non-zero exit status and
a diagnostic message being written to standard error.


## SEE ALSO

ldap_extended_operation_s (3)


## AUTHOR

This manual page was written by Peter Marschall
based on **ldapexop**'s usage message and a few tests
with **ldapexop**.
Do not expect it to be complete or absolutely correct.


## ACKNOWLEDGEMENTS

"OpenLDAP Software"
is developed and maintained by The OpenLDAP Project <http://www.openldap.org/>.
"OpenLDAP Software"
is derived from University of Michigan LDAP 3.3 Release.

