+++
date = "2022-02-19"
manpage_format = "troff"
operating_system = "macos"
author = "None Specified"
manpage_name = "perlfaq3"
description = "This section of the s-1FAQs0 answers questions related to programmer tools and programming support. Have you looked at s-1CPANs0 (see perlfaq2)? The chances are that someone has already written a module that can solve your problem. Have you read t..."
operating_system_version = "15.3"
detected_package_version = "5.34.1"
manpage_section = "1"
title = "perlfaq3(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLFAQ3 1"
PERLFAQ3 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlfaq3 - Programming Tools

## VERSION

Header "VERSION"
version 5.20210411

## DESCRIPTION

Header "DESCRIPTION"
This section of the \s-1FAQ\s0 answers questions related to programmer tools
and programming support.

### How do I do (anything)?

Subsection "How do I do (anything)?"
Have you looked at \s-1CPAN\s0 (see perlfaq2)? The chances are that
someone has already written a module that can solve your problem.
Have you read the appropriate manpages? Here's a brief index:

- Basics
Item "Basics"

> 0

- perldata - Perl data types
Item "perldata - Perl data types"

- perlvar - Perl pre-defined variables
Item "perlvar - Perl pre-defined variables"

- perlsyn - Perl syntax
Item "perlsyn - Perl syntax"

- perlop - Perl operators and precedence
Item "perlop - Perl operators and precedence"

- perlsub - Perl subroutines
Item "perlsub - Perl subroutines"



> 


- Execution
Item "Execution"

> 
- perlrun - how to execute the Perl interpreter
Item "perlrun - how to execute the Perl interpreter"

- perldebug - Perl debugging
Item "perldebug - Perl debugging"



> 


- Functions
Item "Functions"

> 
- perlfunc - Perl builtin functions
Item "perlfunc - Perl builtin functions"



> 


- Objects
Item "Objects"

> 
- perlref - Perl references and nested data structures
Item "perlref - Perl references and nested data structures"

- perlmod - Perl modules (packages and symbol tables)
Item "perlmod - Perl modules (packages and symbol tables)"

- perlobj - Perl objects
Item "perlobj - Perl objects"

- perltie - how to hide an object class in a simple variable
Item "perltie - how to hide an object class in a simple variable"



> 


- Data Structures
Item "Data Structures"

> 
- perlref - Perl references and nested data structures
Item "perlref - Perl references and nested data structures"

- perllol - Manipulating arrays of arrays in Perl
Item "perllol - Manipulating arrays of arrays in Perl"

- perldsc - Perl Data Structures Cookbook
Item "perldsc - Perl Data Structures Cookbook"



> 


- Modules
Item "Modules"

> 
- perlmod - Perl modules (packages and symbol tables)
Item "perlmod - Perl modules (packages and symbol tables)"

- perlmodlib - constructing new Perl modules and finding existing ones
Item "perlmodlib - constructing new Perl modules and finding existing ones"



> 


- Regexes
Item "Regexes"

> 
- perlre - Perl regular expressions
Item "perlre - Perl regular expressions"

- perlfunc - Perl builtin functions>
Item "perlfunc - Perl builtin functions>"

- perlop - Perl operators and precedence
Item "perlop - Perl operators and precedence"

- perllocale - Perl locale handling (internationalization and localization)
Item "perllocale - Perl locale handling (internationalization and localization)"



> 


- Moving to perl5
Item "Moving to perl5"

> 
- perltrap - Perl traps for the unwary
Item "perltrap - Perl traps for the unwary"

- perl
Item "perl"



> 


- Linking with C
Item "Linking with C"

> 
- perlxstut - Tutorial for writing XSUBs
Item "perlxstut - Tutorial for writing XSUBs"

- perlxs - \s-1XS\s0 language reference manual
Item "perlxs - XS language reference manual"

- perlcall - Perl calling conventions from C
Item "perlcall - Perl calling conventions from C"

- perlguts - Introduction to the Perl \s-1API\s0
Item "perlguts - Introduction to the Perl API"

- perlembed - how to embed perl in your C program
Item "perlembed - how to embed perl in your C program"



> 


- Various
Item "Various"
.PD
<http://www.cpan.org/misc/olddoc/FMTEYEWTK.tgz>
(not a man-page but still useful, a collection of various essays on
Perl techniques)

A crude table of contents for the Perl manpage set is found in perltoc.

### How can I use Perl interactively?

Subsection "How can I use Perl interactively?"
The typical approach uses the Perl debugger, described in the
**perldebug**\|(1) manpage, on an \*(L"empty\*(R" program, like this:

.Vb 1
    perl -de 42
.Ve

Now just type in any legal Perl code, and it will be immediately
evaluated. You can also examine the symbol table, get stack
backtraces, check variable values, set breakpoints, and other
operations typically found in symbolic debuggers.

You can also use Devel::REPL which is an interactive shell for Perl,
commonly known as a \s-1REPL\s0 - Read, Evaluate, Print, Loop. It provides
various handy features.

### How do I find which modules are installed on my system?

Subsection "How do I find which modules are installed on my system?"
From the command line, you can use the \f(CW\*(C`cpan\*(C' command's \f(CW\*(C`-l\*(C' switch:

.Vb 1
    $ cpan -l
.Ve

You can also use \f(CW\*(C`cpan\*(C''s \f(CW\*(C`-a\*(C' switch to create an autobundle file
that \f(CW\*(C`CPAN.pm\*(C' understands and can use to re-install every module:

.Vb 1
    $ cpan -a
.Ve

Inside a Perl program, you can use the ExtUtils::Installed module to
show all installed distributions, although it can take awhile to do
its magic. The standard library which comes with Perl just shows up
as \*(L"Perl\*(R" (although you can get those with Module::CoreList).

.Vb 1
    use ExtUtils::Installed;

    my $inst    = ExtUtils::Installed->new();
    my @modules = $inst->modules();
.Ve

If you want a list of all of the Perl module filenames, you
can use File::Find::Rule:

.Vb 1
    use File::Find::Rule;

    my @files = File::Find::Rule->
        extras(\{follow => 1\})->
        file()->
        name( \*(Aq*.pm\*(Aq )->
        in( @INC )
        ;
.Ve

If you do not have that module, you can do the same thing
with File::Find which is part of the standard library:

.Vb 2
    use File::Find;
    my @files;

    find(
        \{
        wanted => sub \{
            push @files, $File::Find::fullname
            if -f $File::Find::fullname && /\\.pm$/
        \},
        follow => 1,
        follow_skip => 2,
        \},
        @INC
    );

    print join "\\n", @files;
.Ve

If you simply need to check quickly to see if a module is
available, you can check for its documentation. If you can
read the documentation the module is most likely installed.
If you cannot read the documentation, the module might not
have any (in rare cases):

.Vb 1
    $ perldoc Module::Name
.Ve

You can also try to include the module in a one-liner to see if
perl finds it:

.Vb 1
    $ perl -MModule::Name -e1
.Ve

(If you don't receive a \*(L"Can't locate ... in \f(CW@INC\*(R" error message, then Perl
found the module name you asked for.)

### How do I debug my Perl programs?

Subsection "How do I debug my Perl programs?"
(contributed by brian d foy)

Before you do anything else, you can help yourself by ensuring that
you let Perl tell you about problem areas in your code. By turning
on warnings and strictures, you can head off many problems before
they get too big. You can find out more about these in strict
and warnings.

.Vb 3
    #!/usr/bin/perl
    use strict;
    use warnings;
.Ve

Beyond that, the simplest debugger is the \f(CW\*(C`print\*(C' function. Use it
to look at values as you run your program:

.Vb 1
    print STDERR "The value is [$value]\\n";
.Ve

The Data::Dumper module can pretty-print Perl data structures:

.Vb 2
    use Data::Dumper qw( Dumper );
    print STDERR "The hash is " . Dumper( \\%hash ) . "\\n";
.Ve

Perl comes with an interactive debugger, which you can start with the
\f(CW\*(C`-d\*(C' switch. It's fully explained in perldebug.

If you'd like a graphical user interface and you have Tk, you can use
\f(CW\*(C`ptkdb\*(C'. It's on \s-1CPAN\s0 and available for free.

If you need something much more sophisticated and controllable, Leon
Brocard's Devel::ebug (which you can call with the \f(CW\*(C`-D\*(C' switch as \f(CW\*(C`-Debug\*(C')
gives you the programmatic hooks into everything you need to write your
own (without too much pain and suffering).

You can also use a commercial debugger such as Affrus (Mac \s-1OS X\s0), Komodo
from Activestate (Windows and Mac \s-1OS X\s0), or \s-1EPIC\s0 (most platforms).

### How do I profile my Perl programs?

Subsection "How do I profile my Perl programs?"
(contributed by brian d foy, updated Fri Jul 25 12:22:26 \s-1PDT 2008\s0)

The \f(CW\*(C`Devel\*(C' namespace has several modules which you can use to
profile your Perl programs.

The Devel::NYTProf (New York Times Profiler) does both statement
and subroutine profiling. It's available from \s-1CPAN\s0 and you also invoke
it with the \f(CW\*(C`-d\*(C' switch:

.Vb 1
    perl -d:NYTProf some_perl.pl
.Ve

It creates a database of the profile information that you can turn into
reports. The \f(CW\*(C`nytprofhtml\*(C' command turns the data into an \s-1HTML\s0 report
similar to the Devel::Cover report:

.Vb 1
    nytprofhtml
.Ve

You might also be interested in using the Benchmark to
measure and compare code snippets.

You can read more about profiling in *Programming Perl*, chapter 20,
or *Mastering Perl*, chapter 5.

perldebguts documents creating a custom debugger if you need to
create a special sort of profiler. brian d foy describes the process
in *The Perl Journal*, \*(L"Creating a Perl Debugger\*(R",
<http://www.ddj.com/184404522> , and \*(L"Profiling in Perl\*(R"
<http://www.ddj.com/184404580> .

Perl.com has two interesting articles on profiling: \*(L"Profiling Perl\*(R",
by Simon Cozens, <https://www.perl.com/pub/2004/06/25/profiling.html/>
and \*(L"Debugging and Profiling mod_perl Applications\*(R", by Frank Wiles,
<http://www.perl.com/pub/a/2006/02/09/debug_mod_perl.html> .

Randal L. Schwartz writes about profiling in \*(L"Speeding up Your Perl
Programs\*(R" for *Unix Review*,
<http://www.stonehenge.com/merlyn/UnixReview/col49.html> , and \*(L"Profiling
in Template Toolkit via Overriding\*(R" for *Linux Magazine*,
<http://www.stonehenge.com/merlyn/LinuxMag/col75.html> .

### How do I cross-reference my Perl programs?

Subsection "How do I cross-reference my Perl programs?"
The B::Xref module can be used to generate cross-reference reports
for Perl programs.

.Vb 1
    perl -MO=Xref[,OPTIONS] scriptname.plx
.Ve

### Is there a pretty-printer (formatter) for Perl?

Subsection "Is there a pretty-printer (formatter) for Perl?"
Perl::Tidy comes with a perl script perltidy which indents and
reformats Perl scripts to make them easier to read by trying to follow
the rules of the perlstyle. If you write Perl, or spend much time reading
Perl, you will probably find it useful.

Of course, if you simply follow the guidelines in perlstyle,
you shouldn't need to reformat. The habit of formatting your code
as you write it will help prevent bugs. Your editor can and should
help you with this. The perl-mode or newer cperl-mode for emacs
can provide remarkable amounts of help with most (but not all)
code, and even less programmable editors can provide significant
assistance. Tom Christiansen and many other \s-1VI\s0 users swear by
the following settings in vi and its clones:

.Vb 2
    set ai sw=4
    map! ^O \{^M\}^[O^T
.Ve

Put that in your *.exrc* file (replacing the caret characters
with control characters) and away you go. In insert mode, ^T is
for indenting, ^D is for undenting, and ^O is for blockdenting\*(--as
it were. A more complete example, with comments, can be found at
<http://www.cpan.org/authors/id/T/TO/TOMC/scripts/toms.exrc.gz>

### Is there an \s-1IDE\s0 or Windows Perl Editor?

Subsection "Is there an IDE or Windows Perl Editor?"
Perl programs are just plain text, so any editor will do.

If you're on Unix, you already have an IDE\*(--Unix itself. The Unix
philosophy is the philosophy of several small tools that each do one
thing and do it well. It's like a carpenter's toolbox.

If you want an \s-1IDE,\s0 check the following (in alphabetical order, not
order of preference):

- Eclipse
Item "Eclipse"
<http://e-p-i-c.sf.net/>
.Sp
The Eclipse Perl Integration Project integrates Perl
editing/debugging with Eclipse.

- Enginsite
Item "Enginsite"
<http://www.enginsite.com/>
.Sp
Perl Editor by EngInSite is a complete integrated development
environment (\s-1IDE\s0) for creating, testing, and  debugging  Perl scripts;
the tool runs on Windows 9x/NT/2000/XP or later.

- IntelliJ \s-1IDEA\s0
Item "IntelliJ IDEA"
<https://plugins.jetbrains.com/plugin/7796>
.Sp
Camelcade plugin provides Perl5 support in IntelliJ \s-1IDEA\s0 and other JetBrains IDEs.

- Kephra
Item "Kephra"
<http://kephra.sf.net>
.Sp
\s-1GUI\s0 editor written in Perl using wxWidgets and Scintilla with lots of smaller features.
Aims for a \s-1UI\s0 based on Perl principles like \s-1TIMTOWTDI\s0 and \*(L"easy things should be easy,
hard things should be possible\*(R".

- Komodo
Item "Komodo"
<http://www.ActiveState.com/Products/Komodo/>
.Sp
ActiveState's cross-platform (as of October 2004, that's Windows, Linux,
and Solaris), multi-language \s-1IDE\s0 has Perl support, including a regular expression
debugger and remote debugging.

- Notepad++
Item "Notepad++"
<http://notepad-plus.sourceforge.net/>

- Open Perl \s-1IDE\s0
Item "Open Perl IDE"
<http://open-perl-ide.sourceforge.net/>
.Sp
Open Perl \s-1IDE\s0 is an integrated development environment for writing
and debugging Perl scripts with ActiveState's ActivePerl distribution
under Windows 95/98/NT/2000.

- OptiPerl
Item "OptiPerl"
<http://www.optiperl.com/>
.Sp
OptiPerl is a Windows \s-1IDE\s0 with simulated \s-1CGI\s0 environment, including
debugger and syntax-highlighting editor.

- Padre
Item "Padre"
<http://padre.perlide.org/>
.Sp
Padre is cross-platform \s-1IDE\s0 for Perl written in Perl using wxWidgets to provide
a native look and feel. It's open source under the Artistic License. It
is one of the newer Perl IDEs.

- PerlBuilder
Item "PerlBuilder"
<http://www.solutionsoft.com/perl.htm>
.Sp
PerlBuilder is an integrated development environment for Windows that
supports Perl development.

- visiPerl+
Item "visiPerl+"
<http://helpconsulting.net/visiperl/index.html>
.Sp
From Help Consulting, for Windows.

- Visual Perl
Item "Visual Perl"
<http://www.activestate.com/Products/Visual_Perl/>
.Sp
Visual Perl is a Visual Studio.NET plug-in from ActiveState.

- Zeus
Item "Zeus"
<http://www.zeusedit.com/lookmain.html>
.Sp
Zeus for Windows is another Win32 multi-language editor/IDE
that comes with support for Perl.

For editors: if you're on Unix you probably have vi or a vi clone
already, and possibly an emacs too, so you may not need to download
anything. In any emacs the cperl-mode (M-x cperl-mode) gives you
perhaps the best available Perl editing mode in any editor.

If you are using Windows, you can use any editor that lets you work
with plain text, such as NotePad or WordPad. Word processors, such as
Microsoft Word or WordPerfect, typically do not work since they insert
all sorts of behind-the-scenes information, although some allow you to
save files as \*(L"Text Only\*(R". You can also download text editors designed
specifically for programming, such as Textpad (
<http://www.textpad.com/> ) and UltraEdit ( <http://www.ultraedit.com/> ),
among others.

If you are using MacOS, the same concerns apply. MacPerl (for Classic
environments) comes with a simple editor. Popular external editors are
BBEdit ( <http://www.barebones.com/products/bbedit/> ) or Alpha (
<http://www.his.com/~jguyer/Alpha/Alpha8.html> ). MacOS X users can use
Unix editors as well.

- \s-1GNU\s0 Emacs
Item "GNU Emacs"
<http://www.gnu.org/software/emacs/windows/ntemacs.html>

- MicroEMACS
Item "MicroEMACS"
<http://www.microemacs.de/>

- XEmacs
Item "XEmacs"
<http://www.xemacs.org/Download/index.html>

- Jed
Item "Jed"
<http://space.mit.edu/~davis/jed/>

or a vi clone such as

- Vim
Item "Vim"
<http://www.vim.org/>

- Vile
Item "Vile"
<http://invisible-island.net/vile/vile.html>

The following are Win32 multilanguage editor/IDEs that support Perl:

- MultiEdit
Item "MultiEdit"
<http://www.MultiEdit.com/>

- SlickEdit
Item "SlickEdit"
<http://www.slickedit.com/>

- ConTEXT
Item "ConTEXT"
<http://www.contexteditor.org/>

There is also a toyedit Text widget based editor written in Perl
that is distributed with the Tk module on \s-1CPAN.\s0 The ptkdb
( <http://ptkdb.sourceforge.net/> ) is a Perl/Tk-based debugger that
acts as a development environment of sorts. Perl Composer
( <http://perlcomposer.sourceforge.net/> ) is an \s-1IDE\s0 for Perl/Tk
\s-1GUI\s0 creation.

In addition to an editor/IDE you might be interested in a more
powerful shell environment for Win32. Your options include

- bash
Item "bash"
from the Cygwin package ( <http://cygwin.com/> )

- zsh
Item "zsh"
<http://www.zsh.org/>

Cygwin is covered by the \s-1GNU\s0 General Public
License (but that shouldn't matter for Perl use). Cygwin
contains (in addition to the shell) a comprehensive set
of standard Unix toolkit utilities.

- BBEdit and TextWrangler
Item "BBEdit and TextWrangler"
are text editors for \s-1OS X\s0 that have a Perl sensitivity mode
( <http://www.barebones.com/> ).

### Where can I get Perl macros for vi?

Subsection "Where can I get Perl macros for vi?"
For a complete version of Tom Christiansen's vi configuration file,
see <http://www.cpan.org/authors/id/T/TO/TOMC/scripts/toms.exrc.gz> ,
the standard benchmark file for vi emulators. The file runs best with nvi,
the current version of vi out of Berkeley, which incidentally can be built
with an embedded Perl interpreter\*(--see <http://www.cpan.org/src/misc/> .

### Where can I get perl-mode or cperl-mode for emacs?

Xref "emacs"
Subsection "Where can I get perl-mode or cperl-mode for emacs?"
Since Emacs version 19 patchlevel 22 or so, there have been both a
perl-mode.el and support for the Perl debugger built in. These should
come with the standard Emacs 19 distribution.

Note that the perl-mode of emacs will have fits with \f(CW"main\*(Aqfoo"
(single quote), and mess up the indentation and highlighting. You
are probably using \f(CW"main::foo" in new Perl code anyway, so this
shouldn't be an issue.

For CPerlMode, see <http://www.emacswiki.org/cgi-bin/wiki/CPerlMode>

### How can I use curses with Perl?

Subsection "How can I use curses with Perl?"
The Curses module from \s-1CPAN\s0 provides a dynamically loadable object
module interface to a curses library. A small demo can be found at the
directory <http://www.cpan.org/authors/id/T/TO/TOMC/scripts/rep.gz> ;
this program repeats a command and updates the screen as needed, rendering
**rep ps axu** similar to **top**.

### How can I write a \s-1GUI\s0 (X, Tk, Gtk, etc.) in Perl?

Xref "GUI Tk Wx WxWidgets Gtk Gtk2 CamelBones Qt"
Subsection "How can I write a GUI (X, Tk, Gtk, etc.) in Perl?"
(contributed by Ben Morrow)

There are a number of modules which let you write GUIs in Perl. Most
\s-1GUI\s0 toolkits have a perl interface: an incomplete list follows.

- Tk
Item "Tk"
This works under Unix and Windows, and the current version doesn't
look half as bad under Windows as it used to. Some of the gui elements
still don't 'feel' quite right, though. The interface is very natural
and 'perlish', making it easy to use in small scripts that just need a
simple gui. It hasn't been updated in a while.

- Wx
Item "Wx"
This is a Perl binding for the cross-platform wxWidgets toolkit
( <http://www.wxwidgets.org> ). It works under Unix, Win32 and Mac \s-1OS X,\s0
using native widgets (Gtk under Unix). The interface follows the \*(C+
interface closely, but the documentation is a little sparse for someone
who doesn't know the library, mostly just referring you to the \*(C+
documentation.

- Gtk and Gtk2
Item "Gtk and Gtk2"
These are Perl bindings for the Gtk toolkit ( <http://www.gtk.org> ). The
interface changed significantly between versions 1 and 2 so they have
separate Perl modules. It runs under Unix, Win32 and Mac \s-1OS X\s0 (currently
it requires an X server on Mac \s-1OS,\s0 but a 'native' port is underway), and
the widgets look the same on every platform: i.e., they don't match the
native widgets. As with Wx, the Perl bindings follow the C \s-1API\s0 closely,
and the documentation requires you to read the C documentation to
understand it.

- Win32::GUI
Item "Win32::GUI"
This provides access to most of the Win32 \s-1GUI\s0 widgets from Perl.
Obviously, it only runs under Win32, and uses native widgets. The Perl
interface doesn't really follow the C interface: it's been made more
Perlish, and the documentation is pretty good. More advanced stuff may
require familiarity with the C Win32 APIs, or reference to \s-1MSDN.\s0

- CamelBones
Item "CamelBones"
CamelBones ( <http://camelbones.sourceforge.net> ) is a Perl interface to
Mac \s-1OS X\s0's Cocoa \s-1GUI\s0 toolkit, and as such can be used to produce native
GUIs on Mac \s-1OS X.\s0 It's not on \s-1CPAN,\s0 as it requires frameworks that
\s-1CPAN\s0.pm doesn't know how to install, but installation is via the
standard \s-1OSX\s0 package installer. The Perl \s-1API\s0 is, again, very close to
the ObjC \s-1API\s0 it's wrapping, and the documentation just tells you how to
translate from one to the other.

- Qt
Item "Qt"
There is a Perl interface to TrollTech's Qt toolkit, but it does not
appear to be maintained.

- Athena
Item "Athena"
Sx is an interface to the Athena widget set which comes with X, but
again it appears not to be much used nowadays.

### How can I make my Perl program run faster?

Subsection "How can I make my Perl program run faster?"
The best way to do this is to come up with a better algorithm. This
can often make a dramatic difference. Jon Bentley's book
*Programming Pearls* (that's not a misspelling!)  has some good tips
on optimization, too. Advice on benchmarking boils down to: benchmark
and profile to make sure you're optimizing the right part, look for
better algorithms instead of microtuning your code, and when all else
fails consider just buying faster hardware. You will probably want to
read the answer to the earlier question \*(L"How do I profile my Perl
programs?\*(R" if you haven't done so already.

A different approach is to autoload seldom-used Perl code. See the
AutoSplit and AutoLoader modules in the standard distribution for
that. Or you could locate the bottleneck and think about writing just
that part in C, the way we used to take bottlenecks in C code and
write them in assembler. Similar to rewriting in C, modules that have
critical sections can be written in C (for instance, the \s-1PDL\s0 module
from \s-1CPAN\s0).

If you're currently linking your perl executable to a shared
*libc.so*, you can often gain a 10-25% performance benefit by
rebuilding it to link with a static libc.a instead. This will make a
bigger perl executable, but your Perl programs (and programmers) may
thank you for it. See the *\s-1INSTALL\s0* file in the source distribution
for more information.

The undump program was an ancient attempt to speed up Perl program by
storing the already-compiled form to disk. This is no longer a viable
option, as it only worked on a few architectures, and wasn't a good
solution anyway.

### How can I make my Perl program take less memory?

Subsection "How can I make my Perl program take less memory?"
When it comes to time-space tradeoffs, Perl nearly always prefers to
throw memory at a problem. Scalars in Perl use more memory than
strings in C, arrays take more than that, and hashes use even more. While
there's still a lot to be done, recent releases have been addressing
these issues. For example, as of 5.004, duplicate hash keys are
shared amongst all hashes using them, so require no reallocation.

In some cases, using **substr()** or **vec()** to simulate arrays can be
highly beneficial. For example, an array of a thousand booleans will
take at least 20,000 bytes of space, but it can be turned into one
125-byte bit vector\*(--a considerable memory savings. The standard
Tie::SubstrHash module can also help for certain types of data
structure. If you're working with specialist data structures
(matrices, for instance) modules that implement these in C may use
less memory than equivalent Perl modules.

Another thing to try is learning whether your Perl was compiled with
the system malloc or with Perl's builtin malloc. Whichever one it
is, try using the other one and see whether this makes a difference.
Information about malloc is in the *\s-1INSTALL\s0* file in the source
distribution. You can find out whether you are using perl's malloc by
typing \f(CW\*(C`perl -V:usemymalloc\*(C'.

Of course, the best way to save memory is to not do anything to waste
it in the first place. Good programming practices can go a long way
toward this:

- Don't slurp!
Item "Don't slurp!"
Don't read an entire file into memory if you can process it line
by line. Or more concretely, use a loop like this:
.Sp
.Vb 6
    #
    # Good Idea
    #
    while (my $line = <$file_handle>) \{
       # ...
    \}
.Ve
.Sp
instead of this:
.Sp
.Vb 7
    #
    # Bad Idea
    #
    my @data = <$file_handle>;
    foreach (@data) \{
        # ...
    \}
.Ve
.Sp
When the files you're processing are small, it doesn't much matter which
way you do it, but it makes a huge difference when they start getting
larger.

- Use map and grep selectively
Item "Use map and grep selectively"
Remember that both map and grep expect a \s-1LIST\s0 argument, so doing this:
.Sp
.Vb 1
        @wanted = grep \{/pattern/\} <$file_handle>;
.Ve
.Sp
will cause the entire file to be slurped. For large files, it's better
to loop:
.Sp
.Vb 3
        while (<$file_handle>) \{
                push(@wanted, $_) if /pattern/;
        \}
.Ve

- Avoid unnecessary quotes and stringification
Item "Avoid unnecessary quotes and stringification"
Don't quote large strings unless absolutely necessary:
.Sp
.Vb 1
        my $copy = "$large_string";
.Ve
.Sp
makes 2 copies of \f(CW$large_string (one for \f(CW$copy and another for the
quotes), whereas
.Sp
.Vb 1
        my $copy = $large_string;
.Ve
.Sp
only makes one copy.
.Sp
Ditto for stringifying large arrays:
.Sp
.Vb 4
    \{
    local $, = "\\n";
    print @big_array;
    \}
.Ve
.Sp
is much more memory-efficient than either
.Sp
.Vb 1
    print join "\\n", @big_array;
.Ve
.Sp
or
.Sp
.Vb 4
    \{
    local $" = "\\n";
    print "@big_array";
    \}
.Ve

- Pass by reference
Item "Pass by reference"
Pass arrays and hashes by reference, not by value. For one thing, it's
the only way to pass multiple lists or hashes (or both) in a single
call/return. It also avoids creating a copy of all the contents. This
requires some judgement, however, because any changes will be propagated
back to the original data. If you really want to mangle (er, modify) a
copy, you'll have to sacrifice the memory needed to make one.

- Tie large variables to disk
Item "Tie large variables to disk"
For \*(L"big\*(R" data stores (i.e. ones that exceed available memory) consider
using one of the \s-1DB\s0 modules to store it on disk instead of in \s-1RAM.\s0 This
will incur a penalty in access time, but that's probably better than
causing your hard disk to thrash due to massive swapping.

### Is it safe to return a reference to local or lexical data?

Subsection "Is it safe to return a reference to local or lexical data?"
Yes. Perl's garbage collection system takes care of this so
everything works out right.

.Vb 4
    sub makeone \{
        my @a = ( 1 .. 10 );
        return \\@a;
    \}

    for ( 1 .. 10 ) \{
        push @many, makeone();
    \}

    print $many[4][5], "\\n";

    print "@many\\n";
.Ve

### How can I free an array or hash so my program shrinks?

Subsection "How can I free an array or hash so my program shrinks?"
(contributed by Michael Carman)

You usually can't. Memory allocated to lexicals (i.e. **my()** variables)
cannot be reclaimed or reused even if they go out of scope. It is
reserved in case the variables come back into scope. Memory allocated
to global variables can be reused (within your program) by using
**undef()** and/or **delete()**.

On most operating systems, memory allocated to a program can never be
returned to the system. That's why long-running programs sometimes re-
exec themselves. Some operating systems (notably, systems that use
**mmap**\|(2) for allocating large chunks of memory) can reclaim memory that
is no longer used, but on such systems, perl must be configured and
compiled to use the \s-1OS\s0's malloc, not perl's.

In general, memory allocation and de-allocation isn't something you can
or should be worrying about much in Perl.

See also \*(L"How can I make my Perl program take less memory?\*(R"

### How can I make my \s-1CGI\s0 script more efficient?

Subsection "How can I make my CGI script more efficient?"
Beyond the normal measures described to make general Perl programs
faster or smaller, a \s-1CGI\s0 program has additional issues. It may be run
several times per second. Given that each time it runs it will need
to be re-compiled and will often allocate a megabyte or more of system
memory, this can be a killer. Compiling into C \fBisn't going to help
you because the process start-up overhead is where the bottleneck is.

There are three popular ways to avoid this overhead. One solution
involves running the Apache \s-1HTTP\s0 server (available from
<http://www.apache.org/> ) with either of the mod_perl or mod_fastcgi
plugin modules.

With mod_perl and the Apache::Registry module (distributed with
mod_perl), httpd will run with an embedded Perl interpreter which
pre-compiles your script and then executes it within the same address
space without forking. The Apache extension also gives Perl access to
the internal server \s-1API,\s0 so modules written in Perl can do just about
anything a module written in C can. For more on mod_perl, see
<http://perl.apache.org/>

With the \s-1FCGI\s0 module (from \s-1CPAN\s0) and the mod_fastcgi
module (available from <http://www.fastcgi.com/> ) each of your Perl
programs becomes a permanent \s-1CGI\s0 daemon process.

Finally, Plack is a Perl module and toolkit that contains \s-1PSGI\s0 middleware,
helpers and adapters to web servers, allowing you to easily deploy scripts which
can continue running, and provides flexibility with regards to which web server
you use. It can allow existing \s-1CGI\s0 scripts to enjoy this flexibility and
performance with minimal changes, or can be used along with modern Perl web
frameworks to make writing and deploying web services with Perl a breeze.

These solutions can have far-reaching effects on your system and on the way you
write your \s-1CGI\s0 programs, so investigate them with care.

See also
<http://www.cpan.org/modules/by-category/15_World_Wide_Web_HTML_HTTP_CGI/> .

### How can I hide the source for my Perl program?

Subsection "How can I hide the source for my Perl program?"
Delete it. :-) Seriously, there are a number of (mostly
unsatisfactory) solutions with varying levels of \*(L"security\*(R".

First of all, however, you *can't* take away read permission, because
the source code has to be readable in order to be compiled and
interpreted. (That doesn't mean that a \s-1CGI\s0 script's source is
readable by people on the web, though\*(--only by people with access to
the filesystem.)  So you have to leave the permissions at the socially
friendly 0755 level.

Some people regard this as a security problem. If your program does
insecure things and relies on people not knowing how to exploit those
insecurities, it is not secure. It is often possible for someone to
determine the insecure things and exploit them without viewing the
source. Security through obscurity, the name for hiding your bugs
instead of fixing them, is little security indeed.

You can try using encryption via source filters (Starting from Perl
5.8 the Filter::Simple and Filter::Util::Call modules are included in
the standard distribution), but any decent programmer will be able to
decrypt it. You can try using the byte code compiler and interpreter
described later in perlfaq3, but the curious might still be able to
de-compile it. You can try using the native-code compiler described
later, but crackers might be able to disassemble it. These pose
varying degrees of difficulty to people wanting to get at your code,
but none can definitively conceal it (true of every language, not just
Perl).

It is very easy to recover the source of Perl programs. You simply
feed the program to the perl interpreter and use the modules in
the B:: hierarchy. The B::Deparse module should be able to
defeat most attempts to hide source. Again, this is not
unique to Perl.

If you're concerned about people profiting from your code, then the
bottom line is that nothing but a restrictive license will give you
legal security. License your software and pepper it with threatening
statements like \*(L"This is unpublished proprietary software of \s-1XYZ\s0 Corp.
Your access to it does not give you permission to use it blah blah
blah.\*(R"  We are not lawyers, of course, so you should see a lawyer if
you want to be sure your license's wording will stand up in court.

### How can I compile my Perl program into byte code or C?

Subsection "How can I compile my Perl program into byte code or C?"
(contributed by brian d foy)

In general, you can't do this. There are some things that may work
for your situation though. People usually ask this question
because they want to distribute their works without giving away
the source code, and most solutions trade disk space for convenience.
You probably won't see much of a speed increase either, since most
solutions simply bundle a Perl interpreter in the final product
(but see \*(L"How can I make my Perl program run faster?\*(R").

The Perl Archive Toolkit is Perl's analog to Java's \s-1JAR.\s0 It's freely
available and on \s-1CPAN\s0 ( <https://metacpan.org/pod/PAR> ).

There are also some commercial products that may work for you, although
you have to buy a license for them.

The Perl Dev Kit ( <http://www.activestate.com/Products/Perl_Dev_Kit/> )
from ActiveState can \*(L"Turn your Perl programs into ready-to-run
executables for HP-UX, Linux, Solaris and Windows.\*(R"

Perl2Exe ( <http://www.indigostar.com/perl2exe.htm> ) is a command line
program for converting perl scripts to executable files. It targets both
Windows and Unix platforms.
.ie n .SS "How can I get ""#!perl"" to work on [\s-1MS-DOS,NT,...\s0]?"
.el .SS "How can I get \f(CW#!perl to work on [\s-1MS-DOS,NT,...\s0]?"
Subsection "How can I get #!perl to work on [MS-DOS,NT,...]?"
For \s-1OS/2\s0 just use

.Vb 1
    extproc perl -S -your_switches
.Ve

as the first line in \f(CW\*(C`*.cmd\*(C' file (\f(CW\*(C`-S\*(C' due to a bug in cmd.exe's
\*(L"extproc\*(R" handling). For \s-1DOS\s0 one should first invent a corresponding
batch file and codify it in \f(CW\*(C`ALTERNATE_SHEBANG\*(C' (see the
*dosish.h* file in the source distribution for more information).

The Win95/NT installation, when using the ActiveState port of Perl,
will modify the Registry to associate the \f(CW\*(C`.pl\*(C' extension with the
perl interpreter. If you install another port, perhaps even building
your own Win95/NT Perl from the standard sources by using a Windows port
of gcc (e.g., with cygwin or mingw32), then you'll have to modify
the Registry yourself. In addition to associating \f(CW\*(C`.pl\*(C' with the
interpreter, \s-1NT\s0 people can use: \f(CW\*(C`SET PATHEXT=%PATHEXT%;.PL\*(C' to let them
run the program \f(CW\*(C`install-linux.pl\*(C' merely by typing \f(CW\*(C`install-linux\*(C'.

Under \*(L"Classic\*(R" MacOS, a perl program will have the appropriate Creator and
Type, so that double-clicking them will invoke the MacPerl application.
Under Mac \s-1OS X,\s0 clickable apps can be made from any \f(CW\*(C`#!\*(C' script using Wil
Sanchez' DropScript utility: <http://www.wsanchez.net/software/> .

*\s-1IMPORTANT\s0!*: Whatever you do, \s-1PLEASE\s0 don't get frustrated, and just
throw the perl interpreter into your cgi-bin directory, in order to
get your programs working for a web server. This is an \s-1EXTREMELY\s0 big
security risk. Take the time to figure out how to do it correctly.

### Can I write useful Perl programs on the command line?

Subsection "Can I write useful Perl programs on the command line?"
Yes. Read perlrun for more information. Some examples follow.
(These assume standard Unix shell quoting rules.)

.Vb 2
    # sum first and last fields
    perl -lane \*(Aqprint $F[0] + $F[-1]\*(Aq *

    # identify text files
    perl -le \*(Aqfor(@ARGV) \{print if -f && -T _\}\*(Aq *

    # remove (most) comments from C program
    perl -0777 -pe \*(Aqs\{/\\*.*?\\*/\}\{\}gs\*(Aq foo.c

    # make file a month younger than today, defeating reaper daemons
    perl -e \*(Aq$X=24*60*60; utime(time(),time() + 30 * $X,@ARGV)\*(Aq *

    # find first unused uid
    perl -le \*(Aq$i++ while getpwuid($i); print $i\*(Aq

    # display reasonable manpath
    echo $PATH | perl -nl -072 -e \*(Aq
    s![^/+]*$!man!&&-d&&!$s\{$_\}++&&push@m,$_;END\{print"@m"\}\*(Aq
.Ve

\s-1OK,\s0 the last one was actually an Obfuscated Perl Contest entry. :-)

### Why dont Perl one-liners work on my DOS/Mac/VMS system?

Subsection "Why don't Perl one-liners work on my DOS/Mac/VMS system?"
The problem is usually that the command interpreters on those systems
have rather different ideas about quoting than the Unix shells under
which the one-liners were created. On some systems, you may have to
change single-quotes to double ones, which you must *\s-1NOT\s0* do on Unix
or Plan9 systems. You might also have to change a single % to a %%.

For example:

.Vb 2
    # Unix (including Mac OS X)
    perl -e \*(Aqprint "Hello world\\n"\*(Aq

    # DOS, etc.
    perl -e "print \\"Hello world\\n\\""

    # Mac Classic
    print "Hello world\\n"
     (then Run "Myscript" or Shift-Command-R)

    # MPW
    perl -e \*(Aqprint "Hello world\\n"\*(Aq

    # VMS
    perl -e "print ""Hello world\\n"""
.Ve

The problem is that none of these examples are reliable: they depend on the
command interpreter. Under Unix, the first two often work. Under \s-1DOS,\s0
it's entirely possible that neither works. If 4DOS was the command shell,
you'd probably have better luck like this:

.Vb 1
  perl -e "print <Ctrl-x>"Hello world\\n<Ctrl-x>""
.Ve

Under the Mac, it depends which environment you are using. The MacPerl
shell, or \s-1MPW,\s0 is much like Unix shells in its support for several
quoting variants, except that it makes free use of the Mac's non-ASCII
characters as control characters.

Using **qq()**, q(), and **qx()**, instead of \*(L"double quotes\*(R", 'single
quotes', and `backticks`, may make one-liners easier to write.

There is no general solution to all of this. It is a mess.

[Some of this answer was contributed by Kenneth Albanowski.]

### Where can I learn about \s-1CGI\s0 or Web programming in Perl?

Subsection "Where can I learn about CGI or Web programming in Perl?"
For modules, get the \s-1CGI\s0 or \s-1LWP\s0 modules from \s-1CPAN.\s0 For textbooks,
see the two especially dedicated to web stuff in the question on
books. For problems and questions related to the web, like \*(L"Why
do I get 500 Errors\*(R" or \*(L"Why doesn't it run from the browser right
when it runs fine on the command line\*(R", see the troubleshooting
guides and references in perlfaq9 or in the \s-1CGI\s0 MetaFAQ:

.Vb 1
    L<http://www.perl.org/CGI_MetaFAQ.html>
.Ve

Looking into <https://plackperl.org> and modern Perl web frameworks is highly recommended,
though; web programming in Perl has evolved a long way from the old days of
simple \s-1CGI\s0 scripts.

### Where can I learn about object-oriented Perl programming?

Subsection "Where can I learn about object-oriented Perl programming?"
A good place to start is perlootut, and you can use perlobj for
reference.

A good book on \s-1OO\s0 on Perl is the \*(L"Object-Oriented Perl\*(R"
by Damian Conway from Manning Publications, or \*(L"Intermediate Perl\*(R"
by Randal Schwartz, brian d foy, and Tom Phoenix from O'Reilly Media.

### Where can I learn about linking C with Perl?

Subsection "Where can I learn about linking C with Perl?"
If you want to call C from Perl, start with perlxstut,
moving on to perlxs, xsubpp, and perlguts. If you want to
call Perl from C, then read perlembed, perlcall, and
perlguts. Don't forget that you can learn a lot from looking at
how the authors of existing extension modules wrote their code and
solved their problems.

You might not need all the power of \s-1XS.\s0 The Inline::C module lets
you put C code directly in your Perl source. It handles all the
magic to make it work. You still have to learn at least some of
the perl \s-1API\s0 but you won't have to deal with the complexity of the
\s-1XS\s0 support files.

### Ive read perlembed, perlguts, etc., but I cant embed perl in my C program; what am I doing wrong?

Subsection "I've read perlembed, perlguts, etc., but I can't embed perl in my C program; what am I doing wrong?"
Download the ExtUtils::Embed kit from \s-1CPAN\s0 and run `make test'. If
the tests pass, read the pods again and again and again. If they
fail, submit a bug report to <https://github.com/Perl/perl5/issues>
with the output of
\f(CW\*(C`make test TEST_VERBOSE=1\*(C' along with \f(CW\*(C`perl -V\*(C'.

### When I tried to run my script, I got this message. What does it mean?

Subsection "When I tried to run my script, I got this message. What does it mean?"
A complete list of Perl's error messages and warnings with explanatory
text can be found in perldiag. You can also use the splain program
(distributed with Perl) to explain the error messages:

.Vb 2
    perl program 2>diag.out
    splain [-v] [-p] diag.out
.Ve

or change your program to explain the messages for you:

.Vb 1
    use diagnostics;
.Ve

or

.Vb 1
    use diagnostics -verbose;
.Ve

### Whats MakeMaker?

Subsection "What's MakeMaker?"
(contributed by brian d foy)

The ExtUtils::MakeMaker module, better known simply as \*(L"MakeMaker\*(R",
turns a Perl script, typically called \f(CW\*(C`Makefile.PL\*(C', into a Makefile.
The Unix tool \f(CW\*(C`make\*(C' uses this file to manage dependencies and actions
to process and install a Perl distribution.

## AUTHOR AND COPYRIGHT

Header "AUTHOR AND COPYRIGHT"
Copyright (c) 1997-2010 Tom Christiansen, Nathan Torkington, and
other authors as noted. All rights reserved.

This documentation is free; you can redistribute it and/or modify it
under the same terms as Perl itself.

Irrespective of its distribution, all code examples here are in the public
domain. You are permitted and encouraged to use this code and any
derivatives thereof in your own programs for fun or for profit as you
see fit. A simple comment in the code giving credit to the \s-1FAQ\s0 would
be courteous but is not required.
