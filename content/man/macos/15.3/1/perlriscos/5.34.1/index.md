+++
author = "None Specified"
manpage_format = "troff"
operating_system_version = "15.3"
operating_system = "macos"
manpage_section = "1"
date = "2022-02-19"
description = "This document gives instructions for building Perl for s-1RISC OS.s0 It is complicated by the need to cross compile. There is a binary version of perl available from <http://www.cp15.org/perl/> which you may wish to use instead of trying to compile..."
title = "perlriscos(1)"
detected_package_version = "5.34.1"
manpage_name = "perlriscos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLRISCOS 1"
PERLRISCOS 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlriscos - Perl version 5 for RISC OS

## DESCRIPTION

Header "DESCRIPTION"
This document gives instructions for building Perl for \s-1RISC OS.\s0 It is
complicated by the need to cross compile. There is a binary version of
perl available from <http://www.cp15.org/perl/> which you may wish to
use instead of trying to compile it yourself.

## BUILD

Header "BUILD"
You need an installed and working gccsdk cross compiler
<http://gccsdk.riscos.info/> and \s-1REXEN\s0
<http://www.cp15.org/programming/>

Firstly, copy the source and build a native copy of perl for your host
system.
Then, in the source to be cross compiled:

- 1.
.Vb 1
    $ ./Configure
.Ve

- 2.
Select the riscos hint file. The default answers for the rest of the
questions are usually sufficient.
.Sp
Note that, if you wish to run Configure non-interactively (see the \s-1INSTALL\s0
document for details), to have it select the correct hint file, you'll
need to provide the argument -Dhintfile=riscos on the Configure
command-line.

- 3.
.Vb 1
    $ make miniperl
.Ve

- 4.
This should build miniperl and then fail when it tries to run it.

- 5.
Copy the miniperl executable from the native build done earlier to
replace the cross compiled miniperl.

- 6.
.Vb 1
    $ make
.Ve

- 7.
This will use miniperl to complete the rest of the build.

## AUTHOR

Header "AUTHOR"
Alex Waugh <alex@alexwaugh.com>
