+++
date = "2022-02-26"
manpage_format = "troff"
operating_system = "macos"
manpage_name = "perlcommunity"
description = "This document aims to provide an overview of the vast perl community, which is far too large and diverse to provide a detailed listing. If any specific niche has been forgotten, it is not meant as an insult but an omission for the sake of brevity...."
operating_system_version = "15.3"
author = "None Specified"
manpage_section = "1"
title = "perlcommunity(1)"
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLCOMMUNITY 1"
PERLCOMMUNITY 1 "2022-02-26" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlcommunity - a brief overview of the Perl community

## DESCRIPTION

Header "DESCRIPTION"
This document aims to provide an overview of the vast perl community, which is
far too large and diverse to provide a detailed listing. If any specific niche
has been forgotten, it is not meant as an insult but an omission for the sake
of brevity.

The Perl community is as diverse as Perl, and there is a large amount of
evidence that the Perl users apply \s-1TMTOWTDI\s0 to all endeavors, not just
programming. From websites, to \s-1IRC,\s0 to mailing lists, there is more than one
way to get involved in the community.

### Where to Find the Community

Subsection "Where to Find the Community"
There is a central directory for the Perl community: <https://perl.org>
maintained by the Perl Foundation (<https://www.perlfoundation.org/>),
which tracks and provides services for a variety of other community sites.

*Raku*
Subsection "Raku"

Perl's sister language, Raku (formerly known as Perl 6), maintains its own
directory of community resources at <https://raku.org/community/>.

### Mailing Lists and Newsgroups

Subsection "Mailing Lists and Newsgroups"
Perl runs on e-mail; there is no doubt about it. The Camel book was originally
written mostly over e-mail and today Perl's development is co-ordinated through
mailing lists. The largest repository of Perl mailing lists is located at
<https://lists.perl.org>.

Most Perl-related projects set up mailing lists for both users and
contributors. If you don't see a certain project listed at
<https://lists.perl.org>, check the particular website for that project.
Most mailing lists are archived at <https://www.nntp.perl.org/>.

### \s-1IRC\s0

Subsection "IRC"
The Perl community has a rather large \s-1IRC\s0 presence. For starters, it has its
own \s-1IRC\s0 network, <irc://irc.perl.org>. General (not help-oriented) chat can be
found at <irc://irc.perl.org/#perl>. Many other more specific chats are also
hosted on the network. Information about irc.perl.org is located on the
network's website: <https://www.irc.perl.org>. For a more help-oriented #perl,
check out <irc://irc.libera.chat/#perl>. Most Perl-related channels
will be kind enough to point you in the right direction if you ask nicely.

Any large \s-1IRC\s0 network (Dalnet, EFnet) is also likely to have a #perl channel,
with varying activity levels.

### Websites

Subsection "Websites"
Perl websites come in a variety of forms, but they fit into two large
categories: forums and news websites. There are many Perl-related
websites, so only a few of the community's largest are mentioned here.

*News sites*
Subsection "News sites"

- <https://perl.com/>
Item "<https://perl.com/>"
Originally run by O'Reilly Media (the publisher of the Camel Book,
this site provides quality articles mostly about technical details of Perl.

- <http://blogs.perl.org/>
Item "<http://blogs.perl.org/>"
Many members of the community have a Perl-related blog on this site. If
you'd like to join them, you can sign up for free.

- <http://perlsphere.net/>
Item "<http://perlsphere.net/>"
Perlsphere is one of several aggregators of Perl-related blog feeds.

- <http://perlweekly.com/>
Item "<http://perlweekly.com/>"
Perl Weekly is a weekly mailing list that keeps you up to date on conferences,
releases and notable blog posts.

*Forums*
Subsection "Forums"

- <https://www.perlmonks.org/>
Item "<https://www.perlmonks.org/>"
PerlMonks is one of the largest Perl forums, and describes itself as \*(L"A place
for individuals to polish, improve, and showcase their Perl skills.\*(R" and \*(L"A
community which allows everyone to grow and learn from each other.\*(R"

- <https://stackoverflow.com/>
Item "<https://stackoverflow.com/>"
Stack Overflow is a free question-and-answer site for programmers. It's not
focussed solely on Perl, but it does have an active group of users who do
their best to help people with their Perl programming questions.

- <http://prepan.org/>
Item "<http://prepan.org/>"
PrePAN is used as a place to discuss modules that you're considering uploading
to the \s-1CPAN.\s0  You can get feedback on their design before you upload.

### User Groups

Subsection "User Groups"
Many cities around the world have local Perl Mongers chapters. A Perl Mongers
chapter is a local user group which typically holds regular in-person meetings,
both social and technical; helps organize local conferences, workshops, and
hackathons; and provides a mailing list or other continual contact method for
its members to keep in touch.

To find your local Perl Mongers (or \s-1PM\s0 as they're commonly abbreviated) group
check the international Perl Mongers directory at <https://www.pm.org/>.

### Workshops

Subsection "Workshops"
Perl workshops are, as the name might suggest, workshops where Perl is taught
in a variety of ways. At the workshops, subjects range from a beginner's
introduction (such as the Pittsburgh Perl Workshop's \*(L"Zero To Perl\*(R") to much
more advanced subjects.

There are several great resources for locating workshops: the
websites mentioned above, the
calendar mentioned below, and the \s-1YAPC\s0 Europe
website, <http://www.yapceurope.org/>, which is probably the best resource for
European Perl events.

### Hackathons

Subsection "Hackathons"
Hackathons are a very different kind of gathering where Perl hackers gather to
do just that, hack nonstop for an extended (several day) period on a specific
project or projects. Information about hackathons can be located in the same
place as information about workshops as well as in
<irc://irc.perl.org/#perl>.

If you have never been to a hackathon, here are a few basic things you need to
know before attending: have a working laptop and know how to use it; check out
the involved projects beforehand; have the necessary version control client;
and bring backup equipment (an extra \s-1LAN\s0 cable, additional power strips, etc.)
because someone will forget.

### Conventions

Subsection "Conventions"
Perl had two major annual conventions: The Perl Conference (now part of \s-1OSCON\s0),
put on by O'Reilly, and Yet Another Perl Conference or \s-1YAPC\s0 (pronounced
yap-see), which is localized into several regional YAPCs (North America,
Europe, Asia) in a stunning grassroots display by the Perl community.

In 2016, \s-1YAPC\s0 was rebranded as The Perl Conference again. It is now referred
to as The Perl and Raku Conference.

\s-1OSCON\s0 had been discontinued.

For more information about either conference, check out their respective web
pages:

- \(bu
The Perl Conference
.Sp
<http://perlconference.us/>.

- \(bu
\s-1OSCON\s0
.Sp
<https://www.oreilly.com/conferences/>

An additional conference franchise with a large Perl portion was the
Open Source Developers Conference or \s-1OSDC.\s0 First held in Australia, it
also spread to Israel and France. More information can be found at:
<http://www.osdc.org.il> for Israel, and <http://www.osdc.fr/> for France.

### Calendar of Perl Events

Subsection "Calendar of Perl Events"
The Perl Review, <http://www.theperlreview.com> maintains a website
and Google calendar for tracking
workshops, hackathons, Perl Mongers meetings, and other events. A view
of this calendar is available at <https://www.perl.org/events.html>.

Not every event or Perl Mongers group is on that calendar, so don't lose
heart if you don't see yours posted. To have your event or group listed,
contact brian d foy (brian@theperlreview.com).

## AUTHOR

Header "AUTHOR"
Edgar \*(L"Trizor\*(R" Bering <trizor@gmail.com>
