+++
operating_system_version = "15.3"
description = "This document describes differences between the 5.10.0 release and the 5.10.1 release. If you are upgrading from an earlier release such as 5.8.8, first read the perl5100delta, which describes differences between 5.8.8 and 5.10.0 The handling of ..."
manpage_format = "troff"
manpage_name = "perl5101delta"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
date = "2022-02-19"
operating_system = "macos"
detected_package_version = "5.34.1"
author = "None Specified"
manpage_section = "1"
title = "perl5101delta(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5101DELTA 1"
PERL5101DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5101delta - what is new for perl v5.10.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.10.0 release and
the 5.10.1 release.

If you are upgrading from an earlier release such as 5.8.8, first read
the perl5100delta, which describes differences between 5.8.8 and
5.10.0

## Incompatible Changes

Header "Incompatible Changes"

### Switch statement changes

Subsection "Switch statement changes"
The handling of complex expressions by the \f(CW\*(C`given\*(C'/\f(CW\*(C`when\*(C' switch
statement has been enhanced. There are two new cases where \f(CW\*(C`when\*(C' now
interprets its argument as a boolean, instead of an expression to be used
in a smart match:

- flip-flop operators
Item "flip-flop operators"
The \f(CW\*(C`..\*(C' and \f(CW\*(C`...\*(C' flip-flop operators are now evaluated in boolean
context, following their usual semantics; see \*(L"Range Operators\*(R" in perlop.
.Sp
Note that, as in perl 5.10.0, \f(CW\*(C`when (1..10)\*(C' will not work to test
whether a given value is an integer between 1 and 10; you should use
\f(CW\*(C`when ([1..10])\*(C' instead (note the array reference).
.Sp
However, contrary to 5.10.0, evaluating the flip-flop operators in boolean
context ensures it can now be useful in a \f(CW\*(C`when()\*(C', notably for
implementing bistable conditions, like in:
.Sp
.Vb 3
    when (/^=begin/ .. /^=end/) \{
      # do something
    \}
.Ve

- defined-or operator
Item "defined-or operator"
A compound expression involving the defined-or operator, as in
\f(CW\*(C`when (expr1 // expr2)\*(C', will be treated as boolean if the first
expression is boolean. (This just extends the existing rule that applies
to the regular or operator, as in \f(CW\*(C`when (expr1 || expr2)\*(C'.)

The next section details more changes brought to the semantics to
the smart match operator, that naturally also modify the behaviour
of the switch statements where smart matching is implicitly used.

### Smart match changes

Subsection "Smart match changes"
*Changes to type-based dispatch*
Subsection "Changes to type-based dispatch"

The smart match operator \f(CW\*(C`~~\*(C' is no longer commutative. The behaviour of
a smart match now depends primarily on the type of its right hand
argument. Moreover, its semantics have been adjusted for greater
consistency or usefulness in several cases. While the general backwards
compatibility is maintained, several changes must be noted:

- \(bu
Code references with an empty prototype are no longer treated specially.
They are passed an argument like the other code references (even if they
choose to ignore it).

- \(bu
\f(CW\*(C`%hash ~~ sub \{\}\*(C' and \f(CW\*(C`@array ~~ sub \{\}\*(C' now test that the subroutine
returns a true value for each key of the hash (or element of the
array), instead of passing the whole hash or array as a reference to
the subroutine.

- \(bu
Due to the commutativity breakage, code references are no longer
treated specially when appearing on the left of the \f(CW\*(C`~~\*(C' operator,
but like any vulgar scalar.

- \(bu
\f(CW\*(C`undef ~~ %hash\*(C' is always false (since \f(CW\*(C`undef\*(C' can't be a key in a
hash). No implicit conversion to \f(CW"" is done (as was the case in perl
5.10.0).

- \(bu
\f(CW\*(C`$scalar ~~ @array\*(C' now always distributes the smart match across the
elements of the array. It's true if one element in \f(CW@array verifies
\f(CW\*(C`$scalar ~~ $element\*(C'. This is a generalization of the old behaviour
that tested whether the array contained the scalar.

The full dispatch table for the smart match operator is given in
\*(L"Smart matching in detail\*(R" in perlsyn.

*Smart match and overloading*
Subsection "Smart match and overloading"

According to the rule of dispatch based on the rightmost argument type,
when an object overloading \f(CW\*(C`~~\*(C' appears on the right side of the
operator, the overload routine will always be called (with a 3rd argument
set to a true value, see overload.) However, when the object will
appear on the left, the overload routine will be called only when the
rightmost argument is a simple scalar. This way distributivity of smart match
across arrays is not broken, as well as the other behaviours with complex
types (coderefs, hashes, regexes). Thus, writers of overloading routines
for smart match mostly need to worry only with comparing against a scalar,
and possibly with stringification overloading; the other common cases
will be automatically handled consistently.

\f(CW\*(C`~~\*(C' will now refuse to work on objects that do not overload it (in order
to avoid relying on the object's underlying structure). (However, if the
object overloads the stringification or the numification operators, and
if overload fallback is active, it will be used instead, as usual.)

### Other incompatible changes

Subsection "Other incompatible changes"

- \(bu
The semantics of \f(CW\*(C`use feature :5.10*\*(C' have changed slightly.
See \*(L"Modules and Pragmata\*(R" for more information.

- \(bu
It is now a run-time error to use the smart match operator \f(CW\*(C`~~\*(C'
with an object that has no overload defined for it. (This way
\f(CW\*(C`~~\*(C' will not break encapsulation by matching against the
object's internal representation as a reference.)

- \(bu
The version control system used for the development of the perl
interpreter has been switched from Perforce to git.  This is mainly an
internal issue that only affects people actively working on the perl core;
but it may have minor external visibility, for example in some of details
of the output of \f(CW\*(C`perl -V\*(C'. See perlrepository for more information.

- \(bu
The internal structure of the \f(CW\*(C`ext/\*(C' directory in the perl source has
been reorganised. In general, a module \f(CW\*(C`Foo::Bar\*(C' whose source was
stored under *ext/Foo/Bar/* is now located under *ext/Foo-Bar/*. Also,
some modules have been moved from *lib/* to *ext/*. This is purely a
source tarball change, and should make no difference to the compilation or
installation of perl, unless you have a very customised build process that
explicitly relies on this structure, or which hard-codes the \f(CW\*(C`nonxs_ext\*(C'
*Configure* parameter. Specifically, this change does not by default
alter the location of any files in the final installation.

- \(bu
As part of the \f(CW\*(C`Test::Harness\*(C' 2.x to 3.x upgrade, the experimental
\f(CW\*(C`Test::Harness::Straps\*(C' module has been removed.
See \*(L"Updated Modules\*(R" for more details.

- \(bu
As part of the \f(CW\*(C`ExtUtils::MakeMaker\*(C' upgrade, the
\f(CW\*(C`ExtUtils::MakeMaker::bytes\*(C' and \f(CW\*(C`ExtUtils::MakeMaker::vmsish\*(C' modules
have been removed from this distribution.

- \(bu
\f(CW\*(C`Module::CoreList\*(C' no longer contains the \f(CW%:patchlevel hash.

- \(bu
This one is actually a change introduced in 5.10.0, but it was missed
from that release's perldelta, so it is mentioned here instead.
.Sp
A bugfix related to the handling of the \f(CW\*(C`/m\*(C' modifier and \f(CW\*(C`qr\*(C' resulted
in a change of behaviour between 5.8.x and 5.10.0:
.Sp
.Vb 2
    # matches in 5.8.x, doesn\*(Aqt match in 5.10.0
    $re = qr/^bar/; "foo\\nbar" =~ /$re/m;
.Ve

## Core Enhancements

Header "Core Enhancements"

### Unicode Character Database 5.1.0

Subsection "Unicode Character Database 5.1.0"
The copy of the Unicode Character Database included in Perl 5.10.1 has
been updated to 5.1.0 from 5.0.0. See
<http://www.unicode.org/versions/Unicode5.1.0/#Notable_Changes> for the
notable changes.

### A proper interface for pluggable Method Resolution Orders

Subsection "A proper interface for pluggable Method Resolution Orders"
As of Perl 5.10.1 there is a new interface for plugging and using method
resolution orders other than the default (linear depth first search).
The C3 method resolution order added in 5.10.0 has been re-implemented as
a plugin, without changing its Perl-space interface. See perlmroapi for
more information.
.ie n .SS "The ""overloading"" pragma"
.el .SS "The \f(CWoverloading pragma"
Subsection "The overloading pragma"
This pragma allows you to lexically disable or enable overloading
for some or all operations. (Yuval Kogman)

### Parallel tests

Subsection "Parallel tests"
The core distribution can now run its regression tests in parallel on
Unix-like platforms. Instead of running \f(CW\*(C`make test\*(C', set \f(CW\*(C`TEST_JOBS\*(C' in
your environment to the number of tests to run in parallel, and run
\f(CW\*(C`make test_harness\*(C'. On a Bourne-like shell, this can be done as

.Vb 1
    TEST_JOBS=3 make test_harness  # Run 3 tests in parallel
.Ve

An environment variable is used, rather than parallel make itself, because
TAP::Harness needs to be able to schedule individual non-conflicting test
scripts itself, and there is no standard interface to \f(CW\*(C`make\*(C' utilities to
interact with their job schedulers.

Note that currently some test scripts may fail when run in parallel (most
notably \f(CW\*(C`ext/IO/t/io_dir.t\*(C'). If necessary run just the failing scripts
again sequentially and see if the failures go away.

### DTrace support

Subsection "DTrace support"
Some support for DTrace has been added. See \*(L"DTrace support\*(R" in *\s-1INSTALL\s0*.
.ie n .SS "Support for ""configure_requires"" in \s-1CPAN\s0 module metadata"
.el .SS "Support for \f(CWconfigure_requires in \s-1CPAN\s0 module metadata"
Subsection "Support for configure_requires in CPAN module metadata"
Both \f(CW\*(C`CPAN\*(C' and \f(CW\*(C`CPANPLUS\*(C' now support the \f(CW\*(C`configure_requires\*(C' keyword
in the \f(CW\*(C`META.yml\*(C' metadata file included in most recent \s-1CPAN\s0 distributions.
This allows distribution authors to specify configuration prerequisites that
must be installed before running *Makefile.PL* or *Build.PL*.

See the documentation for \f(CW\*(C`ExtUtils::MakeMaker\*(C' or \f(CW\*(C`Module::Build\*(C' for more
on how to specify \f(CW\*(C`configure_requires\*(C' when creating a distribution for \s-1CPAN.\s0

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules and Pragmata

Subsection "New Modules and Pragmata"
.ie n .IP """autodie""" 4
.el .IP "\f(CWautodie" 4
Item "autodie"
This is a new lexically-scoped alternative for the \f(CW\*(C`Fatal\*(C' module.
The bundled version is 2.06_01. Note that in this release, using a string
eval when \f(CW\*(C`autodie\*(C' is in effect can cause the autodie behaviour to leak
into the surrounding scope. See \*(L"\s-1BUGS\*(R"\s0 in autodie for more details.
.ie n .IP """Compress::Raw::Bzip2""" 4
.el .IP "\f(CWCompress::Raw::Bzip2" 4
Item "Compress::Raw::Bzip2"
This has been added to the core (version 2.020).
.ie n .IP """parent""" 4
.el .IP "\f(CWparent" 4
Item "parent"
This pragma establishes an \s-1ISA\s0 relationship with base classes at compile
time. It provides the key feature of \f(CW\*(C`base\*(C' without the feature creep.
.ie n .IP """Parse::CPAN::Meta""" 4
.el .IP "\f(CWParse::CPAN::Meta" 4
Item "Parse::CPAN::Meta"
This has been added to the core (version 1.39).

### Pragmata Changes

Subsection "Pragmata Changes"
.ie n .IP """attributes""" 4
.el .IP "\f(CWattributes" 4
Item "attributes"
Upgraded from version 0.08 to 0.09.
.ie n .IP """attrs""" 4
.el .IP "\f(CWattrs" 4
Item "attrs"
Upgraded from version 1.02 to 1.03.
.ie n .IP """base""" 4
.el .IP "\f(CWbase" 4
Item "base"
Upgraded from version 2.13 to 2.14. See parent for a replacement.
.ie n .IP """bigint""" 4
.el .IP "\f(CWbigint" 4
Item "bigint"
Upgraded from version 0.22 to 0.23.
.ie n .IP """bignum""" 4
.el .IP "\f(CWbignum" 4
Item "bignum"
Upgraded from version 0.22 to 0.23.
.ie n .IP """bigrat""" 4
.el .IP "\f(CWbigrat" 4
Item "bigrat"
Upgraded from version 0.22 to 0.23.
.ie n .IP """charnames""" 4
.el .IP "\f(CWcharnames" 4
Item "charnames"
Upgraded from version 1.06 to 1.07.
.Sp
The Unicode *NameAliases.txt* database file has been added. This has the
effect of adding some extra \f(CW\*(C`\\N\*(C' character names that formerly wouldn't
have been recognised; for example, \f(CW"\\N\{LATIN CAPITAL LETTER GHA\}".
.ie n .IP """constant""" 4
.el .IP "\f(CWconstant" 4
Item "constant"
Upgraded from version 1.13 to 1.17.
.ie n .IP """feature""" 4
.el .IP "\f(CWfeature" 4
Item "feature"
The meaning of the \f(CW\*(C`:5.10\*(C' and \f(CW\*(C`:5.10.X\*(C' feature bundles has
changed slightly. The last component, if any (i.e. \f(CW\*(C`X\*(C') is simply ignored.
This is predicated on the assumption that new features will not, in
general, be added to maintenance releases. So \f(CW\*(C`:5.10\*(C' and \f(CW\*(C`:5.10.X\*(C'
have identical effect. This is a change to the behaviour documented for
5.10.0.
.ie n .IP """fields""" 4
.el .IP "\f(CWfields" 4
Item "fields"
Upgraded from version 2.13 to 2.14 (this was just a version bump; there
were no functional changes).
.ie n .IP """lib""" 4
.el .IP "\f(CWlib" 4
Item "lib"
Upgraded from version 0.5565 to 0.62.
.ie n .IP """open""" 4
.el .IP "\f(CWopen" 4
Item "open"
Upgraded from version 1.06 to 1.07.
.ie n .IP """overload""" 4
.el .IP "\f(CWoverload" 4
Item "overload"
Upgraded from version 1.06 to 1.07.
.ie n .IP """overloading""" 4
.el .IP "\f(CWoverloading" 4
Item "overloading"
See "The \f(CW\*(C`overloading\*(C' pragma" above.
.ie n .IP """version""" 4
.el .IP "\f(CWversion" 4
Item "version"
Upgraded from version 0.74 to 0.77.

### Updated Modules

Subsection "Updated Modules"
.ie n .IP """Archive::Extract""" 4
.el .IP "\f(CWArchive::Extract" 4
Item "Archive::Extract"
Upgraded from version 0.24 to 0.34.
.ie n .IP """Archive::Tar""" 4
.el .IP "\f(CWArchive::Tar" 4
Item "Archive::Tar"
Upgraded from version 1.38 to 1.52.
.ie n .IP """Attribute::Handlers""" 4
.el .IP "\f(CWAttribute::Handlers" 4
Item "Attribute::Handlers"
Upgraded from version 0.79 to 0.85.
.ie n .IP """AutoLoader""" 4
.el .IP "\f(CWAutoLoader" 4
Item "AutoLoader"
Upgraded from version 5.63 to 5.68.
.ie n .IP """AutoSplit""" 4
.el .IP "\f(CWAutoSplit" 4
Item "AutoSplit"
Upgraded from version 1.05 to 1.06.
.ie n .IP """B""" 4
.el .IP "\f(CWB" 4
Item "B"
Upgraded from version 1.17 to 1.22.
.ie n .IP """B::Debug""" 4
.el .IP "\f(CWB::Debug" 4
Item "B::Debug"
Upgraded from version 1.05 to 1.11.
.ie n .IP """B::Deparse""" 4
.el .IP "\f(CWB::Deparse" 4
Item "B::Deparse"
Upgraded from version 0.83 to 0.89.
.ie n .IP """B::Lint""" 4
.el .IP "\f(CWB::Lint" 4
Item "B::Lint"
Upgraded from version 1.09 to 1.11.
.ie n .IP """B::Xref""" 4
.el .IP "\f(CWB::Xref" 4
Item "B::Xref"
Upgraded from version 1.01 to 1.02.
.ie n .IP """Benchmark""" 4
.el .IP "\f(CWBenchmark" 4
Item "Benchmark"
Upgraded from version 1.10 to 1.11.
.ie n .IP """Carp""" 4
.el .IP "\f(CWCarp" 4
Item "Carp"
Upgraded from version 1.08 to 1.11.
.ie n .IP """CGI""" 4
.el .IP "\f(CWCGI" 4
Item "CGI"
Upgraded from version 3.29 to 3.43.
(also includes the \*(L"default_value for **popup_menu()**\*(R" fix from 3.45).
.ie n .IP """Compress::Zlib""" 4
.el .IP "\f(CWCompress::Zlib" 4
Item "Compress::Zlib"
Upgraded from version 2.008 to 2.020.
.ie n .IP """CPAN""" 4
.el .IP "\f(CWCPAN" 4
Item "CPAN"
Upgraded from version 1.9205 to 1.9402. \f(CW\*(C`CPAN::FTP\*(C' has a local fix to
stop it being too verbose on download failure.
.ie n .IP """CPANPLUS""" 4
.el .IP "\f(CWCPANPLUS" 4
Item "CPANPLUS"
Upgraded from version 0.84 to 0.88.
.ie n .IP """CPANPLUS::Dist::Build""" 4
.el .IP "\f(CWCPANPLUS::Dist::Build" 4
Item "CPANPLUS::Dist::Build"
Upgraded from version 0.06_02 to 0.36.
.ie n .IP """Cwd""" 4
.el .IP "\f(CWCwd" 4
Item "Cwd"
Upgraded from version 3.25_01 to 3.30.
.ie n .IP """Data::Dumper""" 4
.el .IP "\f(CWData::Dumper" 4
Item "Data::Dumper"
Upgraded from version 2.121_14 to 2.124.
.ie n .IP """DB""" 4
.el .IP "\f(CWDB" 4
Item "DB"
Upgraded from version 1.01 to 1.02.
.ie n .IP """DB_File""" 4
.el .IP "\f(CWDB_File" 4
Item "DB_File"
Upgraded from version 1.816_1 to 1.820.
.ie n .IP """Devel::PPPort""" 4
.el .IP "\f(CWDevel::PPPort" 4
Item "Devel::PPPort"
Upgraded from version 3.13 to 3.19.
.ie n .IP """Digest::MD5""" 4
.el .IP "\f(CWDigest::MD5" 4
Item "Digest::MD5"
Upgraded from version 2.36_01 to 2.39.
.ie n .IP """Digest::SHA""" 4
.el .IP "\f(CWDigest::SHA" 4
Item "Digest::SHA"
Upgraded from version 5.45 to 5.47.
.ie n .IP """DirHandle""" 4
.el .IP "\f(CWDirHandle" 4
Item "DirHandle"
Upgraded from version 1.01 to 1.03.
.ie n .IP """Dumpvalue""" 4
.el .IP "\f(CWDumpvalue" 4
Item "Dumpvalue"
Upgraded from version 1.12 to 1.13.
.ie n .IP """DynaLoader""" 4
.el .IP "\f(CWDynaLoader" 4
Item "DynaLoader"
Upgraded from version 1.08 to 1.10.
.ie n .IP """Encode""" 4
.el .IP "\f(CWEncode" 4
Item "Encode"
Upgraded from version 2.23 to 2.35.
.ie n .IP """Errno""" 4
.el .IP "\f(CWErrno" 4
Item "Errno"
Upgraded from version 1.10 to 1.11.
.ie n .IP """Exporter""" 4
.el .IP "\f(CWExporter" 4
Item "Exporter"
Upgraded from version 5.62 to 5.63.
.ie n .IP """ExtUtils::CBuilder""" 4
.el .IP "\f(CWExtUtils::CBuilder" 4
Item "ExtUtils::CBuilder"
Upgraded from version 0.21 to 0.2602.
.ie n .IP """ExtUtils::Command""" 4
.el .IP "\f(CWExtUtils::Command" 4
Item "ExtUtils::Command"
Upgraded from version 1.13 to 1.16.
.ie n .IP """ExtUtils::Constant""" 4
.el .IP "\f(CWExtUtils::Constant" 4
Item "ExtUtils::Constant"
Upgraded from 0.20 to 0.22. (Note that neither of these versions are
available on \s-1CPAN.\s0)
.ie n .IP """ExtUtils::Embed""" 4
.el .IP "\f(CWExtUtils::Embed" 4
Item "ExtUtils::Embed"
Upgraded from version 1.27 to 1.28.
.ie n .IP """ExtUtils::Install""" 4
.el .IP "\f(CWExtUtils::Install" 4
Item "ExtUtils::Install"
Upgraded from version 1.44 to 1.54.
.ie n .IP """ExtUtils::MakeMaker""" 4
.el .IP "\f(CWExtUtils::MakeMaker" 4
Item "ExtUtils::MakeMaker"
Upgraded from version 6.42 to 6.55_02.
.Sp
Note that \f(CW\*(C`ExtUtils::MakeMaker::bytes\*(C' and \f(CW\*(C`ExtUtils::MakeMaker::vmsish\*(C'
have been removed from this distribution.
.ie n .IP """ExtUtils::Manifest""" 4
.el .IP "\f(CWExtUtils::Manifest" 4
Item "ExtUtils::Manifest"
Upgraded from version 1.51_01 to 1.56.
.ie n .IP """ExtUtils::ParseXS""" 4
.el .IP "\f(CWExtUtils::ParseXS" 4
Item "ExtUtils::ParseXS"
Upgraded from version 2.18_02 to 2.2002.
.ie n .IP """Fatal""" 4
.el .IP "\f(CWFatal" 4
Item "Fatal"
Upgraded from version 1.05 to 2.06_01. See also the new pragma \f(CW\*(C`autodie\*(C'.
.ie n .IP """File::Basename""" 4
.el .IP "\f(CWFile::Basename" 4
Item "File::Basename"
Upgraded from version 2.76 to 2.77.
.ie n .IP """File::Compare""" 4
.el .IP "\f(CWFile::Compare" 4
Item "File::Compare"
Upgraded from version 1.1005 to 1.1006.
.ie n .IP """File::Copy""" 4
.el .IP "\f(CWFile::Copy" 4
Item "File::Copy"
Upgraded from version 2.11 to 2.14.
.ie n .IP """File::Fetch""" 4
.el .IP "\f(CWFile::Fetch" 4
Item "File::Fetch"
Upgraded from version 0.14 to 0.20.
.ie n .IP """File::Find""" 4
.el .IP "\f(CWFile::Find" 4
Item "File::Find"
Upgraded from version 1.12 to 1.14.
.ie n .IP """File::Path""" 4
.el .IP "\f(CWFile::Path" 4
Item "File::Path"
Upgraded from version 2.04 to 2.07_03.
.ie n .IP """File::Spec""" 4
.el .IP "\f(CWFile::Spec" 4
Item "File::Spec"
Upgraded from version 3.2501 to 3.30.
.ie n .IP """File::stat""" 4
.el .IP "\f(CWFile::stat" 4
Item "File::stat"
Upgraded from version 1.00 to 1.01.
.ie n .IP """File::Temp""" 4
.el .IP "\f(CWFile::Temp" 4
Item "File::Temp"
Upgraded from version 0.18 to 0.22.
.ie n .IP """FileCache""" 4
.el .IP "\f(CWFileCache" 4
Item "FileCache"
Upgraded from version 1.07 to 1.08.
.ie n .IP """FileHandle""" 4
.el .IP "\f(CWFileHandle" 4
Item "FileHandle"
Upgraded from version 2.01 to 2.02.
.ie n .IP """Filter::Simple""" 4
.el .IP "\f(CWFilter::Simple" 4
Item "Filter::Simple"
Upgraded from version 0.82 to 0.84.
.ie n .IP """Filter::Util::Call""" 4
.el .IP "\f(CWFilter::Util::Call" 4
Item "Filter::Util::Call"
Upgraded from version 1.07 to 1.08.
.ie n .IP """FindBin""" 4
.el .IP "\f(CWFindBin" 4
Item "FindBin"
Upgraded from version 1.49 to 1.50.
.ie n .IP """GDBM_File""" 4
.el .IP "\f(CWGDBM_File" 4
Item "GDBM_File"
Upgraded from version 1.08 to 1.09.
.ie n .IP """Getopt::Long""" 4
.el .IP "\f(CWGetopt::Long" 4
Item "Getopt::Long"
Upgraded from version 2.37 to 2.38.
.ie n .IP """Hash::Util::FieldHash""" 4
.el .IP "\f(CWHash::Util::FieldHash" 4
Item "Hash::Util::FieldHash"
Upgraded from version 1.03 to 1.04. This fixes a memory leak.
.ie n .IP """I18N::Collate""" 4
.el .IP "\f(CWI18N::Collate" 4
Item "I18N::Collate"
Upgraded from version 1.00 to 1.01.
.ie n .IP """IO""" 4
.el .IP "\f(CWIO" 4
Item "IO"
Upgraded from version 1.23_01 to 1.25.
.Sp
This makes non-blocking mode work on Windows in \f(CW\*(C`IO::Socket::INET\*(C'
[\s-1CPAN\s0 #43573].
.ie n .IP """IO::Compress::*""" 4
.el .IP "\f(CWIO::Compress::*" 4
Item "IO::Compress::*"
Upgraded from version 2.008 to 2.020.
.ie n .IP """IO::Dir""" 4
.el .IP "\f(CWIO::Dir" 4
Item "IO::Dir"
Upgraded from version 1.06 to 1.07.
.ie n .IP """IO::Handle""" 4
.el .IP "\f(CWIO::Handle" 4
Item "IO::Handle"
Upgraded from version 1.27 to 1.28.
.ie n .IP """IO::Socket""" 4
.el .IP "\f(CWIO::Socket" 4
Item "IO::Socket"
Upgraded from version 1.30_01 to 1.31.
.ie n .IP """IO::Zlib""" 4
.el .IP "\f(CWIO::Zlib" 4
Item "IO::Zlib"
Upgraded from version 1.07 to 1.09.
.ie n .IP """IPC::Cmd""" 4
.el .IP "\f(CWIPC::Cmd" 4
Item "IPC::Cmd"
Upgraded from version 0.40_1 to 0.46.
.ie n .IP """IPC::Open3""" 4
.el .IP "\f(CWIPC::Open3" 4
Item "IPC::Open3"
Upgraded from version 1.02 to 1.04.
.ie n .IP """IPC::SysV""" 4
.el .IP "\f(CWIPC::SysV" 4
Item "IPC::SysV"
Upgraded from version 1.05 to 2.01.
.ie n .IP """lib""" 4
.el .IP "\f(CWlib" 4
Item "lib"
Upgraded from version 0.5565 to 0.62.
.ie n .IP """List::Util""" 4
.el .IP "\f(CWList::Util" 4
Item "List::Util"
Upgraded from version 1.19 to 1.21.
.ie n .IP """Locale::MakeText""" 4
.el .IP "\f(CWLocale::MakeText" 4
Item "Locale::MakeText"
Upgraded from version 1.12 to 1.13.
.ie n .IP """Log::Message""" 4
.el .IP "\f(CWLog::Message" 4
Item "Log::Message"
Upgraded from version 0.01 to 0.02.
.ie n .IP """Math::BigFloat""" 4
.el .IP "\f(CWMath::BigFloat" 4
Item "Math::BigFloat"
Upgraded from version 1.59 to 1.60.
.ie n .IP """Math::BigInt""" 4
.el .IP "\f(CWMath::BigInt" 4
Item "Math::BigInt"
Upgraded from version 1.88 to 1.89.
.ie n .IP """Math::BigInt::FastCalc""" 4
.el .IP "\f(CWMath::BigInt::FastCalc" 4
Item "Math::BigInt::FastCalc"
Upgraded from version 0.16 to 0.19.
.ie n .IP """Math::BigRat""" 4
.el .IP "\f(CWMath::BigRat" 4
Item "Math::BigRat"
Upgraded from version 0.21 to 0.22.
.ie n .IP """Math::Complex""" 4
.el .IP "\f(CWMath::Complex" 4
Item "Math::Complex"
Upgraded from version 1.37 to 1.56.
.ie n .IP """Math::Trig""" 4
.el .IP "\f(CWMath::Trig" 4
Item "Math::Trig"
Upgraded from version 1.04 to 1.20.
.ie n .IP """Memoize""" 4
.el .IP "\f(CWMemoize" 4
Item "Memoize"
Upgraded from version 1.01_02 to 1.01_03 (just a minor documentation
change).
.ie n .IP """Module::Build""" 4
.el .IP "\f(CWModule::Build" 4
Item "Module::Build"
Upgraded from version 0.2808_01 to 0.34_02.
.ie n .IP """Module::CoreList""" 4
.el .IP "\f(CWModule::CoreList" 4
Item "Module::CoreList"
Upgraded from version 2.13 to 2.18. This release no longer contains the
\f(CW%Module::CoreList::patchlevel hash.
.ie n .IP """Module::Load""" 4
.el .IP "\f(CWModule::Load" 4
Item "Module::Load"
Upgraded from version 0.12 to 0.16.
.ie n .IP """Module::Load::Conditional""" 4
.el .IP "\f(CWModule::Load::Conditional" 4
Item "Module::Load::Conditional"
Upgraded from version 0.22 to 0.30.
.ie n .IP """Module::Loaded""" 4
.el .IP "\f(CWModule::Loaded" 4
Item "Module::Loaded"
Upgraded from version 0.01 to 0.02.
.ie n .IP """Module::Pluggable""" 4
.el .IP "\f(CWModule::Pluggable" 4
Item "Module::Pluggable"
Upgraded from version 3.6 to 3.9.
.ie n .IP """NDBM_File""" 4
.el .IP "\f(CWNDBM_File" 4
Item "NDBM_File"
Upgraded from version 1.07 to 1.08.
.ie n .IP """Net::Ping""" 4
.el .IP "\f(CWNet::Ping" 4
Item "Net::Ping"
Upgraded from version 2.33 to 2.36.
.ie n .IP """NEXT""" 4
.el .IP "\f(CWNEXT" 4
Item "NEXT"
Upgraded from version 0.60_01 to 0.64.
.ie n .IP """Object::Accessor""" 4
.el .IP "\f(CWObject::Accessor" 4
Item "Object::Accessor"
Upgraded from version 0.32 to 0.34.
.ie n .IP """OS2::REXX""" 4
.el .IP "\f(CWOS2::REXX" 4
Item "OS2::REXX"
Upgraded from version 1.03 to 1.04.
.ie n .IP """Package::Constants""" 4
.el .IP "\f(CWPackage::Constants" 4
Item "Package::Constants"
Upgraded from version 0.01 to 0.02.
.ie n .IP """PerlIO""" 4
.el .IP "\f(CWPerlIO" 4
Item "PerlIO"
Upgraded from version 1.04 to 1.06.
.ie n .IP """PerlIO::via""" 4
.el .IP "\f(CWPerlIO::via" 4
Item "PerlIO::via"
Upgraded from version 0.04 to 0.07.
.ie n .IP """Pod::Man""" 4
.el .IP "\f(CWPod::Man" 4
Item "Pod::Man"
Upgraded from version 2.16 to 2.22.
.ie n .IP """Pod::Parser""" 4
.el .IP "\f(CWPod::Parser" 4
Item "Pod::Parser"
Upgraded from version 1.35 to 1.37.
.ie n .IP """Pod::Simple""" 4
.el .IP "\f(CWPod::Simple" 4
Item "Pod::Simple"
Upgraded from version 3.05 to 3.07.
.ie n .IP """Pod::Text""" 4
.el .IP "\f(CWPod::Text" 4
Item "Pod::Text"
Upgraded from version 3.08 to 3.13.
.ie n .IP """POSIX""" 4
.el .IP "\f(CWPOSIX" 4
Item "POSIX"
Upgraded from version 1.13 to 1.17.
.ie n .IP """Safe""" 4
.el .IP "\f(CWSafe" 4
Item "Safe"
Upgraded from 2.12 to 2.18.
.ie n .IP """Scalar::Util""" 4
.el .IP "\f(CWScalar::Util" 4
Item "Scalar::Util"
Upgraded from version 1.19 to 1.21.
.ie n .IP """SelectSaver""" 4
.el .IP "\f(CWSelectSaver" 4
Item "SelectSaver"
Upgraded from 1.01 to 1.02.
.ie n .IP """SelfLoader""" 4
.el .IP "\f(CWSelfLoader" 4
Item "SelfLoader"
Upgraded from 1.11 to 1.17.
.ie n .IP """Socket""" 4
.el .IP "\f(CWSocket" 4
Item "Socket"
Upgraded from 1.80 to 1.82.
.ie n .IP """Storable""" 4
.el .IP "\f(CWStorable" 4
Item "Storable"
Upgraded from 2.18 to 2.20.
.ie n .IP """Switch""" 4
.el .IP "\f(CWSwitch" 4
Item "Switch"
Upgraded from version 2.13 to 2.14. Please see \*(L"Deprecations\*(R".
.ie n .IP """Symbol""" 4
.el .IP "\f(CWSymbol" 4
Item "Symbol"
Upgraded from version 1.06 to 1.07.
.ie n .IP """Sys::Syslog""" 4
.el .IP "\f(CWSys::Syslog" 4
Item "Sys::Syslog"
Upgraded from version 0.22 to 0.27.
.ie n .IP """Term::ANSIColor""" 4
.el .IP "\f(CWTerm::ANSIColor" 4
Item "Term::ANSIColor"
Upgraded from version 1.12 to 2.00.
.ie n .IP """Term::ReadLine""" 4
.el .IP "\f(CWTerm::ReadLine" 4
Item "Term::ReadLine"
Upgraded from version 1.03 to 1.04.
.ie n .IP """Term::UI""" 4
.el .IP "\f(CWTerm::UI" 4
Item "Term::UI"
Upgraded from version 0.18 to 0.20.
.ie n .IP """Test::Harness""" 4
.el .IP "\f(CWTest::Harness" 4
Item "Test::Harness"
Upgraded from version 2.64 to 3.17.
.Sp
Note that one side-effect of the 2.x to 3.x upgrade is that the
experimental \f(CW\*(C`Test::Harness::Straps\*(C' module (and its supporting
\f(CW\*(C`Assert\*(C', \f(CW\*(C`Iterator\*(C', \f(CW\*(C`Point\*(C' and \f(CW\*(C`Results\*(C' modules) have been
removed. If you still need this, then they are available in the
(unmaintained) \f(CW\*(C`Test-Harness-Straps\*(C' distribution on \s-1CPAN.\s0
.ie n .IP """Test::Simple""" 4
.el .IP "\f(CWTest::Simple" 4
Item "Test::Simple"
Upgraded from version 0.72 to 0.92.
.ie n .IP """Text::ParseWords""" 4
.el .IP "\f(CWText::ParseWords" 4
Item "Text::ParseWords"
Upgraded from version 3.26 to 3.27.
.ie n .IP """Text::Tabs""" 4
.el .IP "\f(CWText::Tabs" 4
Item "Text::Tabs"
Upgraded from version 2007.1117 to 2009.0305.
.ie n .IP """Text::Wrap""" 4
.el .IP "\f(CWText::Wrap" 4
Item "Text::Wrap"
Upgraded from version 2006.1117 to 2009.0305.
.ie n .IP """Thread::Queue""" 4
.el .IP "\f(CWThread::Queue" 4
Item "Thread::Queue"
Upgraded from version 2.00 to 2.11.
.ie n .IP """Thread::Semaphore""" 4
.el .IP "\f(CWThread::Semaphore" 4
Item "Thread::Semaphore"
Upgraded from version 2.01 to 2.09.
.ie n .IP """threads""" 4
.el .IP "\f(CWthreads" 4
Item "threads"
Upgraded from version 1.67 to 1.72.
.ie n .IP """threads::shared""" 4
.el .IP "\f(CWthreads::shared" 4
Item "threads::shared"
Upgraded from version 1.14 to 1.29.
.ie n .IP """Tie::RefHash""" 4
.el .IP "\f(CWTie::RefHash" 4
Item "Tie::RefHash"
Upgraded from version 1.37 to 1.38.
.ie n .IP """Tie::StdHandle""" 4
.el .IP "\f(CWTie::StdHandle" 4
Item "Tie::StdHandle"
This has documentation changes, and has been assigned a version for the
first time: version 4.2.
.ie n .IP """Time::HiRes""" 4
.el .IP "\f(CWTime::HiRes" 4
Item "Time::HiRes"
Upgraded from version 1.9711 to 1.9719.
.ie n .IP """Time::Local""" 4
.el .IP "\f(CWTime::Local" 4
Item "Time::Local"
Upgraded from version 1.18 to 1.1901.
.ie n .IP """Time::Piece""" 4
.el .IP "\f(CWTime::Piece" 4
Item "Time::Piece"
Upgraded from version 1.12 to 1.15.
.ie n .IP """Unicode::Normalize""" 4
.el .IP "\f(CWUnicode::Normalize" 4
Item "Unicode::Normalize"
Upgraded from version 1.02 to 1.03.
.ie n .IP """Unicode::UCD""" 4
.el .IP "\f(CWUnicode::UCD" 4
Item "Unicode::UCD"
Upgraded from version 0.25 to 0.27.
.Sp
\f(CW\*(C`charinfo()\*(C' now works on Unified \s-1CJK\s0 code points added to later versions
of Unicode.
.Sp
\f(CW\*(C`casefold()\*(C' has new fields returned to provide both a simpler interface
and previously missing information. The old fields are retained for
backwards compatibility. Information about Turkic-specific code points is
now returned.
.Sp
The documentation has been corrected and expanded.
.ie n .IP """UNIVERSAL""" 4
.el .IP "\f(CWUNIVERSAL" 4
Item "UNIVERSAL"
Upgraded from version 1.04 to 1.05.
.ie n .IP """Win32""" 4
.el .IP "\f(CWWin32" 4
Item "Win32"
Upgraded from version 0.34 to 0.39.
.ie n .IP """Win32API::File""" 4
.el .IP "\f(CWWin32API::File" 4
Item "Win32API::File"
Upgraded from version 0.1001_01 to 0.1101.
.ie n .IP """XSLoader""" 4
.el .IP "\f(CWXSLoader" 4
Item "XSLoader"
Upgraded from version 0.08 to 0.10.

## Utility Changes

Header "Utility Changes"

- \fIh2ph
Item "h2ph"
Now looks in \f(CW\*(C`include-fixed\*(C' too, which is a recent addition to gcc's
search path.

- \fIh2xs
Item "h2xs"
No longer incorrectly treats enum values like macros (Daniel Burr).
.Sp
Now handles \*(C+ style constants (\f(CW\*(C`//\*(C') properly in enums. (A patch from
Rainer Weikusat was used; Daniel Burr also proposed a similar fix).

- \fIperl5db.pl
Item "perl5db.pl"
\f(CW\*(C`LVALUE\*(C' subroutines now work under the debugger.
.Sp
The debugger now correctly handles proxy constant subroutines, and
subroutine stubs.

- \fIperlthanks
Item "perlthanks"
Perl 5.10.1 adds a new utility *perlthanks*, which is a variant of
*perlbug*, but for sending non-bug-reports to the authors and maintainers
of Perl. Getting nothing but bug reports can become a bit demoralising:
we'll see if this changes things.

## New Documentation

Header "New Documentation"

- perlhaiku
Item "perlhaiku"
This contains instructions on how to build perl for the Haiku platform.

- perlmroapi
Item "perlmroapi"
This describes the new interface for pluggable Method Resolution Orders.

- perlperf
Item "perlperf"
This document, by Richard Foley, provides an introduction to the use of
performance and optimization techniques which can be used with particular
reference to perl programs.

- perlrepository
Item "perlrepository"
This describes how to access the perl source using the *git* version
control system.

- perlthanks
Item "perlthanks"
This describes the new *perlthanks* utility.

## Changes to Existing Documentation

Header "Changes to Existing Documentation"
The various large \f(CW\*(C`Changes*\*(C' files (which listed every change made to perl
over the last 18 years) have been removed, and replaced by a small file,
also called \f(CW\*(C`Changes\*(C', which just explains how that same information may
be extracted from the git version control system.

The file *Porting/patching.pod* has been deleted, as it mainly described
interacting with the old Perforce-based repository, which is now obsolete.
Information still relevant has been moved to perlrepository.

perlapi, perlintern, perlmodlib and perltoc are now all
generated at build time, rather than being shipped as part of the release.

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
A new internal cache means that \f(CW\*(C`isa()\*(C' will often be faster.

- \(bu
Under \f(CW\*(C`use locale\*(C', the locale-relevant information is now cached on
read-only values, such as the list returned by \f(CW\*(C`keys %hash\*(C'. This makes
operations such as \f(CW\*(C`sort keys %hash\*(C' in the scope of \f(CW\*(C`use locale\*(C' much
faster.

- \(bu
Empty \f(CW\*(C`DESTROY\*(C' methods are no longer called.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"

### \fIext/ reorganisation

Subsection "ext/ reorganisation"
The layout of directories in *ext* has been revised. Specifically, all
extensions are now flat, and at the top level, with \f(CW\*(C`/\*(C' in pathnames
replaced by \f(CW\*(C`-\*(C', so that *ext/Data/Dumper/* is now *ext/Data-Dumper/*,
etc.  The names of the extensions as specified to *Configure*, and as
reported by \f(CW%Config::Config under the keys \f(CW\*(C`dynamic_ext\*(C',
\f(CW\*(C`known_extensions\*(C', \f(CW\*(C`nonxs_ext\*(C' and \f(CW\*(C`static_ext\*(C' have not changed, and
still use \f(CW\*(C`/\*(C'. Hence this change will not have any affect once perl is
installed. However, \f(CW\*(C`Attribute::Handlers\*(C', \f(CW\*(C`Safe\*(C' and \f(CW\*(C`mro\*(C' have now
become extensions in their own right, so if you run *Configure* with
options to specify an exact list of extensions to build, you will need to
change it to account for this.

For 5.10.2, it is planned that many dual-life modules will have been moved
from *lib* to *ext*; again this will have no effect on an installed
perl, but will matter if you invoke *Configure* with a pre-canned list of
extensions to build.

### Configuration improvements

Subsection "Configuration improvements"
If \f(CW\*(C`vendorlib\*(C' and \f(CW\*(C`vendorarch\*(C' are the same, then they are only added to
\f(CW@INC once.

\f(CW$Config\{usedevel\} and the C-level \f(CW\*(C`PERL_USE_DEVEL\*(C' are now defined if
perl is built with  \f(CW\*(C`-Dusedevel\*(C'.

*Configure* will enable use of \f(CW\*(C`-fstack-protector\*(C', to provide protection
against stack-smashing attacks, if the compiler supports it.

*Configure* will now determine the correct prototypes for re-entrant
functions, and for \f(CW\*(C`gconvert\*(C', if you are using a \*(C+ compiler rather
than a C compiler.

On Unix, if you build from a tree containing a git repository, the
configuration process will note the commit hash you have checked out, for
display in the output of \f(CW\*(C`perl -v\*(C' and \f(CW\*(C`perl -V\*(C'. Unpushed local commits
are automatically added to the list of local patches displayed by
\f(CW\*(C`perl -V\*(C'.

### Compilation improvements

Subsection "Compilation improvements"
As part of the flattening of *ext*, all extensions on all platforms are
built by *make_ext.pl*. This replaces the Unix-specific
*ext/util/make_ext*, VMS-specific *make_ext.com* and Win32-specific
*win32/buildext.pl*.

### Platform Specific Changes

Subsection "Platform Specific Changes"

- \s-1AIX\s0
Item "AIX"
Removed *libbsd* for \s-1AIX 5L\s0 and 6.1. Only **flock()** was used from *libbsd*.
.Sp
Removed *libgdbm* for \s-1AIX 5L\s0 and 6.1. The *libgdbm* is delivered as an
optional package with the \s-1AIX\s0 Toolbox. Unfortunately the 64 bit version
is broken.
.Sp
Hints changes mean that \s-1AIX 4.2\s0 should work again.

- Cygwin
Item "Cygwin"
On Cygwin we now strip the last number from the \s-1DLL.\s0 This has been the
behaviour in the cygwin.com build for years. The hints files have been
updated.

- FreeBSD
Item "FreeBSD"
The hints files now identify the correct threading libraries on FreeBSD 7
and later.

- Irix
Item "Irix"
We now work around a bizarre preprocessor bug in the Irix 6.5 compiler:
\f(CW\*(C`cc -E -\*(C' unfortunately goes into K&R mode, but \f(CW\*(C`cc -E file.c\*(C' doesn't.

- Haiku
Item "Haiku"
Patches from the Haiku maintainers have been merged in. Perl should now
build on Haiku.

- MirOS \s-1BSD\s0
Item "MirOS BSD"
Perl should now build on MirOS \s-1BSD.\s0

- NetBSD
Item "NetBSD"
Hints now supports versions 5.*.

- Stratus \s-1VOS\s0
Item "Stratus VOS"
Various changes from Stratus have been merged in.

- Symbian
Item "Symbian"
There is now support for Symbian S60 3.2 \s-1SDK\s0 and S60 5.0 \s-1SDK.\s0

- Win32
Item "Win32"
Improved message window handling means that \f(CW\*(C`alarm\*(C' and \f(CW\*(C`kill\*(C' messages
will no longer be dropped under race conditions.

- \s-1VMS\s0
Item "VMS"
Reads from the in-memory temporary files of \f(CW\*(C`PerlIO::scalar\*(C' used to fail
if \f(CW$/ was set to a numeric reference (to indicate record-style reads).
This is now fixed.
.Sp
\s-1VMS\s0 now supports \f(CW\*(C`getgrgid\*(C'.
.Sp
Many improvements and cleanups have been made to the \s-1VMS\s0 file name handling
and conversion code.
.Sp
Enabling the \f(CW\*(C`PERL_VMS_POSIX_EXIT\*(C' logical name now encodes a \s-1POSIX\s0 exit
status in a \s-1VMS\s0 condition value for better interaction with \s-1GNV\s0's bash
shell and other utilities that depend on \s-1POSIX\s0 exit values.  See
\*(L"$?\*(R" in perlvms for details.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
5.10.0 inadvertently disabled an optimisation, which caused a measurable
performance drop in list assignment, such as is often used to assign
function parameters from \f(CW@_. The optimisation has been re-instated, and
the performance regression fixed.

- \(bu
Fixed memory leak on \f(CW\*(C`while (1) \{ map 1, 1 \}\*(C' [\s-1RT\s0 #53038].

- \(bu
Some potential coredumps in PerlIO fixed [\s-1RT\s0 #57322,54828].

- \(bu
The debugger now works with lvalue subroutines.

- \(bu
The debugger's \f(CW\*(C`m\*(C' command was broken on modules that defined constants
[\s-1RT\s0 #61222].

- \(bu
\f(CW\*(C`crypt()\*(C' and string complement could return tainted values for untainted
arguments [\s-1RT\s0 #59998].

- \(bu
The \f(CW\*(C`-i.suffix\*(C' command-line switch now recreates the file using
restricted permissions, before changing its mode to match the original
file. This eliminates a potential race condition [\s-1RT\s0 #60904].

- \(bu
On some Unix systems, the value in \f(CW$? would not have the top bit set
(\f(CW\*(C`$? & 128\*(C') even if the child core dumped.

- \(bu
Under some circumstances, $^R could incorrectly become undefined
[\s-1RT\s0 #57042].

- \(bu
(\s-1XS\s0) In various hash functions, passing a pre-computed hash to when the
key is \s-1UTF-8\s0 might result in an incorrect lookup.

- \(bu
(\s-1XS\s0) Including *\s-1XSUB\s0.h* before *perl.h* gave a compile-time error
[\s-1RT\s0 #57176].

- \(bu
\f(CW\*(C`$object->isa(\*(AqFoo\*(Aq)\*(C' would report false if the package \f(CW\*(C`Foo\*(C' didn't
exist, even if the object's \f(CW@ISA contained \f(CW\*(C`Foo\*(C'.

- \(bu
Various bugs in the new-to 5.10.0 mro code, triggered by manipulating
\f(CW@ISA, have been found and fixed.

- \(bu
Bitwise operations on references could crash the interpreter, e.g.
\f(CW\*(C`$x=\\$y; $x |= "foo"\*(C' [\s-1RT\s0 #54956].

- \(bu
Patterns including alternation might be sensitive to the internal \s-1UTF-8\s0
representation, e.g.
.Sp
.Vb 3
    my $byte = chr(192);
    my $utf8 = chr(192); utf8::upgrade($utf8);
    $utf8 =~ /$byte|X\}/i;       # failed in 5.10.0
.Ve

- \(bu
Within UTF8-encoded Perl source files (i.e. where \f(CW\*(C`use utf8\*(C' is in
effect), double-quoted literal strings could be corrupted where a \f(CW\*(C`\\xNN\*(C',
\f(CW\*(C`\\0NNN\*(C' or \f(CW\*(C`\\N\{\}\*(C' is followed by a literal character with ordinal value
greater than 255 [\s-1RT\s0 #59908].

- \(bu
\f(CW\*(C`B::Deparse\*(C' failed to correctly deparse various constructs:
\f(CW\*(C`readpipe STRING\*(C' [\s-1RT\s0 #62428], \f(CW\*(C`CORE::require(STRING)\*(C' [\s-1RT\s0 #62488],
\f(CW\*(C`sub foo(_)\*(C' [\s-1RT\s0 #62484].

- \(bu
Using \f(CW\*(C`setpgrp()\*(C' with no arguments could corrupt the perl stack.

- \(bu
The block form of \f(CW\*(C`eval\*(C' is now specifically trappable by \f(CW\*(C`Safe\*(C' and
\f(CW\*(C`ops\*(C'.  Previously it was erroneously treated like string \f(CW\*(C`eval\*(C'.

- \(bu
In 5.10.0, the two characters \f(CW\*(C`[~\*(C' were sometimes parsed as the smart
match operator (\f(CW\*(C`~~\*(C') [\s-1RT\s0 #63854].

- \(bu
In 5.10.0, the \f(CW\*(C`*\*(C' quantifier in patterns was sometimes treated as
\f(CW\*(C`\{0,32767\}\*(C' [\s-1RT\s0 #60034, #60464]. For example, this match would fail:
.Sp
.Vb 1
    ("ab" x 32768) =~ /^(ab)*$/
.Ve

- \(bu
\f(CW\*(C`shmget\*(C' was limited to a 32 bit segment size on a 64 bit \s-1OS\s0 [\s-1RT\s0 #63924].

- \(bu
Using \f(CW\*(C`next\*(C' or \f(CW\*(C`last\*(C' to exit a \f(CW\*(C`given\*(C' block no longer produces a
spurious warning like the following:
.Sp
.Vb 1
    Exiting given via last at foo.pl line 123
.Ve

- \(bu
On Windows, \f(CW\*(Aq.\\foo\*(Aq and \f(CW\*(Aq..\\foo\*(Aq  were treated differently than
\f(CW\*(Aq./foo\*(Aq and \f(CW\*(Aq../foo\*(Aq by \f(CW\*(C`do\*(C' and \f(CW\*(C`require\*(C' [\s-1RT\s0 #63492].

- \(bu
Assigning a format to a glob could corrupt the format; e.g.:
.Sp
.Vb 1
     *bar=*foo\{FORMAT\}; # foo format now bad
.Ve

- \(bu
Attempting to coerce a typeglob to a string or number could cause an
assertion failure. The correct error message is now generated,
\f(CW\*(C`Can\*(Aqt coerce GLOB to \f(CI$type\f(CW\*(C'.

- \(bu
Under \f(CW\*(C`use filetest \*(Aqaccess\*(Aq\*(C', \f(CW\*(C`-x\*(C' was using the wrong access mode. This
has been fixed [\s-1RT\s0 #49003].

- \(bu
\f(CW\*(C`length\*(C' on a tied scalar that returned a Unicode value would not be
correct the first time. This has been fixed.

- \(bu
Using an array \f(CW\*(C`tie\*(C' inside in array \f(CW\*(C`tie\*(C' could \s-1SEGV.\s0 This has been
fixed. [\s-1RT\s0 #51636]

- \(bu
A race condition inside \f(CW\*(C`PerlIOStdio_close()\*(C' has been identified and
fixed. This used to cause various threading issues, including SEGVs.

- \(bu
In \f(CW\*(C`unpack\*(C', the use of \f(CW\*(C`()\*(C' groups in scalar context was internally
placing a list on the interpreter's stack, which manifested in various
ways, including SEGVs.  This is now fixed [\s-1RT\s0 #50256].

- \(bu
Magic was called twice in \f(CW\*(C`substr\*(C', \f(CW\*(C`\$x\*(C', \f(CW\*(C`tie $x, $m\*(C' and \f(CW\*(C`chop\*(C'.
These have all been fixed.

- \(bu
A 5.10.0 optimisation to clear the temporary stack within the implicit
loop of \f(CW\*(C`s///ge\*(C' has been reverted, as it turned out to be the cause of
obscure bugs in seemingly unrelated parts of the interpreter [commit
ef0d4e17921ee3de].

- \(bu
The line numbers for warnings inside \f(CW\*(C`elsif\*(C' are now correct.

- \(bu
The \f(CW\*(C`..\*(C' operator now works correctly with ranges whose ends are at or
close to the values of the smallest and largest integers.

- \(bu
\f(CW\*(C`binmode STDIN, \*(Aq:raw\*(Aq\*(C' could lead to segmentation faults on some platforms.
This has been fixed [\s-1RT\s0 #54828].

- \(bu
An off-by-one error meant that \f(CW\*(C`index $str, ...\*(C' was effectively being
executed as \f(CW\*(C`index "$str\\0", ...\*(C'. This has been fixed [\s-1RT\s0 #53746].

- \(bu
Various leaks associated with named captures in regexes have been fixed
[\s-1RT\s0 #57024].

- \(bu
A weak reference to a hash would leak. This was affecting \f(CW\*(C`DBI\*(C'
[\s-1RT\s0 #56908].

- \(bu
Using (?|) in a regex could cause a segfault [\s-1RT\s0 #59734].

- \(bu
Use of a \s-1UTF-8\s0 \f(CW\*(C`tr//\*(C' within a closure could cause a segfault [\s-1RT\s0 #61520].

- \(bu
Calling \f(CW\*(C`sv_chop()\*(C' or otherwise upgrading an \s-1SV\s0 could result in an
unaligned 64-bit access on the \s-1SPARC\s0 architecture [\s-1RT\s0 #60574].

- \(bu
In the 5.10.0 release, \f(CW\*(C`inc_version_list\*(C' would incorrectly list
\f(CW\*(C`5.10.*\*(C' after \f(CW\*(C`5.8.*\*(C'; this affected the \f(CW@INC search order
[\s-1RT\s0 #67628].

- \(bu
In 5.10.0, \f(CW\*(C`pack "a*", $tainted_value\*(C' returned a non-tainted value
[\s-1RT\s0 #52552].

- \(bu
In 5.10.0, \f(CW\*(C`printf\*(C' and \f(CW\*(C`sprintf\*(C' could produce the fatal error
\f(CW\*(C`panic: utf8_mg_pos_cache_update\*(C' when printing \s-1UTF-8\s0 strings
[\s-1RT\s0 #62666].

- \(bu
In the 5.10.0 release, a dynamically created \f(CW\*(C`AUTOLOAD\*(C' method might be
missed (method cache issue) [\s-1RT\s0 #60220,60232].

- \(bu
In the 5.10.0 release, a combination of \f(CW\*(C`use feature\*(C' and \f(CW\*(C`//ee\*(C' could
cause a memory leak [\s-1RT\s0 #63110].

- \(bu
\f(CW\*(C`-C\*(C' on the shebang (\f(CW\*(C`#!\*(C') line is once more permitted if it is also
specified on the command line. \f(CW\*(C`-C\*(C' on the shebang line used to be a
silent no-op *if* it was not also on the command line, so perl 5.10.0
disallowed it, which broke some scripts. Now perl checks whether it is
also on the command line and only dies if it is not [\s-1RT\s0 #67880].

- \(bu
In 5.10.0, certain types of re-entrant regular expression could crash,
or cause the following assertion failure [\s-1RT\s0 #60508]:
.Sp
.Vb 1
    Assertion rx->sublen >= (s - rx->subbeg) + i failed
.Ve

## New or Changed Diagnostics

Header "New or Changed Diagnostics"
.ie n .IP """panic: sv_chop %s""" 4
.el .IP "\f(CWpanic: sv_chop %s" 4
Item "panic: sv_chop %s"
This new fatal error occurs when the C routine \f(CW\*(C`Perl_sv_chop()\*(C' was
passed a position that is not within the scalar's string buffer. This
could be caused by buggy \s-1XS\s0 code, and at this point recovery is not
possible.
.ie n .IP """Can\*(Aqt locate package %s for the parents of %s""" 4
.el .IP "\f(CWCan\*(Aqt locate package %s for the parents of %s" 4
Item "Cant locate package %s for the parents of %s"
This warning has been removed. In general, it only got produced in
conjunction with other warnings, and removing it allowed an \s-1ISA\s0 lookup
optimisation to be added.
.ie n .IP """v-string in use/require is non-portable""" 4
.el .IP "\f(CWv-string in use/require is non-portable" 4
Item "v-string in use/require is non-portable"
This warning has been removed.
.ie n .IP """Deep recursion on subroutine ""%s""""" 4
.el .IP "\f(CWDeep recursion on subroutine ``%s''" 4
Item "Deep recursion on subroutine ""%s"""
It is now possible to change the depth threshold for this warning from the
default of 100, by recompiling the *perl* binary, setting the C
pre-processor macro \f(CW\*(C`PERL_SUB_DEPTH_WARN\*(C' to the desired value.

## Changed Internals

Header "Changed Internals"

- \(bu
The J.R.R. Tolkien quotes at the head of C source file have been checked and
proper citations added, thanks to a patch from Tom Christiansen.

- \(bu
\f(CW\*(C`vcroak()\*(C' now accepts a null first argument. In addition, a full audit
was made of the \*(L"not \s-1NULL\*(R"\s0 compiler annotations, and those for several
other internal functions were corrected.

- \(bu
New macros \f(CW\*(C`dSAVEDERRNO\*(C', \f(CW\*(C`dSAVE_ERRNO\*(C', \f(CW\*(C`SAVE_ERRNO\*(C', \f(CW\*(C`RESTORE_ERRNO\*(C'
have been added to formalise the temporary saving of the \f(CW\*(C`errno\*(C'
variable.

- \(bu
The function \f(CW\*(C`Perl_sv_insert_flags\*(C' has been added to augment
\f(CW\*(C`Perl_sv_insert\*(C'.

- \(bu
The function \f(CW\*(C`Perl_newSV_type(type)\*(C' has been added, equivalent to
\f(CW\*(C`Perl_newSV()\*(C' followed by \f(CW\*(C`Perl_sv_upgrade(type)\*(C'.

- \(bu
The function \f(CW\*(C`Perl_newSVpvn_flags()\*(C' has been added, equivalent to
\f(CW\*(C`Perl_newSVpvn()\*(C' and then performing the action relevant to the flag.
.Sp
Two flag bits are currently supported.

> .ie n .IP """SVf_UTF8""" 4
.el .IP "\f(CWSVf_UTF8" 4
Item "SVf_UTF8"
This will call \f(CW\*(C`SvUTF8_on()\*(C' for you. (Note that this does not convert an
sequence of \s-1ISO 8859-1\s0 characters to \s-1UTF-8\s0). A wrapper, \f(CW\*(C`newSVpvn_utf8()\*(C'
is available for this.
.ie n .IP """SVs_TEMP""" 4
.el .IP "\f(CWSVs_TEMP" 4
Item "SVs_TEMP"
Call \f(CW\*(C`sv_2mortal()\*(C' on the new \s-1SV.\s0



> .Sp
There is also a wrapper that takes constant strings, \f(CW\*(C`newSVpvs_flags()\*(C'.



- \(bu
The function \f(CW\*(C`Perl_croak_xs_usage\*(C' has been added as a wrapper to
\f(CW\*(C`Perl_croak\*(C'.

- \(bu
The functions \f(CW\*(C`PerlIO_find_layer\*(C' and \f(CW\*(C`PerlIO_list_alloc\*(C' are now
exported.

- \(bu
\f(CW\*(C`PL_na\*(C' has been exterminated from the core code, replaced by local \s-1STRLEN\s0
temporaries, or \f(CW\*(C`*_nolen()\*(C' calls. Either approach is faster than \f(CW\*(C`PL_na\*(C',
which is a pointer deference into the interpreter structure under ithreads,
and a global variable otherwise.

- \(bu
\f(CW\*(C`Perl_mg_free()\*(C' used to leave freed memory accessible via **SvMAGIC()** on
the scalar. It now updates the linked list to remove each piece of magic
as it is freed.

- \(bu
Under ithreads, the regex in \f(CW\*(C`PL_reg_curpm\*(C' is now reference counted. This
eliminates a lot of hackish workarounds to cope with it not being reference
counted.

- \(bu
\f(CW\*(C`Perl_mg_magical()\*(C' would sometimes incorrectly turn on \f(CW\*(C`SvRMAGICAL()\*(C'.
This has been fixed.

- \(bu
The *public* \s-1IV\s0 and \s-1NV\s0 flags are now not set if the string value has
trailing \*(L"garbage\*(R". This behaviour is consistent with not setting the
public \s-1IV\s0 or \s-1NV\s0 flags if the value is out of range for the type.

- \(bu
\s-1SV\s0 allocation tracing has been added to the diagnostics enabled by \f(CW\*(C`-Dm\*(C'.
The tracing can alternatively output via the \f(CW\*(C`PERL_MEM_LOG\*(C' mechanism, if
that was enabled when the *perl* binary was compiled.

- \(bu
Uses of \f(CW\*(C`Nullav\*(C', \f(CW\*(C`Nullcv\*(C', \f(CW\*(C`Nullhv\*(C', \f(CW\*(C`Nullop\*(C', \f(CW\*(C`Nullsv\*(C' etc have been
replaced by \f(CW\*(C`NULL\*(C' in the core code, and non-dual-life modules, as \f(CW\*(C`NULL\*(C'
is clearer to those unfamiliar with the core code.

- \(bu
A macro \f(CWMUTABLE_PTR(p) has been added, which on (non-pedantic) gcc will
not cast away \f(CW\*(C`const\*(C', returning a \f(CW\*(C`void *\*(C'. Macros \f(CW\*(C`MUTABLE_SV(av)\*(C',
\f(CW\*(C`MUTABLE_SV(cv)\*(C' etc build on this, casting to \f(CW\*(C`AV *\*(C' etc without
casting away \f(CW\*(C`const\*(C'. This allows proper compile-time auditing of
\f(CW\*(C`const\*(C' correctness in the core, and helped picked up some errors (now
fixed).

- \(bu
Macros \f(CW\*(C`mPUSHs()\*(C' and \f(CW\*(C`mXPUSHs()\*(C' have been added, for pushing SVs on the
stack and mortalizing them.

- \(bu
Use of the private structure \f(CW\*(C`mro_meta\*(C' has changed slightly. Nothing
outside the core should be accessing this directly anyway.

- \(bu
A new tool, \f(CW\*(C`Porting/expand-macro.pl\*(C' has been added, that allows you
to view how a C preprocessor macro would be expanded when compiled.
This is handy when trying to decode the macro hell that is the perl
guts.

## New Tests

Header "New Tests"
Many modules updated from \s-1CPAN\s0 incorporate new tests.

Several tests that have the potential to hang forever if they fail now
incorporate a \*(L"watchdog\*(R" functionality that will kill them after a timeout,
which helps ensure that \f(CW\*(C`make test\*(C' and \f(CW\*(C`make test_harness\*(C' run to
completion automatically. (Jerry Hedden).

Some core-specific tests have been added:

- t/comp/retainedlines.t
Item "t/comp/retainedlines.t"
Check that the debugger can retain source lines from \f(CW\*(C`eval\*(C'.

- t/io/perlio_fail.t
Item "t/io/perlio_fail.t"
Check that bad layers fail.

- t/io/perlio_leaks.t
Item "t/io/perlio_leaks.t"
Check that PerlIO layers are not leaking.

- t/io/perlio_open.t
Item "t/io/perlio_open.t"
Check that certain special forms of open work.

- t/io/perlio.t
Item "t/io/perlio.t"
General PerlIO tests.

- t/io/pvbm.t
Item "t/io/pvbm.t"
Check that there is no unexpected interaction between the internal types
\f(CW\*(C`PVBM\*(C' and \f(CW\*(C`PVGV\*(C'.

- t/mro/package_aliases.t
Item "t/mro/package_aliases.t"
Check that mro works properly in the presence of aliased packages.

- t/op/dbm.t
Item "t/op/dbm.t"
Tests for \f(CW\*(C`dbmopen\*(C' and \f(CW\*(C`dbmclose\*(C'.

- t/op/index_thr.t
Item "t/op/index_thr.t"
Tests for the interaction of \f(CW\*(C`index\*(C' and threads.

- t/op/pat_thr.t
Item "t/op/pat_thr.t"
Tests for the interaction of esoteric patterns and threads.

- t/op/qr_gc.t
Item "t/op/qr_gc.t"
Test that \f(CW\*(C`qr\*(C' doesn't leak.

- t/op/reg_email_thr.t
Item "t/op/reg_email_thr.t"
Tests for the interaction of regex recursion and threads.

- t/op/regexp_qr_embed_thr.t
Item "t/op/regexp_qr_embed_thr.t"
Tests for the interaction of patterns with embedded \f(CW\*(C`qr//\*(C' and threads.

- t/op/regexp_unicode_prop.t
Item "t/op/regexp_unicode_prop.t"
Tests for Unicode properties in regular expressions.

- t/op/regexp_unicode_prop_thr.t
Item "t/op/regexp_unicode_prop_thr.t"
Tests for the interaction of Unicode properties and threads.

- t/op/reg_nc_tie.t
Item "t/op/reg_nc_tie.t"
Test the tied methods of \f(CW\*(C`Tie::Hash::NamedCapture\*(C'.

- t/op/reg_posixcc.t
Item "t/op/reg_posixcc.t"
Check that \s-1POSIX\s0 character classes behave consistently.

- t/op/re.t
Item "t/op/re.t"
Check that exportable \f(CW\*(C`re\*(C' functions in *universal.c* work.

- t/op/setpgrpstack.t
Item "t/op/setpgrpstack.t"
Check that \f(CW\*(C`setpgrp\*(C' works.

- t/op/substr_thr.t
Item "t/op/substr_thr.t"
Tests for the interaction of \f(CW\*(C`substr\*(C' and threads.

- t/op/upgrade.t
Item "t/op/upgrade.t"
Check that upgrading and assigning scalars works.

- t/uni/lex_utf8.t
Item "t/uni/lex_utf8.t"
Check that Unicode in the lexer works.

- t/uni/tie.t
Item "t/uni/tie.t"
Check that Unicode and \f(CW\*(C`tie\*(C' work.

## Known Problems

Header "Known Problems"
This is a list of some significant unfixed bugs, which are regressions
from either 5.10.0 or 5.8.x.

- \(bu
\f(CW\*(C`List::Util::first\*(C' misbehaves in the presence of a lexical \f(CW$_
(typically introduced by \f(CW\*(C`my $_\*(C' or implicitly by \f(CW\*(C`given\*(C'). The variable
which gets set for each iteration is the package variable \f(CW$_, not the
lexical \f(CW$_ [\s-1RT\s0 #67694].
.Sp
A similar issue may occur in other modules that provide functions which
take a block as their first argument, like
.Sp
.Vb 1
    foo \{ ... $_ ...\} list
.Ve

- \(bu
The \f(CW\*(C`charnames\*(C' pragma may generate a run-time error when a regex is
interpolated [\s-1RT\s0 #56444]:
.Sp
.Vb 4
    use charnames \*(Aq:full\*(Aq;
    my $r1 = qr/\\N\{THAI CHARACTER SARA I\}/;
    "foo" =~ $r1;    # okay
    "foo" =~ /$r1+/; # runtime error
.Ve
.Sp
A workaround is to generate the character outside of the regex:
.Sp
.Vb 2
    my $a = "\\N\{THAI CHARACTER SARA I\}";
    my $r1 = qr/$a/;
.Ve

- \(bu
Some regexes may run much more slowly when run in a child thread compared
with the thread the pattern was compiled into [\s-1RT\s0 #55600].

## Deprecations

Header "Deprecations"
The following items are now deprecated.

- \(bu
\f(CW\*(C`Switch\*(C' is buggy and should be avoided. From perl 5.11.0 onwards, it is
intended that any use of the core version of this module will emit a
warning, and that the module will eventually be removed from the core
(probably in perl 5.14.0). See \*(L"Switch statements\*(R" in perlsyn for its
replacement.

- \(bu
\f(CW\*(C`suidperl\*(C' will be removed in 5.12.0. This provides a mechanism to
emulate setuid permission bits on systems that don't support it properly.

## Acknowledgements

Header "Acknowledgements"
Some of the work in this release was funded by a \s-1TPF\s0 grant.

Nicholas Clark officially retired from maintenance pumpking duty at the
end of 2008; however in reality he has put much effort in since then to
help get 5.10.1 into a fit state to be released, including writing a
considerable chunk of this perldelta.

Steffen Mueller and David Golden in particular helped getting \s-1CPAN\s0 modules
polished and synchronised with their in-core equivalents.

Craig Berry was tireless in getting maint to run under \s-1VMS,\s0 no matter how
many times we broke it for him.

The other core committers contributed most of the changes, and applied most
of the patches sent in by the hundreds of contributors listed in *\s-1AUTHORS\s0*.

(Sorry to all the people I haven't mentioned by name).

Finally, thanks to Larry Wall, without whom none of this would be
necessary.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes
all the core committers, who will be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for
security issues in the Perl core, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
