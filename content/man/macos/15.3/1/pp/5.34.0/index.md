+++
manpage_format = "troff"
keywords = ["header", "see", "also", "tkpp", "par", "pl", "parl", "perlcc", "s-1par", "s0", "packer", "module", "scandeps", "getopt", "long", "argvfile", "acknowledgments", "simon", "cozens", "tom", "christiansen", "and", "edward", "peschko", "for", "writing", "fiperlcc", "this", "program", "try", "to", "mimic", "its", "interface", "as", "close", "possible", "copied", "liberally", "from", "their", "code", "jan", "dubois", "the", "fiexetype", "utility", "which", "has", "been", "partially", "adapted", "into", "f", "cw", "c", "g", "flag", "mattia", "barbon", "providing", "myldr", "binary", "loader", "jeff", "goff", "suggesting", "name", "fipp", "authors", "audrey", "tang", "cpan", "audreyt", "org", "steffen", "mueller", "smueller", "roderich", "schupp", "rschupp", "you", "can", "write", "mailing", "list", "at", "perl", "or", "send", "an", "empty", "mail", "par-subscribe", "participate", "in", "discussion", "please", "submit", "bug", "reports", "bug-par-packer", "rt", "copyright", "2002-2009", "by", "neither", "nor", "associated", "impose", "any", "licensing", "restrictions", "on", "files", "generated", "execution", "accordance", "with", "8th", "article", "of", "artistic", "license", "vb", "5", "aggregation", "package", "a", "commercial", "distribution", "is", "always", "permitted", "provided", "that", "use", "embedded", "when", "no", "overt", "attempt", "made", "make", "aqs", "interfaces", "visible", "end", "user", "such", "shall", "not", "be", "construed", "ve", "therefore", "are", "absolutely", "free", "place", "resulting", "executable", "packed", "3rd-party", "libraries", "available", "under", "software", "redistribute", "it", "modify", "same", "terms", "itself", "fi", "s-1license"]
manpage_name = "pp"
description = "pp creates standalone executables from Perl programs, using the compressed packager provided by s-1PARs0, and dependency detection heuristics offered by Module::ScanDeps.  Source files are compressed verbatim without compilation. You may think of ..."
detected_package_version = "5.34.0"
date = "2020-03-08"
title = "pp(1)"
operating_system = "macos"
operating_system_version = "15.3"
author = "None Specified"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "PP 1"
PP 1 "2020-03-08" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

pp - PAR Packager

## SYNOPSIS

Header "SYNOPSIS"
**pp** [ **-ABCEFILMPTSVXacdefghilmnoprsuvxz** ] [ *parfile* | *scriptfile* ]...

## EXAMPLES

Header "EXAMPLES"
Note: When running on Microsoft Windows, the *a.out* below will be
replaced by *a.exe* instead.

.Vb 3
    % pp hello.pl               # Pack \*(Aqhello.pl\*(Aq into executable \*(Aqa.out\*(Aq
    % pp -o hello hello.pl      # Pack \*(Aqhello.pl\*(Aq into executable \*(Aqhello\*(Aq
                                # (or \*(Aqhello.exe\*(Aq on Win32)

    % pp -o foo foo.pl bar.pl   # Pack \*(Aqfoo.pl\*(Aq and \*(Aqbar.pl\*(Aq into \*(Aqfoo\*(Aq
    % ./foo                     # Run \*(Aqfoo.pl\*(Aq inside \*(Aqfoo\*(Aq
    % mv foo bar; ./bar         # Run \*(Aqbar.pl\*(Aq inside \*(Aqfoo\*(Aq
    % mv bar baz; ./baz         # Error: Can\*(Aqt open perl script "baz"

    % pp -p file                # Creates a PAR file, \*(Aqa.par\*(Aq
    % pp -o hello a.par         # Pack \*(Aqa.par\*(Aq to executable \*(Aqhello\*(Aq
    % pp -S -o hello file       # Combine the two steps above

    % pp -p -o out.par file     # Creates \*(Aqout.par\*(Aq from \*(Aqfile\*(Aq
    % pp -B -p -o out.par file  # same as above, but bundles core modules
                                # and removes any local paths from @INC
    % pp -P -o out.pl file      # Creates \*(Aqout.pl\*(Aq from \*(Aqfile\*(Aq
    % pp -B -p -o out.pl file   # same as above, but bundles core modules
                                # and removes any local paths from @INC
                                # (-B is assumed when making executables)

    % pp -e "print 123"         # Pack a one-liner into \*(Aqa.out\*(Aq
    % pp -p -e "print 123"      # Creates a PAR file \*(Aqa.par\*(Aq
    % pp -P -e "print 123"      # Creates a perl script \*(Aqa.pl\*(Aq

    % pp -c hello               # Check dependencies from "perl -c hello"
    % pp -x hello               # Check dependencies from "perl hello"
    % pp -n -x hello            # same as above, but skips static scanning

    % pp -I /foo hello          # Extra include paths
    % pp -M Foo::Bar hello      # Extra modules in the include path
    % pp -M abbrev.pl hello     # Extra libraries in the include path
    % pp -X Foo::Bar hello      # Exclude modules
    % pp -a data.txt hello      # Additional data files

    % pp -r hello               # Pack \*(Aqhello\*(Aq into \*(Aqa.out\*(Aq, runs \*(Aqa.out\*(Aq
    % pp -r hello a b c         # Pack \*(Aqhello\*(Aq into \*(Aqa.out\*(Aq, runs \*(Aqa.out\*(Aq
                                # with arguments \*(Aqa b c\*(Aq

    % pp hello --log=c          # Pack \*(Aqhello\*(Aq into \*(Aqa.out\*(Aq, logs
                                # messages into \*(Aqc\*(Aq

    # Pack \*(Aqhello\*(Aq into a console-less \*(Aqout.exe\*(Aq (Win32 only)
    % pp --gui -o out.exe hello

    % pp @file hello.pl         # Pack \*(Aqhello.pl\*(Aq but read _additional_
                                # options from file \*(Aqfile\*(Aq
.Ve

## DESCRIPTION

Header "DESCRIPTION"
*pp* creates standalone executables from Perl programs, using the
compressed packager provided by \s-1PAR\s0, and dependency detection
heuristics offered by Module::ScanDeps.  Source files are compressed
verbatim without compilation.

You may think of *pp* as "*perlcc* that works without hassle". :-)

A \s-1GUI\s0 interface is also available as the *tkpp* command.

It does not provide the compilation-step acceleration provided by
*perlcc* (however, see **-f** below for byte-compiled, source-hiding
techniques), but makes up for it with better reliability, smaller
executable size, and full retrieval of original source code.

When a single input program is specified, the resulting executable will
behave identically as that program.  However, when multiple programs
are packaged, the produced executable will run the one that has the
same basename as \f(CW$0 (i.e. the filename used to invoke it).  If
nothing matches, it dies with the error \f(CW\*(C`Can\*(Aqt open perl script "$0"\*(C'.

## OPTIONS

Header "OPTIONS"
Options are available in a *short* form and a *long* form.  For
example, the three lines below are all equivalent:

.Vb 3
    % pp -o output.exe input.pl
    % pp --output output.exe input.pl
    % pp --output=output.exe input.pl
.Ve

Since the command lines can become sufficiently long to reach the limits
imposed by some shells, it is possible to have *pp* read some of its
options from one or more text files. The basic usage is to just include
an argument starting with an 'at' (@) sigil. This argument will be
interpreted as a file to read options from. Mixing ordinary options
and \f(CW@file options is possible. This is implemented using the
Getopt::ArgvFile module, so read its documentation for advanced usage.

- \fB-a, \fB--addfile=\fI\s-1FILE\s0|\fI\s-1DIR\s0
Item "-a, --addfile=FILE|DIR"
Add an extra file into the package.  If the file is a directory, recursively
add all files inside that directory, with links turned into actual files.
.Sp
By default, files are placed under \f(CW\*(C`/\*(C' inside the package with their
original names.  You may override this by appending the target filename
after a \f(CW\*(C`;\*(C', like this:
.Sp
.Vb 2
    % pp -a "old_filename.txt;new_filename.txt"
    % pp -a "old_dirname;new_dirname"
.Ve
.Sp
You may specify \f(CW\*(C`-a\*(C' multiple times.

- \fB-A, \fB--addlist=\fI\s-1FILE\s0
Item "-A, --addlist=FILE"
Read a list of file/directory names from *\s-1FILE\s0*, adding them into the
package.  Each line in *\s-1FILE\s0* is taken as an argument to **-a** above.
.Sp
You may specify \f(CW\*(C`-A\*(C' multiple times.

- \fB-B, \fB--bundle
Item "-B, --bundle"
Bundle core modules in the resulting package.  This option is enabled
by default, except when \f(CW\*(C`-p\*(C' or \f(CW\*(C`-P\*(C' is specified.
.Sp
Since \s-1PAR\s0 version 0.953, this also strips any local paths from the
list of module search paths \f(CW@INC before running the contained
script.

- \fB-C, \fB--clean
Item "-C, --clean"
Clean up temporary files extracted from the application at runtime.
By default, these files are cached in the temporary directory; this
allows the program to start up faster next time.

- \fB-c, \fB--compile
Item "-c, --compile"
Run \f(CW\*(C`perl -c inputfile\*(C' to determine additional run-time dependencies.

- \fB-cd, \fB--cachedeps=\fI\s-1FILE\s0
Item "-cd, --cachedeps=FILE"
Use *\s-1FILE\s0* to cache detected dependencies. Creates *\s-1FILE\s0* unless
present. This will speed up the scanning process on subsequent runs.

- \fB-d, \fB--dependent
Item "-d, --dependent"
Reduce the executable size by not including a copy of perl interpreter.
Executables built this way will need a separate *perl5x.dll*
or *libperl.so* to function correctly.  This option is only available
if perl is built as a shared library.

- \fB-e, \fB--eval=\fI\s-1STRING\s0
Item "-e, --eval=STRING"
Package a one-liner, much the same as \f(CW\*(C`perl -e \*(Aq...\*(Aq\*(C'

- \fB-E, \fB--evalfeature=\fI\s-1STRING\s0
Item "-E, --evalfeature=STRING"
Behaves just like \f(CW\*(C`-e\*(C', except that it implicitly enables all optional features
(in the main compilation unit) with Perl 5.10 and later.  See feature.

- \fB-x, \fB--execute
Item "-x, --execute"
Run \f(CW\*(C`perl inputfile\*(C' to determine additional run-time dependencies.
.Sp
Using this option, *pp* may be able to detect the use of modules that
can't be determined by static analysis of \f(CW\*(C`inputfile\*(C'. Examples
are stuff loaded by run-time loaders like Module::Runtime or
\*(L"plugin\*(R" loaders like Module::Loader. Note that which modules are
detected depends on which parts of your program are exercised
when running \f(CW\*(C`inputfile\*(C'. E.g. if your program immediately terminates
when run as \f(CW\*(C`perl inputfile\*(C' because it lacks mandatory arguments,
then this option will probably have no effect. You may use **--xargs** to
supply arguments in this case.

- \fB--xargs=\fI\s-1STRING\s0
Item "--xargs=STRING"
If **-x** is given, splits the \f(CW\*(C`STRING\*(C' using the function
\f(CW\*(C`shellwords\*(C' from Text::ParseWords and passes the result
as \f(CW@ARGV when running \f(CW\*(C`perl inputfile\*(C'.

- \fB-X, \fB--exclude=\fI\s-1MODULE\s0
Item "-X, --exclude=MODULE"
Exclude the given module from the dependency search path and from the
package. If the given file is a zip or par or par executable, all the files
in the given file (except \s-1MANIFEST, META\s0.yml and script/*) will be
excluded and the output file will \*(L"use\*(R" the given file at runtime.

- \fB-f, \fB--filter=\fI\s-1FILTER\s0
Item "-f, --filter=FILTER"
Filter source script(s) with a PAR::Filter subclass.  You may specify
multiple such filters.
.Sp
If you wish to hide the source code from casual prying, this will do:
.Sp
.Vb 1
    % pp -f Bleach source.pl
.Ve
.Sp
If you are more serious about hiding your source code, you should have
a look at Steve Hay's PAR::Filter::Crypto module. Make sure you
understand the Filter::Crypto caveats!

- \fB-g, \fB--gui
Item "-g, --gui"
Build an executable that does not have a console window. This option is
ignored on non-MSWin32 platforms or when \f(CW\*(C`-p\*(C' is specified.

- \fB-h, \fB--help
Item "-h, --help"
Show basic usage information.

- \fB-I, \fB--lib=\fI\s-1DIR\s0
Item "-I, --lib=DIR"
Add the given directory to the perl module search path.  May
be specified multiple times.

- \fB-l, \fB--link=\fI\s-1FILE\s0|\fI\s-1LIBRARY\s0
Item "-l, --link=FILE|LIBRARY"
Add the given shared library (a.k.a. shared object or \s-1DLL\s0) into the
packed file.  Also accepts names under library paths; i.e.
\f(CW\*(C`-l ncurses\*(C' means the same thing as \f(CW\*(C`-l libncurses.so\*(C' or
\f(CW\*(C`-l /usr/local/lib/libncurses.so\*(C' in most Unixes.  May be specified
multiple times.

- \fB-L, \fB--log=\fI\s-1FILE\s0
Item "-L, --log=FILE"
Log the output of packaging to a file rather than to stdout.

- \fB-F, \fB--modfilter=\fIFILTER[=REGEX],
Item "-F, --modfilter=FILTER[=REGEX],"
Filter included perl module(s) with a PAR::Filter subclass.
You may specify multiple such filters.
.Sp
By default, the *PodStrip* filter is applied.  In case
that causes trouble, you can turn this off by setting the
environment variable \f(CW\*(C`PAR_VERBATIM\*(C' to \f(CW1.
.Sp
Since \s-1PAR 0.958,\s0 you can use an optional regular expression (*\s-1REGEX\s0* above)
to select the files in the archive which should be filtered. Example:
.Sp
.Vb 1
  pp -o foo.exe -F Bleach=warnings\\.pm$ foo.pl
.Ve
.Sp
This creates a binary executable *foo.exe* from *foo.pl* packaging all files
as usual except for files ending in \f(CW\*(C`warnings.pm\*(C' which are filtered with
PAR::Filter::Bleach.

- \fB-M, \fB--module=\fI\s-1MODULE\s0
Item "-M, --module=MODULE"
Add the specified module into the package, along with its dependencies.
.Sp
The following variants may be used to add whole module namespaces:

> 
- \fB-M Foo::**
Item "-M Foo::**"
Add every module in the \f(CW\*(C`Foo\*(C' namespace **except** \f(CW\*(C`Foo\*(C' itself, i.e.
add \f(CW\*(C`Foo::Bar\*(C', \f(CW\*(C`Foo::Bar::Quux\*(C' etc up to any depth.

- \fB-M Foo::*
Item "-M Foo::*"
Add every module at level 1 in the \f(CW\*(C`Foo\*(C' namespace, i.e.
add \f(CW\*(C`Foo::Bar\*(C', but **neither** \f(CW\*(C`Foo::Bar::Quux\*(C' **nor** \f(CW\*(C`Foo\*(C'.

- \fB-M Foo::
Item "-M Foo::"
Shorthand for \f(CW\*(C`-M Foo -M Foo:**\*(C': every module in the \f(CW\*(C`Foo\*(C' namespace
including \f(CW\*(C`Foo\*(C' itself.



> .Sp
Instead of a module name, *\s-1MODULE\s0* may also be specified as a filename
relative to the \f(CW@INC path, i.e.  \f(CW\*(C`-M Module/ScanDeps.pm\*(C'
means the same thing as \f(CW\*(C`-M Module::ScanDeps\*(C'.
.Sp
If *\s-1MODULE\s0* has an extension that is not \f(CW\*(C`.pm\*(C'/\f(CW\*(C`.ix\*(C'/\f(CW\*(C`.al\*(C', it will not
be scanned for dependencies, and will be placed under \f(CW\*(C`/\*(C' instead of
\f(CW\*(C`/lib/\*(C' inside the \s-1PAR\s0 file.  This use is **deprecated** \*(-- consider using
the **-a** option instead.
.Sp
You may specify \f(CW\*(C`-M\*(C' multiple times.



- \fB-m, \fB--multiarch
Item "-m, --multiarch"
Build a multi-architecture \s-1PAR\s0 file.  Implies **-p**.

- \fB-n, \fB--noscan
Item "-n, --noscan"
Skip the default static scanning altogether, using run-time
dependencies from **-c** or **-x** exclusively.

- \fB-N, \fB--namespace=\fI\s-1NAMESPACE\s0
Item "-N, --namespace=NAMESPACE"
Add all modules in the namespace into the package,
along with their dependencies. If \f(CW\*(C`NAMESPACE\*(C' is something like \f(CW\*(C`Foo::Bar\*(C'
then this will add all modules \f(CW\*(C`Foo/Bar/Quux.pm\*(C', \f(CW\*(C`Foo/Bar/Fred/Barnie.pm\*(C' etc
that can be located in your module search path. It mimics the behaviour
of \*(L"plugin\*(R" loaders like Module::Loader.
.Sp
This is different from using \f(CW\*(C`-M Foo::Bar::\*(C', as the latter insists
on adding \f(CW\*(C`Foo/Bar.pm\*(C' which might not exist in the above \*(L"plugin\*(R" scenario.
.Sp
You may specify \f(CW\*(C`-N\*(C' multiple times.

- \fB-o, \fB--output=\fI\s-1FILE\s0
Item "-o, --output=FILE"
File name for the final packaged executable.

- \fB-p, \fB--par
Item "-p, --par"
Create \s-1PAR\s0 archives only; do not package to a standalone binary.

- \fB-P, \fB--perlscript
Item "-P, --perlscript"
Create stand-alone perl script; do not package to a standalone binary.

- \fB-r, \fB--run
Item "-r, --run"
Run the resulting packaged script after packaging it.

- \fB--reusable
Item "--reusable"
**\s-1EXPERIMENTAL\s0**
.Sp
Make the packaged executable reusable for running arbitrary, external
Perl scripts as if they were part of the package:
.Sp
.Vb 2
  pp -o myapp --reusable someapp.pl
  ./myapp --par-options --reuse otherapp.pl
.Ve
.Sp
The second line will run *otherapp.pl* instead of *someapp.pl*.

- \fB-S, \fB--save
Item "-S, --save"
Do not delete generated \s-1PAR\s0 file after packaging.

- \fB-s, \fB--sign
Item "-s, --sign"
Cryptographically sign the generated \s-1PAR\s0 or binary file using
Module::Signature.

- \fB-T, \fB--tempcache
Item "-T, --tempcache"
Set the program unique part of the cache directory name that is used
if the program is run without -C. If not set, a hash of the executable
is used.
.Sp
When the program is run, its contents are extracted to a temporary
directory.  On Unix systems, this is commonly
*/tmp/par-USER/cache-XXXXXXX*.  *\s-1USER\s0* is replaced by the
name of the user running the program, but \*(L"spelled\*(R" in hex.
*\s-1XXXXXXX\s0* is either a hash of the
executable or the value passed to the \f(CW\*(C`-T\*(C' or \f(CW\*(C`--tempcache\*(C' switch.

- \fB-u, \fB--unicode
Item "-u, --unicode"
Package Unicode support (essentially *utf8_heavy.pl* and everything
below the directory *unicore* in your perl library).
.Sp
This option exists because it is impossible to detect using static analysis
if your program needs Unicode support at runtime. (Note: If your
program contains \f(CW\*(C`use utf8\*(C' this does **not** imply it needs Unicode
support. It merely says that your program is written in \s-1UTF-8.\s0)
.Sp
If your packed program exits with an error message like
.Sp
.Vb 1
  Can\*(Aqt locate utf8_heavy.pl in @INC (@INC contains: ...)
.Ve
.Sp
try to pack it with \f(CW\*(C`-u\*(C' (or use \f(CW\*(C`-x\*(C').

- \fB-v, \fB--verbose[=\fI\s-1NUMBER\s0]
Item "-v, --verbose[=NUMBER]"
Increase verbosity of output; *\s-1NUMBER\s0* is an integer from \f(CW1 to \f(CW3,
\f(CW3 being the most verbose.  Defaults to \f(CW1 if specified without an
argument.  Alternatively, **-vv** sets verbose level to \f(CW2, and **-vvv**
sets it to \f(CW3.

- \fB-V, \fB--version
Item "-V, --version"
Display the version number and copyrights of this program.

- \fB-z, \fB--compress=\fI\s-1NUMBER\s0
Item "-z, --compress=NUMBER"
Set zip compression level; *\s-1NUMBER\s0* is an integer from \f(CW0 to \f(CW9,
\f(CW0 = no compression, \f(CW9 = max compression.  Defaults to \f(CW6 if
**-z** is not used.

## ENVIRONMENT

Header "ENVIRONMENT"

- \s-1PP_OPTS\s0
Item "PP_OPTS"
Command-line options (switches).  Switches in this variable are taken
as if they were on every *pp* command line.

## NOTES

Header "NOTES"
Here are some recipes showing how to utilize *pp* to bundle
*source.pl* with all its dependencies, on target machines with
different expected settings:

- Stone-alone setup:
Item "Stone-alone setup:"
To make a stand-alone executable, suitable for running on a
machine that doesn't have perl installed:
.Sp
.Vb 3
    % pp -o packed.exe source.pl        # makes packed.exe
    # Now, deploy \*(Aqpacked.exe\*(Aq to target machine...
    $ packed.exe                        # run it
.Ve

- Perl interpreter only, without core modules:
Item "Perl interpreter only, without core modules:"
To make a packed .pl file including core modules, suitable
for running on a machine that has a perl interpreter, but where
you want to be sure of the versions of the core modules that
your program uses:
.Sp
.Vb 3
    % pp -B -P -o packed.pl source.pl   # makes packed.pl
    # Now, deploy \*(Aqpacked.pl\*(Aq to target machine...
    $ perl packed.pl                    # run it
.Ve

- Perl with core modules installed:
Item "Perl with core modules installed:"
To make a packed .pl file without core modules, relying on the target
machine's perl interpreter and its core libraries.  This produces
a significantly smaller file than the previous version:
.Sp
.Vb 3
    % pp -P -o packed.pl source.pl      # makes packed.pl
    # Now, deploy \*(Aqpacked.pl\*(Aq to target machine...
    $ perl packed.pl                    # run it
.Ve

- Perl with \s-1PAR\s0.pm and its dependencies installed:
Item "Perl with PAR.pm and its dependencies installed:"
Make a separate archive and executable that uses the archive. This
relies upon the perl interpreter and libraries on the target machine.
.Sp
.Vb 5
    % pp -p source.pl                   # makes source.par
    % echo "use PAR \*(Aqsource.par\*(Aq;" > packed.pl;
    % cat source.pl >> packed.pl;       # makes packed.pl
    # Now, deploy \*(Aqsource.par\*(Aq and \*(Aqpacked.pl\*(Aq to target machine...
    $ perl packed.pl                    # run it, perl + core modules required
.Ve

Note that even if your perl was built with a shared library, the
'Stand-alone executable' above will *not* need a separate *perl5x.dll*
or *libperl.so* to function correctly.  But even in this case, the
underlying system libraries such as *libc* must be compatible between
the host and target machines.  Use \f(CW\*(C`--dependent\*(C' if you
are willing to ship the shared library with the application, which
can significantly reduce the executable size.

## SEE ALSO

Header "SEE ALSO"
tkpp, par.pl, parl, perlcc

\s-1PAR\s0, PAR::Packer, Module::ScanDeps

Getopt::Long, Getopt::ArgvFile

## ACKNOWLEDGMENTS

Header "ACKNOWLEDGMENTS"
Simon Cozens, Tom Christiansen and Edward Peschko for writing
*perlcc*; this program try to mimic its interface as close
as possible, and copied liberally from their code.

Jan Dubois for writing the *exetype.pl* utility, which has been
partially adapted into the \f(CW\*(C`-g\*(C' flag.

Mattia Barbon for providing the \f(CW\*(C`myldr\*(C' binary loader code.

Jeff Goff for suggesting the name *pp*.

## AUTHORS

Header "AUTHORS"
Audrey Tang <cpan@audreyt.org>,
Steffen Mueller <smueller@cpan.org>
Roderich Schupp <rschupp@cpan.org>

You can write
to the mailing list at <par@perl.org>, or send an empty mail to
<par-subscribe@perl.org> to participate in the discussion.

Please submit bug reports to <bug-par-packer@rt.cpan.org>.

## COPYRIGHT

Header "COPYRIGHT"
Copyright 2002-2009 by Audrey Tang
<cpan@audreyt.org>.

Neither this program nor the associated parl program impose any
licensing restrictions on files generated by their execution, in
accordance with the 8th article of the Artistic License:

.Vb 5
    "Aggregation of this Package with a commercial distribution is
    always permitted provided that the use of this Package is embedded;
    that is, when no overt attempt is made to make this Package\*(Aqs
    interfaces visible to the end user of the commercial distribution.
    Such use shall not be construed as a distribution of this Package."
.Ve

Therefore, you are absolutely free to place any license on the resulting
executable, as long as the packed 3rd-party libraries are also available
under the Artistic License.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See *\s-1LICENSE\s0*.
