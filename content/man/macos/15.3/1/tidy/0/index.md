+++
operating_system = "macos"
manpage_format = "troff"
manpage_name = "tidy"
date = "2002-12-01"
detected_package_version = "0"
description = "Tidy reads HTML, XHTML and XML files and writes cleaned up markup.  For HTML varians, it detects and corrects many common coding errors and strives to produce visually equivalent  markup that is both W3C complaint and works on most browsers. A com..."
manpage_section = "1"
operating_system_version = "15.3"
keywords = ["html", "tidy", "project", "page", "at", "http", "sourceforge", "net", "dave", "raggett", "s", "overview", "www", "w3", "org", "people", "quick", "reference", "docs", "quickref", "for", "information", "about", "tidylib", "see", "libintro", "authors", "dsr", "terry", "teague", "terry_teague", "users", "bjoern", "hoehrmann", "de", "charles", "reitzel", "creitzel", "rcn", "com", "this", "manual", "was", "written", "by", "matej", "vela", "debian", "and", "updated"]
title = "tidy(1)"
author = "None Specified"
+++

TIDY 1 "December 1, 2002"

## NAME

tidy - validate, correct, and pretty-print HTML files

## SYNOPSIS

tidy
[option ...] [file ...] [option ...] [file ...]

## DESCRIPTION

.P
Tidy reads HTML, XHTML and XML files and writes cleaned up
markup.  For HTML varians, it detects and corrects many common
coding errors and strives to produce visually equivalent
markup that is both W3C complaint and works on most browsers.
A common use of Tidy is to convert plain HTML to XHTML.  For
generic XML files, Tidy is limited to correcting basic well-formedness
errors and pretty printing.
.P
If no markup file is specified, Tidy reads the standard
input.  If no output file is specified, Tidy writes markup
to the standard output.  If no error file is specified, Tidy
writes messages to the standard error.

## OPTIONS

.B
Processing directives

-indent  or -i
to indent element content

-omit
to omit optional end tags

-wrap <column>
to wrap text at the specified <column> (default is 68)

-upper   or -u
to force tags to upper case (default is lower case)

-clean   or -c
to replace FONT, NOBR and CENTER tags by CSS

-bare    or -b
to strip out smart quotes and em dashes, etc.

-numeric or -n
to output numeric rather than named entities

-errors  or -e
to only show errors

-quiet   or -q
to suppress nonessential output

-xml
to specify the input is well formed XML

-asxml
to convert HTML to well formed XHTML

-asxhtml
to convert HTML to well formed XHTML

-ashtml
to force XHTML to well formed HTML

-access \<level\>
to do additional accessibility checks (\<level\> = 1, 2, 3)

Character encodings

-raw
to output values above 127 without conversion to entities

-ascii
to use US-ASCII for output, ISO-8859-1 for input

-latin1
to use ISO-8859-1 for both input and output

-iso2022
to use ISO-2022 for both input and output

-utf8
to use UTF-8 for both input and output

-mac
to use MacRoman for input, US-ASCII for output

-utf16le
to use UTF-16LE for both input and output

-utf16be
to use UTF-16BE for both input and output

-utf16
to use UTF-16 for both input and output

-win1252
to use Windows-1252 for input, US-ASCII for output

-big5
to use Big5 for both input and output

-shiftjis
to use Shift_JIS for both input and output

-language \<lang\>
to set the two-letter language code <lang> (for future use)

File manipulation

-output or -o <file>
to write output to the specified <file>

-f      <file>
to write errors to the specified <file>

-config <file>
to set configuration options from the specified <file>

-modify or -m
to modify the original input files

Miscellaneous

-version  or -v
to show the version of Tidy

-help, -h or -?
to list the command line options

-help-config
to list all configuration options

-show-config
to list the current configuration settings

## USAGE

.P
Use --blah blarg for any configuration option "blah" with argument "blarg"
.P
Input/Output default to stdin/stdout respectively
Single letter options apart from -f and -o may be combined
as in:  tidy -f errs.txt -imu foo.html
For further info on HTML see http://www.w3.org/MarkUp
.P
For more information about HTML Tidy, visit the project home page at
http://tidy.sourceforge.net.  Here, you will find links to documentation,
mailing lists (with searchable archives) and links to report bugs.

## ENVIRONMENT


HTML_TIDY
Name of the default configuration file.  This should be an absolute path,
since you will probably invoke
tidy
from different directories.  The value of HTML_TIDY will be parsed after the
compiled-in default (defined with -DCONFIG_FILE), but before any of the
files specified using
-config .

## EXIT STATUS

0
All input files were processed successfully.
1
There were warnings.
2
There were errors.

## SEE ALSO

.P
HTML Tidy Project Page at http://tidy.sourceforge.net
.P
Dave Raggett's Tidy Overview at http://www.w3.org/People/Raggett/tidy/
.P
Tidy Quick Reference at http://tidy.sourceforge.net/docs/quickref.html
.P
For information about TidyLib, see http://tidy.sourceforge.net/libintro.html

## AUTHORS

.P
Dave Raggett <dsr@w3.org>.
.P
Terry Teague <terry_teague@users.sourceforge.net>.
.P
Bjoern Hoehrmann <bjoern@hoehrmann.de>
.P
Charles Reitzel <creitzel@rcn.com>
.P
This manual page was written by Matej Vela <vela@debian.org> and updated
by Charles Reitzel.

