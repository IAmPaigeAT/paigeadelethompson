+++
manpage_format = "troff"
author = "None Specified"
manpage_section = "1"
date = "2003-02-06"
detected_package_version = "0"
description = "tab2space expands tab characters into a specific number of spaces. It also normalizes line endings into a single format. display this help message set line ends to CRLF (PC-DOS/Windows - default) set line ends to CR (classic Mac OS) set line ends..."
keywords = ["html", "tidy", "project", "page", "at", "http", "sourceforge", "net", "author", "dave", "raggett", "dsr", "w3", "org"]
manpage_name = "tab2space"
operating_system_version = "15.3"
title = "tab2space(1)"
operating_system = "macos"
+++

TAB2SPACE 1 "February 6, 2003"

## NAME

tab2space - Utility to expand tabs and ensure consistent line endings

## SYNOPSIS

tab2space
[options] [infile [outfile]] ...

## DESCRIPTION

.P
tab2space expands tab characters into a specific number of spaces.
It also normalizes line endings into a single format.

## OPTIONS


-help or -h
display this help message

-dos or -crlf
set line ends to CRLF (PC-DOS/Windows - default)

-mac or -cr
set line ends to CR (classic Mac OS)

-unix or -lf
set line ends to LF (Unix / Mac OS X)

-tabs
preserve tabs, e.g. for Makefile

-t<n>
set tabs to <n> (default is 4) spaces

## SEE ALSO

.P
HTML Tidy Project Page at http://tidy.sourceforge.net

## AUTHOR

.P
Dave Raggett <dsr@w3.org>
