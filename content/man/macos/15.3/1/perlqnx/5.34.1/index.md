+++
title = "perlqnx(1)"
manpage_format = "troff"
operating_system = "macos"
operating_system_version = "15.3"
description = "As of perl5.7.2 all tests pass under:   QNX 4.24G   Watcom 10.6 with Beta/970211.wcc.update.tar.F   socket3r.lib Nov21 1996. As of perl5.8.1 there is at least one test still failing. Some tests may complain under known circumstances. See below a..."
manpage_section = "1"
detected_package_version = "5.34.1"
author = "None Specified"
manpage_name = "perlqnx"
date = "2022-02-19"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLQNX 1"
PERLQNX 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlqnx - Perl version 5 on QNX

## DESCRIPTION

Header "DESCRIPTION"
As of perl5.7.2 all tests pass under:

.Vb 3
  QNX 4.24G
  Watcom 10.6 with Beta/970211.wcc.update.tar.F
  socket3r.lib Nov21 1996.
.Ve

As of perl5.8.1 there is at least one test still failing.

Some tests may complain under known circumstances.

See below and hints/qnx.sh for more information.

Under \s-1QNX 6.2.0\s0 there are still a few tests which fail.
See below and hints/qnx.sh for more information.

### Required Software for Compiling Perl on \s-1QNX4\s0

Subsection "Required Software for Compiling Perl on QNX4"
As with many unix ports, this one depends on a few \*(L"standard\*(R"
unix utilities which are not necessarily standard for \s-1QNX4.\s0

- /bin/sh
Item "/bin/sh"
This is used heavily by Configure and then by
perl itself. \s-1QNX4\s0's version is fine, but Configure
will choke on the 16-bit version, so if you are
running \s-1QNX 4.22,\s0 link /bin/sh to /bin32/ksh

- ar
Item "ar"
This is the standard unix library builder.
We use wlib. With Watcom 10.6, when wlib is
linked as \*(L"ar\*(R", it behaves like ar and all is
fine. Under 9.5, a cover is required. One is
included in ../qnx

- nm
Item "nm"
This is used (optionally) by configure to list
the contents of libraries. I will generate
a cover function on the fly in the \s-1UU\s0 directory.

- cpp
Item "cpp"
Configure and perl need a way to invoke a C
preprocessor. I have created a simple cover
for cc which does the right thing. Without this,
Configure will create its own wrapper which works,
but it doesn't handle some of the command line arguments
that perl will throw at it.

- make
Item "make"
You really need \s-1GNU\s0 make to compile this. \s-1GNU\s0 make
ships by default with \s-1QNX 4.23,\s0 but you can get it
from quics for earlier versions.

### Outstanding Issues with Perl on \s-1QNX4\s0

Subsection "Outstanding Issues with Perl on QNX4"
There is no support for dynamically linked libraries in \s-1QNX4.\s0

If you wish to compile with the Socket extension, you need
to have the \s-1TCP/IP\s0 toolkit, and you need to make sure that
-lsocket locates the correct copy of socket3r.lib. Beware
that the Watcom compiler ships with a stub version of
socket3r.lib which has very little functionality. Also
beware the order in which wlink searches directories for
libraries. You may have /usr/lib/socket3r.lib pointing to
the correct library, but wlink may pick up
/usr/watcom/10.6/usr/lib/socket3r.lib instead. Make sure
they both point to the correct library, that is,
/usr/tcptk/current/usr/lib/socket3r.lib.

The following tests may report errors under \s-1QNX4:\s0

dist/Cwd/Cwd.t will complain if `pwd` and cwd don't give
the same results. cwd calls `fullpath -t`, so if you
cd `fullpath -t` before running the test, it will
pass.

lib/File/Find/taint.t will complain if '.' is in your
\s-1PATH.\s0 The \s-1PATH\s0 test is triggered because cwd calls
`fullpath -t`.

ext/IO/lib/IO/t/io_sock.t: Subtests 14 and 22 are skipped due to
the fact that the functionality to read back the non-blocking
status of a socket is not implemented in \s-1QNX\s0's \s-1TCP/IP.\s0 This has
been reported to \s-1QNX\s0 and it may work with later versions of
\s-1TCP/IP.\s0

t/io/tell.t: Subtest 27 is failing. We are still investigating.

### \s-1QNX\s0 auxiliary files

Subsection "QNX auxiliary files"
The files in the \*(L"qnx\*(R" directory are:

- qnx/ar
Item "qnx/ar"
A script that emulates the standard unix archive (aka library)
utility.  Under Watcom 10.6, ar is linked to wlib and provides the
expected interface. With Watcom 9.5, a cover function is
required. This one is fairly crude but has proved adequate for
compiling perl.

- qnx/cpp
Item "qnx/cpp"
A script that provides C preprocessing functionality.  Configure can
generate a similar cover, but it doesn't handle all the command-line
options that perl throws at it. This might be reasonably placed in
/usr/local/bin.

### Outstanding issues with perl under \s-1QNX6\s0

Subsection "Outstanding issues with perl under QNX6"
The following tests are still failing for Perl 5.8.1 under \s-1QNX 6.2.0:\s0

.Vb 2
  op/sprintf.........................FAILED at test 91
  lib/Benchmark......................FAILED at test 26
.Ve

This is due to a bug in the C library's printf routine.
printf(\*(L"'%e'\*(R", 0. ) produces '0.000000e+0', but \s-1ANSI\s0 requires
'0.000000e+00'. \s-1QNX\s0 has acknowledged the bug.

### Cross-compilation

Subsection "Cross-compilation"
Perl supports cross-compiling to \s-1QNX NTO\s0 through the
Native Development Kit (\s-1NDK\s0) for the Blackberry 10.  This means that you
can cross-compile for both \s-1ARM\s0 and x86 versions of the platform.

*Setting up a cross-compilation environment*
Subsection "Setting up a cross-compilation environment"

You can download the \s-1NDK\s0 from
<http://developer.blackberry.com/native/downloads/>.

See
<http://developer.blackberry.com/native/documentation/cascades/getting_started/setting_up.html>
for instructions to set up your device prior to attempting anything else.

Once you've installed the \s-1NDK\s0 and set up your device, all that's
left to do is setting up the device and the cross-compilation
environment.  Blackberry provides a script, \f(CW\*(C`bbndk-env.sh\*(C' (occasionally
named something like \f(CW\*(C`bbndk-env_10_1_0_4828.sh\*(C') which can be used
to do this.  However, there's a bit of a snag that we have to work through:
The script modifies \s-1PATH\s0 so that 'gcc' or 'ar' point to their
cross-compilation equivalents, which screws over the build process.

So instead you'll want to do something like this:

.Vb 3
    $ orig_path=$PATH
    $ source $location_of_bbndk/bbndk-env*.sh
    $ export PATH="$orig_path:$PATH"
.Ve

Besides putting the cross-compiler and the rest of the toolchain in your
\s-1PATH,\s0 this will also provide the \s-1QNX_TARGET\s0 variable, which
we will pass to Configure through -Dsysroot.

*Preparing the target system*
Subsection "Preparing the target system"

It's quite possible that the target system doesn't have a readily
available /tmp, so it's generally safer to do something like this:

.Vb 3
 $ ssh $TARGETUSER@$TARGETHOST \*(Aqrm -rf perl; mkdir perl; mkdir perl/tmp\*(Aq
 $ export TARGETDIR=\`ssh $TARGETUSER@$TARGETHOST pwd\`/perl
 $ export TARGETENV="export TMPDIR=$TARGETDIR/tmp; "
.Ve

Later on, we'll pass this to Configure through -Dtargetenv

*Calling Configure*
Subsection "Calling Configure"

If you are targetting an \s-1ARM\s0 device \*(-- which currently includes the vast
majority of phones and tablets \*(-- you'll want to pass
-Dcc=arm-unknown-nto-qnx8.0.0eabi-gcc to Configure.  Alternatively, if you
are targetting an x86 device, or using the simulator provided with the \s-1NDK,\s0
you should specify -Dcc=ntox86-gcc instead.

A sample Configure invocation looks something like this:

.Vb 6
    ./Configure -des -Dusecrosscompile \\
        -Dsysroot=$QNX_TARGET          \\
        -Dtargetdir=$TARGETDIR         \\
        -Dtargetenv="$TARGETENV"       \\
        -Dcc=ntox86-gcc                \\
        -Dtarghost=... # Usual cross-compilation options
.Ve

## AUTHOR

Header "AUTHOR"
Norton T. Allen (allen@huarp.harvard.edu)
