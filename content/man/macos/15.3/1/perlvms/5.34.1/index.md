+++
operating_system = "macos"
detected_package_version = "5.34.1"
manpage_section = "1"
description = "Gathered below are notes describing details of Perl 5s  behavior on s-1VMS.s0  They are a supplement to the regular Perl 5  documentation, so we have focussed on the ways in which Perl  5 functions differently under s-1VMSs0 than it does under Uni..."
manpage_name = "perlvms"
operating_system_version = "15.3"
manpage_format = "troff"
date = "2022-02-19"
author = "None Specified"
title = "perlvms(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLVMS 1"
PERLVMS 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlvms - VMS-specific documentation for Perl

## DESCRIPTION

Header "DESCRIPTION"
Gathered below are notes describing details of Perl 5's
behavior on \s-1VMS.\s0  They are a supplement to the regular Perl 5
documentation, so we have focussed on the ways in which Perl
5 functions differently under \s-1VMS\s0 than it does under Unix,
and on the interactions between Perl and the rest of the
operating system.  We haven't tried to duplicate complete
descriptions of Perl features from the main Perl
documentation, which can be found in the *[.pod]*
subdirectory of the Perl distribution.

We hope these notes will save you from confusion and lost
sleep when writing Perl scripts on \s-1VMS.\s0  If you find we've
missed something you think should appear here, please don't
hesitate to drop a line to vmsperl@perl.org.

## Installation

Header "Installation"
Directions for building and installing Perl 5 can be found in
the file *\s-1README\s0.vms* in the main source directory of the
Perl distribution.

## Organization of Perl Images

Header "Organization of Perl Images"

### Core Images

Subsection "Core Images"
During the build process, three Perl images are produced.
*Miniperl.Exe* is an executable image which contains all of
the basic functionality of Perl, but cannot take advantage of
Perl \s-1XS\s0 extensions and has a hard-wired list of library locations
for loading pure-Perl modules.  It is used extensively to build and
test Perl and various extensions, but is not installed.

Most of the complete Perl resides in the shareable image *PerlShr.Exe*,
which provides a core to which the Perl executable image and all Perl
extensions are linked. It is generally located via the logical name
*\s-1PERLSHR\s0*.  While it's possible to put the image in *\s-1SYS$SHARE\s0* to
make it loadable, that's not recommended. And while you may wish to
\s-1INSTALL\s0 the image for performance reasons, you should not install it
with privileges; if you do, the result will not be what you expect as
image privileges are disabled during Perl start-up.

Finally, *Perl.Exe* is an executable image containing the main
entry point for Perl, as well as some initialization code.  It
should be placed in a public directory, and made world executable.
In order to run Perl with command line arguments, you should
define a foreign command to invoke this image.

### Perl Extensions

Subsection "Perl Extensions"
Perl extensions are packages which provide both \s-1XS\s0 and Perl code
to add new functionality to perl.  (\s-1XS\s0 is a meta-language which
simplifies writing C code which interacts with Perl, see
perlxs for more details.)  The Perl code for an
extension is treated like any other library module - it's
made available in your script through the appropriate
\f(CW\*(C`use\*(C' or \f(CW\*(C`require\*(C' statement, and usually defines a Perl
package containing the extension.

The portion of the extension provided by the \s-1XS\s0 code may be
connected to the rest of Perl in either of two ways.  In the
**static** configuration, the object code for the extension is
linked directly into *PerlShr.Exe*, and is initialized whenever
Perl is invoked.  In the **dynamic** configuration, the extension's
machine code is placed into a separate shareable image, which is
mapped by Perl's DynaLoader when the extension is \f(CW\*(C`use\*(C'd or
\f(CW\*(C`require\*(C'd in your script.  This allows you to maintain the
extension as a separate entity, at the cost of keeping track of the
additional shareable image.  Most extensions can be set up as either
static or dynamic.

The source code for an extension usually resides in its own
directory.  At least three files are generally provided:
*Extshortname**.xs* (where *Extshortname* is the portion of
the extension's name following the last \f(CW\*(C`::\*(C'), containing
the \s-1XS\s0 code, *Extshortname**.pm*, the Perl library module
for the extension, and *Makefile.PL*, a Perl script which uses
the \f(CW\*(C`MakeMaker\*(C' library modules supplied with Perl to generate
a *Descrip.MMS* file for the extension.

### Installing static extensions

Subsection "Installing static extensions"
Since static extensions are incorporated directly into
*PerlShr.Exe*, you'll have to rebuild Perl to incorporate a
new extension.  You should edit the main *Descrip.MMS* or *Makefile*
you use to build Perl, adding the extension's name to the \f(CW\*(C`ext\*(C'
macro, and the extension's object file to the \f(CW\*(C`extobj\*(C' macro.
You'll also need to build the extension's object file, either
by adding dependencies to the main *Descrip.MMS*, or using a
separate *Descrip.MMS* for the extension.  Then, rebuild
*PerlShr.Exe* to incorporate the new code.

Finally, you'll need to copy the extension's Perl library
module to the *[.**Extname**]* subdirectory under one
of the directories in \f(CW@INC, where *Extname* is the name
of the extension, with all \f(CW\*(C`::\*(C' replaced by \f(CW\*(C`.\*(C' (e.g.
the library module for extension Foo::Bar would be copied
to a *[.Foo.Bar]* subdirectory).

### Installing dynamic extensions

Subsection "Installing dynamic extensions"
In general, the distributed kit for a Perl extension includes
a file named Makefile.PL, which is a Perl program which is used
to create a *Descrip.MMS* file which can be used to build and
install the files required by the extension.  The kit should be
unpacked into a directory tree **not** under the main Perl source
directory, and the procedure for building the extension is simply

.Vb 4
    $ perl Makefile.PL  ! Create Descrip.MMS
    $ mmk               ! Build necessary files
    $ mmk test          ! Run test code, if supplied
    $ mmk install       ! Install into public Perl tree
.Ve

\s-1VMS\s0 support for this process in the current release of Perl
is sufficient to handle most extensions.  (See the MakeMaker
documentation for more details on installation options for
extensions.)

- \(bu
the *[.Lib.Auto.**Arch**\f(CI$PVers***Extname*\fI]* subdirectory
of one of the directories in \f(CW@INC (where *PVers*
is the version of Perl you're using, as supplied in \f(CW$],
with '.' converted to '_'), or

- \(bu
one of the directories in \f(CW@INC, or

- \(bu
a directory which the extensions Perl library module
passes to the DynaLoader when asking it to map
the shareable image, or

- \(bu
*Sys$Share* or *Sys$Library*.

If the shareable image isn't in any of these places, you'll need
to define a logical name *Extshortname*, where *Extshortname*
is the portion of the extension's name after the last \f(CW\*(C`::\*(C', which
translates to the full file specification of the shareable image.

## File specifications

Header "File specifications"

### Syntax

Subsection "Syntax"
We have tried to make Perl aware of both VMS-style and Unix-style file
specifications wherever possible.  You may use either style, or both,
on the command line and in scripts, but you may not combine the two
styles within a single file specification.  \s-1VMS\s0 Perl interprets Unix
pathnames in much the same way as the \s-1CRTL\s0 (*e.g.* the first component
of an absolute path is read as the device name for the \s-1VMS\s0 file
specification).  There are a set of functions provided in the
\f(CW\*(C`VMS::Filespec\*(C' package for explicit interconversion between \s-1VMS\s0 and
Unix syntax; its documentation provides more details.

We've tried to minimize the dependence of Perl library
modules on Unix syntax, but you may find that some of these,
as well as some scripts written for Unix systems, will
require that you use Unix syntax, since they will assume that
'/' is the directory separator, *etc.*  If you find instances
of this in the Perl distribution itself, please let us know,
so we can try to work around them.

Also when working on Perl programs on \s-1VMS,\s0 if you need a syntax
in a specific operating system format, then you need either to
check the appropriate \s-1DECC$\s0 feature logical, or call a conversion
routine to force it to that format.

The feature logical name \s-1DECC$FILENAME_UNIX_REPORT\s0 modifies traditional
Perl behavior in the conversion of file specifications from Unix to \s-1VMS\s0
format in order to follow the extended character handling rules now
expected by the \s-1CRTL.\s0  Specifically, when this feature is in effect, the
\f(CW\*(C`./.../\*(C' in a Unix path is now translated to \f(CW\*(C`[.^.^.^.]\*(C' instead of
the traditional \s-1VMS\s0 \f(CW\*(C`[...]\*(C'.  To be compatible with what MakeMaker
expects, if a \s-1VMS\s0 path cannot be translated to a Unix path, it is
passed through unchanged, so \f(CW\*(C`unixify("[...]")\*(C' will return \f(CW\*(C`[...]\*(C'.

There are several ambiguous cases where a conversion routine cannot
determine whether an input filename is in Unix format or in \s-1VMS\s0 format,
since now both \s-1VMS\s0 and Unix file specifications may have characters in
them that could be mistaken for syntax delimiters of the other type. So
some pathnames simply cannot be used in a mode that allows either type
of pathname to be present.  Perl will tend to assume that an ambiguous
filename is in Unix format.

Allowing \*(L".\*(R" as a version delimiter is simply incompatible with
determining whether a pathname is in \s-1VMS\s0 format or in Unix format with
extended file syntax.  There is no way to know whether \*(L"perl-5.8.6\*(R" is a
Unix \*(L"perl-5.8.6\*(R" or a \s-1VMS\s0 \*(L"perl-5.8;6\*(R" when passing it to **unixify()** or
**vmsify()**.

The \s-1DECC$FILENAME_UNIX_REPORT\s0 logical name controls how Perl interprets
filenames to the extent that Perl uses the \s-1CRTL\s0 internally for many
purposes, and attempts to follow \s-1CRTL\s0 conventions for reporting
filenames.  The \s-1DECC$FILENAME_UNIX_ONLY\s0 feature differs in that it
expects all filenames passed to the C run-time to be already in Unix
format.  This feature is not yet supported in Perl since Perl uses
traditional OpenVMS file specifications internally and in the test
harness, and it is not yet clear whether this mode will be useful or
useable.  The feature logical name \s-1DECC$POSIX_COMPLIANT_PATHNAMES\s0 is new
with the \s-1RMS\s0 Symbolic Link \s-1SDK\s0 and included with OpenVMS v8.3, but is
not yet supported in Perl.

### Filename Case

Subsection "Filename Case"
Perl enables \s-1DECC$EFS_CASE_PRESERVE\s0 and \s-1DECC$ARGV_PARSE_STYLE\s0 by
default.  Note that the latter only takes effect when extended parse
is set in the process in which Perl is running.  When these features
are explicitly disabled in the environment or the \s-1CRTL\s0 does not support
them, Perl follows the traditional \s-1CRTL\s0 behavior of downcasing command-line
arguments and returning file specifications in lower case only.

*N. B.*  It is very easy to get tripped up using a mixture of other
programs, external utilities, and Perl scripts that are in varying
states of being able to handle case preservation.  For example, a file
created by an older version of an archive utility or a build utility
such as \s-1MMK\s0 or \s-1MMS\s0 may generate a filename in all upper case even on an
\s-1ODS-5\s0 volume.  If this filename is later retrieved by a Perl script or
module in a case preserving environment, that upper case name may not
match the mixed-case or lower-case expectations of the Perl code.  Your
best bet is to follow an all-or-nothing approach to case preservation:
either don't use it at all, or make sure your entire toolchain and
application environment support and use it.

OpenVMS Alpha v7.3-1 and later and all version of OpenVMS I64 support
case sensitivity as a process setting (see \f(CW\*(C`SET PROCESS
/CASE_LOOKUP=SENSITIVE\*(C'). Perl does not currently support case
sensitivity on \s-1VMS,\s0 but it may in the future, so Perl programs should
use the \f(CW\*(C`File::Spec->case_tolerant\*(C' method to determine the state, and
not the \f(CW$^O variable.

### Symbolic Links

Subsection "Symbolic Links"
When built on an \s-1ODS-5\s0 volume with symbolic links enabled, Perl by
default supports symbolic links when the requisite support is available
in the filesystem and \s-1CRTL\s0 (generally 64-bit OpenVMS v8.3 and later).
There are a number of limitations and caveats to be aware of when
working with symbolic links on \s-1VMS.\s0  Most notably, the target of a valid
symbolic link must be expressed as a Unix-style path and it must exist
on a volume visible from your \s-1POSIX\s0 root (see the \f(CW\*(C`SHOW ROOT\*(C' command
in \s-1DCL\s0 help).  For further details on symbolic link capabilities and
requirements, see chapter 12 of the \s-1CRTL\s0 manual that ships with OpenVMS
v8.3 or later.

### Wildcard expansion

Subsection "Wildcard expansion"
File specifications containing wildcards are allowed both on
the command line and within Perl globs (e.g. \f(CW\*(C`<*.c>\*(C').  If
the wildcard filespec uses \s-1VMS\s0 syntax, the resultant
filespecs will follow \s-1VMS\s0 syntax; if a Unix-style filespec is
passed in, Unix-style filespecs will be returned.
Similar to the behavior of wildcard globbing for a Unix shell,
one can escape command line wildcards with double quotation
marks \f(CW\*(C`"\*(C' around a perl program command line argument.  However,
owing to the stripping of \f(CW\*(C`"\*(C' characters carried out by the C
handling of argv you will need to escape a construct such as
this one (in a directory containing the files *\s-1PERL.C\s0*, *\s-1PERL.EXE\s0*,
*\s-1PERL.H\s0*, and *\s-1PERL.OBJ\s0*):

.Vb 2
    $ perl -e "print join(\*(Aq \*(Aq,@ARGV)" perl.*
    perl.c perl.exe perl.h perl.obj
.Ve

in the following triple quoted manner:

.Vb 2
    $ perl -e "print join(\*(Aq \*(Aq,@ARGV)" """perl.*"""
    perl.*
.Ve

In both the case of unquoted command line arguments or in calls
to \f(CW\*(C`glob()\*(C' \s-1VMS\s0 wildcard expansion is performed. (csh-style
wildcard expansion is available if you use \f(CW\*(C`File::Glob::glob\*(C'.)
If the wildcard filespec contains a device or directory
specification, then the resultant filespecs will also contain
a device and directory; otherwise, device and directory
information are removed.  VMS-style resultant filespecs will
contain a full device and directory, while Unix-style
resultant filespecs will contain only as much of a directory
path as was present in the input filespec.  For example, if
your default directory is Perl_Root:[000000], the expansion
of \f(CW\*(C`[.t]*.*\*(C' will yield filespecs  like
\*(L"perl_root:[t]base.dir\*(R", while the expansion of \f(CW\*(C`t/*/*\*(C' will
yield filespecs like \*(L"t/base.dir\*(R".  (This is done to match
the behavior of glob expansion performed by Unix shells.)

Similarly, the resultant filespec will contain the file version
only if one was present in the input filespec.

### Pipes

Subsection "Pipes"
Input and output pipes to Perl filehandles are supported; the
\*(L"file name\*(R" is passed to lib$**spawn()** for asynchronous
execution.  You should be careful to close any pipes you have
opened in a Perl script, lest you leave any \*(L"orphaned\*(R"
subprocesses around when Perl exits.

You may also use backticks to invoke a \s-1DCL\s0 subprocess, whose
output is used as the return value of the expression.  The
string between the backticks is handled as if it were the
argument to the \f(CW\*(C`system\*(C' operator (see below).  In this case,
Perl will wait for the subprocess to complete before continuing.

The mailbox (\s-1MBX\s0) that perl can create to communicate with a pipe
defaults to a buffer size of 8192 on 64-bit systems, 512 on \s-1VAX.\s0  The
default buffer size is adjustable via the logical name \s-1PERL_MBX_SIZE\s0
provided that the value falls between 128 and the \s-1SYSGEN\s0 parameter
\s-1MAXBUF\s0 inclusive.  For example, to set the mailbox size to 32767 use
\f(CW\*(C`$ENV\{\*(AqPERL_MBX_SIZE\*(Aq\} = 32767;\*(C' and then open and use pipe constructs.
An alternative would be to issue the command:

.Vb 1
    $ Define PERL_MBX_SIZE 32767
.Ve

before running your wide record pipe program.  A larger value may
improve performance at the expense of the \s-1BYTLM UAF\s0 quota.

## PERL5LIB and PERLLIB

Header "PERL5LIB and PERLLIB"
The \s-1PERL5LIB\s0 and \s-1PERLLIB\s0 environment elements work as documented in perl,
except that the element separator is, by default, '|' instead of ':'.
However, when running under a Unix shell as determined by the logical
name \f(CW\*(C`GNV$UNIX_SHELL\*(C', the separator will be ':' as on Unix systems. The
directory specifications may use either \s-1VMS\s0 or Unix syntax.

## The Perl Forked Debugger

Header "The Perl Forked Debugger"
The Perl forked debugger places the debugger commands and output in a
separate X-11 terminal window so that commands and output from multiple
processes are not mixed together.

Perl on \s-1VMS\s0 supports an emulation of the forked debugger when Perl is
run on a \s-1VMS\s0 system that has X11 support installed.

To use the forked debugger, you need to have the default display set to an
X-11 Server and some environment variables set that Unix expects.

The forked debugger requires the environment variable \f(CW\*(C`TERM\*(C' to be \f(CW\*(C`xterm\*(C',
and the environment variable \f(CW\*(C`DISPLAY\*(C' to exist.  \f(CW\*(C`xterm\*(C' must be in
lower case.

.Vb 1
  $define TERM "xterm"

  $define DISPLAY "hostname:0.0"
.Ve

Currently the value of \f(CW\*(C`DISPLAY\*(C' is ignored.  It is recommended that it be set
to be the hostname of the display, the server and screen in Unix notation.  In
the future the value of \s-1DISPLAY\s0 may be honored by Perl instead of using the
default display.

It may be helpful to always use the forked debugger so that script I/O is
separated from debugger I/O.  You can force the debugger to be forked by
assigning a value to the logical name <\s-1PERLDB_PIDS\s0> that is not a process
identification number.

.Vb 1
  $define PERLDB_PIDS XXXX
.Ve

## PERL_VMS_EXCEPTION_DEBUG

Header "PERL_VMS_EXCEPTION_DEBUG"
The \s-1PERL_VMS_EXCEPTION_DEBUG\s0 being defined as \*(L"\s-1ENABLE\*(R"\s0 will cause the \s-1VMS\s0
debugger to be invoked if a fatal exception that is not otherwise
handled is raised.  The purpose of this is to allow debugging of
internal Perl problems that would cause such a condition.

This allows the programmer to look at the execution stack and variables to
find out the cause of the exception.  As the debugger is being invoked as
the Perl interpreter is about to do a fatal exit, continuing the execution
in debug mode is usually not practical.

Starting Perl in the \s-1VMS\s0 debugger may change the program execution
profile in a way that such problems are not reproduced.

The \f(CW\*(C`kill\*(C' function can be used to test this functionality from within
a program.

In typical \s-1VMS\s0 style, only the first letter of the value of this logical
name is actually checked in a case insensitive mode, and it is considered
enabled if it is the value \*(L"T\*(R",\*(L"1\*(R" or \*(L"E\*(R".

This logical name must be defined before Perl is started.

## Command line

Header "Command line"

### I/O redirection and backgrounding

Subsection "I/O redirection and backgrounding"
Perl for \s-1VMS\s0 supports redirection of input and output on the
command line, using a subset of Bourne shell syntax:

- \(bu
\f(CW\*(C`<file\*(C' reads stdin from \f(CW\*(C`file\*(C',

- \(bu
\f(CW\*(C`>file\*(C' writes stdout to \f(CW\*(C`file\*(C',

- \(bu
\f(CW\*(C`>>file\*(C' appends stdout to \f(CW\*(C`file\*(C',

- \(bu
\f(CW\*(C`2>file\*(C' writes stderr to \f(CW\*(C`file\*(C',

- \(bu
\f(CW\*(C`2>>file\*(C' appends stderr to \f(CW\*(C`file\*(C', and

- \(bu
\f(CW\*(C`2>&1\*(C' redirects stderr to stdout.

In addition, output may be piped to a subprocess, using the
character '|'.  Anything after this character on the command
line is passed to a subprocess for execution; the subprocess
takes the output of Perl as its input.

Finally, if the command line ends with '&', the entire
command is run in the background as an asynchronous
subprocess.

### Command line switches

Subsection "Command line switches"
The following command line switches behave differently under
\s-1VMS\s0 than described in perlrun.  Note also that in order
to pass uppercase switches to Perl, you need to enclose
them in double-quotes on the command line, since the \s-1CRTL\s0
downcases all unquoted strings.

On newer 64 bit versions of OpenVMS, a process setting now
controls if the quoting is needed to preserve the case of
command line arguments.

- -i
Item "-i"
If the \f(CW\*(C`-i\*(C' switch is present but no extension for a backup
copy is given, then inplace editing creates a new version of
a file; the existing copy is not deleted.  (Note that if
an extension is given, an existing file is renamed to the backup
file, as is the case under other operating systems, so it does
not remain as a previous version under the original filename.)

- -S
Item "-S"
If the \f(CW"-S" or \f(CW\*(C`-"S"\*(C' switch is present *and* the script
name does not contain a directory, then Perl translates the
logical name \s-1DCL$PATH\s0 as a searchlist, using each translation
as a directory in which to look for the script.  In addition,
if no file type is specified, Perl looks in each directory
for a file matching the name specified, with a blank type,
a type of *.pl*, and a type of *.com*, in that order.

- -u
Item "-u"
The \f(CW\*(C`-u\*(C' switch causes the \s-1VMS\s0 debugger to be invoked
after the Perl program is compiled, but before it has
run.  It does not create a core dump file.

## Perl functions

Header "Perl functions"
As of the time this document was last revised, the following
Perl functions were implemented in the \s-1VMS\s0 port of Perl
(functions marked with * are discussed in more detail below):

.Vb 10
    file tests*, abs, alarm, atan, backticks*, binmode*, bless,
    caller, chdir, chmod, chown, chomp, chop, chr,
    close, closedir, cos, crypt*, defined, delete, die, do, dump*,
    each, endgrent, endpwent, eof, eval, exec*, exists, exit, exp,
    fileno, flock  getc, getgrent*, getgrgid*, getgrnam, getlogin,
    getppid, getpwent*, getpwnam*, getpwuid*, glob, gmtime*, goto,
    grep, hex, ioctl, import, index, int, join, keys, kill*,
    last, lc, lcfirst, lchown*, length, link*, local, localtime, log,
    lstat, m//, map, mkdir, my, next, no, oct, open, opendir, ord,
    pack, pipe, pop, pos, print, printf, push, q//, qq//, qw//,
    qx//*, quotemeta, rand, read, readdir, readlink*, redo, ref,
    rename, require, reset, return, reverse, rewinddir, rindex,
    rmdir, s///, scalar, seek, seekdir, select(internal),
    select (system call)*, setgrent, setpwent, shift, sin, sleep,
    socketpair, sort, splice, split, sprintf, sqrt, srand, stat,
    study, substr, symlink*, sysread, system*, syswrite, tell,
    telldir, tie, time, times*, tr///, uc, ucfirst, umask,
    undef, unlink*, unpack, untie, unshift, use, utime*,
    values, vec, wait, waitpid*, wantarray, warn, write, y///
.Ve

The following functions were not implemented in the \s-1VMS\s0 port,
and calling them produces a fatal error (usually) or
undefined behavior (rarely, we hope):

.Vb 4
    chroot, dbmclose, dbmopen, fork*, getpgrp, getpriority,
    msgctl, msgget, msgsend, msgrcv, semctl,
    semget, semop, setpgrp, setpriority, shmctl, shmget,
    shmread, shmwrite, syscall
.Ve

The following functions are available on Perls compiled with Dec C
5.2 or greater and running \s-1VMS 7.0\s0 or greater:

.Vb 1
    truncate
.Ve

The following functions are available on Perls built on \s-1VMS 7.2\s0 or
greater:

.Vb 1
    fcntl (without locking)
.Ve

The following functions may or may not be implemented,
depending on what type of socket support you've built into
your copy of Perl:

.Vb 9
    accept, bind, connect, getpeername,
    gethostbyname, getnetbyname, getprotobyname,
    getservbyname, gethostbyaddr, getnetbyaddr,
    getprotobynumber, getservbyport, gethostent,
    getnetent, getprotoent, getservent, sethostent,
    setnetent, setprotoent, setservent, endhostent,
    endnetent, endprotoent, endservent, getsockname,
    getsockopt, listen, recv, select(system call)*,
    send, setsockopt, shutdown, socket
.Ve

The following function is available on Perls built on 64 bit OpenVMS v8.2
with hard links enabled on an \s-1ODS-5\s0 formatted build disk.  \s-1CRTL\s0 support
is in principle available as of OpenVMS v7.3-1, and better configuration
support could detect this.

.Vb 1
    link
.Ve

The following functions are available on Perls built on 64 bit OpenVMS
v8.2 and later.  \s-1CRTL\s0 support is in principle available as of OpenVMS
v7.3-2, and better configuration support could detect this.

.Vb 2
   getgrgid, getgrnam, getpwnam, getpwuid,
   setgrent, ttyname
.Ve

The following functions are available on Perls built on 64 bit OpenVMS v8.2
and later.

.Vb 1
   statvfs, socketpair
.Ve

- File tests
Item "File tests"
The tests \f(CW\*(C`-b\*(C', \f(CW\*(C`-B\*(C', \f(CW\*(C`-c\*(C', \f(CW\*(C`-C\*(C', \f(CW\*(C`-d\*(C', \f(CW\*(C`-e\*(C', \f(CW\*(C`-f\*(C',
\f(CW\*(C`-o\*(C', \f(CW\*(C`-M\*(C', \f(CW\*(C`-s\*(C', \f(CW\*(C`-S\*(C', \f(CW\*(C`-t\*(C', \f(CW\*(C`-T\*(C', and \f(CW\*(C`-z\*(C' work as
advertised.  The return values for \f(CW\*(C`-r\*(C', \f(CW\*(C`-w\*(C', and \f(CW\*(C`-x\*(C'
tell you whether you can actually access the file; this may
not reflect the UIC-based file protections.  Since real and
effective \s-1UIC\s0 don't differ under \s-1VMS,\s0 \f(CW\*(C`-O\*(C', \f(CW\*(C`-R\*(C', \f(CW\*(C`-W\*(C',
and \f(CW\*(C`-X\*(C' are equivalent to \f(CW\*(C`-o\*(C', \f(CW\*(C`-r\*(C', \f(CW\*(C`-w\*(C', and \f(CW\*(C`-x\*(C'.
Similarly, several other tests, including \f(CW\*(C`-A\*(C', \f(CW\*(C`-g\*(C', \f(CW\*(C`-k\*(C',
\f(CW\*(C`-l\*(C', \f(CW\*(C`-p\*(C', and \f(CW\*(C`-u\*(C', aren't particularly meaningful under
\s-1VMS,\s0 and the values returned by these tests reflect whatever
your \s-1CRTL\s0 \f(CW\*(C`stat()\*(C' routine does to the equivalent bits in the
st_mode field.  Finally, \f(CW\*(C`-d\*(C' returns true if passed a device
specification without an explicit directory (e.g. \f(CW\*(C`DUA1:\*(C'), as
well as if passed a directory.
.Sp
There are \s-1DECC\s0 feature logical names \s-1AND ODS-5\s0 volume attributes that
also control what values are returned for the date fields.
.Sp
Note: Some sites have reported problems when using the file-access
tests (\f(CW\*(C`-r\*(C', \f(CW\*(C`-w\*(C', and \f(CW\*(C`-x\*(C') on files accessed via \s-1DEC\s0's \s-1DFS.\s0
Specifically, since \s-1DFS\s0 does not currently provide access to the
extended file header of files on remote volumes, attempts to
examine the \s-1ACL\s0 fail, and the file tests will return false,
with \f(CW$! indicating that the file does not exist.  You can
use \f(CW\*(C`stat\*(C' on these files, since that checks UIC-based protection
only, and then manually check the appropriate bits, as defined by
your C compiler's *stat.h*, in the mode value it returns, if you
need an approximation of the file's protections.

- backticks
Item "backticks"
Backticks create a subprocess, and pass the enclosed string
to it for execution as a \s-1DCL\s0 command.  Since the subprocess is
created directly via \f(CW\*(C`lib$spawn()\*(C', any valid \s-1DCL\s0 command string
may be specified.

- binmode \s-1FILEHANDLE\s0
Item "binmode FILEHANDLE"
The \f(CW\*(C`binmode\*(C' operator will attempt to insure that no translation
of carriage control occurs on input from or output to this filehandle.
Since this involves reopening the file and then restoring its
file position indicator, if this function returns \s-1FALSE,\s0 the
underlying filehandle may no longer point to an open file, or may
point to a different position in the file than before \f(CW\*(C`binmode\*(C'
was called.
.Sp
Note that \f(CW\*(C`binmode\*(C' is generally not necessary when using normal
filehandles; it is provided so that you can control I/O to existing
record-structured files when necessary.  You can also use the
\f(CW\*(C`vmsfopen\*(C' function in the VMS::Stdio extension to gain finer
control of I/O to files and devices with different record structures.

- crypt \s-1PLAINTEXT, USER\s0
Item "crypt PLAINTEXT, USER"
The \f(CW\*(C`crypt\*(C' operator uses the \f(CW\*(C`sys$hash_password\*(C' system
service to generate the hashed representation of \s-1PLAINTEXT.\s0
If \s-1USER\s0 is a valid username, the algorithm and salt values
are taken from that user's \s-1UAF\s0 record.  If it is not, then
the preferred algorithm and a salt of 0 are used.  The
quadword encrypted value is returned as an 8-character string.
.Sp
The value returned by \f(CW\*(C`crypt\*(C' may be compared against
the encrypted password from the \s-1UAF\s0 returned by the \f(CW\*(C`getpw*\*(C'
functions, in order to authenticate users.  If you're
going to do this, remember that the encrypted password in
the \s-1UAF\s0 was generated using uppercase username and
password strings; you'll have to upcase the arguments to
\f(CW\*(C`crypt\*(C' to insure that you'll get the proper value:
.Sp
.Vb 9
    sub validate_passwd \{
        my($user,$passwd) = @_;
        my($pwdhash);
        if ( !($pwdhash = (getpwnam($user))[1]) ||
               $pwdhash ne crypt("\\U$passwd","\\U$name") ) \{
            intruder_alert($name);
        \}
        return 1;
    \}
.Ve

- die
Item "die"
\f(CW\*(C`die\*(C' will force the native \s-1VMS\s0 exit status to be an \s-1SS$_ABORT\s0 code
if neither of the $! or $? status values are ones that would cause
the native status to be interpreted as being what \s-1VMS\s0 classifies as
\s-1SEVERE_ERROR\s0 severity for \s-1DCL\s0 error handling.
.Sp
When \f(CW\*(C`PERL_VMS_POSIX_EXIT\*(C' is active (see \*(L"$?\*(R" below), the native \s-1VMS\s0 exit
status value will have either one of the \f(CW$! or \f(CW$? or \f(CW$^E or
the Unix value 255 encoded into it in a way that the effective original
value can be decoded by other programs written in C, including Perl
and the \s-1GNV\s0 package.  As per the normal non-VMS behavior of \f(CW\*(C`die\*(C' if
either \f(CW$! or \f(CW$? are non-zero, one of those values will be
encoded into a native \s-1VMS\s0 status value.  If both of the Unix status
values are 0, and the \f(CW$^E value is set one of \s-1ERROR\s0 or \s-1SEVERE_ERROR\s0
severity, then the \f(CW$^E value will be used as the exit code as is.
If none of the above apply, the Unix value of 255 will be encoded into
a native \s-1VMS\s0 exit status value.
.Sp
Please note a significant difference in the behavior of \f(CW\*(C`die\*(C' in
the \f(CW\*(C`PERL_VMS_POSIX_EXIT\*(C' mode is that it does not force a \s-1VMS
SEVERE_ERROR\s0 status on exit.  The Unix exit values of 2 through
255 will be encoded in \s-1VMS\s0 status values with severity levels of
\s-1SUCCESS.\s0  The Unix exit value of 1 will be encoded in a \s-1VMS\s0 status
value with a severity level of \s-1ERROR.\s0  This is to be compatible with
how the \s-1VMS C\s0 library encodes these values.
.Sp
The minimum severity level set by \f(CW\*(C`die\*(C' in \f(CW\*(C`PERL_VMS_POSIX_EXIT\*(C' mode
may be changed to be \s-1ERROR\s0 or higher in the future depending on the
results of testing and further review.
.Sp
See \*(L"$?\*(R" for a description of the encoding of the Unix value to
produce a native \s-1VMS\s0 status containing it.

- dump
Item "dump"
Rather than causing Perl to abort and dump core, the \f(CW\*(C`dump\*(C'
operator invokes the \s-1VMS\s0 debugger.  If you continue to
execute the Perl program under the debugger, control will
be transferred to the label specified as the argument to
\f(CW\*(C`dump\*(C', or, if no label was specified, back to the
beginning of the program.  All other state of the program
(*e.g.* values of variables, open file handles) are not
affected by calling \f(CW\*(C`dump\*(C'.

- exec \s-1LIST\s0
Item "exec LIST"
A call to \f(CW\*(C`exec\*(C' will cause Perl to exit, and to invoke the command
given as an argument to \f(CW\*(C`exec\*(C' via \f(CW\*(C`lib$do_command\*(C'.  If the
argument begins with '@' or '$' (other than as part of a filespec),
then it is executed as a \s-1DCL\s0 command.  Otherwise, the first token on
the command line is treated as the filespec of an image to run, and
an attempt is made to invoke it (using *.Exe* and the process
defaults to expand the filespec) and pass the rest of \f(CW\*(C`exec\*(C''s
argument to it as parameters.  If the token has no file type, and
matches a file with null type, then an attempt is made to determine
whether the file is an executable image which should be invoked
using \f(CW\*(C`MCR\*(C' or a text file which should be passed to \s-1DCL\s0 as a
command procedure.

- fork
Item "fork"
While in principle the \f(CW\*(C`fork\*(C' operator could be implemented via
(and with the same rather severe limitations as) the \s-1CRTL\s0 \f(CW\*(C`vfork()\*(C'
routine, and while some internal support to do just that is in
place, the implementation has never been completed, making \f(CW\*(C`fork\*(C'
currently unavailable.  A true kernel \f(CW\*(C`fork()\*(C' is expected in a
future version of \s-1VMS,\s0 and the pseudo-fork based on interpreter
threads may be available in a future version of Perl on \s-1VMS\s0 (see
perlfork).  In the meantime, use \f(CW\*(C`system\*(C', backticks, or piped
filehandles to create subprocesses.

- getpwent
Item "getpwent"
0

- getpwnam
Item "getpwnam"

- getpwuid
Item "getpwuid"
.PD
These operators obtain the information described in perlfunc,
if you have the privileges necessary to retrieve the named user's
\s-1UAF\s0 information via \f(CW\*(C`sys$getuai\*(C'.  If not, then only the \f(CW$name,
\f(CW$uid, and \f(CW$gid items are returned.  The \f(CW$dir item contains
the login directory in \s-1VMS\s0 syntax, while the \f(CW$comment item
contains the login directory in Unix syntax. The \f(CW$gcos item
contains the owner field from the \s-1UAF\s0 record.  The \f(CW$quota
item is not used.

- gmtime
Item "gmtime"
The \f(CW\*(C`gmtime\*(C' operator will function properly if you have a
working \s-1CRTL\s0 \f(CW\*(C`gmtime()\*(C' routine, or if the logical name
\s-1SYS$TIMEZONE_DIFFERENTIAL\s0 is defined as the number of seconds
which must be added to \s-1UTC\s0 to yield local time.  (This logical
name is defined automatically if you are running a version of
\s-1VMS\s0 with built-in \s-1UTC\s0 support.)  If neither of these cases is
true, a warning message is printed, and \f(CW\*(C`undef\*(C' is returned.

- kill
Item "kill"
In most cases, \f(CW\*(C`kill\*(C' is implemented via the undocumented system
service \f(CW$SIGPRC, which has the same calling sequence as \f(CW$FORCEX, but
throws an exception in the target process rather than forcing it to call
\f(CW$EXIT.  Generally speaking, \f(CW\*(C`kill\*(C' follows the behavior of the
\s-1CRTL\s0's \f(CW\*(C`kill()\*(C' function, but unlike that function can be called from
within a signal handler.  Also, unlike the \f(CW\*(C`kill\*(C' in some versions of
the \s-1CRTL,\s0 Perl's \f(CW\*(C`kill\*(C' checks the validity of the signal passed in and
returns an error rather than attempting to send an unrecognized signal.
.Sp
Also, negative signal values don't do anything special under
\s-1VMS\s0; they're just converted to the corresponding positive value.

- qx//
Item "qx//"
See the entry on \f(CW\*(C`backticks\*(C' above.

- select (system call)
Item "select (system call)"
If Perl was not built with socket support, the system call
version of \f(CW\*(C`select\*(C' is not available at all.  If socket
support is present, then the system call version of
\f(CW\*(C`select\*(C' functions only for file descriptors attached
to sockets.  It will not provide information about regular
files or pipes, since the \s-1CRTL\s0 \f(CW\*(C`select()\*(C' routine does not
provide this functionality.

- stat \s-1EXPR\s0
Item "stat EXPR"
Since \s-1VMS\s0 keeps track of files according to a different scheme
than Unix, it's not really possible to represent the file's \s-1ID\s0
in the \f(CW\*(C`st_dev\*(C' and \f(CW\*(C`st_ino\*(C' fields of a \f(CW\*(C`struct stat\*(C'.  Perl
tries its best, though, and the values it uses are pretty unlikely
to be the same for two different files.  We can't guarantee this,
though, so caveat scriptor.

- system \s-1LIST\s0
Item "system LIST"
The \f(CW\*(C`system\*(C' operator creates a subprocess, and passes its
arguments to the subprocess for execution as a \s-1DCL\s0 command.
Since the subprocess is created directly via \f(CW\*(C`lib$spawn()\*(C', any
valid \s-1DCL\s0 command string may be specified.  If the string begins with
'@', it is treated as a \s-1DCL\s0 command unconditionally.  Otherwise, if
the first token contains a character used as a delimiter in file
specification (e.g. \f(CW\*(C`:\*(C' or \f(CW\*(C`]\*(C'), an attempt is made to expand it
using  a default type of *.Exe* and the process defaults, and if
successful, the resulting file is invoked via \f(CW\*(C`MCR\*(C'. This allows you
to invoke an image directly simply by passing the file specification
to \f(CW\*(C`system\*(C', a common Unixish idiom.  If the token has no file type,
and matches a file with null type, then an attempt is made to
determine whether the file is an executable image which should be
invoked using \f(CW\*(C`MCR\*(C' or a text file which should be passed to \s-1DCL\s0
as a command procedure.
.Sp
If \s-1LIST\s0 consists of the empty string, \f(CW\*(C`system\*(C' spawns an
interactive \s-1DCL\s0 subprocess, in the same fashion as typing
**\s-1SPAWN\s0** at the \s-1DCL\s0 prompt.
.Sp
Perl waits for the subprocess to complete before continuing
execution in the current process.  As described in perlfunc,
the return value of \f(CW\*(C`system\*(C' is a fake \*(L"status\*(R" which follows
\s-1POSIX\s0 semantics unless the pragma \f(CW\*(C`use vmsish \*(Aqstatus\*(Aq\*(C' is in
effect; see the description of \f(CW$? in this document for more
detail.

- time
Item "time"
The value returned by \f(CW\*(C`time\*(C' is the offset in seconds from
01-JAN-1970 00:00:00 (just like the \s-1CRTL\s0's **times()** routine), in order
to make life easier for code coming in from the POSIX/Unix world.

- times
Item "times"
The array returned by the \f(CW\*(C`times\*(C' operator is divided up
according to the same rules the \s-1CRTL\s0 \f(CW\*(C`times()\*(C' routine.
Therefore, the \*(L"system time\*(R" elements will always be 0, since
there is no difference between \*(L"user time\*(R" and \*(L"system\*(R" time
under \s-1VMS,\s0 and the time accumulated by a subprocess may or may
not appear separately in the \*(L"child time\*(R" field, depending on
whether \f(CW\*(C`times()\*(C' keeps track of subprocesses separately.  Note
especially that the \s-1VAXCRTL\s0 (at least) keeps track only of
subprocesses spawned using \f(CW\*(C`fork()\*(C' and \f(CW\*(C`exec()\*(C'; it will not
accumulate the times of subprocesses spawned via pipes, \f(CW\*(C`system()\*(C',
or backticks.

- unlink \s-1LIST\s0
Item "unlink LIST"
\f(CW\*(C`unlink\*(C' will delete the highest version of a file only; in
order to delete all versions, you need to say
.Sp
.Vb 1
    1 while unlink LIST;
.Ve
.Sp
You may need to make this change to scripts written for a
Unix system which expect that after a call to \f(CW\*(C`unlink\*(C',
no files with the names passed to \f(CW\*(C`unlink\*(C' will exist.
(Note: This can be changed at compile time; if you
\f(CW\*(C`use Config\*(C' and \f(CW$Config\{\*(Aqd_unlink_all_versions\*(Aq\} is
\f(CW\*(C`define\*(C', then \f(CW\*(C`unlink\*(C' will delete all versions of a
file on the first call.)
.Sp
\f(CW\*(C`unlink\*(C' will delete a file if at all possible, even if it
requires changing file protection (though it won't try to
change the protection of the parent directory).  You can tell
whether you've got explicit delete access to a file by using the
\f(CW\*(C`VMS::Filespec::candelete\*(C' operator.  For instance, in order
to delete only files to which you have delete access, you could
say something like
.Sp
.Vb 8
    sub safe_unlink \{
        my($file,$num);
        foreach $file (@_) \{
            next unless VMS::Filespec::candelete($file);
            $num += unlink $file;
        \}
        $num;
    \}
.Ve
.Sp
(or you could just use \f(CW\*(C`VMS::Stdio::remove\*(C', if you've installed
the VMS::Stdio extension distributed with Perl). If \f(CW\*(C`unlink\*(C' has to
change the file protection to delete the file, and you interrupt it
in midstream, the file may be left intact, but with a changed \s-1ACL\s0
allowing you delete access.
.Sp
This behavior of \f(CW\*(C`unlink\*(C' is to be compatible with \s-1POSIX\s0 behavior
and not traditional \s-1VMS\s0 behavior.

- utime \s-1LIST\s0
Item "utime LIST"
This operator changes only the modification time of the file (\s-1VMS\s0
revision date) on \s-1ODS-2\s0 volumes and \s-1ODS-5\s0 volumes without access
dates enabled. On \s-1ODS-5\s0 volumes with access dates enabled, the
true access time is modified.

- waitpid \s-1PID,FLAGS\s0
Item "waitpid PID,FLAGS"
If \s-1PID\s0 is a subprocess started by a piped \f(CW\*(C`open()\*(C' (see open),
\f(CW\*(C`waitpid\*(C' will wait for that subprocess, and return its final status
value in \f(CW$?.  If \s-1PID\s0 is a subprocess created in some other way (e.g.
SPAWNed before Perl was invoked), \f(CW\*(C`waitpid\*(C' will simply check once per
second whether the process has completed, and return when it has.  (If
\s-1PID\s0 specifies a process that isn't a subprocess of the current process,
and you invoked Perl with the \f(CW\*(C`-w\*(C' switch, a warning will be issued.)
.Sp
Returns \s-1PID\s0 on success, -1 on error.  The \s-1FLAGS\s0 argument is ignored
in all cases.

## Perl variables

Header "Perl variables"
The following VMS-specific information applies to the indicated
\*(L"special\*(R" Perl variables, in addition to the general information
in perlvar.  Where there is a conflict, this information
takes precedence.
.ie n .IP "%ENV" 4
.el .IP "\f(CW%ENV" 4
Item "%ENV"
The operation of the \f(CW%ENV array depends on the translation
of the logical name *\s-1PERL_ENV_TABLES\s0*.  If defined, it should
be a search list, each element of which specifies a location
for \f(CW%ENV elements.  If you tell Perl to read or set the
element \f(CW\*(C`$ENV\{\*(C'*name*\f(CW\*(C`\}\*(C', then Perl uses the translations of
*\s-1PERL_ENV_TABLES\s0* as follows:

> 
- \s-1CRTL_ENV\s0
Item "CRTL_ENV"
This string tells Perl to consult the \s-1CRTL\s0's internal \f(CW\*(C`environ\*(C' array
of key-value pairs, using *name* as the key.  In most cases, this
contains only a few keys, but if Perl was invoked via the C
\f(CW\*(C`exec[lv]e()\*(C' function, as is the case for some embedded Perl
applications or when running under a shell such as \s-1GNV\s0 bash, the
\f(CW\*(C`environ\*(C' array may have been populated by the calling program.

- CLISYM_[\s-1LOCAL\s0]
Item "CLISYM_[LOCAL]"
A string beginning with \f(CW\*(C`CLISYM_\*(C'tells Perl to consult the \s-1CLI\s0's
symbol tables, using *name* as the name of the symbol.  When reading
an element of \f(CW%ENV, the local symbol table is scanned first, followed
by the global symbol table..  The characters following \f(CW\*(C`CLISYM_\*(C' are
significant when an element of \f(CW%ENV is set or deleted: if the
complete string is \f(CW\*(C`CLISYM_LOCAL\*(C', the change is made in the local
symbol table; otherwise the global symbol table is changed.

- Any other string
Item "Any other string"
If an element of *\s-1PERL_ENV_TABLES\s0* translates to any other string,
that string is used as the name of a logical name table, which is
consulted using *name* as the logical name.  The normal search
order of access modes is used.



> .Sp
*\s-1PERL_ENV_TABLES\s0* is translated once when Perl starts up; any changes
you make while Perl is running do not affect the behavior of \f(CW%ENV.
If *\s-1PERL_ENV_TABLES\s0* is not defined, then Perl defaults to consulting
first the logical name tables specified by *\s-1LNM$FILE_DEV\s0*, and then
the \s-1CRTL\s0 \f(CW\*(C`environ\*(C' array.  This default order is reversed when the
logical name *\s-1GNV$UNIX_SHELL\s0* is defined, such as when running under
\s-1GNV\s0 bash.
.Sp
For operations on \f(CW%ENV entries based on logical names or \s-1DCL\s0 symbols, the
key string is treated as if it were entirely uppercase, regardless of the
case actually specified in the Perl expression. Entries in \f(CW%ENV based on the
\s-1CRTL\s0's environ array preserve the case of the key string when stored, and
lookups are case sensitive.
.Sp
When an element of \f(CW%ENV is read, the locations to which
*\s-1PERL_ENV_TABLES\s0* points are checked in order, and the value
obtained from the first successful lookup is returned.  If the
name of the \f(CW%ENV element contains a semi-colon, it and
any characters after it are removed.  These are ignored when
the \s-1CRTL\s0 \f(CW\*(C`environ\*(C' array or a \s-1CLI\s0 symbol table is consulted.
However, the name is looked up in a logical name table, the
suffix after the semi-colon is treated as the translation index
to be used for the lookup.   This lets you look up successive values
for search list logical names.  For instance, if you say
.Sp
.Vb 3
   $  Define STORY  once,upon,a,time,there,was
   $  perl -e "for ($i = 0; $i <= 6; $i++) " -
   _$ -e "\{ print $ENV\{\*(Aqstory;\*(Aq.$i\},\*(Aq \*(Aq\}"
.Ve
.Sp
Perl will print \f(CW\*(C`ONCE UPON A TIME THERE WAS\*(C', assuming, of course,
that *\s-1PERL_ENV_TABLES\s0* is set up so that the logical name \f(CW\*(C`story\*(C'
is found, rather than a \s-1CLI\s0 symbol or \s-1CRTL\s0 \f(CW\*(C`environ\*(C' element with
the same name.
.Sp
When an element of \f(CW%ENV is set to a defined string, the
corresponding definition is made in the location to which the
first translation of *\s-1PERL_ENV_TABLES\s0* points.  If this causes a
logical name to be created, it is defined in supervisor mode.
(The same is done if an existing logical name was defined in
executive or kernel mode; an existing user or supervisor mode
logical name is reset to the new value.)  If the value is an empty
string, the logical name's translation is defined as a single \f(CW\*(C`NUL\*(C'
(\s-1ASCII\s0 \f(CW\*(C`\\0\*(C') character, since a logical name cannot translate to a
zero-length string.  (This restriction does not apply to \s-1CLI\s0 symbols
or \s-1CRTL\s0 \f(CW\*(C`environ\*(C' values; they are set to the empty string.)
.Sp
When an element of \f(CW%ENV is set to \f(CW\*(C`undef\*(C', the element is looked
up as if it were being read, and if it is found, it is deleted.  (An
item \*(L"deleted\*(R" from the \s-1CRTL\s0 \f(CW\*(C`environ\*(C' array is set to the empty
string.)  Using \f(CW\*(C`delete\*(C' to remove an element from \f(CW%ENV has a
similar effect, but after the element is deleted, another attempt is
made to look up the element, so an inner-mode logical name or a name
in another location will replace the logical name just deleted. In
either case, only the first value found searching \s-1PERL_ENV_TABLES\s0 is
altered.  It is not possible at present to define a search list
logical name via \f(CW%ENV.
.Sp
The element \f(CW$ENV\{DEFAULT\} is special: when read, it returns
Perl's current default device and directory, and when set, it
resets them, regardless of the definition of *\s-1PERL_ENV_TABLES\s0*.
It cannot be cleared or deleted; attempts to do so are silently
ignored.
.Sp
Note that if you want to pass on any elements of the
C-local environ array to a subprocess which isn't
started by fork/exec, or isn't running a C program, you
can \*(L"promote\*(R" them to logical names in the current
process, which will then be inherited by all subprocesses,
by saying
.Sp
.Vb 4
    foreach my $key (qw[C-local keys you want promoted]) \{
        my $temp = $ENV\{$key\}; # read from C-local array
        $ENV\{$key\} = $temp;    # and define as logical name
    \}
.Ve
.Sp
(You can't just say \f(CW$ENV\{$key\} = $ENV\{$key\}, since the
Perl optimizer is smart enough to elide the expression.)
.Sp
Don't try to clear \f(CW%ENV by saying \f(CW\*(C`%ENV = ();\*(C', it will throw
a fatal error.  This is equivalent to doing the following from \s-1DCL:\s0
.Sp
.Vb 1
    DELETE/LOGICAL *
.Ve
.Sp
You can imagine how bad things would be if, for example, the \s-1SYS$MANAGER\s0
or \s-1SYS$SYSTEM\s0 logical names were deleted.
.Sp
At present, the first time you iterate over \f(CW%ENV using
\f(CW\*(C`keys\*(C', or \f(CW\*(C`values\*(C',  you will incur a time penalty as all
logical names are read, in order to fully populate \f(CW%ENV.
Subsequent iterations will not reread logical names, so they
won't be as slow, but they also won't reflect any changes
to logical name tables caused by other programs.
.Sp
You do need to be careful with the logical names representing
process-permanent files, such as \f(CW\*(C`SYS$INPUT\*(C' and \f(CW\*(C`SYS$OUTPUT\*(C'.
The translations for these logical names are prepended with a
two-byte binary value (0x1B 0x00) that needs to be stripped off
if you want to use it. (In previous versions of Perl it wasn't
possible to get the values of these logical names, as the null
byte acted as an end-of-string marker)



- $!
The string value of \f(CW$! is that returned by the \s-1CRTL\s0's
**strerror()** function, so it will include the \s-1VMS\s0 message for
VMS-specific errors.  The numeric value of \f(CW$! is the
value of \f(CW\*(C`errno\*(C', except if errno is \s-1EVMSERR,\s0 in which
case \f(CW$! contains the value of vaxc$errno.  Setting \f(CW$!
always sets errno to the value specified.  If this value is
\s-1EVMSERR,\s0 it also sets vaxc$errno to 4 (NONAME-F-NOMSG), so
that the string value of \f(CW$! won't reflect the \s-1VMS\s0 error
message from before \f(CW$! was set.

- $^E
Item "$^E"
This variable provides direct access to \s-1VMS\s0 status values
in vaxc$errno, which are often more specific than the
generic Unix-style error messages in \f(CW$!.  Its numeric value
is the value of vaxc$errno, and its string value is the
corresponding \s-1VMS\s0 message string, as retrieved by sys$**getmsg()**.
Setting \f(CW$^E sets vaxc$errno to the value specified.
.Sp
While Perl attempts to keep the vaxc$errno value to be current, if
errno is not \s-1EVMSERR,\s0 it may not be from the current operation.

- $?
The \*(L"status value\*(R" returned in \f(CW$? is synthesized from the
actual exit status of the subprocess in a way that approximates
\s-1POSIX\s0 **wait**\|(5) semantics, in order to allow Perl programs to
portably test for successful completion of subprocesses.  The
low order 8 bits of \f(CW$? are always 0 under \s-1VMS,\s0 since the
termination status of a process may or may not have been
generated by an exception.
.Sp
The next 8 bits contain the termination status of the program.
.Sp
If the child process follows the convention of C programs
compiled with the _POSIX_EXIT macro set, the status value will
contain the actual value of 0 to 255 returned by that program
on a normal exit.
.Sp
With the _POSIX_EXIT macro set, the Unix exit value of zero is
represented as a \s-1VMS\s0 native status of 1, and the Unix values
from 2 to 255 are encoded by the equation:
.Sp
.Vb 1
   VMS_status = 0x35a000 + (unix_value * 8) + 1.
.Ve
.Sp
And in the special case of Unix value 1 the encoding is:
.Sp
.Vb 1
   VMS_status = 0x35a000 + 8 + 2 + 0x10000000.
.Ve
.Sp
For other termination statuses, the severity portion of the
subprocess's exit status is used: if the severity was success or
informational, these bits are all 0; if the severity was
warning, they contain a value of 1; if the severity was
error or fatal error, they contain the actual severity bits,
which turns out to be a value of 2 for error and 4 for severe_error.
Fatal is another term for the severe_error status.
.Sp
As a result, \f(CW$? will always be zero if the subprocess's exit
status indicated successful completion, and non-zero if a
warning or error occurred or a program compliant with encoding
_POSIX_EXIT values was run and set a status.
.Sp
How can you tell the difference between a non-zero status that is
the result of a \s-1VMS\s0 native error status or an encoded Unix status?
You can not unless you look at the $\{^CHILD_ERROR_NATIVE\} value.
The $\{^CHILD_ERROR_NATIVE\} value returns the actual \s-1VMS\s0 status value
and check the severity bits. If the severity bits are equal to 1,
then if the numeric value for \f(CW$? is between 2 and 255 or 0, then
\f(CW$? accurately reflects a value passed back from a Unix application.
If \f(CW$? is 1, and the severity bits indicate a \s-1VMS\s0 error (2), then
\f(CW$? is from a Unix application exit value.
.Sp
In practice, Perl scripts that call programs that return _POSIX_EXIT
type status values will be expecting those values, and programs that
call traditional \s-1VMS\s0 programs will either be expecting the previous
behavior or just checking for a non-zero status.
.Sp
And success is always the value 0 in all behaviors.
.Sp
When the actual \s-1VMS\s0 termination status of the child is an error,
internally the \f(CW$! value will be set to the closest Unix errno
value to that error so that Perl scripts that test for error
messages will see the expected Unix style error message instead
of a \s-1VMS\s0 message.
.Sp
Conversely, when setting \f(CW$? in an \s-1END\s0 block, an attempt is made
to convert the \s-1POSIX\s0 value into a native status intelligible to
the operating system upon exiting Perl.  What this boils down to
is that setting \f(CW$? to zero results in the generic success value
\s-1SS$_NORMAL,\s0 and setting \f(CW$? to a non-zero value results in the
generic failure status \s-1SS$_ABORT.\s0  See also \*(L"exit\*(R" in perlport.
.Sp
With the \f(CW\*(C`PERL_VMS_POSIX_EXIT\*(C' logical name defined as \*(L"\s-1ENABLE\*(R",\s0
setting \f(CW$? will cause the new value to be encoded into \f(CW$^E
so that either the original parent or child exit status values
 0 to 255 can be automatically recovered by C programs expecting
_POSIX_EXIT behavior.  If both a parent and a child exit value are
non-zero, then it will be assumed that this is actually a \s-1VMS\s0 native
status value to be passed through.  The special value of 0xFFFF is
almost a \s-1NOOP\s0 as it will cause the current native \s-1VMS\s0 status in the
C library to become the current native Perl \s-1VMS\s0 status, and is handled
this way as it is known to not be a valid native \s-1VMS\s0 status value.
It is recommend that only values in the range of normal Unix parent or
child status numbers, 0 to 255 are used.
.Sp
The pragma \f(CW\*(C`use vmsish \*(Aqstatus\*(Aq\*(C' makes \f(CW$? reflect the actual
\s-1VMS\s0 exit status instead of the default emulation of \s-1POSIX\s0 status
described above.  This pragma also disables the conversion of
non-zero values to \s-1SS$_ABORT\s0 when setting \f(CW$? in an \s-1END\s0
block (but zero will still be converted to \s-1SS$_NORMAL\s0).
.Sp
Do not use the pragma \f(CW\*(C`use vmsish \*(Aqstatus\*(Aq\*(C' with \f(CW\*(C`PERL_VMS_POSIX_EXIT\*(C'
enabled, as they are at times requesting conflicting actions and the
consequence of ignoring this advice will be undefined to allow future
improvements in the \s-1POSIX\s0 exit handling.
.Sp
In general, with \f(CW\*(C`PERL_VMS_POSIX_EXIT\*(C' enabled, more detailed information
will be available in the exit status for \s-1DCL\s0 scripts or other native \s-1VMS\s0 tools,
and will give the expected information for Posix programs.  It has not been
made the default in order to preserve backward compatibility.
.Sp
N.B. Setting \f(CW\*(C`DECC$FILENAME_UNIX_REPORT\*(C' implicitly enables
\f(CW\*(C`PERL_VMS_POSIX_EXIT\*(C'.

- $|
Setting \f(CW$| for an I/O stream causes data to be flushed
all the way to disk on each write (*i.e.* not just to
the underlying \s-1RMS\s0 buffers for a file).  In other words,
it's equivalent to calling **fflush()** and **fsync()** from C.

## Standard modules with VMS-specific differences

Header "Standard modules with VMS-specific differences"

### SDBM_File

Subsection "SDBM_File"
SDBM_File works properly on \s-1VMS.\s0 It has, however, one minor
difference. The database directory file created has a *.sdbm_dir*
extension rather than a *.dir* extension. *.dir* files are \s-1VMS\s0 filesystem
directory files, and using them for other purposes could cause unacceptable
problems.

## Revision date

Header "Revision date"
Please see the git repository for revision history.

## AUTHOR

Header "AUTHOR"
Charles Bailey  bailey@cor.newman.upenn.edu
Craig Berry  craigberry@mac.com
Dan Sugalski  dan@sidhe.org
John Malmberg wb8tyw@qsl.net
