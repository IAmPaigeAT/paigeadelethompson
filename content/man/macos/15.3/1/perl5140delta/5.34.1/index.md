+++
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_section = "1"
manpage_format = "troff"
description = "This document describes differences between the 5.12.0 release and the 5.14.0 release. If you are upgrading from an earlier release such as 5.10.0, first read perl5120delta, which describes differences between 5.10.0 and 5.12.0. Some of the bug f..."
date = "2022-02-19"
operating_system = "macos"
author = "None Specified"
manpage_name = "perl5140delta"
operating_system_version = "15.3"
title = "perl5140delta(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5140DELTA 1"
PERL5140DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5140delta - what is new for perl v5.14.0

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.12.0 release and
the 5.14.0 release.

If you are upgrading from an earlier release such as 5.10.0, first read
perl5120delta, which describes differences between 5.10.0 and
5.12.0.

Some of the bug fixes in this release have been backported to subsequent
releases of 5.12.x.  Those are indicated with the 5.12.x version in
parentheses.

## Notice

Header "Notice"
As described in perlpolicy, the release of Perl 5.14.0 marks the
official end of support for Perl 5.10.  Users of Perl 5.10 or earlier
should consider upgrading to a more recent release of Perl.

## Core Enhancements

Header "Core Enhancements"

### Unicode

Subsection "Unicode"
*Unicode Version 6.0 is now supported (mostly)*
Subsection "Unicode Version 6.0 is now supported (mostly)"

Perl comes with the Unicode 6.0 data base updated with
Corrigendum #8 <http://www.unicode.org/versions/corrigendum8.html>,
with one exception noted below.
See <http://unicode.org/versions/Unicode6.0.0/> for details on the new
release.  Perl does not support any Unicode provisional properties,
including the new ones for this release.

Unicode 6.0 has chosen to use the name \f(CW\*(C`BELL\*(C' for the character at U+1F514,
which is a symbol that looks like a bell, and is used in Japanese cell
phones.  This conflicts with the long-standing Perl usage of having
\f(CW\*(C`BELL\*(C' mean the \s-1ASCII\s0 \f(CW\*(C`BEL\*(C' character, U+0007.  In Perl 5.14,
\f(CW\*(C`\\N\{BELL\}\*(C' continues to mean U+0007, but its use generates a
deprecation warning message unless such warnings are turned off.  The
new name for U+0007 in Perl is \f(CW\*(C`ALERT\*(C', which corresponds nicely
with the existing shorthand sequence for it, \f(CW"\\a".  \f(CW\*(C`\\N\{BEL\}\*(C'
means U+0007, with no warning given.  The character at U+1F514 has no
name in 5.14, but can be referred to by \f(CW\*(C`\\N\{U+1F514\}\*(C'.
In Perl 5.16, \f(CW\*(C`\\N\{BELL\}\*(C' will refer to U+1F514; all code
that uses \f(CW\*(C`\\N\{BELL\}\*(C' should be converted to use \f(CW\*(C`\\N\{ALERT\}\*(C',
\f(CW\*(C`\\N\{BEL\}\*(C', or \f(CW"\\a" before upgrading.

*Full functionality for \f(CI\*(C`use feature \*(Aqunicode_strings\*(Aq\*(C'\fI*
Subsection "Full functionality for use feature unicode_strings"

This release provides full functionality for \f(CW\*(C`use feature
\*(Aqunicode_strings\*(Aq\*(C'.  Under its scope, all string operations executed and
regular expressions compiled (even if executed outside its scope) have
Unicode semantics.  See \*(L"the 'unicode_strings' feature\*(R" in feature.
However, see \*(L"Inverted bracketed character classes and multi-character folds\*(R",
below.

This feature avoids most forms of the \*(L"Unicode Bug\*(R" (see
\*(L"The \*(R"Unicode Bug"" in perlunicode for details).  If there is any
possibility that your code will process Unicode strings, you are
*strongly* encouraged to use this subpragma to avoid nasty surprises.

*\f(CI\*(C`\\N\{\f(CINAME\f(CI\}\*(C'\fI and \f(CI\*(C`charnames\*(C'\fI enhancements*
Subsection "N\{NAME\} and charnames enhancements"

- \(bu
\f(CW\*(C`\\N\{\f(CINAME\f(CW\}\*(C' and \f(CW\*(C`charnames::vianame\*(C' now know about the abbreviated
character names listed by Unicode, such as \s-1NBSP, SHY, LRO, ZWJ,\s0 etc.; all
customary abbreviations for the C0 and C1 control characters (such as
\s-1ACK, BEL, CAN,\s0 etc.); and a few new variants of some C1 full names that
are in common usage.

- \(bu
Unicode has several *named character sequences*, in which particular sequences
of code points are given names.  \f(CW\*(C`\\N\{\f(CINAME\f(CW\}\*(C' now recognizes these.

- \(bu
\f(CW\*(C`\\N\{\f(CINAME\f(CW\}\*(C', \f(CW\*(C`charnames::vianame\*(C', and \f(CW\*(C`charnames::viacode\*(C'
now know about every character in Unicode.  In earlier releases of
Perl, they didn't know about the Hangul syllables nor several
\s-1CJK\s0 (Chinese/Japanese/Korean) characters.

- \(bu
It is now possible to override Perl's abbreviations with your own custom aliases.

- \(bu
You can now create a custom alias of the ordinal of a
character, known by \f(CW\*(C`\\N\{\f(CINAME\f(CW\}\*(C', \f(CW\*(C`charnames::vianame()\*(C', and
\f(CW\*(C`charnames::viacode()\*(C'.  Previously, aliases had to be to official
Unicode character names.  This made it impossible to create an alias for
unnamed code points, such as those reserved for private
use.

- \(bu
The new function **charnames::string_vianame()** is a run-time version
of \f(CW\*(C`\\N\{\f(CINAME\f(CW\}\}\*(C', returning the string of characters whose Unicode
name is its parameter.  It can handle Unicode named character
sequences, whereas the pre-existing **charnames::vianame()** cannot,
as the latter returns a single code point.

See charnames for details on all these changes.

*New warnings categories for problematic (non-)Unicode code points.*
Subsection "New warnings categories for problematic (non-)Unicode code points."

Three new warnings subcategories of \*(L"utf8\*(R" have been added.  These
allow you to turn off some \*(L"utf8\*(R" warnings, while allowing
other warnings to remain on.  The three categories are:
\f(CW\*(C`surrogate\*(C' when \s-1UTF-16\s0 surrogates are encountered;
\f(CW\*(C`nonchar\*(C' when Unicode non-character code points are encountered;
and \f(CW\*(C`non_unicode\*(C' when code points above the legal Unicode
maximum of 0x10FFFF are encountered.

*Any unsigned value can be encoded as a character*
Subsection "Any unsigned value can be encoded as a character"

With this release, Perl is adopting a model that any unsigned value
can be treated as a code point and encoded internally (as utf8)
without warnings, not just the code points that are legal in Unicode.
However, unless utf8 or the corresponding sub-category (see previous
item) of lexical warnings have been explicitly turned off, outputting
or executing a Unicode-defined operation such as upper-casing
on such a code point generates a warning.  Attempting to input these
using strict rules (such as with the \f(CW\*(C`:encoding(UTF-8)\*(C' layer)
will continue to fail.  Prior to this release, handling was
inconsistent and in places, incorrect.

Unicode non-characters, some of which previously were erroneously
considered illegal in places by Perl, contrary to the Unicode Standard,
are now always legal internally.  Inputting or outputting them
works the same as with the non-legal Unicode code points, because the Unicode
Standard says they are (only) illegal for \*(L"open interchange\*(R".

*Unicode database files not installed*
Subsection "Unicode database files not installed"

The Unicode database files are no longer installed with Perl.  This
doesn't affect any functionality in Perl and saves significant disk
space.  If you need these files, you can download them from
<http://www.unicode.org/Public/zipped/6.0.0/>.

### Regular Expressions

Subsection "Regular Expressions"
*\f(CI\*(C`(?^...)\*(C'\fI construct signifies default modifiers*
Subsection "(?^...) construct signifies default modifiers"

An \s-1ASCII\s0 caret \f(CW"^" immediately following a \f(CW"(?" in a regular
expression now means that the subexpression does not inherit surrounding
modifiers such as \f(CW\*(C`/i\*(C', but reverts to the Perl defaults.  Any modifiers
following the caret override the defaults.

Stringification of regular expressions now uses this notation.
For example, \f(CW\*(C`qr/hlagh/i\*(C' would previously be stringified as
\f(CW\*(C`(?i-xsm:hlagh)\*(C', but now it's stringified as \f(CW\*(C`(?^i:hlagh)\*(C'.

The main purpose of this change is to allow tests that rely on the
stringification *not* to have to change whenever new modifiers are added.
See \*(L"Extended Patterns\*(R" in perlre.

This change is likely to break code that compares stringified regular
expressions with fixed strings containing \f(CW\*(C`?-xism\*(C'.

*\f(CI\*(C`/d\*(C'\fI, \f(CI\*(C`/l\*(C'\fI, \f(CI\*(C`/u\*(C'\fI, and \f(CI\*(C`/a\*(C'\fI modifiers*
Subsection "/d, /l, /u, and /a modifiers"

Four new regular expression modifiers have been added.  These are mutually
exclusive: one only can be turned on at a time.

- \(bu
The \f(CW\*(C`/l\*(C' modifier says to compile the regular expression as if it were
in the scope of \f(CW\*(C`use locale\*(C', even if it is not.

- \(bu
The \f(CW\*(C`/u\*(C' modifier says to compile the regular expression as if it were
in the scope of a \f(CW\*(C`use feature \*(Aqunicode_strings\*(Aq\*(C' pragma.

- \(bu
The \f(CW\*(C`/d\*(C' (default) modifier is used to override any \f(CW\*(C`use locale\*(C' and
\f(CW\*(C`use feature \*(Aqunicode_strings\*(Aq\*(C' pragmas in effect at the time
of compiling the regular expression.

- \(bu
The \f(CW\*(C`/a\*(C' regular expression modifier restricts \f(CW\*(C`\\s\*(C', \f(CW\*(C`\\d\*(C' and \f(CW\*(C`\\w\*(C' and
the \s-1POSIX\s0 (\f(CW\*(C`[[:posix:]]\*(C') character classes to the \s-1ASCII\s0 range.  Their
complements and \f(CW\*(C`\\b\*(C' and \f(CW\*(C`\\B\*(C' are correspondingly
affected.  Otherwise, \f(CW\*(C`/a\*(C' behaves like the \f(CW\*(C`/u\*(C' modifier, in that
case-insensitive matching uses Unicode semantics.
.Sp
If the \f(CW\*(C`/a\*(C' modifier is repeated, then additionally in case-insensitive
matching, no \s-1ASCII\s0 character can match a non-ASCII character.
For example,
.Sp
.Vb 2
    "k"     =~ /\\N\{KELVIN SIGN\}/ai
    "\\xDF" =~ /ss/ai
.Ve
.Sp
match but
.Sp
.Vb 2
    "k"    =~ /\\N\{KELVIN SIGN\}/aai
    "\\xDF" =~ /ss/aai
.Ve
.Sp
do not match.

See \*(L"Modifiers\*(R" in perlre for more detail.

*Non-destructive substitution*
Subsection "Non-destructive substitution"

The substitution (\f(CW\*(C`s///\*(C') and transliteration
(\f(CW\*(C`y///\*(C') operators now support an \f(CW\*(C`/r\*(C' option that
copies the input variable, carries out the substitution on
the copy, and returns the result.  The original remains unmodified.

.Vb 3
  my $old = "cat";
  my $new = $old =~ s/cat/dog/r;
  # $old is "cat" and $new is "dog"
.Ve

This is particularly useful with \f(CW\*(C`map\*(C'.  See perlop for more examples.

*Re-entrant regular expression engine*
Subsection "Re-entrant regular expression engine"

It is now safe to use regular expressions within \f(CW\*(C`(?\{...\})\*(C' and
\f(CW\*(C`(??\{...\})\*(C' code blocks inside regular expressions.

These blocks are still experimental, however, and still have problems with
lexical (\f(CW\*(C`my\*(C') variables and abnormal exiting.

*\f(CI\*(C`use re \*(Aq/flags\*(Aq\*(C'\fI*
Subsection "use re /flags"

The \f(CW\*(C`re\*(C' pragma now has the ability to turn on regular expression flags
till the end of the lexical scope:

.Vb 2
    use re "/x";
    "foo" =~ / (.+) /;  # /x implied
.Ve

See \*(L"'/flags' mode\*(R" in re for details.

*\\o\{...\} for octals*
Subsection "o\{...\} for octals"

There is a new octal escape sequence, \f(CW"\\o", in doublequote-like
contexts.  This construct allows large octal ordinals beyond the
current max of 0777 to be represented.  It also allows you to specify a
character in octal which can safely be concatenated with other regex
snippets and which won't be confused with being a backreference to
a regex capture group.  See \*(L"Capture groups\*(R" in perlre.

*Add \f(CI\*(C`\\p\{Titlecase\}\*(C'\fI as a synonym for \f(CI\*(C`\\p\{Title\}\*(C'\fI*
Subsection "Add p\{Titlecase\} as a synonym for p\{Title\}"

This synonym is added for symmetry with the Unicode property names
\f(CW\*(C`\\p\{Uppercase\}\*(C' and \f(CW\*(C`\\p\{Lowercase\}\*(C'.

*Regular expression debugging output improvement*
Subsection "Regular expression debugging output improvement"

Regular expression debugging output (turned on by \f(CW\*(C`use re \*(Aqdebug\*(Aq\*(C') now
uses hexadecimal when escaping non-ASCII characters, instead of octal.

*Return value of \f(CI\*(C`delete $+\{...\}\*(C'\fI*
Subsection "Return value of delete $+\{...\}"

Custom regular expression engines can now determine the return value of
\f(CW\*(C`delete\*(C' on an entry of \f(CW\*(C`%+\*(C' or \f(CW\*(C`%-\*(C'.

### Syntactical Enhancements

Subsection "Syntactical Enhancements"
*Array and hash container functions accept references*
Subsection "Array and hash container functions accept references"

**Warning:** This feature is considered experimental, as the exact behaviour
may change in a future version of Perl.

All builtin functions that operate directly on array or hash
containers now also accept unblessed hard references to arrays
or hashes:

.Vb 10
  |----------------------------+---------------------------|
  | Traditional syntax         | Terse syntax              |
  |----------------------------+---------------------------|
  | push @$arrayref, @stuff    | push $arrayref, @stuff    |
  | unshift @$arrayref, @stuff | unshift $arrayref, @stuff |
  | pop @$arrayref             | pop $arrayref             |
  | shift @$arrayref           | shift $arrayref           |
  | splice @$arrayref, 0, 2    | splice $arrayref, 0, 2    |
  | keys %$hashref             | keys $hashref             |
  | keys @$arrayref            | keys $arrayref            |
  | values %$hashref           | values $hashref           |
  | values @$arrayref          | values $arrayref          |
  | ($k,$v) = each %$hashref   | ($k,$v) = each $hashref   |
  | ($k,$v) = each @$arrayref  | ($k,$v) = each $arrayref  |
  |----------------------------+---------------------------|
.Ve

This allows these builtin functions to act on long dereferencing chains
or on the return value of subroutines without needing to wrap them in
\f(CW\*(C`@\{\}\*(C' or \f(CW\*(C`%\{\}\*(C':

.Vb 2
  push @\{$obj->tags\}, $new_tag;  # old way
  push $obj->tags,    $new_tag;  # new way

  for ( keys %\{$hoh->\{genres\}\{artists\}\} ) \{...\} # old way
  for ( keys $hoh->\{genres\}\{artists\}    ) \{...\} # new way
.Ve

*Single term prototype*
Subsection "Single term prototype"

The \f(CW\*(C`+\*(C' prototype is a special alternative to \f(CW\*(C`$\*(C' that acts like
\f(CW\*(C`\\[@%]\*(C' when given a literal array or hash variable, but will otherwise
force scalar context on the argument.  See \*(L"Prototypes\*(R" in perlsub.

*\f(CI\*(C`package\*(C'\fI block syntax*
Subsection "package block syntax"

A package declaration can now contain a code block, in which case the
declaration is in scope inside that block only.  So \f(CW\*(C`package Foo \{ ... \}\*(C'
is precisely equivalent to \f(CW\*(C`\{ package Foo; ... \}\*(C'.  It also works with
a version number in the declaration, as in \f(CW\*(C`package Foo 1.2 \{ ... \}\*(C',
which is its most attractive feature.  See perlfunc.

*Statement labels can appear in more places*
Subsection "Statement labels can appear in more places"

Statement labels can now occur before any type of statement or declaration,
such as \f(CW\*(C`package\*(C'.

*Stacked labels*
Subsection "Stacked labels"

Multiple statement labels can now appear before a single statement.

*Uppercase X/B allowed in hexadecimal/binary literals*
Subsection "Uppercase X/B allowed in hexadecimal/binary literals"

Literals may now use either upper case \f(CW\*(C`0X...\*(C' or \f(CW\*(C`0B...\*(C' prefixes,
in addition to the already supported \f(CW\*(C`0x...\*(C' and \f(CW\*(C`0b...\*(C'
syntax [perl #76296].

C, Ruby, Python, and \s-1PHP\s0 already support this syntax, and it makes
Perl more internally consistent: a round-trip with \f(CW\*(C`eval sprintf
"%#X", 0x10\*(C' now returns \f(CW16, just like \f(CW\*(C`eval sprintf "%#x", 0x10\*(C'.

*Overridable tie functions*
Subsection "Overridable tie functions"

\f(CW\*(C`tie\*(C', \f(CW\*(C`tied\*(C' and \f(CW\*(C`untie\*(C' can now be overridden [perl #75902].

### Exception Handling

Subsection "Exception Handling"
To make them more reliable and consistent, several changes have been made
to how \f(CW\*(C`die\*(C', \f(CW\*(C`warn\*(C', and \f(CW$@ behave.

- \(bu
When an exception is thrown inside an \f(CW\*(C`eval\*(C', the exception is no
longer at risk of being clobbered by destructor code running during unwinding.
Previously, the exception was written into \f(CW$@
early in the throwing process, and would be overwritten if \f(CW\*(C`eval\*(C' was
used internally in the destructor for an object that had to be freed
while exiting from the outer \f(CW\*(C`eval\*(C'.  Now the exception is written
into \f(CW$@ last thing before exiting the outer \f(CW\*(C`eval\*(C', so the code
running immediately thereafter can rely on the value in \f(CW$@ correctly
corresponding to that \f(CW\*(C`eval\*(C'.  (\f(CW$@ is still also set before exiting the
\f(CW\*(C`eval\*(C', for the sake of destructors that rely on this.)
.Sp
Likewise, a \f(CW\*(C`local $@\*(C' inside an \f(CW\*(C`eval\*(C' no longer clobbers any
exception thrown in its scope.  Previously, the restoration of \f(CW$@ upon
unwinding would overwrite any exception being thrown.  Now the exception
gets to the \f(CW\*(C`eval\*(C' anyway.  So \f(CW\*(C`local $@\*(C' is safe before a \f(CW\*(C`die\*(C'.
.Sp
Exceptions thrown from object destructors no longer modify the \f(CW$@
of the surrounding context.  (If the surrounding context was exception
unwinding, this used to be another way to clobber the exception being
thrown.)  Previously such an exception was
sometimes emitted as a warning, and then either was
string-appended to the surrounding \f(CW$@ or completely replaced the
surrounding \f(CW$@, depending on whether that exception and the surrounding
\f(CW$@ were strings or objects.  Now, an exception in this situation is
always emitted as a warning, leaving the surrounding \f(CW$@ untouched.
In addition to object destructors, this also affects any function call
run by \s-1XS\s0 code using the \f(CW\*(C`G_KEEPERR\*(C' flag.

- \(bu
Warnings for \f(CW\*(C`warn\*(C' can now be objects in the same way as exceptions
for \f(CW\*(C`die\*(C'.  If an object-based warning gets the default handling
of writing to standard error, it is stringified as before with the
filename and line number appended.  But a \f(CW$SIG\{_\|_WARN_\|_\} handler now
receives an object-based warning as an object, where previously it
was passed the result of stringifying the object.

### Other Enhancements

Subsection "Other Enhancements"
*Assignment to \f(CI$0\fI sets the legacy process name with \f(BIprctl()\fI on Linux*
Subsection "Assignment to $0 sets the legacy process name with prctl() on Linux"

On Linux the legacy process name is now set with **prctl**\|(2), in
addition to altering the \s-1POSIX\s0 name via \f(CW\*(C`argv[0]\*(C', as Perl has done
since version 4.000.  Now system utilities that read the legacy process
name such as *ps*, *top*, and *killall* recognize the name you set when
assigning to \f(CW$0.  The string you supply is truncated at 16 bytes;
this limitation is imposed by Linux.

*\f(BIsrand()\fI now returns the seed*
Subsection "srand() now returns the seed"

This allows programs that need to have repeatable results not to have to come
up with their own seed-generating mechanism.  Instead, they can use **srand()**
and stash the return value for future use.  One example is a test program with
too many combinations to test comprehensively in the time available for
each run.  It can test a random subset each time and, should there be a failure,
log the seed used for that run so this can later be used to produce the same results.

*printf-like functions understand post-1980 size modifiers*
Subsection "printf-like functions understand post-1980 size modifiers"

Perl's printf and sprintf operators, and Perl's internal printf replacement
function, now understand the C90 size modifiers \*(L"hh\*(R" (\f(CW\*(C`char\*(C'), \*(L"z\*(R"
(\f(CW\*(C`size_t\*(C'), and \*(L"t\*(R" (\f(CW\*(C`ptrdiff_t\*(C').  Also, when compiled with a C99
compiler, Perl now understands the size modifier \*(L"j\*(R" (\f(CW\*(C`intmax_t\*(C')
(but this is not portable).

So, for example, on any modern machine, \f(CW\*(C`sprintf("%hhd", 257)\*(C' returns \*(L"1\*(R".

*New global variable \f(CI\*(C`$\{^GLOBAL_PHASE\}\*(C'\fI*
Subsection "New global variable $\{^GLOBAL_PHASE\}"

A new global variable, \f(CW\*(C`$\{^GLOBAL_PHASE\}\*(C', has been added to allow
introspection of the current phase of the Perl interpreter.  It's explained in
detail in \*(L"$\{^GLOBAL_PHASE\}\*(R" in perlvar and in
\*(L"\s-1BEGIN, UNITCHECK, CHECK, INIT\s0 and \s-1END\*(R"\s0 in perlmod.

*\f(CI\*(C`-d:-foo\*(C'\fI calls \f(CI\*(C`Devel::foo::unimport\*(C'\fI*
Subsection "-d:-foo calls Devel::foo::unimport"

The syntax **-d:foo** was extended in 5.6.1 to make **-d:foo=bar**
equivalent to **-MDevel::foo=bar**, which expands
internally to \f(CW\*(C`use Devel::foo \*(Aqbar\*(Aq\*(C'.
Perl now allows prefixing the module name with **-**, with the same
semantics as **-M**; that is:
.ie n .IP """-d:-foo""" 4
.el .IP "\f(CW-d:-foo" 4
Item "-d:-foo"
Equivalent to **-M-Devel::foo**: expands to
\f(CW\*(C`no Devel::foo\*(C' and calls \f(CW\*(C`Devel::foo->unimport()\*(C'
if that method exists.
.ie n .IP """-d:-foo=bar""" 4
.el .IP "\f(CW-d:-foo=bar" 4
Item "-d:-foo=bar"
Equivalent to **-M-Devel::foo=bar**: expands to \f(CW\*(C`no Devel::foo \*(Aqbar\*(Aq\*(C',
and calls \f(CW\*(C`Devel::foo->unimport("bar")\*(C' if that method exists.

This is particularly useful for suppressing the default actions of a
\f(CW\*(C`Devel::*\*(C' module's \f(CW\*(C`import\*(C' method whilst still loading it for debugging.

*Filehandle method calls load IO::File on demand*
Subsection "Filehandle method calls load IO::File on demand"

When a method call on a filehandle would die because the method cannot
be resolved and IO::File has not been loaded, Perl now loads IO::File
via \f(CW\*(C`require\*(C' and attempts method resolution again:

.Vb 2
  open my $fh, ">", $file;
  $fh->binmode(":raw");     # loads IO::File and succeeds
.Ve

This also works for globs like \f(CW\*(C`STDOUT\*(C', \f(CW\*(C`STDERR\*(C', and \f(CW\*(C`STDIN\*(C':

.Vb 1
  STDOUT->autoflush(1);
.Ve

Because this on-demand load happens only if method resolution fails, the
legacy approach of manually loading an IO::File parent class for partial
method support still works as expected:

.Vb 3
  use IO::Handle;
  open my $fh, ">", $file;
  $fh->autoflush(1);        # IO::File not loaded
.Ve

*Improved IPv6 support*
Subsection "Improved IPv6 support"

The \f(CW\*(C`Socket\*(C' module provides new affordances for IPv6,
including implementations of the \f(CW\*(C`Socket::getaddrinfo()\*(C' and
\f(CW\*(C`Socket::getnameinfo()\*(C' functions, along with related constants and a
handful of new functions.  See Socket.

*DTrace probes now include package name*
Subsection "DTrace probes now include package name"

The \f(CW\*(C`DTrace\*(C' probes now include an additional argument, \f(CW\*(C`arg3\*(C', which contains
the package the subroutine being entered or left was compiled in.

For example, using the following DTrace script:

.Vb 4
  perl$target:::sub-entry
  \{
      printf("%s::%s\\n", copyinstr(arg0), copyinstr(arg3));
  \}
.Ve

and then running:

.Vb 1
  $ perl -e \*(Aqsub test \{ \}; test\*(Aq
.Ve

\f(CW\*(C`DTrace\*(C' will print:

.Vb 1
  main::test
.Ve

### New C APIs

Subsection "New C APIs"
See \*(L"Internal Changes\*(R".

## Security

Header "Security"

### User-defined regular expression properties

Subsection "User-defined regular expression properties"
\*(L"User-Defined Character Properties\*(R" in perlunicode documented that you can
create custom properties by defining subroutines whose names begin with
\*(L"In\*(R" or \*(L"Is\*(R".  However, Perl did not actually enforce that naming
restriction, so \f(CW\*(C`\\p\{foo::bar\}\*(C' could call **foo::bar()** if it existed.  The documented
convention is now enforced.

Also, Perl no longer allows tainted regular expressions to invoke a
user-defined property.  It simply dies instead [perl #82616].

## Incompatible Changes

Header "Incompatible Changes"
Perl 5.14.0 is not binary-compatible with any previous stable release.

In addition to the sections that follow, see \*(L"C \s-1API\s0 Changes\*(R".

### Regular Expressions and String Escapes

Subsection "Regular Expressions and String Escapes"
*Inverted bracketed character classes and multi-character folds*
Subsection "Inverted bracketed character classes and multi-character folds"

Some characters match a sequence of two or three characters in \f(CW\*(C`/i\*(C'
regular expression matching under Unicode rules.  One example is
\f(CW\*(C`LATIN SMALL LETTER SHARP S\*(C' which matches the sequence \f(CW\*(C`ss\*(C'.

.Vb 1
 \*(Aqss\*(Aq =~ /\\A[\\N\{LATIN SMALL LETTER SHARP S\}]\\z/i  # Matches
.Ve

This, however, can lead to very counter-intuitive results, especially
when inverted.  Because of this, Perl 5.14 does not use multi-character \f(CW\*(C`/i\*(C'
matching in inverted character classes.

.Vb 1
 \*(Aqss\*(Aq =~ /\\A[^\\N\{LATIN SMALL LETTER SHARP S\}]+\\z/i  # ???
.Ve

This should match any sequences of characters that aren't the \f(CW\*(C`SHARP S\*(C'
nor what \f(CW\*(C`SHARP S\*(C' matches under \f(CW\*(C`/i\*(C'.  \f(CW"s" isn't \f(CW\*(C`SHARP S\*(C', but
Unicode says that \f(CW"ss" is what \f(CW\*(C`SHARP S\*(C' matches under \f(CW\*(C`/i\*(C'.  So
which one \*(L"wins\*(R"? Do you fail the match because the string has \f(CW\*(C`ss\*(C' or
accept it because it has an \f(CW\*(C`s\*(C' followed by another \f(CW\*(C`s\*(C'?

Earlier releases of Perl did allow this multi-character matching,
but due to bugs, it mostly did not work.

*\\400-\\777*
Subsection "400-777"

In certain circumstances, \f(CW\*(C`\\400\*(C'-\f(CW\*(C`\\777\*(C' in regexes have behaved
differently than they behave in all other doublequote-like contexts.
Since 5.10.1, Perl has issued a deprecation warning when this happens.
Now, these literals behave the same in all doublequote-like contexts,
namely to be equivalent to \f(CW\*(C`\\x\{100\}\*(C'-\f(CW\*(C`\\x\{1FF\}\*(C', with no deprecation
warning.

Use of \f(CW\*(C`\\400\*(C'-\f(CW\*(C`\\777\*(C' in the command-line option **-0** retain their
conventional meaning.  They slurp whole input files; previously, this
was documented only for **-0777**.

Because of various ambiguities, you should use the new
\f(CW\*(C`\\o\{...\}\*(C' construct to represent characters in octal instead.

*Most \f(CI\*(C`\\p\{\}\*(C'\fI properties are now immune to case-insensitive matching*
Subsection "Most p\{\} properties are now immune to case-insensitive matching"

For most Unicode properties, it doesn't make sense to have them match
differently under \f(CW\*(C`/i\*(C' case-insensitive matching.  Doing so can lead
to unexpected results and potential security holes.  For example

.Vb 1
 m/\\p\{ASCII_Hex_Digit\}+/i
.Ve

could previously match non-ASCII characters because of the Unicode
matching rules (although there were several bugs with this).  Now
matching under \f(CW\*(C`/i\*(C' gives the same results as non-\f(CW\*(C`/i\*(C' matching except
for those few properties where people have come to expect differences,
namely the ones where casing is an integral part of their meaning, such
as \f(CW\*(C`m/\\p\{Uppercase\}/i\*(C' and \f(CW\*(C`m/\\p\{Lowercase\}/i\*(C', both of which match
the same code points as matched by \f(CW\*(C`m/\\p\{Cased\}/i\*(C'.
Details are in \*(L"Unicode Properties\*(R" in perlrecharclass.

User-defined property handlers that need to match differently under \f(CW\*(C`/i\*(C'
must be changed to read the new boolean parameter passed to them, which
is non-zero if case-insensitive matching is in effect and 0 otherwise.
See \*(L"User-Defined Character Properties\*(R" in perlunicode.

*\\p\{\} implies Unicode semantics*
Subsection "p\{\} implies Unicode semantics"

Specifying a Unicode property in the pattern indicates
that the pattern is meant for matching according to Unicode rules, the way
\f(CW\*(C`\\N\{\f(CINAME\f(CW\}\*(C' does.

*Regular expressions retain their localeness when interpolated*
Subsection "Regular expressions retain their localeness when interpolated"

Regular expressions compiled under \f(CW\*(C`use locale\*(C' now retain this when
interpolated into a new regular expression compiled outside a
\f(CW\*(C`use locale\*(C', and vice-versa.

Previously, one regular expression interpolated into another inherited
the localeness of the surrounding regex, losing whatever state it
originally had.  This is considered a bug fix, but may trip up code that
has come to rely on the incorrect behaviour.

*Stringification of regexes has changed*
Subsection "Stringification of regexes has changed"

Default regular expression modifiers are now notated using
\f(CW\*(C`(?^...)\*(C'.  Code relying on the old stringification will fail.
This is so that when new modifiers are added, such code won't
have to keep changing each time this happens, because the stringification
will automatically incorporate the new modifiers.

Code that needs to work properly with both old- and new-style regexes
can avoid the whole issue by using (for perls since 5.9.5; see re):

.Vb 2
 use re qw(regexp_pattern);
 my ($pat, $mods) = regexp_pattern($re_ref);
.Ve

If the actual stringification is important or older Perls need to be
supported, you can use something like the following:

.Vb 2
    # Accept both old and new-style stringification
    my $modifiers = (qr/foobar/ =~ /\\Q(?^/) ? "^" : "-xism";
.Ve

And then use \f(CW$modifiers instead of \f(CW\*(C`-xism\*(C'.

*Run-time code blocks in regular expressions inherit pragmata*
Subsection "Run-time code blocks in regular expressions inherit pragmata"

Code blocks in regular expressions (\f(CW\*(C`(?\{...\})\*(C' and \f(CW\*(C`(??\{...\})\*(C') previously
did not inherit pragmata (strict, warnings, etc.) if the regular expression
was compiled at run time as happens in cases like these two:

.Vb 3
  use re "eval";
  $foo =~ $bar; # when $bar contains (?\{...\})
  $foo =~ /$bar(?\{ $finished = 1 \})/;
.Ve

This bug has now been fixed, but code that relied on the buggy behaviour
may need to be fixed to account for the correct behaviour.

### Stashes and Package Variables

Subsection "Stashes and Package Variables"
*Localised tied hashes and arrays are no longed tied*
Subsection "Localised tied hashes and arrays are no longed tied"

In the following:

.Vb 6
    tie @a, ...;
    \{
            local @a;
            # here, @a is a now a new, untied array
    \}
    # here, @a refers again to the old, tied array
.Ve

Earlier versions of Perl incorrectly tied the new local array.  This has
now been fixed.  This fix could however potentially cause a change in
behaviour of some code.

*Stashes are now always defined*
Subsection "Stashes are now always defined"

\f(CW\*(C`defined %Foo::\*(C' now always returns true, even when no symbols have yet been
defined in that package.

This is a side-effect of removing a special-case kludge in the tokeniser,
added for 5.10.0, to hide side-effects of changes to the internal storage of
hashes.  The fix drastically reduces hashes' memory overhead.

Calling defined on a stash has been deprecated since 5.6.0, warned on
lexicals since 5.6.0, and warned for stashes and other package
variables since 5.12.0.  \f(CW\*(C`defined %hash\*(C' has always exposed an
implementation detail: emptying a hash by deleting all entries from it does
not make \f(CW\*(C`defined %hash\*(C' false.  Hence \f(CW\*(C`defined %hash\*(C' is not valid code to
determine whether an arbitrary hash is empty.  Instead, use the behaviour
of an empty \f(CW%hash always returning false in scalar context.

*Clearing stashes*
Subsection "Clearing stashes"

Stash list assignment \f(CW\*(C`%foo:: = ()\*(C' used to make the stash temporarily
anonymous while it was being emptied.  Consequently, any of its
subroutines referenced elsewhere would become anonymous,  showing up as
\*(L"(unknown)\*(R" in \f(CW\*(C`caller\*(C'.  They now retain their package names such that
\f(CW\*(C`caller\*(C' returns the original sub name if there is still a reference
to its typeglob and \*(L"foo::_\|_ANON_\|_\*(R" otherwise [perl #79208].

*Dereferencing typeglobs*
Subsection "Dereferencing typeglobs"

If you assign a typeglob to a scalar variable:

.Vb 1
    $glob = *foo;
.Ve

the glob that is copied to \f(CW$glob is marked with a special flag
indicating that the glob is just a copy.  This allows subsequent
assignments to \f(CW$glob to overwrite the glob.  The original glob,
however, is immutable.

Some Perl operators did not distinguish between these two types of globs.
This would result in strange behaviour in edge cases: \f(CW\*(C`untie $scalar\*(C'
would not untie the scalar if the last thing assigned to it was a glob
(because it treated it as \f(CW\*(C`untie *$scalar\*(C', which unties a handle).
Assignment to a glob slot (such as \f(CW\*(C`*$glob = \\@some_array\*(C') would simply
assign \f(CW\*(C`\\@some_array\*(C' to \f(CW$glob.

To fix this, the \f(CW\*(C`*\{\}\*(C' operator (including its \f(CW*foo and \f(CW*$foo forms)
has been modified to make a new immutable glob if its operand is a glob
copy.  This allows operators that make a distinction between globs and
scalars to be modified to treat only immutable globs as globs.  (\f(CW\*(C`tie\*(C',
\f(CW\*(C`tied\*(C' and \f(CW\*(C`untie\*(C' have been left as they are for compatibility's sake,
but will warn.  See \*(L"Deprecations\*(R".)

This causes an incompatible change in code that assigns a glob to the
return value of \f(CW\*(C`*\{\}\*(C' when that operator was passed a glob copy.  Take the
following code, for instance:

.Vb 2
    $glob = *foo;
    *$glob = *bar;
.Ve

The \f(CW*$glob on the second line returns a new immutable glob.  That new
glob is made an alias to \f(CW*bar.  Then it is discarded.  So the second
assignment has no effect.

See <https://github.com/Perl/perl5/issues/10625> for
more detail.

*Magic variables outside the main package*
Subsection "Magic variables outside the main package"

In previous versions of Perl, magic variables like \f(CW$!, \f(CW%SIG, etc. would
\*(L"leak\*(R" into other packages.  So \f(CW%foo::SIG could be used to access signals,
\f(CW\*(C`$\{"foo::!"\}\*(C' (with strict mode off) to access C's \f(CW\*(C`errno\*(C', etc.

This was a bug, or an \*(L"unintentional\*(R" feature, which caused various ill effects,
such as signal handlers being wiped when modules were loaded, etc.

This has been fixed (or the feature has been removed, depending on how you see
it).

*local($_) strips all magic from \f(CI$_\fI*
Subsection "local($_) strips all magic from $_"

**local()** on scalar variables gives them a new value but keeps all
their magic intact.  This has proven problematic for the default
scalar variable \f(CW$_, where perlsub recommends that any subroutine
that assigns to \f(CW$_ should first localize it.  This would throw an
exception if \f(CW$_ is aliased to a read-only variable, and could in general have
various unintentional side-effects.

Therefore, as an exception to the general rule, local($_) will not
only assign a new value to \f(CW$_, but also remove all existing magic from
it as well.

*Parsing of package and variable names*
Subsection "Parsing of package and variable names"

Parsing the names of packages and package variables has changed:
multiple adjacent pairs of colons, as in \f(CW\*(C`foo::::bar\*(C', are now all
treated as package separators.

Regardless of this change, the exact parsing of package separators has
never been guaranteed and is subject to change in future Perl versions.

### Changes to Syntax or to Perl Operators

Subsection "Changes to Syntax or to Perl Operators"
*\f(CI\*(C`given\*(C'\fI return values*
Subsection "given return values"

\f(CW\*(C`given\*(C' blocks now return the last evaluated
expression, or an empty list if the block was exited by \f(CW\*(C`break\*(C'.  Thus you
can now write:

.Vb 8
    my $type = do \{
     given ($num) \{
      break     when undef;
      "integer" when /^[+-]?[0-9]+$/;
      "float"   when /^[+-]?[0-9]+(?:\\.[0-9]+)?$/;
      "unknown";
     \}
    \};
.Ve

See \*(L"Return value\*(R" in perlsyn for details.

*Change in parsing of certain prototypes*
Subsection "Change in parsing of certain prototypes"

Functions declared with the following prototypes now behave correctly as unary
functions:

.Vb 6
  *
  \\$ \\% \\@ \\* \
  \\[...]
  ;$ ;*
  ;\\$ ;\\% etc.
  ;\\[...]
.Ve

Due to this bug fix [perl #75904], functions
using the \f(CW\*(C`(*)\*(C', \f(CW\*(C`(;$)\*(C' and \f(CW\*(C`(;*)\*(C' prototypes
are parsed with higher precedence than before.  So
in the following example:

.Vb 2
  sub foo(;$);
  foo $a < $b;
.Ve

the second line is now parsed correctly as \f(CW\*(C`foo($a) < $b\*(C', rather than
\f(CW\*(C`foo($a < $b)\*(C'.  This happens when one of these operators is used in
an unparenthesised argument:

.Vb 10
  < > <= >= lt gt le ge
  == != <=> eq ne cmp ~~
  &
  | ^
  &&
  || //
  .. ...
  ?:
  = += -= *= etc.
  , =>
.Ve

*Smart-matching against array slices*
Subsection "Smart-matching against array slices"

Previously, the following code resulted in a successful match:

.Vb 3
    my @a = qw(a y0 z);
    my @b = qw(a x0 z);
    @a[0 .. $#b] ~~ @b;
.Ve

This odd behaviour has now been fixed [perl #77468].

*Negation treats strings differently from before*
Subsection "Negation treats strings differently from before"

The unary negation operator, \f(CW\*(C`-\*(C', now treats strings that look like numbers
as numbers [perl #57706].

*Negative zero*
Subsection "Negative zero"

Negative zero (-0.0), when converted to a string, now becomes \*(L"0\*(R" on all
platforms.  It used to become \*(L"-0\*(R" on some, but \*(L"0\*(R" on others.

If you still need to determine whether a zero is negative, use
\f(CW\*(C`sprintf("%g", $zero) =~ /^-/\*(C' or the Data::Float module on \s-1CPAN.\s0

*\f(CI\*(C`:=\*(C'\fI is now a syntax error*
Subsection ":= is now a syntax error"

Previously \f(CW\*(C`my $pi := 4\*(C' was exactly equivalent to \f(CW\*(C`my $pi : = 4\*(C',
with the \f(CW\*(C`:\*(C' being treated as the start of an attribute list, ending before
the \f(CW\*(C`=\*(C'.  The use of \f(CW\*(C`:=\*(C' to mean \f(CW\*(C`: =\*(C' was deprecated in 5.12.0, and is
now a syntax error.  This allows future use of \f(CW\*(C`:=\*(C' as a new token.

Outside the core's tests for it, we find no Perl 5 code on \s-1CPAN\s0
using this construction, so we believe that this change will have
little impact on real-world codebases.

If it is absolutely necessary to have empty attribute lists (for example,
because of a code generator), simply avoid the error by adding a space before
the \f(CW\*(C`=\*(C'.

*Change in the parsing of identifiers*
Subsection "Change in the parsing of identifiers"

Characters outside the Unicode \*(L"XIDStart\*(R" set are no longer allowed at the
beginning of an identifier.  This means that certain accents and marks
that normally follow an alphabetic character may no longer be the first
character of an identifier.

### Threads and Processes

Subsection "Threads and Processes"
*Directory handles not copied to threads*
Subsection "Directory handles not copied to threads"

On systems other than Windows that do not have
a \f(CW\*(C`fchdir\*(C' function, newly-created threads no
longer inherit directory handles from their parent threads.  Such programs
would usually have crashed anyway [perl #75154].

*\f(CI\*(C`close\*(C'\fI on shared pipes*
Subsection "close on shared pipes"

To avoid deadlocks, the \f(CW\*(C`close\*(C' function no longer waits for the
child process to exit if the underlying file descriptor is still
in use by another thread.  It returns true in such cases.

*\f(BIfork()\fI emulation will not wait for signalled children*
Subsection "fork() emulation will not wait for signalled children"

On Windows parent processes would not terminate until all forked
children had terminated first.  However, \f(CW\*(C`kill("KILL", ...)\*(C' is
inherently unstable on pseudo-processes, and \f(CW\*(C`kill("TERM", ...)\*(C'
might not get delivered if the child is blocked in a system call.

To avoid the deadlock and still provide a safe mechanism to terminate
the hosting process, Perl now no longer waits for children that
have been sent a \s-1SIGTERM\s0 signal.  It is up to the parent process to
**waitpid()** for these children if child-cleanup processing must be
allowed to finish.  However, it is also then the responsibility of the
parent to avoid the deadlock by making sure the child process
can't be blocked on I/O.

See perlfork for more information about the **fork()** emulation on
Windows.

### Configuration

Subsection "Configuration"
*Naming fixes in Policy_sh.SH may invalidate Policy.sh*
Subsection "Naming fixes in Policy_sh.SH may invalidate Policy.sh"

Several long-standing typos and naming confusions in *Policy_sh.SH* have
been fixed, standardizing on the variable names used in *config.sh*.

This will change the behaviour of *Policy.sh* if you happen to have been
accidentally relying on its incorrect behaviour.

*Perl source code is read in text mode on Windows*
Subsection "Perl source code is read in text mode on Windows"

Perl scripts used to be read in binary mode on Windows for the benefit
of the ByteLoader module (which is no longer part of core Perl).  This
had the side-effect of breaking various operations on the \f(CW\*(C`DATA\*(C' filehandle,
including **seek()**/**tell()**, and even simply reading from \f(CW\*(C`DATA\*(C' after filehandles
have been flushed by a call to **system()**, backticks, **fork()** etc.

The default build options for Windows have been changed to read Perl source
code on Windows in text mode now.  ByteLoader will (hopefully) be updated on
\s-1CPAN\s0 to automatically handle this situation [perl #28106].

## Deprecations

Header "Deprecations"
See also \*(L"Deprecated C APIs\*(R".

### Omitting a space between a regular expression and subsequent word

Subsection "Omitting a space between a regular expression and subsequent word"
Omitting the space between a regular expression operator or
its modifiers and the following word is deprecated.  For
example, \f(CW\*(C`m/foo/sand $bar\*(C' is for now still parsed
as \f(CW\*(C`m/foo/s and $bar\*(C', but will now issue a warning.
.ie n .SS """\\c\f(CIX"""
.el .SS "\f(CW\\c\f(CIX\f(CW"
Subsection "cX"
The backslash-c construct was designed as a way of specifying
non-printable characters, but there were no restrictions (on \s-1ASCII\s0
platforms) on what the character following the \f(CW\*(C`c\*(C' could be.  Now,
a deprecation warning is raised if that character isn't an \s-1ASCII\s0 character.
Also, a deprecation warning is raised for \f(CW"\\c\{" (which is the same
as simply saying \f(CW";").
.ie n .SS """\\b\{"" and ""\\B\{"""
.el .SS "\f(CW``\\b\{'' and \f(CW``\\B\{''"
Subsection """b\{"" and ""B\{"""
In regular expressions, a literal \f(CW"\{" immediately following a \f(CW"\\b"
(not in a bracketed character class) or a \f(CW"\\B\{" is now deprecated
to allow for its future use by Perl itself.

### Perl 4-era .pl libraries

Subsection "Perl 4-era .pl libraries"
Perl bundles a handful of library files that predate Perl 5.
This bundling is now deprecated for most of these files, which are now
available from \s-1CPAN.\s0  The affected files now warn when run, if they were
installed as part of the core.

This is a mandatory warning, not obeying **-X** or lexical warning bits.
The warning is modelled on that supplied by *deprecate.pm* for
deprecated-in-core *.pm* libraries.  It points to the specific \s-1CPAN\s0
distribution that contains the *.pl* libraries.  The \s-1CPAN\s0 versions, of
course, do not generate the warning.
.ie n .SS "List assignment to $["
.el .SS "List assignment to \f(CW$["
Subsection "List assignment to $["
Assignment to \f(CW$[ was deprecated and started to give warnings in
Perl version 5.12.0.  This version of Perl (5.14) now also emits a warning
when assigning to \f(CW$[ in list context.  This fixes an oversight in 5.12.0.

### Use of qw(...) as parentheses

Subsection "Use of qw(...) as parentheses"
Historically the parser fooled itself into thinking that \f(CW\*(C`qw(...)\*(C' literals
were always enclosed in parentheses, and as a result you could sometimes omit
parentheses around them:

.Vb 1
    for $x qw(a b c) \{ ... \}
.Ve

The parser no longer lies to itself in this way.  Wrap the list literal in
parentheses like this:

.Vb 1
    for $x (qw(a b c)) \{ ... \}
.Ve

This is being deprecated because the parentheses in \f(CW\*(C`for $i (1,2,3) \{ ... \}\*(C'
are not part of expression syntax.  They are part of the statement
syntax, with the \f(CW\*(C`for\*(C' statement wanting literal parentheses.
The synthetic parentheses that a \f(CW\*(C`qw\*(C' expression acquired were only
intended to be treated as part of expression syntax.

Note that this does not change the behaviour of cases like:

.Vb 2
    use POSIX qw(setlocale localeconv);
    our @EXPORT = qw(foo bar baz);
.Ve

where parentheses were never required around the expression.
.ie n .SS """\\N\{BELL\}"""
.el .SS "\f(CW\\N\{BELL\}"
Subsection "N\{BELL\}"
This is because Unicode is using that name for a different character.
See \*(L"Unicode Version 6.0 is now supported (mostly)\*(R" for more
explanation.
.ie n .SS """?PATTERN?"""
.el .SS "\f(CW?PATTERN?"
Subsection "?PATTERN?"
\f(CW\*(C`?PATTERN?\*(C' (without the initial \f(CW\*(C`m\*(C') has been deprecated and now produces
a warning.  This is to allow future use of \f(CW\*(C`?\*(C' in new operators.
The match-once functionality is still available as \f(CW\*(C`m?PATTERN?\*(C'.

### Tie functions on scalars holding typeglobs

Subsection "Tie functions on scalars holding typeglobs"
Calling a tie function (\f(CW\*(C`tie\*(C', \f(CW\*(C`tied\*(C', \f(CW\*(C`untie\*(C') with a scalar argument
acts on a filehandle if the scalar happens to hold a typeglob.

This is a long-standing bug that will be removed in Perl 5.16, as
there is currently no way to tie the scalar itself when it holds
a typeglob, and no way to untie a scalar that has had a typeglob
assigned to it.

Now there is a deprecation warning whenever a tie
function is used on a handle without an explicit \f(CW\*(C`*\*(C'.

### User-defined case-mapping

Subsection "User-defined case-mapping"
This feature is being deprecated due to its many issues, as documented in
\*(L"User-Defined Case Mappings (for serious hackers only)\*(R" in perlunicode.
This feature will be removed in Perl 5.16.  Instead use the \s-1CPAN\s0 module
Unicode::Casing, which provides improved functionality.

### Deprecated modules

Subsection "Deprecated modules"
The following module will be removed from the core distribution in a
future release, and should be installed from \s-1CPAN\s0 instead.  Distributions
on \s-1CPAN\s0 that require this should add it to their prerequisites.  The
core version of these module now issues a deprecation warning.

If you ship a packaged version of Perl, either alone or as part of a
larger system, then you should carefully consider the repercussions of
core module deprecations.  You may want to consider shipping your default
build of Perl with a package for the deprecated module that
installs into \f(CW\*(C`vendor\*(C' or \f(CW\*(C`site\*(C' Perl library directories.  This will
inhibit the deprecation warnings.

Alternatively, you may want to consider patching *lib/deprecate.pm*
to provide deprecation warnings specific to your packaging system
or distribution of Perl, consistent with how your packaging system
or distribution manages a staged transition from a release where the
installation of a single package provides the given functionality, to
a later release where the system administrator needs to know to install
multiple packages to get that same functionality.

You can silence these deprecation warnings by installing the module
in question from \s-1CPAN.\s0  To install the latest version of it by role
rather than by name, just install \f(CW\*(C`Task::Deprecations::5_14\*(C'.

- Devel::DProf
Item "Devel::DProf"
We strongly recommend that you install and use Devel::NYTProf instead
of Devel::DProf, as Devel::NYTProf offers significantly
improved profiling and reporting.

## Performance Enhancements

Header "Performance Enhancements"
.ie n .SS """Safe signals"" optimisation"
.el .SS "``Safe signals'' optimisation"
Subsection "Safe signals optimisation"
Signal dispatch has been moved from the runloop into control ops.
This should give a few percent speed increase, and eliminates nearly
all the speed penalty caused by the introduction of \*(L"safe signals\*(R"
in 5.8.0.  Signals should still be dispatched within the same
statement as they were previously.  If this does *not* happen, or
if you find it possible to create uninterruptible loops, this is a
bug, and reports are encouraged of how to recreate such issues.

### Optimisation of \fBshift() and \fBpop() calls without arguments

Subsection "Optimisation of shift() and pop() calls without arguments"
Two fewer OPs are used for **shift()** and **pop()** calls with no argument (with
implicit \f(CW@_).  This change makes **shift()** 5% faster than \f(CW\*(C`shift @_\*(C'
on non-threaded perls, and 25% faster on threaded ones.

### Optimisation of regexp engine string comparison work

Subsection "Optimisation of regexp engine string comparison work"
The \f(CW\*(C`foldEQ_utf8\*(C' \s-1API\s0 function for case-insensitive comparison of strings (which
is used heavily by the regexp engine) was substantially refactored and
optimised \*(-- and its documentation much improved as a free bonus.

### Regular expression compilation speed-up

Subsection "Regular expression compilation speed-up"
Compiling regular expressions has been made faster when upgrading
the regex to utf8 is necessary but this isn't known when the compilation begins.

### String appending is 100 times faster

Subsection "String appending is 100 times faster"
When doing a lot of string appending, perls built to use the system's
\f(CW\*(C`malloc\*(C' could end up allocating a lot more memory than needed in a
inefficient way.

\f(CW\*(C`sv_grow\*(C', the function used to allocate more memory if necessary
when appending to a string, has been taught to round up the memory
it requests to a certain geometric progression, making it much faster on
certain platforms and configurations.  On Win32, it's now about 100 times
faster.
.ie n .SS "Eliminate ""PL_*"" accessor functions under ithreads"
.el .SS "Eliminate \f(CWPL_* accessor functions under ithreads"
Subsection "Eliminate PL_* accessor functions under ithreads"
When \f(CW\*(C`MULTIPLICITY\*(C' was first developed, and interpreter state moved into
an interpreter struct, thread- and interpreter-local \f(CW\*(C`PL_*\*(C' variables
were defined as macros that called accessor functions (returning the
address of the value) outside the Perl core.  The intent was to allow
members within the interpreter struct to change size without breaking
binary compatibility, so that bug fixes could be merged to a maintenance
branch that necessitated such a size change.  This mechanism was redundant
and penalised well-behaved code.  It has been removed.

### Freeing weak references

Subsection "Freeing weak references"
When there are many weak references to an object, freeing that object
can under some circumstances take O(*N*N*) time to free, where
*N* is the number of references.  The circumstances in which this can happen
have been reduced [perl #75254]

### Lexical array and hash assignments

Subsection "Lexical array and hash assignments"
An earlier optimisation to speed up \f(CW\*(C`my @array = ...\*(C' and
\f(CW\*(C`my %hash = ...\*(C' assignments caused a bug and was disabled in Perl 5.12.0.

Now we have found another way to speed up these assignments [perl #82110].
.ie n .SS "@_ uses less memory"
.el .SS "\f(CW@_ uses less memory"
Subsection "@_ uses less memory"
Previously, \f(CW@_ was allocated for every subroutine at compile time with
enough space for four entries.  Now this allocation is done on demand when
the subroutine is called [perl #72416].

### Size optimisations to \s-1SV\s0 and \s-1HV\s0 structures

Subsection "Size optimisations to SV and HV structures"
\f(CW\*(C`xhv_fill\*(C' has been eliminated from \f(CW\*(C`struct xpvhv\*(C', saving 1 \s-1IV\s0 per hash and
on some systems will cause \f(CW\*(C`struct xpvhv\*(C' to become cache-aligned.  To avoid
this memory saving causing a slowdown elsewhere, boolean use of \f(CW\*(C`HvFILL\*(C'
now calls \f(CW\*(C`HvTOTALKEYS\*(C' instead (which is equivalent), so while the fill
data when actually required are now calculated on demand, cases when
this needs to be done should be rare.

The order of structure elements in \s-1SV\s0 bodies has changed.  Effectively,
the \s-1NV\s0 slot has swapped location with \s-1STASH\s0 and \s-1MAGIC.\s0  As all access to
\s-1SV\s0 members is via macros, this should be completely transparent.  This
change allows the space saving for PVHVs documented above, and may reduce
the memory allocation needed for PVIVs on some architectures.

\f(CW\*(C`XPV\*(C', \f(CW\*(C`XPVIV\*(C', and \f(CW\*(C`XPVNV\*(C' now allocate only the parts of the \f(CW\*(C`SV\*(C' body
they actually use, saving some space.

Scalars containing regular expressions now allocate only the part of the \f(CW\*(C`SV\*(C'
body they actually use, saving some space.

### Memory consumption improvements to Exporter

Subsection "Memory consumption improvements to Exporter"
The \f(CW@EXPORT_FAIL \s-1AV\s0 is no longer created unless needed, hence neither is
the typeglob backing it.  This saves about 200 bytes for every package that
uses Exporter but doesn't use this functionality.

### Memory savings for weak references

Subsection "Memory savings for weak references"
For weak references, the common case of just a single weak reference
per referent has been optimised to reduce the storage required.  In this
case it saves the equivalent of one small Perl array per referent.
.ie n .SS """%+"" and ""%-"" use less memory"
.el .SS "\f(CW%+ and \f(CW%- use less memory"
Subsection "%+ and %- use less memory"
The bulk of the \f(CW\*(C`Tie::Hash::NamedCapture\*(C' module used to be in the Perl
core.  It has now been moved to an \s-1XS\s0 module to reduce overhead for
programs that do not use \f(CW\*(C`%+\*(C' or \f(CW\*(C`%-\*(C'.

### Multiple small improvements to threads

Subsection "Multiple small improvements to threads"
The internal structures of threading now make fewer \s-1API\s0 calls and fewer
allocations, resulting in noticeably smaller object code.  Additionally,
many thread context checks have been deferred so they're done only
as needed (although this is only possible for non-debugging builds).

### Adjacent pairs of nextstate opcodes are now optimized away

Subsection "Adjacent pairs of nextstate opcodes are now optimized away"
Previously, in code such as

.Vb 1
    use constant DEBUG => 0;

    sub GAK \{
        warn if DEBUG;
        print "stuff\\n";
    \}
.Ve

the ops for \f(CW\*(C`warn if DEBUG\*(C' would be folded to a \f(CW\*(C`null\*(C' op (\f(CW\*(C`ex-const\*(C'), but
the \f(CW\*(C`nextstate\*(C' op would remain, resulting in a runtime op dispatch of
\f(CW\*(C`nextstate\*(C', \f(CW\*(C`nextstate\*(C', etc.

The execution of a sequence of \f(CW\*(C`nextstate\*(C' ops is indistinguishable from just
the last \f(CW\*(C`nextstate\*(C' op so the peephole optimizer now eliminates the first of
a pair of \f(CW\*(C`nextstate\*(C' ops except when the first carries a label, since labels
must not be eliminated by the optimizer, and label usage isn't conclusively known
at compile time.

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules and Pragmata

Subsection "New Modules and Pragmata"

- \(bu
CPAN::Meta::YAML 0.003 has been added as a dual-life module.  It supports a
subset of \s-1YAML\s0 sufficient for reading and writing *\s-1META\s0.yml* and *\s-1MYMETA\s0.yml* files
included with \s-1CPAN\s0 distributions or generated by the module installation
toolchain.  It should not be used for any other general \s-1YAML\s0 parsing or
generation task.

- \(bu
CPAN::Meta version 2.110440 has been added as a dual-life module.  It
provides a standard library to read, interpret and write \s-1CPAN\s0 distribution
metadata files (like *\s-1META\s0.json* and *\s-1META\s0.yml*) that describe a
distribution, its contents, and the requirements for building it and
installing it.  The latest \s-1CPAN\s0 distribution metadata specification is
included as CPAN::Meta::Spec and notes on changes in the specification
over time are given in CPAN::Meta::History.

- \(bu
HTTP::Tiny 0.012 has been added as a dual-life module.  It is a very
small, simple \s-1HTTP/1.1\s0 client designed for simple \s-1GET\s0 requests and file
mirroring.  It has been added so that *\s-1CPAN\s0.pm* and \s-1CPANPLUS\s0 can
\*(L"bootstrap\*(R" \s-1HTTP\s0 access to \s-1CPAN\s0 using pure Perl without relying on external
binaries like **curl**\|(1) or **wget**\|(1).

- \(bu
\s-1JSON::PP\s0 2.27105 has been added as a dual-life module to allow \s-1CPAN\s0
clients to read *\s-1META\s0.json* files in \s-1CPAN\s0 distributions.

- \(bu
Module::Metadata 1.000004 has been added as a dual-life module.  It gathers
package and \s-1POD\s0 information from Perl module files.  It is a standalone module
based on Module::Build::ModuleInfo for use by other module installation
toolchain components.  Module::Build::ModuleInfo has been deprecated in
favor of this module instead.

- \(bu
Perl::OSType 1.002 has been added as a dual-life module.  It maps Perl
operating system names (like \*(L"dragonfly\*(R" or \*(L"MSWin32\*(R") to more generic types
with standardized names (like \*(L"Unix\*(R" or \*(L"Windows\*(R").  It has been refactored
out of Module::Build and ExtUtils::CBuilder and consolidates such mappings into
a single location for easier maintenance.

- \(bu
The following modules were added by the Unicode::Collate
upgrade.  See below for details.
.Sp
Unicode::Collate::CJK::Big5
.Sp
Unicode::Collate::CJK::GB2312
.Sp
Unicode::Collate::CJK::JISX0208
.Sp
Unicode::Collate::CJK::Korean
.Sp
Unicode::Collate::CJK::Pinyin
.Sp
Unicode::Collate::CJK::Stroke

- \(bu
Version::Requirements version 0.101020 has been added as a dual-life
module.  It provides a standard library to model and manipulates module
prerequisites and version constraints defined in CPAN::Meta::Spec.

### Updated Modules and Pragma

Subsection "Updated Modules and Pragma"

- \(bu
attributes has been upgraded from version 0.12 to 0.14.

- \(bu
Archive::Extract has been upgraded from version 0.38 to 0.48.
.Sp
Updates since 0.38 include: a safe print method that guards
Archive::Extract from changes to \f(CW\*(C`$\\\*(C'; a fix to the tests when run in core
Perl; support for \s-1TZ\s0 files; a modification for the lzma
logic to favour IO::Uncompress::Unlzma; and a fix
for an issue with NetBSD-current and its new **unzip**\|(1)
executable.

- \(bu
Archive::Tar has been upgraded from version 1.54 to 1.76.
.Sp
Important changes since 1.54 include the following:

> 
- \(bu
Compatibility with busybox implementations of **tar**\|(1).

- \(bu
A fix so that **write()** and **create_archive()**
close only filehandles they themselves opened.

- \(bu
A bug was fixed regarding the exit code of extract_archive.

- \(bu
The **ptar**\|(1) utility has a new option to allow safe creation of
tarballs without world-writable files on Windows, allowing those
archives to be uploaded to \s-1CPAN.\s0

- \(bu
A new **ptargrep**\|(1) utility for using regular expressions against
the contents of files in a tar archive.

- \(bu
pax extended headers are now skipped.



> 


- \(bu
Attribute::Handlers has been upgraded from version 0.87 to 0.89.

- \(bu
autodie has been upgraded from version 2.06_01 to 2.1001.

- \(bu
AutoLoader has been upgraded from version 5.70 to 5.71.

- \(bu
The B module has been upgraded from version 1.23 to 1.29.
.Sp
It no longer crashes when taking apart a \f(CW\*(C`y///\*(C' containing characters
outside the octet range or compiled in a \f(CW\*(C`use utf8\*(C' scope.
.Sp
The size of the shared object has been reduced by about 40%, with no
reduction in functionality.

- \(bu
B::Concise has been upgraded from version 0.78 to 0.83.
.Sp
B::Concise marks **rv2sv()**, **rv2av()**, and **rv2hv()** ops with the new
\f(CW\*(C`OPpDEREF\*(C' flag as \*(L"DREFed\*(R".
.Sp
It no longer produces mangled output with the **-tree** option
[perl #80632].

- \(bu
B::Debug has been upgraded from version 1.12 to 1.16.

- \(bu
B::Deparse has been upgraded from version 0.96 to 1.03.
.Sp
The deparsing of a \f(CW\*(C`nextstate\*(C' op has changed when it has both a
change of package relative to the previous nextstate, or a change of
\f(CW\*(C`%^H\*(C' or other state and a label.  The label was previously emitted
first, but is now emitted last (5.12.1).
.Sp
The \f(CW\*(C`no 5.13.2\*(C' or similar form is now correctly handled by B::Deparse
(5.12.3).
.Sp
B::Deparse now properly handles the code that applies a conditional
pattern match against implicit \f(CW$_ as it was fixed in [perl #20444].
.Sp
Deparsing of \f(CW\*(C`our\*(C' followed by a variable with funny characters
(as permitted under the \f(CW\*(C`use utf8\*(C' pragma) has also been fixed [perl #33752].

- \(bu
B::Lint has been upgraded from version 1.11_01 to 1.13.

- \(bu
base has been upgraded from version 2.15 to 2.16.

- \(bu
Benchmark has been upgraded from version 1.11 to 1.12.

- \(bu
bignum has been upgraded from version 0.23 to 0.27.

- \(bu
Carp has been upgraded from version 1.15 to 1.20.
.Sp
Carp now detects incomplete **caller()**
overrides and avoids using bogus \f(CW@DB::args.  To provide backtraces,
Carp relies on particular behaviour of the **caller()** builtin.
Carp now detects if other code has overridden this with an
incomplete implementation, and modifies its backtrace accordingly.
Previously incomplete overrides would cause incorrect values in
backtraces (best case), or obscure fatal errors (worst case).
.Sp
This fixes certain cases of \*(L"Bizarre copy of \s-1ARRAY\*(R"\s0 caused by modules
overriding **caller()** incorrectly (5.12.2).
.Sp
It now also avoids using regular expressions that cause Perl to
load its Unicode tables, so as to avoid the \*(L"\s-1BEGIN\s0 not safe after
errors\*(R" error that ensue if there has been a syntax error
[perl #82854].

- \(bu
\s-1CGI\s0 has been upgraded from version 3.48 to 3.52.
.Sp
This provides the following security fixes: the \s-1MIME\s0 boundary in
**multipart_init()** is now random and the handling of
newlines embedded in header values has been improved.

- \(bu
Compress::Raw::Bzip2 has been upgraded from version 2.024 to 2.033.
.Sp
It has been updated to use **bzip2**\|(1) 1.0.6.

- \(bu
Compress::Raw::Zlib has been upgraded from version 2.024 to 2.033.

- \(bu
constant has been upgraded from version 1.20 to 1.21.
.Sp
Unicode constants work once more.  They have been broken since Perl 5.10.0
[\s-1CPAN RT\s0 #67525].

- \(bu
\s-1CPAN\s0 has been upgraded from version 1.94_56 to 1.9600.
.Sp
Major highlights:

> 
- \(bu
much less configuration dialog hassle

- \(bu
support for *\s-1META/MYMETA\s0.json*

- \(bu
support for local::lib

- \(bu
support for HTTP::Tiny to reduce the dependency on \s-1FTP\s0 sites

- \(bu
automatic mirror selection

- \(bu
iron out all known bugs in configure_requires

- \(bu
support for distributions compressed with **bzip2**\|(1)

- \(bu
allow *Foo/Bar.pm* on the command line to mean \f(CW\*(C`Foo::Bar\*(C'



> 


- \(bu
\s-1CPANPLUS\s0 has been upgraded from version 0.90 to 0.9103.
.Sp
A change to *cpanp-run-perl*
resolves \s-1RT\s0 #55964 <http://rt.cpan.org/Public/Bug/Display.html?id=55964>
and \s-1RT\s0 #57106 <http://rt.cpan.org/Public/Bug/Display.html?id=57106>, both
of which related to failures to install distributions that use
\f(CW\*(C`Module::Install::DSL\*(C' (5.12.2).
.Sp
A dependency on Config was not recognised as a
core module dependency.  This has been fixed.
.Sp
\s-1CPANPLUS\s0 now includes support for *\s-1META\s0.json* and *\s-1MYMETA\s0.json*.

- \(bu
CPANPLUS::Dist::Build has been upgraded from version 0.46 to 0.54.

- \(bu
Data::Dumper has been upgraded from version 2.125 to 2.130_02.
.Sp
The indentation used to be off when \f(CW$Data::Dumper::Terse was set.  This
has been fixed [perl #73604].
.Sp
This upgrade also fixes a crash when using custom sort functions that might
cause the stack to change [perl #74170].
.Sp
Dumpxs no longer crashes with globs returned by \f(CW*$io_ref
[perl #72332].

- \(bu
DB_File has been upgraded from version 1.820 to 1.821.

- \(bu
DBM_Filter has been upgraded from version 0.03 to 0.04.

- \(bu
Devel::DProf has been upgraded from version 20080331.00 to 20110228.00.
.Sp
Merely loading Devel::DProf now no longer triggers profiling to start.
Both \f(CW\*(C`use Devel::DProf\*(C' and \f(CW\*(C`perl -d:DProf ...\*(C' behave as before and start
the profiler.
.Sp
**\s-1NOTE\s0**: Devel::DProf is deprecated and will be removed from a future
version of Perl.  We strongly recommend that you install and use
Devel::NYTProf instead, as it offers significantly improved
profiling and reporting.

- \(bu
Devel::Peek has been upgraded from version 1.04 to 1.07.

- \(bu
Devel::SelfStubber has been upgraded from version 1.03 to 1.05.

- \(bu
diagnostics has been upgraded from version 1.19 to 1.22.
.Sp
It now renders pod links slightly better, and has been taught to find
descriptions for messages that share their descriptions with other
messages.

- \(bu
Digest::MD5 has been upgraded from version 2.39 to 2.51.
.Sp
It is now safe to use this module in combination with threads.

- \(bu
Digest::SHA has been upgraded from version 5.47 to 5.61.
.Sp
\f(CW\*(C`shasum\*(C' now more closely mimics **sha1sum**\|(1)/**md5sum**\|(1).
.Sp
\f(CW\*(C`addfile\*(C' accepts all \s-1POSIX\s0 filenames.
.Sp
New \s-1SHA-512/224\s0 and \s-1SHA-512/256\s0 transforms (ref. \s-1NIST\s0 Draft \s-1FIPS 180-4\s0
[February 2011])

- \(bu
DirHandle has been upgraded from version 1.03 to 1.04.

- \(bu
Dumpvalue has been upgraded from version 1.13 to 1.16.

- \(bu
DynaLoader has been upgraded from version 1.10 to 1.13.
.Sp
It fixes a buffer overflow when passed a very long file name.
.Sp
It no longer inherits from AutoLoader; hence it no longer
produces weird error messages for unsuccessful method calls on classes that
inherit from DynaLoader [perl #84358].

- \(bu
Encode has been upgraded from version 2.39 to 2.42.
.Sp
Now, all 66 Unicode non-characters are treated the same way U+FFFF has
always been treated: in cases when it was disallowed, all 66 are
disallowed, and in cases where it warned, all 66 warn.

- \(bu
Env has been upgraded from version 1.01 to 1.02.

- \(bu
Errno has been upgraded from version 1.11 to 1.13.
.Sp
The implementation of Errno has been refactored to use about 55% less memory.
.Sp
On some platforms with unusual header files, like Win32 **gcc**\|(1) using \f(CW\*(C`mingw64\*(C'
headers, some constants that weren't actually error numbers have been exposed
by Errno.  This has been fixed [perl #77416].

- \(bu
Exporter has been upgraded from version 5.64_01 to 5.64_03.
.Sp
Exporter no longer overrides \f(CW$SIG\{_\|_WARN_\|_\} [perl #74472]

- \(bu
ExtUtils::CBuilder has been upgraded from version 0.27 to 0.280203.

- \(bu
ExtUtils::Command has been upgraded from version 1.16 to 1.17.

- \(bu
ExtUtils::Constant has been upgraded from 0.22 to 0.23.
.Sp
The \s-1AUTOLOAD\s0 helper code generated by \f(CW\*(C`ExtUtils::Constant::ProxySubs\*(C'
can now **croak()** for missing constants, or generate a complete \f(CW\*(C`AUTOLOAD\*(C'
subroutine in \s-1XS,\s0 allowing simplification of many modules that use it
(Fcntl, File::Glob, GDBM_File, I18N::Langinfo, \s-1POSIX\s0,
Socket).
.Sp
ExtUtils::Constant::ProxySubs can now optionally push the names of all
constants onto the package's \f(CW@EXPORT_OK.

- \(bu
ExtUtils::Install has been upgraded from version 1.55 to 1.56.

- \(bu
ExtUtils::MakeMaker has been upgraded from version 6.56 to 6.57_05.

- \(bu
ExtUtils::Manifest has been upgraded from version 1.57 to 1.58.

- \(bu
ExtUtils::ParseXS has been upgraded from version 2.21 to 2.2210.

- \(bu
Fcntl has been upgraded from version 1.06 to 1.11.

- \(bu
File::Basename has been upgraded from version 2.78 to 2.82.

- \(bu
File::CheckTree has been upgraded from version 4.4 to 4.41.

- \(bu
File::Copy has been upgraded from version 2.17 to 2.21.

- \(bu
File::DosGlob has been upgraded from version 1.01 to 1.04.
.Sp
It allows patterns containing literal parentheses: they no longer need to
be escaped.  On Windows, it no longer
adds an extra *./* to file names
returned when the pattern is a relative glob with a drive specification,
like *C:*.pl* [perl #71712].

- \(bu
File::Fetch has been upgraded from version 0.24 to 0.32.
.Sp
HTTP::Lite is now supported for the \*(L"http\*(R" scheme.
.Sp
The **fetch**\|(1) utility is supported on FreeBSD, NetBSD, and
Dragonfly \s-1BSD\s0 for the \f(CW\*(C`http\*(C' and \f(CW\*(C`ftp\*(C' schemes.

- \(bu
File::Find has been upgraded from version 1.15 to 1.19.
.Sp
It improves handling of backslashes on Windows, so that paths like
*C:\\dir\\/file* are no longer generated [perl #71710].

- \(bu
File::Glob has been upgraded from version 1.07 to 1.12.

- \(bu
File::Spec has been upgraded from version 3.31 to 3.33.
.Sp
Several portability fixes were made in File::Spec::VMS: a colon is now
recognized as a delimiter in native filespecs; caret-escaped delimiters are
recognized for better handling of extended filespecs; **catpath()** returns
an empty directory rather than the current directory if the input directory
name is empty; and **abs2rel()** properly handles Unix-style input (5.12.2).

- \(bu
File::stat has been upgraded from 1.02 to 1.05.
.Sp
The \f(CW\*(C`-x\*(C' and \f(CW\*(C`-X\*(C' file test operators now work correctly when run
by the superuser.

- \(bu
Filter::Simple has been upgraded from version 0.84 to 0.86.

- \(bu
GDBM_File has been upgraded from 1.10 to 1.14.
.Sp
This fixes a memory leak when \s-1DBM\s0 filters are used.

- \(bu
Hash::Util has been upgraded from 0.07 to 0.11.
.Sp
Hash::Util no longer emits spurious \*(L"uninitialized\*(R" warnings when
recursively locking hashes that have undefined values [perl #74280].

- \(bu
Hash::Util::FieldHash has been upgraded from version 1.04 to 1.09.

- \(bu
I18N::Collate has been upgraded from version 1.01 to 1.02.

- \(bu
I18N::Langinfo has been upgraded from version 0.03 to 0.08.
.Sp
**langinfo()** now defaults to using \f(CW$_ if there is no argument given, just
as the documentation has always claimed.

- \(bu
I18N::LangTags has been upgraded from version 0.35 to 0.35_01.

- \(bu
if has been upgraded from version 0.05 to 0.0601.

- \(bu
\s-1IO\s0 has been upgraded from version 1.25_02 to 1.25_04.
.Sp
This version of \s-1IO\s0 includes a new IO::Select, which now allows IO::Handle
objects (and objects in derived classes) to be removed from an IO::Select set
even if the underlying file descriptor is closed or invalid.

- \(bu
IPC::Cmd has been upgraded from version 0.54 to 0.70.
.Sp
Resolves an issue with splitting Win32 command lines.  An argument
consisting of the single character \*(L"0\*(R" used to be omitted (\s-1CPAN RT\s0 #62961).

- \(bu
IPC::Open3 has been upgraded from 1.05 to 1.09.
.Sp
**open3()** now produces an error if the \f(CW\*(C`exec\*(C' call fails, allowing this
condition to be distinguished from a child process that exited with a
non-zero status [perl #72016].
.Sp
The internal **xclose()** routine now knows how to handle file descriptors as
documented, so duplicating \f(CW\*(C`STDIN\*(C' in a child process using its file
descriptor now works [perl #76474].

- \(bu
IPC::SysV has been upgraded from version 2.01 to 2.03.

- \(bu
lib has been upgraded from version 0.62 to 0.63.

- \(bu
Locale::Maketext has been upgraded from version 1.14 to 1.19.
.Sp
Locale::Maketext now supports external caches.
.Sp
This upgrade also fixes an infinite loop in
\f(CW\*(C`Locale::Maketext::Guts::_compile()\*(C' when
working with tainted values (\s-1CPAN RT\s0 #40727).
.Sp
\f(CW\*(C`->maketext\*(C' calls now back up and restore \f(CW$@ so error
messages are not suppressed (\s-1CPAN RT\s0 #34182).

- \(bu
Log::Message has been upgraded from version 0.02 to 0.04.

- \(bu
Log::Message::Simple has been upgraded from version 0.06 to 0.08.

- \(bu
Math::BigInt has been upgraded from version 1.89_01 to 1.994.
.Sp
This fixes, among other things, incorrect results when computing binomial
coefficients [perl #77640].
.Sp
It also prevents \f(CW\*(C`sqrt($int)\*(C' from crashing under \f(CW\*(C`use bigrat\*(C'.
[perl #73534].

- \(bu
Math::BigInt::FastCalc has been upgraded from version 0.19 to 0.28.

- \(bu
Math::BigRat has been upgraded from version 0.24 to 0.26_02.

- \(bu
Memoize has been upgraded from version 1.01_03 to 1.02.

- \(bu
MIME::Base64 has been upgraded from 3.08 to 3.13.
.Sp
Includes new functions to calculate the length of encoded and decoded
base64 strings.
.Sp
Now provides **encode_base64url()** and **decode_base64url()** functions to process
the base64 scheme for \*(L"\s-1URL\s0 applications\*(R".

- \(bu
Module::Build has been upgraded from version 0.3603 to 0.3800.
.Sp
A notable change is the deprecation of several modules.
Module::Build::Version has been deprecated and Module::Build now
relies on the version pragma directly.  Module::Build::ModuleInfo has
been deprecated in favor of a standalone copy called Module::Metadata.
Module::Build::YAML has been deprecated in favor of CPAN::Meta::YAML.
.Sp
Module::Build now also generates *\s-1META\s0.json* and *\s-1MYMETA\s0.json* files
in accordance with version 2 of the \s-1CPAN\s0 distribution metadata specification,
CPAN::Meta::Spec.  The older format *\s-1META\s0.yml* and *\s-1MYMETA\s0.yml* files are
still generated.

- \(bu
Module::CoreList has been upgraded from version 2.29 to 2.47.
.Sp
Besides listing the updated core modules of this release, it also stops listing
the \f(CW\*(C`Filespec\*(C' module.  That module never existed in core.  The scripts
generating Module::CoreList confused it with VMS::Filespec, which actually
is a core module as of Perl 5.8.7.

- \(bu
Module::Load has been upgraded from version 0.16 to 0.18.

- \(bu
Module::Load::Conditional has been upgraded from version 0.34 to 0.44.

- \(bu
The mro pragma has been upgraded from version 1.02 to 1.07.

- \(bu
NDBM_File has been upgraded from version 1.08 to 1.12.
.Sp
This fixes a memory leak when \s-1DBM\s0 filters are used.

- \(bu
Net::Ping has been upgraded from version 2.36 to 2.38.

- \(bu
\s-1NEXT\s0 has been upgraded from version 0.64 to 0.65.

- \(bu
Object::Accessor has been upgraded from version 0.36 to 0.38.

- \(bu
ODBM_File has been upgraded from version 1.07 to 1.10.
.Sp
This fixes a memory leak when \s-1DBM\s0 filters are used.

- \(bu
Opcode has been upgraded from version 1.15 to 1.18.

- \(bu
The overload pragma has been upgraded from 1.10 to 1.13.
.Sp
\f(CW\*(C`overload::Method\*(C' can now handle subroutines that are themselves blessed
into overloaded classes [perl #71998].
.Sp
The documentation has greatly improved.  See \*(L"Documentation\*(R" below.

- \(bu
Params::Check has been upgraded from version 0.26 to 0.28.

- \(bu
The parent pragma has been upgraded from version 0.223 to 0.225.

- \(bu
Parse::CPAN::Meta has been upgraded from version 1.40 to 1.4401.
.Sp
The latest Parse::CPAN::Meta can now read \s-1YAML\s0 and \s-1JSON\s0 files using
CPAN::Meta::YAML and \s-1JSON::PP\s0, which are now part of the Perl core.

- \(bu
PerlIO::encoding has been upgraded from version 0.12 to 0.14.

- \(bu
PerlIO::scalar has been upgraded from 0.07 to 0.11.
.Sp
A **read()** after a **seek()** beyond the end of the string no longer thinks it
has data to read [perl #78716].

- \(bu
PerlIO::via has been upgraded from version 0.09 to 0.11.

- \(bu
Pod::Html has been upgraded from version 1.09 to 1.11.

- \(bu
Pod::LaTeX has been upgraded from version 0.58 to 0.59.

- \(bu
Pod::Perldoc has been upgraded from version 3.15_02 to 3.15_03.

- \(bu
Pod::Simple has been upgraded from version 3.13 to 3.16.

- \(bu
\s-1POSIX\s0 has been upgraded from 1.19 to 1.24.
.Sp
It now includes constants for \s-1POSIX\s0 signal constants.

- \(bu
The re pragma has been upgraded from version 0.11 to 0.18.
.Sp
The \f(CW\*(C`use re \*(Aq/flags\*(Aq\*(C' subpragma is new.
.Sp
The **regmust()** function used to crash when called on a regular expression
belonging to a pluggable engine.  Now it croaks instead.
.Sp
**regmust()** no longer leaks memory.

- \(bu
Safe has been upgraded from version 2.25 to 2.29.
.Sp
Coderefs returned by **reval()** and **rdo()** are now wrapped via
**wrap_code_refs()** (5.12.1).
.Sp
This fixes a possible infinite loop when looking for coderefs.
.Sp
It adds several \f(CW\*(C`version::vxs::*\*(C' routines to the default share.

- \(bu
SDBM_File has been upgraded from version 1.06 to 1.09.

- \(bu
SelfLoader has been upgraded from 1.17 to 1.18.
.Sp
It now works in taint mode [perl #72062].

- \(bu
The sigtrap pragma has been upgraded from version 1.04 to 1.05.
.Sp
It no longer tries to modify read-only arguments when generating a
backtrace [perl #72340].

- \(bu
Socket has been upgraded from version 1.87 to 1.94.
.Sp
See \*(L"Improved IPv6 support\*(R" above.

- \(bu
Storable has been upgraded from version 2.22 to 2.27.
.Sp
Includes performance improvement for overloaded classes.
.Sp
This adds support for serialising code references that contain \s-1UTF-8\s0 strings
correctly.  The Storable minor version
number changed as a result, meaning that
Storable users who set \f(CW$Storable::accept_future_minor to a \f(CW\*(C`FALSE\*(C' value
will see errors (see \*(L"\s-1FORWARD COMPATIBILITY\*(R"\s0 in Storable for more details).
.Sp
Freezing no longer gets confused if the Perl stack gets reallocated
during freezing [perl #80074].

- \(bu
Sys::Hostname has been upgraded from version 1.11 to 1.16.

- \(bu
Term::ANSIColor has been upgraded from version 2.02 to 3.00.

- \(bu
Term::UI has been upgraded from version 0.20 to 0.26.

- \(bu
Test::Harness has been upgraded from version 3.17 to 3.23.

- \(bu
Test::Simple has been upgraded from version 0.94 to 0.98.
.Sp
Among many other things, subtests without a \f(CW\*(C`plan\*(C' or \f(CW\*(C`no_plan\*(C' now have an
implicit **done_testing()** added to them.

- \(bu
Thread::Semaphore has been upgraded from version 2.09 to 2.12.
.Sp
It provides two new methods that give more control over the decrementing of
semaphores: \f(CW\*(C`down_nb\*(C' and \f(CW\*(C`down_force\*(C'.

- \(bu
Thread::Queue has been upgraded from version 2.11 to 2.12.

- \(bu
The threads pragma has been upgraded from version 1.75 to 1.83.

- \(bu
The threads::shared pragma has been upgraded from version 1.32 to 1.37.

- \(bu
Tie::Hash has been upgraded from version 1.03 to 1.04.
.Sp
Calling \f(CW\*(C`Tie::Hash->TIEHASH()\*(C' used to loop forever.  Now it \f(CW\*(C`croak\*(C's.

- \(bu
Tie::Hash::NamedCapture has been upgraded from version 0.06 to 0.08.

- \(bu
Tie::RefHash has been upgraded from version 1.38 to 1.39.

- \(bu
Time::HiRes has been upgraded from version 1.9719 to 1.9721_01.

- \(bu
Time::Local has been upgraded from version 1.1901_01 to 1.2000.

- \(bu
Time::Piece has been upgraded from version 1.15_01 to 1.20_01.

- \(bu
Unicode::Collate has been upgraded from version 0.52_01 to 0.73.
.Sp
Unicode::Collate has been updated to use Unicode 6.0.0.
.Sp
Unicode::Collate::Locale now supports a plethora of new locales: \fIar, be,
bg, de_\|_phonebook, hu, hy, kk, mk, nso, om, tn, vi, hr, ig, ja, ko, ru, sq,
se, sr, to, uk, zh, zh_\|_big5han, zh_\|_gb2312han, zh_\|_pinyin, and *zh_\|_stroke*.
.Sp
The following modules have been added:
.Sp
Unicode::Collate::CJK::Big5 for \f(CW\*(C`zh_\|_big5han\*(C' which makes
tailoring of \s-1CJK\s0 Unified Ideographs in the order of \s-1CLDR\s0's big5han ordering.
.Sp
Unicode::Collate::CJK::GB2312 for \f(CW\*(C`zh_\|_gb2312han\*(C' which makes
tailoring of \s-1CJK\s0 Unified Ideographs in the order of \s-1CLDR\s0's gb2312han ordering.
.Sp
Unicode::Collate::CJK::JISX0208 which makes tailoring of 6355 kanji
(\s-1CJK\s0 Unified Ideographs) in the \s-1JIS X 0208\s0 order.
.Sp
Unicode::Collate::CJK::Korean which makes tailoring of \s-1CJK\s0 Unified Ideographs
in the order of \s-1CLDR\s0's Korean ordering.
.Sp
Unicode::Collate::CJK::Pinyin for \f(CW\*(C`zh_\|_pinyin\*(C' which makes
tailoring of \s-1CJK\s0 Unified Ideographs in the order of \s-1CLDR\s0's pinyin ordering.
.Sp
Unicode::Collate::CJK::Stroke for \f(CW\*(C`zh_\|_stroke\*(C' which makes
tailoring of \s-1CJK\s0 Unified Ideographs in the order of \s-1CLDR\s0's stroke ordering.
.Sp
This also sees the switch from using the pure-Perl version of this
module to the \s-1XS\s0 version.

- \(bu
Unicode::Normalize has been upgraded from version 1.03 to 1.10.

- \(bu
Unicode::UCD has been upgraded from version 0.27 to 0.32.
.Sp
A new function, **Unicode::UCD::num()**, has been added.  This function
returns the numeric value of the string passed it or \f(CW\*(C`undef\*(C' if the string
in its entirety has no \*(L"safe\*(R" numeric value.  (For more detail, and for the
definition of \*(L"safe\*(R", see \*(L"**num()**\*(R" in Unicode::UCD.)
.Sp
This upgrade also includes several bug fixes:

> 
- \fBcharinfo()
Item "charinfo()"

> 0

- \(bu
.PD
It is now updated to Unicode Version 6.0.0 with *Corrigendum #8*,
excepting that, just as with Perl 5.14, the code point at U+1F514 has no name.

- \(bu
Hangul syllable code points have the correct names, and their
decompositions are always output without requiring Lingua::KO::Hangul::Util
to be installed.

- \(bu
\s-1CJK\s0 (Chinese-Japanese-Korean) code points U+2A700 to U+2B734
and U+2B740 to U+2B81D are now properly handled.

- \(bu
Numeric values are now output for those \s-1CJK\s0 code points that have them.

- \(bu
Names output for code points with multiple aliases are now the
corrected ones.



> 


- \fBcharscript()
Item "charscript()"
This now correctly returns \*(L"Unknown\*(R" instead of \f(CW\*(C`undef\*(C' for the script
of a code point that hasn't been assigned another one.

- \fBcharblock()
Item "charblock()"
This now correctly returns \*(L"No_Block\*(R" instead of \f(CW\*(C`undef\*(C' for the block
of a code point that hasn't been assigned to another one.



> 


- \(bu
The version pragma has been upgraded from 0.82 to 0.88.
.Sp
Because of a bug, now fixed, the **is_strict()** and **is_lax()** functions did not
work when exported (5.12.1).

- \(bu
The warnings pragma has been upgraded from version 1.09 to 1.12.
.Sp
Calling \f(CW\*(C`use warnings\*(C' without arguments is now significantly more efficient.

- \(bu
The warnings::register pragma has been upgraded from version 1.01 to 1.02.
.Sp
It is now possible to register warning categories other than the names of
packages using warnings::register.  See **perllexwarn**\|(1) for more information.

- \(bu
XSLoader has been upgraded from version 0.10 to 0.13.

- \(bu
VMS::DCLsym has been upgraded from version 1.03 to 1.05.
.Sp
Two bugs have been fixed [perl #84086]:
.Sp
The symbol table name was lost when tying a hash, due to a thinko in
\f(CW\*(C`TIEHASH\*(C'.  The result was that all tied hashes interacted with the
local symbol table.
.Sp
Unless a symbol table name had been explicitly specified in the call
to the constructor, querying the special key \f(CW\*(C`:LOCAL\*(C' failed to
identify objects connected to the local symbol table.

- \(bu
The Win32 module has been upgraded from version 0.39 to 0.44.
.Sp
This release has several new functions: **Win32::GetSystemMetrics()**,
**Win32::GetProductInfo()**, **Win32::GetOSDisplayName()**.
.Sp
The names returned by **Win32::GetOSName()** and **Win32::GetOSDisplayName()**
have been corrected.

- \(bu
XS::Typemap has been upgraded from version 0.03 to 0.05.

### Removed Modules and Pragmata

Subsection "Removed Modules and Pragmata"
As promised in Perl 5.12.0's release notes, the following modules have
been removed from the core distribution, and if needed should be installed
from \s-1CPAN\s0 instead.

- \(bu
Class::ISA has been removed from the Perl core.  Prior version was 0.36.

- \(bu
Pod::Plainer has been removed from the Perl core.  Prior version was 1.02.

- \(bu
Switch has been removed from the Perl core.  Prior version was 2.16.

The removal of Shell has been deferred until after 5.14, as the
implementation of Shell shipped with 5.12.0 did not correctly issue the
warning that it was to be removed from core.

## Documentation

Header "Documentation"

### New Documentation

Subsection "New Documentation"
*perlgpl*
Subsection "perlgpl"

perlgpl has been updated to contain \s-1GPL\s0 version 1, as is included in the
*\s-1README\s0* distributed with Perl (5.12.1).

*Perl 5.12.x delta files*
Subsection "Perl 5.12.x delta files"

The perldelta files for Perl 5.12.1 to 5.12.3 have been added from the
maintenance branch: perl5121delta, perl5122delta, perl5123delta.

*perlpodstyle*
Subsection "perlpodstyle"

New style guide for \s-1POD\s0 documentation,
split mostly from the \s-1NOTES\s0 section of the **pod2man**\|(1) manpage.

*perlsource, perlinterp, perlhacktut, and perlhacktips*
Subsection "perlsource, perlinterp, perlhacktut, and perlhacktips"

See \*(L"perlhack and perlrepository revamp\*(R", below.

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perlmodlib is now complete*
Subsection "perlmodlib is now complete"

The perlmodlib manpage that came with Perl 5.12.0 was missing several
modules due to a bug in the script that generates the list.  This has been
fixed [perl #74332] (5.12.1).

*Replace incorrect tr/// table in perlebcdic*
Subsection "Replace incorrect tr/// table in perlebcdic"

perlebcdic contains a helpful table to use in \f(CW\*(C`tr///\*(C' to convert
between \s-1EBCDIC\s0 and Latin1/ASCII.  The table was the inverse of the one
it describes, though the code that used the table worked correctly for
the specific example given.

The table has been corrected and the sample code changed to correspond.

The table has also been changed to hex from octal, and the recipes in the
pod have been altered to print out leading zeros to make all values
the same length.

*Tricks for user-defined casing*
Subsection "Tricks for user-defined casing"

perlunicode now contains an explanation of how to override, mangle
and otherwise tweak the way Perl handles upper-, lower- and other-case
conversions on Unicode data, and how to provide scoped changes to alter
one's own code's behaviour without stomping on anybody else's.

*\s-1INSTALL\s0 explicitly states that Perl requires a C89 compiler*
Subsection "INSTALL explicitly states that Perl requires a C89 compiler"

This was already true, but it's now Officially Stated For The Record
(5.12.2).

*Explanation of \f(CI\*(C`\\x\f(CIHH\f(CI\*(C'\fI and \f(CI\*(C`\\o\f(CIOOO\f(CI\*(C'\fI escapes*
Subsection "Explanation of xHH and oOOO escapes"

perlop has been updated with more detailed explanation of these two
character escapes.

*\f(BI-0\f(BI\s-1NNN\s0\f(BI\fI switch*
Subsection "-0NNN switch"

In perlrun, the behaviour of the **-0NNN** switch for **-0400** or higher
has been clarified (5.12.2).

*Maintenance policy*
Subsection "Maintenance policy"

perlpolicy now contains the policy on what patches are acceptable for
maintenance branches (5.12.1).

*Deprecation policy*
Subsection "Deprecation policy"

perlpolicy now contains the policy on compatibility and deprecation
along with definitions of terms like \*(L"deprecation\*(R" (5.12.2).

*New descriptions in perldiag*
Subsection "New descriptions in perldiag"

The following existing diagnostics are now documented:

- \(bu
Ambiguous use of \f(CW%c resolved as operator \f(CW%c

- \(bu
Ambiguous use of \f(CW%c\{%s\} resolved to \f(CW%c%s

- \(bu
Ambiguous use of \f(CW%c\{%s[...]\} resolved to \f(CW%c%s[...]

- \(bu
Ambiguous use of \f(CW%c\{%s\{...\}\} resolved to \f(CW%c%s\{...\}

- \(bu
Ambiguous use of -%s resolved as -&%s()

- \(bu
Invalid strict version format (%s)

- \(bu
Invalid version format (%s)

- \(bu
Invalid version object

*perlbook*
Subsection "perlbook"

perlbook has been expanded to cover many more popular books.

*\f(CI\*(C`SvTRUE\*(C'\fI macro*
Subsection "SvTRUE macro"

The documentation for the \f(CW\*(C`SvTRUE\*(C' macro in
perlapi was simply wrong in stating that
get-magic is not processed.  It has been corrected.

*op manipulation functions*
Subsection "op manipulation functions"

Several \s-1API\s0 functions that process optrees have been newly documented.

*perlvar revamp*
Subsection "perlvar revamp"

perlvar reorders the variables and groups them by topic.  Each variable
introduced after Perl 5.000 notes the first version in which it is
available.  perlvar also has a new section for deprecated variables to
note when they were removed.

*Array and hash slices in scalar context*
Subsection "Array and hash slices in scalar context"

These are now documented in perldata.

*\f(CI\*(C`use locale\*(C'\fI and formats*
Subsection "use locale and formats"

perlform and perllocale have been corrected to state that
\f(CW\*(C`use locale\*(C' affects formats.

*overload*
Subsection "overload"

overload's documentation has practically undergone a rewrite.  It
is now much more straightforward and clear.

*perlhack and perlrepository revamp*
Subsection "perlhack and perlrepository revamp"

The perlhack document is now much shorter, and focuses on the Perl 5
development process and submitting patches to Perl.  The technical content
has been moved to several new documents, perlsource, perlinterp,
perlhacktut, and perlhacktips.  This technical content has
been only lightly edited.

The perlrepository document has been renamed to perlgit.  This new
document is just a how-to on using git with the Perl source code.
Any other content that used to be in perlrepository has been moved
to perlhack.

*Time::Piece examples*
Subsection "Time::Piece examples"

Examples in perlfaq4 have been updated to show the use of
Time::Piece.

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### New Diagnostics

Subsection "New Diagnostics"
*New Errors*
Subsection "New Errors"

- Closure prototype called
Item "Closure prototype called"
This error occurs when a subroutine reference passed to an attribute
handler is called, if the subroutine is a closure [perl #68560].
.ie n .IP "Insecure user-defined property %s" 4
.el .IP "Insecure user-defined property \f(CW%s" 4
Item "Insecure user-defined property %s"
Perl detected tainted data when trying to compile a regular
expression that contains a call to a user-defined character property
function, meaning \f(CW\*(C`\\p\{IsFoo\}\*(C' or \f(CW\*(C`\\p\{InFoo\}\*(C'.
See \*(L"User-Defined Character Properties\*(R" in perlunicode and perlsec.

- panic: gp_free failed to free glob pointer - something is repeatedly re-creating entries
Item "panic: gp_free failed to free glob pointer - something is repeatedly re-creating entries"
This new error is triggered if a destructor called on an object in a
typeglob that is being freed creates a new typeglob entry containing an
object with a destructor that creates a new entry containing an object etc.

- Parsing code internal error (%s)
Item "Parsing code internal error (%s)"
This new fatal error is produced when parsing
code supplied by an extension violates the
parser's \s-1API\s0 in a detectable way.
.ie n .IP "refcnt: fd %d%s" 4
.el .IP "refcnt: fd \f(CW%d%s" 4
Item "refcnt: fd %d%s"
This new error only occurs if an internal consistency check fails when a
pipe is about to be closed.
.ie n .IP "Regexp modifier ""/%c"" may not appear twice" 4
.el .IP "Regexp modifier ``/%c'' may not appear twice" 4
Item "Regexp modifier /%c may not appear twice"
The regular expression pattern has one of the
mutually exclusive modifiers repeated.
.ie n .IP "Regexp modifiers ""/%c"" and ""/%c"" are mutually exclusive" 4
.el .IP "Regexp modifiers ``/%c'' and ``/%c'' are mutually exclusive" 4
Item "Regexp modifiers /%c and /%c are mutually exclusive"
The regular expression pattern has more than one of the mutually
exclusive modifiers.
.ie n .IP "Using !~ with %s doesn't make sense" 4
.el .IP "Using !~ with \f(CW%s doesn't make sense" 4
Item "Using !~ with %s doesn't make sense"
This error occurs when \f(CW\*(C`!~\*(C' is used with \f(CW\*(C`s///r\*(C' or \f(CW\*(C`y///r\*(C'.

*New Warnings*
Subsection "New Warnings"
.ie n .IP """\\b\{"" is deprecated; use ""\\b\\\{"" instead" 4
.el .IP "``\\b\{'' is deprecated; use ``\\b\\\{'' instead" 4
Item "b\{ is deprecated; use b\{ instead"
0
.ie n .IP """\\B\{"" is deprecated; use ""\\B\\\{"" instead" 4
.el .IP "``\\B\{'' is deprecated; use ``\\B\\\{'' instead" 4
Item "B\{ is deprecated; use B\{ instead"
.PD
Use of an unescaped \*(L"\{\*(R" immediately following a \f(CW\*(C`\\b\*(C' or \f(CW\*(C`\\B\*(C' is now
deprecated in order to reserve its use for Perl itself in a future release.
.ie n .IP "Operation ""%s"" returns its argument for ..." 4
.el .IP "Operation ``%s'' returns its argument for ..." 4
Item "Operation %s returns its argument for ..."
Performing an operation requiring Unicode semantics (such as case-folding)
on a Unicode surrogate or a non-Unicode character now triggers this
warning.

- Use of qw(...) as parentheses is deprecated
Item "Use of qw(...) as parentheses is deprecated"
See \*(L"Use of qw(...) as parentheses\*(R", above, for details.

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
The \*(L"Variable \f(CW$foo is not imported\*(R" warning that precedes a
\f(CW\*(C`strict \*(Aqvars\*(Aq\*(C' error has now been assigned the \*(L"misc\*(R" category, so that
\f(CW\*(C`no warnings\*(C' will suppress it [perl #73712].

- \(bu
**warn()** and **die()** now produce \*(L"Wide character\*(R" warnings when fed a
character outside the byte range if \f(CW\*(C`STDERR\*(C' is a byte-sized handle.

- \(bu
The \*(L"Layer does not match this perl\*(R" error message has been replaced with
these more helpful messages [perl #73754]:

> 
- \(bu
PerlIO layer function table size (%d) does not match size expected by this
perl (%d)

- \(bu
PerlIO layer instance size (%d) does not match size expected by this perl
(%d)



> 


- \(bu
The \*(L"Found = in conditional\*(R" warning that is emitted when a constant is
assigned to a variable in a condition is now withheld if the constant is
actually a subroutine or one generated by \f(CW\*(C`use constant\*(C', since the value
of the constant may not be known at the time the program is written
[perl #77762].

- \(bu
Previously, if none of the **gethostbyaddr()**, **gethostbyname()** and
**gethostent()** functions were implemented on a given platform, they would
all die with the message \*(L"Unsupported socket function 'gethostent' called\*(R",
with analogous messages for getnet*() and getserv*().  This has been
corrected.

- \(bu
The warning message about unrecognized regular expression escapes passed
through has been changed to include any literal \*(L"\{\*(R" following the
two-character escape.  For example, \*(L"\\q\{\*(R" is now emitted instead of \*(L"\\q\*(R".

## Utility Changes

Header "Utility Changes"
*\f(BIperlbug\fI\|(1)*
Subsection "perlbug"

- \(bu
perlbug now looks in the \s-1EMAIL\s0 environment variable for a return address
if the REPLY-TO and \s-1REPLYTO\s0 variables are empty.

- \(bu
perlbug did not previously generate a \*(L"From:\*(R" header, potentially
resulting in dropped mail; it now includes that header.

- \(bu
The user's address is now used as the Return-Path.
.Sp
Many systems these days don't have a valid Internet domain name, and
perlbug@perl.org does not accept email with a return-path that does
not resolve.  So the user's address is now passed to sendmail so it's
less likely to get stuck in a mail queue somewhere [perl #82996].

- \(bu
perlbug now always gives the reporter a chance to change the email
address it guesses for them (5.12.2).

- \(bu
perlbug should no longer warn about uninitialized values when using the **-d**
and **-v** options (5.12.2).

*perl5db.pl*
Subsection "perl5db.pl"

- \(bu
The remote terminal works after forking and spawns new sessions, one
per forked process.

*ptargrep*
Subsection "ptargrep"

- \(bu
ptargrep is a new utility to apply pattern matching to the contents of
files  in a tar archive.  It comes with \f(CW\*(C`Archive::Tar\*(C'.

## Configuration and Compilation

Header "Configuration and Compilation"
See also \*(L"Naming fixes in Policy_sh.SH may invalidate Policy.sh\*(R",
above.

- \(bu
\s-1CCINCDIR\s0 and \s-1CCLIBDIR\s0 for the mingw64 cross-compiler are now correctly
under *$(\s-1CCHOME\s0)\\mingw\\include* and *\\lib* rather than immediately below
*$(\s-1CCHOME\s0)*.
.Sp
This means the \*(L"incpath\*(R", \*(L"libpth\*(R", \*(L"ldflags\*(R", \*(L"lddlflags\*(R" and
\*(L"ldflags_nolargefiles\*(R" values in *Config.pm* and *Config_heavy.pl* are now
set correctly.

- \(bu
\f(CW\*(C`make test.valgrind\*(C' has been adjusted to account for *cpan/dist/ext*
separation.

- \(bu
On compilers that support it, **-Wwrite-strings** is now added to cflags by
default.

- \(bu
The Encode module can now (once again) be included in a static Perl
build.  The special-case handling for this situation got broken in Perl
5.11.0, and has now been repaired.

- \(bu
The previous default size of a PerlIO buffer (4096 bytes) has been increased
to the larger of 8192 bytes and your local \s-1BUFSIZ.\s0  Benchmarks show that doubling
this decade-old default increases read and write performance by around
25% to 50% when using the default layers of perlio on top of unix.  To choose
a non-default size, such as to get back the old value or to obtain an even
larger value, configure with:
.Sp
.Vb 1
     ./Configure -Accflags=-DPERLIOBUF_DEFAULT_BUFSIZ=N
.Ve
.Sp
where N is the desired size in bytes; it should probably be a multiple of
your page size.

- \(bu
An \*(L"incompatible operand types\*(R" error in ternary expressions when building
with \f(CW\*(C`clang\*(C' has been fixed (5.12.2).

- \(bu
Perl now skips setuid File::Copy tests on partitions it detects mounted
as \f(CW\*(C`nosuid\*(C' (5.12.2).

## Platform Support

Header "Platform Support"

### New Platforms

Subsection "New Platforms"

- \s-1AIX\s0
Item "AIX"
Perl now builds on \s-1AIX 4.2\s0 (5.12.1).

### Discontinued Platforms

Subsection "Discontinued Platforms"

- Apollo DomainOS
Item "Apollo DomainOS"
The last vestiges of support for this platform have been excised from
the Perl distribution.  It was officially discontinued in version 5.12.0.
It had not worked for years before that.

- MacOS Classic
Item "MacOS Classic"
The last vestiges of support for this platform have been excised from the
Perl distribution.  It was officially discontinued in an earlier version.

### Platform-Specific Notes

Subsection "Platform-Specific Notes"
*\s-1AIX\s0*
Subsection "AIX"

- \(bu
*\s-1README\s0.aix* has been updated with information about the \s-1XL C/\*(C+ V11\s0 compiler
suite (5.12.2).

*\s-1ARM\s0*
Subsection "ARM"

- \(bu
The \f(CW\*(C`d_u32align\*(C' configuration probe on \s-1ARM\s0 has been fixed (5.12.2).

*Cygwin*
Subsection "Cygwin"

- \(bu
MakeMaker has been updated to build manpages on cygwin.

- \(bu
Improved rebase behaviour
.Sp
If a \s-1DLL\s0 is updated on cygwin the old imagebase address is reused.
This solves most rebase errors, especially when updating on core \s-1DLL\s0's.
See <http://www.tishler.net/jason/software/rebase/rebase-2.4.2.README>
for more information.

- \(bu
Support for the standard cygwin dll prefix (needed for FFIs)

- \(bu
Updated build hints file

*FreeBSD 7*
Subsection "FreeBSD 7"

- \(bu
FreeBSD 7 no longer contains */usr/bin/objformat*.  At build time,
Perl now skips the *objformat* check for versions 7 and higher and
assumes \s-1ELF\s0 (5.12.1).

*HP-UX*
Subsection "HP-UX"

- \(bu
Perl now allows **-Duse64bitint** without promoting to \f(CW\*(C`use64bitall\*(C' on HP-UX
(5.12.1).

*\s-1IRIX\s0*
Subsection "IRIX"

- \(bu
Conversion of strings to floating-point numbers is now more accurate on
\s-1IRIX\s0 systems [perl #32380].

*Mac \s-1OS X\s0*
Subsection "Mac OS X"

- \(bu
Early versions of Mac \s-1OS X\s0 (Darwin) had buggy implementations of the
**setregid()**, **setreuid()**, setrgid(,) and **setruid()** functions, so Perl
would pretend they did not exist.
.Sp
These functions are now recognised on Mac \s-1OS 10.5\s0 (Leopard; Darwin 9) and
higher, as they have been fixed [perl #72990].

*MirBSD*
Subsection "MirBSD"

- \(bu
Previously if you built Perl with a shared *libperl.so* on MirBSD (the
default config), it would work up to the installation; however, once
installed, it would be unable to find *libperl*.  Path handling is now
treated as in the other \s-1BSD\s0 dialects.

*NetBSD*
Subsection "NetBSD"

- \(bu
The NetBSD hints file has been changed to make the system malloc the
default.

*OpenBSD*
Subsection "OpenBSD"

- \(bu
OpenBSD > 3.7 has a new malloc implementation which is *mmap*-based,
and as such can release memory back to the \s-1OS\s0; however, Perl's use of
this malloc causes a substantial slowdown, so we now default to using
Perl's malloc instead [perl #75742].

*OpenVOS*
Subsection "OpenVOS"

- \(bu
Perl now builds again with OpenVOS (formerly known as Stratus \s-1VOS\s0)
[perl #78132] (5.12.3).

*Solaris*
Subsection "Solaris"

- \(bu
DTrace is now supported on Solaris.  There used to be build failures, but
these have been fixed [perl #73630] (5.12.3).

*\s-1VMS\s0*
Subsection "VMS"

- \(bu
Extension building on older (pre 7.3-2) \s-1VMS\s0 systems was broken because
configure.com hit the \s-1DCL\s0 symbol length limit of 1K.  We now work within
this limit when assembling the list of extensions in the core build (5.12.1).

- \(bu
We fixed configuring and building Perl with **-Uuseperlio** (5.12.1).

- \(bu
\f(CW\*(C`PerlIOUnix_open\*(C' now honours the default permissions on \s-1VMS.\s0
.Sp
When \f(CW\*(C`perlio\*(C' became the default and \f(CW\*(C`unix\*(C' became the default bottom layer,
the most common path for creating files from Perl became \f(CW\*(C`PerlIOUnix_open\*(C',
which has always explicitly used \f(CW0666 as the permission mask.  This prevents
inheriting permissions from \s-1RMS\s0 defaults and ACLs, so to avoid that problem,
we now pass \f(CW0777 to **open()**.  In the \s-1VMS CRTL,\s0 \f(CW0777 has a special
meaning over and above intersecting with the current umask; specifically, it
allows Unix syscalls to preserve native default permissions (5.12.3).

- \(bu
The shortening of symbols longer than 31 characters in the core C sources
and in extensions is now by default done by the C compiler rather than by
xsubpp (which could only do so for generated symbols in \s-1XS\s0 code).  You can
reenable xsubpp's symbol shortening by configuring with -Uuseshortenedsymbols,
but you'll have some work to do to get the core sources to compile.

- \(bu
Record-oriented files (record format variable or variable with fixed control)
opened for write by the \f(CW\*(C`perlio\*(C' layer will now be line-buffered to prevent the
introduction of spurious line breaks whenever the perlio buffer fills up.

- \(bu
*git_version.h* is now installed on \s-1VMS.\s0  This was an oversight in v5.12.0 which
caused some extensions to fail to build (5.12.2).

- \(bu
Several memory leaks in **stat()** have been fixed (5.12.2).

- \(bu
A memory leak in **Perl_rename()** due to a double allocation has been
fixed (5.12.2).

- \(bu
A memory leak in **vms_fid_to_name()** (used by **realpath()** and
**realname()**> has been fixed (5.12.2).

*Windows*
Subsection "Windows"

See also \*(L"**fork()** emulation will not wait for signalled children\*(R" and
\*(L"Perl source code is read in text mode on Windows\*(R", above.

- \(bu
Fixed build process for \s-1SDK2003SP1\s0 compilers.

- \(bu
Compilation with Visual Studio 2010 is now supported.

- \(bu
When using old 32-bit compilers, the define \f(CW\*(C`_USE_32BIT_TIME_T\*(C' is now
set in \f(CW$Config\{ccflags\}.  This improves portability when compiling
\s-1XS\s0 extensions using new compilers, but for a Perl compiled with old 32-bit
compilers.

- \(bu
\f(CW$Config\{gccversion\} is now set correctly when Perl is built using the
mingw64 compiler from <http://mingw64.org> [perl #73754].

- \(bu
When building Perl with the mingw64 x64 cross-compiler \f(CW\*(C`incpath\*(C',
\f(CW\*(C`libpth\*(C', \f(CW\*(C`ldflags\*(C', \f(CW\*(C`lddlflags\*(C' and \f(CW\*(C`ldflags_nolargefiles\*(C' values
in *Config.pm* and *Config_heavy.pl* were not previously being set
correctly because, with that compiler, the include and lib directories
are not immediately below \f(CW\*(C`$(CCHOME)\*(C' (5.12.2).

- \(bu
The build process proceeds more smoothly with mingw and dmake when
*C:\\MSYS\\bin* is in the \s-1PATH,\s0 due to a \f(CW\*(C`Cwd\*(C' fix.

- \(bu
Support for building with Visual \*(C+ 2010 is now underway, but is not yet
complete.  See *\s-1README\s0.win32* or perlwin32 for more details.

- \(bu
The option to use an externally-supplied **crypt()**, or to build with no
**crypt()** at all, has been removed.  Perl supplies its own **crypt()**
implementation for Windows, and the political situation that required
this part of the distribution to sometimes be omitted is long gone.

## Internal Changes

Header "Internal Changes"

### New APIs

Subsection "New APIs"
*\s-1CLONE_PARAMS\s0 structure added to ease correct thread creation*
Subsection "CLONE_PARAMS structure added to ease correct thread creation"

Modules that create threads should now create \f(CW\*(C`CLONE_PARAMS\*(C' structures
by calling the new function **Perl_clone_params_new()**, and free them with
**Perl_clone_params_del()**.  This will ensure compatibility with any future
changes to the internals of the \f(CW\*(C`CLONE_PARAMS\*(C' structure layout, and that
it is correctly allocated and initialised.

*New parsing functions*
Subsection "New parsing functions"

Several functions have been added for parsing Perl statements and
expressions.  These functions are meant to be used by \s-1XS\s0 code invoked
during Perl parsing, in a recursive-descent manner, to allow modules to
augment the standard Perl syntax.

- \(bu
**parse_stmtseq()**
parses a sequence of statements, up to closing brace or \s-1EOF.\s0

- \(bu
**parse_fullstmt()**
parses a complete Perl statement, including optional label.

- \(bu
**parse_barestmt()**
parses a statement without a label.

- \(bu
**parse_block()**
parses a code block.

- \(bu
**parse_label()**
parses a statement label, separate from statements.

- \(bu
\f(CW\*(C`parse_fullexpr()\*(C',
\f(CW\*(C`parse_listexpr()\*(C',
\f(CW\*(C`parse_termexpr()\*(C', and
\f(CW\*(C`parse_arithexpr()\*(C'
parse expressions at various precedence levels.

*Hints hash \s-1API\s0*
Subsection "Hints hash API"

A new C \s-1API\s0 for introspecting the hinthash \f(CW\*(C`%^H\*(C' at runtime has been
added.  See \f(CW\*(C`cop_hints_2hv\*(C', \f(CW\*(C`cop_hints_fetchpvn\*(C', \f(CW\*(C`cop_hints_fetchpvs\*(C',
\f(CW\*(C`cop_hints_fetchsv\*(C', and \f(CW\*(C`hv_copy_hints_hv\*(C' in perlapi for details.

A new, experimental \s-1API\s0 has been added for accessing the internal
structure that Perl uses for \f(CW\*(C`%^H\*(C'.  See the functions beginning with
\f(CW\*(C`cophh_\*(C' in perlapi.

*C interface to \f(BIcaller()\fI*
Subsection "C interface to caller()"

The \f(CW\*(C`caller_cx\*(C' function has been added as an XSUB-writer's equivalent of
**caller()**.  See perlapi for details.

*Custom per-subroutine check hooks*
Subsection "Custom per-subroutine check hooks"

\s-1XS\s0 code in an extension module can now annotate a subroutine (whether
implemented in \s-1XS\s0 or in Perl) so that nominated \s-1XS\s0 code will be called
at compile time (specifically as part of op checking) to change the op
tree of that subroutine.  The compile-time check function (supplied by
the extension module) can implement argument processing that can't be
expressed as a prototype, generate customised compile-time warnings,
perform constant folding for a pure function, inline a subroutine
consisting of sufficiently simple ops, replace the whole call with a
custom op, and so on.  This was previously all possible by hooking the
\f(CW\*(C`entersub\*(C' op checker, but the new mechanism makes it easy to tie the
hook to a specific subroutine.  See \*(L"cv_set_call_checker\*(R" in perlapi.

To help in writing custom check hooks, several subtasks within standard
\f(CW\*(C`entersub\*(C' op checking have been separated out and exposed in the \s-1API.\s0

*Improved support for custom OPs*
Subsection "Improved support for custom OPs"

Custom ops can now be registered with the new \f(CW\*(C`custom_op_register\*(C' C
function and the \f(CW\*(C`XOP\*(C' structure.  This will make it easier to add new
properties of custom ops in the future.  Two new properties have been added
already, \f(CW\*(C`xop_class\*(C' and \f(CW\*(C`xop_peep\*(C'.

\f(CW\*(C`xop_class\*(C' is one of the OA_*OP constants.  It allows B and other
introspection mechanisms to work with custom ops
that aren't BASEOPs.  \f(CW\*(C`xop_peep\*(C' is a pointer to
a function that will be called for ops of this
type from \f(CW\*(C`Perl_rpeep\*(C'.

See \*(L"Custom Operators\*(R" in perlguts and \*(L"Custom Operators\*(R" in perlapi for more
detail.

The old \f(CW\*(C`PL_custom_op_names\*(C'/\f(CW\*(C`PL_custom_op_descs\*(C' interface is still
supported but discouraged.

*Scope hooks*
Subsection "Scope hooks"

It is now possible for \s-1XS\s0 code to hook into Perl's lexical scope
mechanism at compile time, using the new \f(CW\*(C`Perl_blockhook_register\*(C'
function.  See \*(L"Compile-time scope hooks\*(R" in perlguts.

*The recursive part of the peephole optimizer is now hookable*
Subsection "The recursive part of the peephole optimizer is now hookable"

In addition to \f(CW\*(C`PL_peepp\*(C', for hooking into the toplevel peephole optimizer, a
\f(CW\*(C`PL_rpeepp\*(C' is now available to hook into the optimizer recursing into
side-chains of the optree.

*New non-magical variants of existing functions*
Subsection "New non-magical variants of existing functions"

The following functions/macros have been added to the \s-1API.\s0  The \f(CW*_nomg
macros are equivalent to their non-\f(CW\*(C`_nomg\*(C' variants, except that they ignore
get-magic.  Those ending in \f(CW\*(C`_flags\*(C' allow one to specify whether
get-magic is processed.

.Vb 8
  sv_2bool_flags
  SvTRUE_nomg
  sv_2nv_flags
  SvNV_nomg
  sv_cmp_flags
  sv_cmp_locale_flags
  sv_eq_flags
  sv_collxfrm_flags
.Ve

In some of these cases, the non-\f(CW\*(C`_flags\*(C' functions have
been replaced with wrappers around the new functions.

*pv/pvs/sv versions of existing functions*
Subsection "pv/pvs/sv versions of existing functions"

Many functions ending with pvn now have equivalent \f(CW\*(C`pv/pvs/sv\*(C' versions.

*List op-building functions*
Subsection "List op-building functions"

List op-building functions have been added to the
\s-1API.\s0  See op_append_elem,
op_append_list, and
op_prepend_elem in perlapi.

*\f(CI\*(C`LINKLIST\*(C'\fI*
Subsection "LINKLIST"

The \s-1LINKLIST\s0 macro, part of op building that
constructs the execution-order op chain, has been added to the \s-1API.\s0

*Localisation functions*
Subsection "Localisation functions"

The \f(CW\*(C`save_freeop\*(C', \f(CW\*(C`save_op\*(C', \f(CW\*(C`save_pushi32ptr\*(C' and \f(CW\*(C`save_pushptrptr\*(C'
functions have been added to the \s-1API.\s0

*Stash names*
Subsection "Stash names"

A stash can now have a list of effective names in addition to its usual
name.  The first effective name can be accessed via the \f(CW\*(C`HvENAME\*(C' macro,
which is now the recommended name to use in \s-1MRO\s0 linearisations (\f(CW\*(C`HvNAME\*(C'
being a fallback if there is no \f(CW\*(C`HvENAME\*(C').

These names are added and deleted via \f(CW\*(C`hv_ename_add\*(C' and
\f(CW\*(C`hv_ename_delete\*(C'.  These two functions are *not* part of the \s-1API.\s0

*New functions for finding and removing magic*
Subsection "New functions for finding and removing magic"

The \f(CW\*(C`mg_findext()\*(C' and
\f(CW\*(C`sv_unmagicext()\*(C'
functions have been added to the \s-1API.\s0
They allow extension authors to find and remove magic attached to
scalars based on both the magic type and the magic virtual table, similar to how
**sv_magicext()** attaches magic of a certain type and with a given virtual table
to a scalar.  This eliminates the need for extensions to walk the list of
\f(CW\*(C`MAGIC\*(C' pointers of an \f(CW\*(C`SV\*(C' to find the magic that belongs to them.

*\f(CI\*(C`find_rundefsv\*(C'\fI*
Subsection "find_rundefsv"

This function returns the \s-1SV\s0 representing \f(CW$_, whether it's lexical
or dynamic.

*\f(CI\*(C`Perl_croak_no_modify\*(C'\fI*
Subsection "Perl_croak_no_modify"

**Perl_croak_no_modify()** is short-hand for
\f(CW\*(C`Perl_croak("%s", PL_no_modify)\*(C'.

*\f(CI\*(C`PERL_STATIC_INLINE\*(C'\fI define*
Subsection "PERL_STATIC_INLINE define"

The \f(CW\*(C`PERL_STATIC_INLINE\*(C' define has been added to provide the best-guess
incantation to use for static inline functions, if the C compiler supports
C99-style static inline.  If it doesn't, it'll give a plain \f(CW\*(C`static\*(C'.

\f(CW\*(C`HAS_STATIC_INLINE\*(C' can be used to check if the compiler actually supports
inline functions.

*New \f(CI\*(C`pv_escape\*(C'\fI option for hexadecimal escapes*
Subsection "New pv_escape option for hexadecimal escapes"

A new option, \f(CW\*(C`PERL_PV_ESCAPE_NONASCII\*(C', has been added to \f(CW\*(C`pv_escape\*(C' to
dump all characters above \s-1ASCII\s0 in hexadecimal.  Before, one could get all
characters as hexadecimal or the Latin1 non-ASCII as octal.

*\f(CI\*(C`lex_start\*(C'\fI*
Subsection "lex_start"

\f(CW\*(C`lex_start\*(C' has been added to the \s-1API,\s0 but is considered experimental.

*\f(BIop_scope()\fI and \f(BIop_lvalue()\fI*
Subsection "op_scope() and op_lvalue()"

The **op_scope()** and **op_lvalue()** functions have been added to the \s-1API,\s0
but are considered experimental.

### C \s-1API\s0 Changes

Subsection "C API Changes"
*\f(CI\*(C`PERL_POLLUTE\*(C'\fI has been removed*
Subsection "PERL_POLLUTE has been removed"

The option to define \f(CW\*(C`PERL_POLLUTE\*(C' to expose older 5.005 symbols for
backwards compatibility has been removed.  Its use was always discouraged,
and MakeMaker contains a more specific escape hatch:

.Vb 1
    perl Makefile.PL POLLUTE=1
.Ve

This can be used for modules that have not been upgraded to 5.6 naming
conventions (and really should be completely obsolete by now).

*Check \s-1API\s0 compatibility when loading \s-1XS\s0 modules*
Subsection "Check API compatibility when loading XS modules"

When Perl's \s-1API\s0 changes in incompatible ways (which usually happens between
major releases), \s-1XS\s0 modules compiled for previous versions of Perl will no
longer work.  They need to be recompiled against the new Perl.

The \f(CW\*(C`XS_APIVERSION_BOOTCHECK\*(C' macro has been added to ensure that modules
are recompiled and to prevent users from accidentally loading modules
compiled for old perls into newer perls.  That macro, which is called when
loading every newly compiled extension, compares the \s-1API\s0 version of the
running perl with the version a module has been compiled for and raises an
exception if they don't match.

*Perl_fetch_cop_label*
Subsection "Perl_fetch_cop_label"

The first argument of the C \s-1API\s0 function \f(CW\*(C`Perl_fetch_cop_label\*(C' has changed
from \f(CW\*(C`struct refcounted_he *\*(C' to \f(CW\*(C`COP *\*(C', to insulate the user from
implementation details.

This \s-1API\s0 function was marked as \*(L"may change\*(R", and likely isn't in use outside
the core.  (Neither an unpacked \s-1CPAN\s0 nor Google's codesearch finds any other
references to it.)

*\f(BIGvCV()\fI and \f(BIGvGP()\fI are no longer lvalues*
Subsection "GvCV() and GvGP() are no longer lvalues"

The new **GvCV_set()** and **GvGP_set()** macros are now provided to replace
assignment to those two macros.

This allows a future commit to eliminate some backref magic between \s-1GV\s0
and CVs, which will require complete control over assignment to the
\f(CW\*(C`gp_cv\*(C' slot.

*\f(BICvGV()\fI is no longer an lvalue*
Subsection "CvGV() is no longer an lvalue"

Under some circumstances, the **CvGV()** field of a \s-1CV\s0 is now
reference-counted.  To ensure consistent behaviour, direct assignment to
it, for example \f(CW\*(C`CvGV(cv) = gv\*(C' is now a compile-time error.  A new macro,
\f(CW\*(C`CvGV_set(cv,gv)\*(C' has been introduced to run this operation
safely.  Note that modification of this field is not part of the public
\s-1API,\s0 regardless of this new macro (and despite its being listed in this section).

*\f(BICvSTASH()\fI is no longer an lvalue*
Subsection "CvSTASH() is no longer an lvalue"

The **CvSTASH()** macro can now only be used as an rvalue.  **CvSTASH_set()**
has been added to replace assignment to **CvSTASH()**.  This is to ensure
that backreferences are handled properly.  These macros are not part of the
\s-1API.\s0

*Calling conventions for \f(CI\*(C`newFOROP\*(C'\fI and \f(CI\*(C`newWHILEOP\*(C'\fI*
Subsection "Calling conventions for newFOROP and newWHILEOP"

The way the parser handles labels has been cleaned up and refactored.  As a
result, the **newFOROP()** constructor function no longer takes a parameter
stating what label is to go in the state op.

The **newWHILEOP()** and **newFOROP()** functions no longer accept a line
number as a parameter.

*Flags passed to \f(CI\*(C`uvuni_to_utf8_flags\*(C'\fI and \f(CI\*(C`utf8n_to_uvuni\*(C'\fI*
Subsection "Flags passed to uvuni_to_utf8_flags and utf8n_to_uvuni"

Some of the flags parameters to **uvuni_to_utf8_flags()** and
**utf8n_to_uvuni()** have changed.  This is a result of Perl's now allowing
internal storage and manipulation of code points that are problematic
in some situations.  Hence, the default actions for these functions has
been complemented to allow these code points.  The new flags are
documented in perlapi.  Code that requires the problematic code
points to be rejected needs to change to use the new flags.  Some flag
names are retained for backward source compatibility, though they do
nothing, as they are now the default.  However the flags
\f(CW\*(C`UNICODE_ALLOW_FDD0\*(C', \f(CW\*(C`UNICODE_ALLOW_FFFF\*(C', \f(CW\*(C`UNICODE_ILLEGAL\*(C', and
\f(CW\*(C`UNICODE_IS_ILLEGAL\*(C' have been removed, as they stem from a
fundamentally broken model of how the Unicode non-character code points
should be handled, which is now described in
\*(L"Non-character code points\*(R" in perlunicode.  See also the Unicode section
under \*(L"Selected Bug Fixes\*(R".

### Deprecated C APIs

Subsection "Deprecated C APIs"
.ie n .IP """Perl_ptr_table_clear""" 4
.el .IP "\f(CWPerl_ptr_table_clear" 4
Item "Perl_ptr_table_clear"
\f(CW\*(C`Perl_ptr_table_clear\*(C' is no longer part of Perl's public \s-1API.\s0  Calling it
now generates a deprecation warning, and it will be removed in a future
release.
.ie n .IP """sv_compile_2op""" 4
.el .IP "\f(CWsv_compile_2op" 4
Item "sv_compile_2op"
The **sv_compile_2op()** \s-1API\s0 function is now deprecated.  Searches suggest
that nothing on \s-1CPAN\s0 is using it, so this should have zero impact.
.Sp
It attempted to provide an \s-1API\s0 to compile code down to an optree, but failed
to bind correctly to lexicals in the enclosing scope.  It's not possible to
fix this problem within the constraints of its parameters and return value.
.ie n .IP """find_rundefsvoffset""" 4
.el .IP "\f(CWfind_rundefsvoffset" 4
Item "find_rundefsvoffset"
The \f(CW\*(C`find_rundefsvoffset\*(C' function has been deprecated.  It appeared that
its design was insufficient for reliably getting the lexical \f(CW$_ at
run-time.
.Sp
Use the new \f(CW\*(C`find_rundefsv\*(C' function or the \f(CW\*(C`UNDERBAR\*(C' macro
instead.  They directly return the right \s-1SV\s0
representing \f(CW$_, whether it's
lexical or dynamic.
.ie n .IP """CALL_FPTR"" and ""CPERLscope""" 4
.el .IP "\f(CWCALL_FPTR and \f(CWCPERLscope" 4
Item "CALL_FPTR and CPERLscope"
Those are left from an old implementation of \f(CW\*(C`MULTIPLICITY\*(C' using \*(C+ objects,
which was removed in Perl 5.8.  Nowadays these macros do exactly nothing, so
they shouldn't be used anymore.
.Sp
For compatibility, they are still defined for external \f(CW\*(C`XS\*(C' code.  Only
extensions defining \f(CW\*(C`PERL_CORE\*(C' must be updated now.

### Other Internal Changes

Subsection "Other Internal Changes"
*Stack unwinding*
Subsection "Stack unwinding"

The protocol for unwinding the C stack at the last stage of a \f(CW\*(C`die\*(C'
has changed how it identifies the target stack frame.  This now uses
a separate variable \f(CW\*(C`PL_restartjmpenv\*(C', where previously it relied on
the \f(CW\*(C`blk_eval.cur_top_env\*(C' pointer in the \f(CW\*(C`eval\*(C' context frame that
has nominally just been discarded.  This change means that code running
during various stages of Perl-level unwinding no longer needs to take
care to avoid destroying the ghost frame.

*Scope stack entries*
Subsection "Scope stack entries"

The format of entries on the scope stack has been changed, resulting in a
reduction of memory usage of about 10%.  In particular, the memory used by
the scope stack to record each active lexical variable has been halved.

*Memory allocation for pointer tables*
Subsection "Memory allocation for pointer tables"

Memory allocation for pointer tables has been changed.  Previously
\f(CW\*(C`Perl_ptr_table_store\*(C' allocated memory from the same arena system as
\f(CW\*(C`SV\*(C' bodies and \f(CW\*(C`HE\*(C's, with freed memory remaining bound to those arenas
until interpreter exit.  Now it allocates memory from arenas private to the
specific pointer table, and that memory is returned to the system when
\f(CW\*(C`Perl_ptr_table_free\*(C' is called.  Additionally, allocation and release are
both less \s-1CPU\s0 intensive.

*\f(CI\*(C`UNDERBAR\*(C'\fI*
Subsection "UNDERBAR"

The \f(CW\*(C`UNDERBAR\*(C' macro now calls \f(CW\*(C`find_rundefsv\*(C'.  \f(CW\*(C`dUNDERBAR\*(C' is now a
noop but should still be used to ensure past and future compatibility.

*String comparison routines renamed*
Subsection "String comparison routines renamed"

The \f(CW\*(C`ibcmp_*\*(C' functions have been renamed and are now called \f(CW\*(C`foldEQ\*(C',
\f(CW\*(C`foldEQ_locale\*(C', and \f(CW\*(C`foldEQ_utf8\*(C'.  The old names are still available as
macros.

*\f(CI\*(C`chop\*(C'\fI and \f(CI\*(C`chomp\*(C'\fI implementations merged*
Subsection "chop and chomp implementations merged"

The opcode bodies for \f(CW\*(C`chop\*(C' and \f(CW\*(C`chomp\*(C' and for \f(CW\*(C`schop\*(C' and \f(CW\*(C`schomp\*(C'
have been merged.  The implementation functions **Perl_do_chop()** and
**Perl_do_chomp()**, never part of the public \s-1API,\s0 have been merged and
moved to a static function in *pp.c*.  This shrinks the Perl binary
slightly, and should not affect any code outside the core (unless it is
relying on the order of side-effects when \f(CW\*(C`chomp\*(C' is passed a *list* of
values).

## Selected Bug Fixes

Header "Selected Bug Fixes"

### I/O

Subsection "I/O"

- \(bu
Perl no longer produces this warning:
.Sp
.Vb 2
    $ perl -we \*(Aqopen(my $f, ">", \\my $x); binmode($f, "scalar")\*(Aq
    Use of uninitialized value in binmode at -e line 1.
.Ve

- \(bu
Opening a glob reference via \f(CW\*(C`open($fh, ">", \\*glob)\*(C' no longer
causes the glob to be corrupted when the filehandle is printed to.  This would
cause Perl to crash whenever the glob's contents were accessed
[perl #77492].

- \(bu
PerlIO no longer crashes when called recursively, such as from a signal
handler.  Now it just leaks memory [perl #75556].

- \(bu
Most I/O functions were not warning for unopened handles unless the
\*(L"closed\*(R" and \*(L"unopened\*(R" warnings categories were both enabled.  Now only
\f(CW\*(C`use warnings \*(Aqunopened\*(Aq\*(C' is necessary to trigger these warnings, as
had always been the intention.

- \(bu
There have been several fixes to PerlIO layers:
.Sp
When \f(CW\*(C`binmode(FH, ":crlf")\*(C' pushes the \f(CW\*(C`:crlf\*(C' layer on top of the stack,
it no longer enables crlf layers lower in the stack so as to avoid
unexpected results [perl #38456].
.Sp
Opening a file in \f(CW\*(C`:raw\*(C' mode now does what it advertises to do (first
open the file, then \f(CW\*(C`binmode\*(C' it), instead of simply leaving off the top
layer [perl #80764].
.Sp
The three layers \f(CW\*(C`:pop\*(C', \f(CW\*(C`:utf8\*(C', and \f(CW\*(C`:bytes\*(C' didn't allow stacking when
opening a file.  For example
this:
.Sp
.Vb 1
    open(FH, ">:pop:perlio", "some.file") or die $!;
.Ve
.Sp
would throw an \*(L"Invalid argument\*(R" error.  This has been fixed in this
release [perl #82484].

### Regular Expression Bug Fixes

Subsection "Regular Expression Bug Fixes"

- \(bu
The regular expression engine no longer loops when matching
\f(CW\*(C`"\\N\{LATIN SMALL LIGATURE FF\}" =~ /f+/i\*(C' and similar expressions
[perl #72998] (5.12.1).

- \(bu
The trie runtime code should no longer allocate massive amounts of memory,
fixing #74484.

- \(bu
Syntax errors in \f(CW\*(C`(?\{...\})\*(C' blocks no longer cause panic messages
[perl #2353].

- \(bu
A pattern like \f(CW\*(C`(?:(o)\{2\})?\*(C' no longer causes a \*(L"panic\*(R" error
[perl #39233].

- \(bu
A fatal error in regular expressions containing \f(CW\*(C`(.*?)\*(C' when processing
\s-1UTF-8\s0 data has been fixed [perl #75680] (5.12.2).

- \(bu
An erroneous regular expression engine optimisation that caused regex verbs like
\f(CW*COMMIT sometimes to be ignored has been removed.

- \(bu
The regular expression bracketed character class \f(CW\*(C`[\\8\\9]\*(C' was effectively the
same as \f(CW\*(C`[89\\000]\*(C', incorrectly matching a \s-1NULL\s0 character.  It also gave
incorrect warnings that the \f(CW8 and \f(CW9 were ignored.  Now \f(CW\*(C`[\\8\\9]\*(C' is the
same as \f(CW\*(C`[89]\*(C' and gives legitimate warnings that \f(CW\*(C`\\8\*(C' and \f(CW\*(C`\\9\*(C' are
unrecognized escape sequences, passed-through.

- \(bu
A regular expression match in the right-hand side of a global substitution
(\f(CW\*(C`s///g\*(C') that is in the same scope will no longer cause match variables
to have the wrong values on subsequent iterations.  This can happen when an
array or hash subscript is interpolated in the right-hand side, as in
\f(CW\*(C`s|(.)|@a\{ print($1), /./ \}|g\*(C' [perl #19078].

- \(bu
Several cases in which characters in the Latin-1 non-ASCII range (0x80 to
0xFF) used not to match themselves, or used to match both a character class
and its complement, have been fixed.  For instance, U+00E2 could match both
\f(CW\*(C`\\w\*(C' and \f(CW\*(C`\\W\*(C' [perl #78464] [perl #18281] [perl #60156].

- \(bu
Matching a Unicode character against an alternation containing characters
that happened to match continuation bytes in the former's \s-1UTF8\s0
representation (like \f(CW\*(C`qq\{\\x\{30ab\}\} =~ /\\xab|\\xa9/\*(C') would cause erroneous
warnings [perl #70998].

- \(bu
The trie optimisation was not taking empty groups into account, preventing
\*(L"foo\*(R" from matching \f(CW\*(C`/\\A(?:(?:)foo|bar|zot)\\z/\*(C' [perl #78356].

- \(bu
A pattern containing a \f(CW\*(C`+\*(C' inside a lookahead would sometimes cause an
incorrect match failure in a global match (for example, \f(CW\*(C`/(?=(\\S+))/g\*(C')
[perl #68564].

- \(bu
A regular expression optimisation would sometimes cause a match with a
\f(CW\*(C`\{n,m\}\*(C' quantifier to fail when it should have matched [perl #79152].

- \(bu
Case-insensitive matching in regular expressions compiled under
\f(CW\*(C`use locale\*(C' now works much more sanely when the pattern or target
string is internally encoded in \s-1UTF8.\s0  Previously, under these
conditions the localeness was completely lost.  Now, code points
above 255 are treated as Unicode, but code points between 0 and 255
are treated using the current locale rules, regardless of whether
the pattern or the string is encoded in \s-1UTF8.\s0  The few case-insensitive
matches that cross the 255/256 boundary are not allowed.  For
example, 0xFF does not caselessly match the character at 0x178,
\s-1LATIN CAPITAL LETTER Y WITH DIAERESIS,\s0 because 0xFF may not be \s-1LATIN
SMALL LETTER Y\s0 in the current locale, and Perl has no way of knowing
if that character even exists in the locale, much less what code
point it is.

- \(bu
The \f(CW\*(C`(?|...)\*(C' regular expression construct no longer crashes if the final
branch has more sets of capturing parentheses than any other branch.  This
was fixed in Perl 5.10.1 for the case of a single branch, but that fix did
not take multiple branches into account [perl #84746].

- \(bu
A bug has been fixed in the implementation of \f(CW\*(C`\{...\}\*(C' quantifiers in
regular expressions that prevented the code block in
\f(CW\*(C`/((\\w+)(?\{ print $2 \}))\{2\}/\*(C' from seeing the \f(CW$2 sometimes
[perl #84294].

### Syntax/Parsing Bugs

Subsection "Syntax/Parsing Bugs"

- \(bu
\f(CW\*(C`when (scalar) \{...\}\*(C' no longer crashes, but produces a syntax error
[perl #74114] (5.12.1).

- \(bu
A label right before a string eval (\f(CW\*(C`foo: eval $string\*(C') no longer causes
the label to be associated also with the first statement inside the eval
[perl #74290] (5.12.1).

- \(bu
The \f(CW\*(C`no 5.13.2\*(C' form of \f(CW\*(C`no\*(C' no longer tries to turn on features or
pragmata (like strict) [perl #70075] (5.12.2).

- \(bu
\f(CW\*(C`BEGIN \{require 5.12.0\}\*(C' now behaves as documented, rather than behaving
identically to \f(CW\*(C`use 5.12.0\*(C'.  Previously, \f(CW\*(C`require\*(C' in a \f(CW\*(C`BEGIN\*(C' block
was erroneously executing the \f(CW\*(C`use feature \*(Aq:5.12.0\*(Aq\*(C' and
\f(CW\*(C`use strict\*(C' behaviour, which only \f(CW\*(C`use\*(C' was documented to
provide [perl #69050].

- \(bu
A regression introduced in Perl 5.12.0, making
\f(CW\*(C`my $x = 3; $x = length(undef)\*(C' result in \f(CW$x set to \f(CW3 has been
fixed.  \f(CW$x will now be \f(CW\*(C`undef\*(C' [perl #85508] (5.12.2).

- \(bu
When strict \*(L"refs\*(R" mode is off, \f(CW\*(C`%\{...\}\*(C' in rvalue context returns
\f(CW\*(C`undef\*(C' if its argument is undefined.  An optimisation introduced in Perl
5.12.0 to make \f(CW\*(C`keys %\{...\}\*(C' faster when used as a boolean did not take
this into account, causing \f(CW\*(C`keys %\{+undef\}\*(C' (and \f(CW\*(C`keys %$foo\*(C' when
\f(CW$foo is undefined) to be an error, which it should be so in strict
mode only [perl #81750].

- \(bu
Constant-folding used to cause
.Sp
.Vb 1
  $text =~ ( 1 ? /phoo/ : /bear/)
.Ve
.Sp
to turn into
.Sp
.Vb 1
  $text =~ /phoo/
.Ve
.Sp
at compile time.  Now it correctly matches against \f(CW$_ [perl #20444].

- \(bu
Parsing Perl code (either with string \f(CW\*(C`eval\*(C' or by loading modules) from
within a \f(CW\*(C`UNITCHECK\*(C' block no longer causes the interpreter to crash
[perl #70614].

- \(bu
String \f(CW\*(C`eval\*(C's no longer fail after 2 billion scopes have been
compiled [perl #83364].

- \(bu
The parser no longer hangs when encountering certain Unicode characters,
such as U+387 [perl #74022].

- \(bu
Defining a constant with the same name as one of Perl's special blocks
(like \f(CW\*(C`INIT\*(C') stopped working in 5.12.0, but has now been fixed
[perl #78634].

- \(bu
A reference to a literal value used as a hash key (\f(CW$hash\{\\"foo"\}) used
to be stringified, even if the hash was tied [perl #79178].

- \(bu
A closure containing an \f(CW\*(C`if\*(C' statement followed by a constant or variable
is no longer treated as a constant [perl #63540].

- \(bu
\f(CW\*(C`state\*(C' can now be used with attributes.  It
used to mean the same thing as
\f(CW\*(C`my\*(C' if any attributes were present [perl #68658].

- \(bu
Expressions like \f(CW\*(C`@$a > 3\*(C' no longer cause \f(CW$a to be mentioned in
the \*(L"Use of uninitialized value in numeric gt\*(R" warning when \f(CW$a is
undefined (since it is not part of the \f(CW\*(C`>\*(C' expression, but the operand
of the \f(CW\*(C`@\*(C') [perl #72090].

- \(bu
Accessing an element of a package array with a hard-coded number (as
opposed to an arbitrary expression) would crash if the array did not exist.
Usually the array would be autovivified during compilation, but typeglob
manipulation could remove it, as in these two cases which used to crash:
.Sp
.Vb 2
  *d = *a;  print $d[0];
  undef *d; print $d[0];
.Ve

- \(bu
The **-C** command-line option, when used on the shebang line, can now be
followed by other options [perl #72434].

- \(bu
The \f(CW\*(C`B\*(C' module was returning \f(CW\*(C`B::OP\*(C's instead of \f(CW\*(C`B::LOGOP\*(C's for
\f(CW\*(C`entertry\*(C' [perl #80622].  This was due to a bug in the Perl core,
not in \f(CW\*(C`B\*(C' itself.

### Stashes, Globs and Method Lookup

Subsection "Stashes, Globs and Method Lookup"
Perl 5.10.0 introduced a new internal mechanism for caching MROs (method
resolution orders, or lists of parent classes; aka \*(L"isa\*(R" caches) to make
method lookup faster (so \f(CW@ISA arrays would not have to be searched
repeatedly).  Unfortunately, this brought with it quite a few bugs.  Almost
all of these have been fixed now, along with a few MRO-related bugs that
existed before 5.10.0:

- \(bu
The following used to have erratic effects on method resolution, because
the \*(L"isa\*(R" caches were not reset or otherwise ended up listing the wrong
classes.  These have been fixed.

> 
- Aliasing packages by assigning to globs [perl #77358]
Item "Aliasing packages by assigning to globs [perl #77358]"
0

- Deleting packages by deleting their containing stash elements
Item "Deleting packages by deleting their containing stash elements"
.ie n .IP "Undefining the glob containing a package (""undef *Foo::"")" 4
.el .IP "Undefining the glob containing a package (\f(CWundef *Foo::)" 4
Item "Undefining the glob containing a package (undef *Foo::)"
.ie n .IP "Undefining an \s-1ISA\s0 glob (""undef *Foo::ISA"")" 4
.el .IP "Undefining an \s-1ISA\s0 glob (\f(CWundef *Foo::ISA)" 4
Item "Undefining an ISA glob (undef *Foo::ISA)"
.ie n .IP "Deleting an \s-1ISA\s0 stash element (""delete $Foo::\{ISA\}"")" 4
.el .IP "Deleting an \s-1ISA\s0 stash element (\f(CWdelete $Foo::\{ISA\})" 4
Item "Deleting an ISA stash element (delete $Foo::\{ISA\})"
.ie n .IP "Sharing @ISA arrays between classes (via ""*Foo::ISA = \\@Bar::ISA"" or ""*Foo::ISA = *Bar::ISA"") [perl #77238]" 4
.el .IP "Sharing \f(CW@ISA arrays between classes (via \f(CW*Foo::ISA = \\@Bar::ISA or \f(CW*Foo::ISA = *Bar::ISA) [perl #77238]" 4
Item "Sharing @ISA arrays between classes (via *Foo::ISA = @Bar::ISA or *Foo::ISA = *Bar::ISA) [perl #77238]"



> .PD
.Sp
\f(CW\*(C`undef *Foo::ISA\*(C' would even stop a new \f(CW@Foo::ISA array from updating
caches.



- \(bu
Typeglob assignments would crash if the glob's stash no longer existed, so
long as the glob assigned to were named \f(CW\*(C`ISA\*(C' or the glob on either side of
the assignment contained a subroutine.

- \(bu
\f(CW\*(C`PL_isarev\*(C', which is accessible to Perl via \f(CW\*(C`mro::get_isarev\*(C' is now
updated properly when packages are deleted or removed from the \f(CW@ISA of
other classes.  This allows many packages to be created and deleted without
causing a memory leak [perl #75176].

In addition, various other bugs related to typeglobs and stashes have been
fixed:

- \(bu
Some work has been done on the internal pointers that link between symbol
tables (stashes), typeglobs, and subroutines.  This has the effect that
various edge cases related to deleting stashes or stash entries (for example,
<%FOO:: = ()>), and complex typeglob or code-reference aliasing, will no
longer crash the interpreter.

- \(bu
Assigning a reference to a glob copy now assigns to a glob slot instead of
overwriting the glob with a scalar [perl #1804] [perl #77508].

- \(bu
A bug when replacing the glob of a loop variable within the loop has been fixed
[perl #21469].  This
means the following code will no longer crash:
.Sp
.Vb 3
    for $x (...) \{
        *x = *y;
    \}
.Ve

- \(bu
Assigning a glob to a \s-1PVLV\s0 used to convert it to a plain string.  Now it
works correctly, and a \s-1PVLV\s0 can hold a glob.  This would happen when a
nonexistent hash or array element was passed to a subroutine:
.Sp
.Vb 2
  sub \{ $_[0] = *foo \}->($hash\{key\});
  # $_[0] would have been the string "*main::foo"
.Ve
.Sp
It also happened when a glob was assigned to, or returned from, an element
of a tied array or hash [perl #36051].

- \(bu
When trying to report \f(CW\*(C`Use of uninitialized value $Foo::BAR\*(C', crashes could
occur if the glob holding the global variable in question had been detached
from its original stash by, for example, \f(CW\*(C`delete $::\{"Foo::"\}\*(C'.  This has
been fixed by disabling the reporting of variable names in those
cases.

- \(bu
During the restoration of a localised typeglob on scope exit, any
destructors called as a result would be able to see the typeglob in an
inconsistent state, containing freed entries, which could result in a
crash.  This would affect code like this:
.Sp
.Vb 5
  local *@;
  eval \{ die bless [] \}; # puts an object in $@
  sub DESTROY \{
    local $@; # boom
  \}
.Ve
.Sp
Now the glob entries are cleared before any destructors are called.  This
also means that destructors can vivify entries in the glob.  So Perl tries
again and, if the entries are re-created too many times, dies with a
\*(L"panic: gp_free ...\*(R" error message.

- \(bu
If a typeglob is freed while a subroutine attached to it is still
referenced elsewhere, the subroutine is renamed to \f(CW\*(C`_\|_ANON_\|_\*(C' in the same
package, unless the package has been undefined, in which case the \f(CW\*(C`_\|_ANON_\|_\*(C'
package is used.  This could cause packages to be sometimes autovivified,
such as if the package had been deleted.  Now this no longer occurs.
The \f(CW\*(C`_\|_ANON_\|_\*(C' package is also now used when the original package is
no longer attached to the symbol table.  This avoids memory leaks in some
cases [perl #87664].

- \(bu
Subroutines and package variables inside a package whose name ends with
\f(CW\*(C`::\*(C' can now be accessed with a fully qualified name.

### Unicode

Subsection "Unicode"

- \(bu
What has become known as \*(L"the Unicode Bug\*(R" is almost completely resolved in
this release.  Under \f(CW\*(C`use feature \*(Aqunicode_strings\*(Aq\*(C' (which is
automatically selected by \f(CW\*(C`use 5.012\*(C' and above), the internal
storage format of a string no longer affects the external semantics.
[perl #58182].
.Sp
There are two known exceptions:

> 
- 1.
The now-deprecated, user-defined case-changing
functions require utf8-encoded strings to operate.  The \s-1CPAN\s0 module
Unicode::Casing has been written to replace this feature without its
drawbacks, and the feature is scheduled to be removed in 5.16.

- 2.
**quotemeta()** (and its in-line equivalent \f(CW\*(C`\\Q\*(C') can also give different
results depending on whether a string is encoded in \s-1UTF-8.\s0  See
\*(L"The \*(R"Unicode Bug"" in perlunicode.



> 


- \(bu
Handling of Unicode non-character code points has changed.
Previously they were mostly considered illegal, except that in some
place only one of the 66 of them was known.  The Unicode Standard
considers them all legal, but forbids their \*(L"open interchange\*(R".
This is part of the change to allow internal use of any code
point (see \*(L"Core Enhancements\*(R").  Together, these changes resolve
[perl #38722], [perl #51918], [perl #51936], and [perl #63446].

- \(bu
Case-insensitive \f(CW"/i" regular expression matching of Unicode
characters that match multiple characters now works much more as
intended.  For example
.Sp
.Vb 1
 "\\N\{LATIN SMALL LIGATURE FFI\}" =~ /ffi/ui
.Ve
.Sp
and
.Sp
.Vb 1
 "ffi" =~ /\\N\{LATIN SMALL LIGATURE FFI\}/ui
.Ve
.Sp
are both true.  Previously, there were many bugs with this feature.
What hasn't been fixed are the places where the pattern contains the
multiple characters, but the characters are split up by other things,
such as in
.Sp
.Vb 1
 "\\N\{LATIN SMALL LIGATURE FFI\}" =~ /(f)(f)i/ui
.Ve
.Sp
or
.Sp
.Vb 1
 "\\N\{LATIN SMALL LIGATURE FFI\}" =~ /ffi*/ui
.Ve
.Sp
or
.Sp
.Vb 1
 "\\N\{LATIN SMALL LIGATURE FFI\}" =~ /[a-f][f-m][g-z]/ui
.Ve
.Sp
None of these match.
.Sp
Also, this matching doesn't fully conform to the current Unicode
Standard, which asks that the matching be made upon the \s-1NFD\s0
(Normalization Form Decomposed) of the text.  However, as of this
writing (April 2010), the Unicode Standard is currently in flux about
what they will recommend doing with regard in such scenarios.  It may be
that they will throw out the whole concept of multi-character matches.
[perl #71736].

- \(bu
Naming a deprecated character in \f(CW\*(C`\\N\{\f(CINAME\f(CW\}\*(C' no longer leaks memory.

- \(bu
We fixed a bug that could cause \f(CW\*(C`\\N\{\f(CINAME\f(CW\}\*(C' constructs followed by
a single \f(CW"." to be parsed incorrectly [perl #74978] (5.12.1).

- \(bu
\f(CW\*(C`chop\*(C' now correctly handles characters above \f(CW"\\x\{7fffffff\}"
[perl #73246].

- \(bu
Passing to \f(CW\*(C`index\*(C' an offset beyond the end of the string when the string
is encoded internally in \s-1UTF8\s0 no longer causes panics [perl #75898].

- \(bu
**warn()** and **die()** now respect utf8-encoded scalars [perl #45549].

- \(bu
Sometimes the \s-1UTF8\s0 length cache would not be reset on a value
returned by substr, causing \f(CW\*(C`length(substr($uni_string, ...))\*(C' to give
wrong answers.  With \f(CW\*(C`$\{^UTF8CACHE\}\*(C' set to -1, it would also produce
a \*(L"panic\*(R" error message [perl #77692].

### Ties, Overloading and Other Magic

Subsection "Ties, Overloading and Other Magic"

- \(bu
Overloading now works properly in conjunction with tied
variables.  What formerly happened was that most ops checked their
arguments for overloading *before* checking for magic, so for example
an overloaded object returned by a tied array access would usually be
treated as not overloaded [\s-1RT\s0 #57012].

- \(bu
Various instances of magic (like tie methods) being called on tied variables
too many or too few times have been fixed:

> 
- \(bu
\f(CW\*(C`$tied->()\*(C' did not always call \s-1FETCH\s0 [perl #8438].

- \(bu
Filetest operators and \f(CW\*(C`y///\*(C' and \f(CW\*(C`tr///\*(C' were calling \s-1FETCH\s0 too
many times.

- \(bu
The \f(CW\*(C`=\*(C' operator used to ignore magic on its right-hand side if the
scalar happened to hold a typeglob (if a typeglob was the last thing
returned from or assigned to a tied scalar) [perl #77498].

- \(bu
Dereference operators used to ignore magic if the argument was a
reference already (such as from a previous \s-1FETCH\s0) [perl #72144].

- \(bu
\f(CW\*(C`splice\*(C' now calls set-magic (so changes made
by \f(CW\*(C`splice @ISA\*(C' are respected by method calls) [perl #78400].

- \(bu
In-memory files created by \f(CW\*(C`open($fh, ">", \\$buffer)\*(C' were not calling
\s-1FETCH/STORE\s0 at all [perl #43789] (5.12.2).

- \(bu
**utf8::is_utf8()** now respects get-magic (like \f(CW$1) (5.12.1).



> 


- \(bu
Non-commutative binary operators used to swap their operands if the same
tied scalar was used for both operands and returned a different value for
each \s-1FETCH.\s0  For instance, if \f(CW$t returned 2 the first time and 3 the
second, then \f(CW\*(C`$t/$t\*(C' would evaluate to 1.5.  This has been fixed
[perl #87708].

- \(bu
String \f(CW\*(C`eval\*(C' now detects taintedness of overloaded or tied
arguments [perl #75716].

- \(bu
String \f(CW\*(C`eval\*(C' and regular expression matches against objects with string
overloading no longer cause memory corruption or crashes [perl #77084].

- \(bu
readline now honors \f(CW\*(C`<>\*(C' overloading on tied
arguments.

- \(bu
\f(CW\*(C`<expr>\*(C' always respects overloading now if the expression is
overloaded.
.Sp
Because \*(L"<>\ as glob\*(R" was parsed differently from
\*(L"<>\ as filehandle\*(R" from 5.6 onwards, something like \f(CW\*(C`<$foo[0]>\*(C' did
not handle overloading, even if \f(CW$foo[0] was an overloaded object.  This
was contrary to the documentation for overload, and meant that \f(CW\*(C`<>\*(C'
could not be used as a general overloaded iterator operator.

- \(bu
The fallback behaviour of overloading on binary operators was asymmetric
[perl #71286].

- \(bu
Magic applied to variables in the main package no longer affects other packages.
See \*(L"Magic variables outside the main package\*(R" above [perl #76138].

- \(bu
Sometimes magic (ties, taintedness, etc.) attached to variables could cause
an object to last longer than it should, or cause a crash if a tied
variable were freed from within a tie method.  These have been fixed
[perl #81230].

- \(bu
\s-1DESTROY\s0 methods of objects implementing ties are no longer able to crash by
accessing the tied variable through a weak reference [perl #86328].

- \(bu
Fixed a regression of **kill()** when a match variable is used for the
process \s-1ID\s0 to kill [perl #75812].

- \(bu
\f(CW$AUTOLOAD used to remain tainted forever if it ever became tainted.  Now
it is correctly untainted if an autoloaded method is called and the method
name was not tainted.

- \(bu
\f(CW\*(C`sprintf\*(C' now dies when passed a tainted scalar for the format.  It did
already die for arbitrary expressions, but not for simple scalars
[perl #82250].

- \(bu
\f(CW\*(C`lc\*(C', \f(CW\*(C`uc\*(C', \f(CW\*(C`lcfirst\*(C', and \f(CW\*(C`ucfirst\*(C' no longer return untainted strings
when the argument is tainted.  This has been broken since perl 5.8.9
[perl #87336].

### The Debugger

Subsection "The Debugger"

- \(bu
The Perl debugger now also works in taint mode [perl #76872].

- \(bu
Subroutine redefinition works once more in the debugger [perl #48332].

- \(bu
When **-d** is used on the shebang (\f(CW\*(C`#!\*(C') line, the debugger now has access
to the lines of the main program.  In the past, this sometimes worked and
sometimes did not, depending on the order in which things happened to be
arranged in memory [perl #71806].

- \(bu
A possible memory leak when using **caller()** to set
\f(CW@DB::args has been fixed (5.12.2).

- \(bu
Perl no longer stomps on \f(CW$DB::single, \f(CW$DB::trace, and \f(CW$DB::signal
if these variables already have values when \f(CW$^P is assigned to [perl #72422].

- \(bu
\f(CW\*(C`#line\*(C' directives in string evals were not properly updating the arrays
of lines of code (\f(CW\*(C`@\{"_< ..."\}\*(C') that the debugger (or any debugging or
profiling module) uses.  In threaded builds, they were not being updated at
all.  In non-threaded builds, the line number was ignored, so any change to
the existing line number would cause the lines to be misnumbered
[perl #79442].

### Threads

Subsection "Threads"

- \(bu
Perl no longer accidentally clones lexicals in scope within active stack
frames in the parent when creating a child thread [perl #73086].

- \(bu
Several memory leaks in cloning and freeing threaded Perl interpreters have been
fixed [perl #77352].

- \(bu
Creating a new thread when directory handles were open used to cause a
crash, because the handles were not cloned, but simply passed to the new
thread, resulting in a double free.
.Sp
Now directory handles are cloned properly on Windows
and on systems that have a \f(CW\*(C`fchdir\*(C' function.  On other
systems, new threads simply do not inherit directory
handles from their parent threads [perl #75154].

- \(bu
The typeglob \f(CW\*(C`*,\*(C', which holds the scalar variable \f(CW$, (output field
separator), had the wrong reference count in child threads.

- \(bu
[perl #78494] When pipes are shared between threads, the \f(CW\*(C`close\*(C' function
(and any implicit close, such as on thread exit) no longer blocks.

- \(bu
Perl now does a timely cleanup of SVs that are cloned into a new
thread but then discovered to be orphaned (that is, their owners
are *not* cloned).  This eliminates several \*(L"scalars leaked\*(R"
warnings when joining threads.

### Scoping and Subroutines

Subsection "Scoping and Subroutines"

- \(bu
Lvalue subroutines are again able to return copy-on-write scalars.  This
had been broken since version 5.10.0 [perl #75656] (5.12.3).

- \(bu
\f(CW\*(C`require\*(C' no longer causes \f(CW\*(C`caller\*(C' to return the wrong file name for
the scope that called \f(CW\*(C`require\*(C' and other scopes higher up that had the
same file name [perl #68712].

- \(bu
\f(CW\*(C`sort\*(C' with a \f(CW\*(C`($$)\*(C'-prototyped comparison routine used to cause the value
of \f(CW@_ to leak out of the sort.  Taking a reference to \f(CW@_ within the
sorting routine could cause a crash [perl #72334].

- \(bu
Match variables (like \f(CW$1) no longer persist between calls to a sort
subroutine [perl #76026].

- \(bu
Iterating with \f(CW\*(C`foreach\*(C' over an array returned by an lvalue sub now works
[perl #23790].

- \(bu
\f(CW$@ is now localised during calls to \f(CW\*(C`binmode\*(C' to prevent action at a
distance [perl #78844].

- \(bu
Calling a closure prototype (what is passed to an attribute handler for a
closure) now results in a \*(L"Closure prototype called\*(R" error message instead
of a crash [perl #68560].

- \(bu
Mentioning a read-only lexical variable from the enclosing scope in a
string \f(CW\*(C`eval\*(C' no longer causes the variable to become writable
[perl #19135].

### Signals

Subsection "Signals"

- \(bu
Within signal handlers, \f(CW$! is now implicitly localized.

- \(bu
\s-1CHLD\s0 signals are no longer unblocked after a signal handler is called if
they were blocked before by \f(CW\*(C`POSIX::sigprocmask\*(C' [perl #82040].

- \(bu
A signal handler called within a signal handler could cause leaks or
double-frees.  Now fixed [perl #76248].

### Miscellaneous Memory Leaks

Subsection "Miscellaneous Memory Leaks"

- \(bu
Several memory leaks when loading \s-1XS\s0 modules were fixed (5.12.2).

- \(bu
**substr()**,
**pos()**, **keys()**,
and **vec()** could, when used in combination
with lvalues, result in leaking the scalar value they operate on, and cause its
destruction to happen too late.  This has now been fixed.

- \(bu
The postincrement and postdecrement operators, \f(CW\*(C`++\*(C' and \f(CW\*(C`--\*(C', used to cause
leaks when used on references.  This has now been fixed.

- \(bu
Nested \f(CW\*(C`map\*(C' and \f(CW\*(C`grep\*(C' blocks no longer leak memory when processing
large lists [perl #48004].

- \(bu
\f(CW\*(C`use \f(CIVERSION\f(CW\*(C' and \f(CW\*(C`no \f(CIVERSION\f(CW\*(C' no longer leak memory [perl #78436]
[perl #69050].

- \(bu
\f(CW\*(C`.=\*(C' followed by \f(CW\*(C`<>\*(C' or \f(CW\*(C`readline\*(C' would leak memory if \f(CW$/
contained characters beyond the octet range and the scalar assigned to
happened to be encoded as \s-1UTF8\s0 internally [perl #72246].

- \(bu
\f(CW\*(C`eval \*(AqBEGIN\{die\}\*(Aq\*(C' no longer leaks memory on non-threaded builds.

### Memory Corruption and Crashes

Subsection "Memory Corruption and Crashes"

- \(bu
**glob()** no longer crashes when \f(CW%File::Glob:: is empty and
\f(CW\*(C`CORE::GLOBAL::glob\*(C' isn't present [perl #75464] (5.12.2).

- \(bu
**readline()** has been fixed when interrupted by signals so it no longer
returns the \*(L"same thing\*(R" as before or random memory.

- \(bu
When assigning a list with duplicated keys to a hash, the assignment used to
return garbage and/or freed values:
.Sp
.Vb 1
    @a = %h = (list with some duplicate keys);
.Ve
.Sp
This has now been fixed [perl #31865].

- \(bu
The mechanism for freeing objects in globs used to leave dangling
pointers to freed SVs, meaning Perl users could see corrupted state
during destruction.
.Sp
Perl now frees only the affected slots of the \s-1GV,\s0 rather than freeing
the \s-1GV\s0 itself.  This makes sure that there are no dangling refs or
corrupted state during destruction.

- \(bu
The interpreter no longer crashes when freeing deeply-nested arrays of
arrays.  Hashes have not been fixed yet [perl #44225].

- \(bu
Concatenating long strings under \f(CW\*(C`use encoding\*(C' no longer causes Perl to
crash [perl #78674].

- \(bu
Calling \f(CW\*(C`->import\*(C' on a class lacking an import method could corrupt
the stack, resulting in strange behaviour.  For instance,
.Sp
.Vb 1
  push @a, "foo", $b = bar->import;
.Ve
.Sp
would assign \*(L"foo\*(R" to \f(CW$b [perl #63790].

- \(bu
The \f(CW\*(C`recv\*(C' function could crash when called with the \s-1MSG_TRUNC\s0 flag
[perl #75082].

- \(bu
\f(CW\*(C`formline\*(C' no longer crashes when passed a tainted format picture.  It also
taints \f(CW$^A now if its arguments are tainted [perl #79138].

- \(bu
A bug in how we process filetest operations could cause a segfault.
Filetests don't always expect an op on the stack, so we now use
TOPs only if we're sure that we're not \f(CW\*(C`stat\*(C'ing the \f(CW\*(C`_\*(C' filehandle.
This is indicated by \f(CW\*(C`OPf_KIDS\*(C' (as checked in ck_ftst) [perl #74542]
(5.12.1).

- \(bu
**unpack()** now handles scalar context correctly for \f(CW%32H and \f(CW%32u,
fixing a potential crash.  **split()** would crash because the third item
on the stack wasn't the regular expression it expected.  \f(CW\*(C`unpack("%2H",
...)\*(C' would return both the unpacked result and the checksum on the stack,
as would \f(CW\*(C`unpack("%2u", ...)\*(C' [perl #73814] (5.12.2).

### Fixes to Various Perl Operators

Subsection "Fixes to Various Perl Operators"

- \(bu
The \f(CW\*(C`&\*(C', \f(CW\*(C`|\*(C', and \f(CW\*(C`^\*(C' bitwise operators no longer coerce read-only arguments
[perl #20661].

- \(bu
Stringifying a scalar containing \*(L"-0.0\*(R" no longer has the effect of turning
false into true [perl #45133].

- \(bu
Some numeric operators were converting integers to floating point,
resulting in loss of precision on 64-bit platforms [perl #77456].

- \(bu
**sprintf()** was ignoring locales when called with constant arguments
[perl #78632].

- \(bu
Combining the vector (\f(CW%v) flag and dynamic precision would
cause \f(CW\*(C`sprintf\*(C' to confuse the order of its arguments, making it
treat the string as the precision and vice-versa [perl #83194].

### Bugs Relating to the C \s-1API\s0

Subsection "Bugs Relating to the C API"

- \(bu
The C-level \f(CW\*(C`lex_stuff_pvn\*(C' function would sometimes cause a spurious
syntax error on the last line of the file if it lacked a final semicolon
[perl #74006] (5.12.1).

- \(bu
The \f(CW\*(C`eval_sv\*(C' and \f(CW\*(C`eval_pv\*(C' C functions now set \f(CW$@ correctly when
there is a syntax error and no \f(CW\*(C`G_KEEPERR\*(C' flag, and never set it if the
\f(CW\*(C`G_KEEPERR\*(C' flag is present [perl #3719].

- \(bu
The \s-1XS\s0 multicall \s-1API\s0 no longer causes subroutines to lose reference counts
if called via the multicall interface from within those very subroutines.
This affects modules like List::Util.  Calling one of its functions with an
active subroutine as the first argument could cause a crash [perl #78070].

- \(bu
The \f(CW\*(C`SvPVbyte\*(C' function available to \s-1XS\s0 modules now calls magic before
downgrading the \s-1SV,\s0 to avoid warnings about wide characters [perl #72398].

- \(bu
The ref types in the typemap for \s-1XS\s0 bindings now support magical variables
[perl #72684].

- \(bu
\f(CW\*(C`sv_catsv_flags\*(C' no longer calls \f(CW\*(C`mg_get\*(C' on its second argument (the
source string) if the flags passed to it do not include \s-1SV_GMAGIC.\s0  So it
now matches the documentation.

- \(bu
\f(CW\*(C`my_strftime\*(C' no longer leaks memory.  This fixes a memory leak in
\f(CW\*(C`POSIX::strftime\*(C' [perl #73520].

- \(bu
*\s-1XSUB\s0.h* now correctly redefines fgets under \s-1PERL_IMPLICIT_SYS\s0 [perl #55049]
(5.12.1).

- \(bu
\s-1XS\s0 code using **fputc()** or **fputs()** on Windows could cause an error
due to their arguments being swapped [perl #72704] (5.12.1).

- \(bu
A possible segfault in the \f(CW\*(C`T_PTROBJ\*(C' default typemap has been fixed
(5.12.2).

- \(bu
A bug that could cause \*(L"Unknown error\*(R" messages when
\f(CW\*(C`call_sv(code, G_EVAL)\*(C' is called from an \s-1XS\s0 destructor has been fixed
(5.12.2).

## Known Problems

Header "Known Problems"
This is a list of significant unresolved issues which are regressions
from earlier versions of Perl or which affect widely-used \s-1CPAN\s0 modules.

- \(bu
\f(CW\*(C`List::Util::first\*(C' misbehaves in the presence of a lexical \f(CW$_
(typically introduced by \f(CW\*(C`my $_\*(C' or implicitly by \f(CW\*(C`given\*(C').  The variable
that gets set for each iteration is the package variable \f(CW$_, not the
lexical \f(CW$_.
.Sp
A similar issue may occur in other modules that provide functions which
take a block as their first argument, like
.Sp
.Vb 1
    foo \{ ... $_ ...\} list
.Ve
.Sp
See also: <https://github.com/Perl/perl5/issues/9798>

- \(bu
**readline()** returns an empty string instead of a cached previous value
when it is interrupted by a signal

- \(bu
The changes in prototype handling break Switch.  A patch has been sent
upstream and will hopefully appear on \s-1CPAN\s0 soon.

- \(bu
The upgrade to *ExtUtils-MakeMaker-6.57_05* has caused
some tests in the *Module-Install* distribution on \s-1CPAN\s0 to
fail. (Specifically, *02_mymeta.t* tests 5 and 21; *18_all_from.t*
tests 6 and 15; *19_authors.t* tests 5, 13, 21, and 29; and
*20_authors_with_special_characters.t* tests 6, 15, and 23 in version
1.00 of that distribution now fail.)

- \(bu
On \s-1VMS,\s0 \f(CW\*(C`Time::HiRes\*(C' tests will fail due to a bug in the \s-1CRTL\s0's
implementation of \f(CW\*(C`setitimer\*(C': previous timer values would be cleared
if a timer expired but not if the timer was reset before expiring.  \s-1HP\s0
OpenVMS Engineering have corrected the problem and will release a patch
in due course (Quix case # \s-1QXCM1001115136\s0).

- \(bu
On \s-1VMS,\s0 there were a handful of \f(CW\*(C`Module::Build\*(C' test failures we didn't
get to before the release; please watch \s-1CPAN\s0 for updates.

## Errata

Header "Errata"

### \fBkeys(), \fBvalues(), and \fBeach() work on arrays

Subsection "keys(), values(), and each() work on arrays"
You can now use the **keys()**, **values()**, and **each()** builtins on arrays;
previously you could use them only on hashes.  See perlfunc for details.
This is actually a change introduced in perl 5.12.0, but it was missed from
that release's perl5120delta.
.ie n .SS "**split()** and @_"
.el .SS "**split()** and \f(CW@_"
Subsection "split() and @_"
**split()** no longer modifies \f(CW@_ when called in scalar or void context.
In void context it now produces a \*(L"Useless use of split\*(R" warning.
This was also a perl 5.12.0 change that missed the perldelta.

## Obituary

Header "Obituary"
Randy Kobes, creator of http://kobesearch.cpan.org/ and
contributor/maintainer to several core Perl toolchain modules, passed
away on September 18, 2010 after a battle with lung cancer.  The community
was richer for his involvement.  He will be missed.

## Acknowledgements

Header "Acknowledgements"
Perl 5.14.0 represents one year of development since
Perl 5.12.0 and contains nearly 550,000 lines of changes across nearly
3,000 files from 150 authors and committers.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers.  The following people are known to
have contributed the improvements that became Perl 5.14.0:

Aaron Crane, Abhijit Menon-Sen, Abigail, \*(Aevar Arnfjo\*:r\*(d- Bjarmason,
Alastair Douglas, Alexander Alekseev, Alexander Hartmaier, Alexandr
Ciornii, Alex Davies, Alex Vandiver, Ali Polatel, Allen Smith, Andreas
Ko\*:nig, Andrew Rodland, Andy Armstrong, Andy Dougherty, Aristotle
Pagaltzis, Arkturuz, Arvan, A. Sinan Unur, Ben Morrow, Bo Lindbergh,
Boris Ratner, Brad Gilbert, Bram, brian d foy, Brian Phillips, Casey
West, Charles Bailey, Chas. Owens, Chip Salzenberg, Chris 'BinGOs'
Williams, chromatic, Craig A. Berry, Curtis Jewell, Dagfinn Ilmari
Mannsa\*oker, Dan Dascalescu, Dave Rolsky, David Caldwell, David Cantrell,
David Golden, David Leadbeater, David Mitchell, David Wheeler, Eric
Brine, Father Chrysostomos, Fingle Nark, Florian Ragwitz, Frank Wiegand,
Franz Fasching, Gene Sullivan, George Greer, Gerard Goossen, Gisle Aas,
Goro Fuji, Grant McLean, gregor herrmann, H.Merijn Brand, Hongwen Qiu,
Hugo van der Sanden, Ian Goodacre, James E Keenan, James Mastros, Jan
Dubois, Jay Hannah, Jerry D. Hedden, Jesse Vincent, Jim Cromie, Jirka
HruXka, John Peacock, Joshua ben Jore, Joshua Pritikin, Karl Williamson,
Kevin Ryde, kmx, Lars \s-1DXXXXXX XXX,\s0 Larwan Berke, Leon Brocard, Leon
Timmermans, Lubomir Rintel, Lukas Mai, Maik Hentsche, Marty Pauley,
Marvin Humphrey, Matt Johnson, Matt S Trout, Max Maischein, Michael
Breen, Michael Fig, Michael G Schwern, Michael Parker, Michael Stevens,
Michael Witten, Mike Kelly, Moritz Lenz, Nicholas Clark, Nick Cleaton,
Nick Johnston, Nicolas Kaiser, Niko Tyni, Noirin Shirley, Nuno Carvalho,
Paul Evans, Paul Green, Paul Johnson, Paul Marquess, Peter J. Holzer,
Peter John Acklam, Peter Martini, Philippe Bruhat (BooK), Piotr Fusik,
Rafael Garcia-Suarez, Rainer Tammer, Reini Urban, Renee Baecker, Ricardo
Signes, Richard Mo\*:hn, Richard Soderberg, Rob Hoelz, Robin Barker, Ruslan
Zakirov, Salvador Fandin\*~o, Salvador Ortiz Garcia, Shlomi Fish, Sinan
Unur, Sisyphus, Slaven Rezic, Steffen Mu\*:ller, Steve Hay, Steven
Schubiger, Steve Peters, Sullivan Beck, Tatsuhiko Miyagawa, Tim Bunce,
Todd Rinaldo, Tom Christiansen, Tom Hukins, Tony Cook, Tye McQueen,
Vadim Konovalov, Vernon Lyon, Vincent Pit, Walt Mankowski, Wolfram
Humann, Yves Orton, Zefram, and Zsba\*'n Ambrus.

This is woefully incomplete as it's automatically generated from version
control history.  In particular, it doesn't include the names of the
(very much appreciated) contributors who reported issues in previous
versions of Perl that helped make Perl 5.14.0 better. For a more complete
list of all of Perl's historical contributors, please see the \f(CW\*(C`AUTHORS\*(C'
file in the Perl 5.14.0 distribution.

Many of the changes included in this version originated in the \s-1CPAN\s0
modules included in Perl's core. We're grateful to the entire \s-1CPAN\s0
community for helping Perl to flourish.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the Perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org.  This points to a closed subscription
unarchived mailing list, which includes all the core committers, who are able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported.  Please use this address for
security issues in the Perl core *only*, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
