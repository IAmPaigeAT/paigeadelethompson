+++
manpage_section = "1"
title = "perl582delta(1)"
detected_package_version = "5.34.1"
manpage_name = "perl582delta"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "how", "to", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
operating_system = "macos"
operating_system_version = "15.3"
author = "None Specified"
manpage_format = "troff"
description = "This document describes differences between the 5.8.1 release and the 5.8.2 release. If you are upgrading from an earlier release such as 5.6.1, first read the perl58delta, which describes differences between 5.6.0 and 5.8.0, and the perl581delta,..."
date = "2022-02-19"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL582DELTA 1"
PERL582DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl582delta - what is new for perl v5.8.2

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.8.1 release and
the 5.8.2 release.

If you are upgrading from an earlier release such as 5.6.1, first read
the perl58delta, which describes differences between 5.6.0 and
5.8.0, and the perl581delta, which describes differences between
5.8.0 and 5.8.1.

## Incompatible Changes

Header "Incompatible Changes"
For threaded builds for modules calling certain re-entrant system calls,
binary compatibility was accidentally lost between 5.8.0 and 5.8.1.
Binary compatibility with 5.8.0 has been restored in 5.8.2, which
necessitates breaking compatibility with 5.8.1. We see this as the
lesser of two evils.

This will only affect people who have a threaded perl 5.8.1, and compiled
modules which use these calls, and now attempt to run the compiled modules
with 5.8.2. The fix is to re-compile and re-install the modules using 5.8.2.

## Core Enhancements

Header "Core Enhancements"

### Hash Randomisation

Subsection "Hash Randomisation"
The hash randomisation introduced with 5.8.1 has been amended. It
transpired that although the implementation introduced in 5.8.1 was source
compatible with 5.8.0, it was not binary compatible in certain cases. 5.8.2
contains an improved implementation which is both source and binary
compatible with both 5.8.0 and 5.8.1, and remains robust against the form of
attack which prompted the change for 5.8.1.

We are grateful to the Debian project for their input in this area.
See \*(L"Algorithmic Complexity Attacks\*(R" in perlsec for the original
rationale behind this change.

### Threading

Subsection "Threading"
Several memory leaks associated with variables shared between threads
have been fixed.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules And Pragmata

Subsection "Updated Modules And Pragmata"
The following modules and pragmata have been updated since Perl 5.8.1:

- Devel::PPPort
Item "Devel::PPPort"
0

- Digest::MD5
Item "Digest::MD5"

- I18N::LangTags
Item "I18N::LangTags"

- libnet
Item "libnet"

- MIME::Base64
Item "MIME::Base64"

- Pod::Perldoc
Item "Pod::Perldoc"

- strict
Item "strict"
.PD
Documentation improved

- Tie::Hash
Item "Tie::Hash"
Documentation improved

- Time::HiRes
Item "Time::HiRes"
0

- Unicode::Collate
Item "Unicode::Collate"

- Unicode::Normalize
Item "Unicode::Normalize"

- \s-1UNIVERSAL\s0
Item "UNIVERSAL"
.PD
Documentation improved

## Selected Bug Fixes

Header "Selected Bug Fixes"
Some syntax errors involving unrecognized filetest operators are now handled
correctly by the parser.

## Changed Internals

Header "Changed Internals"
Interpreter initialization is more complete when -DMULTIPLICITY is off.
This should resolve problems with initializing and destroying the Perl
interpreter more than once in a single process.

## Platform Specific Problems

Header "Platform Specific Problems"
Dynamic linker flags have been tweaked for Solaris and \s-1OS X,\s0 which should
solve problems seen while building some \s-1XS\s0 modules.

Bugs in \s-1OS/2\s0 sockets and tmpfile have been fixed.

In \s-1OS X\s0 \f(CW\*(C`setreuid\*(C' and friends are troublesome - perl will now work
around their problems as best possible.

## Future Directions

Header "Future Directions"
Starting with 5.8.3 we intend to make more frequent maintenance releases,
with a smaller number of changes in each. The intent is to propagate
bug fixes out to stable releases more rapidly and make upgrading stable
releases less of an upheaval. This should give end users more
flexibility in their choice of upgrade timing, and allow them easier
assessment of the impact of upgrades. The current plan is for code freezes
as follows

- \(bu
5.8.3 23:59:59 \s-1GMT,\s0 Wednesday December 31st 2003

- \(bu
5.8.4 23:59:59 \s-1GMT,\s0 Wednesday March 31st 2004

- \(bu
5.8.5 23:59:59 \s-1GMT,\s0 Wednesday June 30th 2004

with the release following soon after, when testing is complete.

See \*(L"Future Directions\*(R" in perl581delta for more soothsaying.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://bugs.perl.org/.  There may also be
information at http://www.perl.com/, the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.  You can browse and search
the Perl 5 bugs at http://bugs.perl.org/

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for exhaustive details on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
