+++
keywords = ["header", "see", "also", "fishasum", "is", "implemented", "using", "the", "perl", "module", "digest", "sha"]
description = "Running shasum is often the quickest way to compute s-1SHAs0 message digests.  The user simply feeds data to the script through files or standard input, and then collects the results from standard output. The following command shows how to compute ..."
manpage_section = "1"
operating_system = "macos"
author = "None Specified"
date = "2024-12-14"
operating_system_version = "15.3"
manpage_name = "shasum"
title = "shasum(1)"
manpage_format = "troff"
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "SHASUM 1"
SHASUM 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

shasum - Print or Check SHA Checksums

## SYNOPSIS

Header "SYNOPSIS"
.Vb 3
 Usage: shasum [OPTION]... [FILE]...
 Print or check SHA checksums.
 With no FILE, or when FILE is -, read standard input.

   -a, --algorithm   1 (default), 224, 256, 384, 512, 512224, 512256
   -b, --binary      read in binary mode
   -c, --check       read SHA sums from the FILEs and check them
       --tag         create a BSD-style checksum
   -t, --text        read in text mode (default)
   -U, --UNIVERSAL   read in Universal Newlines mode
                         produces same digest on Windows/Unix/Mac
   -0, --01          read in BITS mode
                         ASCII \*(Aq0\*(Aq interpreted as 0-bit,
                         ASCII \*(Aq1\*(Aq interpreted as 1-bit,
                         all other characters ignored

 The following five options are useful only when verifying checksums:
       --ignore-missing  don\*(Aqt fail or report status for missing files
   -q, --quiet           don\*(Aqt print OK for each successfully verified file
   -s, --status          don\*(Aqt output anything, status code shows success
       --strict          exit non-zero for improperly formatted checksum lines
   -w, --warn            warn about improperly formatted checksum lines

   -h, --help        display this help and exit
   -v, --version     output version information and exit

 When verifying SHA-512/224 or SHA-512/256 checksums, indicate the
 algorithm explicitly using the -a option, e.g.

   shasum -a 512224 -c checksumfile

 The sums are computed as described in FIPS PUB 180-4.  When checking,
 the input should be a former output of this program.  The default
 mode is to print a line with checksum, a character indicating type
 (\`*\*(Aq for binary, \` \*(Aq for text, \`U\*(Aq for UNIVERSAL, \`^\*(Aq for BITS),
 and name for each FILE.  The line starts with a \`\\\*(Aq character if the
 FILE name contains either newlines or backslashes, which are then
 replaced by the two-character sequences \`\\n\*(Aq and \`\\\\\*(Aq respectively.

 Report shasum bugs to mshelor@cpan.org
.Ve

## DESCRIPTION

Header "DESCRIPTION"
Running *shasum* is often the quickest way to compute \s-1SHA\s0 message
digests.  The user simply feeds data to the script through files or
standard input, and then collects the results from standard output.

The following command shows how to compute digests for typical inputs
such as the \s-1NIST\s0 test vector \*(L"abc\*(R":

.Vb 1
        perl -e "print qq(abc)" | shasum
.Ve

Or, if you want to use \s-1SHA-256\s0 instead of the default \s-1SHA-1,\s0 simply say:

.Vb 1
        perl -e "print qq(abc)" | shasum -a 256
.Ve

Since *shasum* mimics the behavior of the combined \s-1GNU\s0 *sha1sum*,
*sha224sum*, *sha256sum*, *sha384sum*, and *sha512sum* programs,
you can install this script as a convenient drop-in replacement.

Unlike the \s-1GNU\s0 programs, *shasum* encompasses the full \s-1SHA\s0 standard by
allowing partial-byte inputs.  This is accomplished through the \s-1BITS\s0
option (*-0*).  The following example computes the \s-1SHA-224\s0 digest of
the 7-bit message *0001100*:

.Vb 1
        perl -e "print qq(0001100)" | shasum -0 -a 224
.Ve

## AUTHOR

Header "AUTHOR"
Copyright (C) 2003-2018 Mark Shelor <mshelor@cpan.org>.

## SEE ALSO

Header "SEE ALSO"
*shasum* is implemented using the Perl module Digest::SHA.
