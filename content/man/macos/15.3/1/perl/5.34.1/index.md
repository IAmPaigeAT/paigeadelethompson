+++
title = "perl(1)"
detected_package_version = "5.34.1"
manpage_format = "troff"
operating_system = "macos"
operating_system_version = "15.3"
date = "2022-02-26"
author = "None Specified"
manpage_section = "1"
description = "Perl officially stands for Practical Extraction and Report Language, except when it doesnt. Perl was originally a language optimized for scanning arbitrary text files, extracting information from those text files, and printing reports based on th..."
manpage_name = "perl"
keywords = ["header", "see", "also", "vb", "4", "https", "www", "perl", "org", "the", "homepage", "com", "articles", "cpan", "comprehensive", "archive", "pm", "mongers", "ve", "diagnostics", "using", "f", "cw", "c", "use", "strict", "pragma", "ensures", "that", "all", "variables", "are", "properly", "declared", "and", "prevents", "other", "misuses", "of", "legacy", "features", "warnings", "produces", "some", "lovely", "one", "can", "fb-w", "flag", "but", "its", "is", "normally", "discouraged", "because", "it", "gets", "applied", "to", "executed", "code", "including", "not", "under", "your", "control", "perldiag", "for", "explanations", "s", "automatically", "turns", "terse", "errors", "into", "these", "longer", "forms", "compilation", "will", "tell", "you", "line", "number", "error", "with", "an", "indication", "next", "token", "or", "type", "was", "be", "examined", "in", "a", "script", "passed", "via", "fb-e", "switches", "each", "counted", "as", "setuid", "scripts", "have", "additional", "constraints", "produce", "messages", "such", "l", "insecure", "dependency", "r", "perlsec", "did", "we", "mention", "should", "definitely", "consider", "fbuse", "bugs", "behavior", "implied", "by", "mandatory", "at", "mercy", "machine", "definitions", "various", "operations", "casting", "fbatof", "floating-point", "output", "fbsprintf", "if", "stdio", "requires", "seek", "eof", "between", "reads", "writes", "on", "particular", "stream", "so", "does", "this", "doesn", "t", "apply", "fbsysread", "fbsyswrite", "while", "none", "built-in", "data", "types", "any", "arbitrary", "size", "limits", "apart", "from", "memory", "there", "still", "few", "given", "variable", "name", "may", "than", "251", "characters", "numbers", "displayed", "internally", "stored", "short", "integers", "they", "limited", "maximum", "65535", "higher", "usually", "being", "affected", "wraparound", "submit", "bug", "reports", "sure", "include", "full", "configuration", "information", "myconfig", "program", "source", "tree", "v", "github", "perl5", "issues", "actually", "stands", "pathologically", "eclectic", "rubbish", "lister", "don", "anyone", "i", "said", "notes", "motto", "more", "way", "do", "divining", "how", "many", "left", "exercise", "reader", "three", "principal", "virtues", "programmer", "laziness", "impatience", "hubris", "camel", "book", "why"]
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL 1"
PERL 1 "2022-02-26" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl - The Perl 5 language interpreter

## SYNOPSIS

Header "SYNOPSIS"
**perl**	[\ **-sTtuUWX**\ ]
	[\ **-hv**\ ]\ [\ **-V**[:*configvar*]\ ]
	[\ **-cw**\ ]\ [\ **-d**[**t**][:*debugger*]\ ]\ [\ **-D**[*number/list*]\ ]
	[\ **-pna**\ ]\ [\ **-F***pattern*\ ]\ [\ **-l**[*octal*]\ ]\ [\ **-0**[*octal/hexadecimal*]\ ]
	[\ **-I***dir*\ ]\ [\ **-m**[**-**]*module*\ ]\ [\ **-M**[**-**]*'module...'*\ ]\ [\ **-f**\ ]
	[\ **-C\ [\f(BInumber/list\fB]\ **]
	[\ **-S**\ ]
	[\ **-x**[*dir*]\ ]
	[\ **-i**[*extension*]\ ]
	[\ [**-e**|**-E**]\ *'command'*\ ]\ [\ **--**\ ]\ [\ *programfile*\ ]\ [\ *argument*\ ]...

For more information on these options, you can run \f(CW\*(C`perldoc perlrun\*(C'.

## GETTING HELP

Header "GETTING HELP"
The *perldoc* program gives you access to all the documentation that comes
with Perl.  You can get more documentation, tutorials and community support
online at <https://www.perl.org/>.

If you're new to Perl, you should start by running \f(CW\*(C`perldoc perlintro\*(C',
which is a general intro for beginners and provides some background to help
you navigate the rest of Perl's extensive documentation.  Run \f(CW\*(C`perldoc
perldoc\*(C' to learn more things you can do with *perldoc*.

For ease of access, the Perl manual has been split up into several sections.

### Overview

Subsection "Overview"
.Vb 4
    perl                Perl overview (this section)
    perlintro           Perl introduction for beginners
    perlrun             Perl execution and options
    perltoc             Perl documentation table of contents
.Ve

### Tutorials

Subsection "Tutorials"
.Vb 3
    perlreftut          Perl references short introduction
    perldsc             Perl data structures intro
    perllol             Perl data structures: arrays of arrays

    perlrequick         Perl regular expressions quick start
    perlretut           Perl regular expressions tutorial

    perlootut           Perl OO tutorial for beginners

    perlperf            Perl Performance and Optimization Techniques

    perlstyle           Perl style guide

    perlcheat           Perl cheat sheet
    perltrap            Perl traps for the unwary
    perldebtut          Perl debugging tutorial

    perlfaq             Perl frequently asked questions
      perlfaq1          General Questions About Perl
      perlfaq2          Obtaining and Learning about Perl
      perlfaq3          Programming Tools
      perlfaq4          Data Manipulation
      perlfaq5          Files and Formats
      perlfaq6          Regexes
      perlfaq7          Perl Language Issues
      perlfaq8          System Interaction
      perlfaq9          Networking
.Ve

### Reference Manual

Subsection "Reference Manual"
.Vb 10
    perlsyn             Perl syntax
    perldata            Perl data structures
    perlop              Perl operators and precedence
    perlsub             Perl subroutines
    perlfunc            Perl built-in functions
      perlopentut       Perl open() tutorial
      perlpacktut       Perl pack() and unpack() tutorial
    perlpod             Perl plain old documentation
    perlpodspec         Perl plain old documentation format specification
    perldocstyle        Perl style guide for core docs
    perlpodstyle        Perl POD style guide
    perldiag            Perl diagnostic messages
    perldeprecation     Perl deprecations
    perllexwarn         Perl warnings and their control
    perldebug           Perl debugging
    perlvar             Perl predefined variables
    perlre              Perl regular expressions, the rest of the story
    perlrebackslash     Perl regular expression backslash sequences
    perlrecharclass     Perl regular expression character classes
    perlreref           Perl regular expressions quick reference
    perlref             Perl references, the rest of the story
    perlform            Perl formats
    perlobj             Perl objects
    perltie             Perl objects hidden behind simple variables
      perldbmfilter     Perl DBM filters

    perlipc             Perl interprocess communication
    perlfork            Perl fork() information
    perlnumber          Perl number semantics

    perlthrtut          Perl threads tutorial

    perlport            Perl portability guide
    perllocale          Perl locale support
    perluniintro        Perl Unicode introduction
    perlunicode         Perl Unicode support
    perlunicook         Perl Unicode cookbook
    perlunifaq          Perl Unicode FAQ
    perluniprops        Index of Unicode properties in Perl
    perlunitut          Perl Unicode tutorial
    perlebcdic          Considerations for running Perl on EBCDIC platforms

    perlsec             Perl security
    perlsecpolicy       Perl security report handling policy

    perlmod             Perl modules: how they work
    perlmodlib          Perl modules: how to write and use
    perlmodstyle        Perl modules: how to write modules with style
    perlmodinstall      Perl modules: how to install from CPAN
    perlnewmod          Perl modules: preparing a new module for distribution
    perlpragma          Perl modules: writing a user pragma

    perlutil            utilities packaged with the Perl distribution

    perlfilter          Perl source filters

    perldtrace          Perl\*(Aqs support for DTrace

    perlglossary        Perl Glossary
.Ve

### Internals and C Language Interface

Subsection "Internals and C Language Interface"
.Vb 11
    perlembed           Perl ways to embed perl in your C or C++ application
    perldebguts         Perl debugging guts and tips
    perlxstut           Perl XS tutorial
    perlxs              Perl XS application programming interface
    perlxstypemap       Perl XS C/Perl type conversion tools
    perlclib            Internal replacements for standard C library functions
    perlguts            Perl internal functions for those doing extensions
    perlcall            Perl calling conventions from C
    perlmroapi          Perl method resolution plugin interface
    perlreapi           Perl regular expression plugin interface
    perlreguts          Perl regular expression engine internals

    perlapi             Perl API listing (autogenerated)
    perlintern          Perl internal functions (autogenerated)
    perliol             C API for Perl\*(Aqs implementation of IO in Layers
    perlapio            Perl internal IO abstraction interface

    perlhack            Perl hackers guide
    perlsource          Guide to the Perl source tree
    perlinterp          Overview of the Perl interpreter source and how it works
    perlhacktut         Walk through the creation of a simple C code patch
    perlhacktips        Tips for Perl core C code hacking
    perlpolicy          Perl development policies
    perlgov             Perl Rules of Governance
    perlgit             Using git with the Perl repository
.Ve

### History

Subsection "History"
.Vb 10
    perlhist            Perl history records
    perldelta           Perl changes since previous version
    perl5340delta       Perl changes in version 5.34.0
    perl5321delta       Perl changes in version 5.32.1
    perl5320delta       Perl changes in version 5.32.0
    perl5303delta       Perl changes in version 5.30.3
    perl5302delta       Perl changes in version 5.30.2
    perl5301delta       Perl changes in version 5.30.1
    perl5300delta       Perl changes in version 5.30.0
    perl5283delta       Perl changes in version 5.28.3
    perl5282delta       Perl changes in version 5.28.2
    perl5281delta       Perl changes in version 5.28.1
    perl5280delta       Perl changes in version 5.28.0
    perl5263delta       Perl changes in version 5.26.3
    perl5262delta       Perl changes in version 5.26.2
    perl5261delta       Perl changes in version 5.26.1
    perl5260delta       Perl changes in version 5.26.0
    perl5244delta       Perl changes in version 5.24.4
    perl5243delta       Perl changes in version 5.24.3
    perl5242delta       Perl changes in version 5.24.2
    perl5241delta       Perl changes in version 5.24.1
    perl5240delta       Perl changes in version 5.24.0
    perl5224delta       Perl changes in version 5.22.4
    perl5223delta       Perl changes in version 5.22.3
    perl5222delta       Perl changes in version 5.22.2
    perl5221delta       Perl changes in version 5.22.1
    perl5220delta       Perl changes in version 5.22.0
    perl5203delta       Perl changes in version 5.20.3
    perl5202delta       Perl changes in version 5.20.2
    perl5201delta       Perl changes in version 5.20.1
    perl5200delta       Perl changes in version 5.20.0
    perl5184delta       Perl changes in version 5.18.4
    perl5182delta       Perl changes in version 5.18.2
    perl5181delta       Perl changes in version 5.18.1
    perl5180delta       Perl changes in version 5.18.0
    perl5163delta       Perl changes in version 5.16.3
    perl5162delta       Perl changes in version 5.16.2
    perl5161delta       Perl changes in version 5.16.1
    perl5160delta       Perl changes in version 5.16.0
    perl5144delta       Perl changes in version 5.14.4
    perl5143delta       Perl changes in version 5.14.3
    perl5142delta       Perl changes in version 5.14.2
    perl5141delta       Perl changes in version 5.14.1
    perl5140delta       Perl changes in version 5.14.0
    perl5125delta       Perl changes in version 5.12.5
    perl5124delta       Perl changes in version 5.12.4
    perl5123delta       Perl changes in version 5.12.3
    perl5122delta       Perl changes in version 5.12.2
    perl5121delta       Perl changes in version 5.12.1
    perl5120delta       Perl changes in version 5.12.0
    perl5101delta       Perl changes in version 5.10.1
    perl5100delta       Perl changes in version 5.10.0
    perl589delta        Perl changes in version 5.8.9
    perl588delta        Perl changes in version 5.8.8
    perl587delta        Perl changes in version 5.8.7
    perl586delta        Perl changes in version 5.8.6
    perl585delta        Perl changes in version 5.8.5
    perl584delta        Perl changes in version 5.8.4
    perl583delta        Perl changes in version 5.8.3
    perl582delta        Perl changes in version 5.8.2
    perl581delta        Perl changes in version 5.8.1
    perl58delta         Perl changes in version 5.8.0
    perl561delta        Perl changes in version 5.6.1
    perl56delta         Perl changes in version 5.6
    perl5005delta       Perl changes in version 5.005
    perl5004delta       Perl changes in version 5.004
.Ve

### Miscellaneous

Subsection "Miscellaneous"
.Vb 2
    perlbook            Perl book information
    perlcommunity       Perl community information

    perldoc             Look up Perl documentation in Pod format

    perlexperiment      A listing of experimental features in Perl

    perlartistic        Perl Artistic License
    perlgpl             GNU General Public License
.Ve

### Language-Specific

Subsection "Language-Specific"
.Vb 4
    perlcn              Perl for Simplified Chinese (in UTF-8)
    perljp              Perl for Japanese (in EUC-JP)
    perlko              Perl for Korean (in EUC-KR)
    perltw              Perl for Traditional Chinese (in Big5)
.Ve

### Platform-Specific

Subsection "Platform-Specific"
.Vb 10
    perlaix             Perl notes for AIX
    perlamiga           Perl notes for AmigaOS
    perlandroid         Perl notes for Android
    perlbs2000          Perl notes for POSIX-BC BS2000
    perlcygwin          Perl notes for Cygwin
    perldos             Perl notes for DOS
    perlfreebsd         Perl notes for FreeBSD
    perlhaiku           Perl notes for Haiku
    perlhpux            Perl notes for HP-UX
    perlhurd            Perl notes for Hurd
    perlirix            Perl notes for Irix
    perllinux           Perl notes for Linux
    perlmacos           Perl notes for Mac OS (Classic)
    perlmacosx          Perl notes for Mac OS X
    perlnetware         Perl notes for NetWare
    perlopenbsd         Perl notes for OpenBSD
    perlos2             Perl notes for OS/2
    perlos390           Perl notes for OS/390
    perlos400           Perl notes for OS/400
    perlplan9           Perl notes for Plan 9
    perlqnx             Perl notes for QNX
    perlriscos          Perl notes for RISC OS
    perlsolaris         Perl notes for Solaris
    perlsynology        Perl notes for Synology
    perltru64           Perl notes for Tru64
    perlvms             Perl notes for VMS
    perlvos             Perl notes for Stratus VOS
    perlwin32           Perl notes for Windows
.Ve

### Stubs for Deleted Documents

Subsection "Stubs for Deleted Documents"
.Vb 6
    perlboot
    perlbot
    perlrepository
    perltodo
    perltooc
    perltoot
.Ve

On a Unix-like system, these documentation files will usually also be
available as manpages for use with the *man* program.

Some documentation is not available as man pages, so if a
cross-reference is not found by man, try it with perldoc.  Perldoc can
also take you directly to documentation for functions (with the **-f**
switch). See \f(CW\*(C`perldoc --help\*(C' (or \f(CW\*(C`perldoc perldoc\*(C' or \f(CW\*(C`man perldoc\*(C')
for other helpful options perldoc has to offer.

In general, if something strange has gone wrong with your program and you're
not sure where you should look for help, try making your code comply with
**use strict** and **use warnings**.  These will often point out exactly
where the trouble is.

## DESCRIPTION

Header "DESCRIPTION"
Perl officially stands for Practical Extraction and Report Language,
except when it doesn't.

Perl was originally a language optimized for scanning arbitrary
text files, extracting information from those text files, and printing
reports based on that information.  It quickly became a good language
for many system management tasks. Over the years, Perl has grown into
a general-purpose programming language. It's widely used for everything
from quick \*(L"one-liners\*(R" to full-scale application development.

The language is intended to be practical (easy to use, efficient,
complete) rather than beautiful (tiny, elegant, minimal).  It combines
(in the author's opinion, anyway) some of the best features of **sed**,
**awk**, and **sh**, making it familiar and easy to use for Unix users to
whip up quick solutions to annoying problems.  Its general-purpose
programming facilities support procedural, functional, and
object-oriented programming paradigms, making Perl a comfortable
language for the long haul on major projects, whatever your bent.

Perl's roots in text processing haven't been forgotten over the years.
It still boasts some of the most powerful regular expressions to be
found anywhere, and its support for Unicode text is world-class.  It
handles all kinds of structured text, too, through an extensive
collection of extensions.  Those libraries, collected in the \s-1CPAN,\s0
provide ready-made solutions to an astounding array of problems.  When
they haven't set the standard themselves, they steal from the best
\*(-- just like Perl itself.

## AVAILABILITY

Header "AVAILABILITY"
Perl is available for most operating systems, including virtually
all Unix-like platforms.  See \*(L"Supported Platforms\*(R" in perlport
for a listing.

## ENVIRONMENT

Header "ENVIRONMENT"
See \*(L"\s-1ENVIRONMENT\*(R"\s0 in perlrun.

## AUTHOR

Header "AUTHOR"
Larry Wall <larry@wall.org>, with the help of oodles of other folks.

If your Perl success stories and testimonials may be of help to others
who wish to advocate the use of Perl in their applications,
or if you wish to simply express your gratitude to Larry and the
Perl developers, please write to perl-thanks@perl.org .

## FILES

Header "FILES"
.Vb 1
 "@INC"                 locations of perl libraries
.Ve

\*(L"@INC\*(R" above is a reference to the built-in variable of the same name;
see perlvar for more information.

## SEE ALSO

Header "SEE ALSO"
.Vb 4
 https://www.perl.org/       the Perl homepage
 https://www.perl.com/       Perl articles
 https://www.cpan.org/       the Comprehensive Perl Archive
 https://www.pm.org/         the Perl Mongers
.Ve

## DIAGNOSTICS

Header "DIAGNOSTICS"
Using the \f(CW\*(C`use strict\*(C' pragma ensures that all variables are properly
declared and prevents other misuses of legacy Perl features.

The \f(CW\*(C`use warnings\*(C' pragma produces some lovely diagnostics. One can
also use the **-w** flag, but its use is normally discouraged, because
it gets applied to all executed Perl code, including that not under
your control.

See perldiag for explanations of all Perl's diagnostics.  The \f(CW\*(C`use
diagnostics\*(C' pragma automatically turns Perl's normally terse warnings
and errors into these longer forms.

Compilation errors will tell you the line number of the error, with an
indication of the next token or token type that was to be examined.
(In a script passed to Perl via **-e** switches, each
**-e** is counted as one line.)

Setuid scripts have additional constraints that can produce error
messages such as \*(L"Insecure dependency\*(R".  See perlsec.

Did we mention that you should definitely consider using the **use warnings**
pragma?

## BUGS

Header "BUGS"
The behavior implied by the **use warnings** pragma is not mandatory.

Perl is at the mercy of your machine's definitions of various
operations such as type casting, **atof()**, and floating-point
output with **sprintf()**.

If your stdio requires a seek or eof between reads and writes on a
particular stream, so does Perl.  (This doesn't apply to **sysread()**
and **syswrite()**.)

While none of the built-in data types have any arbitrary size limits
(apart from memory size), there are still a few arbitrary limits:  a
given variable name may not be longer than 251 characters.  Line numbers
displayed by diagnostics are internally stored as short integers,
so they are limited to a maximum of 65535 (higher numbers usually being
affected by wraparound).

You may submit your bug reports (be sure to include full configuration
information as output by the myconfig program in the perl source
tree, or by \f(CW\*(C`perl -V\*(C') to <https://github.com/Perl/perl5/issues>.

Perl actually stands for Pathologically Eclectic Rubbish Lister, but
don't tell anyone I said that.

## NOTES

Header "NOTES"
The Perl motto is \*(L"There's more than one way to do it.\*(R"  Divining
how many more is left as an exercise to the reader.

The three principal virtues of a programmer are Laziness,
Impatience, and Hubris.  See the Camel Book for why.
