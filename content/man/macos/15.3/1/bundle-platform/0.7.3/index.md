+++
manpage_section = "1"
manpage_format = "troff"
title = "bundle-platform(1)"
date = "Sun Feb 16 04:48:17 2025"
description = "platform will display information from your Gemfile, Gemfile.lock, and Ruby VM about your platform. For instance, using this Gemfile(5):..."
author = "None Specified"
operating_system = "macos"
detected_package_version = "0.7.3"
operating_system_version = "15.3"
manpage_name = "bundle-platform"
+++

.
"BUNDLE-PLATFORM" "1" "November 2018" "" ""
.

## NAME

**bundle-platform** - Displays platform compatibility information
.

## SYNOPSIS

**bundle platform** [--ruby]
.

## DESCRIPTION

**platform** will display information from your Gemfile, Gemfile\.lock, and Ruby VM about your platform\.
.
.P
For instance, using this Gemfile(5):
.
"" 4
.

```

source "https://rubygems\.org"

ruby "1\.9\.3"

gem "rack"
.
```

.
"" 0
.
.P
If you run **bundle platform** on Ruby 1\.9\.3, it will display the following output:
.
"" 4
.

```

Your platform is: x86_64\-linux

Your app has gems that work on these platforms:
* ruby

Your Gemfile specifies a Ruby version requirement:
* ruby 1\.9\.3

Your current platform satisfies the Ruby version requirement\.
.
```

.
"" 0
.
.P
**platform** will list all the platforms in your **Gemfile\.lock** as well as the **ruby** directive if applicable from your Gemfile(5)\. It will also let you know if the **ruby** directive requirement has been met\. If **ruby** directive doesn\'t match the running Ruby VM, it will tell you what part does not\.
.

## OPTIONS

.

**--ruby**
It will display the ruby directive information, so you don\'t have to parse it from the Gemfile(5)\.

