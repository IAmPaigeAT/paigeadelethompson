+++
title = "perlpragma(1)"
author = "None Specified"
operating_system_version = "15.3"
operating_system = "macos"
description = "A pragma is a module which influences some aspect of the compile time or run time behaviour of Perl, such as f(CW*(C`strict*(C or f(CW*(C`warnings*(C. With Perl 5.10 you are no longer limited to the built in pragmata; you can now create user prag..."
manpage_format = "troff"
detected_package_version = "5.34.1"
manpage_section = "1"
manpage_name = "perlpragma"
date = "2022-02-19"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLPRAGMA 1"
PERLPRAGMA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlpragma - how to write a user pragma

## DESCRIPTION

Header "DESCRIPTION"
A pragma is a module which influences some aspect of the compile time or run
time behaviour of Perl, such as \f(CW\*(C`strict\*(C' or \f(CW\*(C`warnings\*(C'. With Perl 5.10 you
are no longer limited to the built in pragmata; you can now create user
pragmata that modify the behaviour of user functions within a lexical scope.

## A basic example

Header "A basic example"
For example, say you need to create a class implementing overloaded
mathematical operators, and would like to provide your own pragma that
functions much like \f(CW\*(C`use integer;\*(C' You'd like this code

.Vb 1
    use MyMaths;

    my $l = MyMaths->new(1.2);
    my $r = MyMaths->new(3.4);

    print "A: ", $l + $r, "\\n";

    use myint;
    print "B: ", $l + $r, "\\n";

    \{
        no myint;
        print "C: ", $l + $r, "\\n";
    \}

    print "D: ", $l + $r, "\\n";

    no myint;
    print "E: ", $l + $r, "\\n";
.Ve

to give the output

.Vb 5
    A: 4.6
    B: 4
    C: 4.6
    D: 4
    E: 4.6
.Ve

*i.e.*, where \f(CW\*(C`use myint;\*(C' is in effect, addition operations are forced
to integer, whereas by default they are not, with the default behaviour being
restored via \f(CW\*(C`no myint;\*(C'

The minimal implementation of the package \f(CW\*(C`MyMaths\*(C' would be something like
this:

.Vb 10
    package MyMaths;
    use warnings;
    use strict;
    use myint();
    use overload \*(Aq+\*(Aq => sub \{
        my ($l, $r) = @_;
        # Pass 1 to check up one call level from here
        if (myint::in_effect(1)) \{
            int($$l) + int($$r);
        \} else \{
            $$l + $$r;
        \}
    \};

    sub new \{
        my ($class, $value) = @_;
        bless \\$value, $class;
    \}

    1;
.Ve

Note how we load the user pragma \f(CW\*(C`myint\*(C' with an empty list \f(CW\*(C`()\*(C' to
prevent its \f(CW\*(C`import\*(C' being called.

The interaction with the Perl compilation happens inside package \f(CW\*(C`myint\*(C':

.Vb 1
    package myint;

    use strict;
    use warnings;

    sub import \{
        $^H\{"myint/in_effect"\} = 1;
    \}

    sub unimport \{
        $^H\{"myint/in_effect"\} = 0;
    \}

    sub in_effect \{
        my $level = shift // 0;
        my $hinthash = (caller($level))[10];
        return $hinthash->\{"myint/in_effect"\};
    \}

    1;
.Ve

As pragmata are implemented as modules, like any other module, \f(CW\*(C`use myint;\*(C'
becomes

.Vb 4
    BEGIN \{
        require myint;
        myint->import();
    \}
.Ve

and \f(CW\*(C`no myint;\*(C' is

.Vb 4
    BEGIN \{
        require myint;
        myint->unimport();
    \}
.Ve

Hence the \f(CW\*(C`import\*(C' and \f(CW\*(C`unimport\*(C' routines are called at **compile time**
for the user's code.

User pragmata store their state by writing to the magical hash \f(CW\*(C`%^H\*(C',
hence these two routines manipulate it. The state information in \f(CW\*(C`%^H\*(C' is
stored in the optree, and can be retrieved read-only at runtime with \f(CW\*(C`caller()\*(C',
at index 10 of the list of returned results. In the example pragma, retrieval
is encapsulated into the routine \f(CW\*(C`in_effect()\*(C', which takes as parameter
the number of call frames to go up to find the value of the pragma in the
user's script. This uses \f(CW\*(C`caller()\*(C' to determine the value of
\f(CW$^H\{"myint/in_effect"\} when each line of the user's script was called, and
therefore provide the correct semantics in the subroutine implementing the
overloaded addition.

## Key naming

Header "Key naming"
There is only a single \f(CW\*(C`%^H\*(C', but arbitrarily many modules that want
to use its scoping semantics.  To avoid stepping on each other's toes,
they need to be sure to use different keys in the hash.  It is therefore
conventional for a module to use only keys that begin with the module's
name (the name of its main package) and a \*(L"/\*(R" character.  After this
module-identifying prefix, the rest of the key is entirely up to the
module: it may include any characters whatsoever.  For example, a module
\f(CW\*(C`Foo::Bar\*(C' should use keys such as \f(CW\*(C`Foo::Bar/baz\*(C' and \f(CW\*(C`Foo::Bar/$%/_!\*(C'.
Modules following this convention all play nicely with each other.

The Perl core uses a handful of keys in \f(CW\*(C`%^H\*(C' which do not follow this
convention, because they predate it.  Keys that follow the convention
won't conflict with the core's historical keys.

## Implementation details

Header "Implementation details"
The optree is shared between threads.  This means there is a possibility that
the optree will outlive the particular thread (and therefore the interpreter
instance) that created it, so true Perl scalars cannot be stored in the
optree.  Instead a compact form is used, which can only store values that are
integers (signed and unsigned), strings or \f(CW\*(C`undef\*(C' - references and
floating point values are stringified.  If you need to store multiple values
or complex structures, you should serialise them, for example with \f(CW\*(C`pack\*(C'.
The deletion of a hash key from \f(CW\*(C`%^H\*(C' is recorded, and as ever can be
distinguished from the existence of a key with value \f(CW\*(C`undef\*(C' with
\f(CW\*(C`exists\*(C'.

**Don't** attempt to store references to data structures as integers which
are retrieved via \f(CW\*(C`caller\*(C' and converted back, as this will not be threadsafe.
Accesses would be to the structure without locking (which is not safe for
Perl's scalars), and either the structure has to leak, or it has to be
freed when its creating thread terminates, which may be before the optree
referencing it is deleted, if other threads outlive it.
