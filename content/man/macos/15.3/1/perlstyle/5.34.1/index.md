+++
title = "perlstyle(1)"
description = "Each programmer will, of course, have his or her own preferences in regards to formatting, but there are some general guidelines that will make your programs easier to read, understand, and maintain. The most important thing is to use strict and wa..."
operating_system = "macos"
manpage_name = "perlstyle"
date = "2022-02-19"
detected_package_version = "5.34.1"
manpage_format = "troff"
operating_system_version = "15.3"
author = "None Specified"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLSTYLE 1"
PERLSTYLE 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlstyle - Perl style guide

## DESCRIPTION

Header "DESCRIPTION"
Each programmer will, of course, have his or her own preferences in
regards to formatting, but there are some general guidelines that will
make your programs easier to read, understand, and maintain.

The most important thing is to use strict and warnings in all your
code or know the reason why not to.  You may turn them off explicitly for
particular portions of code via \f(CW\*(C`no warnings\*(C' or \f(CW\*(C`no strict\*(C', and this
can be limited to the specific warnings or strict features you wish to
disable.  The **-w** flag and \f(CW$^W variable should not be used for this
purpose since they can affect code you use but did not write, such as
modules from core or \s-1CPAN.\s0

Regarding aesthetics of code lay out, about the only thing Larry
cares strongly about is that the closing curly bracket of
a multi-line \s-1BLOCK\s0 should line up with the keyword that started the construct.
Beyond that, he has other preferences that aren't so strong:

- \(bu
4-column indent.

- \(bu
Opening curly on same line as keyword, if possible, otherwise line up.

- \(bu
Space before the opening curly of a multi-line \s-1BLOCK.\s0

- \(bu
One-line \s-1BLOCK\s0 may be put on one line, including curlies.

- \(bu
No space before the semicolon.

- \(bu
Semicolon omitted in \*(L"short\*(R" one-line \s-1BLOCK.\s0

- \(bu
Space around most operators.

- \(bu
Space around a \*(L"complex\*(R" subscript (inside brackets).

- \(bu
Blank lines between chunks that do different things.

- \(bu
Uncuddled elses.

- \(bu
No space between function name and its opening parenthesis.

- \(bu
Space after each comma.

- \(bu
Long lines broken after an operator (except \f(CW\*(C`and\*(C' and \f(CW\*(C`or\*(C').

- \(bu
Space after last parenthesis matching on current line.

- \(bu
Line up corresponding items vertically.

- \(bu
Omit redundant punctuation as long as clarity doesn't suffer.

Larry has his reasons for each of these things, but he doesn't claim that
everyone else's mind works the same as his does.

Here are some other more substantive style issues to think about:

- \(bu
Just because you *\s-1CAN\s0* do something a particular way doesn't mean that
you *\s-1SHOULD\s0* do it that way.  Perl is designed to give you several
ways to do anything, so consider picking the most readable one.  For
instance
.Sp
.Vb 1
    open(my $fh, \*(Aq<\*(Aq, $foo) || die "Can\*(Aqt open $foo: $!";
.Ve
.Sp
is better than
.Sp
.Vb 1
    die "Can\*(Aqt open $foo: $!" unless open(my $fh, \*(Aq<\*(Aq, $foo);
.Ve
.Sp
because the second way hides the main point of the statement in a
modifier.  On the other hand
.Sp
.Vb 1
    print "Starting analysis\\n" if $verbose;
.Ve
.Sp
is better than
.Sp
.Vb 1
    $verbose && print "Starting analysis\\n";
.Ve
.Sp
because the main point isn't whether the user typed **-v** or not.
.Sp
Similarly, just because an operator lets you assume default arguments
doesn't mean that you have to make use of the defaults.  The defaults
are there for lazy systems programmers writing one-shot programs.  If
you want your program to be readable, consider supplying the argument.
.Sp
Along the same lines, just because you *\s-1CAN\s0* omit parentheses in many
places doesn't mean that you ought to:
.Sp
.Vb 2
    return print reverse sort num values %array;
    return print(reverse(sort num (values(%array))));
.Ve
.Sp
When in doubt, parenthesize.  At the very least it will let some poor
schmuck bounce on the % key in **vi**.
.Sp
Even if you aren't in doubt, consider the mental welfare of the person
who has to maintain the code after you, and who will probably put
parentheses in the wrong place.

- \(bu
Don't go through silly contortions to exit a loop at the top or the
bottom, when Perl provides the \f(CW\*(C`last\*(C' operator so you can exit in
the middle.  Just \*(L"outdent\*(R" it a little to make it more visible:
.Sp
.Vb 7
    LINE:
        for (;;) \{
            statements;
          last LINE if $foo;
            next LINE if /^#/;
            statements;
        \}
.Ve

- \(bu
Don't be afraid to use loop labels\*(--they're there to enhance
readability as well as to allow multilevel loop breaks.  See the
previous example.

- \(bu
Avoid using \f(CW\*(C`grep()\*(C' (or \f(CW\*(C`map()\*(C') or `backticks` in a void context, that is,
when you just throw away their return values.  Those functions all
have return values, so use them.  Otherwise use a \f(CW\*(C`foreach()\*(C' loop or
the \f(CW\*(C`system()\*(C' function instead.

- \(bu
For portability, when using features that may not be implemented on
every machine, test the construct in an eval to see if it fails.  If
you know what version or patchlevel a particular feature was
implemented, you can test \f(CW$] (\f(CW$PERL_VERSION in \f(CW\*(C`English\*(C') to see if it
will be there.  The \f(CW\*(C`Config\*(C' module will also let you interrogate values
determined by the **Configure** program when Perl was installed.

- \(bu
Choose mnemonic identifiers.  If you can't remember what mnemonic means,
you've got a problem.

- \(bu
While short identifiers like \f(CW$gotit are probably ok, use underscores to
separate words in longer identifiers.  It is generally easier to read
\f(CW$var_names_like_this than \f(CW$VarNamesLikeThis, especially for
non-native speakers of English. It's also a simple rule that works
consistently with \f(CW\*(C`VAR_NAMES_LIKE_THIS\*(C'.
.Sp
Package names are sometimes an exception to this rule.  Perl informally
reserves lowercase module names for \*(L"pragma\*(R" modules like \f(CW\*(C`integer\*(C' and
\f(CW\*(C`strict\*(C'.  Other modules should begin with a capital letter and use mixed
case, but probably without underscores due to limitations in primitive
file systems' representations of module names as files that must fit into a
few sparse bytes.

- \(bu
You may find it helpful to use letter case to indicate the scope
or nature of a variable. For example:
.Sp
.Vb 3
    $ALL_CAPS_HERE   constants only (beware clashes with perl vars!)
    $Some_Caps_Here  package-wide global/static
    $no_caps_here    function scope my() or local() variables
.Ve
.Sp
Function and method names seem to work best as all lowercase.
E.g., \f(CW\*(C`$obj->as_string()\*(C'.
.Sp
You can use a leading underscore to indicate that a variable or
function should not be used outside the package that defined it.

- \(bu
If you have a really hairy regular expression, use the \f(CW\*(C`/x\*(C'  or \f(CW\*(C`/xx\*(C'
modifiers and put in some whitespace to make it look a little less like
line noise.
Don't use slash as a delimiter when your regexp has slashes or backslashes.

- \(bu
Use the new \f(CW\*(C`and\*(C' and \f(CW\*(C`or\*(C' operators to avoid having to parenthesize
list operators so much, and to reduce the incidence of punctuation
operators like \f(CW\*(C`&&\*(C' and \f(CW\*(C`||\*(C'.  Call your subroutines as if they were
functions or list operators to avoid excessive ampersands and parentheses.

- \(bu
Use here documents instead of repeated \f(CW\*(C`print()\*(C' statements.

- \(bu
Line up corresponding things vertically, especially if it'd be too long
to fit on one line anyway.
.Sp
.Vb 4
    $IDX = $ST_MTIME;
    $IDX = $ST_ATIME       if $opt_u;
    $IDX = $ST_CTIME       if $opt_c;
    $IDX = $ST_SIZE        if $opt_s;

    mkdir $tmpdir, 0700 or die "can\*(Aqt mkdir $tmpdir: $!";
    chdir($tmpdir)      or die "can\*(Aqt chdir $tmpdir: $!";
    mkdir \*(Aqtmp\*(Aq,   0777 or die "can\*(Aqt mkdir $tmpdir/tmp: $!";
.Ve

- \(bu
Always check the return codes of system calls.  Good error messages should
go to \f(CW\*(C`STDERR\*(C', include which program caused the problem, what the failed
system call and arguments were, and (\s-1VERY IMPORTANT\s0) should contain the
standard system error message for what went wrong.  Here's a simple but
sufficient example:
.Sp
.Vb 1
    opendir(my $dh, $dir)        or die "can\*(Aqt opendir $dir: $!";
.Ve

- \(bu
Line up your transliterations when it makes sense:
.Sp
.Vb 2
    tr [abc]
       [xyz];
.Ve

- \(bu
Think about reusability.  Why waste brainpower on a one-shot when you
might want to do something like it again?  Consider generalizing your
code.  Consider writing a module or object class.  Consider making your
code run cleanly with \f(CW\*(C`use strict\*(C' and \f(CW\*(C`use warnings\*(C' in
effect.  Consider giving away your code.  Consider changing your whole
world view.  Consider... oh, never mind.

- \(bu
Try to document your code and use Pod formatting in a consistent way. Here
are commonly expected conventions:

> 
- \(bu
use \f(CW\*(C`C<>\*(C' for function, variable and module names (and more
generally anything that can be considered part of code, like filehandles
or specific values). Note that function names are considered more readable
with parentheses after their name, that is \f(CW\*(C`function()\*(C'.

- \(bu
use \f(CW\*(C`B<>\*(C' for commands names like **cat** or **grep**.

- \(bu
use \f(CW\*(C`F<>\*(C' or \f(CW\*(C`C<>\*(C' for file names. \f(CW\*(C`F<>\*(C' should
be the only Pod code for file names, but as most Pod formatters render it
as italic, Unix and Windows paths with their slashes and backslashes may
be less readable, and better rendered with \f(CW\*(C`C<>\*(C'.



> 


- \(bu
Be consistent.

- \(bu
Be nice.
