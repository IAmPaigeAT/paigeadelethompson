+++
description = "To start a interactive session, invoke the command and optionally provide the name of a database file.  If the database file does not exist, it will be created.  If the database file does exist, it will be opened."
keywords = ["https", "sqlite", "org", "cli", "html", "br", "fiddle", "a", "webassembly", "build", "of", "the", "app", "sqlite3-doc", "package", "author", "this", "manual", "page", "was", "originally", "written", "by", "andreas", "rottmann", "rotty", "debian", "for", "gnu", "linux", "system", "but", "may", "be", "used", "others", "it", "subsequently", "revised", "bill", "bumgarner", "bbum", "mac", "com", "laszlo", "boszormenyi", "gcs", "hu", "scott", "perry", "numist", "net", "and", "sqlite3", "developers"]
manpage_name = "sqlite3"
manpage_format = "troff"
operating_system_version = "15.3"
operating_system = "macos"
author = "None Specified"
date = "2023-01-01"
detected_package_version = "0"
title = "sqlite3(1)"
manpage_section = "1"
+++




The available commands differ by version and build options, so they
are not listed here. Please refer to your local copy for all available
options.


## OPTIONS

sqlite3
has the following options:

-A\ ARGS...
Run
.archive ARGS
and exit

-append
Append the database to the end of the file.

-ascii
Set output mode to 'ascii'.

-bail
Stop after hitting an error.

-batch
Force batch I/O.

-box
Set output mode to 'box'.

-column
Query results will be displayed in a table like form, using
whitespace characters to separate the columns and align the
output.

-cmd\  command
run
command
before reading stdin

-csv
Set output mode to CSV (comma separated values).

-deserialize
Open the database using sqlite3_deserialize()

-echo
Print commands before execution.

-init\  file
Read and execute commands from
file
, which can contain a mix of SQL statements and meta-commands.

-[no]header
Turn headers on or off.

-help
Show help on options and exit.

-hexkey\ key
Open database with hexadecimal encryption key.

-html
Query results will be output as simple HTML tables.

-interactive
Force interactive I/O.

-key\ key
Open database with raw encryption key
key

-json
Set output mode to 'json'.

-line
Query results will be displayed with one value per line, rows
separated by a blank line.  Designed to be easily parsed by
scripts or other programs

-list
Query results will be displayed with the separator (|, by default)
character between each field value.  The default.

-lookaside\  "size n"
Use
n
entries of
size
bytes for lookaside memory

-markdown
Set output mode to 'markdown'.

-maxsize\ N
Limits size of a -deserialize database to
N
bytes

-memtrace
Trace all memory allocations.

-newline\  sep
Set output row separator. Default is '\n'.

-nofollow
Refuse to open paths containing symbolic links.

-nonce\ string
Set the safe-mode escape nonce to
string

-nullvalue\  string
Set string used to represent NULL values.  Default is ''
(empty string).

-quote
Set output mode to quote.

-readonly
Open the database read-only.

-safe
Enable safe-mode.

-separator\  separator
Set output field separator.  Default is '|'.

-stats
Print memory stats before each finalize.

-textkey\ PASSPHRASE
Text to be hashed into the encryption key.

-table
Set output mode to 'table'.

-tabs
Set output mode to 'tabs'.

-version
Show SQLite version.

-vfs\  name
Use
name
as the default VFS.

-zip
Open the file as a zip archive.


## INIT FILE

sqlite3
reads an initialization file to set the configuration of the
interactive environment.  Throughout initialization, any previously
specified setting can be overridden.  The sequence of initialization is
as follows:

o The default configuration is established as follows:



```
.cc |
mode            = LIST
separator       = "|"
main prompt     = "sqlite> "
continue prompt = "   ...> "
|cc .
```


o If the file
$\{XDG_CONFIG_HOME\}/sqlite3/sqliterc
or
~/.sqliterc
exists, the first of those to be found is processed during startup.
It should generally only contain meta-commands.

o If the -init option is present, the specified file is processed.

o All other command line options are processed.


## SEE ALSO

https://sqlite.org/cli.html
.br
https://sqlite.org/fiddle (a WebAssembly build of the CLI app)
.br
The sqlite3-doc package.

## AUTHOR

This manual page was originally written by Andreas Rottmann
<rotty@debian.org>, for the Debian GNU/Linux system (but may be used
by others). It was subsequently revised by Bill Bumgarner <bbum@mac.com>,
Laszlo Boszormenyi <gcs@debian.hu>, Scott Perry <sqlite@numist.net>,
and the sqlite3 developers.
