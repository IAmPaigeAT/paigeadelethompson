+++
manpage_format = "troff"
date = "2024-12-14"
operating_system_version = "15.3"
keywords = ["header", "see", "also", "fbperl", "1", "diagnostics", "the", "usual", "warnings", "if", "it", "can", "t", "read", "or", "write", "files", "involved", "bugs", "doesn", "construct", "f", "cw", "sizeof", "array", "for", "you", "handle", "all", "c", "constructs", "but", "does", "attempt", "to", "isolate", "definitions", "inside", "evals", "so", "that", "get", "at", "translate", "s", "only", "intended", "as", "a", "rough", "tool", "may", "need", "dicker", "with", "produced", "have", "run", "this", "program", "by", "hand", "not", "part", "of", "perl", "installation", "complicated", "expressions", "built", "piecemeal", "la", "vb", "7", "enum", "first_value", "second_value", "ifdef", "abc", "third_value", "endif", "ve", "necessarily", "locate", "your", "compiler", "internally-defined", "symbols"]
description = "h2ph converts any C header files specified to the corresponding Perl header file format. It is most easily run while in /usr/include:         cd /usr/include; h2ph * sys/* or         cd /usr/include; h2ph * sys/* arpa/* netinet/* or         cd..."
detected_package_version = "5.34.1"
title = "h2ph(1)"
author = "None Specified"
operating_system = "macos"
manpage_name = "h2ph"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "H2PH 1"
H2PH 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

h2ph - convert .h C header files to .ph Perl header files

## SYNOPSIS

Header "SYNOPSIS"
\fBh2ph [-d destination directory] [-r | -a] [-l] [-h] [-e] [-D] [-Q]
[headerfiles]

## DESCRIPTION

Header "DESCRIPTION"
*h2ph*
converts any C header files specified to the corresponding Perl header file
format.
It is most easily run while in /usr/include:

.Vb 1
        cd /usr/include; h2ph * sys/*
.Ve

or

.Vb 1
        cd /usr/include; h2ph * sys/* arpa/* netinet/*
.Ve

or

.Vb 1
        cd /usr/include; h2ph -r -l .
.Ve

The output files are placed in the hierarchy rooted at Perl's
architecture dependent library directory.  You can specify a different
hierarchy with a **-d** switch.

If run with no arguments, filters standard input to standard output.

## OPTIONS

Header "OPTIONS"

- -d destination_dir
Item "-d destination_dir"
Put the resulting **.ph** files beneath **destination_dir**, instead of
beneath the default Perl library location (\f(CW$Config\{\*(Aqinstallsitearch\*(Aq\}).

- -r
Item "-r"
Run recursively; if any of **headerfiles** are directories, then run *h2ph*
on all files in those directories (and their subdirectories, etc.).  **-r**
and **-a** are mutually exclusive.

- -a
Item "-a"
Run automagically; convert **headerfiles**, as well as any **.h** files
which they include.  This option will search for **.h** files in all
directories which your C compiler ordinarily uses.  **-a** and **-r** are
mutually exclusive.

- -l
Item "-l"
Symbolic links will be replicated in the destination directory.  If **-l**
is not specified, then links are skipped over.

- -h
Item "-h"
Put 'hints' in the .ph files which will help in locating problems with
*h2ph*.  In those cases when you **require** a **.ph** file containing syntax
errors, instead of the cryptic
.Sp
.Vb 1
        [ some error condition ] at (eval mmm) line nnn
.Ve
.Sp
you will see the slightly more helpful
.Sp
.Vb 1
        [ some error condition ] at filename.ph line nnn
.Ve
.Sp
However, the **.ph** files almost double in size when built using **-h**.

- -e
Item "-e"
If an error is encountered during conversion, output file will be removed and
a warning emitted instead of terminating the conversion immediately.

- -D
Item "-D"
Include the code from the **.h** file as a comment in the **.ph** file.
This is primarily used for debugging *h2ph*.

- -Q
Item "-Q"
'Quiet' mode; don't print out the names of the files being converted.

## ENVIRONMENT

Header "ENVIRONMENT"
No environment variables are used.

## FILES

Header "FILES"
.Vb 2
 /usr/include/*.h
 /usr/include/sys/*.h
.Ve

etc.

## AUTHOR

Header "AUTHOR"
Larry Wall

## SEE ALSO

Header "SEE ALSO"
**perl**\|(1)

## DIAGNOSTICS

Header "DIAGNOSTICS"
The usual warnings if it can't read or write the files involved.

## BUGS

Header "BUGS"
Doesn't construct the \f(CW%sizeof array for you.

It doesn't handle all C constructs, but it does attempt to isolate
definitions inside evals so that you can get at the definitions
that it can translate.

It's only intended as a rough tool.
You may need to dicker with the files produced.

You have to run this program by hand; it's not run as part of the Perl
installation.

Doesn't handle complicated expressions built piecemeal, a la:

.Vb 7
    enum \{
        FIRST_VALUE,
        SECOND_VALUE,
    #ifdef ABC
        THIRD_VALUE
    #endif
    \};
.Ve

Doesn't necessarily locate all of your C compiler's internally-defined
symbols.
