+++
manpage_format = "troff"
description = "None Specified"
manpage_section = "1"
title = "traptoemail(1)"
operating_system_version = "15.3"
date = "Sun Feb 16 04:48:24 2025"
detected_package_version = "0"
manpage_name = "traptoemail"
author = "None Specified"
operating_system = "macos"
+++

traptoemail "1" "16 Nov 2006" V5.6.2.1 "Net-SNMP"

## NAME

traptoemail - snmptrapd handler script to convert snmp traps into emails

## SYNOPSIS


traptoemail
[*-f FROM*]
[*-s SMTPSERVER*]
ADDRESSES

## DESCRIPTION


converts snmp traps into email messages.

## OPTIONS


-f FROM
sender address, defaults to "root"

-s SMTPSERVER
SMTP server, defaults to "localhost"

ADDRESSES
recipient addresses
