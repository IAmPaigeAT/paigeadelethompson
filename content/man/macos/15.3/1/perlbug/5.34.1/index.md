+++
description = "This program is designed to help you generate bug reports (and thank-you notes) about perl5 and the modules which ship with it. In most cases, you can just run it interactively from a command line without any special arguments and follow the prompt..."
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "fbperl", "1", "fbperldebug", "fbperldiag", "fbperlport", "fbperltrap", "fbdiff", "fbpatch", "fbdbx", "fbgdb", "bugs", "none", "known", "guess", "what", "must", "have", "been", "used", "to", "report", "them"]
manpage_section = "1"
author = "None Specified"
operating_system_version = "15.3"
operating_system = "macos"
title = "perlbug(1)"
manpage_format = "troff"
date = "2024-12-14"
manpage_name = "perlbug"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLBUG 1"
PERLBUG 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlbug - how to submit bug reports on Perl

## SYNOPSIS

Header "SYNOPSIS"
**perlbug**

**perlbug** [\ **-v**\ ] [\ **-a**\ *address*\ ] [\ **-s**\ *subject*\ ]
[\ **-b**\ *body*\ |\ **-f**\ *inputfile*\ ] [\ **-F**\ *outputfile*\ ]
[\ **-r**\ *returnaddress*\ ]
[\ **-e**\ *editor*\ ] [\ **-c**\ *adminaddress*\ |\ **-C**\ ]
[\ **-S**\ ] [\ **-t**\ ]  [\ **-d**\ ]  [\ **-h**\ ] [\ **-T**\ ]

**perlbug** [\ **-v**\ ] [\ **-r**\ *returnaddress*\ ]
 [\ **-ok**\ |\ **-okay**\ |\ **-nok**\ |\ **-nokay**\ ]

**perlthanks**

## DESCRIPTION

Header "DESCRIPTION"
This program is designed to help you generate bug reports
(and thank-you notes) about perl5 and the modules which ship with it.

In most cases, you can just run it interactively from a command
line without any special arguments and follow the prompts.

If you have found a bug with a non-standard port (one that was not
part of the *standard distribution*), a binary distribution, or a
non-core module (such as Tk, \s-1DBI,\s0 etc), then please see the
documentation that came with that distribution to determine the
correct place to report bugs.

Bug reports should be submitted to the GitHub issue tracker at
<https://github.com/Perl/perl5/issues>. The **perlbug@perl.org**
address no longer automatically opens tickets. You can use this tool
to compose your report and save it to a file which you can then submit
to the issue tracker.

In extreme cases, **perlbug** may not work well enough on your system
to guide you through composing a bug report. In those cases, you
may be able to use **perlbug -d** or **perl -V** to get system
configuration information to include in your issue report.

When reporting a bug, please run through this checklist:

- What version of Perl you are running?
Item "What version of Perl you are running?"
Type \f(CW\*(C`perl -v\*(C' at the command line to find out.

- Are you running the latest released version of perl?
Item "Are you running the latest released version of perl?"
Look at <http://www.perl.org/> to find out.  If you are not using the
latest released version, please try to replicate your bug on the
latest stable release.
.Sp
Note that reports about bugs in old versions of Perl, especially
those which indicate you haven't also tested the current stable
release of Perl, are likely to receive less attention from the
volunteers who build and maintain Perl than reports about bugs in
the current release.

- Are you sure what you have is a bug?
Item "Are you sure what you have is a bug?"
A significant number of the bug reports we get turn out to be
documented features in Perl.  Make sure the issue you've run into
isn't intentional by glancing through the documentation that comes
with the Perl distribution.
.Sp
Given the sheer volume of Perl documentation, this isn't a trivial
undertaking, but if you can point to documentation that suggests
the behaviour you're seeing is *wrong*, your issue is likely to
receive more attention. You may want to start with **perldoc**
perltrap for pointers to common traps that new (and experienced)
Perl programmers run into.
.Sp
If you're unsure of the meaning of an error message you've run
across, **perldoc** perldiag for an explanation.  If the message
isn't in perldiag, it probably isn't generated by Perl.  You may
have luck consulting your operating system documentation instead.
.Sp
If you are on a non-UNIX platform **perldoc** perlport, as some
features may be unimplemented or work differently.
.Sp
You may be able to figure out what's going wrong using the Perl
debugger.  For information about how to use the debugger **perldoc**
perldebug.

- Do you have a proper test case?
Item "Do you have a proper test case?"
The easier it is to reproduce your bug, the more likely it will be
fixed \*(-- if nobody can duplicate your problem, it probably won't be
addressed.
.Sp
A good test case has most of these attributes: short, simple code;
few dependencies on external commands, modules, or libraries; no
platform-dependent code (unless it's a platform-specific bug);
clear, simple documentation.
.Sp
A good test case is almost always a good candidate to be included in
Perl's test suite.  If you have the time, consider writing your test case so
that it can be easily included into the standard test suite.

- Have you included all relevant information?
Item "Have you included all relevant information?"
Be sure to include the **exact** error messages, if any.
\*(L"Perl gave an error\*(R" is not an exact error message.
.Sp
If you get a core dump (or equivalent), you may use a debugger
(**dbx**, **gdb**, etc) to produce a stack trace to include in the bug
report.
.Sp
\s-1NOTE:\s0 unless your Perl has been compiled with debug info
(often **-g**), the stack trace is likely to be somewhat hard to use
because it will most probably contain only the function names and not
their arguments.  If possible, recompile your Perl with debug info and
reproduce the crash and the stack trace.

- Can you describe the bug in plain English?
Item "Can you describe the bug in plain English?"
The easier it is to understand a reproducible bug, the more likely
it will be fixed.  Any insight you can provide into the problem
will help a great deal.  In other words, try to analyze the problem
(to the extent you can) and report your discoveries.

- Can you fix the bug yourself?
Item "Can you fix the bug yourself?"
If so, that's great news; bug reports with patches are likely to
receive significantly more attention and interest than those without
patches.  Please submit your patch via the GitHub Pull Request workflow
as described in **perldoc** perlhack.  You may also send patches to
**perl5-porters@perl.org**.  When sending a patch, create it using
\f(CW\*(C`git format-patch\*(C' if possible, though a unified diff created with
\f(CW\*(C`diff -pu\*(C' will do nearly as well.
.Sp
Your patch may be returned with requests for changes, or requests for more
detailed explanations about your fix.
.Sp
Here are a few hints for creating high-quality patches:
.Sp
Make sure the patch is not reversed (the first argument to diff is
typically the original file, the second argument your changed file).
Make sure you test your patch by applying it with \f(CW\*(C`git am\*(C' or the
\f(CW\*(C`patch\*(C' program before you send it on its way.  Try to follow the
same style as the code you are trying to patch.  Make sure your patch
really does work (\f(CW\*(C`make test\*(C', if the thing you're patching is covered
by Perl's test suite).
.ie n .IP "Can you use ""perlbug"" to submit a thank-you note?" 4
.el .IP "Can you use \f(CWperlbug to submit a thank-you note?" 4
Item "Can you use perlbug to submit a thank-you note?"
Yes, you can do this by either using the \f(CW\*(C`-T\*(C' option, or by invoking
the program as \f(CW\*(C`perlthanks\*(C'. Thank-you notes are good. It makes people
smile.

Please make your issue title informative.  \*(L"a bug\*(R" is not informative.
Neither is \*(L"perl crashes\*(R" nor is \*(L"\s-1HELP\s0!!!\*(R".  These don't help.  A compact
description of what's wrong is fine.

Having done your bit, please be prepared to wait, to be told the
bug is in your code, or possibly to get no reply at all.  The
volunteers who maintain Perl are busy folks, so if your problem is
an obvious bug in your own code, is difficult to understand or is
a duplicate of an existing report, you may not receive a personal
reply.

If it is important to you that your bug be fixed, do monitor the
issue tracker (you will be subscribed to notifications for issues you
submit or comment on) and the commit logs to development
versions of Perl, and encourage the maintainers with kind words or
offers of frosty beverages.  (Please do be kind to the maintainers.
Harassing or flaming them is likely to have the opposite effect of the
one you want.)

Feel free to update the ticket about your bug on
<https://github.com/Perl/perl5/issues>
if a new version of Perl is released and your bug is still present.

## OPTIONS

Header "OPTIONS"

- \fB-a
Item "-a"
Address to send the report to instead of saving to a file.

- \fB-b
Item "-b"
Body of the report.  If not included on the command line, or
in a file with **-f**, you will get a chance to edit the report.

- \fB-C
Item "-C"
Don't send copy to administrator when sending report by mail.

- \fB-c
Item "-c"
Address to send copy of report to when sending report by mail.
Defaults to the address of the
local perl administrator (recorded when perl was built).

- \fB-d
Item "-d"
Data mode (the default if you redirect or pipe output).  This prints out
your configuration data, without saving or mailing anything.  You can use
this with **-v** to get more complete data.

- \fB-e
Item "-e"
Editor to use.

- \fB-f
Item "-f"
File containing the body of the report.  Use this to quickly send a
prepared report.

- \fB-F
Item "-F"
File to output the results to.  Defaults to **perlbug.rep**.

- \fB-h
Item "-h"
Prints a brief summary of the options.

- \fB-ok
Item "-ok"
Report successful build on this system to perl porters. Forces **-S**
and **-C**. Forces and supplies values for **-s** and **-b**. Only
prompts for a return address if it cannot guess it (for use with
**make**). Honors return address specified with **-r**.  You can use this
with **-v** to get more complete data.   Only makes a report if this
system is less than 60 days old.

- \fB-okay
Item "-okay"
As **-ok** except it will report on older systems.

- \fB-nok
Item "-nok"
Report unsuccessful build on this system.  Forces **-C**.  Forces and
supplies a value for **-s**, then requires you to edit the report
and say what went wrong.  Alternatively, a prepared report may be
supplied using **-f**.  Only prompts for a return address if it
cannot guess it (for use with **make**). Honors return address
specified with **-r**.  You can use this with **-v** to get more
complete data.  Only makes a report if this system is less than 60
days old.

- \fB-nokay
Item "-nokay"
As **-nok** except it will report on older systems.

- \fB-p
Item "-p"
The names of one or more patch files or other text attachments to be
included with the report.  Multiple files must be separated with commas.

- \fB-r
Item "-r"
Your return address.  The program will ask you to confirm its default
if you don't use this option.

- \fB-S
Item "-S"
Save or send the report without asking for confirmation.

- \fB-s
Item "-s"
Subject to include with the report.  You will be prompted if you don't
supply one on the command line.

- \fB-t
Item "-t"
Test mode.  Makes it possible to command perlbug from a pipe or file, for
testing purposes.

- \fB-T
Item "-T"
Send a thank-you note instead of a bug report.

- \fB-v
Item "-v"
Include verbose configuration data in the report.

## AUTHORS

Header "AUTHORS"
Kenneth Albanowski (<kjahds@kjahds.com>), subsequently
*doc*tored by Gurusamy Sarathy (<gsar@activestate.com>),
Tom Christiansen (<tchrist@perl.com>), Nathan Torkington
(<gnat@frii.com>), Charles F. Randall (<cfr@pobox.com>),
Mike Guy (<mjtg@cam.ac.uk>), Dominic Dunlop
(<domo@computer.org>), Hugo van der Sanden (<hv@crypt.org>),
Jarkko Hietaniemi (<jhi@iki.fi>), Chris Nandor
(<pudge@pobox.com>), Jon Orwant (<orwant@media.mit.edu>,
Richard Foley (<richard.foley@rfi.net>), Jesse Vincent
(<jesse@bestpractical.com>), and Craig A. Berry (<craigberry@mac.com>).

## SEE ALSO

Header "SEE ALSO"
**perl**\|(1), **perldebug**\|(1), **perldiag**\|(1), **perlport**\|(1), **perltrap**\|(1),
**diff**\|(1), **patch**\|(1), **dbx**\|(1), **gdb**\|(1)

## BUGS

Header "BUGS"
None known (guess what must have been used to report them?)
