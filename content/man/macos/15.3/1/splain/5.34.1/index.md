+++
detected_package_version = "5.34.1"
description = "This module extends the terse diagnostics normally emitted by both the perl compiler and the perl interpreter (from running perl with a -w  switch or f(CW*(C`use warnings*(C), augmenting them with the more explicative and endearing descriptions fo..."
operating_system_version = "15.3"
manpage_section = "1"
title = "splain(1)"
date = "2024-12-14"
operating_system = "macos"
manpage_name = "splain"
manpage_format = "troff"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "SPLAIN 1"
SPLAIN 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

diagnostics, splain - produce verbose warning diagnostics

## SYNOPSIS

Header "SYNOPSIS"
Using the \f(CW\*(C`diagnostics\*(C' pragma:

.Vb 2
    use diagnostics;
    use diagnostics -verbose;

    enable  diagnostics;
    disable diagnostics;
.Ve

Using the \f(CW\*(C`splain\*(C' standalone filter program:

.Vb 2
    perl program 2>diag.out
    splain [-v] [-p] diag.out
.Ve

Using diagnostics to get stack traces from a misbehaving script:

.Vb 1
    perl -Mdiagnostics=-traceonly my_script.pl
.Ve

## DESCRIPTION

Header "DESCRIPTION"
.ie n .SS "The ""diagnostics"" Pragma"
.el .SS "The \f(CWdiagnostics Pragma"
Subsection "The diagnostics Pragma"
This module extends the terse diagnostics normally emitted by both the
perl compiler and the perl interpreter (from running perl with a -w
switch or \f(CW\*(C`use warnings\*(C'), augmenting them with the more
explicative and endearing descriptions found in perldiag.  Like the
other pragmata, it affects the compilation phase of your program rather
than merely the execution phase.

To use in your program as a pragma, merely invoke

.Vb 1
    use diagnostics;
.Ve

at the start (or near the start) of your program.  (Note
that this *does* enable perl's **-w** flag.)  Your whole
compilation will then be subject(ed :-) to the enhanced diagnostics.
These still go out **\s-1STDERR\s0**.

Due to the interaction between runtime and compiletime issues,
and because it's probably not a very good idea anyway,
you may not use \f(CW\*(C`no diagnostics\*(C' to turn them off at compiletime.
However, you may control their behaviour at runtime using the
**disable()** and **enable()** methods to turn them off and on respectively.

The **-verbose** flag first prints out the perldiag introduction before
any other diagnostics.  The \f(CW$diagnostics::PRETTY variable can generate nicer
escape sequences for pagers.

Warnings dispatched from perl itself (or more accurately, those that match
descriptions found in perldiag) are only displayed once (no duplicate
descriptions).  User code generated warnings a la **warn()** are unaffected,
allowing duplicate user messages to be displayed.

This module also adds a stack trace to the error message when perl dies.
This is useful for pinpointing what
caused the death.  The **-traceonly** (or
just **-t**) flag turns off the explanations of warning messages leaving just
the stack traces.  So if your script is dieing, run it again with

.Vb 1
  perl -Mdiagnostics=-traceonly my_bad_script
.Ve

to see the call stack at the time of death.  By supplying the **-warntrace**
(or just **-w**) flag, any warnings emitted will also come with a stack
trace.

### The \fIsplain Program

Subsection "The splain Program"
While apparently a whole nuther program, *splain* is actually nothing
more than a link to the (executable) *diagnostics.pm* module, as well as
a link to the *diagnostics.pod* documentation.  The **-v** flag is like
the \f(CW\*(C`use diagnostics -verbose\*(C' directive.
The **-p** flag is like the
\f(CW$diagnostics::PRETTY variable.  Since you're post-processing with
*splain*, there's no sense in being able to **enable()** or **disable()** processing.

Output from *splain* is directed to **\s-1STDOUT\s0**, unlike the pragma.

## EXAMPLES

Header "EXAMPLES"
The following file is certain to trigger a few errors at both
runtime and compiletime:

.Vb 8
    use diagnostics;
    print NOWHERE "nothing\\n";
    print STDERR "\\n\\tThis message should be unadorned.\\n";
    warn "\\tThis is a user warning";
    print "\\nDIAGNOSTIC TESTER: Please enter a <CR> here: ";
    my $a, $b = scalar <STDIN>;
    print "\\n";
    print $x/$y;
.Ve

If you prefer to run your program first and look at its problem
afterwards, do this:

.Vb 2
    perl -w test.pl 2>test.out
    ./splain < test.out
.Ve

Note that this is not in general possible in shells of more dubious heritage,
as the theoretical

.Vb 2
    (perl -w test.pl >/dev/tty) >& test.out
    ./splain < test.out
.Ve

Because you just moved the existing **stdout** to somewhere else.

If you don't want to modify your source code, but still have on-the-fly
warnings, do this:

.Vb 1
    exec 3>&1; perl -w test.pl 2>&1 1>&3 3>&- | splain 1>&2 3>&-
.Ve

Nifty, eh?

If you want to control warnings on the fly, do something like this.
Make sure you do the \f(CW\*(C`use\*(C' first, or you won't be able to get
at the **enable()** or **disable()** methods.

.Vb 4
    use diagnostics; # checks entire compilation phase
        print "\\ntime for 1st bogus diags: SQUAWKINGS\\n";
        print BOGUS1 \*(Aqnada\*(Aq;
        print "done with 1st bogus\\n";

    disable diagnostics; # only turns off runtime warnings
        print "\\ntime for 2nd bogus: (squelched)\\n";
        print BOGUS2 \*(Aqnada\*(Aq;
        print "done with 2nd bogus\\n";

    enable diagnostics; # turns back on runtime warnings
        print "\\ntime for 3rd bogus: SQUAWKINGS\\n";
        print BOGUS3 \*(Aqnada\*(Aq;
        print "done with 3rd bogus\\n";

    disable diagnostics;
        print "\\ntime for 4th bogus: (squelched)\\n";
        print BOGUS4 \*(Aqnada\*(Aq;
        print "done with 4th bogus\\n";
.Ve

## INTERNALS

Header "INTERNALS"
Diagnostic messages derive from the *perldiag.pod* file when available at
runtime.  Otherwise, they may be embedded in the file itself when the
splain package is built.   See the *Makefile* for details.

If an extant \f(CW$SIG\{_\|_WARN_\|_\} handler is discovered, it will continue
to be honored, but only after the **diagnostics::splainthis()** function
(the module's \f(CW$SIG\{_\|_WARN_\|_\} interceptor) has had its way with your
warnings.

There is a \f(CW$diagnostics::DEBUG variable you may set if you're desperately
curious what sorts of things are being intercepted.

.Vb 1
    BEGIN \{ $diagnostics::DEBUG = 1 \}
.Ve

## BUGS

Header "BUGS"
Not being able to say \*(L"no diagnostics\*(R" is annoying, but may not be
insurmountable.

The \f(CW\*(C`-pretty\*(C' directive is called too late to affect matters.
You have to do this instead, and *before* you load the module.

.Vb 1
    BEGIN \{ $diagnostics::PRETTY = 1 \}
.Ve

I could start up faster by delaying compilation until it should be
needed, but this gets a \*(L"panic: top_level\*(R" when using the pragma form
in Perl 5.001e.

While it's true that this documentation is somewhat subserious, if you use
a program named *splain*, you should expect a bit of whimsy.

## AUTHOR

Header "AUTHOR"
Tom Christiansen <*tchrist@mox.perl.com*>, 25 June 1995.
