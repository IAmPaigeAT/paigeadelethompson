+++
manpage_name = "perl5122delta"
description = "This document describes differences between the 5.12.1 release and the 5.12.2 release. If you are upgrading from an earlier major version, such as 5.10.1, first read perl5120delta, which describes differences between 5.10.0 and 5.12.0, as well as ..."
manpage_format = "troff"
detected_package_version = "5.34.1"
date = "2022-02-19"
author = "None Specified"
manpage_section = "1"
operating_system_version = "15.3"
title = "perl5122delta(1)"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5122DELTA 1"
PERL5122DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5122delta - what is new for perl v5.12.2

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.12.1 release and
the 5.12.2 release.

If you are upgrading from an earlier major version, such as 5.10.1,
first read perl5120delta, which describes differences between 5.10.0
and 5.12.0, as well as perl5121delta, which describes earlier changes
in the 5.12 stable release series.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.12.1. If any exist, they
are bugs and reports are welcome.

## Core Enhancements

Header "Core Enhancements"
Other than the bug fixes listed below, there should be no user-visible
changes to the core language in this release.

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules and Pragmata

Subsection "New Modules and Pragmata"
This release does not introduce any new modules or pragmata.

### Pragmata Changes

Subsection "Pragmata Changes"
In the previous release, \f(CW\*(C`no \f(CIVERSION\f(CW;\*(C' statements triggered a bug
which could cause feature bundles to be loaded and strict mode to
be enabled unintentionally.

### Updated Modules

Subsection "Updated Modules"
.ie n .IP """Carp""" 4
.el .IP "\f(CWCarp" 4
Item "Carp"
Upgraded from version 1.16 to 1.17.
.Sp
Carp now detects incomplete **caller()**
overrides and avoids using bogus \f(CW@DB::args. To provide backtraces, Carp
relies on particular behaviour of the caller built-in. Carp now detects
if other code has overridden this with an incomplete implementation, and
modifies its backtrace accordingly. Previously incomplete overrides would
cause incorrect values in backtraces (best case), or obscure fatal errors
(worst case)
.Sp
This fixes certain cases of \f(CW\*(C`Bizarre copy of ARRAY\*(C' caused by modules
overriding \f(CW\*(C`caller()\*(C' incorrectly.
.ie n .IP """CPANPLUS""" 4
.el .IP "\f(CWCPANPLUS" 4
Item "CPANPLUS"
A patch to *cpanp-run-perl* has been backported from \s-1CPANPLUS\s0 \f(CW0.9004. This
resolves \s-1RT\s0 #55964 <http://rt.cpan.org/Public/Bug/Display.html?id=55964>
and \s-1RT\s0 #57106 <http://rt.cpan.org/Public/Bug/Display.html?id=57106>, both
of which related to failures to install distributions that use
\f(CW\*(C`Module::Install::DSL\*(C'.
.ie n .IP """File::Glob""" 4
.el .IP "\f(CWFile::Glob" 4
Item "File::Glob"
A regression which caused a failure to find \f(CW\*(C`CORE::GLOBAL::glob\*(C' after
loading \f(CW\*(C`File::Glob\*(C' to crash has been fixed.  Now, it correctly falls back
to external globbing via \f(CW\*(C`pp_glob\*(C'.
.ie n .IP """File::Copy""" 4
.el .IP "\f(CWFile::Copy" 4
Item "File::Copy"
\f(CW\*(C`File::Copy::copy(FILE, DIR)\*(C' is now documented.
.ie n .IP """File::Spec""" 4
.el .IP "\f(CWFile::Spec" 4
Item "File::Spec"
Upgraded from version 3.31 to 3.31_01.
.Sp
Several portability fixes were made in \f(CW\*(C`File::Spec::VMS\*(C': a colon is now
recognized as a delimiter in native filespecs; caret-escaped delimiters are
recognized for better handling of extended filespecs; \f(CW\*(C`catpath()\*(C' returns
an empty directory rather than the current directory if the input directory
name is empty; \f(CW\*(C`abs2rel()\*(C' properly handles Unix-style input.

## Utility Changes

Header "Utility Changes"

- \(bu
*perlbug* now always gives the reporter a chance to change the email address it
guesses for them.

- \(bu
*perlbug* should no longer warn about uninitialized values when using the \f(CW\*(C`-d\*(C'
and \f(CW\*(C`-v\*(C' options.

## Changes to Existing Documentation

Header "Changes to Existing Documentation"

- \(bu
The existing policy on backward-compatibility and deprecation has
been added to perlpolicy, along with definitions of terms like
*deprecation*.

- \(bu
\*(L"srand\*(R" in perlfunc's usage has been clarified.

- \(bu
The entry for \*(L"die\*(R" in perlfunc was reorganized to emphasize its
role in the exception mechanism.

- \(bu
Perl's \s-1INSTALL\s0 file has been clarified to explicitly state that Perl
requires a C89 compliant \s-1ANSI C\s0 Compiler.

- \(bu
IO::Socket's \f(CW\*(C`getsockopt()\*(C' and \f(CW\*(C`setsockopt()\*(C' have been documented.

- \(bu
*\f(BIalarm()\fI*'s inability to interrupt blocking \s-1IO\s0 on Windows has been documented.

- \(bu
Math::TrulyRandom hasn't been updated since 1996 and has been removed
as a recommended solution for random number generation.

- \(bu
perlrun has been updated to clarify the behaviour of octal flags to *perl*.

- \(bu
To ease user confusion, \f(CW$# and \f(CW$*, two special variables that were
removed in earlier versions of Perl have been documented.

- \(bu
The version of perlfaq shipped with the Perl core has been updated from the
official \s-1FAQ\s0 version, which is now maintained in the \f(CW\*(C`briandfoy/perlfaq\*(C'
branch of the Perl repository at <git://perl5.git.perl.org/perl.git>.

## Installation and Configuration Improvements

Header "Installation and Configuration Improvements"

### Configuration improvements

Subsection "Configuration improvements"

- \(bu
The \f(CW\*(C`d_u32align\*(C' configuration probe on \s-1ARM\s0 has been fixed.

### Compilation improvements

Subsection "Compilation improvements"

- \(bu
An "\f(CW\*(C`incompatible operand types\*(C'" error in ternary expressions when building
with \f(CW\*(C`clang\*(C' has been fixed.

- \(bu
Perl now skips setuid \f(CW\*(C`File::Copy\*(C' tests on partitions it detects to be mounted
as \f(CW\*(C`nosuid\*(C'.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
A possible segfault in the \f(CW\*(C`T_PRTOBJ\*(C' default typemap has been fixed.

- \(bu
A possible memory leak when using **caller()** to set
\f(CW@DB::args has been fixed.

- \(bu
Several memory leaks when loading \s-1XS\s0 modules were fixed.

- \(bu
\f(CW\*(C`unpack()\*(C' now handles scalar context correctly for \f(CW%32H and \f(CW%32u,
fixing a potential crash.  \f(CW\*(C`split()\*(C' would crash because the third item
on the stack wasn't the regular expression it expected.  \f(CW\*(C`unpack("%2H",
...)\*(C' would return both the unpacked result and the checksum on the stack,
as would \f(CW\*(C`unpack("%2u", ...)\*(C'.
[\s-1GH\s0 #10257] <https://github.com/Perl/perl5/issues/10257>

- \(bu
Perl now avoids using memory after calling \f(CW\*(C`free()\*(C' in *pp_require*
when there are CODEREFs in \f(CW@INC.

- \(bu
A bug that could cause "\f(CW\*(C`Unknown error\*(C'\*(L" messages when
\*(R"\f(CW\*(C`call_sv(code, G_EVAL)\*(C'" is called from an \s-1XS\s0 destructor has been fixed.

- \(bu
The implementation of the \f(CW\*(C`open $fh, \*(Aq>\*(Aq \\$buffer\*(C' feature
now supports get/set magic and thus tied buffers correctly.

- \(bu
The \f(CW\*(C`pp_getc\*(C', \f(CW\*(C`pp_tell\*(C', and \f(CW\*(C`pp_eof\*(C' opcodes now make room on the
stack for their return values in cases where no argument was passed in.

- \(bu
When matching unicode strings under some conditions inappropriate backtracking would
result in a \f(CW\*(C`Malformed UTF-8 character (fatal)\*(C' error. This should no longer occur.
See  [\s-1GH\s0 #10434] <https://github.com/Perl/perl5/issues/10434>

## Platform Specific Notes

Header "Platform Specific Notes"

### \s-1AIX\s0

Subsection "AIX"

- \(bu
*\s-1README\s0.aix* has been updated with information about the \s-1XL C/\*(C+ V11\s0 compiler
suite.

### Windows

Subsection "Windows"

- \(bu
When building Perl with the mingw64 x64 cross-compiler \f(CW\*(C`incpath\*(C',
\f(CW\*(C`libpth\*(C', \f(CW\*(C`ldflags\*(C', \f(CW\*(C`lddlflags\*(C' and \f(CW\*(C`ldflags_nolargefiles\*(C' values
in *Config.pm* and *Config_heavy.pl* were not previously being set
correctly because, with that compiler, the include and lib directories
are not immediately below \f(CW\*(C`$(CCHOME)\*(C'.

### \s-1VMS\s0

Subsection "VMS"

- \(bu
*git_version.h* is now installed on \s-1VMS.\s0 This was an oversight in v5.12.0 which
caused some extensions to fail to build.

- \(bu
Several memory leaks in **stat()** have been fixed.

- \(bu
A memory leak in \f(CW\*(C`Perl_rename()\*(C' due to a double allocation has been
fixed.

- \(bu
A memory leak in \f(CW\*(C`vms_fid_to_name()\*(C' (used by \f(CW\*(C`realpath()\*(C' and
\f(CW\*(C`realname()\*(C') has been fixed.

## Acknowledgements

Header "Acknowledgements"
Perl 5.12.2 represents approximately three months of development since
Perl 5.12.1 and contains approximately 2,000 lines of changes across
100 files from 36 authors.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers.  The following people are known to
have contributed the improvements that became Perl 5.12.2:

Abigail, \*(Aevar Arnfjo\*:r\*(d- Bjarmason, Ben Morrow, brian d foy, Brian
Phillips, Chas. Owens, Chris 'BinGOs' Williams, Chris Williams,
Craig A. Berry, Curtis Jewell, Dan Dascalescu, David Golden, David
Mitchell, Father Chrysostomos, Florian Ragwitz, George Greer, H.Merijn
Brand, Jan Dubois, Jesse Vincent, Jim Cromie, Karl Williamson, Lars
\s-1DXXXXXX XXX,\s0 Leon Brocard, Maik Hentsche, Matt S Trout,
Nicholas Clark, Rafael Garcia-Suarez, Rainer Tammer, Ricardo Signes,
Salvador Ortiz Garcia, Sisyphus, Slaven Rezic, Steffen Mueller, Tony Cook,
Vincent Pit and Yves Orton.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes
all the core committers, who will be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for
security issues in the Perl core, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
