+++
author = "None Specified"
manpage_section = "1"
description = "Replaces any hex addresses, e.g, f(CW0x128f72ce with f(CW*(C`0xN*(C. Replaces any references to process id or thread id, like f(CW*(C`pid#6254*(C with f(CW*(C`pidN*(C. So a s-1DBIs0 trace line like this:   -> STORE for DBD::DBM::st (DBI::st=HASH..."
operating_system_version = "15.3"
date = "2024-12-14"
operating_system = "macos"
detected_package_version = "5.34.0"
manpage_format = "troff"
manpage_name = "dbilogstrip"
title = "dbilogstrip(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "DBILOGSTRIP 1"
DBILOGSTRIP 1 "2024-12-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

dbilogstrip - filter to normalize DBI trace logs for diff'ing

## SYNOPSIS

Header "SYNOPSIS"
Read \s-1DBI\s0 trace file \f(CW\*(C`dbitrace.log\*(C' and write out a stripped version to \f(CW\*(C`dbitrace_stripped.log\*(C'

.Vb 1
  dbilogstrip dbitrace.log > dbitrace_stripped.log
.Ve

Run \f(CW\*(C`yourscript.pl\*(C' twice, each with different sets of arguments, with
\s-1DBI_TRACE\s0 enabled. Filter the output and trace through \f(CW\*(C`dbilogstrip\*(C' into a
separate file for each run. Then compare using diff. (This example assumes
you're using a standard shell.)

.Vb 3
  DBI_TRACE=2 perl yourscript.pl ...args1... 2>&1 | dbilogstrip > dbitrace1.log
  DBI_TRACE=2 perl yourscript.pl ...args2... 2>&1 | dbilogstrip > dbitrace2.log
  diff -u dbitrace1.log dbitrace2.log
.Ve

## DESCRIPTION

Header "DESCRIPTION"
Replaces any hex addresses, e.g, \f(CW0x128f72ce with \f(CW\*(C`0xN\*(C'.

Replaces any references to process id or thread id, like \f(CW\*(C`pid#6254\*(C' with \f(CW\*(C`pidN\*(C'.

So a \s-1DBI\s0 trace line like this:

.Vb 1
  -> STORE for DBD::DBM::st (DBI::st=HASH(0x19162a0)~0x191f9c8 \*(Aqf_params\*(Aq ARRAY(0x1922018)) thr#1800400
.Ve

will look like this:

.Vb 1
  -> STORE for DBD::DBM::st (DBI::st=HASH(0xN)~0xN \*(Aqf_params\*(Aq ARRAY(0xN)) thrN
.Ve
