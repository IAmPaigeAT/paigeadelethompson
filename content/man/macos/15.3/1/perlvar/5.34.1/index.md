+++
date = "2022-02-19"
manpage_format = "troff"
author = "None Specified"
manpage_section = "1"
manpage_name = "perlvar"
title = "perlvar(1)"
operating_system_version = "15.3"
detected_package_version = "5.34.1"
operating_system = "macos"
description = "Variable names in Perl can have several formats.  Usually, they must begin with a letter or underscore, in which case they can be arbitrarily long (up to an internal limit of 251 characters) and may contain letters, digits, underscores, or the spec..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLVAR 1"
PERLVAR 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlvar - Perl predefined variables

## DESCRIPTION

Header "DESCRIPTION"

### The Syntax of Variable Names

Subsection "The Syntax of Variable Names"
Variable names in Perl can have several formats.  Usually, they
must begin with a letter or underscore, in which case they can be
arbitrarily long (up to an internal limit of 251 characters) and
may contain letters, digits, underscores, or the special sequence
\f(CW\*(C`::\*(C' or \f(CW\*(C`\*(Aq\*(C'.  In this case, the part before the last \f(CW\*(C`::\*(C' or
\f(CW\*(C`\*(Aq\*(C' is taken to be a *package qualifier*; see perlmod.
A Unicode letter that is not \s-1ASCII\s0 is not considered to be a letter
unless \f(CW"use\ utf8" is in effect, and somewhat more complicated
rules apply; see \*(L"Identifier parsing\*(R" in perldata for details.

Perl variable names may also be a sequence of digits, a single
punctuation character, or the two-character sequence: \f(CW\*(C`^\*(C' (caret or
\s-1CIRCUMFLEX ACCENT\s0) followed by any one of the characters \f(CW\*(C`[][A-Z^_?\\]\*(C'.
These names are all reserved for
special uses by Perl; for example, the all-digits names are used
to hold data captured by backreferences after a regular expression
match.

Since Perl v5.6.0, Perl variable names may also be alphanumeric strings
preceded by a caret.  These must all be written in the form \f(CW\*(C`$\{^Foo\}\*(C';
the braces are not optional.  \f(CW\*(C`$\{^Foo\}\*(C' denotes the scalar variable
whose name is considered to be a control-\f(CW\*(C`F\*(C' followed by two \f(CW\*(C`o\*(C''s.
These variables are
reserved for future special uses by Perl, except for the ones that
begin with \f(CW\*(C`^_\*(C' (caret-underscore).  No
name that begins with \f(CW\*(C`^_\*(C' will acquire a special
meaning in any future version of Perl; such names may therefore be
used safely in programs.  \f(CW$^_ itself, however, *is* reserved.

Perl identifiers that begin with digits or
punctuation characters are exempt from the effects of the \f(CW\*(C`package\*(C'
declaration and are always forced to be in package \f(CW\*(C`main\*(C'; they are
also exempt from \f(CW\*(C`strict \*(Aqvars\*(Aq\*(C' errors.  A few other names are also
exempt in these ways:

.Vb 5
    ENV      STDIN
    INC      STDOUT
    ARGV     STDERR
    ARGVOUT
    SIG
.Ve

In particular, the special \f(CW\*(C`$\{^_XYZ\}\*(C' variables are always taken
to be in package \f(CW\*(C`main\*(C', regardless of any \f(CW\*(C`package\*(C' declarations
presently in scope.

## SPECIAL VARIABLES

Header "SPECIAL VARIABLES"
The following names have special meaning to Perl.  Most punctuation
names have reasonable mnemonics, or analogs in the shells.
Nevertheless, if you wish to use long variable names, you need only say:

.Vb 1
    use English;
.Ve

at the top of your program.  This aliases all the short names to the long
names in the current package.  Some even have medium names, generally
borrowed from **awk**.  For more info, please see English.

Before you continue, note the sort order for variables.  In general, we
first list the variables in case-insensitive, almost-lexigraphical
order (ignoring the \f(CW\*(C`\{\*(C' or \f(CW\*(C`^\*(C' preceding words, as in \f(CW\*(C`$\{^UNICODE\}\*(C'
or \f(CW$^T), although \f(CW$_ and \f(CW@_ move up to the top of the pile.
For variables with the same identifier, we list it in order of scalar,
array, hash, and bareword.

### General Variables

Subsection "General Variables"
.ie n .IP "$ARG" 8
.el .IP "\f(CW$ARG" 8
Item "$ARG"
0
.ie n .IP "$_" 8
.el .IP "\f(CW$_" 8
Xref "$_ $ARG"
Item "$_"
.PD
The default input and pattern-searching space.  The following pairs are
equivalent:
.Sp
.Vb 2
    while (<>) \{...\}    # equivalent only in while!
    while (defined($_ = <>)) \{...\}

    /^Subject:/
    $_ =~ /^Subject:/

    tr/a-z/A-Z/
    $_ =~ tr/a-z/A-Z/

    chomp
    chomp($_)
.Ve
.Sp
Here are the places where Perl will assume \f(CW$_ even if you don't use it:

> 
- \(bu
The following functions use \f(CW$_ as a default argument:
.Sp
abs, alarm, chomp, chop, chr, chroot,
cos, defined, eval, evalbytes, exp, fc, glob, hex, int, lc,
lcfirst, length, log, lstat, mkdir, oct, ord, pos, print, printf,
quotemeta, readlink, readpipe, ref, require, reverse (in scalar context only),
rmdir, say, sin, split (for its second
argument), sqrt, stat, study, uc, ucfirst,
unlink, unpack.

- \(bu
All file tests (\f(CW\*(C`-f\*(C', \f(CW\*(C`-d\*(C') except for \f(CW\*(C`-t\*(C', which defaults to \s-1STDIN.\s0
See \*(L"-X\*(R" in perlfunc

- \(bu
The pattern matching operations \f(CW\*(C`m//\*(C', \f(CW\*(C`s///\*(C' and \f(CW\*(C`tr///\*(C' (aka \f(CW\*(C`y///\*(C')
when used without an \f(CW\*(C`=~\*(C' operator.

- \(bu
The default iterator variable in a \f(CW\*(C`foreach\*(C' loop if no other
variable is supplied.

- \(bu
The implicit iterator variable in the \f(CW\*(C`grep()\*(C' and \f(CW\*(C`map()\*(C' functions.

- \(bu
The implicit variable of \f(CW\*(C`given()\*(C'.

- \(bu
The default place to put the next value or input record
when a \f(CW\*(C`<FH>\*(C', \f(CW\*(C`readline\*(C', \f(CW\*(C`readdir\*(C' or \f(CW\*(C`each\*(C'
operation's result is tested by itself as the sole criterion of a \f(CW\*(C`while\*(C'
test.  Outside a \f(CW\*(C`while\*(C' test, this will not happen.



> .Sp
\f(CW$_ is a global variable.
.Sp
However, between perl v5.10.0 and v5.24.0, it could be used lexically by
writing \f(CW\*(C`my $_\*(C'.  Making \f(CW$_ refer to the global \f(CW$_ in the same scope
was then possible with \f(CW\*(C`our $_\*(C'.  This experimental feature was removed and is
now a fatal error, but you may encounter it in older code.
.Sp
Mnemonic: underline is understood in certain operations.


.ie n .IP "@ARG" 8
.el .IP "\f(CW@ARG" 8
Item "@ARG"
0
.ie n .IP "@_" 8
.el .IP "\f(CW@_" 8
Xref "@_ @ARG"
Item "@_"
.PD
Within a subroutine the array \f(CW@_ contains the parameters passed to
that subroutine.  Inside a subroutine, \f(CW@_ is the default array for
the array operators \f(CW\*(C`pop\*(C' and \f(CW\*(C`shift\*(C'.
.Sp
See perlsub.
.ie n .IP "$LIST_SEPARATOR" 8
.el .IP "\f(CW$LIST_SEPARATOR" 8
Item "$LIST_SEPARATOR"
0
"$""" 8
Xref "$"" $LIST_SEPARATOR"
.PD
When an array or an array slice is interpolated into a double-quoted
string or a similar context such as \f(CW\*(C`/.../\*(C', its elements are
separated by this value.  Default is a space.  For example, this:
.Sp
.Vb 1
    print "The array is: @array\\n";
.Ve
.Sp
is equivalent to this:
.Sp
.Vb 1
    print "The array is: " . join($", @array) . "\\n";
.Ve
.Sp
Mnemonic: works in double-quoted context.
.ie n .IP "$PROCESS_ID" 8
.el .IP "\f(CW$PROCESS_ID" 8
Item "$PROCESS_ID"
0
.ie n .IP "$PID" 8
.el .IP "\f(CW$PID" 8
Item "$PID"

- $$
Xref "$$ $PID $PROCESS_ID"
.PD
The process number of the Perl running this script.  Though you *can* set
this variable, doing so is generally discouraged, although it can be
invaluable for some testing purposes.  It will be reset automatically
across \f(CW\*(C`fork()\*(C' calls.
.Sp
Note for Linux and Debian GNU/kFreeBSD users: Before Perl v5.16.0 perl
would emulate \s-1POSIX\s0 semantics on Linux systems using LinuxThreads, a
partial implementation of \s-1POSIX\s0 Threads that has since been superseded
by the Native \s-1POSIX\s0 Thread Library (\s-1NPTL\s0).
.Sp
LinuxThreads is now obsolete on Linux, and caching \f(CW\*(C`getpid()\*(C'
like this made embedding perl unnecessarily complex (since you'd have
to manually update the value of $$), so now \f(CW$$ and \f(CW\*(C`getppid()\*(C'
will always return the same values as the underlying C library.
.Sp
Debian GNU/kFreeBSD systems also used LinuxThreads up until and
including the 6.0 release, but after that moved to FreeBSD thread
semantics, which are POSIX-like.
.Sp
To see if your system is affected by this discrepancy check if
\f(CW\*(C`getconf GNU_LIBPTHREAD_VERSION | grep -q NPTL\*(C' returns a false
value.  \s-1NTPL\s0 threads preserve the \s-1POSIX\s0 semantics.
.Sp
Mnemonic: same as shells.
.ie n .IP "$PROGRAM_NAME" 8
.el .IP "\f(CW$PROGRAM_NAME" 8
Item "$PROGRAM_NAME"
0
.ie n .IP "$0" 8
.el .IP "\f(CW$0" 8
Xref "$0 $PROGRAM_NAME"
Item "$0"
.PD
Contains the name of the program being executed.
.Sp
On some (but not all) operating systems assigning to \f(CW$0 modifies
the argument area that the \f(CW\*(C`ps\*(C' program sees.  On some platforms you
may have to use special \f(CW\*(C`ps\*(C' options or a different \f(CW\*(C`ps\*(C' to see the
changes.  Modifying the \f(CW$0 is more useful as a way of indicating the
current program state than it is for hiding the program you're
running.
.Sp
Note that there are platform-specific limitations on the maximum
length of \f(CW$0.  In the most extreme case it may be limited to the
space occupied by the original \f(CW$0.
.Sp
In some platforms there may be arbitrary amount of padding, for
example space characters, after the modified name as shown by \f(CW\*(C`ps\*(C'.
In some platforms this padding may extend all the way to the original
length of the argument area, no matter what you do (this is the case
for example with Linux 2.2).
.Sp
Note for \s-1BSD\s0 users: setting \f(CW$0 does not completely remove \*(L"perl\*(R"
from the **ps**\|(1) output.  For example, setting \f(CW$0 to \f(CW"foobar" may
result in \f(CW"perl: foobar (perl)" (whether both the \f(CW"perl: " prefix
and the \*(L" (perl)\*(R" suffix are shown depends on your exact \s-1BSD\s0 variant
and version).  This is an operating system feature, Perl cannot help it.
.Sp
In multithreaded scripts Perl coordinates the threads so that any
thread may modify its copy of the \f(CW$0 and the change becomes visible
to **ps**\|(1) (assuming the operating system plays along).  Note that
the view of \f(CW$0 the other threads have will not change since they
have their own copies of it.
.Sp
If the program has been given to perl via the switches \f(CW\*(C`-e\*(C' or \f(CW\*(C`-E\*(C',
\f(CW$0 will contain the string \f(CW"-e".
.Sp
On Linux as of perl v5.14.0 the legacy process name will be set with
\f(CWprctl(2), in addition to altering the \s-1POSIX\s0 name via \f(CW\*(C`argv[0]\*(C' as
perl has done since version 4.000.  Now system utilities that read the
legacy process name such as ps, top and killall will recognize the
name you set when assigning to \f(CW$0.  The string you supply will be
cut off at 16 bytes, this is a limitation imposed by Linux.
.Sp
Mnemonic: same as **sh** and **ksh**.
.ie n .IP "$REAL_GROUP_ID" 8
.el .IP "\f(CW$REAL_GROUP_ID" 8
Item "$REAL_GROUP_ID"
0
.ie n .IP "$GID" 8
.el .IP "\f(CW$GID" 8
Item "$GID"

- $(
Xref "$( $GID $REAL_GROUP_ID"
.PD
The real gid of this process.  If you are on a machine that supports
membership in multiple groups simultaneously, gives a space separated
list of groups you are in.  The first number is the one returned by
\f(CW\*(C`getgid()\*(C', and the subsequent ones by \f(CW\*(C`getgroups()\*(C', one of which may be
the same as the first number.
.Sp
However, a value assigned to \f(CW$( must be a single number used to
set the real gid.  So the value given by \f(CW$( should *not* be assigned
back to \f(CW$( without being forced numeric, such as by adding zero.  Note
that this is different to the effective gid (\f(CW$)) which does take a
list.
.Sp
You can change both the real gid and the effective gid at the same
time by using \f(CW\*(C`POSIX::setgid()\*(C'.  Changes
to \f(CW$( require a check to \f(CW$!
to detect any possible errors after an attempted change.
.Sp
Mnemonic: parentheses are used to *group* things.  The real gid is the
group you *left*, if you're running setgid.
.ie n .IP "$EFFECTIVE_GROUP_ID" 8
.el .IP "\f(CW$EFFECTIVE_GROUP_ID" 8
Item "$EFFECTIVE_GROUP_ID"
0
.ie n .IP "$EGID" 8
.el .IP "\f(CW$EGID" 8
Item "$EGID"

- $)
Xref "$) $EGID $EFFECTIVE_GROUP_ID"
.PD
The effective gid of this process.  If you are on a machine that
supports membership in multiple groups simultaneously, gives a space
separated list of groups you are in.  The first number is the one
returned by \f(CW\*(C`getegid()\*(C', and the subsequent ones by \f(CW\*(C`getgroups()\*(C',
one of which may be the same as the first number.
.Sp
Similarly, a value assigned to \f(CW$) must also be a space-separated
list of numbers.  The first number sets the effective gid, and
the rest (if any) are passed to \f(CW\*(C`setgroups()\*(C'.  To get the effect of an
empty list for \f(CW\*(C`setgroups()\*(C', just repeat the new effective gid; that is,
to force an effective gid of 5 and an effectively empty \f(CW\*(C`setgroups()\*(C'
list, say \f(CW\*(C` $) = "5 5" \*(C'.
.Sp
You can change both the effective gid and the real gid at the same
time by using \f(CW\*(C`POSIX::setgid()\*(C' (use only a single numeric argument).
Changes to \f(CW$) require a check to \f(CW$! to detect any possible errors
after an attempted change.
.Sp
\f(CW$<, \f(CW$>, \f(CW$( and \f(CW$) can be set only on
machines that support the corresponding *set[re][ug]\f(BIid()** routine.  \f(CW$(*
and \f(CW$) can be swapped only on machines supporting \f(CW\*(C`setregid()\*(C'.
.Sp
Mnemonic: parentheses are used to *group* things.  The effective gid
is the group that's *right* for you, if you're running setgid.
.ie n .IP "$REAL_USER_ID" 8
.el .IP "\f(CW$REAL_USER_ID" 8
Item "$REAL_USER_ID"
0
.ie n .IP "$UID" 8
.el .IP "\f(CW$UID" 8
Item "$UID"

- $<
Xref "$< $UID $REAL_USER_ID"
.PD
The real uid of this process.  You can change both the real uid and the
effective uid at the same time by using \f(CW\*(C`POSIX::setuid()\*(C'.  Since
changes to \f(CW$< require a system call, check \f(CW$! after a change
attempt to detect any possible errors.
.Sp
Mnemonic: it's the uid you came *from*, if you're running setuid.
.ie n .IP "$EFFECTIVE_USER_ID" 8
.el .IP "\f(CW$EFFECTIVE_USER_ID" 8
Item "$EFFECTIVE_USER_ID"
0
.ie n .IP "$EUID" 8
.el .IP "\f(CW$EUID" 8
Item "$EUID"

- $>
Xref "$> $EUID $EFFECTIVE_USER_ID"
.PD
The effective uid of this process.  For example:
.Sp
.Vb 2
    $< = $>;            # set real to effective uid
    ($<,$>) = ($>,$<);  # swap real and effective uids
.Ve
.Sp
You can change both the effective uid and the real uid at the same
time by using \f(CW\*(C`POSIX::setuid()\*(C'.  Changes to \f(CW$> require a check
to \f(CW$! to detect any possible errors after an attempted change.
.Sp
\f(CW$< and \f(CW$> can be swapped only on machines
supporting \f(CW\*(C`setreuid()\*(C'.
.Sp
Mnemonic: it's the uid you went *to*, if you're running setuid.
.ie n .IP "$SUBSCRIPT_SEPARATOR" 8
.el .IP "\f(CW$SUBSCRIPT_SEPARATOR" 8
Item "$SUBSCRIPT_SEPARATOR"
0
.ie n .IP "$SUBSEP" 8
.el .IP "\f(CW$SUBSEP" 8
Item "$SUBSEP"

- $;
Xref "$; $SUBSEP SUBSCRIPT_SEPARATOR"
.PD
The subscript separator for multidimensional array emulation.  If you
refer to a hash element as
.Sp
.Vb 1
    $foo\{$x,$y,$z\}
.Ve
.Sp
it really means
.Sp
.Vb 1
    $foo\{join($;, $x, $y, $z)\}
.Ve
.Sp
But don't put
.Sp
.Vb 1
    @foo\{$x,$y,$z\}      # a slice--note the @
.Ve
.Sp
which means
.Sp
.Vb 1
    ($foo\{$x\},$foo\{$y\},$foo\{$z\})
.Ve
.Sp
Default is \*(L"\\034\*(R", the same as \s-1SUBSEP\s0 in **awk**.  If your keys contain
binary data there might not be any safe value for \f(CW$;.
.Sp
Consider using \*(L"real\*(R" multidimensional arrays as described
in perllol.
.Sp
Mnemonic: comma (the syntactic subscript separator) is a semi-semicolon.
.ie n .IP "$a" 8
.el .IP "\f(CW$a" 8
Item "$a"
0
.ie n .IP "$b" 8
.el .IP "\f(CW$b" 8
Xref "$a $b"
Item "$b"
.PD
Special package variables when using \f(CW\*(C`sort()\*(C', see \*(L"sort\*(R" in perlfunc.
Because of this specialness \f(CW$a and \f(CW$b don't need to be declared
(using \f(CW\*(C`use vars\*(C', or \f(CW\*(C`our()\*(C') even when using the \f(CW\*(C`strict \*(Aqvars\*(Aq\*(C'
pragma.  Don't lexicalize them with \f(CW\*(C`my $a\*(C' or \f(CW\*(C`my $b\*(C' if you want to
be able to use them in the \f(CW\*(C`sort()\*(C' comparison block or function.
.ie n .IP "%ENV" 8
.el .IP "\f(CW%ENV" 8
Xref "%ENV"
Item "%ENV"
The hash \f(CW%ENV contains your current environment.  Setting a
value in \f(CW\*(C`ENV\*(C' changes the environment for any child processes
you subsequently \f(CW\*(C`fork()\*(C' off.
.Sp
As of v5.18.0, both keys and values stored in \f(CW%ENV are stringified.
.Sp
.Vb 7
    my $foo = 1;
    $ENV\{\*(Aqbar\*(Aq\} = \\$foo;
    if( ref $ENV\{\*(Aqbar\*(Aq\} ) \{
        say "Pre 5.18.0 Behaviour";
    \} else \{
        say "Post 5.18.0 Behaviour";
    \}
.Ve
.Sp
Previously, only child processes received stringified values:
.Sp
.Vb 2
    my $foo = 1;
    $ENV\{\*(Aqbar\*(Aq\} = \\$foo;

    # Always printed \*(Aqnon ref\*(Aq
    system($^X, \*(Aq-e\*(Aq,
           q/print ( ref $ENV\{\*(Aqbar\*(Aq\}  ? \*(Aqref\*(Aq : \*(Aqnon ref\*(Aq ) /);
.Ve
.Sp
This happens because you can't really share arbitrary data structures with
foreign processes.
.ie n .IP "$OLD_PERL_VERSION" 8
.el .IP "\f(CW$OLD_PERL_VERSION" 8
Item "$OLD_PERL_VERSION"
0

- $]
Xref "$] $OLD_PERL_VERSION"
.PD
The revision, version, and subversion of the Perl interpreter, represented
as a decimal of the form 5.XXXYYY, where \s-1XXX\s0 is the version / 1e3 and \s-1YYY\s0
is the subversion / 1e6.  For example, Perl v5.10.1 would be \*(L"5.010001\*(R".
.Sp
This variable can be used to determine whether the Perl interpreter
executing a script is in the right range of versions:
.Sp
.Vb 1
    warn "No PerlIO!\\n" if "$]" < 5.008;
.Ve
.Sp
When comparing \f(CW$], numeric comparison operators should be used, but the
variable should be stringified first to avoid issues where its original
numeric value is inaccurate.
.Sp
See also the documentation of \f(CW\*(C`use VERSION\*(C' and \f(CW\*(C`require VERSION\*(C'
for a convenient way to fail if the running Perl interpreter is too old.
.Sp
See \*(L"$^V\*(R" for a representation of the Perl version as a version
object, which allows more flexible string comparisons.
.Sp
The main advantage of \f(CW$] over \f(CW$^V is that it works the same on any
version of Perl.  The disadvantages are that it can't easily be compared
to versions in other formats (e.g. literal v-strings, \*(L"v1.2.3\*(R" or
version objects) and numeric comparisons are subject to the binary
floating point representation; it's good for numeric literal version
checks and bad for comparing to a variable that hasn't been
sanity-checked.
.Sp
The \f(CW$OLD_PERL_VERSION form was added in Perl v5.20.0 for historical
reasons but its use is discouraged. (If your reason to use \f(CW$] is to
run code on old perls then referring to it as \f(CW$OLD_PERL_VERSION would
be self-defeating.)
.Sp
Mnemonic: Is this version of perl in the right bracket?
.ie n .IP "$SYSTEM_FD_MAX" 8
.el .IP "\f(CW$SYSTEM_FD_MAX" 8
Item "$SYSTEM_FD_MAX"
0

- $^F
Xref "$^F $SYSTEM_FD_MAX"
Item "$^F"
.PD
The maximum system file descriptor, ordinarily 2.  System file
descriptors are passed to \f(CW\*(C`exec()\*(C'ed processes, while higher file
descriptors are not.  Also, during an
\f(CW\*(C`open()\*(C', system file descriptors are
preserved even if the \f(CW\*(C`open()\*(C' fails (ordinary file descriptors are
closed before the \f(CW\*(C`open()\*(C' is attempted).  The close-on-exec
status of a file descriptor will be decided according to the value of
\f(CW$^F when the corresponding file, pipe, or socket was opened, not the
time of the \f(CW\*(C`exec()\*(C'.
.ie n .IP "@F" 8
.el .IP "\f(CW@F" 8
Xref "@F"
Item "@F"
The array \f(CW@F contains the fields of each line read in when autosplit
mode is turned on.  See perlrun for the **-a** switch.  This
array is package-specific, and must be declared or given a full package
name if not in package main when running under \f(CW\*(C`strict \*(Aqvars\*(Aq\*(C'.
.ie n .IP "@INC" 8
.el .IP "\f(CW@INC" 8
Xref "@INC"
Item "@INC"
The array \f(CW@INC contains the list of places that the \f(CW\*(C`do EXPR\*(C',
\f(CW\*(C`require\*(C', or \f(CW\*(C`use\*(C' constructs look for their library files.  It
initially consists of the arguments to any **-I** command-line
switches, followed by the default Perl library, probably
*/usr/local/lib/perl*.
Prior to Perl 5.26, \f(CW\*(C`.\*(C' -which represents the current directory, was included
in \f(CW@INC; it has been removed. This change in behavior is documented
in \f(CW\*(C`PERL_USE_UNSAFE_INC\*(C' and it is
not recommended that \f(CW\*(C`.\*(C' be re-added to \f(CW@INC.
If you need to modify \f(CW@INC at runtime, you should use the \f(CW\*(C`use lib\*(C' pragma
to get the machine-dependent library properly loaded as well:
.Sp
.Vb 2
    use lib \*(Aq/mypath/libdir/\*(Aq;
    use SomeMod;
.Ve
.Sp
You can also insert hooks into the file inclusion system by putting Perl
code directly into \f(CW@INC.  Those hooks may be subroutine references,
array references or blessed objects.  See \*(L"require\*(R" in perlfunc for details.
.ie n .IP "%INC" 8
.el .IP "\f(CW%INC" 8
Xref "%INC"
Item "%INC"
The hash \f(CW%INC contains entries for each filename included via the
\f(CW\*(C`do\*(C', \f(CW\*(C`require\*(C', or \f(CW\*(C`use\*(C' operators.  The key is the filename
you specified (with module names converted to pathnames), and the
value is the location of the file found.  The \f(CW\*(C`require\*(C'
operator uses this hash to determine whether a particular file has
already been included.
.Sp
If the file was loaded via a hook (e.g. a subroutine reference, see
\*(L"require\*(R" in perlfunc for a description of these hooks), this hook is
by default inserted into \f(CW%INC in place of a filename.  Note, however,
that the hook may have set the \f(CW%INC entry by itself to provide some more
specific info.
.ie n .IP "$INPLACE_EDIT" 8
.el .IP "\f(CW$INPLACE_EDIT" 8
Item "$INPLACE_EDIT"
0

- $^I
Xref "$^I $INPLACE_EDIT"
Item "$^I"
.PD
The current value of the inplace-edit extension.  Use \f(CW\*(C`undef\*(C' to disable
inplace editing.
.Sp
Mnemonic: value of **-i** switch.
.ie n .IP "@ISA" 8
.el .IP "\f(CW@ISA" 8
Xref "@ISA"
Item "@ISA"
Each package contains a special array called \f(CW@ISA which contains a list
of that class's parent classes, if any. This array is simply a list of
scalars, each of which is a string that corresponds to a package name. The
array is examined when Perl does method resolution, which is covered in
perlobj.
.Sp
To load packages while adding them to \f(CW@ISA, see the parent pragma. The
discouraged base pragma does this as well, but should not be used except
when compatibility with the discouraged fields pragma is required.

- $^M
Xref "$^M"
Item "$^M"
By default, running out of memory is an untrappable, fatal error.
However, if suitably built, Perl can use the contents of \f(CW$^M
as an emergency memory pool after \f(CW\*(C`die()\*(C'ing.  Suppose that your Perl
were compiled with \f(CW\*(C`-DPERL_EMERGENCY_SBRK\*(C' and used Perl's malloc.
Then
.Sp
.Vb 1
    $^M = \*(Aqa\*(Aq x (1 << 16);
.Ve
.Sp
would allocate a 64K buffer for use in an emergency.  See the
*\s-1INSTALL\s0* file in the Perl distribution for information on how to
add custom C compilation flags when compiling perl.  To discourage casual
use of this advanced feature, there is no English long name for
this variable.
.Sp
This variable was added in Perl 5.004.
.ie n .IP "$OSNAME" 8
.el .IP "\f(CW$OSNAME" 8
Item "$OSNAME"
0

- $^O
Xref "$^O $OSNAME"
Item "$^O"
.PD
The name of the operating system under which this copy of Perl was
built, as determined during the configuration process.  For examples
see \*(L"\s-1PLATFORMS\*(R"\s0 in perlport.
.Sp
The value is identical to \f(CW$Config\{\*(Aqosname\*(Aq\}.  See also Config
and the **-V** command-line switch documented in perlrun.
.Sp
In Windows platforms, \f(CW$^O is not very helpful: since it is always
\f(CW\*(C`MSWin32\*(C', it doesn't tell the difference between
95/98/ME/NT/2000/XP/CE/.NET.  Use \f(CW\*(C`Win32::GetOSName()\*(C' or
**Win32::GetOSVersion()** (see Win32 and perlport) to distinguish
between the variants.
.Sp
This variable was added in Perl 5.003.
.ie n .IP "%SIG" 8
.el .IP "\f(CW%SIG" 8
Xref "%SIG"
Item "%SIG"
The hash \f(CW%SIG contains signal handlers for signals.  For example:
.Sp
.Vb 6
    sub handler \{   # 1st argument is signal name
        my($sig) = @_;
        print "Caught a SIG$sig--shutting down\\n";
        close(LOG);
        exit(0);
        \}

    $SIG\{\*(AqINT\*(Aq\}  = \handler;
    $SIG\{\*(AqQUIT\*(Aq\} = \handler;
    ...
    $SIG\{\*(AqINT\*(Aq\}  = \*(AqDEFAULT\*(Aq;   # restore default action
    $SIG\{\*(AqQUIT\*(Aq\} = \*(AqIGNORE\*(Aq;    # ignore SIGQUIT
.Ve
.Sp
Using a value of \f(CW\*(AqIGNORE\*(Aq usually has the effect of ignoring the
signal, except for the \f(CW\*(C`CHLD\*(C' signal.  See perlipc for more about
this special case.  Using an empty string or \f(CW\*(C`undef\*(C' as the value has
the same effect as \f(CW\*(AqDEFAULT\*(Aq.
.Sp
Here are some other examples:
.Sp
.Vb 7
    $SIG\{"PIPE"\} = "Plumber";   # assumes main::Plumber (not
                                # recommended)
    $SIG\{"PIPE"\} = \Plumber;   # just fine; assume current
                                # Plumber
    $SIG\{"PIPE"\} = *Plumber;    # somewhat esoteric
    $SIG\{"PIPE"\} = Plumber();   # oops, what did Plumber()
                                # return??
.Ve
.Sp
Be sure not to use a bareword as the name of a signal handler,
lest you inadvertently call it.
.Sp
Using a string that doesn't correspond to any existing function or a
glob that doesn't contain a code slot is equivalent to \f(CW\*(AqIGNORE\*(Aq,
but a warning is emitted when the handler is being called (the warning
is not emitted for the internal hooks described below).
.Sp
If your system has the \f(CW\*(C`sigaction()\*(C' function then signal handlers
are installed using it.  This means you get reliable signal handling.
.Sp
The default delivery policy of signals changed in Perl v5.8.0 from
immediate (also known as \*(L"unsafe\*(R") to deferred, also known as \*(L"safe
signals\*(R".  See perlipc for more information.
.Sp
Certain internal hooks can be also set using the \f(CW%SIG hash.  The
routine indicated by \f(CW$SIG\{_\|_WARN_\|_\} is called when a warning
message is about to be printed.  The warning message is passed as the
first argument.  The presence of a \f(CW\*(C`_\|_WARN_\|_\*(C' hook causes the
ordinary printing of warnings to \f(CW\*(C`STDERR\*(C' to be suppressed.  You can
use this to save warnings in a variable, or turn warnings into fatal
errors, like this:
.Sp
.Vb 2
    local $SIG\{_\|_WARN_\|_\} = sub \{ die $_[0] \};
    eval $proggie;
.Ve
.Sp
As the \f(CW\*(AqIGNORE\*(Aq hook is not supported by \f(CW\*(C`_\|_WARN_\|_\*(C', its effect is
the same as using \f(CW\*(AqDEFAULT\*(Aq.  You can disable warnings using the
empty subroutine:
.Sp
.Vb 1
    local $SIG\{_\|_WARN_\|_\} = sub \{\};
.Ve
.Sp
The routine indicated by \f(CW$SIG\{_\|_DIE_\|_\} is called when a fatal
exception is about to be thrown.  The error message is passed as the
first argument.  When a \f(CW\*(C`_\|_DIE_\|_\*(C' hook routine returns, the exception
processing continues as it would have in the absence of the hook,
unless the hook routine itself exits via a \f(CW\*(C`goto &sub\*(C', a loop exit,
or a \f(CW\*(C`die()\*(C'.  The \f(CW\*(C`_\|_DIE_\|_\*(C' handler is explicitly disabled during
the call, so that you can die from a \f(CW\*(C`_\|_DIE_\|_\*(C' handler.  Similarly
for \f(CW\*(C`_\|_WARN_\|_\*(C'.
.Sp
The \f(CW$SIG\{_\|_DIE_\|_\} hook is called even inside an \f(CW\*(C`eval()\*(C'. It was
never intended to happen this way, but an implementation glitch made
this possible. This used to be deprecated, as it allowed strange action
at a distance like rewriting a pending exception in \f(CW$@. Plans to
rectify this have been scrapped, as users found that rewriting a
pending exception is actually a useful feature, and not a bug.
.Sp
The \f(CW$SIG\{_\|_DIE_\|_\} doesn't support \f(CW\*(AqIGNORE\*(Aq; it has the same
effect as \f(CW\*(AqDEFAULT\*(Aq.
.Sp
\f(CW\*(C`_\|_DIE_\|_\*(C'/\f(CW\*(C`_\|_WARN_\|_\*(C' handlers are very special in one respect: they
may be called to report (probable) errors found by the parser.  In such
a case the parser may be in inconsistent state, so any attempt to
evaluate Perl code from such a handler will probably result in a
segfault.  This means that warnings or errors that result from parsing
Perl should be used with extreme caution, like this:
.Sp
.Vb 5
    require Carp if defined $^S;
    Carp::confess("Something wrong") if defined &Carp::confess;
    die "Something wrong, but could not load Carp to give "
      . "backtrace...\\n\\t"
      . "To see backtrace try starting Perl with -MCarp switch";
.Ve
.Sp
Here the first line will load \f(CW\*(C`Carp\*(C' *unless* it is the parser who
called the handler.  The second line will print backtrace and die if
\f(CW\*(C`Carp\*(C' was available.  The third line will be executed only if \f(CW\*(C`Carp\*(C' was
not available.
.Sp
Having to even think about the \f(CW$^S variable in your exception
handlers is simply wrong.  \f(CW$SIG\{_\|_DIE_\|_\} as currently implemented
invites grievous and difficult to track down errors.  Avoid it
and use an \f(CW\*(C`END\{\}\*(C' or CORE::GLOBAL::die override instead.
.Sp
See \*(L"die\*(R" in perlfunc, \*(L"warn\*(R" in perlfunc, \*(L"eval\*(R" in perlfunc, and
warnings for additional information.
.ie n .IP "$BASETIME" 8
.el .IP "\f(CW$BASETIME" 8
Item "$BASETIME"
0

- $^T
Xref "$^T $BASETIME"
Item "$^T"
.PD
The time at which the program began running, in seconds since the
epoch (beginning of 1970).  The values returned by the **-M**, **-A**,
and **-C** filetests are based on this value.
.ie n .IP "$PERL_VERSION" 8
.el .IP "\f(CW$PERL_VERSION" 8
Item "$PERL_VERSION"
0

- $^V
Xref "$^V $PERL_VERSION"
Item "$^V"
.PD
The revision, version, and subversion of the Perl interpreter,
represented as a version object.
.Sp
This variable first appeared in perl v5.6.0; earlier versions of perl
will see an undefined value.  Before perl v5.10.0 \f(CW$^V was represented
as a v-string rather than a version object.
.Sp
\f(CW$^V can be used to determine whether the Perl interpreter executing
a script is in the right range of versions.  For example:
.Sp
.Vb 1
    warn "Hashes not randomized!\\n" if !$^V or $^V lt v5.8.1
.Ve
.Sp
While version objects overload stringification, to portably convert
\f(CW$^V into its string representation, use \f(CW\*(C`sprintf()\*(C''s \f(CW"%vd"
conversion, which works for both v-strings or version objects:
.Sp
.Vb 1
    printf "version is v%vd\\n", $^V;  # Perl\*(Aqs version
.Ve
.Sp
See the documentation of \f(CW\*(C`use VERSION\*(C' and \f(CW\*(C`require VERSION\*(C'
for a convenient way to fail if the running Perl interpreter is too old.
.Sp
See also \f(CW"$]" for a decimal representation of the Perl version.
.Sp
The main advantage of \f(CW$^V over \f(CW$] is that, for Perl v5.10.0 or
later, it overloads operators, allowing easy comparison against other
version representations (e.g. decimal, literal v-string, \*(L"v1.2.3\*(R", or
objects).  The disadvantage is that prior to v5.10.0, it was only a
literal v-string, which can't be easily printed or compared, whereas
the behavior of \f(CW$] is unchanged on all versions of Perl.
.Sp
Mnemonic: use ^V for a version object.

- $\{^WIN32_SLOPPY_STAT\}
Xref "$\{^WIN32_SLOPPY_STAT\} sitecustomize sitecustomize.pl"
Item "$\{^WIN32_SLOPPY_STAT\}"
This variable no longer has any function.
.Sp
This variable was added in Perl v5.10.0 and removed in Perl v5.34.0.
.ie n .IP "$EXECUTABLE_NAME" 8
.el .IP "\f(CW$EXECUTABLE_NAME" 8
Item "$EXECUTABLE_NAME"
0

- $^X
Xref "$^X $EXECUTABLE_NAME"
Item "$^X"
.PD
The name used to execute the current copy of Perl, from C's
\f(CW\*(C`argv[0]\*(C' or (where supported) */proc/self/exe*.
.Sp
Depending on the host operating system, the value of \f(CW$^X may be
a relative or absolute pathname of the perl program file, or may
be the string used to invoke perl but not the pathname of the
perl program file.  Also, most operating systems permit invoking
programs that are not in the \s-1PATH\s0 environment variable, so there
is no guarantee that the value of \f(CW$^X is in \s-1PATH.\s0  For \s-1VMS,\s0 the
value may or may not include a version number.
.Sp
You usually can use the value of \f(CW$^X to re-invoke an independent
copy of the same perl that is currently running, e.g.,
.Sp
.Vb 1
    @first_run = \`$^X -le "print int rand 100 for 1..100"\`;
.Ve
.Sp
But recall that not all operating systems support forking or
capturing of the output of commands, so this complex statement
may not be portable.
.Sp
It is not safe to use the value of \f(CW$^X as a path name of a file,
as some operating systems that have a mandatory suffix on
executable files do not require use of the suffix when invoking
a command.  To convert the value of \f(CW$^X to a path name, use the
following statements:
.Sp
.Vb 7
    # Build up a set of file names (not command names).
    use Config;
    my $this_perl = $^X;
    if ($^O ne \*(AqVMS\*(Aq) \{
        $this_perl .= $Config\{_exe\}
          unless $this_perl =~ m/$Config\{_exe\}$/i;
        \}
.Ve
.Sp
Because many operating systems permit anyone with read access to
the Perl program file to make a copy of it, patch the copy, and
then execute the copy, the security-conscious Perl programmer
should take care to invoke the installed copy of perl, not the
copy referenced by \f(CW$^X.  The following statements accomplish
this goal, and produce a pathname that can be invoked as a
command or referenced as a file.
.Sp
.Vb 6
    use Config;
    my $secure_perl_path = $Config\{perlpath\};
    if ($^O ne \*(AqVMS\*(Aq) \{
        $secure_perl_path .= $Config\{_exe\}
            unless $secure_perl_path =~ m/$Config\{_exe\}$/i;
        \}
.Ve

### Variables related to regular expressions

Subsection "Variables related to regular expressions"
Most of the special variables related to regular expressions are side
effects.  Perl sets these variables when it has a successful match, so
you should check the match result before using them.  For instance:

.Vb 3
    if( /P(A)TT(ER)N/ ) \{
        print "I found $1 and $2\\n";
        \}
.Ve

These variables are read-only and dynamically-scoped, unless we note
otherwise.

The dynamic nature of the regular expression variables means that
their value is limited to the block that they are in, as demonstrated
by this bit of code:

.Vb 2
    my $outer = \*(AqWallace and Grommit\*(Aq;
    my $inner = \*(AqMutt and Jeff\*(Aq;

    my $pattern = qr/(\\S+) and (\\S+)/;

    sub show_n \{ print "\\$1 is $1; \\$2 is $2\\n" \}

    \{
    OUTER:
        show_n() if $outer =~ m/$pattern/;

        INNER: \{
            show_n() if $inner =~ m/$pattern/;
            \}

        show_n();
    \}
.Ve

The output shows that while in the \f(CW\*(C`OUTER\*(C' block, the values of \f(CW$1
and \f(CW$2 are from the match against \f(CW$outer.  Inside the \f(CW\*(C`INNER\*(C'
block, the values of \f(CW$1 and \f(CW$2 are from the match against
\f(CW$inner, but only until the end of the block (i.e. the dynamic
scope).  After the \f(CW\*(C`INNER\*(C' block completes, the values of \f(CW$1 and
\f(CW$2 return to the values for the match against \f(CW$outer even though
we have not made another match:

.Vb 3
    $1 is Wallace; $2 is Grommit
    $1 is Mutt; $2 is Jeff
    $1 is Wallace; $2 is Grommit
.Ve

*Performance issues*
Subsection "Performance issues"

Traditionally in Perl, any use of any of the three variables  \f(CW\*(C`$\`\*(C', \f(CW$&
or \f(CW\*(C`$\*(Aq\*(C' (or their \f(CW\*(C`use English\*(C' equivalents) anywhere in the code, caused
all subsequent successful pattern matches to make a copy of the matched
string, in case the code might subsequently access one of those variables.
This imposed a considerable performance penalty across the whole program,
so generally the use of these variables has been discouraged.

In Perl 5.6.0 the \f(CW\*(C`@-\*(C' and \f(CW\*(C`@+\*(C' dynamic arrays were introduced that
supply the indices of successful matches. So you could for example do
this:

.Vb 1
    $str =~ /pattern/;

    print $\`, $&, $\*(Aq; # bad: performance hit

    print             # good: no performance hit
        substr($str, 0,     $-[0]),
        substr($str, $-[0], $+[0]-$-[0]),
        substr($str, $+[0]);
.Ve

In Perl 5.10.0 the \f(CW\*(C`/p\*(C' match operator flag and the \f(CW\*(C`$\{^PREMATCH\}\*(C',
\f(CW\*(C`$\{^MATCH\}\*(C', and \f(CW\*(C`$\{^POSTMATCH\}\*(C' variables were introduced, that allowed
you to suffer the penalties only on patterns marked with \f(CW\*(C`/p\*(C'.

In Perl 5.18.0 onwards, perl started noting the presence of each of the
three variables separately, and only copied that part of the string
required; so in

.Vb 1
    $\`; $&; "abcdefgh" =~ /d/
.Ve

perl would only copy the \*(L"abcd\*(R" part of the string. That could make a big
difference in something like

.Vb 3
    $str = \*(Aqx\*(Aq x 1_000_000;
    $&; # whoops
    $str =~ /x/g # one char copied a million times, not a million chars
.Ve

In Perl 5.20.0 a new copy-on-write system was enabled by default, which
finally fixes all performance issues with these three variables, and makes
them safe to use anywhere.

The \f(CW\*(C`Devel::NYTProf\*(C' and \f(CW\*(C`Devel::FindAmpersand\*(C' modules can help you
find uses of these problematic match variables in your code.
.ie n .IP "$<*digits*> ($1, $2, ...)" 8
.el .IP "$<*digits*> ($1, \f(CW$2, ...)" 8
Xref "$1 $2 $3 $\f(ISdigits\f(IE"
Item "$<digits> ($1, $2, ...)"
Contains the subpattern from the corresponding set of capturing
parentheses from the last successful pattern match, not counting patterns
matched in nested blocks that have been exited already.
.Sp
Note there is a distinction between a capture buffer which matches
the empty string a capture buffer which is optional. Eg, \f(CW\*(C`(x?)\*(C' and
\f(CW\*(C`(x)?\*(C' The latter may be undef, the former not.
.Sp
These variables are read-only and dynamically-scoped.
.Sp
Mnemonic: like \\digits.

- @\{^CAPTURE\}
Xref "@\{^CAPTURE\} @^CAPTURE"
Item "@\{^CAPTURE\}"
An array which exposes the contents of the capture buffers, if any, of
the last successful pattern match, not counting patterns matched
in nested blocks that have been exited already.
.Sp
Note that the 0 index of @\{^CAPTURE\} is equivalent to \f(CW$1, the 1 index
is equivalent to \f(CW$2, etc.
.Sp
.Vb 3
    if ("foal"=~/(.)(.)(.)(.)/) \{
        print join "-", @\{^CAPTURE\};
    \}
.Ve
.Sp
should output \*(L"f-o-a-l\*(R".
.Sp
See also "$<*digits*> ($1, \f(CW$2, ...)", \*(L"%\{^CAPTURE\}\*(R" and
\*(L"%\{^CAPTURE_ALL\}\*(R".
.Sp
Note that unlike most other regex magic variables there is no single
letter equivalent to \f(CW\*(C`@\{^CAPTURE\}\*(C'.
.Sp
This variable was added in 5.25.7
.ie n .IP "$MATCH" 8
.el .IP "\f(CW$MATCH" 8
Item "$MATCH"
0

- $&
Xref "$& $MATCH"
.PD
The string matched by the last successful pattern match (not counting
any matches hidden within a \s-1BLOCK\s0 or \f(CW\*(C`eval()\*(C' enclosed by the current
\s-1BLOCK\s0).
.Sp
See \*(L"Performance issues\*(R" above for the serious performance implications
of using this variable (even once) in your code.
.Sp
This variable is read-only and dynamically-scoped.
.Sp
Mnemonic: like \f(CW\*(C`&\*(C' in some editors.

- $\{^MATCH\}
Xref "$\{^MATCH\}"
Item "$\{^MATCH\}"
This is similar to \f(CW$& (\f(CW$MATCH) except that it does not incur the
performance penalty associated with that variable.
.Sp
See \*(L"Performance issues\*(R" above.
.Sp
In Perl v5.18 and earlier, it is only guaranteed
to return a defined value when the pattern was compiled or executed with
the \f(CW\*(C`/p\*(C' modifier.  In Perl v5.20, the \f(CW\*(C`/p\*(C' modifier does nothing, so
\f(CW\*(C`$\{^MATCH\}\*(C' does the same thing as \f(CW$MATCH.
.Sp
This variable was added in Perl v5.10.0.
.Sp
This variable is read-only and dynamically-scoped.
.ie n .IP "$PREMATCH" 8
.el .IP "\f(CW$PREMATCH" 8
Item "$PREMATCH"
0

- $`
Xref "$` $PREMATCH $\{^PREMATCH\}"
.PD
The string preceding whatever was matched by the last successful
pattern match, not counting any matches hidden within a \s-1BLOCK\s0 or \f(CW\*(C`eval\*(C'
enclosed by the current \s-1BLOCK.\s0
.Sp
See \*(L"Performance issues\*(R" above for the serious performance implications
of using this variable (even once) in your code.
.Sp
This variable is read-only and dynamically-scoped.
.Sp
Mnemonic: \f(CW\*(C`\`\*(C' often precedes a quoted string.

- $\{^PREMATCH\}
Xref "$` $\{^PREMATCH\}"
Item "$\{^PREMATCH\}"
This is similar to \f(CW\*(C`$\`\*(C' ($PREMATCH) except that it does not incur the
performance penalty associated with that variable.
.Sp
See \*(L"Performance issues\*(R" above.
.Sp
In Perl v5.18 and earlier, it is only guaranteed
to return a defined value when the pattern was compiled or executed with
the \f(CW\*(C`/p\*(C' modifier.  In Perl v5.20, the \f(CW\*(C`/p\*(C' modifier does nothing, so
\f(CW\*(C`$\{^PREMATCH\}\*(C' does the same thing as \f(CW$PREMATCH.
.Sp
This variable was added in Perl v5.10.0.
.Sp
This variable is read-only and dynamically-scoped.
.ie n .IP "$POSTMATCH" 8
.el .IP "\f(CW$POSTMATCH" 8
Item "$POSTMATCH"
0

- $'
Xref "$' $POSTMATCH $\{^POSTMATCH\} @-"
.PD
The string following whatever was matched by the last successful
pattern match (not counting any matches hidden within a \s-1BLOCK\s0 or \f(CW\*(C`eval()\*(C'
enclosed by the current \s-1BLOCK\s0).  Example:
.Sp
.Vb 3
    local $_ = \*(Aqabcdefghi\*(Aq;
    /def/;
    print "$\`:$&:$\*(Aq\\n";         # prints abc:def:ghi
.Ve
.Sp
See \*(L"Performance issues\*(R" above for the serious performance implications
of using this variable (even once) in your code.
.Sp
This variable is read-only and dynamically-scoped.
.Sp
Mnemonic: \f(CW\*(C`\*(Aq\*(C' often follows a quoted string.

- $\{^POSTMATCH\}
Xref "$\{^POSTMATCH\} $' $POSTMATCH"
Item "$\{^POSTMATCH\}"
This is similar to \f(CW\*(C`$\*(Aq\*(C' (\f(CW$POSTMATCH) except that it does not incur the
performance penalty associated with that variable.
.Sp
See \*(L"Performance issues\*(R" above.
.Sp
In Perl v5.18 and earlier, it is only guaranteed
to return a defined value when the pattern was compiled or executed with
the \f(CW\*(C`/p\*(C' modifier.  In Perl v5.20, the \f(CW\*(C`/p\*(C' modifier does nothing, so
\f(CW\*(C`$\{^POSTMATCH\}\*(C' does the same thing as \f(CW$POSTMATCH.
.Sp
This variable was added in Perl v5.10.0.
.Sp
This variable is read-only and dynamically-scoped.
.ie n .IP "$LAST_PAREN_MATCH" 8
.el .IP "\f(CW$LAST_PAREN_MATCH" 8
Item "$LAST_PAREN_MATCH"
0

- $+
Xref "$+ $LAST_PAREN_MATCH"
.PD
The text matched by the highest used capture group of the last
successful search pattern.  It is logically equivalent to the highest
numbered capture variable (\f(CW$1, \f(CW$2, ...) which has a defined value.
.Sp
This is useful if you don't know which one of a set of alternative patterns
matched.  For example:
.Sp
.Vb 1
    /Version: (.*)|Revision: (.*)/ && ($rev = $+);
.Ve
.Sp
This variable is read-only and dynamically-scoped.
.Sp
Mnemonic: be positive and forward looking.
.ie n .IP "$LAST_SUBMATCH_RESULT" 8
.el .IP "\f(CW$LAST_SUBMATCH_RESULT" 8
Item "$LAST_SUBMATCH_RESULT"
0

- $^N
Xref "$^N $LAST_SUBMATCH_RESULT"
Item "$^N"
.PD
The text matched by the used group most-recently closed (i.e. the group
with the rightmost closing parenthesis) of the last successful search
pattern. This is subtly different from \f(CW$+. For example in
.Sp
.Vb 1
    "ab" =~ /^((.)(.))$/
.Ve
.Sp
we have
.Sp
.Vb 3
    $1,$^N   have the value "ab"
    $2       has  the value "a"
    $3,$+    have the value "b"
.Ve
.Sp
This is primarily used inside \f(CW\*(C`(?\{...\})\*(C' blocks for examining text
recently matched.  For example, to effectively capture text to a variable
(in addition to \f(CW$1, \f(CW$2, etc.), replace \f(CW\*(C`(...)\*(C' with
.Sp
.Vb 1
    (?:(...)(?\{ $var = $^N \}))
.Ve
.Sp
By setting and then using \f(CW$var in this way relieves you from having to
worry about exactly which numbered set of parentheses they are.
.Sp
This variable was added in Perl v5.8.0.
.Sp
Mnemonic: the (possibly) Nested parenthesis that most recently closed.
.ie n .IP "@LAST_MATCH_END" 8
.el .IP "\f(CW@LAST_MATCH_END" 8
Item "@LAST_MATCH_END"
0

- @+
Xref "@+ @LAST_MATCH_END"
.PD
This array holds the offsets of the ends of the last successful
submatches in the currently active dynamic scope.  \f(CW$+[0] is
the offset into the string of the end of the entire match.  This
is the same value as what the \f(CW\*(C`pos\*(C' function returns when called
on the variable that was matched against.  The *n*th element
of this array holds the offset of the *n*th submatch, so
\f(CW$+[1] is the offset past where \f(CW$1 ends, \f(CW$+[2] the offset
past where \f(CW$2 ends, and so on.  You can use \f(CW$#+ to determine
how many subgroups were in the last successful match.  See the
examples given for the \f(CW\*(C`@-\*(C' variable.
.Sp
This variable was added in Perl v5.6.0.

- %\{^CAPTURE\}
Item "%\{^CAPTURE\}"
0
.ie n .IP "%LAST_PAREN_MATCH" 8
.el .IP "\f(CW%LAST_PAREN_MATCH" 8
Item "%LAST_PAREN_MATCH"

- %+
Xref "%+ %LAST_PAREN_MATCH %\{^CAPTURE\}"
.PD
Similar to \f(CW\*(C`@+\*(C', the \f(CW\*(C`%+\*(C' hash allows access to the named capture
buffers, should they exist, in the last successful match in the
currently active dynamic scope.
.Sp
For example, \f(CW$+\{foo\} is equivalent to \f(CW$1 after the following match:
.Sp
.Vb 1
    \*(Aqfoo\*(Aq =~ /(?<foo>foo)/;
.Ve
.Sp
The keys of the \f(CW\*(C`%+\*(C' hash list only the names of buffers that have
captured (and that are thus associated to defined values).
.Sp
If multiple distinct capture groups have the same name, then
\f(CW$+\{NAME\} will refer to the leftmost defined group in the match.
.Sp
The underlying behaviour of \f(CW\*(C`%+\*(C' is provided by the
Tie::Hash::NamedCapture module.
.Sp
**Note:** \f(CW\*(C`%-\*(C' and \f(CW\*(C`%+\*(C' are tied views into a common internal hash
associated with the last successful regular expression.  Therefore mixing
iterative access to them via \f(CW\*(C`each\*(C' may have unpredictable results.
Likewise, if the last successful match changes, then the results may be
surprising.
.Sp
This variable was added in Perl v5.10.0. The \f(CW\*(C`%\{^CAPTURE\}\*(C' alias was
added in 5.25.7.
.Sp
This variable is read-only and dynamically-scoped.
.ie n .IP "@LAST_MATCH_START" 8
.el .IP "\f(CW@LAST_MATCH_START" 8
Item "@LAST_MATCH_START"
0

- @-
Xref "@- @LAST_MATCH_START"
.PD
\f(CW\*(C`$-[0]\*(C' is the offset of the start of the last successful match.
\f(CW\*(C`$-[\f(CIn\f(CW]\*(C' is the offset of the start of the substring matched by
*n*-th subpattern, or undef if the subpattern did not match.
.Sp
Thus, after a match against \f(CW$_, \f(CW$& coincides with \f(CW\*(C`substr $_, $-[0],
$+[0] - $-[0]\*(C'.  Similarly, $*n* coincides with \f(CW\*(C`substr $_, $-[n],
$+[n] - $-[n]\*(C' if \f(CW\*(C`$-[n]\*(C' is defined, and $+ coincides with
\f(CW\*(C`substr $_, $-[$#-], $+[$#-] - $-[$#-]\*(C'.  One can use \f(CW\*(C`$#-\*(C' to find the
last matched subgroup in the last successful match.  Contrast with
\f(CW$#+, the number of subgroups in the regular expression.  Compare
with \f(CW\*(C`@+\*(C'.
.Sp
This array holds the offsets of the beginnings of the last
successful submatches in the currently active dynamic scope.
\f(CW\*(C`$-[0]\*(C' is the offset into the string of the beginning of the
entire match.  The *n*th element of this array holds the offset
of the *n*th submatch, so \f(CW\*(C`$-[1]\*(C' is the offset where \f(CW$1
begins, \f(CW\*(C`$-[2]\*(C' the offset where \f(CW$2 begins, and so on.
.Sp
After a match against some variable \f(CW$var:

> .ie n .IP """$\`"" is the same as ""substr($var, 0, $-[0])""" 5
.el .IP "\f(CW$\` is the same as \f(CWsubstr($var, 0, $-[0])" 5
Item "$ is the same as substr($var, 0, $-[0])"
0
.ie n .IP "$& is the same as ""substr($var, $-[0], $+[0] - $-[0])""" 5
.el .IP "\f(CW$& is the same as \f(CWsubstr($var, $-[0], $+[0] - $-[0])" 5
Item "$& is the same as substr($var, $-[0], $+[0] - $-[0])"
.ie n .IP """$\*(Aq"" is the same as ""substr($var, $+[0])""" 5
.el .IP "\f(CW$\*(Aq is the same as \f(CWsubstr($var, $+[0])" 5
Item "$ is the same as substr($var, $+[0])"
.ie n .IP "$1 is the same as ""substr($var, $-[1], $+[1] - $-[1])""" 5
.el .IP "\f(CW$1 is the same as \f(CWsubstr($var, $-[1], $+[1] - $-[1])" 5
Item "$1 is the same as substr($var, $-[1], $+[1] - $-[1])"
.ie n .IP "$2 is the same as ""substr($var, $-[2], $+[2] - $-[2])""" 5
.el .IP "\f(CW$2 is the same as \f(CWsubstr($var, $-[2], $+[2] - $-[2])" 5
Item "$2 is the same as substr($var, $-[2], $+[2] - $-[2])"
.ie n .IP "$3 is the same as ""substr($var, $-[3], $+[3] - $-[3])""" 5
.el .IP "\f(CW$3 is the same as \f(CWsubstr($var, $-[3], $+[3] - $-[3])" 5
Item "$3 is the same as substr($var, $-[3], $+[3] - $-[3])"



> .PD
.Sp
This variable was added in Perl v5.6.0.



- %\{^CAPTURE_ALL\}
Xref "%\{^CAPTURE_ALL\}"
Item "%\{^CAPTURE_ALL\}"
0

- %-
Xref "%-"
.PD
Similar to \f(CW\*(C`%+\*(C', this variable allows access to the named capture groups
in the last successful match in the currently active dynamic scope.  To
each capture group name found in the regular expression, it associates a
reference to an array containing the list of values captured by all
buffers with that name (should there be several of them), in the order
where they appear.
.Sp
Here's an example:
.Sp
.Vb 12
    if (\*(Aq1234\*(Aq =~ /(?<A>1)(?<B>2)(?<A>3)(?<B>4)/) \{
        foreach my $bufname (sort keys %-) \{
            my $ary = $-\{$bufname\};
            foreach my $idx (0..$#$ary) \{
                print "\\$-\{$bufname\}[$idx] : ",
                      (defined($ary->[$idx])
                          ? "\*(Aq$ary->[$idx]\*(Aq"
                          : "undef"),
                      "\\n";
            \}
        \}
    \}
.Ve
.Sp
would print out:
.Sp
.Vb 4
    $-\{A\}[0] : \*(Aq1\*(Aq
    $-\{A\}[1] : \*(Aq3\*(Aq
    $-\{B\}[0] : \*(Aq2\*(Aq
    $-\{B\}[1] : \*(Aq4\*(Aq
.Ve
.Sp
The keys of the \f(CW\*(C`%-\*(C' hash correspond to all buffer names found in
the regular expression.
.Sp
The behaviour of \f(CW\*(C`%-\*(C' is implemented via the
Tie::Hash::NamedCapture module.
.Sp
**Note:** \f(CW\*(C`%-\*(C' and \f(CW\*(C`%+\*(C' are tied views into a common internal hash
associated with the last successful regular expression.  Therefore mixing
iterative access to them via \f(CW\*(C`each\*(C' may have unpredictable results.
Likewise, if the last successful match changes, then the results may be
surprising.
.Sp
This variable was added in Perl v5.10.0. The \f(CW\*(C`%\{^CAPTURE_ALL\}\*(C' alias was
added in 5.25.7.
.Sp
This variable is read-only and dynamically-scoped.
.ie n .IP "$LAST_REGEXP_CODE_RESULT" 8
.el .IP "\f(CW$LAST_REGEXP_CODE_RESULT" 8
Item "$LAST_REGEXP_CODE_RESULT"
0

- $^R
Xref "$^R $LAST_REGEXP_CODE_RESULT"
Item "$^R"
.PD
The result of evaluation of the last successful \f(CW\*(C`(?\{ code \})\*(C'
regular expression assertion (see perlre).  May be written to.
.Sp
This variable was added in Perl 5.005.

- $\{^RE_COMPILE_RECURSION_LIMIT\}
Xref "$\{^RE_COMPILE_RECURSION_LIMIT\}"
Item "$\{^RE_COMPILE_RECURSION_LIMIT\}"
The current value giving the maximum number of open but unclosed
parenthetical groups there may be at any point during a regular
expression compilation.  The default is currently 1000 nested groups.
You may adjust it depending on your needs and the amount of memory
available.
.Sp
This variable was added in Perl v5.30.0.

- $\{^RE_DEBUG_FLAGS\}
Xref "$\{^RE_DEBUG_FLAGS\}"
Item "$\{^RE_DEBUG_FLAGS\}"
The current value of the regex debugging flags.  Set to 0 for no debug output
even when the \f(CW\*(C`re \*(Aqdebug\*(Aq\*(C' module is loaded.  See re for details.
.Sp
This variable was added in Perl v5.10.0.

- $\{^RE_TRIE_MAXBUF\}
Xref "$\{^RE_TRIE_MAXBUF\}"
Item "$\{^RE_TRIE_MAXBUF\}"
Controls how certain regex optimisations are applied and how much memory they
utilize.  This value by default is 65536 which corresponds to a 512kB
temporary cache.  Set this to a higher value to trade
memory for speed when matching large alternations.  Set
it to a lower value if you want the optimisations to
be as conservative of memory as possible but still occur, and set it to a
negative value to prevent the optimisation and conserve the most memory.
Under normal situations this variable should be of no interest to you.
.Sp
This variable was added in Perl v5.10.0.

### Variables related to filehandles

Subsection "Variables related to filehandles"
Variables that depend on the currently selected filehandle may be set
by calling an appropriate object method on the \f(CW\*(C`IO::Handle\*(C' object,
although this is less efficient than using the regular built-in
variables.  (Summary lines below for this contain the word \s-1HANDLE.\s0)
First you must say

.Vb 1
    use IO::Handle;
.Ve

after which you may use either

.Vb 1
    method HANDLE EXPR
.Ve

or more safely,

.Vb 1
    HANDLE->method(EXPR)
.Ve

Each method returns the old value of the \f(CW\*(C`IO::Handle\*(C' attribute.  The
methods each take an optional \s-1EXPR,\s0 which, if supplied, specifies the
new value for the \f(CW\*(C`IO::Handle\*(C' attribute in question.  If not
supplied, most methods do nothing to the current value\*(--except for
\f(CW\*(C`autoflush()\*(C', which will assume a 1 for you, just to be different.

Because loading in the \f(CW\*(C`IO::Handle\*(C' class is an expensive operation,
you should learn how to use the regular built-in variables.

A few of these variables are considered \*(L"read-only\*(R".  This means that
if you try to assign to this variable, either directly or indirectly
through a reference, you'll raise a run-time exception.

You should be very careful when modifying the default values of most
special variables described in this document.  In most cases you want
to localize these variables before changing them, since if you don't,
the change may affect other modules which rely on the default values
of the special variables that you have changed.  This is one of the
correct ways to read the whole file at once:

.Vb 4
    open my $fh, "<", "foo" or die $!;
    local $/; # enable localized slurp mode
    my $content = <$fh>;
    close $fh;
.Ve

But the following code is quite bad:

.Vb 4
    open my $fh, "<", "foo" or die $!;
    undef $/; # enable slurp mode
    my $content = <$fh>;
    close $fh;
.Ve

since some other module, may want to read data from some file in the
default \*(L"line mode\*(R", so if the code we have just presented has been
executed, the global value of \f(CW$/ is now changed for any other code
running inside the same Perl interpreter.

Usually when a variable is localized you want to make sure that this
change affects the shortest scope possible.  So unless you are already
inside some short \f(CW\*(C`\{\}\*(C' block, you should create one yourself.  For
example:

.Vb 7
    my $content = \*(Aq\*(Aq;
    open my $fh, "<", "foo" or die $!;
    \{
        local $/;
        $content = <$fh>;
    \}
    close $fh;
.Ve

Here is an example of how your own code can go broken:

.Vb 5
    for ( 1..3 )\{
        $\\ = "\\r\\n";
        nasty_break();
        print "$_";
    \}

    sub nasty_break \{
        $\\ = "\\f";
        # do something with $_
    \}
.Ve

You probably expect this code to print the equivalent of

.Vb 1
    "1\\r\\n2\\r\\n3\\r\\n"
.Ve

but instead you get:

.Vb 1
    "1\\f2\\f3\\f"
.Ve

Why? Because \f(CW\*(C`nasty_break()\*(C' modifies \f(CW\*(C`$\\\*(C' without localizing it
first.  The value you set in  \f(CW\*(C`nasty_break()\*(C' is still there when you
return.  The fix is to add \f(CW\*(C`local()\*(C' so the value doesn't leak out of
\f(CW\*(C`nasty_break()\*(C':

.Vb 1
    local $\\ = "\\f";
.Ve

It's easy to notice the problem in such a short example, but in more
complicated code you are looking for trouble if you don't localize
changes to the special variables.
.ie n .IP "$ARGV" 8
.el .IP "\f(CW$ARGV" 8
Xref "$ARGV"
Item "$ARGV"
Contains the name of the current file when reading from \f(CW\*(C`<>\*(C'.
.ie n .IP "@ARGV" 8
.el .IP "\f(CW@ARGV" 8
Xref "@ARGV"
Item "@ARGV"
The array \f(CW@ARGV contains the command-line arguments intended for
the script.  \f(CW$#ARGV is generally the number of arguments minus
one, because \f(CW$ARGV[0] is the first argument, *not* the program's
command name itself.  See \*(L"$0\*(R" for the command name.

- \s-1ARGV\s0
Xref "ARGV"
Item "ARGV"
The special filehandle that iterates over command-line filenames in
\f(CW@ARGV.  Usually written as the null filehandle in the angle operator
\f(CW\*(C`<>\*(C'.  Note that currently \f(CW\*(C`ARGV\*(C' only has its magical effect
within the \f(CW\*(C`<>\*(C' operator; elsewhere it is just a plain filehandle
corresponding to the last file opened by \f(CW\*(C`<>\*(C'.  In particular,
passing \f(CW\*(C`\\*ARGV\*(C' as a parameter to a function that expects a filehandle
may not cause your function to automatically read the contents of all the
files in \f(CW@ARGV.

- \s-1ARGVOUT\s0
Xref "ARGVOUT"
Item "ARGVOUT"
The special filehandle that points to the currently open output file
when doing edit-in-place processing with **-i**.  Useful when you have
to do a lot of inserting and don't want to keep modifying \f(CW$_.  See
perlrun for the **-i** switch.

- IO::Handle->output_field_separator( \s-1EXPR\s0 )
Item "IO::Handle->output_field_separator( EXPR )"
0
.ie n .IP "$OUTPUT_FIELD_SEPARATOR" 8
.el .IP "\f(CW$OUTPUT_FIELD_SEPARATOR" 8
Item "$OUTPUT_FIELD_SEPARATOR"
.ie n .IP "$OFS" 8
.el .IP "\f(CW$OFS" 8
Item "$OFS"

- $,
Xref "$, $OFS $OUTPUT_FIELD_SEPARATOR"
.PD
The output field separator for the print operator.  If defined, this
value is printed between each of print's arguments.  Default is \f(CW\*(C`undef\*(C'.
.Sp
You cannot call \f(CW\*(C`output_field_separator()\*(C' on a handle, only as a
static method.  See IO::Handle.
.Sp
Mnemonic: what is printed when there is a \*(L",\*(R" in your print statement.

- \s-1HANDLE-\s0>input_line_number( \s-1EXPR\s0 )
Item "HANDLE->input_line_number( EXPR )"
0
.ie n .IP "$INPUT_LINE_NUMBER" 8
.el .IP "\f(CW$INPUT_LINE_NUMBER" 8
Item "$INPUT_LINE_NUMBER"
.ie n .IP "$NR" 8
.el .IP "\f(CW$NR" 8
Item "$NR"

- $.
Xref "$. $NR $INPUT_LINE_NUMBER line number"
.PD
Current line number for the last filehandle accessed.
.Sp
Each filehandle in Perl counts the number of lines that have been read
from it.  (Depending on the value of \f(CW$/, Perl's idea of what
constitutes a line may not match yours.)  When a line is read from a
filehandle (via \f(CW\*(C`readline()\*(C' or \f(CW\*(C`<>\*(C'), or when \f(CW\*(C`tell()\*(C' or
\f(CW\*(C`seek()\*(C' is called on it, \f(CW$. becomes an alias to the line counter
for that filehandle.
.Sp
You can adjust the counter by assigning to \f(CW$., but this will not
actually move the seek pointer.  \fILocalizing \f(CI$.\fI will not localize
the filehandle's line count.  Instead, it will localize perl's notion
of which filehandle \f(CW$. is currently aliased to.
.Sp
\f(CW$. is reset when the filehandle is closed, but **not** when an open
filehandle is reopened without an intervening \f(CW\*(C`close()\*(C'.  For more
details, see \*(L"I/O Operators\*(R" in perlop.  Because \f(CW\*(C`<>\*(C' never does
an explicit close, line numbers increase across \f(CW\*(C`ARGV\*(C' files (but see
examples in \*(L"eof\*(R" in perlfunc).
.Sp
You can also use \f(CW\*(C`HANDLE->input_line_number(EXPR)\*(C' to access the
line counter for a given filehandle without having to worry about
which handle you last accessed.
.Sp
Mnemonic: many programs use \*(L".\*(R" to mean the current line number.

- IO::Handle->input_record_separator( \s-1EXPR\s0 )
Item "IO::Handle->input_record_separator( EXPR )"
0
.ie n .IP "$INPUT_RECORD_SEPARATOR" 8
.el .IP "\f(CW$INPUT_RECORD_SEPARATOR" 8
Item "$INPUT_RECORD_SEPARATOR"
.ie n .IP "$RS" 8
.el .IP "\f(CW$RS" 8
Item "$RS"

- $/
Xref "$ $RS $INPUT_RECORD_SEPARATOR"
.PD
The input record separator, newline by default.  This influences Perl's
idea of what a \*(L"line\*(R" is.  Works like **awk**'s \s-1RS\s0 variable, including
treating empty lines as a terminator if set to the null string (an
empty line cannot contain any spaces or tabs).  You may set it to a
multi-character string to match a multi-character terminator, or to
\f(CW\*(C`undef\*(C' to read through the end of file.  Setting it to \f(CW"\\n\\n"
means something slightly different than setting to \f(CW"", if the file
contains consecutive empty lines.  Setting to \f(CW"" will treat two or
more consecutive empty lines as a single empty line.  Setting to
\f(CW"\\n\\n" will blindly assume that the next input character belongs to
the next paragraph, even if it's a newline.
.Sp
.Vb 3
    local $/;           # enable "slurp" mode
    local $_ = <FH>;    # whole file now here
    s/\\n[ \\t]+/ /g;
.Ve
.Sp
Remember: the value of \f(CW$/ is a string, not a regex.  **awk** has to
be better for something. :-)
.Sp
Setting \f(CW$/ to an empty string \*(-- the so-called *paragraph mode* \*(-- merits
special attention.  When \f(CW$/ is set to \f(CW"" and the entire file is read in
with that setting, any sequence of one or more consecutive newlines at the
beginning of the file is discarded.  With the exception of the final record in
the file, each sequence of characters ending in two or more newlines is
treated as one record and is read in to end in exactly two newlines.  If the
last record in the file ends in zero or one consecutive newlines, that record
is read in with that number of newlines.  If the last record ends in two or
more consecutive newlines, it is read in with two newlines like all preceding
records.
.Sp
Suppose we wrote the following string to a file:
.Sp
.Vb 4
    my $string = "\\n\\n\\n";
    $string .= "alpha beta\\ngamma delta\\n\\n\\n";
    $string .= "epsilon zeta eta\\n\\n";
    $string .= "theta\\n";

    my $file = \*(Aqsimple_file.txt\*(Aq;
    open my $OUT, \*(Aq>\*(Aq, $file or die;
    print $OUT $string;
    close $OUT or die;
.Ve
.Sp
Now we read that file in paragraph mode:
.Sp
.Vb 4
    local $/ = ""; # paragraph mode
    open my $IN, \*(Aq<\*(Aq, $file or die;
    my @records = <$IN>;
    close $IN or die;
.Ve
.Sp
\f(CW@records will consist of these 3 strings:
.Sp
.Vb 5
    (
      "alpha beta\\ngamma delta\\n\\n",
      "epsilon zeta eta\\n\\n",
      "theta\\n",
    )
.Ve
.Sp
Setting \f(CW$/ to a reference to an integer, scalar containing an
integer, or scalar that's convertible to an integer will attempt to
read records instead of lines, with the maximum record size being the
referenced integer number of characters.  So this:
.Sp
.Vb 3
    local $/ = \\32768; # or \\"32768", or \\$var_containing_32768
    open my $fh, "<", $myfile or die $!;
    local $_ = <$fh>;
.Ve
.Sp
will read a record of no more than 32768 characters from \f(CW$fh.  If you're
not reading from a record-oriented file (or your \s-1OS\s0 doesn't have
record-oriented files), then you'll likely get a full chunk of data
with every read.  If a record is larger than the record size you've
set, you'll get the record back in pieces.  Trying to set the record
size to zero or less is deprecated and will cause $/ to have the value
of \*(L"undef\*(R", which will cause reading in the (rest of the) whole file.
.Sp
As of 5.19.9 setting \f(CW$/ to any other form of reference will throw a
fatal exception. This is in preparation for supporting new ways to set
\f(CW$/ in the future.
.Sp
On \s-1VMS\s0 only, record reads bypass PerlIO layers and any associated
buffering, so you must not mix record and non-record reads on the
same filehandle.  Record mode mixes with line mode only when the
same buffering layer is in use for both modes.
.Sp
You cannot call \f(CW\*(C`input_record_separator()\*(C' on a handle, only as a
static method.  See IO::Handle.
.Sp
See also \*(L"Newlines\*(R" in perlport.  Also see \*(L"$.\*(R".
.Sp
Mnemonic: / delimits line boundaries when quoting poetry.

- IO::Handle->output_record_separator( \s-1EXPR\s0 )
Item "IO::Handle->output_record_separator( EXPR )"
0
.ie n .IP "$OUTPUT_RECORD_SEPARATOR" 8
.el .IP "\f(CW$OUTPUT_RECORD_SEPARATOR" 8
Item "$OUTPUT_RECORD_SEPARATOR"
.ie n .IP "$ORS" 8
.el .IP "\f(CW$ORS" 8
Item "$ORS"

- $\\
Xref "$\ $ORS $OUTPUT_RECORD_SEPARATOR"
Item "$"
.PD
The output record separator for the print operator.  If defined, this
value is printed after the last of print's arguments.  Default is \f(CW\*(C`undef\*(C'.
.Sp
You cannot call \f(CW\*(C`output_record_separator()\*(C' on a handle, only as a
static method.  See IO::Handle.
.Sp
Mnemonic: you set \f(CW\*(C`$\\\*(C' instead of adding \*(L"\\n\*(R" at the end of the print.
Also, it's just like \f(CW$/, but it's what you get \*(L"back\*(R" from Perl.

- \s-1HANDLE-\s0>autoflush( \s-1EXPR\s0 )
Item "HANDLE->autoflush( EXPR )"
0
.ie n .IP "$OUTPUT_AUTOFLUSH" 8
.el .IP "\f(CW$OUTPUT_AUTOFLUSH" 8
Item "$OUTPUT_AUTOFLUSH"

- $|
Xref "$| autoflush flush $OUTPUT_AUTOFLUSH"
.PD
If set to nonzero, forces a flush right away and after every write or
print on the currently selected output channel.  Default is 0
(regardless of whether the channel is really buffered by the system or
not; \f(CW$| tells you only whether you've asked Perl explicitly to
flush after each write).  \s-1STDOUT\s0 will typically be line buffered if
output is to the terminal and block buffered otherwise.  Setting this
variable is useful primarily when you are outputting to a pipe or
socket, such as when you are running a Perl program under **rsh** and
want to see the output as it's happening.  This has no effect on input
buffering.  See \*(L"getc\*(R" in perlfunc for that.  See \*(L"select\*(R" in perlfunc on
how to select the output channel.  See also IO::Handle.
.Sp
Mnemonic: when you want your pipes to be piping hot.

- $\{^LAST_FH\}
Xref "$\{^LAST_FH\}"
Item "$\{^LAST_FH\}"
This read-only variable contains a reference to the last-read filehandle.
This is set by \f(CW\*(C`<HANDLE>\*(C', \f(CW\*(C`readline\*(C', \f(CW\*(C`tell\*(C', \f(CW\*(C`eof\*(C' and \f(CW\*(C`seek\*(C'.
This is the same handle that \f(CW$. and \f(CW\*(C`tell\*(C' and \f(CW\*(C`eof\*(C' without arguments
use.  It is also the handle used when Perl appends \*(L", <\s-1STDIN\s0> line 1\*(R" to
an error or warning message.
.Sp
This variable was added in Perl v5.18.0.

*Variables related to formats*
Subsection "Variables related to formats"

The special variables for formats are a subset of those for
filehandles.  See perlform for more information about Perl's
formats.
.ie n .IP "$ACCUMULATOR" 8
.el .IP "\f(CW$ACCUMULATOR" 8
Item "$ACCUMULATOR"
0

- $^A
Xref "$^A $ACCUMULATOR"
Item "$^A"
.PD
The current value of the \f(CW\*(C`write()\*(C' accumulator for \f(CW\*(C`format()\*(C' lines.
A format contains \f(CW\*(C`formline()\*(C' calls that put their result into
\f(CW$^A.  After calling its format, \f(CW\*(C`write()\*(C' prints out the contents
of \f(CW$^A and empties.  So you never really see the contents of \f(CW$^A
unless you call \f(CW\*(C`formline()\*(C' yourself and then look at it.  See
perlform and \*(L"formline \s-1PICTURE,LIST\*(R"\s0 in perlfunc.

- IO::Handle->format_formfeed(\s-1EXPR\s0)
Item "IO::Handle->format_formfeed(EXPR)"
0
.ie n .IP "$FORMAT_FORMFEED" 8
.el .IP "\f(CW$FORMAT_FORMFEED" 8
Item "$FORMAT_FORMFEED"

- $^L
Xref "$^L $FORMAT_FORMFEED"
Item "$^L"
.PD
What formats output as a form feed.  The default is \f(CW\*(C`\\f\*(C'.
.Sp
You cannot call \f(CW\*(C`format_formfeed()\*(C' on a handle, only as a static
method.  See IO::Handle.

- \s-1HANDLE-\s0>format_page_number(\s-1EXPR\s0)
Item "HANDLE->format_page_number(EXPR)"
0
.ie n .IP "$FORMAT_PAGE_NUMBER" 8
.el .IP "\f(CW$FORMAT_PAGE_NUMBER" 8
Item "$FORMAT_PAGE_NUMBER"

- $%
Xref "$% $FORMAT_PAGE_NUMBER"
.PD
The current page number of the currently selected output channel.
.Sp
Mnemonic: \f(CW\*(C`%\*(C' is page number in **nroff**.

- \s-1HANDLE-\s0>format_lines_left(\s-1EXPR\s0)
Item "HANDLE->format_lines_left(EXPR)"
0
.ie n .IP "$FORMAT_LINES_LEFT" 8
.el .IP "\f(CW$FORMAT_LINES_LEFT" 8
Item "$FORMAT_LINES_LEFT"

- $-
Xref "$- $FORMAT_LINES_LEFT"
.PD
The number of lines left on the page of the currently selected output
channel.
.Sp
Mnemonic: lines_on_page - lines_printed.

- IO::Handle->format_line_break_characters \s-1EXPR\s0
Item "IO::Handle->format_line_break_characters EXPR"
0
.ie n .IP "$FORMAT_LINE_BREAK_CHARACTERS" 8
.el .IP "\f(CW$FORMAT_LINE_BREAK_CHARACTERS" 8
Item "$FORMAT_LINE_BREAK_CHARACTERS"
.ie n .IP "$:" 8
.el .IP "\f(CW$:" 8
Xref "$: FORMAT_LINE_BREAK_CHARACTERS"
Item "$:"
.PD
The current set of characters after which a string may be broken to
fill continuation fields (starting with \f(CW\*(C`^\*(C') in a format.  The default is
\*(L"\ \\n-\*(R", to break on a space, newline, or a hyphen.
.Sp
You cannot call \f(CW\*(C`format_line_break_characters()\*(C' on a handle, only as
a static method.  See IO::Handle.
.Sp
Mnemonic: a \*(L"colon\*(R" in poetry is a part of a line.

- \s-1HANDLE-\s0>format_lines_per_page(\s-1EXPR\s0)
Item "HANDLE->format_lines_per_page(EXPR)"
0
.ie n .IP "$FORMAT_LINES_PER_PAGE" 8
.el .IP "\f(CW$FORMAT_LINES_PER_PAGE" 8
Item "$FORMAT_LINES_PER_PAGE"

- $=
Xref "$= $FORMAT_LINES_PER_PAGE"
.PD
The current page length (printable lines) of the currently selected
output channel.  The default is 60.
.Sp
Mnemonic: = has horizontal lines.

- \s-1HANDLE-\s0>format_top_name(\s-1EXPR\s0)
Item "HANDLE->format_top_name(EXPR)"
0
.ie n .IP "$FORMAT_TOP_NAME" 8
.el .IP "\f(CW$FORMAT_TOP_NAME" 8
Item "$FORMAT_TOP_NAME"

- $^
Xref "$^ $FORMAT_TOP_NAME"
.PD
The name of the current top-of-page format for the currently selected
output channel.  The default is the name of the filehandle with \f(CW\*(C`_TOP\*(C'
appended.  For example, the default format top name for the \f(CW\*(C`STDOUT\*(C'
filehandle is \f(CW\*(C`STDOUT_TOP\*(C'.
.Sp
Mnemonic: points to top of page.

- \s-1HANDLE-\s0>format_name(\s-1EXPR\s0)
Item "HANDLE->format_name(EXPR)"
0
.ie n .IP "$FORMAT_NAME" 8
.el .IP "\f(CW$FORMAT_NAME" 8
Item "$FORMAT_NAME"

- $~
Xref "$~ $FORMAT_NAME"
.PD
The name of the current report format for the currently selected
output channel.  The default format name is the same as the filehandle
name.  For example, the default format name for the \f(CW\*(C`STDOUT\*(C'
filehandle is just \f(CW\*(C`STDOUT\*(C'.
.Sp
Mnemonic: brother to \f(CW$^.

### Error Variables

Xref "error exception"
Subsection "Error Variables"
The variables \f(CW$@, \f(CW$!, \f(CW$^E, and \f(CW$? contain information
about different types of error conditions that may appear during
execution of a Perl program.  The variables are shown ordered by
the \*(L"distance\*(R" between the subsystem which reported the error and
the Perl process.  They correspond to errors detected by the Perl
interpreter, C library, operating system, or an external program,
respectively.

To illustrate the differences between these variables, consider the
following Perl expression, which uses a single-quoted string.  After
execution of this statement, perl may have set all four special error
variables:

.Vb 5
    eval q\{
        open my $pipe, "/cdrom/install |" or die $!;
        my @res = <$pipe>;
        close $pipe or die "bad pipe: $?, $!";
    \};
.Ve

When perl executes the \f(CW\*(C`eval()\*(C' expression, it translates the
\f(CW\*(C`open()\*(C', \f(CW\*(C`<PIPE>\*(C', and \f(CW\*(C`close\*(C' calls in the C run-time library
and thence to the operating system kernel.  perl sets \f(CW$! to
the C library's \f(CW\*(C`errno\*(C' if one of these calls fails.

\f(CW$@ is set if the string to be \f(CW\*(C`eval\*(C'-ed did not compile (this may
happen if \f(CW\*(C`open\*(C' or \f(CW\*(C`close\*(C' were imported with bad prototypes), or
if Perl code executed during evaluation \f(CW\*(C`die()\*(C'd.  In these cases the
value of \f(CW$@ is the compile error, or the argument to \f(CW\*(C`die\*(C' (which
will interpolate \f(CW$! and \f(CW$?).  (See also Fatal, though.)

Under a few operating systems, \f(CW$^E may contain a more verbose error
indicator, such as in this case, \*(L"\s-1CDROM\s0 tray not closed.\*(R"  Systems that
do not support extended error messages leave \f(CW$^E the same as \f(CW$!.

Finally, \f(CW$? may be set to a non-0 value if the external program
*/cdrom/install* fails.  The upper eight bits reflect specific error
conditions encountered by the program (the program's \f(CW\*(C`exit()\*(C' value).
The lower eight bits reflect mode of failure, like signal death and
core dump information.  See **wait**\|(2) for details.  In contrast to
\f(CW$! and \f(CW$^E, which are set only if an error condition is detected,
the variable \f(CW$? is set on each \f(CW\*(C`wait\*(C' or pipe \f(CW\*(C`close\*(C',
overwriting the old value.  This is more like \f(CW$@, which on every
\f(CW\*(C`eval()\*(C' is always set on failure and cleared on success.

For more details, see the individual descriptions at \f(CW$@, \f(CW$!,
\f(CW$^E, and \f(CW$?.

- $\{^CHILD_ERROR_NATIVE\}
Xref "$^CHILD_ERROR_NATIVE"
Item "$\{^CHILD_ERROR_NATIVE\}"
The native status returned by the last pipe close, backtick (\f(CW\*(C`\`\`\*(C')
command, successful call to \f(CW\*(C`wait()\*(C' or \f(CW\*(C`waitpid()\*(C', or from the
\f(CW\*(C`system()\*(C' operator.  On POSIX-like systems this value can be decoded
with the \s-1WIFEXITED, WEXITSTATUS, WIFSIGNALED, WTERMSIG, WIFSTOPPED,\s0 and
\s-1WSTOPSIG\s0 functions provided by the \s-1POSIX\s0 module.
.Sp
Under \s-1VMS\s0 this reflects the actual \s-1VMS\s0 exit status; i.e. it is the
same as \f(CW$? when the pragma \f(CW\*(C`use vmsish \*(Aqstatus\*(Aq\*(C' is in effect.
.Sp
This variable was added in Perl v5.10.0.
.ie n .IP "$EXTENDED_OS_ERROR" 8
.el .IP "\f(CW$EXTENDED_OS_ERROR" 8
Item "$EXTENDED_OS_ERROR"
0

- $^E
Xref "$^E $EXTENDED_OS_ERROR"
Item "$^E"
.PD
Error information specific to the current operating system.  At the
moment, this differs from \f(CW"$!" under only \s-1VMS, OS/2,\s0 and Win32 (and
for MacPerl).  On all other platforms, \f(CW$^E is always just the same
as \f(CW$!.
.Sp
Under \s-1VMS,\s0 \f(CW$^E provides the \s-1VMS\s0 status value from the last system
error.  This is more specific information about the last system error
than that provided by \f(CW$!.  This is particularly important when \f(CW$!
is set to **\s-1EVMSERR\s0**.
.Sp
Under \s-1OS/2,\s0 \f(CW$^E is set to the error code of the last call to \s-1OS/2
API\s0 either via \s-1CRT,\s0 or directly from perl.
.Sp
Under Win32, \f(CW$^E always returns the last error information reported
by the Win32 call \f(CW\*(C`GetLastError()\*(C' which describes the last error
from within the Win32 \s-1API.\s0  Most Win32-specific code will report errors
via \f(CW$^E.  \s-1ANSI C\s0 and Unix-like calls set \f(CW\*(C`errno\*(C' and so most
portable Perl code will report errors via \f(CW$!.
.Sp
Caveats mentioned in the description of \f(CW"$!" generally apply to
\f(CW$^E, also.
.Sp
This variable was added in Perl 5.003.
.Sp
Mnemonic: Extra error explanation.
.ie n .IP "$EXCEPTIONS_BEING_CAUGHT" 8
.el .IP "\f(CW$EXCEPTIONS_BEING_CAUGHT" 8
Item "$EXCEPTIONS_BEING_CAUGHT"
0

- $^S
Xref "$^S $EXCEPTIONS_BEING_CAUGHT"
Item "$^S"
.PD
Current state of the interpreter.
.Sp
.Vb 5
        $^S         State
        ---------   -------------------------------------
        undef       Parsing module, eval, or main program
        true (1)    Executing an eval
        false (0)   Otherwise
.Ve
.Sp
The first state may happen in \f(CW$SIG\{_\|_DIE_\|_\} and \f(CW$SIG\{_\|_WARN_\|_\}
handlers.
.Sp
The English name \f(CW$EXCEPTIONS_BEING_CAUGHT is slightly misleading, because
the \f(CW\*(C`undef\*(C' value does not indicate whether exceptions are being caught,
since compilation of the main program does not catch exceptions.
.Sp
This variable was added in Perl 5.004.
.ie n .IP "$WARNING" 8
.el .IP "\f(CW$WARNING" 8
Item "$WARNING"
0

- $^W
Xref "$^W $WARNING"
Item "$^W"
.PD
The current value of the warning switch, initially true if **-w** was
used, false otherwise, but directly modifiable.
.Sp
See also warnings.
.Sp
Mnemonic: related to the **-w** switch.

- $\{^WARNING_BITS\}
Xref "$\{^WARNING_BITS\}"
Item "$\{^WARNING_BITS\}"
The current set of warning checks enabled by the \f(CW\*(C`use warnings\*(C' pragma.
It has the same scoping as the \f(CW$^H and \f(CW\*(C`%^H\*(C' variables.  The exact
values are considered internal to the warnings pragma and may change
between versions of Perl.
.Sp
This variable was added in Perl v5.6.0.
.ie n .IP "$OS_ERROR" 8
.el .IP "\f(CW$OS_ERROR" 8
Item "$OS_ERROR"
0
.ie n .IP "$ERRNO" 8
.el .IP "\f(CW$ERRNO" 8
Item "$ERRNO"

- $!
Xref "$! $ERRNO $OS_ERROR"
.PD
When referenced, \f(CW$! retrieves the current value
of the C \f(CW\*(C`errno\*(C' integer variable.
If \f(CW$! is assigned a numerical value, that value is stored in \f(CW\*(C`errno\*(C'.
When referenced as a string, \f(CW$! yields the system error string
corresponding to \f(CW\*(C`errno\*(C'.
.Sp
Many system or library calls set \f(CW\*(C`errno\*(C' if they fail,
to indicate the cause of failure.  They usually do **not**
set \f(CW\*(C`errno\*(C' to zero if they succeed and may set \f(CW\*(C`errno\*(C' to a
non-zero value on success.  This means \f(CW\*(C`errno\*(C', hence \f(CW$!, is
meaningful only *immediately* after a **failure**:
.Sp
.Vb 11
    if (open my $fh, "<", $filename) \{
                # Here $! is meaningless.
                ...
    \}
    else \{
                # ONLY here is $! meaningful.
                ...
                # Already here $! might be meaningless.
    \}
    # Since here we might have either success or failure,
    # $! is meaningless.
.Ve
.Sp
Here, *meaningless* means that \f(CW$! may be unrelated to the outcome
of the \f(CW\*(C`open()\*(C' operator.  Assignment to \f(CW$! is similarly ephemeral.
It can be used immediately before invoking the \f(CW\*(C`die()\*(C' operator,
to set the exit value, or to inspect the system error string
corresponding to error *n*, or to restore \f(CW$! to a meaningful state.
.Sp
Perl itself may set \f(CW\*(C`errno\*(C' to a non-zero on failure even if no
system call is performed.
.Sp
Mnemonic: What just went bang?
.ie n .IP "%OS_ERROR" 8
.el .IP "\f(CW%OS_ERROR" 8
Item "%OS_ERROR"
0
.ie n .IP "%ERRNO" 8
.el .IP "\f(CW%ERRNO" 8
Item "%ERRNO"

- %!
Xref "%! %OS_ERROR %ERRNO"
.PD
Each element of \f(CW\*(C`%!\*(C' has a true value only if \f(CW$! is set to that
value.  For example, \f(CW$!\{ENOENT\} is true if and only if the current
value of \f(CW$! is \f(CW\*(C`ENOENT\*(C'; that is, if the most recent error was \*(L"No
such file or directory\*(R" (or its moral equivalent: not all operating
systems give that exact error, and certainly not all languages).  The
specific true value is not guaranteed, but in the past has generally
been the numeric value of \f(CW$!.  To check if a particular key is
meaningful on your system, use \f(CW\*(C`exists $!\{the_key\}\*(C'; for a list of legal
keys, use \f(CW\*(C`keys %!\*(C'.  See Errno for more information, and also see
\*(L"$!\*(R".
.Sp
This variable was added in Perl 5.005.
.ie n .IP "$CHILD_ERROR" 8
.el .IP "\f(CW$CHILD_ERROR" 8
Item "$CHILD_ERROR"
0

- $?
Xref "$? $CHILD_ERROR"
.PD
The status returned by the last pipe close, backtick (\f(CW\*(C`\`\`\*(C') command,
successful call to \f(CW\*(C`wait()\*(C' or \f(CW\*(C`waitpid()\*(C', or from the \f(CW\*(C`system()\*(C'
operator.  This is just the 16-bit status word returned by the
traditional Unix \f(CW\*(C`wait()\*(C' system call (or else is made up to look
like it).  Thus, the exit value of the subprocess is really (\f(CW\*(C`$? >>
8\*(C'), and \f(CW\*(C`$? & 127\*(C' gives which signal, if any, the process died
from, and \f(CW\*(C`$? & 128\*(C' reports whether there was a core dump.
.Sp
Additionally, if the \f(CW\*(C`h_errno\*(C' variable is supported in C, its value
is returned via \f(CW$? if any \f(CW\*(C`gethost*()\*(C' function fails.
.Sp
If you have installed a signal handler for \f(CW\*(C`SIGCHLD\*(C', the
value of \f(CW$? will usually be wrong outside that handler.
.Sp
Inside an \f(CW\*(C`END\*(C' subroutine \f(CW$? contains the value that is going to be
given to \f(CW\*(C`exit()\*(C'.  You can modify \f(CW$? in an \f(CW\*(C`END\*(C' subroutine to
change the exit status of your program.  For example:
.Sp
.Vb 3
    END \{
        $? = 1 if $? == 255;  # die would make it 255
    \}
.Ve
.Sp
Under \s-1VMS,\s0 the pragma \f(CW\*(C`use vmsish \*(Aqstatus\*(Aq\*(C' makes \f(CW$? reflect the
actual \s-1VMS\s0 exit status, instead of the default emulation of \s-1POSIX\s0
status; see \*(L"$?\*(R" in perlvms for details.
.Sp
Mnemonic: similar to **sh** and **ksh**.
.ie n .IP "$EVAL_ERROR" 8
.el .IP "\f(CW$EVAL_ERROR" 8
Item "$EVAL_ERROR"
0

- $@
Xref "$@ $EVAL_ERROR"
.PD
The Perl error from the last \f(CW\*(C`eval\*(C' operator, i.e. the last exception that
was caught.  For \f(CW\*(C`eval BLOCK\*(C', this is either a runtime error message or the
string or reference \f(CW\*(C`die\*(C' was called with.  The \f(CW\*(C`eval STRING\*(C' form also
catches syntax errors and other compile time exceptions.
.Sp
If no error occurs, \f(CW\*(C`eval\*(C' sets \f(CW$@ to the empty string.
.Sp
Warning messages are not collected in this variable.  You can, however,
set up a routine to process warnings by setting \f(CW$SIG\{_\|_WARN_\|_\} as
described in \*(L"%SIG\*(R".
.Sp
Mnemonic: Where was the error \*(L"at\*(R"?

### Variables related to the interpreter state

Subsection "Variables related to the interpreter state"
These variables provide information about the current interpreter state.
.ie n .IP "$COMPILING" 8
.el .IP "\f(CW$COMPILING" 8
Item "$COMPILING"
0

- $^C
Xref "$^C $COMPILING"
Item "$^C"
.PD
The current value of the flag associated with the **-c** switch.
Mainly of use with **-MO=...** to allow code to alter its behavior
when being compiled, such as for example to \f(CW\*(C`AUTOLOAD\*(C' at compile
time rather than normal, deferred loading.  Setting
\f(CW\*(C`$^C = 1\*(C' is similar to calling \f(CW\*(C`B::minus_c\*(C'.
.Sp
This variable was added in Perl v5.6.0.
.ie n .IP "$DEBUGGING" 8
.el .IP "\f(CW$DEBUGGING" 8
Item "$DEBUGGING"
0

- $^D
Xref "$^D $DEBUGGING"
Item "$^D"
.PD
The current value of the debugging flags.  May be read or set.  Like its
command-line equivalent, you can use numeric
or symbolic values, e.g. \f(CW\*(C`$^D = 10\*(C' or \f(CW\*(C`$^D = "st"\*(C'.  See
"**-D***number*" in perlrun.  The contents of this variable also affects the
debugger operation.  See \*(L"Debugger Internals\*(R" in perldebguts.
.Sp
Mnemonic: value of **-D** switch.

- $\{^ENCODING\}
Xref "$\{^ENCODING\}"
Item "$\{^ENCODING\}"
This variable is no longer supported.
.Sp
It used to hold the *object reference* to the \f(CW\*(C`Encode\*(C' object that was
used to convert the source code to Unicode.
.Sp
Its purpose was to allow your non-ASCII Perl
scripts not to have to be written in \s-1UTF-8\s0; this was
useful before editors that worked on \s-1UTF-8\s0 encoded text were common, but
that was long ago.  It caused problems, such as affecting the operation
of other modules that weren't expecting it, causing general mayhem.
.Sp
If you need something like this functionality, it is recommended that use
you a simple source filter, such as Filter::Encoding.
.Sp
If you are coming here because code of yours is being adversely affected
by someone's use of this variable, you can usually work around it by
doing this:
.Sp
.Vb 1
 local $\{^ENCODING\};
.Ve
.Sp
near the beginning of the functions that are getting broken.  This
undefines the variable during the scope of execution of the including
function.
.Sp
This variable was added in Perl 5.8.2 and removed in 5.26.0.
Setting it to anything other than \f(CW\*(C`undef\*(C' was made fatal in Perl 5.28.0.

- $\{^GLOBAL_PHASE\}
Xref "$\{^GLOBAL_PHASE\}"
Item "$\{^GLOBAL_PHASE\}"
The current phase of the perl interpreter.
.Sp
Possible values are:

> 
- \s-1CONSTRUCT\s0
Item "CONSTRUCT"
The \f(CW\*(C`PerlInterpreter*\*(C' is being constructed via \f(CW\*(C`perl_construct\*(C'.  This
value is mostly there for completeness and for use via the
underlying C variable \f(CW\*(C`PL_phase\*(C'.  It's not really possible for Perl
code to be executed unless construction of the interpreter is
finished.

- \s-1START\s0
Item "START"
This is the global compile-time.  That includes, basically, every
\f(CW\*(C`BEGIN\*(C' block executed directly or indirectly from during the
compile-time of the top-level program.
.Sp
This phase is not called \*(L"\s-1BEGIN\*(R"\s0 to avoid confusion with
\f(CW\*(C`BEGIN\*(C'-blocks, as those are executed during compile-time of any
compilation unit, not just the top-level program.  A new, localised
compile-time entered at run-time, for example by constructs as
\f(CW\*(C`eval "use SomeModule"\*(C' are not global interpreter phases, and
therefore aren't reflected by \f(CW\*(C`$\{^GLOBAL_PHASE\}\*(C'.

- \s-1CHECK\s0
Item "CHECK"
Execution of any \f(CW\*(C`CHECK\*(C' blocks.

- \s-1INIT\s0
Item "INIT"
Similar to \*(L"\s-1CHECK\*(R",\s0 but for \f(CW\*(C`INIT\*(C'-blocks, not \f(CW\*(C`CHECK\*(C' blocks.

- \s-1RUN\s0
Item "RUN"
The main run-time, i.e. the execution of \f(CW\*(C`PL_main_root\*(C'.

- \s-1END\s0
Item "END"
Execution of any \f(CW\*(C`END\*(C' blocks.

- \s-1DESTRUCT\s0
Item "DESTRUCT"
Global destruction.



> .Sp
Also note that there's no value for UNITCHECK-blocks.  That's because
those are run for each compilation unit individually, and therefore is
not a global interpreter phase.
.Sp
Not every program has to go through each of the possible phases, but
transition from one phase to another can only happen in the order
described in the above list.
.Sp
An example of all of the phases Perl code can see:
.Sp
.Vb 1
    BEGIN \{ print "compile-time: $\{^GLOBAL_PHASE\}\\n" \}

    INIT  \{ print "init-time: $\{^GLOBAL_PHASE\}\\n" \}

    CHECK \{ print "check-time: $\{^GLOBAL_PHASE\}\\n" \}

    \{
        package Print::Phase;

        sub new \{
            my ($class, $time) = @_;
            return bless \\$time, $class;
        \}

        sub DESTROY \{
            my $self = shift;
            print "$$self: $\{^GLOBAL_PHASE\}\\n";
        \}
    \}

    print "run-time: $\{^GLOBAL_PHASE\}\\n";

    my $runtime = Print::Phase->new(
        "lexical variables are garbage collected before END"
    );

    END   \{ print "end-time: $\{^GLOBAL_PHASE\}\\n" \}

    our $destruct = Print::Phase->new(
        "package variables are garbage collected after END"
    );
.Ve
.Sp
This will print out
.Sp
.Vb 7
    compile-time: START
    check-time: CHECK
    init-time: INIT
    run-time: RUN
    lexical variables are garbage collected before END: RUN
    end-time: END
    package variables are garbage collected after END: DESTRUCT
.Ve
.Sp
This variable was added in Perl 5.14.0.



- $^H
Xref "$^H"
Item "$^H"
\s-1WARNING:\s0 This variable is strictly for
internal use only.  Its availability,
behavior, and contents are subject to change without notice.
.Sp
This variable contains compile-time hints for the Perl interpreter.  At the
end of compilation of a \s-1BLOCK\s0 the value of this variable is restored to the
value when the interpreter started to compile the \s-1BLOCK.\s0
.Sp
When perl begins to parse any block construct that provides a lexical scope
(e.g., eval body, required file, subroutine body, loop body, or conditional
block), the existing value of \f(CW$^H is saved, but its value is left unchanged.
When the compilation of the block is completed, it regains the saved value.
Between the points where its value is saved and restored, code that
executes within \s-1BEGIN\s0 blocks is free to change the value of \f(CW$^H.
.Sp
This behavior provides the semantic of lexical scoping, and is used in,
for instance, the \f(CW\*(C`use strict\*(C' pragma.
.Sp
The contents should be an integer; different bits of it are used for
different pragmatic flags.  Here's an example:
.Sp
.Vb 1
    sub add_100 \{ $^H |= 0x100 \}

    sub foo \{
        BEGIN \{ add_100() \}
        bar->baz($boon);
    \}
.Ve
.Sp
Consider what happens during execution of the \s-1BEGIN\s0 block.  At this point
the \s-1BEGIN\s0 block has already been compiled, but the body of \f(CW\*(C`foo()\*(C' is still
being compiled.  The new value of \f(CW$^H
will therefore be visible only while
the body of \f(CW\*(C`foo()\*(C' is being compiled.
.Sp
Substitution of \f(CW\*(C`BEGIN \{ add_100() \}\*(C' block with:
.Sp
.Vb 1
    BEGIN \{ require strict; strict->import(\*(Aqvars\*(Aq) \}
.Ve
.Sp
demonstrates how \f(CW\*(C`use strict \*(Aqvars\*(Aq\*(C' is implemented.  Here's a conditional
version of the same lexical pragma:
.Sp
.Vb 3
    BEGIN \{
        require strict; strict->import(\*(Aqvars\*(Aq) if $condition
    \}
.Ve
.Sp
This variable was added in Perl 5.003.

- %^H
Xref "%^H"
Item "%^H"
The \f(CW\*(C`%^H\*(C' hash provides the same scoping semantic as \f(CW$^H.  This makes
it useful for implementation of lexically scoped pragmas.  See
perlpragma.   All the entries are stringified when accessed at
runtime, so only simple values can be accommodated.  This means no
pointers to objects, for example.
.Sp
When putting items into \f(CW\*(C`%^H\*(C', in order to avoid conflicting with other
users of the hash there is a convention regarding which keys to use.
A module should use only keys that begin with the module's name (the
name of its main package) and a \*(L"/\*(R" character.  For example, a module
\f(CW\*(C`Foo::Bar\*(C' should use keys such as \f(CW\*(C`Foo::Bar/baz\*(C'.
.Sp
This variable was added in Perl v5.6.0.

- $\{^OPEN\}
Xref "$\{^OPEN\}"
Item "$\{^OPEN\}"
An internal variable used by PerlIO.  A string in two parts, separated
by a \f(CW\*(C`\\0\*(C' byte, the first part describes the input layers, the second
part describes the output layers.
.Sp
This is the mechanism that applies the lexical effects of the open
pragma, and the main program scope effects of the \f(CW\*(C`io\*(C' or \f(CW\*(C`D\*(C' options
for the -C command-line switch and
\s-1PERL_UNICODE\s0 environment variable.
.Sp
The functions \f(CW\*(C`accept()\*(C', \f(CW\*(C`open()\*(C', \f(CW\*(C`pipe()\*(C', \f(CW\*(C`readpipe()\*(C' (as well
as the related \f(CW\*(C`qx\*(C' and \f(CW\*(C`\`STRING\`\*(C' operators), \f(CW\*(C`socket()\*(C',
\f(CW\*(C`socketpair()\*(C', and \f(CW\*(C`sysopen()\*(C' are affected by the lexical value of
this variable.  The implicit \*(L"\s-1ARGV\*(R"\s0 handle opened by \f(CW\*(C`readline()\*(C' (or
the related \f(CW\*(C`<>\*(C' and \f(CW\*(C`<<>>\*(C' operators) on passed filenames is
also affected (but not if it opens \f(CW\*(C`STDIN\*(C').  If this variable is not
set, these functions will set the default layers as described in
\*(L"Defaults and how to override them\*(R" in PerlIO.
.Sp
\f(CW\*(C`open()\*(C' ignores this variable (and the default layers) when called with
3 arguments and explicit layers are specified.  Indirect calls to these
functions via modules like IO::Handle are not affected as they occur
in a different lexical scope.  Directory handles such as opened by
\f(CW\*(C`opendir()\*(C' are not currently affected.
.Sp
This variable was added in Perl v5.8.0.
.ie n .IP "$PERLDB" 8
.el .IP "\f(CW$PERLDB" 8
Item "$PERLDB"
0

- $^P
Xref "$^P $PERLDB"
Item "$^P"
.PD
The internal variable for debugging support.  The meanings of the
various bits are subject to change, but currently indicate:

> 
- 0x01
Item "0x01"
Debug subroutine enter/exit.

- 0x02
Item "0x02"
Line-by-line debugging.  Causes \f(CW\*(C`DB::DB()\*(C' subroutine to be called for
each statement executed.  Also causes saving source code lines (like
0x400).

- 0x04
Item "0x04"
Switch off optimizations.

- 0x08
Item "0x08"
Preserve more data for future interactive inspections.

- 0x10
Item "0x10"
Keep info about source lines on which a subroutine is defined.

- 0x20
Item "0x20"
Start with single-step on.

- 0x40
Item "0x40"
Use subroutine address instead of name when reporting.

- 0x80
Item "0x80"
Report \f(CW\*(C`goto &subroutine\*(C' as well.

- 0x100
Item "0x100"
Provide informative \*(L"file\*(R" names for evals based on the place they were compiled.

- 0x200
Item "0x200"
Provide informative names to anonymous subroutines based on the place they
were compiled.

- 0x400
Item "0x400"
Save source code lines into \f(CW\*(C`@\{"_<$filename"\}\*(C'.

- 0x800
Item "0x800"
When saving source, include evals that generate no subroutines.

- 0x1000
Item "0x1000"
When saving source, include source that did not compile.



> .Sp
Some bits may be relevant at compile-time only, some at
run-time only.  This is a new mechanism and the details may change.
See also perldebguts.



- $\{^TAINT\}
Xref "$\{^TAINT\}"
Item "$\{^TAINT\}"
Reflects if taint mode is on or off.  1 for on (the program was run with
**-T**), 0 for off, -1 when only taint warnings are enabled (i.e. with
**-t** or **-TU**).
.Sp
This variable is read-only.
.Sp
This variable was added in Perl v5.8.0.

- $\{^SAFE_LOCALES\}
Xref "$\{^SAFE_LOCALES\}"
Item "$\{^SAFE_LOCALES\}"
Reflects if safe locale operations are available to this perl (when the
value is 1) or not (the value is 0).  This variable is always 1 if the
perl has been compiled without threads.  It is also 1 if this perl is
using thread-safe locale operations.  Note that an individual thread may
choose to use the global locale (generally unsafe) by calling
\*(L"switch_to_global_locale\*(R" in perlapi.  This variable currently is still
set to 1 in such threads.
.Sp
This variable is read-only.
.Sp
This variable was added in Perl v5.28.0.

- $\{^UNICODE\}
Xref "$\{^UNICODE\}"
Item "$\{^UNICODE\}"
Reflects certain Unicode settings of Perl.  See
perlrun documentation for the \f(CW\*(C`-C\*(C'
switch for more information about the possible values.
.Sp
This variable is set during Perl startup and is thereafter read-only.
.Sp
This variable was added in Perl v5.8.2.

- $\{^UTF8CACHE\}
Xref "$\{^UTF8CACHE\}"
Item "$\{^UTF8CACHE\}"
This variable controls the state of the internal \s-1UTF-8\s0 offset caching code.
1 for on (the default), 0 for off, -1 to debug the caching code by checking
all its results against linear scans, and panicking on any discrepancy.
.Sp
This variable was added in Perl v5.8.9.  It is subject to change or
removal without notice, but is currently used to avoid recalculating the
boundaries of multi-byte UTF-8-encoded characters.

- $\{^UTF8LOCALE\}
Xref "$\{^UTF8LOCALE\}"
Item "$\{^UTF8LOCALE\}"
This variable indicates whether a \s-1UTF-8\s0 locale was detected by perl at
startup.  This information is used by perl when it's in
adjust-utf8ness-to-locale mode (as when run with the \f(CW\*(C`-CL\*(C' command-line
switch); see perlrun for more info on
this.
.Sp
This variable was added in Perl v5.8.8.

### Deprecated and removed variables

Subsection "Deprecated and removed variables"
Deprecating a variable announces the intent of the perl maintainers to
eventually remove the variable from the language.  It may still be
available despite its status.  Using a deprecated variable triggers
a warning.

Once a variable is removed, its use triggers an error telling you
the variable is unsupported.

See perldiag for details about error messages.

- $#
Xref "$#"
\f(CW$# was a variable that could be used to format printed numbers.
After a deprecation cycle, its magic was removed in Perl v5.10.0 and
using it now triggers a warning: \f(CW\*(C`$# is no longer supported\*(C'.
.Sp
This is not the sigil you use in front of an array name to get the
last index, like \f(CW$#array.  That's still how you get the last index
of an array in Perl.  The two have nothing to do with each other.
.Sp
Deprecated in Perl 5.
.Sp
Removed in Perl v5.10.0.

- $*
Xref "$*"
\f(CW$* was a variable that you could use to enable multiline matching.
After a deprecation cycle, its magic was removed in Perl v5.10.0.
Using it now triggers a warning: \f(CW\*(C`$* is no longer supported\*(C'.
You should use the \f(CW\*(C`/s\*(C' and \f(CW\*(C`/m\*(C' regexp modifiers instead.
.Sp
Deprecated in Perl 5.
.Sp
Removed in Perl v5.10.0.

- $[
Xref "$["
This variable stores the index of the first element in an array, and
of the first character in a substring.  The default is 0, but you could
theoretically set it to 1 to make Perl behave more like **awk** (or Fortran)
when subscripting and when evaluating the **index()** and **substr()** functions.
.Sp
As of release 5 of Perl, assignment to \f(CW$[ is treated as a compiler
directive, and cannot influence the behavior of any other file.
(That's why you can only assign compile-time constants to it.)
Its use is highly discouraged.
.Sp
Prior to Perl v5.10.0, assignment to \f(CW$[ could be seen from outer lexical
scopes in the same file, unlike other compile-time directives (such as
strict).  Using **local()** on it would bind its value strictly to a lexical
block.  Now it is always lexically scoped.
.Sp
As of Perl v5.16.0, it is implemented by the arybase module.
.Sp
As of Perl v5.30.0, or under \f(CW\*(C`use v5.16\*(C', or \f(CW\*(C`no feature "array_base"\*(C',
\f(CW$[ no longer has any effect, and always contains 0.
Assigning 0 to it is permitted, but any other value will produce an error.
.Sp
Mnemonic: [ begins subscripts.
.Sp
Deprecated in Perl v5.12.0.
