+++
description = "spfquery checks if a given set of e-mail parameters (e.g., the s-1SMTPs0 senders s-1IPs0 address) matches the responsible domains Sender Policy Framework (s-1SPFs0) policy.  For more information on s-1SPFs0 see <http://www.openspf.org>. The follo..."
manpage_name = "spfquery"
manpage_format = "troff"
operating_system = "macos"
author = "None Specified"
detected_package_version = "5.34.0"
keywords = ["header", "see", "also", "mail", "spf", "fbspfd", "8", "http", "tools", "ietf", "org", "html", "rfc4408", "authors", "this", "version", "of", "fbspfquery", "is", "a", "complete", "rewrite", "by", "julian", "mehnle", "net", "based", "on", "an", "earlier", "written", "meng", "weng", "wong", "mengwong", "pobox", "com", "and", "wayne", "schlitt"]
manpage_section = "1"
date = "2024-12-14"
title = "spfquery(1)"
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "SPFQUERY 1"
SPFQUERY 1 "2024-12-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

spfquery - (Mail::SPF) - Checks if a given set of e-mail parameters matches a
domain's SPF policy

## VERSION

Header "VERSION"
2.501

## SYNOPSIS

Header "SYNOPSIS"

- \fBPreferred usage:
Item "Preferred usage:"
**spfquery** [**--versions**|**-v** **1**|**2**|**1,2**] [**--scope**|**-s** **helo**|**mfrom**|**pra**]
**--identity**|**--id** *identity* **--ip-address**|**--ip** *ip-address*
[**--helo-identity**|**--helo-id** *helo-identity*] [*\s-1OPTIONS\s0*]
.Sp
**spfquery** [**--versions**|**-v** **1**|**2**|**1,2**] [**--scope**|**-s** **helo**|**mfrom**|**pra**]
**--file**|**-f** *filename*|**-** [*\s-1OPTIONS\s0*]

- \fBLegacy usage:
Item "Legacy usage:"
**spfquery** **--helo** *helo-identity* **--ip-address**|**--ip** *ip-address* [*\s-1OPTIONS\s0*]
.Sp
**spfquery** **--mfrom** *mfrom-identity* **--ip-address**|**--ip** *ip-address*
[**--helo** *helo-identity*] [*\s-1OPTIONS\s0*]
.Sp
**spfquery** **--pra** *pra-identity* **--ip-address**|**--ip** *ip-address* [*\s-1OPTIONS\s0*]

- \fBOther usage:
Item "Other usage:"
**spfquery** **--version**|**-V**
.Sp
**spfquery** **--help**

## DESCRIPTION

Header "DESCRIPTION"
**spfquery** checks if a given set of e-mail parameters (e.g., the \s-1SMTP\s0 sender's
\s-1IP\s0 address) matches the responsible domain's Sender Policy Framework (\s-1SPF\s0)
policy.  For more information on \s-1SPF\s0 see <http://www.openspf.org>.

### Preferred Usage

Subsection "Preferred Usage"
The following usage forms are preferred over the legacy forms
used by older **spfquery** versions:

The **--identity** form checks if the given *ip-address* is an authorized \s-1SMTP\s0
sender for the given \f(CW\*(C`helo\*(C' hostname, \f(CW\*(C`mfrom\*(C' envelope sender e-mail address,
or \f(CW\*(C`pra\*(C' (so-called purported resonsible address) e-mail address, depending
on the value of the **--scope** option (which defaults to **mfrom** if omitted).

The **--file** form reads "*ip-address* *identity* [*helo-identity*]" tuples
from the file with the specified *filename*, or from standard input if
*filename* is **-**, and checks them against the specified scope (**mfrom** by
default).

Both forms support an optional **--versions** option, which specifies a
comma-separated list of the \s-1SPF\s0 version numbers of \s-1SPF\s0 records that may be
used.  **1** means that \f(CW\*(C`v=spf1\*(C' records should be used.  **2** means that
\f(CW\*(C`spf2.0\*(C' records should be used.  Defaults to **1,2**, i.e., uses any \s-1SPF\s0
records that are available.  Records of a higher version are preferred.

### Legacy Usage

Subsection "Legacy Usage"
**spfquery** versions before 2.500 featured the following usage forms, which are
discouraged but still supported for backwards compatibility:

The **--helo** form checks if the given *ip-address* is an authorized \s-1SMTP\s0
sender for the \f(CW\*(C`HELO\*(C' hostname given as the *identity* (so-called \f(CW\*(C`HELO\*(C'
check).

The **--mfrom** form checks if the given *ip-address* is an authorized \s-1SMTP\s0
sender for the envelope sender email-address (or domain) given as the
*identity* (so-called \f(CW\*(C`MAIL FROM\*(C' check).  If a domain is given instead of an
e-mail address, \f(CW\*(C`postmaster\*(C' will be substituted for the localpart.

The **--pra** form checks if the given *ip-address* is an authorized \s-1SMTP\s0
sender for the \s-1PRA\s0 (Purported Responsible Address) e-mail address given as the
identity.

### Other Usage

Subsection "Other Usage"
The **--version** form prints version information of spfquery.  The **--help**
form prints usage information for spfquery.

## OPTIONS

Header "OPTIONS"

### Standard Options

Subsection "Standard Options"
The preferred and legacy forms optionally take any of the following
*\s-1OPTIONS\s0*:

- \fB--default-explanation \fIstring
Item "--default-explanation string"
0

- \fB--def-exp \fIstring
Item "--def-exp string"
.PD
Use the specified *string* as the default explanation if the authority domain
does not specify an explanation string of its own.

- \fB--hostname \fIhostname
Item "--hostname hostname"
Use *hostname* as the host name of the local system instead of auto-detecting
it.

- \fB--keep-comments
Item "--keep-comments"
0

- \fB--no-keep-comments
Item "--no-keep-comments"
.PD
Do (not) print any comments found when reading from a file or from standard
input.

- \fB--sanitize (currently ignored)
Item "--sanitize (currently ignored)"
0

- \fB--no-sanitize (currently ignored)
Item "--no-sanitize (currently ignored)"
.PD
Do (not) sanitize the output by condensing consecutive white-space into a
single space and replacing non-printable characters with question marks.
Enabled by default.

- \fB--debug (currently ignored)
Item "--debug (currently ignored)"
Print out debug information.

### Black Magic Options

Subsection "Black Magic Options"
Several options that were supported by earlier versions of **spfquery** are
considered black magic (i.e. potentially dangerous for the innocent user) and
are thus disabled by default.  If the **Mail::SPF::BlackMagic** Perl module
is installed, they may be enabled by specifying **--enable-black-magic**.

- \fB--max-dns-interactive-terms \fIn
Item "--max-dns-interactive-terms n"
Evaluate a maximum of *n* DNS-interactive mechanisms and modifiers per \s-1SPF\s0
check.  Defaults to **10**.  Do *not* override the default unless you know what
you are doing!

- \fB--max-name-lookups-per-term \fIn
Item "--max-name-lookups-per-term n"
Perform a maximum of *n* \s-1DNS\s0 name look-ups per mechanism or modifier.
Defaults to **10**.  Do *not* override the default unless you know what you are
doing!

- \fB--authorize-mxes-for \fIemail-address|\fIdomain\fB,...
Item "--authorize-mxes-for email-address|domain,..."
Consider all the MXes of the comma-separated list of *email-address*es and
*domain*s as inherently authorized.

- \fB--tfwl
Item "--tfwl"
Perform \f(CW\*(C`trusted-forwarder.org\*(C' accreditation checking.

- \fB--guess \fIspf-terms
Item "--guess spf-terms"
Use *spf-terms* as a default record if no \s-1SPF\s0 record is found.

- \fB--local \fIspf-terms
Item "--local spf-terms"
Process *spf-terms* as local policy before resorting to a default result
(the implicit or explicit \f(CW\*(C`all\*(C' mechanism at the end of the domain's \s-1SPF\s0
record).  For example, this could be used for white-listing one's secondary
MXes: \f(CW\*(C`mx:mydomain.example.org\*(C'.

- \fB--override \fIdomain\fB=\fIspf-record
Item "--override domain=spf-record"
0

- \fB--fallback \fIdomain\fB=\fIspf-record
Item "--fallback domain=spf-record"
.PD
Set overrides and fallbacks.  Each option can be specified multiple times.  For
example:
.Sp
.Vb 3
    --override example.org=\*(Aqv=spf1 -all\*(Aq
    --override \*(Aq*.example.net\*(Aq=\*(Aqv=spf1 a mx -all\*(Aq
    --fallback example.com=\*(Aqv=spf1 -all\*(Aq
.Ve

## RESULT CODES

Header "RESULT CODES"

- \fBpass
Item "pass"
The specified \s-1IP\s0 address is an authorized \s-1SMTP\s0 sender for the identity.

- \fBfail
Item "fail"
The specified \s-1IP\s0 address is not an authorized \s-1SMTP\s0 sender for the identity.

- \fBsoftfail
Item "softfail"
The specified \s-1IP\s0 address is not an authorized \s-1SMTP\s0 sender for the identity,
however the authority domain is still testing out its \s-1SPF\s0 policy.

- \fBneutral
Item "neutral"
The identity's authority domain makes no assertion about the status of the \s-1IP\s0
address.

- \fBpermerror
Item "permerror"
A permanent error occurred while evaluating the authority domain's policy
(e.g., a syntax error in the \s-1SPF\s0 record).  Manual intervention is required
from the authority domain.

- \fBtemperror
Item "temperror"
A temporary error occurred while evaluating the authority domain's policy
(e.g., a \s-1DNS\s0 error).  Try again later.

- \fBnone
Item "none"
There is no applicable \s-1SPF\s0 policy for the identity domain.

## EXIT CODES

Header "EXIT CODES"
.Vb 9
  Result    | Exit code
 -----------+-----------
  pass      |     0
  fail      |     1
  softfail  |     2
  neutral   |     3
  permerror |     4
  temperror |     5
  none      |     6
.Ve

## EXAMPLES

Header "EXAMPLES"
.Vb 3
    spfquery --scope mfrom --id user@example.com --ip 1.2.3.4
    spfquery --file test_data
    echo "127.0.0.1 user@example.com helohost.example.com" | spfquery -f -
.Ve

## COMPATIBILITY

Header "COMPATIBILITY"
**spfquery** has undergone the following interface changes compared to earlier
versions:

- \fB2.500
Item "2.500"

> 0

- \(bu
.PD
A new preferred usage style for performing individual \s-1SPF\s0 checks has been
introduced.  The new style accepts a unified **--identity** option and an
optional **--scope** option that specifies the type (scope) of the identity.  In
contrast, the legacy usage style requires a separate usage form for every
supported scope.  See \*(L"Preferred usage\*(R" and \*(L"Legacy usage\*(R" for details.

- \(bu
The former \f(CW\*(C`unknown\*(C' and \f(CW\*(C`error\*(C' result codes have been renamed to \f(CW\*(C`permerror\*(C'
and \f(CW\*(C`temperror\*(C', respectively, in order to comply with \s-1RFC 4408\s0 terminology.

- \(bu
\s-1SPF\s0 checks with an empty identity are no longer supported.  In the case of an
empty \f(CW\*(C`MAIL FROM\*(C' \s-1SMTP\s0 transaction parameter, perform a check with the \f(CW\*(C`helo\*(C'
scope directly.

- \(bu
The **--debug** and **--(no-)sanitize** options are currently ignored by this
version of **spfquery**.  They will again be supported in the future.

- \(bu
Several features that were supported by earlier versions of **spfquery** are
considered black magic and thus are now disabled by default.  See \*(L"Black
Magic Options\*(R".

- \(bu
Several option names have been deprecated.  This is a list of them and their
preferred synonyms:
.Sp
.Vb 9
  Deprecated options  | Preferred options
 ---------------------+-----------------------------
  --sender, -s        | --mfrom
  --ipv4, -i          | --ip-address, --ip
  --name              | --hostname
  --max-lookup-count, | --max-dns-interactive-terms
    --max-lookup      |
  --rcpt-to, -r       | --authorize-mxes-for
  --trusted           | --tfwl
.Ve



> 


## SEE ALSO

Header "SEE ALSO"
Mail::SPF, **spfd**\|(8)

<http://tools.ietf.org/html/rfc4408>

## AUTHORS

Header "AUTHORS"
This version of **spfquery** is a complete rewrite by Julian Mehnle
<julian@mehnle.net>, based on an earlier version written by Meng Weng Wong
<mengwong+spf@pobox.com> and Wayne Schlitt <wayne@schlitt.net>.
