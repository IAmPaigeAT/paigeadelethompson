+++
manpage_name = "tidy_changelog"
manpage_section = "1"
detected_package_version = "5.34.0"
author = "None Specified"
date = "2014-10-10"
manpage_format = "troff"
operating_system = "macos"
operating_system_version = "15.3"
description = "Takes a changelog file, parse it using CPAN::Changes and prints out the resulting output.  If a file is not given, the program will see if there is one file in the current directory beginning by change (case-insensitive) and, if so, assume it to ..."
title = "tidy_changelog(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "TIDY_CHANGELOG 1"
TIDY_CHANGELOG 1 "2014-10-10" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

tidy_changelog - command-line tool for CPAN::Changes

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
    $ tidy_changelog Changelog
.Ve

## DESCRIPTION

Header "DESCRIPTION"
Takes a changelog file, parse it using CPAN::Changes and prints out
the resulting output.  If a file is not given, the program will see if
there is one file in the current directory beginning by 'change'
(case-insensitive) and, if so, assume it to be the changelog.

## ARGUMENTS

Header "ARGUMENTS"

### --next

Subsection "--next"
If provided, assumes that there is a placeholder
header for an upcoming next release. The placeholder token
is given via *--token*.

### --token

Subsection "--token"
Regular expression to use to detect the token for an upcoming
release if *--next* is used. If not explicitly given, defaults
to \f(CW\*(C`\\\{\\\{\\$NEXT\\\}\\\}\*(C'.

### --headers

Subsection "--headers"
If given, only print out the release header lines, without any of the
changes.

### --reverse

Subsection "--reverse"
Prints the releases in reverse order (from the oldest to latest).

### --check

Subsection "--check"
Only check if the changelog is formatted properly using the changes_file_ok
function of Test::CPAN::Changes.

### --help

Subsection "--help"
This help
