+++
author = "None Specified"
manpage_format = "troff"
keywords = ["header", "see", "also", "file", "find", "rule"]
manpage_name = "findrule"
manpage_section = "1"
operating_system_version = "15.3"
title = "findrule(1)"
date = "2015-12-03"
detected_package_version = "5.34.0"
operating_system = "macos"
description = "f(CW*(C`findrule*(C mostly borrows the interface from s-1GNUs0 find|(1) to provide a command-line interface onto the File::Find::Rule heirarchy of modules. The syntax for expressions is the rule name, preceded by a dash, followed by an optional ar..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "FINDRULE 1"
FINDRULE 1 "2015-12-03" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

findrule - command line wrapper to File::Find::Rule

## USAGE

Header "USAGE"
.Vb 1
  findrule [path...] [expression]
.Ve

## DESCRIPTION

Header "DESCRIPTION"
\f(CW\*(C`findrule\*(C' mostly borrows the interface from \s-1GNU\s0 **find**\|(1) to provide a
command-line interface onto the File::Find::Rule heirarchy of modules.

The syntax for expressions is the rule name, preceded by a dash,
followed by an optional argument.  If the argument is an opening
parenthesis it is taken as a list of arguments, terminated by a
closing parenthesis.

Some examples:

.Vb 1
 find -file -name ( foo bar )
.Ve

files named \f(CW\*(C`foo\*(C' or \f(CW\*(C`bar\*(C', below the current directory.

.Vb 1
 find -file -name foo -bar
.Ve

files named \f(CW\*(C`foo\*(C', that have pubs (for this is what our ficticious
\f(CW\*(C`bar\*(C' clause specifies), below the current directory.

.Vb 1
 find -file -name ( -bar )
.Ve

files named \f(CW\*(C`-bar\*(C', below the current directory.  In this case if
we'd have omitted the parenthesis it would have parsed as a call to
name with no arguments, followed by a call to -bar.

### Supported switches

Subsection "Supported switches"
I'm very slack.  Please consult the File::Find::Rule manpage for now,
and prepend - to the commands that you want.

### Extra bonus switches

Subsection "Extra bonus switches"
findrule automatically loads all of your installed File::Find::Rule::*
extension modules, so check the documentation to see what those would be.

## AUTHOR

Header "AUTHOR"
Richard Clamp <richardc@unixbeard.net> from a suggestion by Tatsuhiko Miyagawa

## COPYRIGHT

Header "COPYRIGHT"
Copyright (C) 2002 Richard Clamp.  All Rights Reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

## SEE ALSO

Header "SEE ALSO"
File::Find::Rule
