+++
operating_system = "macos"
operating_system_version = "15.3"
manpage_section = "1"
detected_package_version = "5.34.1"
title = "perlbook(1)"
description = "There are many books on Perl and Perl-related. A few of these are good, some are s-1OK,s0 but many arent worth your money. There is a list of these books, some with extensive reviews, at <https://www.perl.org/books/library.html> . We list some of ..."
manpage_format = "troff"
date = "2022-02-19"
author = "None Specified"
manpage_name = "perlbook"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLBOOK 1"
PERLBOOK 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlbook - Books about and related to Perl

## DESCRIPTION

Header "DESCRIPTION"
There are many books on Perl and Perl-related. A few of these are
good, some are \s-1OK,\s0 but many aren't worth your money. There is a list
of these books, some with extensive reviews, at
<https://www.perl.org/books/library.html> . We list some of the books here, and while
listing a book implies our
endorsement, don't think that not including a book means anything.

Most of these books are available online through Safari Books Online
( <http://safaribooksonline.com/> ).

### The most popular books

Subsection "The most popular books"
The major reference book on Perl, written by the creator of Perl, is
*Programming Perl*:
.ie n .IP "*Programming Perl* (the ""Camel Book""):" 4
.el .IP "*Programming Perl* (the ``Camel Book''):" 4
Item "Programming Perl (the Camel Book):"
.Vb 4
 by Tom Christiansen, brian d foy, Larry Wall with Jon Orwant
 ISBN 978-0-596-00492-7 [4th edition February 2012]
 ISBN 978-1-4493-9890-3 [ebook]
 https://oreilly.com/catalog/9780596004927
.Ve

The Ram is a cookbook with hundreds of examples of using Perl to
accomplish specific tasks:
.ie n .IP "*The Perl Cookbook* (the ""Ram Book""):" 4
.el .IP "*The Perl Cookbook* (the ``Ram Book''):" 4
Item "The Perl Cookbook (the Ram Book):"
.Vb 5
 by Tom Christiansen and Nathan Torkington,
 with Foreword by Larry Wall
 ISBN 978-0-596-00313-5 [2nd Edition August 2003]
 ISBN 978-0-596-15888-0 [ebook]
 https://oreilly.com/catalog/9780596003135/
.Ve

If you want to learn the basics of Perl, you might start with the
Llama book, which assumes that you already know a little about
programming:
.ie n .IP "*Learning Perl*  (the ""Llama Book"")" 4
.el .IP "*Learning Perl*  (the ``Llama Book'')" 4
Item "Learning Perl (the Llama Book)"
.Vb 4
 by Randal L. Schwartz, Tom Phoenix, and brian d foy
 ISBN 978-1-4493-0358-7 [6th edition June 2011]
 ISBN 978-1-4493-0458-4 [ebook]
 https://www.learning-perl.com/
.Ve

The tutorial started in the Llama continues in the Alpaca, which
introduces the intermediate features of references, data structures,
object-oriented programming, and modules:
.ie n .IP "*Intermediate Perl* (the ""Alpaca Book"")" 4
.el .IP "*Intermediate Perl* (the ``Alpaca Book'')" 4
Item "Intermediate Perl (the Alpaca Book)"
.Vb 5
 by Randal L. Schwartz and brian d foy, with Tom Phoenix
         foreword by Damian Conway
 ISBN 978-1-4493-9309-0 [2nd edition August 2012]
 ISBN 978-1-4493-0459-1 [ebook]
 https://www.intermediateperl.com/
.Ve

### References

Subsection "References"
You might want to keep these desktop references close by your keyboard:

- \fIPerl 5 Pocket Reference
Item "Perl 5 Pocket Reference"
.Vb 4
 by Johan Vromans
 ISBN 978-1-4493-0370-9 [5th edition July 2011]
 ISBN 978-1-4493-0813-1 [ebook]
 https://oreilly.com/catalog/0636920018476/
.Ve

- \fIPerl Debugger Pocket Reference
Item "Perl Debugger Pocket Reference"
.Vb 4
 by Richard Foley
 ISBN 978-0-596-00503-0 [1st edition January 2004]
 ISBN 978-0-596-55625-9 [ebook]
 https://oreilly.com/catalog/9780596005030/
.Ve

- \fIRegular Expression Pocket Reference
Item "Regular Expression Pocket Reference"
.Vb 4
 by Tony Stubblebine
 ISBN 978-0-596-51427-3 [2nd edition July 2007]
 ISBN 978-0-596-55782-9 [ebook]
 https://oreilly.com/catalog/9780596514273/
.Ve

### Tutorials

Subsection "Tutorials"

- \fIBeginning Perl
Item "Beginning Perl"
(There are 2 books with this title)
.Sp
.Vb 3
 by Curtis \*(AqOvid\*(Aq Poe
 ISBN 978-1-118-01384-7
 http://www.wrox.com/WileyCDA/WroxTitle/productCd-1118013840.html

 by James Lee
 ISBN 1-59059-391-X [3rd edition April 2010 & ebook]
 https://www.apress.com/9781430227939
.Ve
.ie n .IP "*Learning Perl* (the ""Llama Book"")" 4
.el .IP "*Learning Perl* (the ``Llama Book'')" 4
Item "Learning Perl (the Llama Book)"
.Vb 4
 by Randal L. Schwartz, Tom Phoenix, and brian d foy
 ISBN 978-1-4493-0358-7 [6th edition June 2011]
 ISBN 978-1-4493-0458-4 [ebook]
 https://www.learning-perl.com/
.Ve
.ie n .IP "*Intermediate Perl* (the ""Alpaca Book"")" 4
.el .IP "*Intermediate Perl* (the ``Alpaca Book'')" 4
Item "Intermediate Perl (the Alpaca Book)"
.Vb 5
 by Randal L. Schwartz and brian d foy, with Tom Phoenix
         foreword by Damian Conway
 ISBN 978-1-4493-9309-0 [2nd edition August 2012]
 ISBN 978-1-4493-0459-1 [ebook]
 https://www.intermediateperl.com/
.Ve

- \fIMastering Perl
Item "Mastering Perl"
.Vb 4
    by brian d foy
 ISBN 9978-1-4493-9311-3 [2st edition January 2014]
 ISBN 978-1-4493-6487-8 [ebook]
 https://www.masteringperl.org/
.Ve

- \fIEffective Perl Programming
Item "Effective Perl Programming"
.Vb 3
 by Joseph N. Hall, Joshua A. McAdams, brian d foy
 ISBN 0-321-49694-9 [2nd edition 2010]
 https://www.effectiveperlprogramming.com/
.Ve

### Task-Oriented

Subsection "Task-Oriented"

- \fIWriting Perl Modules for \s-1CPAN\s0
Item "Writing Perl Modules for CPAN"
.Vb 3
 by Sam Tregar
 ISBN 1-59059-018-X [1st edition August 2002 & ebook]
 https://www.apress.com/9781590590188
.Ve

- \fIThe Perl Cookbook
Item "The Perl Cookbook"
.Vb 5
 by Tom Christiansen and Nathan Torkington,
     with Foreword by Larry Wall
 ISBN 978-0-596-00313-5 [2nd Edition August 2003]
 ISBN 978-0-596-15888-0 [ebook]
 https://oreilly.com/catalog/9780596003135/
.Ve

- \fIAutomating System Administration with Perl
Item "Automating System Administration with Perl"
.Vb 4
 by David N. Blank-Edelman
 ISBN 978-0-596-00639-6 [2nd edition May 2009]
 ISBN 978-0-596-80251-6 [ebook]
 https://oreilly.com/catalog/9780596006396
.Ve

- \fIReal World \s-1SQL\s0 Server Administration with Perl
Item "Real World SQL Server Administration with Perl"
.Vb 3
 by Linchi Shea
 ISBN 1-59059-097-X [1st edition July 2003 & ebook]
 https://www.apress.com/9781590590973
.Ve

### Special Topics

Subsection "Special Topics"

- \fIRegular Expressions Cookbook
Item "Regular Expressions Cookbook"
.Vb 4
 by Jan Goyvaerts and Steven Levithan
 ISBN 978-1-4493-1943-4 [2nd edition August 2012]
 ISBN 978-1-4493-2747-7 [ebook]
 https://shop.oreilly.com/product/0636920023630.do
.Ve

- \fIProgramming the Perl \s-1DBI\s0
Item "Programming the Perl DBI"
.Vb 4
 by Tim Bunce and Alligator Descartes
 ISBN 978-1-56592-699-8 [February 2000]
 ISBN 978-1-4493-8670-2 [ebook]
 https://oreilly.com/catalog/9781565926998
.Ve

- \fIPerl Best Practices
Item "Perl Best Practices"
.Vb 4
 by Damian Conway
 ISBN 978-0-596-00173-5 [1st edition July 2005]
 ISBN 978-0-596-15900-9 [ebook]
 https://oreilly.com/catalog/9780596001735
.Ve

- \fIHigher-Order Perl
Item "Higher-Order Perl"
.Vb 4
 by Mark-Jason Dominus
 ISBN 1-55860-701-3 [1st edition March 2005]
 free ebook https://hop.perl.plover.com/book/
 https://hop.perl.plover.com/
.Ve

- \fIMastering Regular Expressions
Item "Mastering Regular Expressions"
.Vb 4
 by Jeffrey E. F. Friedl
 ISBN 978-0-596-52812-6 [3rd edition August 2006]
 ISBN 978-0-596-55899-4 [ebook]
 https://oreilly.com/catalog/9780596528126
.Ve

- \fINetwork Programming with Perl
Item "Network Programming with Perl"
.Vb 3
 by Lincoln Stein
 ISBN 0-201-61571-1 [1st edition 2001]
 https://www.pearsonhighered.com/educator/product/Network-Programming-with-Perl/9780201615715.page
.Ve

- \fIPerl Template Toolkit
Item "Perl Template Toolkit"
.Vb 4
 by Darren Chamberlain, Dave Cross, and Andy Wardley
 ISBN 978-0-596-00476-7 [December 2003]
 ISBN 978-1-4493-8647-4 [ebook]
 https://oreilly.com/catalog/9780596004767
.Ve

- \fIObject Oriented Perl
Item "Object Oriented Perl"
.Vb 4
 by Damian Conway
     with foreword by Randal L. Schwartz
 ISBN 1-884777-79-1 [1st edition August 1999 & ebook]
 https://www.manning.com/conway/
.Ve

- \fIData Munging with Perl
Item "Data Munging with Perl"
.Vb 3
 by Dave Cross
 ISBN 1-930110-00-6 [1st edition 2001 & ebook]
 https://www.manning.com/cross
.Ve

- \fIMastering Perl/Tk
Item "Mastering Perl/Tk"
.Vb 4
 by Steve Lidie and Nancy Walsh
 ISBN 978-1-56592-716-2 [1st edition January 2002]
 ISBN 978-0-596-10344-6 [ebook]
 https://oreilly.com/catalog/9781565927162
.Ve

- \fIExtending and Embedding Perl
Item "Extending and Embedding Perl"
.Vb 3
 by Tim Jenness and Simon Cozens
 ISBN 1-930110-82-0 [1st edition August 2002 & ebook]
 https://www.manning.com/jenness
.Ve

- \fIPro Perl Debugging
Item "Pro Perl Debugging"
.Vb 3
 by Richard Foley with Andy Lester
 ISBN 1-59059-454-1 [1st edition July 2005 & ebook]
 https://www.apress.com/9781590594544
.Ve

### Free (as in beer) books

Subsection "Free (as in beer) books"
Some of these books are available as free downloads.

*Higher-Order Perl*: <https://hop.perl.plover.com/>

*Modern Perl*: <http://onyxneon.com/books/modern_perl/>

### Other interesting, non-Perl books

Subsection "Other interesting, non-Perl books"
You might notice several familiar Perl concepts in this collection of
\s-1ACM\s0 columns from Jon Bentley. The similarity to the title of the major
Perl book (which came later) is not completely accidental:

- \fIProgramming Pearls
Item "Programming Pearls"
.Vb 2
 by Jon Bentley
 ISBN 978-0-201-65788-3 [2 edition, October 1999]
.Ve

- \fIMore Programming Pearls
Item "More Programming Pearls"
.Vb 2
 by Jon Bentley
 ISBN 0-201-11889-0 [January 1988]
.Ve

### A note on freshness

Subsection "A note on freshness"
Each version of Perl comes with the documentation that was current at
the time of release. This poses a problem for content such as book
lists. There are probably very nice books published after this list
was included in your Perl release, and you can check the latest
released version at <https://perldoc.perl.org/perlbook.html> .

Some of the books we've listed appear almost ancient in internet
scale, but we've included those books because they still describe the
current way of doing things. Not everything in Perl changes every day.
Many of the beginner-level books, too, go over basic features and
techniques that are still valid today. In general though, we try to
limit this list to books published in the past five years.

### Get your book listed

Subsection "Get your book listed"
If your Perl book isn't listed and you think it should be, let us know.
<mailto:perl5-porters@perl.org>
