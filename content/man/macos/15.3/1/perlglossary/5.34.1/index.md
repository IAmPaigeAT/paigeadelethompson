+++
author = "None Specified"
manpage_format = "troff"
manpage_section = "1"
title = "perlglossary(1)"
detected_package_version = "5.34.1"
operating_system_version = "15.3"
operating_system = "macos"
description = "A glossary of terms (technical and otherwise) used in the Perl documentation, derived from the Glossary of  Perl, Fourth Edition.  Words or phrases in bold are defined elsewhere in this glossary. Other useful sources include the Unicode Glossary <..."
date = "2022-02-19"
manpage_name = "perlglossary"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLGLOSSARY 1"
PERLGLOSSARY 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlglossary - Perl Glossary

## VERSION

Header "VERSION"
version 5.20210411

## DESCRIPTION

Header "DESCRIPTION"
A glossary of terms (technical and otherwise) used in the Perl
documentation, derived from the Glossary of \fIProgramming
Perl, Fourth Edition.  Words or phrases in bold are defined elsewhere in
this glossary.

Other useful sources include the Unicode Glossary <http://unicode.org/glossary/>,
the Free On-Line Dictionary of Computing <http://foldoc.org/>,
the Jargon File <http://catb.org/~esr/jargon/>,
and Wikipedia <http://www.wikipedia.org/>.

### A

Subsection "A"

- accessor methods
Item "accessor methods"
A **method** used to
indirectly inspect or update an **object**Xs state (its \fBinstance
variables).
Xref "accessor methods, defined methods, accessor"

- actual arguments
Item "actual arguments"
The **scalar values** that you supply
to a **function** or **subroutine** when you call it. For instance, when you
call \f(CW\*(C`power("puff")\*(C', the string \f(CW"puff" is the actual argument. See also
**argument** and **formal arguments**.
Xref "actual arguments arguments, actual"

- address operator
Item "address operator"
Some languages work directly with the memory addresses of
values, but this can be like playing with fire. Perl provides a set of
asbestos gloves for handling all memory management. The closest to an
address operator in Perl is the backslash operator, but it gives you a
**hard reference**, which is much safer than a memory address.
Xref "address operator"

- algorithm
Item "algorithm"
A well-defined sequence of steps, explained clearly
enough that even a computer could do them.
Xref "algorithms (term)"

- alias
Item "alias"
A nickname for something, which behaves in all ways as
though youXd used the original name instead of the nickname. Temporary
aliases are implicitly created in the loop variable for \f(CW\*(C`foreach\*(C' loops, in
the \f(CW$_ variable for \f(CW\*(C`map\*(C' or \f(CW\*(C`grep\*(C' operators, in \f(CW$a and \f(CW$b
during \f(CW\*(C`sort\*(C'Xs comparison function, and in each element of \f(CW@_ for the
**actual arguments** of a subroutine call. Permanent aliases are explicitly
created in **packages** by **importing** symbols or by assignment to
**typeglobs**. Lexically scoped aliases for package variables are explicitly
created by the \f(CW\*(C`our\*(C' declaration.
Xref "aliases, defined"

- alphabetic
Item "alphabetic"
The sort of characters we put into words. In Unicode, this
is all letters including all ideographs and certain diacritics, letter
numbers like Roman numerals, and various combining marks.
Xref "alphabetic sort"

- alternatives
Item "alternatives"
A list of possible choices from which you may
select only one, as in, XWould you like door A, B, or C?X Alternatives in
regular expressions are separated with a single vertical bar: \f(CW\*(C`|\*(C'.
Alternatives in normal Perl expressions are separated with a double vertical
bar: \f(CW\*(C`||\*(C'. Logical alternatives in **Boolean** expressions are separated
with either \f(CW\*(C`||\*(C' or \f(CW\*(C`or\*(C'.
Xref "alternative characters"

- anonymous
Item "anonymous"
Used to describe a **referent**
that is not directly accessible through a named **variable**. Such a referent
must be indirectly accessible through at least one **hard reference**. When
the last hard reference goes away, the anonymous referent is destroyed
without pity.
Xref "anonymous referents referents, anonymous"

- application
Item "application"
A bigger, fancier sort of **program** with a fancier
name so people donXt realize they are using a program.
Xref "applications (term)"

- architecture
Item "architecture"
The kind of computer youXre working on, where one Xkind of
computerX means all those computers sharing a compatible machine language.
Since Perl programs are (typically) simple text files, not executable
images, a Perl program is much less sensitive to the architecture itXs
running on than programs in other languages, such as C, that are **compiled**
into machine code. See also **platform** and **operating system**.
Xref "architecture"

- argument
Item "argument"
A piece of data supplied to a **program**,
**subroutine**, **function**, or **method** to tell it what itXs supposed to
do. Also called a XparameterX.
Xref "arguments, defined"

- \s-1ARGV\s0
Item "ARGV"
The name of the array containing the **argument** **vector**
from the command line. If you use the empty \f(CW\*(C`<>\*(C' operator, \f(CW\*(C`ARGV\*(C'
is the name of both the **filehandle** used to traverse the arguments and the
**scalar** containing the name of the current input file.
Xref "ARGV filehandle"

- arithmetical operator
Item "arithmetical operator"
A **symbol** such as \f(CW\*(C`+\*(C' or \f(CW\*(C`/\*(C' that tells
Perl to do the arithmetic you were supposed to learn in grade school.
Xref "arithmetic operators, about"

- array
Item "array"
An ordered sequence of **values**, stored such that you can
easily access any of the values using an *integer subscript* that specifies
the valueXs **offset** in the sequence.
Xref "arrays, defined"

- array context
Item "array context"
An archaic expression for what is more correctly referred to
as **list context**.
Xref "array context"

- Artistic License
Item "Artistic License"
The open source license that Larry Wall
created for Perl, maximizing PerlXs usefulness, availability, and
modifiability. The current version is 2. (<http://www.opensource.org/licenses/artistic-license.php>).
Xref "Artistic License Wall, Larry"

- \s-1ASCII\s0
Item "ASCII"
The American Standard Code for
Information Interchange (a 7-bit character set adequate only for poorly
representing English text). Often used loosely to describe the lowest 128
values of the various \s-1ISO-8859-X\s0 character sets, a bunch of mutually
incompatible 8-bit codes best described as half \s-1ASCII.\s0 See also **Unicode**.
Xref "ASCII (American Standard Code for Information Interchange) American Standard Code for Information Interchange (ASCII)"

- assertion
Item "assertion"
A component of a **regular expression** that must be true for the pattern to
match but does not necessarily match any characters itself. Often used
specifically to mean a **zero-width** assertion.
Xref "assertions (in regexes), defined regular expressions, assertions in"

- assignment
Item "assignment"
An **operator** whose assigned mission in life is to
change the value of a **variable**.
Xref "assignments, defined"

- assignment operator
Item "assignment operator"
Either a regular **assignment** or a compound
**operator** composed of an ordinary assignment and some other operator, that
changes the value of a variable in place; that is, relative to its old
value. For example, \f(CW\*(C`$a += 2\*(C' adds \f(CW2 to \f(CW$a.
Xref "assignment operators, about"

- associative array
Item "associative array"
See **hash**. Please. The term associative array is the
old Perl 4 term for a **hash**. Some languages call it a dictionary.
Xref "associative arrays"

- associativity
Item "associativity"
Determines whether you do the left **operator** first or the
right **operator** first when you have \s-1XA\s0 **operator** B **operator** \s-1CX,\s0 and
the two operators are of the same precedence. Operators like \f(CW\*(C`+\*(C' are left
associative, while operators like \f(CW\*(C`**\*(C' are right associative. See Camel
chapter 3, XUnary and Binary OperatorsX for a list of operators and their
associativity.
Xref "associativity"

- asynchronous
Item "asynchronous"
Said of events or activities whose relative
temporal ordering is indeterminate because too many things are going on at
once. Hence, an asynchronous event is one you didnXt know when to expect.
Xref "asynchronous event processing"

- atom
Item "atom"
A **regular expression** component potentially matching a
**substring** containing one or more characters and treated as an indivisible
syntactic unit by any following **quantifier**. (Contrast with an
**assertion** that matches something of **zero width** and may not be quantified.)
Xref "atoms"

- atomic operation
Item "atomic operation"
When Democritus gave the word XatomX to the indivisible
bits of matter, he meant literally something that could not be cut: *X-*
(not) + *-XXXXX* (cuttable). An atomic operation is an action that canXt be
interrupted, not one forbidden in a nuclear-free zone.
Xref "atomic operation"

- attribute
Item "attribute"
A new feature that allows the declaration of
**variables** and **subroutines** with modifiers, as in \f(CW\*(C`sub foo : locked
method\*(C'. Also another name for an **instance variable** of an **object**.
Xref "attribute feature"

- autogeneration
Item "autogeneration"
A feature of **operator overloading** of **objects**,
whereby the behavior of certain **operators** can be reasonably deduced using
more fundamental operators. This assumes that the overloaded operators will
often have the same relationships as the regular operators. See Camel
chapter 13, XOverloadingX.
Xref "autogeneration, about"

- autoincrement
Item "autoincrement"
To add one to something automatically, hence the name
of the \f(CW\*(C`++\*(C' operator. To instead subtract one from something automatically
is known as an XautodecrementX.
Xref "autoincrement (term)"

- autoload
Item "autoload"
To load on demand. (Also called XlazyX loading.)
Specifically, to call an \f(CW\*(C`AUTOLOAD\*(C' subroutine on behalf of an undefined
subroutine.
Xref "autoloading, defined"

- autosplit
Item "autosplit"
To split a string automatically, as the *Xa* **switch**
does when running under *Xp* or *Xn* in order to emulate **awk**. (See also
the \f(CW\*(C`AutoSplit\*(C' module, which has nothing to do with the
\f(CW\*(C`Xa\*(C' switch but a lot to do with autoloading.)
Xref "autosplit (term) AutoSplit module"

- autovivification
Item "autovivification"
A Graeco-Roman word meaning Xto bring oneself to lifeX.
In Perl, storage locations (**lvalues**) spontaneously generate themselves as
needed, including the creation of any **hard reference** values to point to
the next level of storage. The assignment \f(CW\*(C`$a[5][5][5][5][5] = "quintet"\*(C'
potentially creates five scalar storage locations, plus four references (in
the first four scalar locations) pointing to four new anonymous arrays (to
hold the last four scalar locations). But the point of autovivification is
that you donXt have to worry about it.
Xref "autovivification"

- \s-1AV\s0
Item "AV"
Short for Xarray
valueX, which refers to one of PerlXs internal data types that holds an
**array**. The \f(CW\*(C`AV\*(C' type is a subclass of **\s-1SV\s0**.
Xref "AV (array value) array value (AV) values, array"

- awk
Item "awk"
Descriptive editing termXshort for XawkwardX. Also
coincidentally refers to a venerable text-processing language from which
Perl derived some of its high-level ideas.
Xref "awk (editing term)"

### B

Subsection "B"

- backreference
Item "backreference"
A substring **captured**
by a subpattern within unadorned parentheses in a **regex**. Backslashed
decimal numbers (\f(CW\*(C`\\1\*(C', \f(CW\*(C`\\2\*(C', etc.) later in the same pattern refer back to
the corresponding subpattern in the current match. Outside the pattern, the
numbered variables (\f(CW$1, \f(CW$2, etc.) continue to refer to these same
values, as long as the pattern was the last successful match of the current
**dynamic scope**.
Xref "backreferences, about references, backreferences"

- backtracking
Item "backtracking"
The practice of saying, XIf I had to do it all over, IXd do
it differently,X and then actually going back and doing it all over
differently. Mathematically speaking, itXs returning from an unsuccessful
recursion on a tree of possibilities. Perl backtracks when it attempts to
match patterns with a **regular expression**, and its earlier attempts donXt
pan out. See the section XThe Little Engine That /Couldn(nXt)X in Camel
chapter 5, XPattern MatchingX.
Xref "backtracking"

- backward compatibility
Item "backward compatibility"
Means you can still run your old program
because we didnXt break any of the features or bugs it was relying on.
Xref "backward compatibility, defined"

- bareword
Item "bareword"
A word sufficiently ambiguous to be deemed illegal under
\f(CW\*(C`use strict \*(Aqsubs\*(Aq\*(C'. In the absence of that stricture, a bareword is
treated as if quotes were around it.
Xref "barewords, about"

- base class
Item "base class"
A generic **object** type; that is, a **class**
from which other, more specific classes are derived genetically by
**inheritance**. Also called a
XsuperclassX by people who respect their ancestors.
Xref "base classes classes, base superclasses classes, superclasses"

- big-endian
Item "big-endian"
From Swift: someone who
eats eggs big end first. Also used of computers that store the most
significant **byte** of a word at a lower byte address than the least
significant byte. Often considered superior to little-endian machines. See
also **little-endian**.
Xref "bigXendian, defined endianness, bigXendian"

- binary
Item "binary"
Having to do with numbers represented in base 2. That means
thereXs basically two numbers: 0 and 1. Also used to describe a file of
XnontextX, presumably because such a file makes full use of all the binary
bits in its bytes. With the advent of **Unicode**, this distinction, already
suspect, loses even more of its meaning.
Xref "binary (term)"

- binary operator
Item "binary operator"
An **operator** that takes two **operands**.
Xref "binary operators, about"

- bind
Item "bind"
To assign a specific **network address** to a **socket**.
Xref "bind (term)"

- bit
Item "bit"
An integer in the range from 0 to 1, inclusive. The smallest
possible unit of information storage. An eighth of a **byte** or of a dollar.
(The term XPieces of EightX comes from being able to split the old Spanish
dollar into 8 bits, each of which still counted for money. ThatXs why a 25-
cent piece today is still Xtwo bitsX.)
Xref "bits, defined"

- bit shift
Item "bit shift"
The movement of bits left or right in a
computer word, which has the effect of multiplying or dividing by a
power of 2.
Xref "bitXshift operators, defined"

- bit string
Item "bit string"
A sequence of **bits** that is actually being thought of as a
sequence of bits, for once.
Xref "bit string"

- bless
Item "bless"
In corporate life, to grant official
approval to a thing, as in, XThe \s-1VP\s0 of Engineering has blessed our
WebCruncher project.X Similarly, in Perl, to grant official approval to a
**referent** so that it can function as an **object**, such as a WebCruncher
object. See the \f(CW\*(C`bless\*(C' function in Camel chapter 27, XFunctionsX.
Xref "bless function, about bless (term)"

- block
Item "block"
What a **process** does when it has to wait for something:
XMy process blocked waiting for the disk.X As an unrelated noun, it refers
to a large chunk of data, of a size that the **operating system** likes to
deal with (normally a power of 2 such as 512 or 8192). Typically refers to
a chunk of data thatXs coming from or going to a disk file.
Xref "blocks, defined"

- \s-1BLOCK\s0
Item "BLOCK"
A syntactic construct
consisting of a sequence of Perl **statements** that is delimited by braces.
The \f(CW\*(C`if\*(C' and \f(CW\*(C`while\*(C' statements are defined in terms of *\f(CI\*(C`BLOCK\*(C'\fI*s, for
instance. Sometimes we also say XblockX to mean a lexical scope; that is, a
sequence of statements that acts like a *\f(CI\*(C`BLOCK\*(C'\fI*, such as within an
\f(CW\*(C`eval\*(C' or a file, even though the statements arenXt delimited by braces.
Xref "BLOCK construct, about constructs, BLOCK"

- block buffering
Item "block buffering"
A method of making input and output
efficient by passing one **block** at a time. By default, Perl does block
buffering to disk files. See **buffer** and **command buffering**.
Xref "block buffering buffering, block"

- Boolean
Item "Boolean"
A value that is either **true** or
**false**.
Xref "Boolean values values, Boolean"

- Boolean context
Item "Boolean context"
A special kind of \fBscalar
context used in conditionals to decide whether the **scalar value** returned
by an expression is **true** or **false**. Does not evaluate as either a
string or a number. See **context**.
Xref "Boolean context, about context, Boolean"

- breakpoint
Item "breakpoint"
A spot in your program where youXve told the debugger
to stop **execution** so you can poke around and see whether anything is
wrong yet.
Xref "breakpoints, defined"

- broadcast
Item "broadcast"
To send a **datagram** to multiple destinations
simultaneously.
Xref "broadcast (networking term)"

- \s-1BSD\s0
Item "BSD"
A psychoactive drug, popular in the X80s, probably developed at \s-1UC\s0
Berkeley or thereabouts. Similar in many ways to the prescription-only
medication called XSystem \s-1VX,\s0 but infinitely more useful. (Or, at least,
more fun.) The full chemical name is XBerkeley Standard DistributionX.
Xref "BSD (Berkeley Standard Distribution) Berkeley Standard Distribution (BSD)"

- bucket
Item "bucket"
A location in a **hash table** containing (potentially)
multiple entries whose keys XhashX to the same hash value according to its
hash function. (As internal policy, you donXt have to worry about it unless
youXre into internals, or policy.)
Xref "buckets (term)"

- buffer
Item "buffer"
A temporary holding location for data. Data that are
**Block buffering** means that the data is passed on to its destination
whenever the buffer is full. **Line buffering** means that itXs passed on
whenever a complete line is received. **Command buffering** means that itXs
passed every time you do a \f(CW\*(C`print\*(C' command (or equivalent). If your output
is unbuffered, the system processes it one byte at a time without the use of
a holding area. This can be rather inefficient.
Xref "buffers, defined"

- built-in
Item "built-in"
A **function** that is predefined in the
language. Even when hidden by **overriding**, you can always get at a built-
in function by **qualifying** its name with the \f(CW\*(C`CORE::\*(C' pseudopackage.
Xref "builtXin functions, about"

- bundle
Item "bundle"
A group of related modules on **\s-1CPAN\s0**. (Also sometimes
refers to a group of command-line switches grouped into one \fBswitch
cluster.)
Xref "bundles (term)"

- byte
Item "byte"
A piece of data worth eight **bits** in most places.
Xref "bytes (term)"

- bytecode
Item "bytecode"
A pidgin-like lingo spoken among Xdroids when they donXt wish to reveal
their orientation (see **endian**). Named after some similar languages spoken
(for similar reasons) between compilers and interpreters in the late 20XX
century. These languages are characterized by representing everything as a
nonarchitecture-dependent sequence of bytes.

### C

Subsection "C"

- C
Item "C"
A language beloved by many for its inside-out **type**
definitions, inscrutable **precedence** rules, and heavy **overloading** of
the function-call mechanism. (Well, actually, people first switched to C
because they found lowercase identifiers easier to read than upper.) Perl is
written in C, so itXs not surprising that Perl borrowed a few ideas from it.
Xref "C language, about"

- cache
Item "cache"
A data repository. Instead of computing expensive answers
several times, compute it once and save the result.
Xref "cache (term)"

- callback
Item "callback"
A **handler** that you register with some other part of your
program in the hope that the other part of your program will **trigger** your
handler when some event of interest transpires.
Xref "callbacks"

- call by reference
Item "call by reference"
An **argument**-passing mechanism in which the **formal arguments** refer directly to the
**actual arguments**, and the **subroutine** can change the actual arguments
by changing the formal arguments. That is, the formal argument is an
**alias** for the actual argument. See also **call by value**.
Xref "call by reference references, call by reference mechanism"

- call by value
Item "call by value"
An **argument**-passing mechanism in which the \fBformal
arguments refer to a copy of the **actual arguments**, and the
**subroutine** cannot change the actual arguments by changing the formal
arguments. See also **call by reference**.
Xref "call by value"

- canonical
Item "canonical"
Reduced to a standard form to facilitate comparison.
Xref "canonical (term)"

- capture variables
Item "capture variables"
The variablesXsuch as \f(CW$1 and
\f(CW$2, and \f(CW\*(C`%+\*(C' and \f(CW%X Xthat hold the text remembered in a pattern
match. See Camel chapter 5, XPattern MatchingX.
Xref "capture variables variables, capture"

- capturing
Item "capturing"
The use of parentheses around a **subpattern** in a
**regular expression** to store the matched **substring** as a
**backreference**. (Captured strings are also returned as a list in \fBlist
context.) See Camel chapter 5, XPattern MatchingX.
Xref "capturing in pattern matching subpatterns, capturing pattern matching, capturing in"

- cargo cult
Item "cargo cult"
Copying and pasting code without understanding it, while
superstitiously believing in its value. This term originated from
preindustrial cultures dealing with the detritus of explorers and colonizers
of technologically advanced cultures. See *The Gods Must Be Crazy*.
Xref "cargo cult"

- case
Item "case"
A property of certain
characters. Originally, typesetter stored capital letters in the upper of
two cases and small letters in the lower one. Unicode recognizes three
cases: **lowercase** (**character property** \f(CW\*(C`\\p\{lower\}\*(C'), **titlecase**
(\f(CW\*(C`\\p\{title\}\*(C'), and **uppercase** (\f(CW\*(C`\\p\{upper\}\*(C'). A fourth casemapping called
**foldcase** is not itself a distinct case, but it is used internally to
implement **casefolding**. Not all letters have case, and some nonletters
have case.
Xref "case (character) characters, case considerations"

- casefolding
Item "casefolding"
Comparing or matching a string case-insensitively. In Perl, it
is implemented with the \f(CW\*(C`/i\*(C' pattern modifier, the \f(CW\*(C`fc\*(C' function, and the
\f(CW\*(C`\\F\*(C' double-quote translation escape.
Xref "casefolding"

- casemapping
Item "casemapping"
The process of converting a string to one of the four Unicode
**casemaps**; in Perl, it is implemented with the \f(CW\*(C`fc\*(C', \f(CW\*(C`lc\*(C', \f(CW\*(C`ucfirst\*(C',
and \f(CW\*(C`uc\*(C' functions.
Xref "casemapping"

- character
Item "character"
The smallest individual element of a string. Computers
store characters as integers, but Perl lets you operate on them as text. The
integer used to represent a particular character is called that characterXs
**codepoint**.
Xref "characters, defined"

- character class
Item "character class"
A square-bracketed list of
characters used in a **regular expression** to indicate that any character
of the set may occur at a given point. Loosely, any predefined set of
characters so used.
Xref "character classes, about classes, character"

- character property
Item "character property"
A predefined **character class** matchable by the \f(CW\*(C`\\p\*(C'
or \f(CW\*(C`\\P\*(C' **metasymbol**. **Unicode** defines hundreds of standard properties
for every possible codepoint, and Perl defines a few of its own, too.
Xref "character property"

- circumfix operator
Item "circumfix operator"
An **operator** that surrounds its **operand**, like the
angle operator, or parentheses, or a hug.
Xref "circumfix operator"

- class
Item "class"
A user-defined **type**, implemented in Perl via a
**package** that provides (either directly or by inheritance) **methods**
(that is, **subroutines**) to handle **instances** of the class (its
**objects**). See also **inheritance**.
Xref "classes, defined"

- class method
Item "class method"
A **method** whose **invocant** is a
**package** name, not an **object** reference. A method associated with the
class as a whole. Also see **instance method**.
Xref "class methods methods, class"

- client
Item "client"
In networking, a **process** that
initiates contact with a **server** process in order to exchange data and
perhaps receive a service.
Xref "clients, defined processes, client"

- closure
Item "closure"
An **anonymous** subroutine
that, when a reference to it is generated at runtime, keeps track of the
identities of externally visible **lexical variables**, even after those
lexical variables have supposedly gone out of **scope**. TheyXre called
XclosuresX because this sort of behavior gives mathematicians a sense of
closure.
Xref "closure subroutines subroutines, closure"

- cluster
Item "cluster"
A parenthesized **subpattern**
used to group parts of a **regular expression** into a single **atom**.
Xref "clusters, defined subpatterns, cluster"

- \s-1CODE\s0
Item "CODE"
The word returned by the \f(CW\*(C`ref\*(C'
function when you apply it to a reference to a subroutine. See also **\s-1CV\s0**.
Xref "CODE (ref function) ref function, about"

- code generator
Item "code generator"
A system that writes code for you in a low-level
language, such as code to implement the backend of a compiler. See \fBprogram
generator.
Xref "code generators, defined"

- codepoint
Item "codepoint"
The integer a computer uses to represent a given
character. \s-1ASCII\s0 codepoints are in the range 0 to 127; Unicode codepoints
are in the range 0 to 0x1F_FFFF; and Perl codepoints are in the range 0 to
2XXX1 or 0 to 2XXX1, depending on your native integer size. In Perl Culture,
sometimes called **ordinals**.
Xref "codepoints, about"

- code subpattern
Item "code subpattern"
A **regular expression** subpattern
whose real purpose is to execute some Perl codeXfor example, the \f(CW\*(C`(?\{...\})\*(C'
and \f(CW\*(C`(??\{...\})\*(C' subpatterns.
Xref "code subpatterns subpatterns, code"

- collating sequence
Item "collating sequence"
The order into which **characters**
sort. This is used by **string** comparison routines to decide, for example,
where in this glossary to put Xcollating sequenceX.
Xref "collating sequence collating sequence"

- co-maintainer
Item "co-maintainer"
A person with permissions to index a **namespace** in
**\s-1PAUSE\s0**. Anyone can upload any namespace, but only primary and
co-maintainers get their contributions indexed.
Xref "coXmaintainers"

- combining character
Item "combining character"
Any character with the
General Category of Combining Mark (\f(CW\*(C`\\p\{GC=M\}\*(C'), which may be spacing or
nonspacing. Some are even invisible. A sequence of combining characters
following a grapheme base character together make up a single user-visible
character called a **grapheme**. Most but not all diacritics are combining
characters, and vice versa.
Xref "combining characters characters, combining"

- command
Item "command"
In **shell** programming, the syntactic combination of a
program name and its arguments. More loosely, anything you type to a shell
(a command interpreter) that starts it doing something. Even more loosely, a
Perl **statement**, which might start with a **label** and typically ends with
a semicolon.
Xref "commands, defined"

- command buffering
Item "command buffering"
A mechanism in Perl that lets you
store up the output of each Perl **command** and then flush it out as a
single request to the **operating system**. ItXs enabled by setting the \f(CW$|
(\f(CW$AUTOFLUSH) variable to a true value. ItXs used when you donXt want data
sitting around, not going where itXs supposed to, which may happen because
the default on a **file** or **pipe** is to use **block buffering**.
Xref "command buffering buffering, command"

- command-line arguments
Item "command-line arguments"
The **values** you supply
along with a program name when you tell a **shell** to execute a **command**.
These values are passed to a Perl program through \f(CW@ARGV.
Xref "commandXline arguments arguments, commandXline"

- command name
Item "command name"
The name of the program currently executing, as typed on the
command line. In C, the **command** name is passed to the program as the
first command-line argument. In Perl, it comes in separately as \f(CW$0.
Xref "command names"

- comment
Item "comment"
A remark that doesnXt affect the meaning of the program.
In Perl, a comment is introduced by a \f(CW\*(C`#\*(C' character and continues to the
end of the line.
Xref "comments, defined"

- compilation unit
Item "compilation unit"
The **file** (or **string**, in the case of \f(CW\*(C`eval\*(C') that
is currently being **compiled**.
Xref "compilation units"

- compile
Item "compile"
The process of turning source code into a machine-usable form. See \fBcompile
phase.

- compile phase
Item "compile phase"
Any time before Perl starts running your main
program. See also **run phase**. Compile phase is mostly spent in \fBcompile
time, but may also be spent in **runtime** when \f(CW\*(C`BEGIN\*(C' blocks, \f(CW\*(C`use\*(C' or
\f(CW\*(C`no\*(C' declarations, or constant subexpressions are being evaluated. The
startup and import code of any \f(CW\*(C`use\*(C' declaration is also run during
compile phase.
Xref "compile phase, defined"

- compiler
Item "compiler"
Strictly speaking, a program that munches
up another program and spits out yet another file containing the program in
a Xmore executableX form, typically containing native machine instructions.
The *perl* program is not a compiler by this definition, but it does
contain a kind of compiler that takes a program and turns it into a more
executable form (**syntax trees**) within the *perl* process itself, which
the **interpreter** then interprets. There are, however, extension **modules**
to get Perl to act more like a XrealX compiler. See Camel chapter 16,
XCompilingX.
Xref "compilers and compiling, about"

- compile time
Item "compile time"
The time when Perl is trying to make sense of your
code, as opposed to when it thinks it knows what your code means and is
merely trying to do what it thinks your code says to do, which is **runtime**.
Xref "compile time, defined"

- composer
Item "composer"
A XconstructorX for a **referent** that isnXt really an
**object**, like an anonymous array or a hash (or a sonata, for that matter).
For example, a pair of braces acts as a composer for a hash, and a pair of
brackets acts as a composer for an array. See the section XCreating
ReferencesX in Camel chapter 8, XReferencesX.
Xref "composers, about"

- concatenation
Item "concatenation"
The process of gluing one
catXs nose to another catXs tail. Also a similar operation on two
**strings**.
Xref "concatenating strings strings, concatenating"

- conditional
Item "conditional"
Something XiffyX. See **Boolean context**.
Xref "conditional (term)"

- connection
Item "connection"
In telephony, the temporary electrical circuit between
the callerXs and the calleeXs phone. In networking, the same kind of
temporary circuit between a **client** and a **server**.
Xref "connections (term)"

- construct
Item "construct"
As a noun, a piece of syntax made up of smaller
pieces. As a transitive verb, to create an **object** using a **constructor**.
Xref "constructs, defined"

- constructor
Item "constructor"
Any **class method**, **instance**, or **subroutine**
that composes, initializes, blesses, and returns an **object**. Sometimes we
use the term loosely to mean a **composer**.
Xref "constructors, defined"

- context
Item "context"
The surroundings or environment. The context given by the
surrounding code determines what kind of data a particular **expression** is
expected to return. The three primary contexts are **list context**,
**scalar**, and **void context**. Scalar context is sometimes subdivided into
**Boolean context**, **numeric context**, **string context**, and \fBvoid
context. ThereXs also a XdonXt careX context (which is dealt with in Camel
chapter 2, XBits and PiecesX, if you care).
Xref "context, about"

- continuation
Item "continuation"
The treatment of more than one physical **line** as a
single logical line. **Makefile** lines are continued by putting a backslash
before the **newline**. Mail headers, as defined by \s-1RFC 822,\s0 are
continued by putting a space or tab *after* the newline. In general, lines
in Perl do not need any form of continuation mark, because **whitespace**
(including newlines) is gleefully ignored. Usually.
Xref "continuation lines RFC 822"

- core dump
Item "core dump"
The corpse of a **process**, in the form of a file left in the
**working directory** of the process, usually as a result of certain kinds
of fatal errors.
Xref "core dump"

- \s-1CPAN\s0
Item "CPAN"
The Comprehensive Perl Archive Network. (See the Camel Preface
and Camel chapter 19, \s-1XCPANX\s0 for details.)
Xref "Comprehensive Perl Archive Network CPAN (Comprehensive Perl Archive Network), about"

- C preprocessor
Item "C preprocessor"
The typical C compilerXs first pass, which processes lines
beginning with \f(CW\*(C`#\*(C' for conditional compilation and macro definition, and
does various manipulations of the program text based on the current
definitions. Also known as *cpp*(1).
Xref "C preprocessor"

- cracker
Item "cracker"
Someone who breaks security on computer systems. A cracker may
be a true **hacker** or only a **script kiddie**.
Xref "crackers"

- currently selected output channel
Item "currently selected output channel"
The last **filehandle** that was
designated with \f(CW\*(C`select(FILEHANDLE)\*(C'; \f(CW\*(C`STDOUT\*(C', if no filehandle has
been selected.
Xref "currently selected output channel"

- current package
Item "current package"
The **package** in which the current statement is
**compiled**. Scan backward in the text of your program through the current
**lexical scope** or any enclosing lexical scopes until you find a package
declaration. ThatXs your current package name.
Xref "current package"

- current working directory
Item "current working directory"
See **working directory**.
Xref "current working directory"

- \s-1CV\s0
Item "CV"
In academia, a curriculum vit\*(ae, a fancy kind of re\*'sume\*'. In Perl, an internal Xcode valueX typedef holding a
**subroutine**. The \f(CW\*(C`CV\*(C' type is a subclass of **\s-1SV\s0**.
Xref "CV (code value) code value (CV)"

### D

Subsection "D"

- dangling statement
Item "dangling statement"
A bare, single **statement**,
without any braces, hanging off an \f(CW\*(C`if\*(C' or \f(CW\*(C`while\*(C' conditional. C allows
them. Perl doesnXt.
Xref "dangling statements statements, dangling"

- datagram
Item "datagram"
A packet of data, such as a **\s-1UDP\s0** message, that (from
the viewpoint of the programs involved) can be sent independently over the
network. (In fact, all packets are sent independently at the **\s-1IP\s0** level,
but **stream** protocols such as **\s-1TCP\s0** hide this from your program.)
Xref "datagrams, defined"

- data structure
Item "data structure"
How your various pieces of data relate to each
other and what shape they make when you put them all together, as in a
rectangular table or a triangular tree.
Xref "data structures, defined"

- data type
Item "data type"
A set of possible values, together with all the
operations that know how to deal with those values. For example, a numeric
data type has a certain set of numbers that you can work with, as well as
various mathematical operations that you can do on the numbers, but would
make little sense on, say, a string such as \f(CW"Kilroy". Strings have their
own operations, such as **concatenation**. Compound types made of a number of
smaller pieces generally have operations to compose and decompose them, and
perhaps to rearrange them. **Objects** that model things in the real world
often have operations that correspond to real activities. For instance, if
you model an elevator, your elevator object might have an \f(CW\*(C`open_door\*(C'
**method**.
Xref "data types, defined"

- \s-1DBM\s0
Item "DBM"
Stands for XDatabase ManagementX routines, a set of routines that emulate an
**associative array** using disk files. The routines use a dynamic hashing
scheme to locate any entry with only two disk accesses. \s-1DBM\s0 files allow a
Perl program to keep a persistent **hash** across multiple invocations. You
can \f(CW\*(C`tie\*(C' your hash variables to various \s-1DBM\s0 implementations.
Xref "DBM (Database Management) routines Database Management (DBM) routines"

- declaration
Item "declaration"
An **assertion** that states something exists and
perhaps describes what itXs like, without giving any commitment as to how
or where youXll use it. A declaration is like the part of your recipe that
says, Xtwo cups flour, one large egg, four or five tadpolesXX See
**statement** for its opposite. Note that some declarations also function
as statements. Subroutine declarations also act as definitions if a body
is supplied.
Xref "declarations, defined"

- declarator
Item "declarator"
Something that tells your program what sort of variable
youXd like. Perl doesnXt require you to declare variables, but you can use
\f(CW\*(C`my\*(C', \f(CW\*(C`our\*(C', or \f(CW\*(C`state\*(C' to denote that you want something other than
the default.
Xref "declarators"

- decrement
Item "decrement"
To subtract a value from a
variable, as in Xdecrement \f(CW$xX (meaning to remove 1 from its value) or
Xdecrement \f(CW$x by 3X.
Xref "decrementing values values, decrementing"

- default
Item "default"
A **value** chosen for you if you donXt
supply a value of your own.
Xref "default values values, default"

- defined
Item "defined"
Having a meaning. Perl thinks that some of the things
people try to do are devoid of meaning; in particular, making use of
variables that have never been given a **value** and performing certain
operations on data that isnXt there. For example, if you try to read data
past the end of a file, Perl will hand you back an undefined value. See also
**false** and the \f(CW\*(C`defined\*(C' entry in Camel chapter 27, XFunctionsX.
Xref "defined (term)"

- delimiter
Item "delimiter"
A **character** or **string** that sets bounds to an
arbitrarily sized textual object, not to be confused with a **separator** or
**terminator**. XTo delimitX really just means Xto surroundX or Xto encloseX
(like these parentheses are doing).
Xref "delimiters (term)"

- dereference
Item "dereference"
A fancy computer science term
meaning Xto follow a **reference** to what it points toX. The XdeX part of it
refers to the fact that youXre taking away one level of **indirection**.
Xref "dereference (term) references, dereference"

- derived class
Item "derived class"
A **class** that defines some of its **methods** in terms of a more generic class,
called a **base class**. Note that classes arenXt classified exclusively into
base classes or derived classes: a class can function as both a derived
class and a base class simultaneously, which is kind of classy.
Xref "derived classes classes, derived subclasses classes, subclasses"

- descriptor
Item "descriptor"
See **file descriptor**.

- destroy
Item "destroy"
To deallocate the memory of a **referent** (first triggering
its \f(CW\*(C`DESTROY\*(C' method, if it has one).
Xref "destroy (term)"

- destructor
Item "destructor"
A special **method** that is called
when an **object** is thinking about **destroying** itself. A Perl programXs
\f(CW\*(C`DESTROY\*(C' method doesnXt do the actual destruction; Perl just **triggers**
the method in case the **class** wants to do any associated cleanup.
Xref "destructor method methods, destructor"

- device
Item "device"
A whiz-bang hardware gizmo (like a disk or tape drive or a
modem or a joystick or a mouse) attached to your computer, which the
**operating system** tries to make look like a **file** (or a bunch of files).
Under Unix, these fake files tend to live in the */dev* directory.
Xref "devices (term)"

- directive
Item "directive"
A **pod** directive. See Camel chapter 23, XPlain Old
DocumentationX.
Xref "directives, defined"

- directory
Item "directory"
A special file that contains other files. Some
**operating systems** call these XfoldersX, XdrawersX, XcataloguesX, or
XcatalogsX.
Xref "directories, defined"

- directory handle
Item "directory handle"
A name that represents a particular instance of opening a
directory to read it, until you close it. See the \f(CW\*(C`opendir\*(C' function.
Xref "directory handle"

- discipline
Item "discipline"
Some people need this and some people avoid it.
For Perl, itXs an old way to say **I/O layer**.
Xref "discipline (I O layer)"

- dispatch
Item "dispatch"
To send something to its correct destination. Often used
metaphorically to indicate a transfer of programmatic control to a
destination selected algorithmically, often by lookup in a table of function
**references** or, in the case of object **methods**, by traversing the
inheritance tree looking for the most specific definition for the method.
Xref "dispatching"

- distribution
Item "distribution"
A standard, bundled release of a system of
software. The default usage implies source code is included. If that is not
the case, it will be called a Xbinary-onlyX distribution.
Xref "distributions, defined"

- dual-lived
Item "dual-lived"
Some modules live both in the
**Standard Library** and on **\s-1CPAN\s0**. These modules might be developed on two
tracks as people modify either version. The trend currently is to untangle
these situations.
Xref "dualXlived modules modules, dualXlived"

- dweomer
Item "dweomer"
An enchantment, illusion, phantasm, or jugglery. Said when PerlXs
magical **dwimmer** effects donXt do what you expect, but rather seem to be
the product of arcane *dweomercraft*, sorcery, or wonder working. [From
Middle English.]
Xref "dweomer"

- dwimmer
Item "dwimmer"
\s-1DWIM\s0 is
an acronym for XDo What I MeanX, the principle that something
should just do what you want it to do without an undue amount of fuss. A bit
of code that does XdwimmingX is a XdwimmerX. Dwimming can require a great
deal of behind-the-scenes magic, which (if it doesnXt stay properly behind
the scenes) is called a **dweomer** instead.
Xref "DWIM (Do What I Mean) principle Do What I Mean (DWIM) principle dwimming"

- dynamic scoping
Item "dynamic scoping"
Dynamic scoping works over a \fBdynamic
scope, making variables visible throughout the rest of the **block** in
which they are first used and in any **subroutines** that are called by the
rest of the block. Dynamically scoped variables can have their values
temporarily changed (and implicitly restored later) by a \f(CW\*(C`local\*(C' operator.
(Compare **lexical scoping**.) Used more loosely to mean how a subroutine
that is in the middle of calling another subroutine XcontainsX that
subroutine at **runtime**.
Xref "dynamic scope scopes, dynamic"

### E

Subsection "E"

- eclectic
Item "eclectic"
Derived from many sources. Some would say *too* many.
Xref "eclectic (term)"

- element
Item "element"
A basic building block. When youXre talking about an
**array**, itXs one of the items that make up the array.
Xref "elements, about"

- embedding
Item "embedding"
When something is contained in something else,
particularly when that might be considered surprising: XIXve embedded a
complete Perl interpreter in my editor!X
Xref "embedding (term)"

- empty subclass test
Item "empty subclass test"
The notion that an empty **derived class** should
behave exactly like its **base class**.
Xref "empty subclass test"

- encapsulation
Item "encapsulation"
The veil of abstraction separating the **interface**
from the **implementation** (whether enforced or not), which mandates that
all access to an **object**Xs state be through **methods** alone.
Xref "encapsulation (term)"

- endian
Item "endian"
See **little-endian** and **big-endian**.

- en passant
Item "en passant"
When you change a **value** as it is being copied. [From
French Xin passingX, as in the exotic pawn-capturing maneuver in chess.]
Xref "en passant (term)"

- environment
Item "environment"
The collective set of **environment variables** your
**process** inherits from its parent. Accessed via \f(CW%ENV.
Xref "environment (term)"

- environment variable
Item "environment variable"
A mechanism by which some high-level agent such as a user can pass its
preferences down to its future offspring (child **processes**, grandchild
processes, great-grandchild processes, and so on). Each environment
variable is a **key**/**value** pair, like one entry in a **hash**.
Xref "environment variables variables, environment environment variables"

- \s-1EOF\s0
Item "EOF"
End of File. Sometimes used
metaphorically as the terminating string of a **here document**.
Xref "End of File (EOF) EOF (End of File)"

- errno
Item "errno"
The error number returned by a
**syscall** when it fails. Perl refers to the error by the name \f(CW$! (or
\f(CW$OS_ERROR if you use the English module).
Xref "errno (error number) error number (errno)"

- error
Item "error"
See **exception** or **fatal error**.

- escape sequence
Item "escape sequence"
See **metasymbol**.

- exception
Item "exception"
A fancy term for an error. See **fatal error**.

- exception handling
Item "exception handling"
The way a program responds to an error. The
exception-handling mechanism in Perl is the \f(CW\*(C`eval\*(C' operator.
Xref "exception handling, defined"

- exec
Item "exec"
To throw away the current **process**Xs program and replace
it with another, without exiting the process or relinquishing any resources
held (apart from the old memory image).
Xref "exec function"

- executable file
Item "executable file"
A **file** that is specially marked to
tell the **operating system** that itXs okay to run this file as a program.
Usually shortened to XexecutableX.
Xref "executable files files, executable"

- execute
Item "execute"
To run a **program** or **subroutine**. (Has nothing to do
with the \f(CW\*(C`kill\*(C' built-in, unless youXre trying to run a **signal handler**.)
Xref "execute (term)"

- execute bit
Item "execute bit"
The special mark that tells the operating system it can run
this program. There are actually three execute bits under Unix, and which
bit gets used depends on whether you own the file singularly, collectively,
or not at all.
Xref "execute bit"

- exit status
Item "exit status"
See **status**.

- exploit
Item "exploit"
Used as a noun in this case, this refers to a known way
to compromise a program to get it to do something the author didnXt intend.
Your task is to write unexploitable programs.
Xref "exploits, security"

- export
Item "export"
To make symbols from a **module** available for
**import** by other modules.
Xref "exporting, defined"

- expression
Item "expression"
Anything you can legally say in a spot
where a **value** is required. Typically composed of **literals**,
**variables**, **operators**, **functions**, and **subroutine** calls, not
necessarily in that order.
Xref "expressions, defined expressions"

- extension
Item "extension"
A Perl module that also pulls in **compiled** C or \*(C+
code. More generally, any experimental option that can be **compiled** into
Perl, such as multithreading.
Xref "extensions, defined"

### F

Subsection "F"

- false
Item "false"
In Perl, any value that would look like \f(CW""
or \f(CW"0" if evaluated in a string context. Since undefined values evaluate
to \f(CW"", all undefined values are false, but not all false values are
undefined.
Xref "false values values, false"

- \s-1FAQ\s0
Item "FAQ"
Frequently Asked Question (although not necessarily
frequently answered, especially if the answer appears in the Perl \s-1FAQ\s0
shipped standard with Perl).
Xref "FAQ (Frequently Asked Question) Frequently Asked Question (FAQ)"

- fatal error
Item "fatal error"
An uncaught **exception**, which causes termination of the
**process** after printing a message on your **standard error** stream. Errors
that happen inside an \f(CW\*(C`eval\*(C' are not fatal. Instead, the \f(CW\*(C`eval\*(C' terminates
after placing the exception message in the \f(CW$@ (\f(CW$EVAL_ERROR) variable.
You can try to provoke a fatal error with the \f(CW\*(C`die\*(C' operator (known as
throwing or raising an exception), but this may be caught by a dynamically
enclosing \f(CW\*(C`eval\*(C'. If not caught, the \f(CW\*(C`die\*(C' becomes a fatal error.
Xref "fatal errors"

- feeping creaturism
Item "feeping creaturism"
A spoonerism of Xcreeping
featurismX, noting the biological urge to add just one more feature to
a program.
Xref "feeping creaturism creeping featurism"

- field
Item "field"
A single piece of numeric or string data that is part of a
longer **string**, **record**, or **line**. Variable-width fields are usually
split up by **separators** (so use \f(CW\*(C`split\*(C' to extract the fields), while
fixed-width fields are usually at fixed positions (so use \f(CW\*(C`unpack\*(C').
**Instance variables** are also known as XfieldsX.
Xref "fields (term)"

- \s-1FIFO\s0
Item "FIFO"
First In, First Out. See also **\s-1LIFO\s0**. Also a nickname for a **named pipe**.
Xref "First In, First Out (FIFO) FIFO (First In, First Out)"

- file
Item "file"
A named collection of data, usually stored on disk in a
**directory** in a **filesystem**. Roughly like a document, if youXre into
office metaphors. In modern filesystems, you can actually give a file more
than one name. Some files have special properties, like directories and
devices.
Xref "files, defined"

- file descriptor
Item "file descriptor"
The little number the \fBoperating
system uses to keep track of which opened **file** youXre talking about.
Perl hides the file descriptor inside a **standard I/O** stream and then
attaches the stream to a **filehandle**.
Xref "file descriptors descriptors, file"

- fileglob
Item "fileglob"
A XwildcardX match on **filenames**. See the \f(CW\*(C`glob\*(C' function.
Xref "fileglobs"

- filehandle
Item "filehandle"
An identifier (not necessarily related to the real
name of a file) that represents a particular instance of opening a file,
until you close it. If youXre going to open and close several different
files in succession, itXs fine to open each of them with the same
filehandle, so you donXt have to write out separate code to process each
file.
Xref "filehandles, about"

- filename
Item "filename"
One name for a file. This name is listed in a
**directory**. You can use it in an \f(CW\*(C`open\*(C' to tell the **operating system**
exactly which file you want to open, and associate the file with a
**filehandle**, which will carry the subsequent identity of that file in
your program, until you close it.
Xref "filenames, about"

- filesystem
Item "filesystem"
A set of **directories** and **files** residing on a
partition of the disk. Sometimes known as a XpartitionX. You can change the
fileXs name or even move a file around from directory to directory within a
filesystem without actually moving the file itself, at least under Unix.
Xref "filesystems, defined"

- file test operator
Item "file test operator"
A built-in unary operator that you use to
determine whether something is **true** about a file, such as \f(CW\*(C`Xo
$filename\*(C' to test whether youXre the owner of the file.
Xref "file test operators, about"

- filter
Item "filter"
A program designed to take a **stream** of input and
transform it into a stream of output.
Xref "filters, defined"

- first-come
Item "first-come"
The first **\s-1PAUSE\s0**
author to upload a **namespace** automatically becomes the \fBprimary
maintainer for that namespace. The Xfirst comeX permissions distinguish a
**primary maintainer** who was assigned that role from one who received it
automatically.
Xref "firstXcome permissions permissions, firstXcome"

- flag
Item "flag"
We tend to avoid this term because it means so many things.
It may mean a command-line **switch** that takes no argument itself (such as
PerlXs \f(CW\*(C`Xn\*(C' and \f(CW\*(C`Xp\*(C' flags) or, less frequently, a single-bit indicator
(such as the \f(CW\*(C`O_CREAT\*(C' and \f(CW\*(C`O_EXCL\*(C' flags used in \f(CW\*(C`sysopen\*(C'). Sometimes
informally used to refer to certain regex modifiers.
Xref "flags (term)"

- floating point
Item "floating point"
A method of storing
numbers in Xscientific notationX, such that the precision of the number is
independent of its magnitude (the decimal point XfloatsX). Perl does its
numeric work with floating-point numbers (sometimes called XfloatsX) when
it canXt get away with using **integers**. Floating-point numbers are mere
approximations of real numbers.
Xref "floating point methods methods, floating point"

- flush
Item "flush"
The act of emptying a **buffer**,
often before itXs full.
Xref "flushing buffers buffers, flushing"

- \s-1FMTEYEWTK\s0
Item "FMTEYEWTK"
Far More Than Everything You Ever Wanted To Know. An
exhaustive treatise on one narrow topic, something of a super-**\s-1FAQ\s0**. See
Tom for far more.
Xref "FMTEYEWTK acronym"

- foldcase
Item "foldcase"
The casemap used in Unicode when comparing or matching
without regard to case. Comparing lower-, title-, or uppercase are all
unreliable due to UnicodeXs complex, one-to-many case mappings. Foldcase is
a **lowercase** variant (using a partially decomposed **normalization** form
for certain codepoints) created specifically to resolve this.
Xref "foldcase (term)"

- fork
Item "fork"
To create a child **process**
identical to the parent process at its moment of conception, at least until
it gets ideas of its own. A thread with protected memory.
Xref "forking processes processes, forking"

- formal arguments
Item "formal arguments"
The generic names by which a
**subroutine** knows its **arguments**. In many languages, formal arguments
are always given individual names; in Perl, the formal arguments are just
the elements of an array. The formal arguments to a Perl program are
\f(CW$ARGV[0], \f(CW$ARGV[1], and so on. Similarly, the formal arguments to a
Perl subroutine are \f(CW$_[0], \f(CW$_[1], and so on. You may give the
arguments individual names by assigning the values to a \f(CW\*(C`my\*(C' list. See
also **actual arguments**.
Xref "formal arguments arguments, formal"

- format
Item "format"
A specification of how many spaces and digits and things
to put somewhere so that whatever youXre printing comes out nice and
pretty.
Xref "formats, defined"

- freely available
Item "freely available"
Means you donXt have to pay money to get it, but
the copyright on it may still belong to someone else (like Larry).
Xref "freely available (term)"

- freely redistributable
Item "freely redistributable"
Means youXre not in legal trouble if you
give a bootleg copy of it to your friends and we find out about it. In
fact, weXd rather you gave a copy to all your friends.
Xref "freely redistributable (term)"

- freeware
Item "freeware"
Historically, any software that you give away,
particularly if you make the source code available as well. Now often
called **open source software**. Recently there has been a trend to use the
term in contradistinction to **open source software**, to refer only to free
software released under the Free Software
FoundationXs \s-1GPL\s0 (General Public License), but this is difficult to justify
etymologically.
Xref "freeware (term) Free Software Foundation"

- function
Item "function"
Mathematically, a mapping of each of a set of input
values to a particular output value. In computers, refers to a
**subroutine** or **operator** that returns a **value**. It may or may not
have input values (called **arguments**).
Xref "functions, about"

- funny character
Item "funny character"
Someone like Larry, or one of his
peculiar friends. Also refers to the strange prefixes that Perl requires as
noun markers on its variables.
Xref "funny characters characters, funny"

### G

Subsection "G"

- garbage collection
Item "garbage collection"
A misnamed featureXit should be called,
Xexpecting your mother to pick up after youX. Strictly speaking, Perl
doesnXt do this, but it relies on a reference-counting mechanism to keep
things tidy. However, we rarely speak strictly and will often refer to the
reference-counting scheme as a form of garbage collection. (If itXs any
comfort, when your interpreter exits, a XrealX garbage collector runs to
make sure everything is cleaned up if youXve been messy with circular
references and such.)
Xref "garbage collection, defined"

- \s-1GID\s0
Item "GID"
Group IDXin Unix, the numeric group \s-1ID\s0
that the **operating system** uses to identify you and members of your
**group**.
Xref "GID (Group ID) Group ID (GID)"

- glob
Item "glob"
Strictly, the shellXs \f(CW\*(C`*\*(C' character, which will match
a XglobX of characters when youXre trying to generate a list of filenames.
Loosely, the act of using globs and similar symbols to do pattern matching.
See also **fileglob** and **typeglob**.
Xref "glob (* character)"

- global
Item "global"
Something you can see from anywhere, usually used of
**variables** and **subroutines** that are visible everywhere in your
program.  In Perl, only certain special variables are truly globalXmost
variables (and all subroutines) exist only in the current **package**.
Global variables can be declared with \f(CW\*(C`our\*(C'. See XGlobal DeclarationsX in
Camel chapter 4, XStatements and DeclarationsX.
Xref "global (term)"

- global destruction
Item "global destruction"
The **garbage collection** of globals (and the running
of any associated object destructors) that takes place when a Perl
**interpreter** is being shut down. Global destruction should not be
confused with the Apocalypse, except perhaps when it should.
Xref "global destruction"

- glue language
Item "glue language"
A language such as Perl that is good at hooking things
together that werenXt intended to be hooked together.
Xref "glue language"

- granularity
Item "granularity"
The size of the pieces youXre dealing with, mentally
speaking.
Xref "granularity"

- grapheme
Item "grapheme"
A graphene is an allotrope of carbon arranged in a
hexagonal crystal lattice one atom thick. A **grapheme**, or more fully, a
*grapheme cluster string* is a single user-visible **character**, which may
in turn be several characters (**codepoints**) long. For example, a carriage
return plus a line feed is a single grapheme but two characters, while a
\s-1XXX\s0 is a single grapheme but one, two, or even three characters, depending
on **normalization**.
Xref "graphemes, defined"

- greedy
Item "greedy"
A **subpattern** whose
**quantifier** wants to match as many things as possible.
Xref "greedy subpatterns subpatterns, greedy"

- grep
Item "grep"
Originally from the old Unix editor command for XGlobally
search for a Regular Expression and Print itX, now used in the general
sense of any kind of search, especially text searches. Perl has a built-in
\f(CW\*(C`grep\*(C' function that searches a list for elements matching any given
criterion, whereas the **grep**(1) program searches for lines matching a
**regular expression** in one or more files.
Xref "grep function"

- group
Item "group"
A set of users of which you are a member. In some
operating systems (like Unix), you can give certain file access permissions
to other members of your group.
Xref "groups, defined"

- \s-1GV\s0
Item "GV"
An internal Xglob valueX typedef,
holding a **typeglob**. The \f(CW\*(C`GV\*(C' type is a subclass of **\s-1SV\s0**.
Xref "GV (glob value) glob value (GV)"

### H

Subsection "H"

- hacker
Item "hacker"
Someone who is brilliantly persistent in solving technical
problems, whether these involve golfing, fighting orcs, or programming.
Hacker is a neutral term, morally speaking. Good hackers are not to be
confused with evil **crackers** or clueless **script kiddies**. If you
confuse them, we will presume that you are either evil or clueless.
Xref "hackers"

- handler
Item "handler"
A **subroutine** or **method** that Perl calls when your
program needs to respond to some internal event, such as a **signal**, or an
encounter with an operator subject to **operator overloading**. See also
**callback**.
Xref "handlers, defined"

- hard reference
Item "hard reference"
A **scalar** **value** containing
the actual address of a **referent**, such that the referentXs **reference**
count accounts for it. (Some hard references are held internally, such as
the implicit reference from one of a **typeglob**Xs variable slots to its
corresponding referent.) A hard reference is different from a \fBsymbolic
reference.
Xref "hard references, about references, hard"

- hash
Item "hash"
An unordered association of **key**/**value** pairs, stored such that you can easily use a string **key** to
look up its associated data **value**. This glossary is like a hash, where
the word to be defined is the key and the definition is the value. A hash
is also sometimes septisyllabically called an Xassociative arrayX, which is
a pretty good reason for simply calling it a XhashX instead.
Xref "hashes, about key value pairs, about"

- hash table
Item "hash table"
A data structure used internally by Perl for implementing
associative arrays (hashes) efficiently. See also **bucket**.
Xref "hash tables"

- header file
Item "header file"
A file containing certain required
definitions that you must include XaheadX of the rest of your program to do
certain obscure operations. A C header file has a *.h* extension. Perl
doesnXt really have header files, though historically Perl has sometimes
used translated *.h* files with a *.ph* extension. See \f(CW\*(C`require\*(C' in
Camel chapter 27, XFunctionsX. (Header files have been superseded by the
**module** mechanism.)
Xref "header files files, header"

- here document
Item "here document"
So called because of a similar construct in **shells** that
pretends that the **lines** following the **command** are a separate **file**
to be fed to the command, up to some terminating string. In Perl, however,
itXs just a fancy form of quoting.
Xref "here documents"

- hexadecimal
Item "hexadecimal"
A number in base 16, XhexX for short. The digits for 10
through 15 are customarily represented by the letters \f(CW\*(C`a\*(C' through \f(CW\*(C`f\*(C'.
Hexadecimal constants in Perl start with \f(CW\*(C`0x\*(C'. See also the \f(CW\*(C`hex\*(C'
function in Camel chapter 27, XFunctionsX.
Xref "hexadecimals"

- home directory
Item "home directory"
The directory you are put into when
you log in. On a Unix system, the name is often placed into \f(CW$ENV\{HOME\}
or \f(CW$ENV\{LOGDIR\} by *login*, but you can also find it with
\f(CW\*(C`(get\*(C'\f(CW\*(C`pwuid($<))[7]\*(C'. (Some platforms do not have a concept of a
home directory.)
Xref "home directory directories, home"

- host
Item "host"
The computer on which a program or other data resides.
Xref "host computers"

- hubris
Item "hubris"
Excessive pride, the sort of thing for which Zeus zaps
you.  Also the quality that makes you write (and maintain) programs that
other people wonXt want to say bad things about. Hence, the third great
virtue of a programmer. See also **laziness** and **impatience**.
Xref "hubris quality"

- \s-1HV\s0
Item "HV"
Short for a Xhash valueX typedef, which
holds PerlXs internal representation of a hash. The \f(CW\*(C`HV\*(C' type is a
subclass of **\s-1SV\s0**.
Xref "HV (hash value) hash value (HV)"

### I

Subsection "I"

- identifier
Item "identifier"
A legally formed name for most anything in which a
computer program might be interested. Many languages (including Perl) allow
identifiers to start with an alphabetic character, and then contain
alphabetics and digits. Perl also allows connector punctuation like the
underscore character wherever it allows alphabetics. (Perl also has more
complicated names, like **qualified** names.)
Xref "identifiers, defined"

- impatience
Item "impatience"
The anger you feel when the computer is being lazy.
This makes you write programs that donXt just react to your needs, but
actually anticipate them. Or at least that pretend to. Hence, the second
great virtue of a programmer. See also **laziness** and **hubris**.
Xref "impatience quality"

- implementation
Item "implementation"
How a piece of code actually goes about doing its
job. Users of the code should not count on implementation details staying
the same unless they are part of the published **interface**.
Xref "implementation (term)"

- import
Item "import"
To gain access to symbols that are exported from another
module. See \f(CW\*(C`use\*(C' in Camel chapter 27, XFunctionsX.
Xref "import (term)"

- increment
Item "increment"
To increase the value of
something by 1 (or by some other number, if so specified).
Xref "incrementing values values, incrementing"

- indexing
Item "indexing"
In olden days, the act of looking up a **key** in an
actual index (such as a phone book). But now it's merely the act of using
any kind of key or position to find the corresponding **value**, even if no
index is involved. Things have degenerated to the point that PerlXs
\f(CW\*(C`index\*(C' function merely locates the position (index) of one string in
another.
Xref "indexing (term)"

- indirect filehandle
Item "indirect filehandle"
An **expression** that
evaluates to something that can be used as a **filehandle**: a **string**
(filehandle name), a **typeglob**, a typeglob **reference**, or a low-level
**\s-1IO\s0** object.
Xref "indirect filehandles filehandles, indirect"

- indirection
Item "indirection"
If something in a program isnXt the value youXre
looking for but indicates where the value is, thatXs indirection. This can
be done with either **symbolic references** or **hard**.
Xref "indirection (term)"

- indirect object
Item "indirect object"
In English grammar, a short
noun phrase between a verb and its direct object indicating the beneficiary
or recipient of the action. In Perl, \f(CW\*(C`print STDOUT "$foo\\n";\*(C' can be
understood as Xverb indirect-object objectX, where \f(CW\*(C`STDOUT\*(C' is the
recipient of the \f(CW\*(C`print\*(C' action, and \f(CW"$foo" is the object being
printed.  Similarly, when invoking a **method**, you might place the
invocant in the dative slot between the method and its arguments:
Xref "indirect objects, defined objects, indirect"
.Sp
.Vb 3
    $gollum = new Pathetic::Creature "Sme\*'agol";
    give $gollum "Fisssssh!";
    give $gollum "Precious!";
.Ve

- indirect object slot
Item "indirect object slot"
The syntactic position falling between a method call
and its arguments when using the indirect object invocation syntax. (The
slot is distinguished by the absence of a comma between it and the next
argument.) \f(CW\*(C`STDERR\*(C' is in the indirect object slot here:
Xref "indirect object slot"
.Sp
.Vb 1
    print STDERR "Awake! Awake! Fear, Fire, Foes! Awake!\\n";
.Ve

- infix
Item "infix"
An **operator** that comes in between its **operands**,
such as multiplication in \f(CW\*(C`24 * 7\*(C'.
Xref "infix operators"

- inheritance
Item "inheritance"
What you get from your ancestors, genetically or
otherwise. If you happen to be a **class**, your ancestors are called \fBbase
classes and your descendants are called **derived classes**. See \fBsingle
inheritance and **multiple inheritance**.
Xref "inheritance, defined"

- instance
Item "instance"
Short for Xan instance of a classX, meaning an **object**
of that **class**.
Xref "instances (term)"

- instance data
Item "instance data"
See **instance variable**.
Xref "instance data"

- instance method
Item "instance method"
A **method** of an **object**, as
opposed to a **class method**.
Xref "instance methods methods, instance"
.Sp
A **method** whose **invocant** is an **object**, not a **package** name. Every
object of a class shares all the methods of that class, so an instance
method applies to all instances of the class, rather than applying to a
particular instance. Also see **class method**.

- instance variable
Item "instance variable"
An **attribute** of an **object**; data stored with the particular object rather than with the class
as a whole.
Xref "instance variables, defined variables, instance"

- integer
Item "integer"
A number with no fractional (decimal) part. A counting
number, like 1, 2, 3, and so on, but including 0 and the negatives.
Xref "integers (term)"

- interface
Item "interface"
The services a piece of code promises to provide
forever, in contrast to its **implementation**, which it should feel free to
change whenever it likes.
Xref "interfaces (term)"

- interpolation
Item "interpolation"
The insertion of a scalar or list value somewhere
in the middle of another value, such that it appears to have been there all
along. In Perl, variable interpolation happens in double-quoted strings and
patterns, and list interpolation occurs when constructing the list of
values to pass to a list operator or other such construct that takes a
*\f(CI\*(C`LIST\*(C'\fI*.
Xref "interpolation, defined"

- interpreter
Item "interpreter"
Strictly speaking, a program that reads a second
program and does what the second program says directly without turning the
program into a different form first, which is what **compilers** do. Perl is
not an interpreter by this definition, because it contains a kind of
compiler that takes a program and turns it into a more executable form
(**syntax trees**) within the *perl* process itself, which the Perl
**runtime** system then interprets.
Xref "interpreters, defined"

- invocant
Item "invocant"
The agent on whose behalf a **method** is invoked. In a
**class** method, the invocant is a package name. In an **instance** method,
the invocant is an object reference.
Xref "invocants, defined"

- invocation
Item "invocation"
The act of calling up a deity, daemon, program,
method, subroutine, or function to get it to do what you think itXs
supposed to do.  We usually XcallX subroutines but XinvokeX methods, since
it sounds cooler.
Xref "invocation, method"

- I/O
Item "I/O"
Input from, or output to, a **file** or **device**.
Xref "I O (Input Output), defined Input Output (I O), defined"

- \s-1IO\s0
Item "IO"
An internal I/O object. Can also mean **indirect object**.

- I/O layer
Item "I/O layer"
One of the filters between the data and what you get as input
or what you end up with as output.
Xref "I O layer"

- \s-1IPA\s0
Item "IPA"
India Pale Ale. Also the International Phonetic Alphabet, the
standard alphabet used for phonetic notation worldwide. Draws heavily on
Unicode, including many combining characters.
Xref "International Phonetic Alphabet (IPA) IPA (International Phonetic Alphabet)"

- \s-1IP\s0
Item "IP"
Internet Protocol, or
Intellectual
Property.
Xref "Internet Protocol (IP) IP (Internet Protocol) IP (Intellectual Property) Intellectual Property (IP)"

- \s-1IPC\s0
Item "IPC"
Interprocess Communication.
Xref "Interprocess Communication IPC (Interprocess Communication), about communication"

- is-a
Item "is-a"
A relationship between two **objects** in which one
object is considered to be a more specific version of the other, generic
object: \s-1XA\s0 camel is a mammal.X Since the generic object really only exists
in a Platonic sense, we usually add a little abstraction to the notion of
objects and think of the relationship as being between a generic \fBbase
class and a specific **derived class**. Oddly enough, Platonic classes
donXt always have Platonic relationshipsXsee **inheritance**.
Xref "isXa relationship"

- iteration
Item "iteration"
Doing something repeatedly.
Xref "iteration"

- iterator
Item "iterator"
A special programming gizmo that keeps track of where you are
in something that youXre trying to iterate over. The \f(CW\*(C`foreach\*(C' loop in
Perl contains an iterator; so does a hash, allowing you to \f(CW\*(C`each\*(C' through
it.
Xref "iterators"

- \s-1IV\s0
Item "IV"
The integer four, not to be
confused with six, TomXs favorite editor. \s-1IV\s0 also means an internal Integer
Value of the type a **scalar** can hold, not to be confused with an **\s-1NV\s0**.
Xref "IV (Integer Value) Integer Value (IV)"

### J

Subsection "J"

- \s-1JAPH\s0
Item "JAPH"
XJust Another Perl HackerX, a clever but cryptic bit of Perl
code that, when executed, evaluates to that string. Often used to
illustrate a particular Perl feature, and something of an ongoing
Obfuscated Perl Contest seen in \s-1USENET\s0 signatures.
Xref "JAPH acronym"

### K

Subsection "K"

- key
Item "key"
The string index to a **hash**, used to look up the **value**
associated with that key.
Xref "keys, defined"

- keyword
Item "keyword"
See **reserved words**.

### L

Subsection "L"

- label
Item "label"
A name you give to a **statement** so that you can talk
about that statement elsewhere in the program.
Xref "labels, defined"

- laziness
Item "laziness"
The quality that makes you go to great effort to reduce
overall energy expenditure. It makes you write labor-saving programs that
other people will find useful, and then document what you wrote so you
donXt have to answer so many questions about it. Hence, the first great
virtue of a programmer. Also hence, this book. See also **impatience** and
**hubris**.
Xref "laziness quality"

- leftmost longest
Item "leftmost longest"
The preference of the **regular expression** engine to match the
leftmost occurrence of a **pattern**, then given a position at which a match
will occur, the preference for the longest match (presuming the use of a
**greedy** quantifier). See Camel chapter 5, XPattern MatchingX for *much*
more on this subject.
Xref "leftmost longest preference regular expressions, leftmost longest preference"

- left shift
Item "left shift"
A **bit shift** that multiplies the
number by some power of 2.
Xref "left shift (<<) bit operator bitXshift operators, left shift << (left shift) bit operator"

- lexeme
Item "lexeme"
Fancy term for a **token**.
Xref "lexeme (token)"

- lexer
Item "lexer"
Fancy term for a **tokener**.
Xref "lexer (tokener)"

- lexical analysis
Item "lexical analysis"
Fancy term for **tokenizing**.
Xref "lexical analysis"

- lexical scoping
Item "lexical scoping"
Looking at your \fIOxford English
Dictionary through a microscope. (Also known as **static scoping**, because
dictionaries donXt change very fast.) Similarly, looking at variables
stored in a private dictionary (namespace) for each scope, which are
visible only from their point of declaration down to the end of the lexical scope in which they are declared. XSyn.
**static scoping**. XAnt. **dynamic scoping**.
Xref "lexical scopes, defined scopes static scopes scopes, static"

- lexical variable
Item "lexical variable"
A **variable** subject to
**lexical scoping**, declared by \f(CW\*(C`my\*(C'. Often just called a XlexicalX. (The
\f(CW\*(C`our\*(C' declaration declares a lexically scoped name for a global variable,
which is not itself a lexical variable.)
Xref "lexical variables, about variables, lexical"

- library
Item "library"
Generally, a collection of procedures. In ancient
days, referred to a collection of subroutines in a *.pl* file. In modern
times, refers more often to the entire collection of Perl **modules** on
your system.
Xref "libraries, defined"

- \s-1LIFO\s0
Item "LIFO"
Last In, First Out. See also **\s-1FIFO\s0**. A \s-1LIFO\s0 is usually called a
**stack**.
Xref "Last In, First Out (LIFO) LIFO (Last In, First Out) stacks, defined"

- line
Item "line"
In Unix, a sequence of zero or more nonnewline characters
terminated with a **newline** character. On non-Unix machines, this is
emulated by the C library even if the underlying **operating system** has
different ideas.
Xref "line (term)"

- linebreak
Item "linebreak"
A **grapheme** consisting of either a carriage return followed
by a line feed or any character with the Unicode Vertical Space \fBcharacter
property.
Xref "linebreaks"

- line buffering
Item "line buffering"
Used by a **standard I/O** output stream that
flushes its **buffer** after every **newline**. Many standard I/O libraries
automatically set up line buffering on output that is going to the terminal.
Xref "line buffering buffering, line"

- line number
Item "line number"
The number of lines read previous to this one, plus 1. Perl
keeps a separate line number for each source or input file it opens. The
current source fileXs line number is represented by \f(CW\*(C`_\|_LINE_\|_\*(C'. The
current input line number (for the file that was most recently read via
\f(CW\*(C`<FH>\*(C') is represented by the \f(CW$. (\f(CW$INPUT_LINE_NUMBER)
variable. Many error messages report both values, if available.
Xref "line number"

- link
Item "link"
Used as a noun, a name in a **directory** that represents a
**file**. A given file can have multiple links to it. ItXs like having the
same phone number listed in the phone directory under different names. As a
verb, to resolve a partially **compiled** fileXs unresolved symbols into a
(nearly) executable image. Linking can generally be static or dynamic,
which has nothing to do with static or dynamic scoping.
Xref "links, defined"

- \s-1LIST\s0
Item "LIST"
A syntactic construct representing a
comma- separated list of expressions, evaluated to produce a **list value**.
Each **expression** in a *\f(CI\*(C`LIST\*(C'\fI* is evaluated in **list context** and
interpolated into the list value.
Xref "LIST construct constructs, LIST"

- list
Item "list"
An ordered set of scalar values.
Xref "lists, defined"

- list context
Item "list context"
The situation in which an **expression** is
expected by its surroundings (the code calling it) to return a list of
values rather than a single value. Functions that want a *\f(CI\*(C`LIST\*(C'\fI* of
arguments tell those arguments that they should produce a list value. See
also **context**.
Xref "list context context, list"

- list operator
Item "list operator"
An **operator** that does something with a list of
values, such as \f(CW\*(C`join\*(C' or \f(CW\*(C`grep\*(C'. Usually used for named built-in
operators (such as \f(CW\*(C`print\*(C', \f(CW\*(C`unlink\*(C', and \f(CW\*(C`system\*(C') that do not require
parentheses around their **argument** list.
Xref "list operators, about"

- list value
Item "list value"
An unnamed list of temporary scalar
values that may be passed around within a program from any list-generating
function to any function or construct that provides a **list context**.
Xref "list values, about values, list"

- literal
Item "literal"
A token in a programming language, such as a number or
**string**, that gives you an actual **value** instead of merely representing
possible values as a **variable** does.
Xref "literals, defined"

- little-endian
Item "little-endian"
From Swift: someone
who eats eggs little end first. Also used of computers that store the least
significant **byte** of a word at a lower byte address than the most
significant byte. Often considered superior to big-endian machines. See
also **big-endian**.
Xref "littleXendian, defined endianness, littleXendian"

- local
Item "local"
Not meaning the same thing everywhere. A global
variable in Perl can be localized inside a **dynamic scope** via the
\f(CW\*(C`local\*(C' operator.
Xref "local operator, about"

- logical operator
Item "logical operator"
Symbols representing the concepts XandX, XorX,
XxorX, and XnotX.
Xref "logical operators, about"

- lookahead
Item "lookahead"
An **assertion** that peeks at the string to the right of the current match location.
Xref "lookahead assertions assertions (in regexes), lookahead"

- lookbehind
Item "lookbehind"
An **assertion** that peeks at the string to the left of the current match
location.
Xref "lookbehind assertions assertions (in regexes), lookbehind"

- loop
Item "loop"
A construct that
performs something repeatedly, like a roller coaster.
Xref "loop constructs and statements, about constructs, loop"

- loop control statement
Item "loop control statement"
Any statement within the body of a loop that can
make a loop prematurely stop looping or skip an **iteration**. Generally,
you shouldnXt try this on roller coasters.
Xref "statements, loop control"

- loop label
Item "loop label"
A kind of key or name attached to a loop (or
roller coaster) so that loop control statements can talk about which loop
they want to control.
Xref "loop labels labels, loop"

- lowercase
Item "lowercase"
In Unicode, not just
characters with the General Category of Lowercase Letter, but any character
with the Lowercase property, including Modifier Letters, Letter Numbers,
some Other Symbols, and one Combining Mark.
Xref "lowercase characters characters, lowercase"

- lvaluable
Item "lvaluable"
Able to serve as an **lvalue**.
Xref "lvaluable function functions, lvaluable"

- lvalue
Item "lvalue"
Term used by language lawyers for a
storage location you can assign a new **value** to, such as a **variable** or
an element of an **array**. The XlX is short for XleftX, as in the left side
of an assignment, a typical place for lvalues. An **lvaluable** function or
expression is one to which a value may be assigned, as in \f(CW\*(C`pos($x) = 10\*(C'.
Xref "lvalue (term) values, lvalue"

- lvalue modifier
Item "lvalue modifier"
An adjectival pseudofunction that
warps the meaning of an **lvalue** in some declarative fashion. Currently
there are three lvalue modifiers: \f(CW\*(C`my\*(C', \f(CW\*(C`our\*(C', and \f(CW\*(C`local\*(C'.
Xref "lvalue modifier modifiers, lvalue"

### M

Subsection "M"

- magic
Item "magic"
Technically speaking, any extra semantics attached to a
variable such as \f(CW$!, \f(CW$0, \f(CW%ENV, or \f(CW%SIG, or to any tied
variable.  Magical things happen when you diddle those variables.
Xref "magic (term)"

- magical increment
Item "magical increment"
An **increment** operator that knows how to
bump up \s-1ASCII\s0 alphabetics as well as numbers.
Xref "magical increment operator"

- magical variables
Item "magical variables"
Special variables that have side
effects when you access them or assign to them. For example, in Perl,
changing elements of the \f(CW%ENV array also changes the corresponding
environment variables that subprocesses will use. Reading the \f(CW$!
variable gives you the current system error number or message.
Xref "magical variables variables, magical"

- Makefile
Item "Makefile"
A file that controls the compilation of a program. Perl programs
donXt usually need a **Makefile** because the Perl compiler has plenty of
self-control.
Xref "Makefile"

- man
Item "man"
The Unix program that displays online documentation
(manual pages) for you.
Xref "man program (Unix)"

- manpage
Item "manpage"
A XpageX from the manuals, typically accessed via the
*man*(1) command. A manpage contains a \s-1SYNOPSIS,\s0 a \s-1DESCRIPTION,\s0 a list of
\s-1BUGS,\s0 and so on, and is typically longer than a page. There are manpages
documenting **commands**, **syscalls**, **library** **functions**, **devices**,
**protocols**, **files**, and such. In this book, we call any piece of
standard Perl documentation (like perlop or perldelta) a manpage, no
matter what format itXs installed in on your system.
Xref "manpages, defined"

- matching
Item "matching"
See **pattern matching**.
Xref "matching"

- member data
Item "member data"
See **instance variable**.
Xref "member data"

- memory
Item "memory"
This always means your main memory, not your disk.
Clouding the issue is the fact that your machine may implement
**virtual** memory; that is, it will pretend that it has more memory than
it really does, and itXll use disk space to hold inactive bits. This can
make it seem like you have a little more memory than you really do, but
itXs not a substitute for real memory. The best thing that can be said
about virtual memory is that it lets your performance degrade gradually
rather than suddenly when you run out of real memory. But your program
can die when you run out of virtual memory, tooXif you havenXt thrashed
your disk to death first.
Xref "memory, defined"

- metacharacter
Item "metacharacter"
A **character** that is *not* supposed to be treated normally. Which characters
are to be treated specially as metacharacters varies greatly from context to
context. Your **shell** will have certain metacharacters, double-quoted Perl
**strings** have other metacharacters,
and **regular expression** patterns have all the double-quote metacharacters plus
some extra ones of their own.
Xref "metacharacters, about characters, regex metacharacters regular expressions, metacharacters and"

- metasymbol
Item "metasymbol"
Something weXd call a
**metacharacter** except that itXs a sequence of more than one character.
Generally, the first character in the sequence must be a true metacharacter
to get the other characters in the metasymbol to misbehave along with it.
Xref "metasymbols, about escape sequences"

- method
Item "method"
A kind of action that an **object** can take if you tell
it to. See Camel chapter 12, XObjectsX.
Xref "methods, defined"

- method resolution order
Item "method resolution order"
The path Perl takes through \f(CW@INC. By default, this is a double depth first
search, once looking for defined methods and once for \f(CW\*(C`AUTOLOAD\*(C'. However,
Perl lets you configure this with \f(CW\*(C`mro\*(C'.
Xref "method resolution order (mro) mro (method resolution order)"

- minicpan
Item "minicpan"
A \s-1CPAN\s0 mirror that includes just the latest versions for each
distribution, probably created with \f(CW\*(C`CPAN::Mini\*(C'. See
Camel chapter 19, \s-1XCPANX.\s0
Xref "minicpan, defined CPAN (Comprehensive Perl Archive Network), minicpan and CPAN::Mini module"

- minimalism
Item "minimalism"
The belief that Xsmall is beautifulX. Paradoxically, if you
say something in a small language, it turns out big, and if you say it in a
big language, it turns out small. Go figure.
Xref "minimalism"

- mode
Item "mode"
In the context of the *stat*(2) syscall, refers to the field
holding the **permission bits** and the type of the **file**.
Xref "mode"

- modifier
Item "modifier"
See **statement modifier**, **regular expression**, and
**lvalue**, not necessarily in that order.
Xref "modifiers, defined"

- module
Item "module"
A **file** that defines a **package** of (almost) the same
name, which can either **export** symbols or function as an **object** class.
(A moduleXs main *.pm* file may also load in other files in support of the
module.) See the \f(CW\*(C`use\*(C' built-in.
Xref "modules, defined"

- modulus
Item "modulus"
An integer divisor when
youXre interested in the remainder instead of the quotient.
Xref "modulus (%) operator % (modulus) operator"

- mojibake
Item "mojibake"
When you speak one language and the computer thinks youXre
speaking another. YouXll see odd translations when you send \s-1UTFX8,\s0 for
instance, but the computer thinks you sent Latin-1, showing all sorts of
weird characters instead. The term is written XXXXXXin Japanese and
means Xcharacter rotX, an apt description. Pronounced [\f(CW\*(C`modXibake\*(C'] in
standard **\s-1IPA\s0** phonetics, or approximately Xmoh-jee-bah-kehX.
Xref "mojibake"

- monger
Item "monger"
Short for one member of **Perl mongers**, a
purveyor of Perl.
Xref "mongers, Perl Perl mongers"

- mortal
Item "mortal"
A temporary value scheduled to die when the
current statement finishes.
Xref "mortal value values, mortal"

- mro
Item "mro"
See **method resolution order**.

- multidimensional array
Item "multidimensional array"
An array with multiple
subscripts for finding a single element. Perl implements these using
**references**Xsee Camel chapter 9, XData StructuresX.
Xref "multidimensional arrays arrays, multidimensional"

- multiple inheritance
Item "multiple inheritance"
The features you got from
your mother and father, mixed together unpredictably. (See also
**inheritance** and **single inheritance**.) In computer languages (including
Perl), it is the notion that a given class may have multiple direct
ancestors or **base classes**.
Xref "multiple inheritance inheritance, multiple"

### N

Subsection "N"

- named pipe
Item "named pipe"
A **pipe** with a name embedded in the
**filesystem** so that it can be accessed by two unrelated **processes**.
Xref "named pipes pipes, names"

- namespace
Item "namespace"
A domain of names. You neednXt worry about whether the
names in one such domain have been used in another. See **package**.
Xref "namespaces, about"

- NaN
Item "NaN"
Not a number. The value Perl uses
for certain invalid or inexpressible floating-point operations.
Xref "NaN (not a number) not a number (NaN)"

- network address
Item "network address"
The most important attribute of a socket, like your
telephoneXs telephone number. Typically an \s-1IP\s0 address. See also **port**.
Xref "network address"

- newline
Item "newline"
A single character that
represents the end of a line, with the \s-1ASCII\s0 value of 012 octal under Unix
(but 015 on a Mac), and represented by \f(CW\*(C`\\n\*(C' in Perl strings. For Windows
machines writing text files, and for certain physical devices like
terminals, the single newline gets automatically translated by your C
library into a line feed and a carriage return, but normally, no
translation is done.
Xref "newline character characters, newline"

- \s-1NFS\s0
Item "NFS"
Network File System, which allows you to mount a remote filesystem as if it were local.
Xref "NFS (Network File System) Network File System (NFS)"

- normalization
Item "normalization"
Converting a text string into an alternate but equivalent
**canonical** (or compatible) representation that can then be compared for
equivalence. Unicode recognizes four different normalization forms: \s-1NFD,
NFC, NFKD,\s0 and \s-1NFKC.\s0
Xref "normalization"

- null character
Item "null character"
A character with the numeric value of
zero. ItXs used by C to terminate strings, but Perl allows strings to
contain a null.
Xref "null character characters, null"

- null list
Item "null list"
A **list value** with zero elements, represented
in Perl by \f(CW\*(C`()\*(C'.
Xref "null lists lists, null"

- null string
Item "null string"
A **string** containing no characters, not to
be confused with a string containing a **null character**, which has a
positive length and is **true**.
Xref "null strings strings, null"

- numeric context
Item "numeric context"
The situation in which an expression
is expected by its surroundings (the code calling it) to return a number.
See also **context** and **string context**.
Xref "numeric context context, numeric"

- numification
Item "numification"
(Sometimes spelled *nummification* and *nummify*.) Perl lingo
for implicit conversion into a number; the related verb is *numify*.
*Numification* is intended to rhyme with *mummification*, and *numify* with
*mummify*. It is unrelated to English *numen*, *numina*, *numinous*. We
originally forgot the extra *m* a long time ago, and some people got used to
our funny spelling, and so just as with \f(CW\*(C`HTTP_REFERER\*(C'Xs own missing letter,
our weird spelling has stuck around.
Xref "numification"

- \s-1NV\s0
Item "NV"
Short for Nevada, no part of
which will ever be confused with civilization. \s-1NV\s0 also means an internal
floating- point Numeric Value of the type a **scalar** can hold, not to be
confused with an **\s-1IV\s0**.
Xref "Numeric Value (NV) NV (Numeric Value)"

- nybble
Item "nybble"
Half a **byte**, equivalent to one **hexadecimal** digit, and worth
four **bits**.
Xref "nybble"

### O

Subsection "O"

- object
Item "object"
An **instance** of a **class**. Something that XknowsX
what user-defined type (class) it is, and what it can do because of what
class it is. Your program can request an object to do things, but the
object gets to decide whether it wants to do them or not. Some objects are
more accommodating than others.
Xref "objects, defined"

- octal
Item "octal"
A number in base 8. Only the digits 0 through 7 are allowed. Octal
constants in Perl start with 0, as in 013. See also the \f(CW\*(C`oct\*(C' function.
Xref "octals"

- offset
Item "offset"
How many things you have to skip
over when moving from the beginning of a string or array to a specific
position within it. Thus, the minimum offset is zero, not one, because you
donXt skip anything to get to the first item.
Xref "offsets in strings strings, offsets in"

- one-liner
Item "one-liner"
An entire computer program crammed into one line of
text.
Xref "oneXliner programs"

- open source software
Item "open source software"
Programs for which the source code is freely
available and freely redistributable, with no commercial strings attached.
For a more detailed definition, see <http://www.opensource.org/osd.html>.
Xref "open source software"

- operand
Item "operand"
An **expression** that yields a **value** that an
**operator** operates on. See also **precedence**.
Xref "operands (term)"

- operating system
Item "operating system"
A special program that runs on the bare
machine and hides the gory details of managing **processes** and **devices**.
Usually used in a looser sense to indicate a particular culture of
programming. The loose sense can be used at varying levels of specificity.
At one extreme, you might say that all versions of Unix and Unix-lookalikes
are the same operating system (upsetting many people, especially lawyers
and other advocates). At the other extreme, you could say this particular
version of this particular vendorXs operating system is different from any
other version of this or any other vendorXs operating system. Perl is much
more portable across operating systems than many other languages. See also
**architecture** and **platform**.
Xref "operating systems, defined"

- operator
Item "operator"
A gizmo that transforms some number of input values to
some number of output values, often built into a language with a special
syntax or symbol. A given operator may have specific expectations about
what **types** of data you give as its arguments (**operands**) and what type
of data you want back from it.
Xref "operators, about"

- operator overloading
Item "operator overloading"
A kind of
**overloading** that you can do on built-in **operators** to make them work
on **objects** as if the objects were ordinary scalar values, but with the
actual semantics supplied by the object class. This is set up with the
overload **pragma**Xsee Camel chapter 13, XOverloadingX.
Xref "operator overloading, about overloading, operator"

- options
Item "options"
See either **switches** or **regular expression modifiers**.
Xref "options"

- ordinal
Item "ordinal"
An abstract characterXs integer value. Same thing as
**codepoint**.
Xref "ordinals (term)"

- overloading
Item "overloading"
Giving additional meanings to a symbol or construct.
Actually, all languages do overloading to one extent or another, since
people are good at figuring out things from **context**.
Xref "overloading, defined"

- overriding
Item "overriding"
Hiding or invalidating some other definition of the
same name. (Not to be confused with **overloading**, which adds definitions
that must be disambiguated some other way.) To confuse the issue further,
we use the word with two overloaded definitions: to describe how you can
define your own **subroutine** to hide a built-in **function** of the same
name (see the section XOverriding Built-in FunctionsX in Camel chapter 11,
XModulesX), and to describe how you can define a replacement **method** in a
**derived class** to hide a **base class**Xs method of the same name (see
Camel chapter 12, XObjectsX).
Xref "overriding, defined"

- owner
Item "owner"
The one user (apart from the
superuser) who has absolute control over a **file**. A file may also have a
**group** of users who may exercise joint ownership if the real owner
permits it. See **permission bits**.
Xref "ownership, file files, ownership of"

### P

Subsection "P"

- package
Item "package"
A **namespace** for global **variables**, **subroutines**,
and the like, such that they can be kept separate from like-named
**symbols** in other namespaces. In a sense, only the package is global,
since the symbols in the packageXs symbol table are only accessible from
code **compiled** outside the package by naming the package. But in another
sense, all package symbols are also globalsXtheyXre just well-organized
globals.
Xref "packages, defined"

- pad
Item "pad"
Short for **scratchpad**.
Xref "pads (scratchpads)"

- parameter
Item "parameter"
See **argument**.
Xref "parameters"

- parent class
Item "parent class"
See **base class**.
Xref "parent classes classes, parent"

- parse tree
Item "parse tree"
See **syntax tree**.
Xref "parse tree"

- parsing
Item "parsing"
The subtle but sometimes brutal art of attempting to turn
your possibly malformed program into a valid **syntax tree**.
Xref "parsing, about"

- patch
Item "patch"
To fix by applying one, as it were. In the realm of hackerdom, a
listing of the differences between two versions of a program as might be
applied by the **patch**(1) program when you want to fix a bug or upgrade
your old version.
Xref "patches"

- \s-1PATH\s0
Item "PATH"
The list of
**directories** the system searches to find a program you want to
**execute**.  The list is stored as one of your **environment variables**,
accessible in Perl as \f(CW$ENV\{PATH\}.
Xref "PATH environment variable variables, environment"

- pathname
Item "pathname"
A fully qualified filename such as */usr/bin/perl*. Sometimes
confused with \f(CW\*(C`PATH\*(C'.
Xref "pathname"

- pattern
Item "pattern"
A template used in **pattern matching**.
Xref "patterns, defined"

- pattern matching
Item "pattern matching"
Taking a pattern, usually a \fBregular
expression, and trying the pattern various ways on a string to see whether
thereXs any way to make it fit. Often used to pick interesting tidbits out
of a file.
Xref "pattern matching, about"

- \s-1PAUSE\s0
Item "PAUSE"
The Perl Authors Upload SErver (<http://pause.perl.org>), the gateway
for **modules** on their way to **\s-1CPAN\s0**.
Xref "Perl Authors Upload SErver (PAUSE) PAUSE (Perl Authors Upload SErver)"

- Perl mongers
Item "Perl mongers"
A Perl user group, taking the form of its
name from the New York Perl mongers, the first Perl user group. Find one
near you at <http://www.pm.org>.
Xref "Perl mongers mongers, Perl"

- permission bits
Item "permission bits"
Bits that the **owner** of a file sets
or unsets to allow or disallow access to other people. These flag bits are
part of the **mode** word returned by the \f(CW\*(C`stat\*(C' built-in when you ask
about a file. On Unix systems, you can check the *ls*(1) manpage for more
information.
Xref "permission bits bits, permission"

- Pern
Item "Pern"
What you get when you do \f(CW\*(C`Perl++\*(C' twice. Doing it only once
will curl your hair. You have to increment it eight times to shampoo your
hair. Lather, rinse, iterate.
Xref "Pern (term)"

- pipe
Item "pipe"
A direct **connection** that carries the output of one
**process** to the input of another without an intermediate temporary file.
Once the pipe is set up, the two processes in question can read and write
as if they were talking to a normal file, with some caveats.
Xref "pipes, defined"

- pipeline
Item "pipeline"
A series of **processes** all in a row, linked by **pipes**, where
each passes its output stream to the next.
Xref "pipeline"

- platform
Item "platform"
The entire hardware and software context in which a
program runs. A program written in a platform-dependent language might
break if you change any of the following: machine, operating system,
libraries, compiler, or system configuration. The *perl* interpreter has
to be **compiled** differently for each platform because it is implemented
in C, but programs written in the Perl language are largely platform
independent.
Xref "platforms, defined"

- pod
Item "pod"
The markup
used to embed documentation into your Perl code. Pod stands for XPlain old
documentationX. See Camel chapter 23, XPlain Old DocumentationX.
Xref "pod (plain old documentation), about plain old documentation"

- pod command
Item "pod command"
A sequence, such as \f(CW\*(C`=head1\*(C', that denotes
the start of a **pod** section.
Xref "pod commands commands, pod"

- pointer
Item "pointer"
A **variable** in a language like C that contains the exact
memory location of some other item. Perl handles pointers internally so you
donXt have to worry about them. Instead, you just use symbolic pointers in
the form of **keys** and **variable** names, or **hard references**, which
arenXt pointers (but act like pointers and do in fact contain pointers).
Xref "pointers"

- polymorphism
Item "polymorphism"
The notion that you can tell an **object** to do something
generic, and the object will interpret the command in different ways
depending on its type. [< Greek \s-1XXXX- + XXXXX,\s0 many forms.]
Xref "polymorphism"

- port
Item "port"
The part of the address of a \s-1TCP\s0 or \s-1UDP\s0 socket that directs
packets to the correct process after finding the right machine, something
like the phone extension you give when you reach the company operator. Also
the result of converting code to run on a different platform than
originally intended, or the verb denoting this conversion.
Xref "ports (term)"

- portable
Item "portable"
Once upon a time, C code compilable under both \s-1BSD\s0 and
SysV. In general, code that can be easily converted to run on another
**platform**, where XeasilyX can be defined however you like, and usually
is.  Anything may be considered portable if you try hard enough, such as a
mobile home or London Bridge.
Xref "portability, about"

- porter
Item "porter"
Someone who XcarriesX software from one **platform** to another.
Porting programs written in platform-dependent languages such as C can be
difficult work, but porting programs like Perl is very much worth the
agony.
Xref "porters"

- possessive
Item "possessive"
Said of quantifiers and groups in patterns that refuse
to give up anything once theyXve gotten their mitts on it. Catchier and
easier to say than the even more formal *nonbacktrackable*.
Xref "possessive (term)"

- \s-1POSIX\s0
Item "POSIX"
The Portable Operating System Interface
specification.
Xref "Portable Operating System Interface (POSIX), about POSIX (Portable Operating System Interface), about"

- postfix
Item "postfix"
An **operator** that follows its **operand**, as in
\f(CW\*(C`$x++\*(C'.
Xref "postfix operator"

- pp
Item "pp"
An internal shorthand for a
Xpush- popX code; that is, C code implementing PerlXs stack machine.
Xref "pp (pushXpop) code pushXpop (pp) code"

- pragma
Item "pragma"
A standard module whose practical hints and
suggestions are received (and possibly ignored) at compile time. Pragmas
are named in all lowercase.
Xref "pragmas, about modules"

- precedence
Item "precedence"
The rules of
conduct that, in the absence of other guidance, determine what should
happen first.  For example, in the absence of parentheses, you always do
multiplication before addition.
Xref "precedence rules, about operators, precedence rules"

- prefix
Item "prefix"
An **operator** that precedes its **operand**, as in
\f(CW\*(C`++$x\*(C'.
Xref "prefix operators"

- preprocessing
Item "preprocessing"
What some helper **process** did to transform the incoming
data into a form more suitable for the current process. Often done with an
incoming **pipe**. See also **C preprocessor**.
Xref "preprocessing"

- primary maintainer
Item "primary maintainer"
The author that \s-1PAUSE\s0 allows to assign **co-maintainer**
permissions to a **namespace**. A primary maintainer can give up this
distinction by assigning it to another \s-1PAUSE\s0 author. See Camel chapter 19,
\s-1XCPANX.\s0
Xref "primary maintainer"

- procedure
Item "procedure"
A **subroutine**.
Xref "procedures, defined"

- process
Item "process"
An instance of a running program. Under multitasking
systems like Unix, two or more separate processes could be running the same
program independently at the same timeXin fact, the \f(CW\*(C`fork\*(C' function is
designed to bring about this happy state of affairs. Under other operating
systems, processes are sometimes called XthreadsX, XtasksX, or XjobsX,
often with slight nuances in meaning.
Xref "processes, defined"

- program
Item "program"
See **script**.

- program generator
Item "program generator"
A system that algorithmically writes code for you in a
high-level language. See also **code generator**.
Xref "program generators"

- progressive matching
Item "progressive matching"
**Pattern matching**  matching>that picks up where it left off before.
Xref "progressive matching pattern matching, progressive matching"

- property
Item "property"
See either **instance variable** or **character property**.
Xref "property"

- protocol
Item "protocol"
In networking, an agreed-upon way of sending messages
back and forth so that neither correspondent will get too confused.
Xref "protocols (term)"

- prototype
Item "prototype"
An optional part of a **subroutine** declaration telling
the Perl compiler how many and what flavor of arguments may be passed as
**actual arguments**, so you can write subroutine calls that parse much like
built-in functions. (Or donXt parse, as the case may be.)
Xref "prototypes, about"

- pseudofunction
Item "pseudofunction"
A construct that sometimes looks like a function but really
isnXt. Usually reserved for **lvalue** modifiers like \f(CW\*(C`my\*(C', for **context**
modifiers like \f(CW\*(C`scalar\*(C', and for the pick-your-own-quotes constructs,
\f(CW\*(C`q//\*(C', \f(CW\*(C`qq//\*(C', \f(CW\*(C`qx//\*(C', \f(CW\*(C`qw//\*(C', \f(CW\*(C`qr//\*(C', \f(CW\*(C`m//\*(C', \f(CW\*(C`s///\*(C', \f(CW\*(C`y///\*(C', and
\f(CW\*(C`tr///\*(C'.
Xref "pseudofunctions constructs, pseudofunctions functions, pseudofunctions"

- pseudohash
Item "pseudohash"
Formerly, a reference to an array
whose initial element happens to hold a reference to a hash. You used to be
able to treat a pseudohash reference as either an array reference or a hash
reference. Pseudohashes are no longer supported.
Xref "pseudohashes hashes, pseudohashes"

- pseudoliteral
Item "pseudoliteral"
An **operator** X\f(CW\*(C`that looks something like a \f(CBliteral\f(CW,
such as the output-grabbing operator, <literal
moreinfo="none"\*(C'`>*\f(CI\*(C`command\*(C'**\f(CW\*(C`\`\*(C'*.
Xref "pseudoliterals"

- public domain
Item "public domain"
Something not owned by anybody. Perl is copyrighted and is
thus *not* in the public domainXitXs just **freely available** and \fBfreely
redistributable.
Xref "public domain"

- pumpkin
Item "pumpkin"
A notional XbatonX handed around the Perl community
indicating who is the lead integrator in some arena of development.
Xref "pumpkin (term)"

- pumpking
Item "pumpking"
A **pumpkin** holder, the person in charge of pumping the pump,
or at least priming it. Must be willing to play the part of the Great
Pumpkin now and then.
Xref "pumpking"

- \s-1PV\s0
Item "PV"
A Xpointer valueX, which is Perl
Internals Talk for a \f(CW\*(C`char*\*(C'.
Xref "PV (pointer value) pointer value (PV)"

### Q

Subsection "Q"

- qualified
Item "qualified"
Possessing a complete name. The symbol \f(CW$Ent::moot is
qualified; \f(CW$moot is unqualified. A fully qualified filename is specified
from the top-level directory.
Xref "qualified (term)"

- quantifier
Item "quantifier"
A component of a **regular expression** specifying how
many times the foregoing **atom** may occur.
Xref "quantifiers, about"

### R

Subsection "R"

- race condition
Item "race condition"
A race condition exists when the result of
several interrelated events depends on the ordering of those events, but
that order cannot be guaranteed due to nondeterministic timing effects. If
two or more programs, or parts of the same program, try to go through the
same series of events, one might interrupt the work of the other. This is a
good way to find an **exploit**.
Xref "race conditions, defined"

- readable
Item "readable"
With respect to files, one that has the proper permission
bit set to let you access the file. With respect to computer programs, one
thatXs written well enough that someone has a chance of figuring out what
itXs trying to do.
Xref "readable (term)"

- reaping
Item "reaping"
The last rites performed by a parent **process**
on behalf of a deceased child process so that it doesnXt remain a
**zombie**.  See the \f(CW\*(C`wait\*(C' and \f(CW\*(C`waitpid\*(C' function calls.
Xref "reaping zombie processes"

- record
Item "record"
A set of related data values in a **file** or **stream**,
often associated with a unique **key** field. In Unix, often commensurate
with a **line**, or a blank-lineXterminated set of lines (a XparagraphX).
Each line of the */etc/passwd* file is a record, keyed on login name,
containing information about that user.
Xref "records, defined"

- recursion
Item "recursion"
The art of defining something (at least partly) in
terms of itself, which is a naughty no-no in dictionaries but often works
out okay in computer programs if youXre careful not to recurse forever
(which is like an infinite loop with more spectacular failure modes).
Xref "recursion, defined"

- reference
Item "reference"
Where you look to find a pointer to information
somewhere else. (See **indirection**.) References come in two flavors:
**symbolic references** and **hard references**.
Xref "references, about"

- referent
Item "referent"
Whatever a reference refers to, which may or may not
have a name. Common types of referents include scalars, arrays, hashes, and
subroutines.
Xref "referents, defined"

- regex
Item "regex"
See **regular expression**.

- regular expression
Item "regular expression"
A single entity with various
interpretations, like an elephant. To a computer scientist, itXs a grammar
for a little language in which some strings are legal and others arenXt. To
normal people, itXs a pattern you can use to find what youXre looking for
when it varies from case to case. PerlXs regular expressions are far from
regular in the theoretical sense, but in regular use they work quite well.
HereXs a regular expression: \f(CW\*(C`/Oh s.*t./\*(C'. This will match strings like
X\f(CW\*(C`Oh say can you see by the dawn\*(Aqs early light\*(C'X and X\f(CW\*(C`Oh sit!\*(C'X. See
Camel chapter 5, XPattern MatchingX.
Xref "regular expressions, defined"

- regular expression modifier
Item "regular expression modifier"
An option on a pattern or substitution, such as \f(CW\*(C`/i\*(C' to render the pattern
case- insensitive.
Xref "regular expression modifiers modifiers, regular expression"

- regular file
Item "regular file"
A **file** thatXs not a **directory**, a
**device**, a named **pipe** or **socket**, or a **symbolic link**. Perl uses
the \f(CW\*(C`Xf\*(C' file test operator to identify regular files. Sometimes called a
XplainX file.
Xref "regular files files, regular"

- relational operator
Item "relational operator"
An **operator** that says whether a particular
ordering relationship is **true** about a pair of **operands**. Perl has both
numeric and string relational operators. See **collating sequence**.
Xref "relational operators"

- reserved words
Item "reserved words"
A word with a specific, built-in meaning
to a **compiler**, such as \f(CW\*(C`if\*(C' or \f(CW\*(C`delete\*(C'. In many languages (not Perl),
itXs illegal to use reserved words to name anything else. (Which is why
theyXre reserved, after all.) In Perl, you just canXt use them to name
**labels** or **filehandles**. Also called XkeywordsX.
Xref "reserved words keywords (term)"

- return value
Item "return value"
The **value** produced by a **subroutine**
or **expression** when evaluated. In Perl, a return value may be either a
**list** or a **scalar**.
Xref "return values values, return"

- \s-1RFC\s0
Item "RFC"
Request For Comment, which despite the timid connotations is the name of a series of
important standards documents.
Xref "Request For Comment (RFC) RFC (Request For Comment)"

- right shift
Item "right shift"
A **bit shift** that divides
a number by some power of 2.
Xref "right shift (>>) bit operator bitXshift operators, right shift >> (right shift) bit operator"

- role
Item "role"
A name for a concrete set of behaviors. A role is a way to
add behavior to a class without inheritance.
Xref "roles (term)"

- root
Item "root"
The superuser (\f(CW\*(C`UID\*(C' == 0). Also the top-level directory of
the filesystem.
Xref "root (term)"

- \s-1RTFM\s0
Item "RTFM"
What you are told when someone thinks you should Read The
Fine Manual.
Xref "RTFM acronym"

- run phase
Item "run phase"
Any time after Perl starts running your main program.
See also **compile phase**. Run phase is mostly spent in **runtime** but may
also be spent in **compile time** when \f(CW\*(C`require\*(C', \f(CW\*(C`do\*(C' *\f(CI\*(C`FILE\*(C'\fI*, or
\f(CW\*(C`eval\*(C' *\f(CI\*(C`STRING\*(C'\fI* operators are executed, or when a substitution uses
the \f(CW\*(C`/ee\*(C' modifier.
Xref "run phase, defined"

- runtime
Item "runtime"
The time when Perl is actually doing what your
code says to do, as opposed to the earlier period of time when it was
trying to figure out whether what you said made any sense whatsoever, which
is **compile time**.
Xref "runtime (term), defined"

- runtime pattern
Item "runtime pattern"
A pattern that contains one or more
variables to be interpolated before parsing the pattern as a \fBregular
expression, and that therefore cannot be analyzed at compile time, but
must be reanalyzed each time the pattern match operator is evaluated.
Runtime patterns are useful but expensive.
Xref "runtime patterns patterns, runtime"

- \s-1RV\s0
Item "RV"
A recreational vehicle, not
to be confused with vehicular recreation. \s-1RV\s0 also means an internal
Reference Value of the type a **scalar** can hold. See also **\s-1IV\s0** and **\s-1NV\s0**
if youXre not confused yet.
Xref "Reference Value (RV) RV (Reference Value)"

- rvalue
Item "rvalue"
A **value** that you might find on the
right side of an **assignment**. See also **lvalue**.
Xref "rvalue (term) values, rvalue"

### S

Subsection "S"

- sandbox
Item "sandbox"
A walled off area thatXs not supposed to affect beyond
its walls. You let kids play in the sandbox instead of running in the road.
See Camel chapter 20, XSecurityX.
Xref "sandbox, defined"

- scalar
Item "scalar"
A simple, singular value; a number, **string**, or
**reference**.
Xref "scalars, defined"

- scalar context
Item "scalar context"
The situation in which an
**expression** is expected by its surroundings (the code calling it) to
return a single **value** rather than a **list** of values. See also
**context** and **list context**. A scalar context sometimes imposes
additional constraints on the return valueXsee **string context** and
**numeric context**. Sometimes we talk about a **Boolean context** inside
conditionals, but this imposes no additional constraints, since any scalar
value, whether numeric or **string**, is already true or false.
Xref "scalar context, about context, scalar"

- scalar literal
Item "scalar literal"
A number or quoted **string**Xan actual
**value** in the text of your program, as opposed to a **variable**.
Xref "scalar literals literals, scalar"

- scalar value
Item "scalar value"
A value that happens to be a
**scalar** as opposed to a **list**.
Xref "scalar values, about values, scalar SV"

- scalar variable
Item "scalar variable"
A **variable** prefixed with
\f(CW\*(C`$\*(C' that holds a single value.
Xref "scalar variables, defined variables, scalar"

- scope
Item "scope"
From how far away you can see a variable, looking through
one. Perl has two visibility mechanisms. It does **dynamic scoping** of
\f(CW\*(C`local\*(C' **variables**, meaning that the rest of the **block**, and any
**subroutines** that are called by the rest of the block, can see the
variables that are local to the block. Perl does **lexical scoping** of
\f(CW\*(C`my\*(C' variables, meaning that the rest of the block can see the variable,
but other subroutines called by the block *cannot* see the variable.
Xref "scopes, defined"

- scratchpad
Item "scratchpad"
The area in which a particular invocation of a particular
file or subroutine keeps some of its temporary values, including any
lexically scoped variables.
Xref "scratchpads"

- script
Item "script"
A text **file** that is a program
intended to be **executed** directly rather than **compiled** to another form
of file before **execution**.
Xref "scripts (term) programs, defined"
.Sp
Also, in the context of **Unicode**, a writing system for a particular
language or group of languages, such as Greek, Bengali, or Tengwar.

- script kiddie
Item "script kiddie"
A **cracker** who is not a **hacker** but knows just enough
to run canned scripts. A **cargo-cult** programmer.
Xref "script kiddie"

- sed
Item "sed"
A venerable Stream EDitor from
which Perl derives some of its ideas.
Xref "sed (Stream EDitor) Stream EDitor (sed)"

- semaphore
Item "semaphore"
A fancy kind of interlock that prevents multiple **threads** or
**processes** from using up the same resources simultaneously.
Xref "semaphore"

- separator
Item "separator"
A **character** or **string** that keeps two surrounding strings from being
confused with each other. The \f(CW\*(C`split\*(C' function works on separators. Not to be confused with **delimiters**
or **terminators**. The XorX in the previous sentence separated the two
alternatives.
Xref "separators characters, separators strings, separators split function, separators and"

- serialization
Item "serialization"
Putting a fancy **data structure** into
linear order so that it can be stored as a **string** in a disk file or
database, or sent through a **pipe**. Also called marshalling.
Xref "serialization marshalling (term)"

- server
Item "server"
In networking, a **process** that
either advertises a **service** or just hangs around at a known location and
waits for **clients** who need service to get in touch with it.
Xref "servers, defined processes, server"

- service
Item "service"
Something you do for someone else to make them happy,
like giving them the time of day (or of their life). On some machines,
well-known services are listed by the \f(CW\*(C`getservent\*(C'
function.
Xref "services (term) getservent function"

- setgid
Item "setgid"
Same as **setuid**, only having to do with giving
away **group** privileges.
Xref "setgid program, about"

- setuid
Item "setuid"
Said of a program that runs with the privileges of
its **owner** rather than (as is usually the case) the privileges of whoever
is running it. Also describes the bit in the mode word (**permission bits**)
that controls the feature. This bit must be explicitly set by the owner to
enable this feature, and the program must be carefully written not to give
away more privileges than it ought to.
Xref "setuid program, about"

- shared memory
Item "shared memory"
A piece of **memory** accessible by two
different **processes** who otherwise would not see each otherXs memory.
Xref "shared memory memory, shared"

- shebang
Item "shebang"
Irish for the whole McGillicuddy. In Perl culture, a
portmanteau of XsharpX and XbangX, meaning the \f(CW\*(C`#!\*(C' sequence that tells
the system where to find the interpreter.
Xref "shebang (term)"

- shell
Item "shell"
A **command**-line **interpreter**. The program that
interactively gives you a prompt, accepts one or more **lines** of input,
and executes the programs you mentioned, feeding each of them their proper
**arguments** and input data. Shells can also execute scripts containing
such commands. Under Unix, typical shells include the Bourne shell
(*/bin/sh*), the C shell (*/bin/csh*), and the Korn shell (*/bin/ksh*).
Perl is not strictly a shell because itXs not interactive (although Perl
programs can be interactive).
Xref "shell program, defined"

- side effects
Item "side effects"
Something extra that happens when you evaluate an
**expression**. Nowadays it can refer to almost anything. For example,
evaluating a simple assignment statement typically has the Xside effectX of
assigning a value to a variable. (And you thought assigning the value was
your primary intent in the first place!) Likewise, assigning a value to the
special variable \f(CW$| (\f(CW$AUTOFLUSH) has the side effect of forcing a
flush after every \f(CW\*(C`write\*(C' or \f(CW\*(C`print\*(C' on the currently selected
filehandle.
Xref "side effects"

- sigil
Item "sigil"
A glyph used in magic. Or, for Perl, the symbol in front
of a variable name, such as \f(CW\*(C`$\*(C', \f(CW\*(C`@\*(C', and \f(CW\*(C`%\*(C'.
Xref "sigils, defined"

- signal
Item "signal"
A bolt out of the blue; that is, an
event triggered by the **operating system**, probably when youXre least
expecting it.
Xref "signals and signal handling, about"

- signal handler
Item "signal handler"
A **subroutine** that, instead of being content to be
called in the normal fashion, sits around waiting for a bolt out of the
blue before it will deign to **execute**. Under Perl, bolts out of the blue
are called signals, and you send them with the \f(CW\*(C`kill\*(C' built-in. See the
\f(CW%SIG hash in Camel chapter 25, XSpecial NamesX and the section XSignalsX
in Camel chapter 15, XInterprocess CommunicationX.
Xref "handlers, signal"

- single inheritance
Item "single inheritance"
The features you got from your
mother, if she told you that you donXt have a father. (See also
**inheritance** and **multiple inheritance**.) In computer languages, the
idea that **classes** reproduce asexually so that a given class can only
have one direct ancestor or **base class**. Perl supplies no such
restriction, though you may certainly program Perl that way if you like.
Xref "single inheritance inheritance, single"

- slice
Item "slice"
A selection of any number of
**elements** from a **list**, **array**, or **hash**.
Xref "slices of elements elements, slices of"

- slurp
Item "slurp"
To read an entire **file** into a **string** in one operation.
Xref "slurp (term)"

- socket
Item "socket"
An endpoint for network communication among multiple
**processes** that works much like a telephone or a post office box. The
most important thing about a socket is its **network address** (like a phone
number). Different kinds of sockets have different kinds of addressesXsome
look like filenames, and some donXt.
Xref "sockets, defined"

- soft reference
Item "soft reference"
See **symbolic reference**.
Xref "soft references references, soft"

- source filter
Item "source filter"
A special kind of **module** that does
**preprocessing** on your script just before it gets to the **tokener**.
Xref "source filters filters, source"

- stack
Item "stack"
A device you can put things on the top of, and later take
them back off in the opposite order in which you put them on. See **\s-1LIFO\s0**.
Xref "stacks, defined"

- standard
Item "standard"
Included in the official Perl distribution, as in a
standard module, a standard tool, or a standard Perl **manpage**.
Xref "standard (term)"

- standard error
Item "standard error"
The default output **stream** for nasty remarks that donXt belong in
**standard output**. Represented within a Perl program by the output>  **filehandle** \f(CW\*(C`STDERR\*(C'. You can use this
stream explicitly, but the \f(CW\*(C`die\*(C' and \f(CW\*(C`warn\*(C' built-ins write to your
standard error stream automatically (unless trapped or otherwise
intercepted).
Xref "STDERR filehandle, about"

- standard input
Item "standard input"
The default input **stream** for your program,
which if possible shouldnXt care where its data is coming from. Represented
within a Perl program by the **filehandle** \f(CW\*(C`STDIN\*(C'.
Xref "STDIN filehandle, about"

- standard I/O
Item "standard I/O"
A standard C library for doing **buffered** input
and output to the **operating system**. (The XstandardX of standard I/O is
at most marginally related to the XstandardX of standard input and output.)
In general, Perl relies on whatever implementation of standard I/O a given
operating system supplies, so the buffering characteristics of a Perl
program on one machine may not exactly match those on another machine.
Normally this only influences efficiency, not semantics. If your standard
I/O package is doing block buffering and you want it to **flush** the buffer
more often, just set the \f(CW$| variable to a true value.
Xref "standard I O I O (Input Output), standard Input Output (I O), standard STDIO filehandle"

- Standard Library
Item "Standard Library"
Everything that comes with the official
*perl* distribution. Some vendor versions of *perl* change their
distributions, leaving out some parts or including extras. See also
**dual-lived**.
Xref "Standard Perl Library, about"

- standard output
Item "standard output"
The default output **stream** for your program,
which if possible shouldnXt care where its data is going. Represented
within a Perl program by the **filehandle** \f(CW\*(C`STDOUT\*(C'.
Xref "STDOUT filehandle, about"

- statement
Item "statement"
A **command** to the computer about what to do next,
like a step in a recipe: XAdd marmalade to batter and mix until mixed.X A
statement is distinguished from a **declaration**, which doesnXt tell the
computer to do anything, but just to learn something.
Xref "statements, about"

- statement modifier
Item "statement modifier"
A **conditional** or
**loop** that you put after the **statement** instead of before, if you know
what we mean.
Xref "statement modifiers, about modifiers, statement"

- static
Item "static"
Varying slowly compared to something else. (Unfortunately,
everything is relatively stable compared to something else, except for
certain elementary particles, and weXre not so sure about them.) In
computers, where things are supposed to vary rapidly, XstaticX has a
derogatory connotation, indicating a slightly dysfunctional **variable**,
**subroutine**, or **method**. In Perl culture, the word is politely avoided.
Xref "static (term)"
.Sp
If youXre a C or \*(C+ programmer, you might be looking for PerlXs \f(CW\*(C`state\*(C'
keyword.

- static method
Item "static method"
No such thing. See **class method**.
Xref "static methods methods, static"

- static scoping
Item "static scoping"
No such thing. See **lexical scoping**.

- static variable
Item "static variable"
No such thing. Just use a \fBlexical
variable in a scope larger than your **subroutine**, or declare it with
\f(CW\*(C`state\*(C' instead of with \f(CW\*(C`my\*(C'.
Xref "static variables variables, static"

- stat structure
Item "stat structure"
A special internal spot
in which Perl keeps the information about the last **file** on which you
requested information.
Xref "stat structure data structures, stat structure"

- status
Item "status"
The **value** returned to the
parent **process** when one of its child processes dies. This value is
placed in the special variable \f(CW$?. Its upper eight **bits** are the exit
status of the defunct process, and its lower eight bits identify the signal
(if any) that the process died from. On Unix systems, this status value is
the same as the status word returned by *wait*(2). See \f(CW\*(C`system\*(C' in Camel
chapter 27, XFunctionsX.
Xref "status value values, status exit status"

- \s-1STDERR\s0
Item "STDERR"
See **standard error**.

- \s-1STDIN\s0
Item "STDIN"
See **standard input**.

- \s-1STDIO\s0
Item "STDIO"
See **standard I/O**.

- \s-1STDOUT\s0
Item "STDOUT"
See **standard output**.

- stream
Item "stream"
A flow of data into or out of
a process as a steady sequence of bytes or characters, without the
appearance of being broken up into packets. This is a kind of
**interface**Xthe underlying **implementation** may well break your data up
into separate packets for delivery, but this is hidden from you.
Xref "streaming data processes, streaming data"

- string
Item "string"
A sequence of characters such as XHe said !@#*&%@#*?!X.
A string does not have to be entirely printable.
Xref "strings, defined"

- string context
Item "string context"
The situation in which an expression is
expected by its surroundings (the code calling it) to return a **string**.
See also **context** and **numeric context**.
Xref "string context context, string"

- stringification
Item "stringification"
The process of producing a **string** representation of an
abstract object.
Xref "stringification"

- struct
Item "struct"
C keyword introducing a structure definition or name.
Xref "struct keyword"

- structure
Item "structure"
See **data structure**.
Xref "structures"

- subclass
Item "subclass"
See **derived class**.

- subpattern
Item "subpattern"
A component of a **regular expression** pattern.
Xref "subpatterns, defined"

- subroutine
Item "subroutine"
A named or otherwise accessible piece of program
that can be invoked from elsewhere in the program in order to accomplish
some subgoal of the program. A subroutine is often parameterized to
accomplish different but related things depending on its input
**arguments**. If the subroutine returns a meaningful **value**, it is also
called a **function**.
Xref "subroutines, defined"

- subscript
Item "subscript"
A **value** that indicates the position of a particular
**array** **element** in an array.
Xref "subscripts"

- substitution
Item "substitution"
Changing parts of a string via the \f(CW\*(C`s///\*(C'
operator. (We avoid use of this term to mean **variable interpolation**.)
Xref "substitution (s ) operator, about strings, substitution in s (substitution) operator, about"

- substring
Item "substring"
A portion of a **string**, starting at a certain
**character** position (**offset**) and proceeding for a certain number of
characters.
Xref "substrings (term)"

- superclass
Item "superclass"
See **base class**.

- superuser
Item "superuser"
The person whom the **operating system** will let do almost
anything. Typically your system administrator or someone pretending to be
your system administrator. On Unix systems, the **root** user. On Windows
systems, usually the Administrator user.
Xref "superusers"

- \s-1SV\s0
Item "SV"
Short for Xscalar valueX. But
within the Perl interpreter, every **referent** is treated as a member of a
class derived from \s-1SV,\s0 in an object-oriented sort of way. Every **value**
inside Perl is passed around as a C language \f(CW\*(C`SV*\*(C' pointer. The \s-1SV\s0
**struct** knows its own Xreferent typeX, and the code is smart enough (we
hope) not to try to call a **hash** function on a **subroutine**.
Xref "scalar values, about values, scalar"

- switch
Item "switch"
An option you give on a command line to
influence the way your program works, usually introduced with a minus sign.
The word is also used as a nickname for a **switch statement**.
Xref "switches, about switches"

- switch cluster
Item "switch cluster"
The combination of multiple command-
line switches (*e.g.*, \f(CW\*(C`Xa Xb Xc\*(C') into one switch (*e.g.*, \f(CW\*(C`Xabc\*(C').
Any switch with an additional **argument** must be the last switch in a
cluster.
Xref "switch clusters clusters, switch"

- switch statement
Item "switch statement"
A program technique that lets you
evaluate an **expression** and then, based on the value of the expression,
do a multiway branch to the appropriate piece of code for that value. Also
called a Xcase structureX, named after the similar Pascal construct. Most
switch statements in Perl are spelled \f(CW\*(C`given\*(C'. See XThe \f(CW\*(C`given\*(C'
statementX in Camel chapter 4, XStatements and DeclarationsX.
Xref "switch statement statements, switch"

- symbol
Item "symbol"
Generally, any **token** or **metasymbol**. Often used
more specifically to mean the sort of name you might find in a \fBsymbol
table.
Xref "symbols symbols"

- symbolic debugger
Item "symbolic debugger"
A program that lets you step through
the **execution** of your program, stopping or printing things out here and
there to see whether anything has gone wrong, and, if so, what. The
XsymbolicX part just means that you can talk to the debugger using the same
symbols with which your program is written.
Xref "symbolic debugger debugger, about"

- symbolic link
Item "symbolic link"
An alternate filename that points to the
real **filename**, which in turn points to the real **file**. Whenever the
**operating system** is trying to parse a **pathname** containing a symbolic
link, it merely substitutes the new name and continues parsing.
Xref "symbolic links links, symbolic"

- symbolic reference
Item "symbolic reference"
A variable whose value is the
name of another variable or subroutine. By **dereferencing** the first
variable, you can get at the second one. Symbolic references are illegal
under \f(CW\*(C`use strict "refs"\*(C'.
Xref "symbolic references references, symbolic"

- symbol table
Item "symbol table"
Where a **compiler** remembers symbols. A program
like Perl must somehow remember all the names of all the **variables**,
**filehandles**, and **subroutines** youXve used. It does this by placing the
names in a symbol table, which is implemented in Perl using a \fBhash
table. There is a separate symbol table for each **package** to give each
package its own **namespace**.
Xref "symbol tables, about"

- synchronous
Item "synchronous"
Programming in which the orderly sequence of events
can be determined; that is, when things happen one after the other, not at
the same time.
Xref "synchronous (term)"

- syntactic sugar
Item "syntactic sugar"
An alternative way of writing something more easily; a
shortcut.
Xref "syntactic sugar"

- syntax
Item "syntax"
From Greek \s-1XXXXXXXX,\s0 Xwith-arrangementX. How things
(particularly symbols) are put together with each other.
Xref "syntax, about"

- syntax tree
Item "syntax tree"
An internal representation of your program wherein
lower-level **constructs** dangle off the higher-level constructs enclosing
them.
Xref "syntax tree"

- syscall
Item "syscall"
A **function** call directly to the \fBoperating
system. Many of the important subroutines and functions you use arenXt
direct system calls, but are built up in one or more layers above the
system call level. In general, Perl programmers donXt need to worry about
the distinction. However, if you do happen to know which Perl functions are
really syscalls, you can predict which of these will set the \f(CW$!
(\f(CW$ERRNO) variable on failure. Unfortunately, beginning programmers often
confusingly employ the term Xsystem callX to mean what happens when you
call the Perl \f(CW\*(C`system\*(C' function, which actually involves many syscalls. To
avoid any confusion, we nearly always say XsyscallX for something you could
call indirectly via PerlXs \f(CW\*(C`syscall\*(C' function, and never for something you
would call with PerlXs \f(CW\*(C`system\*(C' function.
Xref "syscall function, about"

### T

Subsection "T"

- taint checks
Item "taint checks"
The special bookkeeping Perl does to track the flow
of external data through your program and disallow their use in system
commands.
Xref "taint checks, about"

- tainted
Item "tainted"
Said of data derived from the grubby hands of a user,
and thus unsafe for a secure program to rely on. Perl does taint checks if
you run a **setuid** (or **setgid**) program, or if you use the \f(CW\*(C`XT\*(C' switch.
Xref "tainted data, about"

- taint mode
Item "taint mode"
Running under the \f(CW\*(C`XT\*(C' switch, marking all external data as
suspect and refusing to use it with system commands. See Camel chapter 20,
XSecurityX.
Xref "taint mode"

- \s-1TCP\s0
Item "TCP"
Short for Transmission Control Protocol. A protocol wrapped around the
Internet Protocol to make an unreliable packet transmission mechanism
appear to the application program to be a reliable **stream** of bytes.
(Usually.)
Xref "TCP (Transmission Control Protocol) Transmission Control Protocol (TCP)"

- term
Item "term"
Short for a XterminalXXthat is, a leaf node of a \fBsyntax
tree. A thing that functions grammatically as an **operand** for the
operators in an expression.
Xref "terms, defined"

- terminator
Item "terminator"
A **character** or **string** that marks the end of another string. The \f(CW$/
variable contains the string that terminates a \f(CW\*(C`readline\*(C' operation, which
\f(CW\*(C`chomp\*(C' deletes from the end. Not to be confused with **delimiters** or
**separators**. The period at the end of this sentence is a terminator.
Xref "terminators (term) characters, terminators strings, terminators in"

- ternary
Item "ternary"
An **operator** taking three **operands**. Sometimes
pronounced **trinary**.
Xref "ternary operators"

- text
Item "text"
A **string** or **file** containing primarily printable characters.
Xref "text, defined strings, text files, text text"

- thread
Item "thread"
Like a forked process, but without **fork**Xs inherent
memory protection. A thread is lighter weight than a full process, in that
a process could have multiple threads running around in it, all fighting
over the same processXs memory space unless steps are taken to protect
threads from one another.
Xref "threads (term)"

- tie
Item "tie"
The bond between a magical variable and its
implementation class. See the \f(CW\*(C`tie\*(C' function in Camel chapter 27,
XFunctionsX and Camel chapter 14, XTied VariablesX.
Xref "tied variables, about"

- titlecase
Item "titlecase"
The case used for capitals
that are followed by lowercase characters instead of by more capitals.
Sometimes called sentence case or headline case. English doesnXt use
Unicode titlecase, but casing rules for English titles are more complicated
than simply capitalizing each wordXs first character.
Xref "titlecase characters characters, titlecase"

- \s-1TMTOWTDI\s0
Item "TMTOWTDI"
ThereXs More Than One Way To Do It, the Perl Motto. The
notion that there can be more than one valid path to solving a programming
problem in context. (This doesnXt mean that more ways are always better or
that all possible paths are equally desirableXjust that there need not be
One True Way.)
Xref "TMTOWTDI acronym"

- token
Item "token"
A morpheme in a programming language, the smallest unit
of text with semantic significance.
Xref "tokens, defined"

- tokener
Item "tokener"
A module that breaks a program text into a sequence of
**tokens** for later analysis by a parser.
Xref "tokeners, defined"

- tokenizing
Item "tokenizing"
Splitting up a program text into **tokens**. Also known as
XlexingX, in which case you get XlexemesX instead of tokens.
Xref "tokenizing"

- toolbox approach
Item "toolbox approach"
The notion that, with a complete set of simple tools
that work well together, you can build almost anything you want. Which is
fine if youXre assembling a tricycle, but if youXre building a
defranishizing comboflux regurgalator, you really want your own machine
shop in which to build special tools. Perl is sort of a machine shop.
Xref "toolbox approach"

- topic
Item "topic"
The thing youXre working on. Structures like
\f(CW\*(C`while(<>)\*(C', \f(CW\*(C`for\*(C', \f(CW\*(C`foreach\*(C', and \f(CW\*(C`given\*(C' set the topic for
you by assigning to \f(CW$_, the default (*topic*) variable.
Xref "topics (term)"

- transliterate
Item "transliterate"
To turn one string
representation into another by mapping each character of the source string
to its corresponding character in the result string. Not to be confused
with translation: for example, Greek *\s-1XXXXXXXXXX\s0* transliterates into
*polychromos* but translates into *many-colored*. See the \f(CW\*(C`tr///\*(C'
operator in Camel chapter 5, XPattern MatchingX.
Xref "tr (transliteration) operator, about strings, transliteration of transliteration (tr ) operator, about"

- trigger
Item "trigger"
An event that causes a **handler** to be run.
Xref "triggers (term)"

- trinary
Item "trinary"
Not a stellar system with three stars, but an
**operator** taking three **operands**. Sometimes pronounced **ternary**.
Xref "trinary operators"

- troff
Item "troff"
A venerable typesetting language from which Perl derives
the name of its \f(CW$% variable and which is secretly used in the production
of Camel books.
Xref "troff language"

- true
Item "true"
Any scalar value that doesnXt evaluate to 0 or
\f(CW"".
Xref "true values values, true"

- truncating
Item "truncating"
Emptying a file of existing
contents, either automatically when opening a file for writing or
explicitly via the \f(CW\*(C`truncate\*(C' function.
Xref "truncate function files, truncating"

- type
Item "type"
See **data type** and **class**.
Xref "type"

- type casting
Item "type casting"
Converting data from one type to another. C permits this.
Perl does not need it. Nor want it.
Xref "type casting"

- typedef
Item "typedef"
A type definition in the C and \*(C+ languages.
Xref "typedef"

- typed lexical
Item "typed lexical"
A **lexical variable**  lexical>that is declared with a **class**
type: \f(CW\*(C`my Pony $bill\*(C'.
Xref "typed lexicals lexical variables, typed lexicals variables, variable"

- typeglob
Item "typeglob"
Use of a single identifier, prefixed with \f(CW\*(C`*\*(C'. For
example, \f(CW*name stands for any or all of \f(CW$name, \f(CW@name, \f(CW%name,
\f(CW&name, or just \f(CW\*(C`name\*(C'. How you use it determines whether it is
interpreted as all or only one of them. See XTypeglobs and FilehandlesX in
Camel chapter 2, XBits and PiecesX.
Xref "typeglobs, defined"

- typemap
Item "typemap"
A description of how C types may be transformed to and from Perl
types within an **extension** module written in **\s-1XS\s0**.
Xref "typemap"

### U

Subsection "U"

- \s-1UDP\s0
Item "UDP"
User Datagram Protocol, the typical way to send
**datagrams** over the Internet.
Xref "User Datagram Protocol (UDP) UDP (User Datagram Protocol) datagrams, UDP support"

- \s-1UID\s0
Item "UID"
A user \s-1ID.\s0 Often used in the context of
**file** or **process** ownership.
Xref "UID (user ID) user ID (UID)"

- umask
Item "umask"
A mask of those **permission bits** that should be forced
off when creating files or directories, in order to establish a policy of
whom youXll ordinarily deny access to. See the \f(CW\*(C`umask\*(C' function.
Xref "umask function"

- unary operator
Item "unary operator"
An operator with only one **operand**, like \f(CW\*(C`!\*(C' or
\f(CW\*(C`chdir\*(C'. Unary operators are usually prefix operators; that is, they
precede their operand. The \f(CW\*(C`++\*(C' and \f(CW\*(C`XX\*(C' operators can be either prefix
or postfix. (Their position *does* change their meanings.)
Xref "unary operators, about"

- Unicode
Item "Unicode"
A character set comprising all the major character sets of
the world, more or less. See <http://www.unicode.org>.
Xref "Unicode, about"

- Unix
Item "Unix"
A very large and constantly evolving language with several
alternative and largely incompatible syntaxes, in which anyone can define
anything any way they choose, and usually do. Speakers of this language
think itXs easy to learn because itXs so easily twisted to oneXs own ends,
but dialectical differences make tribal intercommunication nearly
impossible, and travelers are often reduced to a pidgin-like subset of the
language. To be universally understood, a Unix shell programmer must spend
years of study in the art. Many have abandoned this discipline and now
communicate via an Esperanto-like language called Perl.
Xref "Unix language"
.Sp
In ancient times, Unix was also used to refer to some code that a couple of
people at Bell Labs wrote to make use of a \s-1PDP-7\s0 computer that wasnXt doing
much of anything else at the time.

- uppercase
Item "uppercase"
In Unicode, not just
characters with the General Category of Uppercase Letter, but any character
with the Uppercase property, including some Letter Numbers and Symbols. Not
to be confused with **titlecase**.
Xref "uppercase characters characters, uppercase"

### V

Subsection "V"

- value
Item "value"
An actual piece of data, in contrast to all the
variables, references, keys, indices, operators, and whatnot that you need
to access the value.
Xref "values, defined"

- variable
Item "variable"
A named storage location that can hold any
of various kinds of **value**, as your program sees fit.
Xref "variables, defined variables"

- variable interpolation
Item "variable interpolation"
The **interpolation** of
a scalar or array variable into a string.
Xref "variable interpolation interpolation, variable"

- variadic
Item "variadic"
Said of a **function** that happily receives an
indeterminate number of **actual arguments**.
Xref "variadic (term)"

- vector
Item "vector"
Mathematical jargon for a list of **scalar values**.
Xref "vectors"

- virtual
Item "virtual"
Providing the appearance of something without the reality,
as in: virtual memory is not real memory. (See also **memory**.) The
opposite of XvirtualX is XtransparentX, which means providing the reality
of something without the appearance, as in: Perl handles the
variable-length \s-1UTFX8\s0 character encoding transparently.
Xref "virtual (term)"

- void context
Item "void context"
A form of **scalar context** in which an
**expression** is not expected to return any **value** at all and is
evaluated for its **side effects** alone.
Xref "void context context, void"

- v-string
Item "v-string"
A XversionX or XvectorX **string**
specified with a \f(CW\*(C`v\*(C' followed by a series of decimal integers in dot
notation, for instance, \f(CW\*(C`v1.20.300.4000\*(C'. Each number turns into a
**character** with the specified ordinal value. (The \f(CW\*(C`v\*(C' is optional when
there are at least three integers.)
Xref "vXstrings strings, vXstrings"

### W

Subsection "W"

- warning
Item "warning"
A message printed to the \f(CW\*(C`STDERR\*(C' stream to the effect that something might be
wrong but isnXt worth blowing up over. See \f(CW\*(C`warn\*(C' in Camel chapter 27,
XFunctionsX and the \f(CW\*(C`warnings\*(C' pragma in Camel chapter 28, XPragmantic
ModulesX.
Xref "warning messages STDERR filehandle, warning messages and"

- watch expression
Item "watch expression"
An expression which, when its value
changes, causes a breakpoint in the Perl debugger.
Xref "watch expression expressions, watch"

- weak reference
Item "weak reference"
A reference that doesnXt get counted
normally. When all the normal references to data disappear, the data
disappears. These are useful for circular references that would never
disappear otherwise.
Xref "weak references references, weak"

- whitespace
Item "whitespace"
A **character** that moves
your cursor but doesnXt otherwise put anything on your screen. Typically
refers to any of: space, tab, line feed, carriage return, or form feed. In
Unicode, matches many other characters that Unicode considers whitespace,
including the X-XX .
Xref "whitespace characters characters, whitespace"

- word
Item "word"
In normal XcomputereseX, the piece of data of the size most
efficiently handled by your computer, typically 32 bits or so, give or take a
few powers of 2. In Perl culture, it more often refers to an alphanumeric
**identifier** (including underscores), or to a string of nonwhitespace
**characters** bounded by whitespace or string boundaries.
Xref "words (term)"

- working directory
Item "working directory"
Your current **directory**, from
which relative pathnames are interpreted by the **operating system**. The
operating system knows your current directory because you told it with a
\f(CW\*(C`chdir\*(C', or because you started out in the place where your parent
**process** was when you were born.
Xref "working directory directories, working"

- wrapper
Item "wrapper"
A program or subroutine that runs some other program or
subroutine for you, modifying some of its input or output to better suit
your purposes.
Xref "wrappers (term)"

- \s-1WYSIWYG\s0
Item "WYSIWYG"
What You See Is What You Get. Usually used when something
that appears on the screen matches how it will eventually look, like PerlXs
\f(CW\*(C`format\*(C' declarations. Also used to mean the opposite of magic because
everything works exactly as it appears, as in the three- argument form of
\f(CW\*(C`open\*(C'.
Xref "WYSIWYG acronym"

### X

Subsection "X"

- \s-1XS\s0
Item "XS"
An extraordinarily
exported, expeditiously excellent, expressly eXternal Subroutine, executed
in existing C or \*(C+ or in an exciting extension language called
(exasperatingly) \s-1XS.\s0
Xref "XS (eXternal Subroutine) eXternal Subroutine (XS)"

- \s-1XSUB\s0
Item "XSUB"
An external **subroutine** defined in **\s-1XS\s0**.
Xref "XSUB (term)"

### Y

Subsection "Y"

- yacc
Item "yacc"
Yet Another Compiler Compiler. A parser generator without
which Perl probably would not have existed. See the file *perly.y* in the
Perl source distribution.
Xref "yacc acronym"

### Z

Subsection "Z"

- zero width
Item "zero width"
A subpattern **assertion** matching the \fBnull
string between **characters**.
Xref "zeroXwidth assertions subpatterns, zeroXwidth assertions assertions (in regexes), zeroXwidth"

- zombie
Item "zombie"
A process that has died (exited) but
whose parent has not yet received proper notification of its demise by
virtue of having called \f(CW\*(C`wait\*(C' or \f(CW\*(C`waitpid\*(C'. If you \f(CW\*(C`fork\*(C', you must
clean up after your child processes when they exit; otherwise, the process
table will fill up and your system administrator will Not Be Happy with
you.
Xref "zombie processes processes, zombie"

## AUTHOR AND COPYRIGHT

Header "AUTHOR AND COPYRIGHT"
Based on the Glossary of *Programming Perl*, Fourth Edition,
by Tom Christiansen, brian d foy, Larry Wall, & Jon Orwant.
Copyright (c) 2000, 1996, 1991, 2012 O'Reilly Media, Inc.
This document may be distributed under the same terms as Perl itself.
