+++
author = "None Specified"
manpage_name = "uulog"
manpage_section = "1"
manpage_format = "troff"
title = "uulog(1)"
keywords = ["uucp", "author", "ian", "lance", "taylor", "airs", "com", "text", "for", "this", "manpage", "comes", "from", "version", "1", "07", "info", "documentation"]
detected_package_version = "0"
date = "Sun Feb 16 04:48:20 2025"
description = "The  program can be used to display entries in the UUCP log file. It can select the entries for a particular system or a particular user.   You can use it to see what has happened to your queued jobs in the past. Different options may be used to s..."
operating_system_version = "15.3"
operating_system = "macos"
+++

uulog 1 "Taylor UUCP 1.07"

## NAME

uulog - display entries in the UUCP log file.

## SYNOPSIS

uulog
[-#] [-n lines] [-sf system] [-u user] [-DSF] [--lines lines]
[--system system] [--user user] [--debuglog] [--statslog]
[--follow] [--follow=system]

## DESCRIPTION

The
uulog
program can be used to display entries in the UUCP log file.
It can select the entries for a particular system or a particular user.
You can use it to see what has happened to your queued jobs in the past.
Different options may be used to select which parts of the file to display.

## OPTIONS


-#, -n lines, --lines lines
Here '#' is a number; e.g., `-10'.
The specified number of lines is displayed from the end of the log file.
The default is to display the entire log file,
unless the
-f, -F,
or
--follow
options are used, in which case the default is to display 10 lines.

-s system, --system system
Display only log entries pertaining to the specified system.

-u, --user user
Display only log entries pertaining to the specified user.

-D --debuglog
Display the debugging log file.

-S, --statslog
Display the statistics log file.

-F, --follow
Keep displaying the log file forever, printing new lines as they
are appended to the log file.

-f system, --follow=system
Keep displaying the log file forever, displaying only log entries
pertaining to the specified system.

Standard UUCP options:

-X type, --debug type
Turn on particular debugging types.  The following types are
recognized: abnormal, chat, handshake, uucp-proto, proto, port,
config, spooldir, execute, incoming, outgoing.
--debug
option may appear multiple times.  A number may also be given, which
will turn on that many types from the foregoing list; for example,
--debug 2
is equivalent to
--debug abnormal,chat.

-I file, --config
Set configuration file to use.

-v, --version
Report version information and exit.

--help
Print a help message and exit.

## SEE ALSO

uucp(1)

## AUTHOR

Ian Lance Taylor
<ian@airs.com>.
Text for this Manpage comes from Taylor UUCP, version 1.07 Info documentation.

