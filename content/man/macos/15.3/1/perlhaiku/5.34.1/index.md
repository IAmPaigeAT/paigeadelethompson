+++
detected_package_version = "5.34.1"
date = "2022-02-26"
title = "perlhaiku(1)"
manpage_section = "1"
operating_system_version = "15.3"
operating_system = "macos"
manpage_format = "troff"
description = "This file contains instructions how to build Perl for Haiku and lists known problems. The build procedure is completely standard:   ./Configure -de   make   make install Make perl executable and create a symlink for libperl:   chmod a+x /boot/c..."
author = "None Specified"
manpage_name = "perlhaiku"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLHAIKU 1"
PERLHAIKU 1 "2022-02-26" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlhaiku - Perl version 5.10+ on Haiku

## DESCRIPTION

Header "DESCRIPTION"
This file contains instructions how to build Perl for Haiku and lists
known problems.

## BUILD AND INSTALL

Header "BUILD AND INSTALL"
The build procedure is completely standard:

.Vb 3
  ./Configure -de
  make
  make install
.Ve

Make perl executable and create a symlink for libperl:

.Vb 2
  chmod a+x /boot/common/bin/perl
  cd /boot/common/lib; ln -s perl5/5.34.1/BePC-haiku/CORE/libperl.so .
.Ve

Replace \f(CW5.34.1 with your respective version of Perl.

## KNOWN PROBLEMS

Header "KNOWN PROBLEMS"
The following problems are encountered with Haiku revision 28311:

- \(bu
Perl cannot be compiled with threading support \s-1ATM.\s0

- \(bu
The *cpan/Socket/t/socketpair.t* test fails. More precisely: the subtests
using datagram sockets fail. Unix datagram sockets aren't implemented in
Haiku yet.

- \(bu
A subtest of the *cpan/Sys-Syslog/t/syslog.t* test fails. This is due to Haiku
not implementing */dev/log* support yet.

- \(bu
The tests *dist/Net-Ping/t/450_service.t* and *dist/Net-Ping/t/510_ping_udp.t*
fail. This is due to bugs in Haiku's network stack implementation.

## CONTACT

Header "CONTACT"
For Haiku specific problems contact the HaikuPorts developers:
<http://ports.haiku-files.org/>

The initial Haiku port was done by Ingo Weinhold <ingo_weinhold@gmx.de>.

Last update: 2008-10-29
