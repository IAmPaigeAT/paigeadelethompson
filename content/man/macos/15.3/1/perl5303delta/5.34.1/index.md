+++
operating_system = "macos"
operating_system_version = "15.3"
detected_package_version = "5.34.1"
author = "None Specified"
date = "2022-02-19"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
description = "This document describes differences between the 5.30.2 release and the 5.30.3 release. If you are upgrading from an earlier release such as 5.30.1, first read perl5302delta, which describes differences between 5.30.1 and 5.30.2. A signed f(CW*(C`s..."
manpage_section = "1"
title = "perl5303delta(1)"
manpage_format = "troff"
manpage_name = "perl5303delta"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5303DELTA 1"
PERL5303DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5303delta - what is new for perl v5.30.3

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.30.2 release and the 5.30.3
release.

If you are upgrading from an earlier release such as 5.30.1, first read
perl5302delta, which describes differences between 5.30.1 and 5.30.2.

## Security

Header "Security"

### [\s-1CVE-2020-10543\s0] Buffer overflow caused by a crafted regular expression

Subsection "[CVE-2020-10543] Buffer overflow caused by a crafted regular expression"
A signed \f(CW\*(C`size_t\*(C' integer overflow in the storage space calculations for
nested regular expression quantifiers could cause a heap buffer overflow in
Perl's regular expression compiler that overwrites memory allocated after the
regular expression storage space with attacker supplied data.

The target system needs a sufficient amount of memory to allocate partial
expansions of the nested quantifiers prior to the overflow occurring.  This
requirement is unlikely to be met on 64-bit systems.

Discovered by: ManhND of The Tarantula Team, VinCSS (a member of Vingroup).

### [\s-1CVE-2020-10878\s0] Integer overflow via malformed bytecode produced by a crafted regular expression

Subsection "[CVE-2020-10878] Integer overflow via malformed bytecode produced by a crafted regular expression"
Integer overflows in the calculation of offsets between instructions for the
regular expression engine could cause corruption of the intermediate language
state of a compiled regular expression.  An attacker could abuse this behaviour
to insert instructions into the compiled form of a Perl regular expression.

Discovered by: Hugo van der Sanden and Slaven Rezic.

### [\s-1CVE-2020-12723\s0] Buffer overflow caused by a crafted regular expression

Subsection "[CVE-2020-12723] Buffer overflow caused by a crafted regular expression"
Recursive calls to \f(CW\*(C`S_study_chunk()\*(C' by Perl's regular expression compiler to
optimize the intermediate language representation of a regular expression could
cause corruption of the intermediate language state of a compiled regular
expression.

Discovered by: Sergey Aleynikov.

### Additional Note

Subsection "Additional Note"
An application written in Perl would only be vulnerable to any of the above
flaws if it evaluates regular expressions supplied by the attacker.  Evaluating
regular expressions in this fashion is known to be dangerous since the regular
expression engine does not protect against denial of service attacks in this
usage scenario.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with Perl 5.30.2.  If any
exist, they are bugs, and we request that you submit a report.  See
\*(L"Reporting Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Module::CoreList has been upgraded from version 5.20200314 to 5.20200601_30.

## Testing

Header "Testing"
Tests were added and changed to reflect the other additions and changes in this
release.

## Acknowledgements

Header "Acknowledgements"
Perl 5.30.3 represents approximately 3 months of development since Perl 5.30.2
and contains approximately 1,100 lines of changes across 42 files from 7
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 350 lines of changes to 8 .pm, .t, .c and .h files.

Perl continues to flourish into its fourth decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.30.3:

Chris 'BinGOs' Williams, Hugo van der Sanden, John Lightsey, Karl Williamson,
Nicolas R., Sawyer X, Steve Hay.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database at
<https://github.com/Perl/perl5/issues>.  There may also be information at
<https://www.perl.org/>, the Perl Home Page.

If you believe you have an unreported bug, please open an issue at
<https://github.com/Perl/perl5/issues>.  Be sure to trim your bug down to a
tiny but sufficient test case.

If the bug you are reporting has security implications which make it
inappropriate to send to a public issue tracker, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec for details of how to
report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5, you
can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
