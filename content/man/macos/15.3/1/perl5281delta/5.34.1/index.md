+++
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_format = "troff"
title = "perl5281delta(1)"
manpage_name = "perl5281delta"
date = "2022-02-19"
description = "This document describes differences between the 5.28.0 release and the 5.28.1 release. If you are upgrading from an earlier release such as 5.26.0, first read perl5280delta, which describes differences between 5.26.0 and 5.28.0. Integer arithmetic..."
manpage_section = "1"
operating_system = "macos"
operating_system_version = "15.3"
author = "None Specified"
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5281DELTA 1"
PERL5281DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5281delta - what is new for perl v5.28.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.28.0 release and the 5.28.1
release.

If you are upgrading from an earlier release such as 5.26.0, first read
perl5280delta, which describes differences between 5.26.0 and 5.28.0.

## Security

Header "Security"

### [\s-1CVE-2018-18311\s0] Integer overflow leading to buffer overflow and segmentation fault

Subsection "[CVE-2018-18311] Integer overflow leading to buffer overflow and segmentation fault"
Integer arithmetic in \f(CW\*(C`Perl_my_setenv()\*(C' could wrap when the combined length
of the environment variable name and value exceeded around 0x7fffffff.  This
could lead to writing beyond the end of an allocated buffer with attacker
supplied data.

[\s-1GH\s0 #16560] <https://github.com/Perl/perl5/issues/16560>

### [\s-1CVE-2018-18312\s0] Heap-buffer-overflow write in S_regatom (regcomp.c)

Subsection "[CVE-2018-18312] Heap-buffer-overflow write in S_regatom (regcomp.c)"
A crafted regular expression could cause heap-buffer-overflow write during
compilation, potentially allowing arbitrary code execution.

[\s-1GH\s0 #16649] <https://github.com/Perl/perl5/issues/16649>

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.28.0.  If any exist,
they are bugs, and we request that you submit a report.  See
\*(L"Reporting Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Module::CoreList has been upgraded from version 5.20180622 to 5.20181129_28.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Perl 5.28 introduced an \f(CW\*(C`index()\*(C' optimization when comparing to -1 (or
indirectly, e.g. >= 0).  When this optimization was triggered inside a \f(CW\*(C`when\*(C'
clause it caused a warning (\*(L"Argument \f(CW%s isn't numeric in smart match\*(R").  This
has now been fixed.
[\s-1GH\s0 #16626] <https://github.com/Perl/perl5/issues/16626>

- \(bu
Matching of decimal digits in script runs, introduced in Perl 5.28, had a bug
that led to \f(CW"1\\N\{THAI DIGIT FIVE\}" matching \f(CW\*(C`/^(*sr:\\d+)$/\*(C' when it should
not.  This has now been fixed.

- \(bu
The new in-place editing code no longer leaks directory handles.
[\s-1GH\s0 #16602] <https://github.com/Perl/perl5/issues/16602>

## Acknowledgements

Header "Acknowledgements"
Perl 5.28.1 represents approximately 5 months of development since Perl 5.28.0
and contains approximately 6,100 lines of changes across 44 files from 12
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 700 lines of changes to 12 .pm, .t, .c and .h files.

Perl continues to flourish into its fourth decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.28.1:

Aaron Crane, Abigail, Chris 'BinGOs' Williams, Dagfinn Ilmari Mannsa\*oker, David
Mitchell, James E Keenan, John \s-1SJ\s0 Anderson, Karen Etheridge, Karl Williamson,
Sawyer X, Steve Hay, Tony Cook.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database
at <https://rt.perl.org/> .  There may also be information at
<http://www.perl.org/> , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5,
you can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
