+++
title = "cups-config(1)"
description = "The cups-config command allows application developers to determine the necessary command-line options for the compiler and linker, as well as the installation directories for filters, configuration files, and drivers. All values are reported to the s..."
operating_system = "macos"
detected_package_version = "2.0"
author = "None Specified"
date = "Sun Feb 16 04:48:25 2025"
keywords = ["cups", "1", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
operating_system_version = "15.3"
manpage_name = "cups-config"
manpage_format = "troff"
manpage_section = "1"
+++

cups-config 1 "CUPS" "26 April 2019" "Apple Inc."

## NAME

cups-config - get cups api, compiler, directory, and link information.

## SYNOPSIS

cups-config
--api-version
.br
cups-config
--build
.br
cups-config
--cflags
.br
cups-config
--datadir
.br
cups-config
--help
.br
cups-config
--ldflags
.br
cups-config
[
--image
] [
--static
]
--libs
.br
cups-config
--serverbin
.br
cups-config
--serverroot
.br
cups-config
--version
.br

## DESCRIPTION

The **cups-config** command allows application developers to determine the necessary command-line options for the compiler and linker, as well as the installation directories for filters, configuration files, and drivers.
All values are reported to the standard output.

## OPTIONS

The **cups-config** command accepts the following command-line options:

--api-version
Reports the current API version (major.minor).

--build
Reports a system-specific build number.

--cflags
Reports the necessary compiler options.

--datadir
Reports the default CUPS data directory.

--help
Reports the program usage message.

--ldflags
Reports the necessary linker options.

--libs
Reports the necessary libraries to link to.

--serverbin
Reports the default CUPS binary directory, where filters and backends are stored.

--serverroot
Reports the default CUPS configuration file directory.

--static
When used with *--libs*, reports the static libraries instead of the default (shared) libraries.

--version
Reports the full version number of the CUPS installation (major.minor.patch).

## EXAMPLES

Show the currently installed version of CUPS:

```

    cups-config \-\-version

```

Compile a simple one-file CUPS filter:

```

    cc `cups\-config \-\-cflags \-\-ldflags` \-o filter filter.c \\
        `cups\-config \-\-libs`
```


## DEPRECATED OPTIONS

The following options are deprecated but continue to work for backwards compatibility:

--image
Formerly used to add the CUPS imaging library to the list of libraries.

## SEE ALSO

cups (1),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
