+++
description = "Prior to release 5.0 of Perl, a programmer could use dbmopen() to connect an on-disk database in the standard Unix dbm|(3x) format magically to a f(CW%HASH in their program.  However, their Perl was either built with one particular dbm library or a..."
operating_system = "macos"
operating_system_version = "15.3"
date = "2022-02-19"
title = "perltie(1)"
manpage_name = "perltie"
manpage_section = "1"
manpage_format = "troff"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "db_file", "or", "config", "for", "some", "interesting", "fbtie", "implementations", "a", "good", "starting", "point", "many", "is", "with", "one", "of", "the", "modules", "tie", "scalar", "array", "hash", "handle", "bugs", "normal", "return", "provided", "by", "f", "cw", "c", "not", "available", "what", "this", "means", "that", "using", "tied_hash", "in", "boolean", "context", "doesn", "t", "work", "right", "currently", "always", "tests", "false", "regardless", "whether", "empty", "elements", "paragraph", "needs", "review", "light", "changes", "5", "25", "localizing", "tied", "arrays", "hashes", "does", "after", "exiting", "scope", "are", "restored", "counting", "number", "entries", "via", "keys", "values", "inefficient", "since", "it", "to", "iterate", "through", "all", "s-1firstkey", "nextkey", "s0", "slices", "cause", "multiple", "s-1fetch", "store", "pairs", "there", "no", "methods", "slice", "operations", "you", "cannot", "easily", "multilevel", "data", "structure", "such", "as", "dbm", "file", "first", "problem", "but", "s-1gdbm", "and", "berkeley", "s-1db", "have", "size", "limitations", "beyond", "problems", "how", "references", "be", "represented", "on", "disk", "module", "attempt", "address", "need", "deep", "check", "your", "nearest", "s-1cpan", "site", "described", "perlmodlib", "source", "code", "note", "despite", "its", "name", "use", "another", "earlier", "at", "solving", "s-1mldbm", "which", "has", "fairly", "serious", "filehandles", "still", "incomplete", "fbsysopen", "fbtruncate", "fbflock", "fbfcntl", "fbstat", "x", "can", "trapped", "author", "tom", "christiansen", "s-1tiehandle", "sven", "verdoolaege", "fiskimo", "dns", "ufsia", "ac", "doug", "maceachern", "fidougm", "osf", "org", "s-1untie", "nick", "ing-simmons", "finick", "net", "s-1scalar", "tassilo", "von", "parseval", "fitassilo", "rwth-aachen", "de", "tying", "casey", "west", "ficasey", "geeknest", "com"]
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLTIE 1"
PERLTIE 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perltie - how to hide an object class in a simple variable
Xref "tie"

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
 tie VARIABLE, CLASSNAME, LIST

 $object = tied VARIABLE

 untie VARIABLE
.Ve

## DESCRIPTION

Header "DESCRIPTION"
Prior to release 5.0 of Perl, a programmer could use **dbmopen()**
to connect an on-disk database in the standard Unix **dbm**\|(3x)
format magically to a \f(CW%HASH in their program.  However, their Perl was either
built with one particular dbm library or another, but not both, and
you couldn't extend this mechanism to other packages or types of variables.

Now you can.

The **tie()** function binds a variable to a class (package) that will provide
the implementation for access methods for that variable.  Once this magic
has been performed, accessing a tied variable automatically triggers
method calls in the proper class.  The complexity of the class is
hidden behind magic methods calls.  The method names are in \s-1ALL CAPS,\s0
which is a convention that Perl uses to indicate that they're called
implicitly rather than explicitly\*(--just like the \s-1**BEGIN\s0()** and \s-1**END\s0()**
functions.

In the **tie()** call, \f(CW\*(C`VARIABLE\*(C' is the name of the variable to be
enchanted.  \f(CW\*(C`CLASSNAME\*(C' is the name of a class implementing objects of
the correct type.  Any additional arguments in the \f(CW\*(C`LIST\*(C' are passed to
the appropriate constructor method for that class\*(--meaning \s-1**TIESCALAR\s0()**,
\s-1**TIEARRAY\s0()**, \s-1**TIEHASH\s0()**, or \s-1**TIEHANDLE\s0()**.  (Typically these are arguments
such as might be passed to the **dbminit()** function of C.) The object
returned by the \*(L"new\*(R" method is also returned by the **tie()** function,
which would be useful if you wanted to access other methods in
\f(CW\*(C`CLASSNAME\*(C'. (You don't actually have to return a reference to a right
\*(L"type\*(R" (e.g., \s-1HASH\s0 or \f(CW\*(C`CLASSNAME\*(C') so long as it's a properly blessed
object.)  You can also retrieve a reference to the underlying object
using the **tied()** function.

Unlike **dbmopen()**, the **tie()** function will not \f(CW\*(C`use\*(C' or \f(CW\*(C`require\*(C' a module
for you\*(--you need to do that explicitly yourself.

### Tying Scalars

Xref "scalar, tying"
Subsection "Tying Scalars"
A class implementing a tied scalar should define the following methods:
\s-1TIESCALAR, FETCH, STORE,\s0 and possibly \s-1UNTIE\s0 and/or \s-1DESTROY.\s0

Let's look at each in turn, using as an example a tie class for
scalars that allows the user to do something like:

.Vb 2
    tie $his_speed, \*(AqNice\*(Aq, getppid();
    tie $my_speed,  \*(AqNice\*(Aq, $$;
.Ve

And now whenever either of those variables is accessed, its current
system priority is retrieved and returned.  If those variables are set,
then the process's priority is changed!

We'll use Jarkko Hietaniemi <*jhi@iki.fi*>'s BSD::Resource class (not
included) to access the \s-1PRIO_PROCESS, PRIO_MIN,\s0 and \s-1PRIO_MAX\s0 constants
from your system, as well as the **getpriority()** and **setpriority()** system
calls.  Here's the preamble of the class.

.Vb 5
    package Nice;
    use Carp;
    use BSD::Resource;
    use strict;
    $Nice::DEBUG = 0 unless defined $Nice::DEBUG;
.Ve

- \s-1TIESCALAR\s0 classname, \s-1LIST\s0
Xref "TIESCALAR"
Item "TIESCALAR classname, LIST"
This is the constructor for the class.  That means it is
expected to return a blessed reference to a new scalar
(probably anonymous) that it's creating.  For example:
.Sp
.Vb 3
 sub TIESCALAR \{
     my $class = shift;
     my $pid = shift || $$; # 0 means me

     if ($pid !~ /^\\d+$/) \{
         carp "Nice::Tie::Scalar got non-numeric pid $pid" if $^W;
         return undef;
     \}

     unless (kill 0, $pid) \{ # EPERM or ERSCH, no doubt
         carp "Nice::Tie::Scalar got bad pid $pid: $!" if $^W;
         return undef;
     \}

     return bless \\$pid, $class;
 \}
.Ve
.Sp
This tie class has chosen to return an error rather than raising an
exception if its constructor should fail.  While this is how **dbmopen()** works,
other classes may well not wish to be so forgiving.  It checks the global
variable \f(CW$^W to see whether to emit a bit of noise anyway.

- \s-1FETCH\s0 this
Xref "FETCH"
Item "FETCH this"
This method will be triggered every time the tied variable is accessed
(read).  It takes no arguments beyond its self reference, which is the
object representing the scalar we're dealing with.  Because in this case
we're using just a \s-1SCALAR\s0 ref for the tied scalar object, a simple $$self
allows the method to get at the real value stored there.  In our example
below, that real value is the process \s-1ID\s0 to which we've tied our variable.
.Sp
.Vb 10
    sub FETCH \{
        my $self = shift;
        confess "wrong type" unless ref $self;
        croak "usage error" if @_;
        my $nicety;
        local($!) = 0;
        $nicety = getpriority(PRIO_PROCESS, $$self);
        if ($!) \{ croak "getpriority failed: $!" \}
        return $nicety;
    \}
.Ve
.Sp
This time we've decided to blow up (raise an exception) if the renice
fails\*(--there's no place for us to return an error otherwise, and it's
probably the right thing to do.

- \s-1STORE\s0 this, value
Xref "STORE"
Item "STORE this, value"
This method will be triggered every time the tied variable is set
(assigned).  Beyond its self reference, it also expects one (and only one)
argument: the new value the user is trying to assign. Don't worry about
returning a value from \s-1STORE\s0; the semantic of assignment returning the
assigned value is implemented with \s-1FETCH.\s0
.Sp
.Vb 5
 sub STORE \{
     my $self = shift;
     confess "wrong type" unless ref $self;
     my $new_nicety = shift;
     croak "usage error" if @_;

     if ($new_nicety < PRIO_MIN) \{
         carp sprintf
           "WARNING: priority %d less than minimum system priority %d",
               $new_nicety, PRIO_MIN if $^W;
         $new_nicety = PRIO_MIN;
     \}

     if ($new_nicety > PRIO_MAX) \{
         carp sprintf
           "WARNING: priority %d greater than maximum system priority %d",
               $new_nicety, PRIO_MAX if $^W;
         $new_nicety = PRIO_MAX;
     \}

     unless (defined setpriority(PRIO_PROCESS,
                                 $$self,
                                 $new_nicety))
     \{
         confess "setpriority failed: $!";
     \}
 \}
.Ve

- \s-1UNTIE\s0 this
Xref "UNTIE"
Item "UNTIE this"
This method will be triggered when the \f(CW\*(C`untie\*(C' occurs. This can be useful
if the class needs to know when no further calls will be made. (Except \s-1DESTROY\s0
of course.) See "The \f(CW\*(C`untie\*(C' Gotcha" below for more details.

- \s-1DESTROY\s0 this
Xref "DESTROY"
Item "DESTROY this"
This method will be triggered when the tied variable needs to be destructed.
As with other object classes, such a method is seldom necessary, because Perl
deallocates its moribund object's memory for you automatically\*(--this isn't
\*(C+, you know.  We'll use a \s-1DESTROY\s0 method here for debugging purposes only.
.Sp
.Vb 5
    sub DESTROY \{
        my $self = shift;
        confess "wrong type" unless ref $self;
        carp "[ Nice::DESTROY pid $$self ]" if $Nice::DEBUG;
    \}
.Ve

That's about all there is to it.  Actually, it's more than all there
is to it, because we've done a few nice things here for the sake
of completeness, robustness, and general aesthetics.  Simpler
\s-1TIESCALAR\s0 classes are certainly possible.

### Tying Arrays

Xref "array, tying"
Subsection "Tying Arrays"
A class implementing a tied ordinary array should define the following
methods: \s-1TIEARRAY, FETCH, STORE, FETCHSIZE, STORESIZE, CLEAR\s0
and perhaps \s-1UNTIE\s0 and/or \s-1DESTROY.\s0

\s-1FETCHSIZE\s0 and \s-1STORESIZE\s0 are used to provide \f(CW$#array and
equivalent \f(CW\*(C`scalar(@array)\*(C' access.

The methods \s-1POP, PUSH, SHIFT, UNSHIFT, SPLICE, DELETE,\s0 and \s-1EXISTS\s0 are
required if the perl operator with the corresponding (but lowercase) name
is to operate on the tied array. The **Tie::Array** class can be used as a
base class to implement the first five of these in terms of the basic
methods above.  The default implementations of \s-1DELETE\s0 and \s-1EXISTS\s0 in
**Tie::Array** simply \f(CW\*(C`croak\*(C'.

In addition \s-1EXTEND\s0 will be called when perl would have pre-extended
allocation in a real array.

For this discussion, we'll implement an array whose elements are a fixed
size at creation.  If you try to create an element larger than the fixed
size, you'll take an exception.  For example:

.Vb 4
    use FixedElem_Array;
    tie @array, \*(AqFixedElem_Array\*(Aq, 3;
    $array[0] = \*(Aqcat\*(Aq;  # ok.
    $array[1] = \*(Aqdogs\*(Aq; # exception, length(\*(Aqdogs\*(Aq) > 3.
.Ve

The preamble code for the class is as follows:

.Vb 3
    package FixedElem_Array;
    use Carp;
    use strict;
.Ve

- \s-1TIEARRAY\s0 classname, \s-1LIST\s0
Xref "TIEARRAY"
Item "TIEARRAY classname, LIST"
This is the constructor for the class.  That means it is expected to
return a blessed reference through which the new array (probably an
anonymous \s-1ARRAY\s0 ref) will be accessed.
.Sp
In our example, just to show you that you don't *really* have to return an
\s-1ARRAY\s0 reference, we'll choose a \s-1HASH\s0 reference to represent our object.
A \s-1HASH\s0 works out well as a generic record type: the \f(CW\*(C`\{ELEMSIZE\}\*(C' field will
store the maximum element size allowed, and the \f(CW\*(C`\{ARRAY\}\*(C' field will hold the
true \s-1ARRAY\s0 ref.  If someone outside the class tries to dereference the
object returned (doubtless thinking it an \s-1ARRAY\s0 ref), they'll blow up.
This just goes to show you that you should respect an object's privacy.
.Sp
.Vb 11
    sub TIEARRAY \{
      my $class    = shift;
      my $elemsize = shift;
      if ( @_ || $elemsize =~ /\\D/ ) \{
        croak "usage: tie ARRAY, \*(Aq" . _\|_PACKAGE_\|_ . "\*(Aq, elem_size";
      \}
      return bless \{
        ELEMSIZE => $elemsize,
        ARRAY    => [],
      \}, $class;
    \}
.Ve

- \s-1FETCH\s0 this, index
Xref "FETCH"
Item "FETCH this, index"
This method will be triggered every time an individual element the tied array
is accessed (read).  It takes one argument beyond its self reference: the
index whose value we're trying to fetch.
.Sp
.Vb 5
    sub FETCH \{
      my $self  = shift;
      my $index = shift;
      return $self->\{ARRAY\}->[$index];
    \}
.Ve
.Sp
If a negative array index is used to read from an array, the index
will be translated to a positive one internally by calling \s-1FETCHSIZE\s0
before being passed to \s-1FETCH.\s0  You may disable this feature by
assigning a true value to the variable \f(CW$NEGATIVE_INDICES in the
tied array class.
.Sp
As you may have noticed, the name of the \s-1FETCH\s0 method (et al.) is the same
for all accesses, even though the constructors differ in names (\s-1TIESCALAR\s0
vs \s-1TIEARRAY\s0).  While in theory you could have the same class servicing
several tied types, in practice this becomes cumbersome, and it's easiest
to keep them at simply one tie type per class.

- \s-1STORE\s0 this, index, value
Xref "STORE"
Item "STORE this, index, value"
This method will be triggered every time an element in the tied array is set
(written).  It takes two arguments beyond its self reference: the index at
which we're trying to store something and the value we're trying to put
there.
.Sp
In our example, \f(CW\*(C`undef\*(C' is really \f(CW\*(C`$self->\{ELEMSIZE\}\*(C' number of
spaces so we have a little more work to do here:
.Sp
.Vb 11
 sub STORE \{
   my $self = shift;
   my( $index, $value ) = @_;
   if ( length $value > $self->\{ELEMSIZE\} ) \{
     croak "length of $value is greater than $self->\{ELEMSIZE\}";
   \}
   # fill in the blanks
   $self->STORESIZE( $index ) if $index > $self->FETCHSIZE();
   # right justify to keep element size for smaller elements
   $self->\{ARRAY\}->[$index] = sprintf "%$self->\{ELEMSIZE\}s", $value;
 \}
.Ve
.Sp
Negative indexes are treated the same as with \s-1FETCH.\s0

- \s-1FETCHSIZE\s0 this
Xref "FETCHSIZE"
Item "FETCHSIZE this"
Returns the total number of items in the tied array associated with
object *this*. (Equivalent to \f(CW\*(C`scalar(@array)\*(C').  For example:
.Sp
.Vb 4
    sub FETCHSIZE \{
      my $self = shift;
      return scalar $self->\{ARRAY\}->@*;
    \}
.Ve

- \s-1STORESIZE\s0 this, count
Xref "STORESIZE"
Item "STORESIZE this, count"
Sets the total number of items in the tied array associated with
object *this* to be *count*. If this makes the array larger then
class's mapping of \f(CW\*(C`undef\*(C' should be returned for new positions.
If the array becomes smaller then entries beyond count should be
deleted.
.Sp
In our example, 'undef' is really an element containing
\f(CW\*(C`$self->\{ELEMSIZE\}\*(C' number of spaces.  Observe:
.Sp
.Vb 10
    sub STORESIZE \{
      my $self  = shift;
      my $count = shift;
      if ( $count > $self->FETCHSIZE() ) \{
        foreach ( $count - $self->FETCHSIZE() .. $count ) \{
          $self->STORE( $_, \*(Aq\*(Aq );
        \}
      \} elsif ( $count < $self->FETCHSIZE() ) \{
        foreach ( 0 .. $self->FETCHSIZE() - $count - 2 ) \{
          $self->POP();
        \}
      \}
    \}
.Ve

- \s-1EXTEND\s0 this, count
Xref "EXTEND"
Item "EXTEND this, count"
Informative call that array is likely to grow to have *count* entries.
Can be used to optimize allocation. This method need do nothing.
.Sp
In our example there is no reason to implement this method, so we leave
it as a no-op. This method is only relevant to tied array implementations
where there is the possibility of having the allocated size of the array
be larger than is visible to a perl programmer inspecting the size of the
array. Many tied array implementations will have no reason to implement it.
.Sp
.Vb 5
    sub EXTEND \{
      my $self  = shift;
      my $count = shift;
      # nothing to see here, move along.
    \}
.Ve
.Sp
**\s-1NOTE:\s0** It is generally an error to make this equivalent to \s-1STORESIZE.\s0
Perl may from time to time call \s-1EXTEND\s0 without wanting to actually change
the array size directly. Any tied array should function correctly if this
method is a no-op, even if perhaps they might not be as efficient as they
would if this method was implemented.

- \s-1EXISTS\s0 this, key
Xref "EXISTS"
Item "EXISTS this, key"
Verify that the element at index *key* exists in the tied array *this*.
.Sp
In our example, we will determine that if an element consists of
\f(CW\*(C`$self->\{ELEMSIZE\}\*(C' spaces only, it does not exist:
.Sp
.Vb 7
 sub EXISTS \{
   my $self  = shift;
   my $index = shift;
   return 0 if ! defined $self->\{ARRAY\}->[$index] ||
               $self->\{ARRAY\}->[$index] eq \*(Aq \*(Aq x $self->\{ELEMSIZE\};
   return 1;
 \}
.Ve

- \s-1DELETE\s0 this, key
Xref "DELETE"
Item "DELETE this, key"
Delete the element at index *key* from the tied array *this*.
.Sp
In our example, a deleted item is \f(CW\*(C`$self->\{ELEMSIZE\}\*(C' spaces:
.Sp
.Vb 5
    sub DELETE \{
      my $self  = shift;
      my $index = shift;
      return $self->STORE( $index, \*(Aq\*(Aq );
    \}
.Ve

- \s-1CLEAR\s0 this
Xref "CLEAR"
Item "CLEAR this"
Clear (remove, delete, ...) all values from the tied array associated with
object *this*.  For example:
.Sp
.Vb 4
    sub CLEAR \{
      my $self = shift;
      return $self->\{ARRAY\} = [];
    \}
.Ve

- \s-1PUSH\s0 this, \s-1LIST\s0
Xref "PUSH"
Item "PUSH this, LIST"
Append elements of *\s-1LIST\s0* to the array.  For example:
.Sp
.Vb 7
    sub PUSH \{
      my $self = shift;
      my @list = @_;
      my $last = $self->FETCHSIZE();
      $self->STORE( $last + $_, $list[$_] ) foreach 0 .. $#list;
      return $self->FETCHSIZE();
    \}
.Ve

- \s-1POP\s0 this
Xref "POP"
Item "POP this"
Remove last element of the array and return it.  For example:
.Sp
.Vb 4
    sub POP \{
      my $self = shift;
      return pop $self->\{ARRAY\}->@*;
    \}
.Ve

- \s-1SHIFT\s0 this
Xref "SHIFT"
Item "SHIFT this"
Remove the first element of the array (shifting other elements down)
and return it.  For example:
.Sp
.Vb 4
    sub SHIFT \{
      my $self = shift;
      return shift $self->\{ARRAY\}->@*;
    \}
.Ve

- \s-1UNSHIFT\s0 this, \s-1LIST\s0
Xref "UNSHIFT"
Item "UNSHIFT this, LIST"
Insert \s-1LIST\s0 elements at the beginning of the array, moving existing elements
up to make room.  For example:
.Sp
.Vb 9
    sub UNSHIFT \{
      my $self = shift;
      my @list = @_;
      my $size = scalar( @list );
      # make room for our list
      $self->\{ARRAY\}[ $size .. $self->\{ARRAY\}->$#* + $size ]->@*
       = $self->\{ARRAY\}->@*
      $self->STORE( $_, $list[$_] ) foreach 0 .. $#list;
    \}
.Ve

- \s-1SPLICE\s0 this, offset, length, \s-1LIST\s0
Xref "SPLICE"
Item "SPLICE this, offset, length, LIST"
Perform the equivalent of \f(CW\*(C`splice\*(C' on the array.
.Sp
*offset* is optional and defaults to zero, negative values count back
from the end of the array.
.Sp
*length* is optional and defaults to rest of the array.
.Sp
*\s-1LIST\s0* may be empty.
.Sp
Returns a list of the original *length* elements at *offset*.
.Sp
In our example, we'll use a little shortcut if there is a *\s-1LIST\s0*:
.Sp
.Vb 11
    sub SPLICE \{
      my $self   = shift;
      my $offset = shift || 0;
      my $length = shift || $self->FETCHSIZE() - $offset;
      my @list   = ();
      if ( @_ ) \{
        tie @list, _\|_PACKAGE_\|_, $self->\{ELEMSIZE\};
        @list   = @_;
      \}
      return splice $self->\{ARRAY\}->@*, $offset, $length, @list;
    \}
.Ve

- \s-1UNTIE\s0 this
Xref "UNTIE"
Item "UNTIE this"
Will be called when \f(CW\*(C`untie\*(C' happens. (See "The \f(CW\*(C`untie\*(C' Gotcha" below.)

- \s-1DESTROY\s0 this
Xref "DESTROY"
Item "DESTROY this"
This method will be triggered when the tied variable needs to be destructed.
As with the scalar tie class, this is almost never needed in a
language that does its own garbage collection, so this time we'll
just leave it out.

### Tying Hashes

Xref "hash, tying"
Subsection "Tying Hashes"
Hashes were the first Perl data type to be tied (see **dbmopen()**).  A class
implementing a tied hash should define the following methods: \s-1TIEHASH\s0 is
the constructor.  \s-1FETCH\s0 and \s-1STORE\s0 access the key and value pairs.  \s-1EXISTS\s0
reports whether a key is present in the hash, and \s-1DELETE\s0 deletes one.
\s-1CLEAR\s0 empties the hash by deleting all the key and value pairs.  \s-1FIRSTKEY\s0
and \s-1NEXTKEY\s0 implement the **keys()** and **each()** functions to iterate over all
the keys. \s-1SCALAR\s0 is triggered when the tied hash is evaluated in scalar
context, and in 5.28 onwards, by \f(CW\*(C`keys\*(C' in boolean context. \s-1UNTIE\s0 is
called when \f(CW\*(C`untie\*(C' happens, and \s-1DESTROY\s0 is called when the tied variable
is garbage collected.

If this seems like a lot, then feel free to inherit from merely the
standard Tie::StdHash module for most of your methods, redefining only the
interesting ones.  See Tie::Hash for details.

Remember that Perl distinguishes between a key not existing in the hash,
and the key existing in the hash but having a corresponding value of
\f(CW\*(C`undef\*(C'.  The two possibilities can be tested with the \f(CW\*(C`exists()\*(C' and
\f(CW\*(C`defined()\*(C' functions.

Here's an example of a somewhat interesting tied hash class:  it gives you
a hash representing a particular user's dot files.  You index into the hash
with the name of the file (minus the dot) and you get back that dot file's
contents.  For example:

.Vb 8
    use DotFiles;
    tie %dot, \*(AqDotFiles\*(Aq;
    if ( $dot\{profile\} =~ /MANPATH/ ||
         $dot\{login\}   =~ /MANPATH/ ||
         $dot\{cshrc\}   =~ /MANPATH/    )
    \{
        print "you seem to set your MANPATH\\n";
    \}
.Ve

Or here's another sample of using our tied class:

.Vb 5
    tie %him, \*(AqDotFiles\*(Aq, \*(Aqdaemon\*(Aq;
    foreach $f ( keys %him ) \{
        printf "daemon dot file %s is size %d\\n",
            $f, length $him\{$f\};
    \}
.Ve

In our tied hash DotFiles example, we use a regular
hash for the object containing several important
fields, of which only the \f(CW\*(C`\{LIST\}\*(C' field will be what the
user thinks of as the real hash.

- \s-1USER\s0
Item "USER"
whose dot files this object represents

- \s-1HOME\s0
Item "HOME"
where those dot files live

- \s-1CLOBBER\s0
Item "CLOBBER"
whether we should try to change or remove those dot files

- \s-1LIST\s0
Item "LIST"
the hash of dot file names and content mappings

Here's the start of *Dotfiles.pm*:

.Vb 5
    package DotFiles;
    use Carp;
    sub whowasi \{ (caller(1))[3] . \*(Aq()\*(Aq \}
    my $DEBUG = 0;
    sub debug \{ $DEBUG = @_ ? shift : 1 \}
.Ve

For our example, we want to be able to emit debugging info to help in tracing
during development.  We keep also one convenience function around
internally to help print out warnings; **whowasi()** returns the function name
that calls it.

Here are the methods for the DotFiles tied hash.

- \s-1TIEHASH\s0 classname, \s-1LIST\s0
Xref "TIEHASH"
Item "TIEHASH classname, LIST"
This is the constructor for the class.  That means it is expected to
return a blessed reference through which the new object (probably but not
necessarily an anonymous hash) will be accessed.
.Sp
Here's the constructor:
.Sp
.Vb 9
    sub TIEHASH \{
        my $self = shift;
        my $user = shift || $>;
        my $dotdir = shift || \*(Aq\*(Aq;
        croak "usage: @\{[&whowasi]\} [USER [DOTDIR]]" if @_;
        $user = getpwuid($user) if $user =~ /^\\d+$/;
        my $dir = (getpwnam($user))[7]
                || croak "@\{[&whowasi]\}: no user $user";
        $dir .= "/$dotdir" if $dotdir;

        my $node = \{
            USER    => $user,
            HOME    => $dir,
            LIST    => \{\},
            CLOBBER => 0,
        \};

        opendir(DIR, $dir)
                || croak "@\{[&whowasi]\}: can\*(Aqt opendir $dir: $!";
        foreach $dot ( grep /^\\./ && -f "$dir/$_", readdir(DIR)) \{
            $dot =~ s/^\\.//;
            $node->\{LIST\}\{$dot\} = undef;
        \}
        closedir DIR;
        return bless $node, $self;
    \}
.Ve
.Sp
It's probably worth mentioning that if you're going to filetest the
return values out of a readdir, you'd better prepend the directory
in question.  Otherwise, because we didn't **chdir()** there, it would
have been testing the wrong file.

- \s-1FETCH\s0 this, key
Xref "FETCH"
Item "FETCH this, key"
This method will be triggered every time an element in the tied hash is
accessed (read).  It takes one argument beyond its self reference: the key
whose value we're trying to fetch.
.Sp
Here's the fetch for our DotFiles example.
.Sp
.Vb 6
    sub FETCH \{
        carp &whowasi if $DEBUG;
        my $self = shift;
        my $dot = shift;
        my $dir = $self->\{HOME\};
        my $file = "$dir/.$dot";

        unless (exists $self->\{LIST\}->\{$dot\} || -f $file) \{
            carp "@\{[&whowasi]\}: no $dot file" if $DEBUG;
            return undef;
        \}

        if (defined $self->\{LIST\}->\{$dot\}) \{
            return $self->\{LIST\}->\{$dot\};
        \} else \{
            return $self->\{LIST\}->\{$dot\} = \`cat $dir/.$dot\`;
        \}
    \}
.Ve
.Sp
It was easy to write by having it call the Unix **cat**\|(1) command, but it
would probably be more portable to open the file manually (and somewhat
more efficient).  Of course, because dot files are a Unixy concept, we're
not that concerned.

- \s-1STORE\s0 this, key, value
Xref "STORE"
Item "STORE this, key, value"
This method will be triggered every time an element in the tied hash is set
(written).  It takes two arguments beyond its self reference: the index at
which we're trying to store something, and the value we're trying to put
there.
.Sp
Here in our DotFiles example, we'll be careful not to let
them try to overwrite the file unless they've called the **clobber()**
method on the original object reference returned by **tie()**.
.Sp
.Vb 7
    sub STORE \{
        carp &whowasi if $DEBUG;
        my $self = shift;
        my $dot = shift;
        my $value = shift;
        my $file = $self->\{HOME\} . "/.$dot";
        my $user = $self->\{USER\};

        croak "@\{[&whowasi]\}: $file not clobberable"
            unless $self->\{CLOBBER\};

        open(my $f, \*(Aq>\*(Aq, $file) || croak "can\*(Aqt open $file: $!";
        print $f $value;
        close($f);
    \}
.Ve
.Sp
If they wanted to clobber something, they might say:
.Sp
.Vb 3
    $ob = tie %daemon_dots, \*(Aqdaemon\*(Aq;
    $ob->clobber(1);
    $daemon_dots\{signature\} = "A true daemon\\n";
.Ve
.Sp
Another way to lay hands on a reference to the underlying object is to
use the **tied()** function, so they might alternately have set clobber
using:
.Sp
.Vb 2
    tie %daemon_dots, \*(Aqdaemon\*(Aq;
    tied(%daemon_dots)->clobber(1);
.Ve
.Sp
The clobber method is simply:
.Sp
.Vb 4
    sub clobber \{
        my $self = shift;
        $self->\{CLOBBER\} = @_ ? shift : 1;
    \}
.Ve

- \s-1DELETE\s0 this, key
Xref "DELETE"
Item "DELETE this, key"
This method is triggered when we remove an element from the hash,
typically by using the **delete()** function.  Again, we'll
be careful to check whether they really want to clobber files.
.Sp
.Vb 2
 sub DELETE   \{
     carp &whowasi if $DEBUG;

     my $self = shift;
     my $dot = shift;
     my $file = $self->\{HOME\} . "/.$dot";
     croak "@\{[&whowasi]\}: won\*(Aqt remove file $file"
         unless $self->\{CLOBBER\};
     delete $self->\{LIST\}->\{$dot\};
     my $success = unlink($file);
     carp "@\{[&whowasi]\}: can\*(Aqt unlink $file: $!" unless $success;
     $success;
 \}
.Ve
.Sp
The value returned by \s-1DELETE\s0 becomes the return value of the call
to **delete()**.  If you want to emulate the normal behavior of **delete()**,
you should return whatever \s-1FETCH\s0 would have returned for this key.
In this example, we have chosen instead to return a value which tells
the caller whether the file was successfully deleted.

- \s-1CLEAR\s0 this
Xref "CLEAR"
Item "CLEAR this"
This method is triggered when the whole hash is to be cleared, usually by
assigning the empty list to it.
.Sp
In our example, that would remove all the user's dot files!  It's such a
dangerous thing that they'll have to set \s-1CLOBBER\s0 to something higher than
1 to make it happen.
.Sp
.Vb 10
 sub CLEAR    \{
     carp &whowasi if $DEBUG;
     my $self = shift;
     croak "@\{[&whowasi]\}: won\*(Aqt remove all dot files for $self->\{USER\}"
         unless $self->\{CLOBBER\} > 1;
     my $dot;
     foreach $dot ( keys $self->\{LIST\}->%* ) \{
         $self->DELETE($dot);
     \}
 \}
.Ve

- \s-1EXISTS\s0 this, key
Xref "EXISTS"
Item "EXISTS this, key"
This method is triggered when the user uses the **exists()** function
on a particular hash.  In our example, we'll look at the \f(CW\*(C`\{LIST\}\*(C'
hash element for this:
.Sp
.Vb 6
    sub EXISTS   \{
        carp &whowasi if $DEBUG;
        my $self = shift;
        my $dot = shift;
        return exists $self->\{LIST\}->\{$dot\};
    \}
.Ve

- \s-1FIRSTKEY\s0 this
Xref "FIRSTKEY"
Item "FIRSTKEY this"
This method will be triggered when the user is going
to iterate through the hash, such as via a **keys()**, **values()**, or **each()** call.
.Sp
.Vb 6
    sub FIRSTKEY \{
        carp &whowasi if $DEBUG;
        my $self = shift;
        my $a = keys $self->\{LIST\}->%*;  # reset each() iterator
        each $self->\{LIST\}->%*
    \}
.Ve
.Sp
\s-1FIRSTKEY\s0 is always called in scalar context and it should just
return the first key.  **values()**, and **each()** in list context,
will call \s-1FETCH\s0 for the returned keys.

- \s-1NEXTKEY\s0 this, lastkey
Xref "NEXTKEY"
Item "NEXTKEY this, lastkey"
This method gets triggered during a **keys()**, **values()**, or **each()** iteration.  It has a
second argument which is the last key that had been accessed.  This is
useful if you're caring about ordering or calling the iterator from more
than one sequence, or not really storing things in a hash anywhere.
.Sp
\s-1NEXTKEY\s0 is always called in scalar context and it should just
return the next key.  **values()**, and **each()** in list context,
will call \s-1FETCH\s0 for the returned keys.
.Sp
For our example, we're using a real hash so we'll do just the simple
thing, but we'll have to go through the \s-1LIST\s0 field indirectly.
.Sp
.Vb 5
    sub NEXTKEY  \{
        carp &whowasi if $DEBUG;
        my $self = shift;
        return each $self->\{LIST\}->%*
    \}
.Ve
.Sp
If the object underlying your tied hash isn't a real hash and you don't have
\f(CW\*(C`each\*(C' available, then you should return \f(CW\*(C`undef\*(C' or the empty list once you've
reached the end of your list of keys. See \f(CW\*(C`each\*(Aqs own documentation\*(C'
for more details.

- \s-1SCALAR\s0 this
Xref "SCALAR"
Item "SCALAR this"
This is called when the hash is evaluated in scalar context, and in 5.28
onwards, by \f(CW\*(C`keys\*(C' in boolean context. In order to mimic the behaviour of
untied hashes, this method must return a value which when used as boolean,
indicates whether the tied hash is considered empty. If this method does
not exist, perl will make some educated guesses and return true when
the hash is inside an iteration. If this isn't the case, \s-1FIRSTKEY\s0 is
called, and the result will be a false value if \s-1FIRSTKEY\s0 returns the empty
list, true otherwise.
.Sp
However, you should **not** blindly rely on perl always doing the right
thing. Particularly, perl will mistakenly return true when you clear the
hash by repeatedly calling \s-1DELETE\s0 until it is empty. You are therefore
advised to supply your own \s-1SCALAR\s0 method when you want to be absolutely
sure that your hash behaves nicely in scalar context.
.Sp
In our example we can just call \f(CW\*(C`scalar\*(C' on the underlying hash
referenced by \f(CW\*(C`$self->\{LIST\}\*(C':
.Sp
.Vb 5
    sub SCALAR \{
        carp &whowasi if $DEBUG;
        my $self = shift;
        return scalar $self->\{LIST\}->%*
    \}
.Ve
.Sp
\s-1NOTE:\s0 In perl 5.25 the behavior of scalar \f(CW%hash on an untied hash changed
to return the count of keys. Prior to this it returned a string containing
information about the bucket setup of the hash. See
\*(L"bucket_ratio\*(R" in Hash::Util for a backwards compatibility path.

- \s-1UNTIE\s0 this
Xref "UNTIE"
Item "UNTIE this"
This is called when \f(CW\*(C`untie\*(C' occurs.  See "The \f(CW\*(C`untie\*(C' Gotcha" below.

- \s-1DESTROY\s0 this
Xref "DESTROY"
Item "DESTROY this"
This method is triggered when a tied hash is about to go out of
scope.  You don't really need it unless you're trying to add debugging
or have auxiliary state to clean up.  Here's a very simple function:
.Sp
.Vb 3
    sub DESTROY  \{
        carp &whowasi if $DEBUG;
    \}
.Ve

Note that functions such as **keys()** and **values()** may return huge lists
when used on large objects, like \s-1DBM\s0 files.  You may prefer to use the
**each()** function to iterate over such.  Example:

.Vb 7
    # print out history file offsets
    use NDBM_File;
    tie(%HIST, \*(AqNDBM_File\*(Aq, \*(Aq/usr/lib/news/history\*(Aq, 1, 0);
    while (($key,$val) = each %HIST) \{
        print $key, \*(Aq = \*(Aq, unpack(\*(AqL\*(Aq,$val), "\\n";
    \}
    untie(%HIST);
.Ve

### Tying FileHandles

Xref "filehandle, tying"
Subsection "Tying FileHandles"
This is partially implemented now.

A class implementing a tied filehandle should define the following
methods: \s-1TIEHANDLE,\s0 at least one of \s-1PRINT, PRINTF, WRITE, READLINE, GETC,
READ,\s0 and possibly \s-1CLOSE, UNTIE\s0 and \s-1DESTROY.\s0  The class can also provide: \s-1BINMODE,
OPEN, EOF, FILENO, SEEK, TELL\s0 - if the corresponding perl operators are
used on the handle.

When \s-1STDERR\s0 is tied, its \s-1PRINT\s0 method will be called to issue warnings
and error messages.  This feature is temporarily disabled during the call,
which means you can use \f(CW\*(C`warn()\*(C' inside \s-1PRINT\s0 without starting a recursive
loop.  And just like \f(CW\*(C`_\|_WARN_\|_\*(C' and \f(CW\*(C`_\|_DIE_\|_\*(C' handlers, \s-1STDERR\s0's \s-1PRINT\s0
method may be called to report parser errors, so the caveats mentioned under
\*(L"%SIG\*(R" in perlvar apply.

All of this is especially useful when perl is embedded in some other
program, where output to \s-1STDOUT\s0 and \s-1STDERR\s0 may have to be redirected
in some special way.  See nvi and the Apache module for examples.

When tying a handle, the first argument to \f(CW\*(C`tie\*(C' should begin with an
asterisk.  So, if you are tying \s-1STDOUT,\s0 use \f(CW*STDOUT.  If you have
assigned it to a scalar variable, say \f(CW$handle, use \f(CW*$handle.
\f(CW\*(C`tie $handle\*(C' ties the scalar variable \f(CW$handle, not the handle inside
it.

In our example we're going to create a shouting handle.

.Vb 1
    package Shout;
.Ve

- \s-1TIEHANDLE\s0 classname, \s-1LIST\s0
Xref "TIEHANDLE"
Item "TIEHANDLE classname, LIST"
This is the constructor for the class.  That means it is expected to
return a blessed reference of some sort. The reference can be used to
hold some internal information.
.Sp
.Vb 1
    sub TIEHANDLE \{ print "<shout>\\n"; my $i; bless \\$i, shift \}
.Ve

- \s-1WRITE\s0 this, \s-1LIST\s0
Xref "WRITE"
Item "WRITE this, LIST"
This method will be called when the handle is written to via the
\f(CW\*(C`syswrite\*(C' function.
.Sp
.Vb 5
 sub WRITE \{
     $r = shift;
     my($buf,$len,$offset) = @_;
     print "WRITE called, \\$buf=$buf, \\$len=$len, \\$offset=$offset";
 \}
.Ve

- \s-1PRINT\s0 this, \s-1LIST\s0
Xref "PRINT"
Item "PRINT this, LIST"
This method will be triggered every time the tied handle is printed to
with the \f(CW\*(C`print()\*(C' or \f(CW\*(C`say()\*(C' functions.  Beyond its self reference
it also expects the list that was passed to the print function.
.Sp
.Vb 1
  sub PRINT \{ $r = shift; $$r++; print join($,,map(uc($_),@_)),$\\ \}
.Ve
.Sp
\f(CW\*(C`say()\*(C' acts just like \f(CW\*(C`print()\*(C' except $\\ will be localized to \f(CW\*(C`\\n\*(C' so
you need do nothing special to handle \f(CW\*(C`say()\*(C' in \f(CW\*(C`PRINT()\*(C'.

- \s-1PRINTF\s0 this, \s-1LIST\s0
Xref "PRINTF"
Item "PRINTF this, LIST"
This method will be triggered every time the tied handle is printed to
with the \f(CW\*(C`printf()\*(C' function.
Beyond its self reference it also expects the format and list that was
passed to the printf function.
.Sp
.Vb 5
    sub PRINTF \{
        shift;
        my $fmt = shift;
        print sprintf($fmt, @_);
    \}
.Ve

- \s-1READ\s0 this, \s-1LIST\s0
Xref "READ"
Item "READ this, LIST"
This method will be called when the handle is read from via the \f(CW\*(C`read\*(C'
or \f(CW\*(C`sysread\*(C' functions.
.Sp
.Vb 8
 sub READ \{
   my $self = shift;
   my $bufref = \\$_[0];
   my(undef,$len,$offset) = @_;
   print "READ called, \\$buf=$bufref, \\$len=$len, \\$offset=$offset";
   # add to $$bufref, set $len to number of characters read
   $len;
 \}
.Ve

- \s-1READLINE\s0 this
Xref "READLINE"
Item "READLINE this"
This method is called when the handle is read via \f(CW\*(C`<HANDLE>\*(C'
or \f(CW\*(C`readline HANDLE\*(C'.
.Sp
As per \f(CW\*(C`readline\*(C', in scalar context it should return
the next line, or \f(CW\*(C`undef\*(C' for no more data.  In list context it should
return all remaining lines, or an empty list for no more data.  The strings
returned should include the input record separator \f(CW$/ (see perlvar),
unless it is \f(CW\*(C`undef\*(C' (which means \*(L"slurp\*(R" mode).
.Sp
.Vb 10
    sub READLINE \{
      my $r = shift;
      if (wantarray) \{
        return ("all remaining\\n",
                "lines up\\n",
                "to eof\\n");
      \} else \{
        return "READLINE called " . ++$$r . " times\\n";
      \}
    \}
.Ve

- \s-1GETC\s0 this
Xref "GETC"
Item "GETC this"
This method will be called when the \f(CW\*(C`getc\*(C' function is called.
.Sp
.Vb 1
    sub GETC \{ print "Don\*(Aqt GETC, Get Perl"; return "a"; \}
.Ve

- \s-1EOF\s0 this
Xref "EOF"
Item "EOF this"
This method will be called when the \f(CW\*(C`eof\*(C' function is called.
.Sp
Starting with Perl 5.12, an additional integer parameter will be passed.  It
will be zero if \f(CW\*(C`eof\*(C' is called without parameter; \f(CW1 if \f(CW\*(C`eof\*(C' is given
a filehandle as a parameter, e.g. \f(CW\*(C`eof(FH)\*(C'; and \f(CW2 in the very special
case that the tied filehandle is \f(CW\*(C`ARGV\*(C' and \f(CW\*(C`eof\*(C' is called with an empty
parameter list, e.g. \f(CW\*(C`eof()\*(C'.
.Sp
.Vb 1
    sub EOF \{ not length $stringbuf \}
.Ve

- \s-1CLOSE\s0 this
Xref "CLOSE"
Item "CLOSE this"
This method will be called when the handle is closed via the \f(CW\*(C`close\*(C'
function.
.Sp
.Vb 1
    sub CLOSE \{ print "CLOSE called.\\n" \}
.Ve

- \s-1UNTIE\s0 this
Xref "UNTIE"
Item "UNTIE this"
As with the other types of ties, this method will be called when \f(CW\*(C`untie\*(C' happens.
It may be appropriate to \*(L"auto \s-1CLOSE\*(R"\s0 when this occurs.  See
"The \f(CW\*(C`untie\*(C' Gotcha" below.

- \s-1DESTROY\s0 this
Xref "DESTROY"
Item "DESTROY this"
As with the other types of ties, this method will be called when the
tied handle is about to be destroyed. This is useful for debugging and
possibly cleaning up.
.Sp
.Vb 1
    sub DESTROY \{ print "</shout>\\n" \}
.Ve

Here's how to use our little example:

.Vb 5
    tie(*FOO,\*(AqShout\*(Aq);
    print FOO "hello\\n";
    $a = 4; $b = 6;
    print FOO $a, " plus ", $b, " equals ", $a + $b, "\\n";
    print <FOO>;
.Ve

### \s-1UNTIE\s0 this

Xref "UNTIE"
Subsection "UNTIE this"
You can define for all tie types an \s-1UNTIE\s0 method that will be called
at **untie()**.  See "The \f(CW\*(C`untie\*(C' Gotcha" below.
.ie n .SS "The ""untie"" Gotcha"
.el .SS "The \f(CWuntie Gotcha"
Xref "untie"
Subsection "The untie Gotcha"
If you intend making use of the object returned from either **tie()** or
**tied()**, and if the tie's target class defines a destructor, there is a
subtle gotcha you *must* guard against.

As setup, consider this (admittedly rather contrived) example of a
tie; all it does is use a file to keep a log of the values assigned to
a scalar.

.Vb 1
    package Remember;

    use strict;
    use warnings;
    use IO::File;

    sub TIESCALAR \{
        my $class = shift;
        my $filename = shift;
        my $handle = IO::File->new( "> $filename" )
                         or die "Cannot open $filename: $!\\n";

        print $handle "The Start\\n";
        bless \{FH => $handle, Value => 0\}, $class;
    \}

    sub FETCH \{
        my $self = shift;
        return $self->\{Value\};
    \}

    sub STORE \{
        my $self = shift;
        my $value = shift;
        my $handle = $self->\{FH\};
        print $handle "$value\\n";
        $self->\{Value\} = $value;
    \}

    sub DESTROY \{
        my $self = shift;
        my $handle = $self->\{FH\};
        print $handle "The End\\n";
        close $handle;
    \}

    1;
.Ve

Here is an example that makes use of this tie:

.Vb 2
    use strict;
    use Remember;

    my $fred;
    tie $fred, \*(AqRemember\*(Aq, \*(Aqmyfile.txt\*(Aq;
    $fred = 1;
    $fred = 4;
    $fred = 5;
    untie $fred;
    system "cat myfile.txt";
.Ve

This is the output when it is executed:

.Vb 5
    The Start
    1
    4
    5
    The End
.Ve

So far so good.  Those of you who have been paying attention will have
spotted that the tied object hasn't been used so far.  So lets add an
extra method to the Remember class to allow comments to be included in
the file; say, something like this:

.Vb 6
    sub comment \{
        my $self = shift;
        my $text = shift;
        my $handle = $self->\{FH\};
        print $handle $text, "\\n";
    \}
.Ve

And here is the previous example modified to use the \f(CW\*(C`comment\*(C' method
(which requires the tied object):

.Vb 2
    use strict;
    use Remember;

    my ($fred, $x);
    $x = tie $fred, \*(AqRemember\*(Aq, \*(Aqmyfile.txt\*(Aq;
    $fred = 1;
    $fred = 4;
    comment $x "changing...";
    $fred = 5;
    untie $fred;
    system "cat myfile.txt";
.Ve

When this code is executed there is no output.  Here's why:

When a variable is tied, it is associated with the object which is the
return value of the \s-1TIESCALAR, TIEARRAY,\s0 or \s-1TIEHASH\s0 function.  This
object normally has only one reference, namely, the implicit reference
from the tied variable.  When **untie()** is called, that reference is
destroyed.  Then, as in the first example above, the object's
destructor (\s-1DESTROY\s0) is called, which is normal for objects that have
no more valid references; and thus the file is closed.

In the second example, however, we have stored another reference to
the tied object in \f(CW$x.  That means that when **untie()** gets called
there will still be a valid reference to the object in existence, so
the destructor is not called at that time, and thus the file is not
closed.  The reason there is no output is because the file buffers
have not been flushed to disk.

Now that you know what the problem is, what can you do to avoid it?
Prior to the introduction of the optional \s-1UNTIE\s0 method the only way
was the good old \f(CW\*(C`-w\*(C' flag. Which will spot any instances where you call
**untie()** and there are still valid references to the tied object.  If
the second script above this near the top \f(CW\*(C`use warnings \*(Aquntie\*(Aq\*(C'
or was run with the \f(CW\*(C`-w\*(C' flag, Perl prints this
warning message:

.Vb 1
    untie attempted while 1 inner references still exist
.Ve

To get the script to work properly and silence the warning make sure
there are no valid references to the tied object *before* **untie()** is
called:

.Vb 2
    undef $x;
    untie $fred;
.Ve

Now that \s-1UNTIE\s0 exists the class designer can decide which parts of the
class functionality are really associated with \f(CW\*(C`untie\*(C' and which with
the object being destroyed. What makes sense for a given class depends
on whether the inner references are being kept so that non-tie-related
methods can be called on the object. But in most cases it probably makes
sense to move the functionality that would have been in \s-1DESTROY\s0 to the \s-1UNTIE\s0
method.

If the \s-1UNTIE\s0 method exists then the warning above does not occur. Instead the
\s-1UNTIE\s0 method is passed the count of \*(L"extra\*(R" references and can issue its own
warning if appropriate. e.g. to replicate the no \s-1UNTIE\s0 case this method can
be used:

.Vb 6
 sub UNTIE
 \{
  my ($obj,$count) = @_;
  carp "untie attempted while $count inner references still exist"
                                                              if $count;
 \}
.Ve

## SEE ALSO

Header "SEE ALSO"
See DB_File or Config for some interesting **tie()** implementations.
A good starting point for many **tie()** implementations is with one of the
modules Tie::Scalar, Tie::Array, Tie::Hash, or Tie::Handle.

## BUGS

Header "BUGS"
The normal return provided by \f(CW\*(C`scalar(%hash)\*(C' is not
available.  What this means is that using \f(CW%tied_hash in boolean
context doesn't work right (currently this always tests false,
regardless of whether the hash is empty or hash elements).
[ This paragraph needs review in light of changes in 5.25 ]

Localizing tied arrays or hashes does not work.  After exiting the
scope the arrays or the hashes are not restored.

Counting the number of entries in a hash via \f(CW\*(C`scalar(keys(%hash))\*(C'
or \f(CW\*(C`scalar(values(%hash)\*(C') is inefficient since it needs to iterate
through all the entries with \s-1FIRSTKEY/NEXTKEY.\s0

Tied hash/array slices cause multiple \s-1FETCH/STORE\s0 pairs, there are no
tie methods for slice operations.

You cannot easily tie a multilevel data structure (such as a hash of
hashes) to a dbm file.  The first problem is that all but \s-1GDBM\s0 and
Berkeley \s-1DB\s0 have size limitations, but beyond that, you also have problems
with how references are to be represented on disk.  One
module that does attempt to address this need is DBM::Deep.  Check your
nearest \s-1CPAN\s0 site as described in perlmodlib for source code.  Note
that despite its name, DBM::Deep does not use dbm.  Another earlier attempt
at solving the problem is \s-1MLDBM,\s0 which is also available on the \s-1CPAN,\s0 but
which has some fairly serious limitations.

Tied filehandles are still incomplete.  **sysopen()**, **truncate()**,
**flock()**, **fcntl()**, **stat()** and -X can't currently be trapped.

## AUTHOR

Header "AUTHOR"
Tom Christiansen

\s-1TIEHANDLE\s0 by Sven Verdoolaege <*skimo@dns.ufsia.ac.be*> and Doug MacEachern <*dougm@osf.org*>

\s-1UNTIE\s0 by Nick Ing-Simmons <*nick@ing-simmons.net*>

\s-1SCALAR\s0 by Tassilo von Parseval <*tassilo.von.parseval@rwth-aachen.de*>

Tying Arrays by Casey West <*casey@geeknest.com*>
