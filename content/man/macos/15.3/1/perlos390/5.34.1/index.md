+++
manpage_format = "troff"
author = "None Specified"
description = "This is a fully ported Perl for s-1OS/390s0 Version 2 Release 3, 5, 6, 7, 8, and 9.  It may work on other versions or releases, but those are the ones we have tested it on. You may need to carry out some system configuration tasks before running t..."
operating_system = "macos"
manpage_section = "1"
date = "2022-02-19"
detected_package_version = "5.34.1"
operating_system_version = "15.3"
keywords = ["header", "see", "also", "s-1install", "s0", "perlport", "perlebcdic", "extutils", "makemaker", "vb", "1", "http", "www", "ibm", "com", "servers", "eserver", "zseries", "zos", "unix", "bpxa1toy", "html", "redbooks", "sg245944", "bpxa1ty1", "opensrc", "xray", "mpe", "mpg", "de", "mailing-lists", "perl-mvs", "publibz", "boulder", "80", "cgi-bin", "bookmgr_os390", "books", "ceea3030", "cbcug030", "ve", "mailing", "list", "for", "perl", "on", "s-1os", "390", "subsection", "os", "if", "you", "are", "interested", "in", "the", "z", "formerly", "known", "as", "and", "posix-bc", "s-1bs2000", "ports", "of", "then", "to", "subscribe", "send", "an", "empty", "message", "perl-mvs-subscribe", "org", "https", "lists", "there", "web", "archives", "at", "nntp", "group", "mvs", "history", "this", "document", "was", "originally", "written", "by", "david", "fiander", "5", "005", "release", "podified", "005_03", "11", "march", "1999", "updated", "12", "november", "2000", "7", "15", "january", "2001", "24", "mention", "dynamic", "loading", "s-1sys1", "tcpparms", "s-1tcpdata", "28", "broken", "urls", "03", "october", "2019", "perl-5", "33", "3"]
manpage_name = "perlos390"
title = "perlos390(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLOS390 1"
PERLOS390 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlos390 - building and installing Perl for OS/390 and z/OS

## SYNOPSIS

Header "SYNOPSIS"
This document will help you Configure, build, test and install Perl
on \s-1OS/390\s0 (aka z/OS) Unix System Services.

\fBThis document needs to be updated, but we don't know what it should say.
Please submit comments to <https://github.com/Perl/perl5/issues>.

## DESCRIPTION

Header "DESCRIPTION"
This is a fully ported Perl for \s-1OS/390\s0 Version 2 Release 3, 5, 6, 7,
8, and 9.  It may work on other versions or releases, but those are
the ones we have tested it on.

You may need to carry out some system configuration tasks before
running the Configure script for Perl.

### Tools

Subsection "Tools"
The z/OS Unix Tools and Toys list may prove helpful and contains links
to ports of much of the software helpful for building Perl.
<http://www.ibm.com/servers/eserver/zseries/zos/unix/bpxa1toy.html>

### Unpacking Perl distribution on \s-1OS/390\s0

Subsection "Unpacking Perl distribution on OS/390"
If using ftp remember to transfer the distribution in binary format.

Gunzip/gzip for \s-1OS/390\s0 is discussed at:

.Vb 1
  http://www.ibm.com/servers/eserver/zseries/zos/unix/bpxa1ty1.html
.Ve

to extract an \s-1ASCII\s0 tar archive on \s-1OS/390,\s0 try this:

.Vb 1
   pax -o to=IBM-1047,from=ISO8859-1 -r < latest.tar
.Ve

or

.Vb 1
   zcat latest.tar.Z | pax -o to=IBM-1047,from=ISO8859-1 -r
.Ve

If you get lots of errors of the form

.Vb 1
 tar: FSUM7171 ...: cannot set uid/gid: EDC5139I Operation not permitted
.Ve

you did not read the above and tried to use tar instead of pax, you'll
first have to remove the (now corrupt) perl directory

.Vb 1
   rm -rf perl-...
.Ve

and then use pax.

### Setup and utilities for Perl on \s-1OS/390\s0

Subsection "Setup and utilities for Perl on OS/390"
Be sure that your yacc installation is in place including any necessary
parser template files. If you have not already done so then be sure to:

.Vb 1
  cp /samples/yyparse.c /etc
.Ve

This may also be a good time to ensure that your /etc/protocol file
and either your /etc/resolv.conf or /etc/hosts files are in place.
The \s-1IBM\s0 document that described such \s-1USS\s0 system setup issues was
\s-1SC28-1890-07 \*(L"OS/390 UNIX\s0 System Services Planning\*(R", in particular
Chapter 6 on customizing the \s-1OE\s0 shell.

\s-1GNU\s0 make for \s-1OS/390,\s0 which is recommended for the build of perl (as
well as building \s-1CPAN\s0 modules and extensions), is available from the
\*(L"Tools\*(R".

Some people have reported encountering \*(L"Out of memory!\*(R" errors while
trying to build Perl using \s-1GNU\s0 make binaries.  If you encounter such
trouble then try to download the source code kit and build \s-1GNU\s0 make
from source to eliminate any such trouble.  You might also find \s-1GNU\s0 make
(as well as Perl and Apache) in the red-piece/book \*(L"Open Source Software
for \s-1OS/390 UNIX\*(R", SG24-5944-00\s0 from \s-1IBM.\s0

If instead of the recommended \s-1GNU\s0 make you would like to use the system
supplied make program then be sure to install the default rules file
properly via the shell command:

.Vb 1
    cp /samples/startup.mk /etc
.Ve

and be sure to also set the environment variable _C89_CCMODE=1 (exporting
_C89_CCMODE=1 is also a good idea for users of \s-1GNU\s0 make).

You might also want to have \s-1GNU\s0 groff for \s-1OS/390\s0 installed before
running the \*(L"make install\*(R" step for Perl.

There is a syntax error in the /usr/include/sys/socket.h header file
that \s-1IBM\s0 supplies with \s-1USS V2R7, V2R8,\s0 and possibly V2R9.  The problem with
the header file is that near the definition of the \s-1SO_REUSEPORT\s0 constant
there is a spurious extra '/' character outside of a comment like so:

.Vb 2
 #define SO_REUSEPORT    0x0200    /* allow local address & port
                                      reuse */                    /
.Ve

You could edit that header yourself to remove that last '/', or you might
note that Language Environment (\s-1LE\s0) \s-1APAR PQ39997\s0 describes the problem
and \s-1PTF\s0's \s-1UQ46272\s0 and \s-1UQ46271\s0 are the (R8 at least) fixes and apply them.
If left unattended that syntax error will turn up as an inability for Perl
to build its \*(L"Socket\*(R" extension.

For successful testing you may need to turn on the sticky bit for your
world readable /tmp directory if you have not already done so (see man chmod).

### Configure Perl on \s-1OS/390\s0

Subsection "Configure Perl on OS/390"
Once you have unpacked the distribution, run \*(L"sh Configure\*(R" (see \s-1INSTALL\s0
for a full discussion of the Configure options).  There is a \*(L"hints\*(R" file
for os390 that specifies the correct values for most things.  Some things
to watch out for include:

*Shell*
Subsection "Shell"

A message of the form:

.Vb 2
 (I see you are using the Korn shell.  Some ksh\*(Aqs blow up on Configure,
 mainly on older exotic systems.  If yours does, try the Bourne shell instead.)
.Ve

is nothing to worry about at all.

*Samples*
Subsection "Samples"

Some of the parser default template files in /samples are needed in /etc.
In particular be sure that you at least copy /samples/yyparse.c to /etc
before running Perl's Configure.  This step ensures successful extraction
of \s-1EBCDIC\s0 versions of parser files such as perly.c and perly.h.
This has to be done before running Configure the first time.  If you failed
to do so then the easiest way to re-Configure Perl is to delete your
misconfigured build root and re-extract the source from the tar ball.
Then you must ensure that /etc/yyparse.c is properly in place before
attempting to re-run Configure.

*Dynamic loading*
Subsection "Dynamic loading"

Dynamic loading is required if you want to use \s-1XS\s0 modules from \s-1CPAN\s0 (like
\s-1DBI\s0 (and \s-1DBD\s0's), \s-1JSON::XS,\s0 and Text::CSV_XS) or update \s-1CORE\s0 modules from
\s-1CPAN\s0 with newer versions (like Encode) without rebuilding all of the perl
binary.

This port will support dynamic loading, but it is not selected by
default.  If you would like to experiment with dynamic loading then
be sure to specify -Dusedl in the arguments to the Configure script.
See the comments in hints/os390.sh for more information on dynamic loading.
If you build with dynamic loading then you will need to add the
\f(CW$archlibexp/CORE directory to your \s-1LIBPATH\s0 environment variable in order
for perl to work.  See the config.sh file for the value of \f(CW$archlibexp.
If in trying to use Perl you see an error message similar to:

.Vb 3
 CEE3501S The module libperl.dll was not found.
   From entry point _\|_dllstaticinit at compile unit offset +00000194
   at
.Ve

then your \s-1LIBPATH\s0 does not have the location of libperl.x and either
libperl.dll or libperl.so in it.  Add that directory to your \s-1LIBPATH\s0 and
proceed.

In hints/os390.sh, selecting -Dusedl will default to *also* select
-Duseshrplib.  Having a shared plib not only requires \s-1LIBPATH\s0 to be set to
the correct location of libperl.so but also makes it close to impossible
to run more than one different perl that was built this way at the same
time.

All objects that are involved in -Dusedl builds should be compiled for
this, probably by adding to all ccflags

.Vb 1
 -qexportall -qxplink -qdll -Wc,XPLINK,dll,EXPORTALL -Wl,XPLINK,dll
.Ve

*Optimizing*
Subsection "Optimizing"

Do not turn on the compiler optimization flag \*(L"-O\*(R".  There is
a bug in either the optimizer or perl that causes perl to
not work correctly when the optimizer is on.

*Config files*
Subsection "Config files"

Some of the configuration files in /etc used by the
networking APIs are either missing or have the wrong
names.  In particular, make sure that there's either
an /etc/resolv.conf or an /etc/hosts, so that
**gethostbyname()** works, and make sure that the file
/etc/proto has been renamed to /etc/protocol (\s-1NOT\s0
/etc/protocols, as used by other Unix systems).
You may have to look for things like \s-1HOSTNAME\s0 and \s-1DOMAINORIGIN\s0
in the \*(L"//'\s-1SYS1.TCPPARMS\s0(\s-1TCPDATA\s0)'\*(R" \s-1PDS\s0 member in order to
properly set up your /etc networking files.

### Build, Test, Install Perl on \s-1OS/390\s0

Subsection "Build, Test, Install Perl on OS/390"
Simply put:

.Vb 3
    sh Configure
    make
    make test
.Ve

if everything looks ok (see the next section for test/IVP diagnosis) then:

.Vb 1
    make install
.Ve

this last step may or may not require UID=0 privileges depending
on how you answered the questions that Configure asked and whether
or not you have write access to the directories you specified.

### Build Anomalies with Perl on \s-1OS/390\s0

Subsection "Build Anomalies with Perl on OS/390"
\*(L"Out of memory!\*(R" messages during the build of Perl are most often fixed
by re building the \s-1GNU\s0 make utility for \s-1OS/390\s0 from a source code kit.

Building debugging-enabled binaries (with -g or -g3) will increase the
chance of getting these errors. Prevent -g if possible.

Another memory limiting item to check is your \s-1MAXASSIZE\s0 parameter in your
'\s-1SYS1.PARMLIB\s0(BPXPRMxx)' data set (note too that as of V2R8 address space
limits can be set on a per user \s-1ID\s0 basis in the \s-1USS\s0 segment of a \s-1RACF\s0
profile).  People have reported successful builds of Perl with \s-1MAXASSIZE\s0
parameters as small as 503316480 (and it may be possible to build Perl
with a \s-1MAXASSIZE\s0 smaller than that).

Within \s-1USS\s0 your /etc/profile or \f(CW$HOME/.profile may limit your ulimit
settings.  Check that the following command returns reasonable values:

.Vb 1
    ulimit -a
.Ve

To conserve memory you should have your compiler modules loaded into the
Link Pack Area (\s-1LPA/ELPA\s0) rather than in a link list or step lib.

If the c89 compiler complains of syntax errors during the build of the
Socket extension then be sure to fix the syntax error in the system
header /usr/include/sys/socket.h.

### Testing Anomalies with Perl on \s-1OS/390\s0

Subsection "Testing Anomalies with Perl on OS/390"
The \*(L"make test\*(R" step runs a Perl Verification Procedure, usually before
installation.  You might encounter \s-1STDERR\s0 messages even during a successful
run of \*(L"make test\*(R".  Here is a guide to some of the more commonly seen
anomalies:

*Signals*
Subsection "Signals"

A message of the form:

.Vb 4
 io/openpid...........CEE5210S The signal SIGHUP was received.
 CEE5210S The signal SIGHUP was received.
 CEE5210S The signal SIGHUP was received.
 ok
.Ve

indicates that the t/io/openpid.t test of Perl has passed but done so
with extraneous messages on stderr from \s-1CEE.\s0

*File::Temp*
Subsection "File::Temp"

A message of the form:

.Vb 6
 lib/ftmp-security....File::Temp::_gettemp: Parent directory (/tmp/)
 is not safe (sticky bit not set when world writable?) at
 lib/ftmp-security.t line 100
 File::Temp::_gettemp: Parent directory (/tmp/) is not safe (sticky
 bit not set when world writable?) at lib/ftmp-security.t line 100
 ok
.Ve

indicates a problem with the permissions on your /tmp directory within the \s-1HFS.\s0
To correct that problem issue the command:

.Vb 1
    chmod a+t /tmp
.Ve

from an account with write access to the directory entry for /tmp.

*Out of Memory!*
Subsection "Out of Memory!"

Recent perl test suite is quite memory hungry. In addition to the comments
above on memory limitations it is also worth checking for _CEE_RUNOPTS
in your environment. Perl now has (in miniperlmain.c) a C #pragma
to set \s-1CEE\s0 run options, but the environment variable wins.

The C code asks for:

.Vb 1
 #pragma runopts(HEAP(2M,500K,ANYWHERE,KEEP,8K,4K) STACK(,,ANY,) ALL31(ON))
.Ve

The important parts of that are the second argument (the increment) to \s-1HEAP,\s0
and allowing the stack to be \*(L"Above the (16M) line\*(R". If the heap
increment is too small then when perl (for example loading unicode/Name.pl) tries
to create a \*(L"big\*(R" (400K+) string it cannot fit in a single segment
and you get \*(L"Out of Memory!\*(R" - even if there is still plenty of memory
available.

A related issue is use with perl's malloc. Perl's malloc uses \f(CW\*(C`sbrk()\*(C'
to get memory, and \f(CW\*(C`sbrk()\*(C' is limited to the first allocation so in this
case something like:

.Vb 1
  HEAP(8M,500K,ANYWHERE,KEEP,8K,4K)
.Ve

is needed to get through the test suite.

### Installation Anomalies with Perl on \s-1OS/390\s0

Subsection "Installation Anomalies with Perl on OS/390"
The installman script will try to run on \s-1OS/390.\s0  There will be fewer errors
if you have a roff utility installed.  You can obtain \s-1GNU\s0 groff from the
Redbook \s-1SG24-5944-00\s0 ftp site.

### Usage Hints for Perl on \s-1OS/390\s0

Subsection "Usage Hints for Perl on OS/390"
When using perl on \s-1OS/390\s0 please keep in mind that the \s-1EBCDIC\s0 and \s-1ASCII\s0
character sets are different.  See perlebcdic.pod for more on such character
set issues.  Perl builtin functions that may behave differently under
\s-1EBCDIC\s0 are also mentioned in the perlport.pod document.

Open Edition (\s-1UNIX\s0 System Services) from V2R8 onward does support
#!/path/to/perl script invocation.  There is a \s-1PTF\s0 available from
\s-1IBM\s0 for V2R7 that will allow shell/kernel support for #!.  \s-1USS\s0
releases prior to V2R7 did not support the #! means of script invocation.
If you are running V2R6 or earlier then see:

.Vb 1
    head \`whence perldoc\`
.Ve

for an example of how to use the \*(L"eval exec\*(R" trick to ask the shell to
have Perl run your scripts on those older releases of Unix System Services.

If you are having trouble with square brackets then consider switching your
rlogin or telnet client.  Try to avoid older 3270 emulators and \s-1ISHELL\s0 for
working with Perl on \s-1USS.\s0

### Floating Point Anomalies with Perl on \s-1OS/390\s0

Subsection "Floating Point Anomalies with Perl on OS/390"
There appears to be a bug in the floating point implementation on S/390
systems such that calling **int()** on the product of a number and a small
magnitude number is not the same as calling **int()** on the quotient of
that number and a large magnitude number.  For example, in the following
Perl code:

.Vb 4
    my $x = 100000.0;
    my $y = int($x * 1e-5) * 1e5; # \*(Aq0\*(Aq
    my $z = int($x / 1e+5) * 1e5;  # \*(Aq100000\*(Aq
    print "\\$y is $y and \\$z is $z\\n"; # $y is 0 and $z is 100000
.Ve

Although one would expect the quantities \f(CW$y and \f(CW$z to be the same and equal
to 100000 they will differ and instead will be 0 and 100000 respectively.

The problem can be further examined in a roughly equivalent C program:

.Vb 10
    #include <stdio.h>
    #include <math.h>
    main()
    \{
    double r1,r2;
    double x = 100000.0;
    double y = 0.0;
    double z = 0.0;
    x = 100000.0 * 1e-5;
    r1 = modf (x,&y);
    x = 100000.0 / 1e+5;
    r2 = modf (x,&z);
    printf("y is %e and z is %e\\n",y*1e5,z*1e5);
    /* y is 0.000000e+00 and z is 1.000000e+05 (with c89) */
    \}
.Ve

### Modules and Extensions for Perl on \s-1OS/390\s0

Subsection "Modules and Extensions for Perl on OS/390"
Pure Perl (that is non \s-1XS\s0) modules may be installed via the usual:

.Vb 4
    perl Makefile.PL
    make
    make test
    make install
.Ve

If you built perl with dynamic loading capability then that would also
be the way to build \s-1XS\s0 based extensions.  However, if you built perl with
the default static linking you can still build \s-1XS\s0 based extensions for \s-1OS/390\s0
but you will need to follow the instructions in ExtUtils::MakeMaker for
building statically linked perl binaries.  In the simplest configurations
building a static perl + \s-1XS\s0 extension boils down to:

.Vb 6
    perl Makefile.PL
    make
    make perl
    make test
    make install
    make -f Makefile.aperl inst_perl MAP_TARGET=perl
.Ve

In most cases people have reported better results with \s-1GNU\s0 make rather
than the system's /bin/make program, whether for plain modules or for
\s-1XS\s0 based extensions.

If the make process encounters trouble with either compilation or
linking then try setting the _C89_CCMODE to 1.  Assuming sh is your
login shell then run:

.Vb 1
    export _C89_CCMODE=1
.Ve

If tcsh is your login shell then use the setenv command.

## AUTHORS

Header "AUTHORS"
David Fiander and Peter Prymmer with thanks to Dennis Longnecker
and William Raffloer for valuable reports, \s-1LPAR\s0 and \s-1PTF\s0 feedback.
Thanks to Mike MacIsaac and Egon Terwedow for \s-1SG24-5944-00.\s0
Thanks to Ignasi Roca for pointing out the floating point problems.
Thanks to John Goodyear for dynamic loading help.

## SEE ALSO

Header "SEE ALSO"
\s-1INSTALL\s0, perlport, perlebcdic, ExtUtils::MakeMaker.

.Vb 1
 http://www.ibm.com/servers/eserver/zseries/zos/unix/bpxa1toy.html

 http://www.redbooks.ibm.com/redbooks/SG245944.html

 http://www.ibm.com/servers/eserver/zseries/zos/unix/bpxa1ty1.html#opensrc

 http://www.xray.mpe.mpg.de/mailing-lists/perl-mvs/

 http://publibz.boulder.ibm.com:80/cgi-bin/bookmgr_OS390/BOOKS/ceea3030/

 http://publibz.boulder.ibm.com:80/cgi-bin/bookmgr_OS390/BOOKS/CBCUG030/
.Ve

### Mailing list for Perl on \s-1OS/390\s0

Subsection "Mailing list for Perl on OS/390"
If you are interested in the z/OS (formerly known as \s-1OS/390\s0)
and POSIX-BC (\s-1BS2000\s0) ports of Perl then see the perl-mvs mailing list.
To subscribe, send an empty message to perl-mvs-subscribe@perl.org.

See also:

.Vb 1
    https://lists.perl.org/list/perl-mvs.html
.Ve

There are web archives of the mailing list at:

.Vb 1
    https://www.nntp.perl.org/group/perl.mvs/
.Ve

## HISTORY

Header "HISTORY"
This document was originally written by David Fiander for the 5.005
release of Perl.

This document was podified for the 5.005_03 release of Perl 11 March 1999.

Updated 12 November 2000 for the 5.7.1 release of Perl.

Updated 15 January  2001 for the 5.7.1 release of Perl.

Updated 24 January  2001 to mention dynamic loading.

Updated 12 March    2001 to mention //'\s-1SYS1.TCPPARMS\s0(\s-1TCPDATA\s0)'.

Updated 28 November 2001 for broken URLs.

Updated 03 October  2019 for perl-5.33.3+
