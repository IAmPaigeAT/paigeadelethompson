+++
date = "2022-02-19"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
title = "perl5141delta(1)"
description = "This document describes differences between the 5.14.0 release and the 5.14.1 release. If you are upgrading from an earlier release such as 5.12.0, first read perl5140delta, which describes differences between 5.12.0 and 5.14.0. No changes since ..."
author = "None Specified"
manpage_section = "1"
operating_system = "macos"
detected_package_version = "5.34.1"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_name = "perl5141delta"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5141DELTA 1"
PERL5141DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5141delta - what is new for perl v5.14.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.14.0 release and
the 5.14.1 release.

If you are upgrading from an earlier release such as 5.12.0, first read
perl5140delta, which describes differences between 5.12.0 and
5.14.0.

## Core Enhancements

Header "Core Enhancements"
No changes since 5.14.0.

## Security

Header "Security"
No changes since 5.14.0.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.14.0. If any
exist, they are bugs and reports are welcome.

## Deprecations

Header "Deprecations"
There have been no deprecations since 5.14.0.

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules and Pragmata

Subsection "New Modules and Pragmata"
None

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
B::Deparse has been upgraded from version 1.03 to 1.04, to address two
regressions in Perl 5.14.0:
.Sp
Deparsing of the \f(CW\*(C`glob\*(C' operator and its diamond (\f(CW\*(C`<>\*(C') form now
works again. [perl #90898]
.Sp
The presence of subroutines named \f(CW\*(C`::::\*(C' or \f(CW\*(C`::::::\*(C' no longer causes
B::Deparse to hang.

- \(bu
Pod::Perldoc has been upgraded from version 3.15_03 to 3.15_04.
.Sp
It corrects the search paths on \s-1VMS.\s0 [perl #90640]

### Removed Modules and Pragmata

Subsection "Removed Modules and Pragmata"
None

## Documentation

Header "Documentation"

### New Documentation

Subsection "New Documentation"
None

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perlfunc*
Subsection "perlfunc"

- \(bu
\f(CW\*(C`given\*(C', \f(CW\*(C`when\*(C' and \f(CW\*(C`default\*(C' are now listed in perlfunc.

- \(bu
Documentation for \f(CW\*(C`use\*(C' now includes a pointer to *if.pm*.

*perllol*
Subsection "perllol"

- \(bu
perllol has been expanded with examples using the new \f(CW\*(C`push $scalar\*(C'
syntax introduced in Perl 5.14.0.

*perlop*
Subsection "perlop"

- \(bu
The explanation of bitwise operators has been expanded to explain how they
work on Unicode strings.

- \(bu
The section on the triple-dot or yada-yada operator has been moved up, as
it used to separate two closely related sections about the comma operator.

- \(bu
More examples for \f(CW\*(C`m//g\*(C' have been added.

- \(bu
The \f(CW\*(C`<<\\FOO\*(C' here-doc syntax has been documented.

*perlrun*
Subsection "perlrun"

- \(bu
perlrun has undergone a significant clean-up.  Most notably, the
**-0x...** form of the **-0** flag has been clarified, and the final section
on environment variables has been corrected and expanded.

*\s-1POSIX\s0*
Subsection "POSIX"

- \(bu
The invocation documentation for \f(CW\*(C`WIFEXITED\*(C', \f(CW\*(C`WEXITSTATUS\*(C',
\f(CW\*(C`WIFSIGNALED\*(C', \f(CW\*(C`WTERMSIG\*(C', \f(CW\*(C`WIFSTOPPED\*(C', and \f(CW\*(C`WSTOPSIG\*(C' was corrected.

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### New Diagnostics

Subsection "New Diagnostics"
None

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"
None

## Utility Changes

Header "Utility Changes"
None

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
*regexp.h* has been modified for compatibility with \s-1GCC\s0's \f(CW\*(C`-Werror\*(C'
option, as used by some projects that include perl's header files.

## Testing

Header "Testing"

- \(bu
Some test failures in *dist/Locale-Maketext/t/09_compile.t* that could
occur depending on the environment have been fixed. [perl #89896]

- \(bu
A watchdog timer for *t/re/re.t* was lengthened to accommodate \s-1SH-4\s0 systems
which were unable to complete the tests before the previous timer ran out.

## Platform Support

Header "Platform Support"

### New Platforms

Subsection "New Platforms"
None

### Discontinued Platforms

Subsection "Discontinued Platforms"
None

### Platform-Specific Notes

Subsection "Platform-Specific Notes"
*Solaris*
Subsection "Solaris"

- \(bu
Documentation listing the Solaris packages required to build Perl on
Solaris 9 and Solaris 10 has been corrected.

*Mac \s-1OS X\s0*
Subsection "Mac OS X"

- \(bu
The *lib/locale.t* test script has been updated to work on the upcoming
Lion release.

- \(bu
Mac \s-1OS X\s0 specific compilation instructions have been clarified.

*Ubuntu Linux*
Subsection "Ubuntu Linux"

- \(bu
The ODBM_File installation process has been updated with the new library
paths on Ubuntu natty.

## Internal Changes

Header "Internal Changes"

- \(bu
The compiled representation of formats is now stored via the mg_ptr of
their PERL_MAGIC_fm. Previously it was stored in the string buffer,
beyond **SvLEN()**, the regular end of the string. **SvCOMPILED()** and
SvCOMPILED_\{on,off\}() now exist solely for compatibility for \s-1XS\s0 code.
The first is always 0, the other two now no-ops.

## Bug Fixes

Header "Bug Fixes"

- \(bu
A bug has been fixed that would cause a \*(L"Use of freed value in iteration\*(R"
error if the next two hash elements that would be iterated over are
deleted. [perl #85026]

- \(bu
Passing the same constant subroutine to both \f(CW\*(C`index\*(C' and \f(CW\*(C`formline\*(C' no
longer causes one or the other to fail. [perl #89218]

- \(bu
5.14.0 introduced some memory leaks in regular expression character
classes such as \f(CW\*(C`[\\w\\s]\*(C', which have now been fixed.

- \(bu
An edge case in regular expression matching could potentially loop.
This happened only under \f(CW\*(C`/i\*(C' in bracketed character classes that have
characters with multi-character folds, and the target string to match
against includes the first portion of the fold, followed by another
character that has a multi-character fold that begins with the remaining
portion of the fold, plus some more.
.Sp
.Vb 1
 "s\\N\{U+DF\}" =~ /[\\x\{DF\}foo]/i
.Ve
.Sp
is one such case.  \f(CW\*(C`\\xDF\*(C' folds to \f(CW"ss".

- \(bu
Several Unicode case-folding bugs have been fixed.

- \(bu
The new (in 5.14.0) regular expression modifier \f(CW\*(C`/a\*(C' when repeated like
\f(CW\*(C`/aa\*(C' forbids the characters outside the \s-1ASCII\s0 range that match
characters inside that range from matching under \f(CW\*(C`/i\*(C'.  This did not
work under some circumstances, all involving alternation, such as:
.Sp
.Vb 1
 "\\N\{KELVIN SIGN\}" =~ /k|foo/iaa;
.Ve
.Sp
succeeded inappropriately.  This is now fixed.

- \(bu
Fixed a case where it was possible that a freed buffer may have been read
from when parsing a here document.

## Acknowledgements

Header "Acknowledgements"
Perl 5.14.1 represents approximately four weeks of development since
Perl 5.14.0 and contains approximately 3500 lines of changes
across 38 files from 17 authors.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers.  The following people are known to
have contributed the improvements that became Perl 5.14.1:

Bo Lindbergh, Claudio Ramirez, Craig A. Berry, David Leadbeater, Father
Chrysostomos, Jesse Vincent, Jim Cromie, Justin Case, Karl Williamson,
Leo Lapworth, Nicholas Clark, Nobuhiro Iwamatsu, smash, Tom Christiansen,
Ton Hospel, Vladimir Timofeev, and Zsba\*'n Ambrus.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes all the core committers, who be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for
security issues in the Perl core, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
