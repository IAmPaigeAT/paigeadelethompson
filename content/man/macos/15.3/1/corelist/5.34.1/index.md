+++
author = "None Specified"
title = "corelist(1)"
detected_package_version = "5.34.1"
date = "2024-12-14"
description = "See Module::CoreList for one.    corelist -v    corelist [-a|-d] <ModuleName> | /<ModuleRegex>/ [<ModuleVersion>] ...    corelist [-v <PerlVersion>] [ <ModuleName> | /<ModuleRegex>/ ] ...    corelist [-r <PerlVersion>] ...    corelist --utils [-d..."
operating_system_version = "15.3"
manpage_format = "troff"
manpage_name = "corelist"
manpage_section = "1"
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "CORELIST 1"
CORELIST 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

corelist - a commandline frontend to Module::CoreList

## DESCRIPTION

Header "DESCRIPTION"
See Module::CoreList for one.

## SYNOPSIS

Header "SYNOPSIS"
.Vb 9
   corelist -v
   corelist [-a|-d] <ModuleName> | /<ModuleRegex>/ [<ModuleVersion>] ...
   corelist [-v <PerlVersion>] [ <ModuleName> | /<ModuleRegex>/ ] ...
   corelist [-r <PerlVersion>] ...
   corelist --utils [-d] <UtilityName> [<UtilityName>] ...
   corelist --utils -v <PerlVersion>
   corelist --feature <FeatureName> [<FeatureName>] ...
   corelist --diff PerlVersion PerlVersion
   corelist --upstream <ModuleName>
.Ve

## OPTIONS

Header "OPTIONS"

- -a
Item "-a"
lists all versions of the given module (or the matching modules, in case you
used a module regexp) in the perls Module::CoreList knows about.
.Sp
.Vb 1
    corelist -a Unicode

    Unicode was first released with perl v5.6.2
      v5.6.2     3.0.1
      v5.8.0     3.2.0
      v5.8.1     4.0.0
      v5.8.2     4.0.0
      v5.8.3     4.0.0
      v5.8.4     4.0.1
      v5.8.5     4.0.1
      v5.8.6     4.0.1
      v5.8.7     4.1.0
      v5.8.8     4.1.0
      v5.8.9     5.1.0
      v5.9.0     4.0.0
      v5.9.1     4.0.0
      v5.9.2     4.0.1
      v5.9.3     4.1.0
      v5.9.4     4.1.0
      v5.9.5     5.0.0
      v5.10.0    5.0.0
      v5.10.1    5.1.0
      v5.11.0    5.1.0
      v5.11.1    5.1.0
      v5.11.2    5.1.0
      v5.11.3    5.2.0
      v5.11.4    5.2.0
      v5.11.5    5.2.0
      v5.12.0    5.2.0
      v5.12.1    5.2.0
      v5.12.2    5.2.0
      v5.12.3    5.2.0
      v5.12.4    5.2.0
      v5.13.0    5.2.0
      v5.13.1    5.2.0
      v5.13.2    5.2.0
      v5.13.3    5.2.0
      v5.13.4    5.2.0
      v5.13.5    5.2.0
      v5.13.6    5.2.0
      v5.13.7    6.0.0
      v5.13.8    6.0.0
      v5.13.9    6.0.0
      v5.13.10   6.0.0
      v5.13.11   6.0.0
      v5.14.0    6.0.0
      v5.14.1    6.0.0
      v5.15.0    6.0.0
.Ve

- -d
Item "-d"
finds the first perl version where a module has been released by
date, and not by version number (as is the default).

- --diff
Item "--diff"
Given two versions of perl, this prints a human-readable table of all module
changes between the two.  The output format may change in the future, and is
meant for *humans*, not programs.  For programs, use the Module::CoreList
\s-1API.\s0

- -? or -help
Item "-? or -help"
help! help! help! to see more help, try --man.

- -man
Item "-man"
all of the help

- -v
Item "-v"
lists all of the perl release versions we got the CoreList for.
.Sp
If you pass a version argument (value of \f(CW$], like \f(CW5.00503 or \f(CW5.008008),
you get a list of all the modules and their respective versions.
(If you have the \f(CW\*(C`version\*(C' module, you can also use new-style version numbers,
like \f(CW5.8.8.)
.Sp
In module filtering context, it can be used as Perl version filter.

- -r
Item "-r"
lists all of the perl releases and when they were released
.Sp
If you pass a perl version you get the release date for that version only.

- --utils
Item "--utils"
lists the first version of perl each named utility program was released with
.Sp
May be used with -d to modify the first release criteria.
.Sp
If used with -v <version> then all utilities released with that version of perl
are listed, and any utility programs named on the command line are ignored.

- --feature, -f
Item "--feature, -f"
lists the first version bundle of each named feature given

- --upstream, -u
Item "--upstream, -u"
Shows if the given module is primarily maintained in perl core or on \s-1CPAN\s0
and bug tracker \s-1URL.\s0

As a special case, if you specify the module name \f(CW\*(C`Unicode\*(C', you'll get
the version number of the Unicode Character Database bundled with the
requested perl versions.

## EXAMPLES

Header "EXAMPLES"
.Vb 1
    $ corelist File::Spec

    File::Spec was first released with perl 5.005

    $ corelist File::Spec 0.83

    File::Spec 0.83 was released with perl 5.007003

    $ corelist File::Spec 0.89

    File::Spec 0.89 was not in CORE (or so I think)

    $ corelist File::Spec::Aliens

    File::Spec::Aliens  was not in CORE (or so I think)

    $ corelist /IPC::Open/

    IPC::Open2 was first released with perl 5

    IPC::Open3 was first released with perl 5

    $ corelist /MANIFEST/i

    ExtUtils::Manifest was first released with perl 5.001

    $ corelist /Template/

    /Template/  has no match in CORE (or so I think)

    $ corelist -v 5.8.8 B

    B                        1.09_01

    $ corelist -v 5.8.8 /^B::/

    B::Asmdata               1.01
    B::Assembler             0.07
    B::Bblock                1.02_01
    B::Bytecode              1.01_01
    B::C                     1.04_01
    B::CC                    1.00_01
    B::Concise               0.66
    B::Debug                 1.02_01
    B::Deparse               0.71
    B::Disassembler          1.05
    B::Lint                  1.03
    B::O                     1.00
    B::Showlex               1.02
    B::Stackobj              1.00
    B::Stash                 1.00
    B::Terse                 1.03_01
    B::Xref                  1.01
.Ve

## COPYRIGHT

Header "COPYRIGHT"
Copyright (c) 2002-2007 by D.H. aka PodMaster

Currently maintained by the perl 5 porters <perl5-porters@perl.org>.

This program is distributed under the same terms as perl itself.
See http://perl.org/ or http://cpan.org/ for more info on that.
