+++
operating_system = "macos"
title = "perl5221delta(1)"
description = "This document describes differences between the 5.22.0 release and the 5.22.1 release. If you are upgrading from an earlier release such as 5.20.0, first read perl5220delta, which describes differences between 5.20.0 and 5.22.0. There are no chang..."
manpage_name = "perl5221delta"
author = "None Specified"
manpage_section = "1"
detected_package_version = "5.34.1"
manpage_format = "troff"
date = "2022-02-19"
operating_system_version = "15.3"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5221DELTA 1"
PERL5221DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5221delta - what is new for perl v5.22.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.22.0 release and the 5.22.1
release.

If you are upgrading from an earlier release such as 5.20.0, first read
perl5220delta, which describes differences between 5.20.0 and 5.22.0.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.20.0 other than the
following single exception, which we deemed to be a sensible change to make in
order to get the new \f(CW\*(C`\\b\{wb\}\*(C' and (in particular) \f(CW\*(C`\\b\{sb\}\*(C' features sane
before people decided they're worthless because of bugs in their Perl 5.22.0
implementation and avoided them in the future.
If any others exist, they are bugs, and we request that you submit a report.
See \*(L"Reporting Bugs\*(R" below.

### Bounds Checking Constructs

Subsection "Bounds Checking Constructs"
Several bugs, including a segmentation fault, have been fixed with the bounds
checking constructs (introduced in Perl 5.22) \f(CW\*(C`\\b\{gcb\}\*(C', \f(CW\*(C`\\b\{sb\}\*(C', \f(CW\*(C`\\b\{wb\}\*(C',
\f(CW\*(C`\\B\{gcb\}\*(C', \f(CW\*(C`\\B\{sb\}\*(C', and \f(CW\*(C`\\B\{wb\}\*(C'.  All the \f(CW\*(C`\\B\{\}\*(C' ones now match an empty
string; none of the \f(CW\*(C`\\b\{\}\*(C' ones do.
[\s-1GH\s0 #14976] <https://github.com/Perl/perl5/issues/14976>

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Module::CoreList has been upgraded from version 5.20150520 to 5.20151213.

- \(bu
PerlIO::scalar has been upgraded from version 0.22 to 0.23.

- \(bu
\s-1POSIX\s0 has been upgraded from version 1.53 to 1.53_01.
.Sp
If \f(CW\*(C`POSIX::strerror\*(C' was passed \f(CW$! as its argument then it accidentally
cleared \f(CW$!.  This has been fixed.
[\s-1GH\s0 #14951] <https://github.com/Perl/perl5/issues/14951>

- \(bu
Storable has been upgraded from version 2.53 to 2.53_01.

- \(bu
warnings has been upgraded from version 1.32 to 1.34.
.Sp
The \f(CW\*(C`warnings::enabled\*(C' example now actually uses \f(CW\*(C`warnings::enabled\*(C'.
[\s-1GH\s0 #14905] <https://github.com/Perl/perl5/issues/14905>

- \(bu
Win32 has been upgraded from version 0.51 to 0.52.
.Sp
This has been updated for Windows 8.1, 10 and 2012 R2 Server.

## Documentation

Header "Documentation"

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perltie*
Subsection "perltie"

- \(bu
The usage of \f(CW\*(C`FIRSTKEY\*(C' and \f(CW\*(C`NEXTKEY\*(C' has been clarified.

*perlvar*
Subsection "perlvar"

- \(bu
The specific true value of \f(CW$!\{E...\} is now documented, noting that it is
subject to change and not guaranteed.

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
The \f(CW\*(C`printf\*(C' and \f(CW\*(C`sprintf\*(C' builtins are now more careful about the warnings
they emit: argument reordering now disables the \*(L"redundant argument\*(R" warning in
all cases.
[\s-1GH\s0 #14772] <https://github.com/Perl/perl5/issues/14772>

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
Using the \f(CW\*(C`NO_HASH_SEED\*(C' define in combination with the default hash algorithm
\f(CW\*(C`PERL_HASH_FUNC_ONE_AT_A_TIME_HARD\*(C' resulted in a fatal error while compiling
the interpreter, since Perl 5.17.10.  This has been fixed.

- \(bu
Configuring with ccflags containing quotes (e.g.
\f(CW\*(C`-Accflags=\*(Aq-DAPPLLIB_EXP=\\"/usr/libperl\\"\*(Aq\*(C') was broken in Perl 5.22.0
but has now been fixed again.
[\s-1GH\s0 #14732] <https://github.com/Perl/perl5/issues/14732>

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- \s-1IRIX\s0
Item "IRIX"

> 0

- \(bu
.PD
Under some circumstances \s-1IRIX\s0 stdio **fgetc()** and **fread()** set the errno to
\f(CW\*(C`ENOENT\*(C', which made no sense according to either \s-1IRIX\s0 or \s-1POSIX\s0 docs.  Errno
is now cleared in such cases.
[\s-1GH\s0 #14557] <https://github.com/Perl/perl5/issues/14557>

- \(bu
Problems when multiplying long doubles by infinity have been fixed.
[\s-1GH\s0 #14993] <https://github.com/Perl/perl5/issues/14993>

- \(bu
All tests pass now on \s-1IRIX\s0 with the default build configuration.



> 


## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
\f(CW\*(C`qr/(?[ () ])/\*(C' no longer segfaults, giving a syntax error message instead.
[\s-1GH\s0 #14851] <https://github.com/Perl/perl5/issues/14851>

- \(bu
Regular expression possessive quantifier Perl 5.20 regression now fixed.
\f(CW\*(C`qr/\*(C'*\s-1PAT\s0*\f(CW\*(C`\{\*(C'*min*,*max*\f(CW\*(C`\}+\*(C'\f(CW\*(C`/\*(C' is supposed to behave identically to
\f(CW\*(C`qr/(?>\*(C'*\s-1PAT\s0*\f(CW\*(C`\{\*(C'*min*,*max*\f(CW\*(C`\})/\*(C'.  Since Perl 5.20, this didn't work
if *min* and *max* were equal.
[\s-1GH\s0 #14857] <https://github.com/Perl/perl5/issues/14857>

- \(bu
Certain syntax errors in
\*(L"Extended Bracketed Character Classes\*(R" in perlrecharclass caused panics instead
of the proper error message.  This has now been fixed.
[\s-1GH\s0 #15016] <https://github.com/Perl/perl5/issues/15016>

- \(bu
\f(CW\*(C`BEGIN <>\*(C' no longer segfaults and properly produces an error message.
[\s-1GH\s0 #13546] <https://github.com/Perl/perl5/issues/13546>

- \(bu
A regression from Perl 5.20 has been fixed, in which some syntax errors in
\f(CW\*(C`(?[...])\*(C' constructs
within regular expression patterns could cause a segfault instead of a proper
error message.
[\s-1GH\s0 #14933] <https://github.com/Perl/perl5/issues/14933>

- \(bu
Another problem with
\f(CW\*(C`(?[...])\*(C'
constructs has been fixed wherein things like \f(CW\*(C`\\c]\*(C' could cause panics.
[\s-1GH\s0 #14934] <https://github.com/Perl/perl5/issues/14934>

- \(bu
In Perl 5.22.0, the logic changed when parsing a numeric parameter to the -C
option, such that the successfully parsed number was not saved as the option
value if it parsed to the end of the argument.
[\s-1GH\s0 #14748] <https://github.com/Perl/perl5/issues/14748>

- \(bu
Warning fatality is now ignored when rewinding the stack.  This prevents
infinite recursion when the now fatal error also causes rewinding of the stack.
[\s-1GH\s0 #14319] <https://github.com/Perl/perl5/issues/14319>

- \(bu
A crash with \f(CW\*(C`%::=(); J->$\{\\"::"\}\*(C' has been fixed.
[\s-1GH\s0 #14790] <https://github.com/Perl/perl5/issues/14790>

- \(bu
Nested quantifiers such as \f(CW\*(C`/.\{1\}??/\*(C' should cause perl to throw a fatal
error, but were being silently accepted since Perl 5.20.0.  This has been
fixed.
[\s-1GH\s0 #14960] <https://github.com/Perl/perl5/issues/14960>

- \(bu
Regular expression sequences such as \f(CW\*(C`/(?i/\*(C' (and similarly with other
recognized flags or combination of flags) should cause perl to throw a fatal
error, but were being silently accepted since Perl 5.18.0.  This has been
fixed.
[\s-1GH\s0 #14931] <https://github.com/Perl/perl5/issues/14931>

- \(bu
A bug in hexadecimal floating point literal support meant that high-order bits
could be lost in cases where mantissa overflow was caused by too many trailing
zeros in the fractional part.  This has been fixed.
[\s-1GH\s0 #15032] <https://github.com/Perl/perl5/issues/15032>

- \(bu
Another hexadecimal floating point bug, causing low-order bits to be lost in
cases where the last hexadecimal digit of the mantissa has bits straddling the
limit of the number of bits allowed for the mantissa, has also been fixed.
[\s-1GH\s0 #15033] <https://github.com/Perl/perl5/issues/15033>

- \(bu
Further hexadecimal floating point bugs have been fixed: In some circumstances,
the \f(CW%a format specifier could variously lose the sign of the negative zero,
fail to display zeros after the radix point with the requested precision, or
even lose the radix point after the leftmost hexadecimal digit completely.

- \(bu
A crash caused by incomplete expressions within \f(CW\*(C`/(?[ ])/\*(C' (e.g.
\f(CW\*(C`/(?[[0]+()+])/\*(C') has been fixed.
[\s-1GH\s0 #15045] <https://github.com/Perl/perl5/issues/15045>

## Acknowledgements

Header "Acknowledgements"
Perl 5.22.1 represents approximately 6 months of development since Perl 5.22.0
and contains approximately 19,000 lines of changes across 130 files from 27
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 1,700 lines of changes to 44 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.22.1:

Aaron Crane, Abigail, Andy Broad, Aristotle Pagaltzis, Chase Whitener, Chris
'BinGOs' Williams, Craig A. Berry, Daniel Dragan, David Mitchell, Father
Chrysostomos, Herbert Breunung, Hugo van der Sanden, James E Keenan, Jan
Dubois, Jarkko Hietaniemi, Karen Etheridge, Karl Williamson, Lukas Mai, Matthew
Horsfall, Peter Martini, Rafael Garcia-Suarez, Ricardo Signes, Shlomi Fish,
Sisyphus, Steve Hay, Tony Cook, Victor Adam.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the perl bug database at
https://rt.perl.org/ .  There may also be information at
http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send it
to perl5-security-report@perl.org.  This points to a closed subscription
unarchived mailing list, which includes all the core committers, who will be
able to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported.  Please only use this address for
security issues in the Perl core, not for modules independently distributed on
\s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
