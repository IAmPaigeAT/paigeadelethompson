+++
manpage_format = "troff"
manpage_name = "dyld_usage"
title = "dyld_usage(1)"
description = "The dyld_usage utility presents an ongoing display of information pertaining to dynamic linker activity within one or more processes. It requires root privileges due to the kernel tracing facility it uses to operate. By default dyld_usage monitors ..."
date = "Sun Feb 16 04:48:20 2025"
keywords = ["fbdyld", "fbfs_usage", "author", "apple", "inc", "copyright", "2000-2020", "generated", "by", "docutils", "manpage", "writer"]
operating_system_version = "15.3"
operating_system = "macos"
author = "None Specified"
detected_package_version = "0"
manpage_section = "1"
+++


## SYNOPSIS


**dyld_usage** \fB[-e] [-f mode] [-j] [-h] [-t seconds] [-R rawfile [-S start_time]
[-E end_time]] [pid | cmd [pid | cmd] ...]

## DESCRIPTION


The **dyld_usage** utility presents an ongoing display of information
pertaining to dynamic linker activity within one or more processes. It requires
root privileges due to the kernel tracing facility it uses to operate. By
default **dyld_usage** monitors *dyld* activity in all processes except
the running *dyld_usage* process, *Terminal*, *telnetd*, *sshd*, *rlogind*,
*tcsh*, *csh*, and *sh*. These defaults can be overridden such that output is
limited to include or exclude a list of processes specified on the command line.
Processes may be specified either by file name or by process id.

The output of **dyld_usage** is formatted according to the size of your
window. A narrow window will display fewer columns of data. Use a wide window
for maximum data display.

## OPTIONS


**dyld_usage** supports the following options:
0.0

-e
Exclude the specified list of pids and commands from the sample, and exclude
**dyld_usage** by default.
7.0

-j
.UNINDENT

Display output in JSON format.
.UNINDENT
0.0

-h
Display usage information and exit.
.UNINDENT
0.0

-R
Specify a raw trace file to process.
.UNINDENT
0.0

-t
Specify timeout in seconds (for use in automated tools).
.UNINDENT

## DISPLAY


The data columns displayed are as follows:
0.0
3.5
0.0

TIMESTAMP
Time of day when call occurred.

OPERATION
The *dyld* operation triggered by the process. Typically these operations
are triggered by process launch or via a **dlopen** or **dlsym** system
call. System call entries include both the parameters to the system call and
the system call\(aqs return code (e.g., 0 on success).

TIME INTERVAL
The elapsed time spent in the dynamic linker operation or system call.

PROCESS NAME
The process that generated the dynamic linker activity. If space allows, the
thread id will be appended to the process name (i.e., Mail.nnn).
.UNINDENT
.UNINDENT
.UNINDENT

## SAMPLE USAGE

0.0
3.5
**sudo dyld_usage Mail**

**dyld_usage** will display dynamic link operations for all instances of
processes named Mail.
.UNINDENT
.UNINDENT

## SEE ALSO


**dyld(1)**, **fs_usage(1)**

## AUTHOR

Apple, Inc.

## COPYRIGHT

2000-2020, Apple, Inc.
.
