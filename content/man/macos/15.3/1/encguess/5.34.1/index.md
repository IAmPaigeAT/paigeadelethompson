+++
keywords = ["header", "see", "also", "encode", "guess", "detect", "license", "and", "copyright", "2015", "michael", "lagrasta", "dan", "kogai", "this", "program", "is", "free", "software", "you", "can", "redistribute", "it", "or", "modify", "under", "the", "terms", "of", "artistic", "2", "may", "obtain", "a", "copy", "full", "at", "http", "www", "perlfoundation", "org", "artistic_license_2_0"]
manpage_section = "1"
operating_system = "macos"
description = "The encoding identification is done by checking one encoding type at a time until all but the right type are eliminated. The set of encoding types to try is defined by the -s parameter and defaults to ascii, utf8 and s-1UTF-16/32s0 with s-1BOM.s0 T..."
manpage_name = "encguess"
title = "encguess(1)"
author = "None Specified"
detected_package_version = "5.34.1"
manpage_format = "troff"
operating_system_version = "15.3"
date = "2024-12-14"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "ENCGUESS 1"
ENCGUESS 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

encguess - guess character encodings of files

## VERSION

Header "VERSION"
\f(CW$Id: encguess,v 0.3 2020/12/02 01:28:17 dankogai Exp dankogai $

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
  encguess [switches] filename...
.Ve

### \s-1SWITCHES\s0

Subsection "SWITCHES"

- -h
Item "-h"
show this message and exit.

- -s
Item "-s"
specify a list of \*(L"suspect encoding types\*(R" to test,
separated by either \f(CW\*(C`:\*(C' or \f(CW\*(C`,\*(C'

- -S
Item "-S"
output a list of all acceptable encoding types that can be used with
the -s param

- -u
Item "-u"
suppress display of unidentified types

### \s-1EXAMPLES:\s0

Subsection "EXAMPLES:"

- \(bu
Guess encoding of a file named \f(CW\*(C`test.txt\*(C', using only the default
suspect types.
.Sp
.Vb 1
   encguess test.txt
.Ve

- \(bu
Guess the encoding type of a file named \f(CW\*(C`test.txt\*(C', using the suspect
types \f(CW\*(C`euc-jp,shiftjis,7bit-jis\*(C'.
.Sp
.Vb 2
   encguess -s euc-jp,shiftjis,7bit-jis test.txt
   encguess -s euc-jp:shiftjis:7bit-jis test.txt
.Ve

- \(bu
Guess the encoding type of several files, do not display results for
unidentified files.
.Sp
.Vb 1
   encguess -us euc-jp,shiftjis,7bit-jis test*.txt
.Ve

## DESCRIPTION

Header "DESCRIPTION"
The encoding identification is done by checking one encoding type at a
time until all but the right type are eliminated. The set of encoding
types to try is defined by the -s parameter and defaults to ascii,
utf8 and \s-1UTF-16/32\s0 with \s-1BOM.\s0 This can be overridden by passing one or
more encoding types via the -s parameter. If you need to pass in
multiple suspect encoding types, use a quoted string with the a space
separating each value.

## SEE ALSO

Header "SEE ALSO"
Encode::Guess, Encode::Detect

## LICENSE AND COPYRIGHT

Header "LICENSE AND COPYRIGHT"
Copyright 2015 Michael LaGrasta and Dan Kogai.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

<http://www.perlfoundation.org/artistic_license_2_0>
