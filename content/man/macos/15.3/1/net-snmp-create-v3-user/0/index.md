+++
author = "None Specified"
detected_package_version = "0"
title = "net-snmp-create-v3-user(1)"
manpage_format = "troff"
manpage_section = "1"
description = "The net-snmp-create-v3-user shell script is designed to create a new user in net-snmp configuration file (/var/net-snmp/snmpd.conf by default)."
date = "2008-01-01"
operating_system_version = "15.3"
manpage_name = "net-snmp-create-v3-user"
operating_system = "macos"
+++

net-snmp-create-v3-user 1 "17 Sep 2008" V5.6.2.1 "Net-SNMP"

## NAME

net-snmp-create-v3-user - create a SNMPv3 user in net-snmp configuration file

## SYNOPSIS


net-snmp-create-v3-user [-ro] [-a authpass] [-x privpass] [-X DES|AES]
[username]

## DESCRIPTION


The *net-snmp-create-v3-user* shell script is designed to create a
new user in net-snmp configuration file (/var/net-snmp/snmpd.conf by default).


## OPTIONS


**--version**
displays the net-snmp version number

**-ro**
create an user with read-only permissions

**-a authpass**
specify authentication password

**-x privpass**
specify encryption password

**-X DES|AES**
specify encryption algorithm
