+++
author = "None Specified"
description = "Opens the source directory of the provided GEM in your editor. For this to work the EDITOR or BUNDLER_EDITOR environment variable has to be set. Example:..."
operating_system_version = "15.3"
date = "Sun Feb 16 04:48:22 2025"
manpage_format = "troff"
manpage_name = "bundle-open"
detected_package_version = "0.7.3"
manpage_section = "1"
operating_system = "macos"
title = "bundle-open(1)"
+++

.
"BUNDLE-OPEN" "1" "November 2018" "" ""
.

## NAME

**bundle-open** - Opens the source directory for a gem in your bundle
.

## SYNOPSIS

**bundle open** [GEM]
.

## DESCRIPTION

Opens the source directory of the provided GEM in your editor\.
.
.P
For this to work the **EDITOR** or **BUNDLER_EDITOR** environment variable has to be set\.
.
.P
Example:
.
"" 4
.

```

bundle open \'rack\'
.
```

.
"" 0
.
.P
Will open the source directory for the \'rack\' gem in your bundle\.
