+++
operating_system = "macos"
manpage_name = "ip2cc"
operating_system_version = "15.3"
manpage_format = "troff"
date = "2024-12-14"
author = "None Specified"
manpage_section = "1"
detected_package_version = "5.34.0"
description = "Ip2cc is a program to lookup countries of s-1IPs0 addresses. Ip2cc has two modes: interactive and non-interactive.  Interactive mode allows the user to query more than one hostname.  Non-interactive mode is used to print just the country for a sing..."
title = "ip2cc(1)"
keywords = ["header", "see", "also", "ip", "country", "the", "fast", "database", "used", "by", "this", "script", "author", "nigel", "wetters", "gourlay", "nwetters", "cpan", "org"]
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "IP2CC 1"
IP2CC 1 "2024-12-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

ip2cc - lookup country from IP address or hostname

## SYNOPSIS

Header "SYNOPSIS"
ip2cc [host-to-find]

## DESCRIPTION

Header "DESCRIPTION"
Ip2cc is a program to lookup countries of \s-1IP\s0 addresses. Ip2cc
has two modes: interactive and non-interactive.  Interactive mode allows
the user to query more than one hostname.  Non-interactive mode is
used to print just the country for a single host.

## ARGUMENTS

Header "ARGUMENTS"
Interactive mode is entered when no arguments are given.

Non-interactive mode is used when the name or Internet address of the
host to be looked up is given as the first argument.

## SEE ALSO

Header "SEE ALSO"
IP::Country - the fast database used by this script.

## AUTHOR

Header "AUTHOR"
Nigel Wetters Gourlay <nwetters@cpan.org>
