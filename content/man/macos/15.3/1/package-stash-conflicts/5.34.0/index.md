+++
manpage_format = "troff"
author = "None Specified"
operating_system = "macos"
description = "None Specified"
manpage_name = "package-stash-conflicts"
detected_package_version = "5.34.0"
manpage_section = "1"
operating_system_version = "15.3"
title = "package-stash-conflicts(1)"
date = "2018-12-31"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "PACKAGE-STASH-CONFLICTS 1"
PACKAGE-STASH-CONFLICTS 1 "2018-12-31" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

package-stash-conflicts

## VERSION

Header "VERSION"
version 0.38

## SUPPORT

Header "SUPPORT"
Bugs may be submitted through the \s-1RT\s0 bug tracker <https://rt.cpan.org/Public/Dist/Display.html?Name=Package-Stash>
(or bug-Package-Stash@rt.cpan.org <mailto:bug-Package-Stash@rt.cpan.org>).

## AUTHOR

Header "AUTHOR"
Jesse Luehrs <doy@tozt.net>

## COPYRIGHT AND LICENSE

Header "COPYRIGHT AND LICENSE"
This software is copyright (c) 2018 by Jesse Luehrs.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
