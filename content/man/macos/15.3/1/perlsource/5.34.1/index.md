+++
manpage_section = "1"
manpage_name = "perlsource"
detected_package_version = "5.34.1"
description = "This document describes the layout of the Perl source tree. If youre hacking on the Perl core, this will help you find what youre looking for. The Perl source tree is big. Heres some of the thing youll find in it: The C source code and header..."
date = "2022-02-19"
author = "None Specified"
operating_system = "macos"
operating_system_version = "15.3"
title = "perlsource(1)"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLSOURCE 1"
PERLSOURCE 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlsource - A guide to the Perl source tree

## DESCRIPTION

Header "DESCRIPTION"
This document describes the layout of the Perl source tree. If you're
hacking on the Perl core, this will help you find what you're looking
for.

## FINDING YOUR WAY AROUND

Header "FINDING YOUR WAY AROUND"
The Perl source tree is big. Here's some of the thing you'll find in
it:

### C code

Subsection "C code"
The C source code and header files mostly live in the root of the
source tree. There are a few platform-specific directories which
contain C code. In addition, some of the modules shipped with Perl
include C or \s-1XS\s0 code.

See perlinterp for more details on the files that make up the Perl
interpreter, as well as details on how it works.

### Core modules

Subsection "Core modules"
Modules shipped as part of the Perl core live in four subdirectories.
Two of these directories contain modules that live in the core, and two
contain modules that can also be released separately on \s-1CPAN.\s0 Modules
which can be released on cpan are known as \*(L"dual-life\*(R" modules.

- \(bu
*lib/*
.Sp
This directory contains pure-Perl modules which are only released as
part of the core. This directory contains *all* of the modules and
their tests, unlike other core modules.

- \(bu
*ext/*
.Sp
Like *lib/*, this directory contains modules which are only released
as part of the core.  Unlike *lib/*, however, a module under *ext/*
generally has a CPAN-style directory- and file-layout and its own
*Makefile.PL*.  There is no expectation that a module under *ext/*
will work with earlier versions of Perl 5.  Hence, such a module may
take full advantage of syntactical and other improvements in Perl 5
blead.

- \(bu
*dist/*
.Sp
This directory is for dual-life modules where the blead source is
canonical. Note that some modules in this directory may not yet have
been released separately on \s-1CPAN.\s0  Modules under *dist/* should make
an effort to work with earlier versions of Perl 5.

- \(bu
*cpan/*
.Sp
This directory contains dual-life modules where the \s-1CPAN\s0 module is
canonical. Do not patch these modules directly! Changes to these
modules should be submitted to the maintainer of the \s-1CPAN\s0 module. Once
those changes are applied and released, the new version of the module
will be incorporated into the core.

For some dual-life modules, it has not yet been determined if the \s-1CPAN\s0
version or the blead source is canonical. Until that is done, those
modules should be in *cpan/*.

### Tests

Subsection "Tests"
The Perl core has an extensive test suite. If you add new tests (or new
modules with tests), you may need to update the *t/TEST* file so that
the tests are run.

- \(bu
Module tests
.Sp
Tests for core modules in the *lib/* directory are right next to the
module itself. For example, we have *lib/strict.pm* and
*lib/strict.t*.
.Sp
Tests for modules in *ext/* and the dual-life modules are in *t/*
subdirectories for each module, like a standard \s-1CPAN\s0 distribution.

- \(bu
*t/base/*
.Sp
Tests for the absolute basic functionality of Perl. This includes
\f(CW\*(C`if\*(C', basic file reads and writes, simple regexes, etc. These are run
first in the test suite and if any of them fail, something is *really*
broken.

- \(bu
*t/cmd/*
.Sp
Tests for basic control structures, \f(CW\*(C`if\*(C'/\f(CW\*(C`else\*(C', \f(CW\*(C`while\*(C', subroutines,
etc.

- \(bu
*t/comp/*
.Sp
Tests for basic issues of how Perl parses and compiles itself.

- \(bu
*t/io/*
.Sp
Tests for built-in \s-1IO\s0 functions, including command line arguments.

- \(bu
*t/mro/*
.Sp
Tests for perl's method resolution order implementations (see mro).

- \(bu
*t/op/*
.Sp
Tests for perl's built in functions that don't fit into any of the
other directories.

- \(bu
*t/opbasic/*
.Sp
Tests for perl's built in functions which, like those in *t/op/*, do
not fit into any of the other directories, but which, in addition,
cannot use *t/test.pl*,as that program depends on functionality which
the test file itself is testing.

- \(bu
*t/re/*
.Sp
Tests for regex related functions or behaviour. (These used to live in
t/op).

- \(bu
*t/run/*
.Sp
Tests for features of how perl actually runs, including exit codes and
handling of PERL* environment variables.

- \(bu
*t/uni/*
.Sp
Tests for the core support of Unicode.

- \(bu
*t/win32/*
.Sp
Windows-specific tests.

- \(bu
*t/porting/*
.Sp
Tests the state of the source tree for various common errors. For
example, it tests that everyone who is listed in the git log has a
corresponding entry in the *\s-1AUTHORS\s0* file.

- \(bu
*t/lib/*
.Sp
The old home for the module tests, you shouldn't put anything new in
here. There are still some bits and pieces hanging around in here that
need to be moved. Perhaps you could move them?  Thanks!

### Documentation

Subsection "Documentation"
All of the core documentation intended for end users lives in *pod/*.
Individual modules in *lib/*, *ext/*, *dist/*, and *cpan/* usually
have their own documentation, either in the *Module.pm* file or an
accompanying *Module.pod* file.

Finally, documentation intended for core Perl developers lives in the
*Porting/* directory.

### Hacking tools and documentation

Subsection "Hacking tools and documentation"
The *Porting* directory contains a grab bag of code and documentation
intended to help porters work on Perl. Some of the highlights include:

- \(bu
*check**
.Sp
These are scripts which will check the source things like \s-1ANSI C\s0
violations, \s-1POD\s0 encoding issues, etc.

- \(bu
*Maintainers*, *Maintainers.pl*, and *Maintainers.pm*
.Sp
These files contain information on who maintains which modules. Run
\f(CW\*(C`perl Porting/Maintainers -M Module::Name\*(C' to find out more
information about a dual-life module.

- \(bu
*podtidy*
.Sp
Tidies a pod file. It's a good idea to run this on a pod file you've
patched.

### Build system

Subsection "Build system"
The Perl build system on *nix-like systems starts with the *Configure*
script in the root directory.

Platform-specific pieces of the build system also live in
platform-specific directories like *win32/*, *vms/*, etc.
Windows and \s-1VMS\s0 have their own Configure-like scripts, in their
respective directories.

The *Configure* script (or a platform-specific similar script) is
ultimately responsible for generating a *Makefile* from *Makefile.SH*.

The build system that Perl uses is called metaconfig. This system is
maintained separately from the Perl core, and knows about the
platform-specific Configure-like scripts, as well as *Configure*
itself.

The metaconfig system has its own git repository. Please see its \s-1README\s0
file in <https://github.com/Perl/metaconfig> for more details.

The *Cross* directory contains various files related to
cross-compiling Perl. See *Cross/README* for more details.

### \fI\s-1AUTHORS\s0

Subsection "AUTHORS"
This file lists everyone who's contributed to Perl. If you submit a
patch, you should add your name to this file as part of the patch.

### \fI\s-1MANIFEST\s0

Subsection "MANIFEST"
The *\s-1MANIFEST\s0* file in the root of the source tree contains a list of
every file in the Perl core, as well as a brief description of each
file.

You can get an overview of all the files with this command:

.Vb 1
  % perl -lne \*(Aqprint if /^[^\\/]+\\.[ch]\\s+/\*(Aq MANIFEST
.Ve
