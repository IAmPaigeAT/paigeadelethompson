+++
manpage_name = "perlnumber"
manpage_section = "1"
keywords = ["header", "see", "also", "overload", "perlop"]
title = "perlnumber(1)"
manpage_format = "troff"
description = "This document describes how Perl internally handles numeric values. Perls operator overloading facility is completely ignored here.  Operator overloading allows user-defined behaviors for numbers, such as operations over arbitrarily large integers..."
date = "2022-02-19"
operating_system = "macos"
operating_system_version = "15.3"
author = "None Specified"
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLNUMBER 1"
PERLNUMBER 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlnumber - semantics of numbers and numeric operations in Perl

## SYNOPSIS

Header "SYNOPSIS"
.Vb 7
    $n = 1234;              # decimal integer
    $n = 0b1110011;         # binary integer
    $n = 01234;             # octal integer
    $n = 0x1234;            # hexadecimal integer
    $n = 12.34e-56;         # exponential notation
    $n = "-12.34e56";       # number specified as a string
    $n = "1234";            # number specified as a string
.Ve

## DESCRIPTION

Header "DESCRIPTION"
This document describes how Perl internally handles numeric values.

Perl's operator overloading facility is completely ignored here.  Operator
overloading allows user-defined behaviors for numbers, such as operations
over arbitrarily large integers, floating points numbers with arbitrary
precision, operations over \*(L"exotic\*(R" numbers such as modular arithmetic or
p-adic arithmetic, and so on.  See overload for details.

## Storing numbers

Header "Storing numbers"
Perl can internally represent numbers in 3 different ways: as native
integers, as native floating point numbers, and as decimal strings.
Decimal strings may have an exponential notation part, as in \f(CW"12.34e-56".
*Native* here means \*(L"a format supported by the C compiler which was used
to build perl\*(R".

The term \*(L"native\*(R" does not mean quite as much when we talk about native
integers, as it does when native floating point numbers are involved.
The only implication of the term \*(L"native\*(R" on integers is that the limits for
the maximal and the minimal supported true integral quantities are close to
powers of 2.  However, \*(L"native\*(R" floats have a most fundamental
restriction: they may represent only those numbers which have a relatively
\*(L"short\*(R" representation when converted to a binary fraction.  For example,
0.9 cannot be represented by a native float, since the binary fraction
for 0.9 is infinite:

.Vb 1
  binary0.1110011001100...
.Ve

with the sequence \f(CW1100 repeating again and again.  In addition to this
limitation,  the exponent of the binary number is also restricted when it
is represented as a floating point number.  On typical hardware, floating
point values can store numbers with up to 53 binary digits, and with binary
exponents between -1024 and 1024.  In decimal representation this is close
to 16 decimal digits and decimal exponents in the range of -304..304.
The upshot of all this is that Perl cannot store a number like
12345678901234567 as a floating point number on such architectures without
loss of information.

Similarly, decimal strings can represent only those numbers which have a
finite decimal expansion.  Being strings, and thus of arbitrary length, there
is no practical limit for the exponent or number of decimal digits for these
numbers.  (But realize that what we are discussing the rules for just the
*storage* of these numbers.  The fact that you can store such \*(L"large\*(R" numbers
does not mean that the *operations* over these numbers will use all
of the significant digits.
See \*(L"Numeric operators and numeric conversions\*(R" for details.)

In fact numbers stored in the native integer format may be stored either
in the signed native form, or in the unsigned native form.  Thus the limits
for Perl numbers stored as native integers would typically be -2**31..2**32-1,
with appropriate modifications in the case of 64-bit integers.  Again, this
does not mean that Perl can do operations only over integers in this range:
it is possible to store many more integers in floating point format.

Summing up, Perl numeric values can store only those numbers which have
a finite decimal expansion or a \*(L"short\*(R" binary expansion.

## Numeric operators and numeric conversions

Header "Numeric operators and numeric conversions"
As mentioned earlier, Perl can store a number in any one of three formats,
but most operators typically understand only one of those formats.  When
a numeric value is passed as an argument to such an operator, it will be
converted to the format understood by the operator.

Six such conversions are possible:

.Vb 6
  native integer        --> native floating point       (*)
  native integer        --> decimal string
  native floating_point --> native integer              (*)
  native floating_point --> decimal string              (*)
  decimal string        --> native integer
  decimal string        --> native floating point       (*)
.Ve

These conversions are governed by the following general rules:

- \(bu
If the source number can be represented in the target form, that
representation is used.

- \(bu
If the source number is outside of the limits representable in the target form,
a representation of the closest limit is used.  (*Loss of information*)

- \(bu
If the source number is between two numbers representable in the target form,
a representation of one of these numbers is used.  (*Loss of information*)

- \(bu
In \f(CW\*(C`native floating point --> native integer\*(C' conversions the magnitude
of the result is less than or equal to the magnitude of the source.
(*\*(L"Rounding to zero\*(R".*)

- \(bu
If the \f(CW\*(C`decimal string --> native integer\*(C' conversion cannot be done
without loss of information, the result is compatible with the conversion
sequence \f(CW\*(C`decimal_string --> native_floating_point --> native_integer\*(C'.
In particular, rounding is strongly biased to 0, though a number like
\f(CW"0.99999999999999999999" has a chance of being rounded to 1.

**\s-1RESTRICTION\s0**: The conversions marked with \f(CW\*(C`(*)\*(C' above involve steps
performed by the C compiler.  In particular, bugs/features of the compiler
used may lead to breakage of some of the above rules.

## Flavors of Perl numeric operations

Header "Flavors of Perl numeric operations"
Perl operations which take a numeric argument treat that argument in one
of four different ways: they may force it to one of the integer/floating/
string formats, or they may behave differently depending on the format of
the operand.  Forcing a numeric value to a particular format does not
change the number stored in the value.

All the operators which need an argument in the integer format treat the
argument as in modular arithmetic, e.g., \f(CW\*(C`mod 2**32\*(C' on a 32-bit
architecture.  \f(CW\*(C`sprintf "%u", -1\*(C' therefore provides the same result as
\f(CW\*(C`sprintf "%u", ~0\*(C'.

- Arithmetic operators
Item "Arithmetic operators"
The binary operators \f(CW\*(C`+\*(C' \f(CW\*(C`-\*(C' \f(CW\*(C`*\*(C' \f(CW\*(C`/\*(C' \f(CW\*(C`%\*(C' \f(CW\*(C`==\*(C' \f(CW\*(C`!=\*(C' \f(CW\*(C`>\*(C' \f(CW\*(C`<\*(C'
\f(CW\*(C`>=\*(C' \f(CW\*(C`<=\*(C' and the unary operators \f(CW\*(C`-\*(C' \f(CW\*(C`abs\*(C' and \f(CW\*(C`--\*(C' will
attempt to convert arguments to integers.  If both conversions are possible
without loss of precision, and the operation can be performed without
loss of precision then the integer result is used.  Otherwise arguments are
converted to floating point format and the floating point result is used.
The caching of conversions (as described above) means that the integer
conversion does not throw away fractional parts on floating point numbers.

- ++
\f(CW\*(C`++\*(C' behaves as the other operators above, except that if it is a string
matching the format \f(CW\*(C`/^[a-zA-Z]*[0-9]*\\z/\*(C' the string increment described
in perlop is used.
.ie n .IP "Arithmetic operators during ""use integer""" 4
.el .IP "Arithmetic operators during \f(CWuse integer" 4
Item "Arithmetic operators during use integer"
In scopes where \f(CW\*(C`use integer;\*(C' is in force, nearly all the operators listed
above will force their argument(s) into integer format, and return an integer
result.  The exceptions, \f(CW\*(C`abs\*(C', \f(CW\*(C`++\*(C' and \f(CW\*(C`--\*(C', do not change their
behavior with \f(CW\*(C`use integer;\*(C'

- Other mathematical operators
Item "Other mathematical operators"
Operators such as \f(CW\*(C`**\*(C', \f(CW\*(C`sin\*(C' and \f(CW\*(C`exp\*(C' force arguments to floating point
format.

- Bitwise operators
Item "Bitwise operators"
Arguments are forced into the integer format if not strings.
.ie n .IP "Bitwise operators during ""use integer""" 4
.el .IP "Bitwise operators during \f(CWuse integer" 4
Item "Bitwise operators during use integer"
forces arguments to integer format. Also shift operations internally use
signed integers rather than the default unsigned.

- Operators which expect an integer
Item "Operators which expect an integer"
force the argument into the integer format.  This is applicable
to the third and fourth arguments of \f(CW\*(C`sysread\*(C', for example.

- Operators which expect a string
Item "Operators which expect a string"
force the argument into the string format.  For example, this is
applicable to \f(CW\*(C`printf "%s", $value\*(C'.

Though forcing an argument into a particular form does not change the
stored number, Perl remembers the result of such conversions.  In
particular, though the first such conversion may be time-consuming,
repeated operations will not need to redo the conversion.

## AUTHOR

Header "AUTHOR"
Ilya Zakharevich \f(CW\*(C`ilya@math.ohio-state.edu\*(C'

Editorial adjustments by Gurusamy Sarathy <gsar@ActiveState.com>

Updates for 5.8.0 by Nicholas Clark <nick@ccl4.org>

## SEE ALSO

Header "SEE ALSO"
overload, perlop
