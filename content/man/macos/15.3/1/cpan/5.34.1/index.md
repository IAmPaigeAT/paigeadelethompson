+++
author = "None Specified"
date = "2024-12-14"
keywords = ["header", "see", "also", "most", "behaviour", "including", "environment", "variables", "and", "configuration", "comes", "directly", "from", "s-1cpan", "s0", "pm", "source", "availability", "this", "code", "is", "in", "github", "the", "repository", "vb", "1", "https", "com", "andk", "cpanpm", "ve", "used", "to", "be", "tracked", "separately", "another", "repo", "but", "canonical", "now", "above", "credits", "japheth", "cleaver", "added", "bits", "allow", "a", "forced", "install", "f", "jim", "brandt", "suggest", "provided", "initial", "implementation", "for", "up-to-date", "changes", "features", "adam", "kennedy", "pointed", "out", "that", "fbexit", "causes", "problems", "on", "windows", "where", "script", "ends", "up", "with", "bat", "extension", "author", "brian", "d", "foy", "cw", "c", "bdfoy", "cpan", "org", "copyright", "2001-2015", "all", "rights", "reserved", "you", "may", "redistribute", "under", "same", "terms", "as", "perl", "itself"]
detected_package_version = "5.34.1"
title = "cpan(1)"
operating_system_version = "15.3"
manpage_name = "cpan"
manpage_format = "troff"
description = "This script provides a command interface (not a shell) to s-1CPAN.s0 At the moment it uses s-1CPANs0.pm to do the work, but it is not a one-shot command runner for s-1CPANs0.pm. Creates a s-1CPANs0.pm autobundle with CPAN::Shell->autobundle. Shows..."
manpage_section = "1"
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "CPAN 1"
CPAN 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

cpan - easily interact with CPAN from the command line

## SYNOPSIS

Header "SYNOPSIS"
.Vb 2
        # with arguments and no switches, installs specified modules
        cpan module_name [ module_name ... ]

        # with switches, installs modules with extra behavior
        cpan [-cfFimtTw] module_name [ module_name ... ]

        # use local::lib
        cpan -I module_name [ module_name ... ]

        # one time mirror override for faster mirrors
        cpan -p ...

        # with just the dot, install from the distribution in the
        # current directory
        cpan .

        # without arguments, starts CPAN.pm shell
        cpan

        # without arguments, but some switches
        cpan [-ahpruvACDLOPX]
.Ve

## DESCRIPTION

Header "DESCRIPTION"
This script provides a command interface (not a shell) to \s-1CPAN.\s0 At the
moment it uses \s-1CPAN\s0.pm to do the work, but it is not a one-shot command
runner for \s-1CPAN\s0.pm.

### Options

Subsection "Options"

- -a
Item "-a"
Creates a \s-1CPAN\s0.pm autobundle with CPAN::Shell->autobundle.

- -A module [ module ... ]
Item "-A module [ module ... ]"
Shows the primary maintainers for the specified modules.

- -c module
Item "-c module"
Runs a `make clean` in the specified module's directories.

- -C module [ module ... ]
Item "-C module [ module ... ]"
Show the *Changes* files for the specified modules

- -D module [ module ... ]
Item "-D module [ module ... ]"
Show the module details. This prints one line for each out-of-date module
(meaning, modules locally installed but have newer versions on \s-1CPAN\s0).
Each line has three columns: module name, local version, and \s-1CPAN\s0
version.

- -f
Item "-f"
Force the specified action, when it normally would have failed. Use this
to install a module even if its tests fail. When you use this option,
-i is not optional for installing a module when you need to force it:
.Sp
.Vb 1
        % cpan -f -i Module::Foo
.Ve

- -F
Item "-F"
Turn off \s-1CPAN\s0.pm's attempts to lock anything. You should be careful with
this since you might end up with multiple scripts trying to muck in the
same directory. This isn't so much of a concern if you're loading a special
config with \f(CW\*(C`-j\*(C', and that config sets up its own work directories.

- -g module [ module ... ]
Item "-g module [ module ... ]"
Downloads to the current directory the latest distribution of the module.

- -G module [ module ... ]
Item "-G module [ module ... ]"
\s-1UNIMPLEMENTED\s0
.Sp
Download to the current directory the latest distribution of the
modules, unpack each distribution, and create a git repository for each
distribution.
.Sp
If you want this feature, check out Yanick Champoux's \f(CW\*(C`Git::CPAN::Patch\*(C'
distribution.

- -h
Item "-h"
Print a help message and exit. When you specify \f(CW\*(C`-h\*(C', it ignores all
of the other options and arguments.

- -i module [ module ... ]
Item "-i module [ module ... ]"
Install the specified modules. With no other switches, this switch
is implied.

- -I
Item "-I"
Load \f(CW\*(C`local::lib\*(C' (think like \f(CW\*(C`-I\*(C' for loading lib paths). Too bad
\f(CW\*(C`-l\*(C' was already taken.

- -j Config.pm
Item "-j Config.pm"
Load the file that has the \s-1CPAN\s0 configuration data. This should have the
same format as the standard *CPAN/Config.pm* file, which defines
\f(CW$CPAN::Config as an anonymous hash.

- -J
Item "-J"
Dump the configuration in the same format that \s-1CPAN\s0.pm uses. This is useful
for checking the configuration as well as using the dump as a starting point
for a new, custom configuration.

- -l
Item "-l"
List all installed modules with their versions

- -L author [ author ... ]
Item "-L author [ author ... ]"
List the modules by the specified authors.

- -m
Item "-m"
Make the specified modules.

- -M mirror1,mirror2,...
Item "-M mirror1,mirror2,..."
A comma-separated list of mirrors to use for just this run. The \f(CW\*(C`-P\*(C'
option can find them for you automatically.

- -n
Item "-n"
Do a dry run, but don't actually install anything. (unimplemented)

- -O
Item "-O"
Show the out-of-date modules.

- -p
Item "-p"
Ping the configured mirrors and print a report

- -P
Item "-P"
Find the best mirrors you could be using and use them for the current
session.

- -r
Item "-r"
Recompiles dynamically loaded modules with CPAN::Shell->recompile.

- -s
Item "-s"
Drop in the \s-1CPAN\s0.pm shell. This command does this automatically if you don't
specify any arguments.

- -t module [ module ... ]
Item "-t module [ module ... ]"
Run a `make test` on the specified modules.

- -T
Item "-T"
Do not test modules. Simply install them.

- -u
Item "-u"
Upgrade all installed modules. Blindly doing this can really break things,
so keep a backup.

- -v
Item "-v"
Print the script version and \s-1CPAN\s0.pm version then exit.

- -V
Item "-V"
Print detailed information about the cpan client.

- -w
Item "-w"
\s-1UNIMPLEMENTED\s0
.Sp
Turn on cpan warnings. This checks various things, like directory permissions,
and tells you about problems you might have.

- -x module [ module ... ]
Item "-x module [ module ... ]"
Find close matches to the named modules that you think you might have
mistyped. This requires the optional installation of Text::Levenshtein or
Text::Levenshtein::Damerau.

- -X
Item "-X"
Dump all the namespaces to standard output.

### Examples

Subsection "Examples"
.Vb 2
        # print a help message
        cpan -h

        # print the version numbers
        cpan -v

        # create an autobundle
        cpan -a

        # recompile modules
        cpan -r

        # upgrade all installed modules
        cpan -u

        # install modules ( sole -i is optional )
        cpan -i Netscape::Booksmarks Business::ISBN

        # force install modules ( must use -i )
        cpan -fi CGI::Minimal URI

        # install modules but without testing them
        cpan -Ti CGI::Minimal URI
.Ve

### Environment variables

Subsection "Environment variables"
There are several components in \s-1CPAN\s0.pm that use environment variables.
The build tools, ExtUtils::MakeMaker and Module::Build use some,
while others matter to the levels above them. Some of these are specified
by the Perl Toolchain Gang:

Lancaster Concensus: <https://github.com/Perl-Toolchain-Gang/toolchain-site/blob/master/lancaster-consensus.md>

Oslo Concensus: <https://github.com/Perl-Toolchain-Gang/toolchain-site/blob/master/oslo-consensus.md>

- \s-1NONINTERACTIVE_TESTING\s0
Item "NONINTERACTIVE_TESTING"
Assume no one is paying attention and skips prompts for distributions
that do that correctly. \f(CWcpan(1) sets this to \f(CW1 unless it already
has a value (even if that value is false).

- \s-1PERL_MM_USE_DEFAULT\s0
Item "PERL_MM_USE_DEFAULT"
Use the default answer for a prompted questions. \f(CWcpan(1) sets this
to \f(CW1 unless it already has a value (even if that value is false).

- \s-1CPAN_OPTS\s0
Item "CPAN_OPTS"
As with \f(CW\*(C`PERL5OPT\*(C', a string of additional \f(CWcpan(1) options to
add to those you specify on the command line.

- \s-1CPANSCRIPT_LOGLEVEL\s0
Item "CPANSCRIPT_LOGLEVEL"
The log level to use, with either the embedded, minimal logger or
Log::Log4perl if it is installed. Possible values are the same as
the \f(CW\*(C`Log::Log4perl\*(C' levels: \f(CW\*(C`TRACE\*(C', \f(CW\*(C`DEBUG\*(C', \f(CW\*(C`INFO\*(C', \f(CW\*(C`WARN\*(C',
\f(CW\*(C`ERROR\*(C', and \f(CW\*(C`FATAL\*(C'. The default is \f(CW\*(C`INFO\*(C'.

- \s-1GIT_COMMAND\s0
Item "GIT_COMMAND"
The path to the \f(CW\*(C`git\*(C' binary to use for the Git features. The default
is \f(CW\*(C`/usr/local/bin/git\*(C'.

## EXIT VALUES

Header "EXIT VALUES"
The script exits with zero if it thinks that everything worked, or a
positive number if it thinks that something failed. Note, however, that
in some cases it has to divine a failure by the output of things it does
not control. For now, the exit codes are vague:

.Vb 1
        1       An unknown error

        2       The was an external problem

        4       There was an internal problem with the script

        8       A module failed to install
.Ve

## TO DO

Header "TO DO"
* one shot configuration values from the command line

## BUGS

Header "BUGS"
* none noted

## SEE ALSO

Header "SEE ALSO"
Most behaviour, including environment variables and configuration,
comes directly from \s-1CPAN\s0.pm.

## SOURCE AVAILABILITY

Header "SOURCE AVAILABILITY"
This code is in Github in the \s-1CPAN\s0.pm repository:

.Vb 1
        https://github.com/andk/cpanpm
.Ve

The source used to be tracked separately in another GitHub repo,
but the canonical source is now in the above repo.

## CREDITS

Header "CREDITS"
Japheth Cleaver added the bits to allow a forced install (-f).

Jim Brandt suggest and provided the initial implementation for the
up-to-date and Changes features.

Adam Kennedy pointed out that **exit()** causes problems on Windows
where this script ends up with a .bat extension

## AUTHOR

Header "AUTHOR"
brian d foy, \f(CW\*(C`<bdfoy@cpan.org>\*(C'

## COPYRIGHT

Header "COPYRIGHT"
Copyright (c) 2001-2015, brian d foy, All Rights Reserved.

You may redistribute this under the same terms as Perl itself.
