+++
manpage_name = "json_xs"
author = "None Specified"
detected_package_version = "5.34.0"
date = "2018-11-15"
operating_system_version = "15.3"
manpage_section = "1"
title = "json_xs(1)"
description = "json_xs converts between some input and output formats (one of them is s-1JSONs0). The default input format is f(CW*(C`json*(C and the default output format is f(CW*(C`json-pretty*(C. Be slightly more verbose. Read a file in the given format fr..."
operating_system = "macos"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "JSON_XS 1"
JSON_XS 1 "2018-11-15" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

json_xs - JSON::XS commandline utility

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
   json_xs [-v] [-f inputformat] [-t outputformat]
.Ve

## DESCRIPTION

Header "DESCRIPTION"
*json_xs* converts between some input and output formats (one of them is
\s-1JSON\s0).

The default input format is \f(CW\*(C`json\*(C' and the default output format is
\f(CW\*(C`json-pretty\*(C'.

## OPTIONS

Header "OPTIONS"

- -v
Item "-v"
Be slightly more verbose.

- -f fromformat
Item "-f fromformat"
Read a file in the given format from \s-1STDIN.\s0
.Sp
\f(CW\*(C`fromformat\*(C' can be one of:

> 
- json - a json text encoded, either utf-8, utf16-be/le, utf32-be/le
Item "json - a json text encoded, either utf-8, utf16-be/le, utf32-be/le"
0

- cbor - \s-1CBOR\s0 (\s-1RFC 7049,\s0 \s-1CBOR::XS\s0), a kind of binary \s-1JSON\s0
Item "cbor - CBOR (RFC 7049, CBOR::XS), a kind of binary JSON"

- storable - a Storable frozen value
Item "storable - a Storable frozen value"

- storable-file - a Storable file (Storable has two incompatible formats)
Item "storable-file - a Storable file (Storable has two incompatible formats)"

- bencode - use Convert::Bencode, if available (used by torrent files, among others)
Item "bencode - use Convert::Bencode, if available (used by torrent files, among others)"

- clzf - Compress::LZF format (requires that module to be installed)
Item "clzf - Compress::LZF format (requires that module to be installed)"
.ie n .IP "eval - evaluate the given code as (non-utf-8) Perl, basically the reverse of ""-t dump""" 4
.el .IP "eval - evaluate the given code as (non-utf-8) Perl, basically the reverse of ``-t dump''" 4
Item "eval - evaluate the given code as (non-utf-8) Perl, basically the reverse of -t dump"

- yaml - \s-1YAML\s0 format (requires that module to be installed)
Item "yaml - YAML format (requires that module to be installed)"

- string - do not attempt to decode the file data
Item "string - do not attempt to decode the file data"
.ie n .IP "none - nothing is read, creates an ""undef"" scalar - mainly useful with ""-e""" 4
.el .IP "none - nothing is read, creates an \f(CWundef scalar - mainly useful with \f(CW-e" 4
Item "none - nothing is read, creates an undef scalar - mainly useful with -e"



> 


- -t toformat
Item "-t toformat"
.PD
Write the file in the given format to \s-1STDOUT.\s0
.Sp
\f(CW\*(C`toformat\*(C' can be one of:

> 
- json, json-utf-8 - json, utf-8 encoded
Item "json, json-utf-8 - json, utf-8 encoded"
0

- json-pretty - as above, but pretty-printed
Item "json-pretty - as above, but pretty-printed"

- json-utf-16le, json-utf-16be - little endian/big endian utf-16
Item "json-utf-16le, json-utf-16be - little endian/big endian utf-16"

- json-utf-32le, json-utf-32be - little endian/big endian utf-32
Item "json-utf-32le, json-utf-32be - little endian/big endian utf-32"

- cbor - \s-1CBOR\s0 (\s-1RFC 7049,\s0 \s-1CBOR::XS\s0), a kind of binary \s-1JSON\s0
Item "cbor - CBOR (RFC 7049, CBOR::XS), a kind of binary JSON"

- cbor-packed - \s-1CBOR\s0 using extensions to make it smaller
Item "cbor-packed - CBOR using extensions to make it smaller"

- storable - a Storable frozen value in network format
Item "storable - a Storable frozen value in network format"

- storable-file - a Storable file in network format (Storable has two incompatible formats)
Item "storable-file - a Storable file in network format (Storable has two incompatible formats)"

- bencode - use Convert::Bencode, if available (used by torrent files, among others)
Item "bencode - use Convert::Bencode, if available (used by torrent files, among others)"

- clzf - Compress::LZF format
Item "clzf - Compress::LZF format"

- yaml - \s-1YAML::XS\s0 format
Item "yaml - YAML::XS format"

- dump - Data::Dump
Item "dump - Data::Dump"

- dumper - Data::Dumper
Item "dumper - Data::Dumper"

- string - writes the data out as if it were a string
Item "string - writes the data out as if it were a string"
.ie n .IP "none - nothing gets written, mainly useful together with ""-e""" 4
.el .IP "none - nothing gets written, mainly useful together with \f(CW-e" 4
Item "none - nothing gets written, mainly useful together with -e"
.PD
Note that Data::Dumper doesn't handle self-referential data structures
correctly - use \*(L"dump\*(R" instead.



> 


- -e code
Item "-e code"
Evaluate perl code after reading the data and before writing it out again
- can be used to filter, create or extract data. The data that has been
written is in \f(CW$_, and whatever is in there is written out afterwards.

## EXAMPLES

Header "EXAMPLES"
.Vb 1
   json_xs -t none <isitreally.json
.Ve

\*(L"\s-1JSON\s0 Lint\*(R" - tries to parse the file *isitreally.json* as \s-1JSON\s0 - if it
is valid \s-1JSON,\s0 the command outputs nothing, otherwise it will print an
error message and exit with non-zero exit status.

.Vb 1
   <src.json json_xs >pretty.json
.Ve

Prettify the \s-1JSON\s0 file *src.json* to *dst.json*.

.Vb 1
   json_xs -f storable-file <file
.Ve

Read the serialised Storable file *file* and print a human-readable \s-1JSON\s0
version of it to \s-1STDOUT.\s0

.Vb 1
   json_xs -f storable-file -t yaml <file
.Ve

Same as above, but write \s-1YAML\s0 instead (not using \s-1JSON\s0 at all :)

.Vb 1
   json_xs -f none -e \*(Aq$_ = [1, 2, 3]\*(Aq
.Ve

Dump the perl array as \s-1UTF-8\s0 encoded \s-1JSON\s0 text.

.Vb 1
   <torrentfile json_xs -f bencode -e \*(Aq$_ = join "\\n", map @$_, @\{$_->\{"announce-list"\}\}\*(Aq -t string
.Ve

Print the tracker list inside a torrent file.

.Vb 1
   lwp-request http://cpantesters.perl.org/show/JSON-XS.json | json_xs
.Ve

Fetch the cpan-testers result summary \f(CW\*(C`JSON::XS\*(C' and pretty-print it.

## AUTHOR

Header "AUTHOR"
Copyright (C) 2008 Marc Lehmann <json@schmorp.de>
