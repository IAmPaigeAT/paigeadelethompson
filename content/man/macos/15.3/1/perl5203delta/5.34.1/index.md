+++
operating_system = "macos"
author = "None Specified"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_section = "1"
description = "This document describes differences between the 5.20.2 release and the 5.20.3 release. If you are upgrading from an earlier release such as 5.20.1, first read perl5202delta, which describes differences between 5.20.1 and 5.20.2. There are no chang..."
detected_package_version = "5.34.1"
title = "perl5203delta(1)"
date = "2022-02-19"
operating_system_version = "15.3"
manpage_format = "troff"
manpage_name = "perl5203delta"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5203DELTA 1"
PERL5203DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5203delta - what is new for perl v5.20.3

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.20.2 release and the 5.20.3
release.

If you are upgrading from an earlier release such as 5.20.1, first read
perl5202delta, which describes differences between 5.20.1 and 5.20.2.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.20.2.  If any exist,
they are bugs, and we request that you submit a report.  See \*(L"Reporting Bugs\*(R"
below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Errno has been upgraded from version 1.20_05 to 1.20_06.
.Sp
Add **-P** to the pre-processor command-line on \s-1GCC 5.\s0  \s-1GCC\s0 added extra line
directives, breaking parsing of error code definitions.
[\s-1GH\s0 #14491] <https://github.com/Perl/perl5/issues/14491>

- \(bu
Module::CoreList has been upgraded from version 5.20150214 to 5.20150822.
.Sp
Updated to cover the latest releases of Perl.

- \(bu
perl5db.pl has been upgraded from 1.44 to 1.44_01.
.Sp
The debugger would cause an assertion failure.
[\s-1GH\s0 #14605] <https://github.com/Perl/perl5/issues/14605>

## Documentation

Header "Documentation"

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perlfunc*
Subsection "perlfunc"

- \(bu
Mention that \f(CW\*(C`study()\*(C' is currently a no-op.

*perlguts*
Subsection "perlguts"

- \(bu
The \s-1OOK\s0 example has been updated to account for \s-1COW\s0 changes and a change in the
storage of the offset.

*perlhacktips*
Subsection "perlhacktips"

- \(bu
Documentation has been added illustrating the perils of assuming the contents
of static memory pointed to by the return values of Perl wrappers for C library
functions doesn't change.

*perlpodspec*
Subsection "perlpodspec"

- \(bu
The specification of the \s-1POD\s0 language is changing so that the default encoding
of PODs that aren't in \s-1UTF-8\s0 (unless otherwise indicated) is \s-1CP1252\s0 instead of
\s-1ISO-8859-1\s0 (Latin1).

## Utility Changes

Header "Utility Changes"

### h2ph

Subsection "h2ph"

- \(bu
**h2ph** now handles hexadecimal constants in the compiler's predefined macro
definitions, as visible in \f(CW$Config\{cppsymbols\}.
[\s-1GH\s0 #14491] <https://github.com/Perl/perl5/issues/14491>

## Testing

Header "Testing"

- \(bu
*t/perf/taint.t* has been added to see if optimisations with taint issues are
keeping things fast.

- \(bu
*t/porting/re_context.t* has been added to test that utf8 and its
dependencies only use the subset of the \f(CW\*(C`$1..$n\*(C' capture vars that
**Perl_save_re_context()** is hard-coded to localize, because that function has no
efficient way of determining at runtime what vars to localize.

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- Win32
Item "Win32"

> 0

- \(bu
.PD
Previously, when compiling with a 64-bit Visual \*(C+, every Perl \s-1XS\s0 module
(including \s-1CPAN\s0 ones) and Perl aware C file would unconditionally have around a
dozen warnings from *hv_func.h*.  These warnings have been silenced.  \s-1GCC\s0 (all
bitness) and 32-bit Visual \*(C+ were not affected.

- \(bu
**miniperl.exe** is now built with **-fno-strict-aliasing**, allowing 64-bit
builds to complete with \s-1GCC 4.8.\s0
[\s-1GH\s0 #14556] <https://github.com/Perl/perl5/issues/14556>



> 


## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Repeated global pattern matches in scalar context on large tainted strings were
exponentially slow depending on the current match position in the string.
[\s-1GH\s0 #14238] <https://github.com/Perl/perl5/issues/14238>

- \(bu
The original visible value of \f(CW$/ is now preserved
when it is set to an invalid value.  Previously if you set \f(CW$/ to a reference
to an array, for example, perl would produce a runtime error and not set PL_rs,
but Perl code that checked \f(CW$/ would see the array reference.
[\s-1GH\s0 #14245] <https://github.com/Perl/perl5/issues/14245>

- \(bu
Perl 5.14.0 introduced a bug whereby \f(CW\*(C`eval \{ LABEL: \}\*(C' would crash.  This has
been fixed.
[\s-1GH\s0 #14438] <https://github.com/Perl/perl5/issues/14438>

- \(bu
Extending an array cloned from a parent thread could result in \*(L"Modification of
a read-only value attempted\*(R" errors when attempting to modify the new elements.
[\s-1GH\s0 #14605] <https://github.com/Perl/perl5/issues/14605>

- \(bu
Several cases of data used to store environment variable contents in core C
code being potentially overwritten before being used have been fixed.
[\s-1GH\s0 #14476] <https://github.com/Perl/perl5/issues/14476>

- \(bu
\s-1UTF-8\s0 variable names used in array indexes, unquoted \s-1UTF-8\s0 HERE-document
terminators and \s-1UTF-8\s0 function names all now work correctly.
[\s-1GH\s0 #14601] <https://github.com/Perl/perl5/issues/14601>

- \(bu
A subtle bug introduced in Perl 5.20.2 involving \s-1UTF-8\s0 in regular expressions
and sometimes causing a crash has been fixed.  A new test script has been added
to test this fix; see under \*(L"Testing\*(R".
[\s-1GH\s0 #14600] <https://github.com/Perl/perl5/issues/14600>

- \(bu
Some patterns starting with \f(CW\*(C`/.*..../\*(C' matched against long strings have been
slow since Perl 5.8, and some of the form \f(CW\*(C`/.*..../i\*(C' have been slow since
Perl 5.18.  They are now all fast again.
[\s-1GH\s0 #14475] <https://github.com/Perl/perl5/issues/14475>

- \(bu
Warning fatality is now ignored when rewinding the stack.  This prevents
infinite recursion when the now fatal error also causes rewinding of the stack.
[\s-1GH\s0 #14319] <https://github.com/Perl/perl5/issues/14319>

- \(bu
\f(CW\*(C`setpgrp($nonzero)\*(C' (with one argument) was accidentally changed in Perl 5.16
to mean \f(CWsetpgrp(0).  This has been fixed.

- \(bu
A crash with \f(CW\*(C`%::=(); J->$\{\\"::"\}\*(C' has been fixed.
[\s-1GH\s0 #14790] <https://github.com/Perl/perl5/issues/14790>

- \(bu
Regular expression possessive quantifier Perl 5.20 regression now fixed.
\f(CW\*(C`qr/\*(C'*\s-1PAT\s0*\f(CW\*(C`\{\*(C'*min*,*max*\f(CW\*(C`\}+\*(C'\f(CW\*(C`/\*(C' is supposed to behave identically to
\f(CW\*(C`qr/(?>\*(C'*\s-1PAT\s0*\f(CW\*(C`\{\*(C'*min*,*max*\f(CW\*(C`\})/\*(C'.  Since Perl 5.20, this didn't work
if *min* and *max* were equal.
[\s-1GH\s0 #14857] <https://github.com/Perl/perl5/issues/14857>

- \(bu
Code like \f(CW\*(C`/$a[/\*(C' used to read the next line of input and treat it as though
it came immediately after the opening bracket.  Some invalid code consequently
would parse and run, but some code caused crashes, so this is now disallowed.
[\s-1GH\s0 #14462] <https://github.com/Perl/perl5/issues/14462>

## Acknowledgements

Header "Acknowledgements"
Perl 5.20.3 represents approximately 7 months of development since Perl 5.20.2
and contains approximately 3,200 lines of changes across 99 files from 26
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 1,500 lines of changes to 43 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.20.3:

Alex Vandiver, Andy Dougherty, Aristotle Pagaltzis, Chris 'BinGOs' Williams,
Craig A. Berry, Dagfinn Ilmari Mannsa\*oker, Daniel Dragan, David Mitchell,
Father Chrysostomos, H.Merijn Brand, James E Keenan, James McCoy, Jarkko
Hietaniemi, Karen Etheridge, Karl Williamson, kmx, Lajos Veres, Lukas Mai,
Matthew Horsfall, Petr Pi\*'saX, Randy Stauner, Ricardo Signes, Sawyer X, Steve
Hay, Tony Cook, Yves Orton.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the perl bug database at
https://rt.perl.org/ .  There may also be information at
http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send it
to perl5-security-report@perl.org.  This points to a closed subscription
unarchived mailing list, which includes all the core committers, who will be
able to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported.  Please only use this address for
security issues in the Perl core, not for modules independently distributed on
\s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
