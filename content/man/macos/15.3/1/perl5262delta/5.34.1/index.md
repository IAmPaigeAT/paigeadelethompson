+++
manpage_section = "1"
manpage_name = "perl5262delta"
description = "This document describes differences between the 5.26.1 release and the 5.26.2 release. If you are upgrading from an earlier release such as 5.26.0, first read perl5261delta, which describes differences between 5.26.0 and 5.26.1. A crafted regular ..."
manpage_format = "troff"
detected_package_version = "5.34.1"
date = "2022-02-19"
operating_system_version = "15.3"
operating_system = "macos"
author = "None Specified"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
title = "perl5262delta(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5262DELTA 1"
PERL5262DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5262delta - what is new for perl v5.26.2

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.26.1 release and the 5.26.2
release.

If you are upgrading from an earlier release such as 5.26.0, first read
perl5261delta, which describes differences between 5.26.0 and 5.26.1.

## Security

Header "Security"

### [\s-1CVE-2018-6797\s0] heap-buffer-overflow (\s-1WRITE\s0 of size 1) in S_regatom (regcomp.c)

Subsection "[CVE-2018-6797] heap-buffer-overflow (WRITE of size 1) in S_regatom (regcomp.c)"
A crafted regular expression could cause a heap buffer write overflow, with
control over the bytes written.
[\s-1GH\s0 #16185] <https://github.com/Perl/perl5/issues/16185>

### [\s-1CVE-2018-6798\s0] Heap-buffer-overflow in Perl_\|_byte_dump_string (utf8.c)

Subsection "[CVE-2018-6798] Heap-buffer-overflow in Perl__byte_dump_string (utf8.c)"
Matching a crafted locale dependent regular expression could cause a heap
buffer read overflow and potentially information disclosure.
[\s-1GH\s0 #16143] <https://github.com/Perl/perl5/issues/16143>

### [\s-1CVE-2018-6913\s0] heap-buffer-overflow in S_pack_rec

Subsection "[CVE-2018-6913] heap-buffer-overflow in S_pack_rec"
\f(CW\*(C`pack()\*(C' could cause a heap buffer write overflow with a large item count.
[\s-1GH\s0 #16098] <https://github.com/Perl/perl5/issues/16098>

### Assertion failure in Perl_\|_core_swash_init (utf8.c)

Subsection "Assertion failure in Perl__core_swash_init (utf8.c)"
Control characters in a supposed Unicode property name could cause perl to
crash.  This has been fixed.
[perl #132055] <https://rt.perl.org/Public/Bug/Display.html?id=132055>
[perl #132553] <https://rt.perl.org/Public/Bug/Display.html?id=132553>
[perl #132658] <https://rt.perl.org/Public/Bug/Display.html?id=132658>

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.26.1.  If any exist,
they are bugs, and we request that you submit a report.  See \*(L"Reporting
Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Module::CoreList has been upgraded from version 5.20170922_26 to 5.20180414_26.

- \(bu
PerlIO::via has been upgraded from version 0.16 to 0.17.

- \(bu
Term::ReadLine has been upgraded from version 1.16 to 1.17.

- \(bu
Unicode::UCD has been upgraded from version 0.68 to 0.69.

## Documentation

Header "Documentation"

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perluniprops*
Subsection "perluniprops"

- \(bu
This has been updated to note that \f(CW\*(C`\\p\{Word\}\*(C' now includes code points
matching the \f(CW\*(C`\\p\{Join_Control\}\*(C' property.  The change to the property was made
in Perl 5.18, but not documented until now.  There are currently only two code
points that match this property: U+200C (\s-1ZERO WIDTH\s0 NON-JOINER) and U+200D
(\s-1ZERO WIDTH JOINER\s0).

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- Windows
Item "Windows"
Visual \*(C+ compiler version detection has been improved to work on non-English
language systems.
[\s-1GH\s0 #16235] <https://github.com/Perl/perl5/issues/16235>
.Sp
We now set \f(CW$Config\{libpth\} correctly for 64-bit builds using Visual \*(C+
versions earlier than 14.1.
[\s-1GH\s0 #16269] <https://github.com/Perl/perl5/issues/16269>

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
The \f(CW\*(C`readpipe()\*(C' built-in function now checks at compile time that it has only
one parameter expression, and puts it in scalar context, thus ensuring that it
doesn't corrupt the stack at runtime.
[\s-1GH\s0 #2793] <https://github.com/Perl/perl5/issues/2793>

- \(bu
Fixed a use after free bug in \f(CW\*(C`pp_list\*(C' introduced in Perl 5.27.1.
[\s-1GH\s0 #16124] <https://github.com/Perl/perl5/issues/16124>

- \(bu
Parsing a \f(CW\*(C`sub\*(C' definition could cause a use after free if the \f(CW\*(C`sub\*(C' keyword
was followed by whitespace including newlines (and comments).
[\s-1GH\s0 #16097] <https://github.com/Perl/perl5/issues/16097>

- \(bu
The tokenizer now correctly adjusts a parse pointer when skipping whitespace in
an \f(CW\*(C` $\{identifier\} \*(C' construct.
[perl #131949] <https://rt.perl.org/Public/Bug/Display.html?id=131949>

- \(bu
Accesses to \f(CW\*(C`$\{^LAST_FH\}\*(C' no longer assert after using any of a variety of I/O
operations on a non-glob.
[\s-1GH\s0 #15372] <https://github.com/Perl/perl5/issues/15372>

- \(bu
\f(CW\*(C`sort\*(C' now performs correct reference counting when aliasing \f(CW$a and \f(CW$b,
thus avoiding premature destruction and leakage of scalars if they are
re-aliased during execution of the sort comparator.
[\s-1GH\s0 #11422] <https://github.com/Perl/perl5/issues/11422>

- \(bu
Some convoluted kinds of regexp no longer cause an arithmetic overflow when
compiled.
[\s-1GH\s0 #16113] <https://github.com/Perl/perl5/issues/16113>

- \(bu
Fixed a duplicate symbol failure with **-flto -mieee-fp** builds.  *pp.c*
defined \f(CW\*(C`_LIB_VERSION\*(C' which **-lieee** already defines.
[\s-1GH\s0 #16086] <https://github.com/Perl/perl5/issues/16086>

- \(bu
A \s-1NULL\s0 pointer dereference in the \f(CW\*(C`S_regmatch()\*(C' function has been fixed.
[perl #132017] <https://rt.perl.org/Public/Bug/Display.html?id=132017>

- \(bu
Failures while compiling code within other constructs, such as with string
interpolation and the right part of \f(CW\*(C`s///e\*(C' now cause compilation to abort
earlier.
.Sp
Previously compilation could continue in order to report other errors, but the
failed sub-parse could leave partly parsed constructs on the parser
shift-reduce stack, confusing the parser, leading to perl crashes.
[\s-1GH\s0 #14739] <https://github.com/Perl/perl5/issues/14739>

## Acknowledgements

Header "Acknowledgements"
Perl 5.26.2 represents approximately 7 months of development since Perl 5.26.1
and contains approximately 3,300 lines of changes across 82 files from 17
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 1,800 lines of changes to 36 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.26.2:

Aaron Crane, Abigail, Chris 'BinGOs' Williams, H.Merijn Brand, James E Keenan,
Jarkko Hietaniemi, John \s-1SJ\s0 Anderson, Karen Etheridge, Karl Williamson, Lukas
Mai, Renee Baecker, Sawyer X, Steve Hay, Todd Rinaldo, Tony Cook, Yves Orton,
Zefram.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database
at <https://rt.perl.org/> .  There may also be information at
<http://www.perl.org/> , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5,
you can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
