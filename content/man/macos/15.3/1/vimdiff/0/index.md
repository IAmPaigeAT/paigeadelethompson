+++
detected_package_version = "0"
description = "starts on two up to eight files. Each file gets its own window. The differences between the files are highlighted. This is a nice way to inspect changes and to move changes from one version to another version of the same file. See vim(1) for det..."
manpage_section = "1"
manpage_format = "troff"
title = "vimdiff(1)"
keywords = ["vim", "author", "most", "of", "was", "made", "by", "bram", "moolenaar", "with", "a", "lot", "help", "from", "others", "see", "credits", "in"]
operating_system = "macos"
author = "None Specified"
operating_system_version = "15.3"
manpage_name = "vimdiff"
date = "2021-01-01"
+++

VIMDIFF 1 "2021 June 13"

## NAME

vimdiff - edit between two and eight versions of a file with Vim and show differences

## SYNOPSIS

.br
vimdiff
[options] file1 file2 [file3 [file4 [file5 [file6 [file7 [file8]]]]]]

gvimdiff

## DESCRIPTION

Vimdiff
starts
Vim
on two up to eight files.
Each file gets its own window.
The differences between the files are highlighted.
This is a nice way to inspect changes and to move changes from one version
to another version of the same file.

See vim(1) for details about Vim itself.

When started as
gvimdiff
the GUI will be started, if available.

In each window the 'diff' option will be set, which causes the differences
to be highlighted.
.br
The 'wrap' and 'scrollbind' options are set to make the text look good.
.br
The 'foldmethod' option is set to "diff", which puts ranges of lines without
changes in a fold.  'foldcolumn' is set to two to make it easy to spot the
folds and open or close them.

## OPTIONS

Vertical splits are used to align the lines, as if the "-O" argument was used.
To use horizontal splits instead, use the "-o" argument.

For all other arguments see vim(1).

## SEE ALSO

vim(1)

## AUTHOR

Most of
Vim
was made by Bram Moolenaar, with a lot of help from others.
See ":help credits" in
Vim.
