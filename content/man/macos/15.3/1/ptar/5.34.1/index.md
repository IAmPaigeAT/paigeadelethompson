+++
detected_package_version = "5.34.1"
manpage_section = "1"
description = "ptar is a small, tar look-alike program that uses the perl module Archive::Tar to extract, create and list tar archives.     ptar -c [-v] [-z] [-C] [-f ARCHIVE_FILE | -] FILE FILE ...     ptar -c [-v] [-z] [-C] [-T index | -] [-f ARCHIVE_FILE | -]..."
date = "2024-12-14"
manpage_name = "ptar"
operating_system_version = "15.3"
manpage_format = "troff"
keywords = ["header", "see", "also", "fbtar", "1", "archive", "tar"]
author = "None Specified"
operating_system = "macos"
title = "ptar(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PTAR 1"
PTAR 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

ptar - a tar-like program written in perl

## DESCRIPTION

Header "DESCRIPTION"
ptar is a small, tar look-alike program that uses the perl module
Archive::Tar to extract, create and list tar archives.

## SYNOPSIS

Header "SYNOPSIS"
.Vb 5
    ptar -c [-v] [-z] [-C] [-f ARCHIVE_FILE | -] FILE FILE ...
    ptar -c [-v] [-z] [-C] [-T index | -] [-f ARCHIVE_FILE | -]
    ptar -x [-v] [-z] [-f ARCHIVE_FILE | -]
    ptar -t [-z] [-f ARCHIVE_FILE | -]
    ptar -h
.Ve

## OPTIONS

Header "OPTIONS"
.Vb 9
    c   Create ARCHIVE_FILE or STDOUT (-) from FILE
    x   Extract from ARCHIVE_FILE or STDIN (-)
    t   List the contents of ARCHIVE_FILE or STDIN (-)
    f   Name of the ARCHIVE_FILE to use. Default is \*(Aq./default.tar\*(Aq
    z   Read/Write zlib compressed ARCHIVE_FILE (not always available)
    v   Print filenames as they are added or extracted from ARCHIVE_FILE
    h   Prints this help message
    C   CPAN mode - drop 022 from permissions
    T   get names to create from file
.Ve

## SEE ALSO

Header "SEE ALSO"
**tar**\|(1), Archive::Tar.
