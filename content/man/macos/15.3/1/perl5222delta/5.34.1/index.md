+++
date = "2022-02-19"
description = "This document describes differences between the 5.22.1 release and the 5.22.2 release. If you are upgrading from an earlier release such as 5.22.0, first read perl5221delta, which describes differences between 5.22.0 and 5.22.1. This is s-1CVE-201..."
operating_system_version = "15.3"
title = "perl5222delta(1)"
manpage_name = "perl5222delta"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_section = "1"
detected_package_version = "5.34.1"
author = "None Specified"
operating_system = "macos"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5222DELTA 1"
PERL5222DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5222delta - what is new for perl v5.22.2

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.22.1 release and the 5.22.2
release.

If you are upgrading from an earlier release such as 5.22.0, first read
perl5221delta, which describes differences between 5.22.0 and 5.22.1.

## Security

Header "Security"

### Fix out of boundary access in Win32 path handling

Subsection "Fix out of boundary access in Win32 path handling"
This is \s-1CVE-2015-8608.\s0  For more information see
[\s-1GH\s0 #15067] <https://github.com/Perl/perl5/issues/15067>.
.ie n .SS "Fix loss of taint in ""canonpath()"""
.el .SS "Fix loss of taint in \f(CWcanonpath()"
Subsection "Fix loss of taint in canonpath()"
This is \s-1CVE-2015-8607.\s0  For more information see
[\s-1GH\s0 #15084] <https://github.com/Perl/perl5/issues/15084>.
.ie n .SS "Set proper umask before calling mkstemp(3)"
.el .SS "Set proper umask before calling \f(CWmkstemp(3)"
Subsection "Set proper umask before calling mkstemp(3)"
In 5.22.0 perl started setting umask to \f(CW0600 before calling \f(CWmkstemp(3)
and restoring it afterwards.  This wrongfully tells \f(CWopen(2) to strip the
owner read and write bits from the given mode before applying it, rather than
the intended negation of leaving only those bits in place.

Systems that use mode \f(CW0666 in \f(CWmkstemp(3) (like old versions of glibc)
create a file with permissions \f(CW0066, leaving world read and write permissions
regardless of current umask.

This has been fixed by using umask \f(CW0177 instead.

[\s-1GH\s0 #15135] <https://github.com/Perl/perl5/issues/15135>
.ie n .SS "Avoid accessing uninitialized memory in Win32 ""crypt()"""
.el .SS "Avoid accessing uninitialized memory in Win32 \f(CWcrypt()"
Subsection "Avoid accessing uninitialized memory in Win32 crypt()"
Validation that will detect both a short salt and invalid characters in the
salt has been added.

[\s-1GH\s0 #15091] <https://github.com/Perl/perl5/issues/15091>
.ie n .SS "Remove duplicate environment variables from ""environ"""
.el .SS "Remove duplicate environment variables from \f(CWenviron"
Subsection "Remove duplicate environment variables from environ"
Previously, if an environment variable appeared more than once in \f(CW\*(C`environ[]\*(C',
\f(CW%ENV would contain the last entry for that name, while a
typical \f(CW\*(C`getenv()\*(C' would return the first entry.  We now make sure \f(CW%ENV
contains the same as what \f(CW\*(C`getenv()\*(C' returns.

Secondly, we now remove duplicates from \f(CW\*(C`environ[]\*(C', so if a setting with that
name is set in \f(CW%ENV we won't pass an unsafe value to a child process.

This is \s-1CVE-2016-2381.\s0

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with Perl 5.22.1.  If any
exist, they are bugs, and we request that you submit a report.  See
\*(L"Reporting Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
File::Spec has been upgraded from version 3.56 to 3.56_01.
.Sp
\f(CW\*(C`canonpath()\*(C' now preserves taint.  See "Fix loss of taint in
\f(CW\*(C`canonpath()\*(C'".

- \(bu
Module::CoreList has been upgraded from version 5.20151213 to 5.20160429.
.Sp
The version number of Digest::SHA listed for Perl 5.18.4 was wrong and has
been corrected.  Likewise for the version number of Config in 5.18.3 and
5.18.4.
[\s-1GH\s0 #15202] <https://github.com/Perl/perl5/issues/15202>

## Documentation

Header "Documentation"

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perldiag*
Subsection "perldiag"

- \(bu
The explanation of the warning \*(L"unable to close filehandle \f(CW%s properly: \f(CW%s\*(R"
which can occur when doing an implicit close of a filehandle has been expanded
and improved.

*perlfunc*
Subsection "perlfunc"

- \(bu
The documentation of \f(CW\*(C`hex()\*(C' has been revised to clarify valid
inputs.

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
Dtrace builds now build successfully on systems with a newer dtrace that
require an input object file that uses the probes in the *.d* file.
.Sp
Previously the probe would fail and cause a build failure.
.Sp
[\s-1GH\s0 #13985] <https://github.com/Perl/perl5/issues/13985>

- \(bu
*Configure* no longer probes for *libnm* by default.  Originally this was the
\*(L"New Math\*(R" library, but the name has been re-used by the \s-1GNOME\s0 NetworkManager.
.Sp
[\s-1GH\s0 #15115] <https://github.com/Perl/perl5/issues/15115>

- \(bu
*Configure* now knows about gcc 5.

- \(bu
Compiling perl with **-DPERL_MEM_LOG** now works again.

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- Darwin
Item "Darwin"
Compiling perl with **-Dusecbacktrace** on Darwin now works again.
.Sp
[\s-1GH\s0 #15245] <https://github.com/Perl/perl5/issues/15245>

- \s-1OS\s0 X/Darwin
Item "OS X/Darwin"
Builds with both **-DDEBUGGING** and threading enabled would fail with a \*(L"panic:
free from wrong pool\*(R" error when built or tested from Terminal on \s-1OS X.\s0  This
was caused by perl's internal management of the environment conflicting with an
atfork handler using the libc \f(CW\*(C`setenv()\*(C' function to update the environment.
.Sp
Perl now uses \f(CW\*(C`setenv()\*(C'/\f(CW\*(C`unsetenv()\*(C' to update the environment on \s-1OS X.\s0
.Sp
[\s-1GH\s0 #14955] <https://github.com/Perl/perl5/issues/14955>

- ppc64el
Item "ppc64el"
The floating point format of ppc64el (Debian naming for little-endian PowerPC)
is now detected correctly.

- Tru64
Item "Tru64"
A test failure in *t/porting/extrefs.t* has been fixed.

## Internal Changes

Header "Internal Changes"

- \(bu
An unwarranted assertion in \f(CW\*(C`Perl_newATTRSUB_x()\*(C' has been removed.  If a stub
subroutine definition with a prototype has been seen, then any subsequent stub
(or definition) of the same subroutine with an attribute was causing an
assertion failure because of a null pointer.
.Sp
[\s-1GH\s0 #15081] <https://github.com/Perl/perl5/issues/15081>

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Calls to the placeholder \f(CW&PL_sv_yes used internally when an \f(CW\*(C`import()\*(C' or
\f(CW\*(C`unimport()\*(C' method isn't found now correctly handle scalar context.
[\s-1GH\s0 #14902] <https://github.com/Perl/perl5/issues/14902>

- \(bu
The \f(CW\*(C`pipe()\*(C' operator would assert for \f(CW\*(C`DEBUGGING\*(C' builds
instead of producing the correct error message.  The condition asserted on is
detected and reported on correctly without the assertions, so the assertions
were removed.
[\s-1GH\s0 #15015] <https://github.com/Perl/perl5/issues/15015>

- \(bu
In some cases, failing to parse a here-doc would attempt to use freed memory.
This was caused by a pointer not being restored correctly.
[\s-1GH\s0 #15009] <https://github.com/Perl/perl5/issues/15009>

- \(bu
Perl now reports more context when it sees an array where it expects to see an
operator, and avoids an assertion failure.
[\s-1GH\s0 #14472] <https://github.com/Perl/perl5/issues/14472>

- \(bu
If a here-doc was found while parsing another operator, the parser had already
read end of file, and the here-doc was not terminated, perl could produce an
assertion or a segmentation fault.  This now reliably complains about the
unterminated here-doc.
[\s-1GH\s0 #14789] <https://github.com/Perl/perl5/issues/14789>

- \(bu
Parsing beyond the end of the buffer when processing a \f(CW\*(C`#line\*(C' directive with
no filename is now avoided.
[\s-1GH\s0 #15139] <https://github.com/Perl/perl5/issues/15139>

- \(bu
Perl 5.22.0 added support for the C99 hexadecimal floating point notation, but
sometimes misparsed hex floats.  This has been fixed.
[\s-1GH\s0 #15120] <https://github.com/Perl/perl5/issues/15120>

- \(bu
Certain regex patterns involving a complemented posix class in an inverted
bracketed character class, and matching something else optionally would
improperly fail to match.  An example of one that could fail is
\f(CW\*(C`qr/_?[^\\Wbar]\\x\{100\}/\*(C'.  This has been fixed.
[\s-1GH\s0 #15181] <https://github.com/Perl/perl5/issues/15181>

- \(bu
Fixed an issue with \f(CW\*(C`pack()\*(C' where \f(CW\*(C`pack "H"\*(C' (and
\f(CW\*(C`pack "h"\*(C') could read past the source when given a non-utf8 source and a
utf8 target.
[\s-1GH\s0 #14977] <https://github.com/Perl/perl5/issues/14977>

- \(bu
Fixed some cases where perl would abort due to a segmentation fault, or a
C-level assert.
[\s-1GH\s0 #14941] <https://github.com/Perl/perl5/issues/14941>
[\s-1GH\s0 #14962] <https://github.com/Perl/perl5/issues/14962>
[\s-1GH\s0 #14963] <https://github.com/Perl/perl5/issues/14963>
[\s-1GH\s0 #14997] <https://github.com/Perl/perl5/issues/14997>
[\s-1GH\s0 #15039] <https://github.com/Perl/perl5/issues/15039>
[\s-1GH\s0 #15247] <https://github.com/Perl/perl5/issues/15247>
[\s-1GH\s0 #15251] <https://github.com/Perl/perl5/issues/15251>

- \(bu
A memory leak when setting \f(CW$ENV\{foo\} on Darwin has been fixed.
[\s-1GH\s0 #14955] <https://github.com/Perl/perl5/issues/14955>

- \(bu
Perl now correctly raises an error when trying to compile patterns with
unterminated character classes while there are trailing backslashes.
[\s-1GH\s0 #14919] <https://github.com/Perl/perl5/issues/14919>

- \(bu
\f(CW\*(C`NOTHING\*(C' regops and \f(CW\*(C`EXACTFU_SS\*(C' regops in \f(CW\*(C`make_trie()\*(C' are now handled
properly.
[\s-1GH\s0 #14945] <https://github.com/Perl/perl5/issues/14945>

- \(bu
Perl now only tests \f(CW\*(C`semctl()\*(C' if we have everything needed to use it.  In
FreeBSD the \f(CW\*(C`semctl()\*(C' entry point may exist, but it can be disabled by
policy.
[\s-1GH\s0 #15180] <https://github.com/Perl/perl5/issues/15180>

- \(bu
A regression that allowed undeclared barewords as hash keys to work despite
strictures has been fixed.
[\s-1GH\s0 #15099] <https://github.com/Perl/perl5/issues/15099>

- \(bu
As an optimization (introduced in Perl 5.20.0), \f(CW\*(C`uc()\*(C',
\f(CW\*(C`lc()\*(C', \f(CW\*(C`ucfirst()\*(C' and
\f(CW\*(C`lcfirst()\*(C' sometimes modify their argument in-place
rather than returning a modified copy.  The criteria for this optimization has
been made stricter to avoid these functions accidentally modifying in-place
when they should not, which has been happening in some cases, e.g. in
List::Util.

- \(bu
Excessive memory usage in the compilation of some regular expressions involving
non-ASCII characters has been reduced.  A more complete fix is forthcoming in
Perl 5.24.0.

## Acknowledgements

Header "Acknowledgements"
Perl 5.22.2 represents approximately 5 months of development since Perl 5.22.1
and contains approximately 3,000 lines of changes across 110 files from 24
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 1,500 lines of changes to 52 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.22.2:

Aaron Crane, Abigail, Andreas Ko\*:nig, Aristotle Pagaltzis, Chris 'BinGOs'
Williams, Craig A. Berry, Dagfinn Ilmari Mannsa\*oker, David Golden, David
Mitchell, H.Merijn Brand, James E Keenan, Jarkko Hietaniemi, Karen Etheridge,
Karl Williamson, Matthew Horsfall, Niko Tyni, Ricardo Signes, Sawyer X, Stevan
Little, Steve Hay, Todd Rinaldo, Tony Cook, Vladimir Timofeev, Yves Orton.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the perl bug database at
https://rt.perl.org/ .  There may also be information at http://www.perl.org/ ,
the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send it
to perl5-security-report@perl.org.  This points to a closed subscription
unarchived mailing list, which includes all the core committers, who will be
able to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported.  Please only use this address for
security issues in the Perl core, not for modules independently distributed on
\s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
