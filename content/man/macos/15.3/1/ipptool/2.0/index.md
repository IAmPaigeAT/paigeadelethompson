+++
keywords = ["ipptoolfile", "5", "iana", "ipp", "registry", "http", "www", "org", "assignments", "ipp-registrations", "pwg", "internet", "printing", "protocol", "workgroup", "rfc", "8011", "tools", "ietf", "html", "rfc8011", "copyright", "co", "2007-2019", "by", "apple", "inc"]
manpage_section = "1"
title = "ipptool(1)"
manpage_name = "ipptool"
date = "Sun Feb 16 04:48:18 2025"
author = "None Specified"
description = "sends IPP requests to the specified and tests and/or displays the results. Each named defines one or more requests, including the expected response status, attributes, and values. Output is either a plain text, formatted text, CSV, or XML report o..."
operating_system = "macos"
operating_system_version = "15.3"
manpage_format = "troff"
detected_package_version = "2.0"
+++

ipptool 1 "CUPS" "26 April 2019" "Apple Inc."

## NAME

ipptool - perform internet printing protocol requests

## SYNOPSIS

ipptool
[
--help
] [
--ippserver
filename
] [
--stop-after-include-error
] [
--version
] [
-4
] [
-6
] [
-C
] [
-E
] [
-I
] [
-L
] [
-P
filename.plist
] [
-S
] [
-T
seconds
] [
-V
version
] [
-X
] [
-c
] [
-d
name=value
] [
-f
filename
] [
-h
] [
-i
seconds
] [
-n
repeat-count
] [
-q
] [
-t
] [
-v ]
printer-uri
testfile
[ ...
testfile
]

## DESCRIPTION

ipptool
sends IPP requests to the specified
printer-uri
and tests and/or displays the results.
Each named
testfile
defines one or more requests, including the expected response status, attributes, and values.
Output is either a plain text, formatted text, CSV, or XML report on the standard output, with a non-zero exit status indicating that one or more tests have failed.
The
testfile
format is described in
ipptoolfile (5).

## OPTIONS

The following options are recognized by
ipptool:

--help
Shows program help.

**--ippserver **filename
Specifies that the test results should be written to the named
ippserver
attributes file.

--stop-after-include-error
Tells
ipptool
to stop if an error occurs in an included file. Normally
ipptool
will continue with subsequent tests after the INCLUDE directive.

--version
Shows the version of
ipptool
being used.

-4
Specifies that
ipptool
must connect to the printer or server using IPv4.

-6
Specifies that
ipptool
must connect to the printer or server using IPv6.

-C
Specifies that requests should be sent using the HTTP/1.1 "Transfer-Encoding: chunked" header, which is required for conformance by all versions of IPP.
The default is to use "Transfer-Encoding: chunked" for requests with attached files and "Content-Length:" for requests without attached files.

-E
Forces TLS encryption when connecting to the server using the HTTP "Upgrade" header.

-I
Specifies that
ipptool
will continue past errors.

-L
Specifies that requests should be sent using the HTTP/1.0 "Content-Length:" header, which is required for conformance by all versions of IPP.
The default is to use "Transfer-Encoding: chunked" for requests with attached files and "Content-Length:" for requests without attached files.

-P \ filename.plist
Specifies that the test results should be written to the named XML (Apple plist) file in addition to the regular test report (**-t**).
This option is incompatible with the **-i** (interval) and **-n** (repeat-count) options.

-S
Forces (dedicated) TLS encryption when connecting to the server.

-T \ seconds
Specifies a timeout for IPP requests in seconds.

-V \ version
Specifies the default IPP version to use: 1.0, 1.1, 2.0, 2.1, or 2.2. If not specified, version 1.1 is used.

-X
Specifies that XML (Apple plist) output is desired instead of the plain text report.
This option is incompatible with the **-i** (interval) and **-n** (repeat-count) options.

-c
Specifies that CSV (comma-separated values) output is desired instead of the plain text output.

-d \ name=value
Defines the named variable.

-f \ filename
Defines the default request filename for tests.

-h
Validate HTTP response headers.

-i \ seconds
Specifies that the (last)
testfile
should be repeated at the specified interval.
This option is incompatible with the **-X** (XML plist output) option.

-l
Specifies that plain text output is desired.

-n \ repeat-count
Specifies that the (last)
testfile
should be repeated the specified number of times.
This option is incompatible with the *-X* (XML plist output) option.

-q
Be quiet and produce no output.

-t
Specifies that CUPS test report output is desired instead of the plain text output.

-v
Specifies that all request and response attributes should be output in CUPS test mode (**-t**).
This is the default for XML output.

## EXIT STATUS

The
ipptool
program returns 0 if all tests were successful and 1 otherwise.

## FILES

The following standard files are available:

```
.I color.jpg
.I create\-printer\-subscription.test
.I document\-a4.pdf
.I document\-a4.ps
.I document\-letter.pdf
.I document\-letter.ps
.I get\-completed\-jobs.test
.I get\-jobs.test
.I get\-notifications.test
.I get\-printer\-attributes.test
.I get\-subscriptions.test
.I gray.jpg
.I ipp\-1.1.test
.I ipp\-2.0.test
.I ipp\-2.1.test
.I ipp\-2.2.test
.I ipp\-everywhere.test
.I onepage\-a4.pdf
.I onepage\-a4.ps
.I onepage\-letter.pdf
.I onepage\-letter.ps
.I print\-job.test
.I print\-job\-deflate.test
.I print\-job\-gzip.test
.I testfile.jpg
.I testfile.pcl
.I testfile.pdf
.I testfile.ps
.I testfile.txt
.I validate\-job.test
```


## CONFORMING TO

The
ipptool
program is unique to CUPS and conforms to the Internet Printing Protocol up to version 2.2.

## EXAMPLES

Get a list of completed jobs for "myprinter":

```

    ipptool ipp://localhost/printers/myprinter get\-completed\-jobs.test
```


Send email notifications to "user@example.com" when "myprinter" changes:

```

    ipptool \-d recipient=mailto:user@example.com \\
        ipp://localhost/printers/myprinter create\-printer\-subscription.test
```


## SEE ALSO

ipptoolfile (5),
IANA IPP Registry (http://www.iana.org/assignments/ipp-registrations),
PWG Internet Printing Protocol Workgroup (http://www.pwg.org/ipp)
RFC 8011 (http://tools.ietf.org/html/rfc8011),

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
