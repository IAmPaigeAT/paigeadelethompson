+++
manpage_format = "troff"
operating_system_version = "15.3"
operating_system = "macos"
detected_package_version = "2.0"
keywords = ["ppdc", "1", "ppdcfile", "5", "ppdi", "ppdmerge", "ppdpo", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
description = "ppdhtml reads a driver information file and produces a HTML summary page that lists all of the drivers in a file and the supported options. This program is deprecated and will be removed in a future release of CUPS. ppdhtml supports the following op..."
author = "None Specified"
title = "ppdhtml(1)"
manpage_name = "ppdhtml"
manpage_section = "1"
date = "Sun Feb 16 04:48:25 2025"
+++

ppdhtml 1 "CUPS" "26 April 2019" "Apple Inc."

## NAME

ppdhtml - cups html summary generator (deprecated)

## SYNOPSIS

ppdhtml
[
**-D **name[**=**value]
] [
-I
include-directory
]
source-file

## DESCRIPTION

**ppdhtml** reads a driver information file and produces a HTML summary page that lists all of the drivers in a file and the supported options.
**This program is deprecated and will be removed in a future release of CUPS.**

## OPTIONS

**ppdhtml** supports the following options:

**-D **name[**=**value]
Sets the named variable for use in the source file.
It is equivalent to using the *#define* directive in the source file.

**-I **include-directory
Specifies an alternate include directory.
Multiple *-I* options can be supplied to add additional directories.

## NOTES

PPD files are deprecated and will no longer be supported in a future feature release of CUPS.
Printers that do not support IPP can be supported using applications such as
ippeveprinter (1).

## SEE ALSO

ppdc (1),
ppdcfile (5),
ppdi (1),
ppdmerge (1),
ppdpo (1),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
