+++
description = "None Specified"
title = "perlmodlib(1)"
manpage_format = "troff"
date = "2024-12-14"
operating_system = "macos"
author = "None Specified"
operating_system_version = "15.3"
manpage_section = "1"
detected_package_version = "5.34.1"
manpage_name = "perlmodlib"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLMODLIB 1"
PERLMODLIB 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlmodlib - constructing new Perl modules and finding existing ones

## THE PERL MODULE LIBRARY

Header "THE PERL MODULE LIBRARY"
Many modules are included in the Perl distribution.  These are described
below, and all end in *.pm*.  You may discover compiled library
files (usually ending in *.so*) or small pieces of modules to be
autoloaded (ending in *.al*); these were automatically generated
by the installation process.  You may also discover files in the
library directory that end in either *.pl* or *.ph*.  These are
old libraries supplied so that old programs that use them still
run.  The *.pl* files will all eventually be converted into standard
modules, and the *.ph* files made by **h2ph** will probably end up
as extension modules made by **h2xs**.  (Some *.ph* values may
already be available through the \s-1POSIX,\s0 Errno, or Fcntl modules.)
The **pl2pm** file in the distribution may help in your conversion,
but it's just a mechanical process and therefore far from bulletproof.

### Pragmatic Modules

Subsection "Pragmatic Modules"
They work somewhat like compiler directives (pragmata) in that they
tend to affect the compilation of your program, and thus will usually
work well only when used within a \f(CW\*(C`use\*(C', or \f(CW\*(C`no\*(C'.  Most of these
are lexically scoped, so an inner \s-1BLOCK\s0 may countermand them
by saying:

.Vb 3
    no integer;
    no strict \*(Aqrefs\*(Aq;
    no warnings;
.Ve

which lasts until the end of that \s-1BLOCK.\s0

Some pragmas are lexically scoped\*(--typically those that affect the
\f(CW$^H hints variable.  Others affect the current package instead,
like \f(CW\*(C`use vars\*(C' and \f(CW\*(C`use subs\*(C', which allow you to predeclare a
variables or subroutines within a particular *file* rather than
just a block.  Such declarations are effective for the entire file
for which they were declared.  You cannot rescind them with \f(CW\*(C`no
vars\*(C' or \f(CW\*(C`no subs\*(C'.

The following pragmas are defined (and have their own documentation).

- attributes
Item "attributes"
Get/set subroutine or variable attributes

- autodie
Item "autodie"
Replace functions with ones that succeed or die with lexical scope

- autodie::exception
Item "autodie::exception"
Exceptions from autodying functions.

- autodie::exception::system
Item "autodie::exception::system"
Exceptions from autodying **system()**.

- autodie::hints
Item "autodie::hints"
Provide hints about user subroutines to autodie

- autodie::skip
Item "autodie::skip"
Skip a package when throwing autodie exceptions

- autouse
Item "autouse"
Postpone load of modules until a function is used

- base
Item "base"
Establish an \s-1ISA\s0 relationship with base classes at compile time

- bigint
Item "bigint"
Transparent BigInteger support for Perl

- bignum
Item "bignum"
Transparent BigNumber support for Perl

- bigrat
Item "bigrat"
Transparent BigNumber/BigRational support for Perl

- blib
Item "blib"
Use MakeMaker's uninstalled version of a package

- bytes
Item "bytes"
Expose the individual bytes of characters

- charnames
Item "charnames"
Access to Unicode character names and named character sequences; also define character names

- constant
Item "constant"
Declare constants

- deprecate
Item "deprecate"
Perl pragma for deprecating the inclusion of a module in core

- diagnostics
Item "diagnostics"
Produce verbose warning diagnostics

- encoding
Item "encoding"
Allows you to write your script in non-ASCII and non-UTF-8

- encoding::warnings
Item "encoding::warnings"
Warn on implicit encoding conversions

- experimental
Item "experimental"
Experimental features made easy

- feature
Item "feature"
Enable new features

- fields
Item "fields"
Compile-time class fields

- filetest
Item "filetest"
Control the filetest permission operators

- if
Item "if"
\f(CW\*(C`use\*(C' a Perl module if a condition holds

- integer
Item "integer"
Use integer arithmetic instead of floating point

- less
Item "less"
Request less of something

- lib
Item "lib"
Manipulate \f(CW@INC at compile time

- locale
Item "locale"
Use or avoid \s-1POSIX\s0 locales for built-in operations

- mro
Item "mro"
Method Resolution Order

- ok
Item "ok"
Alternative to Test::More::use_ok

- open
Item "open"
Set default PerlIO layers for input and output

- ops
Item "ops"
Restrict unsafe operations when compiling

- overload
Item "overload"
Package for overloading Perl operations

- overloading
Item "overloading"
Lexically control overloading

- parent
Item "parent"
Establish an \s-1ISA\s0 relationship with base classes at compile time

- re
Item "re"
Alter regular expression behaviour

- sigtrap
Item "sigtrap"
Enable simple signal handling

- sort
Item "sort"
Control **sort()** behaviour

- strict
Item "strict"
Restrict unsafe constructs

- subs
Item "subs"
Predeclare subroutine names

- threads
Item "threads"
Perl interpreter-based threads

- threads::shared
Item "threads::shared"
Perl extension for sharing data structures between threads

- utf8
Item "utf8"
Enable/disable \s-1UTF-8\s0 (or UTF-EBCDIC) in source code

- vars
Item "vars"
Predeclare global variable names

- version
Item "version"
Perl extension for Version Objects

- vmsish
Item "vmsish"
Control VMS-specific language features

- warnings
Item "warnings"
Control optional warnings

- warnings::register
Item "warnings::register"
Warnings import function

### Standard Modules

Subsection "Standard Modules"
Standard, bundled modules are all expected to behave in a well-defined
manner with respect to namespace pollution because they use the
Exporter module.  See their own documentation for details.

It's possible that not all modules listed below are installed on your
system. For example, the GDBM_File module will not be installed if you
don't have the gdbm library.

- Amiga::ARexx
Item "Amiga::ARexx"
Perl extension for ARexx support

- Amiga::Exec
Item "Amiga::Exec"
Perl extension for low level amiga support

- AnyDBM_File
Item "AnyDBM_File"
Provide framework for multiple DBMs

- App::Cpan
Item "App::Cpan"
Easily interact with \s-1CPAN\s0 from the command line

- App::Prove
Item "App::Prove"
Implements the \f(CW\*(C`prove\*(C' command.

- App::Prove::State
Item "App::Prove::State"
State storage for the \f(CW\*(C`prove\*(C' command.

- App::Prove::State::Result
Item "App::Prove::State::Result"
Individual test suite results.

- App::Prove::State::Result::Test
Item "App::Prove::State::Result::Test"
Individual test results.

- Archive::Tar
Item "Archive::Tar"
Module for manipulations of tar archives

- Archive::Tar::File
Item "Archive::Tar::File"
A subclass for in-memory extracted file from Archive::Tar

- Attribute::Handlers
Item "Attribute::Handlers"
Simpler definition of attribute handlers

- AutoLoader
Item "AutoLoader"
Load subroutines only on demand

- AutoSplit
Item "AutoSplit"
Split a package for autoloading

- B
Item "B"
The Perl Compiler Backend

- B::Concise
Item "B::Concise"
Walk Perl syntax tree, printing concise info about ops

- B::Deparse
Item "B::Deparse"
Perl compiler backend to produce perl code

- B::Op_private
Item "B::Op_private"
\s-1OP\s0 op_private flag definitions

- B::Showlex
Item "B::Showlex"
Show lexical variables used in functions or files

- B::Terse
Item "B::Terse"
Walk Perl syntax tree, printing terse info about ops

- B::Xref
Item "B::Xref"
Generates cross reference reports for Perl programs

- Benchmark
Item "Benchmark"
Benchmark running times of Perl code
.ie n .IP """IO::Socket::IP""" 12
.el .IP "\f(CWIO::Socket::IP" 12
Item "IO::Socket::IP"
Family-neutral \s-1IP\s0 socket supporting both IPv4 and IPv6
.ie n .IP """Socket""" 12
.el .IP "\f(CWSocket" 12
Item "Socket"
Networking constants and support functions

- \s-1CORE\s0
Item "CORE"
Namespace for Perl's core routines

- \s-1CPAN\s0
Item "CPAN"
Query, download and build perl modules from \s-1CPAN\s0 sites

- \s-1CPAN::API::HOWTO\s0
Item "CPAN::API::HOWTO"
A recipe book for programming with \s-1CPAN\s0.pm

- CPAN::Debug
Item "CPAN::Debug"
Internal debugging for \s-1CPAN\s0.pm

- CPAN::Distroprefs
Item "CPAN::Distroprefs"
Read and match distroprefs

- CPAN::FirstTime
Item "CPAN::FirstTime"
Utility for CPAN::Config file Initialization

- CPAN::HandleConfig
Item "CPAN::HandleConfig"
Internal configuration handling for \s-1CPAN\s0.pm

- CPAN::Kwalify
Item "CPAN::Kwalify"
Interface between \s-1CPAN\s0.pm and Kwalify.pm

- CPAN::Meta
Item "CPAN::Meta"
The distribution metadata for a \s-1CPAN\s0 dist

- CPAN::Meta::Converter
Item "CPAN::Meta::Converter"
Convert \s-1CPAN\s0 distribution metadata structures

- CPAN::Meta::Feature
Item "CPAN::Meta::Feature"
An optional feature provided by a \s-1CPAN\s0 distribution

- CPAN::Meta::History
Item "CPAN::Meta::History"
History of \s-1CPAN\s0 Meta Spec changes

- CPAN::Meta::History::Meta_1_0
Item "CPAN::Meta::History::Meta_1_0"
Version 1.0 metadata specification for \s-1META\s0.yml

- CPAN::Meta::History::Meta_1_1
Item "CPAN::Meta::History::Meta_1_1"
Version 1.1 metadata specification for \s-1META\s0.yml

- CPAN::Meta::History::Meta_1_2
Item "CPAN::Meta::History::Meta_1_2"
Version 1.2 metadata specification for \s-1META\s0.yml

- CPAN::Meta::History::Meta_1_3
Item "CPAN::Meta::History::Meta_1_3"
Version 1.3 metadata specification for \s-1META\s0.yml

- CPAN::Meta::History::Meta_1_4
Item "CPAN::Meta::History::Meta_1_4"
Version 1.4 metadata specification for \s-1META\s0.yml

- CPAN::Meta::Merge
Item "CPAN::Meta::Merge"
Merging \s-1CPAN\s0 Meta fragments

- CPAN::Meta::Prereqs
Item "CPAN::Meta::Prereqs"
A set of distribution prerequisites by phase and type

- CPAN::Meta::Requirements
Item "CPAN::Meta::Requirements"
A set of version requirements for a \s-1CPAN\s0 dist

- CPAN::Meta::Spec
Item "CPAN::Meta::Spec"
Specification for \s-1CPAN\s0 distribution metadata

- CPAN::Meta::Validator
Item "CPAN::Meta::Validator"
Validate \s-1CPAN\s0 distribution metadata structures

- CPAN::Meta::YAML
Item "CPAN::Meta::YAML"
Read and write a subset of \s-1YAML\s0 for \s-1CPAN\s0 Meta files

- CPAN::Nox
Item "CPAN::Nox"
Wrapper around \s-1CPAN\s0.pm without using any \s-1XS\s0 module

- CPAN::Plugin
Item "CPAN::Plugin"
Base class for \s-1CPAN\s0 shell extensions

- CPAN::Plugin::Specfile
Item "CPAN::Plugin::Specfile"
Proof of concept implementation of a trivial CPAN::Plugin

- CPAN::Queue
Item "CPAN::Queue"
Internal queue support for \s-1CPAN\s0.pm

- CPAN::Tarzip
Item "CPAN::Tarzip"
Internal handling of tar archives for \s-1CPAN\s0.pm

- CPAN::Version
Item "CPAN::Version"
Utility functions to compare \s-1CPAN\s0 versions

- Carp
Item "Carp"
Alternative warn and die for modules

- Class::Struct
Item "Class::Struct"
Declare struct-like datatypes as Perl classes

- Compress::Raw::Bzip2
Item "Compress::Raw::Bzip2"
Low-Level Interface to bzip2 compression library

- Compress::Raw::Zlib
Item "Compress::Raw::Zlib"
Low-Level Interface to zlib compression library

- Compress::Zlib
Item "Compress::Zlib"
Interface to zlib compression library

- Config
Item "Config"
Access Perl configuration information

- Config::Extensions
Item "Config::Extensions"
Hash lookup of which core extensions were built.

- Config::Perl::V
Item "Config::Perl::V"
Structured data retrieval of perl -V output

- Cwd
Item "Cwd"
Get pathname of current working directory

- \s-1DB\s0
Item "DB"
Programmatic interface to the Perl debugging \s-1API\s0

- DBM_Filter
Item "DBM_Filter"
Filter \s-1DBM\s0 keys/values

- DBM_Filter::compress
Item "DBM_Filter::compress"
Filter for DBM_Filter

- DBM_Filter::encode
Item "DBM_Filter::encode"
Filter for DBM_Filter

- DBM_Filter::int32
Item "DBM_Filter::int32"
Filter for DBM_Filter

- DBM_Filter::null
Item "DBM_Filter::null"
Filter for DBM_Filter

- DBM_Filter::utf8
Item "DBM_Filter::utf8"
Filter for DBM_Filter

- DB_File
Item "DB_File"
Perl5 access to Berkeley \s-1DB\s0 version 1.x

- Data::Dumper
Item "Data::Dumper"
Stringified perl data structures, suitable for both printing and \f(CW\*(C`eval\*(C'

- Devel::PPPort
Item "Devel::PPPort"
Perl/Pollution/Portability

- Devel::Peek
Item "Devel::Peek"
A data debugging tool for the \s-1XS\s0 programmer

- Devel::SelfStubber
Item "Devel::SelfStubber"
Generate stubs for a SelfLoading module

- Digest
Item "Digest"
Modules that calculate message digests

- Digest::MD5
Item "Digest::MD5"
Perl interface to the \s-1MD5\s0 Algorithm

- Digest::SHA
Item "Digest::SHA"
Perl extension for \s-1SHA-1/224/256/384/512\s0

- Digest::base
Item "Digest::base"
Digest base class

- Digest::file
Item "Digest::file"
Calculate digests of files

- DirHandle
Item "DirHandle"
(obsolete) supply object methods for directory handles

- Dumpvalue
Item "Dumpvalue"
Provides screen dump of Perl data.

- DynaLoader
Item "DynaLoader"
Dynamically load C libraries into Perl code

- Encode
Item "Encode"
Character encodings in Perl

- Encode::Alias
Item "Encode::Alias"
Alias definitions to encodings

- Encode::Byte
Item "Encode::Byte"
Single Byte Encodings

- Encode::CJKConstants
Item "Encode::CJKConstants"
Internally used by Encode::??::ISO_2022_*

- Encode::CN
Item "Encode::CN"
China-based Chinese Encodings

- Encode::CN::HZ
Item "Encode::CN::HZ"
Internally used by Encode::CN

- Encode::Config
Item "Encode::Config"
Internally used by Encode

- Encode::EBCDIC
Item "Encode::EBCDIC"
\s-1EBCDIC\s0 Encodings

- Encode::Encoder
Item "Encode::Encoder"
Object Oriented Encoder

- Encode::Encoding
Item "Encode::Encoding"
Encode Implementation Base Class

- Encode::GSM0338
Item "Encode::GSM0338"
\s-1ETSI GSM 03.38\s0 Encoding

- Encode::Guess
Item "Encode::Guess"
Guesses encoding from data

- Encode::JP
Item "Encode::JP"
Japanese Encodings

- Encode::JP::H2Z
Item "Encode::JP::H2Z"
Internally used by Encode::JP::2022_JP*

- Encode::JP::JIS7
Item "Encode::JP::JIS7"
Internally used by Encode::JP

- Encode::KR
Item "Encode::KR"
Korean Encodings

- Encode::KR::2022_KR
Item "Encode::KR::2022_KR"
Internally used by Encode::KR

- Encode::MIME::Header
Item "Encode::MIME::Header"
\s-1MIME\s0 encoding for an unstructured email header

- Encode::MIME::Name
Item "Encode::MIME::Name"
Internally used by Encode

- Encode::PerlIO
Item "Encode::PerlIO"
A detailed document on Encode and PerlIO

- Encode::Supported
Item "Encode::Supported"
Encodings supported by Encode

- Encode::Symbol
Item "Encode::Symbol"
Symbol Encodings

- Encode::TW
Item "Encode::TW"
Taiwan-based Chinese Encodings

- Encode::Unicode
Item "Encode::Unicode"
Various Unicode Transformation Formats

- Encode::Unicode::UTF7
Item "Encode::Unicode::UTF7"
\s-1UTF-7\s0 encoding

- English
Item "English"
Use nice English (or awk) names for ugly punctuation variables

- Env
Item "Env"
Perl module that imports environment variables as scalars or arrays

- Errno
Item "Errno"
System errno constants

- Exporter
Item "Exporter"
Implements default import method for modules

- Exporter::Heavy
Item "Exporter::Heavy"
Exporter guts

- ExtUtils::CBuilder
Item "ExtUtils::CBuilder"
Compile and link C code for Perl modules

- ExtUtils::CBuilder::Platform::Windows
Item "ExtUtils::CBuilder::Platform::Windows"
Builder class for Windows platforms

- ExtUtils::Command
Item "ExtUtils::Command"
Utilities to replace common \s-1UNIX\s0 commands in Makefiles etc.

- ExtUtils::Command::MM
Item "ExtUtils::Command::MM"
Commands for the \s-1MM\s0's to use in Makefiles

- ExtUtils::Constant
Item "ExtUtils::Constant"
Generate \s-1XS\s0 code to import C header constants

- ExtUtils::Constant::Base
Item "ExtUtils::Constant::Base"
Base class for ExtUtils::Constant objects

- ExtUtils::Constant::Utils
Item "ExtUtils::Constant::Utils"
Helper functions for ExtUtils::Constant

- ExtUtils::Constant::XS
Item "ExtUtils::Constant::XS"
Generate C code for \s-1XS\s0 modules' constants.

- ExtUtils::Embed
Item "ExtUtils::Embed"
Utilities for embedding Perl in C/\*(C+ applications

- ExtUtils::Install
Item "ExtUtils::Install"
Install files from here to there

- ExtUtils::Installed
Item "ExtUtils::Installed"
Inventory management of installed modules

- ExtUtils::Liblist
Item "ExtUtils::Liblist"
Determine libraries to use and how to use them

- ExtUtils::MM
Item "ExtUtils::MM"
\s-1OS\s0 adjusted ExtUtils::MakeMaker subclass

- ExtUtils::MM_AIX
Item "ExtUtils::MM_AIX"
\s-1AIX\s0 specific subclass of ExtUtils::MM_Unix

- ExtUtils::MM_Any
Item "ExtUtils::MM_Any"
Platform-agnostic \s-1MM\s0 methods

- ExtUtils::MM_BeOS
Item "ExtUtils::MM_BeOS"
Methods to override UN*X behaviour in ExtUtils::MakeMaker

- ExtUtils::MM_Cygwin
Item "ExtUtils::MM_Cygwin"
Methods to override UN*X behaviour in ExtUtils::MakeMaker

- ExtUtils::MM_DOS
Item "ExtUtils::MM_DOS"
\s-1DOS\s0 specific subclass of ExtUtils::MM_Unix

- ExtUtils::MM_Darwin
Item "ExtUtils::MM_Darwin"
Special behaviors for \s-1OS X\s0

- ExtUtils::MM_MacOS
Item "ExtUtils::MM_MacOS"
Once produced Makefiles for MacOS Classic

- ExtUtils::MM_NW5
Item "ExtUtils::MM_NW5"
Methods to override UN*X behaviour in ExtUtils::MakeMaker

- ExtUtils::MM_OS2
Item "ExtUtils::MM_OS2"
Methods to override UN*X behaviour in ExtUtils::MakeMaker

- ExtUtils::MM_OS390
Item "ExtUtils::MM_OS390"
\s-1OS390\s0 specific subclass of ExtUtils::MM_Unix

- ExtUtils::MM_QNX
Item "ExtUtils::MM_QNX"
\s-1QNX\s0 specific subclass of ExtUtils::MM_Unix

- ExtUtils::MM_UWIN
Item "ExtUtils::MM_UWIN"
U/WIN specific subclass of ExtUtils::MM_Unix

- ExtUtils::MM_Unix
Item "ExtUtils::MM_Unix"
Methods used by ExtUtils::MakeMaker

- ExtUtils::MM_VMS
Item "ExtUtils::MM_VMS"
Methods to override UN*X behaviour in ExtUtils::MakeMaker

- ExtUtils::MM_VOS
Item "ExtUtils::MM_VOS"
\s-1VOS\s0 specific subclass of ExtUtils::MM_Unix

- ExtUtils::MM_Win32
Item "ExtUtils::MM_Win32"
Methods to override UN*X behaviour in ExtUtils::MakeMaker

- ExtUtils::MM_Win95
Item "ExtUtils::MM_Win95"
Method to customize MakeMaker for Win9X

- ExtUtils::MY
Item "ExtUtils::MY"
ExtUtils::MakeMaker subclass for customization

- ExtUtils::MakeMaker
Item "ExtUtils::MakeMaker"
Create a module Makefile

- ExtUtils::MakeMaker::Config
Item "ExtUtils::MakeMaker::Config"
Wrapper around Config.pm

- ExtUtils::MakeMaker::FAQ
Item "ExtUtils::MakeMaker::FAQ"
Frequently Asked Questions About MakeMaker

- ExtUtils::MakeMaker::Locale
Item "ExtUtils::MakeMaker::Locale"
Bundled Encode::Locale

- ExtUtils::MakeMaker::Tutorial
Item "ExtUtils::MakeMaker::Tutorial"
Writing a module with MakeMaker

- ExtUtils::Manifest
Item "ExtUtils::Manifest"
Utilities to write and check a \s-1MANIFEST\s0 file

- ExtUtils::Miniperl
Item "ExtUtils::Miniperl"
Write the C code for miniperlmain.c and perlmain.c

- ExtUtils::Mkbootstrap
Item "ExtUtils::Mkbootstrap"
Make a bootstrap file for use by DynaLoader

- ExtUtils::Mksymlists
Item "ExtUtils::Mksymlists"
Write linker options files for dynamic extension

- ExtUtils::PL2Bat
Item "ExtUtils::PL2Bat"
Batch file creation to run perl scripts on Windows

- ExtUtils::Packlist
Item "ExtUtils::Packlist"
Manage .packlist files

- ExtUtils::ParseXS
Item "ExtUtils::ParseXS"
Converts Perl \s-1XS\s0 code into C code

- ExtUtils::ParseXS::Constants
Item "ExtUtils::ParseXS::Constants"
Initialization values for some globals

- ExtUtils::ParseXS::Eval
Item "ExtUtils::ParseXS::Eval"
Clean package to evaluate code in

- ExtUtils::ParseXS::Utilities
Item "ExtUtils::ParseXS::Utilities"
Subroutines used with ExtUtils::ParseXS

- ExtUtils::Typemaps
Item "ExtUtils::Typemaps"
Read/Write/Modify Perl/XS typemap files

- ExtUtils::Typemaps::Cmd
Item "ExtUtils::Typemaps::Cmd"
Quick commands for handling typemaps

- ExtUtils::Typemaps::InputMap
Item "ExtUtils::Typemaps::InputMap"
Entry in the \s-1INPUT\s0 section of a typemap

- ExtUtils::Typemaps::OutputMap
Item "ExtUtils::Typemaps::OutputMap"
Entry in the \s-1OUTPUT\s0 section of a typemap

- ExtUtils::Typemaps::Type
Item "ExtUtils::Typemaps::Type"
Entry in the \s-1TYPEMAP\s0 section of a typemap

- ExtUtils::XSSymSet
Item "ExtUtils::XSSymSet"
Keep sets of symbol names palatable to the \s-1VMS\s0 linker

- ExtUtils::testlib
Item "ExtUtils::testlib"
Add blib/* directories to \f(CW@INC

- Fatal
Item "Fatal"
Replace functions with equivalents which succeed or die

- Fcntl
Item "Fcntl"
Load the C Fcntl.h defines

- File::Basename
Item "File::Basename"
Parse file paths into directory, filename and suffix.

- File::Compare
Item "File::Compare"
Compare files or filehandles

- File::Copy
Item "File::Copy"
Copy files or filehandles

- File::DosGlob
Item "File::DosGlob"
\s-1DOS\s0 like globbing and then some

- File::Fetch
Item "File::Fetch"
A generic file fetching mechanism

- File::Find
Item "File::Find"
Traverse a directory tree.

- File::Glob
Item "File::Glob"
Perl extension for \s-1BSD\s0 glob routine

- File::GlobMapper
Item "File::GlobMapper"
Extend File Glob to Allow Input and Output Files

- File::Path
Item "File::Path"
Create or remove directory trees

- File::Spec
Item "File::Spec"
Portably perform operations on file names

- File::Spec::AmigaOS
Item "File::Spec::AmigaOS"
File::Spec for AmigaOS

- File::Spec::Cygwin
Item "File::Spec::Cygwin"
Methods for Cygwin file specs

- File::Spec::Epoc
Item "File::Spec::Epoc"
Methods for Epoc file specs

- File::Spec::Functions
Item "File::Spec::Functions"
Portably perform operations on file names

- File::Spec::Mac
Item "File::Spec::Mac"
File::Spec for Mac \s-1OS\s0 (Classic)

- File::Spec::OS2
Item "File::Spec::OS2"
Methods for \s-1OS/2\s0 file specs

- File::Spec::Unix
Item "File::Spec::Unix"
File::Spec for Unix, base for other File::Spec modules

- File::Spec::VMS
Item "File::Spec::VMS"
Methods for \s-1VMS\s0 file specs

- File::Spec::Win32
Item "File::Spec::Win32"
Methods for Win32 file specs

- File::Temp
Item "File::Temp"
Return name and handle of a temporary file safely

- File::stat
Item "File::stat"
By-name interface to Perl's built-in **stat()** functions

- FileCache
Item "FileCache"
Keep more files open than the system permits

- FileHandle
Item "FileHandle"
Supply object methods for filehandles

- Filter::Simple
Item "Filter::Simple"
Simplified source filtering

- Filter::Util::Call
Item "Filter::Util::Call"
Perl Source Filter Utility Module

- FindBin
Item "FindBin"
Locate directory of original perl script

- GDBM_File
Item "GDBM_File"
Perl5 access to the gdbm library.

- Getopt::Long
Item "Getopt::Long"
Extended processing of command line options

- Getopt::Std
Item "Getopt::Std"
Process single-character switches with switch clustering

- HTTP::Tiny
Item "HTTP::Tiny"
A small, simple, correct \s-1HTTP/1.1\s0 client

- Hash::Util
Item "Hash::Util"
A selection of general-utility hash subroutines

- Hash::Util::FieldHash
Item "Hash::Util::FieldHash"
Support for Inside-Out Classes

- I18N::Collate
Item "I18N::Collate"
Compare 8-bit scalar data according to the current locale

- I18N::LangTags
Item "I18N::LangTags"
Functions for dealing with RFC3066-style language tags

- I18N::LangTags::Detect
Item "I18N::LangTags::Detect"
Detect the user's language preferences

- I18N::LangTags::List
Item "I18N::LangTags::List"
Tags and names for human languages

- I18N::Langinfo
Item "I18N::Langinfo"
Query locale information

- \s-1IO\s0
Item "IO"
Load various \s-1IO\s0 modules

- IO::Compress::Base
Item "IO::Compress::Base"
Base Class for IO::Compress modules

- IO::Compress::Bzip2
Item "IO::Compress::Bzip2"
Write bzip2 files/buffers

- IO::Compress::Deflate
Item "IO::Compress::Deflate"
Write \s-1RFC 1950\s0 files/buffers

- IO::Compress::FAQ
Item "IO::Compress::FAQ"
Frequently Asked Questions about IO::Compress

- IO::Compress::Gzip
Item "IO::Compress::Gzip"
Write \s-1RFC 1952\s0 files/buffers

- IO::Compress::RawDeflate
Item "IO::Compress::RawDeflate"
Write \s-1RFC 1951\s0 files/buffers

- IO::Compress::Zip
Item "IO::Compress::Zip"
Write zip files/buffers

- IO::Dir
Item "IO::Dir"
Supply object methods for directory handles

- IO::File
Item "IO::File"
Supply object methods for filehandles

- IO::Handle
Item "IO::Handle"
Supply object methods for I/O handles

- IO::Pipe
Item "IO::Pipe"
Supply object methods for pipes

- IO::Poll
Item "IO::Poll"
Object interface to system poll call

- IO::Seekable
Item "IO::Seekable"
Supply seek based methods for I/O objects

- IO::Select
Item "IO::Select"
\s-1OO\s0 interface to the select system call

- IO::Socket
Item "IO::Socket"
Object interface to socket communications

- IO::Socket::INET
Item "IO::Socket::INET"
Object interface for \s-1AF_INET\s0 domain sockets

- IO::Socket::UNIX
Item "IO::Socket::UNIX"
Object interface for \s-1AF_UNIX\s0 domain sockets

- IO::Uncompress::AnyInflate
Item "IO::Uncompress::AnyInflate"
Uncompress zlib-based (zip, gzip) file/buffer

- IO::Uncompress::AnyUncompress
Item "IO::Uncompress::AnyUncompress"
Uncompress gzip, zip, bzip2, zstd, xz, lzma, lzip, lzf or lzop file/buffer

- IO::Uncompress::Base
Item "IO::Uncompress::Base"
Base Class for IO::Uncompress modules

- IO::Uncompress::Bunzip2
Item "IO::Uncompress::Bunzip2"
Read bzip2 files/buffers

- IO::Uncompress::Gunzip
Item "IO::Uncompress::Gunzip"
Read \s-1RFC 1952\s0 files/buffers

- IO::Uncompress::Inflate
Item "IO::Uncompress::Inflate"
Read \s-1RFC 1950\s0 files/buffers

- IO::Uncompress::RawInflate
Item "IO::Uncompress::RawInflate"
Read \s-1RFC 1951\s0 files/buffers

- IO::Uncompress::Unzip
Item "IO::Uncompress::Unzip"
Read zip files/buffers

- IO::Zlib
Item "IO::Zlib"
\s-1IO::\s0 style interface to Compress::Zlib

- IPC::Cmd
Item "IPC::Cmd"
Finding and running system commands made easy

- IPC::Msg
Item "IPC::Msg"
SysV Msg \s-1IPC\s0 object class

- IPC::Open2
Item "IPC::Open2"
Open a process for both reading and writing using **open2()**

- IPC::Open3
Item "IPC::Open3"
Open a process for reading, writing, and error handling using **open3()**

- IPC::Semaphore
Item "IPC::Semaphore"
SysV Semaphore \s-1IPC\s0 object class

- IPC::SharedMem
Item "IPC::SharedMem"
SysV Shared Memory \s-1IPC\s0 object class

- IPC::SysV
Item "IPC::SysV"
System V \s-1IPC\s0 constants and system calls

- Internals
Item "Internals"
Reserved special namespace for internals related functions

- \s-1JSON::PP\s0
Item "JSON::PP"
\s-1JSON::XS\s0 compatible pure-Perl module.

- JSON::PP::Boolean
Item "JSON::PP::Boolean"
Dummy module providing JSON::PP::Boolean

- List::Util
Item "List::Util"
A selection of general-utility list subroutines

- List::Util::XS
Item "List::Util::XS"
Indicate if List::Util was compiled with a C compiler

- Locale::Maketext
Item "Locale::Maketext"
Framework for localization

- Locale::Maketext::Cookbook
Item "Locale::Maketext::Cookbook"
Recipes for using Locale::Maketext

- Locale::Maketext::Guts
Item "Locale::Maketext::Guts"
Deprecated module to load Locale::Maketext utf8 code

- Locale::Maketext::GutsLoader
Item "Locale::Maketext::GutsLoader"
Deprecated module to load Locale::Maketext utf8 code

- Locale::Maketext::Simple
Item "Locale::Maketext::Simple"
Simple interface to Locale::Maketext::Lexicon

- Locale::Maketext::TPJ13
Item "Locale::Maketext::TPJ13"
Article about software localization

- MIME::Base64
Item "MIME::Base64"
Encoding and decoding of base64 strings

- MIME::QuotedPrint
Item "MIME::QuotedPrint"
Encoding and decoding of quoted-printable strings

- Math::BigFloat
Item "Math::BigFloat"
Arbitrary size floating point math package

- Math::BigInt
Item "Math::BigInt"
Arbitrary size integer/float math package

- Math::BigInt::Calc
Item "Math::BigInt::Calc"
Pure Perl module to support Math::BigInt

- Math::BigInt::FastCalc
Item "Math::BigInt::FastCalc"
Math::BigInt::Calc with some \s-1XS\s0 for more speed

- Math::BigInt::Lib
Item "Math::BigInt::Lib"
Virtual parent class for Math::BigInt libraries

- Math::BigRat
Item "Math::BigRat"
Arbitrary big rational numbers

- Math::Complex
Item "Math::Complex"
Complex numbers and associated mathematical functions

- Math::Trig
Item "Math::Trig"
Trigonometric functions

- Memoize
Item "Memoize"
Make functions faster by trading space for time

- Memoize::AnyDBM_File
Item "Memoize::AnyDBM_File"
Glue to provide \s-1EXISTS\s0 for AnyDBM_File for Storable use

- Memoize::Expire
Item "Memoize::Expire"
Plug-in module for automatic expiration of memoized values

- Memoize::ExpireFile
Item "Memoize::ExpireFile"
Test for Memoize expiration semantics

- Memoize::ExpireTest
Item "Memoize::ExpireTest"
Test for Memoize expiration semantics

- Memoize::NDBM_File
Item "Memoize::NDBM_File"
Glue to provide \s-1EXISTS\s0 for NDBM_File for Storable use

- Memoize::SDBM_File
Item "Memoize::SDBM_File"
Glue to provide \s-1EXISTS\s0 for SDBM_File for Storable use

- Memoize::Storable
Item "Memoize::Storable"
Store Memoized data in Storable database

- Module::CoreList
Item "Module::CoreList"
What modules shipped with versions of perl

- Module::CoreList::Utils
Item "Module::CoreList::Utils"
What utilities shipped with versions of perl

- Module::Load
Item "Module::Load"
Runtime require of both modules and files

- Module::Load::Conditional
Item "Module::Load::Conditional"
Looking up module information / loading at runtime

- Module::Loaded
Item "Module::Loaded"
Mark modules as loaded or unloaded

- Module::Metadata
Item "Module::Metadata"
Gather package and \s-1POD\s0 information from perl module files

- NDBM_File
Item "NDBM_File"
Tied access to ndbm files

- \s-1NEXT\s0
Item "NEXT"
Provide a pseudo-class \s-1NEXT\s0 (et al) that allows method redispatch

- Net::Cmd
Item "Net::Cmd"
Network Command class (as used by \s-1FTP, SMTP\s0 etc)

- Net::Config
Item "Net::Config"
Local configuration data for libnet

- Net::Domain
Item "Net::Domain"
Attempt to evaluate the current host's internet name and domain

- Net::FTP
Item "Net::FTP"
\s-1FTP\s0 Client class

- Net::FTP::dataconn
Item "Net::FTP::dataconn"
\s-1FTP\s0 Client data connection class

- Net::NNTP
Item "Net::NNTP"
\s-1NNTP\s0 Client class

- Net::Netrc
Item "Net::Netrc"
\s-1OO\s0 interface to users netrc file

- Net::POP3
Item "Net::POP3"
Post Office Protocol 3 Client class (\s-1RFC1939\s0)

- Net::Ping
Item "Net::Ping"
Check a remote host for reachability

- Net::SMTP
Item "Net::SMTP"
Simple Mail Transfer Protocol Client

- Net::Time
Item "Net::Time"
Time and daytime network client interface

- Net::hostent
Item "Net::hostent"
By-name interface to Perl's built-in gethost*() functions

- Net::libnetFAQ
Item "Net::libnetFAQ"
Libnet Frequently Asked Questions

- Net::netent
Item "Net::netent"
By-name interface to Perl's built-in getnet*() functions

- Net::protoent
Item "Net::protoent"
By-name interface to Perl's built-in getproto*() functions

- Net::servent
Item "Net::servent"
By-name interface to Perl's built-in getserv*() functions

- O
Item "O"
Generic interface to Perl Compiler backends

- ODBM_File
Item "ODBM_File"
Tied access to odbm files

- Opcode
Item "Opcode"
Disable named opcodes when compiling perl code

- \s-1POSIX\s0
Item "POSIX"
Perl interface to \s-1IEEE\s0 Std 1003.1

- Params::Check
Item "Params::Check"
A generic input parsing/checking mechanism.

- Parse::CPAN::Meta
Item "Parse::CPAN::Meta"
Parse \s-1META\s0.yml and \s-1META\s0.json \s-1CPAN\s0 metadata files

- Perl::OSType
Item "Perl::OSType"
Map Perl operating system names to generic types

- PerlIO
Item "PerlIO"
On demand loader for PerlIO layers and root of PerlIO::* name space

- PerlIO::encoding
Item "PerlIO::encoding"
Encoding layer

- PerlIO::mmap
Item "PerlIO::mmap"
Memory mapped \s-1IO\s0

- PerlIO::scalar
Item "PerlIO::scalar"
In-memory \s-1IO,\s0 scalar \s-1IO\s0

- PerlIO::via
Item "PerlIO::via"
Helper class for PerlIO layers implemented in perl

- PerlIO::via::QuotedPrint
Item "PerlIO::via::QuotedPrint"
PerlIO layer for quoted-printable strings

- Pod::Checker
Item "Pod::Checker"
Check pod documents for syntax errors

- Pod::Escapes
Item "Pod::Escapes"
For resolving Pod E<...> sequences

- Pod::Functions
Item "Pod::Functions"
Group Perl's functions a la perlfunc.pod

- Pod::Html
Item "Pod::Html"
Module to convert pod files to \s-1HTML\s0

- Pod::Man
Item "Pod::Man"
Convert \s-1POD\s0 data to formatted *roff input

- Pod::ParseLink
Item "Pod::ParseLink"
Parse an L<> formatting code in \s-1POD\s0 text

- Pod::Perldoc
Item "Pod::Perldoc"
Look up Perl documentation in Pod format.

- Pod::Perldoc::BaseTo
Item "Pod::Perldoc::BaseTo"
Base for Pod::Perldoc formatters

- Pod::Perldoc::GetOptsOO
Item "Pod::Perldoc::GetOptsOO"
Customized option parser for Pod::Perldoc

- Pod::Perldoc::ToANSI
Item "Pod::Perldoc::ToANSI"
Render Pod with \s-1ANSI\s0 color escapes

- Pod::Perldoc::ToChecker
Item "Pod::Perldoc::ToChecker"
Let Perldoc check Pod for errors

- Pod::Perldoc::ToMan
Item "Pod::Perldoc::ToMan"
Let Perldoc render Pod as man pages

- Pod::Perldoc::ToNroff
Item "Pod::Perldoc::ToNroff"
Let Perldoc convert Pod to nroff

- Pod::Perldoc::ToPod
Item "Pod::Perldoc::ToPod"
Let Perldoc render Pod as ... Pod!

- Pod::Perldoc::ToRtf
Item "Pod::Perldoc::ToRtf"
Let Perldoc render Pod as \s-1RTF\s0

- Pod::Perldoc::ToTerm
Item "Pod::Perldoc::ToTerm"
Render Pod with terminal escapes

- Pod::Perldoc::ToText
Item "Pod::Perldoc::ToText"
Let Perldoc render Pod as plaintext

- Pod::Perldoc::ToTk
Item "Pod::Perldoc::ToTk"
Let Perldoc use Tk::Pod to render Pod

- Pod::Perldoc::ToXml
Item "Pod::Perldoc::ToXml"
Let Perldoc render Pod as \s-1XML\s0

- Pod::Simple
Item "Pod::Simple"
Framework for parsing Pod

- Pod::Simple::Checker
Item "Pod::Simple::Checker"
Check the Pod syntax of a document

- Pod::Simple::Debug
Item "Pod::Simple::Debug"
Put Pod::Simple into trace/debug mode

- Pod::Simple::DumpAsText
Item "Pod::Simple::DumpAsText"
Dump Pod-parsing events as text

- Pod::Simple::DumpAsXML
Item "Pod::Simple::DumpAsXML"
Turn Pod into \s-1XML\s0

- Pod::Simple::HTML
Item "Pod::Simple::HTML"
Convert Pod to \s-1HTML\s0

- Pod::Simple::HTMLBatch
Item "Pod::Simple::HTMLBatch"
Convert several Pod files to several \s-1HTML\s0 files

- Pod::Simple::JustPod
Item "Pod::Simple::JustPod"
Just the Pod, the whole Pod, and nothing but the Pod

- Pod::Simple::LinkSection
Item "Pod::Simple::LinkSection"
Represent \*(L"section\*(R" attributes of L codes

- Pod::Simple::Methody
Item "Pod::Simple::Methody"
Turn Pod::Simple events into method calls

- Pod::Simple::PullParser
Item "Pod::Simple::PullParser"
A pull-parser interface to parsing Pod

- Pod::Simple::PullParserEndToken
Item "Pod::Simple::PullParserEndToken"
End-tokens from Pod::Simple::PullParser

- Pod::Simple::PullParserStartToken
Item "Pod::Simple::PullParserStartToken"
Start-tokens from Pod::Simple::PullParser

- Pod::Simple::PullParserTextToken
Item "Pod::Simple::PullParserTextToken"
Text-tokens from Pod::Simple::PullParser

- Pod::Simple::PullParserToken
Item "Pod::Simple::PullParserToken"
Tokens from Pod::Simple::PullParser

- Pod::Simple::RTF
Item "Pod::Simple::RTF"
Format Pod as \s-1RTF\s0

- Pod::Simple::Search
Item "Pod::Simple::Search"
Find \s-1POD\s0 documents in directory trees

- Pod::Simple::SimpleTree
Item "Pod::Simple::SimpleTree"
Parse Pod into a simple parse tree

- Pod::Simple::Subclassing
Item "Pod::Simple::Subclassing"
Write a formatter as a Pod::Simple subclass

- Pod::Simple::Text
Item "Pod::Simple::Text"
Format Pod as plaintext

- Pod::Simple::TextContent
Item "Pod::Simple::TextContent"
Get the text content of Pod

- Pod::Simple::XHTML
Item "Pod::Simple::XHTML"
Format Pod as validating \s-1XHTML\s0

- Pod::Simple::XMLOutStream
Item "Pod::Simple::XMLOutStream"
Turn Pod into \s-1XML\s0

- Pod::Text
Item "Pod::Text"
Convert \s-1POD\s0 data to formatted text

- Pod::Text::Color
Item "Pod::Text::Color"
Convert \s-1POD\s0 data to formatted color \s-1ASCII\s0 text

- Pod::Text::Overstrike
Item "Pod::Text::Overstrike"
Convert \s-1POD\s0 data to formatted overstrike text

- Pod::Text::Termcap
Item "Pod::Text::Termcap"
Convert \s-1POD\s0 data to \s-1ASCII\s0 text with format escapes

- Pod::Usage
Item "Pod::Usage"
Extracts \s-1POD\s0 documentation and shows usage information

- SDBM_File
Item "SDBM_File"
Tied access to sdbm files

- Safe
Item "Safe"
Compile and execute code in restricted compartments

- Scalar::Util
Item "Scalar::Util"
A selection of general-utility scalar subroutines

- Search::Dict
Item "Search::Dict"
Look - search for key in dictionary file

- SelectSaver
Item "SelectSaver"
Save and restore selected file handle

- SelfLoader
Item "SelfLoader"
Load functions only on demand

- Storable
Item "Storable"
Persistence for Perl data structures

- Sub::Util
Item "Sub::Util"
A selection of utility subroutines for subs and \s-1CODE\s0 references

- Symbol
Item "Symbol"
Manipulate Perl symbols and their names

- Sys::Hostname
Item "Sys::Hostname"
Try every conceivable way to get hostname

- Sys::Syslog
Item "Sys::Syslog"
Perl interface to the \s-1UNIX\s0 **syslog**\|(3) calls

- Sys::Syslog::Win32
Item "Sys::Syslog::Win32"
Win32 support for Sys::Syslog

- TAP::Base
Item "TAP::Base"
Base class that provides common functionality to TAP::Parser

- TAP::Formatter::Base
Item "TAP::Formatter::Base"
Base class for harness output delegates

- TAP::Formatter::Color
Item "TAP::Formatter::Color"
Run Perl test scripts with color

- TAP::Formatter::Console
Item "TAP::Formatter::Console"
Harness output delegate for default console output

- TAP::Formatter::Console::ParallelSession
Item "TAP::Formatter::Console::ParallelSession"
Harness output delegate for parallel console output

- TAP::Formatter::Console::Session
Item "TAP::Formatter::Console::Session"
Harness output delegate for default console output

- TAP::Formatter::File
Item "TAP::Formatter::File"
Harness output delegate for file output

- TAP::Formatter::File::Session
Item "TAP::Formatter::File::Session"
Harness output delegate for file output

- TAP::Formatter::Session
Item "TAP::Formatter::Session"
Abstract base class for harness output delegate

- TAP::Harness
Item "TAP::Harness"
Run test scripts with statistics

- TAP::Harness::Env
Item "TAP::Harness::Env"
Parsing harness related environmental variables where appropriate

- TAP::Object
Item "TAP::Object"
Base class that provides common functionality to all \f(CW\*(C`TAP::*\*(C' modules

- TAP::Parser
Item "TAP::Parser"
Parse \s-1TAP\s0 output

- TAP::Parser::Aggregator
Item "TAP::Parser::Aggregator"
Aggregate TAP::Parser results

- TAP::Parser::Grammar
Item "TAP::Parser::Grammar"
A grammar for the Test Anything Protocol.

- TAP::Parser::Iterator
Item "TAP::Parser::Iterator"
Base class for \s-1TAP\s0 source iterators

- TAP::Parser::Iterator::Array
Item "TAP::Parser::Iterator::Array"
Iterator for array-based \s-1TAP\s0 sources

- TAP::Parser::Iterator::Process
Item "TAP::Parser::Iterator::Process"
Iterator for process-based \s-1TAP\s0 sources

- TAP::Parser::Iterator::Stream
Item "TAP::Parser::Iterator::Stream"
Iterator for filehandle-based \s-1TAP\s0 sources

- TAP::Parser::IteratorFactory
Item "TAP::Parser::IteratorFactory"
Figures out which SourceHandler objects to use for a given Source

- TAP::Parser::Multiplexer
Item "TAP::Parser::Multiplexer"
Multiplex multiple TAP::Parsers

- TAP::Parser::Result
Item "TAP::Parser::Result"
Base class for TAP::Parser output objects

- TAP::Parser::Result::Bailout
Item "TAP::Parser::Result::Bailout"
Bailout result token.

- TAP::Parser::Result::Comment
Item "TAP::Parser::Result::Comment"
Comment result token.

- TAP::Parser::Result::Plan
Item "TAP::Parser::Result::Plan"
Plan result token.

- TAP::Parser::Result::Pragma
Item "TAP::Parser::Result::Pragma"
\s-1TAP\s0 pragma token.

- TAP::Parser::Result::Test
Item "TAP::Parser::Result::Test"
Test result token.

- TAP::Parser::Result::Unknown
Item "TAP::Parser::Result::Unknown"
Unknown result token.

- TAP::Parser::Result::Version
Item "TAP::Parser::Result::Version"
\s-1TAP\s0 syntax version token.

- TAP::Parser::Result::YAML
Item "TAP::Parser::Result::YAML"
\s-1YAML\s0 result token.

- TAP::Parser::ResultFactory
Item "TAP::Parser::ResultFactory"
Factory for creating TAP::Parser output objects

- TAP::Parser::Scheduler
Item "TAP::Parser::Scheduler"
Schedule tests during parallel testing

- TAP::Parser::Scheduler::Job
Item "TAP::Parser::Scheduler::Job"
A single testing job.

- TAP::Parser::Scheduler::Spinner
Item "TAP::Parser::Scheduler::Spinner"
A no-op job.

- TAP::Parser::Source
Item "TAP::Parser::Source"
A \s-1TAP\s0 source & meta data about it

- TAP::Parser::SourceHandler
Item "TAP::Parser::SourceHandler"
Base class for different \s-1TAP\s0 source handlers

- TAP::Parser::SourceHandler::Executable
Item "TAP::Parser::SourceHandler::Executable"
Stream output from an executable \s-1TAP\s0 source

- TAP::Parser::SourceHandler::File
Item "TAP::Parser::SourceHandler::File"
Stream \s-1TAP\s0 from a text file.

- TAP::Parser::SourceHandler::Handle
Item "TAP::Parser::SourceHandler::Handle"
Stream \s-1TAP\s0 from an IO::Handle or a \s-1GLOB.\s0

- TAP::Parser::SourceHandler::Perl
Item "TAP::Parser::SourceHandler::Perl"
Stream \s-1TAP\s0 from a Perl executable

- TAP::Parser::SourceHandler::RawTAP
Item "TAP::Parser::SourceHandler::RawTAP"
Stream output from raw \s-1TAP\s0 in a scalar/array ref.

- TAP::Parser::YAMLish::Reader
Item "TAP::Parser::YAMLish::Reader"
Read YAMLish data from iterator

- TAP::Parser::YAMLish::Writer
Item "TAP::Parser::YAMLish::Writer"
Write YAMLish data

- Term::ANSIColor
Item "Term::ANSIColor"
Color screen output using \s-1ANSI\s0 escape sequences

- Term::Cap
Item "Term::Cap"
Perl termcap interface

- Term::Complete
Item "Term::Complete"
Perl word completion module

- Term::ReadLine
Item "Term::ReadLine"
Perl interface to various \f(CW\*(C`readline\*(C' packages.

- Test
Item "Test"
Provides a simple framework for writing test scripts

- Test2
Item "Test2"
Framework for writing test tools that all work together.

- Test2::API
Item "Test2::API"
Primary interface for writing Test2 based testing tools.

- Test2::API::Breakage
Item "Test2::API::Breakage"
What breaks at what version

- Test2::API::Context
Item "Test2::API::Context"
Object to represent a testing context.

- Test2::API::Instance
Item "Test2::API::Instance"
Object used by Test2::API under the hood

- Test2::API::InterceptResult
Item "Test2::API::InterceptResult"
Representation of a list of events.

- Test2::API::InterceptResult::Event
Item "Test2::API::InterceptResult::Event"
Representation of an event for use in

- Test2::API::InterceptResult::Hub
Item "Test2::API::InterceptResult::Hub"
Hub used by InterceptResult.

- Test2::API::InterceptResult::Squasher
Item "Test2::API::InterceptResult::Squasher"
Encapsulation of the algorithm that

- Test2::API::Stack
Item "Test2::API::Stack"
Object to manage a stack of Test2::Hub

- Test2::Event
Item "Test2::Event"
Base class for events

- Test2::Event::Bail
Item "Test2::Event::Bail"
Bailout!

- Test2::Event::Diag
Item "Test2::Event::Diag"
Diag event type

- Test2::Event::Encoding
Item "Test2::Event::Encoding"
Set the encoding for the output stream

- Test2::Event::Exception
Item "Test2::Event::Exception"
Exception event

- Test2::Event::Fail
Item "Test2::Event::Fail"
Event for a simple failed assertion

- Test2::Event::Generic
Item "Test2::Event::Generic"
Generic event type.

- Test2::Event::Note
Item "Test2::Event::Note"
Note event type

- Test2::Event::Ok
Item "Test2::Event::Ok"
Ok event type

- Test2::Event::Pass
Item "Test2::Event::Pass"
Event for a simple passing assertion

- Test2::Event::Plan
Item "Test2::Event::Plan"
The event of a plan

- Test2::Event::Skip
Item "Test2::Event::Skip"
Skip event type

- Test2::Event::Subtest
Item "Test2::Event::Subtest"
Event for subtest types

- Test2::Event::TAP::Version
Item "Test2::Event::TAP::Version"
Event for \s-1TAP\s0 version.

- Test2::Event::V2
Item "Test2::Event::V2"
Second generation event.

- Test2::Event::Waiting
Item "Test2::Event::Waiting"
Tell all procs/threads it is time to be done

- Test2::EventFacet
Item "Test2::EventFacet"
Base class for all event facets.

- Test2::EventFacet::About
Item "Test2::EventFacet::About"
Facet with event details.

- Test2::EventFacet::Amnesty
Item "Test2::EventFacet::Amnesty"
Facet for assertion amnesty.

- Test2::EventFacet::Assert
Item "Test2::EventFacet::Assert"
Facet representing an assertion.

- Test2::EventFacet::Control
Item "Test2::EventFacet::Control"
Facet for hub actions and behaviors.

- Test2::EventFacet::Error
Item "Test2::EventFacet::Error"
Facet for errors that need to be shown.

- Test2::EventFacet::Hub
Item "Test2::EventFacet::Hub"
Facet for the hubs an event passes through.

- Test2::EventFacet::Info
Item "Test2::EventFacet::Info"
Facet for information a developer might care about.

- Test2::EventFacet::Info::Table
Item "Test2::EventFacet::Info::Table"
Intermediary representation of a table.

- Test2::EventFacet::Meta
Item "Test2::EventFacet::Meta"
Facet for meta-data

- Test2::EventFacet::Parent
Item "Test2::EventFacet::Parent"
Facet for events contains other events

- Test2::EventFacet::Plan
Item "Test2::EventFacet::Plan"
Facet for setting the plan

- Test2::EventFacet::Render
Item "Test2::EventFacet::Render"
Facet that dictates how to render an event.

- Test2::EventFacet::Trace
Item "Test2::EventFacet::Trace"
Debug information for events

- Test2::Formatter
Item "Test2::Formatter"
Namespace for formatters.

- Test2::Formatter::TAP
Item "Test2::Formatter::TAP"
Standard \s-1TAP\s0 formatter

- Test2::Hub
Item "Test2::Hub"
The conduit through which all events flow.

- Test2::Hub::Interceptor
Item "Test2::Hub::Interceptor"
Hub used by interceptor to grab results.

- Test2::Hub::Interceptor::Terminator
Item "Test2::Hub::Interceptor::Terminator"
Exception class used by

- Test2::Hub::Subtest
Item "Test2::Hub::Subtest"
Hub used by subtests

- Test2::IPC
Item "Test2::IPC"
Turn on \s-1IPC\s0 for threading or forking support.

- Test2::IPC::Driver
Item "Test2::IPC::Driver"
Base class for Test2 \s-1IPC\s0 drivers.

- Test2::IPC::Driver::Files
Item "Test2::IPC::Driver::Files"
Temp dir + Files concurrency model.

- Test2::Tools::Tiny
Item "Test2::Tools::Tiny"
Tiny set of tools for unfortunate souls who cannot use

- Test2::Transition
Item "Test2::Transition"
Transition notes when upgrading to Test2

- Test2::Util
Item "Test2::Util"
Tools used by Test2 and friends.

- Test2::Util::ExternalMeta
Item "Test2::Util::ExternalMeta"
Allow third party tools to safely attach meta-data

- Test2::Util::Facets2Legacy
Item "Test2::Util::Facets2Legacy"
Convert facet data to the legacy event \s-1API.\s0

- Test2::Util::HashBase
Item "Test2::Util::HashBase"
Build hash based classes.

- Test2::Util::Trace
Item "Test2::Util::Trace"
Legacy wrapper fro Test2::EventFacet::Trace.

- Test::Builder
Item "Test::Builder"
Backend for building test libraries

- Test::Builder::Formatter
Item "Test::Builder::Formatter"
Test::Builder subclass of Test2::Formatter::TAP

- Test::Builder::IO::Scalar
Item "Test::Builder::IO::Scalar"
A copy of IO::Scalar for Test::Builder

- Test::Builder::Module
Item "Test::Builder::Module"
Base class for test modules

- Test::Builder::Tester
Item "Test::Builder::Tester"
Test testsuites that have been built with

- Test::Builder::Tester::Color
Item "Test::Builder::Tester::Color"
Turn on colour in Test::Builder::Tester

- Test::Builder::TodoDiag
Item "Test::Builder::TodoDiag"
Test::Builder subclass of Test2::Event::Diag

- Test::Harness
Item "Test::Harness"
Run Perl standard test scripts with statistics

- Test::Harness::Beyond
Item "Test::Harness::Beyond"
Beyond make test

- Test::More
Item "Test::More"
Yet another framework for writing test scripts

- Test::Simple
Item "Test::Simple"
Basic utilities for writing tests.

- Test::Tester
Item "Test::Tester"
Ease testing test modules built with Test::Builder

- Test::Tester::Capture
Item "Test::Tester::Capture"
Help testing test modules built with Test::Builder

- Test::Tester::CaptureRunner
Item "Test::Tester::CaptureRunner"
Help testing test modules built with Test::Builder

- Test::Tutorial
Item "Test::Tutorial"
A tutorial about writing really basic tests

- Test::use::ok
Item "Test::use::ok"
Alternative to Test::More::use_ok

- Text::Abbrev
Item "Text::Abbrev"
Abbrev - create an abbreviation table from a list

- Text::Balanced
Item "Text::Balanced"
Extract delimited text sequences from strings.

- Text::ParseWords
Item "Text::ParseWords"
Parse text into an array of tokens or array of arrays

- Text::Tabs
Item "Text::Tabs"
Expand and unexpand tabs like unix **expand**\|(1) and **unexpand**\|(1)

- Text::Wrap
Item "Text::Wrap"
Line wrapping to form simple paragraphs

- Thread
Item "Thread"
Manipulate threads in Perl (for old code only)

- Thread::Queue
Item "Thread::Queue"
Thread-safe queues

- Thread::Semaphore
Item "Thread::Semaphore"
Thread-safe semaphores

- Tie::Array
Item "Tie::Array"
Base class for tied arrays

- Tie::File
Item "Tie::File"
Access the lines of a disk file via a Perl array

- Tie::Handle
Item "Tie::Handle"
Base class definitions for tied handles

- Tie::Hash
Item "Tie::Hash"
Base class definitions for tied hashes

- Tie::Hash::NamedCapture
Item "Tie::Hash::NamedCapture"
Named regexp capture buffers

- Tie::Memoize
Item "Tie::Memoize"
Add data to hash when needed

- Tie::RefHash
Item "Tie::RefHash"
Use references as hash keys

- Tie::Scalar
Item "Tie::Scalar"
Base class definitions for tied scalars

- Tie::StdHandle
Item "Tie::StdHandle"
Base class definitions for tied handles

- Tie::SubstrHash
Item "Tie::SubstrHash"
Fixed-table-size, fixed-key-length hashing

- Time::HiRes
Item "Time::HiRes"
High resolution alarm, sleep, gettimeofday, interval timers

- Time::Local
Item "Time::Local"
Efficiently compute time from local and \s-1GMT\s0 time

- Time::Piece
Item "Time::Piece"
Object Oriented time objects

- Time::Seconds
Item "Time::Seconds"
A simple \s-1API\s0 to convert seconds to other date values

- Time::gmtime
Item "Time::gmtime"
By-name interface to Perl's built-in **gmtime()** function

- Time::localtime
Item "Time::localtime"
By-name interface to Perl's built-in **localtime()** function

- Time::tm
Item "Time::tm"
Internal object used by Time::gmtime and Time::localtime

- \s-1UNIVERSAL\s0
Item "UNIVERSAL"
Base class for \s-1ALL\s0 classes (blessed references)

- Unicode::Collate
Item "Unicode::Collate"
Unicode Collation Algorithm

- Unicode::Collate::CJK::Big5
Item "Unicode::Collate::CJK::Big5"
Weighting \s-1CJK\s0 Unified Ideographs

- Unicode::Collate::CJK::GB2312
Item "Unicode::Collate::CJK::GB2312"
Weighting \s-1CJK\s0 Unified Ideographs

- Unicode::Collate::CJK::JISX0208
Item "Unicode::Collate::CJK::JISX0208"
Weighting \s-1JIS KANJI\s0 for Unicode::Collate

- Unicode::Collate::CJK::Korean
Item "Unicode::Collate::CJK::Korean"
Weighting \s-1CJK\s0 Unified Ideographs

- Unicode::Collate::CJK::Pinyin
Item "Unicode::Collate::CJK::Pinyin"
Weighting \s-1CJK\s0 Unified Ideographs

- Unicode::Collate::CJK::Stroke
Item "Unicode::Collate::CJK::Stroke"
Weighting \s-1CJK\s0 Unified Ideographs

- Unicode::Collate::CJK::Zhuyin
Item "Unicode::Collate::CJK::Zhuyin"
Weighting \s-1CJK\s0 Unified Ideographs

- Unicode::Collate::Locale
Item "Unicode::Collate::Locale"
Linguistic tailoring for \s-1DUCET\s0 via Unicode::Collate

- Unicode::Normalize
Item "Unicode::Normalize"
Unicode Normalization Forms

- Unicode::UCD
Item "Unicode::UCD"
Unicode character database

- User::grent
Item "User::grent"
By-name interface to Perl's built-in getgr*() functions

- User::pwent
Item "User::pwent"
By-name interface to Perl's built-in getpw*() functions

- VMS::DCLsym
Item "VMS::DCLsym"
Perl extension to manipulate \s-1DCL\s0 symbols

- VMS::Filespec
Item "VMS::Filespec"
Convert between \s-1VMS\s0 and Unix file specification syntax

- VMS::Stdio
Item "VMS::Stdio"
Standard I/O functions via \s-1VMS\s0 extensions

- Win32
Item "Win32"
Interfaces to some Win32 \s-1API\s0 Functions

- Win32API::File
Item "Win32API::File"
Low-level access to Win32 system \s-1API\s0 calls for files/dirs.

- Win32CORE
Item "Win32CORE"
Win32 \s-1CORE\s0 function stubs

- XS::APItest
Item "XS::APItest"
Test the perl C \s-1API\s0

- XS::Typemap
Item "XS::Typemap"
Module to test the \s-1XS\s0 typemaps distributed with perl

- XSLoader
Item "XSLoader"
Dynamically load C libraries into Perl code

- autodie::Scope::Guard
Item "autodie::Scope::Guard"
Wrapper class for calling subs at end of scope

- autodie::Scope::GuardStack
Item "autodie::Scope::GuardStack"
Hook stack for managing scopes via %^H

- autodie::Util
Item "autodie::Util"
Internal Utility subroutines for autodie and Fatal

- version::Internals
Item "version::Internals"
Perl extension for Version Objects

To find out *all* modules installed on your system, including
those without documentation or outside the standard release,
just use the following command (under the default win32 shell,
double quotes should be used instead of single quotes).

.Vb 3
    % perl -MFile::Find=find -MFile::Spec::Functions -Tlwe \\
      \*(Aqfind \{ wanted => sub \{ print canonpath $_ if /\\.pm\\z/ \},
      no_chdir => 1 \}, @INC\*(Aq
.Ve

(The -T is here to prevent '.' from being listed in \f(CW@INC.)
They should all have their own documentation installed and accessible
via your system **man**\|(1) command.  If you do not have a **find**
program, you can use the Perl **find2perl** program instead, which
generates Perl code as output you can run through perl.  If you
have a **man** program but it doesn't find your modules, you'll have
to fix your manpath.  See perl for details.  If you have no
system **man** command, you might try the **perldoc** program.

Note also that the command \f(CW\*(C`perldoc perllocal\*(C' gives you a (possibly
incomplete) list of the modules that have been further installed on
your system. (The perllocal.pod file is updated by the standard MakeMaker
install process.)

### Extension Modules

Subsection "Extension Modules"
Extension modules are written in C (or a mix of Perl and C).  They
are usually dynamically loaded into Perl if and when you need them,
but may also be linked in statically.  Supported extension modules
include Socket, Fcntl, and \s-1POSIX.\s0

Many popular C extension modules do not come bundled (at least, not
completely) due to their sizes, volatility, or simply lack of time
for adequate testing and configuration across the multitude of
platforms on which Perl was beta-tested.  You are encouraged to
look for them on \s-1CPAN\s0 (described below), or using web search engines
like Google or DuckDuckGo.

## CPAN

Header "CPAN"
\s-1CPAN\s0 stands for Comprehensive Perl Archive Network; it's a globally
replicated trove of Perl materials, including documentation, style
guides, tricks and traps, alternate ports to non-Unix systems and
occasional binary distributions for these.   Search engines for
\s-1CPAN\s0 can be found at https://www.cpan.org/

Most importantly, \s-1CPAN\s0 includes around a thousand unbundled modules,
some of which require a C compiler to build.  Major categories of
modules are:

- \(bu
Language Extensions and Documentation Tools

- \(bu
Development Support

- \(bu
Operating System Interfaces

- \(bu
Networking, Device Control (modems) and InterProcess Communication

- \(bu
Data Types and Data Type Utilities

- \(bu
Database Interfaces

- \(bu
User Interfaces

- \(bu
Interfaces to / Emulations of Other Programming Languages

- \(bu
File Names, File Systems and File Locking (see also File Handles)

- \(bu
String Processing, Language Text Processing, Parsing, and Searching

- \(bu
Option, Argument, Parameter, and Configuration File Processing

- \(bu
Internationalization and Locale

- \(bu
Authentication, Security, and Encryption

- \(bu
World Wide Web, \s-1HTML, HTTP, CGI, MIME\s0

- \(bu
Server and Daemon Utilities

- \(bu
Archiving and Compression

- \(bu
Images, Pixmap and Bitmap Manipulation, Drawing, and Graphing

- \(bu
Mail and Usenet News

- \(bu
Control Flow Utilities (callbacks and exceptions etc)

- \(bu
File Handle and Input/Output Stream Utilities

- \(bu
Miscellaneous Modules

The list of the registered \s-1CPAN\s0 sites follows.
Please note that the sorting order is alphabetical on fields:

Continent
   |
   |-->Country
         |
         |-->[state/province]
                   |
                   |-->ftp
                   |
                   |-->[http]

and thus the North American servers happen to be listed between the
European and the South American sites.

Registered \s-1CPAN\s0 sites

### Africa

Subsection "Africa"

- South Africa
Item "South Africa"
.Vb 8
  http://mirror.is.co.za/pub/cpan/
  ftp://ftp.is.co.za/pub/cpan/
  http://cpan.mirror.ac.za/
  ftp://cpan.mirror.ac.za/
  http://cpan.saix.net/
  ftp://ftp.saix.net/pub/CPAN/
  http://ftp.wa.co.za/pub/CPAN/
  ftp://ftp.wa.co.za/pub/CPAN/
.Ve

- Uganda
Item "Uganda"
.Vb 1
  http://mirror.ucu.ac.ug/cpan/
.Ve

- Zimbabwe
Item "Zimbabwe"
.Vb 2
  http://mirror.zol.co.zw/CPAN/
  ftp://mirror.zol.co.zw/CPAN/
.Ve

### Asia

Subsection "Asia"

- Bangladesh
Item "Bangladesh"
.Vb 2
  http://mirror.dhakacom.com/CPAN/
  ftp://mirror.dhakacom.com/CPAN/
.Ve

- China
Item "China"
.Vb 10
  http://cpan.communilink.net/
  http://ftp.cuhk.edu.hk/pub/packages/perl/CPAN/
  ftp://ftp.cuhk.edu.hk/pub/packages/perl/CPAN/
  http://mirrors.hust.edu.cn/CPAN/
  http://mirrors.neusoft.edu.cn/cpan/
  http://mirror.lzu.edu.cn/CPAN/
  http://mirrors.163.com/cpan/
  http://mirrors.sohu.com/CPAN/
  http://mirrors.ustc.edu.cn/CPAN/
  ftp://mirrors.ustc.edu.cn/CPAN/
  http://mirrors.xmu.edu.cn/CPAN/
  ftp://mirrors.xmu.edu.cn/CPAN/
  http://mirrors.zju.edu.cn/CPAN/
.Ve

- India
Item "India"
.Vb 2
  http://cpan.excellmedia.net/
  http://perlmirror.indialinks.com/
.Ve

- Indonesia
Item "Indonesia"
.Vb 5
  http://kambing.ui.ac.id/cpan/
  http://cpan.pesat.net.id/
  http://mirror.poliwangi.ac.id/CPAN/
  http://kartolo.sby.datautama.net.id/CPAN/
  http://mirror.wanxp.id/cpan/
.Ve

- Iran
Item "Iran"
.Vb 1
  http://mirror.yazd.ac.ir/cpan/
.Ve

- Israel
Item "Israel"
.Vb 1
  http://biocourse.weizmann.ac.il/CPAN/
.Ve

- Japan
Item "Japan"
.Vb 12
  http://ftp.jaist.ac.jp/pub/CPAN/
  ftp://ftp.jaist.ac.jp/pub/CPAN/
  http://mirror.jre655.com/CPAN/
  ftp://mirror.jre655.com/CPAN/
  ftp://ftp.kddilabs.jp/CPAN/
  http://ftp.nara.wide.ad.jp/pub/CPAN/
  ftp://ftp.nara.wide.ad.jp/pub/CPAN/
  http://ftp.riken.jp/lang/CPAN/
  ftp://ftp.riken.jp/lang/CPAN/
  ftp://ftp.u-aizu.ac.jp/pub/CPAN/
  http://ftp.yz.yamagata-u.ac.jp/pub/lang/cpan/
  ftp://ftp.yz.yamagata-u.ac.jp/pub/lang/cpan/
.Ve

- Kazakhstan
Item "Kazakhstan"
.Vb 2
  http://mirror.neolabs.kz/CPAN/
  ftp://mirror.neolabs.kz/CPAN/
.Ve

- Philippines
Item "Philippines"
.Vb 4
  http://mirror.pregi.net/CPAN/
  ftp://mirror.pregi.net/CPAN/
  http://mirror.rise.ph/cpan/
  ftp://mirror.rise.ph/cpan/
.Ve

- Qatar
Item "Qatar"
.Vb 2
  http://mirror.qnren.qa/CPAN/
  ftp://mirror.qnren.qa/CPAN/
.Ve

- Republic of Korea
Item "Republic of Korea"
.Vb 9
  http://cpan.mirror.cdnetworks.com/
  ftp://cpan.mirror.cdnetworks.com/CPAN/
  http://ftp.kaist.ac.kr/pub/CPAN/
  ftp://ftp.kaist.ac.kr/CPAN/
  http://ftp.kr.freebsd.org/pub/CPAN/
  ftp://ftp.kr.freebsd.org/pub/CPAN/
  http://mirror.navercorp.com/CPAN/
  http://ftp.neowiz.com/CPAN/
  ftp://ftp.neowiz.com/CPAN/
.Ve

- Singapore
Item "Singapore"
.Vb 3
  http://cpan.mirror.choon.net/
  http://mirror.0x.sg/CPAN/
  ftp://mirror.0x.sg/CPAN/
.Ve

- Taiwan
Item "Taiwan"
.Vb 10
  http://cpan.cdpa.nsysu.edu.tw/Unix/Lang/CPAN/
  ftp://cpan.cdpa.nsysu.edu.tw/Unix/Lang/CPAN/
  http://cpan.stu.edu.tw/
  ftp://ftp.stu.edu.tw/CPAN/
  http://ftp.yzu.edu.tw/CPAN/
  ftp://ftp.yzu.edu.tw/CPAN/
  http://cpan.nctu.edu.tw/
  ftp://cpan.nctu.edu.tw/
  http://ftp.ubuntu-tw.org/mirror/CPAN/
  ftp://ftp.ubuntu-tw.org/mirror/CPAN/
.Ve

- Turkey
Item "Turkey"
.Vb 4
  http://cpan.ulak.net.tr/
  ftp://ftp.ulak.net.tr/pub/perl/CPAN/
  http://mirror.vit.com.tr/mirror/CPAN/
  ftp://mirror.vit.com.tr/CPAN/
.Ve

- Viet Nam
Item "Viet Nam"
.Vb 3
  http://mirrors.digipower.vn/CPAN/
  http://mirror.downloadvn.com/cpan/
  http://mirrors.vinahost.vn/CPAN/
.Ve

### Europe

Subsection "Europe"

- Austria
Item "Austria"
.Vb 6
  http://cpan.inode.at/
  ftp://cpan.inode.at/
  http://mirror.easyname.at/cpan/
  ftp://mirror.easyname.at/cpan/
  http://gd.tuwien.ac.at/languages/perl/CPAN/
  ftp://gd.tuwien.ac.at/pub/CPAN/
.Ve

- Belarus
Item "Belarus"
.Vb 4
  http://ftp.byfly.by/pub/CPAN/
  ftp://ftp.byfly.by/pub/CPAN/
  http://mirror.datacenter.by/pub/CPAN/
  ftp://mirror.datacenter.by/pub/CPAN/
.Ve

- Belgium
Item "Belgium"
.Vb 5
  http://ftp.belnet.be/ftp.cpan.org/
  ftp://ftp.belnet.be/mirror/ftp.cpan.org/
  http://cpan.cu.be/
  http://lib.ugent.be/CPAN/
  http://cpan.weepeetelecom.be/
.Ve

- Bosnia and Herzegovina
Item "Bosnia and Herzegovina"
.Vb 2
  http://cpan.mirror.ba/
  ftp://ftp.mirror.ba/CPAN/
.Ve

- Bulgaria
Item "Bulgaria"
.Vb 4
  http://mirrors.neterra.net/CPAN/
  ftp://mirrors.neterra.net/CPAN/
  http://mirrors.netix.net/CPAN/
  ftp://mirrors.netix.net/CPAN/
.Ve

- Croatia
Item "Croatia"
.Vb 2
  http://ftp.carnet.hr/pub/CPAN/
  ftp://ftp.carnet.hr/pub/CPAN/
.Ve

- Czech Republic
Item "Czech Republic"
.Vb 7
  http://mirror.dkm.cz/cpan/
  ftp://mirror.dkm.cz/cpan/
  ftp://ftp.fi.muni.cz/pub/CPAN/
  http://mirrors.nic.cz/CPAN/
  ftp://mirrors.nic.cz/pub/CPAN/
  http://cpan.mirror.vutbr.cz/
  ftp://mirror.vutbr.cz/cpan/
.Ve

- Denmark
Item "Denmark"
.Vb 3
  http://www.cpan.dk/
  http://mirrors.dotsrc.org/cpan/
  ftp://mirrors.dotsrc.org/cpan/
.Ve

- Finland
Item "Finland"
.Vb 1
  ftp://ftp.funet.fi/pub/languages/perl/CPAN/
.Ve

- France
Item "France"
.Vb 11
  http://ftp.ciril.fr/pub/cpan/
  ftp://ftp.ciril.fr/pub/cpan/
  http://distrib-coffee.ipsl.jussieu.fr/pub/mirrors/cpan/
  ftp://distrib-coffee.ipsl.jussieu.fr/pub/mirrors/cpan/
  http://ftp.lip6.fr/pub/perl/CPAN/
  ftp://ftp.lip6.fr/pub/perl/CPAN/
  http://mirror.ibcp.fr/pub/CPAN/
  ftp://ftp.oleane.net/pub/CPAN/
  http://cpan.mirrors.ovh.net/ftp.cpan.org/
  ftp://cpan.mirrors.ovh.net/ftp.cpan.org/
  http://cpan.enstimac.fr/
.Ve

- Germany
Item "Germany"
.Vb 10
  http://mirror.23media.de/cpan/
  ftp://mirror.23media.de/cpan/
  http://artfiles.org/cpan.org/
  ftp://artfiles.org/cpan.org/
  http://mirror.bibleonline.ru/cpan/
  http://mirror.checkdomain.de/CPAN/
  ftp://mirror.checkdomain.de/CPAN/
  http://cpan.noris.de/
  http://mirror.de.leaseweb.net/CPAN/
  ftp://mirror.de.leaseweb.net/CPAN/
  http://cpan.mirror.euserv.net/
  ftp://mirror.euserv.net/cpan/
  http://ftp-stud.hs-esslingen.de/pub/Mirrors/CPAN/
  ftp://mirror.fraunhofer.de/CPAN/
  ftp://ftp.freenet.de/pub/ftp.cpan.org/pub/CPAN/
  http://ftp.hosteurope.de/pub/CPAN/
  ftp://ftp.hosteurope.de/pub/CPAN/
  ftp://ftp.fu-berlin.de/unix/languages/perl/
  http://ftp.gwdg.de/pub/languages/perl/CPAN/
  ftp://ftp.gwdg.de/pub/languages/perl/CPAN/
  http://ftp.hawo.stw.uni-erlangen.de/CPAN/
  ftp://ftp.hawo.stw.uni-erlangen.de/CPAN/
  http://cpan.mirror.iphh.net/
  ftp://cpan.mirror.iphh.net/pub/CPAN/
  ftp://ftp.mpi-inf.mpg.de/pub/perl/CPAN/
  http://cpan.netbet.org/
  http://mirror.netcologne.de/cpan/
  ftp://mirror.netcologne.de/cpan/
  ftp://mirror.petamem.com/CPAN/
  http://www.planet-elektronik.de/CPAN/
  http://ftp.halifax.rwth-aachen.de/cpan/
  ftp://ftp.halifax.rwth-aachen.de/cpan/
  http://mirror.softaculous.com/cpan/
  http://ftp.u-tx.net/CPAN/
  ftp://ftp.u-tx.net/CPAN/
  http://mirror.reismil.ch/CPAN/
.Ve

- Greece
Item "Greece"
.Vb 4
  http://cpan.cc.uoc.gr/mirrors/CPAN/
  ftp://ftp.cc.uoc.gr/mirrors/CPAN/
  http://ftp.ntua.gr/pub/lang/perl/
  ftp://ftp.ntua.gr/pub/lang/perl/
.Ve

- Hungary
Item "Hungary"
.Vb 1
  http://mirror.met.hu/CPAN/
.Ve

- Ireland
Item "Ireland"
.Vb 2
  http://ftp.heanet.ie/mirrors/ftp.perl.org/pub/CPAN/
  ftp://ftp.heanet.ie/mirrors/ftp.perl.org/pub/CPAN/
.Ve

- Italy
Item "Italy"
.Vb 5
  http://bo.mirror.garr.it/mirrors/CPAN/
  ftp://ftp.eutelia.it/CPAN_Mirror/
  http://cpan.panu.it/
  ftp://ftp.panu.it/pub/mirrors/perl/CPAN/
  http://cpan.muzzy.it/
.Ve

- Latvia
Item "Latvia"
.Vb 1
  http://kvin.lv/pub/CPAN/
.Ve

- Lithuania
Item "Lithuania"
.Vb 2
  http://ftp.litnet.lt/pub/CPAN/
  ftp://ftp.litnet.lt/pub/CPAN/
.Ve

- Moldova
Item "Moldova"
.Vb 2
  http://mirror.as43289.net/pub/CPAN/
  ftp://mirror.as43289.net/pub/CPAN/
.Ve

- Netherlands
Item "Netherlands"
.Vb 12
  http://cpan.cs.uu.nl/
  ftp://ftp.cs.uu.nl/pub/CPAN/
  http://mirror.nl.leaseweb.net/CPAN/
  ftp://mirror.nl.leaseweb.net/CPAN/
  http://ftp.nluug.nl/languages/perl/CPAN/
  ftp://ftp.nluug.nl/pub/languages/perl/CPAN/
  http://mirror.transip.net/CPAN/
  ftp://mirror.transip.net/CPAN/
  http://cpan.mirror.triple-it.nl/
  http://ftp.tudelft.nl/cpan/
  ftp://ftp.tudelft.nl/pub/CPAN/
  ftp://download.xs4all.nl/pub/mirror/CPAN/
.Ve

- Norway
Item "Norway"
.Vb 4
  http://cpan.uib.no/
  ftp://cpan.uib.no/pub/CPAN/
  ftp://ftp.uninett.no/pub/languages/perl/CPAN/
  http://cpan.vianett.no/
.Ve

- Poland
Item "Poland"
.Vb 7
  http://ftp.agh.edu.pl/CPAN/
  ftp://ftp.agh.edu.pl/CPAN/
  http://ftp.piotrkosoft.net/pub/mirrors/CPAN/
  ftp://ftp.piotrkosoft.net/pub/mirrors/CPAN/
  ftp://ftp.ps.pl/pub/CPAN/
  http://sunsite.icm.edu.pl/pub/CPAN/
  ftp://sunsite.icm.edu.pl/pub/CPAN/
.Ve

- Portugal
Item "Portugal"
.Vb 4
  http://cpan.dcc.fc.up.pt/
  http://mirrors.fe.up.pt/pub/CPAN/
  http://cpan.perl-hackers.net/
  http://cpan.perl.pt/
.Ve

- Romania
Item "Romania"
.Vb 7
  http://mirrors.hostingromania.ro/cpan.org/
  ftp://ftp.lug.ro/CPAN/
  http://mirrors.m247.ro/CPAN/
  http://mirrors.evowise.com/CPAN/
  http://mirrors.teentelecom.net/CPAN/
  ftp://mirrors.teentelecom.net/CPAN/
  http://mirrors.xservers.ro/CPAN/
.Ve

- Russian Federation
Item "Russian Federation"
.Vb 10
  ftp://ftp.aha.ru/CPAN/
  http://cpan.rinet.ru/
  ftp://cpan.rinet.ru/pub/mirror/CPAN/
  http://cpan-mirror.rbc.ru/pub/CPAN/
  http://mirror.rol.ru/CPAN/
  http://cpan.uni-altai.ru/
  http://cpan.webdesk.ru/
  ftp://cpan.webdesk.ru/cpan/
  http://mirror.yandex.ru/mirrors/cpan/
  ftp://mirror.yandex.ru/mirrors/cpan/
.Ve

- Serbia
Item "Serbia"
.Vb 2
  http://mirror.sbb.rs/CPAN/
  ftp://mirror.sbb.rs/CPAN/
.Ve

- Slovakia
Item "Slovakia"
.Vb 3
  http://cpan.lnx.sk/
  http://tux.rainside.sk/CPAN/
  ftp://tux.rainside.sk/CPAN/
.Ve

- Slovenia
Item "Slovenia"
.Vb 2
  http://ftp.arnes.si/software/perl/CPAN/
  ftp://ftp.arnes.si/software/perl/CPAN/
.Ve

- Spain
Item "Spain"
.Vb 4
  http://mirrors.evowise.com/CPAN/
  http://osl.ugr.es/CPAN/
  http://ftp.rediris.es/mirror/CPAN/
  ftp://ftp.rediris.es/mirror/CPAN/
.Ve

- Sweden
Item "Sweden"
.Vb 2
  http://ftp.acc.umu.se/mirror/CPAN/
  ftp://ftp.acc.umu.se/mirror/CPAN/
.Ve

- Switzerland
Item "Switzerland"
.Vb 3
  http://www.pirbot.com/mirrors/cpan/
  http://mirror.switch.ch/ftp/mirror/CPAN/
  ftp://mirror.switch.ch/mirror/CPAN/
.Ve

- Ukraine
Item "Ukraine"
.Vb 2
  http://cpan.ip-connect.vn.ua/
  ftp://cpan.ip-connect.vn.ua/mirror/cpan/
.Ve

- United Kingdom
Item "United Kingdom"
.Vb 10
  http://cpan.mirror.anlx.net/
  ftp://ftp.mirror.anlx.net/CPAN/
  http://mirror.bytemark.co.uk/CPAN/
  ftp://mirror.bytemark.co.uk/CPAN/
  http://mirrors.coreix.net/CPAN/
  http://cpan.etla.org/
  ftp://cpan.etla.org/pub/CPAN/
  http://cpan.cpantesters.org/
  http://mirror.sax.uk.as61049.net/CPAN/
  http://mirror.sov.uk.goscomb.net/CPAN/
  http://www.mirrorservice.org/sites/cpan.perl.org/CPAN/
  ftp://ftp.mirrorservice.org/sites/cpan.perl.org/CPAN/
  http://mirror.ox.ac.uk/sites/www.cpan.org/
  ftp://mirror.ox.ac.uk/sites/www.cpan.org/
  http://ftp.ticklers.org/pub/CPAN/
  ftp://ftp.ticklers.org/pub/CPAN/
  http://cpan.mirrors.uk2.net/
  ftp://mirrors.uk2.net/pub/CPAN/
  http://mirror.ukhost4u.com/CPAN/
.Ve

### North America

Subsection "North America"

- Canada
Item "Canada"
.Vb 8
  http://CPAN.mirror.rafal.ca/
  ftp://CPAN.mirror.rafal.ca/pub/CPAN/
  http://mirror.csclub.uwaterloo.ca/CPAN/
  ftp://mirror.csclub.uwaterloo.ca/CPAN/
  http://mirrors.gossamer-threads.com/CPAN/
  http://mirror.its.dal.ca/cpan/
  ftp://mirror.its.dal.ca/cpan/
  ftp://ftp.ottix.net/pub/CPAN/
.Ve

- Costa Rica
Item "Costa Rica"
.Vb 1
  http://mirrors.ucr.ac.cr/CPAN/
.Ve

- Mexico
Item "Mexico"
.Vb 2
  http://www.msg.com.mx/CPAN/
  ftp://ftp.msg.com.mx/pub/CPAN/
.Ve

- United States
Item "United States"

> 0

- Alabama
Item "Alabama"
.PD
.Vb 1
  http://mirror.teklinks.com/CPAN/
.Ve

- Arizona
Item "Arizona"
.Vb 3
  http://mirror.n5tech.com/CPAN/
  http://mirrors.namecheap.com/CPAN/
  ftp://mirrors.namecheap.com/CPAN/
.Ve

- California
Item "California"
.Vb 6
  http://cpan.develooper.com/
  http://httpupdate127.cpanel.net/CPAN/
  http://mirrors.sonic.net/cpan/
  ftp://mirrors.sonic.net/cpan/
  http://www.perl.com/CPAN/
  http://cpan.yimg.com/
.Ve

- Idaho
Item "Idaho"
.Vb 2
  http://mirrors.syringanetworks.net/CPAN/
  ftp://mirrors.syringanetworks.net/CPAN/
.Ve

- Illinois
Item "Illinois"
.Vb 3
  http://cpan.mirrors.hoobly.com/
  http://mirror.team-cymru.org/CPAN/
  ftp://mirror.team-cymru.org/CPAN/
.Ve

- Indiana
Item "Indiana"
.Vb 3
  http://cpan.netnitco.net/
  ftp://cpan.netnitco.net/pub/mirrors/CPAN/
  ftp://ftp.uwsg.iu.edu/pub/perl/CPAN/
.Ve

- Kansas
Item "Kansas"
.Vb 1
  http://mirrors.concertpass.com/cpan/
.Ve

- Massachusetts
Item "Massachusetts"
.Vb 1
  http://mirrors.ccs.neu.edu/CPAN/
.Ve

- Michigan
Item "Michigan"
.Vb 6
  http://cpan.cse.msu.edu/
  ftp://cpan.cse.msu.edu/
  http://httpupdate118.cpanel.net/CPAN/
  http://mirrors-usa.go-parts.com/cpan/
  http://ftp.wayne.edu/CPAN/
  ftp://ftp.wayne.edu/CPAN/
.Ve

- New Hampshire
Item "New Hampshire"
.Vb 1
  http://mirror.metrocast.net/cpan/
.Ve

- New Jersey
Item "New Jersey"
.Vb 5
  http://mirror.datapipe.net/CPAN/
  ftp://mirror.datapipe.net/pub/CPAN/
  http://www.hoovism.com/CPAN/
  ftp://ftp.hoovism.com/CPAN/
  http://cpan.mirror.nac.net/
.Ve

- New York
Item "New York"
.Vb 10
  http://mirror.cc.columbia.edu/pub/software/cpan/
  ftp://mirror.cc.columbia.edu/pub/software/cpan/
  http://cpan.belfry.net/
  http://cpan.erlbaum.net/
  ftp://cpan.erlbaum.net/CPAN/
  http://cpan.hexten.net/
  ftp://cpan.hexten.net/
  http://mirror.nyi.net/CPAN/
  ftp://mirror.nyi.net/pub/CPAN/
  http://noodle.portalus.net/CPAN/
  ftp://noodle.portalus.net/CPAN/
  http://mirrors.rit.edu/CPAN/
  ftp://mirrors.rit.edu/CPAN/
.Ve

- North Carolina
Item "North Carolina"
.Vb 2
  http://httpupdate140.cpanel.net/CPAN/
  http://mirrors.ibiblio.org/CPAN/
.Ve

- Oregon
Item "Oregon"
.Vb 3
  http://ftp.osuosl.org/pub/CPAN/
  ftp://ftp.osuosl.org/pub/CPAN/
  http://mirror.uoregon.edu/CPAN/
.Ve

- Pennsylvania
Item "Pennsylvania"
.Vb 3
  http://cpan.pair.com/
  ftp://cpan.pair.com/pub/CPAN/
  http://cpan.mirrors.ionfish.org/
.Ve

- South Carolina
Item "South Carolina"
.Vb 1
  http://cpan.mirror.clemson.edu/
.Ve

- Texas
Item "Texas"
.Vb 1
  http://mirror.uta.edu/CPAN/
.Ve

- Utah
Item "Utah"
.Vb 3
  http://cpan.cs.utah.edu/
  ftp://cpan.cs.utah.edu/CPAN/
  ftp://mirror.xmission.com/CPAN/
.Ve

- Virginia
Item "Virginia"
.Vb 6
  http://mirror.cogentco.com/pub/CPAN/
  ftp://mirror.cogentco.com/pub/CPAN/
  http://mirror.jmu.edu/pub/CPAN/
  ftp://mirror.jmu.edu/pub/CPAN/
  http://mirror.us.leaseweb.net/CPAN/
  ftp://mirror.us.leaseweb.net/CPAN/
.Ve

- Washington
Item "Washington"
.Vb 2
  http://cpan.llarian.net/
  ftp://cpan.llarian.net/pub/CPAN/
.Ve

- Wisconsin
Item "Wisconsin"
.Vb 2
  http://cpan.mirrors.tds.net/
  ftp://cpan.mirrors.tds.net/pub/CPAN/
.Ve



> 


### Oceania

Subsection "Oceania"

- Australia
Item "Australia"
.Vb 9
  http://mirror.as24220.net/pub/cpan/
  ftp://mirror.as24220.net/pub/cpan/
  http://cpan.mirrors.ilisys.com.au/
  http://cpan.mirror.digitalpacific.com.au/
  ftp://mirror.internode.on.net/pub/cpan/
  http://mirror.optusnet.com.au/CPAN/
  http://cpan.mirror.serversaustralia.com.au/
  http://cpan.uberglobalmirror.com/
  http://mirror.waia.asn.au/pub/cpan/
.Ve

- New Caledonia
Item "New Caledonia"
.Vb 4
  http://cpan.lagoon.nc/pub/CPAN/
  ftp://cpan.lagoon.nc/pub/CPAN/
  http://cpan.nautile.nc/CPAN/
  ftp://cpan.nautile.nc/CPAN/
.Ve

- New Zealand
Item "New Zealand"
.Vb 7
  ftp://ftp.auckland.ac.nz/pub/perl/CPAN/
  http://cpan.catalyst.net.nz/CPAN/
  ftp://cpan.catalyst.net.nz/pub/CPAN/
  http://cpan.inspire.net.nz/
  ftp://cpan.inspire.net.nz/cpan/
  http://mirror.webtastix.net/CPAN/
  ftp://mirror.webtastix.net/CPAN/
.Ve

### South America

Subsection "South America"

- Argentina
Item "Argentina"
.Vb 1
  http://cpan.mmgdesigns.com.ar/
.Ve

- Brazil
Item "Brazil"
.Vb 3
  http://cpan.kinghost.net/
  http://linorg.usp.br/CPAN/
  http://mirror.nbtelecom.com.br/CPAN/
.Ve

- Chile
Item "Chile"
.Vb 2
  http://cpan.dcc.uchile.cl/
  ftp://cpan.dcc.uchile.cl/pub/lang/cpan/
.Ve

### \s-1RSYNC\s0 Mirrors

Subsection "RSYNC Mirrors"
.Vb 10
                rsync://ftp.is.co.za/IS-Mirror/ftp.cpan.org/
                rsync://mirror.ac.za/CPAN/
                rsync://mirror.zol.co.zw/CPAN/
                rsync://mirror.dhakacom.com/CPAN/
                rsync://mirrors.ustc.edu.cn/CPAN/
                rsync://mirrors.xmu.edu.cn/CPAN/
                rsync://kambing.ui.ac.id/CPAN/
                rsync://ftp.jaist.ac.jp/pub/CPAN/
                rsync://mirror.jre655.com/CPAN/
                rsync://ftp.kddilabs.jp/cpan/
                rsync://ftp.nara.wide.ad.jp/cpan/
                rsync://ftp.riken.jp/cpan/
                rsync://mirror.neolabs.kz/CPAN/
                rsync://mirror.qnren.qa/CPAN/
                rsync://ftp.neowiz.com/CPAN/
                rsync://mirror.0x.sg/CPAN/
                rsync://ftp.yzu.edu.tw/pub/CPAN/
                rsync://ftp.ubuntu-tw.org/CPAN/
                rsync://mirrors.digipower.vn/CPAN/
                rsync://cpan.inode.at/CPAN/
                rsync://ftp.byfly.by/CPAN/
                rsync://mirror.datacenter.by/CPAN/
                rsync://ftp.belnet.be/cpan/
                rsync://cpan.mirror.ba/CPAN/
                rsync://mirrors.neterra.net/CPAN/
                rsync://mirrors.netix.net/CPAN/
                rsync://mirror.dkm.cz/cpan/
                rsync://mirrors.nic.cz/CPAN/
                rsync://cpan.mirror.vutbr.cz/cpan/
                rsync://rsync.nic.funet.fi/CPAN/
                rsync://ftp.ciril.fr/pub/cpan/
                rsync://distrib-coffee.ipsl.jussieu.fr/pub/mirrors/cpan/
                rsync://cpan.mirrors.ovh.net/CPAN/
                rsync://mirror.de.leaseweb.net/CPAN/
                rsync://mirror.euserv.net/cpan/
                rsync://ftp-stud.hs-esslingen.de/CPAN/
                rsync://ftp.gwdg.de/pub/languages/perl/CPAN/
                rsync://ftp.hawo.stw.uni-erlangen.de/CPAN/
                rsync://cpan.mirror.iphh.net/CPAN/
                rsync://mirror.netcologne.de/cpan/
                rsync://ftp.halifax.rwth-aachen.de/cpan/
                rsync://ftp.ntua.gr/CPAN/
                rsync://mirror.met.hu/CPAN/
                rsync://ftp.heanet.ie/mirrors/ftp.perl.org/pub/CPAN/
                rsync://rsync.panu.it/CPAN/
                rsync://mirror.as43289.net/CPAN/
                rsync://rsync.cs.uu.nl/CPAN/
                rsync://mirror.nl.leaseweb.net/CPAN/
                rsync://ftp.nluug.nl/CPAN/
                rsync://mirror.transip.net/CPAN/
                rsync://cpan.uib.no/cpan/
                rsync://cpan.vianett.no/CPAN/
                rsync://cpan.perl-hackers.net/CPAN/
                rsync://cpan.perl.pt/cpan/
                rsync://mirrors.m247.ro/CPAN/
                rsync://mirrors.teentelecom.net/CPAN/
                rsync://cpan.webdesk.ru/CPAN/
                rsync://mirror.yandex.ru/mirrors/cpan/
                rsync://mirror.sbb.rs/CPAN/
                rsync://ftp.acc.umu.se/mirror/CPAN/
                rsync://rsync.pirbot.com/ftp/cpan/
                rsync://cpan.ip-connect.vn.ua/CPAN/
                rsync://rsync.mirror.anlx.net/CPAN/
                rsync://mirror.bytemark.co.uk/CPAN/
                rsync://mirror.sax.uk.as61049.net/CPAN/
                rsync://rsync.mirrorservice.org/cpan.perl.org/CPAN/
                rsync://ftp.ticklers.org/CPAN/
                rsync://mirrors.uk2.net/CPAN/
                rsync://CPAN.mirror.rafal.ca/CPAN/
                rsync://mirror.csclub.uwaterloo.ca/CPAN/
                rsync://mirrors.namecheap.com/CPAN/
                rsync://mirrors.syringanetworks.net/CPAN/
                rsync://mirror.team-cymru.org/CPAN/
                rsync://debian.cse.msu.edu/cpan/
                rsync://mirrors-usa.go-parts.com/mirrors/cpan/
                rsync://rsync.hoovism.com/CPAN/
                rsync://mirror.cc.columbia.edu/cpan/
                rsync://noodle.portalus.net/CPAN/
                rsync://mirrors.rit.edu/cpan/
                rsync://mirrors.ibiblio.org/CPAN/
                rsync://cpan.pair.com/CPAN/
                rsync://cpan.cs.utah.edu/CPAN/
                rsync://mirror.cogentco.com/CPAN/
                rsync://mirror.jmu.edu/CPAN/
                rsync://mirror.us.leaseweb.net/CPAN/
                rsync://cpan.mirror.digitalpacific.com.au/cpan/
                rsync://mirror.internode.on.net/cpan/
                rsync://uberglobalmirror.com/cpan/
                rsync://cpan.lagoon.nc/cpan/
                rsync://mirrors.mmgdesigns.com.ar/CPAN/
.Ve

For an up-to-date listing of \s-1CPAN\s0 sites,
see <https://www.cpan.org/SITES> or <ftp://www.cpan.org/SITES>.

## Modules: Creation, Use, and Abuse

Header "Modules: Creation, Use, and Abuse"
(The following section is borrowed directly from Tim Bunce's modules
file, available at your nearest \s-1CPAN\s0 site.)

Perl implements a class using a package, but the presence of a
package doesn't imply the presence of a class.  A package is just a
namespace.  A class is a package that provides subroutines that can be
used as methods.  A method is just a subroutine that expects, as its
first argument, either the name of a package (for \*(L"static\*(R" methods),
or a reference to something (for \*(L"virtual\*(R" methods).

A module is a file that (by convention) provides a class of the same
name (sans the .pm), plus an import method in that class that can be
called to fetch exported symbols.  This module may implement some of
its methods by loading dynamic C or \*(C+ objects, but that should be
totally transparent to the user of the module.  Likewise, the module
might set up an \s-1AUTOLOAD\s0 function to slurp in subroutine definitions on
demand, but this is also transparent.  Only the *.pm* file is required to
exist.  See perlsub, perlobj, and AutoLoader for details about
the \s-1AUTOLOAD\s0 mechanism.

### Guidelines for Module Creation

Subsection "Guidelines for Module Creation"

- \(bu
Do similar modules already exist in some form?
.Sp
If so, please try to reuse the existing modules either in whole or
by inheriting useful features into a new class.  If this is not
practical try to get together with the module authors to work on
extending or enhancing the functionality of the existing modules.
A perfect example is the plethora of packages in perl4 for dealing
with command line options.
.Sp
If you are writing a module to expand an already existing set of
modules, please coordinate with the author of the package.  It
helps if you follow the same naming scheme and module interaction
scheme as the original author.

- \(bu
Try to design the new module to be easy to extend and reuse.
.Sp
Try to \f(CW\*(C`use warnings;\*(C' (or \f(CW\*(C`use warnings qw(...);\*(C').
Remember that you can add \f(CW\*(C`no warnings qw(...);\*(C' to individual blocks
of code that need less warnings.
.Sp
Use blessed references.  Use the two argument form of bless to bless
into the class name given as the first parameter of the constructor,
e.g.,:
.Sp
.Vb 4
 sub new \{
     my $class = shift;
     return bless \{\}, $class;
 \}
.Ve
.Sp
or even this if you'd like it to be used as either a static
or a virtual method.
.Sp
.Vb 5
 sub new \{
     my $self  = shift;
     my $class = ref($self) || $self;
     return bless \{\}, $class;
 \}
.Ve
.Sp
Pass arrays as references so more parameters can be added later
(it's also faster).  Convert functions into methods where
appropriate.  Split large methods into smaller more flexible ones.
Inherit methods from other modules if appropriate.
.Sp
Avoid class name tests like: \f(CW\*(C`die "Invalid" unless ref $ref eq \*(AqFOO\*(Aq\*(C'.
Generally you can delete the \f(CW\*(C`eq \*(AqFOO\*(Aq\*(C' part with no harm at all.
Let the objects look after themselves! Generally, avoid hard-wired
class names as far as possible.
.Sp
Avoid \f(CW\*(C`$r->Class::func()\*(C' where using \f(CW\*(C`@ISA=qw(... Class ...)\*(C' and
\f(CW\*(C`$r->func()\*(C' would work.
.Sp
Use autosplit so little used or newly added functions won't be a
burden to programs that don't use them. Add test functions to
the module after _\|_END_\|_ either using AutoSplit or by saying:
.Sp
.Vb 1
 eval join(\*(Aq\*(Aq,<main::DATA>) || die $@ unless caller();
.Ve
.Sp
Does your module pass the 'empty subclass' test? If you say
\f(CW\*(C`@SUBCLASS::ISA = qw(YOURCLASS);\*(C' your applications should be able
to use \s-1SUBCLASS\s0 in exactly the same way as \s-1YOURCLASS.\s0  For example,
does your application still work if you change:  \f(CW\*(C`$obj = YOURCLASS->new();\*(C'
into: \f(CW\*(C`$obj = SUBCLASS->new();\*(C' ?
.Sp
Avoid keeping any state information in your packages. It makes it
difficult for multiple other packages to use yours. Keep state
information in objects.
.Sp
Always use **-w**.
.Sp
Try to \f(CW\*(C`use strict;\*(C' (or \f(CW\*(C`use strict qw(...);\*(C').
Remember that you can add \f(CW\*(C`no strict qw(...);\*(C' to individual blocks
of code that need less strictness.
.Sp
Always use **-w**.
.Sp
Follow the guidelines in perlstyle.
.Sp
Always use **-w**.

- \(bu
Some simple style guidelines
.Sp
The perlstyle manual supplied with Perl has many helpful points.
.Sp
Coding style is a matter of personal taste. Many people evolve their
style over several years as they learn what helps them write and
maintain good code.  Here's one set of assorted suggestions that
seem to be widely used by experienced developers:
.Sp
Use underscores to separate words.  It is generally easier to read
\f(CW$var_names_like_this than \f(CW$VarNamesLikeThis, especially for
non-native speakers of English. It's also a simple rule that works
consistently with \s-1VAR_NAMES_LIKE_THIS.\s0
.Sp
Package/Module names are an exception to this rule. Perl informally
reserves lowercase module names for 'pragma' modules like integer
and strict. Other modules normally begin with a capital letter and
use mixed case with no underscores (need to be short and portable).
.Sp
You may find it helpful to use letter case to indicate the scope
or nature of a variable. For example:
.Sp
.Vb 3
 $ALL_CAPS_HERE   constants only (beware clashes with Perl vars)
 $Some_Caps_Here  package-wide global/static
 $no_caps_here    function scope my() or local() variables
.Ve
.Sp
Function and method names seem to work best as all lowercase.
e.g., \f(CW\*(C`$obj->as_string()\*(C'.
.Sp
You can use a leading underscore to indicate that a variable or
function should not be used outside the package that defined it.

- \(bu
Select what to export.
.Sp
Do \s-1NOT\s0 export method names!
.Sp
Do \s-1NOT\s0 export anything else by default without a good reason!
.Sp
Exports pollute the namespace of the module user.  If you must
export try to use \f(CW@EXPORT_OK in preference to \f(CW@EXPORT and avoid
short or common names to reduce the risk of name clashes.
.Sp
Generally anything not exported is still accessible from outside the
module using the ModuleName::item_name (or \f(CW\*(C`$blessed_ref->method\*(C')
syntax.  By convention you can use a leading underscore on names to
indicate informally that they are 'internal' and not for public use.
.Sp
(It is actually possible to get private functions by saying:
\f(CW\*(C`my $subref = sub \{ ... \};  &$subref;\*(C'.  But there's no way to call that
directly as a method, because a method must have a name in the symbol
table.)
.Sp
As a general rule, if the module is trying to be object oriented
then export nothing. If it's just a collection of functions then
\f(CW@EXPORT_OK anything but use \f(CW@EXPORT with caution.

- \(bu
Select a name for the module.
.Sp
This name should be as descriptive, accurate, and complete as
possible.  Avoid any risk of ambiguity. Always try to use two or
more whole words.  Generally the name should reflect what is special
about what the module does rather than how it does it.  Please use
nested module names to group informally or categorize a module.
There should be a very good reason for a module not to have a nested name.
Module names should begin with a capital letter.
.Sp
Having 57 modules all called Sort will not make life easy for anyone
(though having 23 called Sort::Quick is only marginally better :-).
Imagine someone trying to install your module alongside many others.
.Sp
If you are developing a suite of related modules/classes it's good
practice to use nested classes with a common prefix as this will
avoid namespace clashes. For example: Xyz::Control, Xyz::View,
Xyz::Model etc. Use the modules in this list as a naming guide.
.Sp
If adding a new module to a set, follow the original author's
standards for naming modules and the interface to methods in
those modules.
.Sp
If developing modules for private internal or project specific use,
that will never be released to the public, then you should ensure
that their names will not clash with any future public module. You
can do this either by using the reserved Local::* category or by
using a category name that includes an underscore like Foo_Corp::*.
.Sp
To be portable each component of a module name should be limited to
11 characters. If it might be used on MS-DOS then try to ensure each is
unique in the first 8 characters. Nested modules make this easier.
.Sp
For additional guidance on the naming of modules, please consult:
.Sp
.Vb 1
    https://pause.perl.org/pause/query?ACTION=pause_namingmodules
.Ve
.Sp
or send mail to the <module-authors@perl.org> mailing list.

- \(bu
Have you got it right?
.Sp
How do you know that you've made the right decisions? Have you
picked an interface design that will cause problems later? Have
you picked the most appropriate name? Do you have any questions?
.Sp
The best way to know for sure, and pick up many helpful suggestions,
is to ask someone who knows. The <module-authors@perl.org> mailing list
is useful for this purpose; it's also accessible via news interface as
perl.module-authors at nntp.perl.org.
.Sp
All you need to do is post a short summary of the module, its
purpose and interfaces. A few lines on each of the main methods is
probably enough. (If you post the whole module it might be ignored
by busy people - generally the very people you want to read it!)
.Sp
Don't worry about posting if you can't say when the module will be
ready - just say so in the message. It might be worth inviting
others to help you, they may be able to complete it for you!

- \(bu
\s-1README\s0 and other Additional Files.
.Sp
It's well known that software developers usually fully document the
software they write. If, however, the world is in urgent need of
your software and there is not enough time to write the full
documentation please at least provide a \s-1README\s0 file containing:

> 
- \(bu
A description of the module/package/extension etc.

- \(bu
A copyright notice - see below.

- \(bu
Prerequisites - what else you may need to have.

- \(bu
How to build it - possible changes to Makefile.PL etc.

- \(bu
How to install it.

- \(bu
Recent changes in this release, especially incompatibilities

- \(bu
Changes / enhancements you plan to make in the future.



> .Sp
If the \s-1README\s0 file seems to be getting too large you may wish to
split out some of the sections into separate files: \s-1INSTALL,\s0
Copying, ToDo etc.

- \(bu
Adding a Copyright Notice.
.Sp
How you choose to license your work is a personal decision.
The general mechanism is to assert your Copyright and then make
a declaration of how others may copy/use/modify your work.
.Sp
Perl, for example, is supplied with two types of licence: The \s-1GNU GPL\s0
and The Artistic Licence (see the files \s-1README,\s0 Copying, and Artistic,
or perlgpl and perlartistic).  Larry has good reasons for \s-1NOT\s0
just using the \s-1GNU GPL.\s0
.Sp
My personal recommendation, out of respect for Larry, Perl, and the
Perl community at large is to state something simply like:
.Sp
.Vb 3
 Copyright (c) 1995 Your Name. All rights reserved.
 This program is free software; you can redistribute it and/or
 modify it under the same terms as Perl itself.
.Ve
.Sp
This statement should at least appear in the \s-1README\s0 file. You may
also wish to include it in a Copying file and your source files.
Remember to include the other words in addition to the Copyright.

- \(bu
Give the module a version/issue/release number.
.Sp
To be fully compatible with the Exporter and MakeMaker modules you
should store your module's version number in a non-my package
variable called \f(CW$VERSION.  This should be a positive floating point
number with at least two digits after the decimal (i.e., hundredths,
e.g, \f(CW\*(C`$VERSION = "0.01"\*(C').  Don't use a \*(L"1.3.2\*(R" style version.
See Exporter for details.
.Sp
It may be handy to add a function or method to retrieve the number.
Use the number in announcements and archive file names when
releasing the module (ModuleName-1.02.tar.Z).
See perldoc ExtUtils::MakeMaker.pm for details.

- \(bu
How to release and distribute a module.
.Sp
If possible, register the module with \s-1CPAN.\s0 Follow the instructions
and links on:
.Sp
.Vb 1
   https://www.cpan.org/modules/04pause.html
.Ve
.Sp
and upload to:
.Sp
.Vb 1
   https://pause.perl.org/
.Ve
.Sp
and notify <modules@perl.org>. This will allow anyone to install
your module using the \f(CW\*(C`cpan\*(C' tool distributed with Perl.
.Sp
By using the \s-1WWW\s0 interface you can ask the Upload Server to mirror
your modules from your ftp or \s-1WWW\s0 site into your own directory on
\s-1CPAN\s0!

- \(bu
Take care when changing a released module.
.Sp
Always strive to remain compatible with previous released versions.
Otherwise try to add a mechanism to revert to the
old behavior if people rely on it.  Document incompatible changes.



> 


### Guidelines for Converting Perl 4 Library Scripts into Modules

Subsection "Guidelines for Converting Perl 4 Library Scripts into Modules"

- \(bu
There is no requirement to convert anything.
.Sp
If it ain't broke, don't fix it! Perl 4 library scripts should
continue to work with no problems. You may need to make some minor
changes (like escaping non-array @'s in double quoted strings) but
there is no need to convert a .pl file into a Module for just that.

- \(bu
Consider the implications.
.Sp
All Perl applications that make use of the script will need to
be changed (slightly) if the script is converted into a module.  Is
it worth it unless you plan to make other changes at the same time?

- \(bu
Make the most of the opportunity.
.Sp
If you are going to convert the script to a module you can use the
opportunity to redesign the interface.  The guidelines for module
creation above include many of the issues you should consider.

- \(bu
The pl2pm utility will get you started.
.Sp
This utility will read *.pl files (given as parameters) and write
corresponding *.pm files. The pl2pm utilities does the following:

> 
- \(bu
Adds the standard Module prologue lines

- \(bu
Converts package specifiers from ' to ::

- \(bu
Converts die(...) to croak(...)

- \(bu
Several other minor changes



> .Sp
Being a mechanical process pl2pm is not bullet proof. The converted
code will need careful checking, especially any package statements.
Don't delete the original .pl file till the new .pm one works!



### Guidelines for Reusing Application Code

Subsection "Guidelines for Reusing Application Code"

- \(bu
Complete applications rarely belong in the Perl Module Library.

- \(bu
Many applications contain some Perl code that could be reused.
.Sp
Help save the world! Share your code in a form that makes it easy
to reuse.

- \(bu
Break-out the reusable code into one or more separate module files.

- \(bu
Take the opportunity to reconsider and redesign the interfaces.

- \(bu
In some cases the 'application' can then be reduced to a small
.Sp
fragment of code built on top of the reusable modules. In these cases
the application could invoked as:
.Sp
.Vb 3
     % perl -e \*(Aquse Module::Name; method(@ARGV)\*(Aq ...
or
     % perl -mModule::Name ...    (in perl5.002 or higher)
.Ve

## NOTE

Header "NOTE"
Perl does not enforce private and public parts of its modules as you may
have been used to in other languages like \*(C+, Ada, or Modula-17.  Perl
doesn't have an infatuation with enforced privacy.  It would prefer
that you stayed out of its living room because you weren't invited, not
because it has a shotgun.

The module and its user have a contract, part of which is common law,
and part of which is \*(L"written\*(R".  Part of the common law contract is
that a module doesn't pollute any namespace it wasn't asked to.  The
written contract for the module (A.K.A. documentation) may make other
provisions.  But then you know when you \f(CW\*(C`use RedefineTheWorld\*(C' that
you're redefining the world and willing to take the consequences.
