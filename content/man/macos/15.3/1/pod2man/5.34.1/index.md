+++
author = "None Specified"
operating_system = "macos"
operating_system_version = "15.3"
manpage_format = "troff"
detected_package_version = "5.34.1"
manpage_name = "pod2man"
title = "pod2man(1)"
manpage_section = "1"
date = "2024-12-14"
keywords = ["header", "see", "also", "pod", "man", "simple", "fbman", "1", "fbnroff", "fbperlpod", "fbpodchecker", "fbperlpodstyle", "fbtroff", "7", "the", "page", "documenting", "an", "macro", "set", "may", "be", "5", "instead", "of", "on", "your", "system", "current", "version", "this", "script", "is", "always", "available", "from", "its", "web", "site", "at", "https", "www", "eyrie", "org", "eagle", "software", "podlators", "it", "part", "perl", "core", "distribution", "as", "6"]
description = "pod2man is a front-end for Pod::Man, using it to generate *roff input from s-1PODs0 source.  The resulting *roff code is suitable for display on a terminal using nroff|(1), normally via man|(1), or printing using troff|(1). input is the file to rea..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "POD2MAN 1"
POD2MAN 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

pod2man - Convert POD data to formatted *roff input

## SYNOPSIS

Header "SYNOPSIS"
pod2man [**--center**=*string*] [**--date**=*string*] [**--errors**=*style*]
    [**--fixed**=*font*] [**--fixedbold**=*font*] [**--fixeditalic**=*font*]
    [**--fixedbolditalic**=*font*] [**--name**=*name*] [**--nourls**]
    [**--official**] [**--release**=*version*] [**--section**=*manext*]
    [**--quotes**=*quotes*] [**--lquote**=*quote*] [**--rquote**=*quote*]
    [**--stderr**] [**--utf8**] [**--verbose**] [*input* [*output*] ...]

pod2man **--help**

## DESCRIPTION

Header "DESCRIPTION"
**pod2man** is a front-end for Pod::Man, using it to generate *roff input
from \s-1POD\s0 source.  The resulting *roff code is suitable for display on a
terminal using **nroff**\|(1), normally via **man**\|(1), or printing using **troff**\|(1).

*input* is the file to read for \s-1POD\s0 source (the \s-1POD\s0 can be embedded in
code).  If *input* isn't given, it defaults to \f(CW\*(C`STDIN\*(C'.  *output*, if
given, is the file to which to write the formatted output.  If *output*
isn't given, the formatted output is written to \f(CW\*(C`STDOUT\*(C'.  Several \s-1POD\s0
files can be processed in the same **pod2man** invocation (saving module
load and compile times) by providing multiple pairs of *input* and
*output* files on the command line.

**--section**, **--release**, **--center**, **--date**, and **--official** can
be used to set the headers and footers to use; if not given, Pod::Man will
assume various defaults.  See below or Pod::Man for details.

**pod2man** assumes that your *roff formatters have a fixed-width font
named \f(CW\*(C`CW\*(C'.  If yours is called something else (like \f(CW\*(C`CR\*(C'), use
**--fixed** to specify it.  This generally only matters for troff output
for printing.  Similarly, you can set the fonts used for bold, italic, and
bold italic fixed-width output.

Besides the obvious pod conversions, Pod::Man, and therefore pod2man also
takes care of formatting **func()**, func(n), and simple variable references
like \f(CW$foo or \f(CW@bar so you don't have to use code escapes for them; complex
expressions like \f(CW$fred\{\*(Aqstuff\*(Aq\} will still need to be escaped, though.
It also translates dashes that aren't used as hyphens into en dashes, makes
long dashes\*(--like this\*(--into proper em dashes, fixes \*(L"paired quotes,\*(R" and
takes care of several other troff-specific tweaks.  See Pod::Man for
complete information.

## OPTIONS

Header "OPTIONS"

- \fB-c \fIstring, \fB--center=\fIstring
Item "-c string, --center=string"
Sets the centered page header for the \f(CW\*(C`.TH\*(C' macro to *string*.  The
default is \*(L"User Contributed Perl Documentation\*(R", but also see
**--official** below.

- \fB-d \fIstring, \fB--date=\fIstring
Item "-d string, --date=string"
Set the left-hand footer string for the \f(CW\*(C`.TH\*(C' macro to *string*.  By
default, the modification date of the input file will be used, or the
current date if input comes from \f(CW\*(C`STDIN\*(C', and will be based on \s-1UTC\s0 (so
that the output will be reproducible regardless of local time zone).

- \fB--errors=\fIstyle
Item "--errors=style"
Set the error handling style.  \f(CW\*(C`die\*(C' says to throw an exception on any
\s-1POD\s0 formatting error.  \f(CW\*(C`stderr\*(C' says to report errors on standard error,
but not to throw an exception.  \f(CW\*(C`pod\*(C' says to include a \s-1POD ERRORS\s0
section in the resulting documentation summarizing the errors.  \f(CW\*(C`none\*(C'
ignores \s-1POD\s0 errors entirely, as much as possible.
.Sp
The default is \f(CW\*(C`die\*(C'.

- \fB--fixed=\fIfont
Item "--fixed=font"
The fixed-width font to use for verbatim text and code.  Defaults to
\f(CW\*(C`CW\*(C'.  Some systems may want \f(CW\*(C`CR\*(C' instead.  Only matters for **troff**\|(1)
output.

- \fB--fixedbold=\fIfont
Item "--fixedbold=font"
Bold version of the fixed-width font.  Defaults to \f(CW\*(C`CB\*(C'.  Only matters
for **troff**\|(1) output.

- \fB--fixeditalic=\fIfont
Item "--fixeditalic=font"
Italic version of the fixed-width font (actually, something of a misnomer,
since most fixed-width fonts only have an oblique version, not an italic
version).  Defaults to \f(CW\*(C`CI\*(C'.  Only matters for **troff**\|(1) output.

- \fB--fixedbolditalic=\fIfont
Item "--fixedbolditalic=font"
Bold italic (probably actually oblique) version of the fixed-width font.
Pod::Man doesn't assume you have this, and defaults to \f(CW\*(C`CB\*(C'.  Some
systems (such as Solaris) have this font available as \f(CW\*(C`CX\*(C'.  Only matters
for **troff**\|(1) output.

- \fB-h, \fB--help
Item "-h, --help"
Print out usage information.

- \fB-l, \fB--lax
Item "-l, --lax"
No longer used.  **pod2man** used to check its input for validity as a
manual page, but this should now be done by **podchecker**\|(1) instead.
Accepted for backward compatibility; this option no longer does anything.

- \fB--lquote=\fIquote
Item "--lquote=quote"
0

- \fB--rquote=\fIquote
Item "--rquote=quote"
.PD
Sets the quote marks used to surround C<> text.  **--lquote** sets the
left quote mark and **--rquote** sets the right quote mark.  Either may also
be set to the special value \f(CW\*(C`none\*(C', in which case no quote mark is added
on that side of C<> text (but the font is still changed for troff
output).
.Sp
Also see the **--quotes** option, which can be used to set both quotes at once.
If both **--quotes** and one of the other options is set, **--lquote** or
**--rquote** overrides **--quotes**.

- \fB-n \fIname, \fB--name=\fIname
Item "-n name, --name=name"
Set the name of the manual page for the \f(CW\*(C`.TH\*(C' macro to *name*.  Without
this option, the manual name is set to the uppercased base name of the
file being converted unless the manual section is 3, in which case the
path is parsed to see if it is a Perl module path.  If it is, a path like
\f(CW\*(C`.../lib/Pod/Man.pm\*(C' is converted into a name like \f(CW\*(C`Pod::Man\*(C'.  This
option, if given, overrides any automatic determination of the name.
.Sp
Although one does not have to follow this convention, be aware that the
convention for \s-1UNIX\s0 man pages for commands is for the man page title to be
in all-uppercase, even if the command isn't.
.Sp
This option is probably not useful when converting multiple \s-1POD\s0 files at
once.
.Sp
When converting \s-1POD\s0 source from standard input, the name will be set to
\f(CW\*(C`STDIN\*(C' if this option is not provided.  Providing this option is strongly
recommended to set a meaningful manual page name.

- \fB--nourls
Item "--nourls"
Normally, L<> formatting codes with a \s-1URL\s0 but anchor text are formatted
to show both the anchor text and the \s-1URL.\s0  In other words:
.Sp
.Vb 1
    L<foo|http://example.com/>
.Ve
.Sp
is formatted as:
.Sp
.Vb 1
    foo <http://example.com/>
.Ve
.Sp
This flag, if given, suppresses the \s-1URL\s0 when anchor text is given, so this
example would be formatted as just \f(CW\*(C`foo\*(C'.  This can produce less
cluttered output in cases where the URLs are not particularly important.

- \fB-o, \fB--official
Item "-o, --official"
Set the default header to indicate that this page is part of the standard
Perl release, if **--center** is not also given.

- \fB-q \fIquotes, \fB--quotes=\fIquotes
Item "-q quotes, --quotes=quotes"
Sets the quote marks used to surround C<> text to *quotes*.  If
*quotes* is a single character, it is used as both the left and right
quote.  Otherwise, it is split in half, and the first half of the string
is used as the left quote and the second is used as the right quote.
.Sp
*quotes* may also be set to the special value \f(CW\*(C`none\*(C', in which case no
quote marks are added around C<> text (but the font is still changed for
troff output).
.Sp
Also see the **--lquote** and **--rquote** options, which can be used to set the
left and right quotes independently.  If both **--quotes** and one of the other
options is set, **--lquote** or **--rquote** overrides **--quotes**.

- \fB-r \fIversion, \fB--release=\fIversion
Item "-r version, --release=version"
Set the centered footer for the \f(CW\*(C`.TH\*(C' macro to *version*.  By default,
this is set to the version of Perl you run **pod2man** under.  Setting this
to the empty string will cause some *roff implementations to use the
system default value.
.Sp
Note that some system \f(CW\*(C`an\*(C' macro sets assume that the centered footer
will be a modification date and will prepend something like \*(L"Last
modified: \*(R".  If this is the case for your target system, you may want to
set **--release** to the last modified date and **--date** to the version
number.

- \fB-s \fIstring, \fB--section=\fIstring
Item "-s string, --section=string"
Set the section for the \f(CW\*(C`.TH\*(C' macro.  The standard section numbering
convention is to use 1 for user commands, 2 for system calls, 3 for
functions, 4 for devices, 5 for file formats, 6 for games, 7 for
miscellaneous information, and 8 for administrator commands.  There is a lot
of variation here, however; some systems (like Solaris) use 4 for file
formats, 5 for miscellaneous information, and 7 for devices.  Still others
use 1m instead of 8, or some mix of both.  About the only section numbers
that are reliably consistent are 1, 2, and 3.
.Sp
By default, section 1 will be used unless the file ends in \f(CW\*(C`.pm\*(C', in
which case section 3 will be selected.

- \fB--stderr
Item "--stderr"
By default, **pod2man** dies if any errors are detected in the \s-1POD\s0 input.
If **--stderr** is given and no **--errors** flag is present, errors are
sent to standard error, but **pod2man** does not abort.  This is equivalent
to \f(CW\*(C`--errors=stderr\*(C' and is supported for backward compatibility.

- \fB-u, \fB--utf8
Item "-u, --utf8"
By default, **pod2man** produces the most conservative possible *roff
output to try to ensure that it will work with as many different *roff
implementations as possible.  Many *roff implementations cannot handle
non-ASCII characters, so this means all non-ASCII characters are converted
either to a *roff escape sequence that tries to create a properly accented
character (at least for troff output) or to \f(CW\*(C`X\*(C'.
.Sp
This option says to instead output literal \s-1UTF-8\s0 characters.  If your
*roff implementation can handle it, this is the best output format to use
and avoids corruption of documents containing non-ASCII characters.
However, be warned that *roff source with literal \s-1UTF-8\s0 characters is not
supported by many implementations and may even result in segfaults and
other bad behavior.
.Sp
Be aware that, when using this option, the input encoding of your \s-1POD\s0
source should be properly declared unless it's US-ASCII.  Pod::Simple will
attempt to guess the encoding and may be successful if it's Latin-1 or
\s-1UTF-8,\s0 but it will warn, which by default results in a **pod2man** failure.
Use the \f(CW\*(C`=encoding\*(C' command to declare the encoding.  See **perlpod**\|(1)
for more information.

- \fB-v, \fB--verbose
Item "-v, --verbose"
Print out the name of each output file as it is being generated.

## EXIT STATUS

Header "EXIT STATUS"
As long as all documents processed result in some output, even if that
output includes errata (a \f(CW\*(C`POD ERRORS\*(C' section generated with
\f(CW\*(C`--errors=pod\*(C'), **pod2man** will exit with status 0.  If any of the
documents being processed do not result in an output document, **pod2man**
will exit with status 1.  If there are syntax errors in a \s-1POD\s0 document
being processed and the error handling style is set to the default of
\f(CW\*(C`die\*(C', **pod2man** will abort immediately with exit status 255.

## DIAGNOSTICS

Header "DIAGNOSTICS"
If **pod2man** fails with errors, see Pod::Man and Pod::Simple for
information about what those errors might mean.

## EXAMPLES

Header "EXAMPLES"
.Vb 3
    pod2man program > program.1
    pod2man SomeModule.pm /usr/perl/man/man3/SomeModule.3
    pod2man --section=7 note.pod > note.7
.Ve

If you would like to print out a lot of man page continuously, you probably
want to set the C and D registers to set contiguous page numbering and
even/odd paging, at least on some versions of **man**\|(7).

.Vb 1
    troff -man -rC1 -rD1 perl.1 perldata.1 perlsyn.1 ...
.Ve

To get index entries on \f(CW\*(C`STDERR\*(C', turn on the F register, as in:

.Vb 1
    troff -man -rF1 perl.1
.Ve

The indexing merely outputs messages via \f(CW\*(C`.tm\*(C' for each major page,
section, subsection, item, and any \f(CW\*(C`X<>\*(C' directives.  See
Pod::Man for more details.

## BUGS

Header "BUGS"
Lots of this documentation is duplicated from Pod::Man.

## AUTHOR

Header "AUTHOR"
Russ Allbery <rra@cpan.org>, based *very* heavily on the original
**pod2man** by Larry Wall and Tom Christiansen.

## COPYRIGHT AND LICENSE

Header "COPYRIGHT AND LICENSE"
Copyright 1999-2001, 2004, 2006, 2008, 2010, 2012-2019 Russ Allbery
<rra@cpan.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

## SEE ALSO

Header "SEE ALSO"
Pod::Man, Pod::Simple, **man**\|(1), **nroff**\|(1), **perlpod**\|(1),
**podchecker**\|(1), **perlpodstyle**\|(1), **troff**\|(1), **man**\|(7)

The man page documenting the an macro set may be **man**\|(5) instead of
**man**\|(7) on your system.

The current version of this script is always available from its web site at
<https://www.eyrie.org/~eagle/software/podlators/>.  It is also part of the
Perl core distribution as of 5.6.0.
