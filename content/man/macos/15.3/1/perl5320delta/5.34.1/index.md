+++
manpage_name = "perl5320delta"
detected_package_version = "5.34.1"
operating_system = "macos"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
title = "perl5320delta(1)"
manpage_section = "1"
operating_system_version = "15.3"
description = "This document describes differences between the 5.30.0 release and the 5.32.0 release. If you are upgrading from an earlier release such as 5.28.0, first read perl5300delta, which describes differences between 5.28.0 and 5.30.0. A new experimental..."
date = "2022-02-19"
manpage_format = "troff"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5320DELTA 1"
PERL5320DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5320delta - what is new for perl v5.32.0

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.30.0 release and the 5.32.0
release.

If you are upgrading from an earlier release such as 5.28.0, first read
perl5300delta, which describes differences between 5.28.0 and 5.30.0.

## Core Enhancements

Header "Core Enhancements"

### The isa Operator

Subsection "The isa Operator"
A new experimental infix operator called \f(CW\*(C`isa\*(C' tests whether a given object
is an instance of a given class or a class derived from it:

.Vb 1
    if( $obj isa Package::Name ) \{ ... \}
.Ve

For more detail see \*(L"Class Instance Operator\*(R" in perlop.

### Unicode 13.0 is supported

Subsection "Unicode 13.0 is supported"
See <https://www.unicode.org/versions/Unicode13.0.0/> for details.

### Chained comparisons capability

Subsection "Chained comparisons capability"
Some comparison operators, as their associativity, *chain* with some
operators of the same precedence (but never with operators of different
precedence).

.Vb 1
    if ( $x < $y <= $z ) \{...\}
.Ve

behaves exactly like:

.Vb 1
    if ( $x < $y && $y <= $z ) \{...\}
.Ve

(assuming that \f(CW"$y" is as simple a scalar as it looks.)

You can read more about this in perlop under
\*(L"Operator Precedence and Associativity\*(R" in perlop.
.ie n .SS "New Unicode properties ""Identifier_Status"" and ""Identifier_Type"" supported"
.el .SS "New Unicode properties \f(CWIdentifier_Status and \f(CWIdentifier_Type supported"
Subsection "New Unicode properties Identifier_Status and Identifier_Type supported"
Unicode has revised its regular expression requirements:
<https://www.unicode.org/reports/tr18/tr18-21.html>.
As part of that they are wanting more properties to be exposed, ones
that aren't part of the strict \s-1UCD\s0 (Unicode character database). These
two are used for examining inputs for security purposes. Details on
their usage is at <https://www.unicode.org/reports/tr39/>.
.ie n .SS "It is now possible to write ""qr/\\p\{Name=...\}/"", or ""qr!\\p\{na=/(SMILING|GRINNING) FACE/\}!"""
.el .SS "It is now possible to write \f(CWqr/\\p\{Name=...\}/, or \f(CWqr!\\p\{na=/(SMILING|GRINNING) FACE/\}!"
Subsection "It is now possible to write qr/p\{Name=...\}/, or qr!p\{na=/(SMILING|GRINNING) FACE/\}!"
The Unicode Name property is now accessible in regular expression
patterns, as an alternative to \f(CW\*(C`\\N\{...\}\*(C'.
A comparison of the two methods is given in
\*(L"Comparison of \\N\{...\} and \\p\{name=...\}\*(R" in perlunicode.

The second example above shows that wildcard subpatterns are also usable
in this property. See \*(L"Wildcards in Property Values\*(R" in perlunicode.
.ie n .SS "Improvement of ""POSIX::mblen()"", ""mbtowc"", and ""wctomb"""
.el .SS "Improvement of \f(CWPOSIX::mblen(), \f(CWmbtowc, and \f(CWwctomb"
Subsection "Improvement of POSIX::mblen(), mbtowc, and wctomb"
The \f(CW\*(C`POSIX::mblen()\*(C', \f(CW\*(C`mbtowc\*(C', and \f(CW\*(C`wctomb\*(C' functions now
work on shift state locales and are thread-safe on C99 and above
compilers when executed on a platform that has locale thread-safety; the
length parameters are now optional.

These functions are always executed under the current C language locale.
(See perllocale.)  Most locales are stateless, but a few, notably the
very rarely encountered \s-1ISO 2022,\s0 maintain a state between calls to
these functions. Previously the state was cleared on every call, but
now the state is not reset unless the appropriate parameter is \f(CW\*(C`undef\*(C'.

On threaded perls, the C99 functions **mbrlen**\|(3), **mbrtowc**\|(3), and
**wcrtomb**\|(3), when available, are substituted for the plain functions.
This makes these functions thread-safe when executing on a locale
thread-safe platform.

The string length parameters in \f(CW\*(C`mblen\*(C' and \f(CW\*(C`mbtowc\*(C' are now optional;
useful only if you wish to restrict the length parsed in the source
string to less than the actual length.

### Alpha assertions are no longer experimental

Subsection "Alpha assertions are no longer experimental"
See \*(L"(*pla:pattern)\*(R" in perlre, \*(L"(*plb:pattern)\*(R" in perlre,
\*(L"(*nla:pattern)\*(R" in perlre>, and \*(L"(*nlb:pattern)\*(R" in perlre.
Use of these no longer generates a warning; existing code that disables
the warning category \f(CW\*(C`experimental::alpha_assertions\*(C' will continue to work
without any changes needed. Enabling the category has no effect.

### Script runs are no longer experimental

Subsection "Script runs are no longer experimental"
See \*(L"Script Runs\*(R" in perlre. Use of these no longer generates a warning;
existing code that disables the warning category
\f(CW\*(C`experimental::script_run\*(C' will continue to work without any
changes needed. Enabling the category has no effect.

### Feature checks are now faster

Subsection "Feature checks are now faster"
Previously feature checks in the parser required a hash lookup when
features were set outside of a feature bundle, this has been optimized
to a bit mask check. [\s-1GH\s0 #17229 <https://github.com/Perl/perl5/issues/17229>]

### Perl is now developed on GitHub

Subsection "Perl is now developed on GitHub"
Perl is now developed on GitHub. You can find us at
<https://github.com/Perl/perl5>.

Non-security bugs should now be reported via GitHub. Security issues should
continue to be reported as documented in perlsec.

### Compiled patterns can now be dumped before optimization

Subsection "Compiled patterns can now be dumped before optimization"
This is primarily useful for tracking down bugs in the regular
expression compiler. This dump happens on \f(CW\*(C`-DDEBUGGING\*(C' perls, if you
specify \f(CW\*(C`-Drv\*(C' on the command line; or on any perl if the pattern is
compiled within the scope of \f(CW\*(C`use\ re\ qw(Debug\ DUMP_PRE_OPTIMIZE)\*(C' or
\f(CW\*(C`use\ re\ qw(Debug\ COMPILE\ EXTRA)\*(C'. (All but the second case display
other information as well.)

## Security

Header "Security"

### [\s-1CVE-2020-10543\s0] Buffer overflow caused by a crafted regular expression

Subsection "[CVE-2020-10543] Buffer overflow caused by a crafted regular expression"
A signed \f(CW\*(C`size_t\*(C' integer overflow in the storage space calculations for
nested regular expression quantifiers could cause a heap buffer overflow in
Perl's regular expression compiler that overwrites memory allocated after the
regular expression storage space with attacker supplied data.

The target system needs a sufficient amount of memory to allocate partial
expansions of the nested quantifiers prior to the overflow occurring.  This
requirement is unlikely to be met on 64-bit systems.

Discovered by: ManhND of The Tarantula Team, VinCSS (a member of Vingroup).

### [\s-1CVE-2020-10878\s0] Integer overflow via malformed bytecode produced by a crafted regular expression

Subsection "[CVE-2020-10878] Integer overflow via malformed bytecode produced by a crafted regular expression"
Integer overflows in the calculation of offsets between instructions for the
regular expression engine could cause corruption of the intermediate language
state of a compiled regular expression.  An attacker could abuse this behaviour
to insert instructions into the compiled form of a Perl regular expression.

Discovered by: Hugo van der Sanden and Slaven Rezic.

### [\s-1CVE-2020-12723\s0] Buffer overflow caused by a crafted regular expression

Subsection "[CVE-2020-12723] Buffer overflow caused by a crafted regular expression"
Recursive calls to \f(CW\*(C`S_study_chunk()\*(C' by Perl's regular expression compiler to
optimize the intermediate language representation of a regular expression could
cause corruption of the intermediate language state of a compiled regular
expression.

Discovered by: Sergey Aleynikov.

### Additional Note

Subsection "Additional Note"
An application written in Perl would only be vulnerable to any of the above
flaws if it evaluates regular expressions supplied by the attacker.  Evaluating
regular expressions in this fashion is known to be dangerous since the regular
expression engine does not protect against denial of service attacks in this
usage scenario.

## Incompatible Changes

Header "Incompatible Changes"

### Certain pattern matching features are now prohibited in compiling Unicode property value wildcard subpatterns

Subsection "Certain pattern matching features are now prohibited in compiling Unicode property value wildcard subpatterns"
These few features are either inappropriate or interfere with the
algorithm used to accomplish this task. The complete list is in
\*(L"Wildcards in Property Values\*(R" in perlunicode.
.ie n .SS "Unused functions ""POSIX::mbstowcs"" and ""POSIX::wcstombs"" are removed"
.el .SS "Unused functions \f(CWPOSIX::mbstowcs and \f(CWPOSIX::wcstombs are removed"
Subsection "Unused functions POSIX::mbstowcs and POSIX::wcstombs are removed"
These functions could never have worked due to a defective interface
specification. There is clearly no demand for them, given that no one
has ever complained in the many years the functions were claimed to be
available, hence so-called \*(L"support\*(R" for them is now dropped.
.ie n .SS "A bug fix for ""(?[...])"" may have caused some patterns to no longer compile"
.el .SS "A bug fix for \f(CW(?[...]) may have caused some patterns to no longer compile"
Subsection "A bug fix for (?[...]) may have caused some patterns to no longer compile"
See \*(L"Selected Bug Fixes\*(R". The heuristics previously used may have let
some constructs compile (perhaps not with the programmer's intended
effect) that should have been errors. None are known, but it is
possible that some erroneous constructs no longer compile.
.ie n .SS """\\p\{\f(CIuser-defined\}"" properties now always override official Unicode ones"
.el .SS "\f(CW\\p\{\f(CIuser-defined\f(CW\} properties now always override official Unicode ones"
Subsection "p\{user-defined\} properties now always override official Unicode ones"
Previously, if and only if a user-defined property was declared prior to
the compilation of the regular expression pattern that contains it, its
definition was used instead of any official Unicode property with the
same name. Now, it always overrides the official property. This
change could break existing code that relied (likely unwittingly) on the
previous behavior. Without this fix, if Unicode released a new version
with a new property that happens to have the same name as the one you
had long been using, your program would break when you upgraded to a
perl that used that new Unicode version. See \*(L"User-Defined
Character Properties\*(R" in perlunicode. [\s-1GH\s0 #17205 <https://github.com/Perl/perl5/issues/17205>]

### Modifiable variables are no longer permitted in constants

Subsection "Modifiable variables are no longer permitted in constants"
Code like:

.Vb 2
    my $var;
    $sub = sub () \{ $var \};
.Ve

where \f(CW$var is referenced elsewhere in some sort of modifiable context now
produces an exception when the sub is defined.

This error can be avoided by adding a return to the sub definition:

.Vb 1
    $sub = sub () \{ return $var \};
.Ve

This has been deprecated since Perl 5.22.
[\s-1GH\s0 #17020] <https://github.com/Perl/perl5/issues/17020>
.ie n .SS "Use of ""vec"" on strings with code points above 0xFF is forbidden"
.el .SS "Use of \f(CWvec on strings with code points above 0xFF is forbidden"
Subsection "Use of vec on strings with code points above 0xFF is forbidden"
Such strings are represented internally in \s-1UTF-8,\s0 and \f(CW\*(C`vec\*(C' is a
bit-oriented operation that will likely give unexpected results on those
strings. This was deprecated in perl 5.28.0.

### Use of code points over 0xFF in string bitwise operators

Subsection "Use of code points over 0xFF in string bitwise operators"
Some uses of these were already illegal after a previous deprecation
cycle. The remaining uses are now prohibited, having been deprecated in perl
5.28.0. See perldeprecation.
.ie n .SS """Sys::Hostname::hostname()"" does not accept arguments"
.el .SS "\f(CWSys::Hostname::hostname() does not accept arguments"
Subsection "Sys::Hostname::hostname() does not accept arguments"
This usage was deprecated in perl 5.28.0 and is now fatal.
.ie n .SS "Plain ""0"" string now treated as a number for range operator"
.el .SS "Plain ``0'' string now treated as a number for range operator"
Subsection "Plain 0 string now treated as a number for range operator"
Previously a range \f(CW"0" .. "-1" would produce a range of numeric
strings from \*(L"0\*(R" through \*(L"99\*(R"; this now produces an empty list, just
as \f(CW\*(C`0 .. -1\*(C' does. This also means that \f(CW"0" .. "9" now produces a
list of integers, where previously it would produce a list of strings.

This was due to a special case that treated strings starting with \*(L"0\*(R"
as strings so ranges like \f(CW"00" .. "03" produced \f(CW"00", "01", "02", "03",
but didn't specially handle the string \f(CW"0".
[\s-1GH\s0 #16770] <https://github.com/Perl/perl5/issues/16770>
.ie n .SS """\\K"" now disallowed in look-ahead and look-behind assertions"
.el .SS "\f(CW\\K now disallowed in look-ahead and look-behind assertions"
Subsection "K now disallowed in look-ahead and look-behind assertions"
This was disallowed because it causes unexpected behaviour, and no-one
could define what the desired behaviour should be.
[\s-1GH\s0 #14638] <https://github.com/Perl/perl5/issues/14638>

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
\f(CW\*(C`my_strnlen\*(C' has been sped up for systems that don't have their own
\f(CW\*(C`strnlen\*(C' implementation.

- \(bu
\f(CW\*(C`grok_bin_oct_hex\*(C' (and so, \f(CW\*(C`grok_bin\*(C', \f(CW\*(C`grok_oct\*(C', and \f(CW\*(C`grok_hex\*(C')
have been sped up.

- \(bu
\f(CW\*(C`grok_number_flags\*(C' has been sped up.

- \(bu
\f(CW\*(C`sort\*(C' is now noticeably faster in cases such as \f(CW\*(C`sort \{$a <=> $b\}\*(C' or
\f(CW\*(C`sort \{$b <=> $a\}\*(C'. [\s-1GH\s0 #17608 <https://github.com/Perl/perl5/pull/17608>]

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Archive::Tar has been upgraded from version 2.32 to 2.36.

- \(bu
autodie has been upgraded from version 2.29 to 2.32.

- \(bu
B has been upgraded from version 1.76 to 1.80.

- \(bu
B::Deparse has been upgraded from version 1.49 to 1.54.

- \(bu
Benchmark has been upgraded from version 1.22 to 1.23.

- \(bu
charnames has been upgraded from version 1.45 to 1.48.

- \(bu
Class::Struct has been upgraded from version 0.65 to 0.66.

- \(bu
Compress::Raw::Bzip2 has been upgraded from version 2.084 to 2.093.

- \(bu
Compress::Raw::Zlib has been upgraded from version 2.084 to 2.093.

- \(bu
\s-1CPAN\s0 has been upgraded from version 2.22 to 2.27.

- \(bu
DB_File has been upgraded from version 1.843 to 1.853.

- \(bu
Devel::PPPort has been upgraded from version 3.52 to 3.57.
.Sp
The test files generated on Win32 are now identical to when they are
generated on POSIX-like systems.

- \(bu
diagnostics has been upgraded from version 1.36 to 1.37.

- \(bu
Digest::MD5 has been upgraded from version 2.55 to 2.55_01.

- \(bu
Dumpvalue has been upgraded from version 1.18 to 1.21.
.Sp
Previously, when dumping elements of an array and encountering an undefined
value, the string printed would have been \f(CW\*(C`empty array\*(C'. This has been
changed to what was apparently originally intended:  \f(CW\*(C`empty slot\*(C'.

- \(bu
DynaLoader has been upgraded from version 1.45 to 1.47.

- \(bu
Encode has been upgraded from version 3.01 to 3.06.

- \(bu
encoding has been upgraded from version 2.22 to 3.00.

- \(bu
English has been upgraded from version 1.10 to 1.11.

- \(bu
Exporter has been upgraded from version 5.73 to 5.74.

- \(bu
ExtUtils::CBuilder has been upgraded from version 0.280231 to 0.280234.

- \(bu
ExtUtils::MakeMaker has been upgraded from version 7.34 to 7.44.

- \(bu
feature has been upgraded from version 1.54 to 1.58.
.Sp
A new \f(CW\*(C`indirect\*(C' feature has been added, which is enabled by default
but allows turning off indirect object syntax.

- \(bu
File::Find has been upgraded from version 1.36 to 1.37.
.Sp
On Win32, the tests no longer require either a file in the drive root
directory, or a writable root directory.

- \(bu
File::Glob has been upgraded from version 1.32 to 1.33.

- \(bu
File::stat has been upgraded from version 1.08 to 1.09.

- \(bu
Filter::Simple has been upgraded from version 0.95 to 0.96.

- \(bu
Getopt::Long has been upgraded from version 2.5 to 2.51.

- \(bu
Hash::Util has been upgraded from version 0.22 to 0.23.
.Sp
The Synopsis has been updated as the example code stopped working with
newer perls.
[\s-1GH\s0 #17399 <https://github.com/Perl/perl5/issues/17399>]

- \(bu
I18N::Langinfo has been upgraded from version 0.18 to 0.19.

- \(bu
I18N::LangTags has been upgraded from version 0.43 to 0.44.
.Sp
Document the \f(CW\*(C`IGNORE_WIN32_LOCALE\*(C' environment variable.

- \(bu
\s-1IO\s0 has been upgraded from version 1.40 to 1.43.
.Sp
IO::Socket no longer caches a zero protocol value, since this
indicates that the implementation will select a protocol. This means
that on platforms that don't implement \f(CW\*(C`SO_PROTOCOL\*(C' for a given
socket type the protocol method may return \f(CW\*(C`undef\*(C'.
.Sp
The supplied *\s-1TO\s0* is now always honoured on calls to the \f(CW\*(C`send()\*(C'
method. [\s-1GH\s0 #16891] <https://github.com/Perl/perl5/issues/16891>

- \(bu
IO-Compress has been upgraded from version 2.084 to 2.093.

- \(bu
IPC::Cmd has been upgraded from version 1.02 to 1.04.

- \(bu
IPC::Open3 has been upgraded from version 1.20 to 1.21.

- \(bu
\s-1JSON::PP\s0 has been upgraded from version 4.02 to 4.04.

- \(bu
Math::BigInt has been upgraded from version 1.999816 to 1.999818.

- \(bu
Math::BigInt::FastCalc has been upgraded from version 0.5008 to 0.5009.

- \(bu
Module::CoreList has been upgraded from version 5.20190522 to 5.20200620.

- \(bu
Module::Load::Conditional has been upgraded from version 0.68 to 0.70.

- \(bu
Module::Metadata has been upgraded from version 1.000036 to 1.000037.

- \(bu
mro has been upgraded from version 1.22 to 1.23.

- \(bu
Net::Ping has been upgraded from version 2.71 to 2.72.

- \(bu
Opcode has been upgraded from version 1.43 to 1.47.

- \(bu
open has been upgraded from version 1.11 to 1.12.

- \(bu
overload has been upgraded from version 1.30 to 1.31.

- \(bu
parent has been upgraded from version 0.237 to 0.238.

- \(bu
perlfaq has been upgraded from version 5.20190126 to 5.20200523.

- \(bu
PerlIO has been upgraded from version 1.10 to 1.11.

- \(bu
PerlIO::encoding has been upgraded from version 0.27 to 0.28.

- \(bu
PerlIO::via has been upgraded from version 0.17 to 0.18.

- \(bu
Pod::Html has been upgraded from version 1.24 to 1.25.

- \(bu
Pod::Simple has been upgraded from version 3.35 to 3.40.

- \(bu
podlators has been upgraded from version 4.11 to 4.14.

- \(bu
\s-1POSIX\s0 has been upgraded from version 1.88 to 1.94.

- \(bu
re has been upgraded from version 0.37 to 0.40.

- \(bu
Safe has been upgraded from version 2.40 to 2.41.

- \(bu
Scalar::Util has been upgraded from version 1.50 to 1.55.

- \(bu
SelfLoader has been upgraded from version 1.25 to 1.26.

- \(bu
Socket has been upgraded from version 2.027 to 2.029.

- \(bu
Storable has been upgraded from version 3.15 to 3.21.
.Sp
Use of \f(CW\*(C`note()\*(C' from Test::More is now optional in tests. This works
around a circular dependency with Test::More when installing on very
old perls from \s-1CPAN.\s0
.Sp
Vstring magic strings over 2GB are now disallowed.
.Sp
Regular expressions objects weren't properly counted for object id
purposes on retrieve. This would corrupt the resulting structure, or
cause a runtime error in some cases. [\s-1GH\s0 #17037] <https://github.com/Perl/perl5/issues/17037>

- \(bu
Sys::Hostname has been upgraded from version 1.22 to 1.23.

- \(bu
Sys::Syslog has been upgraded from version 0.35 to 0.36.

- \(bu
Term::ANSIColor has been upgraded from version 4.06 to 5.01.

- \(bu
Test::Simple has been upgraded from version 1.302162 to 1.302175.

- \(bu
Thread has been upgraded from version 3.04 to 3.05.

- \(bu
Thread::Queue has been upgraded from version 3.13 to 3.14.

- \(bu
threads has been upgraded from version 2.22 to 2.25.

- \(bu
threads::shared has been upgraded from version 1.60 to 1.61.

- \(bu
Tie::File has been upgraded from version 1.02 to 1.06.

- \(bu
Tie::Hash::NamedCapture has been upgraded from version 0.10 to 0.13.

- \(bu
Tie::Scalar has been upgraded from version 1.04 to 1.05.

- \(bu
Tie::StdHandle has been upgraded from version 4.5 to 4.6.

- \(bu
Time::HiRes has been upgraded from version 1.9760 to 1.9764.
.Sp
Removed obsolete code such as support for pre-5.6 perl and classic
MacOS. [\s-1GH\s0 #17096] <https://github.com/Perl/perl5/issues/17096>

- \(bu
Time::Piece has been upgraded from version 1.33 to 1.3401.

- \(bu
Unicode::Normalize has been upgraded from version 1.26 to 1.27.

- \(bu
Unicode::UCD has been upgraded from version 0.72 to 0.75.

- \(bu
VMS::Stdio has been upgraded from version 2.44 to 2.45.

- \(bu
warnings has been upgraded from version 1.44 to 1.47.

- \(bu
Win32 has been upgraded from version 0.52 to 0.53.

- \(bu
Win32API::File has been upgraded from version 0.1203 to 0.1203_01.

- \(bu
XS::APItest has been upgraded from version 1.00 to 1.09.

### Removed Modules and Pragmata

Subsection "Removed Modules and Pragmata"

- \(bu
Pod::Parser has been removed from the core distribution.
It still is available for download from \s-1CPAN.\s0 This resolves
[#13194 <https://github.com/Perl/perl5/issues/13194>].

## Documentation

Header "Documentation"

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
We have attempted to update the documentation to reflect the changes
listed in this document. If you find any we have missed, open an issue
at <https://github.com/Perl/perl5/issues>.

Additionally, the following selected changes have been made:

*perldebguts*
Subsection "perldebguts"

- \(bu
Simplify a few regnode definitions
.Sp
Update \f(CW\*(C`BOUND\*(C' and \f(CW\*(C`NBOUND\*(C' definitions.

- \(bu
Add ANYOFHs regnode
.Sp
This node is like \f(CW\*(C`ANYOFHb\*(C', but is used when more than one leading byte
is the same in all the matched code points.
.Sp
\f(CW\*(C`ANYOFHb\*(C' is used to avoid having to convert from \s-1UTF-8\s0 to code point for
something that won't match. It checks that the first byte in the \s-1UTF-8\s0
encoded target is the desired one, thus ruling out most of the possible
code points.

*perlapi*
Subsection "perlapi"

- \(bu
\f(CW\*(C`sv_2pvbyte\*(C' updated to mention it will croak if the \s-1SV\s0 cannot be
downgraded.

- \(bu
\f(CW\*(C`sv_setpvn\*(C' updated to mention that the \s-1UTF-8\s0 flag will not be changed by
this function, and a terminating \s-1NUL\s0 byte is guaranteed.

- \(bu
Documentation for \f(CW\*(C`PL_phase\*(C' has been added.

- \(bu
The documentation for \f(CW\*(C`grok_bin\*(C', \f(CW\*(C`grok_oct\*(C', and \f(CW\*(C`grok_hex\*(C' has been
updated and clarified.

*perldiag*
Subsection "perldiag"

- \(bu
Add documentation for experimental 'isa' operator
.Sp
(S experimental::isa) This warning is emitted if you use the (\f(CW\*(C`isa\*(C')
operator. This operator is currently experimental and its behaviour may
change in future releases of Perl.

*perlfunc*
Subsection "perlfunc"
.ie n .IP """caller""" 4
.el .IP "\f(CWcaller" 4
Item "caller"
Like \f(CW\*(C`_\|_FILE_\|_\*(C' and \f(CW\*(C`_\|_LINE_\|_\*(C', the filename and
line number returned here may be altered by the mechanism described at
\*(L"Plain Old Comments (Not!)\*(R" in perlsyn.
.ie n .IP """_\|_FILE_\|_""" 4
.el .IP "\f(CW_\|_FILE_\|_" 4
Item "__FILE__"
It can be altered by the mechanism described at
\*(L"Plain Old Comments (Not!)\*(R" in perlsyn.
.ie n .IP """_\|_LINE_\|_""" 4
.el .IP "\f(CW_\|_LINE_\|_" 4
Item "__LINE__"
It can be altered by the mechanism described at
\*(L"Plain Old Comments (Not!)\*(R" in perlsyn.
.ie n .IP """return""" 4
.el .IP "\f(CWreturn" 4
Item "return"
Now mentions that you cannot return from \f(CW\*(C`do BLOCK\*(C'.
.ie n .IP """open""" 4
.el .IP "\f(CWopen" 4
Item "open"
The \f(CW\*(C`open()\*(C' section had been renovated significantly.

*perlguts*
Subsection "perlguts"

- \(bu
No longer suggesting using perl's \f(CW\*(C`malloc\*(C'. Modern system \f(CW\*(C`malloc\*(C' is
assumed to be much better than perl's implementation now.

- \(bu
Documentation about *embed.fnc* flags has been removed. *embed.fnc* now has
sufficient comments within it. Anyone changing that file will see those
comments first, so entries here are now redundant.

- \(bu
Updated documentation for \f(CW\*(C`UTF8f\*(C'

- \(bu
Added missing \f(CW\*(C`=for apidoc\*(C' lines

*perlhacktips*
Subsection "perlhacktips"

- \(bu
The differences between Perl strings and C strings are now detailed.

*perlintro*
Subsection "perlintro"

- \(bu
The documentation for the repetition operator \f(CW\*(C`x\*(C' have been clarified.
[\s-1GH\s0 #17335 <https://github.com/Perl/perl5/issues/17335>]

*perlipc*
Subsection "perlipc"

- \(bu
The documentation surrounding \f(CW\*(C`open\*(C' and handle usage has been modernized
to prefer 3-arg open and lexical variables instead of barewords.

- \(bu
Various updates and fixes including making all examples strict-safe and
replacing \f(CW\*(C`-w\*(C' with \f(CW\*(C`use warnings\*(C'.

*perlop*
Subsection "perlop"

- \(bu
'isa' operator is experimental
.Sp
This is an experimental feature and is available when enabled
by \f(CW\*(C`use feature \*(Aqisa\*(Aq\*(C'. It emits a warning in the \f(CW\*(C`experimental::isa\*(C'
category.

*perlpod*
Subsection "perlpod"

- \(bu
Details of the various stacks within the perl interpreter are now explained
here.

- \(bu
Advice has been added regarding the usage of \f(CW\*(C`Z<>\*(C'.

*perlport*
Subsection "perlport"

- \(bu
Update \f(CW\*(C`timegm\*(C' example to use the correct year format *1970* instead of *70*.
[\s-1GH\s0 #16431 <https://github.com/Perl/perl5/issues/16431>]

*perlreref*
Subsection "perlreref"

- \(bu
Fix some typos.

*perlvar*
Subsection "perlvar"

- \(bu
Now recommends stringifying \f(CW$] and comparing it numerically.

*perlapi, perlintern*
Subsection "perlapi, perlintern"

- \(bu
Documentation has been added for several functions that were
lacking it before.

*perlxs*
Subsection "perlxs"

- \(bu
Suggest using \f(CW\*(C`libffi\*(C' for simple library bindings via \s-1CPAN\s0 modules
like FFI::Platypus or FFI::Raw.

*\s-1POSIX\s0*
Subsection "POSIX"

- \(bu
\f(CW\*(C`setlocale\*(C' warning about threaded builds updated to note it does not
apply on Perl 5.28.X and later.

- \(bu
\f(CW\*(C`Posix::SigSet->new(...)\*(C' updated to state it throws an error if any of
the supplied signals cannot be added to the set.

Additionally, the following selected changes have been made:

*Updating of links*
Subsection "Updating of links"

- \(bu
Links to the now defunct <https://search.cpan.org> site now point at
the equivalent <https://metacpan.org> \s-1URL.\s0 [\s-1GH\s0 #17393 <https://github.com/Perl/perl5/issues/17393>]

- \(bu
The man page for ExtUtils::XSSymSet is now only installed on \s-1VMS,\s0
which is the only platform the module is installed on. [\s-1GH\s0 #17424 <https://github.com/Perl/perl5/issues/17424>]

- \(bu
URLs have been changed to \f(CW\*(C`https://\*(C' and stale links have been updated.
.Sp
Where applicable, the URLs in the documentation have been moved from using the
\f(CW\*(C`http://\*(C' protocol to \f(CW\*(C`https://\*(C'. This also affects the location of the bug
tracker at <https://rt.perl.org>.

- \(bu
Some links to \s-1OS/2\s0 libraries, Address Sanitizer and other system tools had gone
stale. These have been updated with working links.

- \(bu
Some links to old email addresses on perl5-porters had gone stale. These have been
updated with working links.

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages. For the complete list of
diagnostic messages, see perldiag.

### New Diagnostics

Subsection "New Diagnostics"
*New Errors*
Subsection "New Errors"

- \(bu
Expecting interpolated extended charclass in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
This is a replacement for several error messages listed under
\*(L"Changes to Existing Diagnostics\*(R".

- \(bu
\f(CW\*(C`No digits found for %s literal\*(C'
.Sp
(F) No hexadecimal digits were found following \f(CW\*(C`0x\*(C' or no binary digits were
found following \f(CW\*(C`0b\*(C'.

*New Warnings*
Subsection "New Warnings"

- \(bu
Code point 0x%X is not Unicode, and not portable
.Sp
This is actually not a new message, but it is now output when the
warnings category \f(CW\*(C`portable\*(C' is enabled.
.Sp
When raised during regular expression pattern compilation, the warning
has extra text added at the end marking where precisely in the pattern
it occurred.

- \(bu
Non-hex character '%c' terminates \\x early.  Resolved as \*(L"%s\*(R"
.Sp
This replaces a warning that was much less specific, and which gave
false information. This new warning parallels the similar
already-existing one raised for \f(CW\*(C`\\o\{\}\*(C'.

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
Character following \*(L"\\c\*(R" must be printable \s-1ASCII\s0
.Sp
...now has extra text added at the end, when raised during regular
expression pattern compilation, marking where precisely in the pattern
it occurred.

- \(bu
Use \*(L"%s\*(R" instead of \*(L"%s\*(R"
.Sp
...now has extra text added at the end, when raised during regular
expression pattern compilation, marking where precisely in the pattern
it occurred.

- \(bu
Sequence \*(L"\\c\{\*(R" invalid
.Sp
...now has extra text added at the end, when raised during regular
expression pattern compilation, marking where precisely in the pattern
it occurred.

- \(bu
\*(L"\\c%c\*(R" is more clearly written simply as \*(L"%s\*(R"
.Sp
...now has extra text added at the end, when raised during regular
expression pattern compilation, marking where precisely in the pattern
it occurred.

- \(bu
Non-octal character '%c' terminates \\o early.  Resolved as \*(L"%s\*(R"
.Sp
...now includes the phrase \*(L"terminates \\o early\*(R", and has extra text added
at the end, when raised during regular expression pattern compilation,
marking where precisely in the pattern it occurred. In some instances
the text of the resolution has been clarified.

- \(bu
'%s' resolved to '\\o\{%s\}%d'
.Sp
As of Perl 5.32, this message is no longer generated. Instead,
\*(L"Non-octal character '%c' terminates \\o early.  Resolved as \*(R"%s"" in perldiag
is used instead.

- \(bu
Use of code point 0x%s is not allowed; the permissible max is 0x%X
.Sp
Some instances of this message previously output the hex digits \f(CW\*(C`A\*(C',
\f(CW\*(C`B\*(C', \f(CW\*(C`C\*(C', \f(CW\*(C`D\*(C', \f(CW\*(C`E\*(C', and \f(CW\*(C`F\*(C' in lower case. Now they are all
consistently upper case.

- \(bu
The following three diagnostics have been removed, and replaced by
\f(CW\*(C`Expecting interpolated extended charclass in regex; marked by <-- HERE in m/%s/\*(C'
:
\f(CW\*(C`Expecting close paren for nested extended charclass in regex; marked
by <-- HERE in m/%s/\*(C',
\f(CW\*(C`Expecting close paren for wrapper for nested extended charclass in
regex; marked by <-- HERE in m/%s/\*(C',
and
\f(CW\*(C`Expecting \*(Aq(?flags:(?[...\*(Aq in regex; marked by <--\ HERE in m/%s/\*(C'.

- \(bu
The \f(CW\*(C`Code point 0x%X is not Unicode, and not portable\*(C' warning removed
the line \f(CW\*(C`Code points above 0xFFFF_FFFF require larger than a 32 bit word.\*(C'
as code points that large are no longer legal on 32-bit platforms.

- \(bu
Can't use global \f(CW%s in \f(CW%s
.Sp
This error message has been slightly reformatted from the original \f(CW\*(C`Can\*(Aqt use
global %s in "%s"\*(C', and in particular misleading error messages like \f(CW\*(C`Can\*(Aqt
use global $_ in "my"\*(C' are now rendered as \f(CW\*(C`Can\*(Aqt use global $_ in subroutine
signature\*(C'.

- \(bu
Constants from lexical variables potentially modified elsewhere are no longer permitted
.Sp
This error message replaces the former \f(CW\*(C`Constants from lexical variables
potentially modified elsewhere are deprecated. This will not be allowed in Perl
5.32\*(C' to reflect the fact that this previously deprecated usage has now been
transformed into an exception. The message's classification has also been
updated from D (deprecated) to F (fatal).
.Sp
See also \*(L"Incompatible Changes\*(R".

- \(bu
\f(CW\*(C`\\N\{\} here is restricted to one character\*(C' is now emitted in the same
circumstances where previously \f(CW\*(C`\\N\{\} in inverted character class or as a range
end-point is restricted to one character\*(C' was.
.Sp
This is due to new circumstances having been added in Perl 5.30 that weren't
covered by the earlier wording.

## Utility Changes

Header "Utility Changes"

### perlbug

Subsection "perlbug"

- \(bu
The bug tracker homepage \s-1URL\s0 now points to GitHub.

### streamzip

Subsection "streamzip"

- \(bu
This is a new utility, included as part of an
IO::Compress::Base upgrade.
.Sp
streamzip creates a zip file from stdin. The program will read data
from stdin, compress it into a zip container and, by default, write a
streamed zip file to stdout.

## Configuration and Compilation

Header "Configuration and Compilation"

### \fIConfigure

Subsection "Configure"

- \(bu
For clang++, add \f(CW\*(C`#include <stdlib.h>\*(C' to Configure's probes for
\f(CW\*(C`futimes\*(C', \f(CW\*(C`strtoll\*(C', \f(CW\*(C`strtoul\*(C', \f(CW\*(C`strtoull\*(C', \f(CW\*(C`strtouq\*(C', otherwise the
probes would fail to compile.

- \(bu
Use a compile and run test for \f(CW\*(C`lchown\*(C' to satisfy clang++ which should
more reliably detect it.

- \(bu
For \*(C+ compilers, add \f(CW\*(C`#include <stdio.h>\*(C' to Configure's probes for
\f(CW\*(C`getpgrp\*(C' and \f(CW\*(C`setpgrp\*(C' as they use printf and \*(C+ compilers may fail
compilation instead of just warning.

- \(bu
Check if the compiler can handle inline attribute.

- \(bu
Check for character data alignment.

- \(bu
*Configure* now correctly handles gcc-10. Previously it was interpreting it
as gcc-1 and turned on \f(CW\*(C`-fpcc-struct-return\*(C'.

- \(bu
Perl now no longer probes for \f(CW\*(C`d_u32align\*(C', defaulting to \f(CW\*(C`define\*(C' on all
platforms. This check was error-prone when it was done, which was on 32-bit
platforms only.
[\s-1GH\s0 #16680] <https://github.com/Perl/perl5/issues/16680>

- \(bu
Documentation and hints for building perl on Z/OS (native \s-1EBCDIC\s0) have been
updated. This is still a work in progress.

- \(bu
A new probe for \f(CW\*(C`malloc_usable_size\*(C' has been added.

- \(bu
Improvements in *Configure* to detection in \*(C+ and clang++. Work ongoing by
Andy Dougherty. [\s-1GH\s0 #17033] <https://github.com/Perl/perl5/issues/17033>

- \(bu
*autodoc.pl*
.Sp
This tool that regenerates perlintern and perlapi has been overhauled
significantly, restoring consistency in flags used in *embed.fnc* and
Devel::PPPort and allowing removal of many redundant \f(CW\*(C`=for apidoc\*(C'
entries in code.

- \(bu
The \f(CW\*(C`ECHO\*(C' macro is now defined. This is used in a \f(CW\*(C`dtrace\*(C' rule that was
originally changed for FreeBSD, and the FreeBSD make apparently predefines it.
The Solaris make does not predefine \f(CW\*(C`ECHO\*(C' which broke this rule on Solaris.
[\s-1GH\s0 #17057] <https://github.com/Perl/perl5/issues/17057>

- \(bu
Bison versions 3.1 through 3.4 are now supported.

## Testing

Header "Testing"
Tests were added and changed to reflect the other additions and
changes in this release. Furthermore, these significant changes were
made:

- \(bu
*t/run/switches.t* no longer uses (and re-uses) the *tmpinplace/*
directory under *t/*. This may prevent spurious failures. [\s-1GH\s0 #17424 <https://github.com/Perl/perl5/issues/17424>]

- \(bu
Various bugs in \f(CW\*(C`POSIX::mbtowc\*(C' were fixed. Potential races with
other threads are now avoided, and previously the returned wide
character could well be garbage.

- \(bu
Various bugs in \f(CW\*(C`POSIX::wctomb\*(C' were fixed. Potential races with other
threads are now avoided, and previously it would segfault if the string
parameter was shared or hadn't been pre-allocated with a string of
sufficient length to hold the result.

- \(bu
Certain test output of scalars containing control characters and Unicode
has been fixed on \s-1EBCDIC.\s0

- \(bu
*t/charset_tools.pl*: Avoid some work on \s-1ASCII\s0 platforms.

- \(bu
*t/re/regexp.t*: Speed up many regex tests on \s-1ASCII\s0 platform

- \(bu
*t/re/pat.t*: Skip tests that don't work on \s-1EBCDIC.\s0

## Platform Support

Header "Platform Support"

### Discontinued Platforms

Subsection "Discontinued Platforms"

- Windows \s-1CE\s0
Item "Windows CE"
Support for building perl on Windows \s-1CE\s0 has now been removed.

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- Linux
Item "Linux"
\f(CW\*(C`cc\*(C' will be used to populate \f(CW\*(C`plibpth\*(C' if \f(CW\*(C`cc\*(C' is \f(CW\*(C`clang\*(C'.
[\s-1GH\s0 #17043] <https://github.com/Perl/perl5/issues/17043>

- NetBSD 8.0
Item "NetBSD 8.0"
Fix compilation of Perl on NetBSD 8.0 with g++.
[\s-1GH\s0 #17381 <https://github.com/Perl/perl5/issues/17381>]

- Windows
Item "Windows"

> 0

- \(bu
.PD
The configuration for \f(CW\*(C`ccflags\*(C' and \f(CW\*(C`optimize\*(C' are now separate, as
with \s-1POSIX\s0 platforms. [\s-1GH\s0 #17156 <https://github.com/Perl/perl5/issues/17156>]

- \(bu
Support for building perl with Visual \*(C+ 6.0 has now been removed.

- \(bu
The locale tests could crash on Win32 due to a Windows bug, and
separately due to the \s-1CRT\s0 throwing an exception if the locale name
wasn't validly encoded in the current code page.
.Sp
For the second we now decode the locale name ourselves, and always
decode it as \s-1UTF-8.\s0 [\s-1GH\s0 #16922] <https://github.com/Perl/perl5/issues/16922>

- \(bu
*t/op/magic.t* could fail if environment variables starting with
\f(CW\*(C`FOO\*(C' already existed.

- \(bu
\s-1MYMALLOC\s0 (\s-1PERL_MALLOC\s0) build has been fixed.



> 


- Solaris
Item "Solaris"

> 0

- \(bu
.PD
\f(CW\*(C`Configure\*(C' will now find recent versions of the Oracle Developer Studio
compiler, which are found under \f(CW\*(C`/opt/developerstudio*\*(C'.

- \(bu
\f(CW\*(C`Configure\*(C' now uses the detected types for \f(CW\*(C`gethostby*\*(C' functions, allowing
Perl to once again compile on certain configurations of Solaris.



> 


- \s-1VMS\s0
Item "VMS"

> 0

- \(bu
.PD
With the release of the patch kit C99 V2.0, \s-1VSI\s0 has provided support for a
number of previously-missing C99 features. On systems with that patch kit
installed, Perl's configuration process will now detect the presence of the
header \f(CW\*(C`stdint.h\*(C' and the following functions: \f(CW\*(C`fpclassify\*(C', \f(CW\*(C`isblank\*(C', \f(CW\*(C`isless\*(C',
\f(CW\*(C`llrint\*(C', \f(CW\*(C`llrintl\*(C', \f(CW\*(C`llround\*(C', \f(CW\*(C`llroundl\*(C', \f(CW\*(C`nearbyint\*(C', \f(CW\*(C`round\*(C', \f(CW\*(C`scalbn\*(C',
and \f(CW\*(C`scalbnl\*(C'.

- \(bu
\f(CW\*(C`-Duse64bitint\*(C' is now the default on \s-1VMS.\s0



> 


- z/OS
Item "z/OS"
Perl 5.32 has been tested on z/OS 2.4, with the following caveats:

> 
- \(bu
Only static builds (the default) build reliably

- \(bu
When using locales, z/OS does not handle the \f(CW\*(C`LC_MESSAGES\*(C' category
properly, so when compiling perl, you should add the following to your
*Configure* options
.Sp
.Vb 1
 ./Configure <other options> -Accflags=-DNO_LOCALE_MESSAGES
.Ve

- \(bu
z/OS does not support locales with threads, so when compiling a threaded
perl, you should add the following to your *Configure* options
.Sp
.Vb 1
 ./Configure <other Configure options> -Accflags=-DNO_LOCALE
.Ve

- \(bu
Some \s-1CPAN\s0 modules that are shipped with perl fail at least one of their
self-tests.  These are:
Archive::Tar,
Config::Perl::V,
CPAN::Meta,
CPAN::Meta::YAML,
Digest::MD5,
Digest::SHA,
Encode,
ExtUtils::MakeMaker,
ExtUtils::Manifest,
HTTP::Tiny,
IO::Compress,
IPC::Cmd,
\s-1JSON::PP,\s0
libnet,
MIME::Base64,
Module::Metadata,
PerlIO::via-QuotedPrint,
Pod::Checker,
podlators,
Pod::Simple,
Socket,
and Test::Harness.
.Sp
The causes of the failures range from the self-test itself is flawed,
and the module actually works fine, up to the module doesn't work at all
on \s-1EBCDIC\s0 platforms.



> 


## Internal Changes

Header "Internal Changes"

- \(bu
\f(CW\*(C`savepvn\*(C''s len parameter is now a \f(CW\*(C`Size_t\*(C' instead of an \f(CW\*(C`I32\*(C' since we
can handle longer strings than 31 bits.

- \(bu
The lexer (\f(CW\*(C`Perl_yylex()\*(C' in *toke.c*) was previously a single 4100-line
function, relying heavily on \f(CW\*(C`goto\*(C' and a lot of widely-scoped local variables
to do its work. It has now been pulled apart into a few dozen smaller static
functions; the largest remaining chunk (\f(CW\*(C`yyl_word_or_keyword()\*(C') is a little
over 900 lines, and consists of a single \f(CW\*(C`switch\*(C' statement, all of whose
\f(CW\*(C`case\*(C' groups are independent. This should be much easier to understand and
maintain.

- \(bu
The OS-level signal handlers and type (Sighandler_t) used by the perl core
were declared as having three parameters, but the \s-1OS\s0 was always told to
call them with one argument. This has been fixed by declaring them to have
one parameter. See the merge commit \f(CW\*(C`v5.31.5-346-g116e19abbf\*(C' for full
details.

- \(bu
The code that handles \f(CW\*(C`tr///\*(C' has been extensively revised, fixing
various bugs, especially when the source and/or replacement strings
contain characters whose code points are above 255. Some of the bugs
were undocumented, one being that under some circumstances (but not all)
with \f(CW\*(C`/s\*(C', the squeezing was done based on the source, rather than the
replacement. A documented bug that got fixed was
[\s-1GH\s0 #14777] <https://github.com/Perl/perl5/issues/14777>.

- \(bu
A new macro for \s-1XS\s0 writers dealing with UTF-8-encoded Unicode strings
has been created "\f(CW\*(C`UTF8_CHK_SKIP\*(C'" in perlapi that is safer in the face
of malformed \s-1UTF-8\s0 input than "\f(CW\*(C`UTF8_SKIP\*(C'" in perlapi (but not as safe
as "\f(CW\*(C`UTF8_SAFE_SKIP\*(C'" in perlapi). It won't read past a \s-1NUL\s0 character.
It has been backported in Devel::PPPort 3.55 and later.

- \(bu
Added the \f(CW\*(C`PL_curstackinfo->si_cxsubix\*(C' field. This records the stack index
of the most recently pushed sub/format/eval context. It is set and restored
automatically by \f(CW\*(C`cx_pushsub()\*(C', \f(CW\*(C`cx_popsub()\*(C' etc., but would need to be
manually managed if you do any unusual manipulation of the context stack.

- \(bu
Various macros dealing with character type classification and changing case
where the input is encoded in \s-1UTF-8\s0 now require an extra parameter to prevent
potential reads beyond the end of the buffer. Use of these has generated a
deprecation warning since Perl 5.26. Details are in
\*(L"In \s-1XS\s0 code, use of various macros dealing with \s-1UTF-8.\*(R"\s0 in perldeprecation

- \(bu
A new parser function **parse_subsignature()**
allows a keyword plugin to parse a subroutine signature while \f(CW\*(C`use feature
\*(Aqsignatures\*(Aq\*(C' is in effect. This allows custom keywords to implement
semantics similar to regular \f(CW\*(C`sub\*(C' declarations that include signatures.
[\s-1GH\s0 #16261] <https://github.com/Perl/perl5/issues/16261>

- \(bu
Since on some platforms we need to hold a mutex when temporarily
switching locales, new macros (\f(CW\*(C`STORE_LC_NUMERIC_SET_TO_NEEDED_IN\*(C',
\f(CW\*(C`WITH_LC_NUMERIC_SET_TO_NEEDED\*(C' and \f(CW\*(C`WITH_LC_NUMERIC_SET_TO_NEEDED_IN\*(C')
have been added to make it easier to do this safely and efficiently
as part of [\s-1GH\s0 #17034] <https://github.com/Perl/perl5/issues/17034>.

- \(bu
The memory bookkeeping overhead for allocating an \s-1OP\s0 structure has been
reduced by 8 bytes per \s-1OP\s0 on 64-bit systems.

- \(bu
**eval_pv()** no longer stringifies the exception when
\f(CW\*(C`[GH #17035]|https://github.com/Perl/perl5/issues/17035\*(C']

- \(bu
The \s-1PERL_DESTRUCT_LEVEL\s0 environment variable was formerly only honoured on perl
binaries built with \s-1DEBUGGING\s0 support. It is now checked on all perl builds.
Its normal use is to force perl to individually free every block of memory
which it has allocated before exiting, which is useful when using automated
leak detection tools such as valgrind.

- \(bu
The \s-1API\s0 **eval_sv()** now accepts a \f(CW\*(C`G_RETHROW\*(C' flag. If this flag is set and an
exception is thrown while compiling or executing the supplied code, it will be
rethrown, and **eval_sv()** will not return.
[\s-1GH\s0 #17036] <https://github.com/Perl/perl5/issues/17036>

- \(bu
As part of the fix for
[\s-1GH\s0 #1537] <https://github.com/Perl/perl5/issues/1537> **perl_parse()**
now returns non-zero if **exit**\|(0) is called in a \f(CW\*(C`BEGIN\*(C', \f(CW\*(C`UNITCHECK\*(C' or
\f(CW\*(C`CHECK\*(C' block.

- \(bu
Most functions which recursively walked an op tree during compilation have been
made non-recursive. This avoids SEGVs from stack overflow when the op tree is
deeply nested, such as \f(CW\*(C`$n == 1 ? "one" : $n == 2 ? "two" : ....\*(C' (especially
in code which is auto-generated).
.Sp
This is particularly noticeable where the code is compiled within a separate
thread, as threads tend to have small stacks by default.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Previously \*(L"require\*(R" in perlfunc would only treat the special built-in
\s-1SV\s0 \f(CW&PL_sv_undef as a value in \f(CW%INC as if a previous \f(CW\*(C`require\*(C'
has failed, treating other undefined SVs as if the previous \f(CW\*(C`require\*(C'
has succeeded. This could cause unexpected success from \f(CW\*(C`require\*(C'
e.g., on \f(CW\*(C`local %INC = %INC;\*(C'. This has been fixed. [\s-1GH\s0 #17428 <https://github.com/Perl/perl5/issues/17428>]

- \(bu
\f(CW\*(C`(?\{...\})\*(C' eval groups in regular expressions no longer unintentionally
trigger \*(L"\s-1EVAL\s0 without pos change exceeded limit in regex\*(R" [\s-1GH\s0 #17490 <https://github.com/Perl/perl5/issues/17490>].

- \(bu
\f(CW\*(C`(?[...])\*(C' extended bracketed character classes do not wrongly raise an
error on some cases where a previously-compiled such class is
interpolated into another. The heuristics previously used have been
replaced by a reliable method, and hence the diagnostics generated have
changed. See \*(L"Diagnostics\*(R".

- \(bu
The debug display (say by specifying \f(CW\*(C`-Dr\*(C' or \f(CW\*(C`use\ re\*(C' (with
appropriate options) of compiled Unicode property wildcard subpatterns no
longer has extraneous output.

- \(bu
Fix an assertion failure in the regular expression engine.
[\s-1GH\s0 #17372 <https://github.com/Perl/perl5/issues/17372>]

- \(bu
Fix coredump in pp_hot.c after \f(CW\*(C`B::UNOP_AUX::aux_list()\*(C'.
[\s-1GH\s0 #17301 <https://github.com/Perl/perl5/issues/17301>]

- \(bu
Loading \s-1IO\s0 is now threadsafe.
[\s-1GH\s0 #14816 <https://github.com/Perl/perl5/issues/14816>]

- \(bu
\f(CW\*(C`\\p\{user-defined\}\*(C' overrides official Unicode [\s-1GH\s0 #17025 <https://github.com/Perl/perl5/issues/17025>]
.Sp
Prior to this patch, the override was only sometimes in effect.

- \(bu
Properly handle filled \f(CW\*(C`/il\*(C' regnodes and multi-char folds

- \(bu
Compilation error during make minitest [\s-1GH\s0 #17293 <https://github.com/Perl/perl5/issues/17293>]

- \(bu
Move the implementation of \f(CW\*(C`%-\*(C', \f(CW\*(C`%+\*(C' into core.

- \(bu
Read beyond buffer in \f(CW\*(C`grok_inf_nan\*(C' [\s-1GH\s0 #17370 <https://github.com/Perl/perl5/issues/17370>]

- \(bu
Workaround glibc bug with \f(CW\*(C`LC_MESSAGES\*(C' [\s-1GH\s0 #17081 <https://github.com/Perl/perl5/issues/17081>]

- \(bu
\f(CW\*(C`printf()\*(C' or \f(CW\*(C`sprintf()\*(C' with the \f(CW%n format could cause a panic on
debugging builds, or report an incorrectly cached length value when
producing \f(CW\*(C`SVfUTF8\*(C' flagged strings. [\s-1GH\s0 #17221 <https://github.com/Perl/perl5/issues/17221>]

- \(bu
The tokenizer has been extensively refactored.
[\s-1GH\s0 #17241 <https://github.com/Perl/perl5/issues/17241>]
[\s-1GH\s0 #17189 <https://github.com/Perl/perl5/issues/17189>]

- \(bu
\f(CW\*(C`use strict "subs"\*(C' is now enforced for bareword constants optimized
into a \f(CW\*(C`multiconcat\*(C' operator. [\s-1GH\s0 #17254 <https://github.com/Perl/perl5/issues/17254>]

- \(bu
A memory leak in regular expression patterns has been fixed. [\s-1GH\s0 #17218 <https://github.com/Perl/perl5/issues/17218>]

- \(bu
Perl no longer treats strings starting with \*(L"0x\*(R" or \*(L"0b\*(R" as hex or
binary numbers respectively when converting a string to a number.
This reverts a change in behaviour inadvertently introduced in perl
5.30.0 intended to improve precision when converting a string to a
floating point number. [\s-1GH\s0 #17062] <https://github.com/Perl/perl5/issues/17062>

- \(bu
Matching a non-\f(CW\*(C`SVf_UTF8\*(C' string against a regular expression
containing unicode literals could leak a \s-1SV\s0 on each match attempt.
[\s-1GH\s0 #17140] <https://github.com/Perl/perl5/issues/17140>

- \(bu
Overloads for octal and binary floating point literals were always
passed a string with a \f(CW\*(C`0x\*(C' prefix instead of the appropriate \f(CW0 or
\f(CW\*(C`[GH #14791]|https://github.com/Perl/perl5/issues/14791\*(C']

- \(bu
\f(CW\*(C`$@ = 100; die;\*(C' now correctly propagates the 100 as an exception
instead of ignoring it. [\s-1GH\s0 #17098] <https://github.com/Perl/perl5/issues/17098>

- \(bu
\f(CW\*(C`[GH #17108]|https://github.com/Perl/perl5/issues/17108\*(C']

- \(bu
Exceptions thrown while \f(CW$@ is read-only could result in infinite
recursion as perl tried to update \f(CW$@, which throws another
exception, resulting in a stack overflow. Perl now replaces \f(CW$@
with a copy if it's not a simple writable \s-1SV.\s0 [\s-1GH\s0 #17083] <https://github.com/Perl/perl5/issues/17083>

- \(bu
Setting \f(CW$) now properly sets supplementary group ids if you have
the necessary privileges. [\s-1GH\s0 #17031] <https://github.com/Perl/perl5/issues/17031>

- \(bu
**close()** on a pipe now preemptively clears the PerlIO object from the
\s-1IO SV.\s0 This prevents a second attempt to close the already closed
PerlIO object if a signal handler calls **die()** or **exit()** while **close()**
is waiting for the child process to complete. [\s-1GH\s0 #13929] <https://github.com/Perl/perl5/issues/13929>

- \(bu
\f(CW\*(C`sprintf("%.*a", -10000, $x)\*(C' would cause a buffer overflow due
to mishandling of the negative precision value. [\s-1GH\s0 #16942] <https://github.com/Perl/perl5/issues/16942>

- \(bu
**scalar()** on a reference could cause an erroneous assertion failure
during compilation. [\s-1GH\s0 #16969] <https://github.com/Perl/perl5/issues/16969>

- \(bu
\f(CW\*(C`%\{^CAPTURE_ALL\}\*(C' is now an alias to \f(CW\*(C`%-\*(C' as documented, rather than
incorrectly an alias for \f(CW\*(C`[GH #16105]|https://github.com/Perl/perl5/issues/16105\*(C']

- \(bu
\f(CW\*(C`%\{^CAPTURE\}\*(C' didn't work if \f(CW\*(C`@\{^CAPTURE\}\*(C' was mentioned first.
Similarly for \f(CW\*(C`%\{^CAPTURE_ALL\}\*(C' and \f(CW\*(C`@\{^CAPTURE_ALL\}\*(C', though
\f(CW\*(C`[GH #17045]|https://github.com/Perl/perl5/issues/17045\*(C']

- \(bu
Extraordinarily large (over 2GB) floating point format widths could
cause an integer overflow in the underlying call to **snprintf()**,
resulting in an assertion. Formatted floating point widths are now
limited to the range of int, the return value of **snprintf()**.
[#16881 <https://github.com/Perl/perl5/issues/16881>]

- \(bu
Parsing the following constructs within a sub-parse (such as with
\f(CW"$\{code here\}" or \f(CW\*(C`s/.../code here/e\*(C') has changed to match how
they're parsed normally:

> 
- \(bu
\f(CW\*(C`print $fh ...\*(C' no longer produces a syntax error.

- \(bu
Code like \f(CW\*(C`s/.../ $\{time\} /e\*(C' now properly produces an \*(L"Ambiguous use
of $\{time\} resolved to \f(CW$time at ...\*(R" warning when warnings are enabled.

- \(bu
\f(CW\*(C`@x \{"a"\}\*(C' (with the space) in a sub-parse now properly produces a
\*(L"better written as\*(R" warning when warnings are enabled.

- \(bu
Attributes can now be used in a sub-parse.
[\s-1GH\s0 #16847] <https://github.com/Perl/perl5/issues/16847>



> 


- \(bu
Incomplete hex and binary literals like \f(CW\*(C`0x\*(C' and \f(CW\*(C`0b\*(C' are now
treated as if the \f(CW\*(C`x\*(C' or \f(CW\*(C`b\*(C' is part of the next token.
[#17010 <https://github.com/Perl/perl5/issues/17010>]

- \(bu
A spurious \f(CW\*(C`)\*(C' in a subparse, such as in \f(CW\*(C`s/.../code here/e\*(C' or
\f(CW"...$\{code here\}", no longer confuses the parser.
.Sp
Previously a subparse was bracketed with generated \f(CW\*(C`(\*(C' and \f(CW\*(C`)\*(C'
tokens, so a spurious \f(CW\*(C`)\*(C' would close the construct without doing the
normal subparse clean up, confusing the parser and possible causing an
assertion failure.
.Sp
Such constructs are now surrounded by artificial tokens that can't be
included in the source. [\s-1GH\s0 #15814] <https://github.com/Perl/perl5/issues/15814>

- \(bu
Reference assignment of a sub, such as \f(CW\*(C`\foo = \bar;\*(C', silently did
nothing in the \f(CW\*(C`[GH #16987]|https://github.com/Perl/perl5/issues/16987\*(C']

- \(bu
**sv_gets()** now recovers better if the target \s-1SV\s0 is modified by a signal
handler. [\s-1GH\s0 #16960] <https://github.com/Perl/perl5/issues/16960>

- \(bu
\f(CW\*(C`readline @foo\*(C' now evaluates \f(CW@foo in scalar context. Previously
it would be evaluated in list context, and since **readline()** pops only
one argument from the stack, the stack could underflow, or be left
with unexpected values on the stack. [\s-1GH\s0 #16929] <https://github.com/Perl/perl5/issues/16929>

- \(bu
Parsing incomplete hex or binary literals was changed in 5.31.1 to treat such a
literal as just the 0, leaving the following \f(CW\*(C`x\*(C' or \f(CW\*(C`b\*(C' to be parsed as part
of the next token. This could lead to some silent changes in behaviour, so now
incomplete hex or binary literals produce a fatal error.
[\s-1GH\s0 #17010] <https://github.com/Perl/perl5/issues/17010>

- \(bu
**eval_pv()**'s *croak_on_error* flag will now throw even if the exception is a
false overloaded value.
[\s-1GH\s0 #17036] <https://github.com/Perl/perl5/issues/17036>

- \(bu
\f(CW\*(C`INIT\*(C' blocks and the program itself are no longer run if **exit**\|(0) is called
within a \f(CW\*(C`BEGIN\*(C', \f(CW\*(C`UNITCHECK\*(C' or \f(CW\*(C`CHECK\*(C' block.
[\s-1GH\s0 #1537] <https://github.com/Perl/perl5/issues/1537>

- \(bu
\f(CW\*(C`open my $fh, ">>+", undef\*(C' now opens the temporary file in append mode:
writes will seek to the end of file before writing.
[\s-1GH\s0 #17058] <https://github.com/Perl/perl5/issues/17058>

- \(bu
Fixed a \s-1SEGV\s0 when searching for the source of an uninitialized value warning on
an op whose subtree includes an \s-1OP_MULTIDEREF.\s0
[\s-1GH\s0 #17088] <https://github.com/Perl/perl5/issues/17088>

## Obituary

Header "Obituary"
Jeff Goff (\s-1JGOFF\s0 or DrForr), an integral part of the Perl and Raku
communities and a dear friend to all of us, has passed away on March
13th, 2020. DrForr was a prominent member of the communities, attending
and speaking at countless events, contributing to numerous projects,
and assisting and helping in any way he could.

His passing leaves a hole in our hearts and in our communities and he
will be sorely missed.

## Acknowledgements

Header "Acknowledgements"
Perl 5.32.0 represents approximately 13 months of development since Perl
5.30.0 and contains approximately 220,000 lines of changes across 1,800
files from 89 authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 140,000 lines of changes to 880 .pm, .t, .c and .h files.

Perl continues to flourish into its fourth decade thanks to a vibrant
community of users and developers. The following people are known to have
contributed the improvements that became Perl 5.32.0:

Aaron Crane, Alberto Simo\*~es, Alexandr Savca, Andreas Ko\*:nig, Andrew Fresh,
Andy Dougherty, Ask Bjo\*/rn Hansen, Atsushi Sugawara, Bernhard M. Wiedemann,
brian d foy, Bryan Stenson, Chad Granum, Chase Whitener, Chris 'BinGOs'
Williams, Craig A. Berry, Dagfinn Ilmari Mannsa\*oker, Dan Book, Daniel
Dragan, Dan Kogai, Dave Cross, Dave Rolsky, David Cantrell, David Mitchell,
Dominic Hargreaves, E. Choroba, Felipe Gasper, Florian Weimer, Graham Knop,
Ha\*okon H\*(aegland, Hauke D, H.Merijn Brand, Hugo van der Sanden, Ichinose
Shogo, James E Keenan, Jason McIntosh, Jerome Duval, Johan Vromans, John
Lightsey, John Paul Adrian Glaubitz, Kang-min Liu, Karen Etheridge, Karl
Williamson, Leon Timmermans, Manuel Mausz, Marc Green, Matthew Horsfall,
Matt Turner, Max Maischein, Michael Haardt, Nicholas Clark, Nicolas R., Niko
Tyni, Pali, Paul Evans, Paul Johnson, Paul Marquess, Peter Eisentraut, Peter
John Acklam, Peter Oliver, Petr Pi\*'saX, Renee Baecker, Ricardo Signes,
Richard Leach, Russ Allbery, Samuel Smith, Santtu Ojanpera\*:, Sawyer X,
Sergey Aleynikov, Sergiy Borodych, Shirakata Kentaro, Shlomi Fish, Sisyphus,
Slaven Rezic, Smylers, Stefan Seifert, Steve Hay, Steve Peters, Svyatoslav,
Thibault Duponchelle, Todd Rinaldo, Tomasz Konojacki, Tom Hukins, Tony Cook,
Unicode Consortium, VanL, Vickenty Fesunov, Vitali Peil, Yves Orton, Zefram.

The list above is almost certainly incomplete as it is automatically
generated from version control history. In particular, it does not include
the names of the (very much appreciated) contributors who reported issues to
the Perl bug tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core. We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please
see the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database
at <https://github.com/Perl/perl5/issues>. There may also be information at
<http://www.perl.org/>, the Perl Home Page.

If you believe you have an unreported bug, please open an issue at
<https://github.com/Perl/perl5/issues>. Be sure to trim your bug down to a
tiny but sufficient test case.

If the bug you are reporting has security implications which make it
inappropriate to send to a public issue tracker, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5,
you can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
