+++
manpage_section = "1"
author = "None Specified"
title = "spfd(1)"
manpage_format = "troff"
keywords = ["header", "see", "also", "mail", "spf", "query", "http", "www", "openspf", "org", "authors", "this", "version", "of", "fbspfd", "was", "written", "by", "meng", "weng", "wong", "mengwong", "pobox", "com", "improved", "argument", "parsing", "added", "julian", "mehnle", "net", "man-page"]
operating_system = "macos"
operating_system_version = "15.3"
detected_package_version = "5.34.0"
date = "2006-02-07"
description = "spfd is a simple forking Sender Policy Framework (s-1SPFs0) query proxy server. spfd receives and answers s-1SPFs0 query requests on a s-1TCP/IPs0 or s-1UNIXs0 domain socket. The --port form listens on a s-1TCP/IPs0 socket on the specified port.  T..."
manpage_name = "spfd"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "SPFD 1"
SPFD 1 "2006-02-07" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

spfd - simple forking daemon to provide SPF query services

## VERSION

Header "VERSION"
2006-02-07

## SYNOPSIS

Header "SYNOPSIS"
**spfd** **--port** *port* [**--set-user** *uid*|*username*] [**--set-group**
*gid*|*groupname*]

**spfd** **--socket** *filename* [**--socket-user** *uid*|*username*]
[**--socket-group** *gid*|*groupname*] [**--socket-perms** *octal-perms*]
[**--set-user** *uid*|*username*] [**--set-group** *gid*|*groupname*]

**spfd** **--help**

## DESCRIPTION

Header "DESCRIPTION"
**spfd** is a simple forking Sender Policy Framework (\s-1SPF\s0) query proxy server.
spfd receives and answers \s-1SPF\s0 query requests on a \s-1TCP/IP\s0 or \s-1UNIX\s0 domain
socket.

The **--port** form listens on a \s-1TCP/IP\s0 socket on the specified *port*.  The
default port is **5970**.

The **--socket** form listens on a \s-1UNIX\s0 domain socket that is created with the
specified *filename*.  The socket can be assigned specific user and group
ownership with the **--socket-user** and **--socket-group** options, and specific
filesystem permissions with the **--socket-perms** option.

Generally, spfd can be instructed with the **--set-user** and **--set-group**
options to drop root privileges and change to another user and group before it
starts listening for requests.

The **--help** form prints usage information for **spfd**.

## REQUEST

Header "REQUEST"
A request consists of a series of lines delimited by \\x0A (\s-1LF\s0) characters (or
whatever your system considers a newline).  Each line must be of the form
*key***=***value*, where the following keys are required:

- \fBip
Item "ip"
The sender \s-1IP\s0 address.

- \fBsender
Item "sender"
The envelope sender address (from the \s-1SMTP\s0 \f(CW\*(C`MAIL FROM\*(C' command).

- \fBhelo
Item "helo"
The envelope sender hostname (from the \s-1SMTP\s0 \f(CW\*(C`HELO\*(C' command).

## RESPONSE

Header "RESPONSE"
spfd responds to query requests with similar series of lines of the form
*key***=***value*.  The most important response keys are:

- \fBresult
Item "result"
The result of the \s-1SPF\s0 query:

> 
- \fIpass
Item "pass"
The specified \s-1IP\s0 address is an authorized mailer for the sender domain/address.

- \fIfail
Item "fail"
The specified \s-1IP\s0 address is not an authorized mailer for the sender
domain/address.

- \fIsoftfail
Item "softfail"
The specified \s-1IP\s0 address is not an authorized mailer for the sender
domain/address, however the domain is still in the process of transitioning to
\s-1SPF.\s0

- \fIneutral
Item "neutral"
The sender domain makes no assertion about the status of the \s-1IP\s0 address.

- \fIunknown
Item "unknown"
The sender domain has a syntax error in its \s-1SPF\s0 record.

- \fIerror
Item "error"
A temporary \s-1DNS\s0 error occurred while resolving the sender policy.  Try again
later.

- \fInone
Item "none"
There is no \s-1SPF\s0 record for the sender domain.



> 


- \fBsmtp_comment
Item "smtp_comment"
The text that should be included in the receiver's \s-1SMTP\s0 response.

- \fBheader_comment
Item "header_comment"
The text that should be included as a comment in the message's \f(CW\*(C`Received-SPF:\*(C'
header.

- \fBspf_record
Item "spf_record"
The \s-1SPF\s0 record of the envelope sender domain.

For the description of other response keys see Mail::SPF::Query.

For more information on \s-1SPF\s0 see <http://www.openspf.org>.

## EXAMPLE

Header "EXAMPLE"
A running spfd could be tested using the \f(CW\*(C`netcat\*(C' utility like this:

.Vb 11
    $ echo -e "ip=11.22.33.44\\nsender=user@pobox.com\\nhelo=spammer.example.net\\n" | nc localhost 5970
    result=neutral
    smtp_comment=Please see http://spf.pobox.com/why.html?sender=user%40pobox.com&ip=11.22.33.44&receiver=localhost
    header_comment=localhost: 11.22.33.44 is neither permitted nor denied by domain of user@pobox.com
    guess=neutral
    smtp_guess=
    header_guess=
    guess_tf=neutral
    smtp_tf=
    header_tf=
    spf_record=v=spf1 ?all
.Ve

## SEE ALSO

Header "SEE ALSO"
Mail::SPF::Query, <http://www.openspf.org>

## AUTHORS

Header "AUTHORS"
This version of **spfd** was written by Meng Weng Wong <mengwong+spf@pobox.com>.
Improved argument parsing was added by Julian Mehnle <julian@mehnle.net>.

This man-page was written by Julian Mehnle <julian@mehnle.net>.
