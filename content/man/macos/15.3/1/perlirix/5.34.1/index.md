+++
date = "2022-02-19"
operating_system_version = "15.3"
detected_package_version = "5.34.1"
author = "None Specified"
manpage_name = "perlirix"
manpage_section = "1"
manpage_format = "troff"
title = "perlirix(1)"
description = "This document describes various features of Irix that will affect how Perl version 5 (hereafter just Perl) is compiled and/or runs. Use         sh Configure -Dcc=*(Aqcc -n32*(Aq to compile Perl 32-bit.  Dont bother with -n32 unless you have 7.1 ..."
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLIRIX 1"
PERLIRIX 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlirix - Perl version 5 on Irix systems

## DESCRIPTION

Header "DESCRIPTION"
This document describes various features of Irix that will affect how Perl
version 5 (hereafter just Perl) is compiled and/or runs.

### Building 32-bit Perl in Irix

Subsection "Building 32-bit Perl in Irix"
Use

.Vb 1
        sh Configure -Dcc=\*(Aqcc -n32\*(Aq
.Ve

to compile Perl 32-bit.  Don't bother with -n32 unless you have 7.1
or later compilers (use cc -version to check).

(Building 'cc -n32' is the default.)

### Building 64-bit Perl in Irix

Subsection "Building 64-bit Perl in Irix"
Use

.Vb 1
        sh Configure -Dcc=\*(Aqcc -64\*(Aq -Duse64bitint
.Ve

This requires require a 64-bit \s-1MIPS CPU\s0 (R8000, R10000, ...)

You can also use

.Vb 1
        sh Configure -Dcc=\*(Aqcc -64\*(Aq -Duse64bitall
.Ve

but that makes no difference compared with the -Duse64bitint because
of the \f(CW\*(C`cc -64\*(C'.

You can also do

.Vb 1
        sh Configure -Dcc=\*(Aqcc -n32\*(Aq -Duse64bitint
.Ve

to use long longs for the 64-bit integer type, in case you don't
have a 64-bit \s-1CPU.\s0

If you are using gcc, just

.Vb 1
        sh Configure -Dcc=gcc -Duse64bitint
.Ve

should be enough, the Configure should automatically probe for the
correct 64-bit settings.

### About Compiler Versions of Irix

Subsection "About Compiler Versions of Irix"
Some Irix cc versions, e.g. 7.3.1.1m (try cc -version) have been known
to have issues (coredumps) when compiling perl.c.  If you've used
-OPT:fast_io=ON and this happens, try removing it.  If that fails, or
you didn't use that, then try adjusting other optimization options
(-LNO, -INLINE, -O3 to -O2, et cetera).  The compiler bug has been
reported to \s-1SGI.\s0  (Allen Smith <easmith@beatrice.rutgers.edu>)

### Linker Problems in Irix

Subsection "Linker Problems in Irix"
If you get complaints about so_locations then search in the file
hints/irix_6.sh for \*(L"lddflags\*(R" and do the suggested adjustments.
(David Billinghurst <David.Billinghurst@riotinto.com.au>)

### Malloc in Irix

Subsection "Malloc in Irix"
Do not try to use Perl's malloc, this will lead into very mysterious
errors (especially with -Duse64bitall).

### Building with threads in Irix

Subsection "Building with threads in Irix"
Run Configure with -Duseithreads which will configure Perl with
the Perl 5.8.0 \*(L"interpreter threads\*(R", see threads.

For Irix 6.2 with perl threads, you have to have the following
patches installed:

.Vb 5
        1404 Irix 6.2 Posix 1003.1b man pages
        1645 Irix 6.2 & 6.3 POSIX header file updates
        2000 Irix 6.2 Posix 1003.1b support modules
        2254 Pthread library fixes
        2401 6.2 all platform kernel rollup
.Ve

**\s-1IMPORTANT\s0**: Without patch 2401, a kernel bug in Irix 6.2 will cause
your machine to panic and crash when running threaded perl.  Irix 6.3
and later are okay.

.Vb 2
    Thanks to Hannu Napari <Hannu.Napari@hut.fi> for the IRIX
    pthreads patches information.
.Ve

### Irix 5.3

Subsection "Irix 5.3"
While running Configure and when building, you are likely to get
quite a few of these warnings:

.Vb 3
  ld:
  The shared object /usr/lib/libm.so did not resolve any symbols.
        You may want to remove it from your link line.
.Ve

Ignore them: in \s-1IRIX 5.3\s0 there is no way to quieten ld about this.

During compilation you will see this warning from toke.c:

.Vb 3
  uopt: Warning: Perl_yylex: this procedure not optimized because it
        exceeds size threshold; to optimize this procedure, use -Olimit
        option with value >= 4252.
.Ve

Ignore the warning.

In \s-1IRIX 5.3\s0 and with Perl 5.8.1 (Perl 5.8.0 didn't compile in \s-1IRIX 5.3\s0)
the following failures are known.

.Vb 8
 Failed Test                  Stat Wstat Total Fail  Failed  List of Failed
 -----------------------------------------------------------------------
 ../ext/List/Util/t/shuffle.t    0   139    ??   ??       %  ??
 ../lib/Math/Trig.t            255 65280    29   12  41.38%  24-29
 ../lib/sort.t                   0   138   119   72  60.50%  48-119
 56 tests and 474 subtests skipped.
 Failed 3/811 test scripts, 99.63% okay. 78/75813 subtests failed,
    99.90% okay.
.Ve

They are suspected to be compiler errors (at least the shuffle.t
failure is known from some \s-1IRIX 6\s0 setups) and math library errors
(the Trig.t failure), but since \s-1IRIX 5\s0 is long since end-of-lifed,
further fixes for the \s-1IRIX\s0 are unlikely.  If you can get gcc for 5.3,
you could try that, too, since gcc in \s-1IRIX 6\s0 is a known workaround for
at least the shuffle.t and sort.t failures.

## AUTHOR

Header "AUTHOR"
Jarkko Hietaniemi <jhi@iki.fi>

Please report any errors, updates, or suggestions to
<https://github.com/Perl/perl5/issues>.
