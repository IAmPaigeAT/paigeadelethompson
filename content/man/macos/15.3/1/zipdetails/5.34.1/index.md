+++
operating_system_version = "15.3"
manpage_format = "troff"
manpage_section = "1"
date = "2024-12-14"
description = "Zipdetails displays information about the internal record structure of zip files. It is not concerned with displaying any details of the compressed data stored in the zip file. The program assumes prior understanding of the internal structure of a ..."
author = "None Specified"
manpage_name = "zipdetails"
keywords = ["header", "see", "also", "the", "primary", "reference", "for", "zip", "files", "is", "l", "s-1appnote", "r", "s0", "document", "available", "at", "http", "www", "pkware", "com", "documents", "casestudies", "appnote", "txt", "an", "alternative", "info-zip", "this", "from", "ftp", "org", "pub", "infozip", "doc", "f", "cw", "c", "zipinfo", "program", "that", "comes", "with", "distribution", "can", "display", "details", "of", "structure", "a", "file", "archive", "simplezip", "io", "compress", "uncompress", "unzip", "author", "paul", "marquess", "fipmqs", "cpan", "copyright", "2011-2021", "all", "rights", "reserved", "free", "software", "you", "redistribute", "it", "and", "or", "modify", "under", "same", "terms", "as", "perl", "itself"]
title = "zipdetails(1)"
operating_system = "macos"
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "ZIPDETAILS 1"
ZIPDETAILS 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

zipdetails - display the internal structure of zip files

## SYNOPSIS

Header "SYNOPSIS"
.Vb 3
    zipdetails [-v][--scan] zipfile.zip
    zipdetails -h
    zipdetails --version
.Ve

## DESCRIPTION

Header "DESCRIPTION"
Zipdetails displays information about the internal record structure of zip
files. It is not concerned with displaying any details of the compressed
data stored in the zip file.

The program assumes prior understanding of the internal structure of a Zip
file. You should have a copy of the Zip \s-1APPNOTE\s0 file at hand to help
understand the output from this program (\*(L"\s-1SEE ALSO\*(R"\s0 for details).

### Default Behaviour

Subsection "Default Behaviour"
By default the program expects to be given a well-formed zip file.  It will
navigate the Zip file by first parsing the zip central directory at the end
of the file.  If that is found, it will then walk through the zip records
starting at the beginning of the file. Any badly formed zip data structures
encountered are likely to terminate the program.

If the program finds any structural problems with the zip file it will
print a summary at the end of the output report. The set of error cases
reported is very much a work in progress, so don't rely on this feature to
find all the possible errors in a zip file. If you have suggestions for
use-cases where this could be enhanced please consider creating an
enhancement request (see \*(L"\s-1SUPPORT\*(R"\s0).

### Scan-Mode

Subsection "Scan-Mode"
If you do have a potentially corrupt zip file, particulatly where the
central directory at the end of the file is absent/incomplete, you can try
usng the \f(CW\*(C`--scan\*(C' option to search for zip records that are still present.

When Scan-mode is enabled, the program will walk the zip file from the
start blindly looking for the 4-byte signatures that preceed each of the
zip data structures. If it finds any of the recognised signatures it will
attempt to dump the associated zip record. For very large zip files, this
operation can take a long time to run.

Note that the 4-byte signatures used in zip files can sometimes match with
random data stored in the zip file, so care is needed interpreting the
results.

### \s-1OPTIONS\s0

Subsection "OPTIONS"

- -h
Item "-h"
Display help

- --scan
Item "--scan"
Walk the zip file loking for possible zip records. Can be error-prone.
See \*(L"Scan-Mode\*(R"

- -v
Item "-v"
Enable Verbose mode. See \*(L"Verbose Output\*(R".

- --version
Item "--version"
Display version number of the program and exit.

### Default Output

Subsection "Default Output"
By default zipdetails will output the details of the zip file in three
columns.

- Column 1
Item "Column 1"
This contains the offset from the start of the file in hex.

- Column 2
Item "Column 2"
This contains a textual description of the field.

- Column 3
Item "Column 3"
If the field contains a numeric value it will be displayed in hex. Zip
stores most numbers in little-endian format - the value displayed will have
the little-endian encoding removed.
.Sp
Next, is an optional description of what the value means.

### Verbose Output

Subsection "Verbose Output"
If the \f(CW\*(C`-v\*(C' option is present, column 1 is expanded to include

- \(bu
The offset from the start of the file in hex.

- \(bu
The length of the field in hex.

- \(bu
A hex dump of the bytes in field in the order they are stored in the zip
file.

## LIMITATIONS

Header "LIMITATIONS"
The following zip file features are not supported by this program:

- \(bu
Multi-part archives.

- \(bu
The strong encryption features defined in the \*(L"\s-1APPNOTE\*(R"\s0 document.

## TODO

Header "TODO"
Error handling is a work in progress. If the program encounters a problem
reading a zip file it is likely to terminate with an unhelpful error
message.

## SUPPORT

Header "SUPPORT"
General feedback/questions/bug reports should be sent to
<https://github.com/pmqs/IO-Compress/issues> (preferred) or
<https://rt.cpan.org/Public/Dist/Display.html?Name=IO-Compress>.

## SEE ALSO

Header "SEE ALSO"
The primary reference for Zip files is the \*(L"\s-1APPNOTE\*(R"\s0 document available at
<http://www.pkware.com/documents/casestudies/APPNOTE.TXT>.

An alternative reference is the Info-Zip appnote. This is available from
<ftp://ftp.info-zip.org/pub/infozip/doc/>

The \f(CW\*(C`zipinfo\*(C' program that comes with the info-zip distribution
(<http://www.info-zip.org/>) can also display details of the structure of
a zip file.

See also Archive::Zip::SimpleZip, IO::Compress::Zip,
IO::Uncompress::Unzip.

## AUTHOR

Header "AUTHOR"
Paul Marquess *pmqs@cpan.org*.

## COPYRIGHT

Header "COPYRIGHT"
Copyright (c) 2011-2021 Paul Marquess. All rights reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.
