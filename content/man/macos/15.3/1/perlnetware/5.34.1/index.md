+++
manpage_section = "1"
title = "perlnetware(1)"
manpage_name = "perlnetware"
author = "None Specified"
date = "2022-02-19"
operating_system = "macos"
detected_package_version = "5.34.1"
operating_system_version = "15.3"
manpage_format = "troff"
description = "This file gives instructions for building Perl 5.7 and above, and also  Perl modules for NetWare. Before you start, you may want to read the s-1READMEs0 file found in the top level directory into which the Perl source code distribution was extracte..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLNETWARE 1"
PERLNETWARE 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlnetware - Perl for NetWare

## DESCRIPTION

Header "DESCRIPTION"
This file gives instructions for building Perl 5.7 and above, and also
Perl modules for NetWare. Before you start, you may want to read the
\s-1README\s0 file found in the top level directory into which the Perl source
code distribution was extracted. Make sure you read and understand
the terms under which the software is being distributed.

## BUILD

Header "BUILD"
This section describes the steps to be performed to build a Perl \s-1NLM\s0
and other associated NLMs.

### Tools & \s-1SDK\s0

Subsection "Tools & SDK"
The build requires CodeWarrior compiler and linker.  In addition,
the \*(L"NetWare \s-1SDK\*(R", \*(L"NLM &\s0 NetWare Libraries for C\*(R" and
\*(L"NetWare Server Protocol Libraries for C\*(R", all available at
<http://developer.novell.com/wiki/index.php/Category:Novell_Developer_Kit>,
are required. Microsoft Visual \*(C+ version 4.2 or later is also
required.

### Setup

Subsection "Setup"
The build process is dependent on the location of the NetWare \s-1SDK.\s0
Once the Tools & \s-1SDK\s0 are installed, the build environment has to
be setup.  The following batch files setup the environment.

- SetNWBld.bat
Item "SetNWBld.bat"
The Execution of this file takes 2 parameters as input. The first
being the NetWare \s-1SDK\s0 path, second being the path for CodeWarrior
Compiler & tools. Execution of this file sets these paths and also
sets the build type to Release by default.

- Buildtype.bat
Item "Buildtype.bat"
This is used to set the build type to debug or release. Change the
build type only after executing SetNWBld.bat
.Sp
Example:

> 
- 1.
Typing \*(L"buildtype d on\*(R" at the command prompt causes the buildtype
to be set to Debug type with D2 flag set.

- 2.
Typing \*(L"buildtype d off\*(R" or \*(L"buildtype d\*(R" at the command prompt causes
the buildtype to be set to Debug type with D1 flag set.

- 3.
Typing \*(L"buildtype r\*(R" at the command prompt sets it to Release Build type.



> 


### Make

Subsection "Make"
The make process runs only under WinNT shell.  The NetWare makefile is
located under the NetWare folder.  This makes use of miniperl.exe to
run some of the Perl scripts. To create miniperl.exe, first set the
required paths for Visual c++ compiler (specify vcvars32 location) at
the command prompt.  Then run nmake from win32 folder through WinNT
command prompt.  The build process can be stopped after miniperl.exe
is created. Then run nmake from NetWare folder through WinNT command
prompt.

Currently the following two build types are tested on NetWare:

- \(bu
\s-1USE_MULTI, USE_ITHREADS & USE_IMP_SYS\s0 defined

- \(bu
\s-1USE_MULTI & USE_IMP_SYS\s0 defined and \s-1USE_ITHREADS\s0 not defined

### Interpreter

Subsection "Interpreter"
Once miniperl.exe creation is over, run nmake from the NetWare folder.
This will build the Perl interpreter for NetWare as *perl.nlm*.
This is copied under the *Release* folder if you are doing
a release build, else will be copied under *Debug* folder for debug builds.

### Extensions

Subsection "Extensions"
The make process also creates the Perl extensions as *<Extension*.nlm>

## INSTALL

Header "INSTALL"
To install NetWare Perl onto a NetWare server, first map the Sys
volume of a NetWare server to *i:*. This is because the makefile by
default sets the drive letter to *i:*.  Type *nmake nwinstall* from
NetWare folder on a WinNT command prompt.  This will copy the binaries
and module files onto the NetWare server under *sys:\\Perl*
folder. The Perl interpreter, *perl.nlm*, is copied under
*sys:\\perl\\system* folder.  Copy this to *sys:\\system* folder.

Example: At the command prompt Type \*(L"nmake nwinstall\*(R".
          This will install NetWare Perl on the NetWare Server.
          Similarly, if you type \*(L"nmake install\*(R",
          this will cause the binaries to be installed on the local machine.
          (Typically under the c:\\perl folder)

## BUILD NEW EXTENSIONS

Header "BUILD NEW EXTENSIONS"
To build extensions other than standard extensions, NetWare Perl has
to be installed on Windows along with Windows Perl. The Perl for
Windows can be either downloaded from the \s-1CPAN\s0 site and built using
the sources, or the binaries can be directly downloaded from the
ActiveState site.  Installation can be done by invoking \fInmake
install from the NetWare folder on a WinNT command prompt after
building NetWare Perl by following steps given above.  This will copy
all the *.pm files and other required files.  Documentation files are
not copied.  Thus one must first install Windows Perl, Then install
NetWare Perl.

Once this is done, do the following to build any extension:

- \(bu
Change to the extension directory where its source files are present.

- \(bu
Run the following command at the command prompt:
.Sp
.Vb 1
    perl -II<path to NetWare lib dir> -II<path to lib> Makefile.pl
.Ve
.Sp
Example:
.Sp
.Vb 2
    perl -Ic:/perl/5.6.1/lib/NetWare-x86-multi-thread           \\
                                -Ic:\\perl\\5.6.1\\lib MakeFile.pl
.Ve
.Sp
or
.Sp
.Vb 2
    perl -Ic:/perl/5.8.0/lib/NetWare-x86-multi-thread           \\
                                -Ic:\\perl\\5.8.0\\lib MakeFile.pl
.Ve

- \(bu
nmake

- \(bu
nmake install
.Sp
Install will copy the files into the Windows machine where NetWare
Perl is installed and these files may have to be copied to the NetWare
server manually. Alternatively, pass *INSTALLSITELIB=i:\\perl\\lib* as
an input to makefile.pl above. Here *i:* is the mapped drive to the
sys: volume of the server where Perl on NetWare is installed. Now
typing *nmake install*, will copy the files onto the NetWare server.
.Sp
Example: You can execute the following on the command prompt.
.Sp
.Vb 3
  perl -Ic:/perl/5.6.1/lib/NetWare-x86-multi-thread             \\
                                -Ic:\\perl\\5.6.1\\lib MakeFile.pl
  INSTALLSITELIB=i:\\perl\\lib
.Ve
.Sp
or
.Sp
.Vb 3
  perl -Ic:/perl/5.8.0/lib/NetWare-x86-multi-thread             \\
                                -Ic:\\perl\\5.8.0\\lib MakeFile.pl
  INSTALLSITELIB=i:\\perl\\lib
.Ve

- \(bu
Note: Some modules downloaded from \s-1CPAN\s0 may require NetWare related
\s-1API\s0 in order to build on NetWare.  Other modules may however build
smoothly with or without minor changes depending on the type of
module.

## ACKNOWLEDGEMENTS

Header "ACKNOWLEDGEMENTS"
The makefile for Win32 is used as a reference to create the makefile
for NetWare.  Also, the make process for NetWare port uses
miniperl.exe to run scripts during the make and installation process.

## AUTHORS

Header "AUTHORS"
Anantha Kesari H Y (hyanantha@novell.com)
Aditya C (caditya@novell.com)

## DATE

Header "DATE"

- \(bu
Created - 18 Jan 2001

- \(bu
Modified - 25 June 2001

- \(bu
Modified - 13 July 2001

- \(bu
Modified - 28 May 2002
