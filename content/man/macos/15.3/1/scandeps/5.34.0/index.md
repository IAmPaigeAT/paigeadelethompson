+++
operating_system = "macos"
keywords = ["header", "see", "also", "module", "scandeps", "cpanplus", "backend", "s-1par", "s0", "acknowledgments", "simon", "cozens", "for", "suggesting", "this", "script", "to", "be", "written", "authors", "audrey", "tang", "autrijus", "org", "copyright", "2003", "2004", "2005", "2006", "by", "program", "is", "free", "software", "you", "can", "redistribute", "it", "and", "or", "modify", "under", "the", "same", "terms", "as", "perl", "itself", "http", "www", "com", "misc", "artistic", "html"]
title = "scandeps(1)"
manpage_section = "1"
date = "2019-01-15"
detected_package_version = "5.34.0"
manpage_format = "troff"
description = "scandeps.pl is a simple-minded utility that prints out the f(CW*(C`PREREQ_PM*(C section needed by modules. If the option f(CW*(C`-T*(C is specified and you have s-1CPANPLUSs0 installed, modules that are part of an earlier modules distribution w..."
manpage_name = "scandeps"
operating_system_version = "15.3"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "SCANDEPS 1"
SCANDEPS 1 "2019-01-15" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

scandeps.pl - Scan file prerequisites

## SYNOPSIS

Header "SYNOPSIS"
.Vb 6
    % scandeps.pl *.pm          # Print PREREQ_PM section for *.pm
    % scandeps.pl -e \*(AqSTRING\*(Aq   # Scan an one-liner
    % scandeps.pl -B *.pm       # Include core modules
    % scandeps.pl -V *.pm       # Show autoload/shared/data files
    % scandeps.pl -R *.pm       # Don\*(Aqt recurse
    % scandeps.pl -C CACHEFILE  # use CACHEFILE to cache dependencies
.Ve

## DESCRIPTION

Header "DESCRIPTION"
*scandeps.pl* is a simple-minded utility that prints out the
\f(CW\*(C`PREREQ_PM\*(C' section needed by modules.

If the option \f(CW\*(C`-T\*(C' is specified and
you have **\s-1CPANPLUS\s0** installed, modules that are part of an
earlier module's distribution with be denoted with \f(CW\*(C`S\*(C'; modules
without a distribution name on \s-1CPAN\s0 are marked with \f(CW\*(C`?\*(C'.

Also, if the \f(CW\*(C`-B\*(C' option is specified, module belongs to a perl
distribution on \s-1CPAN\s0 (and thus uninstallable by \f(CW\*(C`CPAN.pm\*(C' or
\f(CW\*(C`CPANPLUS.pm\*(C') are marked with \f(CW\*(C`C\*(C'.

Finally, modules that has loadable shared object files (usually
needing a compiler to install) are marked with \f(CW\*(C`X\*(C'; with the
\f(CW\*(C`-V\*(C' flag, those files (and all other files found) will be listed
before the main output. Additionally, all module files that the
scanned code depends on but were not found (and thus not scanned
recursively) are listed. These may include genuinely missing
modules or false positives. That means, modules your code does
not depend on (on this particular platform) but that were picked
up by the heuristic anyway.

## OPTIONS

Header "OPTIONS"

- \fB-e, \fB--eval=\fI\s-1STRING\s0
Item "-e, --eval=STRING"
Scan *\s-1STRING\s0* as a string containing perl code.

- \fB-c, \fB--compile
Item "-c, --compile"
Compiles the code and inspects its \f(CW%INC, in addition to static scanning.

- \fB-x, \fB--execute
Item "-x, --execute"
Executes the code and inspects its \f(CW%INC, in addition to static scanning.
You may use **--xargs** to specify \f(CW@ARGV when executing the code.

- \fB--xargs=\fI\s-1STRING\s0
Item "--xargs=STRING"
If **-x** is given, splits the \f(CW\*(C`STRING\*(C' using the function
\f(CW\*(C`shellwords\*(C' from Text::ParseWords and passes the result
as \f(CW@ARGV when executing the code.

- \fB-B, \fB--bundle
Item "-B, --bundle"
Include core modules in the output and the recursive search list.

- \fB-R, \fB--no-recurse
Item "-R, --no-recurse"
Only show dependencies found in the files listed and do not recurse.

- \fB-V, \fB--verbose
Item "-V, --verbose"
Verbose mode: Output all files found during the process;
show dependencies between modules and availability.
.Sp
Additionally, warns of any missing dependencies. If you find missing
dependencies that aren't really dependencies, you have probably found
false positives.

- \fB-C, \fB--cachedeps=\fI\s-1CACHEFILE\s0
Item "-C, --cachedeps=CACHEFILE"
Use \s-1CACHEFILE\s0 to speed up the scanning process by caching dependencies.
Creates \s-1CACHEFILE\s0 if it does not exist yet.

- \fB-T, \fB--modtree
Item "-T, --modtree"
Retrieves module information from \s-1CPAN\s0 if you have **\s-1CPANPLUS\s0** installed.

## SEE ALSO

Header "SEE ALSO"
Module::ScanDeps, CPANPLUS::Backend, \s-1PAR\s0

## ACKNOWLEDGMENTS

Header "ACKNOWLEDGMENTS"
Simon Cozens, for suggesting this script to be written.

## AUTHORS

Header "AUTHORS"
Audrey Tang <autrijus@autrijus.org>

## COPYRIGHT

Header "COPYRIGHT"
Copyright 2003, 2004, 2005, 2006 by Audrey Tang <autrijus@autrijus.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See <http://www.perl.com/perl/misc/Artistic.html>
