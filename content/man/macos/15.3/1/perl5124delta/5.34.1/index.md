+++
title = "perl5124delta(1)"
manpage_section = "1"
detected_package_version = "5.34.1"
operating_system_version = "15.3"
author = "None Specified"
manpage_name = "perl5124delta"
date = "2022-02-19"
description = "This document describes differences between the 5.12.3 release and the 5.12.4 release. If you are upgrading from an earlier release such as 5.12.2, first read perl5123delta, which describes differences between 5.12.2 and 5.12.3. The major changes ..."
operating_system = "macos"
manpage_format = "troff"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5124DELTA 1"
PERL5124DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5124delta - what is new for perl v5.12.4

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.12.3 release and
the 5.12.4 release.

If you are upgrading from an earlier release such as 5.12.2, first read
perl5123delta, which describes differences between 5.12.2
and 5.12.3. The major changes made in 5.12.0 are described in perl5120delta.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.12.3. If any
exist, they are bugs and reports are welcome.

## Selected Bug Fixes

Header "Selected Bug Fixes"
When strict \*(L"refs\*(R" mode is off, \f(CW\*(C`%\{...\}\*(C' in rvalue context returns
\f(CW\*(C`undef\*(C' if its argument is undefined.  An optimisation introduced in Perl
5.12.0 to make \f(CW\*(C`keys %\{...\}\*(C' faster when used as a boolean did not take
this into account, causing \f(CW\*(C`keys %\{+undef\}\*(C' (and \f(CW\*(C`keys %$foo\*(C' when
\f(CW$foo is undefined) to be an error, which it should be so in strict
mode only [perl #81750].

\f(CW\*(C`lc\*(C', \f(CW\*(C`uc\*(C', \f(CW\*(C`lcfirst\*(C', and \f(CW\*(C`ucfirst\*(C' no longer return untainted strings
when the argument is tainted. This has been broken since perl 5.8.9
[perl #87336].

Fixed a case where it was possible that a freed buffer may have been read
from when parsing a here document.

## Modules and Pragmata

Header "Modules and Pragmata"
Module::CoreList has been upgraded from version 2.43 to 2.50.

## Testing

Header "Testing"
The *cpan/CGI/t/http.t* test script has been fixed to work when the
environment has HTTPS_* environment variables, such as \s-1HTTPS_PROXY.\s0

## Documentation

Header "Documentation"
Updated the documentation for **rand()** in perlfunc to note that it is not
cryptographically secure.

## Platform Specific Notes

Header "Platform Specific Notes"

- Linux
Item "Linux"
Support Ubuntu 11.04's new multi-arch library layout.

## Acknowledgements

Header "Acknowledgements"
Perl 5.12.4 represents approximately 5 months of development since
Perl 5.12.3 and contains approximately 200 lines of changes across
11 files from 8 authors.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers.  The following people are known to
have contributed the improvements that became Perl 5.12.4:

Andy Dougherty, David Golden, David Leadbeater, Father Chrysostomos,
Florian Ragwitz, Jesse Vincent, Leon Brocard, Zsba\*'n Ambrus.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the **perlbug**
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes all the core committers, who be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for
security issues in the Perl core, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
