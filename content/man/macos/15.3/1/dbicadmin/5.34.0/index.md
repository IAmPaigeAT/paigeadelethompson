+++
operating_system_version = "15.3"
title = "dbicadmin(1)"
description = "None Specified"
manpage_section = "1"
manpage_name = "dbicadmin"
detected_package_version = "5.34.0"
operating_system = "macos"
date = "2018-01-29"
manpage_format = "troff"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "DBICADMIN 1"
DBICADMIN 1 "2018-01-29" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

dbicadmin - utility for administrating DBIx::Class schemata

## SYNOPSIS

Header "SYNOPSIS"
dbicadmin: [-I] [long options...]

.Vb 4
  deploy a schema to a database
  dbicadmin --schema=MyApp::Schema \\
    --connect=\*(Aq["dbi:SQLite:my.db", "", ""]\*(Aq \\
    --deploy

  update an existing record
  dbicadmin --schema=MyApp::Schema --class=Employee \\
    --connect=\*(Aq["dbi:SQLite:my.db", "", ""]\*(Aq \\
    --op=update --set=\*(Aq\{ "name": "New_Employee" \}\*(Aq
.Ve

## OPTIONS

Header "OPTIONS"

### Actions

Subsection "Actions"

- \fB--create
Item "--create"
Create version diffs needs preversion

- \fB--upgrade
Item "--upgrade"
Upgrade the database to the current schema

- \fB--install
Item "--install"
Install the schema version tables to an existing database

- \fB--deploy
Item "--deploy"
Deploy the schema to the database

- \fB--select
Item "--select"
Select data from the schema

- \fB--insert
Item "--insert"
Insert data into the schema

- \fB--update
Item "--update"
Update data in the schema

- \fB--delete
Item "--delete"
Delete data from the schema

- \fB--op
Item "--op"
compatibility option all of the above can be supplied as --op=<action>

- \fB--help
Item "--help"
display this help

### Arguments

Subsection "Arguments"

- \fB--config-file or \fB--config
Item "--config-file or --config"
Supply the config file for parsing by Config::Any

- \fB--connect-info
Item "--connect-info"
Supply the connect info as trailing options e.g. --connect-info dsn=<dsn> user=<user> password=<pass>

- \fB--connect
Item "--connect"
Supply the connect info as a JSON-encoded structure, e.g. an --connect=[\*(L"dsn\*(R",\*(L"user\*(R",\*(L"pass\*(R"]

- \fB--schema-class
Item "--schema-class"
The class of the schema to load

- \fB--config-stanza
Item "--config-stanza"
Where in the config to find the connection_info, supply in form MyApp::Model::DB

- \fB--resultset or \fB--resultset-class or \fB--class
Item "--resultset or --resultset-class or --class"
The resultset to operate on for data manipulation

- \fB--sql-dir
Item "--sql-dir"
The directory where sql diffs will be created

- \fB--sql-type
Item "--sql-type"
The RDBMs flavour you wish to use

- \fB--version
Item "--version"
Supply a version install

- \fB--preversion
Item "--preversion"
The previous version to diff against

- \fB--set
Item "--set"
\s-1JSON\s0 data used to perform data operations

- \fB--attrs
Item "--attrs"
\s-1JSON\s0 string to be used for the second argument for search

- \fB--where
Item "--where"
\s-1JSON\s0 string to be used for the where clause of search

- \fB--force
Item "--force"
Be forceful with some operations

- \fB--trace
Item "--trace"
Turn on DBIx::Class trace output

- \fB--quiet
Item "--quiet"
Be less verbose

- \fB-I
Item "-I"
Same as perl's -I, prepended to current \f(CW@INC

## AUTHORS

Header "AUTHORS"
See \*(L"\s-1AUTHORS\*(R"\s0 in DBIx::Class

## LICENSE

Header "LICENSE"
You may distribute this code under the same terms as Perl itself
