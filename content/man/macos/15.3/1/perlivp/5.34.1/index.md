+++
manpage_format = "troff"
operating_system = "macos"
title = "perlivp(1)"
date = "2024-12-14"
detected_package_version = "5.34.1"
manpage_section = "1"
operating_system_version = "15.3"
description = "The perlivp program is set up at Perl source code build time to test the Perl version it was built under.  It can be used after running:     make install (or your platforms equivalent procedure) to verify that perl and its libraries have been ins..."
author = "None Specified"
manpage_name = "perlivp"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLIVP 1"
PERLIVP 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlivp - Perl Installation Verification Procedure

## SYNOPSIS

Header "SYNOPSIS"
**perlivp** [**-p**] [**-v**] [**-h**]

## DESCRIPTION

Header "DESCRIPTION"
The **perlivp** program is set up at Perl source code build time to test the
Perl version it was built under.  It can be used after running:

.Vb 1
    make install
.Ve

(or your platform's equivalent procedure) to verify that **perl** and its
libraries have been installed correctly.  A correct installation is verified
by output that looks like:

.Vb 2
    ok 1
    ok 2
.Ve

etc.

## OPTIONS

Header "OPTIONS"

- \fB-h help
Item "-h help"
Prints out a brief help message.

- \fB-p print preface
Item "-p print preface"
Gives a description of each test prior to performing it.

- \fB-v verbose
Item "-v verbose"
Gives more detailed information about each test, after it has been performed.
Note that any failed tests ought to print out some extra information whether
or not -v is thrown.

## DIAGNOSTICS

Header "DIAGNOSTICS"

- \(bu
print \*(L"# Perl binary '$perlpath' does not appear executable.\\n\*(R";
.Sp
Likely to occur for a perl binary that was not properly installed.
Correct by conducting a proper installation.

- \(bu
print \*(L"# Perl version '$]' installed, expected \f(CW$ivp_VERSION.\\n\*(R";
.Sp
Likely to occur for a perl that was not properly installed.
Correct by conducting a proper installation.

- \(bu
print \*(L"# Perl \\@INC directory '$_' does not appear to exist.\\n\*(R";
.Sp
Likely to occur for a perl library tree that was not properly installed.
Correct by conducting a proper installation.

- \(bu
print \*(L"# Needed module '$_' does not appear to be properly installed.\\n\*(R";
.Sp
One of the two modules that is used by perlivp was not present in the
installation.  This is a serious error since it adversely affects perlivp's
ability to function.  You may be able to correct this by performing a
proper perl installation.

- \(bu
print \*(L"# Required module '$_' does not appear to be properly installed.\\n\*(R";
.Sp
An attempt to \f(CW\*(C`eval "require $module"\*(C' failed, even though the list of
extensions indicated that it should succeed.  Correct by conducting a proper
installation.

- \(bu
print \*(L"# Unnecessary module 'bLuRfle' appears to be installed.\\n\*(R";
.Sp
This test not coming out ok could indicate that you have in fact installed
a bLuRfle.pm module or that the \f(CW\*(C`eval " require \\"$module_name.pm\\"; "\*(C'
test may give misleading results with your installation of perl.  If yours
is the latter case then please let the author know.

- \(bu
print \*(L"# file\*(R",+($#missing == 0) ? '' : 's',\*(L" missing from installation:\\n\*(R";
.Sp
One or more files turned up missing according to a run of
\f(CW\*(C`ExtUtils::Installed -> validate()\*(C' over your installation.
Correct by conducting a proper installation.

For further information on how to conduct a proper installation consult the
\s-1INSTALL\s0 file that comes with the perl source and the \s-1README\s0 file for your
platform.

## AUTHOR

Header "AUTHOR"
Peter Prymmer
