+++
operating_system_version = "15.3"
manpage_format = "troff"
date = "2022-02-19"
operating_system = "macos"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
description = "This document describes differences between the 5.14.3 release and the 5.14.4 release. If you are upgrading from an earlier release such as 5.12.0, first read perl5140delta, which describes differences between 5.12.0 and 5.14.0. No changes since ..."
author = "None Specified"
manpage_section = "1"
manpage_name = "perl5144delta"
title = "perl5144delta(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5144DELTA 1"
PERL5144DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5144delta - what is new for perl v5.14.4

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.14.3 release and
the 5.14.4 release.

If you are upgrading from an earlier release such as 5.12.0, first read
perl5140delta, which describes differences between 5.12.0 and
5.14.0.

## Core Enhancements

Header "Core Enhancements"
No changes since 5.14.0.

## Security

Header "Security"
This release contains one major, and medium, and a number of minor
security fixes.  The latter are included mainly to allow the test suite to
pass cleanly with the clang compiler's address sanitizer facility.

### \s-1CVE-2013-1667:\s0 memory exhaustion with arbitrary hash keys

Subsection "CVE-2013-1667: memory exhaustion with arbitrary hash keys"
With a carefully crafted set of hash keys (for example arguments on a
\s-1URL\s0), it is possible to cause a hash to consume a large amount of memory
and \s-1CPU,\s0 and thus possibly to achieve a Denial-of-Service.

This problem has been fixed.

### memory leak in Encode

Subsection "memory leak in Encode"
The \s-1UTF-8\s0 encoding implementation in Encode.xs had a memory leak which has been
fixed.

### [perl #111594] Socket::unpack_sockaddr_un heap-buffer-overflow

Subsection "[perl #111594] Socket::unpack_sockaddr_un heap-buffer-overflow"
A read buffer overflow could occur when copying \f(CW\*(C`sockaddr\*(C' buffers.
Fairly harmless.

This problem has been fixed.
.ie n .SS "[perl #111586] SDBM_File: fix off-by-one access to global "".dir"""
.el .SS "[perl #111586] SDBM_File: fix off-by-one access to global ``.dir''"
Subsection "[perl #111586] SDBM_File: fix off-by-one access to global .dir"
An extra byte was being copied for some string literals. Fairly harmless.

This problem has been fixed.

### off-by-two error in List::Util

Subsection "off-by-two error in List::Util"
A string literal was being used that included two bytes beyond the
end of the string. Fairly harmless.

This problem has been fixed.

### [perl #115994] fix segv in regcomp.\fBc:S_join_exact()

Subsection "[perl #115994] fix segv in regcomp.c:S_join_exact()"
Under debugging builds, while marking optimised-out regex nodes as type
\f(CW\*(C`OPTIMIZED\*(C', it could treat blocks of exact text as if they were nodes,
and thus \s-1SEGV.\s0 Fairly harmless.

This problem has been fixed.

### [perl #115992] PL_eval_start use-after-free

Subsection "[perl #115992] PL_eval_start use-after-free"
The statement \f(CW\*(C`local $[;\*(C', when preceded by an \f(CW\*(C`eval\*(C', and when not part
of an assignment, could crash. Fairly harmless.

This problem has been fixed.

### wrap-around with \s-1IO\s0 on long strings

Subsection "wrap-around with IO on long strings"
Reading or writing strings greater than 2**31 bytes in size could segfault
due to integer wraparound.

This problem has been fixed.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.14.0. If any
exist, they are bugs and reports are welcome.

## Deprecations

Header "Deprecations"
There have been no deprecations since 5.14.0.

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules and Pragmata

Subsection "New Modules and Pragmata"
None

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"
The following modules have just the minor code fixes as listed above in
\*(L"Security\*(R" (version numbers have not changed):

- Socket
Item "Socket"
0

- SDBM_File
Item "SDBM_File"

- List::Util
Item "List::Util"
.PD

Encode has been upgraded from version 2.42_01 to version 2.42_02.

Module::CoreList has been updated to version 2.49_06 to add data for
this release.

### Removed Modules and Pragmata

Subsection "Removed Modules and Pragmata"
None.

## Documentation

Header "Documentation"

### New Documentation

Subsection "New Documentation"
None.

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
None.

## Diagnostics

Header "Diagnostics"
No new or changed diagnostics.

## Utility Changes

Header "Utility Changes"
None

## Configuration and Compilation

Header "Configuration and Compilation"
No changes.

## Platform Support

Header "Platform Support"

### New Platforms

Subsection "New Platforms"
None.

### Discontinued Platforms

Subsection "Discontinued Platforms"
None.

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- \s-1VMS\s0
Item "VMS"
5.14.3 failed to compile on \s-1VMS\s0 due to incomplete application of a patch
series that allowed \f(CW\*(C`userelocatableinc\*(C' and \f(CW\*(C`usesitecustomize\*(C' to be
used simultaneously.  Other platforms were not affected and the problem
has now been corrected.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
In Perl 5.14.0, \f(CW\*(C`$tainted ~~ @array\*(C' stopped working properly.  Sometimes
it would erroneously fail (when \f(CW$tainted contained a string that occurs
in the array *after* the first element) or erroneously succeed (when
\f(CW\*(C`undef\*(C' occurred after the first element) [perl #93590].

## Known Problems

Header "Known Problems"
None.

## Acknowledgements

Header "Acknowledgements"
Perl 5.14.4 represents approximately 5 months of development since Perl 5.14.3
and contains approximately 1,700 lines of changes across 49 files from 12
authors.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers. The following people are known to have contributed the
improvements that became Perl 5.14.4:

Andy Dougherty, Chris 'BinGOs' Williams, Christian Hansen, Craig A. Berry,
Dave Rolsky, David Mitchell, Dominic Hargreaves, Father Chrysostomos,
Florian Ragwitz, Reini Urban, Ricardo Signes, Yves Orton.

The list above is almost certainly incomplete as it is automatically generated
from version control history. In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes all the core committers, who be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for
security issues in the Perl core, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
