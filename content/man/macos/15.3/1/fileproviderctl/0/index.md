+++
operating_system = "macos"
manpage_section = "1"
manpage_format = "troff"
keywords = ["fpck"]
operating_system_version = "15.3"
date = "Sun Feb 16 04:48:18 2025"
author = "None Specified"
description = "f[B]fileproviderctlf[R] allows you to control the fileproviderd daemon and enumerate and manipulate files. The following commands take parameters of the following forms: <provider> a (partial) provider identifier <bookmark> a string of the forma..."
manpage_name = "fileproviderctl"
detected_package_version = "0"
title = "fileproviderctl(1)"
+++

"fileproviderctl" "1" "" "" ""
.hy

## NAME


fileproviderctl - introspect file provider extensions

## SYNOPSIS


\f[B]fileproviderctl\f[R] <command> [command-options and arguments]

## DESCRIPTION


\f[B]fileproviderctl\f[R] allows you to control the fileproviderd daemon
and enumerate and manipulate files.

## GENERAL OPTIONS


The following commands take parameters of the following forms:

<provider>
a (partial) provider identifier

<bookmark>
a string of the format \[lq]fileprovider:<provider bundle
identifier>/<domain identifier>/<item identifier>\[rq]

<item>
a file URL, path or bookmark

<item id>
a simple item identifier (for commands where the provider is already
otherwise specified)

\f[B]-h\f[R], \f[B]\[en]help\f[R]
Display a friendly help message.

\f[B]enumerate\f[R], \f[B]ls\f[R]\ <provider>
Runs an interactive enumeration of the specified provider.
You can press Ctrl-C to enter an interactive command line which allows
you to execute commands on enumerated items.

\f[B]materialize\f[R]\ <item>
Causes the specified item to be written on disk, and lists the resulting
contents.

\f[B]validate\f[R]\ <provider>
Runs the validation suite against the specified provider.

\f[B]dump\f[R]
Dumps the state of the file provider subsystem, the sync engine and all
providers.

## SEE ALSO


fpck(1)
