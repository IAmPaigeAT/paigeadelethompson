+++
operating_system_version = "15.3"
title = "par(1)"
manpage_name = "par"
date = "2020-03-08"
manpage_format = "troff"
author = "None Specified"
description = "This stand-alone command offers roughly the same feature as f(CW*(C`perl -MPAR*(C, except that it takes the pre-loaded .par files via f(CW*(C`-Afoo.par*(C instead of f(CW*(C`-MPAR=foo.par*(C. Additionally, it lets you convert a s-1CPANs0 distrib..."
detected_package_version = "5.34.0"
manpage_section = "1"
keywords = ["header", "see", "also", "s-1par", "s0", "par", "dist", "parl", "pp", "authors", "audrey", "tang", "cpan", "audreyt", "org", "steffen", "mueller", "smueller", "you", "can", "write", "to", "the", "mailing", "list", "at", "perl", "or", "send", "an", "empty", "mail", "par-subscribe", "participate", "in", "discussion", "please", "submit", "bug", "reports", "bug-par-packer", "rt", "copyright", "2002-2009", "by", "neither", "this", "program", "nor", "associated", "impose", "any", "licensing", "restrictions", "on", "files", "generated", "their", "execution", "accordance", "with", "8th", "article", "of", "artistic", "license", "vb", "5", "aggregation", "package", "a", "commercial", "distribution", "is", "always", "permitted", "provided", "that", "use", "embedded", "when", "no", "overt", "attempt", "made", "make", "aqs", "interfaces", "visible", "end", "user", "such", "shall", "not", "be", "construed", "as", "ve", "therefore", "are", "absolutely", "free", "place", "resulting", "executable", "long", "packed", "3rd-party", "libraries", "available", "under", "software", "redistribute", "it", "and", "modify", "same", "terms", "itself", "fi", "s-1license"]
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "PAR 1"
PAR 1 "2020-03-08" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

par.pl - Make and run Perl Archives

## SYNOPSIS

Header "SYNOPSIS"
(Please see pp for convenient ways to make self-contained
executables, scripts or \s-1PAR\s0 archives from perl programs.)

To make a *\s-1PAR\s0 distribution* from a \s-1CPAN\s0 module distribution:

.Vb 2
    % par.pl -p                 # make a PAR dist under the current path
    % par.pl -p Foo-0.01        # assume unpacked CPAN dist in Foo-0.01/
.Ve

To manipulate a *\s-1PAR\s0 distribution*:

.Vb 6
    % par.pl -i Foo-0.01-i386-freebsd-5.8.0.par # install
    % par.pl -i http://foo.com/Foo-0.01         # auto-appends archname + perlver
    % par.pl -i cpan://AUTRIJUS/PAR-0.74        # uses CPAN author directory
    % par.pl -u Foo-0.01-i386-freebsd-5.8.0.par # uninstall
    % par.pl -s Foo-0.01-i386-freebsd-5.8.0.par # sign
    % par.pl -v Foo-0.01-i386-freebsd-5.8.0.par # verify
.Ve

To use *Hello.pm* from *./foo.par*:

.Vb 2
    % par.pl -A./foo.par -MHello
    % par.pl -A./foo -MHello    # the .par part is optional
.Ve

Same thing, but search *foo.par* in the *\f(CI@INC\fI*;

.Vb 2
    % par.pl -Ifoo.par -MHello
    % par.pl -Ifoo -MHello      # ditto
.Ve

Run *test.pl* or *script/test.pl* from *foo.par*:

.Vb 2
    % par.pl foo.par test.pl    # looks for \*(Aqmain.pl\*(Aq by default,
                                # otherwise run \*(Aqtest.pl\*(Aq
.Ve

To make a self-containing script containing a \s-1PAR\s0 file :

.Vb 2
    % par.pl -O./foo.pl foo.par
    % ./foo.pl test.pl          # same as above
.Ve

To embed the necessary non-core modules and shared objects for \s-1PAR\s0's
execution (like \f(CW\*(C`Zlib\*(C', \f(CW\*(C`IO\*(C', \f(CW\*(C`Cwd\*(C', etc), use the **-b** flag:

.Vb 2
    % par.pl -b -O./foo.pl foo.par
    % ./foo.pl test.pl          # runs anywhere with core modules installed
.Ve

If you also wish to embed *core* modules along, use the **-B** flag
instead:

.Vb 2
    % par.pl -B -O./foo.pl foo.par
    % ./foo.pl test.pl          # runs anywhere with the perl interpreter
.Ve

This is particularly useful when making stand-alone binary
executables; see pp for details.

## DESCRIPTION

Header "DESCRIPTION"
This stand-alone command offers roughly the same feature as \f(CW\*(C`perl
-MPAR\*(C', except that it takes the pre-loaded *.par* files via
\f(CW\*(C`-Afoo.par\*(C' instead of \f(CW\*(C`-MPAR=foo.par\*(C'.

Additionally, it lets you convert a \s-1CPAN\s0 distribution to a \s-1PAR\s0
distribution, as well as manipulate such distributions.  For more
information about \s-1PAR\s0 distributions, see PAR::Dist.

### Binary \s-1PAR\s0 loader (parl)

Subsection "Binary PAR loader (parl)"
If you have a C compiler, or a pre-built binary package of **\s-1PAR\s0** is
available for your platform, a binary version of **par.pl** will also be
automatically installed as **parl**.  You can use it to run *.par* files:

.Vb 3
    # runs script/run.pl in archive, uses its lib/* as libraries
    % parl myapp.par run.pl     # runs run.pl or script/run.pl in myapp.par
    % parl otherapp.pl          # also runs normal perl scripts
.Ve

However, if the *.par* archive contains either *main.pl* or
*script/main.pl*, it is used instead:

.Vb 1
    % parl myapp.par run.pl     # runs main.pl, with \*(Aqrun.pl\*(Aq as @ARGV
.Ve

Finally, the \f(CW\*(C`-O\*(C' option makes a stand-alone binary executable from a
\s-1PAR\s0 file:

.Vb 2
    % parl -B -Omyapp myapp.par
    % ./myapp                   # run it anywhere without perl binaries
.Ve

With the \f(CW\*(C`--par-options\*(C' flag, generated binaries can act as \f(CW\*(C`parl\*(C'
to pack new binaries:

.Vb 2
    % ./myapp --par-options -Omyap2 myapp.par   # identical to ./myapp
    % ./myapp --par-options -Omyap3 myap3.par   # now with different PAR
.Ve

### Stand-alone executable format

Subsection "Stand-alone executable format"
The format for the stand-alone executable is simply concatenating the
following elements:

- \(bu
The executable itself
.Sp
Either in plain-text (*par.pl*) or native executable format (*parl*
or *parl.exe*).

- \(bu
Any number of embedded files
.Sp
These are typically used for bootstrapping \s-1PAR\s0's various \s-1XS\s0 dependencies.
Each section contains:

> .ie n .IP "The magic string """"FILE""""" 4
.el .IP "The magic string ``\f(CWFILE''" 4
Item "The magic string ""FILE"""
0
.ie n .IP "Length of file name in ""pack(\*(AqN\*(Aq)"" format plus 9" 4
.el .IP "Length of file name in \f(CWpack(\*(AqN\*(Aq) format plus 9" 4
Item "Length of file name in pack(N) format plus 9"

- 8 bytes of hex-encoded \s-1CRC32\s0 of file content
Item "8 bytes of hex-encoded CRC32 of file content"
.ie n .IP "A single slash (""""/"""")" 4
.el .IP "A single slash (``\f(CW/'')" 4
Item "A single slash (""/"")"

- The file name (without path)
Item "The file name (without path)"
.ie n .IP "File length in ""pack(\*(AqN\*(Aq)"" format" 4
.el .IP "File length in \f(CWpack(\*(AqN\*(Aq) format" 4
Item "File length in pack(N) format"

- The file's content (not compressed)
Item "The file's content (not compressed)"



> 


- \(bu
.PD
One \s-1PAR\s0 file
.Sp
This is just a zip file beginning with the magic string "\f(CW\*(C`PK\\003\\004\*(C'".

- \(bu
Ending section
.Sp
The pre-computed cache name.  A pack('Z40') string of the value of -T
(--tempcache) or the hash of the file, followed by \f(CW\*(C`\\0CACHE\*(C'.  The hash
of the file is calculated with Digest::SHA.
.Sp
A pack('N') number of the total length of \s-1FILE\s0 and \s-1PAR\s0 sections,
followed by a 8-bytes magic string: "\f(CW\*(C`\\012PAR.pm\\012\*(C'".

## SEE ALSO

Header "SEE ALSO"
\s-1PAR\s0, PAR::Dist, parl, pp

## AUTHORS

Header "AUTHORS"
Audrey Tang <cpan@audreyt.org>,
Steffen Mueller <smueller@cpan.org>

You can write
to the mailing list at <par@perl.org>, or send an empty mail to
<par-subscribe@perl.org> to participate in the discussion.

Please submit bug reports to <bug-par-packer@rt.cpan.org>.

## COPYRIGHT

Header "COPYRIGHT"
Copyright 2002-2009 by Audrey Tang <cpan@audreyt.org>.

Neither this program nor the associated parl program impose any
licensing restrictions on files generated by their execution, in
accordance with the 8th article of the Artistic License:

.Vb 5
    "Aggregation of this Package with a commercial distribution is
    always permitted provided that the use of this Package is embedded;
    that is, when no overt attempt is made to make this Package\*(Aqs
    interfaces visible to the end user of the commercial distribution.
    Such use shall not be construed as a distribution of this Package."
.Ve

Therefore, you are absolutely free to place any license on the resulting
executable, as long as the packed 3rd-party libraries are also available
under the Artistic License.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See *\s-1LICENSE\s0*.
