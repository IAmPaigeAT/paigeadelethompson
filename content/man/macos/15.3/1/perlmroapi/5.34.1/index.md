+++
detected_package_version = "5.34.1"
manpage_name = "perlmroapi"
title = "perlmroapi(1)"
author = "None Specified"
manpage_section = "1"
manpage_format = "troff"
operating_system = "macos"
operating_system_version = "15.3"
description = "As of Perl 5.10.1 there is a new interface for plugging and using method resolution orders other than the default (linear depth first search). The C3 method resolution order added in 5.10.0 has been re-implemented as a plugin, without changing its ..."
date = "2022-02-19"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLMROAPI 1"
PERLMROAPI 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlmroapi - Perl method resolution plugin interface

## DESCRIPTION

Header "DESCRIPTION"
As of Perl 5.10.1 there is a new interface for plugging and using method
resolution orders other than the default (linear depth first search).
The C3 method resolution order added in 5.10.0 has been re-implemented as
a plugin, without changing its Perl-space interface.

Each plugin should register itself by providing
the following structure

.Vb 7
    struct mro_alg \{
        AV *(*resolve)(pTHX_ HV *stash, U32 level);
        const char *name;
        U16 length;
        U16 kflags;
        U32 hash;
    \};
.Ve

and calling \f(CW\*(C`Perl_mro_register\*(C':

.Vb 1
    Perl_mro_register(aTHX_ &my_mro_alg);
.Ve

- resolve
Item "resolve"
Pointer to the linearisation function, described below.

- name
Item "name"
Name of the \s-1MRO,\s0 either in \s-1ISO-8859-1\s0 or \s-1UTF-8.\s0

- length
Item "length"
Length of the name.

- kflags
Item "kflags"
If the name is given in \s-1UTF-8,\s0 set this to \f(CW\*(C`HVhek_UTF8\*(C'. The value is passed
direct as the parameter *kflags* to \f(CW\*(C`hv_common()\*(C'.

- hash
Item "hash"
A precomputed hash value for the \s-1MRO\s0's name, or 0.

## Callbacks

Header "Callbacks"
The \f(CW\*(C`resolve\*(C' function is called to generate a linearised \s-1ISA\s0 for the
given stash, using this \s-1MRO.\s0 It is called with a pointer to the stash, and
a *level* of 0. The core always sets *level* to 0 when it calls your
function - the parameter is provided to allow your implementation to track
depth if it needs to recurse.

The function should return a reference to an array containing the parent
classes in order. The names of the classes should be the result of calling
\f(CW\*(C`HvENAME()\*(C' on the stash. In those cases where \f(CW\*(C`HvENAME()\*(C' returns null,
\f(CW\*(C`HvNAME()\*(C' should be used instead.

The caller is responsible for incrementing the reference count of the array
returned if it wants to keep the structure. Hence, if you have created a
temporary value that you keep no pointer to, \f(CW\*(C`sv_2mortal()\*(C' to ensure that
it is disposed of correctly. If you have cached your return value, then
return a pointer to it without changing the reference count.

## Caching

Header "Caching"
Computing MROs can be expensive. The implementation provides a cache, in
which you can store a single \f(CW\*(C`SV *\*(C', or anything that can be cast to
\f(CW\*(C`SV *\*(C', such as \f(CW\*(C`AV *\*(C'. To read your private value, use the macro
\f(CW\*(C`MRO_GET_PRIVATE_DATA()\*(C', passing it the \f(CW\*(C`mro_meta\*(C' structure from the
stash, and a pointer to your \f(CW\*(C`mro_alg\*(C' structure:

.Vb 2
    meta = HvMROMETA(stash);
    private_sv = MRO_GET_PRIVATE_DATA(meta, &my_mro_alg);
.Ve

To set your private value, call \f(CW\*(C`Perl_mro_set_private_data()\*(C':

.Vb 1
    Perl_mro_set_private_data(aTHX_ meta, &c3_alg, private_sv);
.Ve

The private data cache will take ownership of a reference to private_sv,
much the same way that \f(CW\*(C`hv_store()\*(C' takes ownership of a reference to the
value that you pass it.

## Examples

Header "Examples"
For examples of \s-1MRO\s0 implementations, see \f(CW\*(C`S_mro_get_linear_isa_c3()\*(C'
and the \f(CW\*(C`BOOT:\*(C' section of *ext/mro/mro.xs*, and
\f(CW\*(C`S_mro_get_linear_isa_dfs()\*(C' in *mro_core.c*

## AUTHORS

Header "AUTHORS"
The implementation of the C3 \s-1MRO\s0 and switchable MROs within the perl core was
written by Brandon L Black. Nicholas Clark created the pluggable interface,
refactored Brandon's implementation to work with it, and wrote this document.
