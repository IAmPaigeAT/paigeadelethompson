+++
operating_system = "macos"
detected_package_version = "2.0"
operating_system_version = "15.3"
manpage_section = "1"
manpage_format = "troff"
title = "ppdc(1)"
author = "None Specified"
description = "ppdc compiles PPDC source files into one or more PPD files. This program is deprecated and will be removed in a future release of CUPS. ppdc supports the following options: -D name[=value] Sets the named variable for use in the source file. It is..."
keywords = ["ppdhtml", "1", "ppdi", "ppdmerge", "ppdpo", "ppdcfile", "5", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
date = "Sun Feb 16 04:48:19 2025"
manpage_name = "ppdc"
+++

ppdc 1 "CUPS" "26 April 2019" "Apple Inc."

## NAME

ppdc - cups ppd compiler (deprecated)

## SYNOPSIS

ppdc
[
**-D **name[**=**value]
] [
-I
include-directory
] [
-c
message-catalog
] [
-d
output-directory
] [
-l
language(s)
] [
-m
] [
-t
] [
-v
] [
-z
] [
--cr
] [
--crlf
] [
--lf
]
source-file

## DESCRIPTION

**ppdc** compiles PPDC source files into one or more PPD files.
**This program is deprecated and will be removed in a future release of CUPS.**

## OPTIONS

**ppdc** supports the following options:

**-D **name[**=**value]
Sets the named variable for use in the source file.
It is equivalent to using the *#define* directive in the source file.

**-I **include-directory
Specifies an alternate include directory.
Multiple *-I* options can be supplied to add additional directories.

**-c **message-catalog
Specifies a single message catalog file in GNU gettext (filename.po) or Apple strings (filename.strings) format to be used for localization.

**-d **output-directory
Specifies the output directory for PPD files.
The default output directory is "ppd".

**-l **language(s)
Specifies one or more languages to use when localizing the PPD file(s).
The default language is "en" (English).
Separate multiple languages with commas, for example "de_DE,en_UK,es_ES,es_MX,es_US,fr_CA,fr_FR,it_IT" will create PPD files with German, UK English, Spanish (Spain, Mexico, and US), French (France and Canada), and Italian languages in each file.

-m
Specifies that the output filename should be based on the ModelName value instead of FileName or PCFilenName.

-t
Specifies that PPD files should be tested instead of generated.

-v
Specifies verbose output, basically a running status of which files are being loaded or written.
-z
Generates compressed PPD files (filename.ppd.gz).
The default is to generate uncompressed PPD files.

**--cr**

**--crlf**

**--lf**
Specifies the line ending to use - carriage return, carriage return and line feed, or line feed alone.
The default is to use the line feed character alone.

## NOTES

PPD files are deprecated and will no longer be supported in a future feature release of CUPS.
Printers that do not support IPP can be supported using applications such as
ippeveprinter (1).

## SEE ALSO

ppdhtml (1),
ppdi (1),
ppdmerge (1),
ppdpo (1),
ppdcfile (5),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
