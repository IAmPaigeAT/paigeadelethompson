+++
manpage_name = "pcap-config"
author = ", 1988, 1989, 1990, 1991, 1992, 1994, 1995, 1996, 1997"
detected_package_version = "0"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_section = "1"
description = "When run with the option, writes to the standard output the compiler flags required to include libpcaps header files. When run with the option, writes to the standard output the and linker flags required to link with libpcap, including flags..."
date = "2015-01-01"
operating_system = "macos"
title = "pcap-config(1)"
keywords = ["pcap", "3pcap"]
+++

PCAP-CONFIG 1 "15 February 2015"

## NAME

pcap-config - write libpcap compiler and linker flags to standard output

## SYNOPSIS

.na
pcap-config
[
--static
]
[
--cflags | --libs | --additional-libs
]

## DESCRIPTION


When run with the
--cflags
option,
pcap-config
writes to the standard output the
-I
compiler flags required to include libpcap's header files.
When run with the
--libs
option,
pcap-config
writes to the standard output the
-L
and
-l
linker flags required to link with libpcap, including
-l
flags for libraries required by libpcap.
When run with the
--additional-libs
option,
pcap-config
writes to the standard output the
-L
and
-l
flags for libraries required by libpcap, but not the
-lpcap
flag to link with libpcap itself.

By default, it writes flags appropriate for compiling with a
dynamically-linked version of libpcap; the
--static
flag causes it to write flags appropriate for compiling with a
statically-linked version of libpcap.

## SEE ALSO

pcap (3PCAP)
