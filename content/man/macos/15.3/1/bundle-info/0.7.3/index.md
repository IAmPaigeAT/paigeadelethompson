+++
date = "Sun Feb 16 04:48:19 2025"
operating_system_version = "15.3"
manpage_name = "bundle-info"
manpage_section = "1"
detected_package_version = "0.7.3"
title = "bundle-info(1)"
description = "Print the basic information about the provided GEM such as homepage, version, path and summary. --path Print the path of the given gem..."
operating_system = "macos"
manpage_format = "troff"
author = "None Specified"
+++

.
"BUNDLE-INFO" "1" "November 2018" "" ""
.

## NAME

**bundle-info** - Show information for the given gem in your bundle
.

## SYNOPSIS

**bundle info** [GEM] [--path]
.

## DESCRIPTION

Print the basic information about the provided GEM such as homepage, version, path and summary\.
.

## OPTIONS

.

**--path**
Print the path of the given gem

