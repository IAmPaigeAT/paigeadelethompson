+++
author = "None Specified"
manpage_section = "1"
manpage_name = "perlcheat"
manpage_format = "troff"
detected_package_version = "5.34.1"
date = "2022-02-19"
keywords = ["header", "see", "also", "bu", "4", "https", "perlmonks", "org", "node_id", "216602", "the", "original", "s-1pm", "s0", "post", "238031", "damian", "conway", "s", "raku", "version", "juerd", "nl", "site", "plp", "perlcheat", "home", "of", "perl", "cheat", "sheet"]
description = "This cheat sheet is a handy reference, meant for beginning Perl programmers. Not everything is mentioned, but 195 features may already be overwhelming.   CONTEXTS  SIGILS  ref        ARRAYS        HASHES   void      $scalar SCALAR     @array    ..."
title = "perlcheat(1)"
operating_system_version = "15.3"
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLCHEAT 1"
PERLCHEAT 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlcheat - Perl 5 Cheat Sheet

## DESCRIPTION

Header "DESCRIPTION"
This 'cheat sheet' is a handy reference, meant for beginning Perl
programmers. Not everything is mentioned, but 195 features may
already be overwhelming.

### The sheet

Subsection "The sheet"
.Vb 10
  CONTEXTS  SIGILS  ref        ARRAYS        HASHES
  void      $scalar SCALAR     @array        %hash
  scalar    @array  ARRAY      @array[0, 2]  @hash\{\*(Aqa\*(Aq, \*(Aqb\*(Aq\}
  list      %hash   HASH       $array[0]     $hash\{\*(Aqa\*(Aq\}
            &sub    CODE
            *glob   GLOB       SCALAR VALUES
                    FORMAT     number, string, ref, glob, undef
  REFERENCES
  \\      reference       $$foo[1]       aka $foo->[1]
  $@%&*  dereference     $$foo\{bar\}     aka $foo->\{bar\}
  []     anon. arrayref  $\{$$foo[1]\}[2] aka $foo->[1]->[2]
  \{\}     anon. hashref   $\{$$foo[1]\}[2] aka $foo->[1][2]
  \\()    list of refs
                         SYNTAX
  OPERATOR PRECEDENCE    foreach (LIST) \{ \}     for (a;b;c) \{ \}
  ->                     while   (e) \{ \}        until (e)   \{ \}
  ++ --                  if      (e) \{ \} elsif (e) \{ \} else \{ \}
  **                     unless  (e) \{ \} elsif (e) \{ \} else \{ \}
  ! ~ \\ u+ u-            given   (e) \{ when (e) \{\} default \{\} \}
  =~ !~
  * / % x                 NUMBERS vs STRINGS  FALSE vs TRUE
  + - .                   =          =        undef, "", 0, "0"
  << >>                   +          .        anything else
  named uops              == !=      eq ne
  < > <= >= lt gt le ge   < > <= >=  lt gt le ge
  == != <=> eq ne cmp ~~  <=>        cmp
  &
  | ^             REGEX MODIFIERS       REGEX METACHARS
  &&              /i case insensitive   ^      string begin
  || //           /m line based ^$      $      str end (bfr \\n)
  .. ...          /s . includes \\n      +      one or more
  ?:              /x /xx ign. wh.space  *      zero or more
  = += last goto  /p preserve           ?      zero or one
  , =>            /a ASCII    /aa safe  \{3,7\}  repeat in range
  list ops        /l locale   /d  dual  |      alternation
  not             /u Unicode            []     character class
  and             /e evaluate /ee rpts  \\b     boundary
  or xor          /g global             \\z     string end
                  /o compile pat once   ()     capture
  DEBUG                                 (?:p)  no capture
  -MO=Deparse     REGEX CHARCLASSES     (?#t)  comment
  -MO=Terse       .   [^\\n]             (?=p)  ZW pos ahead
  -D##            \\s  whitespace        (?!p)  ZW neg ahead
  -d:Trace        \\w  word chars        (?<=p) ZW pos behind \\K
                  \\d  digits            (?<!p) ZW neg behind
  CONFIGURATION   \\pP named property    (?>p)  no backtrack
  perl -V:ivsize  \\h  horiz.wh.space    (?|p|p)branch reset
                  \\R  linebreak         (?<n>p)named capture
                  \\S \\W \\D \\H negate    \\g\{n\}  ref to named cap
                                        \\K     keep left part
  FUNCTION RETURN LISTS
  stat      localtime    caller         SPECIAL VARIABLES
   0 dev    0 second      0 package     $_    default variable
   1 ino    1 minute      1 filename    $0    program name
   2 mode   2 hour        2 line        $/    input separator
   3 nlink  3 day         3 subroutine  $\\    output separator
   4 uid    4 month-1     4 hasargs     $|    autoflush
   5 gid    5 year-1900   5 wantarray   $!    sys/libcall error
   6 rdev   6 weekday     6 evaltext    $@    eval error
   7 size   7 yearday     7 is_require  $$    process ID
   8 atime  8 is_dst      8 hints       $.    line number
   9 mtime                9 bitmask     @ARGV command line args
  10 ctime               10 hinthash    @INC  include paths
  11 blksz               3..10 only     @_    subroutine args
  12 blcks               with EXPR      %ENV  environment
.Ve

## ACKNOWLEDGEMENTS

Header "ACKNOWLEDGEMENTS"
The first version of this document appeared on Perl Monks, where several
people had useful suggestions. Thank you, Perl Monks.

A special thanks to Damian Conway, who didn't only suggest important changes,
but also took the time to count the number of listed features and make a
Raku version to show that Perl will stay Perl.

## AUTHOR

Header "AUTHOR"
Juerd Waalboer <#####@juerd.nl>, with the help of many Perl Monks.

## SEE ALSO

Header "SEE ALSO"

- \(bu
<https://perlmonks.org/?node_id=216602> - the original \s-1PM\s0 post

- \(bu
<https://perlmonks.org/?node_id=238031> - Damian Conway's Raku version

- \(bu
<https://juerd.nl/site.plp/perlcheat> - home of the Perl Cheat Sheet
