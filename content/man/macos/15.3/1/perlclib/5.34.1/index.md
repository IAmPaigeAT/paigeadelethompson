+++
detected_package_version = "5.34.1"
manpage_section = "1"
operating_system = "macos"
date = "2022-02-19"
manpage_name = "perlclib"
title = "perlclib(1)"
operating_system_version = "15.3"
manpage_format = "troff"
author = "None Specified"
description = "One thing Perl porters should note is that perl doesnt tend to use that much of the C standard library internally; youll see very little use of,  for example, the ctype.h functions in there. This is because Perl tends to reimplement or abstract s..."
keywords = ["header", "see", "also", "perlapi", "perlapio", "perlguts"]
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLCLIB 1"
PERLCLIB 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlclib - Internal replacements for standard C library functions

## DESCRIPTION

Header "DESCRIPTION"
One thing Perl porters should note is that *perl* doesn't tend to use that
much of the C standard library internally; you'll see very little use of,
for example, the *ctype.h* functions in there. This is because Perl
tends to reimplement or abstract standard library functions, so that we
know exactly how they're going to operate.

This is a reference card for people who are familiar with the C library
and who want to do things the Perl way; to tell them which functions
they ought to use instead of the more normal C functions.

### Conventions

Subsection "Conventions"
In the following tables:
.ie n .IP """t""" 3
.el .IP "\f(CWt" 3
Item "t"
is a type.
.ie n .IP """p""" 3
.el .IP "\f(CWp" 3
Item "p"
is a pointer.
.ie n .IP """n""" 3
.el .IP "\f(CWn" 3
Item "n"
is a number.
.ie n .IP """s""" 3
.el .IP "\f(CWs" 3
Item "s"
is a string.

\f(CW\*(C`sv\*(C', \f(CW\*(C`av\*(C', \f(CW\*(C`hv\*(C', etc. represent variables of their respective types.

### File Operations

Subsection "File Operations"
Instead of the *stdio.h* functions, you should use the Perl abstraction
layer. Instead of \f(CW\*(C`FILE*\*(C' types, you need to be handling \f(CW\*(C`PerlIO*\*(C'
types.  Don't forget that with the new PerlIO layered I/O abstraction
\f(CW\*(C`FILE*\*(C' types may not even be available. See also the \f(CW\*(C`perlapio\*(C'
documentation for more information about the following functions:

.Vb 1
 Instead Of:                 Use:

 stdin                       PerlIO_stdin()
 stdout                      PerlIO_stdout()
 stderr                      PerlIO_stderr()

 fopen(fn, mode)             PerlIO_open(fn, mode)
 freopen(fn, mode, stream)   PerlIO_reopen(fn, mode, perlio) (Dep-
                               recated)
 fflush(stream)              PerlIO_flush(perlio)
 fclose(stream)              PerlIO_close(perlio)
.Ve

### File Input and Output

Subsection "File Input and Output"
.Vb 1
 Instead Of:                 Use:

 fprintf(stream, fmt, ...)   PerlIO_printf(perlio, fmt, ...)

 [f]getc(stream)             PerlIO_getc(perlio)
 [f]putc(stream, n)          PerlIO_putc(perlio, n)
 ungetc(n, stream)           PerlIO_ungetc(perlio, n)
.Ve

Note that the PerlIO equivalents of \f(CW\*(C`fread\*(C' and \f(CW\*(C`fwrite\*(C' are slightly
different from their C library counterparts:

.Vb 2
 fread(p, size, n, stream)   PerlIO_read(perlio, buf, numbytes)
 fwrite(p, size, n, stream)  PerlIO_write(perlio, buf, numbytes)

 fputs(s, stream)            PerlIO_puts(perlio, s)
.Ve

There is no equivalent to \f(CW\*(C`fgets\*(C'; one should use \f(CW\*(C`sv_gets\*(C' instead:

.Vb 1
 fgets(s, n, stream)         sv_gets(sv, perlio, append)
.Ve

### File Positioning

Subsection "File Positioning"
.Vb 1
 Instead Of:                 Use:

 feof(stream)                PerlIO_eof(perlio)
 fseek(stream, n, whence)    PerlIO_seek(perlio, n, whence)
 rewind(stream)              PerlIO_rewind(perlio)

 fgetpos(stream, p)          PerlIO_getpos(perlio, sv)
 fsetpos(stream, p)          PerlIO_setpos(perlio, sv)

 ferror(stream)              PerlIO_error(perlio)
 clearerr(stream)            PerlIO_clearerr(perlio)
.Ve

### Memory Management and String Handling

Subsection "Memory Management and String Handling"
.Vb 1
 Instead Of:                    Use:

 t* p = malloc(n)               Newx(p, n, t)
 t* p = calloc(n, s)            Newxz(p, n, t)
 p = realloc(p, n)              Renew(p, n, t)
 memcpy(dst, src, n)            Copy(src, dst, n, t)
 memmove(dst, src, n)           Move(src, dst, n, t)
 memcpy(dst, src, sizeof(t))    StructCopy(src, dst, t)
 memset(dst, 0, n * sizeof(t))  Zero(dst, n, t)
 memzero(dst, 0)                Zero(dst, n, char)
 free(p)                        Safefree(p)

 strdup(p)                      savepv(p)
 strndup(p, n)                  savepvn(p, n) (Hey, strndup doesn\*(Aqt
                                               exist!)

 strstr(big, little)            instr(big, little)
 strcmp(s1, s2)                 strLE(s1, s2) / strEQ(s1, s2)
                                              / strGT(s1,s2)
 strncmp(s1, s2, n)             strnNE(s1, s2, n) / strnEQ(s1, s2, n)

 memcmp(p1, p2, n)              memNE(p1, p2, n)
 !memcmp(p1, p2, n)             memEQ(p1, p2, n)
.Ve

Notice the different order of arguments to \f(CW\*(C`Copy\*(C' and \f(CW\*(C`Move\*(C' than used
in \f(CW\*(C`memcpy\*(C' and \f(CW\*(C`memmove\*(C'.

Most of the time, though, you'll want to be dealing with SVs internally
instead of raw \f(CW\*(C`char *\*(C' strings:

.Vb 6
 strlen(s)                   sv_len(sv)
 strcpy(dt, src)             sv_setpv(sv, s)
 strncpy(dt, src, n)         sv_setpvn(sv, s, n)
 strcat(dt, src)             sv_catpv(sv, s)
 strncat(dt, src)            sv_catpvn(sv, s)
 sprintf(s, fmt, ...)        sv_setpvf(sv, fmt, ...)
.Ve

Note also the existence of \f(CW\*(C`sv_catpvf\*(C' and \f(CW\*(C`sv_vcatpvfn\*(C', combining
concatenation with formatting.

Sometimes instead of zeroing the allocated heap by using **Newxz()** you
should consider \*(L"poisoning\*(R" the data.  This means writing a bit
pattern into it that should be illegal as pointers (and floating point
numbers), and also hopefully surprising enough as integers, so that
any code attempting to use the data without forethought will break
sooner rather than later.  Poisoning can be done using the **Poison()**
macros, which have similar arguments to **Zero()**:

.Vb 4
 PoisonWith(dst, n, t, b)    scribble memory with byte b
 PoisonNew(dst, n, t)        equal to PoisonWith(dst, n, t, 0xAB)
 PoisonFree(dst, n, t)       equal to PoisonWith(dst, n, t, 0xEF)
 Poison(dst, n, t)           equal to PoisonFree(dst, n, t)
.Ve

### Character Class Tests

Subsection "Character Class Tests"
There are several types of character class tests that Perl implements.
The only ones described here are those that directly correspond to C
library functions that operate on 8-bit characters, but there are
equivalents that operate on wide characters, and \s-1UTF-8\s0 encoded strings.
All are more fully described in \*(L"Character classification\*(R" in perlapi and
\*(L"Character case changing\*(R" in perlapi.

The C library routines listed in the table below return values based on
the current locale.  Use the entries in the final column for that
functionality.  The other two columns always assume a \s-1POSIX\s0 (or C)
locale.  The entries in the \s-1ASCII\s0 column are only meaningful for \s-1ASCII\s0
inputs, returning \s-1FALSE\s0 for anything else.  Use these only when you
**know** that is what you want.  The entries in the Latin1 column assume
that the non-ASCII 8-bit characters are as Unicode defines, them, the
same as \s-1ISO-8859-1,\s0 often called Latin 1.

.Vb 1
 Instead Of:  Use for ASCII:   Use for Latin1:      Use for locale:

 isalnum(c)  isALPHANUMERIC(c) isALPHANUMERIC_L1(c) isALPHANUMERIC_LC(c)
 isalpha(c)  isALPHA(c)        isALPHA_L1(c)        isALPHA_LC(u )
 isascii(c)  isASCII(c)                             isASCII_LC(c)
 isblank(c)  isBLANK(c)        isBLANK_L1(c)        isBLANK_LC(c)
 iscntrl(c)  isCNTRL(c)        isCNTRL_L1(c)        isCNTRL_LC(c)
 isdigit(c)  isDIGIT(c)        isDIGIT_L1(c)        isDIGIT_LC(c)
 isgraph(c)  isGRAPH(c)        isGRAPH_L1(c)        isGRAPH_LC(c)
 islower(c)  isLOWER(c)        isLOWER_L1(c)        isLOWER_LC(c)
 isprint(c)  isPRINT(c)        isPRINT_L1(c)        isPRINT_LC(c)
 ispunct(c)  isPUNCT(c)        isPUNCT_L1(c)        isPUNCT_LC(c)
 isspace(c)  isSPACE(c)        isSPACE_L1(c)        isSPACE_LC(c)
 isupper(c)  isUPPER(c)        isUPPER_L1(c)        isUPPER_LC(c)
 isxdigit(c) isXDIGIT(c)       isXDIGIT_L1(c)       isXDIGIT_LC(c)

 tolower(c)  toLOWER(c)        toLOWER_L1(c)
 toupper(c)  toUPPER(c)
.Ve

To emphasize that you are operating only on \s-1ASCII\s0 characters, you can
append \f(CW\*(C`_A\*(C' to each of the macros in the \s-1ASCII\s0 column: \f(CW\*(C`isALPHA_A\*(C',
\f(CW\*(C`isDIGIT_A\*(C', and so on.

(There is no entry in the Latin1 column for \f(CW\*(C`isascii\*(C' even though there
is an \f(CW\*(C`isASCII_L1\*(C', which is identical to \f(CW\*(C`isASCII\*(C';  the
latter name is clearer.  There is no entry in the Latin1 column for
\f(CW\*(C`toupper\*(C' because the result can be non-Latin1.  You have to use
\f(CW\*(C`toUPPER_uvchr\*(C', as described in \*(L"Character case changing\*(R" in perlapi.)

### \fIstdlib.h functions

Subsection "stdlib.h functions"
.Vb 1
 Instead Of:                 Use:

 atof(s)                     Atof(s)
 atoi(s)                     grok_atoUV(s, &uv, &e)
 atol(s)                     grok_atoUV(s, &uv, &e)
 strtod(s, &p)               Strtod(s, &p)
 strtol(s, &p, n)            Strtol(s, &p, b)
 strtoul(s, &p, n)           Strtoul(s, &p, b)
.Ve

Typical use is to do range checks on \f(CW\*(C`uv\*(C' before casting:

.Vb 9
  int i; UV uv;
  char* end_ptr = input_end;
  if (grok_atoUV(input, &uv, &end_ptr)
      && uv <= INT_MAX)
    i = (int)uv;
    ... /* continue parsing from end_ptr */
  \} else \{
    ... /* parse error: not a decimal integer in range 0 .. MAX_IV */
  \}
.Ve

Notice also the \f(CW\*(C`grok_bin\*(C', \f(CW\*(C`grok_hex\*(C', and \f(CW\*(C`grok_oct\*(C' functions in
*numeric.c* for converting strings representing numbers in the respective
bases into \f(CW\*(C`NV\*(C's.  Note that **grok_atoUV()** doesn't handle negative inputs,
or leading whitespace (being purposefully strict).

Note that **strtol()** and **strtoul()** may be disguised as **Strtol()**, **Strtoul()**,
**Atol()**, **Atoul()**.  Avoid those, too.

In theory \f(CW\*(C`Strtol\*(C' and \f(CW\*(C`Strtoul\*(C' may not be defined if the machine perl is
built on doesn't actually have strtol and strtoul. But as those 2
functions are part of the 1989 \s-1ANSI C\s0 spec we suspect you'll find them
everywhere by now.

.Vb 3
 int rand()                  double Drand01()
 srand(n)                    \{ seedDrand01((Rand_seed_t)n);
                               PL_srand_called = TRUE; \}

 exit(n)                     my_exit(n)
 system(s)                   Don\*(Aqt. Look at pp_system or use my_popen.

 getenv(s)                   PerlEnv_getenv(s)
 setenv(s, val)              my_setenv(s, val)
.Ve

### Miscellaneous functions

Subsection "Miscellaneous functions"
You should not even **want** to use *setjmp.h* functions, but if you
think you do, use the \f(CW\*(C`JMPENV\*(C' stack in *scope.h* instead.

For \f(CW\*(C`signal\*(C'/\f(CW\*(C`sigaction\*(C', use \f(CW\*(C`rsignal(signo, handler)\*(C'.

## SEE ALSO

Header "SEE ALSO"
perlapi, perlapio, perlguts
