+++
title = "config_data(1)"
operating_system = "macos"
manpage_section = "1"
operating_system_version = "15.3"
detected_package_version = "5.34.0"
manpage_format = "troff"
author = "None Specified"
date = "2024-12-14"
manpage_name = "config_data"
keywords = ["header", "see", "also", "fbmodule", "build", "3", "fbperl", "1"]
description = "The f(CW*(C`config_data*(C tool provides a command-line interface to the configuration of Perl modules.  By *(Lconfiguration*(R, we mean something akin to *(Luser preferences*(R or *(Llocal settings*(R.  This is a formalization and abstracti..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "CONFIG_DATA 1"
CONFIG_DATA 1 "2024-12-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

config_data - Query or change configuration of Perl modules

## SYNOPSIS

Header "SYNOPSIS"
.Vb 3
  # Get config/feature values
  config_data --module Foo::Bar --feature bazzable
  config_data --module Foo::Bar --config magic_number

  # Set config/feature values
  config_data --module Foo::Bar --set_feature bazzable=1
  config_data --module Foo::Bar --set_config magic_number=42

  # Print a usage message
  config_data --help
.Ve

## DESCRIPTION

Header "DESCRIPTION"
The \f(CW\*(C`config_data\*(C' tool provides a command-line interface to the
configuration of Perl modules.  By \*(L"configuration\*(R", we mean something
akin to \*(L"user preferences\*(R" or \*(L"local settings\*(R".  This is a
formalization and abstraction of the systems that people like Andreas
Koenig (\f(CW\*(C`CPAN::Config\*(C'), Jon Swartz (\f(CW\*(C`HTML::Mason::Config\*(C'), Andy
Wardley (\f(CW\*(C`Template::Config\*(C'), and Larry Wall (perl's own Config.pm)
have developed independently.

The configuration system employed here was developed in the context of
\f(CW\*(C`Module::Build\*(C'.  Under this system, configuration information for a
module \f(CW\*(C`Foo\*(C', for example, is stored in a module called
\f(CW\*(C`Foo::ConfigData\*(C') (I would have called it \f(CW\*(C`Foo::Config\*(C', but that
was taken by all those other systems mentioned in the previous
paragraph...).  These \f(CW\*(C`...::ConfigData\*(C' modules contain the
configuration data, as well as publicly accessible methods for
querying and setting (yes, actually re-writing) the configuration
data.  The \f(CW\*(C`config_data\*(C' script (whose docs you are currently
reading) is merely a front-end for those methods.  If you wish, you
may create alternate front-ends.

The two types of data that may be stored are called \f(CW\*(C`config\*(C' values
and \f(CW\*(C`feature\*(C' values.  A \f(CW\*(C`config\*(C' value may be any perl scalar,
including references to complex data structures.  It must, however, be
serializable using \f(CW\*(C`Data::Dumper\*(C'.  A \f(CW\*(C`feature\*(C' is a boolean (1 or
0) value.

## USAGE

Header "USAGE"
This script functions as a basic getter/setter wrapper around the
configuration of a single module.  On the command line, specify which
module's configuration you're interested in, and pass options to get
or set \f(CW\*(C`config\*(C' or \f(CW\*(C`feature\*(C' values.  The following options are
supported:

- module
Item "module"
Specifies the name of the module to configure (required).

- feature
Item "feature"
When passed the name of a \f(CW\*(C`feature\*(C', shows its value.  The value will
be 1 if the feature is enabled, 0 if the feature is not enabled, or
empty if the feature is unknown.  When no feature name is supplied,
the names and values of all known features will be shown.

- config
Item "config"
When passed the name of a \f(CW\*(C`config\*(C' entry, shows its value.  The value
will be displayed using \f(CW\*(C`Data::Dumper\*(C' (or similar) as perl code.
When no config name is supplied, the names and values of all known
config entries will be shown.

- set_feature
Item "set_feature"
Sets the given \f(CW\*(C`feature\*(C' to the given boolean value.  Specify the value
as either 1 or 0.

- set_config
Item "set_config"
Sets the given \f(CW\*(C`config\*(C' entry to the given value.

- eval
Item "eval"
If the \f(CW\*(C`--eval\*(C' option is used, the values in \f(CW\*(C`set_config\*(C' will be
evaluated as perl code before being stored.  This allows moderately
complicated data structures to be stored.  For really complicated
structures, you probably shouldn't use this command-line interface,
just use the Perl \s-1API\s0 instead.

- help
Item "help"
Prints a help message, including a few examples, and exits.

## AUTHOR

Header "AUTHOR"
Ken Williams, kwilliams@cpan.org

## COPYRIGHT

Header "COPYRIGHT"
Copyright (c) 1999, Ken Williams.  All rights reserved.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

## SEE ALSO

Header "SEE ALSO"
**Module::Build**\|(3), **perl**\|(1).
