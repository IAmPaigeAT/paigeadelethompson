+++
operating_system = "macos"
detected_package_version = "1.0"
manpage_name = "plockstat"
date = "2007-01-01"
title = "plockstat(1)"
keywords = ["fbdtrace"]
description = "None Specified"
manpage_section = "1"
manpage_format = "troff"
author = "None Specified"
operating_system_version = "15.3"
+++

PLOCKSTAT 1 "July 2007" "1.0" ""

## NAME

plockstat - front-end to DTrace to print statistics about POSIX mutexes and read/write locks
\fB

## SYNOPSIS


```
.fam C
plockstat [-vACHV] [-n count] [-s depth] [-e secs] [-x opt[=val]]
command [arg\.\.\.]
.PP
plockstat [-vACHV] [-n count] [-s depth] [-e secs] [-x opt[=val]]
-p pid
.fam T
```


## OVERVIEW

The *plockstat* command is a front-end to DTrace that can be used to print
statistics about POSIX mutexes and read/write locks.
.br
.P
Since OS X 10.11, in order to use this, your process must be run with DYLD_LIBRARY_PATH set to
contain /usr/lib/system/introspection:


> DYLD_LIBRARY_PATH=/usr/lib/system/introspection
.br


.P
Which contains the necessary static DTrace probes.


## OPTIONS



.B
**-v**
print a message when tracing starts

.B
**-A**
trace contention and hold events (same as **-CH**)

.B
**-C**
trace contention events for mutexes and rwlocks

.B
**-H**
trace hold events for mutexes and rwlocks

.B
**-V**
print the dtrace script to run

.B
**-n** *count*
display only '*count*' entries for each event type

.B
**-s** *depth*
show stack trace upto '*depth*' entries

.B
**-e** *secs*
exit after specified seconds

.B
**-x** arg[=val]
enable a DTrace runtime option or a D compiler option

.B
**-p** *pid*
attach and trace the specified process id

## SEE ALSO

**dtrace**(1)
