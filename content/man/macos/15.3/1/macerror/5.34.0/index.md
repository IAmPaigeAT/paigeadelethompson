+++
manpage_format = "troff"
date = "2018-06-20"
title = "macerror(1)"
detected_package_version = "5.34.0"
operating_system_version = "15.3"
manpage_section = "1"
description = "None Specified"
author = "None Specified"
operating_system = "macos"
manpage_name = "macerror"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "MACERROR 1"
MACERROR 1 "2018-06-20" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

macerror

## SYNOPSIS

Header "SYNOPSIS"
% macerror -23

## DESCRIPTION

Header "DESCRIPTION"
The macerror script translates Mac error numbers into their
symbolic name and description.

## SEE ALSO

Header "SEE ALSO"
Mac::Errors

## AUTHOR

Header "AUTHOR"
Chris Nandor, pudge@pobox.com

## COPYRIGHT

Header "COPYRIGHT"
Copryright 2002, Chris Nandor, All rights reserved

You may use this under the same terms as Perl itself.
