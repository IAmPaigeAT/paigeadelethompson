+++
manpage_format = "troff"
operating_system_version = "15.3"
title = "perlplan9(1)"
date = "2022-02-19"
description = "These are a few notes describing features peculiar to Plan 9 Perl. As such, it is not intended to be a replacement for the rest of the Perl 5 documentation (which is both  copious and excellent). If you have any questions to  which you cant find ..."
author = "None Specified"
detected_package_version = "5.34.1"
manpage_name = "perlplan9"
operating_system = "macos"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLPLAN9 1"
PERLPLAN9 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlplan9 - Plan 9-specific documentation for Perl

## DESCRIPTION

Header "DESCRIPTION"
These are a few notes describing features peculiar to
Plan 9 Perl. As such, it is not intended to be a replacement
for the rest of the Perl 5 documentation (which is both
copious and excellent). If you have any questions to
which you can't find answers in these man pages, contact
Luther Huffman at lutherh@stratcom.com and we'll try to
answer them.

### Invoking Perl

Subsection "Invoking Perl"
Perl is invoked from the command line as described in
perl. Most perl scripts, however, do have a first line
such as \*(L"#!/usr/local/bin/perl\*(R". This is known as a shebang
(shell-bang) statement and tells the \s-1OS\s0 shell where to find
the perl interpreter. In Plan 9 Perl this statement should be
\*(L"#!/bin/perl\*(R" if you wish to be able to directly invoke the
script by its name.
     Alternatively, you may invoke perl with the command \*(L"Perl\*(R"
instead of \*(L"perl\*(R". This will produce Acme-friendly error
messages of the form \*(L"filename:18\*(R".

Some scripts, usually identified with a *.PL extension, are
self-configuring and are able to correctly create their own
shebang path from config information located in Plan 9
Perl. These you won't need to be worried about.

### Whats in Plan 9 Perl

Subsection "What's in Plan 9 Perl"
Although Plan 9 Perl currently only  provides static
loading, it is built with a number of useful extensions.
These include Opcode, FileHandle, Fcntl, and \s-1POSIX.\s0 Expect
to see others (and DynaLoading!) in the future.

### Whats not in Plan 9 Perl

Subsection "What's not in Plan 9 Perl"
As mentioned previously, dynamic loading isn't currently
available nor is MakeMaker. Both are high-priority items.

### Perl5 Functions not currently supported in Plan 9 Perl

Subsection "Perl5 Functions not currently supported in Plan 9 Perl"
Some, such as \f(CW\*(C`chown\*(C' and \f(CW\*(C`umask\*(C' aren't provided
because the concept does not exist within Plan 9. Others,
such as some of the socket-related functions, simply
haven't been written yet. Many in the latter category
may be supported in the future.

The functions not currently implemented include:

.Vb 5
    chown, chroot, dbmclose, dbmopen, getsockopt,
    setsockopt, recvmsg, sendmsg, getnetbyname,
    getnetbyaddr, getnetent, getprotoent, getservent,
    sethostent, setnetent, setprotoent, setservent,
    endservent, endnetent, endprotoent, umask
.Ve

There may be several other functions that have undefined
behavior so this list shouldn't be considered complete.

### Signals in Plan 9 Perl

Subsection "Signals in Plan 9 Perl"
For compatibility with perl scripts written for the Unix
environment, Plan 9 Perl uses the \s-1POSIX\s0 signal emulation
provided in Plan 9's \s-1ANSI POSIX\s0 Environment (\s-1APE\s0). Signal stacking
isn't supported. The signals provided are:

.Vb 4
    SIGHUP, SIGINT, SIGQUIT, SIGILL, SIGABRT,
    SIGFPE, SIGKILL, SIGSEGV, SIGPIPE, SIGPIPE, SIGALRM,
    SIGTERM, SIGUSR1, SIGUSR2, SIGCHLD, SIGCONT,
    SIGSTOP, SIGTSTP, SIGTTIN, SIGTTOU
.Ve

## COMPILING AND INSTALLING PERL ON PLAN 9

Header "COMPILING AND INSTALLING PERL ON PLAN 9"
\s-1WELCOME\s0 to Plan 9 Perl, brave soul!

.Vb 5
   This is a preliminary alpha version of Plan 9 Perl. Still to be
implemented are MakeMaker and DynaLoader. Many perl commands are
missing or currently behave in an inscrutable manner. These gaps will,
with perseverance and a modicum of luck, be remedied in the near
future.To install this software:
.Ve

1. Create the source directories and libraries for perl by running the
plan9/setup.rc command (i.e., located in the plan9 subdirectory).
Note: the setup routine assumes that you haven't dearchived these
files into /sys/src/cmd/perl. After running setup.rc you may delete
the copy of the source you originally detarred, as source code has now
been installed in /sys/src/cmd/perl. If you plan on installing perl
binaries for all architectures, run \*(L"setup.rc -a\*(R".

2. After making sure that you have adequate privileges to build system
software, from /sys/src/cmd/perl/5.00301 (adjust version
appropriately) run:

.Vb 1
        mk install
.Ve

If you wish to install perl versions for all architectures (68020,
mips, sparc and 386) run:

.Vb 1
        mk installall
.Ve

3. Wait. The build process will take a *long* time because perl
bootstraps itself. A 75MHz Pentium, 16MB \s-1RAM\s0 machine takes roughly 30
minutes to build the distribution from scratch.

### Installing Perl Documentation on Plan 9

Subsection "Installing Perl Documentation on Plan 9"
This perl distribution comes with a tremendous amount of
documentation. To add these to the built-in manuals that come with
Plan 9, from /sys/src/cmd/perl/5.00301 (adjust version appropriately)
run:

.Vb 1
        mk man
.Ve

To begin your reading, start with:

.Vb 1
        man perl
.Ve

This is a good introduction and will direct you towards other man
pages that may interest you.

(Note: \*(L"mk man\*(R" may produce some extraneous noise. Fear not.)

## BUGS

Header "BUGS"
\*(L"As many as there are grains of sand on all the beaches of the
world . . .\*(R" - Carl Sagan

## Revision date

Header "Revision date"
This document was revised 09-October-1996 for Perl 5.003_7.

## AUTHOR

Header "AUTHOR"
Direct questions, comments, and the unlikely bug report (ahem) direct
comments toward:

Luther Huffman, lutherh@stratcom.com,
Strategic Computer Solutions, Inc.
