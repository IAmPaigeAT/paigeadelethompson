+++
date = "2022-02-19"
author = "None Specified"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
detected_package_version = "5.34.1"
manpage_name = "perl5242delta"
operating_system = "macos"
manpage_section = "1"
operating_system_version = "15.3"
title = "perl5242delta(1)"
description = "This document describes differences between the 5.24.1 release and the 5.24.2 release. If you are upgrading from an earlier release such as 5.24.0, first read perl5241delta, which describes differences between 5.24.0 and 5.24.1. The handling of (t..."
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5242DELTA 1"
PERL5242DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5242delta - what is new for perl v5.24.2

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.24.1 release and the 5.24.2
release.

If you are upgrading from an earlier release such as 5.24.0, first read
perl5241delta, which describes differences between 5.24.0 and 5.24.1.

## Security

Header "Security"
.ie n .SS "Improved handling of '.' in @INC in base.pm"
.el .SS "Improved handling of '.' in \f(CW@INC in base.pm"
Subsection "Improved handling of '.' in @INC in base.pm"
The handling of (the removal of) \f(CW\*(Aq.\*(Aq in \f(CW@INC in base has been
improved.  This resolves some problematic behaviour in the approach taken in
Perl 5.24.1, which is probably best described in the following two threads on
the Perl 5 Porters mailing list:
<http://www.nntp.perl.org/group/perl.perl5.porters/2016/08/msg238991.html>,
<http://www.nntp.perl.org/group/perl.perl5.porters/2016/10/msg240297.html>.
.ie n .SS """Escaped"" colons and relative paths in \s-1PATH\s0"
.el .SS "``Escaped'' colons and relative paths in \s-1PATH\s0"
Subsection "Escaped colons and relative paths in PATH"
On Unix systems, Perl treats any relative paths in the \s-1PATH\s0 environment
variable as tainted when starting a new process.  Previously, it was allowing a
backslash to escape a colon (unlike the \s-1OS\s0), consequently allowing relative
paths to be considered safe if the \s-1PATH\s0 was set to something like \f(CW\*(C`/\\:.\*(C'.  The
check has been fixed to treat \f(CW\*(C`.\*(C' as tainted in that example.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
base has been upgraded from version 2.23 to 2.23_01.

- \(bu
Module::CoreList has been upgraded from version 5.20170114_24 to 5.20170715_24.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Fixed a crash with \f(CW\*(C`s///l\*(C' where it thought it was dealing with \s-1UTF-8\s0 when it
wasn't.
[\s-1GH\s0 #15543] <https://github.com/Perl/perl5/issues/15543>

## Acknowledgements

Header "Acknowledgements"
Perl 5.24.2 represents approximately 6 months of development since Perl 5.24.1
and contains approximately 2,500 lines of changes across 53 files from 18
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 960 lines of changes to 17 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.24.2:

Aaron Crane, Abigail, Aristotle Pagaltzis, Chris 'BinGOs' Williams, Dan
Collins, David Mitchell, Eric Herman, Father Chrysostomos, James E Keenan, Karl
Williamson, Lukas Mai, Renee Baecker, Ricardo Signes, Sawyer X, Stevan Little,
Steve Hay, Tony Cook, Yves Orton.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the perl bug database at
<https://rt.perl.org/> .  There may also be information at
<http://www.perl.org/> , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
