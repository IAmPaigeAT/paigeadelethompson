+++
manpage_format = "troff"
operating_system_version = "15.3"
title = "perl5200delta(1)"
manpage_section = "1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
operating_system = "macos"
detected_package_version = "5.34.1"
manpage_name = "perl5200delta"
author = "None Specified"
description = "This document describes differences between the 5.18.0 release and the 5.20.0 release. If you are upgrading from an earlier release such as 5.16.0, first read perl5180delta, which describes differences between 5.16.0 and 5.18.0. Declarative syntax..."
date = "2022-02-19"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5200DELTA 1"
PERL5200DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5200delta - what is new for perl v5.20.0

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.18.0 release and the
5.20.0 release.

If you are upgrading from an earlier release such as 5.16.0, first read
perl5180delta, which describes differences between 5.16.0 and 5.18.0.

## Core Enhancements

Header "Core Enhancements"

### Experimental Subroutine signatures

Subsection "Experimental Subroutine signatures"
Declarative syntax to unwrap argument list into lexical variables.
\f(CW\*(C`sub foo ($a,$b) \{...\}\*(C' checks the number of arguments and puts the
arguments into lexical variables.  Signatures are not equivalent to
the existing idiom of \f(CW\*(C`sub foo \{ my($a,$b) = @_; ... \}\*(C'.  Signatures
are only available by enabling a non-default feature, and generate
warnings about being experimental.  The syntactic clash with
prototypes is managed by disabling the short prototype syntax when
signatures are enabled.

See \*(L"Signatures\*(R" in perlsub for details.
.ie n .SS """sub""s now take a ""prototype"" attribute"
.el .SS "\f(CWsubs now take a \f(CWprototype attribute"
Subsection "subs now take a prototype attribute"
When declaring or defining a \f(CW\*(C`sub\*(C', the prototype can now be specified inside
of a \f(CW\*(C`prototype\*(C' attribute instead of in parens following the name.

For example, \f(CW\*(C`sub foo($$)\{\}\*(C' could be rewritten as
\f(CW\*(C`sub foo : prototype($$)\{\}\*(C'.

### More consistent prototype parsing

Subsection "More consistent prototype parsing"
Multiple semicolons in subroutine prototypes have long been tolerated and
treated as a single semicolon.  There was one case where this did not
happen.  A subroutine whose prototype begins with \*(L"*\*(R" or \*(L";*\*(R" can affect
whether a bareword is considered a method name or sub call.  This now
applies also to \*(L";;;*\*(R".

Whitespace has long been allowed inside subroutine prototypes, so
\f(CW\*(C`sub( $ $ )\*(C' is equivalent to \f(CW\*(C`sub($$)\*(C', but until now it was stripped
when the subroutine was parsed.  Hence, whitespace was *not* allowed in
prototypes set by \f(CW\*(C`Scalar::Util::set_prototype\*(C'.  Now it is permitted,
and the parser no longer strips whitespace.  This means
\f(CW\*(C`prototype &mysub\*(C' returns the original prototype, whitespace and all.
.ie n .SS """rand"" now uses a consistent random number generator"
.el .SS "\f(CWrand now uses a consistent random number generator"
Subsection "rand now uses a consistent random number generator"
Previously perl would use a platform specific random number generator, varying
between the libc **rand()**, **random()** or **drand48()**.

This meant that the quality of perl's random numbers would vary from platform
to platform, from the 15 bits of **rand()** on Windows to 48-bits on \s-1POSIX\s0
platforms such as Linux with **drand48()**.

Perl now uses its own internal **drand48()** implementation on all platforms.  This
does not make perl's \f(CW\*(C`rand\*(C' cryptographically secure.  [perl #115928]

### New slice syntax

Subsection "New slice syntax"
The new \f(CW%hash\{...\} and \f(CW%array[...] syntax returns a list of key/value (or
index/value) pairs.  See \*(L"Key/Value Hash Slices\*(R" in perldata.

### Experimental Postfix Dereferencing

Subsection "Experimental Postfix Dereferencing"
When the \f(CW\*(C`postderef\*(C' feature is in effect, the following syntactical
equivalencies are set up:

.Vb 5
  $sref->$*;  # same as $\{ $sref \}  # interpolates
  $aref->@*;  # same as @\{ $aref \}  # interpolates
  $href->%*;  # same as %\{ $href \}
  $cref->&*;  # same as &\{ $cref \}
  $gref->**;  # same as *\{ $gref \}

  $aref->$#*; # same as $#\{ $aref \}

  $gref->*\{ $slot \}; # same as *\{ $gref \}\{ $slot \}

  $aref->@[ ... ];  # same as @$aref[ ... ]  # interpolates
  $href->@\{ ... \};  # same as @$href\{ ... \}  # interpolates
  $aref->%[ ... ];  # same as %$aref[ ... ]
  $href->%\{ ... \};  # same as %$href\{ ... \}
.Ve

Those marked as interpolating only interpolate if the associated
\f(CW\*(C`postderef_qq\*(C' feature is also enabled.  This feature is **experimental** and
will trigger \f(CW\*(C`experimental::postderef\*(C'-category warnings when used, unless
they are suppressed.

For more information, consult the Postfix Dereference Syntax section of
perlref.

### Unicode 6.3 now supported

Subsection "Unicode 6.3 now supported"
Perl now supports and is shipped with Unicode 6.3 (though Perl may be
recompiled with any previous Unicode release as well).  A detailed list of
Unicode 6.3 changes is at <http://www.unicode.org/versions/Unicode6.3.0/>.
.ie n .SS "New ""\\p\{Unicode\}"" regular expression pattern property"
.el .SS "New \f(CW\\p\{Unicode\} regular expression pattern property"
Subsection "New p\{Unicode\} regular expression pattern property"
This is a synonym for \f(CW\*(C`\\p\{Any\}\*(C' and matches the set of Unicode-defined
code points 0 - 0x10FFFF.

### Better 64-bit support

Subsection "Better 64-bit support"
On 64-bit platforms, the internal array functions now use 64-bit offsets,
allowing Perl arrays to hold more than 2**31 elements, if you have the memory
available.

The regular expression engine now supports strings longer than 2**31
characters.  [perl #112790, #116907]

The functions PerlIO_get_bufsiz, PerlIO_get_cnt, PerlIO_set_cnt and
PerlIO_set_ptrcnt now have SSize_t, rather than int, return values and
parameters.
.ie n .SS """use\ locale"" now works on \s-1UTF-8\s0 locales"
.el .SS "\f(CWuse\ locale now works on \s-1UTF-8\s0 locales"
Subsection "uselocale now works on UTF-8 locales"
Until this release, only single-byte locales, such as the \s-1ISO 8859\s0
series were supported.  Now, the increasingly common multi-byte \s-1UTF-8\s0
locales are also supported.  A \s-1UTF-8\s0 locale is one in which the
character set is Unicode and the encoding is \s-1UTF-8.\s0  The \s-1POSIX\s0
\f(CW\*(C`LC_CTYPE\*(C' category operations (case changing (like \f(CW\*(C`lc()\*(C', \f(CW"\\U"),
and character classification (\f(CW\*(C`\\w\*(C', \f(CW\*(C`\\D\*(C', \f(CW\*(C`qr/[[:punct:]]/\*(C')) under
such a locale work just as if not under locale, but instead as if under
\f(CW\*(C`use\ feature\ \*(Aqunicode_strings\*(Aq\*(C', except taint rules are followed.
Sorting remains by code point order in this release.  [perl #56820].
.ie n .SS """use\ locale"" now compiles on systems without locale ability"
.el .SS "\f(CWuse\ locale now compiles on systems without locale ability"
Subsection "uselocale now compiles on systems without locale ability"
Previously doing this caused the program to not compile.  Within its
scope the program behaves as if in the \*(L"C\*(R" locale.  Thus programs
written for platforms that support locales can run on locale-less
platforms without change.  Attempts to change the locale away from the
\*(L"C\*(R" locale will, of course, fail.

### More locale initialization fallback options

Subsection "More locale initialization fallback options"
If there was an error with locales during Perl start-up, it immediately
gave up and tried to use the \f(CW"C" locale.  Now it first tries using
other locales given by the environment variables, as detailed in
\*(L"\s-1ENVIRONMENT\*(R"\s0 in perllocale.  For example, if \f(CW\*(C`LC_ALL\*(C' and \f(CW\*(C`LANG\*(C' are
both set, and using the \f(CW\*(C`LC_ALL\*(C' locale fails, Perl will now try the
\f(CW\*(C`LANG\*(C' locale, and only if that fails, will it fall back to \f(CW"C".  On
Windows machines, Perl will try, ahead of using \f(CW"C", the system
default locale if all the locales given by environment variables fail.
.ie n .SS """-DL"" runtime option now added for tracing locale setting"
.el .SS "\f(CW-DL runtime option now added for tracing locale setting"
Subsection "-DL runtime option now added for tracing locale setting"
This is designed for Perl core developers to aid in field debugging bugs
regarding locales.

### \fB-F now implies \fB-a and \fB-a implies \fB-n

Subsection "-F now implies -a and -a implies -n"
Previously **-F** without **-a** was a no-op, and **-a** without **-n** or **-p**
was a no-op, with this change, if you supply **-F** then both **-a** and **-n**
are implied and if you supply **-a** then **-n** is implied.

You can still use **-p** for its extra behaviour. [perl #116190]
.ie n .SS "$a and $b warnings exemption"
.el .SS "\f(CW$a and \f(CW$b warnings exemption"
Subsection "$a and $b warnings exemption"
The special variables \f(CW$a and \f(CW$b, used in \f(CW\*(C`sort\*(C', are now exempt from \*(L"used
once\*(R" warnings, even where \f(CW\*(C`sort\*(C' is not used.  This makes it easier for
\s-1CPAN\s0 modules to provide functions using \f(CW$a and \f(CW$b for similar purposes.
[perl #120462]

## Security

Header "Security"

### Avoid possible read of \fBfree()d memory during parsing

Subsection "Avoid possible read of free()d memory during parsing"
It was possible that **free()**d memory could be read during parsing in the unusual
circumstance of the Perl program ending with a heredoc and the last line of the
file on disk having no terminating newline character.  This has now been fixed.

## Incompatible Changes

Header "Incompatible Changes"
.ie n .SS """do"" can no longer be used to call subroutines"
.el .SS "\f(CWdo can no longer be used to call subroutines"
Subsection "do can no longer be used to call subroutines"
The \f(CW\*(C`do SUBROUTINE(LIST)\*(C' form has resulted in a deprecation warning
since Perl v5.0.0, and is now a syntax error.

### Quote-like escape changes

Subsection "Quote-like escape changes"
The character after \f(CW\*(C`\\c\*(C' in a double-quoted string (\*(L"...\*(R" or qq(...))
or regular expression must now be a printable character and may not be
\f(CW\*(C`\{\*(C'.

A literal \f(CW\*(C`\{\*(C' after \f(CW\*(C`\\B\*(C' or \f(CW\*(C`\\b\*(C' is now fatal.

These were deprecated in perl v5.14.0.

### Tainting happens under more circumstances; now conforms to documentation

Subsection "Tainting happens under more circumstances; now conforms to documentation"
This affects regular expression matching and changing the case of a
string (\f(CW\*(C`lc\*(C', \f(CW"\\U", *etc*.) within the scope of \f(CW\*(C`use locale\*(C'.
The result is now tainted based on the operation, no matter what the
contents of the string were, as the documentation (perlsec,
\*(L"\s-1SECURITY\*(R"\s0 in perllocale) indicates it should.  Previously, for the case
change operation, if the string contained no characters whose case
change could be affected by the locale, the result would not be tainted.
For example, the result of \f(CW\*(C`uc()\*(C' on an empty string or one containing
only above-Latin1 code points is now tainted, and wasn't before.  This
leads to more consistent tainting results.  Regular expression patterns
taint their non-binary results (like \f(CW$&, \f(CW$2) if and only if the
pattern contains elements whose matching depends on the current
(potentially tainted) locale.  Like the case changing functions, the
actual contents of the string being matched now do not matter, whereas
formerly it did.  For example, if the pattern contains a \f(CW\*(C`\\w\*(C', the
results will be tainted even if the match did not have to use that
portion of the pattern to succeed or fail, because what a \f(CW\*(C`\\w\*(C' matches
depends on locale.  However, for example, a \f(CW\*(C`.\*(C' in a pattern will not
enable tainting, because the dot matches any single character, and what
the current locale is doesn't change in any way what matches and what
doesn't.
.ie n .SS """\\p\{\}"", ""\\P\{\}"" matching has changed for non-Unicode code points."
.el .SS "\f(CW\\p\{\}, \f(CW\\P\{\} matching has changed for non-Unicode code points."
Subsection "p\{\}, P\{\} matching has changed for non-Unicode code points."
\f(CW\*(C`\\p\{\}\*(C' and \f(CW\*(C`\\P\{\}\*(C' are defined by Unicode only on Unicode-defined code
points (\f(CW\*(C`U+0000\*(C' through \f(CW\*(C`U+10FFFF\*(C').  Their behavior on matching
these legal Unicode code points is unchanged, but there are changes for
code points \f(CW0x110000 and above.  Previously, Perl treated the result
of matching \f(CW\*(C`\\p\{\}\*(C' and \f(CW\*(C`\\P\{\}\*(C' against these as \f(CW\*(C`undef\*(C', which
translates into \*(L"false\*(R".  For \f(CW\*(C`\\P\{\}\*(C', this was then complemented into
\*(L"true\*(R".  A warning was supposed to be raised when this happened.
However, various optimizations could prevent the warning, and the
results were often counter-intuitive, with both a match and its seeming
complement being false.  Now all non-Unicode code points are treated as
typical unassigned Unicode code points.  This generally is more
Do-What-I-Mean.  A warning is raised only if the results are arguably
different from a strict Unicode approach, and from what Perl used to do.
Code that needs to be strictly Unicode compliant can make this warning
fatal, and then Perl always raises the warning.

Details are in \*(L"Beyond Unicode code points\*(R" in perlunicode.
.ie n .SS """\\p\{All\}"" has been expanded to match all possible code points"
.el .SS "\f(CW\\p\{All\} has been expanded to match all possible code points"
Subsection "p\{All\} has been expanded to match all possible code points"
The Perl-defined regular expression pattern element \f(CW\*(C`\\p\{All\}\*(C', unused
on \s-1CPAN,\s0 used to match just the Unicode code points; now it matches all
possible code points; that is, it is equivalent to \f(CW\*(C`qr/./s\*(C'.  Thus
\f(CW\*(C`\\p\{All\}\*(C' is no longer synonymous with \f(CW\*(C`\\p\{Any\}\*(C', which continues to
match just the Unicode code points, as Unicode says it should.

### Data::Dumpers output may change

Subsection "Data::Dumper's output may change"
Depending on the data structures dumped and the settings set for
Data::Dumper, the dumped output may have changed from previous
versions.

If you have tests that depend on the exact output of Data::Dumper,
they may fail.

To avoid this problem in your code, test against the data structure
from evaluating the dumped structure, instead of the dump itself.
.ie n .SS "Locale decimal point character no longer leaks outside of ""use\ locale"" scope"
.el .SS "Locale decimal point character no longer leaks outside of \f(CWuse\ locale scope"
Subsection "Locale decimal point character no longer leaks outside of uselocale scope"
This is actually a bug fix, but some code has come to rely on the bug
being present, so this change is listed here.  The current locale that
the program is running under is not supposed to be visible to Perl code
except within the scope of a \f(CW\*(C`use\ locale\*(C'.  However, until now under
certain circumstances, the character used for a decimal point (often a
comma) leaked outside the scope.  If your code is affected by this
change, simply add a \f(CW\*(C`use\ locale\*(C'.

### Assignments of Windows sockets error codes to $! now prefer \fIerrno.h values over \fBWSAGetLastError() values

Subsection "Assignments of Windows sockets error codes to $! now prefer errno.h values over WSAGetLastError() values"
In previous versions of Perl, Windows sockets error codes as returned by
**WSAGetLastError()** were assigned to $!, and some constants such as \s-1ECONNABORTED,\s0
not in *errno.h* in \s-1VC++\s0 (or the various Windows ports of gcc) were defined to
corresponding WSAE* values to allow $! to be tested against the E* constants
exported by Errno and \s-1POSIX\s0.

This worked well until \s-1VC++ 2010\s0 and later, which introduced new E* constants
with values > 100 into *errno.h*, including some being (re)defined by perl
to WSAE* values.  That caused problems when linking \s-1XS\s0 code against other
libraries which used the original definitions of *errno.h* constants.

To avoid this incompatibility, perl now maps WSAE* error codes to E* values
where possible, and assigns those values to $!.  The E* constants exported by
Errno and \s-1POSIX\s0 are updated to match so that testing $! against them,
wherever previously possible, will continue to work as expected, and all E*
constants found in *errno.h* are now exported from those modules with their
original *errno.h* values.

In order to avoid breakage in existing Perl code which assigns WSAE* values to
$!, perl now intercepts the assignment and performs the same mapping to E*
values as it uses internally when assigning to $! itself.

However, one backwards-incompatibility remains: existing Perl code which
compares $! against the numeric values of the WSAE* error codes that were
previously assigned to $! will now be broken in those cases where a
corresponding E* value has been assigned instead.  This is only an issue for
those E* values < 100, which were always exported from Errno and
\s-1POSIX\s0 with their original *errno.h* values, and therefore could not be used
for WSAE* error code tests (e.g. \s-1WSAEINVAL\s0 is 10022, but the corresponding
\s-1EINVAL\s0 is 22).  (E* values > 100, if present, were redefined to WSAE*
values anyway, so compatibility can be achieved by using the E* constants,
which will work both before and after this change, albeit using different
numeric values under the hood.)
.ie n .SS "Functions ""PerlIO_vsprintf"" and ""PerlIO_sprintf"" have been removed"
.el .SS "Functions \f(CWPerlIO_vsprintf and \f(CWPerlIO_sprintf have been removed"
Subsection "Functions PerlIO_vsprintf and PerlIO_sprintf have been removed"
These two functions, undocumented, unused in \s-1CPAN,\s0 and problematic, have been
removed.

## Deprecations

Header "Deprecations"
.ie n .SS "The ""/\\C/"" character class"
.el .SS "The \f(CW/\\C/ character class"
Subsection "The /C/ character class"
The \f(CW\*(C`/\\C/\*(C' regular expression character class is deprecated. From perl
5.22 onwards it will generate a warning, and from perl 5.24 onwards it
will be a regular expression compiler error. If you need to examine the
individual bytes that make up a UTF8-encoded character, then use
\f(CW\*(C`utf8::encode()\*(C' on the string (or a copy) first.

### Literal control characters in variable names

Subsection "Literal control characters in variable names"
This deprecation affects things like $\\cT, where \\cT is a literal control (such
as a \f(CW\*(C`NAK\*(C' or \f(CW\*(C`NEGATIVE ACKNOWLEDGE\*(C' character) in
the source code.  Surprisingly, it appears that originally this was intended as
the canonical way of accessing variables like $^T, with the caret form only
being added as an alternative.

The literal control form is being deprecated for two main reasons.  It has what
are likely unfixable bugs, such as $\\cI not working as an alias for $^I, and
their usage not being portable to non-ASCII platforms: While $^T will work
everywhere, \\cT is whitespace in \s-1EBCDIC.\s0  [perl #119123]
.ie n .SS "References to non-integers and non-positive integers in $/"
.el .SS "References to non-integers and non-positive integers in \f(CW$/"
Subsection "References to non-integers and non-positive integers in $/"
Setting \f(CW$/ to a reference to zero or a reference to a negative integer is
now deprecated, and will behave **exactly** as though it was set to \f(CW\*(C`undef\*(C'.
If you want slurp behavior set \f(CW$/ to \f(CW\*(C`undef\*(C' explicitly.

Setting \f(CW$/ to a reference to a non integer is now forbidden and will
throw an error. Perl has never documented what would happen in this
context and while it used to behave the same as setting \f(CW$/ to
the address of the references in future it may behave differently, so we
have forbidden this usage.

### Character matching routines in \s-1POSIX\s0

Subsection "Character matching routines in POSIX"
Use of any of these functions in the \f(CW\*(C`POSIX\*(C' module is now deprecated:
\f(CW\*(C`isalnum\*(C', \f(CW\*(C`isalpha\*(C', \f(CW\*(C`iscntrl\*(C', \f(CW\*(C`isdigit\*(C', \f(CW\*(C`isgraph\*(C', \f(CW\*(C`islower\*(C',
\f(CW\*(C`isprint\*(C', \f(CW\*(C`ispunct\*(C', \f(CW\*(C`isspace\*(C', \f(CW\*(C`isupper\*(C', and \f(CW\*(C`isxdigit\*(C'.  The
functions are buggy and don't work on \s-1UTF-8\s0 encoded strings.  See their
entries in \s-1POSIX\s0 for more information.

A warning is raised on the first call to any of them from each place in
the code that they are called.  (Hence a repeated statement in a loop
will raise just the one warning.)

### Interpreter-based threads are now \fIdiscouraged

Subsection "Interpreter-based threads are now discouraged"
The \*(L"interpreter-based threads\*(R" provided by Perl are not the fast, lightweight
system for multitasking that one might expect or hope for.  Threads are
implemented in a way that make them easy to misuse.  Few people know how to
use them correctly or will be able to provide help.

The use of interpreter-based threads in perl is officially
discouraged.

### Module removals

Subsection "Module removals"
The following modules will be removed from the core distribution in a
future release, and will at that time need to be installed from \s-1CPAN.\s0
Distributions on \s-1CPAN\s0 which require these modules will need to list them as
prerequisites.

The core versions of these modules will now issue \f(CW"deprecated"-category
warnings to alert you to this fact.  To silence these deprecation warnings,
install the modules in question from \s-1CPAN.\s0

Note that the planned removal of these modules from core does not reflect a
judgement about the quality of the code and should not be taken as a suggestion
that their use be halted.  Their disinclusion from core primarily hinges on
their necessity to bootstrapping a fully functional, CPAN-capable Perl
installation, not on concerns over their design.

- \s-1CGI\s0 and its associated \s-1CGI::\s0 packages
Item "CGI and its associated CGI:: packages"
0

- inc::latest
Item "inc::latest"

- Package::Constants
Item "Package::Constants"

- Module::Build and its associated Module::Build:: packages
Item "Module::Build and its associated Module::Build:: packages"
.PD

### Utility removals

Subsection "Utility removals"
The following utilities will be removed from the core distribution in a
future release, and will at that time need to be installed from \s-1CPAN.\s0

- find2perl
Item "find2perl"
0

- s2p
Item "s2p"

- a2p
Item "a2p"
.PD

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
Perl has a new copy-on-write mechanism that avoids the need to copy the
internal string buffer when assigning from one scalar to another. This
makes copying large strings appear much faster.  Modifying one of the two
(or more) strings after an assignment will force a copy internally. This
makes it unnecessary to pass strings by reference for efficiency.
.Sp
This feature was already available in 5.18.0, but wasn't enabled by
default. It is the default now, and so you no longer need build perl with
the *Configure* argument:
.Sp
.Vb 1
    -Accflags=-DPERL_NEW_COPY_ON_WRITE
.Ve
.Sp
It can be disabled (for now) in a perl build with:
.Sp
.Vb 1
    -Accflags=-DPERL_NO_COW
.Ve
.Sp
On some operating systems Perl can be compiled in such a way that any
attempt to modify string buffers shared by multiple SVs will crash.  This
way \s-1XS\s0 authors can test that their modules handle copy-on-write scalars
correctly.  See \*(L"Copy on Write\*(R" in perlguts for detail.

- \(bu
Perl has an optimizer for regular expression patterns.  It analyzes the pattern
to find things such as the minimum length a string has to be to match, etc.  It
now better handles code points that are above the Latin1 range.

- \(bu
Executing a regex that contains the \f(CW\*(C`^\*(C' anchor (or its variant under the
\f(CW\*(C`/m\*(C' flag) has been made much faster in several situations.

- \(bu
Precomputed hash values are now used in more places during method lookup.

- \(bu
Constant hash key lookups (\f(CW$hash\{key\} as opposed to \f(CW$hash\{$key\}) have
long had the internal hash value computed at compile time, to speed up
lookup.  This optimisation has only now been applied to hash slices as
well.

- \(bu
Combined \f(CW\*(C`and\*(C' and \f(CW\*(C`or\*(C' operators in void context, like those
generated for \f(CW\*(C`unless ($a && $b)\*(C' and \f(CW\*(C`if ($a || b)\*(C' now
short circuit directly to the end of the statement. [perl #120128]

- \(bu
In certain situations, when \f(CW\*(C`return\*(C' is the last statement in a subroutine's
main scope, it will be optimized out. This means code like:
.Sp
.Vb 1
  sub baz \{ return $cat; \}
.Ve
.Sp
will now behave like:
.Sp
.Vb 1
  sub baz \{ $cat; \}
.Ve
.Sp
which is notably faster.
.Sp
[perl #120765]

- \(bu
Code like:
.Sp
.Vb 2
  my $x; # or @x, %x
  my $y;
.Ve
.Sp
is now optimized to:
.Sp
.Vb 1
  my ($x, $y);
.Ve
.Sp
In combination with the padrange optimization introduced in
v5.18.0, this means longer uninitialized my
variable statements are also optimized, so:
.Sp
.Vb 1
  my $x; my @y; my %z;
.Ve
.Sp
becomes:
.Sp
.Vb 1
  my ($x, @y, %z);
.Ve
.Sp
[perl #121077]

- \(bu
The creation of certain sorts of lists, including array and hash slices, is now
faster.

- \(bu
The optimisation for arrays indexed with a small constant integer is now
applied for integers in the range -128..127, rather than 0..255. This should
speed up Perl code using expressions like \f(CW$x[-1], at the expense of
(presumably much rarer) code using expressions like \f(CW$x[200].

- \(bu
The first iteration over a large hash (using \f(CW\*(C`keys\*(C' or \f(CW\*(C`each\*(C') is now
faster. This is achieved by preallocating the hash's internal iterator
state, rather than lazily creating it when the hash is first iterated. (For
small hashes, the iterator is still created only when first needed. The
assumption is that small hashes are more likely to be used as objects, and
therefore never allocated. For large hashes, that's less likely to be true,
and the cost of allocating the iterator is swamped by the cost of allocating
space for the hash itself.)

- \(bu
When doing a global regex match on a string that came from the \f(CW\*(C`readline\*(C'
or \f(CW\*(C`<>\*(C' operator, the data is no longer copied unnecessarily.
[perl #121259]

- \(bu
Dereferencing (as in \f(CW\*(C`$obj->[0]\*(C' or \f(CW\*(C`$obj->\{k\}\*(C') is now faster
when \f(CW$obj is an instance of a class that has overloaded methods, but
doesn't overload any of the dereferencing methods \f(CW\*(C`@\{\}\*(C', \f(CW\*(C`%\{\}\*(C', and so on.

- \(bu
Perl's optimiser no longer skips optimising code that follows certain
\f(CW\*(C`eval \{\}\*(C' expressions (including those with an apparent infinite loop).

- \(bu
The implementation now does a better job of avoiding meaningless work at
runtime. Internal effect-free \*(L"null\*(R" operations (created as a side-effect of
parsing Perl programs) are normally deleted during compilation. That
deletion is now applied in some situations that weren't previously handled.

- \(bu
Perl now does less disk I/O when dealing with Unicode properties that cover
up to three ranges of consecutive code points.

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules and Pragmata

Subsection "New Modules and Pragmata"

- \(bu
experimental 0.007 has been added to the Perl core.

- \(bu
IO::Socket::IP 0.29 has been added to the Perl core.

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Archive::Tar has been upgraded from version 1.90 to 1.96.

- \(bu
arybase has been upgraded from version 0.06 to 0.07.

- \(bu
Attribute::Handlers has been upgraded from version 0.94 to 0.96.

- \(bu
attributes has been upgraded from version 0.21 to 0.22.

- \(bu
autodie has been upgraded from version 2.13 to 2.23.

- \(bu
AutoLoader has been upgraded from version 5.73 to 5.74.

- \(bu
autouse has been upgraded from version 1.07 to 1.08.

- \(bu
B has been upgraded from version 1.42 to 1.48.

- \(bu
B::Concise has been upgraded from version 0.95 to 0.992.

- \(bu
B::Debug has been upgraded from version 1.18 to 1.19.

- \(bu
B::Deparse has been upgraded from version 1.20 to 1.26.

- \(bu
base has been upgraded from version 2.18 to 2.22.

- \(bu
Benchmark has been upgraded from version 1.15 to 1.18.

- \(bu
bignum has been upgraded from version 0.33 to 0.37.

- \(bu
Carp has been upgraded from version 1.29 to 1.3301.

- \(bu
\s-1CGI\s0 has been upgraded from version 3.63 to 3.65.
\s-1NOTE:\s0 \s-1CGI\s0 is deprecated and may be removed from a future version of Perl.

- \(bu
charnames has been upgraded from version 1.36 to 1.40.

- \(bu
Class::Struct has been upgraded from version 0.64 to 0.65.

- \(bu
Compress::Raw::Bzip2 has been upgraded from version 2.060 to 2.064.

- \(bu
Compress::Raw::Zlib has been upgraded from version 2.060 to 2.065.

- \(bu
Config::Perl::V has been upgraded from version 0.17 to 0.20.

- \(bu
constant has been upgraded from version 1.27 to 1.31.

- \(bu
\s-1CPAN\s0 has been upgraded from version 2.00 to 2.05.

- \(bu
CPAN::Meta has been upgraded from version 2.120921 to 2.140640.

- \(bu
CPAN::Meta::Requirements has been upgraded from version 2.122 to 2.125.

- \(bu
CPAN::Meta::YAML has been upgraded from version 0.008 to 0.012.

- \(bu
Data::Dumper has been upgraded from version 2.145 to 2.151.

- \(bu
\s-1DB\s0 has been upgraded from version 1.04 to 1.07.

- \(bu
DB_File has been upgraded from version 1.827 to 1.831.

- \(bu
DBM_Filter has been upgraded from version 0.05 to 0.06.

- \(bu
deprecate has been upgraded from version 0.02 to 0.03.

- \(bu
Devel::Peek has been upgraded from version 1.11 to 1.16.

- \(bu
Devel::PPPort has been upgraded from version 3.20 to 3.21.

- \(bu
diagnostics has been upgraded from version 1.31 to 1.34.

- \(bu
Digest::MD5 has been upgraded from version 2.52 to 2.53.

- \(bu
Digest::SHA has been upgraded from version 5.84 to 5.88.

- \(bu
DynaLoader has been upgraded from version 1.18 to 1.25.

- \(bu
Encode has been upgraded from version 2.49 to 2.60.

- \(bu
encoding has been upgraded from version 2.6_01 to 2.12.

- \(bu
English has been upgraded from version 1.06 to 1.09.
.Sp
\f(CW$OLD_PERL_VERSION was added as an alias of \f(CW$].

- \(bu
Errno has been upgraded from version 1.18 to 1.20_03.

- \(bu
Exporter has been upgraded from version 5.68 to 5.70.

- \(bu
ExtUtils::CBuilder has been upgraded from version 0.280210 to 0.280216.

- \(bu
ExtUtils::Command has been upgraded from version 1.17 to 1.18.

- \(bu
ExtUtils::Embed has been upgraded from version 1.30 to 1.32.

- \(bu
ExtUtils::Install has been upgraded from version 1.59 to 1.67.

- \(bu
ExtUtils::MakeMaker has been upgraded from version 6.66 to 6.98.

- \(bu
ExtUtils::Miniperl has been upgraded from version  to 1.01.

- \(bu
ExtUtils::ParseXS has been upgraded from version 3.18 to 3.24.

- \(bu
ExtUtils::Typemaps has been upgraded from version 3.19 to 3.24.

- \(bu
ExtUtils::XSSymSet has been upgraded from version 1.2 to 1.3.

- \(bu
feature has been upgraded from version 1.32 to 1.36.

- \(bu
fields has been upgraded from version 2.16 to 2.17.

- \(bu
File::Basename has been upgraded from version 2.84 to 2.85.

- \(bu
File::Copy has been upgraded from version 2.26 to 2.29.

- \(bu
File::DosGlob has been upgraded from version 1.10 to 1.12.

- \(bu
File::Fetch has been upgraded from version 0.38 to 0.48.

- \(bu
File::Find has been upgraded from version 1.23 to 1.27.

- \(bu
File::Glob has been upgraded from version 1.20 to 1.23.

- \(bu
File::Spec has been upgraded from version 3.40 to 3.47.

- \(bu
File::Temp has been upgraded from version 0.23 to 0.2304.

- \(bu
FileCache has been upgraded from version 1.08 to 1.09.

- \(bu
Filter::Simple has been upgraded from version 0.89 to 0.91.

- \(bu
Filter::Util::Call has been upgraded from version 1.45 to 1.49.

- \(bu
Getopt::Long has been upgraded from version 2.39 to 2.42.

- \(bu
Getopt::Std has been upgraded from version 1.07 to 1.10.

- \(bu
Hash::Util::FieldHash has been upgraded from version 1.10 to 1.15.

- \(bu
HTTP::Tiny has been upgraded from version 0.025 to 0.043.

- \(bu
I18N::Langinfo has been upgraded from version 0.10 to 0.11.

- \(bu
I18N::LangTags has been upgraded from version 0.39 to 0.40.

- \(bu
if has been upgraded from version 0.0602 to 0.0603.

- \(bu
inc::latest has been upgraded from version 0.4003 to 0.4205.
\s-1NOTE:\s0 inc::latest is deprecated and may be removed from a future version of Perl.

- \(bu
integer has been upgraded from version 1.00 to 1.01.

- \(bu
\s-1IO\s0 has been upgraded from version 1.28 to 1.31.

- \(bu
IO::Compress::Gzip and friends have been upgraded from version 2.060 to
2.064.

- \(bu
IPC::Cmd has been upgraded from version 0.80 to 0.92.

- \(bu
IPC::Open3 has been upgraded from version 1.13 to 1.16.

- \(bu
IPC::SysV has been upgraded from version 2.03 to 2.04.

- \(bu
\s-1JSON::PP\s0 has been upgraded from version 2.27202 to 2.27203.

- \(bu
List::Util has been upgraded from version 1.27 to 1.38.

- \(bu
locale has been upgraded from version 1.02 to 1.03.

- \(bu
Locale::Codes has been upgraded from version 3.25 to 3.30.

- \(bu
Locale::Maketext has been upgraded from version 1.23 to 1.25.

- \(bu
Math::BigInt has been upgraded from version 1.9991 to 1.9993.

- \(bu
Math::BigInt::FastCalc has been upgraded from version 0.30 to 0.31.

- \(bu
Math::BigRat has been upgraded from version 0.2604 to 0.2606.

- \(bu
MIME::Base64 has been upgraded from version 3.13 to 3.14.

- \(bu
Module::Build has been upgraded from version 0.4003 to 0.4205.
\s-1NOTE:\s0 Module::Build is deprecated and may be removed from a future version of Perl.

- \(bu
Module::CoreList has been upgraded from version 2.89 to 3.10.

- \(bu
Module::Load has been upgraded from version 0.24 to 0.32.

- \(bu
Module::Load::Conditional has been upgraded from version 0.54 to 0.62.

- \(bu
Module::Metadata has been upgraded from version 1.000011 to 1.000019.

- \(bu
mro has been upgraded from version 1.11 to 1.16.

- \(bu
Net::Ping has been upgraded from version 2.41 to 2.43.

- \(bu
Opcode has been upgraded from version 1.25 to 1.27.

- \(bu
Package::Constants has been upgraded from version 0.02 to 0.04.
\s-1NOTE:\s0 Package::Constants is deprecated and may be removed from a future version of Perl.

- \(bu
Params::Check has been upgraded from version 0.36 to 0.38.

- \(bu
parent has been upgraded from version 0.225 to 0.228.

- \(bu
Parse::CPAN::Meta has been upgraded from version 1.4404 to 1.4414.

- \(bu
Perl::OSType has been upgraded from version 1.003 to 1.007.

- \(bu
perlfaq has been upgraded from version 5.0150042 to 5.0150044.

- \(bu
PerlIO has been upgraded from version 1.07 to 1.09.

- \(bu
PerlIO::encoding has been upgraded from version 0.16 to 0.18.

- \(bu
PerlIO::scalar has been upgraded from version 0.16 to 0.18.

- \(bu
PerlIO::via has been upgraded from version 0.12 to 0.14.

- \(bu
Pod::Escapes has been upgraded from version 1.04 to 1.06.

- \(bu
Pod::Functions has been upgraded from version 1.06 to 1.08.

- \(bu
Pod::Html has been upgraded from version 1.18 to 1.21.

- \(bu
Pod::Parser has been upgraded from version 1.60 to 1.62.

- \(bu
Pod::Perldoc has been upgraded from version 3.19 to 3.23.

- \(bu
Pod::Usage has been upgraded from version 1.61 to 1.63.

- \(bu
\s-1POSIX\s0 has been upgraded from version 1.32 to 1.38_03.

- \(bu
re has been upgraded from version 0.23 to 0.26.

- \(bu
Safe has been upgraded from version 2.35 to 2.37.

- \(bu
Scalar::Util has been upgraded from version 1.27 to 1.38.

- \(bu
SDBM_File has been upgraded from version 1.09 to 1.11.

- \(bu
Socket has been upgraded from version 2.009 to 2.013.

- \(bu
Storable has been upgraded from version 2.41 to 2.49.

- \(bu
strict has been upgraded from version 1.07 to 1.08.

- \(bu
subs has been upgraded from version 1.01 to 1.02.

- \(bu
Sys::Hostname has been upgraded from version 1.17 to 1.18.

- \(bu
Sys::Syslog has been upgraded from version 0.32 to 0.33.

- \(bu
Term::Cap has been upgraded from version 1.13 to 1.15.

- \(bu
Term::ReadLine has been upgraded from version 1.12 to 1.14.

- \(bu
Test::Harness has been upgraded from version 3.26 to 3.30.

- \(bu
Test::Simple has been upgraded from version 0.98 to 1.001002.

- \(bu
Text::ParseWords has been upgraded from version 3.28 to 3.29.

- \(bu
Text::Tabs has been upgraded from version 2012.0818 to 2013.0523.

- \(bu
Text::Wrap has been upgraded from version 2012.0818 to 2013.0523.

- \(bu
Thread has been upgraded from version 3.02 to 3.04.

- \(bu
Thread::Queue has been upgraded from version 3.02 to 3.05.

- \(bu
threads has been upgraded from version 1.86 to 1.93.

- \(bu
threads::shared has been upgraded from version 1.43 to 1.46.

- \(bu
Tie::Array has been upgraded from version 1.05 to 1.06.

- \(bu
Tie::File has been upgraded from version 0.99 to 1.00.

- \(bu
Tie::Hash has been upgraded from version 1.04 to 1.05.

- \(bu
Tie::Scalar has been upgraded from version 1.02 to 1.03.

- \(bu
Tie::StdHandle has been upgraded from version 4.3 to 4.4.

- \(bu
Time::HiRes has been upgraded from version 1.9725 to 1.9726.

- \(bu
Time::Piece has been upgraded from version 1.20_01 to 1.27.

- \(bu
Unicode::Collate has been upgraded from version 0.97 to 1.04.

- \(bu
Unicode::Normalize has been upgraded from version 1.16 to 1.17.

- \(bu
Unicode::UCD has been upgraded from version 0.51 to 0.57.

- \(bu
utf8 has been upgraded from version 1.10 to 1.13.

- \(bu
version has been upgraded from version 0.9902 to 0.9908.

- \(bu
vmsish has been upgraded from version 1.03 to 1.04.

- \(bu
warnings has been upgraded from version 1.18 to 1.23.

- \(bu
Win32 has been upgraded from version 0.47 to 0.49.

- \(bu
XS::Typemap has been upgraded from version 0.10 to 0.13.

- \(bu
XSLoader has been upgraded from version 0.16 to 0.17.

## Documentation

Header "Documentation"

### New Documentation

Subsection "New Documentation"
*perlrepository*
Subsection "perlrepository"

This document was removed (actually, renamed perlgit and given a major
overhaul) in Perl v5.14, causing Perl documentation websites to show the now
out of date version in Perl v5.12 as the latest version.  It has now been
restored in stub form, directing readers to current information.

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perldata*
Subsection "perldata"

- \(bu
New sections have been added to document the new index/value array slice and
key/value hash slice syntax.

*perldebguts*
Subsection "perldebguts"

- \(bu
The \f(CW\*(C`DB::goto\*(C' and \f(CW\*(C`DB::lsub\*(C' debugger subroutines are now documented.  [perl
#77680]

*perlexperiment*
Subsection "perlexperiment"

- \(bu
\f(CW\*(C`\\s\*(C' matching \f(CW\*(C`\\cK\*(C' is marked experimental.

- \(bu
ithreads were accepted in v5.8.0 (but are discouraged as of v5.20.0).

- \(bu
Long doubles are not considered experimental.

- \(bu
Code in regular expressions, regular expression backtracking verbs,
and lvalue subroutines are no longer listed as experimental.  (This
also affects perlre and perlsub.)

*perlfunc*
Subsection "perlfunc"

- \(bu
\f(CW\*(C`chop\*(C' and \f(CW\*(C`chomp\*(C' now note that they can reset the hash iterator.

- \(bu
\f(CW\*(C`exec\*(C''s handling of arguments is now more clearly documented.

- \(bu
\f(CW\*(C`eval EXPR\*(C' now has caveats about expanding floating point numbers in some
locales.

- \(bu
\f(CW\*(C`goto EXPR\*(C' is now documented to handle an expression that evalutes to a
code reference as if it was \f(CW\*(C`goto &$coderef\*(C'.  This behavior is at least ten
years old.

- \(bu
Since Perl v5.10, it has been possible for subroutines in \f(CW@INC to return
a reference to a scalar holding initial source code to prepend to the file.
This is now documented.

- \(bu
The documentation of \f(CW\*(C`ref\*(C' has been updated to recommend the use of
\f(CW\*(C`blessed\*(C', \f(CW\*(C`isa\*(C' and \f(CW\*(C`reftype\*(C' when dealing with references to blessed
objects.

*perlguts*
Subsection "perlguts"

- \(bu
Numerous minor changes have been made to reflect changes made to the perl
internals in this release.

- \(bu
New sections on Read-Only Values and
Copy on Write have been added.

*perlhack*
Subsection "perlhack"

- \(bu
The Super Quick Patch Guide section has
been updated.

*perlhacktips*
Subsection "perlhacktips"

- \(bu
The documentation has been updated to include some more examples of \f(CW\*(C`gdb\*(C'
usage.

*perllexwarn*
Subsection "perllexwarn"

- \(bu
The perllexwarn documentation used to describe the hierarchy of warning
categories understood by the warnings pragma. That description has now
been moved to the warnings documentation itself, leaving perllexwarn
as a stub that points to it. This change consolidates all documentation for
lexical warnings in a single place.

*perllocale*
Subsection "perllocale"

- \(bu
The documentation now mentions *\f(BIfc()** and \f(CW\*(C`\\F\*(C'*, and includes many
clarifications and corrections in general.

*perlop*
Subsection "perlop"

- \(bu
The language design of Perl has always called for monomorphic operators.
This is now mentioned explicitly.

*perlopentut*
Subsection "perlopentut"

- \(bu
The \f(CW\*(C`open\*(C' tutorial has been completely rewritten by Tom Christiansen, and now
focuses on covering only the basics, rather than providing a comprehensive
reference to all things openable.  This rewrite came as the result of a
vigorous discussion on perl5-porters kicked off by a set of improvements
written by Alexander Hartmaier to the existing perlopentut.  A "more than
you ever wanted to know about \f(CW\*(C`open\*(C'" document may follow in subsequent
versions of perl.

*perlre*
Subsection "perlre"

- \(bu
The fact that the regexp engine makes no effort to call (?\{\}) and (??\{\})
constructs any specified number of times (although it will basically \s-1DWIM\s0
in case of a successful match) has been documented.

- \(bu
The \f(CW\*(C`/r\*(C' modifier (for non-destructive substitution) is now documented. [perl
#119151]

- \(bu
The documentation for \f(CW\*(C`/x\*(C' and \f(CW\*(C`(?# comment)\*(C' has been expanded and clarified.

*perlreguts*
Subsection "perlreguts"

- \(bu
The documentation has been updated in the light of recent changes to
*regcomp.c*.

*perlsub*
Subsection "perlsub"

- \(bu
The need to predeclare recursive functions with prototypes in order for the
prototype to be honoured in the recursive call is now documented. [perl #2726]

- \(bu
A list of subroutine names used by the perl implementation is now included.
[perl #77680]

*perltrap*
Subsection "perltrap"

- \(bu
There is now a JavaScript section.

*perlunicode*
Subsection "perlunicode"

- \(bu
The documentation has been updated to reflect \f(CW\*(C`Bidi_Class\*(C' changes in
Unicode 6.3.

*perlvar*
Subsection "perlvar"

- \(bu
A new section explaining the performance issues of $`, $& and $', including
workarounds and changes in different versions of Perl, has been added.

- \(bu
Three English variable names which have long been documented but do not
actually exist have been removed from the documentation.  These were
\f(CW$OLD_PERL_VERSION, \f(CW$OFMT, and \f(CW$ARRAY_BASE.
.Sp
(Actually, \f(CW\*(C`OLD_PERL_VERSION\*(C' *does* exist, starting with this revision, but
remained undocumented until perl 5.22.0.)

*perlxs*
Subsection "perlxs"

- \(bu
Several problems in the \f(CW\*(C`MY_CXT\*(C' example have been fixed.

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### New Diagnostics

Subsection "New Diagnostics"
*New Errors*
Subsection "New Errors"

- \(bu
delete argument is index/value array slice, use array slice
.Sp
(F) You used index/value array slice syntax (\f(CW%array[...]) as the argument to
\f(CW\*(C`delete\*(C'.  You probably meant \f(CW@array[...] with an @ symbol instead.

- \(bu
delete argument is key/value hash slice, use hash slice
.Sp
(F) You used key/value hash slice syntax (\f(CW%hash\{...\}) as the argument to
\f(CW\*(C`delete\*(C'.  You probably meant \f(CW@hash\{...\} with an @ symbol instead.

- \(bu
Magical list constants are not supported
.Sp
(F) You assigned a magical array to a stash element, and then tried to use the
subroutine from the same slot.  You are asking Perl to do something it cannot
do, details subject to change between Perl versions.

- \(bu
Added Setting $/ to a \f(CW%s reference is forbidden

*New Warnings*
Subsection "New Warnings"

- \(bu
\f(CW%s on reference is experimental:
.Sp
The \*(L"auto-deref\*(R" feature is experimental.
.Sp
Starting in v5.14.0, it was possible to use push, pop, keys, and other
built-in functions not only on aggregate types, but on references to
them.  The feature was not deployed to its original intended
specification, and now may become redundant to postfix dereferencing.
It has always been categorized as an experimental feature, and in
v5.20.0 is carries a warning as such.
.Sp
Warnings will now be issued at compile time when these operations are
detected.
.Sp
.Vb 1
  no if $] >= 5.01908, warnings => "experimental::autoderef";
.Ve
.Sp
Consider, though, replacing the use of these features, as they may
change behavior again before becoming stable.

- \(bu
A sequence of multiple spaces in a charnames alias definition is deprecated
.Sp
Trailing white-space in a charnames alias definition is deprecated
.Sp
These two deprecation warnings involving \f(CW\*(C`\\N\{...\}\*(C' were incorrectly
implemented.  They did not warn by default (now they do) and could not be
made fatal via \f(CW\*(C`use warnings FATAL => \*(Aqdeprecated\*(Aq\*(C' (now they can).

- \(bu
Attribute prototype(%s) discards earlier prototype attribute in same sub
.Sp
(W misc) A sub was declared as \f(CW\*(C`sub foo : prototype(A) : prototype(B) \{\}\*(C', for
example.  Since each sub can only have one prototype, the earlier
declaration(s) are discarded while the last one is applied.

- \(bu
Invalid \\0 character in \f(CW%s for \f(CW%s: \f(CW%s\\0%s
.Sp
(W syscalls) Embedded \\0 characters in pathnames or other system call arguments
produce a warning as of 5.20.  The parts after the \\0 were formerly ignored by
system calls.

- \(bu
Matched non-Unicode code point 0x%X against Unicode property; may not be portable.
.Sp
This replaces the message \*(L"Code point 0x%X is not Unicode, all \\p\{\} matches
fail; all \\P\{\} matches succeed\*(R".

- \(bu
Missing ']' in prototype for \f(CW%s : \f(CW%s
.Sp
(W illegalproto) A grouping was started with \f(CW\*(C`[\*(C' but never closed with \f(CW\*(C`]\*(C'.

- \(bu
Possible precedence issue with control flow operator
.Sp
(W syntax) There is a possible problem with the mixing of a control flow
operator (e.g. \f(CW\*(C`return\*(C') and a low-precedence operator like \f(CW\*(C`or\*(C'.  Consider:
.Sp
.Vb 1
    sub \{ return $a or $b; \}
.Ve
.Sp
This is parsed as:
.Sp
.Vb 1
    sub \{ (return $a) or $b; \}
.Ve
.Sp
Which is effectively just:
.Sp
.Vb 1
    sub \{ return $a; \}
.Ve
.Sp
Either use parentheses or the high-precedence variant of the operator.
.Sp
Note this may be also triggered for constructs like:
.Sp
.Vb 1
    sub \{ 1 if die; \}
.Ve

- \(bu
Postfix dereference is experimental
.Sp
(S experimental::postderef) This warning is emitted if you use the experimental
postfix dereference syntax.  Simply suppress the warning if you want to use the
feature, but know that in doing so you are taking the risk of using an
experimental feature which may change or be removed in a future Perl version:
.Sp
.Vb 6
    no warnings "experimental::postderef";
    use feature "postderef", "postderef_qq";
    $ref->$*;
    $aref->@*;
    $aref->@[@indices];
    ... etc ...
.Ve

- \(bu
Prototype '%s' overridden by attribute 'prototype(%s)' in \f(CW%s
.Sp
(W prototype) A prototype was declared in both the parentheses after the sub
name and via the prototype attribute.  The prototype in parentheses is useless,
since it will be replaced by the prototype from the attribute before it's ever
used.

- \(bu
Scalar value @%s[%s] better written as $%s[%s]
.Sp
(W syntax) In scalar context, you've used an array index/value slice (indicated
by %) to select a single element of an array.  Generally it's better to ask for
a scalar value (indicated by $).  The difference is that \f(CW$foo[&bar] always
behaves like a scalar, both in the value it returns and when evaluating its
argument, while \f(CW%foo[&bar] provides a list context to its subscript, which
can do weird things if you're expecting only one subscript.  When called in
list context, it also returns the index (what \f(CW&bar returns) in addition to
the value.

- \(bu
Scalar value @%s\{%s\} better written as $%s\{%s\}
.Sp
(W syntax) In scalar context, you've used a hash key/value slice (indicated by
%) to select a single element of a hash.  Generally it's better to ask for a
scalar value (indicated by $).  The difference is that \f(CW$foo\{&bar\} always
behaves like a scalar, both in the value it returns and when evaluating its
argument, while \f(CW@foo\{&bar\} and provides a list context to its subscript,
which can do weird things if you're expecting only one subscript.  When called
in list context, it also returns the key in addition to the value.

- \(bu
Setting $/ to a reference to \f(CW%s as a form of slurp is deprecated, treating as undef

- \(bu
Unexpected exit \f(CW%u
.Sp
(S) **exit()** was called or the script otherwise finished gracefully when
\f(CW\*(C`PERL_EXIT_WARN\*(C' was set in \f(CW\*(C`PL_exit_flags\*(C'.

- \(bu
Unexpected exit failure \f(CW%d
.Sp
(S) An uncaught **die()** was called when \f(CW\*(C`PERL_EXIT_WARN\*(C' was set in
\f(CW\*(C`PL_exit_flags\*(C'.

- \(bu
Use of literal control characters in variable names is deprecated
.Sp
(D deprecated) Using literal control characters in the source to refer to the
^FOO variables, like $^X and $\{^GLOBAL_PHASE\} is now deprecated.  This only
affects code like $\\cT, where \\cT is a control (like a \f(CW\*(C`SOH\*(C') in the
source code: $\{\*(L"\\cT\*(R"\} and $^T remain valid.

- \(bu
Useless use of greediness modifier
.Sp
This fixes [Perl #42957].

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
Warnings and errors from the regexp engine are now \s-1UTF-8\s0 clean.

- \(bu
The \*(L"Unknown switch condition\*(R" error message has some slight changes.  This
error triggers when there is an unknown condition in a \f(CW\*(C`(?(foo))\*(C' conditional.
The error message used to read:
.Sp
.Vb 1
    Unknown switch condition (?(%s in regex;
.Ve
.Sp
But what \f(CW%s could be was mostly up to luck.  For \f(CW\*(C`(?(foobar))\*(C', you might have
seen \*(L"fo\*(R" or \*(L"f\*(R".  For Unicode characters, you would generally get a corrupted
string.  The message has been changed to read:
.Sp
.Vb 1
    Unknown switch condition (?(...)) in regex;
.Ve
.Sp
Additionally, the \f(CW\*(Aq<-- HERE\*(Aq marker in the error will now point to the
correct spot in the regex.

- \(bu
The \*(L"%s \*(R"\\x%X\*(L" does not map to Unicode\*(R" warning is now correctly listed as a
severe warning rather than as a fatal error.

- \(bu
Under rare circumstances, one could get a \*(L"Can't coerce readonly \s-1REF\s0 to
string\*(R" instead of the customary \*(L"Modification of a read-only value\*(R".  This
alternate error message has been removed.

- \(bu
\*(L"Ambiguous use of * resolved as operator *\*(R": This and similar warnings
about \*(L"%\*(R" and \*(L"&\*(R" used to occur in some circumstances where there was no
operator of the type cited, so the warning was completely wrong.  This has
been fixed [perl #117535, #76910].

- \(bu
Warnings about malformed subroutine prototypes are now more consistent in
how the prototypes are rendered.  Some of these warnings would truncate
prototypes containing nulls.  In other cases one warning would suppress
another.  The warning about illegal characters in prototypes no longer says
\*(L"after '_'\*(R" if the bad character came before the underscore.

- \(bu
Perl folding rules are not up-to-date for 0x%X; please use the perlbug
utility to report; in regex; marked by <-- \s-1HERE\s0 in
m/%s/
.Sp
This message is now only in the regexp category, and not in the deprecated
category.  It is still a default (i.e., severe) warning [perl #89648].

- \(bu
%%s[%s] in scalar context better written as $%s[%s]
.Sp
This warning now occurs for any \f(CW%array[$index] or \f(CW%hash\{key\} known to
be in scalar context at compile time.  Previously it was worded \*(L"Scalar
value %%s[%s] better written as $%s[%s]\*(R".

- \(bu
Switch condition not recognized in regex; marked by <-- \s-1HERE\s0 in m/%s/:
.Sp
The description for this diagnostic has been extended to cover all cases where the warning may occur.
Issues with the positioning of the arrow indicator have also been resolved.

- \(bu
The error messages for \f(CW\*(C`my($a?$b$c)\*(C' and \f(CW\*(C`my(do\{\})\*(C' now mention \*(L"conditional
expression\*(R" and \*(L"do block\*(R", respectively, instead of reading 'Can't declare
null operation in \*(L"my\*(R"'.

- \(bu
When \f(CW\*(C`use re "debug"\*(C' executes a regex containing a backreference, the
debugging output now shows what string is being matched.

- \(bu
The now fatal error message \f(CW\*(C`Character following "\\c" must be ASCII\*(C' has been
reworded as \f(CW\*(C`Character following "\\c" must be printable ASCII\*(C' to emphasize
that in \f(CW\*(C`\\c\f(CIX\f(CW\*(C', *X* must be a *printable (non-control)* \s-1ASCII\s0 character.

## Utility Changes

Header "Utility Changes"
*a2p*
Subsection "a2p"

- \(bu
A possible crash from an off-by-one error when trying to access before the
beginning of a buffer has been fixed.  [perl #120244]

*\fIbisect.pl\fI*
Subsection "bisect.pl"

The git bisection tool *Porting/bisect.pl* has had many enhancements.

It is provided as part of the source distribution but not installed because
it is not self-contained as it relies on being run from within a git
checkout. Note also that it makes no attempt to fix tests, correct runtime
bugs or make something useful to install - its purpose is to make minimal
changes to get any historical revision of interest to build and run as close
as possible to \*(L"as-was\*(R", and thereby make \f(CW\*(C`git bisect\*(C' easy to use.

- \(bu
Can optionally run the test case with a timeout.

- \(bu
Can now run in-place in a clean git checkout.

- \(bu
Can run the test case under \f(CW\*(C`valgrind\*(C'.

- \(bu
Can apply user supplied patches and fixes to the source checkout before
building.

- \(bu
Now has fixups to enable building several more historical ranges of bleadperl,
which can be useful for pinpointing the origins of bugs or behaviour changes.

*find2perl*
Subsection "find2perl"

- \(bu
find2perl now handles \f(CW\*(C`?\*(C' wildcards correctly.  [perl #113054]

*perlbug*
Subsection "perlbug"

- \(bu
*perlbug* now has a \f(CW\*(C`-p\*(C' option for attaching patches with a bug report.

- \(bu
perlbug has been modified to supply the report template with \s-1CRLF\s0 line
endings on Windows.
[\s-1GH\s0 #13612] <https://github.com/Perl/perl5/issues/13612>

- \(bu
perlbug now makes as few assumptions as possible about the encoding of the
report.  This will likely change in the future to assume \s-1UTF-8\s0 by default but
allow a user override.

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
The *Makefile.PL* for SDBM_File now generates a better *Makefile*, which
avoids a race condition during parallel makes, which could cause the build to
fail.  This is the last known parallel make problem (on *nix platforms), and
therefore we believe that a parallel make should now always be error free.

- \(bu
*installperl* and *installman*'s option handling has been refactored to use
Getopt::Long. Both are used by the *Makefile* \f(CW\*(C`install\*(C' targets, and
are not installed, so these changes are only likely to affect custom
installation scripts.

> 
- \(bu
Single letter options now also have long names.

- \(bu
Invalid options are now rejected.

- \(bu
Command line arguments that are not options are now rejected.

- \(bu
Each now has a \f(CW\*(C`--help\*(C' option to display the usage message.



> .Sp
The behaviour for all valid documented invocations is unchanged.



- \(bu
Where possible, the build now avoids recursive invocations of *make* when
building pure-Perl extensions, without removing any parallelism from the
build. Currently around 80 extensions can be processed directly by the
*make_ext.pl* tool, meaning that 80 invocations of *make* and 160
invocations of *miniperl* are no longer made.

- \(bu
The build system now works correctly when compiling under \s-1GCC\s0 or Clang with
link-time optimization enabled (the \f(CW\*(C`-flto\*(C' option). [perl #113022]

- \(bu
Distinct library basenames with \f(CW\*(C`d_libname_unique\*(C'.
.Sp
When compiling perl with this option, the library files for \s-1XS\s0 modules are
named something \*(L"unique\*(R" \*(-- for example, Hash/Util/Util.so becomes
Hash/Util/PL_Hash_\|_Util.so.  This behavior is similar to what currently
happens on \s-1VMS,\s0 and serves as groundwork for the Android port.

- \(bu
\f(CW\*(C`sysroot\*(C' option to indicate the logical root directory under gcc and clang.
.Sp
When building with this option set, both Configure and the compilers search
for all headers and libraries under this new sysroot, instead of /.
.Sp
This is a huge time saver if cross-compiling, but can also help
on native builds if your toolchain's files have non-standard locations.

- \(bu
The cross-compilation model has been renovated.
There's several new options, and some backwards-incompatible changes:
.Sp
We now build binaries for miniperl and generate_uudmap to be used on the host,
rather than running every miniperl call on the target; this means that, short
of 'make test', we no longer need access to the target system once Configure is
done.  You can provide already-built binaries through the \f(CW\*(C`hostperl\*(C' and
\f(CW\*(C`hostgenerate\*(C' options to Configure.
.Sp
Additionally, if targeting an \s-1EBCDIC\s0 platform from an \s-1ASCII\s0 host,
or viceversa, you'll need to run Configure with \f(CW\*(C`-Uhostgenerate\*(C', to
indicate that generate_uudmap should be run on the target.
.Sp
Finally, there's also a way of having Configure end early, right after
building the host binaries, by cross-compiling without specifying a
\f(CW\*(C`targethost\*(C'.
.Sp
The incompatible changes include no longer using xconfig.h, xlib, or
Cross.pm, so canned config files and Makefiles will have to be updated.

- \(bu
Related to the above, there is now a way of specifying the location of sh
(or equivalent) on the target system: \f(CW\*(C`targetsh\*(C'.
.Sp
For example, Android has its sh in /system/bin/sh, so if cross-compiling
from a more normal Unixy system with sh in /bin/sh, \*(L"targetsh\*(R" would end
up as /system/bin/sh, and \*(L"sh\*(R" as /bin/sh.

- \(bu
By default, **gcc** 4.9 does some optimizations that break perl.  The **-fwrapv**
option disables those optimizations (and probably others), so for **gcc** 4.3
and later (since the there might be similar problems lurking on older versions
too, but **-fwrapv** was broken before 4.3, and the optimizations probably won't
go away), *Configure* now adds **-fwrapv** unless the user requests
**-fno-wrapv**, which disables **-fwrapv**, or **-fsanitize=undefined**, which
turns the overflows **-fwrapv** ignores into runtime errors.
[\s-1GH\s0 #13690] <https://github.com/Perl/perl5/issues/13690>

## Testing

Header "Testing"

- \(bu
The \f(CW\*(C`test.valgrind\*(C' make target now allows tests to be run in parallel.
This target allows Perl's test suite to be run under Valgrind, which detects
certain sorts of C programming errors, though at significant cost in running
time. On suitable hardware, allowing parallel execution claws back a lot of
that additional cost. [perl #121431]

- \(bu
Various tests in *t/porting/* are no longer skipped when the perl
*.git* directory is outside the perl tree and pointed to by
\f(CW$GIT_DIR. [perl #120505]

- \(bu
The test suite no longer fails when the user's interactive shell maintains a
\f(CW$PWD environment variable, but the */bin/sh* used for running tests
doesn't.

## Platform Support

Header "Platform Support"

### New Platforms

Subsection "New Platforms"

- Android
Item "Android"
Perl can now be built for Android, either natively or through
cross-compilation, for all three currently available architectures (\s-1ARM,
MIPS,\s0 and x86), on a wide range of versions.

- Bitrig
Item "Bitrig"
Compile support has been added for Bitrig, a fork of OpenBSD.

- FreeMiNT
Item "FreeMiNT"
Support has been added for FreeMiNT, a free open-source \s-1OS\s0 for the Atari \s-1ST\s0
system and its successors, based on the original MiNT that was officially
adopted by Atari.

- Synology
Item "Synology"
Synology ships its \s-1NAS\s0 boxes with a lean Linux distribution (\s-1DSM\s0) on relative
cheap \s-1CPU\s0's (like the Marvell Kirkwood mv6282 - ARMv5tel or Freescale QorIQ
P1022 ppc - e500v2) not meant for workstations or development. These boxes
should build now. The basic problems are the non-standard location for tools.

### Discontinued Platforms

Subsection "Discontinued Platforms"
.ie n .IP """sfio""" 4
.el .IP "\f(CWsfio" 4
Item "sfio"
Code related to supporting the \f(CW\*(C`sfio\*(C' I/O system has been removed.
.Sp
Perl 5.004 added support to use the native \s-1API\s0 of \f(CW\*(C`sfio\*(C', \s-1AT&T\s0's Safe/Fast
I/O library. This code still built with v5.8.0, albeit with many regression
tests failing, but was inadvertently broken before the v5.8.1 release,
meaning that it has not worked on any version of Perl released since then.
In over a decade we have received no bug reports about this, hence it is clear
that no-one is using this functionality on any version of Perl that is still
supported to any degree.

- \s-1AT&T\s0 3b1
Item "AT&T 3b1"
Configure support for the 3b1, also known as the \s-1AT&T\s0 Unix \s-1PC\s0 (and the similar
\s-1AT&T 7300\s0), has been removed.

- \s-1DG/UX\s0
Item "DG/UX"
\s-1DG/UX\s0 was a Unix sold by Data General. The last release was in April 2001.
It only runs on Data General's own hardware.

- \s-1EBCDIC\s0
Item "EBCDIC"
In the absence of a regular source of smoke reports, code intended to support
native \s-1EBCDIC\s0 platforms will be removed from perl before 5.22.0.

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- Cygwin
Item "Cygwin"

> 0

- \(bu
.PD
**recv()** on a connected handle would populate the returned sender
address with whatever happened to be in the working buffer.  **recv()**
now uses a workaround similar to the Win32 **recv()** wrapper and returns
an empty string when **recvfrom**\|(2) doesn't modify the supplied address
length. [perl #118843]

- \(bu
Fixed a build error in cygwin.c on Cygwin 1.7.28.
.Sp
Tests now handle the errors that occur when \f(CW\*(C`cygserver\*(C' isn't
running.



> 


- GNU/Hurd
Item "GNU/Hurd"
The \s-1BSD\s0 compatibility library \f(CW\*(C`libbsd\*(C' is no longer required for builds.

- Linux
Item "Linux"
The hints file now looks for \f(CW\*(C`libgdbm_compat\*(C' only if \f(CW\*(C`libgdbm\*(C' itself is
also wanted. The former is never useful without the latter, and in some
circumstances, including it could actually prevent building.

- Mac \s-1OS\s0
Item "Mac OS"
The build system now honors an \f(CW\*(C`ld\*(C' setting supplied by the user running
*Configure*.

- MidnightBSD
Item "MidnightBSD"
\f(CW\*(C`objformat\*(C' was removed from version 0.4-RELEASE of MidnightBSD and had been
deprecated on earlier versions.  This caused the build environment to be
erroneously configured for \f(CW\*(C`a.out\*(C' rather than \f(CW\*(C`elf\*(C'.  This has been now
been corrected.

- Mixed-endian platforms
Item "Mixed-endian platforms"
The code supporting \f(CW\*(C`pack\*(C' and \f(CW\*(C`unpack\*(C' operations on mixed endian
platforms has been removed. We believe that Perl has long been unable to
build on mixed endian architectures (such as PDP-11s), so we don't think
that this change will affect any platforms which were able to build v5.18.0.

- \s-1VMS\s0
Item "VMS"

> 0

- \(bu
.PD
The \f(CW\*(C`PERL_ENV_TABLES\*(C' feature to control the population of \f(CW%ENV at perl
start-up was broken in Perl 5.16.0 but has now been fixed.

- \(bu
Skip access checks on remotes in **opendir()**.  [perl #121002]

- \(bu
A check for glob metacharacters in a path returned by the
\f(CW\*(C`glob()\*(C' operator has been replaced with a check for \s-1VMS\s0
wildcard characters.  This saves a significant number of unnecessary
\f(CW\*(C`lstat()\*(C' calls such that some simple glob operations become
60-80% faster.



> 


- Win32
Item "Win32"

> 0

- \(bu
.PD
\f(CW\*(C`rename\*(C' and \f(CW\*(C`link\*(C' on Win32 now set $! to \s-1ENOSPC\s0 and \s-1EDQUOT\s0 when
appropriate.  [perl #119857]

- \(bu
The \s-1BUILD_STATIC\s0 and \s-1ALL_STATIC\s0 makefile options for linking some or (nearly)
all extensions statically (into perl520.dll, and into a separate
perl-static.exe too) were broken for MinGW builds. This has now been fixed.
.Sp
The \s-1ALL_STATIC\s0 option has also been improved to include the Encode and Win32
extensions (for both \s-1VC++\s0 and MinGW builds).

- \(bu
Support for building with Visual \*(C+ 2013 has been added.  There are currently
two possible test failures (see \*(L"Testing Perl on Windows\*(R" in perlwin32) which
will hopefully be resolved soon.

- \(bu
Experimental support for building with Intel \*(C+ Compiler has been added.  The
nmake makefile (win32/Makefile) and the dmake makefile (win32/makefile.mk) can
be used.  A \*(L"nmake test\*(R" will not pass at this time due to *cpan/CGI/t/url.t*.

- \(bu
Killing a process tree with \*(L"kill\*(R" in perlfunc and a negative signal, was broken
starting in 5.18.0. In this bug, \f(CW\*(C`kill\*(C' always returned 0 for a negative
signal even for valid PIDs, and no processes were terminated. This has been
fixed [perl #121230].

- \(bu
The time taken to build perl on Windows has been reduced quite significantly
(time savings in the region of 30-40% are typically seen) by reducing the
number of, usually failing, I/O calls for each \f(CW\*(C`require()\*(C'
(for **miniperl.exe** only).
[\s-1GH\s0 #13566] <https://github.com/Perl/perl5/issues/13566>

- \(bu
About 15 minutes of idle sleeping was removed from running \f(CW\*(C`make test\*(C' due to
a bug in which the timeout monitor used for tests could not be cancelled once
the test completes, and the full timeout period elapsed before running the next
test file.
[\s-1GH\s0 #13647] <https://github.com/Perl/perl5/issues/13647>

- \(bu
On a perl built without pseudo-fork (pseudo-fork builds were not affected by
this bug), killing a process tree with \f(CW\*(C`kill()\*(C' and a negative
signal resulted in \f(CW\*(C`kill()\*(C' inverting the returned value.  For example, if
\f(CW\*(C`kill()\*(C' killed 1 process tree \s-1PID\s0 then it returned 0 instead of 1, and if
\f(CW\*(C`kill()\*(C' was passed 2 invalid PIDs then it returned 2 instead of 0.  This has
probably been the case since the process tree kill feature was implemented on
Win32.  It has now been corrected to follow the documented behaviour.
[\s-1GH\s0 #13595] <https://github.com/Perl/perl5/issues/13595>

- \(bu
When building a 64-bit perl, an uninitialized memory read in **miniperl.exe**,
used during the build process, could lead to a 4GB **wperl.exe** being created.
This has now been fixed.  (Note that **perl.exe** itself was unaffected, but
obviously **wperl.exe** would have been completely broken.)
[\s-1GH\s0 #13677] <https://github.com/Perl/perl5/issues/13677>

- \(bu
Perl can now be built with **gcc** version 4.8.1 from <http://www.mingw.org>.
This was previously broken due to an incorrect definition of **DllMain()** in one
of perl's source files.  Earlier **gcc** versions were also affected when using
version 4 of the w32api package.  Versions of **gcc** available from
<http://mingw-w64.sourceforge.net/> were not affected.
[\s-1GH\s0 #13733] <https://github.com/Perl/perl5/issues/13733>

- \(bu
The test harness now has no failures when perl is built on a \s-1FAT\s0 drive with the
Windows \s-1OS\s0 on an \s-1NTFS\s0 drive.
[\s-1GH\s0 #6348] <https://github.com/Perl/perl5/issues/6348>

- \(bu
When cloning the context stack in **fork()** emulation, **Perl_cx_dup()**
would crash accessing parameter information for context stack entries
that included no parameters, as with \f(CW\*(C`&foo;\*(C'.
[\s-1GH\s0 #13763] <https://github.com/Perl/perl5/issues/13763>

- \(bu
Introduced by
[\s-1GH\s0 #12161] <https://github.com/Perl/perl5/issues/12161>, a memory
leak on every call to \f(CW\*(C`system\*(C' and backticks (\f(CW\*(C` \`\` \*(C'), on most Win32 Perls
starting from 5.18.0 has been fixed.  The memory leak only occurred if you
enabled pseudo-fork in your build of Win32 Perl, and were running that build on
Server 2003 R2 or newer \s-1OS.\s0  The leak does not appear on WinXP \s-1SP3.\s0
[\s-1GH\s0 #13741] <https://github.com/Perl/perl5/issues/13741>



> 


- WinCE
Item "WinCE"

> 0

- \(bu
.PD
The building of \s-1XS\s0 modules has largely been restored.  Several still cannot
(yet) be built but it is now possible to build Perl on WinCE with only a couple
of further patches (to Socket and ExtUtils::MakeMaker), hopefully to be
incorporated soon.

- \(bu
Perl can now be built in one shot with no user intervention on WinCE by running
\f(CW\*(C`nmake -f Makefile.ce all\*(C'.
.Sp
Support for building with \s-1EVC\s0 (Embedded Visual \*(C+) 4 has been restored.  Perl
can also be built using Smart Devices for Visual \*(C+ 2005 or 2008.



> 


## Internal Changes

Header "Internal Changes"

- \(bu
The internal representation has changed for the match variables \f(CW$1, \f(CW$2 etc.,
$`, $&, $', $\{^PREMATCH\}, $\{^MATCH\} and $\{^POSTMATCH\}.  It uses slightly less
memory, avoids string comparisons and numeric conversions during lookup, and
uses 23 fewer lines of C.  This change should not affect any external code.

- \(bu
Arrays now use \s-1NULL\s0 internally to represent unused slots, instead of
&PL_sv_undef.  &PL_sv_undef is no longer treated as a special value, so
av_store(av, 0, &PL_sv_undef) will cause element 0 of that array to hold a
read-only undefined scalar.  \f(CW\*(C`$array[0] = anything\*(C' will croak and
\f(CW\*(C`\\$array[0]\*(C' will compare equal to \f(CW\*(C`\\undef\*(C'.

- \(bu
The \s-1SV\s0 returned by **HeSVKEY_force()** now correctly reflects the UTF8ness of the
underlying hash key when that key is not stored as a \s-1SV.\s0  [perl #79074]

- \(bu
Certain rarely used functions and macros available to \s-1XS\s0 code are now
deprecated.  These are:
\f(CW\*(C`utf8_to_uvuni_buf\*(C' (use \f(CW\*(C`utf8_to_uvchr_buf\*(C' instead),
\f(CW\*(C`valid_utf8_to_uvuni\*(C' (use \f(CW\*(C`utf8_to_uvchr_buf\*(C' instead),
\f(CW\*(C`NATIVE_TO_NEED\*(C' (this did not work properly anyway),
and \f(CW\*(C`ASCII_TO_NEED\*(C' (this did not work properly anyway).
.Sp
Starting in this release, almost never does application code need to
distinguish between the platform's character set and Latin1, on which the
lowest 256 characters of Unicode are based.  New code should not use
\f(CW\*(C`utf8n_to_uvuni\*(C' (use \f(CW\*(C`utf8_to_uvchr_buf\*(C' instead),
nor
\f(CW\*(C`uvuni_to_utf8\*(C' (use \f(CW\*(C`uvchr_to_utf8\*(C' instead),

- \(bu
The Makefile shortcut targets for many rarely (or never) used testing and
profiling targets have been removed, or merged into the only other Makefile
target that uses them.  Specifically, these targets are gone, along with
documentation that referenced them or explained how to use them:
.Sp
.Vb 10
    check.third check.utf16 check.utf8 coretest minitest.prep
    minitest.utf16 perl.config.dashg perl.config.dashpg
    perl.config.gcov perl.gcov perl.gprof perl.gprof.config
    perl.pixie perl.pixie.atom perl.pixie.config perl.pixie.irix
    perl.third perl.third.config perl.valgrind.config purecovperl
    pureperl quantperl test.deparse test.taintwarn test.third
    test.torture test.utf16 test.utf8 test_notty.deparse
    test_notty.third test_notty.valgrind test_prep.third
    test_prep.valgrind torturetest ucheck ucheck.third ucheck.utf16
    ucheck.valgrind utest utest.third utest.utf16 utest.valgrind
.Ve
.Sp
It's still possible to run the relevant commands by \*(L"hand\*(R" - no underlying
functionality has been removed.

- \(bu
It is now possible to keep Perl from initializing locale handling.
For the most part, Perl doesn't pay attention to locale.  (See
perllocale.)  Nonetheless, until now, on startup, it has always
initialized locale handling to the system default, just in case the
program being executed ends up using locales.  (This is one of the first
things a locale-aware program should do, long before Perl knows if it
will actually be needed or not.)  This works well except when Perl is
embedded in another application which wants a locale that isn't the
system default.  Now, if the environment variable
\f(CW\*(C`PERL_SKIP_LOCALE_INIT\*(C' is set at the time Perl is started, this
initialization step is skipped.  Prior to this, on Windows platforms,
the only workaround for this deficiency was to use a hacked-up copy of
internal Perl code.  Applications that need to use older Perls can
discover if the embedded Perl they are using needs the workaround by
testing that the C preprocessor symbol \f(CW\*(C`HAS_SKIP_LOCALE_INIT\*(C' is not
defined.  [\s-1RT\s0 #38193]

- \(bu
\f(CW\*(C`BmRARE\*(C' and \f(CW\*(C`BmPREVIOUS\*(C' have been removed.  They were not used anywhere
and are not part of the \s-1API.\s0  For \s-1XS\s0 modules, they are now #defined as 0.

- \(bu
\f(CW\*(C`sv_force_normal\*(C', which usually croaks on read-only values, used to allow
read-only values to be modified at compile time.  This has been changed to
croak on read-only values regardless.  This change uncovered several core
bugs.

- \(bu
Perl's new copy-on-write mechanism  (which is now enabled by default),
allows any \f(CW\*(C`SvPOK\*(C' scalar to be automatically upgraded to a copy-on-write
scalar when copied. A reference count on the string buffer is stored in
the string buffer itself.
.Sp
For example:
.Sp
.Vb 10
    $ perl -MDevel::Peek -e\*(Aq$a="abc"; $b = $a; Dump $a; Dump $b\*(Aq
    SV = PV(0x260cd80) at 0x2620ad8
      REFCNT = 1
      FLAGS = (POK,IsCOW,pPOK)
      PV = 0x2619bc0 "abc"\\0
      CUR = 3
      LEN = 16
      COW_REFCNT = 1
    SV = PV(0x260ce30) at 0x2620b20
      REFCNT = 1
      FLAGS = (POK,IsCOW,pPOK)
      PV = 0x2619bc0 "abc"\\0
      CUR = 3
      LEN = 16
      COW_REFCNT = 1
.Ve
.Sp
Note that both scalars share the same \s-1PV\s0 buffer and have a \s-1COW_REFCNT\s0
greater than zero.
.Sp
This means that \s-1XS\s0 code which wishes to modify the \f(CW\*(C`SvPVX()\*(C' buffer of an
\s-1SV\s0 should call \f(CW\*(C`SvPV_force()\*(C' or similar first, to ensure a valid (and
unshared) buffer, and to call \f(CW\*(C`SvSETMAGIC()\*(C' afterwards. This in fact has
always been the case (for example hash keys were already copy-on-write);
this change just spreads the \s-1COW\s0 behaviour to a wider variety of SVs.
.Sp
One important difference is that before 5.18.0, shared hash-key scalars
used to have the \f(CW\*(C`SvREADONLY\*(C' flag set; this is no longer the case.
.Sp
This new behaviour can still be disabled by running *Configure* with
**-Accflags=-DPERL_NO_COW**.  This option will probably be removed in Perl
5.22.

- \(bu
\f(CW\*(C`PL_sawampersand\*(C' is now a constant.  The switch this variable provided
(to enable/disable the pre-match copy depending on whether \f(CW$& had been
seen) has been removed and replaced with copy-on-write, eliminating a few
bugs.
.Sp
The previous behaviour can still be enabled by running *Configure* with
**-Accflags=-DPERL_SAWAMPERSAND**.

- \(bu
The functions \f(CW\*(C`my_swap\*(C', \f(CW\*(C`my_htonl\*(C' and \f(CW\*(C`my_ntohl\*(C' have been removed.
It is unclear why these functions were ever marked as *A*, part of the
\s-1API. XS\s0 code can't call them directly, as it can't rely on them being
compiled. Unsurprisingly, no code on \s-1CPAN\s0 references them.

- \(bu
The signature of the \f(CW\*(C`Perl_re_intuit_start()\*(C' regex function has changed;
the function pointer \f(CW\*(C`intuit\*(C' in the regex engine plugin structure
has also changed accordingly. A new parameter, \f(CW\*(C`strbeg\*(C' has been added;
this has the same meaning as the same-named parameter in
\f(CW\*(C`Perl_regexec_flags\*(C'. Previously intuit would try to guess the start of
the string from the passed \s-1SV\s0 (if any), and would sometimes get it wrong
(e.g. with an overloaded \s-1SV\s0).

- \(bu
The signature of the \f(CW\*(C`Perl_regexec_flags()\*(C' regex function has
changed; the function pointer \f(CW\*(C`exec\*(C' in the regex engine plugin
structure has also changed to match.  The \f(CW\*(C`minend\*(C' parameter now has
type \f(CW\*(C`SSize_t\*(C' to better support 64-bit systems.

- \(bu
\s-1XS\s0 code may use various macros to change the case of a character or code
point (for example \f(CW\*(C`toLOWER_utf8()\*(C').  Only a couple of these were
documented until now;
and now they should be used in preference to calling the underlying
functions.  See \*(L"Character case changing\*(R" in perlapi.

- \(bu
The code dealt rather inconsistently with uids and gids. Some
places assumed that they could be safely stored in UVs, others
in IVs, others in ints. Four new macros are introduced:
**SvUID()**, **sv_setuid()**, **SvGID()**, and **sv_setgid()**

- \(bu
\f(CW\*(C`sv_pos_b2u_flags\*(C' has been added to the \s-1API.\s0  It is similar to \f(CW\*(C`sv_pos_b2u\*(C',
but supports long strings on 64-bit platforms.

- \(bu
\f(CW\*(C`PL_exit_flags\*(C' can now be used by perl embedders or other \s-1XS\s0 code to have
perl \f(CW\*(C`warn\*(C' or \f(CW\*(C`abort\*(C' on an attempted exit. [perl #52000]

- \(bu
Compiling with \f(CW\*(C`-Accflags=-PERL_BOOL_AS_CHAR\*(C' now allows C99 and \*(C+
compilers to emulate the aliasing of \f(CW\*(C`bool\*(C' to \f(CW\*(C`char\*(C' that perl does for
C89 compilers.  [perl #120314]

- \(bu
The \f(CW\*(C`sv\*(C' argument in \*(L"sv_2pv_flags\*(R" in perlapi, \*(L"sv_2iv_flags\*(R" in perlapi,
\*(L"sv_2uv_flags\*(R" in perlapi, and \*(L"sv_2nv_flags\*(R" in perlapi and their older wrappers
sv_2pv, sv_2iv, sv_2uv, sv_2nv, is now non-NULL. Passing \s-1NULL\s0 now will crash.
When the non-NULL marker was introduced en masse in 5.9.3 the functions
were marked non-NULL, but since the creation of the \s-1SV API\s0 in 5.0 alpha 2, if
\s-1NULL\s0 was passed, the functions returned 0 or false-type values. The code that
supports \f(CW\*(C`sv\*(C' argument being non-NULL dates to 5.0 alpha 2 directly, and
indirectly to Perl 1.0 (pre 5.0 api). The lack of documentation that the
functions accepted a \s-1NULL\s0 \f(CW\*(C`sv\*(C' was corrected in 5.11.0 and between 5.11.0
and 5.19.5 the functions were marked \s-1NULLOK.\s0 As an optimization the \s-1NULLOK\s0 code
has now been removed, and the functions became non-NULL marked again, because
core getter-type macros never pass \s-1NULL\s0 to these functions and would crash
before ever passing \s-1NULL.\s0
.Sp
The only way a \s-1NULL\s0 \f(CW\*(C`sv\*(C' can be passed to sv_2*v* functions is if \s-1XS\s0 code
directly calls sv_2*v*. This is unlikely as \s-1XS\s0 code uses Sv*V* macros to get
the underlying value out of the \s-1SV.\s0 One possible situation which leads to
a \s-1NULL\s0 \f(CW\*(C`sv\*(C' being passed to sv_2*v* functions, is if \s-1XS\s0 code defines its own
getter type Sv*V* macros, which check for \s-1NULL\s0 **before** dereferencing and
checking the \s-1SV\s0's flags through public \s-1API\s0 Sv*OK* macros or directly using
private \s-1API\s0 \f(CW\*(C`SvFLAGS\*(C', and if \f(CW\*(C`sv\*(C' is \s-1NULL,\s0 then calling the sv_2*v functions
with a \s-1NULL\s0 literal or passing the \f(CW\*(C`sv\*(C' containing a \s-1NULL\s0 value.

- \(bu
newATTRSUB is now a macro
.Sp
The public \s-1API\s0 newATTRSUB was previously a macro to the private
function Perl_newATTRSUB. Function Perl_newATTRSUB has been removed. newATTRSUB
is now macro to a different internal function.

- \(bu
Changes in warnings raised by \f(CW\*(C`utf8n_to_uvchr()\*(C'
.Sp
This bottom level function decodes the first character of a \s-1UTF-8\s0 string
into a code point.  It is accessible to \f(CW\*(C`XS\*(C' level code, but it's
discouraged from using it directly.  There are higher level functions
that call this that should be used instead, such as
\*(L"utf8_to_uvchr_buf\*(R" in perlapi.  For completeness though, this documents
some changes to it.  Now, tests for malformations are done before any
tests for other potential issues.  One of those issues involves code
points so large that they have never appeared in any official standard
(the current standard has scaled back the highest acceptable code point
from earlier versions).  It is possible (though not done in \s-1CPAN\s0) to
warn and/or forbid these code points, while accepting smaller code
points that are still above the legal Unicode maximum.  The warning
message for this now includes the code point if representable on the
machine.  Previously it always displayed raw bytes, which is what it
still does for non-representable code points.

- \(bu
Regexp engine changes that affect the pluggable regex engine interface
.Sp
Many flags that used to be exposed via regexp.h and used to populate the
extflags member of struct regexp have been removed. These fields were
technically private to Perl's own regexp engine and should not have been
exposed there in the first place.
.Sp
The affected flags are:
.Sp
.Vb 8
    RXf_NOSCAN
    RXf_CANY_SEEN
    RXf_GPOS_SEEN
    RXf_GPOS_FLOAT
    RXf_ANCH_BOL
    RXf_ANCH_MBOL
    RXf_ANCH_SBOL
    RXf_ANCH_GPOS
.Ve
.Sp
As well as the follow flag masks:
.Sp
.Vb 2
    RXf_ANCH_SINGLE
    RXf_ANCH
.Ve
.Sp
All have been renamed to PREGf_ equivalents and moved to regcomp.h.
.Sp
The behavior previously achieved by setting one or more of the RXf_ANCH_
flags (via the RXf_ANCH mask) have now been replaced by a *single* flag bit
in extflags:
.Sp
.Vb 1
    RXf_IS_ANCHORED
.Ve
.Sp
pluggable regex engines which previously used to set these flags should
now set this flag \s-1ALONE.\s0

- \(bu
The Perl core now consistently uses \f(CW\*(C`av_tindex()\*(C' (\*(L"the top index of an
array\*(R") as a more clearly-named synonym for \f(CW\*(C`av_len()\*(C'.

- \(bu
The obscure interpreter variable \f(CW\*(C`PL_timesbuf\*(C' is expected to be removed
early in the 5.21.x development series, so that Perl 5.22.0 will not provide
it to \s-1XS\s0 authors.  While the variable still exists in 5.20.0, we hope that
this advance warning of the deprecation will help anyone who is using that
variable.

## Selected Bug Fixes

Header "Selected Bug Fixes"

### Regular Expressions

Subsection "Regular Expressions"

- \(bu
Fixed a small number of regexp constructions that could either fail to
match or crash perl when the string being matched against was
allocated above the 2GB line on 32-bit systems. [\s-1RT\s0 #118175]

- \(bu
Various memory leaks involving the parsing of the \f(CW\*(C`(?[...])\*(C' regular
expression construct have been fixed.

- \(bu
\f(CW\*(C`(?[...])\*(C' now allows interpolation of precompiled patterns consisting of
\f(CW\*(C`(?[...])\*(C' with bracketed character classes inside (\f(CW\*(C`$pat =
qr/(?[\ [a]\ ])/; /(?[\ $pat\ ])/\*(C').  Formerly, the brackets would
confuse the regular expression parser.

- \(bu
The \*(L"Quantifier unexpected on zero-length expression\*(R" warning message could
appear twice starting in Perl v5.10 for a regular expression also
containing alternations (e.g., \*(L"a|b\*(R") triggering the trie optimisation.

- \(bu
Perl v5.18 inadvertently introduced a bug whereby interpolating mixed up-
and down-graded \s-1UTF-8\s0 strings in a regex could result in malformed \s-1UTF-8\s0
in the pattern: specifically if a downgraded character in the range
\f(CW\*(C`\\x80..\\xff\*(C' followed a \s-1UTF-8\s0 string, e.g.
.Sp
.Vb 3
    utf8::upgrade(  my $u = "\\x\{e5\}");
    utf8::downgrade(my $d = "\\x\{e5\}");
    /$u$d/
.Ve
.Sp
[\s-1RT\s0 #118297]

- \(bu
In regular expressions containing multiple code blocks, the values of
\f(CW$1, \f(CW$2, etc., set by nested regular expression calls would leak from
one block to the next.  Now these variables always refer to the outer
regular expression at the start of an embedded block [perl #117917].

- \(bu
\f(CW\*(C`/$qr/p\*(C' was broken in Perl 5.18.0; the \f(CW\*(C`/p\*(C' flag was ignored.  This has been
fixed. [perl #118213]

- \(bu
Starting in Perl 5.18.0, a construct like \f(CW\*(C`/[#](?\{\})/x\*(C' would have its \f(CW\*(C`#\*(C'
incorrectly interpreted as a comment.  The code block would be skipped,
unparsed.  This has been corrected.

- \(bu
Starting in Perl 5.001, a regular expression like \f(CW\*(C`/[#$a]/x\*(C' or \f(CW\*(C`/[#]$a/x\*(C'
would have its \f(CW\*(C`#\*(C' incorrectly interpreted as a comment, so the variable would
not interpolate.  This has been corrected. [perl #45667]

- \(bu
Perl 5.18.0 inadvertently made dereferenced regular expressions
(\f(CW\*(C`$\{\ qr//\ \}\*(C') false as booleans.  This has been fixed.

- \(bu
The use of \f(CW\*(C`\\G\*(C' in regular expressions, where it's not at the start of the
pattern, is now slightly less buggy (although it is still somewhat
problematic).

- \(bu
Where a regular expression included code blocks (\f(CW\*(C`/(?\{...\})/\*(C'), and where the
use of constant overloading triggered a re-compilation of the code block, the
second compilation didn't see its outer lexical scope.  This was a regression
in Perl 5.18.0.

- \(bu
The string position set by \f(CW\*(C`pos\*(C' could shift if the string changed
representation internally to or from utf8.  This could happen, e.g., with
references to objects with string overloading.

- \(bu
Taking references to the return values of two \f(CW\*(C`pos\*(C' calls with the same
argument, and then assigning a reference to one and \f(CW\*(C`undef\*(C' to the other,
could result in assertion failures or memory leaks.

- \(bu
Elements of @- and @+ now update correctly when they refer to non-existent
captures.  Previously, a referenced element (\f(CW\*(C`$ref = \\$-[1]\*(C') could refer to
the wrong match after subsequent matches.

- \(bu
The code that parses regex backrefs (or ambiguous backref/octals) such as \\123
did a simple **atoi()**, which could wrap round to negative values on long digit
strings and cause segmentation faults.  This has now been fixed.  [perl
#119505]

- \(bu
Assigning another typeglob to \f(CW\*(C`*^R\*(C' no longer makes the regular expression
engine crash.

- \(bu
The \f(CW\*(C`\\N\*(C' regular expression escape, when used without the curly braces (to
mean \f(CW\*(C`[^\\n]\*(C'), was ignoring a following \f(CW\*(C`*\*(C' if followed by whitespace
under /x.  It had been this way since \f(CW\*(C`\\N\*(C' to mean \f(CW\*(C`[^\\n]\*(C' was introduced
in 5.12.0.

- \(bu
\f(CW\*(C`s///\*(C', \f(CW\*(C`tr///\*(C' and \f(CW\*(C`y///\*(C' now work when a wide character is used as the
delimiter.  [perl #120463]

- \(bu
Some cases of unterminated (?...) sequences in regular expressions (e.g.,
\f(CW\*(C`/(?</\*(C') have been fixed to produce the proper error message instead of
\*(L"panic: memory wrap\*(R".  Other cases (e.g., \f(CW\*(C`/(?(/\*(C') have yet to be fixed.

- \(bu
When a reference to a reference to an overloaded object was returned from
a regular expression \f(CW\*(C`(??\{...\})\*(C' code block, an incorrect implicit
dereference could take place if the inner reference had been returned by
a code block previously.

- \(bu
A tied variable returned from \f(CW\*(C`(??\{...\})\*(C' sees the inner values of match
variables (i.e., the \f(CW$1 etc. from any matches inside the block) in its
\s-1FETCH\s0 method.  This was not the case if a reference to an overloaded object
was the last thing assigned to the tied variable.  Instead, the match
variables referred to the outer pattern during the \s-1FETCH\s0 call.

- \(bu
Fix unexpected tainting via regexp using locale. Previously, under certain
conditions, the use of character classes could cause tainting when it
shouldn't. Some character classes are locale-dependent, but before this
patch, sometimes tainting was happening even for character classes that
don't depend on the locale. [perl #120675]

- \(bu
Under certain conditions, Perl would throw an error if in a lookbehind
assertion in a regexp, the assertion referred to a named subpattern,
complaining the lookbehind was variable when it wasn't. This has been
fixed. [perl #120600], [perl #120618]. The current fix may be improved
on in the future.

- \(bu
\f(CW$^R wasn't available outside of the regular expression that
initialized it.  [perl #121070]

- \(bu
A large set of fixes and refactoring for **re_intuit_start()** was merged,
the highlights are:

> 
- \(bu
Fixed a panic when compiling the regular expression
\f(CW\*(C`/\\x\{100\}[xy]\\x\{100\}\{2\}/\*(C'.

- \(bu
Fixed a performance regression when performing a global pattern match
against a \s-1UTF-8\s0 string.  [perl #120692]

- \(bu
Fixed another performance issue where matching a regular expression
like \f(CW\*(C`/ab.\{1,2\}x/\*(C' against a long \s-1UTF-8\s0 string would unnecessarily
calculate byte offsets for a large portion of the string. [perl
#120692]



> 


- \(bu
Fixed an alignment error when compiling regular expressions when built
with \s-1GCC\s0 on HP-UX 64-bit.

- \(bu
On 64-bit platforms \f(CW\*(C`pos\*(C' can now be set to a value higher than 2**31-1.
[perl #72766]

### Perl 5 Debugger and -d

Subsection "Perl 5 Debugger and -d"

- \(bu
The debugger's \f(CW\*(C`man\*(C' command been fixed. It was broken in the v5.18.0
release. The \f(CW\*(C`man\*(C' command is aliased to the names \f(CW\*(C`doc\*(C' and \f(CW\*(C`perldoc\*(C' -
all now work again.

- \(bu
\f(CW@_ is now correctly visible in the debugger, fixing a regression
introduced in v5.18.0's debugger. [\s-1RT\s0 #118169]

- \(bu
Under copy-on-write builds (the default as of 5.20.0) \f(CW\*(C`$\{\*(Aq_<-e\*(Aq\}[0]\*(C'
no longer gets mangled.  This is the first line of input saved for the
debugger's use for one-liners [perl #118627].

- \(bu
On non-threaded builds, setting \f(CW\*(C`$\{"_<filename"\}\*(C' to a reference or
typeglob no longer causes \f(CW\*(C`_\|_FILE_\|_\*(C' and some error messages to produce a
corrupt string, and no longer prevents \f(CW\*(C`#line\*(C' directives in string evals from
providing the source lines to the debugger.  Threaded builds were unaffected.

- \(bu
Starting with Perl 5.12, line numbers were off by one if the **-d** switch was
used on the #! line.  Now they are correct.

- \(bu
\f(CW\*(C`*DB::DB = sub \{\} if 0\*(C' no longer stops Perl's debugging mode from finding
\f(CW\*(C`DB::DB\*(C' subs declared thereafter.

- \(bu
\f(CW\*(C`%\{\*(Aq_<...\*(Aq\}\*(C' hashes now set breakpoints on the corresponding \f(CW\*(C`@\{\*(Aq_<...\*(Aq\}\*(C'
rather than whichever array \f(CW@DB::dbline is aliased to.  [perl #119799]

- \(bu
Call set-magic when setting \f(CW$DB::sub.  [perl #121255]

- \(bu
The debugger's \*(L"n\*(R" command now respects lvalue subroutines and steps over
them [perl #118839].

### Lexical Subroutines

Subsection "Lexical Subroutines"

- \(bu
Lexical constants (\f(CW\*(C`my sub a() \{ 42 \}\*(C') no longer crash when inlined.

- \(bu
Parameter prototypes attached to lexical subroutines are now respected when
compiling sub calls without parentheses.  Previously, the prototypes were
honoured only for calls *with* parentheses. [\s-1RT\s0 #116735]

- \(bu
Syntax errors in lexical subroutines in combination with calls to the same
subroutines no longer cause crashes at compile time.

- \(bu
Deep recursion warnings no longer crash lexical subroutines. [\s-1RT\s0 #118521]

- \(bu
The dtrace sub-entry probe now works with lexical subs, instead of
crashing [perl #118305].

- \(bu
Undefining an inlinable lexical subroutine (\f(CW\*(C`my sub foo() \{ 42 \} undef
&foo\*(C') would result in a crash if warnings were turned on.

- \(bu
An undefined lexical sub used as an inherited method no longer crashes.

- \(bu
The presence of a lexical sub named \*(L"\s-1CORE\*(R"\s0 no longer stops the \s-1CORE::\s0
prefix from working.

### Everything Else

Subsection "Everything Else"

- \(bu
The \s-1OP\s0 allocation code now returns correctly aligned memory in all cases
for \f(CW\*(C`struct pmop\*(C'. Previously it could return memory only aligned to a
4-byte boundary, which is not correct for an ithreads build with 64 bit IVs
on some 32 bit platforms. Notably, this caused the build to fail completely
on sparc GNU/Linux. [\s-1RT\s0 #118055]

- \(bu
Evaluating large hashes in scalar context is now much faster, as the number
of used chains in the hash is now cached for larger hashes. Smaller hashes
continue not to store it and calculate it when needed, as this saves one \s-1IV.\s0
That would be 1 \s-1IV\s0 overhead for every object built from a hash. [\s-1RT\s0 #114576]

- \(bu
Perl v5.16 inadvertently introduced a bug whereby calls to XSUBs that were
not visible at compile time were treated as lvalues and could be assigned
to, even when the subroutine was not an lvalue sub.  This has been fixed.
[\s-1RT\s0 #117947]

- \(bu
In Perl v5.18.0 dualvars that had an empty string for the string part but a
non-zero number for the number part starting being treated as true.  In
previous versions they were treated as false, the string representation
taking precedence.  The old behaviour has been restored. [\s-1RT\s0 #118159]

- \(bu
Since Perl v5.12, inlining of constants that override built-in keywords of
the same name had countermanded \f(CW\*(C`use subs\*(C', causing subsequent mentions of
the constant to use the built-in keyword instead.  This has been fixed.

- \(bu
The warning produced by \f(CW\*(C`-l $handle\*(C' now applies to \s-1IO\s0 refs and globs, not
just to glob refs.  That warning is also now UTF8-clean. [\s-1RT\s0 #117595]

- \(bu
\f(CW\*(C`delete local $ENV\{nonexistent_env_var\}\*(C' no longer leaks memory.

- \(bu
\f(CW\*(C`sort\*(C' and \f(CW\*(C`require\*(C' followed by a keyword prefixed with \f(CW\*(C`CORE::\*(C' now
treat it as a keyword, and not as a subroutine or module name. [\s-1RT\s0 #24482]

- \(bu
Through certain conundrums, it is possible to cause the current package to
be freed.  Certain operators (\f(CW\*(C`bless\*(C', \f(CW\*(C`reset\*(C', \f(CW\*(C`open\*(C', \f(CW\*(C`eval\*(C') could
not cope and would crash.  They have been made more resilient. [\s-1RT\s0 #117941]

- \(bu
Aliasing filehandles through glob-to-glob assignment would not update
internal method caches properly if a package of the same name as the
filehandle existed, resulting in filehandle method calls going to the
package instead.  This has been fixed.

- \(bu
\f(CW\*(C`./Configure -de -Dusevendorprefix\*(C' didn't default. [\s-1RT\s0 #64126]

- \(bu
The \f(CW\*(C`Statement unlikely to be reached\*(C' warning was listed in
perldiag as an \f(CW\*(C`exec\*(C'-category warning, but was enabled and disabled
by the \f(CW\*(C`syntax\*(C' category.  On the other hand, the \f(CW\*(C`exec\*(C' category
controlled its fatal-ness.  It is now entirely handled by the \f(CW\*(C`exec\*(C'
category.

- \(bu
The \*(L"Replacement list is longer that search list\*(R" warning for \f(CW\*(C`tr///\*(C' and
\f(CW\*(C`y///\*(C' no longer occurs in the presence of the \f(CW\*(C`/c\*(C' flag. [\s-1RT\s0 #118047]

- \(bu
Stringification of NVs are not cached so that the lexical locale controls
stringification of the decimal point. [perl #108378] [perl #115800]

- \(bu
There have been several fixes related to Perl's handling of locales.  perl
#38193 was described above in \*(L"Internal Changes\*(R".
Also fixed is
#118197, where the radix (decimal point) character had to be an \s-1ASCII\s0
character (which doesn't work for some non-Western languages);
and #115808, in which \f(CW\*(C`POSIX::setlocale()\*(C' on failure returned an
\f(CW\*(C`undef\*(C' which didn't warn about not being defined even if those
warnings were enabled.

- \(bu
Compiling a \f(CW\*(C`split\*(C' operator whose third argument is a named constant
evaluating to 0 no longer causes the constant's value to change.

- \(bu
A named constant used as the second argument to \f(CW\*(C`index\*(C' no longer gets
coerced to a string if it is a reference, regular expression, dualvar, etc.

- \(bu
A named constant evaluating to the undefined value used as the second
argument to \f(CW\*(C`index\*(C' no longer produces \*(L"uninitialized\*(R" warnings at compile
time.  It will still produce them at run time.

- \(bu
When a scalar was returned from a subroutine in \f(CW@INC, the referenced scalar
was magically converted into an \s-1IO\s0 thingy, possibly resulting in \*(L"Bizarre
copy\*(R" errors if that scalar continued to be used elsewhere.  Now Perl uses
an internal copy of the scalar instead.

- \(bu
Certain uses of the \f(CW\*(C`sort\*(C' operator are optimised to modify an array in
place, such as \f(CW\*(C`@a = sort @a\*(C'.  During the sorting, the array is made
read-only.  If a sort block should happen to die, then the array remained
read-only even outside the \f(CW\*(C`sort\*(C'.  This has been fixed.

- \(bu
\f(CW$a and \f(CW$b inside a sort block are aliased to the actual arguments to
\f(CW\*(C`sort\*(C', so they can be modified through those two variables.  This did not
always work, e.g., for lvalue subs and \f(CW$#ary, and probably many other
operators.  It works now.

- \(bu
The arguments to \f(CW\*(C`sort\*(C' are now all in list context.  If the \f(CW\*(C`sort\*(C'
itself were called in void or scalar context, then *some*, but not all, of
the arguments used to be in void or scalar context.

- \(bu
Subroutine prototypes with Unicode characters above U+00FF were getting
mangled during closure cloning.  This would happen with subroutines closing
over lexical variables declared outside, and with lexical subs.

- \(bu
\f(CW\*(C`UNIVERSAL::can\*(C' now treats its first argument the same way that method
calls do: Typeglobs and glob references with non-empty \s-1IO\s0 slots are treated
as handles, and strings are treated as filehandles, rather than packages,
if a handle with that name exists [perl #113932].

- \(bu
Method calls on typeglobs (e.g., \f(CW\*(C`*ARGV->getline\*(C') used to stringify
the typeglob and then look it up again.  Combined with changes in Perl
5.18.0, this allowed \f(CW\*(C`*foo->bar\*(C' to call methods on the \*(L"foo\*(R" package
(like \f(CW\*(C`foo->bar\*(C').  In some cases it could cause the method to be
called on the wrong handle.  Now a typeglob argument is treated as a
handle (just like \f(CW\*(C`(\\*foo)->bar\*(C'), or, if its \s-1IO\s0 slot is empty, an
error is raised.

- \(bu
Assigning a vstring to a tied variable or to a subroutine argument aliased
to a nonexistent hash or array element now works, without flattening the
vstring into a regular string.

- \(bu
\f(CW\*(C`pos\*(C', \f(CW\*(C`tie\*(C', \f(CW\*(C`tied\*(C' and \f(CW\*(C`untie\*(C' did not work
properly on subroutine arguments aliased to nonexistent
hash and array elements [perl #77814, #27010].

- \(bu
The \f(CW\*(C`=>\*(C' fat arrow operator can now quote built-in keywords even if it
occurs on the next line, making it consistent with how it treats other
barewords.

- \(bu
Autovivifying a subroutine stub via \f(CW\*(C`\$glob\*(C' started causing crashes in Perl
5.18.0 if the \f(CW$glob was merely a copy of a real glob, i.e., a scalar that had
had a glob assigned to it.  This has been fixed. [perl #119051]

- \(bu
Perl used to leak an implementation detail when it came to referencing the
return values of certain operators.  \f(CW\*(C`for ($a+$b) \{ warn \\$_; warn \\$_ \}\*(C' used
to display two different memory addresses, because the \f(CW\*(C`\\\*(C' operator was
copying the variable.  Under threaded builds, it would also happen for
constants (\f(CW\*(C`for(1) \{ ... \}\*(C').  This has been fixed. [perl #21979, #78194,
#89188, #109746, #114838, #115388]

- \(bu
The range operator \f(CW\*(C`..\*(C' was returning the same modifiable scalars with each
call, unless it was the only thing in a \f(CW\*(C`foreach\*(C' loop header.  This meant
that changes to values within the list returned would be visible the next time
the operator was executed. [perl #3105]

- \(bu
Constant folding and subroutine inlining no longer cause operations that would
normally return new modifiable scalars to return read-only values instead.

- \(bu
Closures of the form \f(CW\*(C`sub () \{ $some_variable \}\*(C' are no longer inlined,
causing changes to the variable to be ignored by callers of the subroutine.
[perl #79908]

- \(bu
Return values of certain operators such as \f(CW\*(C`ref\*(C' would sometimes be shared
between recursive calls to the same subroutine, causing the inner call to
modify the value returned by \f(CW\*(C`ref\*(C' in the outer call.  This has been fixed.

- \(bu
\f(CW\*(C`_\|_PACKAGE_\|_\*(C' and constants returning a package name or hash key are now
consistently read-only.  In various previous Perl releases, they have become
mutable under certain circumstances.

- \(bu
Enabling \*(L"used once\*(R" warnings no longer causes crashes on stash circularities
created at compile time (\f(CW\*(C`*Foo::Bar::Foo:: = *Foo::\*(C').

- \(bu
Undef constants used in hash keys (\f(CW\*(C`use constant u => undef; $h\{+u\}\*(C') no
longer produce \*(L"uninitialized\*(R" warnings at compile time.

- \(bu
Modifying a substitution target inside the substitution replacement no longer
causes crashes.

- \(bu
The first statement inside a string eval used to use the wrong pragma setting
sometimes during constant folding.  \f(CW\*(C`eval \*(Aquc chr 0xe0\*(Aq\*(C' would randomly choose
between Unicode, byte, and locale semantics.  This has been fixed.

- \(bu
The handling of return values of \f(CW@INC filters (subroutines returned by
subroutines in \f(CW@INC) has been fixed in various ways.  Previously tied variables
were mishandled, and setting \f(CW$_ to a reference or typeglob could result in
crashes.

- \(bu
The \f(CW\*(C`SvPVbyte\*(C' \s-1XS\s0 function has been fixed to work with tied scalars returning
something other than a string.  It used to return utf8 in those cases where
\f(CW\*(C`SvPV\*(C' would.

- \(bu
Perl 5.18.0 inadvertently made \f(CW\*(C`--\*(C' and \f(CW\*(C`++\*(C' crash on dereferenced regular
expressions, and stopped \f(CW\*(C`++\*(C' from flattening vstrings.

- \(bu
\f(CW\*(C`bless\*(C' no longer dies with \*(L"Can't bless non-reference value\*(R" if its first
argument is a tied reference.

- \(bu
\f(CW\*(C`reset\*(C' with an argument no longer skips copy-on-write scalars, regular
expressions, typeglob copies, and vstrings.  Also, when encountering those or
read-only values, it no longer skips any array or hash with the same name.

- \(bu
\f(CW\*(C`reset\*(C' with an argument now skips scalars aliased to typeglobs
(\f(CW\*(C`for $z (*foo) \{ reset "z" \}\*(C').  Previously it would corrupt memory or crash.

- \(bu
\f(CW\*(C`ucfirst\*(C' and \f(CW\*(C`lcfirst\*(C' were not respecting the bytes pragma.  This was a
regression from Perl 5.12. [perl #117355]

- \(bu
Changes to \f(CW\*(C`UNIVERSAL::DESTROY\*(C' now update \s-1DESTROY\s0 caches in all classes,
instead of causing classes that have already had objects destroyed to continue
using the old sub.  This was a regression in Perl 5.18. [perl #114864]

- \(bu
All known false-positive occurrences of the deprecation warning \*(L"Useless use of
'\\'; doesn't escape metacharacter '%c'\*(R", added in Perl 5.18.0, have been
removed. [perl #119101]

- \(bu
The value of $^E is now saved across signal handlers on Windows.  [perl #85104]

- \(bu
A lexical filehandle (as in \f(CW\*(C`open my $fh...\*(C') is usually given a name based on
the current package and the name of the variable, e.g. \*(L"main::$fh\*(R".  Under
recursion, the filehandle was losing the \*(L"$fh\*(R" part of the name.  This has been
fixed.

- \(bu
Uninitialized values returned by XSUBs are no longer exempt from uninitialized
warnings.  [perl #118693]

- \(bu
\f(CW\*(C`elsif ("")\*(C' no longer erroneously produces a warning about void context.
[perl #118753]

- \(bu
Passing \f(CW\*(C`undef\*(C' to a subroutine now causes \f(CW@_ to contain the same read-only
undefined scalar that \f(CW\*(C`undef\*(C' returns.  Furthermore, \f(CW\*(C`exists $_[0]\*(C' will now
return true if \f(CW\*(C`undef\*(C' was the first argument.  [perl #7508, #109726]

- \(bu
Passing a non-existent array element to a subroutine does not usually
autovivify it unless the subroutine modifies its argument.  This did not work
correctly with negative indices and with non-existent elements within the
array.  The element would be vivified immediately.  The delayed vivification
has been extended to work with those.  [perl #118691]

- \(bu
Assigning references or globs to the scalar returned by $#foo after the \f(CW@foo
array has been freed no longer causes assertion failures on debugging builds
and memory leaks on regular builds.

- \(bu
On 64-bit platforms, large ranges like 1..1000000000000 no longer crash, but
eat up all your memory instead.  [perl #119161]

- \(bu
\f(CW\*(C`_\|_DATA_\|_\*(C' now puts the \f(CW\*(C`DATA\*(C' handle in the right package, even if the
current package has been renamed through glob assignment.

- \(bu
When \f(CW\*(C`die\*(C', \f(CW\*(C`last\*(C', \f(CW\*(C`next\*(C', \f(CW\*(C`redo\*(C', \f(CW\*(C`goto\*(C' and \f(CW\*(C`exit\*(C' unwind the scope,
it is possible for \f(CW\*(C`DESTROY\*(C' recursively to call a subroutine or format that
is currently being exited.  It that case, sometimes the lexical variables
inside the sub would start out having values from the outer call, instead of
being undefined as they should.  This has been fixed.  [perl #119311]

- \(bu
$\{^MPEN\} is no longer treated as a synonym for $\{^MATCH\}.

- \(bu
Perl now tries a little harder to return the correct line number in
\f(CW\*(C`(caller)[2]\*(C'.  [perl #115768]

- \(bu
Line numbers inside multiline quote-like operators are now reported correctly.
[perl #3643]

- \(bu
\f(CW\*(C`#line\*(C' directives inside code embedded in quote-like operators are now
respected.

- \(bu
Line numbers are now correct inside the second here-doc when two here-doc
markers occur on the same line.

- \(bu
An optimization in Perl 5.18 made incorrect assumptions causing a bad
interaction with the Devel::CallParser \s-1CPAN\s0 module.  If the module was
loaded then lexical variables declared in separate statements following a
\f(CW\*(C`my(...)\*(C' list might fail to be cleared on scope exit.

- \(bu
\f(CW&xsub and \f(CW\*(C`goto &xsub\*(C' calls now allow the called subroutine to autovivify
elements of \f(CW@_.

- \(bu
\f(CW&xsub and \f(CW\*(C`goto &xsub\*(C' no longer crash if *_ has been undefined and has no
\s-1ARRAY\s0 entry (i.e. \f(CW@_ does not exist).

- \(bu
\f(CW&xsub and \f(CW\*(C`goto &xsub\*(C' now work with tied \f(CW@_.

- \(bu
Overlong identifiers no longer cause a buffer overflow (and a crash).  They
started doing so in Perl 5.18.

- \(bu
The warning \*(L"Scalar value \f(CW@hash\{foo\} better written as \f(CW$hash\{foo\}\*(R" now produces
far fewer false positives.  In particular, \f(CW@hash\{+function_returning_a_list\}
and \f(CW@hash\{ qw "foo bar baz" \} no longer warn.  The same applies to array
slices.  [perl #28380, #114024]

- \(bu
\f(CW\*(C`$! = EINVAL; waitpid(0, WNOHANG);\*(C' no longer goes into an internal infinite
loop.  [perl #85228]

- \(bu
A possible segmentation fault in filehandle duplication has been fixed.

- \(bu
A subroutine in \f(CW@INC can return a reference to a scalar containing the initial
contents of the file.  However, that scalar was freed prematurely if not
referenced elsewhere, giving random results.

- \(bu
\f(CW\*(C`last\*(C' no longer returns values that the same statement has accumulated so
far, fixing amongst other things the long-standing bug that \f(CW\*(C`push @a, last\*(C'
would try to return the \f(CW@a, copying it like a scalar in the process and
resulting in the error, \*(L"Bizarre copy of \s-1ARRAY\s0 in last.\*(R"  [perl #3112]

- \(bu
In some cases, closing file handles opened to pipe to or from a process, which
had been duplicated into a standard handle, would call perl's internal waitpid
wrapper with a pid of zero.  With the fix for [perl #85228] this zero pid was
passed to \f(CW\*(C`waitpid\*(C', possibly blocking the process.  This wait for process
zero no longer occurs.  [perl #119893]

- \(bu
\f(CW\*(C`select\*(C' used to ignore magic on the fourth (timeout) argument, leading to
effects such as \f(CW\*(C`select\*(C' blocking indefinitely rather than the expected sleep
time.  This has now been fixed.  [perl #120102]

- \(bu
The class name in \f(CW\*(C`for my class $foo\*(C' is now parsed correctly.  In the case of
the second character of the class name being followed by a digit (e.g. 'a1b')
this used to give the error \*(L"Missing $ on loop variable\*(R".  [perl #120112]

- \(bu
Perl 5.18.0 accidentally disallowed \f(CW\*(C`-bareword\*(C' under \f(CW\*(C`use strict\*(C' and
\f(CW\*(C`use integer\*(C'.  This has been fixed.  [perl #120288]

- \(bu
\f(CW\*(C`-a\*(C' at the start of a line (or a hyphen with any single letter that is
not a filetest operator) no longer produces an erroneous 'Use of \*(L"-a\*(R"
without parentheses is ambiguous' warning.  [perl #120288]

- \(bu
Lvalue context is now properly propagated into bare blocks and \f(CW\*(C`if\*(C' and
\f(CW\*(C`else\*(C' blocks in lvalue subroutines.  Previously, arrays and hashes would
sometimes incorrectly be flattened when returned in lvalue list context, or
\*(L"Bizarre copy\*(R" errors could occur.  [perl #119797]

- \(bu
Lvalue context is now propagated to the branches of \f(CW\*(C`||\*(C' and \f(CW\*(C`&&\*(C' (and
their alphabetic equivalents, \f(CW\*(C`or\*(C' and \f(CW\*(C`and\*(C').  This means
\f(CW\*(C`foreach (pos $x || pos $y) \{...\}\*(C' now allows \f(CW\*(C`pos\*(C' to be modified
through \f(CW$_.

- \(bu
\f(CW\*(C`stat\*(C' and \f(CW\*(C`readline\*(C' remember the last handle used; the former
for the special \f(CW\*(C`_\*(C' filehandle, the latter for \f(CW\*(C`$\{^LAST_FH\}\*(C'.
\f(CW\*(C`eval "*foo if 0"\*(C' where *foo was the last handle passed to \f(CW\*(C`stat\*(C'
or \f(CW\*(C`readline\*(C' could cause that handle to be forgotten if the
handle were not opened yet.  This has been fixed.

- \(bu
Various cases of \f(CW\*(C`delete $::\{a\}\*(C', \f(CW\*(C`delete $::\{ENV\}\*(C' etc. causing a crash
have been fixed.  [perl #54044]

- \(bu
Setting \f(CW$! to \s-1EACCESS\s0 before calling \f(CW\*(C`require\*(C' could affect
\f(CW\*(C`require\*(C''s behaviour.  This has been fixed.

- \(bu
The \*(L"Can't use \\1 to mean \f(CW$1 in expression\*(R" warning message now only occurs
on the right-hand (replacement) part of a substitution.  Formerly it could
happen in code embedded in the left-hand side, or in any other quote-like
operator.

- \(bu
Blessing into a reference (\f(CW\*(C`bless $thisref, $thatref\*(C') has long been
disallowed, but magical scalars for the second like \f(CW$/ and those tied
were exempt.  They no longer are.  [perl #119809]

- \(bu
Blessing into a reference was accidentally allowed in 5.18 if the class
argument were a blessed reference with stale method caches (i.e., whose
class had had subs defined since the last method call).  They are
disallowed once more, as in 5.16.

- \(bu
\f(CW\*(C`$x->\{key\}\*(C' where \f(CW$x was declared as \f(CW\*(C`my Class $x\*(C' no longer crashes
if a Class::FIELDS subroutine stub has been declared.

- \(bu
\f(CW@$obj\{\*(Aqkey\*(Aq\} and \f(CW\*(C`$\{$obj\}\{key\}\*(C' used to be exempt from compile-time
field checking (\*(L"No such class field\*(R"; see fields) but no longer are.

- \(bu
A nonexistent array element with a large index passed to a subroutine that
ties the array and then tries to access the element no longer results in a
crash.

- \(bu
Declaring a subroutine stub named \s-1NEGATIVE_INDICES\s0 no longer makes negative
array indices crash when the current package is a tied array class.

- \(bu
Declaring a \f(CW\*(C`require\*(C', \f(CW\*(C`glob\*(C', or \f(CW\*(C`do\*(C' subroutine stub in the
\s-1CORE::GLOBAL::\s0 package no longer makes compilation of calls to the
corresponding functions crash.

- \(bu
Aliasing \s-1CORE::GLOBAL::\s0 functions to constants stopped working in Perl 5.10
but has now been fixed.

- \(bu
When \f(CW\*(C`\`...\`\*(C' or \f(CW\*(C`qx/.../\*(C' calls a \f(CW\*(C`readpipe\*(C' override, double-quotish
interpolation now happens, as is the case when there is no override.
Previously, the presence of an override would make these quote-like
operators act like \f(CW\*(C`q\{\}\*(C', suppressing interpolation.  [perl #115330]

- \(bu
\f(CW\*(C`<<<\`...\`\*(C' here-docs (with backticks as the delimiters) now call
\f(CW\*(C`readpipe\*(C' overrides.  [perl #119827]

- \(bu
\f(CW\*(C`&CORE::exit()\*(C' and \f(CW\*(C`&CORE::die()\*(C' now respect vmsish hints.

- \(bu
Undefining a glob that triggers a \s-1DESTROY\s0 method that undefines the same
glob is now safe.  It used to produce \*(L"Attempt to free unreferenced glob
pointer\*(R" warnings and leak memory.

- \(bu
If subroutine redefinition (\f(CW\*(C`eval \*(Aqsub foo\{\}\*(Aq\*(C' or \f(CW\*(C`newXS\*(C' for \s-1XS\s0 code)
triggers a \s-1DESTROY\s0 method on the sub that is being redefined, and that
method assigns a subroutine to the same slot (\f(CW\*(C`*foo = sub \{\}\*(C'), \f(CW$_[0]
is no longer left pointing to a freed scalar.  Now \s-1DESTROY\s0 is delayed until
the new subroutine has been installed.

- \(bu
On Windows, perl no longer calls **CloseHandle()** on a socket handle.  This makes
debugging easier on Windows by removing certain irrelevant bad handle
exceptions.  It also fixes a race condition that made socket functions randomly
fail in a Perl process with multiple \s-1OS\s0 threads, and possible test failures in
*dist/IO/t/cachepropagate-tcp.t*.  [perl #120091/118059]

- \(bu
Formats involving \s-1UTF-8\s0 encoded strings, or strange vars like ties,
overloads, or stringified refs (and in recent
perls, pure \s-1NOK\s0 vars) would generally do the wrong thing in formats
when the var is treated as a string and repeatedly chopped, as in
\f(CW\*(C`^<<<~~\*(C' and similar. This has now been resolved.
[perl #33832/45325/113868/119847/119849/119851]

- \(bu
\f(CW\*(C`semctl(..., SETVAL, ...)\*(C' would set the semaphore to the top
32-bits of the supplied integer instead of the bottom 32-bits on
64-bit big-endian systems. [perl #120635]

- \(bu
\f(CW\*(C`readdir()\*(C' now only sets \f(CW$! on error.  \f(CW$! is no longer set
to \f(CW\*(C`EBADF\*(C' when then terminating \f(CW\*(C`undef\*(C' is read from the directory
unless the system call sets \f(CW$!. [perl #118651]

- \(bu
\f(CW&CORE::glob no longer causes an intermittent crash due to perl's stack
getting corrupted. [perl #119993]

- \(bu
\f(CW\*(C`open\*(C' with layers that load modules (e.g., \*(L"<:encoding(utf8)\*(R") no longer
runs the risk of crashing due to stack corruption.

- \(bu
Perl 5.18 broke autoloading via \f(CW\*(C`->SUPER::foo\*(C' method calls by looking
up \s-1AUTOLOAD\s0 from the current package rather than the current package's
superclass.  This has been fixed. [perl #120694]

- \(bu
A longstanding bug causing \f(CW\*(C`do \{\} until CONSTANT\*(C', where the constant
holds a true value, to read unallocated memory has been resolved.  This
would usually happen after a syntax error.  In past versions of Perl it has
crashed intermittently. [perl #72406]

- \(bu
Fix HP-UX \f(CW$! failure. HP-UX **strerror()** returns an empty string for an
unknown error code.  This caused an assertion to fail under \s-1DEBUGGING\s0
builds.  Now instead, the returned string for \f(CW"$!" contains text
indicating the code is for an unknown error.

- \(bu
Individually-tied elements of \f(CW@INC (as in \f(CW\*(C`tie $INC[0]...\*(C') are now
handled correctly.  Formerly, whether a sub returned by such a tied element
would be treated as a sub depended on whether a \s-1FETCH\s0 had occurred
previously.

- \(bu
\f(CW\*(C`getc\*(C' on a byte-sized handle after the same \f(CW\*(C`getc\*(C' operator had been
used on a utf8 handle used to treat the bytes as utf8, resulting in erratic
behavior (e.g., malformed \s-1UTF-8\s0 warnings).

- \(bu
An initial \f(CW\*(C`\{\*(C' at the beginning of a format argument line was always
interpreted as the beginning of a block prior to v5.18.  In Perl v5.18, it
started being treated as an ambiguous token.  The parser would guess
whether it was supposed to be an anonymous hash constructor or a block
based on the contents.  Now the previous behaviour has been restored.
[perl #119973]

- \(bu
In Perl v5.18 \f(CW\*(C`undef *_; goto &sub\*(C' and \f(CW\*(C`local *_; goto &sub\*(C' started
crashing.  This has been fixed. [perl #119949]

- \(bu
Backticks (\f(CW\*(C` \`\` \*(C' or \f(CW\*(C` qx// \*(C') combined with multiple threads on
Win32 could result in output sent to stdout on one thread being
captured by backticks of an external command in another thread.
.Sp
This could occur for pseudo-forked processes too, as Win32's
pseudo-fork is implemented in terms of threads.  [perl #77672]

- \(bu
\f(CW\*(C`open $fh, ">+", undef\*(C' no longer leaks memory when \s-1TMPDIR\s0 is set
but points to a directory a temporary file cannot be created in.  [perl
#120951]

- \(bu
\f(CW\*(C` for ( $h\{k\} || \*(Aq\*(Aq ) \*(C' no longer auto-vivifies \f(CW$h\{k\}.  [perl
#120374]

- \(bu
On Windows machines, Perl now emulates the \s-1POSIX\s0 use of the environment
for locale initialization.  Previously, the environment was ignored.
See \*(L"\s-1ENVIRONMENT\*(R"\s0 in perllocale.

- \(bu
Fixed a crash when destroying a self-referencing \s-1GLOB.\s0  [perl #121242]

## Known Problems

Header "Known Problems"

- \(bu
IO::Socket is known to fail tests on \s-1AIX 5.3.\s0  There is
a patch <https://github.com/Perl/perl5/issues/13484> in the request
tracker, #120835, which may be applied to future releases.

- \(bu
The following modules are known to have test failures with this version of
Perl.  Patches have been submitted, so there will hopefully be new releases
soon:

> 
- \(bu
Data::Structure::Util version 0.15

- \(bu
HTML::StripScripts version 1.05

- \(bu
List::Gather version 0.08.



> 


## Obituary

Header "Obituary"
Diana Rosa, 27, of Rio de Janeiro, went to her long rest on May 10,
2014, along with the plush camel she kept hanging on her computer screen
all the time. She was a passionate Perl hacker who loved the language and its
community, and who never missed a Rio.pm event. She was a true artist, an
enthusiast about writing code, singing arias and graffiting walls. We'll never
forget you.

Greg McCarroll died on August 28, 2013.

Greg was well known for many good reasons. He was one of the organisers of
the first YAPC::Europe, which concluded with an unscheduled auction where he
frantically tried to raise extra money to avoid the conference making a
loss. It was Greg who mistakenly arrived for a london.pm meeting a week
late; some years later he was the one who sold the choice of official
meeting date at a YAPC::Europe auction, and eventually as glorious leader of
london.pm he got to inherit the irreverent confusion that he had created.

Always helpful, friendly and cheerfully optimistic, you will be missed, but
never forgotten.

## Acknowledgements

Header "Acknowledgements"
Perl 5.20.0 represents approximately 12 months of development since Perl 5.18.0
and contains approximately 470,000 lines of changes across 2,900 files from 124
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 280,000 lines of changes to 1,800 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers. The following people are known to have contributed the
improvements that became Perl 5.20.0:

Aaron Crane, Abhijit Menon-Sen, Abigail, Abir Viqar, Alan Haggai Alavi, Alan
Hourihane, Alexander Voronov, Alexandr Ciornii, Andy Dougherty, Anno Siegel,
Aristotle Pagaltzis, Arthur Axel 'fREW' Schmidt, Brad Gilbert, Brendan Byrd,
Brian Childs, Brian Fraser, Brian Gottreu, Chris 'BinGOs' Williams, Christian
Millour, Colin Kuskie, Craig A. Berry, Dabrien 'Dabe' Murphy, Dagfinn Ilmari
Mannsa\*oker, Daniel Dragan, Darin McBride, David Golden, David Leadbeater, David
Mitchell, David Nicol, David Steinbrunner, Dennis Kaarsemaker, Dominic
Hargreaves, Ed Avis, Eric Brine, Evan Zacks, Father Chrysostomos, Florian
Ragwitz, Franc\*,ois Perrad, Gavin Shelley, Gideon Israel Dsouza, Gisle Aas,
Graham Knop, H.Merijn Brand, Hauke D, Heiko Eissfeldt, Hiroo Hayashi, Hojung
Youn, James E Keenan, Jarkko Hietaniemi, Jerry D. Hedden, Jess Robinson, Jesse
Luehrs, Johan Vromans, John Gardiner Myers, John Goodyear, John P. Linderman,
John Peacock, kafka, Kang-min Liu, Karen Etheridge, Karl Williamson, Keedi Kim,
Kent Fredric, kevin dawson, Kevin Falcone, Kevin Ryde, Leon Timmermans, Lukas
Mai, Marc Simpson, Marcel Gru\*:nauer, Marco Peereboom, Marcus Holland-Moritz,
Mark Jason Dominus, Martin McGrath, Matthew Horsfall, Max Maischein, Mike
Doherty, Moritz Lenz, Nathan Glenn, Nathan Trapuzzano, Neil Bowers, Neil
Williams, Nicholas Clark, Niels Thykier, Niko Tyni, Olivier Mengue\*', Owain G.
Ainsworth, Paul Green, Paul Johnson, Peter John Acklam, Peter Martini, Peter
Rabbitson, Petr Pi\*'saX, Philip Boulain, Philip Guenther, Piotr Roszatycki,
Rafael Garcia-Suarez, Reini Urban, Reuben Thomas, Ricardo Signes, Ruslan
Zakirov, Sergey Alekseev, Shirakata Kentaro, Shlomi Fish, Slaven Rezic,
Smylers, Steffen Mu\*:ller, Steve Hay, Sullivan Beck, Thomas Sibley, Tobias
Leich, Toby Inkster, Tokuhiro Matsuno, Tom Christiansen, Tom Hukins, Tony Cook,
Victor Efimov, Viktor Turskyi, Vladimir Timofeev, \s-1YAMASHINA\s0 Hio, Yves Orton,
Zefram, Zsba\*'n Ambrus, \*(Aevar Arnfjo\*:r\*(d- Bjarmason.

The list above is almost certainly incomplete as it is automatically generated
from version control history. In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core. We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the perl bug database at
http://rt.perl.org/perlbug/ .  There may also be information at
http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send it
to perl5-security-report@perl.org.  This points to a closed subscription
unarchived mailing list, which includes all the core committers, who will be
able to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported.  Please only use this address for
security issues in the Perl core, not for modules independently distributed on
\s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
