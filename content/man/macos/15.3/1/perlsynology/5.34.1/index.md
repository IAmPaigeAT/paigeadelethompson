+++
manpage_name = "perlsynology"
manpage_format = "troff"
author = "None Specified"
manpage_section = "1"
description = "Synology manufactures a vast number of Network Attached Storage (s-1NASs0) devices that are very popular in large organisations as well as small businesses and homes. The s-1NASs0 systems are equipped with Synology Disk Storage Manager (s-1DSMs0),..."
date = "2022-02-19"
operating_system = "macos"
operating_system_version = "15.3"
detected_package_version = "5.34.1"
title = "perlsynology(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLSYNOLOGY 1"
PERLSYNOLOGY 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlsynology - Perl 5 on Synology DSM systems

## DESCRIPTION

Header "DESCRIPTION"
Synology manufactures a vast number of Network Attached Storage (\s-1NAS\s0)
devices that are very popular in large organisations as well as small
businesses and homes.

The \s-1NAS\s0 systems are equipped with Synology Disk Storage Manager (\s-1DSM\s0),
which is a trimmed-down Linux system enhanced with several tools for
managing the \s-1NAS.\s0 There are several flavours of hardware: Marvell
Armada (ARMv5tel, ARMv7l), Intel Atom (i686, x86_64), Freescale QorIQ
(\s-1PPC\s0), and more. For a full list see the
Synology \s-1FAQ\s0 <https://forum.synology.com/wiki/index.php/What_kind_of_CPU_does_my_NAS_have>.

Since it is based on Linux, the \s-1NAS\s0 can run many popular Linux
software packages, including Perl. In fact, Synology provides a
ready-to-install package for Perl, depending on the version of \s-1DSM\s0
the installed perl ranges from 5.8.6 on \s-1DSM-4.3\s0 to 5.24.0 on \s-1DSM-6.1.\s0

There is an active user community that provides many software packages
for the Synology \s-1DSM\s0 systems; at the time of writing this document
they provide Perl version 5.24.1.

This document describes various features of Synology \s-1DSM\s0 operating
system that will affect how Perl 5 (hereafter just Perl) is
configured, compiled and/or runs. It has been compiled and verified by
Johan Vromans for the Synology \s-1DS413\s0 (QorIQ), with feedback from
H.Merijn Brand (\s-1DS213,\s0 ARMv5tel and \s-1RS815,\s0 Intel Atom x64).

### Setting up the build environment

Subsection "Setting up the build environment"
*\s-1DSM 5\s0*
Subsection "DSM 5"

As \s-1DSM\s0 is a trimmed-down Linux system, it lacks many of the tools and
libraries commonly found on Linux. The basic tools like sh, cp, rm,
etc. are implemented using
BusyBox <https://en.wikipedia.org/wiki/BusyBox>.

- \(bu
Using your favourite browser open the \s-1DSM\s0 management page and start
the Package Center.

- \(bu
If you want to smoke test Perl, install \f(CW\*(C`Perl\*(C'.

- \(bu
In Settings, add the following Package Sources:
.Sp
.Vb 2
  https://www.cphub.net
  http://packages.quadrat4.de
.Ve

- \(bu
Still in Settings, in Channel Update, select Beta Channel.

- \(bu
Press Refresh. In the left panel the item \*(L"Community\*(R" will appear.
Click it. Select \*(L"Bootstrap Installer Beta\*(R" and install it.

- \(bu
Likewise, install \*(L"iPKGui Beta\*(R".
.Sp
The application window should now show an icon for iPKGui.

- \(bu
Start iPKGui. Install the packages \f(CW\*(C`make\*(C', \f(CW\*(C`gcc\*(C' and \f(CW\*(C`coreutils\*(C'.
.Sp
If you want to smoke test Perl, install \f(CW\*(C`patch\*(C'.

The next step is to add some symlinks to system libraries. For
example, the development software expect a library \f(CW\*(C`libm.so\*(C' that
normally is a symlink to \f(CW\*(C`libm.so.6\*(C'. Synology only provides the
latter and not the symlink.

Here the actual architecture of the Synology system matters. You have
to find out where the gcc libraries have been installed. Look in /opt
for a directory similar to arm-none-linux-gnueab or
powerpc-linux-gnuspe. In the instructions below I'll use
powerpc-linux-gnuspe as an example.

- \(bu
On the \s-1DSM\s0 management page start the Control Panel.

- \(bu
Click Terminal, and enable \s-1SSH\s0 service.

- \(bu
Close Terminal and the Control Panel.

- \(bu
Open a shell on the Synology using ssh and become root.

- \(bu
Execute the following commands:
.Sp
.Vb 7
  cd /lib
  ln -s libm.so.6 libm.so
  ln -s libcrypt.so.1 libcrypt.so
  ln -s libdl.so.2 libdl.so
  cd /opt/powerpc-linux-gnuspe/lib  (or
                                    /opt/arm-none-linux-gnueabi/lib)
  ln -s /lib/libdl.so.2 libdl.so
.Ve

**\s-1WARNING:\s0** When you perform a system software upgrade, these links
will disappear and need to be re-established.

*\s-1DSM 6\s0*
Subsection "DSM 6"

Using iPkg has been deprecated on \s-1DSM 6,\s0 but an alternative is available
for \s-1DSM 6:\s0 entware/opkg. For instructions on how to use that, please read
Install Entware-ng on Synology \s-1NAS\s0 <https://github.com/Entware-ng/Entware-ng/wiki/Install-on-Synology-NAS>

That sadly does not (yet) work on QorIQ. At the moment of writing, the
supported architectures are armv5, armv7, mipsel, wl500g, x86_32, and x86_64.
Check here <https://pkg.entware.net/binaries/> for supported platforms.

Entware-ng comes with a precompiled 5.24.1 (June 2017) that allowes
building shared \s-1XS\s0 code. Note that this installation does **not** use
a site_perl folder. The available \f(CW\*(C`cpan\*(C' works. If all required
development packages are installed too, also for \s-1XS.\s0

### Compiling Perl 5

Subsection "Compiling Perl 5"
When the build environment has been set up, building and testing Perl
is straightforward. The only thing you need to do is download the
sources as usual, and add a file Policy.sh as follows:

.Vb 2
  # Administrivia.
  perladmin="your.email@goes.here"

  # Install Perl in a tree in /opt/perl instead of /opt/bin.
  prefix=/opt/perl

  # Select the compiler. Note that there is no \*(Aqcc\*(Aq alias or link.
  cc=gcc

  # Build flags.
  ccflags="-DDEBUGGING"

  # Library and include paths.
  libpth="/lib"
  locincpth="/opt/include"
  loclibpth="/lib"
.Ve

You may want to create the destination directory and give it the right
permissions before installing, thus eliminating the need to build Perl
as a super user.

In the directory where you unpacked the sources, issue the familiar
commands:

.Vb 4
  ./Configure -des
  make
  make test
  make install
.Ve

### Known problems

Subsection "Known problems"
*Configure*
Subsection "Configure"

No known problems yet

*Build*
Subsection "Build"
.ie n .IP "Error message ""No error definitions found""." 4
.el .IP "Error message ``No error definitions found''." 4
Item "Error message No error definitions found."
This error is generated when it is not possible to find the local
definitions for error codes, due to the uncommon structure of the
Synology file system.
.Sp
This error was fixed in the Perl development git for version 5.19,
commit 7a8f1212e5482613c8a5b0402528e3105b26ff24.

*Failing tests*
Subsection "Failing tests"

- \fIext/DynaLoader/t/DynaLoader.t
Item "ext/DynaLoader/t/DynaLoader.t"
One subtest fails due to the uncommon structure of the Synology file
system. The file */lib/glibc.so* is missing.
.Sp
**\s-1WARNING:\s0** Do not symlink */lib/glibc.so.6* to */lib/glibc.so* or
some system components will start to fail.

### Smoke testing Perl 5

Subsection "Smoke testing Perl 5"
If building completes successfully, you can set up smoke testing as
described in the Test::Smoke documentation.

For smoke testing you need a running Perl. You can either install the
Synology supplied package for Perl 5.8.6, or build and install your
own, much more recent version.

Note that I could not run successful smokes when initiated by the
Synology Task Scheduler. I resorted to initiating the smokes via a
cron job run on another system, using ssh:

.Vb 1
  ssh nas1 wrk/Test-Smoke/smoke/smokecurrent.sh
.Ve

*Local patches*
Subsection "Local patches"

When local patches are applied with smoke testing, the test driver
will automatically request regeneration of certain tables after the
patches are applied. The Synology supplied Perl 5.8.6 (at least on the
\s-1DS413\s0) **is \s-1NOT\s0 capable** of generating these tables. It will generate
opcodes with bogus values, causing the build to fail.

You can prevent regeneration by adding the setting

.Vb 1
  \*(Aqflags\*(Aq => 0,
.Ve

to the smoke config, or by adding another patch that inserts

.Vb 1
  exit 0 if $] == 5.008006;
.Ve

in the beginning of the \f(CW\*(C`regen.pl\*(C' program.

### Adding libraries

Subsection "Adding libraries"
The above procedure describes a basic environment and hence results in
a basic Perl. If you want to add additional libraries to Perl, you may
need some extra settings.

For example, the basic Perl does not have any of the \s-1DB\s0 libraries (db,
dbm, ndbm, gdsm). You can add these using iPKGui, however, you need to
set environment variable \s-1LD_LIBRARY_PATH\s0 to the appropriate value:

.Vb 2
  LD_LIBRARY_PATH=/lib:/opt/lib
  export LD_LIBRARY_PATH
.Ve

This setting needs to be in effect while Perl is built, but also when
the programs are run.

## REVISION

Header "REVISION"
June 2017, for Synology \s-1DSM 5.1.5022\s0 and \s-1DSM 6.1-15101-4.\s0

## AUTHOR

Header "AUTHOR"
Johan Vromans <jvromans@squirrel.nl>
H. Merijn Brand <h.m.brand@xs4all.nl>
