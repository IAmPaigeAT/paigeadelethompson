+++
author = "None Specified"
manpage_section = "1"
date = "2024-12-14"
operating_system = "macos"
manpage_name = "dbiprof"
description = "This tool is a command-line client for the DBI::ProfileData.  It allows you to analyze the profile data file produced by DBI::ProfileDumper and produce various useful reports. This program accepts the following options: Produce this many items in ..."
keywords = ["header", "see", "also", "dbi", "profiledumper", "profile", "s-1dbi", "s0"]
manpage_format = "troff"
detected_package_version = "5.34.0"
title = "dbiprof(1)"
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "DBIPROF 1"
DBIPROF 1 "2024-12-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

dbiprof - command-line client for DBI::ProfileData

## SYNOPSIS

Header "SYNOPSIS"
See a report of the ten queries with the longest total runtime in the
profile dump file *prof1.out*:

.Vb 1
 dbiprof prof1.out
.Ve

See the top 10 most frequently run queries in the profile file
*dbi.prof* (the default):

.Vb 1
  dbiprof --sort count
.Ve

See the same report with 15 entries:

.Vb 1
  dbiprof --sort count --number 15
.Ve

## DESCRIPTION

Header "DESCRIPTION"
This tool is a command-line client for the DBI::ProfileData.  It
allows you to analyze the profile data file produced by
DBI::ProfileDumper and produce various useful reports.

## OPTIONS

Header "OPTIONS"
This program accepts the following options:

- --number N
Item "--number N"
Produce this many items in the report.  Defaults to 10.  If set to
\*(L"all\*(R" then all results are shown.

- --sort field
Item "--sort field"
Sort results by the given field. Sorting by multiple fields isn't currently
supported (patches welcome).  The available sort fields are:

> 
- total
Item "total"
Sorts by total time run time across all runs.  This is the default
sort.

- longest
Item "longest"
Sorts by the longest single run.

- count
Item "count"
Sorts by total number of runs.

- first
Item "first"
Sorts by the time taken in the first run.

- shortest
Item "shortest"
Sorts by the shortest single run.

- key1
Item "key1"
Sorts by the value of the first element in the Path, which should be numeric.
You can also sort by \f(CW\*(C`key2\*(C' and \f(CW\*(C`key3\*(C'.



> 


- --reverse
Item "--reverse"
Reverses the selected sort.  For example, to see a report of the
shortest overall time:
.Sp
.Vb 1
  dbiprof --sort total --reverse
.Ve

- --match keyN=value
Item "--match keyN=value"
Consider only items where the specified key matches the given value.
Keys are numbered from 1.  For example, let's say you used a
DBI::Profile Path of:
.Sp
.Vb 1
  [ DBIprofile_Statement, DBIprofile_Methodname ]
.Ve
.Sp
And called dbiprof as in:
.Sp
.Vb 1
  dbiprof --match key2=execute
.Ve
.Sp
Your report would only show execute queries, leaving out prepares,
fetches, etc.
.Sp
If the value given starts and ends with slashes (\f(CW\*(C`/\*(C') then it will be
treated as a regular expression.  For example, to only include \s-1SELECT\s0
queries where key1 is the statement:
.Sp
.Vb 1
  dbiprof --match key1=/^SELECT/
.Ve
.Sp
By default the match expression is matched case-insensitively, but
this can be changed with the --case-sensitive option.

- --exclude keyN=value
Item "--exclude keyN=value"
Remove items for where the specified key matches the given value.  For
example, to exclude all prepare entries where key2 is the method name:
.Sp
.Vb 1
  dbiprof --exclude key2=prepare
.Ve
.Sp
Like \f(CW\*(C`--match\*(C', If the value given starts and ends with slashes
(\f(CW\*(C`/\*(C') then it will be treated as a regular expression.  For example,
to exclude \s-1UPDATE\s0 queries where key1 is the statement:
.Sp
.Vb 1
  dbiprof --match key1=/^UPDATE/
.Ve
.Sp
By default the exclude expression is matched case-insensitively, but
this can be changed with the --case-sensitive option.

- --case-sensitive
Item "--case-sensitive"
Using this option causes --match and --exclude to work
case-sensitively.  Defaults to off.

- --delete
Item "--delete"
Sets the \f(CW\*(C`DeleteFiles\*(C' option to DBI::ProfileData which causes the
files to be deleted after reading. See DBI::ProfileData for more details.

- --dumpnodes
Item "--dumpnodes"
Print the list of nodes in the form of a perl data structure.
Use the \f(CW\*(C`-sort\*(C' option if you want the list sorted.

- --version
Item "--version"
Print the dbiprof version number and exit.

## AUTHOR

Header "AUTHOR"
Sam Tregar <sam@tregar.com>

## COPYRIGHT AND LICENSE

Header "COPYRIGHT AND LICENSE"
Copyright (C) 2002 Sam Tregar

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl 5 itself.

## SEE ALSO

Header "SEE ALSO"
DBI::ProfileDumper,
DBI::Profile, \s-1DBI\s0.
