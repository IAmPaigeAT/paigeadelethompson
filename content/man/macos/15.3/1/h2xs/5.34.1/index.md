+++
operating_system = "macos"
detected_package_version = "5.34.1"
operating_system_version = "15.3"
author = "None Specified"
title = "h2xs(1)"
manpage_section = "1"
description = "h2xs builds a Perl extension from C header files.  The extension will include functions which can be used to retrieve the value of any #define statement which was in the C header files. The module_name will be used for the name of the extension.  I..."
manpage_name = "h2xs"
manpage_format = "troff"
keywords = ["header", "see", "also", "perl", "perlxstut", "extutils", "makemaker", "and", "autoloader", "diagnostics", "the", "usual", "warnings", "if", "it", "cannot", "read", "or", "write", "files", "involved", "limitations", "of", "fb-x", "x", "fih2xs", "would", "not", "distinguish", "whether", "an", "argument", "to", "a", "c", "function", "which", "is", "form", "say", "f", "cw", "int", "input", "output", "parameter", "in", "particular", "declarations", "vb", "3", "foo", "n", "ve", "should", "be", "better", "rewritten", "as", "additionally", "has", "no", "facilities", "intuit", "that", "4", "addr", "l", "char", "takes", "pair", "address", "length", "data", "at", "this", "so", "rewrite", "11", "sv", "preinit", "strlen", "len", "s", "code", "svpv", "retval", "alternately", "5", "static", "my_foo", "return", "module", "package", "prefix", "my_", "perlxs", "for", "additional", "details"]
date = "2024-12-14"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "H2XS 1"
H2XS 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

h2xs - convert .h C header files to Perl extensions

## SYNOPSIS

Header "SYNOPSIS"
**h2xs** [**\s-1OPTIONS\s0** ...] [headerfile ... [extra_libraries]]

**h2xs** **-h**|**-?**|**--help**

## DESCRIPTION

Header "DESCRIPTION"
*h2xs* builds a Perl extension from C header files.  The extension
will include functions which can be used to retrieve the value of any
#define statement which was in the C header files.

The *module_name* will be used for the name of the extension.  If
module_name is not supplied then the name of the first header file
will be used, with the first character capitalized.

If the extension might need extra libraries, they should be included
here.  The extension Makefile.PL will take care of checking whether
the libraries actually exist and how they should be loaded.  The extra
libraries should be specified in the form -lm -lposix, etc, just as on
the cc command line.  By default, the Makefile.PL will search through
the library path determined by Configure.  That path can be augmented
by including arguments of the form **-L/another/library/path** in the
extra-libraries argument.

In spite of its name, *h2xs* may also be used to create a skeleton pure
Perl module. See the **-X** option.

## OPTIONS

Header "OPTIONS"

- \fB-A, \fB--omit-autoload
Item "-A, --omit-autoload"
Omit all autoload facilities.  This is the same as **-c** but also
removes the \f(CW\*(C`use\ AutoLoader\*(C' statement from the .pm file.

- \fB-B, \fB--beta-version
Item "-B, --beta-version"
Use an alpha/beta style version number.  Causes version number to
be \*(L"0.00_01\*(R" unless **-v** is specified.

- \fB-C, \fB--omit-changes
Item "-C, --omit-changes"
Omits creation of the *Changes* file, and adds a \s-1HISTORY\s0 section to
the \s-1POD\s0 template.

- \fB-F, \fB--cpp-flags=\fIaddflags
Item "-F, --cpp-flags=addflags"
Additional flags to specify to C preprocessor when scanning header for
function declarations.  Writes these options in the generated *Makefile.PL*
too.

- \fB-M, \fB--func-mask=\fIregular expression
Item "-M, --func-mask=regular expression"
selects functions/macros to process.

- \fB-O, \fB--overwrite-ok
Item "-O, --overwrite-ok"
Allows a pre-existing extension directory to be overwritten.

- \fB-P, \fB--omit-pod
Item "-P, --omit-pod"
Omit the autogenerated stub \s-1POD\s0 section.

- \fB-X, \fB--omit-XS
Item "-X, --omit-XS"
Omit the \s-1XS\s0 portion. Used to generate a skeleton pure Perl module.
\f(CW\*(C`-c\*(C' and \f(CW\*(C`-f\*(C' are implicitly enabled.

- \fB-a, \fB--gen-accessors
Item "-a, --gen-accessors"
Generate an accessor method for each element of structs and unions. The
generated methods are named after the element name; will return the current
value of the element if called without additional arguments; and will set
the element to the supplied value (and return the new value) if called with
an additional argument. Embedded structures and unions are returned as a
pointer rather than the complete structure, to facilitate chained calls.
.Sp
These methods all apply to the Ptr type for the structure; additionally
two methods are constructed for the structure type itself, \f(CW\*(C`_to_ptr\*(C'
which returns a Ptr type pointing to the same structure, and a \f(CW\*(C`new\*(C'
method to construct and return a new structure, initialised to zeroes.

- \fB-b, \fB--compat-version=\fIversion
Item "-b, --compat-version=version"
Generates a .pm file which is backwards compatible with the specified
perl version.
.Sp
For versions < 5.6.0, the changes are.
    - no use of 'our' (uses 'use vars' instead)
    - no 'use warnings'
.Sp
Specifying a compatibility version higher than the version of perl you
are using to run h2xs will have no effect.  If unspecified h2xs will default
to compatibility with the version of perl you are using to run h2xs.

- \fB-c, \fB--omit-constant
Item "-c, --omit-constant"
Omit \f(CW\*(C`constant()\*(C' from the .xs file and corresponding specialised
\f(CW\*(C`AUTOLOAD\*(C' from the .pm file.

- \fB-d, \fB--debugging
Item "-d, --debugging"
Turn on debugging messages.

- \fB-e, \fB--omit-enums=[\fIregular expression]
Item "-e, --omit-enums=[regular expression]"
If *regular expression* is not given, skip all constants that are defined in
a C enumeration. Otherwise skip only those constants that are defined in an
enum whose name matches *regular expression*.
.Sp
Since *regular expression* is optional, make sure that this switch is followed
by at least one other switch if you omit *regular expression* and have some
pending arguments such as header-file names. This is ok:
.Sp
.Vb 1
    h2xs -e -n Module::Foo foo.h
.Ve
.Sp
This is not ok:
.Sp
.Vb 1
    h2xs -n Module::Foo -e foo.h
.Ve
.Sp
In the latter, foo.h is taken as *regular expression*.

- \fB-f, \fB--force
Item "-f, --force"
Allows an extension to be created for a header even if that header is
not found in standard include directories.

- \fB-g, \fB--global
Item "-g, --global"
Include code for safely storing static data in the .xs file.
Extensions that do no make use of static data can ignore this option.

- \fB-h, \fB-?, \fB--help
Item "-h, -?, --help"
Print the usage, help and version for this h2xs and exit.

- \fB-k, \fB--omit-const-func
Item "-k, --omit-const-func"
For function arguments declared as \f(CW\*(C`const\*(C', omit the const attribute in the
generated \s-1XS\s0 code.

- \fB-m, \fB--gen-tied-var
Item "-m, --gen-tied-var"
**Experimental**: for each variable declared in the header file(s), declare
a perl variable of the same name magically tied to the C variable.

- \fB-n, \fB--name=\fImodule_name
Item "-n, --name=module_name"
Specifies a name to be used for the extension, e.g., -n\ \s-1RPC::DCE\s0

- \fB-o, \fB--opaque-re=\fIregular expression
Item "-o, --opaque-re=regular expression"
Use \*(L"opaque\*(R" data type for the C types matched by the regular
expression, even if these types are \f(CW\*(C`typedef\*(C'-equivalent to types
from typemaps.  Should not be used without **-x**.
.Sp
This may be useful since, say, types which are \f(CW\*(C`typedef\*(C'-equivalent
to integers may represent OS-related handles, and one may want to work
with these handles in OO-way, as in \f(CW\*(C`$handle->do_something()\*(C'.
Use \f(CW\*(C`-o .\*(C' if you want to handle all the \f(CW\*(C`typedef\*(C'ed types as opaque
types.
.Sp
The type-to-match is whitewashed (except for commas, which have no
whitespace before them, and multiple \f(CW\*(C`*\*(C' which have no whitespace
between them).

- \fB-p, \fB--remove-prefix=\fIprefix
Item "-p, --remove-prefix=prefix"
Specify a prefix which should be removed from the Perl function names,
e.g., -p\ sec_rgy_ This sets up the \s-1XS\s0 **\s-1PREFIX\s0** keyword and removes
the prefix from functions that are autoloaded via the \f(CW\*(C`constant()\*(C'
mechanism.

- \fB-s, \fB--const-subs=\fIsub1,sub2
Item "-s, --const-subs=sub1,sub2"
Create a perl subroutine for the specified macros rather than autoload
with the **constant()** subroutine.  These macros are assumed to have a
return type of **char ***, e.g.,
-s\ sec_rgy_wildcard_name,sec_rgy_wildcard_sid.

- \fB-t, \fB--default-type=\fItype
Item "-t, --default-type=type"
Specify the internal type that the **constant()** mechanism uses for macros.
The default is \s-1IV\s0 (signed integer).  Currently all macros found during the
header scanning process will be assumed to have this type.  Future versions
of \f(CW\*(C`h2xs\*(C' may gain the ability to make educated guesses.

- \fB--use-new-tests
Item "--use-new-tests"
When **--compat-version** (**-b**) is present the generated tests will use
\f(CW\*(C`Test::More\*(C' rather than \f(CW\*(C`Test\*(C' which is the default for versions before
5.6.2.  \f(CW\*(C`Test::More\*(C' will be added to \s-1PREREQ_PM\s0 in the generated
\f(CW\*(C`Makefile.PL\*(C'.

- \fB--use-old-tests
Item "--use-old-tests"
Will force the generation of test code that uses the older \f(CW\*(C`Test\*(C' module.

- \fB--skip-exporter
Item "--skip-exporter"
Do not use \f(CW\*(C`Exporter\*(C' and/or export any symbol.

- \fB--skip-ppport
Item "--skip-ppport"
Do not use \f(CW\*(C`Devel::PPPort\*(C': no portability to older version.

- \fB--skip-autoloader
Item "--skip-autoloader"
Do not use the module \f(CW\*(C`AutoLoader\*(C'; but keep the **constant()** function
and \f(CW\*(C`sub AUTOLOAD\*(C' for constants.

- \fB--skip-strict
Item "--skip-strict"
Do not use the pragma \f(CW\*(C`strict\*(C'.

- \fB--skip-warnings
Item "--skip-warnings"
Do not use the pragma \f(CW\*(C`warnings\*(C'.

- \fB-v, \fB--version=\fIversion
Item "-v, --version=version"
Specify a version number for this extension.  This version number is added
to the templates.  The default is 0.01, or 0.00_01 if \f(CW\*(C`-B\*(C' is specified.
The version specified should be numeric.

- \fB-x, \fB--autogen-xsubs
Item "-x, --autogen-xsubs"
Automatically generate XSUBs basing on function declarations in the
header file.  The package \f(CW\*(C`C::Scan\*(C' should be installed. If this
option is specified, the name of the header file may look like
\f(CW\*(C`NAME1,NAME2\*(C'. In this case \s-1NAME1\s0 is used instead of the specified
string, but XSUBs are emitted only for the declarations included from
file \s-1NAME2.\s0
.Sp
Note that some types of arguments/return-values for functions may
result in XSUB-declarations/typemap-entries which need
hand-editing. Such may be objects which cannot be converted from/to a
pointer (like \f(CW\*(C`long long\*(C'), pointers to functions, or arrays.  See
also the section on "\s-1LIMITATIONS\s0 of **-x**".

## EXAMPLES

Header "EXAMPLES"
.Vb 2
    # Default behavior, extension is Rusers
    h2xs rpcsvc/rusers

    # Same, but extension is RUSERS
    h2xs -n RUSERS rpcsvc/rusers

    # Extension is rpcsvc::rusers. Still finds <rpcsvc/rusers.h>
    h2xs rpcsvc::rusers

    # Extension is ONC::RPC.  Still finds <rpcsvc/rusers.h>
    h2xs -n ONC::RPC rpcsvc/rusers

    # Without constant() or AUTOLOAD
    h2xs -c rpcsvc/rusers

    # Creates templates for an extension named RPC
    h2xs -cfn RPC

    # Extension is ONC::RPC.
    h2xs -cfn ONC::RPC

    # Extension is a pure Perl module with no XS code.
    h2xs -X My::Module

    # Extension is Lib::Foo which works at least with Perl5.005_03.
    # Constants are created for all #defines and enums h2xs can find
    # in foo.h.
    h2xs -b 5.5.3 -n Lib::Foo foo.h

    # Extension is Lib::Foo which works at least with Perl5.005_03.
    # Constants are created for all #defines but only for enums
    # whose names do not start with \*(Aqbar_\*(Aq.
    h2xs -b 5.5.3 -e \*(Aq^bar_\*(Aq -n Lib::Foo foo.h

    # Makefile.PL will look for library -lrpc in
    # additional directory /opt/net/lib
    h2xs rpcsvc/rusers -L/opt/net/lib -lrpc

    # Extension is DCE::rgynbase
    # prefix "sec_rgy_" is dropped from perl function names
    h2xs -n DCE::rgynbase -p sec_rgy_ dce/rgynbase

    # Extension is DCE::rgynbase
    # prefix "sec_rgy_" is dropped from perl function names
    # subroutines are created for sec_rgy_wildcard_name and
    # sec_rgy_wildcard_sid
    h2xs -n DCE::rgynbase -p sec_rgy_ \\
    -s sec_rgy_wildcard_name,sec_rgy_wildcard_sid dce/rgynbase

    # Make XS without defines in perl.h, but with function declarations
    # visible from perl.h. Name of the extension is perl1.
    # When scanning perl.h, define -DEXT=extern -DdEXT= -DINIT(x)=
    # Extra backslashes below because the string is passed to shell.
    # Note that a directory with perl header files would
    #  be added automatically to include path.
    h2xs -xAn perl1 -F "-DEXT=extern -DdEXT= -DINIT\\(x\\)=" perl.h

    # Same with function declaration in proto.h as visible from perl.h.
    h2xs -xAn perl2 perl.h,proto.h

    # Same but select only functions which match /^av_/
    h2xs -M \*(Aq^av_\*(Aq -xAn perl2 perl.h,proto.h

    # Same but treat SV* etc as "opaque" types
    h2xs -o \*(Aq^[S]V \\*$\*(Aq -M \*(Aq^av_\*(Aq -xAn perl2 perl.h,proto.h
.Ve

### Extension based on \fI.h and \fI.c files

Subsection "Extension based on .h and .c files"
Suppose that you have some C files implementing some functionality,
and the corresponding header files.  How to create an extension which
makes this functionality accessible in Perl?  The example below
assumes that the header files are *interface_simple.h* and
*interface_hairy.h*, and you want the perl module be named as
\f(CW\*(C`Ext::Ension\*(C'.  If you need some preprocessor directives and/or
linking with external libraries, see the flags \f(CW\*(C`-F\*(C', \f(CW\*(C`-L\*(C' and \f(CW\*(C`-l\*(C'
in \*(L"\s-1OPTIONS\*(R"\s0.

- Find the directory name
Item "Find the directory name"
Start with a dummy run of h2xs:
.Sp
.Vb 1
  h2xs -Afn Ext::Ension
.Ve
.Sp
The only purpose of this step is to create the needed directories, and
let you know the names of these directories.  From the output you can
see that the directory for the extension is *Ext/Ension*.

- Copy C files
Item "Copy C files"
Copy your header files and C files to this directory *Ext/Ension*.

- Create the extension
Item "Create the extension"
Run h2xs, overwriting older autogenerated files:
.Sp
.Vb 1
  h2xs -Oxan Ext::Ension interface_simple.h interface_hairy.h
.Ve
.Sp
h2xs looks for header files *after* changing to the extension
directory, so it will find your header files \s-1OK.\s0

- Archive and test
Item "Archive and test"
As usual, run
.Sp
.Vb 5
  cd Ext/Ension
  perl Makefile.PL
  make dist
  make
  make test
.Ve

- Hints
Item "Hints"
It is important to do \f(CW\*(C`make dist\*(C' as early as possible.  This way you
can easily **merge**\|(1) your changes to autogenerated files if you decide
to edit your \f(CW\*(C`.h\*(C' files and rerun h2xs.
.Sp
Do not forget to edit the documentation in the generated *.pm* file.
.Sp
Consider the autogenerated files as skeletons only, you may invent
better interfaces than what h2xs could guess.
.Sp
Consider this section as a guideline only, some other options of h2xs
may better suit your needs.

## ENVIRONMENT

Header "ENVIRONMENT"
No environment variables are used.

## AUTHOR

Header "AUTHOR"
Larry Wall and others

## SEE ALSO

Header "SEE ALSO"
perl, perlxstut, ExtUtils::MakeMaker, and AutoLoader.

## DIAGNOSTICS

Header "DIAGNOSTICS"
The usual warnings if it cannot read or write the files involved.

## LIMITATIONS of \fB-x

Header "LIMITATIONS of -x"
*h2xs* would not distinguish whether an argument to a C function
which is of the form, say, \f(CW\*(C`int *\*(C', is an input, output, or
input/output parameter.  In particular, argument declarations of the
form

.Vb 3
    int
    foo(n)
        int *n
.Ve

should be better rewritten as

.Vb 3
    int
    foo(n)
        int &n
.Ve

if \f(CW\*(C`n\*(C' is an input parameter.

Additionally, *h2xs* has no facilities to intuit that a function

.Vb 4
   int
   foo(addr,l)
        char *addr
        int   l
.Ve

takes a pair of address and length of data at this address, so it is better
to rewrite this function as

.Vb 11
    int
    foo(sv)
            SV *addr
        PREINIT:
            STRLEN len;
            char *s;
        CODE:
            s = SvPV(sv,len);
            RETVAL = foo(s, len);
        OUTPUT:
            RETVAL
.Ve

or alternately

.Vb 5
    static int
    my_foo(SV *sv)
    \{
        STRLEN len;
        char *s = SvPV(sv,len);

        return foo(s, len);
    \}

    MODULE = foo        PACKAGE = foo   PREFIX = my_

    int
    foo(sv)
        SV *sv
.Ve

See perlxs and perlxstut for additional details.
