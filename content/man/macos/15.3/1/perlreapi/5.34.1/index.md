+++
manpage_section = "1"
operating_system_version = "15.3"
manpage_format = "troff"
description = "As of Perl 5.9.5 there is a new interface for plugging and using regular expression engines other than the default one. Each engine is supposed to provide access to a constant structure of the following format:     typedef struct regexp_engine { ..."
detected_package_version = "5.34.1"
manpage_name = "perlreapi"
author = "None Specified"
operating_system = "macos"
title = "perlreapi(1)"
date = "2022-02-19"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLREAPI 1"
PERLREAPI 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlreapi - Perl regular expression plugin interface

## DESCRIPTION

Header "DESCRIPTION"
As of Perl 5.9.5 there is a new interface for plugging and using
regular expression engines other than the default one.

Each engine is supposed to provide access to a constant structure of the
following format:

.Vb 10
    typedef struct regexp_engine \{
        REGEXP* (*comp) (pTHX_
                         const SV * const pattern, const U32 flags);
        I32     (*exec) (pTHX_
                         REGEXP * const rx,
                         char* stringarg,
                         char* strend, char* strbeg,
                         SSize_t minend, SV* sv,
                         void* data, U32 flags);
        char*   (*intuit) (pTHX_
                           REGEXP * const rx, SV *sv,
                           const char * const strbeg,
                           char *strpos, char *strend, U32 flags,
                           struct re_scream_pos_data_s *data);
        SV*     (*checkstr) (pTHX_ REGEXP * const rx);
        void    (*free) (pTHX_ REGEXP * const rx);
        void    (*numbered_buff_FETCH) (pTHX_
                                        REGEXP * const rx,
                                        const I32 paren,
                                        SV * const sv);
        void    (*numbered_buff_STORE) (pTHX_
                                        REGEXP * const rx,
                                        const I32 paren,
                                        SV const * const value);
        I32     (*numbered_buff_LENGTH) (pTHX_
                                         REGEXP * const rx,
                                         const SV * const sv,
                                         const I32 paren);
        SV*     (*named_buff) (pTHX_
                               REGEXP * const rx,
                               SV * const key,
                               SV * const value,
                               U32 flags);
        SV*     (*named_buff_iter) (pTHX_
                                    REGEXP * const rx,
                                    const SV * const lastkey,
                                    const U32 flags);
        SV*     (*qr_package)(pTHX_ REGEXP * const rx);
    #ifdef USE_ITHREADS
        void*   (*dupe) (pTHX_ REGEXP * const rx, CLONE_PARAMS *param);
    #endif
        REGEXP* (*op_comp) (...);
.Ve

When a regexp is compiled, its \f(CW\*(C`engine\*(C' field is then set to point at
the appropriate structure, so that when it needs to be used Perl can find
the right routines to do so.

In order to install a new regexp handler, \f(CW$^H\{regcomp\} is set
to an integer which (when casted appropriately) resolves to one of these
structures.  When compiling, the \f(CW\*(C`comp\*(C' method is executed, and the
resulting \f(CW\*(C`regexp\*(C' structure's engine field is expected to point back at
the same structure.

The pTHX_ symbol in the definition is a macro used by Perl under threading
to provide an extra argument to the routine holding a pointer back to
the interpreter that is executing the regexp. So under threading all
routines get an extra argument.

## Callbacks

Header "Callbacks"

### comp

Subsection "comp"
.Vb 1
    REGEXP* comp(pTHX_ const SV * const pattern, const U32 flags);
.Ve

Compile the pattern stored in \f(CW\*(C`pattern\*(C' using the given \f(CW\*(C`flags\*(C' and
return a pointer to a prepared \f(CW\*(C`REGEXP\*(C' structure that can perform
the match.  See \*(L"The \s-1REGEXP\s0 structure\*(R" below for an explanation of
the individual fields in the \s-1REGEXP\s0 struct.

The \f(CW\*(C`pattern\*(C' parameter is the scalar that was used as the
pattern.  Previous versions of Perl would pass two \f(CW\*(C`char*\*(C' indicating
the start and end of the stringified pattern; the following snippet can
be used to get the old parameters:

.Vb 3
    STRLEN plen;
    char*  exp = SvPV(pattern, plen);
    char* xend = exp + plen;
.Ve

Since any scalar can be passed as a pattern, it's possible to implement
an engine that does something with an array (\f(CW\*(C`"ook" =~ [ qw/ eek
hlagh / ]\*(C') or with the non-stringified form of a compiled regular
expression (\f(CW\*(C`"ook" =~ qr/eek/\*(C').  Perl's own engine will always
stringify everything using the snippet above, but that doesn't mean
other engines have to.

The \f(CW\*(C`flags\*(C' parameter is a bitfield which indicates which of the
\f(CW\*(C`msixpn\*(C' flags the regex was compiled with.  It also contains
additional info, such as if \f(CW\*(C`use locale\*(C' is in effect.

The \f(CW\*(C`eogc\*(C' flags are stripped out before being passed to the comp
routine.  The regex engine does not need to know if any of these
are set, as those flags should only affect what Perl does with the
pattern and its match variables, not how it gets compiled and
executed.

By the time the comp callback is called, some of these flags have
already had effect (noted below where applicable).  However most of
their effect occurs after the comp callback has run, in routines that
read the \f(CW\*(C`rx->extflags\*(C' field which it populates.

In general the flags should be preserved in \f(CW\*(C`rx->extflags\*(C' after
compilation, although the regex engine might want to add or delete
some of them to invoke or disable some special behavior in Perl.  The
flags along with any special behavior they cause are documented below:

The pattern modifiers:
.ie n .IP """/m"" - RXf_PMf_MULTILINE" 4
.el .IP "\f(CW/m - RXf_PMf_MULTILINE" 4
Item "/m - RXf_PMf_MULTILINE"
If this is in \f(CW\*(C`rx->extflags\*(C' it will be passed to
\f(CW\*(C`Perl_fbm_instr\*(C' by \f(CW\*(C`pp_split\*(C' which will treat the subject string
as a multi-line string.
.ie n .IP """/s"" - RXf_PMf_SINGLELINE" 4
.el .IP "\f(CW/s - RXf_PMf_SINGLELINE" 4
Item "/s - RXf_PMf_SINGLELINE"
0
.ie n .IP """/i"" - RXf_PMf_FOLD" 4
.el .IP "\f(CW/i - RXf_PMf_FOLD" 4
Item "/i - RXf_PMf_FOLD"
.ie n .IP """/x"" - RXf_PMf_EXTENDED" 4
.el .IP "\f(CW/x - RXf_PMf_EXTENDED" 4
Item "/x - RXf_PMf_EXTENDED"
.PD
If present on a regex, \f(CW"#" comments will be handled differently by the
tokenizer in some cases.
.Sp
\s-1TODO:\s0 Document those cases.
.ie n .IP """/p"" - RXf_PMf_KEEPCOPY" 4
.el .IP "\f(CW/p - RXf_PMf_KEEPCOPY" 4
Item "/p - RXf_PMf_KEEPCOPY"
\s-1TODO:\s0 Document this

- Character set
Item "Character set"
The character set rules are determined by an enum that is contained
in this field.  This is still experimental and subject to change, but
the current interface returns the rules by use of the in-line function
\f(CW\*(C`get_regex_charset(const U32 flags)\*(C'.  The only currently documented
value returned from it is \s-1REGEX_LOCALE_CHARSET,\s0 which is set if
\f(CW\*(C`use locale\*(C' is in effect. If present in \f(CW\*(C`rx->extflags\*(C',
\f(CW\*(C`split\*(C' will use the locale dependent definition of whitespace
when RXf_SKIPWHITE or RXf_WHITE is in effect.  \s-1ASCII\s0 whitespace
is defined as per isSPACE, and by the internal
macros \f(CW\*(C`is_utf8_space\*(C' under \s-1UTF-8,\s0 and \f(CW\*(C`isSPACE_LC\*(C' under \f(CW\*(C`use
locale\*(C'.

Additional flags:

- RXf_SPLIT
Item "RXf_SPLIT"
This flag was removed in perl 5.18.0.  \f(CW\*(C`split \*(Aq \*(Aq\*(C' is now special-cased
solely in the parser.  RXf_SPLIT is still #defined, so you can test for it.
This is how it used to work:
.Sp
If \f(CW\*(C`split\*(C' is invoked as \f(CW\*(C`split \*(Aq \*(Aq\*(C' or with no arguments (which
really means \f(CW\*(C`split(\*(Aq \*(Aq, $_)\*(C', see split), Perl will
set this flag.  The regex engine can then check for it and set the
\s-1SKIPWHITE\s0 and \s-1WHITE\s0 extflags.  To do this, the Perl engine does:
.Sp
.Vb 2
    if (flags & RXf_SPLIT && r->prelen == 1 && r->precomp[0] == \*(Aq \*(Aq)
        r->extflags |= (RXf_SKIPWHITE|RXf_WHITE);
.Ve

These flags can be set during compilation to enable optimizations in
the \f(CW\*(C`split\*(C' operator.

- RXf_SKIPWHITE
Item "RXf_SKIPWHITE"
This flag was removed in perl 5.18.0.  It is still #defined, so you can
set it, but doing so will have no effect.  This is how it used to work:
.Sp
If the flag is present in \f(CW\*(C`rx->extflags\*(C' \f(CW\*(C`split\*(C' will delete
whitespace from the start of the subject string before it's operated
on.  What is considered whitespace depends on if the subject is a
\s-1UTF-8\s0 string and if the \f(CW\*(C`RXf_PMf_LOCALE\*(C' flag is set.
.Sp
If RXf_WHITE is set in addition to this flag, \f(CW\*(C`split\*(C' will behave like
\f(CW\*(C`split " "\*(C' under the Perl engine.

- RXf_START_ONLY
Item "RXf_START_ONLY"
Tells the split operator to split the target string on newlines
(\f(CW\*(C`\\n\*(C') without invoking the regex engine.
.Sp
Perl's engine sets this if the pattern is \f(CW\*(C`/^/\*(C' (\f(CW\*(C`plen == 1 && *exp
== \*(Aq^\*(Aq\*(C'), even under \f(CW\*(C`/^/s\*(C'; see split.  Of course a
different regex engine might want to use the same optimizations
with a different syntax.

- RXf_WHITE
Item "RXf_WHITE"
Tells the split operator to split the target string on whitespace
without invoking the regex engine.  The definition of whitespace varies
depending on if the target string is a \s-1UTF-8\s0 string and on
if RXf_PMf_LOCALE is set.
.Sp
Perl's engine sets this flag if the pattern is \f(CW\*(C`\\s+\*(C'.

- RXf_NULL
Item "RXf_NULL"
Tells the split operator to split the target string on
characters.  The definition of character varies depending on if
the target string is a \s-1UTF-8\s0 string.
.Sp
Perl's engine sets this flag on empty patterns, this optimization
makes \f(CW\*(C`split //\*(C' much faster than it would otherwise be.  It's even
faster than \f(CW\*(C`unpack\*(C'.

- RXf_NO_INPLACE_SUBST
Item "RXf_NO_INPLACE_SUBST"
Added in perl 5.18.0, this flag indicates that a regular expression might
perform an operation that would interfere with inplace substitution. For
instance it might contain lookbehind, or assign to non-magical variables
(such as \f(CW$REGMARK and \f(CW$REGERROR) during matching.  \f(CW\*(C`s///\*(C' will skip
certain optimisations when this is set.

### exec

Subsection "exec"
.Vb 4
    I32 exec(pTHX_ REGEXP * const rx,
             char *stringarg, char* strend, char* strbeg,
             SSize_t minend, SV* sv,
             void* data, U32 flags);
.Ve

Execute a regexp. The arguments are

- rx
Item "rx"
The regular expression to execute.

- sv
Item "sv"
This is the \s-1SV\s0 to be matched against.  Note that the
actual char array to be matched against is supplied by the arguments
described below; the \s-1SV\s0 is just used to determine UTF8ness, \f(CW\*(C`pos()\*(C' etc.

- strbeg
Item "strbeg"
Pointer to the physical start of the string.

- strend
Item "strend"
Pointer to the character following the physical end of the string (i.e.
the \f(CW\*(C`\\0\*(C', if any).

- stringarg
Item "stringarg"
Pointer to the position in the string where matching should start; it might
not be equal to \f(CW\*(C`strbeg\*(C' (for example in a later iteration of \f(CW\*(C`/.../g\*(C').

- minend
Item "minend"
Minimum length of string (measured in bytes from \f(CW\*(C`stringarg\*(C') that must
match; if the engine reaches the end of the match but hasn't reached this
position in the string, it should fail.

- data
Item "data"
Optimisation data; subject to change.

- flags
Item "flags"
Optimisation flags; subject to change.

### intuit

Subsection "intuit"
.Vb 8
    char* intuit(pTHX_
                REGEXP * const rx,
                SV *sv,
                const char * const strbeg,
                char *strpos,
                char *strend,
                const U32 flags,
                struct re_scream_pos_data_s *data);
.Ve

Find the start position where a regex match should be attempted,
or possibly if the regex engine should not be run because the
pattern can't match.  This is called, as appropriate, by the core,
depending on the values of the \f(CW\*(C`extflags\*(C' member of the \f(CW\*(C`regexp\*(C'
structure.

Arguments:

.Vb 11
    rx:     the regex to match against
    sv:     the SV being matched: only used for utf8 flag; the string
            itself is accessed via the pointers below. Note that on
            something like an overloaded SV, SvPOK(sv) may be false
            and the string pointers may point to something unrelated to
            the SV itself.
    strbeg: real beginning of string
    strpos: the point in the string at which to begin matching
    strend: pointer to the byte following the last char of the string
    flags   currently unused; set to 0
    data:   currently unused; set to NULL
.Ve

### checkstr

Subsection "checkstr"
.Vb 1
    SV* checkstr(pTHX_ REGEXP * const rx);
.Ve

Return a \s-1SV\s0 containing a string that must appear in the pattern. Used
by \f(CW\*(C`split\*(C' for optimising matches.

### free

Subsection "free"
.Vb 1
    void free(pTHX_ REGEXP * const rx);
.Ve

Called by Perl when it is freeing a regexp pattern so that the engine
can release any resources pointed to by the \f(CW\*(C`pprivate\*(C' member of the
\f(CW\*(C`regexp\*(C' structure.  This is only responsible for freeing private data;
Perl will handle releasing anything else contained in the \f(CW\*(C`regexp\*(C' structure.

### Numbered capture callbacks

Subsection "Numbered capture callbacks"
Called to get/set the value of \f(CW\*(C`$\`\*(C', \f(CW\*(C`$\*(Aq\*(C', \f(CW$& and their named
equivalents, $\{^PREMATCH\}, $\{^POSTMATCH\} and $\{^MATCH\}, as well as the
numbered capture groups (\f(CW$1, \f(CW$2, ...).

The \f(CW\*(C`paren\*(C' parameter will be \f(CW1 for \f(CW$1, \f(CW2 for \f(CW$2 and so
forth, and have these symbolic values for the special variables:

.Vb 6
    $\{^PREMATCH\}  RX_BUFF_IDX_CARET_PREMATCH
    $\{^POSTMATCH\} RX_BUFF_IDX_CARET_POSTMATCH
    $\{^MATCH\}     RX_BUFF_IDX_CARET_FULLMATCH
    $\`            RX_BUFF_IDX_PREMATCH
    $\*(Aq            RX_BUFF_IDX_POSTMATCH
    $&            RX_BUFF_IDX_FULLMATCH
.Ve

Note that in Perl 5.17.3 and earlier, the last three constants were also
used for the caret variants of the variables.

The names have been chosen by analogy with Tie::Scalar methods
names with an additional **\s-1LENGTH\s0** callback for efficiency.  However
named capture variables are currently not tied internally but
implemented via magic.

*numbered_buff_FETCH*
Subsection "numbered_buff_FETCH"

.Vb 2
    void numbered_buff_FETCH(pTHX_ REGEXP * const rx, const I32 paren,
                             SV * const sv);
.Ve

Fetch a specified numbered capture.  \f(CW\*(C`sv\*(C' should be set to the scalar
to return, the scalar is passed as an argument rather than being
returned from the function because when it's called Perl already has a
scalar to store the value, creating another one would be
redundant.  The scalar can be set with \f(CW\*(C`sv_setsv\*(C', \f(CW\*(C`sv_setpvn\*(C' and
friends, see perlapi.

This callback is where Perl untaints its own capture variables under
taint mode (see perlsec).  See the \f(CW\*(C`Perl_reg_numbered_buff_fetch\*(C'
function in *regcomp.c* for how to untaint capture variables if
that's something you'd like your engine to do as well.

*numbered_buff_STORE*
Subsection "numbered_buff_STORE"

.Vb 4
    void    (*numbered_buff_STORE) (pTHX_
                                    REGEXP * const rx,
                                    const I32 paren,
                                    SV const * const value);
.Ve

Set the value of a numbered capture variable.  \f(CW\*(C`value\*(C' is the scalar
that is to be used as the new value.  It's up to the engine to make
sure this is used as the new value (or reject it).

Example:

.Vb 4
    if ("ook" =~ /(o*)/) \{
        # \*(Aqparen\*(Aq will be \*(Aq1\*(Aq and \*(Aqvalue\*(Aq will be \*(Aqee\*(Aq
        $1 =~ tr/o/e/;
    \}
.Ve

Perl's own engine will croak on any attempt to modify the capture
variables, to do this in another engine use the following callback
(copied from \f(CW\*(C`Perl_reg_numbered_buff_store\*(C'):

.Vb 9
    void
    Example_reg_numbered_buff_store(pTHX_
                                    REGEXP * const rx,
                                    const I32 paren,
                                    SV const * const value)
    \{
        PERL_UNUSED_ARG(rx);
        PERL_UNUSED_ARG(paren);
        PERL_UNUSED_ARG(value);

        if (!PL_localizing)
            Perl_croak(aTHX_ PL_no_modify);
    \}
.Ve

Actually Perl will not *always* croak in a statement that looks
like it would modify a numbered capture variable.  This is because the
\s-1STORE\s0 callback will not be called if Perl can determine that it
doesn't have to modify the value.  This is exactly how tied variables
behave in the same situation:

.Vb 2
    package CaptureVar;
    use parent \*(AqTie::Scalar\*(Aq;

    sub TIESCALAR \{ bless [] \}
    sub FETCH \{ undef \}
    sub STORE \{ die "This doesn\*(Aqt get called" \}

    package main;

    tie my $sv => "CaptureVar";
    $sv =~ y/a/b/;
.Ve

Because \f(CW$sv is \f(CW\*(C`undef\*(C' when the \f(CW\*(C`y///\*(C' operator is applied to it,
the transliteration won't actually execute and the program won't
\f(CW\*(C`die\*(C'.  This is different to how 5.8 and earlier versions behaved
since the capture variables were \s-1READONLY\s0 variables then; now they'll
just die when assigned to in the default engine.

*numbered_buff_LENGTH*
Subsection "numbered_buff_LENGTH"

.Vb 4
    I32 numbered_buff_LENGTH (pTHX_
                              REGEXP * const rx,
                              const SV * const sv,
                              const I32 paren);
.Ve

Get the \f(CW\*(C`length\*(C' of a capture variable.  There's a special callback
for this so that Perl doesn't have to do a \s-1FETCH\s0 and run \f(CW\*(C`length\*(C' on
the result, since the length is (in Perl's case) known from an offset
stored in \f(CW\*(C`rx->offs\*(C', this is much more efficient:

.Vb 3
    I32 s1  = rx->offs[paren].start;
    I32 s2  = rx->offs[paren].end;
    I32 len = t1 - s1;
.Ve

This is a little bit more complex in the case of \s-1UTF-8,\s0 see what
\f(CW\*(C`Perl_reg_numbered_buff_length\*(C' does with
is_utf8_string_loclen.

### Named capture callbacks

Subsection "Named capture callbacks"
Called to get/set the value of \f(CW\*(C`%+\*(C' and \f(CW\*(C`%-\*(C', as well as by some
utility functions in re.

There are two callbacks, \f(CW\*(C`named_buff\*(C' is called in all the cases the
\s-1FETCH, STORE, DELETE, CLEAR, EXISTS\s0 and \s-1SCALAR\s0 Tie::Hash callbacks
would be on changes to \f(CW\*(C`%+\*(C' and \f(CW\*(C`%-\*(C' and \f(CW\*(C`named_buff_iter\*(C' in the
same cases as \s-1FIRSTKEY\s0 and \s-1NEXTKEY.\s0

The \f(CW\*(C`flags\*(C' parameter can be used to determine which of these
operations the callbacks should respond to.  The following flags are
currently defined:

Which Tie::Hash operation is being performed from the Perl level on
\f(CW\*(C`%+\*(C' or \f(CW\*(C`%+\*(C', if any:

.Vb 8
    RXapif_FETCH
    RXapif_STORE
    RXapif_DELETE
    RXapif_CLEAR
    RXapif_EXISTS
    RXapif_SCALAR
    RXapif_FIRSTKEY
    RXapif_NEXTKEY
.Ve

If \f(CW\*(C`%+\*(C' or \f(CW\*(C`%-\*(C' is being operated on, if any.

.Vb 2
    RXapif_ONE /* %+ */
    RXapif_ALL /* %- */
.Ve

If this is being called as \f(CW\*(C`re::regname\*(C', \f(CW\*(C`re::regnames\*(C' or
\f(CW\*(C`re::regnames_count\*(C', if any.  The first two will be combined with
\f(CW\*(C`RXapif_ONE\*(C' or \f(CW\*(C`RXapif_ALL\*(C'.

.Vb 3
    RXapif_REGNAME
    RXapif_REGNAMES
    RXapif_REGNAMES_COUNT
.Ve

Internally \f(CW\*(C`%+\*(C' and \f(CW\*(C`%-\*(C' are implemented with a real tied interface
via Tie::Hash::NamedCapture.  The methods in that package will call
back into these functions.  However the usage of
Tie::Hash::NamedCapture for this purpose might change in future
releases.  For instance this might be implemented by magic instead
(would need an extension to mgvtbl).

*named_buff*
Subsection "named_buff"

.Vb 2
    SV*     (*named_buff) (pTHX_ REGEXP * const rx, SV * const key,
                           SV * const value, U32 flags);
.Ve

*named_buff_iter*
Subsection "named_buff_iter"

.Vb 4
    SV*     (*named_buff_iter) (pTHX_
                                REGEXP * const rx,
                                const SV * const lastkey,
                                const U32 flags);
.Ve

### qr_package

Subsection "qr_package"
.Vb 1
    SV* qr_package(pTHX_ REGEXP * const rx);
.Ve

The package the qr// magic object is blessed into (as seen by \f(CW\*(C`ref
qr//\*(C').  It is recommended that engines change this to their package
name for identification regardless of if they implement methods
on the object.

The package this method returns should also have the internal
\f(CW\*(C`Regexp\*(C' package in its \f(CW@ISA.  \f(CW\*(C`qr//->isa("Regexp")\*(C' should always
be true regardless of what engine is being used.

Example implementation might be:

.Vb 6
    SV*
    Example_qr_package(pTHX_ REGEXP * const rx)
    \{
        PERL_UNUSED_ARG(rx);
        return newSVpvs("re::engine::Example");
    \}
.Ve

Any method calls on an object created with \f(CW\*(C`qr//\*(C' will be dispatched to the
package as a normal object.

.Vb 3
    use re::engine::Example;
    my $re = qr//;
    $re->meth; # dispatched to re::engine::Example::meth()
.Ve

To retrieve the \f(CW\*(C`REGEXP\*(C' object from the scalar in an \s-1XS\s0 function use
the \f(CW\*(C`SvRX\*(C' macro, see \*(L"\s-1REGEXP\s0 Functions\*(R" in perlapi.

.Vb 3
    void meth(SV * rv)
    PPCODE:
        REGEXP * re = SvRX(sv);
.Ve

### dupe

Subsection "dupe"
.Vb 1
    void* dupe(pTHX_ REGEXP * const rx, CLONE_PARAMS *param);
.Ve

On threaded builds a regexp may need to be duplicated so that the pattern
can be used by multiple threads.  This routine is expected to handle the
duplication of any private data pointed to by the \f(CW\*(C`pprivate\*(C' member of
the \f(CW\*(C`regexp\*(C' structure.  It will be called with the preconstructed new
\f(CW\*(C`regexp\*(C' structure as an argument, the \f(CW\*(C`pprivate\*(C' member will point at
the **old** private structure, and it is this routine's responsibility to
construct a copy and return a pointer to it (which Perl will then use to
overwrite the field as passed to this routine.)

This allows the engine to dupe its private data but also if necessary
modify the final structure if it really must.

On unthreaded builds this field doesn't exist.

### op_comp

Subsection "op_comp"
This is private to the Perl core and subject to change. Should be left
null.

## The REGEXP structure

Header "The REGEXP structure"
The \s-1REGEXP\s0 struct is defined in *regexp.h*.
All regex engines must be able to
correctly build such a structure in their \*(L"comp\*(R" routine.

The \s-1REGEXP\s0 structure contains all the data that Perl needs to be aware of
to properly work with the regular expression.  It includes data about
optimisations that Perl can use to determine if the regex engine should
really be used, and various other control info that is needed to properly
execute patterns in various contexts, such as if the pattern anchored in
some way, or what flags were used during the compile, or if the
program contains special constructs that Perl needs to be aware of.

In addition it contains two fields that are intended for the private
use of the regex engine that compiled the pattern.  These are the
\f(CW\*(C`intflags\*(C' and \f(CW\*(C`pprivate\*(C' members.  \f(CW\*(C`pprivate\*(C' is a void pointer to
an arbitrary structure, whose use and management is the responsibility
of the compiling engine.  Perl will never modify either of these
values.

.Vb 3
    typedef struct regexp \{
        /* what engine created this regexp? */
        const struct regexp_engine* engine;

        /* what re is this a lightweight copy of? */
        struct regexp* mother_re;

        /* Information about the match that the Perl core uses to manage
         * things */
        U32 extflags;   /* Flags used both externally and internally */
        I32 minlen;     /* mininum possible number of chars in */
                           string to match */
        I32 minlenret;  /* mininum possible number of chars in $& */
        U32 gofs;       /* chars left of pos that we search from */

        /* substring data about strings that must appear
           in the final match, used for optimisations */
        struct reg_substr_data *substrs;

        U32 nparens;  /* number of capture groups */

        /* private engine specific data */
        U32 intflags;   /* Engine Specific Internal flags */
        void *pprivate; /* Data private to the regex engine which
                           created this object. */

        /* Data about the last/current match. These are modified during
         * matching*/
        U32 lastparen;            /* highest close paren matched ($+) */
        U32 lastcloseparen;       /* last close paren matched ($^N) */
        regexp_paren_pair *offs;  /* Array of offsets for (@-) and
                                     (@+) */

        char *subbeg;  /* saved or original string so \\digit works
                          forever. */
        SV_SAVED_COPY  /* If non-NULL, SV which is COW from original */
        I32 sublen;    /* Length of string pointed by subbeg */
        I32 suboffset;  /* byte offset of subbeg from logical start of
                           str */
        I32 subcoffset; /* suboffset equiv, but in chars (for @-/@+) */

        /* Information about the match that isn\*(Aqt often used */
        I32 prelen;           /* length of precomp */
        const char *precomp;  /* pre-compilation regular expression */

        char *wrapped;  /* wrapped version of the pattern */
        I32 wraplen;    /* length of wrapped */

        I32 seen_evals;   /* number of eval groups in the pattern - for
                             security checks */
        HV *paren_names;  /* Optional hash of paren names */

        /* Refcount of this regexp */
        I32 refcnt;             /* Refcount of this regexp */
    \} regexp;
.Ve

The fields are discussed in more detail below:
.ie n .SS """engine"""
.el .SS "\f(CWengine"
Subsection "engine"
This field points at a \f(CW\*(C`regexp_engine\*(C' structure which contains pointers
to the subroutines that are to be used for performing a match.  It
is the compiling routine's responsibility to populate this field before
returning the regexp object.

Internally this is set to \f(CW\*(C`NULL\*(C' unless a custom engine is specified in
\f(CW$^H\{regcomp\}, Perl's own set of callbacks can be accessed in the struct
pointed to by \f(CW\*(C`RE_ENGINE_PTR\*(C'.
.ie n .SS """mother_re"""
.el .SS "\f(CWmother_re"
Subsection "mother_re"
\s-1TODO,\s0 see commit 28d8d7f41a.
.ie n .SS """extflags"""
.el .SS "\f(CWextflags"
Subsection "extflags"
This will be used by Perl to see what flags the regexp was compiled
with, this will normally be set to the value of the flags parameter by
the comp callback.  See the comp documentation for
valid flags.
.ie n .SS """minlen"" ""minlenret"""
.el .SS "\f(CWminlen \f(CWminlenret"
Subsection "minlen minlenret"
The minimum string length (in characters) required for the pattern to match.
This is used to
prune the search space by not bothering to match any closer to the end of a
string than would allow a match.  For instance there is no point in even
starting the regex engine if the minlen is 10 but the string is only 5
characters long.  There is no way that the pattern can match.

\f(CW\*(C`minlenret\*(C' is the minimum length (in characters) of the string that would
be found in $& after a match.

The difference between \f(CW\*(C`minlen\*(C' and \f(CW\*(C`minlenret\*(C' can be seen in the
following pattern:

.Vb 1
    /ns(?=\\d)/
.Ve

where the \f(CW\*(C`minlen\*(C' would be 3 but \f(CW\*(C`minlenret\*(C' would only be 2 as the \\d is
required to match but is not actually
included in the matched content.  This
distinction is particularly important as the substitution logic uses the
\f(CW\*(C`minlenret\*(C' to tell if it can do in-place substitutions (these can
result in considerable speed-up).
.ie n .SS """gofs"""
.el .SS "\f(CWgofs"
Subsection "gofs"
Left offset from **pos()** to start match at.
.ie n .SS """substrs"""
.el .SS "\f(CWsubstrs"
Subsection "substrs"
Substring data about strings that must appear in the final match.  This
is currently only used internally by Perl's engine, but might be
used in the future for all engines for optimisations.
.ie n .SS """nparens"", ""lastparen"", and ""lastcloseparen"""
.el .SS "\f(CWnparens, \f(CWlastparen, and \f(CWlastcloseparen"
Subsection "nparens, lastparen, and lastcloseparen"
These fields are used to keep track of: how many paren capture groups
there are in the pattern; which was the highest paren to be closed (see
\*(L"$+\*(R" in perlvar); and which was the most recent paren to be closed (see
\*(L"$^N\*(R" in perlvar).
.ie n .SS """intflags"""
.el .SS "\f(CWintflags"
Subsection "intflags"
The engine's private copy of the flags the pattern was compiled with. Usually
this is the same as \f(CW\*(C`extflags\*(C' unless the engine chose to modify one of them.
.ie n .SS """pprivate"""
.el .SS "\f(CWpprivate"
Subsection "pprivate"
A void* pointing to an engine-defined
data structure.  The Perl engine uses the
\f(CW\*(C`regexp_internal\*(C' structure (see \*(L"Base Structures\*(R" in perlreguts) but a custom
engine should use something else.
.ie n .SS """offs"""
.el .SS "\f(CWoffs"
Subsection "offs"
A \f(CW\*(C`regexp_paren_pair\*(C' structure which defines offsets into the string being
matched which correspond to the \f(CW$& and \f(CW$1, \f(CW$2 etc. captures, the
\f(CW\*(C`regexp_paren_pair\*(C' struct is defined as follows:

.Vb 4
    typedef struct regexp_paren_pair \{
        I32 start;
        I32 end;
    \} regexp_paren_pair;
.Ve

If \f(CW\*(C`->offs[num].start\*(C' or \f(CW\*(C`->offs[num].end\*(C' is \f(CW\*(C`-1\*(C' then that
capture group did not match.
\f(CW\*(C`->offs[0].start/end\*(C' represents \f(CW$& (or
\f(CW\*(C`$\{^MATCH\}\*(C' under \f(CW\*(C`/p\*(C') and \f(CW\*(C`->offs[paren].end\*(C' matches \f(CW$$paren where
\f(CW$paren = 1>.
.ie n .SS """precomp"" ""prelen"""
.el .SS "\f(CWprecomp \f(CWprelen"
Subsection "precomp prelen"
Used for optimisations.  \f(CW\*(C`precomp\*(C' holds a copy of the pattern that
was compiled and \f(CW\*(C`prelen\*(C' its length.  When a new pattern is to be
compiled (such as inside a loop) the internal \f(CW\*(C`regcomp\*(C' operator
checks if the last compiled \f(CW\*(C`REGEXP\*(C''s \f(CW\*(C`precomp\*(C' and \f(CW\*(C`prelen\*(C'
are equivalent to the new one, and if so uses the old pattern instead
of compiling a new one.

The relevant snippet from \f(CW\*(C`Perl_pp_regcomp\*(C':

.Vb 3
        if (!re || !re->precomp || re->prelen != (I32)len ||
            memNE(re->precomp, t, len))
        /* Compile a new pattern */
.Ve
.ie n .SS """paren_names"""
.el .SS "\f(CWparen_names"
Subsection "paren_names"
This is a hash used internally to track named capture groups and their
offsets.  The keys are the names of the buffers the values are dualvars,
with the \s-1IV\s0 slot holding the number of buffers with the given name and the
pv being an embedded array of I32.  The values may also be contained
independently in the data array in cases where named backreferences are
used.
.ie n .SS """substrs"""
.el .SS "\f(CWsubstrs"
Subsection "substrs"
Holds information on the longest string that must occur at a fixed
offset from the start of the pattern, and the longest string that must
occur at a floating offset from the start of the pattern.  Used to do
Fast-Boyer-Moore searches on the string to find out if its worth using
the regex engine at all, and if so where in the string to search.
.ie n .SS """subbeg"" ""sublen"" ""saved_copy"" ""suboffset"" ""subcoffset"""
.el .SS "\f(CWsubbeg \f(CWsublen \f(CWsaved_copy \f(CWsuboffset \f(CWsubcoffset"
Subsection "subbeg sublen saved_copy suboffset subcoffset"
Used during the execution phase for managing search and replace patterns,
and for providing the text for \f(CW$&, \f(CW$1 etc. \f(CW\*(C`subbeg\*(C' points to a
buffer (either the original string, or a copy in the case of
\f(CW\*(C`RX_MATCH_COPIED(rx)\*(C'), and \f(CW\*(C`sublen\*(C' is the length of the buffer.  The
\f(CW\*(C`RX_OFFS\*(C' start and end indices index into this buffer.

In the presence of the \f(CW\*(C`REXEC_COPY_STR\*(C' flag, but with the addition of
the \f(CW\*(C`REXEC_COPY_SKIP_PRE\*(C' or \f(CW\*(C`REXEC_COPY_SKIP_POST\*(C' flags, an engine
can choose not to copy the full buffer (although it must still do so in
the presence of \f(CW\*(C`RXf_PMf_KEEPCOPY\*(C' or the relevant bits being set in
\f(CW\*(C`PL_sawampersand\*(C').  In this case, it may set \f(CW\*(C`suboffset\*(C' to indicate the
number of bytes from the logical start of the buffer to the physical start
(i.e. \f(CW\*(C`subbeg\*(C').  It should also set \f(CW\*(C`subcoffset\*(C', the number of
characters in the offset. The latter is needed to support \f(CW\*(C`@-\*(C' and \f(CW\*(C`@+\*(C'
which work in characters, not bytes.
.ie n .SS """wrapped"" ""wraplen"""
.el .SS "\f(CWwrapped \f(CWwraplen"
Subsection "wrapped wraplen"
Stores the string \f(CW\*(C`qr//\*(C' stringifies to. The Perl engine for example
stores \f(CW\*(C`(?^:eek)\*(C' in the case of \f(CW\*(C`qr/eek/\*(C'.

When using a custom engine that doesn't support the \f(CW\*(C`(?:)\*(C' construct
for inline modifiers, it's probably best to have \f(CW\*(C`qr//\*(C' stringify to
the supplied pattern, note that this will create undesired patterns in
cases such as:

.Vb 3
    my $x = qr/a|b/;  # "a|b"
    my $y = qr/c/i;   # "c"
    my $z = qr/$x$y/; # "a|bc"
.Ve

There's no solution for this problem other than making the custom
engine understand a construct like \f(CW\*(C`(?:)\*(C'.
.ie n .SS """seen_evals"""
.el .SS "\f(CWseen_evals"
Subsection "seen_evals"
This stores the number of eval groups in
the pattern.  This is used for security
purposes when embedding compiled regexes into larger patterns with \f(CW\*(C`qr//\*(C'.
.ie n .SS """refcnt"""
.el .SS "\f(CWrefcnt"
Subsection "refcnt"
The number of times the structure is referenced.  When
this falls to 0, the regexp is automatically freed
by a call to \f(CW\*(C`pregfree\*(C'.  This should be set to 1 in
each engine's \*(L"comp\*(R" routine.

## HISTORY

Header "HISTORY"
Originally part of perlreguts.

## AUTHORS

Header "AUTHORS"
Originally written by Yves Orton, expanded by \*(Aevar Arnfjo\*:r\*(d-
Bjarmason.

## LICENSE

Header "LICENSE"
Copyright 2006 Yves Orton and 2007 \*(Aevar Arnfjo\*:r\*(d- Bjarmason.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.
