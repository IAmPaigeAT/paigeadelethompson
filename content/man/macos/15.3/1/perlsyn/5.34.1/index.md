+++
manpage_section = "1"
description = "A Perl program consists of a sequence of declarations and statements which run from the top to the bottom.  Loops, subroutines, and other control structures allow you to jump around within the code. Perl is a free-form language: you can format and ..."
title = "perlsyn(1)"
date = "2022-02-19"
author = "None Specified"
detected_package_version = "5.34.1"
manpage_name = "perlsyn"
operating_system = "macos"
manpage_format = "troff"
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLSYN 1"
PERLSYN 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlsyn - Perl syntax
Xref "syntax"

## DESCRIPTION

Header "DESCRIPTION"
A Perl program consists of a sequence of declarations and statements
which run from the top to the bottom.  Loops, subroutines, and other
control structures allow you to jump around within the code.

Perl is a **free-form** language: you can format and indent it however
you like.  Whitespace serves mostly to separate tokens, unlike
languages like Python where it is an important part of the syntax,
or Fortran where it is immaterial.

Many of Perl's syntactic elements are **optional**.  Rather than
requiring you to put parentheses around every function call and
declare every variable, you can often leave such explicit elements off
and Perl will figure out what you meant.  This is known as \fBDo What I
Mean, abbreviated **\s-1DWIM\s0**.  It allows programmers to be **lazy** and to
code in a style with which they are comfortable.

Perl **borrows syntax** and concepts from many languages: awk, sed, C,
Bourne Shell, Smalltalk, Lisp and even English.  Other
languages have borrowed syntax from Perl, particularly its regular
expression extensions.  So if you have programmed in another language
you will see familiar pieces in Perl.  They often work the same, but
see perltrap for information about how they differ.

### Declarations

Xref "declaration undef undefined uninitialized"
Subsection "Declarations"
The only things you need to declare in Perl are report formats and
subroutines (and sometimes not even subroutines).  A scalar variable holds
the undefined value (\f(CW\*(C`undef\*(C') until it has been assigned a defined
value, which is anything other than \f(CW\*(C`undef\*(C'.  When used as a number,
\f(CW\*(C`undef\*(C' is treated as \f(CW0; when used as a string, it is treated as
the empty string, \f(CW""; and when used as a reference that isn't being
assigned to, it is treated as an error.  If you enable warnings,
you'll be notified of an uninitialized value whenever you treat
\f(CW\*(C`undef\*(C' as a string or a number.  Well, usually.  Boolean contexts,
such as:

.Vb 1
    if ($a) \{\}
.Ve

are exempt from warnings (because they care about truth rather than
definedness).  Operators such as \f(CW\*(C`++\*(C', \f(CW\*(C`--\*(C', \f(CW\*(C`+=\*(C',
\f(CW\*(C`-=\*(C', and \f(CW\*(C`.=\*(C', that operate on undefined variables such as:

.Vb 2
    undef $a;
    $a++;
.Ve

are also always exempt from such warnings.

A declaration can be put anywhere a statement can, but has no effect on
the execution of the primary sequence of statements: declarations all
take effect at compile time.  All declarations are typically put at
the beginning or the end of the script.  However, if you're using
lexically-scoped private variables created with \f(CW\*(C`my()\*(C',
\f(CW\*(C`state()\*(C', or \f(CW\*(C`our()\*(C', you'll have to make sure
your format or subroutine definition is within the same block scope
as the my if you expect to be able to access those private variables.

Declaring a subroutine allows a subroutine name to be used as if it were a
list operator from that point forward in the program.  You can declare a
subroutine without defining it by saying \f(CW\*(C`sub name\*(C', thus:
Xref "subroutine, declaration"

.Vb 2
    sub myname;
    $me = myname $0             or die "can\*(Aqt get myname";
.Ve

A bare declaration like that declares the function to be a list operator,
not a unary operator, so you have to be careful to use parentheses (or
\f(CW\*(C`or\*(C' instead of \f(CW\*(C`||\*(C'.)  The \f(CW\*(C`||\*(C' operator binds too tightly to use after
list operators; it becomes part of the last element.  You can always use
parentheses around the list operators arguments to turn the list operator
back into something that behaves more like a function call.  Alternatively,
you can use the prototype \f(CW\*(C`($)\*(C' to turn the subroutine into a unary
operator:

.Vb 2
  sub myname ($);
  $me = myname $0             || die "can\*(Aqt get myname";
.Ve

That now parses as you'd expect, but you still ought to get in the habit of
using parentheses in that situation.  For more on prototypes, see
perlsub.

Subroutines declarations can also be loaded up with the \f(CW\*(C`require\*(C' statement
or both loaded and imported into your namespace with a \f(CW\*(C`use\*(C' statement.
See perlmod for details on this.

A statement sequence may contain declarations of lexically-scoped
variables, but apart from declaring a variable name, the declaration acts
like an ordinary statement, and is elaborated within the sequence of
statements as if it were an ordinary statement.  That means it actually
has both compile-time and run-time effects.

### Comments

Xref "comment #"
Subsection "Comments"
Text from a \f(CW"#" character until the end of the line is a comment,
and is ignored.  Exceptions include \f(CW"#" inside a string or regular
expression.

### Simple Statements

Xref "statement semicolon expression ;"
Subsection "Simple Statements"
The only kind of simple statement is an expression evaluated for its
side-effects.  Every simple statement must be terminated with a
semicolon, unless it is the final statement in a block, in which case
the semicolon is optional.  But put the semicolon in anyway if the
block takes up more than one line, because you may eventually add
another line.  Note that there are operators like \f(CW\*(C`eval \{\}\*(C', \f(CW\*(C`sub \{\}\*(C', and
\f(CW\*(C`do \{\}\*(C' that *look* like compound statements, but aren't\*(--they're just
TERMs in an expression\*(--and thus need an explicit termination when used
as the last item in a statement.

### Statement Modifiers

Xref "statement modifier modifier if unless while until when foreach for"
Subsection "Statement Modifiers"
Any simple statement may optionally be followed by a *\s-1SINGLE\s0* modifier,
just before the terminating semicolon (or block ending).  The possible
modifiers are:

.Vb 7
    if EXPR
    unless EXPR
    while EXPR
    until EXPR
    for LIST
    foreach LIST
    when EXPR
.Ve

The \f(CW\*(C`EXPR\*(C' following the modifier is referred to as the \*(L"condition\*(R".
Its truth or falsehood determines how the modifier will behave.

\f(CW\*(C`if\*(C' executes the statement once *if* and only if the condition is
true.  \f(CW\*(C`unless\*(C' is the opposite, it executes the statement *unless*
the condition is true (that is, if the condition is false).  See
\*(L"Scalar values\*(R" in perldata for definitions of true and false.

.Vb 2
    print "Basset hounds got long ears" if length $ear >= 10;
    go_outside() and play() unless $is_raining;
.Ve

The \f(CW\*(C`for(each)\*(C' modifier is an iterator: it executes the statement once
for each item in the \s-1LIST\s0 (with \f(CW$_ aliased to each item in turn).
There is no syntax to specify a C-style for loop or a lexically scoped
iteration variable in this form.

.Vb 1
    print "Hello $_!\\n" for qw(world Dolly nurse);
.Ve

\f(CW\*(C`while\*(C' repeats the statement *while* the condition is true.
Postfix \f(CW\*(C`while\*(C' has the same magic treatment of some kinds of condition
that prefix \f(CW\*(C`while\*(C' has.
\f(CW\*(C`until\*(C' does the opposite, it repeats the statement *until* the
condition is true (or while the condition is false):

.Vb 3
    # Both of these count from 0 to 10.
    print $i++ while $i <= 10;
    print $j++ until $j >  10;
.Ve

The \f(CW\*(C`while\*(C' and \f(CW\*(C`until\*(C' modifiers have the usual "\f(CW\*(C`while\*(C' loop"
semantics (conditional evaluated first), except when applied to a
\f(CW\*(C`do\*(C'-BLOCK (or to the Perl4 \f(CW\*(C`do\*(C'-SUBROUTINE statement), in
which case the block executes once before the conditional is
evaluated.

This is so that you can write loops like:

.Vb 4
    do \{
        $line = <STDIN>;
        ...
    \} until !defined($line) || $line eq ".\\n"
.Ve

See \*(L"do\*(R" in perlfunc.  Note also that the loop control statements described
later will *\s-1NOT\s0* work in this construct, because modifiers don't take
loop labels.  Sorry.  You can always put another block inside of it
(for \f(CW\*(C`next\*(C'/\f(CW\*(C`redo\*(C') or around it (for \f(CW\*(C`last\*(C') to do that sort of thing.
Xref "next last redo"

For \f(CW\*(C`next\*(C' or \f(CW\*(C`redo\*(C', just double the braces:

.Vb 4
    do \{\{
        next if $x == $y;
        # do something here
    \}\} until $x++ > $z;
.Ve

For \f(CW\*(C`last\*(C', you have to be more elaborate and put braces around it:
Xref "last"

.Vb 6
    \{
        do \{
            last if $x == $y**2;
            # do something here
        \} while $x++ <= $z;
    \}
.Ve

If you need both \f(CW\*(C`next\*(C' and \f(CW\*(C`last\*(C', you have to do both and also use a
loop label:

.Vb 7
    LOOP: \{
        do \{\{
            next if $x == $y;
            last LOOP if $x == $y**2;
            # do something here
        \}\} until $x++ > $z;
    \}
.Ve

**\s-1NOTE:\s0** The behaviour of a \f(CW\*(C`my\*(C', \f(CW\*(C`state\*(C', or
\f(CW\*(C`our\*(C' modified with a statement modifier conditional
or loop construct (for example, \f(CW\*(C`my $x if ...\*(C') is
**undefined**.  The value of the \f(CW\*(C`my\*(C' variable may be \f(CW\*(C`undef\*(C', any
previously assigned value, or possibly anything else.  Don't rely on
it.  Future versions of perl might do something different from the
version of perl you try it out on.  Here be dragons.
Xref "my"

The \f(CW\*(C`when\*(C' modifier is an experimental feature that first appeared in Perl
5.14.  To use it, you should include a \f(CW\*(C`use v5.14\*(C' declaration.
(Technically, it requires only the \f(CW\*(C`switch\*(C' feature, but that aspect of it
was not available before 5.14.)  Operative only from within a \f(CW\*(C`foreach\*(C'
loop or a \f(CW\*(C`given\*(C' block, it executes the statement only if the smartmatch
\f(CW\*(C`$_ ~~ \f(CIEXPR\f(CW\*(C' is true.  If the statement executes, it is followed by
a \f(CW\*(C`next\*(C' from inside a \f(CW\*(C`foreach\*(C' and \f(CW\*(C`break\*(C' from inside a \f(CW\*(C`given\*(C'.

Under the current implementation, the \f(CW\*(C`foreach\*(C' loop can be
anywhere within the \f(CW\*(C`when\*(C' modifier's dynamic scope, but must be
within the \f(CW\*(C`given\*(C' block's lexical scope.  This restriction may
be relaxed in a future release.  See \*(L"Switch Statements\*(R" below.

### Compound Statements

Xref "statement, compound block bracket, curly curly bracket brace \{ \} if unless given while until foreach for continue"
Subsection "Compound Statements"
In Perl, a sequence of statements that defines a scope is called a block.
Sometimes a block is delimited by the file containing it (in the case
of a required file, or the program as a whole), and sometimes a block
is delimited by the extent of a string (in the case of an eval).

But generally, a block is delimited by curly brackets, also known as
braces.  We will call this syntactic construct a \s-1BLOCK.\s0  Because enclosing
braces are also the syntax for hash reference constructor expressions
(see perlref), you may occasionally need to disambiguate by placing a
\f(CW\*(C`;\*(C' immediately after an opening brace so that Perl realises the brace
is the start of a block.  You will more frequently need to disambiguate
the other way, by placing a \f(CW\*(C`+\*(C' immediately before an opening brace to
force it to be interpreted as a hash reference constructor expression.
It is considered good style to use these disambiguating mechanisms
liberally, not only when Perl would otherwise guess incorrectly.

The following compound statements may be used to control flow:

.Vb 4
    if (EXPR) BLOCK
    if (EXPR) BLOCK else BLOCK
    if (EXPR) BLOCK elsif (EXPR) BLOCK ...
    if (EXPR) BLOCK elsif (EXPR) BLOCK ... else BLOCK

    unless (EXPR) BLOCK
    unless (EXPR) BLOCK else BLOCK
    unless (EXPR) BLOCK elsif (EXPR) BLOCK ...
    unless (EXPR) BLOCK elsif (EXPR) BLOCK ... else BLOCK

    given (EXPR) BLOCK

    LABEL while (EXPR) BLOCK
    LABEL while (EXPR) BLOCK continue BLOCK

    LABEL until (EXPR) BLOCK
    LABEL until (EXPR) BLOCK continue BLOCK

    LABEL for (EXPR; EXPR; EXPR) BLOCK
    LABEL for VAR (LIST) BLOCK
    LABEL for VAR (LIST) BLOCK continue BLOCK

    LABEL foreach (EXPR; EXPR; EXPR) BLOCK
    LABEL foreach VAR (LIST) BLOCK
    LABEL foreach VAR (LIST) BLOCK continue BLOCK

    LABEL BLOCK
    LABEL BLOCK continue BLOCK

    PHASE BLOCK
.Ve

If enabled by the experimental \f(CW\*(C`try\*(C' feature, the following may also be used

.Vb 1
    try BLOCK catch (VAR) BLOCK
.Ve

The experimental \f(CW\*(C`given\*(C' statement is *not automatically enabled*; see
\*(L"Switch Statements\*(R" below for how to do so, and the attendant caveats.

Unlike in C and Pascal, in Perl these are all defined in terms of BLOCKs,
not statements.  This means that the curly brackets are *required*--no
dangling statements allowed.  If you want to write conditionals without
curly brackets, there are several other ways to do it.  The following
all do the same thing:

.Vb 5
    if (!open(FOO)) \{ die "Can\*(Aqt open $FOO: $!" \}
    die "Can\*(Aqt open $FOO: $!" unless open(FOO);
    open(FOO)  || die "Can\*(Aqt open $FOO: $!";
    open(FOO) ? () : die "Can\*(Aqt open $FOO: $!";
        # a bit exotic, that last one
.Ve

The \f(CW\*(C`if\*(C' statement is straightforward.  Because BLOCKs are always
bounded by curly brackets, there is never any ambiguity about which
\f(CW\*(C`if\*(C' an \f(CW\*(C`else\*(C' goes with.  If you use \f(CW\*(C`unless\*(C' in place of \f(CW\*(C`if\*(C',
the sense of the test is reversed.  Like \f(CW\*(C`if\*(C', \f(CW\*(C`unless\*(C' can be followed
by \f(CW\*(C`else\*(C'.  \f(CW\*(C`unless\*(C' can even be followed by one or more \f(CW\*(C`elsif\*(C'
statements, though you may want to think twice before using that particular
language construct, as everyone reading your code will have to think at least
twice before they can understand what's going on.

The \f(CW\*(C`while\*(C' statement executes the block as long as the expression is
true.
The \f(CW\*(C`until\*(C' statement executes the block as long as the expression is
false.
The \s-1LABEL\s0 is optional, and if present, consists of an identifier followed
by a colon.  The \s-1LABEL\s0 identifies the loop for the loop control
statements \f(CW\*(C`next\*(C', \f(CW\*(C`last\*(C', and \f(CW\*(C`redo\*(C'.
If the \s-1LABEL\s0 is omitted, the loop control statement
refers to the innermost enclosing loop.  This may include dynamically
searching through your call-stack at run time to find the \s-1LABEL.\s0  Such
desperate behavior triggers a warning if you use the \f(CW\*(C`use warnings\*(C'
pragma or the **-w** flag.

If the condition expression of a \f(CW\*(C`while\*(C' statement is based
on any of a group of iterative expression types then it gets
some magic treatment.  The affected iterative expression types
are \f(CW\*(C`readline\*(C', the \f(CW\*(C`<FILEHANDLE>\*(C' input operator, \f(CW\*(C`readdir\*(C', \f(CW\*(C`glob\*(C', the \f(CW\*(C`<PATTERN>\*(C' globbing operator, and \f(CW\*(C`each\*(C'.  If the condition expression is one of these expression types, then
the value yielded by the iterative operator will be implicitly assigned
to \f(CW$_.  If the condition expression is one of these expression types
or an explicit assignment of one of them to a scalar, then the condition
actually tests for definedness of the expression's value, not for its
regular truth value.

If there is a \f(CW\*(C`continue\*(C' \s-1BLOCK,\s0 it is always executed just before the
conditional is about to be evaluated again.  Thus it can be used to
increment a loop variable, even when the loop has been continued via
the \f(CW\*(C`next\*(C' statement.

When a block is preceded by a compilation phase keyword such as \f(CW\*(C`BEGIN\*(C',
\f(CW\*(C`END\*(C', \f(CW\*(C`INIT\*(C', \f(CW\*(C`CHECK\*(C', or \f(CW\*(C`UNITCHECK\*(C', then the block will run only
during the corresponding phase of execution.  See perlmod for more details.

Extension modules can also hook into the Perl parser to define new
kinds of compound statements.  These are introduced by a keyword which
the extension recognizes, and the syntax following the keyword is
defined entirely by the extension.  If you are an implementor, see
\*(L"PL_keyword_plugin\*(R" in perlapi for the mechanism.  If you are using such
a module, see the module's documentation for details of the syntax that
it defines.

### Loop Control

Xref "loop control loop, control next last redo continue"
Subsection "Loop Control"
The \f(CW\*(C`next\*(C' command starts the next iteration of the loop:

.Vb 4
    LINE: while (<STDIN>) \{
        next LINE if /^#/;      # discard comments
        ...
    \}
.Ve

The \f(CW\*(C`last\*(C' command immediately exits the loop in question.  The
\f(CW\*(C`continue\*(C' block, if any, is not executed:

.Vb 4
    LINE: while (<STDIN>) \{
        last LINE if /^$/;      # exit when done with header
        ...
    \}
.Ve

The \f(CW\*(C`redo\*(C' command restarts the loop block without evaluating the
conditional again.  The \f(CW\*(C`continue\*(C' block, if any, is *not* executed.
This command is normally used by programs that want to lie to themselves
about what was just input.

For example, when processing a file like */etc/termcap*.
If your input lines might end in backslashes to indicate continuation, you
want to skip ahead and get the next record.

.Vb 8
    while (<>) \{
        chomp;
        if (s/\\\\$//) \{
            $_ .= <>;
            redo unless eof();
        \}
        # now process $_
    \}
.Ve

which is Perl shorthand for the more explicitly written version:

.Vb 8
    LINE: while (defined($line = <ARGV>)) \{
        chomp($line);
        if ($line =~ s/\\\\$//) \{
            $line .= <ARGV>;
            redo LINE unless eof(); # not eof(ARGV)!
        \}
        # now process $line
    \}
.Ve

Note that if there were a \f(CW\*(C`continue\*(C' block on the above code, it would
get executed only on lines discarded by the regex (since redo skips the
continue block).  A continue block is often used to reset line counters
or \f(CW\*(C`m?pat?\*(C' one-time matches:

.Vb 10
    # inspired by :1,$g/fred/s//WILMA/
    while (<>) \{
        m?(fred)?    && s//WILMA $1 WILMA/;
        m?(barney)?  && s//BETTY $1 BETTY/;
        m?(homer)?   && s//MARGE $1 MARGE/;
    \} continue \{
        print "$ARGV $.: $_";
        close ARGV  if eof;             # reset $.
        reset       if eof;             # reset ?pat?
    \}
.Ve

If the word \f(CW\*(C`while\*(C' is replaced by the word \f(CW\*(C`until\*(C', the sense of the
test is reversed, but the conditional is still tested before the first
iteration.

Loop control statements don't work in an \f(CW\*(C`if\*(C' or \f(CW\*(C`unless\*(C', since
they aren't loops.  You can double the braces to make them such, though.

.Vb 6
    if (/pattern/) \{\{
        last if /fred/;
        next if /barney/; # same effect as "last",
                          # but doesn\*(Aqt document as well
        # do something here
    \}\}
.Ve

This is caused by the fact that a block by itself acts as a loop that
executes once, see \*(L"Basic BLOCKs\*(R".

The form \f(CW\*(C`while/if BLOCK BLOCK\*(C', available in Perl 4, is no longer
available.   Replace any occurrence of \f(CW\*(C`if BLOCK\*(C' by \f(CW\*(C`if (do BLOCK)\*(C'.

### For Loops

Xref "for foreach"
Subsection "For Loops"
Perl's C-style \f(CW\*(C`for\*(C' loop works like the corresponding \f(CW\*(C`while\*(C' loop;
that means that this:

.Vb 3
    for ($i = 1; $i < 10; $i++) \{
        ...
    \}
.Ve

is the same as this:

.Vb 6
    $i = 1;
    while ($i < 10) \{
        ...
    \} continue \{
        $i++;
    \}
.Ve

There is one minor difference: if variables are declared with \f(CW\*(C`my\*(C'
in the initialization section of the \f(CW\*(C`for\*(C', the lexical scope of
those variables is exactly the \f(CW\*(C`for\*(C' loop (the body of the loop
and the control sections).  To illustrate:
Xref "my"

.Vb 5
    my $i = \*(Aqsamba\*(Aq;
    for (my $i = 1; $i <= 4; $i++) \{
        print "$i\\n";
    \}
    print "$i\\n";
.Ve

when executed, gives:

.Vb 5
    1
    2
    3
    4
    samba
.Ve

As a special case, if the test in the \f(CW\*(C`for\*(C' loop (or the corresponding
\f(CW\*(C`while\*(C' loop) is empty, it is treated as true.  That is, both

.Vb 3
    for (;;) \{
        ...
    \}
.Ve

and

.Vb 3
    while () \{
        ...
    \}
.Ve

are treated as infinite loops.

Besides the normal array index looping, \f(CW\*(C`for\*(C' can lend itself
to many other interesting applications.  Here's one that avoids the
problem you get into if you explicitly test for end-of-file on
an interactive file descriptor causing your program to appear to
hang.
Xref "eof end-of-file end of file"

.Vb 5
    $on_a_tty = -t STDIN && -t STDOUT;
    sub prompt \{ print "yes? " if $on_a_tty \}
    for ( prompt(); <STDIN>; prompt() ) \{
        # do something
    \}
.Ve

The condition expression of a \f(CW\*(C`for\*(C' loop gets the same magic treatment of
\f(CW\*(C`readline\*(C' et al that the condition expression of a \f(CW\*(C`while\*(C' loop gets.

### Foreach Loops

Xref "for foreach"
Subsection "Foreach Loops"
The \f(CW\*(C`foreach\*(C' loop iterates over a normal list value and sets the scalar
variable \s-1VAR\s0 to be each element of the list in turn.  If the variable
is preceded with the keyword \f(CW\*(C`my\*(C', then it is lexically scoped, and
is therefore visible only within the loop.  Otherwise, the variable is
implicitly local to the loop and regains its former value upon exiting
the loop.  If the variable was previously declared with \f(CW\*(C`my\*(C', it uses
that variable instead of the global one, but it's still localized to
the loop.  This implicit localization occurs *only* in a \f(CW\*(C`foreach\*(C'
loop.
Xref "my local"

The \f(CW\*(C`foreach\*(C' keyword is actually a synonym for the \f(CW\*(C`for\*(C' keyword, so
you can use either.  If \s-1VAR\s0 is omitted, \f(CW$_ is set to each value.
Xref "$_"

If any element of \s-1LIST\s0 is an lvalue, you can modify it by modifying
\s-1VAR\s0 inside the loop.  Conversely, if any element of \s-1LIST\s0 is \s-1NOT\s0 an
lvalue, any attempt to modify that element will fail.  In other words,
the \f(CW\*(C`foreach\*(C' loop index variable is an implicit alias for each item
in the list that you're looping over.
Xref "alias"

If any part of \s-1LIST\s0 is an array, \f(CW\*(C`foreach\*(C' will get very confused if
you add or remove elements within the loop body, for example with
\f(CW\*(C`splice\*(C'.   So don't do that.
Xref "splice"

\f(CW\*(C`foreach\*(C' probably won't do what you expect if \s-1VAR\s0 is a tied or other
special variable.   Don't do that either.

As of Perl 5.22, there is an experimental variant of this loop that accepts
a variable preceded by a backslash for \s-1VAR,\s0 in which case the items in the
\s-1LIST\s0 must be references.  The backslashed variable will become an alias
to each referenced item in the \s-1LIST,\s0 which must be of the correct type.
The variable needn't be a scalar in this case, and the backslash may be
followed by \f(CW\*(C`my\*(C'.  To use this form, you must enable the \f(CW\*(C`refaliasing\*(C'
feature via \f(CW\*(C`use feature\*(C'.  (See feature.  See also \*(L"Assigning
to References\*(R" in perlref.)

Examples:

.Vb 1
    for (@ary) \{ s/foo/bar/ \}

    for my $elem (@elements) \{
        $elem *= 2;
    \}

    for $count (reverse(1..10), "BOOM") \{
        print $count, "\\n";
        sleep(1);
    \}

    for (1..15) \{ print "Merry Christmas\\n"; \}

    foreach $item (split(/:[\\\\\\n:]*/, $ENV\{TERMCAP\})) \{
        print "Item: $item\\n";
    \}

    use feature "refaliasing";
    no warnings "experimental::refaliasing";
    foreach \\my %hash (@array_of_hash_references) \{
        # do something which each %hash
    \}
.Ve

Here's how a C programmer might code up a particular algorithm in Perl:

.Vb 9
    for (my $i = 0; $i < @ary1; $i++) \{
        for (my $j = 0; $j < @ary2; $j++) \{
            if ($ary1[$i] > $ary2[$j]) \{
                last; # can\*(Aqt go to outer :-(
            \}
            $ary1[$i] += $ary2[$j];
        \}
        # this is where that last takes me
    \}
.Ve

Whereas here's how a Perl programmer more comfortable with the idiom might
do it:

.Vb 6
    OUTER: for my $wid (@ary1) \{
    INNER:   for my $jet (@ary2) \{
                next OUTER if $wid > $jet;
                $wid += $jet;
             \}
          \}
.Ve

See how much easier this is?  It's cleaner, safer, and faster.  It's
cleaner because it's less noisy.  It's safer because if code gets added
between the inner and outer loops later on, the new code won't be
accidentally executed.  The \f(CW\*(C`next\*(C' explicitly iterates the other loop
rather than merely terminating the inner one.  And it's faster because
Perl executes a \f(CW\*(C`foreach\*(C' statement more rapidly than it would the
equivalent C-style \f(CW\*(C`for\*(C' loop.

Perceptive Perl hackers may have noticed that a \f(CW\*(C`for\*(C' loop has a return
value, and that this value can be captured by wrapping the loop in a \f(CW\*(C`do\*(C'
block.  The reward for this discovery is this cautionary advice:  The
return value of a \f(CW\*(C`for\*(C' loop is unspecified and may change without notice.
Do not rely on it.

### Try Catch Exception Handling

Xref "try catch"
Subsection "Try Catch Exception Handling"
The \f(CW\*(C`try\*(C'/\f(CW\*(C`catch\*(C' syntax provides control flow relating to exception
handling. The \f(CW\*(C`try\*(C' keyword introduces a block which will be executed when it
is encountered, and the \f(CW\*(C`catch\*(C' block provides code to handle any exception
that may be thrown by the first.

.Vb 9
    try \{
        my $x = call_a_function();
        $x < 100 or die "Too big";
        send_output($x);
    \}
    catch ($e) \{
        warn "Unable to output a value; $e";
    \}
    print "Finished\\n";
.Ve

Here, the body of the \f(CW\*(C`catch\*(C' block (i.e. the \f(CW\*(C`warn\*(C' statement) will be
executed if the initial block invokes the conditional \f(CW\*(C`die\*(C', or if either of
the functions it invokes throws an uncaught exception. The \f(CW\*(C`catch\*(C' block can
inspect the \f(CW$e lexical variable in this case to see what the exception was.
If no exception was thrown then the \f(CW\*(C`catch\*(C' block does not happen. In either
case, execution will then continue from the following statement - in this
example the \f(CW\*(C`print\*(C'.

The \f(CW\*(C`catch\*(C' keyword must be immediately followed by a variable declaration in
parentheses, which introduces a new variable visible to the body of the
subsequent block. Inside the block this variable will contain the exception
value that was thrown by the code in the \f(CW\*(C`try\*(C' block. It is not necessary
to use the \f(CW\*(C`my\*(C' keyword to declare this variable; this is implied (similar
as it is for subroutine signatures).

Both the \f(CW\*(C`try\*(C' and the \f(CW\*(C`catch\*(C' blocks are permitted to contain control-flow
expressions, such as \f(CW\*(C`return\*(C', \f(CW\*(C`goto\*(C', or \f(CW\*(C`next\*(C'/\f(CW\*(C`last\*(C'/\f(CW\*(C`redo\*(C'. In all
cases they behave as expected without warnings. In particular, a \f(CW\*(C`return\*(C'
expression inside the \f(CW\*(C`try\*(C' block will make its entire containing function
return - this is in contrast to its behaviour inside an \f(CW\*(C`eval\*(C' block, where
it would only make that block return.

Like other control-flow syntax, \f(CW\*(C`try\*(C' and \f(CW\*(C`catch\*(C' will yield the last
evaluated value when placed as the final statement in a function or a \f(CW\*(C`do\*(C'
block. This permits the syntax to be used to create a value. In this case
remember not to use the \f(CW\*(C`return\*(C' expression, or that will cause the
containing function to return.

.Vb 9
    my $value = do \{
        try \{
            get_thing(@args);
        \}
        catch ($e) \{
            warn "Unable to get thing - $e";
            $DEFAULT_THING;
        \}
    \};
.Ve

As with other control-flow syntax, \f(CW\*(C`try\*(C' blocks are not visible to
\f(CW\*(C`caller()\*(C' (just as for example, \f(CW\*(C`while\*(C' or \f(CW\*(C`foreach\*(C' loops are not).
Successive levels of the \f(CW\*(C`caller\*(C' result can see subroutine calls and
\f(CW\*(C`eval\*(C' blocks, because those affect the way that \f(CW\*(C`return\*(C' would work. Since
\f(CW\*(C`try\*(C' blocks do not intercept \f(CW\*(C`return\*(C', they are not of interest to
\f(CW\*(C`caller\*(C'.

This syntax is currently experimental and must be enabled with
\f(CW\*(C`use feature \*(Aqtry\*(Aq\*(C'. It emits a warning in the \f(CW\*(C`experimental::try\*(C' category.

### Basic BLOCKs

Xref "block"
Subsection "Basic BLOCKs"
A \s-1BLOCK\s0 by itself (labeled or not) is semantically equivalent to a
loop that executes once.  Thus you can use any of the loop control
statements in it to leave or restart the block.  (Note that this is
*\s-1NOT\s0* true in \f(CW\*(C`eval\{\}\*(C', \f(CW\*(C`sub\{\}\*(C', or contrary to popular belief
\f(CW\*(C`do\{\}\*(C' blocks, which do *\s-1NOT\s0* count as loops.)  The \f(CW\*(C`continue\*(C'
block is optional.

The \s-1BLOCK\s0 construct can be used to emulate case structures.

.Vb 6
    SWITCH: \{
        if (/^abc/) \{ $abc = 1; last SWITCH; \}
        if (/^def/) \{ $def = 1; last SWITCH; \}
        if (/^xyz/) \{ $xyz = 1; last SWITCH; \}
        $nothing = 1;
    \}
.Ve

You'll also find that \f(CW\*(C`foreach\*(C' loop used to create a topicalizer
and a switch:

.Vb 7
    SWITCH:
    for ($var) \{
        if (/^abc/) \{ $abc = 1; last SWITCH; \}
        if (/^def/) \{ $def = 1; last SWITCH; \}
        if (/^xyz/) \{ $xyz = 1; last SWITCH; \}
        $nothing = 1;
    \}
.Ve

Such constructs are quite frequently used, both because older versions of
Perl had no official \f(CW\*(C`switch\*(C' statement, and also because the new version
described immediately below remains experimental and can sometimes be confusing.

### Switch Statements

Subsection "Switch Statements"

Xref "switch case given when default"

Starting from Perl 5.10.1 (well, 5.10.0, but it didn't work
right), you can say

.Vb 1
    use feature "switch";
.Ve

to enable an experimental switch feature.  This is loosely based on an
old version of a Raku proposal, but it no longer resembles the Raku
construct.   You also get the switch feature whenever you declare that your
code prefers to run under a version of Perl that is 5.10 or later.  For
example:

.Vb 1
    use v5.14;
.Ve

Under the \*(L"switch\*(R" feature, Perl gains the experimental keywords
\f(CW\*(C`given\*(C', \f(CW\*(C`when\*(C', \f(CW\*(C`default\*(C', \f(CW\*(C`continue\*(C', and \f(CW\*(C`break\*(C'.
Starting from Perl 5.16, one can prefix the switch
keywords with \f(CW\*(C`CORE::\*(C' to access the feature without a \f(CW\*(C`use feature\*(C'
statement.  The keywords \f(CW\*(C`given\*(C' and
\f(CW\*(C`when\*(C' are analogous to \f(CW\*(C`switch\*(C' and
\f(CW\*(C`case\*(C' in other languages \*(-- though \f(CW\*(C`continue\*(C' is not \*(-- so the code
in the previous section could be rewritten as

.Vb 7
    use v5.10.1;
    for ($var) \{
        when (/^abc/) \{ $abc = 1 \}
        when (/^def/) \{ $def = 1 \}
        when (/^xyz/) \{ $xyz = 1 \}
        default       \{ $nothing = 1 \}
    \}
.Ve

The \f(CW\*(C`foreach\*(C' is the non-experimental way to set a topicalizer.
If you wish to use the highly experimental \f(CW\*(C`given\*(C', that could be
written like this:

.Vb 7
    use v5.10.1;
    given ($var) \{
        when (/^abc/) \{ $abc = 1 \}
        when (/^def/) \{ $def = 1 \}
        when (/^xyz/) \{ $xyz = 1 \}
        default       \{ $nothing = 1 \}
    \}
.Ve

As of 5.14, that can also be written this way:

.Vb 7
    use v5.14;
    for ($var) \{
        $abc = 1 when /^abc/;
        $def = 1 when /^def/;
        $xyz = 1 when /^xyz/;
        default \{ $nothing = 1 \}
    \}
.Ve

Or if you don't care to play it safe, like this:

.Vb 7
    use v5.14;
    given ($var) \{
        $abc = 1 when /^abc/;
        $def = 1 when /^def/;
        $xyz = 1 when /^xyz/;
        default \{ $nothing = 1 \}
    \}
.Ve

The arguments to \f(CW\*(C`given\*(C' and \f(CW\*(C`when\*(C' are in scalar context,
and \f(CW\*(C`given\*(C' assigns the \f(CW$_ variable its topic value.

Exactly what the *\s-1EXPR\s0* argument to \f(CW\*(C`when\*(C' does is hard to describe
precisely, but in general, it tries to guess what you want done.  Sometimes
it is interpreted as \f(CW\*(C`$_ ~~ \f(CIEXPR\f(CW\*(C', and sometimes it is not.  It
also behaves differently when lexically enclosed by a \f(CW\*(C`given\*(C' block than
it does when dynamically enclosed by a \f(CW\*(C`foreach\*(C' loop.  The rules are far
too difficult to understand to be described here.  See \*(L"Experimental Details
on given and when\*(R" later on.

Due to an unfortunate bug in how \f(CW\*(C`given\*(C' was implemented between Perl 5.10
and 5.16, under those implementations the version of \f(CW$_ governed by
\f(CW\*(C`given\*(C' is merely a lexically scoped copy of the original, not a
dynamically scoped alias to the original, as it would be if it were a
\f(CW\*(C`foreach\*(C' or under both the original and the current Raku language
specification.  This bug was fixed in Perl 5.18 (and lexicalized \f(CW$_ itself
was removed in Perl 5.24).

If your code still needs to run on older versions,
stick to \f(CW\*(C`foreach\*(C' for your topicalizer and
you will be less unhappy.

### Goto

Xref "goto"
Subsection "Goto"
Although not for the faint of heart, Perl does support a \f(CW\*(C`goto\*(C'
statement.  There are three forms: \f(CW\*(C`goto\*(C'-LABEL, \f(CW\*(C`goto\*(C'-EXPR, and
\f(CW\*(C`goto\*(C'-&NAME.  A loop's \s-1LABEL\s0 is not actually a valid target for
a \f(CW\*(C`goto\*(C'; it's just the name of the loop.

The \f(CW\*(C`goto\*(C'-LABEL form finds the statement labeled with \s-1LABEL\s0 and resumes
execution there.  It may not be used to go into any construct that
requires initialization, such as a subroutine or a \f(CW\*(C`foreach\*(C' loop.  It
also can't be used to go into a construct that is optimized away.  It
can be used to go almost anywhere else within the dynamic scope,
including out of subroutines, but it's usually better to use some other
construct such as \f(CW\*(C`last\*(C' or \f(CW\*(C`die\*(C'.  The author of Perl has never felt the
need to use this form of \f(CW\*(C`goto\*(C' (in Perl, that is\*(--C is another matter).

The \f(CW\*(C`goto\*(C'-EXPR form expects a label name, whose scope will be resolved
dynamically.  This allows for computed \f(CW\*(C`goto\*(C's per \s-1FORTRAN,\s0 but isn't
necessarily recommended if you're optimizing for maintainability:

.Vb 1
    goto(("FOO", "BAR", "GLARCH")[$i]);
.Ve

The \f(CW\*(C`goto\*(C'-&NAME form is highly magical, and substitutes a call to the
named subroutine for the currently running subroutine.  This is used by
\f(CW\*(C`AUTOLOAD()\*(C' subroutines that wish to load another subroutine and then
pretend that the other subroutine had been called in the first place
(except that any modifications to \f(CW@_ in the current subroutine are
propagated to the other subroutine.)  After the \f(CW\*(C`goto\*(C', not even \f(CW\*(C`caller()\*(C'
will be able to tell that this routine was called first.

In almost all cases like this, it's usually a far, far better idea to use the
structured control flow mechanisms of \f(CW\*(C`next\*(C', \f(CW\*(C`last\*(C', or \f(CW\*(C`redo\*(C' instead of
resorting to a \f(CW\*(C`goto\*(C'.  For certain applications, the catch and throw pair of
\f(CW\*(C`eval\{\}\*(C' and **die()** for exception processing can also be a prudent approach.

### The Ellipsis Statement

Xref "... ... statement ellipsis operator elliptical statement unimplemented statement unimplemented operator yada-yada yada-yada operator ... operator whatever operator triple-dot operator"
Subsection "The Ellipsis Statement"
Beginning in Perl 5.12, Perl accepts an ellipsis, "\f(CW\*(C`...\*(C'", as a
placeholder for code that you haven't implemented yet.
When Perl 5.12 or later encounters an ellipsis statement, it parses this
without error, but if and when you should actually try to execute it, Perl
throws an exception with the text \f(CW\*(C`Unimplemented\*(C':

.Vb 6
    use v5.12;
    sub unimplemented \{ ... \}
    eval \{ unimplemented() \};
    if ($@ =~ /^Unimplemented at /) \{
        say "I found an ellipsis!";
    \}
.Ve

You can only use the elliptical statement to stand in for a complete
statement.  Syntactically, "\f(CW\*(C`...;\*(C'\*(L" is a complete statement, but,
as with other kinds of semicolon-terminated statement, the semicolon
may be omitted if \*(R"\f(CW\*(C`...\*(C'" appears immediately before a closing brace.
These examples show how the ellipsis works:

.Vb 10
    use v5.12;
    \{ ... \}
    sub foo \{ ... \}
    ...;
    eval \{ ... \};
    sub somemeth \{
        my $self = shift;
        ...;
    \}
    $x = do \{
        my $n;
        ...;
        say "Hurrah!";
        $n;
    \};
.Ve

The elliptical statement cannot stand in for an expression that
is part of a larger statement.
These examples of attempts to use an ellipsis are syntax errors:

.Vb 1
    use v5.12;

    print ...;
    open(my $fh, ">", "/dev/passwd") or ...;
    if ($condition && ... ) \{ say "Howdy" \};
    ... if $a > $b;
    say "Cromulent" if ...;
    $flub = 5 + ...;
.Ve

There are some cases where Perl can't immediately tell the difference
between an expression and a statement.  For instance, the syntax for a
block and an anonymous hash reference constructor look the same unless
there's something in the braces to give Perl a hint.  The ellipsis is a
syntax error if Perl doesn't guess that the \f(CW\*(C`\{ ... \}\*(C' is a block.
Inside your block, you can use a \f(CW\*(C`;\*(C' before the ellipsis to denote that the
\f(CW\*(C`\{ ... \}\*(C' is a block and not a hash reference constructor.

Note: Some folks colloquially refer to this bit of punctuation as a
\*(L"yada-yada\*(R" or \*(L"triple-dot\*(R", but its true name
is actually an ellipsis.

### PODs: Embedded Documentation

Xref "POD documentation"
Subsection "PODs: Embedded Documentation"
Perl has a mechanism for intermixing documentation with source code.
While it's expecting the beginning of a new statement, if the compiler
encounters a line that begins with an equal sign and a word, like this

.Vb 1
    =head1 Here There Be Pods!
.Ve

Then that text and all remaining text up through and including a line
beginning with \f(CW\*(C`=cut\*(C' will be ignored.  The format of the intervening
text is described in perlpod.

This allows you to intermix your source code
and your documentation text freely, as in

.Vb 1
    =item snazzle($)

    The snazzle() function will behave in the most spectacular
    form that you can possibly imagine, not even excepting
    cybernetic pyrotechnics.

    =cut back to the compiler, nuff of this pod stuff!

    sub snazzle($) \{
        my $thingie = shift;
        .........
    \}
.Ve

Note that pod translators should look at only paragraphs beginning
with a pod directive (it makes parsing easier), whereas the compiler
actually knows to look for pod escapes even in the middle of a
paragraph.  This means that the following secret stuff will be
ignored by both the compiler and the translators.

.Vb 5
    $a=3;
    =secret stuff
     warn "Neither POD nor CODE!?"
    =cut back
    print "got $a\\n";
.Ve

You probably shouldn't rely upon the \f(CW\*(C`warn()\*(C' being podded out forever.
Not all pod translators are well-behaved in this regard, and perhaps
the compiler will become pickier.

One may also use pod directives to quickly comment out a section
of code.

### Plain Old Comments (Not!)

Xref "comment line # preprocessor eval"
Subsection "Plain Old Comments (Not!)"
Perl can process line directives, much like the C preprocessor.  Using
this, one can control Perl's idea of filenames and line numbers in
error or warning messages (especially for strings that are processed
with \f(CW\*(C`eval()\*(C').  The syntax for this mechanism is almost the same as for
most C preprocessors: it matches the regular expression

.Vb 5
    # example: \*(Aq# line 42 "new_filename.plx"\*(Aq
    /^\\#   \\s*
      line \\s+ (\\d+)   \\s*
      (?:\\s("?)([^"]+)\\g2)? \\s*
     $/x
.Ve

with \f(CW$1 being the line number for the next line, and \f(CW$3 being
the optional filename (specified with or without quotes).  Note that
no whitespace may precede the \f(CW\*(C`#\*(C', unlike modern C preprocessors.

There is a fairly obvious gotcha included with the line directive:
Debuggers and profilers will only show the last source line to appear
at a particular line number in a given file.  Care should be taken not
to cause line number collisions in code you'd like to debug later.

Here are some examples that you should be able to type into your command
shell:

.Vb 6
    % perl
    # line 200 "bzzzt"
    # the \*(Aq#\*(Aq on the previous line must be the first char on line
    die \*(Aqfoo\*(Aq;
    _\|_END_\|_
    foo at bzzzt line 201.

    % perl
    # line 200 "bzzzt"
    eval qq[\\n#line 2001 ""\\ndie \*(Aqfoo\*(Aq]; print $@;
    _\|_END_\|_
    foo at - line 2001.

    % perl
    eval qq[\\n#line 200 "foo bar"\\ndie \*(Aqfoo\*(Aq]; print $@;
    _\|_END_\|_
    foo at foo bar line 200.

    % perl
    # line 345 "goop"
    eval "\\n#line " . _\|_LINE_\|_ . \*(Aq "\*(Aq . _\|_FILE_\|_ ."\\"\\ndie \*(Aqfoo\*(Aq";
    print $@;
    _\|_END_\|_
    foo at goop line 345.
.Ve

### Experimental Details on given and when

Subsection "Experimental Details on given and when"
As previously mentioned, the \*(L"switch\*(R" feature is considered highly
experimental; it is subject to change with little notice.  In particular,
\f(CW\*(C`when\*(C' has tricky behaviours that are expected to change to become less
tricky in the future.  Do not rely upon its current (mis)implementation.
Before Perl 5.18, \f(CW\*(C`given\*(C' also had tricky behaviours that you should still
beware of if your code must run on older versions of Perl.

Here is a longer example of \f(CW\*(C`given\*(C':

.Vb 10
    use feature ":5.10";
    given ($foo) \{
        when (undef) \{
            say \*(Aq$foo is undefined\*(Aq;
        \}
        when ("foo") \{
            say \*(Aq$foo is the string "foo"\*(Aq;
        \}
        when ([1,3,5,7,9]) \{
            say \*(Aq$foo is an odd digit\*(Aq;
            continue; # Fall through
        \}
        when ($_ < 100) \{
            say \*(Aq$foo is numerically less than 100\*(Aq;
        \}
        when (\complicated_check) \{
            say \*(Aqa complicated check for $foo is true\*(Aq;
        \}
        default \{
            die q(I don\*(Aqt know what to do with $foo);
        \}
    \}
.Ve

Before Perl 5.18, \f(CW\*(C`given(EXPR)\*(C' assigned the value of *\s-1EXPR\s0* to
merely a lexically scoped *\f(BIcopy** (!) of \f(CW$_*, not a dynamically
scoped alias the way \f(CW\*(C`foreach\*(C' does.  That made it similar to

.Vb 1
        do \{ my $_ = EXPR; ... \}
.Ve

except that the block was automatically broken out of by a successful
\f(CW\*(C`when\*(C' or an explicit \f(CW\*(C`break\*(C'.  Because it was only a copy, and because
it was only lexically scoped, not dynamically scoped, you could not do the
things with it that you are used to in a \f(CW\*(C`foreach\*(C' loop.  In particular,
it did not work for arbitrary function calls if those functions might try
to access \f(CW$_.  Best stick to \f(CW\*(C`foreach\*(C' for that.

Most of the power comes from the implicit smartmatching that can
sometimes apply.  Most of the time, \f(CW\*(C`when(EXPR)\*(C' is treated as an
implicit smartmatch of \f(CW$_, that is, \f(CW\*(C`$_ ~~ EXPR\*(C'.  (See
\*(L"Smartmatch Operator\*(R" in perlop for more information on smartmatching.)
But when *\s-1EXPR\s0* is one of the 10 exceptional cases (or things like them)
listed below, it is used directly as a boolean.

- 1.
Item "1."
A user-defined subroutine call or a method invocation.

- 2.
Item "2."
A regular expression match in the form of \f(CW\*(C`/REGEX/\*(C', \f(CW\*(C`$foo =~ /REGEX/\*(C',
or \f(CW\*(C`$foo =~ EXPR\*(C'.  Also, a negated regular expression match in
the form \f(CW\*(C`!/REGEX/\*(C', \f(CW\*(C`$foo !~ /REGEX/\*(C', or \f(CW\*(C`$foo !~ EXPR\*(C'.

- 3.
Item "3."
A smart match that uses an explicit \f(CW\*(C`~~\*(C' operator, such as \f(CW\*(C`EXPR ~~ EXPR\*(C'.
.Sp
**\s-1NOTE:\s0** You will often have to use \f(CW\*(C`$c ~~ $_\*(C' because the default case
uses \f(CW\*(C`$_ ~~ $c\*(C' , which is frequently the opposite of what you want.

- 4.
Item "4."
A boolean comparison operator such as \f(CW\*(C`$_ < 10\*(C' or \f(CW\*(C`$x eq "abc"\*(C'.  The
relational operators that this applies to are the six numeric comparisons
(\f(CW\*(C`<\*(C', \f(CW\*(C`>\*(C', \f(CW\*(C`<=\*(C', \f(CW\*(C`>=\*(C', \f(CW\*(C`==\*(C', and \f(CW\*(C`!=\*(C'), and
the six string comparisons (\f(CW\*(C`lt\*(C', \f(CW\*(C`gt\*(C', \f(CW\*(C`le\*(C', \f(CW\*(C`ge\*(C', \f(CW\*(C`eq\*(C', and \f(CW\*(C`ne\*(C').

- 5.
Item "5."
At least the three builtin functions \f(CW\*(C`defined(...)\*(C', \f(CW\*(C`exists(...)\*(C', and
\f(CW\*(C`eof(...)\*(C'.  We might someday add more of these later if we think of them.

- 6.
Item "6."
A negated expression, whether \f(CW\*(C`!(EXPR)\*(C' or \f(CW\*(C`not(EXPR)\*(C', or a logical
exclusive-or, \f(CW\*(C`(EXPR1) xor (EXPR2)\*(C'.  The bitwise versions (\f(CW\*(C`~\*(C' and \f(CW\*(C`^\*(C')
are not included.

- 7.
Item "7."
A filetest operator, with exactly 4 exceptions: \f(CW\*(C`-s\*(C', \f(CW\*(C`-M\*(C', \f(CW\*(C`-A\*(C', and
\f(CW\*(C`-C\*(C', as these return numerical values, not boolean ones.  The \f(CW\*(C`-z\*(C'
filetest operator is not included in the exception list.

- 8.
Item "8."
The \f(CW\*(C`..\*(C' and \f(CW\*(C`...\*(C' flip-flop operators.  Note that the \f(CW\*(C`...\*(C' flip-flop
operator is completely different from the \f(CW\*(C`...\*(C' elliptical statement
just described.

In those 8 cases above, the value of \s-1EXPR\s0 is used directly as a boolean, so
no smartmatching is done.  You may think of \f(CW\*(C`when\*(C' as a smartsmartmatch.

Furthermore, Perl inspects the operands of logical operators to
decide whether to use smartmatching for each one by applying the
above test to the operands:

- 9.
Item "9."
If \s-1EXPR\s0 is \f(CW\*(C`EXPR1 && EXPR2\*(C' or \f(CW\*(C`EXPR1 and EXPR2\*(C', the test is applied
*recursively* to both \s-1EXPR1\s0 and \s-1EXPR2.\s0
Only if *both* operands also pass the
test, *recursively*, will the expression be treated as boolean.  Otherwise,
smartmatching is used.

- 10.
Item "10."
If \s-1EXPR\s0 is \f(CW\*(C`EXPR1 || EXPR2\*(C', \f(CW\*(C`EXPR1 // EXPR2\*(C', or \f(CW\*(C`EXPR1 or EXPR2\*(C', the
test is applied *recursively* to \s-1EXPR1\s0 only (which might itself be a
higher-precedence \s-1AND\s0 operator, for example, and thus subject to the
previous rule), not to \s-1EXPR2.\s0  If \s-1EXPR1\s0 is to use smartmatching, then \s-1EXPR2\s0
also does so, no matter what \s-1EXPR2\s0 contains.  But if \s-1EXPR2\s0 does not get to
use smartmatching, then the second argument will not be either.  This is
quite different from the \f(CW\*(C`&&\*(C' case just described, so be careful.

These rules are complicated, but the goal is for them to do what you want
(even if you don't quite understand why they are doing it).  For example:

.Vb 1
    when (/^\\d+$/ && $_ < 75) \{ ... \}
.Ve

will be treated as a boolean match because the rules say both
a regex match and an explicit test on \f(CW$_ will be treated
as boolean.

Also:

.Vb 1
    when ([qw(foo bar)] && /baz/) \{ ... \}
.Ve

will use smartmatching because only *one* of the operands is a boolean:
the other uses smartmatching, and that wins.

Further:

.Vb 1
    when ([qw(foo bar)] || /^baz/) \{ ... \}
.Ve

will use smart matching (only the first operand is considered), whereas

.Vb 1
    when (/^baz/ || [qw(foo bar)]) \{ ... \}
.Ve

will test only the regex, which causes both operands to be
treated as boolean.  Watch out for this one, then, because an
arrayref is always a true value, which makes it effectively
redundant.  Not a good idea.

Tautologous boolean operators are still going to be optimized
away.  Don't be tempted to write

.Vb 1
    when ("foo" or "bar") \{ ... \}
.Ve

This will optimize down to \f(CW"foo", so \f(CW"bar" will never be considered (even
though the rules say to use a smartmatch
on \f(CW"foo").  For an alternation like
this, an array ref will work, because this will instigate smartmatching:

.Vb 1
    when ([qw(foo bar)] \{ ... \}
.Ve

This is somewhat equivalent to the C-style switch statement's fallthrough
functionality (not to be confused with *Perl's* fallthrough
functionality\*(--see below), wherein the same block is used for several
\f(CW\*(C`case\*(C' statements.

Another useful shortcut is that, if you use a literal array or hash as the
argument to \f(CW\*(C`given\*(C', it is turned into a reference.  So \f(CW\*(C`given(@foo)\*(C' is
the same as \f(CW\*(C`given(\\@foo)\*(C', for example.

\f(CW\*(C`default\*(C' behaves exactly like \f(CW\*(C`when(1 == 1)\*(C', which is
to say that it always matches.

*Breaking out*
Subsection "Breaking out"

You can use the \f(CW\*(C`break\*(C' keyword to break out of the enclosing
\f(CW\*(C`given\*(C' block.  Every \f(CW\*(C`when\*(C' block is implicitly ended with
a \f(CW\*(C`break\*(C'.

*Fall-through*
Subsection "Fall-through"

You can use the \f(CW\*(C`continue\*(C' keyword to fall through from one
case to the next immediate \f(CW\*(C`when\*(C' or \f(CW\*(C`default\*(C':

.Vb 5
    given($foo) \{
        when (/x/) \{ say \*(Aq$foo contains an x\*(Aq; continue \}
        when (/y/) \{ say \*(Aq$foo contains a y\*(Aq            \}
        default    \{ say \*(Aq$foo does not contain a y\*(Aq    \}
    \}
.Ve

*Return value*
Subsection "Return value"

When a \f(CW\*(C`given\*(C' statement is also a valid expression (for example,
when it's the last statement of a block), it evaluates to:

- \(bu
An empty list as soon as an explicit \f(CW\*(C`break\*(C' is encountered.

- \(bu
The value of the last evaluated expression of the successful
\f(CW\*(C`when\*(C'/\f(CW\*(C`default\*(C' clause, if there happens to be one.

- \(bu
The value of the last evaluated expression of the \f(CW\*(C`given\*(C' block if no
condition is true.

In both last cases, the last expression is evaluated in the context that
was applied to the \f(CW\*(C`given\*(C' block.

Note that, unlike \f(CW\*(C`if\*(C' and \f(CW\*(C`unless\*(C', failed \f(CW\*(C`when\*(C' statements always
evaluate to an empty list.

.Vb 8
    my $price = do \{
        given ($item) \{
            when (["pear", "apple"]) \{ 1 \}
            break when "vote";      # My vote cannot be bought
            1e10  when /Mona Lisa/;
            "unknown";
        \}
    \};
.Ve

Currently, \f(CW\*(C`given\*(C' blocks can't always
be used as proper expressions.  This
may be addressed in a future version of Perl.

*Switching in a loop*
Subsection "Switching in a loop"

Instead of using \f(CW\*(C`given()\*(C', you can use a \f(CW\*(C`foreach()\*(C' loop.
For example, here's one way to count how many times a particular
string occurs in an array:

.Vb 6
    use v5.10.1;
    my $count = 0;
    for (@array) \{
        when ("foo") \{ ++$count \}
    \}
    print "\\@array contains $count copies of \*(Aqfoo\*(Aq\\n";
.Ve

Or in a more recent version:

.Vb 6
    use v5.14;
    my $count = 0;
    for (@array) \{
        ++$count when "foo";
    \}
    print "\\@array contains $count copies of \*(Aqfoo\*(Aq\\n";
.Ve

At the end of all \f(CW\*(C`when\*(C' blocks, there is an implicit \f(CW\*(C`next\*(C'.
You can override that with an explicit \f(CW\*(C`last\*(C' if you're
interested in only the first match alone.

This doesn't work if you explicitly specify a loop variable, as
in \f(CW\*(C`for $item (@array)\*(C'.  You have to use the default variable \f(CW$_.

*Differences from Raku*
Subsection "Differences from Raku"

The Perl 5 smartmatch and \f(CW\*(C`given\*(C'/\f(CW\*(C`when\*(C' constructs are not compatible
with their Raku analogues.  The most visible difference and least
important difference is that, in Perl 5, parentheses are required around
the argument to \f(CW\*(C`given()\*(C' and \f(CW\*(C`when()\*(C' (except when this last one is used
as a statement modifier).  Parentheses in Raku are always optional in a
control construct such as \f(CW\*(C`if()\*(C', \f(CW\*(C`while()\*(C', or \f(CW\*(C`when()\*(C'; they can't be
made optional in Perl 5 without a great deal of potential confusion,
because Perl 5 would parse the expression

.Vb 3
    given $foo \{
        ...
    \}
.Ve

as though the argument to \f(CW\*(C`given\*(C' were an element of the hash
\f(CW%foo, interpreting the braces as hash-element syntax.

However, their are many, many other differences.  For example,
this works in Perl 5:

.Vb 2
    use v5.12;
    my @primary = ("red", "blue", "green");

    if (@primary ~~ "red") \{
        say "primary smartmatches red";
    \}

    if ("red" ~~ @primary) \{
        say "red smartmatches primary";
    \}

    say "that\*(Aqs all, folks!";
.Ve

But it doesn't work at all in Raku.  Instead, you should
use the (parallelizable) \f(CW\*(C`any\*(C' operator:

.Vb 3
   if any(@primary) eq "red" \{
       say "primary smartmatches red";
   \}

   if "red" eq any(@primary) \{
       say "red smartmatches primary";
   \}
.Ve

The table of smartmatches in \*(L"Smartmatch Operator\*(R" in perlop is not
identical to that proposed by the Raku specification, mainly due to
differences between Raku's and Perl 5's data models, but also because
the Raku spec has changed since Perl 5 rushed into early adoption.

In Raku, \f(CW\*(C`when()\*(C' will always do an implicit smartmatch with its
argument, while in Perl 5 it is convenient (albeit potentially confusing) to
suppress this implicit smartmatch in various rather loosely-defined
situations, as roughly outlined above.  (The difference is largely because
Perl 5 does not have, even internally, a boolean type.)
