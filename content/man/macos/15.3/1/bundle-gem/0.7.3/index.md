+++
detected_package_version = "0.7.3"
date = "Sun Feb 16 04:48:21 2025"
description = "Generates a directory named GEM_NAME with a Rakefile, GEM_NAME.gemspec, and other supporting files and directories that can be used to develop a rubygem with that name. Run rake -T in the resulting project for a list of Rake tasks that can be used to..."
author = "None Specified"
title = "bundle-gem(1)"
manpage_name = "bundle-gem"
manpage_section = "1"
manpage_format = "troff"
operating_system_version = "15.3"
operating_system = "macos"
keywords = ["bu", "4", "bundle", "config", "fibundle-config", "1", "html"]
+++

.
"BUNDLE-GEM" "1" "November 2018" "" ""
.

## NAME

**bundle-gem** - Generate a project skeleton for creating a rubygem
.

## SYNOPSIS

**bundle gem** *GEM_NAME* *OPTIONS*
.

## DESCRIPTION

Generates a directory named **GEM_NAME** with a **Rakefile**, **GEM_NAME\.gemspec**, and other supporting files and directories that can be used to develop a rubygem with that name\.
.
.P
Run **rake -T** in the resulting project for a list of Rake tasks that can be used to test and publish the gem to rubygems\.org\.
.
.P
The generated project skeleton can be customized with OPTIONS, as explained below\. Note that these options can also be specified via Bundler\'s global configuration file using the following names:
.

- \(bu
**gem\.coc**
.

- \(bu
**gem\.mit**
.

- \(bu
**gem\.test**
.
"" 0
.

## OPTIONS

.

**--exe** or **-b** or **--bin**
Specify that Bundler should create a binary executable (as **exe/GEM_NAME**) in the generated rubygem project\. This binary will also be added to the **GEM_NAME\.gemspec** manifest\. This behavior is disabled by default\.
.

**--no-exe**
Do not create a binary (overrides **--exe** specified in the global config)\.
.

**--coc**
Add a **CODE_OF_CONDUCT\.md** file to the root of the generated project\. If this option is unspecified, an interactive prompt will be displayed and the answer will be saved in Bundler\'s global config for future **bundle gem** use\.
.

**--no-coc**
Do not create a **CODE_OF_CONDUCT\.md** (overrides **--coc** specified in the global config)\.
.

**--ext**
Add boilerplate for C extension code to the generated project\. This behavior is disabled by default\.
.

**--no-ext**
Do not add C extension code (overrides **--ext** specified in the global config)\.
.

**--mit**
Add an MIT license to a **LICENSE\.txt** file in the root of the generated project\. Your name from the global git config is used for the copyright statement\. If this option is unspecified, an interactive prompt will be displayed and the answer will be saved in Bundler\'s global config for future **bundle gem** use\.
.

**--no-mit**
Do not create a **LICENSE\.txt** (overrides **--mit** specified in the global config)\.
.

**-t**, **--test=minitest**, **--test=rspec**
Specify the test framework that Bundler should use when generating the project\. Acceptable values are **minitest** and **rspec**\. The **GEM_NAME\.gemspec** will be configured and a skeleton test/spec directory will be created based on this option\. If this option is unspecified, an interactive prompt will be displayed and the answer will be saved in Bundler\'s global config for future **bundle gem** use\. If no option is specified, the default testing framework is RSpec\.
.

**-e**, **--edit[=EDITOR]**
Open the resulting GEM_NAME\.gemspec in EDITOR, or the default editor if not specified\. The default is **$BUNDLER_EDITOR**, **$VISUAL**, or **$EDITOR**\.
.

## SEE ALSO

.

- \(bu
bundle config(1) *bundle-config\.1\.html*
.
"" 0

