+++
manpage_section = "1"
manpage_name = "perlmacos"
manpage_format = "troff"
title = "perlmacos(1)"
operating_system = "macos"
author = "None Specified"
date = "2022-02-19"
description = "The port of Perl to Mac s-1OSs0 was officially removed as of Perl 5.12, though the last official production release of MacPerl corresponded to  Perl 5.6. While Perl 5.10 included the port to Mac s-1OS,s0 ExtUtils::MakeMaker, a core part of Perls m..."
detected_package_version = "5.34.1"
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLMACOS 1"
PERLMACOS 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlmacos - Perl under Mac OS (Classic)

## SYNOPSIS

Header "SYNOPSIS"
For Mac \s-1OS X\s0 see \s-1README\s0.macosx

Perl under Mac \s-1OS\s0 Classic has not been supported since before Perl 5.10
(April 2004).

When we say \*(L"Mac \s-1OS\*(R"\s0 below, we mean Mac \s-1OS 7, 8,\s0 and 9, and *not*
Mac \s-1OS X.\s0

## DESCRIPTION

Header "DESCRIPTION"
The port of Perl to Mac \s-1OS\s0 was officially removed as of Perl 5.12,
though the last official production release of MacPerl corresponded to
Perl 5.6. While Perl 5.10 included the port to Mac \s-1OS,\s0 ExtUtils::MakeMaker,
a core part of Perl's module installation infrastructure officially dropped support for Mac \s-1OS\s0 in April 2004.

## AUTHOR

Header "AUTHOR"
Perl was ported to Mac \s-1OS\s0 by Matthias Neeracher
<neeracher@mac.com>. Chris Nandor <pudge@pobox.com>
continued development and maintenance for the duration of the port's life.
