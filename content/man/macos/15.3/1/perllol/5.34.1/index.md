+++
manpage_format = "troff"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "perldata", "perlref", "perldsc", "author", "tom", "christiansen", "fitchrist", "perl", "com", "last", "update", "tue", "apr", "26", "18", "30", "55", "s-1mdt", "2011", "s0"]
operating_system = "macos"
description = "The simplest two-level data structure to build in Perl is an array of arrays, sometimes casually called a list of lists.  Its reasonably easy to understand, and almost everything that applies here will also be applicable later on with the fancier ..."
author = "None Specified"
date = "2022-02-19"
manpage_section = "1"
manpage_name = "perllol"
title = "perllol(1)"
operating_system_version = "15.3"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLLOL 1"
PERLLOL 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perllol - Manipulating Arrays of Arrays in Perl

## DESCRIPTION

Header "DESCRIPTION"

### Declaration and Access of Arrays of Arrays

Subsection "Declaration and Access of Arrays of Arrays"
The simplest two-level data structure to build in Perl is an array of
arrays, sometimes casually called a list of lists.  It's reasonably easy to
understand, and almost everything that applies here will also be applicable
later on with the fancier data structures.

An array of an array is just a regular old array \f(CW@AoA that you can
get at with two subscripts, like \f(CW$AoA[3][2].  Here's a declaration
of the array:

.Vb 1
    use 5.010;  # so we can use say()

    # assign to our array, an array of array references
    @AoA = (
           [ "fred", "barney", "pebbles", "bambam", "dino", ],
           [ "george", "jane", "elroy", "judy", ],
           [ "homer", "bart", "marge", "maggie", ],
    );
    say $AoA[2][1];
  bart
.Ve

Now you should be very careful that the outer bracket type
is a round one, that is, a parenthesis.  That's because you're assigning to
an \f(CW@array, so you need parentheses.  If you wanted there *not* to be an \f(CW@AoA,
but rather just a reference to it, you could do something more like this:

.Vb 8
    # assign a reference to array of array references
    $ref_to_AoA = [
        [ "fred", "barney", "pebbles", "bambam", "dino", ],
        [ "george", "jane", "elroy", "judy", ],
        [ "homer", "bart", "marge", "maggie", ],
    ];
    say $ref_to_AoA->[2][1];
  bart
.Ve

Notice that the outer bracket type has changed, and so our access syntax
has also changed.  That's because unlike C, in perl you can't freely
interchange arrays and references thereto.  \f(CW$ref_to_AoA is a reference to an
array, whereas \f(CW@AoA is an array proper.  Likewise, \f(CW$AoA[2] is not an
array, but an array ref.  So how come you can write these:

.Vb 2
    $AoA[2][2]
    $ref_to_AoA->[2][2]
.Ve

instead of having to write these:

.Vb 2
    $AoA[2]->[2]
    $ref_to_AoA->[2]->[2]
.Ve

Well, that's because the rule is that on adjacent brackets only (whether
square or curly), you are free to omit the pointer dereferencing arrow.
But you cannot do so for the very first one if it's a scalar containing
a reference, which means that \f(CW$ref_to_AoA always needs it.

### Growing Your Own

Subsection "Growing Your Own"
That's all well and good for declaration of a fixed data structure,
but what if you wanted to add new elements on the fly, or build
it up entirely from scratch?

First, let's look at reading it in from a file.  This is something like
adding a row at a time.  We'll assume that there's a flat file in which
each line is a row and each word an element.  If you're trying to develop an
\f(CW@AoA array containing all these, here's the right way to do that:

.Vb 4
    while (<>) \{
        @tmp = split;
        push @AoA, [ @tmp ];
    \}
.Ve

You might also have loaded that from a function:

.Vb 3
    for $i ( 1 .. 10 ) \{
        $AoA[$i] = [ somefunc($i) ];
    \}
.Ve

Or you might have had a temporary variable sitting around with the
array in it.

.Vb 4
    for $i ( 1 .. 10 ) \{
        @tmp = somefunc($i);
        $AoA[$i] = [ @tmp ];
    \}
.Ve

It's important you make sure to use the \f(CW\*(C`[ ]\*(C' array reference
constructor.  That's because this wouldn't work:

.Vb 1
    $AoA[$i] = @tmp;   # WRONG!
.Ve

The reason that doesn't do what you want is because assigning a
named array like that to a scalar is taking an array in scalar
context, which means just counts the number of elements in \f(CW@tmp.

If you are running under \f(CW\*(C`use strict\*(C' (and if you aren't, why in
the world aren't you?), you'll have to add some declarations to
make it happy:

.Vb 6
    use strict;
    my(@AoA, @tmp);
    while (<>) \{
        @tmp = split;
        push @AoA, [ @tmp ];
    \}
.Ve

Of course, you don't need the temporary array to have a name at all:

.Vb 3
    while (<>) \{
        push @AoA, [ split ];
    \}
.Ve

You also don't have to use **push()**.  You could just make a direct assignment
if you knew where you wanted to put it:

.Vb 5
    my (@AoA, $i, $line);
    for $i ( 0 .. 10 ) \{
        $line = <>;
        $AoA[$i] = [ split " ", $line ];
    \}
.Ve

or even just

.Vb 4
    my (@AoA, $i);
    for $i ( 0 .. 10 ) \{
        $AoA[$i] = [ split " ", <> ];
    \}
.Ve

You should in general be leery of using functions that could
potentially return lists in scalar context without explicitly stating
such.  This would be clearer to the casual reader:

.Vb 4
    my (@AoA, $i);
    for $i ( 0 .. 10 ) \{
        $AoA[$i] = [ split " ", scalar(<>) ];
    \}
.Ve

If you wanted to have a \f(CW$ref_to_AoA variable as a reference to an array,
you'd have to do something like this:

.Vb 3
    while (<>) \{
        push @$ref_to_AoA, [ split ];
    \}
.Ve

Now you can add new rows.  What about adding new columns?  If you're
dealing with just matrices, it's often easiest to use simple assignment:

.Vb 5
    for $x (1 .. 10) \{
        for $y (1 .. 10) \{
            $AoA[$x][$y] = func($x, $y);
        \}
    \}

    for $x ( 3, 7, 9 ) \{
        $AoA[$x][20] += func2($x);
    \}
.Ve

It doesn't matter whether those elements are already
there or not: it'll gladly create them for you, setting
intervening elements to \f(CW\*(C`undef\*(C' as need be.

If you wanted just to append to a row, you'd have
to do something a bit funnier looking:

.Vb 2
    # add new columns to an existing row
    push $AoA[0]->@*, "wilma", "betty";   # explicit deref
.Ve

### Access and Printing

Subsection "Access and Printing"
Now it's time to print your data structure out.  How
are you going to do that?  Well, if you want only one
of the elements, it's trivial:

.Vb 1
    print $AoA[0][0];
.Ve

If you want to print the whole thing, though, you can't
say

.Vb 1
    print @AoA;         # WRONG
.Ve

because you'll get just references listed, and perl will never
automatically dereference things for you.  Instead, you have to
roll yourself a loop or two.  This prints the whole structure,
using the shell-style **for()** construct to loop across the outer
set of subscripts.

.Vb 3
    for $aref ( @AoA ) \{
        say "\\t [ @$aref ],";
    \}
.Ve

If you wanted to keep track of subscripts, you might do this:

.Vb 3
    for $i ( 0 .. $#AoA ) \{
        say "\\t elt $i is [ @\{$AoA[$i]\} ],";
    \}
.Ve

or maybe even this.  Notice the inner loop.

.Vb 5
    for $i ( 0 .. $#AoA ) \{
        for $j ( 0 .. $#\{$AoA[$i]\} ) \{
            say "elt $i $j is $AoA[$i][$j]";
        \}
    \}
.Ve

As you can see, it's getting a bit complicated.  That's why
sometimes is easier to take a temporary on your way through:

.Vb 6
    for $i ( 0 .. $#AoA ) \{
        $aref = $AoA[$i];
        for $j ( 0 .. $#\{$aref\} ) \{
            say "elt $i $j is $AoA[$i][$j]";
        \}
    \}
.Ve

Hmm... that's still a bit ugly.  How about this:

.Vb 7
    for $i ( 0 .. $#AoA ) \{
        $aref = $AoA[$i];
        $n = @$aref - 1;
        for $j ( 0 .. $n ) \{
            say "elt $i $j is $AoA[$i][$j]";
        \}
    \}
.Ve

When you get tired of writing a custom print for your data structures,
you might look at the standard Dumpvalue or Data::Dumper modules.
The former is what the Perl debugger uses, while the latter generates
parsable Perl code.  For example:

.Vb 1
 use v5.14;     # using the + prototype, new to v5.14

 sub show(+) \{
        require Dumpvalue;
        state $prettily = new Dumpvalue::
                            tick        => q("),
                            compactDump => 1,  # comment these two lines
                                               # out
                            veryCompact => 1,  # if you want a bigger
                                               # dump
                        ;
        dumpValue $prettily @_;
 \}

 # Assign a list of array references to an array.
 my @AoA = (
           [ "fred", "barney" ],
           [ "george", "jane", "elroy" ],
           [ "homer", "marge", "bart" ],
 );
 push $AoA[0]->@*, "wilma", "betty";
 show @AoA;
.Ve

will print out:

.Vb 3
    0  0..3  "fred" "barney" "wilma" "betty"
    1  0..2  "george" "jane" "elroy"
    2  0..2  "homer" "marge" "bart"
.Ve

Whereas if you comment out the two lines I said you might wish to,
then it shows it to you this way instead:

.Vb 10
    0  ARRAY(0x8031d0)
       0  "fred"
       1  "barney"
       2  "wilma"
       3  "betty"
    1  ARRAY(0x803d40)
       0  "george"
       1  "jane"
       2  "elroy"
    2  ARRAY(0x803e10)
       0  "homer"
       1  "marge"
       2  "bart"
.Ve

### Slices

Subsection "Slices"
If you want to get at a slice (part of a row) in a multidimensional
array, you're going to have to do some fancy subscripting.  That's
because while we have a nice synonym for single elements via the
pointer arrow for dereferencing, no such convenience exists for slices.

Here's how to do one operation using a loop.  We'll assume an \f(CW@AoA
variable as before.

.Vb 5
    @part = ();
    $x = 4;
    for ($y = 7; $y < 13; $y++) \{
        push @part, $AoA[$x][$y];
    \}
.Ve

That same loop could be replaced with a slice operation:

.Vb 1
    @part = $AoA[4]->@[ 7..12 ];
.Ve

Now, what if you wanted a *two-dimensional slice*, such as having
\f(CW$x run from 4..8 and \f(CW$y run from 7 to 12?  Hmm... here's the simple way:

.Vb 6
    @newAoA = ();
    for ($startx = $x = 4; $x <= 8; $x++) \{
        for ($starty = $y = 7; $y <= 12; $y++) \{
            $newAoA[$x - $startx][$y - $starty] = $AoA[$x][$y];
        \}
    \}
.Ve

We can reduce some of the looping through slices

.Vb 3
    for ($x = 4; $x <= 8; $x++) \{
        push @newAoA, [ $AoA[$x]->@[ 7..12 ] ];
    \}
.Ve

If you were into Schwartzian Transforms, you would probably
have selected map for that

.Vb 1
    @newAoA = map \{ [ $AoA[$_]->@[ 7..12 ] ] \} 4 .. 8;
.Ve

Although if your manager accused you of seeking job security (or rapid
insecurity) through inscrutable code, it would be hard to argue. :-)
If I were you, I'd put that in a function:

.Vb 5
    @newAoA = splice_2D( \\@AoA, 4 => 8, 7 => 12 );
    sub splice_2D \{
        my $lrr = shift;        # ref to array of array refs!
        my ($x_lo, $x_hi,
            $y_lo, $y_hi) = @_;

        return map \{
            [ $lrr->[$_]->@[ $y_lo .. $y_hi ] ]
        \} $x_lo .. $x_hi;
    \}
.Ve

## SEE ALSO

Header "SEE ALSO"
perldata, perlref, perldsc

## AUTHOR

Header "AUTHOR"
Tom Christiansen <*tchrist@perl.com*>

Last update: Tue Apr 26 18:30:55 \s-1MDT 2011\s0
