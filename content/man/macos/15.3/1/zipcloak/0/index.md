+++
manpage_format = "troff"
date = "2008-01-01"
author = "None Specified"
detected_package_version = "0"
manpage_section = "1"
description = "encrypts all unencrypted entries in the zipfile.  This is the default action."
title = "zipcloak(1)"
operating_system = "macos"
operating_system_version = "15.3"
keywords = ["zip", "unzip", "author", "info-zip"]
manpage_name = "zipcloak"
+++

zipcloak 1 "v3.0 of 8 May 2008"

## NAME

zipcloak - encrypt entries in a zipfile


## SYNOPSIS

zipcloak
[ -d ]
[ -b\ path ]
[ -h ]
[ -v ]
[ -L ]
zipfile


## ARGUMENTS

.in +13
.ti -13
zipfile  Zipfile to encrypt entries in


## OPTIONS


0
-b\ path

.PD
--temp-path path
Use the directory given by path for the temporary zip file.


0
-d

.PD
--decrypt
Decrypt encrypted entries (copy if given wrong password).


0
-h

.PD
--help\
Show a short help.


0
-L

.PD
--license
Show software license.


0
-O\ path

.PD
--output-file\ zipfile
Write output to new archive zipfile, leaving original archive as is.


0
-q

.PD
--quiet
Quiet operation.  Suppresses some informational messages.


0
-v

.PD
--version
Show version information.


## DESCRIPTION

zipcloak
encrypts all unencrypted entries in the zipfile.  This is the default action.


The -d option is used to decrypt encrypted entries in the zipfile.


*zipcloak *uses original zip encryption which is considered weak.


Note:
The encryption code of this program is not copyrighted and is put in
the public domain.  It was originally written in Europe and can be freely
distributed from any country including the U.S.A.  (Previously if this
program was imported into the U.S.A, it could not be re-exported from
the U.S.A to another country.)  See the file README.CR included in the
source distribution for more on this.  Otherwise, the Info-ZIP license
applies.


## EXAMPLES

To be added.


## BUGS

Large files (> 2 GB) and large archives not yet supported.

Split archives not yet supported.  A work around is to convert the
split archive to a single-file archive using *zip* and then
use *zipcloak* on the single-file archive.  If needed, the
resulting archive can then be split again using *zip*.



## SEE ALSO

zip(1), unzip(1)

## AUTHOR

Info-ZIP
