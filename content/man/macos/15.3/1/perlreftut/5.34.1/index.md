+++
detected_package_version = "5.34.1"
manpage_format = "troff"
operating_system_version = "15.3"
title = "perlreftut(1)"
description = "One of the most important new features in Perl 5 was the capability to manage complicated data structures like multidimensional arrays and nested hashes.  To enable these, Perl 5 introduced a feature called references, and using references is the k..."
date = "2022-02-19"
manpage_name = "perlreftut"
manpage_section = "1"
operating_system = "macos"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLREFTUT 1"
PERLREFTUT 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlreftut - Mark's very short tutorial about references

## DESCRIPTION

Header "DESCRIPTION"
One of the most important new features in Perl 5 was the capability to
manage complicated data structures like multidimensional arrays and
nested hashes.  To enable these, Perl 5 introduced a feature called
*references*, and using references is the key to managing complicated,
structured data in Perl.  Unfortunately, there's a lot of funny syntax
to learn, and the main manual page can be hard to follow.  The manual
is quite complete, and sometimes people find that a problem, because
it can be hard to tell what is important and what isn't.

Fortunately, you only need to know 10% of what's in the main page to get
90% of the benefit.  This page will show you that 10%.

## Who Needs Complicated Data Structures?

Header "Who Needs Complicated Data Structures?"
One problem that comes up all the time is needing a hash whose values are
lists.  Perl has hashes, of course, but the values have to be scalars;
they can't be lists.

Why would you want a hash of lists?  Let's take a simple example: You
have a file of city and country names, like this:

.Vb 6
        Chicago, USA
        Frankfurt, Germany
        Berlin, Germany
        Washington, USA
        Helsinki, Finland
        New York, USA
.Ve

and you want to produce an output like this, with each country mentioned
once, and then an alphabetical list of the cities in that country:

.Vb 3
        Finland: Helsinki.
        Germany: Berlin, Frankfurt.
        USA:  Chicago, New York, Washington.
.Ve

The natural way to do this is to have a hash whose keys are country
names.  Associated with each country name key is a list of the cities in
that country.  Each time you read a line of input, split it into a country
and a city, look up the list of cities already known to be in that
country, and append the new city to the list.  When you're done reading
the input, iterate over the hash as usual, sorting each list of cities
before you print it out.

If hash values couldn't be lists, you lose.  You'd probably have to
combine all the cities into a single string somehow, and then when
time came to write the output, you'd have to break the string into a
list, sort the list, and turn it back into a string.  This is messy
and error-prone.  And it's frustrating, because Perl already has
perfectly good lists that would solve the problem if only you could
use them.

## The Solution

Header "The Solution"
By the time Perl 5 rolled around, we were already stuck with this
design: Hash values must be scalars.  The solution to this is
references.

A reference is a scalar value that *refers to* an entire array or an
entire hash (or to just about anything else).  Names are one kind of
reference that you're already familiar with.  Each human being is a
messy, inconvenient collection of cells. But to refer to a particular
human, for instance the first computer programmer, it isn't necessary to
describe each of their cells; all you need is the easy, convenient
scalar string \*(L"Ada Lovelace\*(R".

References in Perl are like names for arrays and hashes.  They're
Perl's private, internal names, so you can be sure they're
unambiguous.  Unlike a human name, a reference only refers to one
thing, and you always know what it refers to.  If you have a reference
to an array, you can recover the entire array from it.  If you have a
reference to a hash, you can recover the entire hash.  But the
reference is still an easy, compact scalar value.

You can't have a hash whose values are arrays; hash values can only be
scalars.  We're stuck with that.  But a single reference can refer to
an entire array, and references are scalars, so you can have a hash of
references to arrays, and it'll act a lot like a hash of arrays, and
it'll be just as useful as a hash of arrays.

We'll come back to this city-country problem later, after we've seen
some syntax for managing references.

## Syntax

Header "Syntax"
There are just two ways to make a reference, and just two ways to use
it once you have it.

### Making References

Subsection "Making References"
*\f(BIMake Rule 1\fI*
Subsection "Make Rule 1"

If you put a \f(CW\*(C`\\\*(C' in front of a variable, you get a
reference to that variable.

.Vb 3
    $aref = \\@array;         # $aref now holds a reference to @array
    $href = \\%hash;          # $href now holds a reference to %hash
    $sref = \\$scalar;        # $sref now holds a reference to $scalar
.Ve

Once the reference is stored in a variable like \f(CW$aref or \f(CW$href, you
can copy it or store it just the same as any other scalar value:

.Vb 3
    $xy = $aref;             # $xy now holds a reference to @array
    $p[3] = $href;           # $p[3] now holds a reference to %hash
    $z = $p[3];              # $z now holds a reference to %hash
.Ve

These examples show how to make references to variables with names.
Sometimes you want to make an array or a hash that doesn't have a
name.  This is analogous to the way you like to be able to use the
string \f(CW"\\n" or the number 80 without having to store it in a named
variable first.

*\f(BIMake Rule 2\fI*
Subsection "Make Rule 2"

\f(CW\*(C`[ ITEMS ]\*(C' makes a new, anonymous array, and returns a reference to
that array.  \f(CW\*(C`\{ ITEMS \}\*(C' makes a new, anonymous hash, and returns a
reference to that hash.

.Vb 2
    $aref = [ 1, "foo", undef, 13 ];
    # $aref now holds a reference to an array

    $href = \{ APR => 4, AUG => 8 \};
    # $href now holds a reference to a hash
.Ve

The references you get from rule 2 are the same kind of
references that you get from rule 1:

.Vb 2
        # This:
        $aref = [ 1, 2, 3 ];

        # Does the same as this:
        @array = (1, 2, 3);
        $aref = \\@array;
.Ve

The first line is an abbreviation for the following two lines, except
that it doesn't create the superfluous array variable \f(CW@array.

If you write just \f(CW\*(C`[]\*(C', you get a new, empty anonymous array.
If you write just \f(CW\*(C`\{\}\*(C', you get a new, empty anonymous hash.

### Using References

Subsection "Using References"
What can you do with a reference once you have it?  It's a scalar
value, and we've seen that you can store it as a scalar and get it back
again just like any scalar.  There are just two more ways to use it:

*\f(BIUse Rule 1\fI*
Subsection "Use Rule 1"

You can always use an array reference, in curly braces, in place of
the name of an array.  For example, \f(CW\*(C`@\{$aref\}\*(C' instead of \f(CW@array.

Here are some examples of that:

Arrays:

.Vb 4
        @a              @\{$aref\}                An array
        reverse @a      reverse @\{$aref\}        Reverse the array
        $a[3]           $\{$aref\}[3]             An element of the array
        $a[3] = 17;     $\{$aref\}[3] = 17        Assigning an element
.Ve

On each line are two expressions that do the same thing.  The
left-hand versions operate on the array \f(CW@a.  The right-hand
versions operate on the array that is referred to by \f(CW$aref.  Once
they find the array they're operating on, both versions do the same
things to the arrays.

Using a hash reference is *exactly* the same:

.Vb 4
        %h              %\{$href\}              A hash
        keys %h         keys %\{$href\}         Get the keys from the hash
        $h\{\*(Aqred\*(Aq\}       $\{$href\}\{\*(Aqred\*(Aq\}       An element of the hash
        $h\{\*(Aqred\*(Aq\} = 17  $\{$href\}\{\*(Aqred\*(Aq\} = 17  Assigning an element
.Ve

Whatever you want to do with a reference, **Use Rule 1** tells you how
to do it.  You just write the Perl code that you would have written
for doing the same thing to a regular array or hash, and then replace
the array or hash name with \f(CW\*(C`\{$reference\}\*(C'.  \*(L"How do I loop over an
array when all I have is a reference?\*(R"  Well, to loop over an array, you
would write

.Vb 3
        for my $element (@array) \{
          ...
        \}
.Ve

so replace the array name, \f(CW@array, with the reference:

.Vb 3
        for my $element (@\{$aref\}) \{
          ...
        \}
.Ve

\*(L"How do I print out the contents of a hash when all I have is a
reference?\*(R"  First write the code for printing out a hash:

.Vb 3
        for my $key (keys %hash) \{
          print "$key => $hash\{$key\}\\n";
        \}
.Ve

And then replace the hash name with the reference:

.Vb 3
        for my $key (keys %\{$href\}) \{
          print "$key => $\{$href\}\{$key\}\\n";
        \}
.Ve

*\f(BIUse Rule 2\fI*
Subsection "Use Rule 2"

**Use Rule 1** is all you really need, because it tells
you how to do absolutely everything you ever need to do with references.
But the most common thing to do with an array or a hash is to extract a
single element, and the **Use Rule 1** notation is
cumbersome.  So there is an abbreviation.

\f(CW\*(C`$\{$aref\}[3]\*(C' is too hard to read, so you can write \f(CW\*(C`$aref->[3]\*(C'
instead.

\f(CW\*(C`$\{$href\}\{red\}\*(C' is too hard to read, so you can write
\f(CW\*(C`$href->\{red\}\*(C' instead.

If \f(CW$aref holds a reference to an array, then \f(CW\*(C`$aref->[3]\*(C' is
the fourth element of the array.  Don't confuse this with \f(CW$aref[3],
which is the fourth element of a totally different array, one
deceptively named \f(CW@aref.  \f(CW$aref and \f(CW@aref are unrelated the
same way that \f(CW$item and \f(CW@item are.

Similarly, \f(CW\*(C`$href->\{\*(Aqred\*(Aq\}\*(C' is part of the hash referred to by
the scalar variable \f(CW$href, perhaps even one with no name.
\f(CW$href\{\*(Aqred\*(Aq\} is part of the deceptively named \f(CW%href hash.  It's
easy to forget to leave out the \f(CW\*(C`->\*(C', and if you do, you'll get
bizarre results when your program gets array and hash elements out of
totally unexpected hashes and arrays that weren't the ones you wanted
to use.

### An Example

Subsection "An Example"
Let's see a quick example of how all this is useful.

First, remember that \f(CW\*(C`[1, 2, 3]\*(C' makes an anonymous array containing
\f(CW\*(C`(1, 2, 3)\*(C', and gives you a reference to that array.

Now think about

.Vb 4
        @a = ( [1, 2, 3],
               [4, 5, 6],
               [7, 8, 9]
             );
.Ve

\f(CW@a is an array with three elements, and each one is a reference to
another array.

\f(CW$a[1] is one of these references.  It refers to an array, the array
containing \f(CW\*(C`(4, 5, 6)\*(C', and because it is a reference to an array,
**Use Rule 2** says that we can write \f(CW$a[1]->[2]
to get the third element from that array.  \f(CW$a[1]->[2] is the 6.
Similarly, \f(CW$a[0]->[1] is the 2.  What we have here is like a
two-dimensional array; you can write \f(CW$a[ROW]->[COLUMN] to get or
set the element in any row and any column of the array.

The notation still looks a little cumbersome, so there's one more
abbreviation:

### Arrow Rule

Subsection "Arrow Rule"
In between two **subscripts**, the arrow is optional.

Instead of \f(CW$a[1]->[2], we can write \f(CW$a[1][2]; it means the
same thing.  Instead of \f(CW\*(C`$a[0]->[1] = 23\*(C', we can write
\f(CW\*(C`$a[0][1] = 23\*(C'; it means the same thing.

Now it really looks like two-dimensional arrays!

You can see why the arrows are important.  Without them, we would have
had to write \f(CW\*(C`$\{$a[1]\}[2]\*(C' instead of \f(CW$a[1][2].  For
three-dimensional arrays, they let us write \f(CW$x[2][3][5] instead of
the unreadable \f(CW\*(C`$\{$\{$x[2]\}[3]\}[5]\*(C'.

## Solution

Header "Solution"
Here's the answer to the problem I posed earlier, of reformatting a
file of city and country names.

.Vb 1
    1   my %table;

    2   while (<>) \{
    3     chomp;
    4     my ($city, $country) = split /, /;
    5     $table\{$country\} = [] unless exists $table\{$country\};
    6     push @\{$table\{$country\}\}, $city;
    7   \}

    8   for my $country (sort keys %table) \{
    9     print "$country: ";
   10     my @cities = @\{$table\{$country\}\};
   11     print join \*(Aq, \*(Aq, sort @cities;
   12     print ".\\n";
   13   \}
.Ve

The program has two pieces: Lines 2-7 read the input and build a data
structure, and lines 8-13 analyze the data and print out the report.
We're going to have a hash, \f(CW%table, whose keys are country names,
and whose values are references to arrays of city names.  The data
structure will look like this:

.Vb 10
           %table
        +-------+---+
        |       |   |   +-----------+--------+
        |Germany| *---->| Frankfurt | Berlin |
        |       |   |   +-----------+--------+
        +-------+---+
        |       |   |   +----------+
        |Finland| *---->| Helsinki |
        |       |   |   +----------+
        +-------+---+
        |       |   |   +---------+------------+----------+
        |  USA  | *---->| Chicago | Washington | New York |
        |       |   |   +---------+------------+----------+
        +-------+---+
.Ve

We'll look at output first.  Supposing we already have this structure,
how do we print it out?

.Vb 6
    8   for my $country (sort keys %table) \{
    9     print "$country: ";
   10     my @cities = @\{$table\{$country\}\};
   11     print join \*(Aq, \*(Aq, sort @cities;
   12     print ".\\n";
   13   \}
.Ve

\f(CW%table is an ordinary hash, and we get a list of keys from it, sort
the keys, and loop over the keys as usual.  The only use of references
is in line 10.  \f(CW$table\{$country\} looks up the key \f(CW$country in the
hash and gets the value, which is a reference to an array of cities in
that country.  **Use Rule 1** says that we can recover
the array by saying \f(CW\*(C`@\{$table\{$country\}\}\*(C'.  Line 10 is just like

.Vb 1
        @cities = @array;
.Ve

except that the name \f(CW\*(C`array\*(C' has been replaced by the reference
\f(CW\*(C`\{$table\{$country\}\}\*(C'.  The \f(CW\*(C`@\*(C' tells Perl to get the entire array.
Having gotten the list of cities, we sort it, join it, and print it
out as usual.

Lines 2-7 are responsible for building the structure in the first
place.  Here they are again:

.Vb 6
    2   while (<>) \{
    3     chomp;
    4     my ($city, $country) = split /, /;
    5     $table\{$country\} = [] unless exists $table\{$country\};
    6     push @\{$table\{$country\}\}, $city;
    7   \}
.Ve

Lines 2-4 acquire a city and country name.  Line 5 looks to see if the
country is already present as a key in the hash.  If it's not, the
program uses the \f(CW\*(C`[]\*(C' notation (**Make Rule 2**) to
manufacture a new, empty anonymous array of cities, and installs a
reference to it into the hash under the appropriate key.

Line 6 installs the city name into the appropriate array.
\f(CW$table\{$country\} now holds a reference to the array of cities seen
in that country so far.  Line 6 is exactly like

.Vb 1
        push @array, $city;
.Ve

except that the name \f(CW\*(C`array\*(C' has been replaced by the reference
\f(CW\*(C`\{$table\{$country\}\}\*(C'.  The \f(CW\*(C`push\*(C' adds a
city name to the end of the referred-to array.

There's one fine point I skipped.  Line 5 is unnecessary, and we can
get rid of it.

.Vb 6
    2   while (<>) \{
    3     chomp;
    4     my ($city, $country) = split /, /;
    5   ####  $table\{$country\} = [] unless exists $table\{$country\};
    6     push @\{$table\{$country\}\}, $city;
    7   \}
.Ve

If there's already an entry in \f(CW%table for the current \f(CW$country,
then nothing is different.  Line 6 will locate the value in
\f(CW$table\{$country\}, which is a reference to an array, and push \f(CW$city
into the array.  But what does it do when \f(CW$country holds a key, say
\f(CW\*(C`Greece\*(C', that is not yet in \f(CW%table?

This is Perl, so it does the exact right thing.  It sees that you want
to push \f(CW\*(C`Athens\*(C' onto an array that doesn't exist, so it helpfully
makes a new, empty, anonymous array for you, installs it into
\f(CW%table, and then pushes \f(CW\*(C`Athens\*(C' onto it.  This is called
*autovivification*--bringing things to life automatically.  Perl saw
that the key wasn't in the hash, so it created a new hash entry
automatically. Perl saw that you wanted to use the hash value as an
array, so it created a new empty array and installed a reference to it
in the hash automatically.  And as usual, Perl made the array one
element longer to hold the new city name.

## The Rest

Header "The Rest"
I promised to give you 90% of the benefit with 10% of the details, and
that means I left out 90% of the details.  Now that you have an
overview of the important parts, it should be easier to read the
perlref manual page, which discusses 100% of the details.

Some of the highlights of perlref:

- \(bu
You can make references to anything, including scalars, functions, and
other references.

- \(bu
In **Use Rule 1**, you can omit the curly brackets
whenever the thing inside them is an atomic scalar variable like
\f(CW$aref.  For example, \f(CW@$aref is the same as \f(CW\*(C`@\{$aref\}\*(C', and
\f(CW$$aref[1] is the same as \f(CW\*(C`$\{$aref\}[1]\*(C'.  If you're just starting
out, you may want to adopt the habit of always including the curly
brackets.

- \(bu
This doesn't copy the underlying array:
.Sp
.Vb 1
        $aref2 = $aref1;
.Ve
.Sp
You get two references to the same array.  If you modify
\f(CW\*(C`$aref1->[23]\*(C' and then look at
\f(CW\*(C`$aref2->[23]\*(C' you'll see the change.
.Sp
To copy the array, use
.Sp
.Vb 1
        $aref2 = [@\{$aref1\}];
.Ve
.Sp
This uses \f(CW\*(C`[...]\*(C' notation to create a new anonymous array, and
\f(CW$aref2 is assigned a reference to the new array.  The new array is
initialized with the contents of the array referred to by \f(CW$aref1.
.Sp
Similarly, to copy an anonymous hash, you can use
.Sp
.Vb 1
        $href2 = \{%\{$href1\}\};
.Ve

- \(bu
To see if a variable contains a reference, use the
\f(CW\*(C`ref\*(C' function.  It returns true if its argument
is a reference.  Actually it's a little better than that: It returns
\f(CW\*(C`HASH\*(C' for hash references and \f(CW\*(C`ARRAY\*(C' for array references.

- \(bu
If you try to use a reference like a string, you get strings like
.Sp
.Vb 1
        ARRAY(0x80f5dec)   or    HASH(0x826afc0)
.Ve
.Sp
If you ever see a string that looks like this, you'll know you
printed out a reference by mistake.
.Sp
A side effect of this representation is that you can use
\f(CW\*(C`eq\*(C' to see if two references refer to the
same thing.  (But you should usually use
\f(CW\*(C`==\*(C' instead because it's much faster.)

- \(bu
You can use a string as if it were a reference.  If you use the string
\f(CW"foo" as an array reference, it's taken to be a reference to the
array \f(CW@foo.  This is called a *symbolic reference*.  The declaration
\f(CW\*(C`use strict \*(Aqrefs\*(Aq\*(C' disables this feature, which can cause
all sorts of trouble if you use it by accident.

You might prefer to go on to perllol instead of perlref; it
discusses lists of lists and multidimensional arrays in detail.  After
that, you should move on to perldsc; it's a Data Structure Cookbook
that shows recipes for using and printing out arrays of hashes, hashes
of arrays, and other kinds of data.

## Summary

Header "Summary"
Everyone needs compound data structures, and in Perl the way you get
them is with references.  There are four important rules for managing
references: Two for making references and two for using them.  Once
you know these rules you can do most of the important things you need
to do with references.

## Credits

Header "Credits"
Author: Mark Jason Dominus, Plover Systems (\f(CW\*(C`mjd-perl-ref+@plover.com\*(C')

This article originally appeared in *The Perl Journal*
( <http://www.tpj.com/> ) volume 3, #2.  Reprinted with permission.

The original title was *Understand References Today*.

### Distribution Conditions

Subsection "Distribution Conditions"
Copyright 1998 The Perl Journal.

This documentation is free; you can redistribute it and/or modify it
under the same terms as Perl itself.

Irrespective of its distribution, all code examples in these files are
hereby placed into the public domain.  You are permitted and
encouraged to use this code in your own programs for fun or for profit
as you see fit.  A simple comment in the code giving credit would be
courteous but is not required.
