+++
manpage_format = "troff"
date = "2022-02-19"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
description = "This document describes differences between the 5.30.0 release and the 5.30.1 release. If you are upgrading from an earlier release such as 5.28.0, first read perl5300delta, which describes differences between 5.28.0 and 5.30.0. There are no chang..."
manpage_section = "1"
author = "None Specified"
title = "perl5301delta(1)"
operating_system = "macos"
operating_system_version = "15.3"
detected_package_version = "5.34.1"
manpage_name = "perl5301delta"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5301DELTA 1"
PERL5301DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5301delta - what is new for perl v5.30.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.30.0 release and the 5.30.1
release.

If you are upgrading from an earlier release such as 5.28.0, first read
perl5300delta, which describes differences between 5.28.0 and 5.30.0.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.30.1.  If any exist,
they are bugs, and we request that you submit a report.  See
\*(L"Reporting Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Module::CoreList has been upgraded from version 5.20190522 to 5.20191110.

## Documentation

Header "Documentation"

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
We have attempted to update the documentation to reflect the changes listed in
this document.  If you find any we have missed, send email to
perlbug@perl.org <mailto:perlbug@perl.org>.

Additionally, documentation has been updated to reference GitHub as the new
canonical repository and to describe the new GitHub pull request workflow.

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
The \f(CW\*(C`ECHO\*(C' macro is now defined.  This is used in a \f(CW\*(C`dtrace\*(C' rule that was
originally changed for FreeBSD, and the FreeBSD make apparently predefines it.
The Solaris make does not predefine \f(CW\*(C`ECHO\*(C' which broke this rule on Solaris.
[perl #17057] <https://github.com/perl/perl5/issues/17057>

## Testing

Header "Testing"
Tests were added and changed to reflect the other additions and changes in this
release.

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- Win32
Item "Win32"
The locale tests could crash on Win32 due to a Windows bug, and separately due
to the \s-1CRT\s0 throwing an exception if the locale name wasn't validly encoded in
the current code page.
.Sp
For the second we now decode the locale name ourselves, and always decode it as
\s-1UTF-8.\s0
.Sp
[perl #16922] <https://github.com/perl/perl5/issues/16922>

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Setting \f(CW$) now properly sets supplementary group ids, if you have the
necessary privileges.
[perl #17031] <https://github.com/perl/perl5/issues/17031>

- \(bu
\f(CW\*(C`readline @foo\*(C' now evaluates \f(CW@foo in scalar context.  Previously, it would
be evaluated in list context, and since **readline()** pops only one argument from
the stack, the stack could underflow, or be left with unexpected values on it.
[perl #16929] <https://github.com/perl/perl5/issues/16929>

- \(bu
**sv_gets()** now recovers better if the target \s-1SV\s0 is modified by a signal handler.
[perl #16960] <https://github.com/perl/perl5/issues/16960>

- \(bu
Matching a non-\f(CW\*(C`SVf_UTF8\*(C' string against a regular expression containing
Unicode literals could leak an \s-1SV\s0 on each match attempt.
[perl #17140] <https://github.com/perl/perl5/issues/17140>

- \(bu
\f(CW\*(C`sprintf("%.*a", -10000, $x)\*(C' would cause a buffer overflow due to
mishandling of the negative precision value.
[perl #16942] <https://github.com/perl/perl5/issues/16942>

- \(bu
\f(CW\*(C`scalar()\*(C' on a reference could cause an erroneous assertion failure during
compilation.
[perl #16969] <https://github.com/perl/perl5/issues/16969>

## Acknowledgements

Header "Acknowledgements"
Perl 5.30.1 represents approximately 6 months of development since Perl 5.30.0
and contains approximately 4,700 lines of changes across 67 files from 14
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 910 lines of changes to 20 .pm, .t, .c and .h files.

Perl continues to flourish into its fourth decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.30.1:

Chris 'BinGOs' Williams, Dan Book, David Mitchell, Hugo van der Sanden, James E
Keenan, Karen Etheridge, Karl Williamson, Manuel Mausz, Max Maischein, Nicolas
R., Sawyer X, Steve Hay, Tom Hukins, Tony Cook.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database at
<https://rt.perl.org/>.  There may also be information at
<http://www.perl.org/>, the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec for details of how to
report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5, you
can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
