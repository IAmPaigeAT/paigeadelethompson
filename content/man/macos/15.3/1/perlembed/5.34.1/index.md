+++
manpage_name = "perlembed"
manpage_section = "1"
description = "Do you want to: Read perlxstut, perlxs, h2xs, perlguts, and perlapi. Read about back-quotes and about f(CW*(C`system*(C and f(CW*(C`exec*(C in perlfunc. Read about *(Ldo*(R in perlfunc and *(Leval*(R in perlfunc and *(Lrequire*(R in perlfun..."
date = "2022-02-19"
manpage_format = "troff"
operating_system_version = "15.3"
detected_package_version = "5.34.1"
title = "perlembed(1)"
author = "None Specified"
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLEMBED 1"
PERLEMBED 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlembed - how to embed perl in your C program

## DESCRIPTION

Header "DESCRIPTION"

### \s-1PREAMBLE\s0

Subsection "PREAMBLE"
Do you want to:

- \fBUse C from Perl?
Item "Use C from Perl?"
Read perlxstut, perlxs, h2xs, perlguts, and perlapi.

- \fBUse a Unix program from Perl?
Item "Use a Unix program from Perl?"
Read about back-quotes and about \f(CW\*(C`system\*(C' and \f(CW\*(C`exec\*(C' in perlfunc.

- \fBUse Perl from Perl?
Item "Use Perl from Perl?"
Read about \*(L"do\*(R" in perlfunc and \*(L"eval\*(R" in perlfunc and \*(L"require\*(R" in perlfunc
and \*(L"use\*(R" in perlfunc.

- \fBUse C from C?
Item "Use C from C?"
Rethink your design.

- \fBUse Perl from C?
Item "Use Perl from C?"
Read on...

### \s-1ROADMAP\s0

Subsection "ROADMAP"

- \(bu
Compiling your C program

- \(bu
Adding a Perl interpreter to your C program

- \(bu
Calling a Perl subroutine from your C program

- \(bu
Evaluating a Perl statement from your C program

- \(bu
Performing Perl pattern matches and substitutions from your C program

- \(bu
Fiddling with the Perl stack from your C program

- \(bu
Maintaining a persistent interpreter

- \(bu
Maintaining multiple interpreter instances

- \(bu
Using Perl modules, which themselves use C libraries, from your C program

- \(bu
Embedding Perl under Win32

### Compiling your C program

Subsection "Compiling your C program"
If you have trouble compiling the scripts in this documentation,
you're not alone.  The cardinal rule: \s-1COMPILE THE PROGRAMS IN EXACTLY
THE SAME WAY THAT YOUR PERL WAS COMPILED.\s0  (Sorry for yelling.)

Also, every C program that uses Perl must link in the *perl library*.
What's that, you ask?  Perl is itself written in C; the perl library
is the collection of compiled C programs that were used to create your
perl executable (*/usr/bin/perl* or equivalent).  (Corollary: you
can't use Perl from your C program unless Perl has been compiled on
your machine, or installed properly\*(--that's why you shouldn't blithely
copy Perl executables from machine to machine without also copying the
*lib* directory.)

When you use Perl from C, your C program will\*(--usually--allocate,
\*(L"run\*(R", and deallocate a *PerlInterpreter* object, which is defined by
the perl library.

If your copy of Perl is recent enough to contain this documentation
(version 5.002 or later), then the perl library (and *\s-1EXTERN\s0.h* and
*perl.h*, which you'll also need) will reside in a directory
that looks like this:

.Vb 1
    /usr/local/lib/perl5/your_architecture_here/CORE
.Ve

or perhaps just

.Vb 1
    /usr/local/lib/perl5/CORE
.Ve

or maybe something like

.Vb 1
    /usr/opt/perl5/CORE
.Ve

Execute this statement for a hint about where to find \s-1CORE:\s0

.Vb 1
    perl -MConfig -e \*(Aqprint $Config\{archlib\}\*(Aq
.Ve

Here's how you'd compile the example in the next section,
\*(L"Adding a Perl interpreter to your C program\*(R", on my Linux box:

.Vb 4
    % gcc -O2 -Dbool=char -DHAS_BOOL -I/usr/local/include
    -I/usr/local/lib/perl5/i586-linux/5.003/CORE
    -L/usr/local/lib/perl5/i586-linux/5.003/CORE
    -o interp interp.c -lperl -lm
.Ve

(That's all one line.)  On my \s-1DEC\s0 Alpha running old 5.003_05, the
incantation is a bit different:

.Vb 4
    % cc -O2 -Olimit 2900 -I/usr/local/include
    -I/usr/local/lib/perl5/alpha-dec_osf/5.00305/CORE
    -L/usr/local/lib/perl5/alpha-dec_osf/5.00305/CORE -L/usr/local/lib
    -D_\|_LANGUAGE_C_\|_ -D_NO_PROTO -o interp interp.c -lperl -lm
.Ve

How can you figure out what to add?  Assuming your Perl is post-5.001,
execute a \f(CW\*(C`perl -V\*(C' command and pay special attention to the \*(L"cc\*(R" and
\*(L"ccflags\*(R" information.

You'll have to choose the appropriate compiler (*cc*, *gcc*, et al.) for
your machine: \f(CW\*(C`perl -MConfig -e \*(Aqprint $Config\{cc\}\*(Aq\*(C' will tell you what
to use.

You'll also have to choose the appropriate library directory
(*/usr/local/lib/...*) for your machine.  If your compiler complains
that certain functions are undefined, or that it can't locate
*-lperl*, then you need to change the path following the \f(CW\*(C`-L\*(C'.  If it
complains that it can't find *\s-1EXTERN\s0.h* and *perl.h*, you need to
change the path following the \f(CW\*(C`-I\*(C'.

You may have to add extra libraries as well.  Which ones?
Perhaps those printed by

.Vb 1
   perl -MConfig -e \*(Aqprint $Config\{libs\}\*(Aq
.Ve

Provided your perl binary was properly configured and installed the
**ExtUtils::Embed** module will determine all of this information for
you:

.Vb 1
   % cc -o interp interp.c \`perl -MExtUtils::Embed -e ccopts -e ldopts\`
.Ve

If the **ExtUtils::Embed** module isn't part of your Perl distribution,
you can retrieve it from
<https://metacpan.org/pod/ExtUtils::Embed>
(If this documentation came from your Perl distribution, then you're
running 5.004 or better and you already have it.)

The **ExtUtils::Embed** kit on \s-1CPAN\s0 also contains all source code for
the examples in this document, tests, additional examples and other
information you may find useful.

### Adding a Perl interpreter to your C program

Subsection "Adding a Perl interpreter to your C program"
In a sense, perl (the C program) is a good example of embedding Perl
(the language), so I'll demonstrate embedding with *miniperlmain.c*,
included in the source distribution.  Here's a bastardized, non-portable
version of *miniperlmain.c* containing the essentials of embedding:

.Vb 2
 #include <EXTERN.h>               /* from the Perl distribution     */
 #include <perl.h>                 /* from the Perl distribution     */

 static PerlInterpreter *my_perl;  /***    The Perl interpreter    ***/

 int main(int argc, char **argv, char **env)
 \{
        PERL_SYS_INIT3(&argc,&argv,&env);
        my_perl = perl_alloc();
        perl_construct(my_perl);
        PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
        perl_parse(my_perl, NULL, argc, argv, (char **)NULL);
        perl_run(my_perl);
        perl_destruct(my_perl);
        perl_free(my_perl);
        PERL_SYS_TERM();
        exit(EXIT_SUCCESS);
 \}
.Ve

Notice that we don't use the \f(CW\*(C`env\*(C' pointer.  Normally handed to
\f(CW\*(C`perl_parse\*(C' as its final argument, \f(CW\*(C`env\*(C' here is replaced by
\f(CW\*(C`NULL\*(C', which means that the current environment will be used.

The macros \s-1**PERL_SYS_INIT3\s0()** and \s-1**PERL_SYS_TERM\s0()** provide system-specific
tune up of the C runtime environment necessary to run Perl interpreters;
they should only be called once regardless of how many interpreters you
create or destroy. Call \s-1**PERL_SYS_INIT3\s0()** before you create your first
interpreter, and \s-1**PERL_SYS_TERM\s0()** after you free your last interpreter.

Since \s-1**PERL_SYS_INIT3\s0()** may change \f(CW\*(C`env\*(C', it may be more appropriate to
provide \f(CW\*(C`env\*(C' as an argument to **perl_parse()**.

Also notice that no matter what arguments you pass to **perl_parse()**,
\s-1**PERL_SYS_INIT3\s0()** must be invoked on the C **main()** argc, argv and env and
only once.

Mind that argv[argc] must be \s-1NULL,\s0 same as those passed to a main
function in C.

Now compile this program (I'll call it *interp.c*) into an executable:

.Vb 1
    % cc -o interp interp.c \`perl -MExtUtils::Embed -e ccopts -e ldopts\`
.Ve

After a successful compilation, you'll be able to use *interp* just
like perl itself:

.Vb 6
    % interp
    print "Pretty Good Perl \\n";
    print "10890 - 9801 is ", 10890 - 9801;
    <CTRL-D>
    Pretty Good Perl
    10890 - 9801 is 1089
.Ve

or

.Vb 2
    % interp -e \*(Aqprintf("%x", 3735928559)\*(Aq
    deadbeef
.Ve

You can also read and execute Perl statements from a file while in the
midst of your C program, by placing the filename in *argv[1]* before
calling *perl_run*.

### Calling a Perl subroutine from your C program

Subsection "Calling a Perl subroutine from your C program"
To call individual Perl subroutines, you can use any of the **call_***
functions documented in perlcall.
In this example we'll use \f(CW\*(C`call_argv\*(C'.

That's shown below, in a program I'll call *showtime.c*.

.Vb 2
    #include <EXTERN.h>
    #include <perl.h>

    static PerlInterpreter *my_perl;

    int main(int argc, char **argv, char **env)
    \{
        char *args[] = \{ NULL \};
        PERL_SYS_INIT3(&argc,&argv,&env);
        my_perl = perl_alloc();
        perl_construct(my_perl);

        perl_parse(my_perl, NULL, argc, argv, NULL);
        PL_exit_flags |= PERL_EXIT_DESTRUCT_END;

        /*** skipping perl_run() ***/

        call_argv("showtime", G_DISCARD | G_NOARGS, args);

        perl_destruct(my_perl);
        perl_free(my_perl);
        PERL_SYS_TERM();
        exit(EXIT_SUCCESS);
    \}
.Ve

where *showtime* is a Perl subroutine that takes no arguments (that's the
*G_NOARGS*) and for which I'll ignore the return value (that's the
*G_DISCARD*).  Those flags, and others, are discussed in perlcall.

I'll define the *showtime* subroutine in a file called *showtime.pl*:

.Vb 1
 print "I shan\*(Aqt be printed.";

 sub showtime \{
     print time;
 \}
.Ve

Simple enough. Now compile and run:

.Vb 4
 % cc -o showtime showtime.c \\
     \`perl -MExtUtils::Embed -e ccopts -e ldopts\`
 % showtime showtime.pl
 818284590
.Ve

yielding the number of seconds that elapsed between January 1, 1970
(the beginning of the Unix epoch), and the moment I began writing this
sentence.

In this particular case we don't have to call *perl_run*, as we set
the PL_exit_flag \s-1PERL_EXIT_DESTRUCT_END\s0 which executes \s-1END\s0 blocks in
perl_destruct.

If you want to pass arguments to the Perl subroutine, you can add
strings to the \f(CW\*(C`NULL\*(C'-terminated \f(CW\*(C`args\*(C' list passed to
*call_argv*.  For other data types, or to examine return values,
you'll need to manipulate the Perl stack.  That's demonstrated in
\*(L"Fiddling with the Perl stack from your C program\*(R".

### Evaluating a Perl statement from your C program

Subsection "Evaluating a Perl statement from your C program"
Perl provides two \s-1API\s0 functions to evaluate pieces of Perl code.
These are \*(L"eval_sv\*(R" in perlapi and \*(L"eval_pv\*(R" in perlapi.

Arguably, these are the only routines you'll ever need to execute
snippets of Perl code from within your C program.  Your code can be as
long as you wish; it can contain multiple statements; it can employ
\*(L"use\*(R" in perlfunc, \*(L"require\*(R" in perlfunc, and \*(L"do\*(R" in perlfunc to
include external Perl files.

*eval_pv* lets us evaluate individual Perl strings, and then
extract variables for coercion into C types.  The following program,
*string.c*, executes three Perl strings, extracting an \f(CW\*(C`int\*(C' from
the first, a \f(CW\*(C`float\*(C' from the second, and a \f(CW\*(C`char *\*(C' from the third.

.Vb 2
 #include <EXTERN.h>
 #include <perl.h>

 static PerlInterpreter *my_perl;

 main (int argc, char **argv, char **env)
 \{
     char *embedding[] = \{ "", "-e", "0", NULL \};

     PERL_SYS_INIT3(&argc,&argv,&env);
     my_perl = perl_alloc();
     perl_construct( my_perl );

     perl_parse(my_perl, NULL, 3, embedding, NULL);
     PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
     perl_run(my_perl);

     /** Treat $a as an integer **/
     eval_pv("$a = 3; $a **= 2", TRUE);
     printf("a = %d\\n", SvIV(get_sv("a", 0)));

     /** Treat $a as a float **/
     eval_pv("$a = 3.14; $a **= 2", TRUE);
     printf("a = %f\\n", SvNV(get_sv("a", 0)));

     /** Treat $a as a string **/
     eval_pv(
       "$a = \*(AqrekcaH lreP rehtonA tsuJ\*(Aq; $a = reverse($a);", TRUE);
     printf("a = %s\\n", SvPV_nolen(get_sv("a", 0)));

     perl_destruct(my_perl);
     perl_free(my_perl);
     PERL_SYS_TERM();
 \}
.Ve

All of those strange functions with *sv* in their names help convert Perl
scalars to C types.  They're described in perlguts and perlapi.

If you compile and run *string.c*, you'll see the results of using
*\f(BISvIV()** to create an \f(CW\*(C`int\*(C'*, *\f(BISvNV()** to create a \f(CW\*(C`float\*(C'*, and
*\f(BISvPV()\fI* to create a string:

.Vb 3
   a = 9
   a = 9.859600
   a = Just Another Perl Hacker
.Ve

In the example above, we've created a global variable to temporarily
store the computed value of our eval'ed expression.  It is also
possible and in most cases a better strategy to fetch the return value
from *\f(BIeval_pv()\fI* instead.  Example:

.Vb 4
   ...
   SV *val = eval_pv("reverse \*(AqrekcaH lreP rehtonA tsuJ\*(Aq", TRUE);
   printf("%s\\n", SvPV_nolen(val));
   ...
.Ve

This way, we avoid namespace pollution by not creating global
variables and we've simplified our code as well.

### Performing Perl pattern matches and substitutions from your C program

Subsection "Performing Perl pattern matches and substitutions from your C program"
The *\f(BIeval_sv()\fI* function lets us evaluate strings of Perl code, so we can
define some functions that use it to \*(L"specialize\*(R" in matches and
substitutions: *\f(BImatch()**, *\f(BIsubstitute()\fI*, and \fI\f(BImatches()\fI*.

.Vb 1
   I32 match(SV *string, char *pattern);
.Ve

Given a string and a pattern (e.g., \f(CW\*(C`m/clasp/\*(C' or \f(CW\*(C`/\\b\\w*\\b/\*(C', which
in your C program might appear as \*(L"/\\\\b\\\\w*\\\\b/\*(R"), **match()**
returns 1 if the string matches the pattern and 0 otherwise.

.Vb 1
   int substitute(SV **string, char *pattern);
.Ve

Given a pointer to an \f(CW\*(C`SV\*(C' and an \f(CW\*(C`=~\*(C' operation (e.g.,
\f(CW\*(C`s/bob/robert/g\*(C' or \f(CW\*(C`tr[A-Z][a-z]\*(C'), **substitute()** modifies the string
within the \f(CW\*(C`SV\*(C' as according to the operation, returning the number of
substitutions made.

.Vb 1
   SSize_t matches(SV *string, char *pattern, AV **matches);
.Ve

Given an \f(CW\*(C`SV\*(C', a pattern, and a pointer to an empty \f(CW\*(C`AV\*(C',
**matches()** evaluates \f(CW\*(C`$string =~ $pattern\*(C' in a list context, and
fills in *matches* with the array elements, returning the number of matches
found.

Here's a sample program, *match.c*, that uses all three (long lines have
been wrapped here):

.Vb 2
 #include <EXTERN.h>
 #include <perl.h>

 static PerlInterpreter *my_perl;

 /** my_eval_sv(code, error_check)
 ** kinda like eval_sv(),
 ** but we pop the return value off the stack
 **/
 SV* my_eval_sv(SV *sv, I32 croak_on_error)
 \{
     dSP;
     SV* retval;


     PUSHMARK(SP);
     eval_sv(sv, G_SCALAR);

     SPAGAIN;
     retval = POPs;
     PUTBACK;

     if (croak_on_error && SvTRUE(ERRSV))
        croak_sv(ERRSV);

     return retval;
 \}

 /** match(string, pattern)
 **
 ** Used for matches in a scalar context.
 **
 ** Returns 1 if the match was successful; 0 otherwise.
 **/

 I32 match(SV *string, char *pattern)
 \{
     SV *command = newSV(0), *retval;

     sv_setpvf(command, "my $string = \*(Aq%s\*(Aq; $string =~ %s",
              SvPV_nolen(string), pattern);

     retval = my_eval_sv(command, TRUE);
     SvREFCNT_dec(command);

     return SvIV(retval);
 \}

 /** substitute(string, pattern)
 **
 ** Used for =~ operations that
 ** modify their left-hand side (s/// and tr///)
 **
 ** Returns the number of successful matches, and
 ** modifies the input string if there were any.
 **/

 I32 substitute(SV **string, char *pattern)
 \{
     SV *command = newSV(0), *retval;

     sv_setpvf(command, "$string = \*(Aq%s\*(Aq; ($string =~ %s)",
              SvPV_nolen(*string), pattern);

     retval = my_eval_sv(command, TRUE);
     SvREFCNT_dec(command);

     *string = get_sv("string", 0);
     return SvIV(retval);
 \}

 /** matches(string, pattern, matches)
 **
 ** Used for matches in a list context.
 **
 ** Returns the number of matches,
 ** and fills in **matches with the matching substrings
 **/

 SSize_t matches(SV *string, char *pattern, AV **match_list)
 \{
     SV *command = newSV(0);
     SSize_t num_matches;

     sv_setpvf(command, "my $string = \*(Aq%s\*(Aq; @array = ($string =~ %s)",
              SvPV_nolen(string), pattern);

     my_eval_sv(command, TRUE);
     SvREFCNT_dec(command);

     *match_list = get_av("array", 0);
     num_matches = av_top_index(*match_list) + 1;

     return num_matches;
 \}

 main (int argc, char **argv, char **env)
 \{
     char *embedding[] = \{ "", "-e", "0", NULL \};
     AV *match_list;
     I32 num_matches, i;
     SV *text;

     PERL_SYS_INIT3(&argc,&argv,&env);
     my_perl = perl_alloc();
     perl_construct(my_perl);
     perl_parse(my_perl, NULL, 3, embedding, NULL);
     PL_exit_flags |= PERL_EXIT_DESTRUCT_END;

     text = newSV(0);
     sv_setpv(text, "When he is at a convenience store and the "
        "bill comes to some amount like 76 cents, Maynard is "
        "aware that there is something he *should* do, something "
        "that will enable him to get back a quarter, but he has "
        "no idea *what*.  He fumbles through his red squeezey "
        "changepurse and gives the boy three extra pennies with "
        "his dollar, hoping that he might luck into the correct "
        "amount.  The boy gives him back two of his own pennies "
        "and then the big shiny quarter that is his prize. "
        "-RICHH");

     if (match(text, "m/quarter/")) /** Does text contain \*(Aqquarter\*(Aq? **/
        printf("match: Text contains the word \*(Aqquarter\*(Aq.\\n\\n");
     else
        printf("match: Text doesn\*(Aqt contain the word \*(Aqquarter\*(Aq.\\n\\n");

     if (match(text, "m/eighth/")) /** Does text contain \*(Aqeighth\*(Aq? **/
        printf("match: Text contains the word \*(Aqeighth\*(Aq.\\n\\n");
     else
        printf("match: Text doesn\*(Aqt contain the word \*(Aqeighth\*(Aq.\\n\\n");

     /** Match all occurrences of /wi../ **/
     num_matches = matches(text, "m/(wi..)/g", &match_list);
     printf("matches: m/(wi..)/g found %d matches...\\n", num_matches);

     for (i = 0; i < num_matches; i++)
         printf("match: %s\\n",
                  SvPV_nolen(*av_fetch(match_list, i, FALSE)));
     printf("\\n");

     /** Remove all vowels from text **/
     num_matches = substitute(&text, "s/[aeiou]//gi");
     if (num_matches) \{
        printf("substitute: s/[aeiou]//gi...%lu substitutions made.\\n",
               (unsigned long)num_matches);
        printf("Now text is: %s\\n\\n", SvPV_nolen(text));
     \}

     /** Attempt a substitution **/
     if (!substitute(&text, "s/Perl/C/")) \{
        printf("substitute: s/Perl/C...No substitution made.\\n\\n");
     \}

     SvREFCNT_dec(text);
     PL_perl_destruct_level = 1;
     perl_destruct(my_perl);
     perl_free(my_perl);
     PERL_SYS_TERM();
 \}
.Ve

which produces the output (again, long lines have been wrapped here)

.Vb 1
  match: Text contains the word \*(Aqquarter\*(Aq.

  match: Text doesn\*(Aqt contain the word \*(Aqeighth\*(Aq.

  matches: m/(wi..)/g found 2 matches...
  match: will
  match: with

  substitute: s/[aeiou]//gi...139 substitutions made.
  Now text is: Whn h s t  cnvnnc str nd th bll cms t sm mnt lk 76 cnts,
  Mynrd s wr tht thr s smthng h *shld* d, smthng tht wll nbl hm t gt
  bck qrtr, bt h hs n d *wht*.  H fmbls thrgh hs rd sqzy chngprs nd
  gvs th by thr xtr pnns wth hs dllr, hpng tht h mght lck nt th crrct
  mnt.  Th by gvs hm bck tw f hs wn pnns nd thn th bg shny qrtr tht s
  hs prz. -RCHH

  substitute: s/Perl/C...No substitution made.
.Ve

### Fiddling with the Perl stack from your C program

Subsection "Fiddling with the Perl stack from your C program"
When trying to explain stacks, most computer science textbooks mumble
something about spring-loaded columns of cafeteria plates: the last
thing you pushed on the stack is the first thing you pop off.  That'll
do for our purposes: your C program will push some arguments onto \*(L"the Perl
stack\*(R", shut its eyes while some magic happens, and then pop the
results\*(--the return value of your Perl subroutine\*(--off the stack.

First you'll need to know how to convert between C types and Perl
types, with **newSViv()** and **sv_setnv()** and **newAV()** and all their
friends.  They're described in perlguts and perlapi.

Then you'll need to know how to manipulate the Perl stack.  That's
described in perlcall.

Once you've understood those, embedding Perl in C is easy.

Because C has no builtin function for integer exponentiation, let's
make Perl's ** operator available to it (this is less useful than it
sounds, because Perl implements ** with C's *\f(BIpow()\fI* function).  First
I'll create a stub exponentiation function in *power.pl*:

.Vb 4
    sub expo \{
        my ($a, $b) = @_;
        return $a ** $b;
    \}
.Ve

Now I'll create a C program, *power.c*, with a function
*\f(BIPerlPower()\fI* that contains all the perlguts necessary to push the
two arguments into *\f(BIexpo()\fI* and to pop the return value out.  Take a
deep breath...

.Vb 2
 #include <EXTERN.h>
 #include <perl.h>

 static PerlInterpreter *my_perl;

 static void
 PerlPower(int a, int b)
 \{
   dSP;                            /* initialize stack pointer      */
   ENTER;                          /* everything created after here */
   SAVETMPS;                       /* ...is a temporary variable.   */
   PUSHMARK(SP);                   /* remember the stack pointer    */
   XPUSHs(sv_2mortal(newSViv(a))); /* push the base onto the stack  */
   XPUSHs(sv_2mortal(newSViv(b))); /* push the exponent onto stack  */
   PUTBACK;                      /* make local stack pointer global */
   call_pv("expo", G_SCALAR);      /* call the function             */
   SPAGAIN;                        /* refresh stack pointer         */
                                 /* pop the return value from stack */
   printf ("%d to the %dth power is %d.\\n", a, b, POPi);
   PUTBACK;
   FREETMPS;                       /* free that return value        */
   LEAVE;                       /* ...and the XPUSHed "mortal" args.*/
 \}

 int main (int argc, char **argv, char **env)
 \{
   char *my_argv[] = \{ "", "power.pl", NULL \};

   PERL_SYS_INIT3(&argc,&argv,&env);
   my_perl = perl_alloc();
   perl_construct( my_perl );

   perl_parse(my_perl, NULL, 2, my_argv, (char **)NULL);
   PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
   perl_run(my_perl);

   PerlPower(3, 4);                      /*** Compute 3 ** 4 ***/

   perl_destruct(my_perl);
   perl_free(my_perl);
   PERL_SYS_TERM();
   exit(EXIT_SUCCESS);
 \}
.Ve

Compile and run:

.Vb 1
    % cc -o power power.c \`perl -MExtUtils::Embed -e ccopts -e ldopts\`

    % power
    3 to the 4th power is 81.
.Ve

### Maintaining a persistent interpreter

Subsection "Maintaining a persistent interpreter"
When developing interactive and/or potentially long-running
applications, it's a good idea to maintain a persistent interpreter
rather than allocating and constructing a new interpreter multiple
times.  The major reason is speed: since Perl will only be loaded into
memory once.

However, you have to be more cautious with namespace and variable
scoping when using a persistent interpreter.  In previous examples
we've been using global variables in the default package \f(CW\*(C`main\*(C'.  We
knew exactly what code would be run, and assumed we could avoid
variable collisions and outrageous symbol table growth.

Let's say your application is a server that will occasionally run Perl
code from some arbitrary file.  Your server has no way of knowing what
code it's going to run.  Very dangerous.

If the file is pulled in by \f(CW\*(C`perl_parse()\*(C', compiled into a newly
constructed interpreter, and subsequently cleaned out with
\f(CW\*(C`perl_destruct()\*(C' afterwards, you're shielded from most namespace
troubles.

One way to avoid namespace collisions in this scenario is to translate
the filename into a guaranteed-unique package name, and then compile
the code into that package using \*(L"eval\*(R" in perlfunc.  In the example
below, each file will only be compiled once.  Or, the application
might choose to clean out the symbol table associated with the file
after it's no longer needed.  Using \*(L"call_argv\*(R" in perlapi, We'll
call the subroutine \f(CW\*(C`Embed::Persistent::eval_file\*(C' which lives in the
file \f(CW\*(C`persistent.pl\*(C' and pass the filename and boolean cleanup/cache
flag as arguments.

Note that the process will continue to grow for each file that it
uses.  In addition, there might be \f(CW\*(C`AUTOLOAD\*(C'ed subroutines and other
conditions that cause Perl's symbol table to grow.  You might want to
add some logic that keeps track of the process size, or restarts
itself after a certain number of requests, to ensure that memory
consumption is minimized.  You'll also want to scope your variables
with \*(L"my\*(R" in perlfunc whenever possible.

.Vb 2
 package Embed::Persistent;
 #persistent.pl

 use strict;
 our %Cache;
 use Symbol qw(delete_package);

 sub valid_package_name \{
     my($string) = @_;
     $string =~ s/([^A-Za-z0-9\\/])/sprintf("_%2x",unpack("C",$1))/eg;
     # second pass only for words starting with a digit
     $string =~ s|/(\\d)|sprintf("/_%2x",unpack("C",$1))|eg;

     # Dress it up as a real package name
     $string =~ s|/|::|g;
     return "Embed" . $string;
 \}

 sub eval_file \{
     my($filename, $delete) = @_;
     my $package = valid_package_name($filename);
     my $mtime = -M $filename;
     if(defined $Cache\{$package\}\{mtime\}
        &&
        $Cache\{$package\}\{mtime\} <= $mtime)
     \{
        # we have compiled this subroutine already,
        # it has not been updated on disk, nothing left to do
        print STDERR "already compiled $package->handler\\n";
     \}
     else \{
        local *FH;
        open FH, $filename or die "open \*(Aq$filename\*(Aq $!";
        local($/) = undef;
        my $sub = <FH>;
        close FH;

        #wrap the code into a subroutine inside our unique package
        my $eval = qq\{package $package; sub handler \{ $sub; \}\};
        \{
            # hide our variables within this block
            my($filename,$mtime,$package,$sub);
            eval $eval;
        \}
        die $@ if $@;

        #cache it unless we\*(Aqre cleaning out each time
        $Cache\{$package\}\{mtime\} = $mtime unless $delete;
     \}

     eval \{$package->handler;\};
     die $@ if $@;

     delete_package($package) if $delete;

     #take a look if you want
     #print Devel::Symdump->rnew($package)->as_string, $/;
 \}

 1;

 _\|_END_\|_

 /* persistent.c */
 #include <EXTERN.h>
 #include <perl.h>

 /* 1 = clean out filename\*(Aqs symbol table after each request,
    0 = don\*(Aqt
 */
 #ifndef DO_CLEAN
 #define DO_CLEAN 0
 #endif

 #define BUFFER_SIZE 1024

 static PerlInterpreter *my_perl = NULL;

 int
 main(int argc, char **argv, char **env)
 \{
     char *embedding[] = \{ "", "persistent.pl", NULL \};
     char *args[] = \{ "", DO_CLEAN, NULL \};
     char filename[BUFFER_SIZE];
     int failing, exitstatus;

     PERL_SYS_INIT3(&argc,&argv,&env);
     if((my_perl = perl_alloc()) == NULL) \{
        fprintf(stderr, "no memory!");
        exit(EXIT_FAILURE);
     \}
     perl_construct(my_perl);

     PL_origalen = 1; /* don\*(Aqt let $0 assignment update the
                         proctitle or embedding[0] */
     failing = perl_parse(my_perl, NULL, 2, embedding, NULL);
     PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
     if(!failing)
        failing = perl_run(my_perl);
     if(!failing) \{
        while(printf("Enter file name: ") &&
              fgets(filename, BUFFER_SIZE, stdin)) \{

            filename[strlen(filename)-1] = \*(Aq\\0\*(Aq; /* strip \\n */
            /* call the subroutine,
                     passing it the filename as an argument */
            args[0] = filename;
            call_argv("Embed::Persistent::eval_file",
                           G_DISCARD | G_EVAL, args);

            /* check $@ */
            if(SvTRUE(ERRSV))
                fprintf(stderr, "eval error: %s\\n", SvPV_nolen(ERRSV));
        \}
     \}

     PL_perl_destruct_level = 0;
     exitstatus = perl_destruct(my_perl);
     perl_free(my_perl);
     PERL_SYS_TERM();
     exit(exitstatus);
 \}
.Ve

Now compile:

.Vb 2
 % cc -o persistent persistent.c \\
        \`perl -MExtUtils::Embed -e ccopts -e ldopts\`
.Ve

Here's an example script file:

.Vb 3
 #test.pl
 my $string = "hello";
 foo($string);

 sub foo \{
     print "foo says: @_\\n";
 \}
.Ve

Now run:

.Vb 7
 % persistent
 Enter file name: test.pl
 foo says: hello
 Enter file name: test.pl
 already compiled Embed::test_2epl->handler
 foo says: hello
 Enter file name: ^C
.Ve

### Execution of \s-1END\s0 blocks

Subsection "Execution of END blocks"
Traditionally \s-1END\s0 blocks have been executed at the end of the perl_run.
This causes problems for applications that never call perl_run. Since
perl 5.7.2 you can specify \f(CW\*(C`PL_exit_flags |= PERL_EXIT_DESTRUCT_END\*(C'
to get the new behaviour. This also enables the running of \s-1END\s0 blocks if
the perl_parse fails and \f(CW\*(C`perl_destruct\*(C' will return the exit value.
.ie n .SS "$0 assignments"
.el .SS "\f(CW$0 assignments"
Subsection "$0 assignments"
When a perl script assigns a value to \f(CW$0 then the perl runtime will
try to make this value show up as the program name reported by \*(L"ps\*(R" by
updating the memory pointed to by the argv passed to **perl_parse()** and
also calling \s-1API\s0 functions like **setproctitle()** where available.  This
behaviour might not be appropriate when embedding perl and can be
disabled by assigning the value \f(CW1 to the variable \f(CW\*(C`PL_origalen\*(C'
before **perl_parse()** is called.

The *persistent.c* example above is for instance likely to segfault
when \f(CW$0 is assigned to if the \f(CW\*(C`PL_origalen = 1;\*(C' assignment is
removed.  This because perl will try to write to the read only memory
of the \f(CW\*(C`embedding[]\*(C' strings.

### Maintaining multiple interpreter instances

Subsection "Maintaining multiple interpreter instances"
Some rare applications will need to create more than one interpreter
during a session.  Such an application might sporadically decide to
release any resources associated with the interpreter.

The program must take care to ensure that this takes place *before*
the next interpreter is constructed.  By default, when perl is not
built with any special options, the global variable
\f(CW\*(C`PL_perl_destruct_level\*(C' is set to \f(CW0, since extra cleaning isn't
usually needed when a program only ever creates a single interpreter
in its entire lifetime.

Setting \f(CW\*(C`PL_perl_destruct_level\*(C' to \f(CW1 makes everything squeaky clean:

.Vb 10
 while(1) \{
     ...
     /* reset global variables here with PL_perl_destruct_level = 1 */
     PL_perl_destruct_level = 1;
     perl_construct(my_perl);
     ...
     /* clean and reset _everything_ during perl_destruct */
     PL_perl_destruct_level = 1;
     perl_destruct(my_perl);
     perl_free(my_perl);
     ...
     /* let\*(Aqs go do it again! */
 \}
.Ve

When *\f(BIperl_destruct()\fI* is called, the interpreter's syntax parse tree
and symbol tables are cleaned up, and global variables are reset.  The
second assignment to \f(CW\*(C`PL_perl_destruct_level\*(C' is needed because
perl_construct resets it to \f(CW0.

Now suppose we have more than one interpreter instance running at the
same time.  This is feasible, but only if you used the Configure option
\f(CW\*(C`-Dusemultiplicity\*(C' or the options \f(CW\*(C`-Dusethreads -Duseithreads\*(C' when
building perl.  By default, enabling one of these Configure options
sets the per-interpreter global variable \f(CW\*(C`PL_perl_destruct_level\*(C' to
\f(CW1, so that thorough cleaning is automatic and interpreter variables
are initialized correctly.  Even if you don't intend to run two or
more interpreters at the same time, but to run them sequentially, like
in the above example, it is recommended to build perl with the
\f(CW\*(C`-Dusemultiplicity\*(C' option otherwise some interpreter variables may
not be initialized correctly between consecutive runs and your
application may crash.

See also \*(L"Thread-aware system interfaces\*(R" in perlxs.

Using \f(CW\*(C`-Dusethreads -Duseithreads\*(C' rather than \f(CW\*(C`-Dusemultiplicity\*(C'
is more appropriate if you intend to run multiple interpreters
concurrently in different threads, because it enables support for
linking in the thread libraries of your system with the interpreter.

Let's give it a try:

.Vb 2
 #include <EXTERN.h>
 #include <perl.h>

 /* we\*(Aqre going to embed two interpreters */

 #define SAY_HELLO "-e", "print qq(Hi, I\*(Aqm $^X\\n)"

 int main(int argc, char **argv, char **env)
 \{
     PerlInterpreter *one_perl, *two_perl;
     char *one_args[] = \{ "one_perl", SAY_HELLO, NULL \};
     char *two_args[] = \{ "two_perl", SAY_HELLO, NULL \};

     PERL_SYS_INIT3(&argc,&argv,&env);
     one_perl = perl_alloc();
     two_perl = perl_alloc();

     PERL_SET_CONTEXT(one_perl);
     perl_construct(one_perl);
     PERL_SET_CONTEXT(two_perl);
     perl_construct(two_perl);

     PERL_SET_CONTEXT(one_perl);
     perl_parse(one_perl, NULL, 3, one_args, (char **)NULL);
     PERL_SET_CONTEXT(two_perl);
     perl_parse(two_perl, NULL, 3, two_args, (char **)NULL);

     PERL_SET_CONTEXT(one_perl);
     perl_run(one_perl);
     PERL_SET_CONTEXT(two_perl);
     perl_run(two_perl);

     PERL_SET_CONTEXT(one_perl);
     perl_destruct(one_perl);
     PERL_SET_CONTEXT(two_perl);
     perl_destruct(two_perl);

     PERL_SET_CONTEXT(one_perl);
     perl_free(one_perl);
     PERL_SET_CONTEXT(two_perl);
     perl_free(two_perl);
     PERL_SYS_TERM();
     exit(EXIT_SUCCESS);
 \}
.Ve

Note the calls to \s-1**PERL_SET_CONTEXT\s0()**.  These are necessary to initialize
the global state that tracks which interpreter is the \*(L"current\*(R" one on
the particular process or thread that may be running it.  It should
always be used if you have more than one interpreter and are making
perl \s-1API\s0 calls on both interpreters in an interleaved fashion.

\s-1PERL_SET_CONTEXT\s0(interp) should also be called whenever \f(CW\*(C`interp\*(C' is
used by a thread that did not create it (using either **perl_alloc()**, or
the more esoteric **perl_clone()**).

Compile as usual:

.Vb 2
 % cc -o multiplicity multiplicity.c \\
  \`perl -MExtUtils::Embed -e ccopts -e ldopts\`
.Ve

Run it, Run it:

.Vb 3
 % multiplicity
 Hi, I\*(Aqm one_perl
 Hi, I\*(Aqm two_perl
.Ve

### Using Perl modules, which themselves use C libraries, from your C program

Subsection "Using Perl modules, which themselves use C libraries, from your C program"
If you've played with the examples above and tried to embed a script
that *\f(BIuse()**s a Perl module (such as \fISocket*) which itself uses a C or \*(C+
library, this probably happened:

.Vb 3
 Can\*(Aqt load module Socket, dynamic loading not available in this perl.
  (You may need to build a new perl executable which either supports
  dynamic loading or has the Socket module statically linked into it.)
.Ve

What's wrong?

Your interpreter doesn't know how to communicate with these extensions
on its own.  A little glue will help.  Up until now you've been
calling *\f(BIperl_parse()\fI*, handing it \s-1NULL\s0 for the second argument:

.Vb 1
 perl_parse(my_perl, NULL, argc, my_argv, NULL);
.Ve

That's where the glue code can be inserted to create the initial contact
between Perl and linked C/\*(C+ routines. Let's take a look some pieces of
*perlmain.c* to see how Perl does this:

.Vb 1
 static void xs_init (pTHX);

 EXTERN_C void boot_DynaLoader (pTHX_ CV* cv);
 EXTERN_C void boot_Socket (pTHX_ CV* cv);


 EXTERN_C void
 xs_init(pTHX)
 \{
        char *file = _\|_FILE_\|_;
        /* DynaLoader is a special case */
        newXS("DynaLoader::boot_DynaLoader", boot_DynaLoader, file);
        newXS("Socket::bootstrap", boot_Socket, file);
 \}
.Ve

Simply put: for each extension linked with your Perl executable
(determined during its initial configuration on your
computer or when adding a new extension),
a Perl subroutine is created to incorporate the extension's
routines.  Normally, that subroutine is named
*\f(BIModule::bootstrap()** and is invoked when you say \fIuse Module*.  In
turn, this hooks into an \s-1XSUB,\s0 *boot_Module*, which creates a Perl
counterpart for each of the extension's XSUBs.  Don't worry about this
part; leave that to the *xsubpp* and extension authors.  If your
extension is dynamically loaded, DynaLoader creates *\f(BIModule::bootstrap()\fI*
for you on the fly.  In fact, if you have a working DynaLoader then there
is rarely any need to link in any other extensions statically.

Once you have this code, slap it into the second argument of *\f(BIperl_parse()\fI*:

.Vb 1
 perl_parse(my_perl, xs_init, argc, my_argv, NULL);
.Ve

Then compile:

.Vb 1
 % cc -o interp interp.c \`perl -MExtUtils::Embed -e ccopts -e ldopts\`

 % interp
   use Socket;
   use SomeDynamicallyLoadedModule;

   print "Now I can use extensions!\\n"\*(Aq
.Ve

**ExtUtils::Embed** can also automate writing the *xs_init* glue code.

.Vb 4
 % perl -MExtUtils::Embed -e xsinit -- -o perlxsi.c
 % cc -c perlxsi.c \`perl -MExtUtils::Embed -e ccopts\`
 % cc -c interp.c  \`perl -MExtUtils::Embed -e ccopts\`
 % cc -o interp perlxsi.o interp.o \`perl -MExtUtils::Embed -e ldopts\`
.Ve

Consult perlxs, perlguts, and perlapi for more details.

### Using embedded Perl with \s-1POSIX\s0 locales

Subsection "Using embedded Perl with POSIX locales"
(See perllocale for information about these.)
When a Perl interpreter normally starts up, it tells the system it wants
to use the system's default locale.  This is often, but not necessarily,
the \*(L"C\*(R" or \*(L"\s-1POSIX\*(R"\s0 locale.  Absent a \f(CW"use\ locale" within the perl
code, this mostly has no effect (but see \*(L"Not within the
scope of \*(R"use locale"" in perllocale).  Also, there is not a problem if the
locale you want to use in your embedded perl is the same as the system
default.  However, this doesn't work if you have set up and want to use
a locale that isn't the system default one.  Starting in Perl v5.20, you
can tell the embedded Perl interpreter that the locale is already
properly set up, and to skip doing its own normal initialization.  It
skips if the environment variable \f(CW\*(C`PERL_SKIP_LOCALE_INIT\*(C' is set (even
if set to 0 or \f(CW"").  A perl that has this capability will define the
C pre-processor symbol \f(CW\*(C`HAS_SKIP_LOCALE_INIT\*(C'.  This allows code that
has to work with multiple Perl versions to do some sort of work-around
when confronted with an earlier Perl.

If your program is using the \s-1POSIX 2008\s0 multi-thread locale
functionality, you should switch into the global locale and set that up
properly before starting the Perl interpreter.  It will then properly
switch back to using the thread-safe functions.

## Hiding Perl_

Header "Hiding Perl_"
If you completely hide the short forms of the Perl public \s-1API,\s0
add -DPERL_NO_SHORT_NAMES to the compilation flags.  This means that
for example instead of writing

.Vb 1
    warn("%d bottles of beer on the wall", bottlecount);
.Ve

you will have to write the explicit full form

.Vb 1
    Perl_warn(aTHX_ "%d bottles of beer on the wall", bottlecount);
.Ve

(See \*(L"Background and \s-1PERL_IMPLICIT_CONTEXT\*(R"\s0 in perlguts for the explanation
of the \f(CW\*(C`aTHX_\*(C'. )  Hiding the short forms is very useful for avoiding
all sorts of nasty (C preprocessor or otherwise) conflicts with other
software packages (Perl defines about 2400 APIs with these short names,
take or leave few hundred, so there certainly is room for conflict.)

## MORAL

Header "MORAL"
You can sometimes *write faster code* in C, but
you can always *write code faster* in Perl.  Because you can use
each from the other, combine them as you wish.

## AUTHOR

Header "AUTHOR"
Jon Orwant <*orwant@media.mit.edu*> and Doug MacEachern
<*dougm@covalent.net*>, with small contributions from Tim Bunce, Tom
Christiansen, Guy Decoux, Hallvard Furuseth, Dov Grobgeld, and Ilya
Zakharevich.

Doug MacEachern has an article on embedding in Volume 1, Issue 4 of
The Perl Journal ( <http://www.tpj.com/> ).  Doug is also the developer of the
most widely-used Perl embedding: the mod_perl system
(perl.apache.org), which embeds Perl in the Apache web server.
Oracle, Binary Evolution, ActiveState, and Ben Sugars's nsapi_perl
have used this model for Oracle, Netscape and Internet Information
Server Perl plugins.

## COPYRIGHT

Header "COPYRIGHT"
Copyright (C) 1995, 1996, 1997, 1998 Doug MacEachern and Jon Orwant.  All
Rights Reserved.

This document may be distributed under the same terms as Perl itself.
