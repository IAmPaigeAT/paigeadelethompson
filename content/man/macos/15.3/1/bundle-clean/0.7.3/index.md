+++
date = "Sun Feb 16 04:48:25 2025"
author = "None Specified"
manpage_section = "1"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_name = "bundle-clean"
title = "bundle-clean(1)"
detected_package_version = "0.7.3"
description = "This command will remove all unused gems in your bundler directory. This is useful when you have made many changes to your gem dependencies. --dry-run Print the changes, but do not clean the unused gems. --force Force a clean even if --path is not..."
operating_system = "macos"
+++

.
"BUNDLE-CLEAN" "1" "November 2018" "" ""
.

## NAME

**bundle-clean** - Cleans up unused gems in your bundler directory
.

## SYNOPSIS

**bundle clean** [--dry-run] [--force]
.

## DESCRIPTION

This command will remove all unused gems in your bundler directory\. This is useful when you have made many changes to your gem dependencies\.
.

## OPTIONS

.

**--dry-run**
Print the changes, but do not clean the unused gems\.
.

**--force**
Force a clean even if **--path** is not set\.

