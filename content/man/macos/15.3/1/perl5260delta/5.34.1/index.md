+++
operating_system = "macos"
operating_system_version = "15.3"
date = "2022-02-19"
manpage_name = "perl5260delta"
description = "This document describes the differences between the 5.24.0 release and the 5.26.0 release. This release includes three updates with widespread effects: f(CW. no longer in f(CW@INC For security reasons, the current directory (f(CW.) is no longe..."
author = "None Specified"
title = "perl5260delta(1)"
manpage_format = "troff"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5260DELTA 1"
PERL5260DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5260delta - what is new for perl v5.26.0

## DESCRIPTION

Header "DESCRIPTION"
This document describes the differences between the 5.24.0 release and the
5.26.0 release.

## Notice

Header "Notice"
This release includes three updates with widespread effects:

- \(bu
\f(CW"." no longer in \f(CW@INC
.Sp
For security reasons, the current directory (\f(CW".") is no longer included
by default at the end of the module search path (\f(CW@INC). This may have
widespread implications for the building, testing and installing of
modules, and for the execution of scripts.  See the section
"Removal of the current directory (\f(CW".") from \f(CW@INC"
for the full details.

- \(bu
\f(CW\*(C`do\*(C' may now warn
.Sp
\f(CW\*(C`do\*(C' now gives a deprecation warning when it fails to load a file which
it would have loaded had \f(CW"." been in \f(CW@INC.

- \(bu
In regular expression patterns, a literal left brace \f(CW"\{"
should be escaped
.Sp
See "Unescaped literal \f(CW"\{" characters in regular expression patterns are no longer permissible".

## Core Enhancements

Header "Core Enhancements"

### Lexical subroutines are no longer experimental

Subsection "Lexical subroutines are no longer experimental"
Using the \f(CW\*(C`lexical_subs\*(C' feature introduced in v5.18 no longer emits a warning.  Existing
code that disables the \f(CW\*(C`experimental::lexical_subs\*(C' warning category
that the feature previously used will continue to work.  The
\f(CW\*(C`lexical_subs\*(C' feature has no effect; all Perl code can use lexical
subroutines, regardless of what feature declarations are in scope.

### Indented Here-documents

Subsection "Indented Here-documents"
This adds a new modifier \f(CW"~" to here-docs that tells the parser
that it should look for \f(CW\*(C`/^\\s*$DELIM\\n/\*(C' as the closing delimiter.

These syntaxes are all supported:

.Vb 8
    <<~EOF;
    <<~\\EOF;
    <<~\*(AqEOF\*(Aq;
    <<~"EOF";
    <<~\`EOF\`;
    <<~ \*(AqEOF\*(Aq;
    <<~ "EOF";
    <<~ \`EOF\`;
.Ve

The \f(CW"~" modifier will strip, from each line in the here-doc, the
same whitespace that appears before the delimiter.

Newlines will be copied as-is, and lines that don't include the
proper beginning whitespace will cause perl to croak.

For example:

.Vb 5
    if (1) \{
      print <<~EOF;
        Hello there
        EOF
    \}
.Ve

prints \*(L"Hello there\\n\*(R" with no leading whitespace.
.ie n .SS "New regular expression modifier ""/xx"""
.el .SS "New regular expression modifier \f(CW/xx"
Subsection "New regular expression modifier /xx"
Specifying two \f(CW"x" characters to modify a regular expression pattern
does everything that a single one does, but additionally \s-1TAB\s0 and \s-1SPACE\s0
characters within a bracketed character class are generally ignored and
can be added to improve readability, like
\f(CW\*(C`/[\ ^\ A-Z\ d-f\ p-x\ ]/xx\*(C'.  Details are at
\*(L"/x and /xx\*(R" in perlre.
.ie n .SS """@\{^CAPTURE\}"", ""%\{^CAPTURE\}"", and ""%\{^CAPTURE_ALL\}"""
.el .SS "\f(CW@\{^CAPTURE\}, \f(CW%\{^CAPTURE\}, and \f(CW%\{^CAPTURE_ALL\}"
Subsection "@\{^CAPTURE\}, %\{^CAPTURE\}, and %\{^CAPTURE_ALL\}"
\f(CW\*(C`@\{^CAPTURE\}\*(C' exposes the capture buffers of the last match as an
array.  So \f(CW$1 is \f(CW\*(C`$\{^CAPTURE\}[0]\*(C'.  This is a more efficient equivalent
to code like \f(CW\*(C`substr($matched_string,$-[0],$+[0]-$-[0])\*(C', and you don't
have to keep track of the \f(CW$matched_string either.  This variable has no
single character equivalent.  Note that, like the other regex magic variables,
the contents of this variable is dynamic; if you wish to store it beyond
the lifetime of the match you must copy it to another array.

\f(CW\*(C`%\{^CAPTURE\}\*(C' is equivalent to \f(CW\*(C`%+\*(C' (*i.e.*, named captures).  Other than
being more self-documenting there is no difference between the two forms.

\f(CW\*(C`%\{^CAPTURE_ALL\}\*(C' is equivalent to \f(CW\*(C`%-\*(C' (*i.e.*, all named captures).
Other than being more self-documenting there is no difference between the
two forms.

### Declaring a reference to a variable

Subsection "Declaring a reference to a variable"
As an experimental feature, Perl now allows the referencing operator to come
after \f(CW\*(C`my()\*(C', \f(CW\*(C`state()\*(C',
\f(CW\*(C`our()\*(C', or \f(CW\*(C`local()\*(C'.  This syntax must
be enabled with \f(CW\*(C`use feature \*(Aqdeclared_refs\*(Aq\*(C'.  It is experimental, and will
warn by default unless \f(CW\*(C`no warnings \*(Aqexperimental::refaliasing\*(Aq\*(C' is in effect.
It is intended mainly for use in assignments to references.  For example:

.Vb 2
    use experimental \*(Aqrefaliasing\*(Aq, \*(Aqdeclared_refs\*(Aq;
    my \\$a = \\$b;
.Ve

See \*(L"Assigning to References\*(R" in perlref for more details.

### Unicode 9.0 is now supported

Subsection "Unicode 9.0 is now supported"
A list of changes is at <http://www.unicode.org/versions/Unicode9.0.0/>.
Modules that are shipped with core Perl but not maintained by p5p do not
necessarily support Unicode 9.0.  Unicode::Normalize does work on 9.0.
.ie n .SS "Use of ""\\p\{\f(CIscript\}"" uses the improved Script_Extensions property"
.el .SS "Use of \f(CW\\p\{\f(CIscript\f(CW\} uses the improved Script_Extensions property"
Subsection "Use of p\{script\} uses the improved Script_Extensions property"
Unicode 6.0 introduced an improved form of the Script (\f(CW\*(C`sc\*(C') property, and
called it Script_Extensions (\f(CW\*(C`scx\*(C').  Perl now uses this improved
version when a property is specified as just \f(CW\*(C`\\p\{\f(CIscript\f(CW\}\*(C'.  This
should make programs more accurate when determining if a character is
used in a given script, but there is a slight chance of breakage for
programs that very specifically needed the old behavior.  The meaning of
compound forms, like \f(CW\*(C`\\p\{sc=\f(CIscript\f(CW\}\*(C' are unchanged.  See
\*(L"Scripts\*(R" in perlunicode.

### Perl can now do default collation in \s-1UTF-8\s0 locales on platforms that support it

Subsection "Perl can now do default collation in UTF-8 locales on platforms that support it"
Some platforms natively do a reasonable job of collating and sorting in
\s-1UTF-8\s0 locales.  Perl now works with those.  For portability and full
control, Unicode::Collate is still recommended, but now you may
not need to do anything special to get good-enough results, depending on
your application.  See
"Category \f(CW\*(C`LC_COLLATE\*(C': Collation: Text Comparisons and Sorting" in perllocale.
.ie n .SS "Better locale collation of strings containing embedded ""NUL"" characters"
.el .SS "Better locale collation of strings containing embedded \f(CWNUL characters"
Subsection "Better locale collation of strings containing embedded NUL characters"
In locales that have multi-level character weights, \f(CW\*(C`NUL\*(C's are now
ignored at the higher priority ones.  There are still some gotchas in
some strings, though.  See
"Collation of strings containing embedded \f(CW\*(C`NUL\*(C' characters" in perllocale.
.ie n .SS """CORE"" subroutines for hash and array functions callable via reference"
.el .SS "\f(CWCORE subroutines for hash and array functions callable via reference"
Subsection "CORE subroutines for hash and array functions callable via reference"
The hash and array functions in the \f(CW\*(C`CORE\*(C' namespace (\f(CW\*(C`keys\*(C', \f(CW\*(C`each\*(C',
\f(CW\*(C`values\*(C', \f(CW\*(C`push\*(C', \f(CW\*(C`pop\*(C', \f(CW\*(C`shift\*(C', \f(CW\*(C`unshift\*(C' and \f(CW\*(C`splice\*(C') can now
be called with ampersand syntax (\f(CW\*(C`&CORE::keys(\\%hash\*(C') and via reference
(\f(CW\*(C`my $k = \CORE::keys; $k->(\\%hash)\*(C').  Previously they could only be
used when inlined.

### New Hash Function For 64-bit Builds

Subsection "New Hash Function For 64-bit Builds"
We have switched to a hybrid hash function to better balance
performance for short and long keys.

For short keys, 16 bytes and under, we use an optimised variant of
One At A Time Hard, and for longer keys we use Siphash 1-3.  For very
long keys this is a big improvement in performance.  For shorter keys
there is a modest improvement.

## Security

Header "Security"
.ie n .SS "Removal of the current directory (""."") from @INC"
.el .SS "Removal of the current directory (\f(CW``.'') from \f(CW@INC"
Subsection "Removal of the current directory (""."") from @INC"
The perl binary includes a default set of paths in \f(CW@INC.  Historically
it has also included the current directory (\f(CW".") as the final entry,
unless run with taint mode enabled (\f(CW\*(C`perl -T\*(C').  While convenient, this has
security implications: for example, where a script attempts to load an
optional module when its current directory is untrusted (such as */tmp*),
it could load and execute code from under that directory.

Starting with v5.26, \f(CW"." is always removed by default, not just under
tainting.  This has major implications for installing modules and executing
scripts.

The following new features have been added to help ameliorate these
issues.

- \(bu
*Configure -Udefault_inc_excludes_dot*
.Sp
There is a new *Configure* option, \f(CW\*(C`default_inc_excludes_dot\*(C' (enabled
by default) which builds a perl executable without \f(CW"."; unsetting this
option using \f(CW\*(C`-U\*(C' reverts perl to the old behaviour.  This may fix your
path issues but will reintroduce all the security concerns, so don't
build a perl executable like this unless you're *really* confident that
such issues are not a concern in your environment.

- \(bu
\f(CW\*(C`PERL_USE_UNSAFE_INC\*(C'
.Sp
There is a new environment variable recognised by the perl interpreter.
If this variable has the value 1 when the perl interpreter starts up,
then \f(CW"." will be automatically appended to \f(CW@INC (except under tainting).
.Sp
This allows you restore the old perl interpreter behaviour on a
case-by-case basis.  But note that this is intended to be a temporary crutch,
and this feature will likely be removed in some future perl version.
It is currently set by the \f(CW\*(C`cpan\*(C' utility and \f(CW\*(C`Test::Harness\*(C' to
ease installation of \s-1CPAN\s0 modules which have not been updated to handle the
lack of dot.  Once again, don't use this unless you are sure that this
will not reintroduce any security concerns.

- \(bu
A new deprecation warning issued by \f(CW\*(C`do\*(C'.
.Sp
While it is well-known that \f(CW\*(C`use\*(C' and \f(CW\*(C`require\*(C' use \f(CW@INC to search
for the file to load, many people don't realise that \f(CW\*(C`do "file"\*(C' also
searches \f(CW@INC if the file is a relative path.  With the removal of \f(CW".",
a simple \f(CW\*(C`do "file.pl"\*(C' will fail to read in and execute \f(CW\*(C`file.pl\*(C' from
the current directory.  Since this is commonly expected behaviour, a new
deprecation warning is now issued whenever \f(CW\*(C`do\*(C' fails to load a file which
it otherwise would have found if a dot had been in \f(CW@INC.

Here are some things script and module authors may need to do to make
their software work in the new regime.

- \(bu
Script authors
.Sp
If the issue is within your own code (rather than within included
modules), then you have two main options.  Firstly, if you are confident
that your script will only be run within a trusted directory (under which
you expect to find trusted files and modules), then add \f(CW"." back into the
path; *e.g.*:
.Sp
.Vb 6
    BEGIN \{
        my $dir = "/some/trusted/directory";
        chdir $dir or die "Can\*(Aqt chdir to $dir: $!\\n";
        # safe now
        push @INC, \*(Aq.\*(Aq;
    \}

    use "Foo::Bar"; # may load /some/trusted/directory/Foo/Bar.pm
    do "config.pl"; # may load /some/trusted/directory/config.pl
.Ve
.Sp
On the other hand, if your script is intended to be run from within
untrusted directories (such as */tmp*), then your script suddenly failing
to load files may be indicative of a security issue.  You most likely want
to replace any relative paths with full paths; for example,
.Sp
.Vb 1
    do "foo_config.pl"
.Ve
.Sp
might become
.Sp
.Vb 1
    do "$ENV\{HOME\}/foo_config.pl"
.Ve
.Sp
If you are absolutely certain that you want your script to load and
execute a file from the current directory, then use a \f(CW\*(C`./\*(C' prefix; for
example:
.Sp
.Vb 1
    do "./foo_config.pl"
.Ve

- \(bu
Installing and using \s-1CPAN\s0 modules
.Sp
If you install a \s-1CPAN\s0 module using an automatic tool like \f(CW\*(C`cpan\*(C', then
this tool will itself set the \f(CW\*(C`PERL_USE_UNSAFE_INC\*(C' environment variable
while building and testing the module, which may be sufficient to install
a distribution which hasn't been updated to be dot-aware.  If you want to
install such a module manually, then you'll need to replace the
traditional invocation:
.Sp
.Vb 1
    perl Makefile.PL && make && make test && make install
.Ve
.Sp
with something like
.Sp
.Vb 2
    (export PERL_USE_UNSAFE_INC=1; \\
     perl Makefile.PL && make && make test && make install)
.Ve
.Sp
Note that this only helps build and install an unfixed module.  It's
possible for the tests to pass (since they were run under
\f(CW\*(C`PERL_USE_UNSAFE_INC=1\*(C'), but for the module itself to fail to perform
correctly in production.  In this case, you may have to temporarily modify
your script until a fixed version of the module is released.
For example:
.Sp
.Vb 6
    use Foo::Bar;
    \{
        local @INC = (@INC, \*(Aq.\*(Aq);
        # assuming read_config() needs \*(Aq.\*(Aq in @INC
        $config = Foo::Bar->read_config();
    \}
.Ve
.Sp
This is only rarely expected to be necessary.  Again, if doing this,
assess the resultant risks first.

- \(bu
Module Authors
.Sp
If you maintain a \s-1CPAN\s0 distribution, it may need updating to run in
a dotless environment.  Although \f(CW\*(C`cpan\*(C' and other such tools will
currently set the \f(CW\*(C`PERL_USE_UNSAFE_INC\*(C' during module build, this is a
temporary workaround for the set of modules which rely on \f(CW"." being in
\f(CW@INC for installation and testing, and this may mask deeper issues.  It
could result in a module which passes tests and installs, but which
fails at run time.
.Sp
During build, test, and install, it will normally be the case that any perl
processes will be executing directly within the root directory of the
untarred distribution, or a known subdirectory of that, such as *t/*.  It
may well be that *Makefile.PL* or *t/foo.t* will attempt to include
local modules and configuration files using their direct relative
filenames, which will now fail.
.Sp
However, as described above, automatic tools like *cpan* will (for now)
set the \f(CW\*(C`PERL_USE_UNSAFE_INC\*(C' environment variable, which introduces
dot during a build.
.Sp
This makes it likely that your existing build and test code will work, but
this may mask issues with your code which only manifest when used after
install.  It is prudent to try and run your build process with that
variable explicitly disabled:
.Sp
.Vb 2
    (export PERL_USE_UNSAFE_INC=0; \\
     perl Makefile.PL && make && make test && make install)
.Ve
.Sp
This is more likely to show up any potential problems with your module's
build process, or even with the module itself.  Fixing such issues will
ensure both that your module can again be installed manually, and that
it will still build once the \f(CW\*(C`PERL_USE_UNSAFE_INC\*(C' crutch goes away.
.Sp
When fixing issues in tests due to the removal of dot from \f(CW@INC,
reinsertion of dot into \f(CW@INC should be performed with caution, for this
too may suppress real errors in your runtime code.  You are encouraged
wherever possible to apply the aforementioned approaches with explicit
absolute/relative paths, or to relocate your needed files into a
subdirectory and insert that subdirectory into \f(CW@INC instead.
.Sp
If your runtime code has problems under the dotless \f(CW@INC, then the comments
above on how to fix for script authors will mostly apply here too.  Bear in
mind though that it is considered bad form for a module to globally add a dot to
\f(CW@INC, since it introduces both a security risk and hides issues of
accidentally requiring dot in \f(CW@INC, as explained above.

### Escaped colons and relative paths in \s-1PATH\s0

Subsection "Escaped colons and relative paths in PATH"
On Unix systems, Perl treats any relative paths in the \f(CW\*(C`PATH\*(C' environment
variable as tainted when starting a new process.  Previously, it was
allowing a backslash to escape a colon (unlike the \s-1OS\s0), consequently
allowing relative paths to be considered safe if the \s-1PATH\s0 was set to
something like \f(CW\*(C`/\\:.\*(C'.  The check has been fixed to treat \f(CW"." as tainted
in that example.
.ie n .SS "New ""-Di"" switch is now required for PerlIO debugging output"
.el .SS "New \f(CW-Di switch is now required for PerlIO debugging output"
Subsection "New -Di switch is now required for PerlIO debugging output"
This is used for debugging of code within PerlIO to avoid recursive
calls.  Previously this output would be sent to the file specified
by the \f(CW\*(C`PERLIO_DEBUG\*(C' environment variable if perl wasn't running
setuid and the \f(CW\*(C`-T\*(C' or \f(CW\*(C`-t\*(C' switches hadn't been parsed yet.

If perl performed output at a point where it hadn't yet parsed its
switches this could result in perl creating or overwriting the file
named by \f(CW\*(C`PERLIO_DEBUG\*(C' even when the \f(CW\*(C`-T\*(C' switch had been supplied.

Perl now requires the \f(CW\*(C`-Di\*(C' switch to be present before it will produce
PerlIO debugging
output.  By default this is written to \f(CW\*(C`stderr\*(C', but can optionally
be redirected to a file by setting the \f(CW\*(C`PERLIO_DEBUG\*(C' environment
variable.

If perl is running setuid or the \f(CW\*(C`-T\*(C' switch was supplied,
\f(CW\*(C`PERLIO_DEBUG\*(C' is ignored and the debugging output is sent to
\f(CW\*(C`stderr\*(C' as for any other \f(CW\*(C`-D\*(C' switch.

## Incompatible Changes

Header "Incompatible Changes"
.ie n .SS "Unescaped literal ""\{"" characters in regular expression patterns are no longer permissible"
.el .SS "Unescaped literal \f(CW``\{'' characters in regular expression patterns are no longer permissible"
Subsection "Unescaped literal ""\{"" characters in regular expression patterns are no longer permissible"
You have to now say something like \f(CW"\\\{" or \f(CW"[\{]" to specify to
match a \s-1LEFT CURLY BRACKET\s0; otherwise, it is a fatal pattern compilation
error.  This change will allow future extensions to the language.

These have been deprecated since v5.16, with a deprecation message
raised for some uses starting in v5.22.  Unfortunately, the code added
to raise the message was buggy and failed to warn in some cases where
it should have.  Therefore, enforcement of this ban for these cases is
deferred until Perl 5.30, but the code has been fixed to raise a
default-on deprecation message for them in the meantime.

Some uses of literal \f(CW"\{" occur in contexts where we do not foresee
the meaning ever being anything but the literal, such as the very first
character in the pattern, or after a \f(CW"|" meaning alternation.  Thus

.Vb 1
 qr/\{fee|\{fie/
.Ve

matches either of the strings \f(CW\*(C`\{fee\*(C' or \f(CW\*(C`\{fie\*(C'.  To avoid forcing
unnecessary code changes, these uses do not need to be escaped, and no
warning is raised about them, and there are no current plans to change this.

But it is always correct to escape \f(CW"\{", and the simple rule to
remember is to always do so.

See Unescaped left brace in regex is illegal here.
.ie n .SS """scalar(%hash)"" return signature changed"
.el .SS "\f(CWscalar(%hash) return signature changed"
Subsection "scalar(%hash) return signature changed"
The value returned for \f(CW\*(C`scalar(%hash)\*(C' will no longer show information about
the buckets allocated in the hash.  It will simply return the count of used
keys.  It is thus equivalent to \f(CW\*(C`0+keys(%hash)\*(C'.

A form of backward compatibility is provided via
\f(CW\*(C`Hash::Util::bucket_ratio()\*(C' which provides
the same behavior as
\f(CW\*(C`scalar(%hash)\*(C' provided in Perl 5.24 and earlier.
.ie n .SS """keys"" returned from an lvalue subroutine"
.el .SS "\f(CWkeys returned from an lvalue subroutine"
Subsection "keys returned from an lvalue subroutine"
\f(CW\*(C`keys\*(C' returned from an lvalue subroutine can no longer be assigned
to in list context.

.Vb 4
    sub foo : lvalue \{ keys(%INC) \}
    (foo) = 3; # death
    sub bar : lvalue \{ keys(@_) \}
    (bar) = 3; # also an error
.Ve

This makes the lvalue sub case consistent with \f(CW\*(C`(keys %hash) = ...\*(C' and
\f(CW\*(C`(keys @_) = ...\*(C', which are also errors.
[\s-1GH\s0 #15339] <https://github.com/Perl/perl5/issues/15339>
.ie n .SS "The ""$\{^ENCODING\}"" facility has been removed"
.el .SS "The \f(CW$\{^ENCODING\} facility has been removed"
Subsection "The $\{^ENCODING\} facility has been removed"
The special behaviour associated with assigning a value to this variable
has been removed.  As a consequence, the encoding pragma's default mode
is no longer supported.  If
you still need to write your source code in encodings other than \s-1UTF-8,\s0 use a
source filter such as Filter::Encoding on \s-1CPAN\s0 or encoding's \f(CW\*(C`Filter\*(C'
option.
.ie n .SS """POSIX::tmpnam()"" has been removed"
.el .SS "\f(CWPOSIX::tmpnam() has been removed"
Subsection "POSIX::tmpnam() has been removed"
The fundamentally unsafe \f(CW\*(C`tmpnam()\*(C' interface was deprecated in
Perl 5.22 and has now been removed.  In its place, you can use,
for example, the File::Temp interfaces.

### require ::Foo::Bar is now illegal.

Subsection "require ::Foo::Bar is now illegal."
Formerly, \f(CW\*(C`require ::Foo::Bar\*(C' would try to read */Foo/Bar.pm*.  Now any
bareword require which starts with a double colon dies instead.

### Literal control character variable names are no longer permissible

Subsection "Literal control character variable names are no longer permissible"
A variable name may no longer contain a literal control character under
any circumstances.  These previously were allowed in single-character
names on \s-1ASCII\s0 platforms, but have been deprecated there since Perl
5.20.  This affects things like \f(CW\*(C`$\f(CI\\cT\f(CW\*(C', where *\\cT* is a literal
control (such as a \f(CW\*(C`NAK\*(C' or \f(CW\*(C`NEGATIVE ACKNOWLEDGE\*(C' character) in the
source code.
.ie n .SS """NBSP"" is no longer permissible in ""\\N\{...\}"""
.el .SS "\f(CWNBSP is no longer permissible in \f(CW\\N\{...\}"
Subsection "NBSP is no longer permissible in N\{...\}"
The name of a character may no longer contain non-breaking spaces.  It
has been deprecated to do so since Perl 5.22.

## Deprecations

Header "Deprecations"

### String delimiters that arent stand-alone graphemes are now deprecated

Subsection "String delimiters that aren't stand-alone graphemes are now deprecated"
For Perl to eventually allow string delimiters to be Unicode
grapheme clusters (which look like a single character, but may be
a sequence of several ones), we have to stop allowing a single character
delimiter that isn't a grapheme by itself.  These are unlikely to exist
in actual code, as they would typically display as attached to the
character in front of them.
.ie n .SS """\\c\f(CIX"" that maps to a printable is no longer deprecated"
.el .SS "\f(CW\\c\f(CIX\f(CW that maps to a printable is no longer deprecated"
Subsection "cX that maps to a printable is no longer deprecated"
This means we have no plans to remove this feature.  It still raises a
warning, but only if syntax warnings are enabled.  The feature was
originally intended to be a way to express non-printable characters that
don't have a mnemonic (\f(CW\*(C`\\t\*(C' and \f(CW\*(C`\\n\*(C' are mnemonics for two
non-printable characters, but most non-printables don't have a
mnemonic.)  But the feature can be used to specify a few printable
characters, though those are more clearly expressed as the printable
itself.  See
<http://www.nntp.perl.org/group/perl.perl5.porters/2017/02/msg242944.html>.

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
A hash in boolean context is now sometimes faster, *e.g.*
.Sp
.Vb 1
    if (!%h) \{ ... \}
.Ve
.Sp
This was already special-cased, but some cases were missed (such as
\f(CW\*(C`grep %$_, @AoH\*(C'), and even the ones which weren't have been improved.

- \(bu
New Faster Hash Function on 64 bit builds
.Sp
We use a different hash function for short and long keys.  This should
improve performance and security, especially for long keys.

- \(bu
readline is faster
.Sp
Reading from a file line-by-line with \f(CW\*(C`readline()\*(C' or \f(CW\*(C`<>\*(C' should
now typically be faster due to a better implementation of the code that
searches for the next newline character.

- \(bu
Assigning one reference to another, *e.g.* \f(CW\*(C`$ref1 = $ref2\*(C' has been
optimized in some cases.

- \(bu
Remove some exceptions to creating Copy-on-Write strings. The string
buffer growth algorithm has been slightly altered so that you're less
likely to encounter a string which can't be COWed.

- \(bu
Better optimise array and hash assignment: where an array or hash appears
in the \s-1LHS\s0 of a list assignment, such as \f(CW\*(C`(..., @a) = (...);\*(C', it's
likely to be considerably faster, especially if it involves emptying the
array/hash. For example, this code runs about a third faster compared to
Perl 5.24.0:
.Sp
.Vb 5
    my @a;
    for my $i (1..10_000_000) \{
        @a = (1,2,3);
        @a = ();
    \}
.Ve

- \(bu
Converting a single-digit string to a number is now substantially faster.

- \(bu
The \f(CW\*(C`split\*(C' builtin is now slightly faster in many cases: in particular
for the two specially-handled forms
.Sp
.Vb 2
    my    @a = split ...;
    local @a = split ...;
.Ve

- \(bu
The rather slow implementation for the experimental subroutine signatures
feature has been made much faster; it is now comparable in speed with the
traditional \f(CW\*(C`my ($a, $b, @c) = @_\*(C'.

- \(bu
Bareword constant strings are now permitted to take part in constant
folding.  They were originally exempted from constant folding in August 1999,
during the development of Perl 5.6, to ensure that \f(CW\*(C`use strict "subs"\*(C'
would still apply to bareword constants.  That has now been accomplished a
different way, so barewords, like other constants, now gain the performance
benefits of constant folding.
.Sp
This also means that void-context warnings on constant expressions of
barewords now report the folded constant operand, rather than the operation;
this matches the behaviour for non-bareword constants.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
IO::Compress has been upgraded from version 2.069 to 2.074.

- \(bu
Archive::Tar has been upgraded from version 2.04 to 2.24.

- \(bu
arybase has been upgraded from version 0.11 to 0.12.

- \(bu
attributes has been upgraded from version 0.27 to 0.29.
.Sp
The deprecation message for the \f(CW\*(C`:unique\*(C' and \f(CW\*(C`:locked\*(C' attributes
now mention that they will disappear in Perl 5.28.

- \(bu
B has been upgraded from version 1.62 to 1.68.

- \(bu
B::Concise has been upgraded from version 0.996 to 0.999.
.Sp
Its output is now more descriptive for \f(CW\*(C`op_private\*(C' flags.

- \(bu
B::Debug has been upgraded from version 1.23 to 1.24.

- \(bu
B::Deparse has been upgraded from version 1.37 to 1.40.

- \(bu
B::Xref has been upgraded from version 1.05 to 1.06.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
base has been upgraded from version 2.23 to 2.25.

- \(bu
bignum has been upgraded from version 0.42 to 0.47.

- \(bu
Carp has been upgraded from version 1.40 to 1.42.

- \(bu
charnames has been upgraded from version 1.43 to 1.44.

- \(bu
Compress::Raw::Bzip2 has been upgraded from version 2.069 to 2.074.

- \(bu
Compress::Raw::Zlib has been upgraded from version 2.069 to 2.074.

- \(bu
Config::Perl::V has been upgraded from version 0.25 to 0.28.

- \(bu
\s-1CPAN\s0 has been upgraded from version 2.11 to 2.18.

- \(bu
CPAN::Meta has been upgraded from version 2.150005 to 2.150010.

- \(bu
Data::Dumper has been upgraded from version 2.160 to 2.167.
.Sp
The \s-1XS\s0 implementation now supports Deparse.

- \(bu
DB_File has been upgraded from version 1.835 to 1.840.

- \(bu
Devel::Peek has been upgraded from version 1.23 to 1.26.

- \(bu
Devel::PPPort has been upgraded from version 3.32 to 3.35.

- \(bu
Devel::SelfStubber has been upgraded from version 1.05 to 1.06.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
diagnostics has been upgraded from version 1.34 to 1.36.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
Digest has been upgraded from version 1.17 to 1.17_01.

- \(bu
Digest::MD5 has been upgraded from version 2.54 to 2.55.

- \(bu
Digest::SHA has been upgraded from version 5.95 to 5.96.

- \(bu
DynaLoader has been upgraded from version 1.38 to 1.42.

- \(bu
Encode has been upgraded from version 2.80 to 2.88.

- \(bu
encoding has been upgraded from version 2.17 to 2.19.
.Sp
This module's default mode is no longer supported.  It now
dies when imported, unless the \f(CW\*(C`Filter\*(C' option is being used.

- \(bu
encoding::warnings has been upgraded from version 0.12 to 0.13.
.Sp
This module is no longer supported.  It emits a warning to
that effect and then does nothing.

- \(bu
Errno has been upgraded from version 1.25 to 1.28.
.Sp
It now documents that using \f(CW\*(C`%!\*(C' automatically loads Errno for you.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
ExtUtils::Embed has been upgraded from version 1.33 to 1.34.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
ExtUtils::MakeMaker has been upgraded from version 7.10_01 to 7.24.

- \(bu
ExtUtils::Miniperl has been upgraded from version 1.05 to 1.06.

- \(bu
ExtUtils::ParseXS has been upgraded from version 3.31 to 3.34.

- \(bu
ExtUtils::Typemaps has been upgraded from version 3.31 to 3.34.

- \(bu
feature has been upgraded from version 1.42 to 1.47.

- \(bu
File::Copy has been upgraded from version 2.31 to 2.32.

- \(bu
File::Fetch has been upgraded from version 0.48 to 0.52.

- \(bu
File::Glob has been upgraded from version 1.26 to 1.28.
.Sp
It now Issues a deprecation message for \f(CW\*(C`File::Glob::glob()\*(C'.

- \(bu
File::Spec has been upgraded from version 3.63 to 3.67.

- \(bu
FileHandle has been upgraded from version 2.02 to 2.03.

- \(bu
Filter::Simple has been upgraded from version 0.92 to 0.93.
.Sp
It no longer treats \f(CW\*(C`no MyFilter\*(C' immediately following \f(CW\*(C`use MyFilter\*(C' as
end-of-file.
[\s-1GH\s0 #11853] <https://github.com/Perl/perl5/issues/11853>

- \(bu
Getopt::Long has been upgraded from version 2.48 to 2.49.

- \(bu
Getopt::Std has been upgraded from version 1.11 to 1.12.

- \(bu
Hash::Util has been upgraded from version 0.19 to 0.22.

- \(bu
HTTP::Tiny has been upgraded from version 0.056 to 0.070.
.Sp
Internal 599-series errors now include the redirect history.

- \(bu
I18N::LangTags has been upgraded from version 0.40 to 0.42.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
\s-1IO\s0 has been upgraded from version 1.36 to 1.38.

- \(bu
IO::Socket::IP has been upgraded from version 0.37 to 0.38.

- \(bu
IPC::Cmd has been upgraded from version 0.92 to 0.96.

- \(bu
IPC::SysV has been upgraded from version 2.06_01 to 2.07.

- \(bu
\s-1JSON::PP\s0 has been upgraded from version 2.27300 to 2.27400_02.

- \(bu
lib has been upgraded from version 0.63 to 0.64.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
List::Util has been upgraded from version 1.42_02 to 1.46_02.

- \(bu
Locale::Codes has been upgraded from version 3.37 to 3.42.

- \(bu
Locale::Maketext has been upgraded from version 1.26 to 1.28.

- \(bu
Locale::Maketext::Simple has been upgraded from version 0.21 to 0.21_01.

- \(bu
Math::BigInt has been upgraded from version 1.999715 to 1.999806.

- \(bu
Math::BigInt::FastCalc has been upgraded from version 0.40 to 0.5005.

- \(bu
Math::BigRat has been upgraded from version 0.260802 to 0.2611.

- \(bu
Math::Complex has been upgraded from version 1.59 to 1.5901.

- \(bu
Memoize has been upgraded from version 1.03 to 1.03_01.

- \(bu
Module::CoreList has been upgraded from version 5.20170420 to 5.20170530.

- \(bu
Module::Load::Conditional has been upgraded from version 0.64 to 0.68.

- \(bu
Module::Metadata has been upgraded from version 1.000031 to 1.000033.

- \(bu
mro has been upgraded from version 1.18 to 1.20.

- \(bu
Net::Ping has been upgraded from version 2.43 to 2.55.
.Sp
IPv6 addresses and \f(CW\*(C`AF_INET6\*(C' sockets are now supported, along with several
other enhancements.

- \(bu
\s-1NEXT\s0 has been upgraded from version 0.65 to 0.67.

- \(bu
Opcode has been upgraded from version 1.34 to 1.39.

- \(bu
open has been upgraded from version 1.10 to 1.11.

- \(bu
OS2::Process has been upgraded from version 1.11 to 1.12.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
overload has been upgraded from version 1.26 to 1.28.
.Sp
Its compilation speed has been improved slightly.

- \(bu
parent has been upgraded from version 0.234 to 0.236.

- \(bu
perl5db.pl has been upgraded from version 1.50 to 1.51.
.Sp
It now ignores */dev/tty* on non-Unix systems.
[\s-1GH\s0 #12244] <https://github.com/Perl/perl5/issues/12244>

- \(bu
Perl::OSType has been upgraded from version 1.009 to 1.010.

- \(bu
perlfaq has been upgraded from version 5.021010 to 5.021011.

- \(bu
PerlIO has been upgraded from version 1.09 to 1.10.

- \(bu
PerlIO::encoding has been upgraded from version 0.24 to 0.25.

- \(bu
PerlIO::scalar has been upgraded from version 0.24 to 0.26.

- \(bu
Pod::Checker has been upgraded from version 1.60 to 1.73.

- \(bu
Pod::Functions has been upgraded from version 1.10 to 1.11.

- \(bu
Pod::Html has been upgraded from version 1.22 to 1.2202.

- \(bu
Pod::Perldoc has been upgraded from version 3.25_02 to 3.28.

- \(bu
Pod::Simple has been upgraded from version 3.32 to 3.35.

- \(bu
Pod::Usage has been upgraded from version 1.68 to 1.69.

- \(bu
\s-1POSIX\s0 has been upgraded from version 1.65 to 1.76.
.Sp
This remedies several defects in making its symbols exportable.
[\s-1GH\s0 #15260] <https://github.com/Perl/perl5/issues/15260>
.Sp
The \f(CW\*(C`POSIX::tmpnam()\*(C' interface has been removed,
see \*(L"**POSIX::tmpnam()** has been removed\*(R".
.Sp
The following deprecated functions have been removed:
.Sp
.Vb 10
    POSIX::isalnum
    POSIX::isalpha
    POSIX::iscntrl
    POSIX::isdigit
    POSIX::isgraph
    POSIX::islower
    POSIX::isprint
    POSIX::ispunct
    POSIX::isspace
    POSIX::isupper
    POSIX::isxdigit
    POSIX::tolower
    POSIX::toupper
.Ve
.Sp
Trying to import \s-1POSIX\s0 subs that have no real implementations
(like \f(CW\*(C`POSIX::atend()\*(C') now fails at import time, instead of
waiting until runtime.

- \(bu
re has been upgraded from version 0.32 to 0.34
.Sp
This adds support for the new \f(CW\*(C`/xx\*(C'
regular expression pattern modifier, and a change to the \f(CW\*(C`use\ re\ \*(Aqstrict\*(Aq\*(C' experimental feature.  When \f(CW\*(C`re\ \*(Aqstrict\*(Aq\*(C' is enabled, a warning now will be generated for all
unescaped uses of the two characters \f(CW"\}" and \f(CW"]" in regular
expression patterns (outside bracketed character classes) that are taken
literally.  This brings them more in line with the \f(CW")" character which
is always a metacharacter unless escaped.  Being a metacharacter only
sometimes, depending on an action at a distance, can lead to silently
having the pattern mean something quite different than was intended,
which the \f(CW\*(C`re\ \*(Aqstrict\*(Aq\*(C' mode is intended to minimize.

- \(bu
Safe has been upgraded from version 2.39 to 2.40.

- \(bu
Scalar::Util has been upgraded from version 1.42_02 to 1.46_02.

- \(bu
Storable has been upgraded from version 2.56 to 2.62.
.Sp
Fixes
[\s-1GH\s0 #15714] <https://github.com/Perl/perl5/issues/15714>.

- \(bu
Symbol has been upgraded from version 1.07 to 1.08.

- \(bu
Sys::Syslog has been upgraded from version 0.33 to 0.35.

- \(bu
Term::ANSIColor has been upgraded from version 4.04 to 4.06.

- \(bu
Term::ReadLine has been upgraded from version 1.15 to 1.16.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
Test has been upgraded from version 1.28 to 1.30.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
Test::Harness has been upgraded from version 3.36 to 3.38.

- \(bu
Test::Simple has been upgraded from version 1.001014 to 1.302073.

- \(bu
Thread::Queue has been upgraded from version 3.09 to 3.12.

- \(bu
Thread::Semaphore has been upgraded from 2.12 to 2.13.
.Sp
Added the \f(CW\*(C`down_timed\*(C' method.

- \(bu
threads has been upgraded from version 2.07 to 2.15.

- \(bu
threads::shared has been upgraded from version 1.51 to 1.56.

- \(bu
Tie::Hash::NamedCapture has been upgraded from version 0.09 to 0.10.

- \(bu
Time::HiRes has been upgraded from version 1.9733 to 1.9741.
.Sp
It now builds on systems with \*(C+11 compilers (such as G++ 6 and Clang++
3.9).
.Sp
Now uses \f(CW\*(C`clockid_t\*(C'.

- \(bu
Time::Local has been upgraded from version 1.2300 to 1.25.

- \(bu
Unicode::Collate has been upgraded from version 1.14 to 1.19.

- \(bu
Unicode::UCD has been upgraded from version 0.64 to 0.68.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
version has been upgraded from version 0.9916 to 0.9917.

- \(bu
VMS::DCLsym has been upgraded from version 1.06 to 1.08.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

- \(bu
warnings has been upgraded from version 1.36 to 1.37.

- \(bu
XS::Typemap has been upgraded from version 0.14 to 0.15.

- \(bu
XSLoader has been upgraded from version 0.21 to 0.27.
.Sp
Fixed a security hole in which binary files could be loaded from a path
outside of \f(CW@INC.
.Sp
It now uses 3-arg \f(CW\*(C`open()\*(C' instead of 2-arg \f(CW\*(C`open()\*(C'.
[\s-1GH\s0 #15721] <https://github.com/Perl/perl5/issues/15721>

## Documentation

Header "Documentation"

### New Documentation

Subsection "New Documentation"
*perldeprecation*
Subsection "perldeprecation"

This file documents all upcoming deprecations, and some of the deprecations
which already have been removed.  The purpose of this documentation is
two-fold: document what will disappear, and by which version, and serve
as a guide for people dealing with code which has features that no longer
work after an upgrade of their perl.

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
We have attempted to update the documentation to reflect the changes
listed in this document.  If you find any we have missed, send email to
perlbug@perl.org <mailto:perlbug@perl.org>.

Additionally, all references to Usenet have been removed, and the
following selected changes have been made:

*perlfunc*
Subsection "perlfunc"

- \(bu
Removed obsolete text about \f(CW\*(C`defined()\*(C'
on aggregates that should have been deleted earlier, when the feature
was removed.

- \(bu
Corrected documentation of \f(CW\*(C`eval()\*(C',
and \f(CW\*(C`evalbytes()\*(C'.

- \(bu
Clarified documentation of \f(CW\*(C`seek()\*(C',
\f(CW\*(C`tell()\*(C' and \f(CW\*(C`sysseek()\*(C'
emphasizing that positions are in bytes and not characters.
[\s-1GH\s0 #15438] <https://github.com/Perl/perl5/issues/15438>

- \(bu
Clarified documentation of \f(CW\*(C`sort()\*(C' concerning
the variables \f(CW$a and \f(CW$b.

- \(bu
In \f(CW\*(C`split()\*(C' noted that certain pattern modifiers are
legal, and added a caution about its use in Perls before v5.11.

- \(bu
Removed obsolete documentation of \f(CW\*(C`study()\*(C', noting
that it is now a no-op.

- \(bu
Noted that \f(CW\*(C`vec()\*(C' doesn't work well when the string
contains characters whose code points are above 255.

*perlguts*
Subsection "perlguts"

- \(bu
Added advice on
formatted printing of operands of \f(CW\*(C`Size_t\*(C' and \f(CW\*(C`SSize_t\*(C'

*perlhack*
Subsection "perlhack"

- \(bu
Clarify what editor tab stop rules to use, and note that we are
migrating away from using tabs, replacing them with sequences of \s-1SPACE\s0
characters.

*perlhacktips*
Subsection "perlhacktips"

- \(bu
Give another reason to use \f(CW\*(C`cBOOL\*(C' to cast an expression to boolean.

- \(bu
Note that the macros \f(CW\*(C`TRUE\*(C' and \f(CW\*(C`FALSE\*(C' are available to express
boolean values.

*perlinterp*
Subsection "perlinterp"

- \(bu
perlinterp has been expanded to give a more detailed example of how to
hunt around in the parser for how a given operator is handled.

*perllocale*
Subsection "perllocale"

- \(bu
Some locales aren't compatible with Perl.  Note that these can cause
core dumps.

*perlmod*
Subsection "perlmod"

- \(bu
Various clarifications have been added.

*perlmodlib*
Subsection "perlmodlib"

- \(bu
Updated the site mirror list.

*perlobj*
Subsection "perlobj"

- \(bu
Added a section on calling methods using their fully qualified names.

- \(bu
Do not discourage manual \f(CW@ISA.

*perlootut*
Subsection "perlootut"

- \(bu
Mention \f(CW\*(C`Moo\*(C' more.

*perlop*
Subsection "perlop"

- \(bu
Note that white space must be used for quoting operators if the
delimiter is a word character (*i.e.*, matches \f(CW\*(C`\\w\*(C').

- \(bu
Clarify that in regular expression patterns delimited by single quotes,
no variable interpolation is done.

*perlre*
Subsection "perlre"

- \(bu
The first part was extensively rewritten to incorporate various basic
points, that in earlier versions were mentioned in sort of an appendix
on Version 8 regular expressions.

- \(bu
Note that it is common to have the \f(CW\*(C`/x\*(C' modifier and forget that this
means that \f(CW"#" has to be escaped.

*perlretut*
Subsection "perlretut"

- \(bu
Add introductory material.

- \(bu
Note that a metacharacter occurring in a context where it can't mean
that, silently loses its meta-ness and matches literally.
\f(CW\*(C`use re \*(Aqstrict\*(Aq\*(C' can catch some of these.

*perlunicode*
Subsection "perlunicode"

- \(bu
Corrected the text about Unicode \s-1BYTE ORDER MARK\s0 handling.

- \(bu
Updated the text to correspond with changes in Unicode UTS#18, concerning
regular expressions, and Perl compatibility with what it says.

*perlvar*
Subsection "perlvar"

- \(bu
Document \f(CW@ISA.  It was documented in other places, but not in perlvar.

## Diagnostics

Header "Diagnostics"

### New Diagnostics

Subsection "New Diagnostics"
*New Errors*
Subsection "New Errors"

- \(bu
A signature parameter must start with \f(CW\*(Aq$\*(Aq, \f(CW\*(Aq@\*(Aq or \f(CW\*(Aq%\*(Aq

- \(bu
Bareword in require contains \*(L"%s\*(R"

- \(bu
Bareword in require maps to empty filename

- \(bu
Bareword in require maps to disallowed filename \*(L"%s\*(R"

- \(bu
Bareword in require must not start with a double-colon: \*(L"%s\*(R"

- \(bu
\f(CW%s: command not found
.Sp
(A) You've accidentally run your script through **bash** or another shell
instead of Perl.  Check the \f(CW\*(C`#!\*(C' line, or manually feed your script into
Perl yourself.  The \f(CW\*(C`#!\*(C' line at the top of your file could look like:
.Sp
.Vb 1
  #!/usr/bin/perl
.Ve

- \(bu
\f(CW%s: command not found: \f(CW%s
.Sp
(A) You've accidentally run your script through **zsh** or another shell
instead of Perl.  Check the \f(CW\*(C`#!\*(C' line, or manually feed your script into
Perl yourself.  The \f(CW\*(C`#!\*(C' line at the top of your file could look like:
.Sp
.Vb 1
  #!/usr/bin/perl
.Ve

- \(bu
The experimental declared_refs feature is not enabled
.Sp
(F) To declare references to variables, as in \f(CW\*(C`my \\%x\*(C', you must first enable
the feature:
.Sp
.Vb 2
    no warnings "experimental::declared_refs";
    use feature "declared_refs";
.Ve
.Sp
See \*(L"Declaring a reference to a variable\*(R".

- \(bu
Illegal character following sigil in a subroutine signature

- \(bu
Indentation on line \f(CW%d of here-doc doesn't match delimiter

- \(bu
Infinite recursion via empty pattern.
.Sp
Using the empty pattern (which re-executes the last successfully-matched
pattern) inside a code block in another regex, as in \f(CW\*(C`/(?\{ s!!new! \})/\*(C', has
always previously yielded a segfault.  It now produces this error.

- \(bu
Malformed \s-1UTF-8\s0 string in \*(L"%s\*(R"

- \(bu
Multiple slurpy parameters not allowed

- \(bu
\f(CW\*(Aq#\*(Aq not allowed immediately following a sigil in a subroutine signature

- \(bu
panic: unknown OA_*: \f(CW%x

- \(bu
Unescaped left brace in regex is illegal here
.Sp
Unescaped left braces are now illegal in some contexts in regular expression
patterns.  In other contexts, they are still just deprecated; they will
be illegal in Perl 5.30.

- \(bu
Version control conflict marker
.Sp
(F) The parser found a line starting with \f(CW\*(C`<<<<<<<\*(C',
\f(CW\*(C`>>>>>>>\*(C', or \f(CW\*(C`=======\*(C'.  These may be left by a
version control system to mark conflicts after a failed merge operation.

*New Warnings*
Subsection "New Warnings"

- \(bu
Can't determine class of operator \f(CW%s, assuming \f(CW\*(C`BASEOP\*(C'

- \(bu
Declaring references is experimental
.Sp
(S experimental::declared_refs) This warning is emitted if you use a reference
constructor on the right-hand side of \f(CW\*(C`my()\*(C', \f(CW\*(C`state()\*(C', \f(CW\*(C`our()\*(C', or
\f(CW\*(C`local()\*(C'.  Simply suppress the warning if you want to use the feature, but
know that in doing so you are taking the risk of using an experimental feature
which may change or be removed in a future Perl version:
.Sp
.Vb 3
    no warnings "experimental::declared_refs";
    use feature "declared_refs";
    $fooref = my \\$foo;
.Ve
.Sp
See \*(L"Declaring a reference to a variable\*(R".

- \(bu
do \*(L"%s\*(R" failed, '.' is no longer in \f(CW@INC
.Sp
Since \f(CW"." is now removed from \f(CW@INC by default, \f(CW\*(C`do\*(C' will now trigger a warning recommending to fix the \f(CW\*(C`do\*(C' statement.

- \(bu
\f(CW\*(C`File::Glob::glob()\*(C' will disappear in perl 5.30. Use \f(CW\*(C`File::Glob::bsd_glob()\*(C' instead.

- \(bu
Unescaped literal '%c' in regex; marked by <-- \s-1HERE\s0 in m/%s/

- \(bu
Use of unassigned code point or non-standalone grapheme for a delimiter will be a fatal error starting in Perl 5.30
.Sp
See \*(L"Deprecations\*(R"

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
When a \f(CW\*(C`require\*(C' fails, we now do not provide \f(CW@INC when the \f(CW\*(C`require\*(C'
is for a file instead of a module.

- \(bu
When \f(CW@INC is not scanned for a \f(CW\*(C`require\*(C' call, we no longer display
\f(CW@INC to avoid confusion.

- \(bu
Attribute \*(L"locked\*(R" is deprecated, and will disappear in Perl 5.28
.Sp
This existing warning has had the *and will disappear* text added in this
release.

- \(bu
Attribute \*(L"unique\*(R" is deprecated, and will disappear in Perl 5.28
.Sp
This existing warning has had the *and will disappear* text added in this
release.

- \(bu
Calling POSIX::%s() is deprecated
.Sp
This warning has been removed, as the deprecated functions have been
removed from \s-1POSIX.\s0

- \(bu
Constants from lexical variables potentially modified elsewhere are deprecated. This will not be allowed in Perl 5.32
.Sp
This existing warning has had the *this will not be allowed* text added
in this release.

- \(bu
Deprecated use of \f(CW\*(C`my()\*(C' in false conditional. This will be a fatal error in Perl 5.30
.Sp
This existing warning has had the *this will be a fatal error* text added
in this release.

- \(bu
\f(CW\*(C`dump()\*(C' better written as \f(CW\*(C`CORE::dump()\*(C'. \f(CW\*(C`dump()\*(C' will no longer be available in Perl 5.30
.Sp
This existing warning has had the *no longer be available* text added in
this release.

- \(bu
Experimental \f(CW%s on scalar is now forbidden
.Sp
This message is now followed by more helpful text.
[\s-1GH\s0 #15291] <https://github.com/Perl/perl5/issues/15291>

- \(bu
Experimental \*(L"%s\*(R" subs not enabled
.Sp
This warning was been removed, as lexical subs are no longer experimental.

- \(bu
Having more than one /%c regexp modifier is deprecated
.Sp
This deprecation warning has been removed, since \f(CW\*(C`/xx\*(C' now has a new
meaning.

- \(bu
%s() is deprecated on \f(CW\*(C`:utf8\*(C' handles. This will be a fatal error in Perl 5.30
.
.Sp
where \*(L"%s\*(R" is one of \f(CW\*(C`sysread\*(C', \f(CW\*(C`recv\*(C', \f(CW\*(C`syswrite\*(C', or \f(CW\*(C`send\*(C'.
.Sp
This existing warning has had the *this will be a fatal error* text added
in this release.
.Sp
This warning is now enabled by default, as all \f(CW\*(C`deprecated\*(C' category
warnings should be.

- \(bu
\f(CW$* is no longer supported. Its use will be fatal in Perl 5.30
.Sp
This existing warning has had the *its use will be fatal* text added in
this release.

- \(bu
\f(CW$# is no longer supported. Its use will be fatal in Perl 5.30
.Sp
This existing warning has had the *its use will be fatal* text added in
this release.

- \(bu
Malformed \s-1UTF-8\s0 character%s
.Sp
Details as to the exact problem have been added at the end of this
message

- \(bu
Missing or undefined argument to \f(CW%s
.Sp
This warning used to warn about \f(CW\*(C`require\*(C', even if it was actually \f(CW\*(C`do\*(C'
which being executed. It now gets the operation name right.

- \(bu
NO-BREAK \s-1SPACE\s0 in a charnames alias definition is deprecated
.Sp
This warning has been removed as the behavior is now an error.

- \(bu
Odd name/value argument for subroutine '%s'
.Sp
This warning now includes the name of the offending subroutine.

- \(bu
Opening dirhandle \f(CW%s also as a file. This will be a fatal error in Perl 5.28
.Sp
This existing warning has had the *this will be a fatal error* text added
in this release.

- \(bu
Opening filehandle \f(CW%s also as a directory. This will be a fatal error in Perl 5.28
.Sp
This existing warning has had the *this will be a fatal error* text added
in this release.

- \(bu
panic: ck_split, type=%u
.Sp
panic: pp_split, pm=%p, s=%p
.Sp
These panic errors have been removed.

- \(bu
Passing malformed \s-1UTF-8\s0 to \*(L"%s\*(R" is deprecated
.Sp
This warning has been changed to the fatal
Malformed \s-1UTF-8\s0 string in \*(L"%s\*(R"

- \(bu
Setting \f(CW$/ to a reference to \f(CW%s as a form of slurp is deprecated, treating as undef. This will be fatal in Perl 5.28
.Sp
This existing warning has had the *this will be fatal* text added in
this release.

- \(bu
\f(CW\*(C`$\{^ENCODING\}\*(C' is no longer supported. Its use will be fatal in Perl 5.28
.Sp
This warning used to be: "Setting \f(CW\*(C`$\{^ENCODING\}\*(C' is deprecated".
.Sp
The special action of the variable \f(CW\*(C`$\{^ENCODING\}\*(C' was formerly used to
implement the \f(CW\*(C`encoding\*(C' pragma. As of Perl 5.26, rather than being
deprecated, assigning to this variable now has no effect except to issue
the warning.

- \(bu
Too few arguments for subroutine '%s'
.Sp
This warning now includes the name of the offending subroutine.

- \(bu
Too many arguments for subroutine '%s'
.Sp
This warning now includes the name of the offending subroutine.

- \(bu
Unescaped left brace in regex is deprecated here (and will be fatal in Perl 5.30), passed through in regex; marked by <--\ \s-1HERE\s0 in m/%s/
.Sp
This existing warning has had the *here (and will be fatal...)* text
added in this release.

- \(bu
Unknown charname '' is deprecated. Its use will be fatal in Perl 5.28
.Sp
This existing warning has had the *its use will be fatal* text added in
this release.

- \(bu
Use of bare << to mean <<"" is deprecated. Its use will be fatal in Perl 5.28
.Sp
This existing warning has had the *its use will be fatal* text added in
this release.

- \(bu
Use of code point 0x%s is deprecated; the permissible max is 0x%s.  This will be fatal in Perl 5.28
.Sp
This existing warning has had the *this will be fatal* text added in
this release.

- \(bu
Use of comma-less variable list is deprecated. Its use will be fatal in Perl 5.28
.Sp
This existing warning has had the *its use will be fatal* text added in
this release.

- \(bu
Use of inherited \f(CW\*(C`AUTOLOAD\*(C' for non-method %s() is deprecated. This will be fatal in Perl 5.28
.Sp
This existing warning has had the *this will be fatal* text added in
this release.

- \(bu
Use of strings with code points over 0xFF as arguments to \f(CW%s operator is deprecated. This will be a fatal error in Perl 5.28
.Sp
This existing warning has had the *this will be a fatal error* text added in
this release.

## Utility Changes

Header "Utility Changes"

### \fIc2ph and \fIpstruct

Subsection "c2ph and pstruct"

- \(bu
These old utilities have long since superceded by h2xs, and are
now gone from the distribution.

### \fIPorting/pod_lib.pl

Subsection "Porting/pod_lib.pl"

- \(bu
Removed spurious executable bit.

- \(bu
Account for the possibility of \s-1DOS\s0 file endings.

### \fIPorting/sync-with-cpan

Subsection "Porting/sync-with-cpan"

- \(bu
Many improvements.

### \fIperf/benchmarks

Subsection "perf/benchmarks"

- \(bu
Tidy file, rename some symbols.

### \fIPorting/checkAUTHORS.pl

Subsection "Porting/checkAUTHORS.pl"

- \(bu
Replace obscure character range with \f(CW\*(C`\\w\*(C'.

### \fIt/porting/regen.t

Subsection "t/porting/regen.t"

- \(bu
Try to be more helpful when tests fail.

### \fIutils/h2xs.PL

Subsection "utils/h2xs.PL"

- \(bu
Avoid infinite loop for enums.

### perlbug

Subsection "perlbug"

- \(bu
Long lines in the message body are now wrapped at 900 characters, to stay
well within the 1000-character limit imposed by \s-1SMTP\s0 mail transfer agents.
This is particularly likely to be important for the list of arguments to
*Configure*, which can readily exceed the limit if, for example, it names
several non-default installation paths.  This change also adds the first unit
tests for perlbug.
[perl #128020] <https://rt.perl.org/Public/Bug/Display.html?id=128020>

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
\f(CW\*(C`-Ddefault_inc_excludes_dot\*(C' has added, and enabled by default.

- \(bu
The \f(CW\*(C`dtrace\*(C' build process has further changes
[\s-1GH\s0 #15718] <https://github.com/Perl/perl5/issues/15718>:

> 
- \(bu
If the \f(CW\*(C`-xnolibs\*(C' is available, use that so a *dtrace* perl can be
built within a FreeBSD jail.

- \(bu
On systems that build a *dtrace* object file (FreeBSD, Solaris, and
SystemTap's dtrace emulation), copy the input objects to a separate
directory and process them there, and use those objects in the link,
since \f(CW\*(C`dtrace -G\*(C' also modifies these objects.

- \(bu
Add *libelf* to the build on FreeBSD 10.x, since *dtrace* adds
references to *libelf* symbols.

- \(bu
Generate a dummy *dtrace_main.o* if \f(CW\*(C`dtrace -G\*(C' fails to build it.  A
default build on Solaris generates probes from the unused inline
functions, while they don't on FreeBSD, which causes \f(CW\*(C`dtrace -G\*(C' to
fail.



> 


- \(bu
You can now disable perl's use of the \f(CW\*(C`PERL_HASH_SEED\*(C' and
\f(CW\*(C`PERL_PERTURB_KEYS\*(C' environment variables by configuring perl with
\f(CW\*(C`-Accflags=NO_PERL_HASH_ENV\*(C'.

- \(bu
You can now disable perl's use of the \f(CW\*(C`PERL_HASH_SEED_DEBUG\*(C' environment
variable by configuring perl with
\f(CW\*(C`-Accflags=-DNO_PERL_HASH_SEED_DEBUG\*(C'.

- \(bu
*Configure* now zeroes out the alignment bytes when calculating the bytes
for 80-bit \f(CW\*(C`NaN\*(C' and \f(CW\*(C`Inf\*(C' to make builds more reproducible.
[\s-1GH\s0 #15725] <https://github.com/Perl/perl5/issues/15725>

- \(bu
Since v5.18, for testing purposes we have included support for
building perl with a variety of non-standard, and non-recommended
hash functions.  Since we do not recommend the use of these functions,
we have removed them and their corresponding build options.  Specifically
this includes the following build options:
.Sp
.Vb 8
    PERL_HASH_FUNC_SDBM
    PERL_HASH_FUNC_DJB2
    PERL_HASH_FUNC_SUPERFAST
    PERL_HASH_FUNC_MURMUR3
    PERL_HASH_FUNC_ONE_AT_A_TIME
    PERL_HASH_FUNC_ONE_AT_A_TIME_OLD
    PERL_HASH_FUNC_MURMUR_HASH_64A
    PERL_HASH_FUNC_MURMUR_HASH_64B
.Ve

- \(bu
Remove \*(L"Warning: perl appears in your path\*(R"
.Sp
This install warning is more or less obsolete, since most platforms already
**will** have a */usr/bin/perl* or similar provided by the \s-1OS.\s0

- \(bu
Reduce verbosity of \f(CW\*(C`make install.man\*(C'
.Sp
Previously, two progress messages were emitted for each manpage: one by
installman itself, and one by the function in *install_lib.pl* that it calls to
actually install the file.  Disabling the second of those in each case saves
over 750 lines of unhelpful output.

- \(bu
Cleanup for \f(CW\*(C`clang -Weverything\*(C' support.
[\s-1GH\s0 #15683] <https://github.com/Perl/perl5/issues/15683>

- \(bu
*Configure*: signbit scan was assuming too much, stop assuming negative 0.

- \(bu
Various compiler warnings have been silenced.

- \(bu
Several smaller changes have been made to remove impediments to compiling
under \*(C+11.

- \(bu
Builds using \f(CW\*(C`USE_PAD_RESET\*(C' now work again; this configuration had
bit-rotted.

- \(bu
A probe for \f(CW\*(C`gai_strerror\*(C' was added to *Configure* that checks if
the \f(CW\*(C`gai_strerror()\*(C' routine is available and can be used to
translate error codes returned by \f(CW\*(C`getaddrinfo()\*(C' into human
readable strings.

- \(bu
*Configure* now aborts if both \f(CW\*(C`-Duselongdouble\*(C' and \f(CW\*(C`-Dusequadmath\*(C' are
requested.
[\s-1GH\s0 #14944] <https://github.com/Perl/perl5/issues/14944>

- \(bu
Fixed a bug in which *Configure* could append \f(CW\*(C`-quadmath\*(C' to the
archname even if it was already present.
[\s-1GH\s0 #15423] <https://github.com/Perl/perl5/issues/15423>

- \(bu
Clang builds with \f(CW\*(C`-DPERL_GLOBAL_STRUCT\*(C' or
\f(CW\*(C`-DPERL_GLOBAL_STRUCT_PRIVATE\*(C' have
been fixed (by disabling Thread Safety Analysis for these configurations).

- \(bu
*make_ext.pl* no longer updates a module's *pm_to_blib* file when no
files require updates.  This could cause dependencies, *perlmain.c*
in particular, to be rebuilt unnecessarily.
[\s-1GH\s0 #15060] <https://github.com/Perl/perl5/issues/15060>

- \(bu
The output of \f(CW\*(C`perl -V\*(C' has been reformatted so that each configuration
and compile-time option is now listed one per line, to improve
readability.

- \(bu
*Configure* now builds \f(CW\*(C`miniperl\*(C' and \f(CW\*(C`generate_uudmap\*(C' if you
invoke it with \f(CW\*(C`-Dusecrosscompiler\*(C' but not \f(CW\*(C`-Dtargethost=somehost\*(C'.
This means you can supply your target platform \f(CW\*(C`config.sh\*(C', generate
the headers and proceed to build your cross-target perl.
[\s-1GH\s0 #15126] <https://github.com/Perl/perl5/issues/15126>

- \(bu
Perl built with \f(CW\*(C`-Accflags=-DPERL_TRACE_OPS\*(C' now only dumps the operator
counts when the environment variable \f(CW\*(C`PERL_TRACE_OPS\*(C' is set to a
non-zero integer.  This allows \f(CW\*(C`make test\*(C' to pass on such a build.

- \(bu
When building with \s-1GCC 6\s0 and link-time optimization (the \f(CW\*(C`-flto\*(C' option to
\f(CW\*(C`gcc\*(C'), *Configure* was treating all probed symbols as present on the
system, regardless of whether they actually exist.  This has been fixed.
[\s-1GH\s0 #15322] <https://github.com/Perl/perl5/issues/15322>

- \(bu
The *t/test.pl* library is used for internal testing of Perl itself, and
also copied by several \s-1CPAN\s0 modules.  Some of those modules must work on
older versions of Perl, so *t/test.pl* must in turn avoid newer Perl
features.  Compatibility with Perl 5.8 was inadvertently removed some time
ago; it has now been restored.
[\s-1GH\s0 #15302] <https://github.com/Perl/perl5/issues/15302>

- \(bu
The build process no longer emits an extra blank line before building each
\*(L"simple\*(R" extension (those with only **.pm* and **.pod* files).

## Testing

Header "Testing"
Tests were added and changed to reflect the other additions and changes
in this release.  Furthermore, these substantive changes were made:

- \(bu
A new test script, *comp/parser_run.t*, has been added that is like
*comp/parser.t* but with *test.pl* included so that \f(CW\*(C`runperl()\*(C' and the
like are available for use.

- \(bu
Tests for locales were erroneously using locales incompatible with Perl.

- \(bu
Some parts of the test suite that try to exhaustively test edge cases in the
regex implementation have been restricted to running for a maximum of five
minutes.  On slow systems they could otherwise take several hours, without
significantly improving our understanding of the correctness of the code
under test.

- \(bu
A new internal facility allows analysing the time taken by the individual
tests in Perl's own test suite; see *Porting/harness-timer-report.pl*.

- \(bu
*t/re/regexp_nonull.t* has been added to test that the regular expression
engine can handle scalars that do not have a null byte just past the end of
the string.

- \(bu
A new test script, *t/op/decl-refs.t*, has been added to test the new feature
\*(L"Declaring a reference to a variable\*(R".

- \(bu
A new test script, *t/re/keep_tabs.t* has been added to contain tests
where \f(CW\*(C`\\t\*(C' characters should not be expanded into spaces.

- \(bu
A new test script, *t/re/anyof.t*, has been added to test that the \s-1ANYOF\s0 nodes
generated by bracketed character classes are as expected.

- \(bu
There is now more extensive testing of the Unicode-related \s-1API\s0 macros
and functions.

- \(bu
Several of the longer running \s-1API\s0 test files have been split into
multiple test files so that they can be run in parallel.

- \(bu
*t/harness* now tries really hard not to run tests which are located
outside of the Perl source tree.
[\s-1GH\s0 #14578] <https://github.com/Perl/perl5/issues/14578>

- \(bu
Prevent debugger tests (*lib/perl5db.t*) from failing due to the contents
of \f(CW$ENV\{PERLDB_OPTS\}.
[\s-1GH\s0 #15782] <https://github.com/Perl/perl5/issues/15782>

## Platform Support

Header "Platform Support"

### New Platforms

Subsection "New Platforms"

- NetBSD/VAX
Item "NetBSD/VAX"
Perl now compiles under NetBSD on \s-1VAX\s0 machines.  However, it's not
possible for that platform to implement floating-point infinities and
NaNs compatible with most modern systems, which implement the \s-1IEEE-754\s0
floating point standard.  The hexadecimal floating point (\f(CW\*(C`0x...p[+-]n\*(C'
literals, \f(CW\*(C`printf %a\*(C') is not implemented, either.
The \f(CW\*(C`make test\*(C' passes 98% of tests.

> 
- \(bu
Test fixes and minor updates.

- \(bu
Account for lack of \f(CW\*(C`inf\*(C', \f(CW\*(C`nan\*(C', and \f(CW\*(C`-0.0\*(C' support.



> 


### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- Darwin
Item "Darwin"

> 0

- \(bu
.PD
Don't treat \f(CW\*(C`-Dprefix=/usr\*(C' as special: instead require an extra option
\f(CW\*(C`-Ddarwin_distribution\*(C' to produce the same results.

- \(bu
\s-1OS X\s0 El Capitan doesn't implement the \f(CW\*(C`clock_gettime()\*(C' or
\f(CW\*(C`clock_getres()\*(C' APIs; emulate them as necessary.

- \(bu
Deprecated \f(CWsyscall(2) on macOS 10.12.



> 


- \s-1EBCDIC\s0
Item "EBCDIC"
Several tests have been updated to work (or be skipped) on \s-1EBCDIC\s0 platforms.

- HP-UX
Item "HP-UX"
The Net::Ping \s-1UDP\s0 test is now skipped on HP-UX.

- Hurd
Item "Hurd"
The hints for Hurd have been improved, enabling malloc wrap and reporting the
\s-1GNU\s0 libc used (previously it was an empty string when reported).

- \s-1VAX\s0
Item "VAX"
\s-1VAX\s0 floating point formats are now supported on NetBSD.

- \s-1VMS\s0
Item "VMS"

> 0

- \(bu
.PD
The path separator for the \f(CW\*(C`PERL5LIB\*(C' and \f(CW\*(C`PERLLIB\*(C' environment entries is
now a colon (\f(CW":") when running under a Unix shell.  There is no change when
running under \s-1DCL\s0 (it's still \f(CW"|").

- \(bu
*configure.com* now recognizes the VSI-branded C compiler and no longer
recognizes the \*(L"\s-1DEC\s0\*(R"-branded C compiler (as there hasn't been such a thing for
15 or more years).



> 


- Windows
Item "Windows"

> 0

- \(bu
.PD
Support for compiling perl on Windows using Microsoft Visual Studio 2015
(containing Visual \*(C+ 14.0) has been added.
.Sp
This version of \s-1VC++\s0 includes a completely rewritten C run-time library, some
of the changes in which mean that work done to resolve a socket
\f(CW\*(C`close()\*(C' bug in
perl #120091 and perl #118059 is not workable in its current state with this
version of \s-1VC++.\s0  Therefore, we have effectively reverted that bug fix for
\s-1VS2015\s0 onwards on the basis that being able to build with \s-1VS2015\s0 onwards is
more important than keeping the bug fix.  We may revisit this in the future to
attempt to fix the bug again in a way that is compatible with \s-1VS2015.\s0
.Sp
These changes do not affect compilation with \s-1GCC\s0 or with Visual Studio versions
up to and including \s-1VS2013,\s0 *i.e.*, the bug fix is retained (unchanged) for those
compilers.
.Sp
Note that you may experience compatibility problems if you mix a perl built
with \s-1GCC\s0 or \s-1VS\s0 <= \s-1VS2013\s0 with \s-1XS\s0 modules built with \s-1VS2015,\s0 or if you mix a
perl built with \s-1VS2015\s0 with \s-1XS\s0 modules built with \s-1GCC\s0 or \s-1VS\s0 <= \s-1VS2013.\s0
Some incompatibility may arise because of the bug fix that has been reverted
for \s-1VS2015\s0 builds of perl, but there may well be incompatibility anyway because
of the rewritten \s-1CRT\s0 in \s-1VS2015\s0 (*e.g.*, see discussion at
<http://stackoverflow.com/questions/30412951>).

- \(bu
It now automatically detects \s-1GCC\s0 versus Visual C and sets the \s-1VC\s0 version
number on Win32.



> 


- Linux
Item "Linux"
Drop support for Linux *a.out* executable format. Linux has used \s-1ELF\s0 for
over twenty years.

- OpenBSD 6
Item "OpenBSD 6"
OpenBSD 6 still does not support returning \f(CW\*(C`pid\*(C', \f(CW\*(C`gid\*(C', or \f(CW\*(C`uid\*(C' with
\f(CW\*(C`SA_SIGINFO\*(C'.  Make sure to account for it.

- FreeBSD
Item "FreeBSD"
*t/uni/overload.t*: Skip hanging test on FreeBSD.

- DragonFly \s-1BSD\s0
Item "DragonFly BSD"
DragonFly \s-1BSD\s0 now has support for \f(CW\*(C`setproctitle()\*(C'.
[\s-1GH\s0 #15703] <https://github.com/Perl/perl5/issues/15703>.

## Internal Changes

Header "Internal Changes"

- \(bu
A new \s-1API\s0 function \f(CW\*(C`sv_setpv_bufsize()\*(C'
allows simultaneously setting the
length and the allocated size of the buffer in an \f(CW\*(C`SV\*(C', growing the
buffer if necessary.

- \(bu
A new \s-1API\s0 macro \f(CW\*(C`SvPVCLEAR()\*(C' sets its \f(CW\*(C`SV\*(C'
argument to an empty string,
like Perl-space \f(CW\*(C`$x = \*(Aq\*(Aq\*(C', but with several optimisations.

- \(bu
Several new macros and functions for dealing with Unicode and
UTF-8-encoded strings have been added to the \s-1API,\s0 as well as some
changes in the
functionality of existing functions (see \*(L"Unicode Support\*(R" in perlapi for
more details):

> 
- \(bu
New versions of the \s-1API\s0 macros like \f(CW\*(C`isALPHA_utf8\*(C' and \f(CW\*(C`toLOWER_utf8\*(C'
have been added, each with the suffix \f(CW\*(C`_safe\*(C', like
\f(CW\*(C`isSPACE_utf8_safe\*(C'.  These take an extra
parameter, giving an upper
limit of how far into the string it is safe to read.  Using the old
versions could cause attempts to read beyond the end of the input buffer
if the \s-1UTF-8\s0 is not well-formed, and their use now raises a deprecation
warning.  Details are at \*(L"Character classification\*(R" in perlapi.

- \(bu
Macros like \f(CW\*(C`isALPHA_utf8\*(C' and
\f(CW\*(C`toLOWER_utf8\*(C' now die if they detect
that their input \s-1UTF-8\s0 is malformed.  A deprecation warning had been
issued since Perl 5.18.

- \(bu
Several new macros for analysing the validity of utf8 sequences. These
are:
.Sp
\f(CW\*(C`UTF8_GOT_ABOVE_31_BIT\*(C'
\f(CW\*(C`UTF8_GOT_CONTINUATION\*(C'
\f(CW\*(C`UTF8_GOT_EMPTY\*(C'
\f(CW\*(C`UTF8_GOT_LONG\*(C'
\f(CW\*(C`UTF8_GOT_NONCHAR\*(C'
\f(CW\*(C`UTF8_GOT_NON_CONTINUATION\*(C'
\f(CW\*(C`UTF8_GOT_OVERFLOW\*(C'
\f(CW\*(C`UTF8_GOT_SHORT\*(C'
\f(CW\*(C`UTF8_GOT_SUPER\*(C'
\f(CW\*(C`UTF8_GOT_SURROGATE\*(C'
\f(CW\*(C`UTF8_IS_INVARIANT\*(C'
\f(CW\*(C`UTF8_IS_NONCHAR\*(C'
\f(CW\*(C`UTF8_IS_SUPER\*(C'
\f(CW\*(C`UTF8_IS_SURROGATE\*(C'
\f(CW\*(C`UVCHR_IS_INVARIANT\*(C'
\f(CW\*(C`isUTF8_CHAR_flags\*(C'
\f(CW\*(C`isSTRICT_UTF8_CHAR\*(C'
\f(CW\*(C`isC9_STRICT_UTF8_CHAR\*(C'

- \(bu
Functions that are all extensions of the \f(CW\*(C`is_utf8_string_\f(CI*\f(CW()\*(C' functions,
that apply various restrictions to the \s-1UTF-8\s0 recognized as valid:
.Sp
\f(CW\*(C`is_strict_utf8_string\*(C',
\f(CW\*(C`is_strict_utf8_string_loc\*(C',
\f(CW\*(C`is_strict_utf8_string_loclen\*(C',
.Sp
\f(CW\*(C`is_c9strict_utf8_string\*(C',
\f(CW\*(C`is_c9strict_utf8_string_loc\*(C',
\f(CW\*(C`is_c9strict_utf8_string_loclen\*(C',
.Sp
\f(CW\*(C`is_utf8_string_flags\*(C',
\f(CW\*(C`is_utf8_string_loc_flags\*(C',
\f(CW\*(C`is_utf8_string_loclen_flags\*(C',
.Sp
\f(CW\*(C`is_utf8_fixed_width_buf_flags\*(C',
\f(CW\*(C`is_utf8_fixed_width_buf_loc_flags\*(C',
\f(CW\*(C`is_utf8_fixed_width_buf_loclen_flags\*(C'.
.Sp
\f(CW\*(C`is_utf8_invariant_string\*(C'.
\f(CW\*(C`is_utf8_valid_partial_char\*(C'.
\f(CW\*(C`is_utf8_valid_partial_char_flags\*(C'.

- \(bu
The functions \f(CW\*(C`utf8n_to_uvchr\*(C' and its
derivatives have had several changes of behaviour.
.Sp
Calling them, while passing a string length of 0 is now asserted against
in \s-1DEBUGGING\s0 builds, and otherwise, returns the Unicode \s-1REPLACEMENT
CHARACTER.\s0   If you have nothing to decode, you shouldn't call the decode
function.
.Sp
They now return the Unicode \s-1REPLACEMENT CHARACTER\s0 if called with \s-1UTF-8\s0
that has the overlong malformation and that malformation is allowed by
the input parameters.  This malformation is where the \s-1UTF-8\s0 looks valid
syntactically, but there is a shorter sequence that yields the same code
point.  This has been forbidden since Unicode version 3.1.
.Sp
They now accept an input
flag to allow the overflow malformation.  This malformation is when the
\s-1UTF-8\s0 may be syntactically valid, but the code point it represents is
not capable of being represented in the word length on the platform.
What \*(L"allowed\*(R" means, in this case, is that the function doesn't return an
error, and it advances the parse pointer to beyond the \s-1UTF-8\s0 in
question, but it returns the Unicode \s-1REPLACEMENT CHARACTER\s0 as the value
of the code point (since the real value is not representable).
.Sp
They no longer abandon searching for other malformations when the first
one is encountered.  A call to one of these functions thus can generate
multiple diagnostics, instead of just one.

- \(bu
\f(CW\*(C`valid_utf8_to_uvchr()\*(C' has been added
to the \s-1API\s0 (although it was
present in core earlier). Like \f(CW\*(C`utf8_to_uvchr_buf()\*(C', but assumes that
the next character is well-formed.  Use with caution.

- \(bu
A new function, \f(CW\*(C`utf8n_to_uvchr_error\*(C',
has been added for
use by modules that need to know the details of \s-1UTF-8\s0 malformations
beyond pass/fail.  Previously, the only ways to know why a sequence was
ill-formed was to capture and parse the generated diagnostics or to do
your own analysis.

- \(bu
There is now a safer version of **utf8_hop()**, called
\f(CW\*(C`utf8_hop_safe()\*(C'.
Unlike **utf8_hop()**, **utf8_hop_safe()** won't navigate before the beginning or
after the end of the supplied buffer.

- \(bu
Two new functions, \f(CW\*(C`utf8_hop_forward()\*(C' and
\f(CW\*(C`utf8_hop_back()\*(C' are
similar to \f(CW\*(C`utf8_hop_safe()\*(C' but are for when you know which direction
you wish to travel.

- \(bu
Two new macros which return useful utf8 byte sequences:
.Sp
\f(CW\*(C`BOM_UTF8\*(C'
.Sp
\f(CW\*(C`REPLACEMENT_CHARACTER_UTF8\*(C'



> 


- \(bu
Perl is now built with the \f(CW\*(C`PERL_OP_PARENT\*(C' compiler define enabled by
default.  To disable it, use the \f(CW\*(C`PERL_NO_OP_PARENT\*(C' compiler define.
This flag alters how the \f(CW\*(C`op_sibling\*(C' field is used in \f(CW\*(C`OP\*(C' structures,
and has been available optionally since perl 5.22.
.Sp
See \*(L"Internal Changes\*(R" in perl5220delta for more details of what this
build option does.

- \(bu
Three new ops, \f(CW\*(C`OP_ARGELEM\*(C', \f(CW\*(C`OP_ARGDEFELEM\*(C', and \f(CW\*(C`OP_ARGCHECK\*(C' have
been added.  These are intended principally to implement the individual
elements of a subroutine signature, plus any overall checking required.

- \(bu
The \f(CW\*(C`OP_PUSHRE\*(C' op has been eliminated and the \f(CW\*(C`OP_SPLIT\*(C' op has been
changed from class \f(CW\*(C`LISTOP\*(C' to \f(CW\*(C`PMOP\*(C'.
.Sp
Formerly the first child of a split would be a \f(CW\*(C`pushre\*(C', which would have the
\f(CW\*(C`split\*(C''s regex attached to it. Now the regex is attached directly to the
\f(CW\*(C`split\*(C' op, and the \f(CW\*(C`pushre\*(C' has been eliminated.

- \(bu
The \f(CW\*(C`op_class()\*(C' \s-1API\s0 function has been added.  This
is like the existing
\f(CW\*(C`OP_CLASS()\*(C' macro, but can more accurately determine what struct an op
has been allocated as.  For example \f(CW\*(C`OP_CLASS()\*(C' might return
\f(CW\*(C`OA_BASEOP_OR_UNOP\*(C' indicating that ops of this type are usually
allocated as an \f(CW\*(C`OP\*(C' or \f(CW\*(C`UNOP\*(C'; while \f(CW\*(C`op_class()\*(C' will return
\f(CW\*(C`OPclass_BASEOP\*(C' or \f(CW\*(C`OPclass_UNOP\*(C' as appropriate.

- \(bu
All parts of the internals now agree that the \f(CW\*(C`sassign\*(C' op is a \f(CW\*(C`BINOP\*(C';
previously it was listed as a \f(CW\*(C`BASEOP\*(C' in *regen/opcodes*, which meant
that several parts of the internals had to be special-cased to accommodate
it.  This oddity's original motivation was to handle code like \f(CW\*(C`$x ||= 1\*(C';
that is now handled in a simpler way.

- \(bu
The output format of the \f(CW\*(C`op_dump()\*(C' function (as
used by \f(CW\*(C`perl -Dx\*(C')
has changed: it now displays an \*(L"ASCII-art\*(R" tree structure, and shows more
low-level details about each op, such as its address and class.

- \(bu
The \f(CW\*(C`PADOFFSET\*(C' type has changed from being unsigned to signed, and
several pad-related variables such as \f(CW\*(C`PL_padix\*(C' have changed from being
of type \f(CW\*(C`I32\*(C' to type \f(CW\*(C`PADOFFSET\*(C'.

- \(bu
The \f(CW\*(C`DEBUGGING\*(C'-mode output for regex compilation and execution has been
enhanced.

- \(bu
Several obscure \s-1SV\s0 flags have been eliminated, sometimes along with the
macros which manipulate them: \f(CW\*(C`SVpbm_VALID\*(C', \f(CW\*(C`SVpbm_TAIL\*(C', \f(CW\*(C`SvTAIL_on\*(C',
\f(CW\*(C`SvTAIL_off\*(C', \f(CW\*(C`SVrepl_EVAL\*(C', \f(CW\*(C`SvEVALED\*(C'.

- \(bu
An \s-1OP\s0 \f(CW\*(C`op_private\*(C' flag has been eliminated: \f(CW\*(C`OPpRUNTIME\*(C'. This used to
often get set on \f(CW\*(C`PMOP\*(C' ops, but had become meaningless over time.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
Perl no longer panics when switching into some locales on machines with
buggy \f(CW\*(C`strxfrm()\*(C' implementations in their *libc*.
[\s-1GH\s0 #13768] <https://github.com/Perl/perl5/issues/13768>

- \(bu
\f(CW\*(C` $-\{$name\} \*(C' would leak an \f(CW\*(C`AV\*(C' on each access if the regular
expression had no named captures.  The same applies to access to any
hash tied with Tie::Hash::NamedCapture and \f(CW\*(C`all => 1\*(C'.
[\s-1GH\s0 #15882] <https://github.com/Perl/perl5/issues/15882>

- \(bu
Attempting to use the deprecated variable \f(CW$# as the object in an
indirect object method call could cause a heap use after free or
buffer overflow.
[\s-1GH\s0 #15599] <https://github.com/Perl/perl5/issues/15599>

- \(bu
When checking for an indirect object method call, in some rare cases
the parser could reallocate the line buffer but then continue to use
pointers to the old buffer.
[\s-1GH\s0 #15585] <https://github.com/Perl/perl5/issues/15585>

- \(bu
Supplying a glob as the format argument to
\f(CW\*(C`formline\*(C' would
cause an assertion failure.
[\s-1GH\s0 #15862] <https://github.com/Perl/perl5/issues/15862>

- \(bu
Code like \f(CW\*(C` $value1 =~ qr/.../ ~~ $value2 \*(C' would have the match
converted into a \f(CW\*(C`qr//\*(C' operator, leaving extra elements on the stack to
confuse any surrounding expression.
[\s-1GH\s0 #15859] <https://github.com/Perl/perl5/issues/15859>

- \(bu
Since v5.24 in some obscure cases, a regex which included code blocks
from multiple sources (*e.g.*, via embedded via \f(CW\*(C`qr//\*(C' objects) could end up
with the wrong current pad and crash or give weird results.
[\s-1GH\s0 #15657] <https://github.com/Perl/perl5/issues/15657>

- \(bu
Occasionally \f(CW\*(C`local()\*(C's in a code block within a patterns weren't being
undone when the pattern matching backtracked over the code block.
[\s-1GH\s0 #15056] <https://github.com/Perl/perl5/issues/15056>

- \(bu
Using \f(CW\*(C`substr()\*(C' to modify a magic variable could access freed memory
in some cases.
[\s-1GH\s0 #15871] <https://github.com/Perl/perl5/issues/15871>

- \(bu
Under \f(CW\*(C`use utf8\*(C', the entire source code is now checked for being \s-1UTF-8\s0
well formed, not just quoted strings as before.
[\s-1GH\s0 #14973] <https://github.com/Perl/perl5/issues/14973>.

- \(bu
The range operator \f(CW".." on strings now handles its arguments correctly when in
the scope of the \f(CW\*(C`unicode_strings\*(C'
feature.  The previous behaviour was sufficiently unexpected that we believe no
correct program could have made use of it.

- \(bu
The \f(CW\*(C`split\*(C' operator did not ensure enough space was allocated for
its return value in scalar context.  It could then write a single
pointer immediately beyond the end of the memory block allocated for
the stack.
[\s-1GH\s0 #15749] <https://github.com/Perl/perl5/issues/15749>

- \(bu
Using a large code point with the \f(CW"W" pack template character with
the current output position aligned at just the right point could
cause a write of a single zero byte immediately beyond the end of an
allocated buffer.
[\s-1GH\s0 #15572] <https://github.com/Perl/perl5/issues/15572>

- \(bu
Supplying a format's picture argument as part of the format argument list
where the picture specifies modifying the argument could cause an
access to the new freed compiled format.
[\s-1GH\s0 #15566] <https://github.com/Perl/perl5/issues/15566>

- \(bu
The **sort()** operator's built-in numeric comparison
function didn't handle large integers that weren't exactly
representable by a double.  This now uses the same code used to
implement the \f(CW\*(C`<=>\*(C' operator.
[\s-1GH\s0 #15768] <https://github.com/Perl/perl5/issues/15768>

- \(bu
Fix issues with \f(CW\*(C`/(?\{ ... <<EOF \})/\*(C' that broke
Method::Signatures.
[\s-1GH\s0 #15779] <https://github.com/Perl/perl5/issues/15779>

- \(bu
Fixed an assertion failure with \f(CW\*(C`chop\*(C' and \f(CW\*(C`chomp\*(C', which
could be triggered by \f(CW\*(C`chop(@x =~ tr/1/1/)\*(C'.
[\s-1GH\s0 #15738] <https://github.com/Perl/perl5/issues/15738>.

- \(bu
Fixed a comment skipping error in patterns under \f(CW\*(C`/x\*(C'; it could stop
skipping a byte early, which could be in the middle of a \s-1UTF-8\s0
character.
[\s-1GH\s0 #15790] <https://github.com/Perl/perl5/issues/15790>.

- \(bu
*perldb* now ignores */dev/tty* on non-Unix systems.
[\s-1GH\s0 #12244] <https://github.com/Perl/perl5/issues/12244>;

- \(bu
Fix assertion failure for \f(CW\*(C`\{\}->$x\*(C' when \f(CW$x isn't defined.
[\s-1GH\s0 #15791] <https://github.com/Perl/perl5/issues/15791>.

- \(bu
Fix an assertion error which could be triggered when a lookahead string
in patterns exceeded a minimum length.
[\s-1GH\s0 #15796] <https://github.com/Perl/perl5/issues/15796>.

- \(bu
Only warn once per literal number about a misplaced \f(CW"_".
[\s-1GH\s0 #9989] <https://github.com/Perl/perl5/issues/9989>.

- \(bu
The \f(CW\*(C`tr///\*(C' parse code could be looking at uninitialized data after a
perse error.
[\s-1GH\s0 #15624] <https://github.com/Perl/perl5/issues/15624>.

- \(bu
In a pattern match, a back-reference (\f(CW\*(C`\\1\*(C') to an unmatched capture could
read back beyond the start of the string being matched.
[\s-1GH\s0 #15634] <https://github.com/Perl/perl5/issues/15634>.

- \(bu
\f(CW\*(C`use re \*(Aqstrict\*(Aq\*(C' is supposed to warn if you use a range (such as
\f(CW\*(C`/(?[ [ X-Y ] ])/\*(C') whose start and end digit aren't from the same group
of 10.  It didn't do that for five groups of mathematical digits starting
at \f(CW\*(C`U+1D7E\*(C'.

- \(bu
A sub containing a \*(L"forward\*(R" declaration with the same name (*e.g.*,
\f(CW\*(C`sub c \{ sub c; \}\*(C') could sometimes crash or loop infinitely.
[\s-1GH\s0 #15557] <https://github.com/Perl/perl5/issues/15557>

- \(bu
A crash in executing a regex with a non-anchored \s-1UTF-8\s0 substring against a
target string that also used \s-1UTF-8\s0 has been fixed.
[\s-1GH\s0 #15628] <https://github.com/Perl/perl5/issues/15628>

- \(bu
Previously, a shebang line like \f(CW\*(C`#!perl -i u\*(C' could be erroneously
interpreted as requesting the \f(CW\*(C`-u\*(C' option.  This has been fixed.
[\s-1GH\s0 #15623] <https://github.com/Perl/perl5/issues/15623>

- \(bu
The regex engine was previously producing incorrect results in some rare
situations when backtracking past an alternation that matches only one
thing; this
showed up as capture buffers (\f(CW$1, \f(CW$2, *etc.*) erroneously containing data
from regex execution paths that weren't actually executed for the final
match.
[\s-1GH\s0 #15666] <https://github.com/Perl/perl5/issues/15666>

- \(bu
Certain regexes making use of the experimental \f(CW\*(C`regex_sets\*(C' feature could
trigger an assertion failure.  This has been fixed.
[\s-1GH\s0 #15620] <https://github.com/Perl/perl5/issues/15620>

- \(bu
Invalid assignments to a reference constructor (*e.g.*, \f(CW\*(C`\\eval=time\*(C') could
sometimes crash in addition to giving a syntax error.
[\s-1GH\s0 #14815] <https://github.com/Perl/perl5/issues/14815>

- \(bu
The parser could sometimes crash if a bareword came after \f(CW\*(C`evalbytes\*(C'.
[\s-1GH\s0 #15586] <https://github.com/Perl/perl5/issues/15586>

- \(bu
Autoloading via a method call would warn erroneously (\*(L"Use of inherited
\s-1AUTOLOAD\s0 for non-method\*(R") if there was a stub present in the package into
which the invocant had been blessed.  The warning is no longer emitted in
such circumstances.
[\s-1GH\s0 #9094] <https://github.com/Perl/perl5/issues/9094>

- \(bu
The use of \f(CW\*(C`splice\*(C' on arrays with non-existent elements could cause other
operators to crash.
[\s-1GH\s0 #15577] <https://github.com/Perl/perl5/issues/15577>

- \(bu
A possible buffer overrun when a pattern contains a fixed utf8 substring.
[\s-1GH\s0 #15534] <https://github.com/Perl/perl5/issues/15534>

- \(bu
Fixed two possible use-after-free bugs in perl's lexer.
[\s-1GH\s0 #15549] <https://github.com/Perl/perl5/issues/15549>

- \(bu
Fixed a crash with \f(CW\*(C`s///l\*(C' where it thought it was dealing with \s-1UTF-8\s0
when it wasn't.
[\s-1GH\s0 #15543] <https://github.com/Perl/perl5/issues/15543>

- \(bu
Fixed a place where the regex parser was not setting the syntax error
correctly on a syntactically incorrect pattern.
[\s-1GH\s0 #15565] <https://github.com/Perl/perl5/issues/15565>

- \(bu
The \f(CW\*(C`&.\*(C' operator (and the \f(CW"&" operator, when it treats its arguments as
strings) were failing to append a trailing null byte if at least one string
was marked as utf8 internally.  Many code paths (system calls, regexp
compilation) still expect there to be a null byte in the string buffer
just past the end of the logical string.  An assertion failure was the
result.
[\s-1GH\s0 #15606] <https://github.com/Perl/perl5/issues/15606>

- \(bu
Avoid a heap-after-use error in the parser when creating an error messge
for a syntactically invalid heredoc.
[\s-1GH\s0 #15527] <https://github.com/Perl/perl5/issues/15527>

- \(bu
Fix a segfault when run with \f(CW\*(C`-DC\*(C' options on \s-1DEBUGGING\s0 builds.
[\s-1GH\s0 #15563] <https://github.com/Perl/perl5/issues/15563>

- \(bu
Fixed the parser error handling in subroutine attributes for an
'\f(CW\*(C`:attr(foo\*(C'' that does not have an ending '\f(CW")"'.

- \(bu
Fix the perl lexer to correctly handle a backslash as the last char in
quoted-string context. This actually fixed two bugs,
[\s-1GH\s0 #15546] <https://github.com/Perl/perl5/issues/15546> and
[\s-1GH\s0 #15582] <https://github.com/Perl/perl5/issues/15582>.

- \(bu
In the \s-1API\s0 function \f(CW\*(C`gv_fetchmethod_pvn_flags\*(C', rework separator parsing
to prevent possible string overrun with an invalid \f(CW\*(C`len\*(C' argument.
[\s-1GH\s0 #15598] <https://github.com/Perl/perl5/issues/15598>

- \(bu
Problems with in-place array sorts: code like \f(CW\*(C`@a = sort \{ ... \} @a\*(C',
where the source and destination of the sort are the same plain array, are
optimised to do less copying around.  Two side-effects of this optimisation
were that the contents of \f(CW@a as seen by sort routines were
partially sorted; and under some circumstances accessing \f(CW@a during the
sort could crash the interpreter.  Both these issues have been fixed, and
Sort functions see the original value of \f(CW@a.
[\s-1GH\s0 #15387] <https://github.com/Perl/perl5/issues/15387>

- \(bu
Non-ASCII string delimiters are now reported correctly in error messages
for unterminated strings.
[\s-1GH\s0 #15469] <https://github.com/Perl/perl5/issues/15469>

- \(bu
\f(CW\*(C`pack("p", ...)\*(C' used to emit its warning (\*(L"Attempt to pack pointer to
temporary value\*(R") erroneously in some cases, but has been fixed.

- \(bu
\f(CW@DB::args is now exempt from \*(L"used once\*(R" warnings.  The warnings only
occurred under **-w**, because *warnings.pm* itself uses \f(CW@DB::args
multiple times.

- \(bu
The use of built-in arrays or hash slices in a double-quoted string no
longer issues a warning (\*(L"Possible unintended interpolation...\*(R") if the
variable has not been mentioned before.  This affected code like
\f(CW\*(C`qq|@DB::args|\*(C' and \f(CW\*(C`qq|@SIG\{\*(AqCHLD\*(Aq, \*(AqHUP\*(Aq\}|\*(C'.  (The special variables
\f(CW\*(C`@-\*(C' and \f(CW\*(C`@+\*(C' were already exempt from the warning.)

- \(bu
\f(CW\*(C`gethostent\*(C' and similar functions now perform a null check internally, to
avoid crashing with the torsocks library.  This was a regression from v5.22.
[\s-1GH\s0 #15478] <https://github.com/Perl/perl5/issues/15478>

- \(bu
\f(CW\*(C`defined *\{\*(Aq!\*(Aq\}\*(C', \f(CW\*(C`defined *\{\*(Aq[\*(Aq\}\*(C', and \f(CW\*(C`defined *\{\*(Aq-\*(Aq\}\*(C' no longer leak
memory if the typeglob in question has never been accessed before.

- \(bu
Mentioning the same constant twice in a row (which is a syntax error) no
longer fails an assertion under debugging builds.  This was a regression
from v5.20.
[\s-1GH\s0 #15017] <https://github.com/Perl/perl5/issues/15017>

- \(bu
Many issues relating to \f(CW\*(C`printf "%a"\*(C' of hexadecimal floating point
were fixed.  In addition, the \*(L"subnormals\*(R" (formerly known as \*(L"denormals\*(R")
floating point numbers are now supported both with the plain \s-1IEEE 754\s0
floating point numbers (64-bit or 128-bit) and the x86 80-bit
\*(L"extended precision\*(R".  Note that subnormal hexadecimal floating
point literals will give a warning about \*(L"exponent underflow\*(R".
[\s-1GH\s0 #15495] <https://github.com/Perl/perl5/issues/15495>
[\s-1GH\s0 #15503] <https://github.com/Perl/perl5/issues/15503>
[\s-1GH\s0 #15504] <https://github.com/Perl/perl5/issues/15504>
[\s-1GH\s0 #15505] <https://github.com/Perl/perl5/issues/15505>
[\s-1GH\s0 #15510] <https://github.com/Perl/perl5/issues/15510>
[\s-1GH\s0 #15512] <https://github.com/Perl/perl5/issues/15512>

- \(bu
A regression in v5.24 with \f(CW\*(C`tr/\\N\{U+...\}/foo/\*(C' when the code point was between
128 and 255 has been fixed.
[\s-1GH\s0 #15475] <https://github.com/Perl/perl5/issues/15475>.

- \(bu
Use of a string delimiter whose code point is above 2**31 now works
correctly on platforms that allow this.  Previously, certain characters,
due to truncation, would be confused with other delimiter characters
with special meaning (such as \f(CW"?" in \f(CW\*(C`m?...?\*(C'), resulting
in inconsistent behaviour.  Note that this is non-portable,
and is based on Perl's extension to \s-1UTF-8,\s0 and is probably not
displayable nor enterable by any editor.
[\s-1GH\s0 #15477] <https://github.com/Perl/perl5/issues/15477>

- \(bu
\f(CW\*(C`@\{x\*(C' followed by a newline where \f(CW"x" represents a control or non-ASCII
character no longer produces a garbled syntax error message or a crash.
[\s-1GH\s0 #15518] <https://github.com/Perl/perl5/issues/15518>

- \(bu
An assertion failure with \f(CW\*(C`%: = 0\*(C' has been fixed.
[\s-1GH\s0 #15358] <https://github.com/Perl/perl5/issues/15358>

- \(bu
In Perl 5.18, the parsing of \f(CW"$foo::$bar" was accidentally changed, such
that it would be treated as \f(CW\*(C`$foo."::".$bar\*(C'.  The previous behavior, which
was to parse it as \f(CW\*(C`$foo:: . $bar\*(C', has been restored.
[\s-1GH\s0 #15408] <https://github.com/Perl/perl5/issues/15408>

- \(bu
Since Perl 5.20, line numbers have been off by one when perl is invoked with
the **-x** switch.  This has been fixed.
[\s-1GH\s0 #15413] <https://github.com/Perl/perl5/issues/15413>

- \(bu
Vivifying a subroutine stub in a deleted stash (*e.g.*,
\f(CW\*(C`delete $My::\{"Foo::"\}; \My::Foo::foo\*(C') no longer crashes.  It had begun
crashing in Perl 5.18.
[\s-1GH\s0 #15420] <https://github.com/Perl/perl5/issues/15420>

- \(bu
Some obscure cases of subroutines and file handles being freed at the same time
could result in crashes, but have been fixed.  The crash was introduced in Perl
5.22.
[\s-1GH\s0 #15435] <https://github.com/Perl/perl5/issues/15435>

- \(bu
Code that looks for a variable name associated with an uninitialized value
could cause an assertion failure in cases where magic is involved, such as
\f(CW$ISA[0][0].  This has now been fixed.
[\s-1GH\s0 #15364] <https://github.com/Perl/perl5/issues/15364>

- \(bu
A crash caused by code generating the warning \*(L"Subroutine \s-1STASH::NAME\s0
redefined\*(R" in cases such as \f(CW\*(C`sub P::f\{\} undef *P::; *P::f =sub\{\};\*(C' has been
fixed.  In these cases, where the \s-1STASH\s0 is missing, the warning will now appear
as \*(L"Subroutine \s-1NAME\s0 redefined\*(R".
[\s-1GH\s0 #15368] <https://github.com/Perl/perl5/issues/15368>

- \(bu
Fixed an assertion triggered by some code that handles deprecated behavior in
formats, *e.g.*, in cases like this:
.Sp
.Vb 3
    format STDOUT =
    @
    0"$x"
.Ve
.Sp
[\s-1GH\s0 #15366] <https://github.com/Perl/perl5/issues/15366>

- \(bu
A possible divide by zero in string transformation code on Windows has been
avoided, fixing a crash when collating an empty string.
[\s-1GH\s0 #15439] <https://github.com/Perl/perl5/issues/15439>

- \(bu
Some regular expression parsing glitches could lead to assertion failures with
regular expressions such as \f(CW\*(C`/(?<=/\*(C' and \f(CW\*(C`/(?<!/\*(C'.  This has now been fixed.
[\s-1GH\s0 #15332] <https://github.com/Perl/perl5/issues/15332>

- \(bu
\f(CW\*(C` until ($x = 1) \{ ... \} \*(C' and \f(CW\*(C` ... until $x = 1 \*(C' now properly
warn when syntax warnings are enabled.
[\s-1GH\s0 #15138] <https://github.com/Perl/perl5/issues/15138>

- \(bu
**socket()** now leaves the error code returned by the system in \f(CW$! on
failure.
[\s-1GH\s0 #15383] <https://github.com/Perl/perl5/issues/15383>

- \(bu
Assignment variants of any bitwise ops under the \f(CW\*(C`bitwise\*(C' feature would
crash if the left-hand side was an array or hash.
[\s-1GH\s0 #15346] <https://github.com/Perl/perl5/issues/15346>

- \(bu
\f(CW\*(C`require\*(C' followed by a single colon (as in \f(CW\*(C`foo() ? require : ...\*(C' is
now parsed correctly as \f(CW\*(C`require\*(C' with implicit \f(CW$_, rather than
\f(CW\*(C`require ""\*(C'.
[\s-1GH\s0 #15380] <https://github.com/Perl/perl5/issues/15380>

- \(bu
Scalar \f(CW\*(C`keys %hash\*(C' can now be assigned to consistently in all scalar
lvalue contexts.  Previously it worked for some contexts but not others.

- \(bu
List assignment to \f(CW\*(C`vec\*(C' or \f(CW\*(C`substr\*(C' with an array or hash for its first
argument used to result in crashes or \*(L"Can't coerce\*(R" error messages at run
time, unlike scalar assignment, which would give an error at compile time.
List assignment now gives a compile-time error, too.
[\s-1GH\s0 #15370] <https://github.com/Perl/perl5/issues/15370>

- \(bu
Expressions containing an \f(CW\*(C`&&\*(C' or \f(CW\*(C`||\*(C' operator (or their synonyms \f(CW\*(C`and\*(C'
and \f(CW\*(C`or\*(C') were being compiled incorrectly in some cases.  If the left-hand
side consisted of either a negated bareword constant or a negated \f(CW\*(C`do \{\}\*(C'
block containing a constant expression, and the right-hand side consisted of
a negated non-foldable expression, one of the negations was effectively
ignored.  The same was true of \f(CW\*(C`if\*(C' and \f(CW\*(C`unless\*(C' statement modifiers,
though with the left-hand and right-hand sides swapped.  This long-standing
bug has now been fixed.
[\s-1GH\s0 #15285] <https://github.com/Perl/perl5/issues/15285>

- \(bu
\f(CW\*(C`reset\*(C' with an argument no longer crashes when encountering stash entries
other than globs.
[\s-1GH\s0 #15314] <https://github.com/Perl/perl5/issues/15314>

- \(bu
Assignment of hashes to, and deletion of, typeglobs named \f(CW*:::::: no
longer causes crashes.
[\s-1GH\s0 #15307] <https://github.com/Perl/perl5/issues/15307>

- \(bu
Perl wasn't correctly handling true/false values in the \s-1LHS\s0 of a list
assign; specifically the truth values returned by boolean operators.
This could trigger an assertion failure in something like the following:
.Sp
.Vb 3
    for ($x > $y) \{
        ($_, ...) = (...); # here $_ is aliased to a truth value
    \}
.Ve
.Sp
This was a regression from v5.24.
[\s-1GH\s0 #15690] <https://github.com/Perl/perl5/issues/15690>

- \(bu
Assertion failure with user-defined Unicode-like properties.
[\s-1GH\s0 #15696] <https://github.com/Perl/perl5/issues/15696>

- \(bu
Fix error message for unclosed \f(CW\*(C`\\N\{\*(C' in a regex.  An unclosed \f(CW\*(C`\\N\{\*(C'
could give the wrong error message:
\f(CW"\\N\{NAME\} must be resolved by the lexer".

- \(bu
List assignment in list context where the \s-1LHS\s0 contained aggregates and
where there were not enough \s-1RHS\s0 elements, used to skip scalar lvalues.
Previously, \f(CW\*(C`(($a,$b,@c,$d) = (1))\*(C' in list context returned \f(CW\*(C`($a)\*(C'; now
it returns \f(CW\*(C`($a,$b,$d)\*(C'.  \f(CW\*(C`(($a,$b,$c) = (1))\*(C' is unchanged: it still
returns \f(CW\*(C`($a,$b,$c)\*(C'.  This can be seen in the following:
.Sp
.Vb 2
    sub inc \{ $_++ for @_ \}
    inc(($a,$b,@c,$d) = (10))
.Ve
.Sp
Formerly, the values of \f(CW\*(C`($a,$b,$d)\*(C' would be left as \f(CW\*(C`(11,undef,undef)\*(C';
now they are \f(CW\*(C`(11,1,1)\*(C'.

- \(bu
Code like this: \f(CW\*(C`/(?\{ s!!! \})/\*(C' could trigger infinite recursion on the C
stack (not the normal perl stack) when the last successful pattern in
scope is itself.  We avoid the segfault by simply forbidding the use of
the empty pattern when it would resolve to the currently executing
pattern.
[\s-1GH\s0 #15669] <https://github.com/Perl/perl5/issues/15669>

- \(bu
Avoid reading beyond the end of the line buffer in perl's lexer when
there's a short \s-1UTF-8\s0 character at the end.
[\s-1GH\s0 #15531] <https://github.com/Perl/perl5/issues/15531>

- \(bu
Alternations in regular expressions were sometimes failing to match
a utf8 string against a utf8 alternate.
[\s-1GH\s0 #15680] <https://github.com/Perl/perl5/issues/15680>

- \(bu
Make \f(CW\*(C`do "a\\0b"\*(C' fail silently (and return \f(CW\*(C`undef\*(C' and set \f(CW$!)
instead of throwing an error.
[\s-1GH\s0 #15676] <https://github.com/Perl/perl5/issues/15676>

- \(bu
\f(CW\*(C`chdir\*(C' with no argument didn't ensure that there was stack space
available for returning its result.
[\s-1GH\s0 #15569] <https://github.com/Perl/perl5/issues/15569>

- \(bu
All error messages related to \f(CW\*(C`do\*(C' now refer to \f(CW\*(C`do\*(C'; some formerly
claimed to be from \f(CW\*(C`require\*(C' instead.

- \(bu
Executing \f(CW\*(C`undef $x\*(C' where \f(CW$x is tied or magical no longer incorrectly
blames the variable for an uninitialized-value warning encountered by the
tied/magical code.

- \(bu
Code like \f(CW\*(C`$x = $x . "a"\*(C' was incorrectly failing to yield a
use of uninitialized value
warning when \f(CW$x was a lexical variable with an undefined value. That has
now been fixed.
[\s-1GH\s0 #15269] <https://github.com/Perl/perl5/issues/15269>

- \(bu
\f(CW\*(C`undef *_; shift\*(C' or \f(CW\*(C`undef *_; pop\*(C' inside a subroutine, with no
argument to \f(CW\*(C`shift\*(C' or \f(CW\*(C`pop\*(C', began crashing in Perl 5.14, but has now
been fixed.

- \(bu
\f(CW"string$scalar->$*" now correctly prefers concatenation
overloading to string overloading if \f(CW\*(C`$scalar->$*\*(C' returns an
overloaded object, bringing it into consistency with \f(CW$$scalar.

- \(bu
\f(CW\*(C`/@0\{0*->@*/*0\*(C' and similar contortions used to crash, but no longer
do, but merely produce a syntax error.
[\s-1GH\s0 #15333] <https://github.com/Perl/perl5/issues/15333>

- \(bu
\f(CW\*(C`do\*(C' or \f(CW\*(C`require\*(C' with an argument which is a reference or typeglob
which, when stringified,
contains a null character, started crashing in Perl 5.20, but has now been
fixed.
[\s-1GH\s0 #15337] <https://github.com/Perl/perl5/issues/15337>

- \(bu
Improve the error message for a missing \f(CW\*(C`tie()\*(C' package/method. This
brings the error messages in line with the ones used for normal method
calls.

- \(bu
Parsing bad \s-1POSIX\s0 charclasses no longer leaks memory.
[\s-1GH\s0 #15382] <https://github.com/Perl/perl5/issues/15382>

## Known Problems

Header "Known Problems"

- \(bu
G++ 6 handles subnormal (denormal) floating point values differently
than gcc 6 or g++ 5 resulting in \*(L"flush-to-zero\*(R". The end result is
that if you specify very small values using the hexadecimal floating
point format, like \f(CW\*(C`0x1.fffffffffffffp-1022\*(C', they become zeros.
[\s-1GH\s0 #15990] <https://github.com/Perl/perl5/issues/15990>

## Errata From Previous Releases

Header "Errata From Previous Releases"

- \(bu
Fixed issues with recursive regexes.  The behavior was fixed in Perl 5.24.
[\s-1GH\s0 #14935] <https://github.com/Perl/perl5/issues/14935>

## Obituary

Header "Obituary"
Jon Portnoy (\s-1AVENJ\s0), a prolific Perl author and admired Gentoo community
member, has passed away on August 10, 2016.  He will be remembered and
missed by all those who he came in contact with, and enriched with his
intellect, wit, and spirit.

It is with great sadness that we also note Kip Hampton's passing.  Probably
best known as the author of the Perl & \s-1XML\s0 column on \s-1XML\s0.com, he was a
core contributor to AxKit, an \s-1XML\s0 server platform that became an Apache
Foundation project.  He was a frequent speaker in the early days at
\s-1OSCON,\s0 and most recently at \s-1YAPC::NA\s0 in Madison.  He was frequently on
irc.perl.org as ubu, generally in the #axkit-dahut community, the
group responsible for \s-1YAPC::NA\s0 Asheville in 2011.

Kip and his constant contributions to the community will be greatly
missed.

## Acknowledgements

Header "Acknowledgements"
Perl 5.26.0 represents approximately 13 months of development since Perl 5.24.0
and contains approximately 360,000 lines of changes across 2,600 files from 86
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 230,000 lines of changes to 1,800 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed the
improvements that became Perl 5.26.0:

Aaron Crane, Abigail, \*(Aevar Arnfjo\*:r\*(d- Bjarmason, Alex Vandiver, Andreas
Ko\*:nig, Andreas Voegele, Andrew Fresh, Andy Lester, Aristotle Pagaltzis, Chad
Granum, Chase Whitener, Chris 'BinGOs' Williams, Chris Lamb, Christian Hansen,
Christian Millour, Colin Newell, Craig A. Berry, Dagfinn Ilmari Mannsa\*oker, Dan
Collins, Daniel Dragan, Dave Cross, Dave Rolsky, David Golden, David H.
Gutteridge, David Mitchell, Dominic Hargreaves, Doug Bell, E. Choroba, Ed Avis,
Father Chrysostomos, Franc\*,ois Perrad, Hauke D, H.Merijn Brand, Hugo van der
Sanden, Ivan Pozdeev, James E Keenan, James Raspass, Jarkko Hietaniemi, Jerry
D. Hedden, Jim Cromie, J. Nick Koston, John Lightsey, Karen Etheridge, Karl
Williamson, Leon Timmermans, Lukas Mai, Matthew Horsfall, Maxwell Carey, Misty
De Meo, Neil Bowers, Nicholas Clark, Nicolas R., Niko Tyni, Pali, Paul
Marquess, Peter Avalos, Petr Pi\*'saX, Pino Toscano, Rafael Garcia-Suarez, Reini
Urban, Renee Baecker, Ricardo Signes, Richard Levitte, Rick Delaney, Salvador
Fandin\*~o, Samuel Thibault, Sawyer X, Se\*'bastien Aperghis-Tramoni, Sergey
Aleynikov, Shlomi Fish, Smylers, Stefan Seifert, Steffen Mu\*:ller, Stevan
Little, Steve Hay, Steven Humphrey, Sullivan Beck, Theo Buehler, Thomas Sibley,
Todd Rinaldo, Tomasz Konojacki, Tony Cook, Unicode Consortium, Yaroslav Kuzmin,
Yves Orton, Zefram.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database at
<https://rt.perl.org/>.  There may also be information at
<http://www.perl.org/>, the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to \f(CW\*(C`perlbug@perl.org\*(C' to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5,
you can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
