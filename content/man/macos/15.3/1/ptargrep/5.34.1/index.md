+++
manpage_section = "1"
title = "ptargrep(1)"
operating_system = "macos"
detected_package_version = "5.34.1"
manpage_format = "troff"
manpage_name = "ptargrep"
operating_system_version = "15.3"
date = "2024-12-14"
author = "None Specified"
description = "This utility allows you to apply pattern matching to the contents of files contained in a tar archive.  You might use this to identify all files in an archive which contain lines matching the specified pattern and either print out the pathnames or ..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PTARGREP 1"
PTARGREP 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

ptargrep - Apply pattern matching to the contents of files in a tar archive

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
  ptargrep [options] <pattern> <tar file> ...

  Options:

   --basename|-b     ignore directory paths from archive
   --ignore-case|-i  do case-insensitive pattern matching
   --list-only|-l    list matching filenames rather than extracting matches
   --verbose|-v      write debugging message to STDERR
   --help|-?         detailed help message
.Ve

## DESCRIPTION

Header "DESCRIPTION"
This utility allows you to apply pattern matching to **the contents** of files
contained in a tar archive.  You might use this to identify all files in an
archive which contain lines matching the specified pattern and either print out
the pathnames or extract the files.

The pattern will be used as a Perl regular expression (as opposed to a simple
grep regex).

Multiple tar archive filenames can be specified - they will each be processed
in turn.

## OPTIONS

Header "OPTIONS"

- \fB--basename (alias -b)
Item "--basename (alias -b)"
When matching files are extracted, ignore the directory path from the archive
and write to the current directory using the basename of the file from the
archive.  Beware: if two matching files in the archive have the same basename,
the second file extracted will overwrite the first.

- \fB--ignore-case (alias -i)
Item "--ignore-case (alias -i)"
Make pattern matching case-insensitive.

- \fB--list-only (alias -l)
Item "--list-only (alias -l)"
Print the pathname of each matching file from the archive to \s-1STDOUT.\s0  Without
this option, the default behaviour is to extract each matching file.

- \fB--verbose (alias -v)
Item "--verbose (alias -v)"
Log debugging info to \s-1STDERR.\s0

- \fB--help (alias -?)
Item "--help (alias -?)"
Display this documentation.

## COPYRIGHT

Header "COPYRIGHT"
Copyright 2010 Grant McLean <grantm@cpan.org>

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.
