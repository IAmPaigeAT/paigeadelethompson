+++
title = "uuname(1)"
operating_system = "macos"
date = "Sun Feb 16 04:48:25 2025"
description = "By default, the  program simply lists the names of all the remote systems mentioned in the UUCP configuration files. The  program may also be used to print the UUCP name of the local system. The  program is mainly for use by shell scripts. List..."
manpage_format = "troff"
operating_system_version = "15.3"
author = "None Specified"
manpage_section = "1"
keywords = ["uucp", "files", "5", "etc", "sys", "system", "configuration", "file", "used", "to", "describe", "all", "known", "sites", "the", "local", "host", "author", "ian", "lance", "taylor", "airs", "com", "text", "for", "this", "manpage", "comes", "from", "version", "1", "07", "info", "documentation"]
manpage_name = "uuname"
detected_package_version = "0"
+++

uuname 1 "Taylor UUCP 1.07"

## NAME

uuname - list remote UUCP sites

## SYNOPSIS

uuname
[-a] [--aliases]

uuname
[-l] [--local]

## DESCRIPTION

By default, the
uuname
program simply lists the names of all the remote systems
mentioned in the UUCP configuration files.

The
uuname
program may also be used to print the UUCP name of the local system.

The
uuname
program is mainly for use by shell scripts.

## OPTIONS


-a, --aliases
List all aliases for remote systems, as well as their canonical names.
Aliases may be specified in the `sys' file.

-l, --local
Print the UUCP name of the local system,
rather than listing the names of all the remote systems.

Standard UUCP options:

-x type, --debug type
Turn on particular debugging types.  The following types are
recognized: abnormal, chat, handshake, uucp-proto, proto, port,
config, spooldir, execute, incoming, outgoing.

-I file, --config file
Set configuration file to use.

-v, --version
Report version information and exit.

--help
Print a help message and exit.

## SEE ALSO

uucp(1)

## FILES


/etc/uucp/sys
UUCP system configuration file used to describe all known sites to the local host.

## AUTHOR

Ian Lance Taylor
<ian@airs.com>.
Text for this Manpage comes from Taylor UUCP, version 1.07 Info documentation.

