+++
date = "2022-02-19"
manpage_name = "perlfaq6"
manpage_section = "1"
title = "perlfaq6(1)"
operating_system_version = "15.3"
operating_system = "macos"
description = "This section is surprisingly small because the rest of the s-1FAQs0 is littered with answers involving regular expressions. For example, decoding a s-1URLs0 and checking whether something is a number can be handled with regular expressions, but tho..."
detected_package_version = "5.34.1"
manpage_format = "troff"
author = "None Specified"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLFAQ6 1"
PERLFAQ6 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlfaq6 - Regular Expressions

## VERSION

Header "VERSION"
version 5.20210411

## DESCRIPTION

Header "DESCRIPTION"
This section is surprisingly small because the rest of the \s-1FAQ\s0 is
littered with answers involving regular expressions. For example,
decoding a \s-1URL\s0 and checking whether something is a number can be handled
with regular expressions, but those answers are found elsewhere in
this document (in perlfaq9: \*(L"How do I decode or create those %-encodings
on the web\*(R" and perlfaq4: \*(L"How do I determine whether a scalar is
a number/whole/integer/float\*(R", to be precise).

### How can I hope to use regular expressions without creating illegible and unmaintainable code?

Xref "regex, legibility regexp, legibility regular expression, legibility x"
Subsection "How can I hope to use regular expressions without creating illegible and unmaintainable code?"
Three techniques can make regular expressions maintainable and
understandable.

- Comments Outside the Regex
Item "Comments Outside the Regex"
Describe what you're doing and how you're doing it, using normal Perl
comments.
.Sp
.Vb 3
    # turn the line into the first word, a colon, and the
    # number of characters on the rest of the line
    s/^(\\w+)(.*)/ lc($1) . ":" . length($2) /meg;
.Ve

- Comments Inside the Regex
Item "Comments Inside the Regex"
The \f(CW\*(C`/x\*(C' modifier causes whitespace to be ignored in a regex pattern
(except in a character class and a few other places), and also allows you to
use normal comments there, too. As you can imagine, whitespace and comments
help a lot.
.Sp
\f(CW\*(C`/x\*(C' lets you turn this:
.Sp
.Vb 1
    s\{<(?:[^>\*(Aq"]*|".*?"|\*(Aq.*?\*(Aq)+>\}\{\}gs;
.Ve
.Sp
into this:
.Sp
.Vb 10
    s\{ <                    # opening angle bracket
        (?:                 # Non-backreffing grouping paren
            [^>\*(Aq"] *        # 0 or more things that are neither > nor \*(Aq nor "
                |           #    or else
            ".*?"           # a section between double quotes (stingy match)
                |           #    or else
            \*(Aq.*?\*(Aq           # a section between single quotes (stingy match)
        ) +                 #   all occurring one or more times
        >                   # closing angle bracket
    \}\{\}gsx;                 # replace with nothing, i.e. delete
.Ve
.Sp
It's still not quite so clear as prose, but it is very useful for
describing the meaning of each part of the pattern.

- Different Delimiters
Item "Different Delimiters"
While we normally think of patterns as being delimited with \f(CW\*(C`/\*(C'
characters, they can be delimited by almost any character. perlre
describes this. For example, the \f(CW\*(C`s///\*(C' above uses braces as
delimiters. Selecting another delimiter can avoid quoting the
delimiter within the pattern:
.Sp
.Vb 2
    s/\\/usr\\/local/\\/usr\\/share/g;    # bad delimiter choice
    s#/usr/local#/usr/share#g;        # better
.Ve
.Sp
Using logically paired delimiters can be even more readable:
.Sp
.Vb 1
    s\{/usr/local/\}\{/usr/share\}g;      # better still
.Ve

### Im having trouble matching over more than one line. Whats wrong?

Xref "regex, multiline regexp, multiline regular expression, multiline"
Subsection "I'm having trouble matching over more than one line. What's wrong?"
Either you don't have more than one line in the string you're looking
at (probably), or else you aren't using the correct modifier(s) on
your pattern (possibly).

There are many ways to get multiline data into a string. If you want
it to happen automatically while reading input, you'll want to set $/
(probably to '' for paragraphs or \f(CW\*(C`undef\*(C' for the whole file) to
allow you to read more than one line at a time.

Read perlre to help you decide which of \f(CW\*(C`/s\*(C' and \f(CW\*(C`/m\*(C' (or both)
you might want to use: \f(CW\*(C`/s\*(C' allows dot to include newline, and \f(CW\*(C`/m\*(C'
allows caret and dollar to match next to a newline, not just at the
end of the string. You do need to make sure that you've actually
got a multiline string in there.

For example, this program detects duplicate words, even when they span
line breaks (but not paragraph ones). For this example, we don't need
\f(CW\*(C`/s\*(C' because we aren't using dot in a regular expression that we want
to cross line boundaries. Neither do we need \f(CW\*(C`/m\*(C' because we don't
want caret or dollar to match at any point inside the record next
to newlines. But it's imperative that $/ be set to something other
than the default, or else we won't actually ever have a multiline
record read in.

.Vb 6
    $/ = \*(Aq\*(Aq;          # read in whole paragraph, not just one line
    while ( <> ) \{
        while ( /\\b([\\w\*(Aq-]+)(\\s+\\g1)+\\b/gi ) \{     # word starts alpha
            print "Duplicate $1 at paragraph $.\\n";
        \}
    \}
.Ve

Here's some code that finds sentences that begin with \*(L"From \*(R" (which would
be mangled by many mailers):

.Vb 6
    $/ = \*(Aq\*(Aq;          # read in whole paragraph, not just one line
    while ( <> ) \{
        while ( /^From /gm ) \{ # /m makes ^ match next to \\n
        print "leading From in paragraph $.\\n";
        \}
    \}
.Ve

Here's code that finds everything between \s-1START\s0 and \s-1END\s0 in a paragraph:

.Vb 6
    undef $/;          # read in whole file, not just one line or paragraph
    while ( <> ) \{
        while ( /START(.*?)END/sgm ) \{ # /s makes . cross line boundaries
            print "$1\\n";
        \}
    \}
.Ve

### How can I pull out lines between two patterns that are themselves on different lines?

Xref ".."
Subsection "How can I pull out lines between two patterns that are themselves on different lines?"
You can use Perl's somewhat exotic \f(CW\*(C`..\*(C' operator (documented in
perlop):

.Vb 1
    perl -ne \*(Aqprint if /START/ .. /END/\*(Aq file1 file2 ...
.Ve

If you wanted text and not lines, you would use

.Vb 1
    perl -0777 -ne \*(Aqprint "$1\\n" while /START(.*?)END/gs\*(Aq file1 file2 ...
.Ve

But if you want nested occurrences of \f(CW\*(C`START\*(C' through \f(CW\*(C`END\*(C', you'll
run up against the problem described in the question in this section
on matching balanced text.

Here's another example of using \f(CW\*(C`..\*(C':

.Vb 7
    while (<>) \{
        my $in_header =   1  .. /^$/;
        my $in_body   = /^$/ .. eof;
    # now choose between them
    \} continue \{
        $. = 0 if eof;    # fix $.
    \}
.Ve

### How do I match \s-1XML, HTML,\s0 or other nasty, ugly things with a regex?

Xref "regex, XML regex, HTML XML HTML pain frustration sucking out, will to live"
Subsection "How do I match XML, HTML, or other nasty, ugly things with a regex?"
Do not use regexes. Use a module and forget about the
regular expressions. The XML::LibXML, HTML::TokeParser and
HTML::TreeBuilder modules are good starts, although each namespace
has other parsing modules specialized for certain tasks and different
ways of doing it. Start at \s-1CPAN\s0 Search ( <http://metacpan.org/> )
and wonder at all the work people have done for you already! :)

### I put a regular expression into $/ but it didnt work. Whats wrong?

Xref "$ , regexes in $INPUT_RECORD_SEPARATOR, regexes in $RS, regexes in"
Subsection "I put a regular expression into $/ but it didn't work. What's wrong?"
$/ has to be a string. You can use these examples if you really need to
do this.

If you have File::Stream, this is easy.

.Vb 1
    use File::Stream;

    my $stream = File::Stream->new(
        $filehandle,
        separator => qr/\\s*,\\s*/,
        );

    print "$_\\n" while <$stream>;
.Ve

If you don't have File::Stream, you have to do a little more work.

You can use the four-argument form of sysread to continually add to
a buffer. After you add to the buffer, you check if you have a
complete line (using your regular expression).

.Vb 7
    local $_ = "";
    while( sysread FH, $_, 8192, length ) \{
        while( s/^((?s).*?)your_pattern// ) \{
            my $record = $1;
            # do stuff here.
        \}
    \}
.Ve

You can do the same thing with foreach and a match using the
c flag and the \\G anchor, if you do not mind your entire file
being in memory at the end.

.Vb 7
    local $_ = "";
    while( sysread FH, $_, 8192, length ) \{
        foreach my $record ( m/\\G((?s).*?)your_pattern/gc ) \{
            # do stuff here.
        \}
        substr( $_, 0, pos ) = "" if pos;
    \}
.Ve

### How do I substitute case-insensitively on the \s-1LHS\s0 while preserving case on the \s-1RHS\s0?

Xref "replace, case preserving substitute, case preserving substitution, case preserving s, case preserving"
Subsection "How do I substitute case-insensitively on the LHS while preserving case on the RHS?"
Here's a lovely Perlish solution by Larry Rosler. It exploits
properties of bitwise xor on \s-1ASCII\s0 strings.

.Vb 1
    $_= "this is a TEsT case";

    $old = \*(Aqtest\*(Aq;
    $new = \*(Aqsuccess\*(Aq;

    s\{(\\Q$old\\E)\}
    \{ uc $new | (uc $1 ^ $1) .
        (uc(substr $1, -1) ^ substr $1, -1) x
        (length($new) - length $1)
    \}egi;

    print;
.Ve

And here it is as a subroutine, modeled after the above:

.Vb 3
    sub preserve_case \{
        my ($old, $new) = @_;
        my $mask = uc $old ^ $old;

        uc $new | $mask .
            substr($mask, -1) x (length($new) - length($old))
    \}

    $string = "this is a TEsT case";
    $string =~ s/(test)/preserve_case($1, "success")/egi;
    print "$string\\n";
.Ve

This prints:

.Vb 1
    this is a SUcCESS case
.Ve

As an alternative, to keep the case of the replacement word if it is
longer than the original, you can use this code, by Jeff Pinyan:

.Vb 3
    sub preserve_case \{
        my ($from, $to) = @_;
        my ($lf, $lt) = map length, @_;

        if ($lt < $lf) \{ $from = substr $from, 0, $lt \}
        else \{ $from .= substr $to, $lf \}

        return uc $to | ($from ^ uc $from);
    \}
.Ve

This changes the sentence to \*(L"this is a SUcCess case.\*(R"

Just to show that C programmers can write C in any programming language,
if you prefer a more C-like solution, the following script makes the
substitution have the same case, letter by letter, as the original.
(It also happens to run about 240% slower than the Perlish solution runs.)
If the substitution has more characters than the string being substituted,
the case of the last character is used for the rest of the substitution.

.Vb 8
    # Original by Nathan Torkington, massaged by Jeffrey Friedl
    #
    sub preserve_case
    \{
        my ($old, $new) = @_;
        my $state = 0; # 0 = no change; 1 = lc; 2 = uc
        my ($i, $oldlen, $newlen, $c) = (0, length($old), length($new));
        my $len = $oldlen < $newlen ? $oldlen : $newlen;

        for ($i = 0; $i < $len; $i++) \{
            if ($c = substr($old, $i, 1), $c =~ /[\\W\\d_]/) \{
                $state = 0;
            \} elsif (lc $c eq $c) \{
                substr($new, $i, 1) = lc(substr($new, $i, 1));
                $state = 1;
            \} else \{
                substr($new, $i, 1) = uc(substr($new, $i, 1));
                $state = 2;
            \}
        \}
        # finish up with any remaining new (for when new is longer than old)
        if ($newlen > $oldlen) \{
            if ($state == 1) \{
                substr($new, $oldlen) = lc(substr($new, $oldlen));
            \} elsif ($state == 2) \{
                substr($new, $oldlen) = uc(substr($new, $oldlen));
            \}
        \}
        return $new;
    \}
.Ve
.ie n .SS "How can I make ""\\w"" match national character sets?"
.el .SS "How can I make \f(CW\\w match national character sets?"
Xref "\w"
Subsection "How can I make w match national character sets?"
Put \f(CW\*(C`use locale;\*(C' in your script. The \\w character class is taken
from the current locale.

See perllocale for details.
.ie n .SS "How can I match a locale-smart version of ""/[a-zA-Z]/""?"
.el .SS "How can I match a locale-smart version of \f(CW/[a-zA-Z]/?"
Xref "alpha"
Subsection "How can I match a locale-smart version of /[a-zA-Z]/?"
You can use the \s-1POSIX\s0 character class syntax \f(CW\*(C`/[[:alpha:]]/\*(C'
documented in perlre.

No matter which locale you are in, the alphabetic characters are
the characters in \\w without the digits and the underscore.
As a regex, that looks like \f(CW\*(C`/[^\\W\\d_]/\*(C'. Its complement,
the non-alphabetics, is then everything in \\W along with
the digits and the underscore, or \f(CW\*(C`/[\\W\\d_]/\*(C'.

### How can I quote a variable to use in a regex?

Xref "regex, escaping regexp, escaping regular expression, escaping"
Subsection "How can I quote a variable to use in a regex?"
The Perl parser will expand \f(CW$variable and \f(CW@variable references in
regular expressions unless the delimiter is a single quote. Remember,
too, that the right-hand side of a \f(CW\*(C`s///\*(C' substitution is considered
a double-quoted string (see perlop for more details). Remember
also that any regex special characters will be acted on unless you
precede the substitution with \\Q. Here's an example:

.Vb 2
    $string = "Placido P. Octopus";
    $regex  = "P.";

    $string =~ s/$regex/Polyp/;
    # $string is now "Polypacido P. Octopus"
.Ve

Because \f(CW\*(C`.\*(C' is special in regular expressions, and can match any
single character, the regex \f(CW\*(C`P.\*(C' here has matched the <Pl> in the
original string.

To escape the special meaning of \f(CW\*(C`.\*(C', we use \f(CW\*(C`\\Q\*(C':

.Vb 2
    $string = "Placido P. Octopus";
    $regex  = "P.";

    $string =~ s/\\Q$regex/Polyp/;
    # $string is now "Placido Polyp Octopus"
.Ve

The use of \f(CW\*(C`\\Q\*(C' causes the \f(CW\*(C`.\*(C' in the regex to be treated as a
regular character, so that \f(CW\*(C`P.\*(C' matches a \f(CW\*(C`P\*(C' followed by a dot.
.ie n .SS "What is ""/o"" really for?"
.el .SS "What is \f(CW/o really for?"
Xref " o, regular expressions compile, regular expressions"
Subsection "What is /o really for?"
(contributed by brian d foy)

The \f(CW\*(C`/o\*(C' option for regular expressions (documented in perlop and
perlreref) tells Perl to compile the regular expression only once.
This is only useful when the pattern contains a variable. Perls 5.6
and later handle this automatically if the pattern does not change.

Since the match operator \f(CW\*(C`m//\*(C', the substitution operator \f(CW\*(C`s///\*(C',
and the regular expression quoting operator \f(CW\*(C`qr//\*(C' are double-quotish
constructs, you can interpolate variables into the pattern. See the
answer to \*(L"How can I quote a variable to use in a regex?\*(R" for more
details.

This example takes a regular expression from the argument list and
prints the lines of input that match it:

.Vb 1
    my $pattern = shift @ARGV;

    while( <> ) \{
        print if m/$pattern/;
    \}
.Ve

Versions of Perl prior to 5.6 would recompile the regular expression
for each iteration, even if \f(CW$pattern had not changed. The \f(CW\*(C`/o\*(C'
would prevent this by telling Perl to compile the pattern the first
time, then reuse that for subsequent iterations:

.Vb 1
    my $pattern = shift @ARGV;

    while( <> ) \{
        print if m/$pattern/o; # useful for Perl < 5.6
    \}
.Ve

In versions 5.6 and later, Perl won't recompile the regular expression
if the variable hasn't changed, so you probably don't need the \f(CW\*(C`/o\*(C'
option. It doesn't hurt, but it doesn't help either. If you want any
version of Perl to compile the regular expression only once even if
the variable changes (thus, only using its initial value), you still
need the \f(CW\*(C`/o\*(C'.

You can watch Perl's regular expression engine at work to verify for
yourself if Perl is recompiling a regular expression. The \f(CW\*(C`use re
\*(Aqdebug\*(Aq\*(C' pragma (comes with Perl 5.005 and later) shows the details.
With Perls before 5.6, you should see \f(CW\*(C`re\*(C' reporting that its
compiling the regular expression on each iteration. With Perl 5.6 or
later, you should only see \f(CW\*(C`re\*(C' report that for the first iteration.

.Vb 1
    use re \*(Aqdebug\*(Aq;

    my $regex = \*(AqPerl\*(Aq;
    foreach ( qw(Perl Java Ruby Python) ) \{
        print STDERR "-" x 73, "\\n";
        print STDERR "Trying $_...\\n";
        print STDERR "\\t$_ is good!\\n" if m/$regex/;
    \}
.Ve

### How do I use a regular expression to strip C-style comments from a file?

Subsection "How do I use a regular expression to strip C-style comments from a file?"
While this actually can be done, it's much harder than you'd think.
For example, this one-liner

.Vb 1
    perl -0777 -pe \*(Aqs\{/\\*.*?\\*/\}\{\}gs\*(Aq foo.c
.Ve

will work in many but not all cases. You see, it's too simple-minded for
certain kinds of C programs, in particular, those with what appear to be
comments in quoted strings. For that, you'd need something like this,
created by Jeffrey Friedl and later modified by Fred Curtis.

.Vb 4
    $/ = undef;
    $_ = <>;
    s#/\\*[^*]*\\*+([^/*][^*]*\\*+)*/|("(\\\\.|[^"\\\\])*"|\*(Aq(\\\\.|[^\*(Aq\\\\])*\*(Aq|.[^/"\*(Aq\\\\]*)#defined $2 ? $2 : ""#gse;
    print;
.Ve

This could, of course, be more legibly written with the \f(CW\*(C`/x\*(C' modifier, adding
whitespace and comments. Here it is expanded, courtesy of Fred Curtis.

.Vb 8
    s\{
       /\\*         ##  Start of /* ... */ comment
       [^*]*\\*+    ##  Non-* followed by 1-or-more *\*(Aqs
       (
         [^/*][^*]*\\*+
       )*          ##  0-or-more things which don\*(Aqt start with /
                   ##    but do end with \*(Aq*\*(Aq
       /           ##  End of /* ... */ comment

     |         ##     OR  various things which aren\*(Aqt comments:

       (
         "           ##  Start of " ... " string
         (
           \\\\.           ##  Escaped char
         |               ##    OR
           [^"\\\\]        ##  Non "\\
         )*
         "           ##  End of " ... " string

       |         ##     OR

         \*(Aq           ##  Start of \*(Aq ... \*(Aq string
         (
           \\\\.           ##  Escaped char
         |               ##    OR
           [^\*(Aq\\\\]        ##  Non \*(Aq\\
         )*
         \*(Aq           ##  End of \*(Aq ... \*(Aq string

       |         ##     OR

         .           ##  Anything other char
         [^/"\*(Aq\\\\]*   ##  Chars which doesn\*(Aqt start a comment, string or escape
       )
     \}\{defined $2 ? $2 : ""\}gxse;
.Ve

A slight modification also removes \*(C+ comments, possibly spanning multiple lines
using a continuation character:

.Vb 1
 s#/\\*[^*]*\\*+([^/*][^*]*\\*+)*/|//([^\\\\]|[^\\n][\\n]?)*?\\n|("(\\\\.|[^"\\\\])*"|\*(Aq(\\\\.|[^\*(Aq\\\\])*\*(Aq|.[^/"\*(Aq\\\\]*)#defined $3 ? $3 : ""#gse;
.Ve

### Can I use Perl regular expressions to match balanced text?

Xref "regex, matching balanced test regexp, matching balanced test regular expression, matching balanced test possessive PARNO Text::Balanced Regexp::Common backtracking recursion"
Subsection "Can I use Perl regular expressions to match balanced text?"
(contributed by brian d foy)

Your first try should probably be the Text::Balanced module, which
is in the Perl standard library since Perl 5.8. It has a variety of
functions to deal with tricky text. The Regexp::Common module can
also help by providing canned patterns you can use.

As of Perl 5.10, you can match balanced text with regular expressions
using recursive patterns. Before Perl 5.10, you had to resort to
various tricks such as using Perl code in \f(CW\*(C`(??\{\})\*(C' sequences.

Here's an example using a recursive regular expression. The goal is to
capture all of the text within angle brackets, including the text in
nested angle brackets. This sample text has two \*(L"major\*(R" groups: a
group with one level of nesting and a group with two levels of
nesting. There are five total groups in angle brackets:

.Vb 3
    I have some <brackets in <nested brackets> > and
    <another group <nested once <nested twice> > >
    and that\*(Aqs it.
.Ve

The regular expression to match the balanced text uses two new (to
Perl 5.10) regular expression features. These are covered in perlre
and this example is a modified version of one in that documentation.

First, adding the new possessive \f(CW\*(C`+\*(C' to any quantifier finds the
longest match and does not backtrack. That's important since you want
to handle any angle brackets through the recursion, not backtracking.
The group \f(CW\*(C`[^<>]++\*(C' finds one or more non-angle brackets without
backtracking.

Second, the new \f(CW\*(C`(?PARNO)\*(C' refers to the sub-pattern in the
particular capture group given by \f(CW\*(C`PARNO\*(C'. In the following regex,
the first capture group finds (and remembers) the balanced text, and
you need that same pattern within the first buffer to get past the
nested text. That's the recursive part. The \f(CW\*(C`(?1)\*(C' uses the pattern
in the outer capture group as an independent part of the regex.

Putting it all together, you have:

.Vb 1
    #!/usr/local/bin/perl5.10.0

    my $string =<<"HERE";
    I have some <brackets in <nested brackets> > and
    <another group <nested once <nested twice> > >
    and that\*(Aqs it.
    HERE

    my @groups = $string =~ m/
            (                   # start of capture group 1
            <                   # match an opening angle bracket
                (?:
                    [^<>]++     # one or more non angle brackets, non backtracking
                      |
                    (?1)        # found < or >, so recurse to capture group 1
                )*
            >                   # match a closing angle bracket
            )                   # end of capture group 1
            /xg;

    $" = "\\n\\t";
    print "Found:\\n\\t@groups\\n";
.Ve

The output shows that Perl found the two major groups:

.Vb 3
    Found:
        <brackets in <nested brackets> >
        <another group <nested once <nested twice> > >
.Ve

With a little extra work, you can get all of the groups in angle
brackets even if they are in other angle brackets too. Each time you
get a balanced match, remove its outer delimiter (that's the one you
just matched so don't match it again) and add it to a queue of strings
to process. Keep doing that until you get no matches:

.Vb 1
    #!/usr/local/bin/perl5.10.0

    my @queue =<<"HERE";
    I have some <brackets in <nested brackets> > and
    <another group <nested once <nested twice> > >
    and that\*(Aqs it.
    HERE

    my $regex = qr/
            (                   # start of bracket 1
            <                   # match an opening angle bracket
                (?:
                    [^<>]++     # one or more non angle brackets, non backtracking
                      |
                    (?1)        # recurse to bracket 1
                )*
            >                   # match a closing angle bracket
            )                   # end of bracket 1
            /x;

    $" = "\\n\\t";

    while( @queue ) \{
        my $string = shift @queue;

        my @groups = $string =~ m/$regex/g;
        print "Found:\\n\\t@groups\\n\\n" if @groups;

        unshift @queue, map \{ s/^<//; s/>$//; $_ \} @groups;
    \}
.Ve

The output shows all of the groups. The outermost matches show up
first and the nested matches show up later:

.Vb 3
    Found:
        <brackets in <nested brackets> >
        <another group <nested once <nested twice> > >

    Found:
        <nested brackets>

    Found:
        <nested once <nested twice> >

    Found:
        <nested twice>
.Ve

### What does it mean that regexes are greedy? How can I get around it?

Xref "greedy greediness"
Subsection "What does it mean that regexes are greedy? How can I get around it?"
Most people mean that greedy regexes match as much as they can.
Technically speaking, it's actually the quantifiers (\f(CW\*(C`?\*(C', \f(CW\*(C`*\*(C', \f(CW\*(C`+\*(C',
\f(CW\*(C`\{\}\*(C') that are greedy rather than the whole pattern; Perl prefers local
greed and immediate gratification to overall greed. To get non-greedy
versions of the same quantifiers, use (\f(CW\*(C`??\*(C', \f(CW\*(C`*?\*(C', \f(CW\*(C`+?\*(C', \f(CW\*(C`\{\}?\*(C').

An example:

.Vb 3
    my $s1 = my $s2 = "I am very very cold";
    $s1 =~ s/ve.*y //;      # I am cold
    $s2 =~ s/ve.*?y //;     # I am very cold
.Ve

Notice how the second substitution stopped matching as soon as it
encountered \*(L"y \*(R". The \f(CW\*(C`*?\*(C' quantifier effectively tells the regular
expression engine to find a match as quickly as possible and pass
control on to whatever is next in line, as you would if you were
playing hot potato.

### How do I process each word on each line?

Xref "word"
Subsection "How do I process each word on each line?"
Use the split function:

.Vb 5
    while (<>) \{
        foreach my $word ( split ) \{
            # do something with $word here
        \}
    \}
.Ve

Note that this isn't really a word in the English sense; it's just
chunks of consecutive non-whitespace characters.

To work with only alphanumeric sequences (including underscores), you
might consider

.Vb 5
    while (<>) \{
        foreach $word (m/(\\w+)/g) \{
            # do something with $word here
        \}
    \}
.Ve

### How can I print out a word-frequency or line-frequency summary?

Subsection "How can I print out a word-frequency or line-frequency summary?"
To do this, you have to parse out each word in the input stream. We'll
pretend that by word you mean chunk of alphabetics, hyphens, or
apostrophes, rather than the non-whitespace chunk idea of a word given
in the previous question:

.Vb 6
    my (%seen);
    while (<>) \{
        while ( /(\\b[^\\W_\\d][\\w\*(Aq-]+\\b)/g ) \{   # misses "\`sheep\*(Aq"
            $seen\{$1\}++;
        \}
    \}

    while ( my ($word, $count) = each %seen ) \{
        print "$count $word\\n";
    \}
.Ve

If you wanted to do the same thing for lines, you wouldn't need a
regular expression:

.Vb 1
    my (%seen);

    while (<>) \{
        $seen\{$_\}++;
    \}

    while ( my ($line, $count) = each %seen ) \{
        print "$count $line";
    \}
.Ve

If you want these output in a sorted order, see perlfaq4: \*(L"How do I
sort a hash (optionally by value instead of key)?\*(R".

### How can I do approximate matching?

Xref "match, approximate matching, approximate"
Subsection "How can I do approximate matching?"
See the module String::Approx available from \s-1CPAN.\s0

### How do I efficiently match many regular expressions at once?

Xref "regex, efficiency regexp, efficiency regular expression, efficiency"
Subsection "How do I efficiently match many regular expressions at once?"
(contributed by brian d foy)

You want to
avoid compiling a regular expression every time you want to match it.
In this example, perl must recompile the regular expression for every
iteration of the \f(CW\*(C`foreach\*(C' loop since \f(CW$pattern can change:

.Vb 1
    my @patterns = qw( fo+ ba[rz] );

    LINE: while( my $line = <> ) \{
        foreach my $pattern ( @patterns ) \{
            if( $line =~ m/\\b$pattern\\b/i ) \{
                print $line;
                next LINE;
            \}
        \}
    \}
.Ve

The \f(CW\*(C`qr//\*(C' operator compiles a regular
expression, but doesn't apply it. When you use the pre-compiled
version of the regex, perl does less work. In this example, I inserted
a \f(CW\*(C`map\*(C' to turn each pattern into its pre-compiled form. The rest of
the script is the same, but faster:

.Vb 1
    my @patterns = map \{ qr/\\b$_\\b/i \} qw( fo+ ba[rz] );

    LINE: while( my $line = <> ) \{
        foreach my $pattern ( @patterns ) \{
            if( $line =~ m/$pattern/ ) \{
                print $line;
                next LINE;
            \}
        \}
    \}
.Ve

In some cases, you may be able to make several patterns into a single
regular expression. Beware of situations that require backtracking
though. In this example, the regex is only compiled once because
\f(CW$regex doesn't change between iterations:

.Vb 1
    my $regex = join \*(Aq|\*(Aq, qw( fo+ ba[rz] );

    while( my $line = <> ) \{
        print if $line =~ m/\\b(?:$regex)\\b/i;
    \}
.Ve

The function \*(L"list2re\*(R" in Data::Munge on \s-1CPAN\s0 can also be used to form
a single regex that matches a list of literal strings (not regexes).

For more details on regular expression efficiency, see \fIMastering
Regular Expressions by Jeffrey Friedl. He explains how the regular
expressions engine works and why some patterns are surprisingly
inefficient. Once you understand how perl applies regular expressions,
you can tune them for individual situations.
.ie n .SS "Why don't word-boundary searches with ""\\b"" work for me?"
.el .SS "Why don't word-boundary searches with \f(CW\\b work for me?"
Xref "\b"
Subsection "Why don't word-boundary searches with b work for me?"
(contributed by brian d foy)

Ensure that you know what \\b really does: it's the boundary between a
word character, \\w, and something that isn't a word character. That
thing that isn't a word character might be \\W, but it can also be the
start or end of the string.

It's not (not!) the boundary between whitespace and non-whitespace,
and it's not the stuff between words we use to create sentences.

In regex speak, a word boundary (\\b) is a \*(L"zero width assertion\*(R",
meaning that it doesn't represent a character in the string, but a
condition at a certain position.

For the regular expression, /\\bPerl\\b/, there has to be a word
boundary before the \*(L"P\*(R" and after the \*(L"l\*(R". As long as something other
than a word character precedes the \*(L"P\*(R" and succeeds the \*(L"l\*(R", the
pattern will match. These strings match /\\bPerl\\b/.

.Vb 4
    "Perl"    # no word char before "P" or after "l"
    "Perl "   # same as previous (space is not a word char)
    "\*(AqPerl\*(Aq"  # the "\*(Aq" char is not a word char
    "Perl\*(Aqs"  # no word char before "P", non-word char after "l"
.Ve

These strings do not match /\\bPerl\\b/.

.Vb 2
    "Perl_"   # "_" is a word char!
    "Perler"  # no word char before "P", but one after "l"
.Ve

You don't have to use \\b to match words though. You can look for
non-word characters surrounded by word characters. These strings
match the pattern /\\b'\\b/.

.Vb 2
    "don\*(Aqt"   # the "\*(Aq" char is surrounded by "n" and "t"
    "qep\*(Aqa\*(Aq"  # the "\*(Aq" char is surrounded by "p" and "a"
.Ve

These strings do not match /\\b'\\b/.

.Vb 1
    "foo\*(Aq"    # there is no word char after non-word "\*(Aq"
.Ve

You can also use the complement of \\b, \\B, to specify that there
should not be a word boundary.

In the pattern /\\Bam\\B/, there must be a word character before the \*(L"a\*(R"
and after the \*(L"m\*(R". These patterns match /\\Bam\\B/:

.Vb 2
    "llama"   # "am" surrounded by word chars
    "Samuel"  # same
.Ve

These strings do not match /\\Bam\\B/

.Vb 2
    "Sam"      # no word boundary before "a", but one after "m"
    "I am Sam" # "am" surrounded by non-word chars
.Ve

### Why does using $&, $`, or $ slow my program down?

Xref "$MATCH $& $POSTMATCH $' $PREMATCH $`"
Subsection "Why does using $&, $`, or $' slow my program down?"
(contributed by Anno Siegel)

Once Perl sees that you need one of these variables anywhere in the
program, it provides them on each and every pattern match. That means
that on every pattern match the entire string will be copied, part of it
to $`, part to $&, and part to $'. Thus the penalty is most severe with
long strings and patterns that match often. Avoid $&, $', and $` if you
can, but if you can't, once you've used them at all, use them at will
because you've already paid the price. Remember that some algorithms
really appreciate them. As of the 5.005 release, the $& variable is no
longer \*(L"expensive\*(R" the way the other two are.

Since Perl 5.6.1 the special variables @- and @+ can functionally replace
$`, $& and $'. These arrays contain pointers to the beginning and end
of each match (see perlvar for the full story), so they give you
essentially the same information, but without the risk of excessive
string copying.

Perl 5.10 added three specials, \f(CW\*(C`$\{^MATCH\}\*(C', \f(CW\*(C`$\{^PREMATCH\}\*(C', and
\f(CW\*(C`$\{^POSTMATCH\}\*(C' to do the same job but without the global performance
penalty. Perl 5.10 only sets these variables if you compile or execute the
regular expression with the \f(CW\*(C`/p\*(C' modifier.
.ie n .SS "What good is ""\\G"" in a regular expression?"
.el .SS "What good is \f(CW\\G in a regular expression?"
Xref "\G"
Subsection "What good is G in a regular expression?"
You use the \f(CW\*(C`\\G\*(C' anchor to start the next match on the same
string where the last match left off. The regular
expression engine cannot skip over any characters to find
the next match with this anchor, so \f(CW\*(C`\\G\*(C' is similar to the
beginning of string anchor, \f(CW\*(C`^\*(C'. The \f(CW\*(C`\\G\*(C' anchor is typically
used with the \f(CW\*(C`g\*(C' modifier. It uses the value of \f(CW\*(C`pos()\*(C'
as the position to start the next match. As the match
operator makes successive matches, it updates \f(CW\*(C`pos()\*(C' with the
position of the next character past the last match (or the
first character of the next match, depending on how you like
to look at it). Each string has its own \f(CW\*(C`pos()\*(C' value.

Suppose you want to match all of consecutive pairs of digits
in a string like \*(L"1122a44\*(R" and stop matching when you
encounter non-digits. You want to match \f(CW11 and \f(CW22 but
the letter \f(CW\*(C`a\*(C' shows up between \f(CW22 and \f(CW44 and you want
to stop at \f(CW\*(C`a\*(C'. Simply matching pairs of digits skips over
the \f(CW\*(C`a\*(C' and still matches \f(CW44.

.Vb 2
    $_ = "1122a44";
    my @pairs = m/(\\d\\d)/g;   # qw( 11 22 44 )
.Ve

If you use the \f(CW\*(C`\\G\*(C' anchor, you force the match after \f(CW22 to
start with the \f(CW\*(C`a\*(C'. The regular expression cannot match
there since it does not find a digit, so the next match
fails and the match operator returns the pairs it already
found.

.Vb 2
    $_ = "1122a44";
    my @pairs = m/\\G(\\d\\d)/g; # qw( 11 22 )
.Ve

You can also use the \f(CW\*(C`\\G\*(C' anchor in scalar context. You
still need the \f(CW\*(C`g\*(C' modifier.

.Vb 4
    $_ = "1122a44";
    while( m/\\G(\\d\\d)/g ) \{
        print "Found $1\\n";
    \}
.Ve

After the match fails at the letter \f(CW\*(C`a\*(C', perl resets \f(CW\*(C`pos()\*(C'
and the next match on the same string starts at the beginning.

.Vb 4
    $_ = "1122a44";
    while( m/\\G(\\d\\d)/g ) \{
        print "Found $1\\n";
    \}

    print "Found $1 after while" if m/(\\d\\d)/g; # finds "11"
.Ve

You can disable \f(CW\*(C`pos()\*(C' resets on fail with the \f(CW\*(C`c\*(C' modifier, documented
in perlop and perlreref. Subsequent matches start where the last
successful match ended (the value of \f(CW\*(C`pos()\*(C') even if a match on the
same string has failed in the meantime. In this case, the match after
the \f(CW\*(C`while()\*(C' loop starts at the \f(CW\*(C`a\*(C' (where the last match stopped),
and since it does not use any anchor it can skip over the \f(CW\*(C`a\*(C' to find
\f(CW44.

.Vb 4
    $_ = "1122a44";
    while( m/\\G(\\d\\d)/gc ) \{
        print "Found $1\\n";
    \}

    print "Found $1 after while" if m/(\\d\\d)/g; # finds "44"
.Ve

Typically you use the \f(CW\*(C`\\G\*(C' anchor with the \f(CW\*(C`c\*(C' modifier
when you want to try a different match if one fails,
such as in a tokenizer. Jeffrey Friedl offers this example
which works in 5.004 or later.

.Vb 9
    while (<>) \{
        chomp;
        PARSER: \{
            m/ \\G( \\d+\\b    )/gcx   && do \{ print "number: $1\\n";  redo; \};
            m/ \\G( \\w+      )/gcx   && do \{ print "word:   $1\\n";  redo; \};
            m/ \\G( \\s+      )/gcx   && do \{ print "space:  $1\\n";  redo; \};
            m/ \\G( [^\\w\\d]+ )/gcx   && do \{ print "other:  $1\\n";  redo; \};
        \}
    \}
.Ve

For each line, the \f(CW\*(C`PARSER\*(C' loop first tries to match a series
of digits followed by a word boundary. This match has to
start at the place the last match left off (or the beginning
of the string on the first match). Since \f(CW\*(C`m/ \\G( \\d+\\b
)/gcx\*(C' uses the \f(CW\*(C`c\*(C' modifier, if the string does not match that
regular expression, perl does not reset **pos()** and the next
match starts at the same position to try a different
pattern.

### Are Perl regexes DFAs or NFAs? Are they \s-1POSIX\s0 compliant?

Xref "DFA NFA POSIX"
Subsection "Are Perl regexes DFAs or NFAs? Are they POSIX compliant?"
While it's true that Perl's regular expressions resemble the DFAs
(deterministic finite automata) of the **egrep**\|(1) program, they are in
fact implemented as NFAs (non-deterministic finite automata) to allow
backtracking and backreferencing. And they aren't POSIX-style either,
because those guarantee worst-case behavior for all cases. (It seems
that some people prefer guarantees of consistency, even when what's
guaranteed is slowness.) See the book \*(L"Mastering Regular Expressions\*(R"
(from O'Reilly) by Jeffrey Friedl for all the details you could ever
hope to know on these matters (a full citation appears in
perlfaq2).

### Whats wrong with using grep in a void context?

Xref "grep"
Subsection "What's wrong with using grep in a void context?"
The problem is that grep builds a return list, regardless of the context.
This means you're making Perl go to the trouble of building a list that
you then just throw away. If the list is large, you waste both time and space.
If your intent is to iterate over the list, then use a for loop for this
purpose.

In perls older than 5.8.1, map suffers from this problem as well.
But since 5.8.1, this has been fixed, and map is context aware - in void
context, no lists are constructed.

### How can I match strings with multibyte characters?

Xref "regex, and multibyte characters regexp, and multibyte characters regular expression, and multibyte characters martian encoding, Martian"
Subsection "How can I match strings with multibyte characters?"
Starting from Perl 5.6 Perl has had some level of multibyte character
support. Perl 5.8 or later is recommended. Supported multibyte
character repertoires include Unicode, and legacy encodings
through the Encode module. See perluniintro, perlunicode,
and Encode.

If you are stuck with older Perls, you can do Unicode with the
Unicode::String module, and character conversions using the
Unicode::Map8 and Unicode::Map modules. If you are using
Japanese encodings, you might try using the jperl 5.005_03.

Finally, the following set of approaches was offered by Jeffrey
Friedl, whose article in issue #5 of The Perl Journal talks about
this very matter.

Let's suppose you have some weird Martian encoding where pairs of
\s-1ASCII\s0 uppercase letters encode single Martian letters (i.e. the two
bytes \*(L"\s-1CV\*(R"\s0 make a single Martian letter, as do the two bytes \*(L"\s-1SG\*(R",
\*(L"VS\*(R", \*(L"XX\*(R",\s0 etc.). Other bytes represent single characters, just like
\s-1ASCII.\s0

So, the string of Martian \*(L"I am \s-1CVSGXX\s0!\*(R" uses 12 bytes to encode the
nine characters 'I', ' ', 'a', 'm', ' ', '\s-1CV\s0', '\s-1SG\s0', '\s-1XX\s0', '!'.

Now, say you want to search for the single character \f(CW\*(C`/GX/\*(C'. Perl
doesn't know about Martian, so it'll find the two bytes \*(L"\s-1GX\*(R"\s0 in the \*(L"I
am \s-1CVSGXX\s0!\*(R" string, even though that character isn't there: it just
looks like it is because \*(L"\s-1SG\*(R"\s0 is next to \*(L"\s-1XX\*(R",\s0 but there's no real
\*(L"\s-1GX\*(R".\s0 This is a big problem.

Here are a few ways, all painful, to deal with it:

.Vb 2
    # Make sure adjacent "martian" bytes are no longer adjacent.
    $martian =~ s/([A-Z][A-Z])/ $1 /g;

    print "found GX!\\n" if $martian =~ /GX/;
.Ve

Or like this:

.Vb 6
    my @chars = $martian =~ m/([A-Z][A-Z]|[^A-Z])/g;
    # above is conceptually similar to:     my @chars = $text =~ m/(.)/g;
    #
    foreach my $char (@chars) \{
        print "found GX!\\n", last if $char eq \*(AqGX\*(Aq;
    \}
.Ve

Or like this:

.Vb 6
    while ($martian =~ m/\\G([A-Z][A-Z]|.)/gs) \{  # \\G probably unneeded
        if ($1 eq \*(AqGX\*(Aq) \{
            print "found GX!\\n";
            last;
        \}
    \}
.Ve

Here's another, slightly less painful, way to do it from Benjamin
Goldberg, who uses a zero-width negative look-behind assertion.

.Vb 5
    print "found GX!\\n" if    $martian =~ m/
        (?<![A-Z])
        (?:[A-Z][A-Z])*?
        GX
        /x;
.Ve

This succeeds if the \*(L"martian\*(R" character \s-1GX\s0 is in the string, and fails
otherwise. If you don't like using (?<!), a zero-width negative
look-behind assertion, you can replace (?<![A-Z]) with (?:^|[^A-Z]).

It does have the drawback of putting the wrong thing in $-[0] and $+[0],
but this usually can be worked around.

### How do I match a regular expression thats in a variable?

Xref "regex, in variable eval regex quotemeta \Q, regex \E, regex qr"
Subsection "How do I match a regular expression that's in a variable?"
(contributed by brian d foy)

We don't have to hard-code patterns into the match operator (or
anything else that works with regular expressions). We can put the
pattern in a variable for later use.

The match operator is a double quote context, so you can interpolate
your variable just like a double quoted string. In this case, you
read the regular expression as user input and store it in \f(CW$regex.
Once you have the pattern in \f(CW$regex, you use that variable in the
match operator.

.Vb 1
    chomp( my $regex = <STDIN> );

    if( $string =~ m/$regex/ ) \{ ... \}
.Ve

Any regular expression special characters in \f(CW$regex are still
special, and the pattern still has to be valid or Perl will complain.
For instance, in this pattern there is an unpaired parenthesis.

.Vb 1
    my $regex = "Unmatched ( paren";

    "Two parens to bind them all" =~ m/$regex/;
.Ve

When Perl compiles the regular expression, it treats the parenthesis
as the start of a memory match. When it doesn't find the closing
parenthesis, it complains:

.Vb 1
    Unmatched ( in regex; marked by <-- HERE in m/Unmatched ( <-- HERE  paren/ at script line 3.
.Ve

You can get around this in several ways depending on our situation.
First, if you don't want any of the characters in the string to be
special, you can escape them with \f(CW\*(C`quotemeta\*(C' before you use the string.

.Vb 2
    chomp( my $regex = <STDIN> );
    $regex = quotemeta( $regex );

    if( $string =~ m/$regex/ ) \{ ... \}
.Ve

You can also do this directly in the match operator using the \f(CW\*(C`\\Q\*(C'
and \f(CW\*(C`\\E\*(C' sequences. The \f(CW\*(C`\\Q\*(C' tells Perl where to start escaping
special characters, and the \f(CW\*(C`\\E\*(C' tells it where to stop (see perlop
for more details).

.Vb 1
    chomp( my $regex = <STDIN> );

    if( $string =~ m/\\Q$regex\\E/ ) \{ ... \}
.Ve

Alternately, you can use \f(CW\*(C`qr//\*(C', the regular expression quote operator (see
perlop for more details). It quotes and perhaps compiles the pattern,
and you can apply regular expression flags to the pattern.

.Vb 1
    chomp( my $input = <STDIN> );

    my $regex = qr/$input/is;

    $string =~ m/$regex/  # same as m/$input/is;
.Ve

You might also want to trap any errors by wrapping an \f(CW\*(C`eval\*(C' block
around the whole thing.

.Vb 1
    chomp( my $input = <STDIN> );

    eval \{
        if( $string =~ m/\\Q$input\\E/ ) \{ ... \}
    \};
    warn $@ if $@;
.Ve

Or...

.Vb 7
    my $regex = eval \{ qr/$input/is \};
    if( defined $regex ) \{
        $string =~ m/$regex/;
    \}
    else \{
        warn $@;
    \}
.Ve

## AUTHOR AND COPYRIGHT

Header "AUTHOR AND COPYRIGHT"
Copyright (c) 1997-2010 Tom Christiansen, Nathan Torkington, and
other authors as noted. All rights reserved.

This documentation is free; you can redistribute it and/or modify it
under the same terms as Perl itself.

Irrespective of its distribution, all code examples in this file
are hereby placed into the public domain. You are permitted and
encouraged to use this code in your own programs for fun
or for profit as you see fit. A simple comment in the code giving
credit would be courteous but is not required.
