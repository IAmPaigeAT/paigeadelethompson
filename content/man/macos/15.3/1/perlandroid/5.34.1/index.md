+++
operating_system = "macos"
description = "This document describes how to set up your host environment when attempting to build Perl for Android. These instructions assume an Unixish build environment on your host system; theyve been tested on Linux and s-1OS X,s0 and may work on Cygwin an..."
detected_package_version = "5.34.1"
title = "perlandroid(1)"
operating_system_version = "15.3"
author = "None Specified"
manpage_section = "1"
date = "2022-02-19"
manpage_format = "troff"
manpage_name = "perlandroid"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLANDROID 1"
PERLANDROID 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlandroid - Perl under Android

## SYNOPSIS

Header "SYNOPSIS"
The first portions of this document contains instructions
to cross-compile Perl for Android 2.0 and later, using the
binaries provided by Google.  The latter portions describe how to build
perl native using one of the toolchains available on the Play Store.

## DESCRIPTION

Header "DESCRIPTION"
This document describes how to set up your host environment when
attempting to build Perl for Android.

## Cross-compilation

Header "Cross-compilation"
These instructions assume an Unixish build environment on your host system;
they've been tested on Linux and \s-1OS X,\s0 and may work on Cygwin and \s-1MSYS.\s0
While Google also provides an \s-1NDK\s0 for Windows, these steps won't work
native there, although it may be possible to cross-compile through different
means.

If your host system's architecture is 32 bits, remember to change the
\f(CW\*(C`x86_64\*(C''s below to \f(CW\*(C`x86\*(C''s.  On a similar vein, the examples below
use the 4.8 toolchain; if you want to use something older or newer (for
example, the 4.4.3 toolchain included in the 8th revision of the \s-1NDK\s0), just
change those to the relevant version.

### Get the Android Native Development Kit (\s-1NDK\s0)

Subsection "Get the Android Native Development Kit (NDK)"
You can download the \s-1NDK\s0 from <https://developer.android.com/tools/sdk/ndk/index.html>.
You'll want the normal, non-legacy version.

### Determine the architecture youll be cross-compiling for

Subsection "Determine the architecture you'll be cross-compiling for"
There's three possible options: arm-linux-androideabi for \s-1ARM,\s0
mipsel-linux-android for \s-1MIPS,\s0 and simply x86 for x86.
As of 2014, most Android devices run on \s-1ARM,\s0 so that is generally a safe bet.

With those two in hand, you should add

.Vb 1
  $ANDROID_NDK/toolchains/$TARGETARCH-4.8/prebuilt/\`uname | tr \*(Aq[A-Z]\*(Aq \*(Aq[a-z]\*(Aq\`-x86_64/bin
.Ve

to your \f(CW\*(C`PATH\*(C', where \f(CW$ANDROID_NDK is the location where you unpacked the
\s-1NDK,\s0 and \f(CW$TARGETARCH is your target's architecture.

### Set up a standalone toolchain

Subsection "Set up a standalone toolchain"
This creates a working sysroot that we can feed to Configure later.

.Vb 7
    $ export ANDROID_TOOLCHAIN=/tmp/my-toolchain-$TARGETARCH
    $ export SYSROOT=$ANDROID_TOOLCHAIN/sysroot
    $ $ANDROID_NDK/build/tools/make-standalone-toolchain.sh \\
            --platform=android-9 \\
            --install-dir=$ANDROID_TOOLCHAIN \\
            --system=\`uname | tr \*(Aq[A-Z]\*(Aq \*(Aq[a-z]\*(Aq\`-x86_64 \\
            --toolchain=$TARGETARCH-4.8
.Ve

### adb or ssh?

Subsection "adb or ssh?"
adb is the Android Debug Bridge.  For our purposes, it's basically a way
of establishing an ssh connection to an Android device without having to
install anything on the device itself, as long as the device is either on
the same local network as the host, or it is connected to the host through
\s-1USB.\s0

Perl can be cross-compiled using either adb or a normal ssh connection;
in general, if you can connect your device to the host using a \s-1USB\s0 port,
or if you don't feel like installing an sshd app on your device,
you may want to use adb, although you may be forced to switch to ssh if
your device is not rooted and you're unlucky \*(-- more on that later.
Alternatively, if you're cross-compiling to an emulator, you'll have to
use adb.

*adb*
Subsection "adb"

To use adb, download the Android \s-1SDK\s0 from <https://developer.android.com/sdk/index.html>.
The \*(L"\s-1SDK\s0 Tools Only\*(R" version should suffice \*(-- if you downloaded the \s-1ADT\s0
Bundle, you can find the sdk under *\f(CI$ADT_BUNDLE\fI/sdk/*.

Add *\f(CI$ANDROID_SDK*/platform-tools* to your \f(CW\*(C`PATH\*(C'*, which should give you access
to adb.  You'll now have to find your device's name using \f(CW\*(C`adb devices\*(C',
and later pass that to Configure through \f(CW\*(C`-Dtargethost=$DEVICE\*(C'.

However, before calling Configure, you need to check if using adb is a
viable choice in the first place.  Because Android doesn't have a */tmp*,
nor does it allow executables in the sdcard, we need to find somewhere in
the device for Configure to put some files in, as well as for the tests
to run in. If your device is rooted, then you're good.  Try running these:

.Vb 2
    $ export TARGETDIR=/mnt/asec/perl
    $ adb -s $DEVICE shell "echo sh -c \*(Aq\\"mkdir $TARGETDIR\\"\*(Aq | su --"
.Ve

Which will create the directory we need, and you can move on to the next
step.  */mnt/asec* is mounted as a tmpfs in Android, but it's only
accessible to root.

If your device is not rooted, you may still be in luck. Try running this:

.Vb 2
    $ export TARGETDIR=/data/local/tmp/perl
    $ adb -s $DEVICE shell "mkdir $TARGETDIR"
.Ve

If the command works, you can move to the next step, but beware:
\fBYou'll have to remove the directory from the device once you are done!
Unlike \f(BI/mnt/asec\fB, \f(BI/data/local/tmp\fB may not get automatically garbage
collected once you shut off the phone.

If neither of those work, then you can't use adb to cross-compile to your
device.  Either try rooting it, or go for the ssh route.

*ssh*
Subsection "ssh"

To use ssh, you'll need to install and run a sshd app and set it up
properly.  There are several paid and free apps that do this rather
easily, so you should be able to spot one on the store.
Remember that Perl requires a passwordless connection, so set up a
public key.

Note that several apps spew crap to stderr every time you
connect, which can throw off Configure.  You may need to monkeypatch
the part of Configure that creates \f(CW\*(C`run-ssh\*(C' to have it discard stderr.

Since you're using ssh, you'll have to pass some extra arguments to
Configure:

.Vb 1
  -Dtargetrun=ssh -Dtargethost=$TARGETHOST -Dtargetuser=$TARGETUSER -Dtargetport=$TARGETPORT
.Ve

### Configure and beyond

Subsection "Configure and beyond"
With all of the previous done, you're now ready to call Configure.

If using adb, a \*(L"basic\*(R" Configure line will look like this:

.Vb 5
  $ ./Configure -des -Dusedevel -Dusecrosscompile -Dtargetrun=adb \\
      -Dcc=$TARGETARCH-gcc   \\
      -Dsysroot=$SYSROOT     \\
      -Dtargetdir=$TARGETDIR \\
      -Dtargethost=$DEVICE
.Ve

If using ssh, it's not too different \*(-- we just change targetrun to ssh,
and pass in targetuser and targetport.  It ends up looking like this:

.Vb 7
  $ ./Configure -des -Dusedevel -Dusecrosscompile -Dtargetrun=ssh \\
      -Dcc=$TARGETARCH-gcc        \\
      -Dsysroot=$SYSROOT          \\
      -Dtargetdir=$TARGETDIR      \\
      -Dtargethost="$TARGETHOST"  \\
      -Dtargetuser=$TARGETUSER    \\
      -Dtargetport=$TARGETPORT
.Ve

Now you're ready to run \f(CW\*(C`make\*(C' and \f(CW\*(C`make test\*(C'!

As a final word of warning, if you're using adb, \f(CW\*(C`make test\*(C' may appear to
hang; this is because it doesn't output anything until it finishes
running all tests.  You can check its progress by logging into the
device, moving to *\f(CI$TARGETDIR**, and looking at the file \fIoutput.stdout*.

*Notes*
Subsection "Notes"

- \(bu
If you are targetting x86 Android, you will have to change \f(CW\*(C`$TARGETARCH-gcc\*(C'
to \f(CW\*(C`i686-linux-android-gcc\*(C'.

- \(bu
On some older low-end devices \*(-- think early 2.2 era \*(-- some tests,
particularly *t/re/uniprops.t*, may crash the phone, causing it to turn
itself off once, and then back on again.

## Native Builds

Header "Native Builds"
While Google doesn't provide a native toolchain for Android,
you can still get one from the Play Store.

### CCTools

Subsection "CCTools"
You may be able to get the CCTools app, which is free.
Keep in mind that you want a full toolchain;
some apps tend to default to installing only a barebones
version without some important utilities, like ar or nm.

Once you have the toolchain set up properly, the only
remaining hurdle is actually locating where in the device it was installed
in.  For example, CCTools installs its toolchain in
*/data/data/com.pdaxrom.cctools/root/cctools*.  With the path in hand,
compiling perl is little more than:

.Vb 3
 export SYSROOT=<location of the native toolchain>
 export LD_LIBRARY_PATH="$SYSROOT/lib:\`pwd\`:\`pwd\`/lib:\`pwd\`/lib/auto:$LD_LIBRARY_PATH"
 sh Configure -des -Dsysroot=$SYSROOT -Alibpth="/system/lib /vendor/lib"
.Ve

### Termux

Subsection "Termux"
Termux <https://termux.com/> provides an Android terminal emulator and Linux environment.
It comes with a cross-compiled perl already installed.

Natively compiling perl 5.30 or later should be as straightforward as:

.Vb 1
 sh Configure -des -Alibpth="/system/lib /vendor/lib"
.Ve

This certainly works on Android 8.1 (Oreo) at least...

## AUTHOR

Header "AUTHOR"
Brian Fraser <fraserbn@gmail.com>
