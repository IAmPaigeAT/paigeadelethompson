+++
date = "2022-02-19"
author = "None Specified"
manpage_section = "1"
title = "perlfilter(1)"
detected_package_version = "5.34.1"
operating_system_version = "15.3"
manpage_format = "troff"
manpage_name = "perlfilter"
operating_system = "macos"
description = "This article is about a little-known feature of Perl called source filters. Source filters alter the program text of a module before Perl sees it, much as a C preprocessor alters the source text of a C program before the compiler sees it. This arti..."
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLFILTER 1"
PERLFILTER 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlfilter - Source Filters

## DESCRIPTION

Header "DESCRIPTION"
This article is about a little-known feature of Perl called
*source filters*. Source filters alter the program text of a module
before Perl sees it, much as a C preprocessor alters the source text of
a C program before the compiler sees it. This article tells you more
about what source filters are, how they work, and how to write your
own.

The original purpose of source filters was to let you encrypt your
program source to prevent casual piracy. This isn't all they can do, as
you'll soon learn. But first, the basics.

## CONCEPTS

Header "CONCEPTS"
Before the Perl interpreter can execute a Perl script, it must first
read it from a file into memory for parsing and compilation. If that
script itself includes other scripts with a \f(CW\*(C`use\*(C' or \f(CW\*(C`require\*(C'
statement, then each of those scripts will have to be read from their
respective files as well.

Now think of each logical connection between the Perl parser and an
individual file as a *source stream*. A source stream is created when
the Perl parser opens a file, it continues to exist as the source code
is read into memory, and it is destroyed when Perl is finished parsing
the file. If the parser encounters a \f(CW\*(C`require\*(C' or \f(CW\*(C`use\*(C' statement in
a source stream, a new and distinct stream is created just for that
file.

The diagram below represents a single source stream, with the flow of
source from a Perl script file on the left into the Perl parser on the
right. This is how Perl normally operates.

.Vb 1
    file -------> parser
.Ve

There are two important points to remember:

- 1.
Although there can be any number of source streams in existence at any
given time, only one will be active.

- 2.
Every source stream is associated with only one file.

A source filter is a special kind of Perl module that intercepts and
modifies a source stream before it reaches the parser. A source filter
changes our diagram like this:

.Vb 1
    file ----> filter ----> parser
.Ve

If that doesn't make much sense, consider the analogy of a command
pipeline. Say you have a shell script stored in the compressed file
*trial.gz*. The simple pipeline command below runs the script without
needing to create a temporary file to hold the uncompressed file.

.Vb 1
    gunzip -c trial.gz | sh
.Ve

In this case, the data flow from the pipeline can be represented as follows:

.Vb 1
    trial.gz ----> gunzip ----> sh
.Ve

With source filters, you can store the text of your script compressed and use a source filter to uncompress it for Perl's parser:

.Vb 2
     compressed           gunzip
    Perl program ---> source filter ---> parser
.Ve

## USING FILTERS

Header "USING FILTERS"
So how do you use a source filter in a Perl script? Above, I said that
a source filter is just a special kind of module. Like all Perl
modules, a source filter is invoked with a use statement.

Say you want to pass your Perl source through the C preprocessor before
execution. As it happens, the source filters distribution comes with a C
preprocessor filter module called Filter::cpp.

Below is an example program, \f(CW\*(C`cpp_test\*(C', which makes use of this filter.
Line numbers have been added to allow specific lines to be referenced
easily.

.Vb 4
    1: use Filter::cpp;
    2: #define TRUE 1
    3: $a = TRUE;
    4: print "a = $a\\n";
.Ve

When you execute this script, Perl creates a source stream for the
file. Before the parser processes any of the lines from the file, the
source stream looks like this:

.Vb 1
    cpp_test ---------> parser
.Ve

Line 1, \f(CW\*(C`use Filter::cpp\*(C', includes and installs the \f(CW\*(C`cpp\*(C' filter
module. All source filters work this way. The use statement is compiled
and executed at compile time, before any more of the file is read, and
it attaches the cpp filter to the source stream behind the scenes. Now
the data flow looks like this:

.Vb 1
    cpp_test ----> cpp filter ----> parser
.Ve

As the parser reads the second and subsequent lines from the source
stream, it feeds those lines through the \f(CW\*(C`cpp\*(C' source filter before
processing them. The \f(CW\*(C`cpp\*(C' filter simply passes each line through the
real C preprocessor. The output from the C preprocessor is then
inserted back into the source stream by the filter.

.Vb 5
                  .-> cpp --.
                  |         |
                  |         |
                  |       <-\*(Aq
   cpp_test ----> cpp filter ----> parser
.Ve

The parser then sees the following code:

.Vb 3
    use Filter::cpp;
    $a = 1;
    print "a = $a\\n";
.Ve

Let's consider what happens when the filtered code includes another
module with use:

.Vb 5
    1: use Filter::cpp;
    2: #define TRUE 1
    3: use Fred;
    4: $a = TRUE;
    5: print "a = $a\\n";
.Ve

The \f(CW\*(C`cpp\*(C' filter does not apply to the text of the Fred module, only
to the text of the file that used it (\f(CW\*(C`cpp_test\*(C'). Although the use
statement on line 3 will pass through the cpp filter, the module that
gets included (\f(CW\*(C`Fred\*(C') will not. The source streams look like this
after line 3 has been parsed and before line 4 is parsed:

.Vb 1
    cpp_test ---> cpp filter ---> parser (INACTIVE)

    Fred.pm ----> parser
.Ve

As you can see, a new stream has been created for reading the source
from \f(CW\*(C`Fred.pm\*(C'. This stream will remain active until all of \f(CW\*(C`Fred.pm\*(C'
has been parsed. The source stream for \f(CW\*(C`cpp_test\*(C' will still exist,
but is inactive. Once the parser has finished reading Fred.pm, the
source stream associated with it will be destroyed. The source stream
for \f(CW\*(C`cpp_test\*(C' then becomes active again and the parser reads line 4
and subsequent lines from \f(CW\*(C`cpp_test\*(C'.

You can use more than one source filter on a single file. Similarly,
you can reuse the same filter in as many files as you like.

For example, if you have a uuencoded and compressed source file, it is
possible to stack a uudecode filter and an uncompression filter like
this:

.Vb 4
    use Filter::uudecode; use Filter::uncompress;
    M\*(AqXL(".H<US4\*(Aq\*(AqV9I;F%L\*(Aq)Q;>7/;1I;_>_I3=&E=%:F*I"T?22Q/
    M6]9*<IQCO*XFT"0[PL%%\*(AqY+IG?WN^ZYN-$\*(AqJ.[.JE$,20/?K=_[>
    ...
.Ve

Once the first line has been processed, the flow will look like this:

.Vb 2
    file ---> uudecode ---> uncompress ---> parser
               filter         filter
.Ve

Data flows through filters in the same order they appear in the source
file. The uudecode filter appeared before the uncompress filter, so the
source file will be uudecoded before it's uncompressed.

## WRITING A SOURCE FILTER

Header "WRITING A SOURCE FILTER"
There are three ways to write your own source filter. You can write it
in C, use an external program as a filter, or write the filter in Perl.
I won't cover the first two in any great detail, so I'll get them out
of the way first. Writing the filter in Perl is most convenient, so
I'll devote the most space to it.

## WRITING A SOURCE FILTER IN C

Header "WRITING A SOURCE FILTER IN C"
The first of the three available techniques is to write the filter
completely in C. The external module you create interfaces directly
with the source filter hooks provided by Perl.

The advantage of this technique is that you have complete control over
the implementation of your filter. The big disadvantage is the
increased complexity required to write the filter - not only do you
need to understand the source filter hooks, but you also need a
reasonable knowledge of Perl guts. One of the few times it is worth
going to this trouble is when writing a source scrambler. The
\f(CW\*(C`decrypt\*(C' filter (which unscrambles the source before Perl parses it)
included with the source filter distribution is an example of a C
source filter (see Decryption Filters, below).

- \fBDecryption Filters
Item "Decryption Filters"
All decryption filters work on the principle of \*(L"security through
obscurity.\*(R" Regardless of how well you write a decryption filter and
how strong your encryption algorithm is, anyone determined enough can
retrieve the original source code. The reason is quite simple - once
the decryption filter has decrypted the source back to its original
form, fragments of it will be stored in the computer's memory as Perl
parses it. The source might only be in memory for a short period of
time, but anyone possessing a debugger, skill, and lots of patience can
eventually reconstruct your program.
.Sp
That said, there are a number of steps that can be taken to make life
difficult for the potential cracker. The most important: Write your
decryption filter in C and statically link the decryption module into
the Perl binary. For further tips to make life difficult for the
potential cracker, see the file *decrypt.pm* in the source filters
distribution.

## CREATING A SOURCE FILTER AS A SEPARATE EXECUTABLE

Header "CREATING A SOURCE FILTER AS A SEPARATE EXECUTABLE"
An alternative to writing the filter in C is to create a separate
executable in the language of your choice. The separate executable
reads from standard input, does whatever processing is necessary, and
writes the filtered data to standard output. \f(CW\*(C`Filter::cpp\*(C' is an
example of a source filter implemented as a separate executable - the
executable is the C preprocessor bundled with your C compiler.

The source filter distribution includes two modules that simplify this
task: \f(CW\*(C`Filter::exec\*(C' and \f(CW\*(C`Filter::sh\*(C'. Both allow you to run any
external executable. Both use a coprocess to control the flow of data
into and out of the external executable. (For details on coprocesses,
see Stephens, W.R., \*(L"Advanced Programming in the \s-1UNIX\s0 Environment.\*(R"
Addison-Wesley, \s-1ISBN 0-210-56317-7,\s0 pages 441-445.) The difference
between them is that \f(CW\*(C`Filter::exec\*(C' spawns the external command
directly, while \f(CW\*(C`Filter::sh\*(C' spawns a shell to execute the external
command. (Unix uses the Bourne shell; \s-1NT\s0 uses the cmd shell.) Spawning
a shell allows you to make use of the shell metacharacters and
redirection facilities.

Here is an example script that uses \f(CW\*(C`Filter::sh\*(C':

.Vb 3
    use Filter::sh \*(Aqtr XYZ PQR\*(Aq;
    $a = 1;
    print "XYZ a = $a\\n";
.Ve

The output you'll get when the script is executed:

.Vb 1
    PQR a = 1
.Ve

Writing a source filter as a separate executable works fine, but a
small performance penalty is incurred. For example, if you execute the
small example above, a separate subprocess will be created to run the
Unix \f(CW\*(C`tr\*(C' command. Each use of the filter requires its own subprocess.
If creating subprocesses is expensive on your system, you might want to
consider one of the other options for creating source filters.

## WRITING A SOURCE FILTER IN PERL

Header "WRITING A SOURCE FILTER IN PERL"
The easiest and most portable option available for creating your own
source filter is to write it completely in Perl. To distinguish this
from the previous two techniques, I'll call it a Perl source filter.

To help understand how to write a Perl source filter we need an example
to study. Here is a complete source filter that performs rot13
decoding. (Rot13 is a very simple encryption scheme used in Usenet
postings to hide the contents of offensive posts. It moves every letter
forward thirteen places, so that A becomes N, B becomes O, and Z
becomes M.)

.Vb 1
   package Rot13;

   use Filter::Util::Call;

   sub import \{
      my ($type) = @_;
      my ($ref) = [];
      filter_add(bless $ref);
   \}

   sub filter \{
      my ($self) = @_;
      my ($status);

      tr/n-za-mN-ZA-M/a-zA-Z/
         if ($status = filter_read()) > 0;
      $status;
   \}

   1;
.Ve

All Perl source filters are implemented as Perl classes and have the
same basic structure as the example above.

First, we include the \f(CW\*(C`Filter::Util::Call\*(C' module, which exports a
number of functions into your filter's namespace. The filter shown
above uses two of these functions, \f(CW\*(C`filter_add()\*(C' and
\f(CW\*(C`filter_read()\*(C'.

Next, we create the filter object and associate it with the source
stream by defining the \f(CW\*(C`import\*(C' function. If you know Perl well
enough, you know that \f(CW\*(C`import\*(C' is called automatically every time a
module is included with a use statement. This makes \f(CW\*(C`import\*(C' the ideal
place to both create and install a filter object.

In the example filter, the object (\f(CW$ref) is blessed just like any
other Perl object. Our example uses an anonymous array, but this isn't
a requirement. Because this example doesn't need to store any context
information, we could have used a scalar or hash reference just as
well. The next section demonstrates context data.

The association between the filter object and the source stream is made
with the \f(CW\*(C`filter_add()\*(C' function. This takes a filter object as a
parameter (\f(CW$ref in this case) and installs it in the source stream.

Finally, there is the code that actually does the filtering. For this
type of Perl source filter, all the filtering is done in a method
called \f(CW\*(C`filter()\*(C'. (It is also possible to write a Perl source filter
using a closure. See the \f(CW\*(C`Filter::Util::Call\*(C' manual page for more
details.) It's called every time the Perl parser needs another line of
source to process. The \f(CW\*(C`filter()\*(C' method, in turn, reads lines from
the source stream using the \f(CW\*(C`filter_read()\*(C' function.

If a line was available from the source stream, \f(CW\*(C`filter_read()\*(C'
returns a status value greater than zero and appends the line to \f(CW$_.
A status value of zero indicates end-of-file, less than zero means an
error. The filter function itself is expected to return its status in
the same way, and put the filtered line it wants written to the source
stream in \f(CW$_. The use of \f(CW$_ accounts for the brevity of most Perl
source filters.

In order to make use of the rot13 filter we need some way of encoding
the source file in rot13 format. The script below, \f(CW\*(C`mkrot13\*(C', does
just that.

.Vb 5
    die "usage mkrot13 filename\\n" unless @ARGV;
    my $in = $ARGV[0];
    my $out = "$in.tmp";
    open(IN, "<$in") or die "Cannot open file $in: $!\\n";
    open(OUT, ">$out") or die "Cannot open file $out: $!\\n";

    print OUT "use Rot13;\\n";
    while (<IN>) \{
       tr/a-zA-Z/n-za-mN-ZA-M/;
       print OUT;
    \}

    close IN;
    close OUT;
    unlink $in;
    rename $out, $in;
.Ve

If we encrypt this with \f(CW\*(C`mkrot13\*(C':

.Vb 1
    print " hello fred \\n";
.Ve

the result will be this:

.Vb 2
    use Rot13;
    cevag "uryyb serq\\a";
.Ve

Running it produces this output:

.Vb 1
    hello fred
.Ve

## USING CONTEXT: THE DEBUG FILTER

Header "USING CONTEXT: THE DEBUG FILTER"
The rot13 example was a trivial example. Here's another demonstration
that shows off a few more features.

Say you wanted to include a lot of debugging code in your Perl script
during development, but you didn't want it available in the released
product. Source filters offer a solution. In order to keep the example
simple, let's say you wanted the debugging output to be controlled by
an environment variable, \f(CW\*(C`DEBUG\*(C'. Debugging code is enabled if the
variable exists, otherwise it is disabled.

Two special marker lines will bracket debugging code, like this:

.Vb 5
    ## DEBUG_BEGIN
    if ($year > 1999) \{
       warn "Debug: millennium bug in year $year\\n";
    \}
    ## DEBUG_END
.Ve

The filter ensures that Perl parses the code between the <\s-1DEBUG_BEGIN\s0>
and \f(CW\*(C`DEBUG_END\*(C' markers only when the \f(CW\*(C`DEBUG\*(C' environment variable
exists. That means that when \f(CW\*(C`DEBUG\*(C' does exist, the code above
should be passed through the filter unchanged. The marker lines can
also be passed through as-is, because the Perl parser will see them as
comment lines. When \f(CW\*(C`DEBUG\*(C' isn't set, we need a way to disable the
debug code. A simple way to achieve that is to convert the lines
between the two markers into comments:

.Vb 5
    ## DEBUG_BEGIN
    #if ($year > 1999) \{
    #     warn "Debug: millennium bug in year $year\\n";
    #\}
    ## DEBUG_END
.Ve

Here is the complete Debug filter:

.Vb 1
    package Debug;

    use strict;
    use warnings;
    use Filter::Util::Call;

    use constant TRUE => 1;
    use constant FALSE => 0;

    sub import \{
       my ($type) = @_;
       my (%context) = (
         Enabled => defined $ENV\{DEBUG\},
         InTraceBlock => FALSE,
         Filename => (caller)[1],
         LineNo => 0,
         LastBegin => 0,
       );
       filter_add(bless \\%context);
    \}

    sub Die \{
       my ($self) = shift;
       my ($message) = shift;
       my ($line_no) = shift || $self->\{LastBegin\};
       die "$message at $self->\{Filename\} line $line_no.\\n"
    \}

    sub filter \{
       my ($self) = @_;
       my ($status);
       $status = filter_read();
       ++ $self->\{LineNo\};

       # deal with EOF/error first
       if ($status <= 0) \{
           $self->Die("DEBUG_BEGIN has no DEBUG_END")
               if $self->\{InTraceBlock\};
           return $status;
       \}

       if ($self->\{InTraceBlock\}) \{
          if (/^\\s*##\\s*DEBUG_BEGIN/ ) \{
              $self->Die("Nested DEBUG_BEGIN", $self->\{LineNo\})
          \} elsif (/^\\s*##\\s*DEBUG_END/) \{
              $self->\{InTraceBlock\} = FALSE;
          \}

          # comment out the debug lines when the filter is disabled
          s/^/#/ if ! $self->\{Enabled\};
       \} elsif ( /^\\s*##\\s*DEBUG_BEGIN/ ) \{
          $self->\{InTraceBlock\} = TRUE;
          $self->\{LastBegin\} = $self->\{LineNo\};
       \} elsif ( /^\\s*##\\s*DEBUG_END/ ) \{
          $self->Die("DEBUG_END has no DEBUG_BEGIN", $self->\{LineNo\});
       \}
       return $status;
    \}

    1;
.Ve

The big difference between this filter and the previous example is the
use of context data in the filter object. The filter object is based on
a hash reference, and is used to keep various pieces of context
information between calls to the filter function. All but two of the
hash fields are used for error reporting. The first of those two,
Enabled, is used by the filter to determine whether the debugging code
should be given to the Perl parser. The second, InTraceBlock, is true
when the filter has encountered a \f(CW\*(C`DEBUG_BEGIN\*(C' line, but has not yet
encountered the following \f(CW\*(C`DEBUG_END\*(C' line.

If you ignore all the error checking that most of the code does, the
essence of the filter is as follows:

.Vb 4
    sub filter \{
       my ($self) = @_;
       my ($status);
       $status = filter_read();

       # deal with EOF/error first
       return $status if $status <= 0;
       if ($self->\{InTraceBlock\}) \{
          if (/^\\s*##\\s*DEBUG_END/) \{
             $self->\{InTraceBlock\} = FALSE
          \}

          # comment out debug lines when the filter is disabled
          s/^/#/ if ! $self->\{Enabled\};
       \} elsif ( /^\\s*##\\s*DEBUG_BEGIN/ ) \{
          $self->\{InTraceBlock\} = TRUE;
       \}
       return $status;
    \}
.Ve

Be warned: just as the C-preprocessor doesn't know C, the Debug filter
doesn't know Perl. It can be fooled quite easily:

.Vb 3
    print <<EOM;
    ##DEBUG_BEGIN
    EOM
.Ve

Such things aside, you can see that a lot can be achieved with a modest
amount of code.

## CONCLUSION

Header "CONCLUSION"
You now have better understanding of what a source filter is, and you
might even have a possible use for them. If you feel like playing with
source filters but need a bit of inspiration, here are some extra
features you could add to the Debug filter.

First, an easy one. Rather than having debugging code that is
all-or-nothing, it would be much more useful to be able to control
which specific blocks of debugging code get included. Try extending the
syntax for debug blocks to allow each to be identified. The contents of
the \f(CW\*(C`DEBUG\*(C' environment variable can then be used to control which
blocks get included.

Once you can identify individual blocks, try allowing them to be
nested. That isn't difficult either.

Here is an interesting idea that doesn't involve the Debug filter.
Currently Perl subroutines have fairly limited support for formal
parameter lists. You can specify the number of parameters and their
type, but you still have to manually take them out of the \f(CW@_ array
yourself. Write a source filter that allows you to have a named
parameter list. Such a filter would turn this:

.Vb 1
    sub MySub ($first, $second, @rest) \{ ... \}
.Ve

into this:

.Vb 6
    sub MySub($$@) \{
       my ($first) = shift;
       my ($second) = shift;
       my (@rest) = @_;
       ...
    \}
.Ve

Finally, if you feel like a real challenge, have a go at writing a
full-blown Perl macro preprocessor as a source filter. Borrow the
useful features from the C preprocessor and any other macro processors
you know. The tricky bit will be choosing how much knowledge of Perl's
syntax you want your filter to have.

## LIMITATIONS

Header "LIMITATIONS"
Source filters only work on the string level, thus are highly limited
in its ability to change source code on the fly. It cannot detect
comments, quoted strings, heredocs, it is no replacement for a real
parser.
The only stable usage for source filters are encryption, compression,
or the byteloader, to translate binary code back to source code.

See for example the limitations in Switch, which uses source filters,
and thus is does not work inside a string eval, the presence of
regexes with embedded newlines that are specified with raw \f(CW\*(C`/.../\*(C'
delimiters and don't have a modifier \f(CW\*(C`//x\*(C' are indistinguishable from
code chunks beginning with the division operator \f(CW\*(C`/\*(C'. As a workaround
you must use \f(CW\*(C`m/.../\*(C' or \f(CW\*(C`m?...?\*(C' for such patterns. Also, the presence of
regexes specified with raw \f(CW\*(C`?...?\*(C' delimiters may cause mysterious
errors. The workaround is to use \f(CW\*(C`m?...?\*(C' instead.  See
<https://metacpan.org/pod/Switch#LIMITATIONS>.

Currently the content of the \f(CW\*(C`_\|_DATA_\|_\*(C' block is not filtered.

Currently internal buffer lengths are limited to 32-bit only.

## THINGS TO LOOK OUT FOR

Header "THINGS TO LOOK OUT FOR"
.ie n .IP "Some Filters Clobber the ""DATA"" Handle" 5
.el .IP "Some Filters Clobber the \f(CWDATA Handle" 5
Item "Some Filters Clobber the DATA Handle"
Some source filters use the \f(CW\*(C`DATA\*(C' handle to read the calling program.
When using these source filters you cannot rely on this handle, nor expect
any particular kind of behavior when operating on it.  Filters based on
Filter::Util::Call (and therefore Filter::Simple) do not alter the \f(CW\*(C`DATA\*(C'
filehandle, but on the other hand totally ignore the text after \f(CW\*(C`_\|_DATA_\|_\*(C'.

## REQUIREMENTS

Header "REQUIREMENTS"
The Source Filters distribution is available on \s-1CPAN,\s0 in

.Vb 1
    CPAN/modules/by-module/Filter
.Ve

Starting from Perl 5.8 Filter::Util::Call (the core part of the
Source Filters distribution) is part of the standard Perl distribution.
Also included is a friendlier interface called Filter::Simple, by
Damian Conway.

## AUTHOR

Header "AUTHOR"
Paul Marquess <Paul.Marquess@btinternet.com>

Reini Urban <rurban@cpan.org>

## Copyrights

Header "Copyrights"
The first version of this article originally appeared in The Perl
Journal #11, and is copyright 1998 The Perl Journal. It appears
courtesy of Jon Orwant and The Perl Journal.  This document may be
distributed under the same terms as Perl itself.
