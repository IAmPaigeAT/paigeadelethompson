+++
date = "2024-12-14"
description = "This program will read data from f(CW*(C`stdin*(C, compress it into a zip container and, by default, write a streamed zip file to f(CW*(C`stdout*(C. No temporary files are created. The zip container written to f(CW*(C`stdout*(C is, by necessity,..."
title = "streamzip(1)"
detected_package_version = "5.34.1"
manpage_format = "troff"
manpage_name = "streamzip"
author = "None Specified"
operating_system = "macos"
operating_system_version = "15.3"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "STREAMZIP 1"
STREAMZIP 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

streamzip - create a zip file from stdin

## SYNOPSIS

Header "SYNOPSIS"
.Vb 2
    producer | streamzip [opts] | consumer
    producer | streamzip [opts] -zipfile=output.zip
.Ve

## DESCRIPTION

Header "DESCRIPTION"
This program will read data from \f(CW\*(C`stdin\*(C', compress it into a zip container
and, by default, write a *streamed* zip file to \f(CW\*(C`stdout\*(C'. No temporary
files are created.

The zip container written to \f(CW\*(C`stdout\*(C' is, by necessity, written in
streaming format. Most programs that read Zip files can cope with a
streamed zip file, but if interoperability is important, and your workflow
allows you to write the zip file directly to disk you can create a
non-streamed zip file using the \f(CW\*(C`zipfile\*(C' option.

### \s-1OPTIONS\s0

Subsection "OPTIONS"

- -zip64
Item "-zip64"
Create a Zip64-compliant zip container. Use this option if the input is
greater than 4Gig.
.Sp
Default is disabled.

- -zipfile=F
Item "-zipfile=F"
Write zip container to the filename \f(CW\*(C`F\*(C'.
.Sp
Use the \f(CW\*(C`Stream\*(C' option to force the creation of a streamed zip file.

- -member-name=M
Item "-member-name=M"
This option is used to name the \*(L"file\*(R" in the zip container.
.Sp
Default is '-'.

- -stream
Item "-stream"
Ignored when writing to \f(CW\*(C`stdout\*(C'.
.Sp
If the \f(CW\*(C`zipfile\*(C' option is specified, including this option will trigger
the creation of a streamed zip file.
.Sp
Default: Always enabled when writing to \f(CW\*(C`stdout\*(C', otherwise disabled.

- -method=M
Item "-method=M"
Compress using method \f(CW\*(C`M\*(C'.
.Sp
Valid method names are
.Sp
.Vb 6
    * store    Store without compression
    * deflate  Use Deflate compression [Deflault]
    * bzip2    Use Bzip2 compression
    * lzma     Use LZMA compression
    * xz       Use xz compression
    * zstd     Use Zstandard compression
.Ve
.Sp
Note that Lzma compress needs \f(CW\*(C`IO::Compress::Lzma\*(C' to be installed.
.Sp
Note that Zstd compress needs \f(CW\*(C`IO::Compress::Zstd\*(C' to be installed.
.Sp
Default is \f(CW\*(C`deflate\*(C'.

- -0, -1, -2, -3, -4, -5, -6, -7, -8, -9
Item "-0, -1, -2, -3, -4, -5, -6, -7, -8, -9"
Sets the compression level for \f(CW\*(C`deflate\*(C'. Ignored for all other compression methods.
.Sp
\f(CW\*(C`-0\*(C' means no compression and \f(CW\*(C`-9\*(C' for maximum compression.
.Sp
Default is 6

- -version
Item "-version"
Display version number

- -help
Item "-help"
Display help

### Examples

Subsection "Examples"
Create a zip file bt reading daa from stdin

.Vb 1
    $ echo Lorem ipsum dolor sit | perl ./bin/streamzip >abcd.zip
.Ve

Check the contents of \f(CW\*(C`abcd,zip\*(C' with the standard \f(CW\*(C`unzip\*(C' utility

.Vb 6
    Archive:  abcd.zip
      Length      Date    Time    Name
    ---------  ---------- -----   ----
           22  2021-01-08 19:45   -
    ---------                     -------
           22                     1 file
.Ve

Notice how the \f(CW\*(C`Name\*(C' is set to \f(CW\*(C`-\*(C'.
That is the default for a few zip utilities whwre the member name is not given.

If you want to explicitly name the file, use the \f(CW\*(C`-member-name\*(C' option as follows

.Vb 1
    $ echo Lorem ipsum dolor sit | perl ./bin/streamzip -member-name latin >abcd.zip

    $ unzip -l abcd.zip
    Archive:  abcd.zip
      Length      Date    Time    Name
    ---------  ---------- -----   ----
           22  2021-01-08 19:47   latin
    ---------                     -------
           22                     1 file
.Ve

### When to write a Streamed Zip File

Subsection "When to write a Streamed Zip File"
A Streamed Zip File is useful in situations where you cannot seek
backwards/forwards in the file.

A good examples is when you are serving dynamic content from a Web Server
straight into a socket without needing to create a temporary zip file in
the filesystsm.

Similarly if your workfow uses a Linux pipelined commands.

## SUPPORT

Header "SUPPORT"
General feedback/questions/bug reports should be sent to
<https://github.com/pmqs/IO-Compress/issues> (preferred) or
<https://rt.cpan.org/Public/Dist/Display.html?Name=IO-Compress>.

## AUTHOR

Header "AUTHOR"
Paul Marquess *pmqs@cpan.org*.

## COPYRIGHT

Header "COPYRIGHT"
Copyright (c) 2019-2021 Paul Marquess. All rights reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.
