+++
manpage_name = "postkick"
manpage_format = "troff"
manpage_section = "1"
description = "The postkick(1) command sends request to the specified service over a local transport channel. This command makes Postfix private IPC accessible for use in, for example, shell scripts."
date = "Sun Feb 16 04:48:25 2025"
detected_package_version = "0"
operating_system = "macos"
title = "postkick(1)"
operating_system_version = "15.3"
author = "None Specified"
keywords = ["na", "qmgr", "queue", "manager", "trigger", "protocol", "pickup", "local", "daemon", "postconf", "configuration", "parameters", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
+++

POSTKICK 1


## NAME

postkick
-
kick a Postfix service

## SYNOPSIS

.na

```
```

**postkick** [**-c **config_dir] [**-v**]
*class service request*

## DESCRIPTION


The **postkick**(1) command sends *request* to the
specified *service* over a local transport channel.
This command makes Postfix private IPC accessible
for use in, for example, shell scripts.

Options:

- \fB-c
Read the **main.cf** configuration file in the named directory
instead of the default configuration directory.
**-v**
Enable verbose logging for debugging purposes. Multiple **-v**
options make the software increasingly verbose.

Arguments:
*class*
Name of a class of local transport channel endpoints,
either **public** (accessible by any local user) or
**private** (administrative access only).
*service*
The name of a local transport endpoint within the named class.
*request*
A string. The list of valid requests is service-specific.

## DIAGNOSTICS


Problems and transactions are logged to the standard error
stream.

## ENVIRONMENT

.na

```
.ad
```

**MAIL_CONFIG**
Directory with Postfix configuration files.
**MAIL_VERBOSE**
Enable verbose logging for debugging purposes.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

The following **main.cf** parameters are especially relevant to
this program.
The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBapplication_event_drain_time
How long the **postkick**(1) command waits for a request to enter the
Postfix daemon process input buffer before giving up.

- \fBimport_environment (see 'postconf -d'
The list of environment parameters that a privileged Postfix
process will import from a non-Postfix parent process, or name=value
environment overrides.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

## FILES

.na

```
/var/spool/postfix/private, private class endpoints
/var/spool/postfix/public, public class endpoints
.SH "SEE ALSO"
.na

```
qmgr(8), queue manager trigger protocol
pickup(8), local pickup daemon
postconf(5), configuration parameters
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
