+++
title = "postlog(1)"
description = "The postlog(1) command implements a Postfix-compatible logging interface for use in, for example, shell scripts."
detected_package_version = "0"
author = "None Specified"
keywords = ["na", "postconf", "configuration", "parameters", "syslogd", "syslog", "daemon", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
manpage_section = "1"
operating_system = "macos"
operating_system_version = "15.3"
manpage_format = "troff"
manpage_name = "postlog"
date = "Sun Feb 16 04:48:22 2025"
+++

POSTLOG 1


## NAME

postlog
-
Postfix-compatible logging utility

## SYNOPSIS

.na

```
```

**postlog** [**-iv**] [**-c **config_dir]
[**-p **priority] [**-t **tag] [*text...*]

## DESCRIPTION


The **postlog**(1) command implements a Postfix-compatible logging
interface for use in, for example, shell scripts.

By default, **postlog**(1) logs the *text* given on the command
line as one record. If no *text* is specified on the command
line, **postlog**(1) reads from standard input and logs each input
line as one record.

Logging is sent to **syslogd**(8); when the standard error stream
is connected to a terminal, logging is sent there as well.

The following options are implemented:

- \fB-c
Read the **main.cf** configuration file in the named directory
instead of the default configuration directory.
**-i**
Include the process ID in the logging tag.

- \fB-p \fIpriority (default:
Specifies the logging severity: **info**, **warn**,
**error**, **fatal**, or **panic**. With Postfix 3.1
and later, the program will pause for 1 second after reporting
a **fatal** or **panic** condition, just like other
Postfix programs.

- \fB-t
Specifies the logging tag, that is, the identifying name that
appears at the beginning of each logging record. A default tag
is used when none is specified.
**-v**
Enable verbose logging for debugging purposes. Multiple **-v**
options make the software increasingly verbose.

## ENVIRONMENT

.na

```
.ad
```

MAIL_CONFIG
Directory with the **main.cf** file.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

The following **main.cf** parameters are especially relevant to
this program.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBimport_environment (see 'postconf -d'
The list of environment parameters that a privileged Postfix
process will import from a non-Postfix parent process, or name=value
environment overrides.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## SEE ALSO

.na

```
postconf(5), configuration parameters
syslogd(8), syslog daemon
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
