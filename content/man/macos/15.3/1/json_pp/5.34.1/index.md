+++
manpage_name = "json_pp"
operating_system_version = "15.3"
author = "None Specified"
detected_package_version = "5.34.1"
operating_system = "macos"
keywords = ["header", "see", "also", "s-1json", "pp", "s0", "json_xs", "author", "makamaka", "hannyaharamitu", "at", "cpan", "org", "copyright", "and", "license", "2010", "by", "this", "library", "is", "free", "software", "you", "can", "redistribute", "it", "or", "modify", "under", "the", "same", "terms", "as", "perl", "itself"]
manpage_section = "1"
description = "json_pp converts between some input and output formats (one of them is s-1JSONs0). This program was copied from json_xs and modified. The default input format is json and the default output format is json with pretty option.     -f from_format Rea..."
manpage_format = "troff"
title = "json_pp(1)"
date = "2024-12-14"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "JSON_PP 1"
JSON_PP 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

json_pp - JSON::PP command utility

## SYNOPSIS

Header "SYNOPSIS"
.Vb 1
    json_pp [-v] [-f from_format] [-t to_format] [-json_opt options_to_json1[,options_to_json2[,...]]]
.Ve

## DESCRIPTION

Header "DESCRIPTION"
json_pp converts between some input and output formats (one of them is \s-1JSON\s0).
This program was copied from json_xs and modified.

The default input format is json and the default output format is json with pretty option.

## OPTIONS

Header "OPTIONS"

### -f

Subsection "-f"
.Vb 1
    -f from_format
.Ve

Reads a data in the given format from \s-1STDIN.\s0

Format types:

- json
Item "json"
as \s-1JSON\s0

- eval
Item "eval"
as Perl code

### -t

Subsection "-t"
Writes a data in the given format to \s-1STDOUT.\s0

- null
Item "null"
no action.

- json
Item "json"
as \s-1JSON\s0

- dumper
Item "dumper"
as Data::Dumper

### -json_opt

Subsection "-json_opt"
options to \s-1JSON::PP\s0

Acceptable options are:

.Vb 2
    ascii latin1 utf8 pretty indent space_before space_after relaxed canonical allow_nonref
    allow_singlequote allow_barekey allow_bignum loose escape_slash indent_length
.Ve

Multiple options must be separated by commas:

.Vb 1
    Right: -json_opt pretty,canonical

    Wrong: -json_opt pretty -json_opt canonical
.Ve

### -v

Subsection "-v"
Verbose option, but currently no action in fact.

### -V

Subsection "-V"
Prints version and exits.

## EXAMPLES

Header "EXAMPLES"
.Vb 2
    $ perl -e\*(Aqprint q|\{"foo":"XX","bar":1234567890000000000000000\}|\*(Aq |\\
       json_pp -f json -t dumper -json_opt pretty,utf8,allow_bignum

    $VAR1 = \{
              \*(Aqbar\*(Aq => bless( \{
                                \*(Aqvalue\*(Aq => [
                                             \*(Aq0000000\*(Aq,
                                             \*(Aq0000000\*(Aq,
                                             \*(Aq5678900\*(Aq,
                                             \*(Aq1234\*(Aq
                                           ],
                                \*(Aqsign\*(Aq => \*(Aq+\*(Aq
                              \}, \*(AqMath::BigInt\*(Aq ),
              \*(Aqfoo\*(Aq => "\\x\{3042\}\\x\{3044\}"
            \};

    $ perl -e\*(Aqprint q|\{"foo":"XX","bar":1234567890000000000000000\}|\*(Aq |\\
       json_pp -f json -t dumper -json_opt pretty

    $VAR1 = \{
              \*(Aqbar\*(Aq => \*(Aq1234567890000000000000000\*(Aq,
              \*(Aqfoo\*(Aq => "\\x\{e3\}\\x\{81\}\\x\{82\}\\x\{e3\}\\x\{81\}\\x\{84\}"
            \};
.Ve

## SEE ALSO

Header "SEE ALSO"
\s-1JSON::PP\s0, json_xs

## AUTHOR

Header "AUTHOR"
Makamaka Hannyaharamitu, <makamaka[at]cpan.org>

## COPYRIGHT AND LICENSE

Header "COPYRIGHT AND LICENSE"
Copyright 2010 by Makamaka Hannyaharamitu

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.
