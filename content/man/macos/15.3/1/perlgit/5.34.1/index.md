+++
author = "None Specified"
manpage_name = "perlgit"
manpage_format = "troff"
title = "perlgit(1)"
date = "2022-02-19"
description = "This document provides details on using git to develop Perl. If you are just interested in working on a quick patch, see perlhack first. This document is intended for people who are regular contributors to Perl, including those with write access to..."
operating_system_version = "15.3"
operating_system = "macos"
manpage_section = "1"
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLGIT 1"
PERLGIT 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlgit - Detailed information about git and the Perl repository

## DESCRIPTION

Header "DESCRIPTION"
This document provides details on using git to develop Perl. If you are
just interested in working on a quick patch, see perlhack first.
This document is intended for people who are regular contributors to
Perl, including those with write access to the git repository.

## CLONING THE REPOSITORY

Header "CLONING THE REPOSITORY"
All of Perl's source code is kept centrally in a Git repository at
*github.com*.

You can make a read-only clone of the repository by running:

.Vb 1
  % git clone git://github.com/Perl/perl5.git perl
.Ve

This uses the git protocol (port 9418).

If you cannot use the git protocol for firewall reasons, you can also
clone via http:

.Vb 1
  % git clone https://github.com/Perl/perl5.git perl
.Ve

## WORKING WITH THE REPOSITORY

Header "WORKING WITH THE REPOSITORY"
Once you have changed into the repository directory, you can inspect
it. After a clone the repository will contain a single local branch,
which will be the current branch as well, as indicated by the asterisk.

.Vb 2
  % git branch
  * blead
.Ve

Using the -a switch to \f(CW\*(C`branch\*(C' will also show the remote tracking
branches in the repository:

.Vb 5
  % git branch -a
  * blead
    origin/HEAD
    origin/blead
  ...
.Ve

The branches that begin with \*(L"origin\*(R" correspond to the \*(L"git remote\*(R"
that you cloned from (which is named \*(L"origin\*(R"). Each branch on the
remote will be exactly tracked by these branches. You should \s-1NEVER\s0 do
work on these remote tracking branches. You only ever do work in a
local branch. Local branches can be configured to automerge (on pull)
from a designated remote tracking branch. This is the case with the
default branch \f(CW\*(C`blead\*(C' which will be configured to merge from the
remote tracking branch \f(CW\*(C`origin/blead\*(C'.

You can see recent commits:

.Vb 1
  % git log
.Ve

And pull new changes from the repository, and update your local
repository (must be clean first)

.Vb 1
  % git pull
.Ve

Assuming we are on the branch \f(CW\*(C`blead\*(C' immediately after a pull, this
command would be more or less equivalent to:

.Vb 2
  % git fetch
  % git merge origin/blead
.Ve

In fact if you want to update your local repository without touching
your working directory you do:

.Vb 1
  % git fetch
.Ve

And if you want to update your remote-tracking branches for all defined
remotes simultaneously you can do

.Vb 1
  % git remote update
.Ve

Neither of these last two commands will update your working directory,
however both will update the remote-tracking branches in your
repository.

To make a local branch of a remote branch:

.Vb 1
  % git checkout -b maint-5.10 origin/maint-5.10
.Ve

To switch back to blead:

.Vb 1
  % git checkout blead
.Ve

### Finding out your status

Subsection "Finding out your status"
The most common git command you will use will probably be

.Vb 1
  % git status
.Ve

This command will produce as output a description of the current state
of the repository, including modified files and unignored untracked
files, and in addition it will show things like what files have been
staged for the next commit, and usually some useful information about
how to change things. For instance the following:

.Vb 3
 % git status
 On branch blead
 Your branch is ahead of \*(Aqorigin/blead\*(Aq by 1 commit.

 Changes to be committed:
   (use "git reset HEAD <file>..." to unstage)

       modified:   pod/perlgit.pod

 Changes not staged for commit:
   (use "git add <file>..." to update what will be committed)
   (use "git checkout -- <file>..." to discard changes in working
                                                              directory)

       modified:   pod/perlgit.pod

 Untracked files:
   (use "git add <file>..." to include in what will be committed)

       deliberate.untracked
.Ve

This shows that there were changes to this document staged for commit,
and that there were further changes in the working directory not yet
staged. It also shows that there was an untracked file in the working
directory, and as you can see shows how to change all of this. It also
shows that there is one commit on the working branch \f(CW\*(C`blead\*(C' which has
not been pushed to the \f(CW\*(C`origin\*(C' remote yet. **\s-1NOTE\s0**: This output
is also what you see as a template if you do not provide a message to
\f(CW\*(C`git commit\*(C'.

### Patch workflow

Subsection "Patch workflow"
First, please read perlhack for details on hacking the Perl core.
That document covers many details on how to create a good patch.

If you already have a Perl repository, you should ensure that you're on
the *blead* branch, and your repository is up to date:

.Vb 2
  % git checkout blead
  % git pull
.Ve

It's preferable to patch against the latest blead version, since this
is where new development occurs for all changes other than critical bug
fixes. Critical bug fix patches should be made against the relevant
maint branches, or should be submitted with a note indicating all the
branches where the fix should be applied.

Now that we have everything up to date, we need to create a temporary
new branch for these changes and switch into it:

.Vb 1
  % git checkout -b orange
.Ve

which is the short form of

.Vb 2
  % git branch orange
  % git checkout orange
.Ve

Creating a topic branch makes it easier for the maintainers to rebase
or merge back into the master blead for a more linear history. If you
don't work on a topic branch the maintainer has to manually cherry pick
your changes onto blead before they can be applied.

That'll get you scolded on perl5-porters, so don't do that. Be Awesome.

Then make your changes. For example, if Leon Brocard changes his name
to Orange Brocard, we should change his name in the \s-1AUTHORS\s0 file:

.Vb 1
  % perl -pi -e \*(Aqs\{Leon Brocard\}\{Orange Brocard\}\*(Aq AUTHORS
.Ve

You can see what files are changed:

.Vb 4
  % git status
  On branch orange
  Changes to be committed:
    (use "git reset HEAD <file>..." to unstage)

     modified:   AUTHORS
.Ve

And you can see the changes:

.Vb 10
 % git diff
 diff --git a/AUTHORS b/AUTHORS
 index 293dd70..722c93e 100644
 --- a/AUTHORS
 +++ b/AUTHORS
 @@ -541,7 +541,7 @@    Lars Hecking              <lhecking@nmrc.ucc.ie>
  Laszlo Molnar                  <laszlo.molnar@eth.ericsson.se>
  Leif Huhn                      <leif@hale.dkstat.com>
  Len Johnson                    <lenjay@ibm.net>
 -Leon Brocard                   <acme@astray.com>
 +Orange Brocard                 <acme@astray.com>
  Les Peters                     <lpeters@aol.net>
  Lesley Binks                   <lesley.binks@gmail.com>
  Lincoln D. Stein               <lstein@cshl.org>
.Ve

Now commit your change locally:

.Vb 3
 % git commit -a -m \*(AqRename Leon Brocard to Orange Brocard\*(Aq
 Created commit 6196c1d: Rename Leon Brocard to Orange Brocard
  1 files changed, 1 insertions(+), 1 deletions(-)
.Ve

The \f(CW\*(C`-a\*(C' option is used to include all files that git tracks that you
have changed. If at this time, you only want to commit some of the
files you have worked on, you can omit the \f(CW\*(C`-a\*(C' and use the command
\f(CW\*(C`git\ add\ \f(CIFILE\ ...\f(CW\*(C' before doing the commit. \f(CW\*(C`git\ add\ --interactive\*(C' allows you to even just commit portions of files
instead of all the changes in them.

The \f(CW\*(C`-m\*(C' option is used to specify the commit message. If you omit it,
git will open a text editor for you to compose the message
interactively. This is useful when the changes are more complex than
the sample given here, and, depending on the editor, to know that the
first line of the commit message doesn't exceed the 50 character legal
maximum. See \*(L"Commit message\*(R" in perlhack for more information about what
makes a good commit message.

Once you've finished writing your commit message and exited your
editor, git will write your change to disk and tell you something like
this:

.Vb 2
 Created commit daf8e63: explain git status and stuff about remotes
  1 files changed, 83 insertions(+), 3 deletions(-)
.Ve

If you re-run \f(CW\*(C`git status\*(C', you should see something like this:

.Vb 4
 % git status
 On branch orange
 Untracked files:
   (use "git add <file>..." to include in what will be committed)

       deliberate.untracked

 nothing added to commit but untracked files present (use "git add" to
                                                                  track)
.Ve

When in doubt, before you do anything else, check your status and read
it carefully, many questions are answered directly by the git status
output.

You can examine your last commit with:

.Vb 1
  % git show HEAD
.Ve

and if you are not happy with either the description or the patch
itself you can fix it up by editing the files once more and then issue:

.Vb 1
  % git commit -a --amend
.Ve

Now, create a fork on GitHub to push your branch to, and add it as a
remote if you haven't already, as described in the GitHub documentation
at <https://help.github.com/en/articles/working-with-forks>:

.Vb 1
  % git remote add fork git@github.com:MyUser/perl5.git
.Ve

And push the branch to your fork:

.Vb 1
  % git push -u fork orange
.Ve

You should now submit a Pull Request (\s-1PR\s0) on GitHub from the new branch
to blead. For more information, see the GitHub documentation at
<https://help.github.com/en/articles/creating-a-pull-request-from-a-fork>.

You can also send patch files to
perl5-porters@perl.org <mailto:perl5-porters@perl.org> directly if the
patch is not ready to be applied, but intended for discussion.

To create a patch file for all your local changes:

.Vb 2
  % git format-patch -M blead..
  0001-Rename-Leon-Brocard-to-Orange-Brocard.patch
.Ve

Or for a lot of changes, e.g. from a topic branch:

.Vb 1
  % git format-patch --stdout -M blead.. > topic-branch-changes.patch
.Ve

If you want to delete your temporary branch, you may do so with:

.Vb 6
 % git checkout blead
 % git branch -d orange
 error: The branch \*(Aqorange\*(Aq is not an ancestor of your current HEAD.
 If you are sure you want to delete it, run \*(Aqgit branch -D orange\*(Aq.
 % git branch -D orange
 Deleted branch orange.
.Ve

### A note on derived files

Subsection "A note on derived files"
Be aware that many files in the distribution are derivative\*(--avoid
patching them, because git won't see the changes to them, and the build
process will overwrite them. Patch the originals instead. Most
utilities (like perldoc) are in this category, i.e. patch
*utils/perldoc.PL* rather than *utils/perldoc*. Similarly, don't
create patches for files under *\f(CI$src_root\fI/ext* from their copies found
in *\f(CI$install_root\fI/lib*. If you are unsure about the proper location of
a file that may have gotten copied while building the source
distribution, consult the *\s-1MANIFEST\s0*.

### Cleaning a working directory

Subsection "Cleaning a working directory"
The command \f(CW\*(C`git clean\*(C' can with varying arguments be used as a
replacement for \f(CW\*(C`make clean\*(C'.

To reset your working directory to a pristine condition you can do:

.Vb 1
  % git clean -dxf
.Ve

However, be aware this will delete \s-1ALL\s0 untracked content. You can use

.Vb 1
  % git clean -Xf
.Ve

to remove all ignored untracked files, such as build and test
byproduct, but leave any manually created files alone.

If you only want to cancel some uncommitted edits, you can use \f(CW\*(C`git
checkout\*(C' and give it a list of files to be reverted, or \f(CW\*(C`git checkout
-f\*(C' to revert them all.

If you want to cancel one or several commits, you can use \f(CW\*(C`git reset\*(C'.

### Bisecting

Subsection "Bisecting"
\f(CW\*(C`git\*(C' provides a built-in way to determine which commit should be blamed
for introducing a given bug. \f(CW\*(C`git bisect\*(C' performs a binary search of
history to locate the first failing commit. It is fast, powerful and
flexible, but requires some setup and to automate the process an auxiliary
shell script is needed.

The core provides a wrapper program, *Porting/bisect.pl*, which attempts to
simplify as much as possible, making bisecting as simple as running a Perl
one-liner. For example, if you want to know when this became an error:

.Vb 1
    perl -e \*(Aqmy $a := 2\*(Aq
.Ve

you simply run this:

.Vb 1
    .../Porting/bisect.pl -e \*(Aqmy $a := 2;\*(Aq
.Ve

Using *Porting/bisect.pl*, with one command (and no other files) it's easy to
find out

- \(bu
Which commit caused this example code to break?

- \(bu
Which commit caused this example code to start working?

- \(bu
Which commit added the first file to match this regex?

- \(bu
Which commit removed the last file to match this regex?

usually without needing to know which versions of perl to use as start and
end revisions, as *Porting/bisect.pl* automatically searches to find the
earliest stable version for which the test case passes. Run
\f(CW\*(C`Porting/bisect.pl --help\*(C' for the full documentation, including how to
set the \f(CW\*(C`Configure\*(C' and build time options.

If you require more flexibility than *Porting/bisect.pl* has to offer, you'll
need to run \f(CW\*(C`git bisect\*(C' yourself. It's most useful to use \f(CW\*(C`git bisect run\*(C'
to automate the building and testing of perl revisions. For this you'll need
a shell script for \f(CW\*(C`git\*(C' to call to test a particular revision. An example
script is *Porting/bisect-example.sh*, which you should copy **outside** of
the repository, as the bisect process will reset the state to a clean checkout
as it runs. The instructions below assume that you copied it as *~/run* and
then edited it as appropriate.

You first enter in bisect mode with:

.Vb 1
  % git bisect start
.Ve

For example, if the bug is present on \f(CW\*(C`HEAD\*(C' but wasn't in 5.10.0,
\f(CW\*(C`git\*(C' will learn about this when you enter:

.Vb 3
  % git bisect bad
  % git bisect good perl-5.10.0
  Bisecting: 853 revisions left to test after this
.Ve

This results in checking out the median commit between \f(CW\*(C`HEAD\*(C' and
\f(CW\*(C`perl-5.10.0\*(C'. You can then run the bisecting process with:

.Vb 1
  % git bisect run ~/run
.Ve

When the first bad commit is isolated, \f(CW\*(C`git bisect\*(C' will tell you so:

.Vb 4
  ca4cfd28534303b82a216cfe83a1c80cbc3b9dc5 is first bad commit
  commit ca4cfd28534303b82a216cfe83a1c80cbc3b9dc5
  Author: Dave Mitchell <davem@fdisolutions.com>
  Date:   Sat Feb 9 14:56:23 2008 +0000

      [perl #49472] Attributes + Unknown Error
      ...

  bisect run success
.Ve

You can peek into the bisecting process with \f(CW\*(C`git bisect log\*(C' and
\f(CW\*(C`git bisect visualize\*(C'. \f(CW\*(C`git bisect reset\*(C' will get you out of bisect
mode.

Please note that the first \f(CW\*(C`good\*(C' state must be an ancestor of the
first \f(CW\*(C`bad\*(C' state. If you want to search for the commit that *solved*
some bug, you have to negate your test case (i.e. exit with \f(CW1 if \s-1OK\s0
and \f(CW0 if not) and still mark the lower bound as \f(CW\*(C`good\*(C' and the
upper as \f(CW\*(C`bad\*(C'. The \*(L"first bad commit\*(R" has then to be understood as
the \*(L"first commit where the bug is solved\*(R".

\f(CW\*(C`git help bisect\*(C' has much more information on how you can tweak your
binary searches.

Following bisection you may wish to configure, build and test perl at
commits identified by the bisection process.  Sometimes, particularly
with older perls, \f(CW\*(C`make\*(C' may fail during this process.  In this case
you may be able to patch the source code at the older commit point.  To
do so, please follow the suggestions provided in
\*(L"Building perl at older commits\*(R" in perlhack.

### Topic branches and rewriting history

Subsection "Topic branches and rewriting history"
Individual committers should create topic branches under
**yourname**/**some_descriptive_name**:

.Vb 4
  % branch="$yourname/$some_descriptive_name"
  % git checkout -b $branch
  ... do local edits, commits etc ...
  % git push origin -u $branch
.Ve

Should you be stuck with an ancient version of git (prior to 1.7), then
\f(CW\*(C`git push\*(C' will not have the \f(CW\*(C`-u\*(C' switch, and you have to replace the
last step with the following sequence:

.Vb 3
  % git push origin $branch:refs/heads/$branch
  % git config branch.$branch.remote origin
  % git config branch.$branch.merge refs/heads/$branch
.Ve

If you want to make changes to someone else's topic branch, you should
check with its creator before making any change to it.

You
might sometimes find that the original author has edited the branch's
history. There are lots of good reasons for this. Sometimes, an author
might simply be rebasing the branch onto a newer source point.
Sometimes, an author might have found an error in an early commit which
they wanted to fix before merging the branch to blead.

Currently the master repository is configured to forbid
non-fast-forward merges. This means that the branches within can not be
rebased and pushed as a single step.

The only way you will ever be allowed to rebase or modify the history
of a pushed branch is to delete it and push it as a new branch under
the same name. Please think carefully about doing this. It may be
better to sequentially rename your branches so that it is easier for
others working with you to cherry-pick their local changes onto the new
version. (\s-1XXX:\s0 needs explanation).

If you want to rebase a personal topic branch, you will have to delete
your existing topic branch and push as a new version of it. You can do
this via the following formula (see the explanation about \f(CW\*(C`refspec\*(C''s
in the git push documentation for details) after you have rebased your
branch:

.Vb 4
  # first rebase
  % git checkout $user/$topic
  % git fetch
  % git rebase origin/blead

  # then "delete-and-push"
  % git push origin :$user/$topic
  % git push origin $user/$topic
.Ve

**\s-1NOTE:\s0** it is forbidden at the repository level to delete any of the
\*(L"primary\*(R" branches. That is any branch matching
\f(CW\*(C`m!^(blead|maint|perl)!\*(C'. Any attempt to do so will result in git
producing an error like this:

.Vb 7
  % git push origin :blead
  *** It is forbidden to delete blead/maint branches in this repository
  error: hooks/update exited with error code 1
  error: hook declined to update refs/heads/blead
  To ssh://perl5.git.perl.org/perl
   ! [remote rejected] blead (hook declined)
   error: failed to push some refs to \*(Aqssh://perl5.git.perl.org/perl\*(Aq
.Ve

As a matter of policy we do **not** edit the history of the blead and
maint-* branches. If a typo (or worse) sneaks into a commit to blead or
maint-*, we'll fix it in another commit. The only types of updates
allowed on these branches are \*(L"fast-forwards\*(R", where all history is
preserved.

Annotated tags in the canonical perl.git repository will never be
deleted or modified. Think long and hard about whether you want to push
a local tag to perl.git before doing so. (Pushing simple tags is
not allowed.)

### Grafts

Subsection "Grafts"
The perl history contains one mistake which was not caught in the
conversion: a merge was recorded in the history between blead and
maint-5.10 where no merge actually occurred. Due to the nature of git,
this is now impossible to fix in the public repository. You can remove
this mis-merge locally by adding the following line to your
\f(CW\*(C`.git/info/grafts\*(C' file:

.Vb 1
 296f12bbbbaa06de9be9d09d3dcf8f4528898a49 434946e0cb7a32589ed92d18008aaa1d88515930
.Ve

It is particularly important to have this graft line if any bisecting
is done in the area of the \*(L"merge\*(R" in question.

## WRITE ACCESS TO THE GIT REPOSITORY

Header "WRITE ACCESS TO THE GIT REPOSITORY"
Once you have write access, you will need to modify the \s-1URL\s0 for the
origin remote to enable pushing. Edit *.git/config* with the
**git-config**\|(1) command:

.Vb 1
  % git config remote.origin.url git@github.com:Perl/perl5.git
.Ve

You can also set up your user name and e-mail address. Most people do
this once globally in their *~/.gitconfig* by doing something like:

.Vb 2
  % git config --global user.name "\*(Aevar Arnfjo\*:r\*(d- Bjarmason"
  % git config --global user.email avarab@gmail.com
.Ve

However, if you'd like to override that just for perl,
execute something like the following in *perl*:

.Vb 1
  % git config user.email avar@cpan.org
.Ve

It is also possible to keep \f(CW\*(C`origin\*(C' as a git remote, and add a new
remote for ssh access:

.Vb 1
  % git remote add camel git@github.com:Perl/perl5.git
.Ve

This allows you to update your local repository by pulling from
\f(CW\*(C`origin\*(C', which is faster and doesn't require you to authenticate, and
to push your changes back with the \f(CW\*(C`camel\*(C' remote:

.Vb 2
  % git fetch camel
  % git push camel
.Ve

The \f(CW\*(C`fetch\*(C' command just updates the \f(CW\*(C`camel\*(C' refs, as the objects
themselves should have been fetched when pulling from \f(CW\*(C`origin\*(C'.

### Working with Github pull requests

Subsection "Working with Github pull requests"
Pull requests typically originate from outside of the \f(CW\*(C`Perl/perl.git\*(C'
repository, so if you want to test or work with it locally a vanilla
\f(CW\*(C`git fetch\*(C' from the \f(CW\*(C`Perl/perl5.git\*(C' repository won't fetch it.

However Github does provide a mechanism to fetch a pull request to a
local branch.  They are available on Github remotes under \f(CW\*(C`pull/\*(C', so
you can use \f(CW\*(C`git fetch pull/\f(CIPRID\f(CW/head:\f(CIlocalname\f(CW\*(C' to make a
local copy.  eg.  to fetch pull request 9999 to the local branch
\f(CW\*(C`local-branch-name\*(C' run:

.Vb 1
  git fetch origin pull/9999/head:local-branch-name
.Ve

and then:

.Vb 1
  git checkout local-branch-name
.Ve

Note: this branch is not rebased on \f(CW\*(C`blead\*(C', so instead of the
checkout above, you might want:

.Vb 1
  git rebase origin/blead local-branch-name
.Ve

which rebases \f(CW\*(C`local-branch-name\*(C' on \f(CW\*(C`blead\*(C', and checks it out.

Alternatively you can configure the remote to fetch all pull requests
as remote-tracking branches.  To do this edit the remote in
*.git/config*, for example if your github remote is \f(CW\*(C`origin\*(C' you'd
have:

.Vb 3
  [remote "origin"]
          url = git@github.com:/Perl/perl5.git
          fetch = +refs/heads/*:refs/remotes/origin/*
.Ve

Add a line to map the remote pull request branches to remote-tracking
branches:

.Vb 4
  [remote "origin"]
          url = git@github.com:/Perl/perl5.git
          fetch = +refs/heads/*:refs/remotes/origin/*
          fetch = +refs/pull/*/head:refs/remotes/origin/pull/*
.Ve

and then do a fetch as normal:

.Vb 1
  git fetch origin
.Ve

This will create a remote-tracking branch for every pull request, including
closed requests.

To remove those remote-tracking branches, remove the line added above
and prune:

.Vb 1
  git fetch -p origin # or git remote prune origin
.Ve

### Accepting a patch

Subsection "Accepting a patch"
If you have received a patch file generated using the above section,
you should try out the patch.

First we need to create a temporary new branch for these changes and
switch into it:

.Vb 1
 % git checkout -b experimental
.Ve

Patches that were formatted by \f(CW\*(C`git format-patch\*(C' are applied with
\f(CW\*(C`git am\*(C':

.Vb 2
 % git am 0001-Rename-Leon-Brocard-to-Orange-Brocard.patch
 Applying Rename Leon Brocard to Orange Brocard
.Ve

Note that some \s-1UNIX\s0 mail systems can mess with text attachments containing
'From '. This will fix them up:

.Vb 2
 % perl -pi -e\*(Aqs/^>From /From /\*(Aq \\
                        0001-Rename-Leon-Brocard-to-Orange-Brocard.patch
.Ve

If just a raw diff is provided, it is also possible use this two-step
process:

.Vb 3
 % git apply bugfix.diff
 % git commit -a -m "Some fixing" \\
                            --author="That Guy <that.guy@internets.com>"
.Ve

Now we can inspect the change:

.Vb 4
 % git show HEAD
 commit b1b3dab48344cff6de4087efca3dbd63548ab5e2
 Author: Leon Brocard <acme@astray.com>
 Date:   Fri Dec 19 17:02:59 2008 +0000

   Rename Leon Brocard to Orange Brocard

 diff --git a/AUTHORS b/AUTHORS
 index 293dd70..722c93e 100644
 --- a/AUTHORS
 +++ b/AUTHORS
 @@ -541,7 +541,7 @@ Lars Hecking                 <lhecking@nmrc.ucc.ie>
  Laszlo Molnar                  <laszlo.molnar@eth.ericsson.se>
  Leif Huhn                      <leif@hale.dkstat.com>
  Len Johnson                    <lenjay@ibm.net>
 -Leon Brocard                   <acme@astray.com>
 +Orange Brocard                 <acme@astray.com>
  Les Peters                     <lpeters@aol.net>
  Lesley Binks                   <lesley.binks@gmail.com>
  Lincoln D. Stein               <lstein@cshl.org>
.Ve

If you are a committer to Perl and you think the patch is good, you can
then merge it into blead then push it out to the main repository:

.Vb 3
  % git checkout blead
  % git merge experimental
  % git push origin blead
.Ve

If you want to delete your temporary branch, you may do so with:

.Vb 7
 % git checkout blead
 % git branch -d experimental
 error: The branch \*(Aqexperimental\*(Aq is not an ancestor of your current
 HEAD.  If you are sure you want to delete it, run \*(Aqgit branch -D
 experimental\*(Aq.
 % git branch -D experimental
 Deleted branch experimental.
.Ve

### Committing to blead

Subsection "Committing to blead"
The 'blead' branch will become the next production release of Perl.

Before pushing *any* local change to blead, it's incredibly important
that you do a few things, lest other committers come after you with
pitchforks and torches:

- \(bu
Make sure you have a good commit message. See \*(L"Commit
message\*(R" in perlhack for details.

- \(bu
Run the test suite. You might not think that one typo fix would break a
test file. You'd be wrong. Here's an example of where not running the
suite caused problems. A patch was submitted that added a couple of
tests to an existing *.t*. It couldn't possibly affect anything else, so
no need to test beyond the single affected *.t*, right?  But, the
submitter's email address had changed since the last of their
submissions, and this caused other tests to fail. Running the test
target given in the next item would have caught this problem.

- \(bu
If you don't run the full test suite, at least \f(CW\*(C`make test_porting\*(C'.
This will run basic sanity checks. To see which sanity checks, have a
look in *t/porting*.

- \(bu
If you make any changes that affect miniperl or core routines that have
different code paths for miniperl, be sure to run \f(CW\*(C`make minitest\*(C'.
This will catch problems that even the full test suite will not catch
because it runs a subset of tests under miniperl rather than perl.

### On merging and rebasing

Subsection "On merging and rebasing"
Simple, one-off commits pushed to the 'blead' branch should be simple
commits that apply cleanly.  In other words, you should make sure your
work is committed against the current position of blead, so that you can
push back to the master repository without merging.

Sometimes, blead will move while you're building or testing your
changes.  When this happens, your push will be rejected with a message
like this:

.Vb 7
 To ssh://perl5.git.perl.org/perl.git
  ! [rejected]        blead -> blead (non-fast-forward)
 error: failed to push some refs to \*(Aqssh://perl5.git.perl.org/perl.git\*(Aq
 To prevent you from losing history, non-fast-forward updates were
 rejected Merge the remote changes (e.g. \*(Aqgit pull\*(Aq) before pushing
 again.  See the \*(AqNote about fast-forwards\*(Aq section of \*(Aqgit push --help\*(Aq
 for details.
.Ve

When this happens, you can just *rebase* your work against the new
position of blead, like this (assuming your remote for the master
repository is \*(L"p5p\*(R"):

.Vb 2
  % git fetch p5p
  % git rebase p5p/blead
.Ve

You will see your commits being re-applied, and you will then be able to
push safely.  More information about rebasing can be found in the
documentation for the **git-rebase**\|(1) command.

For larger sets of commits that only make sense together, or that would
benefit from a summary of the set's purpose, you should use a merge
commit.  You should perform your work on a topic branch, which you should regularly rebase
against blead to ensure that your code is not broken by blead moving.
When you have finished your work, please perform a final rebase and
test.  Linear history is something that gets lost with every
commit on blead, but a final rebase makes the history linear
again, making it easier for future maintainers to see what has
happened.  Rebase as follows (assuming your work was on the
branch \f(CW\*(C`committer/somework\*(C'):

.Vb 2
  % git checkout committer/somework
  % git rebase blead
.Ve

Then you can merge it into master like this:

.Vb 3
  % git checkout blead
  % git merge --no-ff --no-commit committer/somework
  % git commit -a
.Ve

The switches above deserve explanation.  \f(CW\*(C`--no-ff\*(C' indicates that even
if all your work can be applied linearly against blead, a merge commit
should still be prepared.  This ensures that all your work will be shown
as a side branch, with all its commits merged into the mainstream blead
by the merge commit.

\f(CW\*(C`--no-commit\*(C' means that the merge commit will be *prepared* but not
*committed*.  The commit is then actually performed when you run the
next command, which will bring up your editor to describe the commit.
Without \f(CW\*(C`--no-commit\*(C', the commit would be made with nearly no useful
message, which would greatly diminish the value of the merge commit as a
placeholder for the work's description.

When describing the merge commit, explain the purpose of the branch, and
keep in mind that this description will probably be used by the
eventual release engineer when reviewing the next perldelta document.

### Committing to maintenance versions

Subsection "Committing to maintenance versions"
Maintenance versions should only be altered to add critical bug fixes,
see perlpolicy.

To commit to a maintenance version of perl, you need to create a local
tracking branch:

.Vb 1
  % git checkout --track -b maint-5.005 origin/maint-5.005
.Ve

This creates a local branch named \f(CW\*(C`maint-5.005\*(C', which tracks the
remote branch \f(CW\*(C`origin/maint-5.005\*(C'. Then you can pull, commit, merge
and push as before.

You can also cherry-pick commits from blead and another branch, by
using the \f(CW\*(C`git cherry-pick\*(C' command. It is recommended to use the
**-x** option to \f(CW\*(C`git cherry-pick\*(C' in order to record the \s-1SHA1\s0 of the
original commit in the new commit message.

Before pushing any change to a maint version, make sure you've
satisfied the steps in \*(L"Committing to blead\*(R" above.

### Using a smoke-me branch to test changes

Subsection "Using a smoke-me branch to test changes"
Sometimes a change affects code paths which you cannot test on the OSes
which are directly available to you and it would be wise to have users
on other OSes test the change before you commit it to blead.

Fortunately, there is a way to get your change smoke-tested on various
OSes: push it to a \*(L"smoke-me\*(R" branch and wait for certain automated
smoke-testers to report the results from their OSes.
A \*(L"smoke-me\*(R" branch is identified by the branch name: specifically, as
seen on github.com it must be a local branch whose first name
component is precisely \f(CW\*(C`smoke-me\*(C'.

The procedure for doing this is roughly as follows (using the example of
tonyc's smoke-me branch called win32stat):

First, make a local branch and switch to it:

.Vb 1
  % git checkout -b win32stat
.Ve

Make some changes, build perl and test your changes, then commit them to
your local branch. Then push your local branch to a remote smoke-me
branch:

.Vb 1
  % git push origin win32stat:smoke-me/tonyc/win32stat
.Ve

Now you can switch back to blead locally:

.Vb 1
  % git checkout blead
.Ve

and continue working on other things while you wait a day or two,
keeping an eye on the results reported for your smoke-me branch at
<http://perl.develop-help.com/?b=smoke-me/tonyc/win32state>.

If all is well then update your blead branch:

.Vb 1
  % git pull
.Ve

then checkout your smoke-me branch once more and rebase it on blead:

.Vb 1
  % git rebase blead win32stat
.Ve

Now switch back to blead and merge your smoke-me branch into it:

.Vb 2
  % git checkout blead
  % git merge win32stat
.Ve

As described earlier, if there are many changes on your smoke-me branch
then you should prepare a merge commit in which to give an overview of
those changes by using the following command instead of the last
command above:

.Vb 1
  % git merge win32stat --no-ff --no-commit
.Ve

You should now build perl and test your (merged) changes one last time
(ideally run the whole test suite, but failing that at least run the
*t/porting/*.t* tests) before pushing your changes as usual:

.Vb 1
  % git push origin blead
.Ve

Finally, you should then delete the remote smoke-me branch:

.Vb 1
  % git push origin :smoke-me/tonyc/win32stat
.Ve

(which is likely to produce a warning like this, which can be ignored:

.Vb 4
 remote: fatal: ambiguous argument
                                  \*(Aqrefs/heads/smoke-me/tonyc/win32stat\*(Aq:
 unknown revision or path not in the working tree.
 remote: Use \*(Aq--\*(Aq to separate paths from revisions
.Ve

) and then delete your local branch:

.Vb 1
  % git branch -d win32stat
.Ve
