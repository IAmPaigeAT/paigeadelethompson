+++
manpage_section = "1"
operating_system_version = "15.3"
date = "2022-02-19"
detected_package_version = "5.34.1"
author = "None Specified"
manpage_format = "troff"
description = "This document describes various features of s-1IBMs0s s-1UNIXs0 operating system s-1AIXs0 that will affect how Perl version 5 (hereafter just Perl) is compiled and/or runs. For information on compilers on older versions of s-1AIX,s0 see *(LCompil..."
manpage_name = "perlaix"
operating_system = "macos"
title = "perlaix(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLAIX 1"
PERLAIX 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlaix - Perl version 5 on IBM AIX (UNIX) systems

## DESCRIPTION

Header "DESCRIPTION"
This document describes various features of \s-1IBM\s0's \s-1UNIX\s0 operating
system \s-1AIX\s0 that will affect how Perl version 5 (hereafter just Perl)
is compiled and/or runs.

### Compiling Perl 5 on \s-1AIX\s0

Subsection "Compiling Perl 5 on AIX"
For information on compilers on older versions of \s-1AIX,\s0 see \*(L"Compiling
Perl 5 on older \s-1AIX\s0 versions up to 4.3.3\*(R".

When compiling Perl, you must use an \s-1ANSI C\s0 compiler. \s-1AIX\s0 does not ship
an \s-1ANSI\s0 compliant C compiler with \s-1AIX\s0 by default, but binary builds of
gcc for \s-1AIX\s0 are widely available. A version of gcc is also included in
the \s-1AIX\s0 Toolbox which is shipped with \s-1AIX.\s0

### Supported Compilers

Subsection "Supported Compilers"
Currently all versions of \s-1IBM\s0's \*(L"xlc\*(R", \*(L"xlc_r\*(R", \*(L"cc\*(R", \*(L"cc_r\*(R" or
\*(L"vac\*(R" \s-1ANSI/C\s0 compiler will work for building Perl if that compiler
works on your system.

If you plan to link Perl to any module that requires thread-support,
like DBD::Oracle, it is better to use the _r version of the compiler.
This will not build a threaded Perl, but a thread-enabled Perl. See
also \*(L"Threaded Perl\*(R" later on.

As of writing (2010-09) only the *\s-1IBM XL C\s0 for \s-1AIX\s0* or \fI\s-1IBM XL C/\*(C+\s0
for \s-1AIX\s0 compiler is supported by \s-1IBM\s0 on \s-1AIX 5L/6.1/7.1.\s0

The following compiler versions are currently supported by \s-1IBM:\s0

.Vb 1
    IBM XL C and IBM XL C/C++ V8, V9, V10, V11
.Ve

The \s-1XL C\s0 for \s-1AIX\s0 is integrated in the \s-1XL C/\*(C+\s0 for \s-1AIX\s0 compiler and
therefore also supported.

If you choose \s-1XL C/\*(C+ V9\s0 you need \s-1APAR IZ35785\s0 installed
otherwise the integrated SDBM_File do not compile correctly due
to an optimization bug. You can circumvent this problem by
adding -qipa to the optimization flags (-Doptimize='-O -qipa').
The \s-1PTF\s0 for \s-1APAR IZ35785\s0 which solves this problem is available
from \s-1IBM\s0 (April 2009 \s-1PTF\s0 for \s-1XL C/\*(C+\s0 Enterprise Edition for \s-1AIX, V9.0\s0).

If you choose \s-1XL C/\*(C+ V11\s0 you need the April 2010 \s-1PTF\s0 (or newer)
installed otherwise you will not get a working Perl version.

Perl can be compiled with either \s-1IBM\s0's \s-1ANSI C\s0 compiler or with gcc.
The former is recommended, as not only it can compile Perl with no
difficulty, but also can take advantage of features listed later
that require the use of \s-1IBM\s0 compiler-specific command-line flags.

If you decide to use gcc, make sure your installation is recent and
complete, and be sure to read the Perl \s-1INSTALL\s0 file for more gcc-specific
details. Please report any hoops you had to jump through to the
development team.

### Incompatibility with \s-1AIX\s0 Toolbox lib gdbm

Subsection "Incompatibility with AIX Toolbox lib gdbm"
If the \s-1AIX\s0 Toolbox version of lib gdbm < 1.8.3-5 is installed on your
system then Perl will not work. This library contains the header files
/opt/freeware/include/gdbm/dbm.h|ndbm.h which conflict with the \s-1AIX\s0
system versions. The lib gdbm will be automatically removed from the
wanted libraries if the presence of one of these two header files is
detected. If you want to build Perl with \s-1GDBM\s0 support then please install
at least gdbm-devel-1.8.3-5 (or higher).

### Perl 5 was successfully compiled and tested on:

Subsection "Perl 5 was successfully compiled and tested on:"
.Vb 10
 Perl   | AIX Level           | Compiler Level          | w th | w/o th
 -------+---------------------+-------------------------+------+-------
 5.12.2 |5.1 TL9 32 bit       | XL C/C++ V7             | OK   | OK
 5.12.2 |5.1 TL9 64 bit       | XL C/C++ V7             | OK   | OK
 5.12.2 |5.2 TL10 SP8 32 bit  | XL C/C++ V8             | OK   | OK
 5.12.2 |5.2 TL10 SP8 32 bit  | gcc 3.2.2               | OK   | OK
 5.12.2 |5.2 TL10 SP8 64 bit  | XL C/C++ V8             | OK   | OK
 5.12.2 |5.3 TL8 SP8 32 bit   | XL C/C++ V9 + IZ35785   | OK   | OK
 5.12.2 |5.3 TL8 SP8 32 bit   | gcc 4.2.4               | OK   | OK
 5.12.2 |5.3 TL8 SP8 64 bit   | XL C/C++ V9 + IZ35785   | OK   | OK
 5.12.2 |5.3 TL10 SP3 32 bit  | XL C/C++ V11 + Apr 2010 | OK   | OK
 5.12.2 |5.3 TL10 SP3 64 bit  | XL C/C++ V11 + Apr 2010 | OK   | OK
 5.12.2 |6.1 TL1 SP7 32 bit   | XL C/C++ V10            | OK   | OK
 5.12.2 |6.1 TL1 SP7 64 bit   | XL C/C++ V10            | OK   | OK
 5.13   |7.1 TL0 SP1 32 bit   | XL C/C++ V11 + Jul 2010 | OK   | OK
 5.13   |7.1 TL0 SP1 64 bit   | XL C/C++ V11 + Jul 2010 | OK   | OK

 w th   = with thread support
 w/o th = without thread support
 OK     = tested
.Ve

Successfully tested means that all \*(L"make test\*(R" runs finish with a
result of 100% \s-1OK.\s0 All tests were conducted with -Duseshrplib set.

All tests were conducted on the oldest supported \s-1AIX\s0 technology level
with the latest support package applied. If the tested \s-1AIX\s0 version is
out of support (\s-1AIX 4.3.3, 5.1, 5.2\s0) then the last available support
level was used.

### Building Dynamic Extensions on \s-1AIX\s0

Subsection "Building Dynamic Extensions on AIX"
Starting from Perl 5.7.2 (and consequently 5.8.x / 5.10.x / 5.12.x)
and \s-1AIX 4.3\s0 or newer Perl uses the \s-1AIX\s0 native dynamic loading interface
in the so called runtime linking mode instead of the emulated interface
that was used in Perl releases 5.6.1 and earlier or, for \s-1AIX\s0 releases
4.2 and earlier. This change does break backward compatibility with
compiled modules from earlier Perl releases. The change was made to make
Perl more compliant with other applications like Apache/mod_perl which are
using the \s-1AIX\s0 native interface. This change also enables the use of
\*(C+ code with static constructors and destructors in Perl extensions,
which was not possible using the emulated interface.

It is highly recommended to use the new interface.

### Using Large Files with Perl

Subsection "Using Large Files with Perl"
Should yield no problems.

### Threaded Perl

Subsection "Threaded Perl"
Should yield no problems with \s-1AIX 5.1 / 5.2 / 5.3 / 6.1 / 7.1.\s0

\s-1IBM\s0 uses the \s-1AIX\s0 system Perl (V5.6.0 on \s-1AIX 5.1\s0 and V5.8.2 on
\s-1AIX 5.2 / 5.3\s0 and 6.1; V5.8.8 on \s-1AIX 5.3 TL11\s0 and \s-1AIX 6.1 TL4\s0; V5.10.1
on \s-1AIX 7.1\s0) for some \s-1AIX\s0 system scripts. If you switch the links in
/usr/bin from the \s-1AIX\s0 system Perl (/usr/opt/perl5) to the newly build
Perl then you get the same features as with the \s-1IBM AIX\s0 system Perl if
the threaded options are used.

The threaded Perl build works also on \s-1AIX 5.1\s0 but the \s-1IBM\s0 Perl
build (Perl v5.6.0) is not threaded on \s-1AIX 5.1.\s0

Perl 5.12 an newer is not compatible with the \s-1IBM\s0 fileset perl.libext.

### 64-bit Perl

Subsection "64-bit Perl"
If your \s-1AIX\s0 system is installed with 64-bit support, you can expect 64-bit
configurations to work. If you want to use 64-bit Perl on \s-1AIX 6.1\s0
you need an \s-1APAR\s0 for a libc.a bug which affects (n)dbm_XXX functions.
The \s-1APAR\s0 number for this problem is \s-1IZ39077.\s0

If you need more memory (larger data segment) for your Perl programs you
can set:

.Vb 3
    /etc/security/limits
    default:                    (or your user)
        data = -1               (default is 262144 * 512 byte)
.Ve

With the default setting the size is limited to 128MB.
The -1 removes this limit. If the \*(L"make test\*(R" fails please change
your /etc/security/limits as stated above.

### Long doubles

Subsection "Long doubles"
\s-1IBM\s0 calls its implementation of long doubles 128-bit, but it is not
the \s-1IEEE\s0 128-bit (\*(L"quadruple precision\*(R") which would give 116 bit of
mantissa (nor it is implemented in hardware), instead it's a special
software implementation called \*(L"double-double\*(R", which gives 106 bits
of mantissa.

There seem to be various problems in this long double implementation.
If Configure detects this brokenness, it will disable the long double support.
This can be overridden with explicit \f(CW\*(C`-Duselongdouble\*(C' (or \f(CW\*(C`-Dusemorebits\*(C',
which enables both long doubles and 64 bit integers).  If you decide to
enable long doubles, for most of the broken things Perl has implemented
workarounds, but the handling of the special values infinity and NaN
remains badly broken: for example infinity plus zero results in NaN.

### Recommended Options \s-1AIX 5.1/5.2/5.3/6.1\s0 and 7.1 (threaded/32-bit)

Subsection "Recommended Options AIX 5.1/5.2/5.3/6.1 and 7.1 (threaded/32-bit)"
With the following options you get a threaded Perl version which
passes all make tests in threaded 32-bit mode, which is the default
configuration for the Perl builds that \s-1AIX\s0 ships with.

.Vb 7
    rm config.sh
    ./Configure \\
    -d \\
    -Dcc=cc_r \\
    -Duseshrplib \\
    -Dusethreads \\
    -Dprefix=/usr/opt/perl5_32
.Ve

The -Dprefix option will install Perl in a directory parallel to the
\s-1IBM AIX\s0 system Perl installation.

### Recommended Options \s-1AIX 5.1/5.2/5.3/6.1\s0 and 7.1 (32-bit)

Subsection "Recommended Options AIX 5.1/5.2/5.3/6.1 and 7.1 (32-bit)"
With the following options you get a Perl version which passes
all make tests in 32-bit mode.

.Vb 6
    rm config.sh
    ./Configure \\
    -d \\
    -Dcc=cc_r \\
    -Duseshrplib \\
    -Dprefix=/usr/opt/perl5_32
.Ve

The -Dprefix option will install Perl in a directory parallel to the
\s-1IBM AIX\s0 system Perl installation.

### Recommended Options \s-1AIX 5.1/5.2/5.3/6.1\s0 and 7.1 (threaded/64-bit)

Subsection "Recommended Options AIX 5.1/5.2/5.3/6.1 and 7.1 (threaded/64-bit)"
With the following options you get a threaded Perl version which
passes all make tests in 64-bit mode.

.Vb 1
 export OBJECT_MODE=64 / setenv OBJECT_MODE 64 (depending on your shell)

 rm config.sh
 ./Configure \\
 -d \\
 -Dcc=cc_r \\
 -Duseshrplib \\
 -Dusethreads \\
 -Duse64bitall \\
 -Dprefix=/usr/opt/perl5_64
.Ve

### Recommended Options \s-1AIX 5.1/5.2/5.3/6.1\s0 and 7.1 (64-bit)

Subsection "Recommended Options AIX 5.1/5.2/5.3/6.1 and 7.1 (64-bit)"
With the following options you get a Perl version which passes all
make tests in 64-bit mode.

.Vb 1
 export OBJECT_MODE=64 / setenv OBJECT_MODE 64 (depending on your shell)

 rm config.sh
 ./Configure \\
 -d \\
 -Dcc=cc_r \\
 -Duseshrplib \\
 -Duse64bitall \\
 -Dprefix=/usr/opt/perl5_64
.Ve

The -Dprefix option will install Perl in a directory parallel to the
\s-1IBM AIX\s0 system Perl installation.

If you choose gcc to compile 64-bit Perl then you need to add the
following option:

.Vb 1
    -Dcc=\*(Aqgcc -maix64\*(Aq
.Ve

### Compiling Perl 5 on \s-1AIX 7.1.0\s0

Subsection "Compiling Perl 5 on AIX 7.1.0"
A regression in \s-1AIX 7\s0 causes a failure in make test in Time::Piece during
daylight savings time.  \s-1APAR IV16514\s0 provides the fix for this.  A quick
test to see if it's required, assuming it is currently daylight savings
in Eastern Time, would be to run \f(CW\*(C` TZ=EST5 date +%Z \*(C'.  This will come
back with \f(CW\*(C`EST\*(C' normally, but nothing if you have the problem.

### Compiling Perl 5 on older \s-1AIX\s0 versions up to 4.3.3

Subsection "Compiling Perl 5 on older AIX versions up to 4.3.3"
Due to the fact that \s-1AIX 4.3.3\s0 reached end-of-service in December 31,
2003 this information is provided as is. The Perl versions prior to
Perl 5.8.9 could be compiled on \s-1AIX\s0 up to 4.3.3 with the following
settings (your mileage may vary):

When compiling Perl, you must use an \s-1ANSI C\s0 compiler. \s-1AIX\s0 does not ship
an \s-1ANSI\s0 compliant C-compiler with \s-1AIX\s0 by default, but binary builds of
gcc for \s-1AIX\s0 are widely available.

At the moment of writing, \s-1AIX\s0 supports two different native C compilers,
for which you have to pay: **xlC** and **vac**. If you decide to use either
of these two (which is quite a lot easier than using gcc), be sure to
upgrade to the latest available patch level. Currently:

.Vb 2
    xlC.C     3.1.4.10 or 3.6.6.0 or 4.0.2.2 or 5.0.2.9 or 6.0.0.3
    vac.C     4.4.0.3  or 5.0.2.6 or 6.0.0.1
.Ve

note that xlC has the \s-1OS\s0 version in the name as of version 4.0.2.0, so
you will find xlC.C for \s-1AIX-5.0\s0 as package

.Vb 1
    xlC.aix50.rte   5.0.2.0 or 6.0.0.3
.Ve

subversions are not the same \*(L"latest\*(R" on all \s-1OS\s0 versions. For example,
the latest xlC-5 on aix41 is 5.0.2.9, while on aix43, it is 5.0.2.7.

Perl can be compiled with either \s-1IBM\s0's \s-1ANSI C\s0 compiler or with gcc.
The former is recommended, as not only can it compile Perl with no
difficulty, but also can take advantage of features listed later that
require the use of \s-1IBM\s0 compiler-specific command-line flags.

The \s-1IBM\s0's compiler patch levels 5.0.0.0 and 5.0.1.0 have compiler
optimization bugs that affect compiling perl.c and regcomp.c,
respectively.  If Perl's configuration detects those compiler patch
levels, optimization is turned off for the said source code files.
Upgrading to at least 5.0.2.0 is recommended.

If you decide to use gcc, make sure your installation is recent and
complete, and be sure to read the Perl \s-1INSTALL\s0 file for more gcc-specific
details. Please report any hoops you had to jump through to the development
team.

### \s-1OS\s0 level

Subsection "OS level"
Before installing the patches to the \s-1IBM\s0 C-compiler you need to know the
level of patching for the Operating System. \s-1IBM\s0's command 'oslevel' will
show the base, but is not always complete (in this example oslevel shows
4.3.NULL, whereas the system might run most of 4.3.THREE):

.Vb 6
    # oslevel
    4.3.0.0
    # lslpp -l | grep \*(Aqbos.rte \*(Aq
    bos.rte           4.3.3.75  COMMITTED  Base Operating System Runtime
    bos.rte            4.3.2.0  COMMITTED  Base Operating System Runtime
    #
.Ve

The same might happen to \s-1AIX 5.1\s0 or other \s-1OS\s0 levels. As a side note, Perl
cannot be built without bos.adt.syscalls and bos.adt.libm installed

.Vb 4
    # lslpp -l | egrep "syscalls|libm"
    bos.adt.libm      5.1.0.25  COMMITTED  Base Application Development
    bos.adt.syscalls  5.1.0.36  COMMITTED  System Calls Application
    #
.Ve

### Building Dynamic Extensions on \s-1AIX\s0 < 5L

Subsection "Building Dynamic Extensions on AIX < 5L"
\s-1AIX\s0 supports dynamically loadable objects as well as shared libraries.
Shared libraries by convention end with the suffix .a, which is a bit
misleading, as an archive can contain static as well as dynamic members.
For Perl dynamically loaded objects we use the .so suffix also used on
many other platforms.

Note that starting from Perl 5.7.2 (and consequently 5.8.0) and \s-1AIX 4.3\s0
or newer Perl uses the \s-1AIX\s0 native dynamic loading interface in the so
called runtime linking mode instead of the emulated interface that was
used in Perl releases 5.6.1 and earlier or, for \s-1AIX\s0 releases 4.2 and
earlier.  This change does break backward compatibility with compiled
modules from earlier Perl releases.  The change was made to make Perl
more compliant with other applications like Apache/mod_perl which are
using the \s-1AIX\s0 native interface. This change also enables the use of \*(C+
code with static constructors and destructors in Perl extensions, which
was not possible using the emulated interface.

### The \s-1IBM ANSI C\s0 Compiler

Subsection "The IBM ANSI C Compiler"
All defaults for Configure can be used.

If you've chosen to use vac 4, be sure to run 4.4.0.3. Older versions
will turn up nasty later on. For vac 5 be sure to run at least 5.0.1.0,
but vac 5.0.2.6 or up is highly recommended. Note that since \s-1IBM\s0 has
removed vac 5.0.2.1 through 5.0.2.5 from the software depot, these
versions should be considered obsolete.

Here's a brief lead of how to upgrade the compiler to the latest
level.  Of course this is subject to changes.  You can only upgrade
versions from ftp-available updates if the first three digit groups
are the same (in where you can skip intermediate unlike the patches
in the developer snapshots of Perl), or to one version up where the
\*(L"base\*(R" is available.  In other words, the \s-1AIX\s0 compiler patches are
cumulative.

.Vb 3
 vac.C.4.4.0.1 => vac.C.4.4.0.3  is OK     (vac.C.4.4.0.2 not needed)
 xlC.C.3.1.3.3 => xlC.C.3.1.4.10 is NOT OK (xlC.C.3.1.4.0 is not
                                                              available)

 # ftp ftp.software.ibm.com
 Connected to service.boulder.ibm.com.
 : welcome message ...
 Name (ftp.software.ibm.com:merijn): anonymous
 331 Guest login ok, send your complete e-mail address as password.
 Password:
 ... accepted login stuff
 ftp> cd /aix/fixes/v4/
 ftp> dir other other.ll
 output to local-file: other.ll? y
 200 PORT command successful.
 150 Opening ASCII mode data connection for /bin/ls.
 226 Transfer complete.
 ftp> dir xlc xlc.ll
 output to local-file: xlc.ll? y
 200 PORT command successful.
 150 Opening ASCII mode data connection for /bin/ls.
 226 Transfer complete.
 ftp> bye
 ... goodbye messages
 # ls -l *.ll
 -rw-rw-rw-   1 merijn   system    1169432 Nov  2 17:29 other.ll
 -rw-rw-rw-   1 merijn   system      29170 Nov  2 17:29 xlc.ll
.Ve

On \s-1AIX 4.2\s0 using xlC, we continue:

.Vb 10
 # lslpp -l | fgrep \*(AqxlC.C \*(Aq
   xlC.C                     3.1.4.9  COMMITTED  C for AIX Compiler
   xlC.C                     3.1.4.0  COMMITTED  C for AIX Compiler
 # grep \*(AqxlC.C.3.1.4.*.bff\*(Aq xlc.ll
 -rw-r--r--   1 45776101 1       6286336 Jul 22 1996  xlC.C.3.1.4.1.bff
 -rw-rw-r--   1 45776101 1       6173696 Aug 24 1998  xlC.C.3.1.4.10.bff
 -rw-r--r--   1 45776101 1       6319104 Aug 14 1996  xlC.C.3.1.4.2.bff
 -rw-r--r--   1 45776101 1       6316032 Oct 21 1996  xlC.C.3.1.4.3.bff
 -rw-r--r--   1 45776101 1       6315008 Dec 20 1996  xlC.C.3.1.4.4.bff
 -rw-rw-r--   1 45776101 1       6178816 Mar 28 1997  xlC.C.3.1.4.5.bff
 -rw-rw-r--   1 45776101 1       6188032 May 22 1997  xlC.C.3.1.4.6.bff
 -rw-rw-r--   1 45776101 1       6191104 Sep  5 1997  xlC.C.3.1.4.7.bff
 -rw-rw-r--   1 45776101 1       6185984 Jan 13 1998  xlC.C.3.1.4.8.bff
 -rw-rw-r--   1 45776101 1       6169600 May 27 1998  xlC.C.3.1.4.9.bff
 # wget ftp://ftp.software.ibm.com/aix/fixes/v4/xlc/xlC.C.3.1.4.10.bff
 #
.Ve

On \s-1AIX 4.3\s0 using vac, we continue:

.Vb 10
 # lslpp -l | grep \*(Aqvac.C \*(Aq
  vac.C                      5.0.2.2  COMMITTED  C for AIX Compiler
  vac.C                      5.0.2.0  COMMITTED  C for AIX Compiler
 # grep \*(Aqvac.C.5.0.2.*.bff\*(Aq other.ll
 -rw-rw-r--   1 45776101 1       13592576 Apr 16 2001  vac.C.5.0.2.0.bff
 -rw-rw-r--   1 45776101 1       14133248 Apr  9 2002  vac.C.5.0.2.3.bff
 -rw-rw-r--   1 45776101 1       14173184 May 20 2002  vac.C.5.0.2.4.bff
 -rw-rw-r--   1 45776101 1       14192640 Nov 22 2002  vac.C.5.0.2.6.bff
 # wget ftp://ftp.software.ibm.com/aix/fixes/v4/other/vac.C.5.0.2.6.bff
 #
.Ve

Likewise on all other \s-1OS\s0 levels. Then execute the following command, and
fill in its choices

.Vb 5
 # smit install_update
  -> Install and Update from LATEST Available Software
  * INPUT device / directory for software [ vac.C.5.0.2.6.bff    ]
  [ OK ]
  [ OK ]
.Ve

Follow the messages ... and you're done.

If you like a more web-like approach, a good start point can be
<http://www14.software.ibm.com/webapp/download/downloadaz.jsp> and click
\*(L"C for \s-1AIX\*(R",\s0 and follow the instructions.

### The usenm option

Subsection "The usenm option"
If linking miniperl

.Vb 1
 cc -o miniperl ... miniperlmain.o opmini.o perl.o ... -lm -lc ...
.Ve

causes error like this

.Vb 9
 ld: 0711-317 ERROR: Undefined symbol: .aintl
 ld: 0711-317 ERROR: Undefined symbol: .copysignl
 ld: 0711-317 ERROR: Undefined symbol: .syscall
 ld: 0711-317 ERROR: Undefined symbol: .eaccess
 ld: 0711-317 ERROR: Undefined symbol: .setresuid
 ld: 0711-317 ERROR: Undefined symbol: .setresgid
 ld: 0711-317 ERROR: Undefined symbol: .setproctitle
 ld: 0711-345 Use the -bloadmap or -bnoquiet option to obtain more
                                                            information.
.Ve

you could retry with

.Vb 3
 make realclean
 rm config.sh
 ./Configure -Dusenm ...
.Ve

which makes Configure to use the \f(CW\*(C`nm\*(C' tool when scanning for library
symbols, which usually is not done in \s-1AIX.\s0

Related to this, you probably should not use the \f(CW\*(C`-r\*(C' option of
Configure in \s-1AIX,\s0 because that affects of how the \f(CW\*(C`nm\*(C' tool is used.

### Using \s-1GNU\s0s gcc for building Perl

Subsection "Using GNU's gcc for building Perl"
Using gcc-3.x (tested with 3.0.4, 3.1, and 3.2) now works out of the box,
as do recent gcc-2.9 builds available directly from \s-1IBM\s0 as part of their
Linux compatibility packages, available here:

.Vb 1
  http://www.ibm.com/servers/aix/products/aixos/linux/
.Ve

### Using Large Files with Perl < 5L

Subsection "Using Large Files with Perl < 5L"
Should yield no problems.

### Threaded Perl < 5L

Subsection "Threaded Perl < 5L"
Threads seem to work \s-1OK,\s0 though at the moment not all tests pass when
threads are used in combination with 64-bit configurations.

You may get a warning when doing a threaded build:

.Vb 2
  "pp_sys.c", line 4640.39: 1506-280 (W) Function argument assignment
  between types "unsigned char*" and "const void*" is not allowed.
.Ve

The exact line number may vary, but if the warning (W) comes from a line
line this

.Vb 1
  hent = PerlSock_gethostbyaddr(addr, (Netdb_hlen_t) addrlen, addrtype);
.Ve

in the \*(L"pp_ghostent\*(R" function, you may ignore it safely.  The warning
is caused by the reentrant variant of **gethostbyaddr()** having a slightly
different prototype than its non-reentrant variant, but the difference
is not really significant here.

### 64-bit Perl < 5L

Subsection "64-bit Perl < 5L"
If your \s-1AIX\s0 is installed with 64-bit support, you can expect 64-bit
configurations to work. In combination with threads some tests might
still fail.

### \s-1AIX 4.2\s0 and extensions using \*(C+ with statics

Subsection "AIX 4.2 and extensions using with statics"
In \s-1AIX 4.2\s0 Perl extensions that use \*(C+ functions that use statics
may have problems in that the statics are not getting initialized.
In newer \s-1AIX\s0 releases this has been solved by linking Perl with
the libC_r library, but unfortunately in \s-1AIX 4.2\s0 the said library
has an obscure bug where the various functions related to time
(such as **time()** and **gettimeofday()**) return broken values, and
therefore in \s-1AIX 4.2\s0 Perl is not linked against the libC_r.

## AUTHORS

Header "AUTHORS"
Rainer Tammer <tammer@tammer.net>
