+++
date = "2022-02-19"
detected_package_version = "5.34.1"
author = "None Specified"
operating_system = "macos"
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
manpage_section = "1"
manpage_format = "troff"
manpage_name = "perl5263delta"
description = "This document describes differences between the 5.26.2 release and the 5.26.3 release. If you are upgrading from an earlier release such as 5.26.1, first read perl5262delta, which describes differences between 5.26.1 and 5.26.2. By default, Archiv..."
operating_system_version = "15.3"
title = "perl5263delta(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5263DELTA 1"
PERL5263DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5263delta - what is new for perl v5.26.3

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.26.2 release and the 5.26.3
release.

If you are upgrading from an earlier release such as 5.26.1, first read
perl5262delta, which describes differences between 5.26.1 and 5.26.2.

## Security

Header "Security"

### [\s-1CVE-2018-12015\s0] Directory traversal in module Archive::Tar

Subsection "[CVE-2018-12015] Directory traversal in module Archive::Tar"
By default, Archive::Tar doesn't allow extracting files outside the current
working directory.  However, this secure extraction mode could be bypassed by
putting a symlink and a regular file with the same name into the tar file.

[\s-1GH\s0 #16580] <https://github.com/Perl/perl5/issues/16580>
[cpan #125523] <https://rt.cpan.org/Ticket/Display.html?id=125523>

### [\s-1CVE-2018-18311\s0] Integer overflow leading to buffer overflow and segmentation fault

Subsection "[CVE-2018-18311] Integer overflow leading to buffer overflow and segmentation fault"
Integer arithmetic in \f(CW\*(C`Perl_my_setenv()\*(C' could wrap when the combined length
of the environment variable name and value exceeded around 0x7fffffff.  This
could lead to writing beyond the end of an allocated buffer with attacker
supplied data.

[\s-1GH\s0 #16560] <https://github.com/Perl/perl5/issues/16560>

### [\s-1CVE-2018-18312\s0] Heap-buffer-overflow write in S_regatom (regcomp.c)

Subsection "[CVE-2018-18312] Heap-buffer-overflow write in S_regatom (regcomp.c)"
A crafted regular expression could cause heap-buffer-overflow write during
compilation, potentially allowing arbitrary code execution.

[\s-1GH\s0 #16649] <https://github.com/Perl/perl5/issues/16649>

### [\s-1CVE-2018-18313\s0] Heap-buffer-overflow read in S_grok_bslash_N (regcomp.c)

Subsection "[CVE-2018-18313] Heap-buffer-overflow read in S_grok_bslash_N (regcomp.c)"
A crafted regular expression could cause heap-buffer-overflow read during
compilation, potentially leading to sensitive information being leaked.

[\s-1GH\s0 #16554] <https://github.com/Perl/perl5/issues/16554>

### [\s-1CVE-2018-18314\s0] Heap-buffer-overflow write in S_regatom (regcomp.c)

Subsection "[CVE-2018-18314] Heap-buffer-overflow write in S_regatom (regcomp.c)"
A crafted regular expression could cause heap-buffer-overflow write during
compilation, potentially allowing arbitrary code execution.

[\s-1GH\s0 #16041] <https://github.com/Perl/perl5/issues/16041>

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.26.2.  If any exist,
they are bugs, and we request that you submit a report.  See
\*(L"Reporting Bugs\*(R" below.

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Archive::Tar has been upgraded from version 2.24 to 2.24_01.

- \(bu
Module::CoreList has been upgraded from version 5.20180414_26 to 5.20181129_26.

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### New Diagnostics

Subsection "New Diagnostics"
*New Errors*
Subsection "New Errors"

- \(bu
Unexpected ']' with no following ')' in (?[... in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
(F) While parsing an extended character class a ']' character was encountered
at a point in the definition where the only legal use of ']' is to close the
character class definition as part of a '])', you may have forgotten the close
paren, or otherwise confused the parser.

- \(bu
Expecting close paren for nested extended charclass in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
(F) While parsing a nested extended character class like:
.Sp
.Vb 2
    (?[ ... (?flags:(?[ ... ])) ... ])
                             ^
.Ve
.Sp
we expected to see a close paren ')' (marked by ^) but did not.

- \(bu
Expecting close paren for wrapper for nested extended charclass in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
(F) While parsing a nested extended character class like:
.Sp
.Vb 2
    (?[ ... (?flags:(?[ ... ])) ... ])
                              ^
.Ve
.Sp
we expected to see a close paren ')' (marked by ^) but did not.

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
Syntax error in (?[...]) in regex; marked by <-- \s-1HERE\s0 in m/%s/
.Sp
This fatal error message has been slightly expanded (from \*(L"Syntax error in
(?[...]) in regex m/%s/\*(R") for greater clarity.

## Acknowledgements

Header "Acknowledgements"
Perl 5.26.3 represents approximately 8 months of development since Perl 5.26.2
and contains approximately 4,500 lines of changes across 51 files from 15
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 770 lines of changes to 10 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.26.3:

Aaron Crane, Abigail, Chris 'BinGOs' Williams, Dagfinn Ilmari Mannsa\*oker, David
Mitchell, H.Merijn Brand, James E Keenan, John \s-1SJ\s0 Anderson, Karen Etheridge,
Karl Williamson, Sawyer X, Steve Hay, Todd Rinaldo, Tony Cook, Yves Orton.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the perl bug database
at <https://rt.perl.org/> .  There may also be information at
<http://www.perl.org/> , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications which make it
inappropriate to send to a publicly archived mailing list, then see
\*(L"\s-1SECURITY VULNERABILITY CONTACT INFORMATION\*(R"\s0 in perlsec
for details of how to report the issue.

## Give Thanks

Header "Give Thanks"
If you wish to thank the Perl 5 Porters for the work we had done in Perl 5,
you can do so by running the \f(CW\*(C`perlthanks\*(C' program:

.Vb 1
    perlthanks
.Ve

This will send an email to the Perl 5 Porters list with your show of thanks.

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
