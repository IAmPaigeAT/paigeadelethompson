+++
manpage_name = "perl5142delta"
title = "perl5142delta(1)"
operating_system_version = "15.3"
manpage_section = "1"
author = "None Specified"
manpage_format = "troff"
description = "This document describes differences between the 5.14.1 release and the 5.14.2 release. If you are upgrading from an earlier release such as 5.14.0, first read perl5141delta, which describes differences between 5.14.0 and 5.14.1. No changes since ..."
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
operating_system = "macos"
date = "2022-02-19"
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5142DELTA 1"
PERL5142DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5142delta - what is new for perl v5.14.2

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.14.1 release and
the 5.14.2 release.

If you are upgrading from an earlier release such as 5.14.0, first read
perl5141delta, which describes differences between 5.14.0 and
5.14.1.

## Core Enhancements

Header "Core Enhancements"
No changes since 5.14.0.

## Security

Header "Security"
.ie n .SS """File::Glob::bsd_glob()"" memory error with \s-1GLOB_ALTDIRFUNC\s0 (\s-1CVE-2011-2728\s0)."
.el .SS "\f(CWFile::Glob::bsd_glob() memory error with \s-1GLOB_ALTDIRFUNC\s0 (\s-1CVE-2011-2728\s0)."
Subsection "File::Glob::bsd_glob() memory error with GLOB_ALTDIRFUNC (CVE-2011-2728)."
Calling \f(CW\*(C`File::Glob::bsd_glob\*(C' with the unsupported flag \s-1GLOB_ALTDIRFUNC\s0 would
cause an access violation / segfault.  A Perl program that accepts a flags value from
an external source could expose itself to denial of service or arbitrary code
execution attacks.  There are no known exploits in the wild.  The problem has been
corrected by explicitly disabling all unsupported flags and setting unused function
pointers to null.  Bug reported by Cle\*'ment Lecigne.
.ie n .SS """Encode"" decode_xs n-byte heap-overflow (\s-1CVE-2011-2939\s0)"
.el .SS "\f(CWEncode decode_xs n-byte heap-overflow (\s-1CVE-2011-2939\s0)"
Subsection "Encode decode_xs n-byte heap-overflow (CVE-2011-2939)"
A bug in \f(CW\*(C`Encode\*(C' could, on certain inputs, cause the heap to overflow.
This problem has been corrected.  Bug reported by Robert Zacek.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.14.0. If any
exist, they are bugs and reports are welcome.

## Deprecations

Header "Deprecations"
There have been no deprecations since 5.14.0.

## Modules and Pragmata

Header "Modules and Pragmata"

### New Modules and Pragmata

Subsection "New Modules and Pragmata"
None

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
\s-1CPAN\s0 has been upgraded from version 1.9600 to version 1.9600_01.
.Sp
CPAN::Distribution has been upgraded from version 1.9602 to 1.9602_01.
.Sp
Backported bugfixes from \s-1CPAN\s0 version 1.9800.  Ensures proper
detection of \f(CW\*(C`configure_requires\*(C' prerequisites from \s-1CPAN\s0 Meta files
in the case where \f(CW\*(C`dynamic_config\*(C' is true.  [rt.cpan.org #68835]
.Sp
Also ensures that \f(CW\*(C`configure_requires\*(C' is only checked in \s-1META\s0 files,
not \s-1MYMETA\s0 files, so protect against \s-1MYMETA\s0 generation that drops
\f(CW\*(C`configure_requires\*(C'.

- \(bu
Encode has been upgraded from version 2.42 to 2.42_01.
.Sp
See \*(L"Security\*(R".

- \(bu
File::Glob has been upgraded from version 1.12 to version 1.13.
.Sp
See \*(L"Security\*(R".

- \(bu
PerlIO::scalar has been upgraded from version 0.11 to 0.11_01.
.Sp
It fixes a problem with \f(CW\*(C`open my $fh, ">", \\$scalar\*(C' not working if
\f(CW$scalar is a copy-on-write scalar.

### Removed Modules and Pragmata

Subsection "Removed Modules and Pragmata"
None

## Platform Support

Header "Platform Support"

### New Platforms

Subsection "New Platforms"
None

### Discontinued Platforms

Subsection "Discontinued Platforms"
None

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- HP-UX \s-1PA-RISC/64\s0 now supports gcc-4.x
Item "HP-UX PA-RISC/64 now supports gcc-4.x"
A fix to correct the socketsize now makes the test suite pass on HP-UX
PA-RISC for 64bitall builds.

- Building on \s-1OS X 10.7\s0 Lion and Xcode 4 works again
Item "Building on OS X 10.7 Lion and Xcode 4 works again"
The build system has been updated to work with the build tools under Mac \s-1OS X
10.7.\s0

## Bug Fixes

Header "Bug Fixes"

- \(bu
In \f(CW@INC filters (subroutines returned by subroutines in \f(CW@INC), \f(CW$_ used to
misbehave: If returned from a subroutine, it would not be copied, but the
variable itself would be returned; and freeing \f(CW$_ (e.g., with \f(CW\*(C`undef *_\*(C')
would cause perl to crash.  This has been fixed [perl #91880].

- \(bu
Perl 5.10.0 introduced some faulty logic that made \*(L"U*\*(R" in the middle of
a pack template equivalent to \*(L"U0\*(R" if the input string was empty.  This has
been fixed [perl #90160].

- \(bu
\f(CW\*(C`caller\*(C' no longer leaks memory when called from the \s-1DB\s0 package if
\f(CW@DB::args was assigned to after the first call to \f(CW\*(C`caller\*(C'.  Carp
was triggering this bug [perl #97010].

- \(bu
\f(CW\*(C`utf8::decode\*(C' had a nasty bug that would modify copy-on-write scalars'
string buffers in place (i.e., skipping the copy).  This could result in
hashes having two elements with the same key [perl #91834].

- \(bu
Localising a tied variable used to make it read-only if it contained a
copy-on-write string.

- \(bu
Elements of restricted hashes (see the fields pragma) containing
copy-on-write values couldn't be deleted, nor could such hashes be cleared
(\f(CW\*(C`%hash = ()\*(C').

- \(bu
Locking a hash element that is a glob copy no longer causes subsequent
assignment to it to corrupt the glob.

- \(bu
A panic involving the combination of the regular expression modifiers
\f(CW\*(C`/aa\*(C' introduced in 5.14.0 and the \f(CW\*(C`\\b\*(C' escape sequence has been
fixed [perl #95964].

## Known Problems

Header "Known Problems"
This is a list of some significant unfixed bugs, which are regressions
from 5.12.0.

- \(bu
\f(CW\*(C`PERL_GLOBAL_STRUCT\*(C' is broken.
.Sp
Since perl 5.14.0, building with \f(CW\*(C`-DPERL_GLOBAL_STRUCT\*(C' hasn't been
possible. This means that perl currently doesn't work on any platforms that
require it to be built this way, including Symbian.
.Sp
While \f(CW\*(C`PERL_GLOBAL_STRUCT\*(C' now works again on recent development versions of
perl, it actually working on Symbian again hasn't been verified.
.Sp
We'd be very interested in hearing from anyone working with Perl on Symbian.

## Acknowledgements

Header "Acknowledgements"
Perl 5.14.2 represents approximately three months of development since
Perl 5.14.1 and contains approximately 1200 lines of changes
across 61 files from 9 authors.

Perl continues to flourish into its third decade thanks to a vibrant
community of users and developers.  The following people are known to
have contributed the improvements that became Perl 5.14.2:

Craig A. Berry, David Golden, Father Chrysostomos, Florian Ragwitz, H.Merijn
Brand, Karl Williamson, Nicholas Clark, Pau Amma and Ricardo Signes.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles
recently posted to the comp.lang.perl.misc newsgroup and the perl
bug database at http://rt.perl.org/perlbug/ .  There may also be
information at http://www.perl.org/ , the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug
program included with your release.  Be sure to trim your bug down
to a tiny but sufficient test case.  Your bug report, along with the
output of \f(CW\*(C`perl -V\*(C', will be sent off to perlbug@perl.org to be
analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send
it to perl5-security-report@perl.org. This points to a closed subscription
unarchived mailing list, which includes all the core committers, who be able
to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported. Please only use this address for
security issues in the Perl core, not for modules independently
distributed on \s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details
on what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
