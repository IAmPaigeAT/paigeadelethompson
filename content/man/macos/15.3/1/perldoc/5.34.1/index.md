+++
detected_package_version = "5.34.1"
manpage_format = "troff"
keywords = ["header", "see", "also", "perlpod", "pod", "perldoc", "author", "current", "maintainer", "mark", "allen", "f", "cw", "c", "mallen", "cpan", "org", "past", "contributors", "are", "brian", "d", "foy", "bdfoy", "adriano", "r", "ferreira", "sean", "m", "burke", "sburke", "kenneth", "albanowski", "kjahds", "com", "andy", "dougherty", "doughera", "lafcol", "lafayette", "edu", "and", "many", "others"]
description = "perldoc looks up documentation in .pod format that is embedded in the perl installation tree or in a perl script, and displays it using a variety of formatters.  This is primarily used for the documentation for the perl library modules. Your syste..."
operating_system_version = "15.3"
operating_system = "macos"
date = "2022-02-19"
author = "None Specified"
title = "perldoc(1)"
manpage_name = "perldoc"
manpage_section = "1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLDOC 1"
PERLDOC 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perldoc - Look up Perl documentation in Pod format.

## SYNOPSIS

Header "SYNOPSIS"
.Vb 10
    perldoc [-h] [-D] [-t] [-u] [-m] [-l] [-U] [-F]
        [-i] [-V] [-T] [-r]
        [-d destination_file]
        [-o formatname]
        [-M FormatterClassName]
        [-w formatteroption:value]
        [-n nroff-replacement]
        [-X]
        [-L language_code]
        PageName|ModuleName|ProgramName|URL
.Ve

Examples:

.Vb 1
    perldoc -f BuiltinFunction

    perldoc -L it -f BuiltinFunction

    perldoc -q FAQ Keyword

    perldoc -L fr -q FAQ Keyword

    perldoc -v PerlVariable

    perldoc -a PerlAPI
.Ve

See below for more description of the switches.

## DESCRIPTION

Header "DESCRIPTION"
**perldoc** looks up documentation in .pod format that is embedded in the perl
installation tree or in a perl script, and displays it using a variety of
formatters.  This is primarily used for the documentation for the perl library
modules.

Your system may also have man pages installed for those modules, in
which case you can probably just use the **man**\|(1) command.

If you are looking for a table of contents to the Perl library modules
documentation, see the perltoc page.

## OPTIONS

Header "OPTIONS"

- \fB-h
Item "-h"
Prints out a brief **h**elp message.

- \fB-D
Item "-D"
**D**escribes search for the item in **d**etail.

- \fB-t
Item "-t"
Display docs using plain **t**ext converter, instead of nroff. This may be faster,
but it probably won't look as nice.

- \fB-u
Item "-u"
Skip the real Pod formatting, and just show the raw Pod source (**U**nformatted)

- \fB-m \fImodule
Item "-m module"
Display the entire module: both code and unformatted pod documentation.
This may be useful if the docs don't explain a function in the detail
you need, and you'd like to inspect the code directly; perldoc will find
the file for you and simply hand it off for display.

- \fB-l
Item "-l"
Display on**l**y the file name of the module found.

- \fB-U
Item "-U"
When running as the superuser, don't attempt drop privileges for security.
This option is implied with **-F**.
.Sp
**\s-1NOTE\s0**: Please see the heading \s-1SECURITY\s0 below for more information.

- \fB-F
Item "-F"
Consider arguments as file names; no search in directories will be performed.
Implies **-U** if run as the superuser.

- \fB-f \fIperlfunc
Item "-f perlfunc"
The **-f** option followed by the name of a perl built-in function will
extract the documentation of this function from perlfunc.
.Sp
Example:
.Sp
.Vb 1
      perldoc -f sprintf
.Ve

- \fB-q \fIperlfaq-search-regexp
Item "-q perlfaq-search-regexp"
The **-q** option takes a regular expression as an argument.  It will search
the **q**uestion headings in perlfaq[1-9] and print the entries matching
the regular expression.
.Sp
Example:
.Sp
.Vb 1
     perldoc -q shuffle
.Ve

- \fB-a \fIperlapifunc
Item "-a perlapifunc"
The **-a** option followed by the name of a perl api function will
extract the documentation of this function from perlapi.
.Sp
Example:
.Sp
.Vb 1
     perldoc -a newHV
.Ve

- \fB-v \fIperlvar
Item "-v perlvar"
The **-v** option followed by the name of a Perl predefined variable will
extract the documentation of this variable from perlvar.
.Sp
Examples:
.Sp
.Vb 3
     perldoc -v \*(Aq$"\*(Aq
     perldoc -v @+
     perldoc -v DATA
.Ve

- \fB-T
Item "-T"
This specifies that the output is not to be sent to a pager, but is to
be sent directly to \s-1STDOUT.\s0

- \fB-d \fIdestination-filename
Item "-d destination-filename"
This specifies that the output is to be sent neither to a pager nor
to \s-1STDOUT,\s0 but is to be saved to the specified filename.  Example:
\f(CW\*(C`perldoc -oLaTeX -dtextwrapdocs.tex Text::Wrap\*(C'

- \fB-o \fIoutput-formatname
Item "-o output-formatname"
This specifies that you want Perldoc to try using a Pod-formatting
class for the output format that you specify.  For example:
\f(CW\*(C`-oman\*(C'.  This is actually just a wrapper around the \f(CW\*(C`-M\*(C' switch;
using \f(CW\*(C`-o\f(CIformatname\f(CW\*(C' just looks for a loadable class by adding
that format name (with different capitalizations) to the end of
different classname prefixes.
.Sp
For example, \f(CW\*(C`-oLaTeX\*(C' currently tries all of the following classes:
Pod::Perldoc::ToLaTeX Pod::Perldoc::Tolatex Pod::Perldoc::ToLatex
Pod::Perldoc::ToLATEX Pod::Simple::LaTeX Pod::Simple::latex
Pod::Simple::Latex Pod::Simple::LATEX Pod::LaTeX Pod::latex Pod::Latex
Pod::LATEX.

- \fB-M \fImodule-name
Item "-M module-name"
This specifies the module that you want to try using for formatting the
pod.  The class must at least provide a \f(CW\*(C`parse_from_file\*(C' method.
For example: \f(CW\*(C`perldoc -MPod::Perldoc::ToChecker\*(C'.
.Sp
You can specify several classes to try by joining them with commas
or semicolons, as in \f(CW\*(C`-MTk::SuperPod;Tk::Pod\*(C'.

- \fB-w \fIoption:value or \fB-w \fIoption
Item "-w option:value or -w option"
This specifies an option to call the formatter **w**ith.  For example,
\f(CW\*(C`-w textsize:15\*(C' will call
\f(CW\*(C`$formatter->textsize(15)\*(C' on the formatter object before it is
used to format the object.  For this to be valid, the formatter class
must provide such a method, and the value you pass should be valid.
(So if \f(CW\*(C`textsize\*(C' expects an integer, and you do \f(CW\*(C`-w textsize:big\*(C',
expect trouble.)
.Sp
You can use \f(CW\*(C`-w optionname\*(C' (without a value) as shorthand for
\f(CW\*(C`-w optionname:\f(CITRUE\f(CW\*(C'.  This is presumably useful in cases of on/off
features like: \f(CW\*(C`-w page_numbering\*(C'.
.Sp
You can use an \*(L"=\*(R" instead of the \*(L":\*(R", as in: \f(CW\*(C`-w textsize=15\*(C'.  This
might be more (or less) convenient, depending on what shell you use.

- \fB-X
Item "-X"
Use an index if it is present. The **-X** option looks for an entry
whose basename matches the name given on the command line in the file
\f(CW\*(C`$Config\{archlib\}/pod.idx\*(C'. The *pod.idx* file should contain fully
qualified filenames, one per line.

- \fB-L \fIlanguage_code
Item "-L language_code"
This allows one to specify the *language code* for the desired language
translation. If the \f(CW\*(C`POD2::<language_code>\*(C' package isn't
installed in your system, the switch is ignored.
All available translation packages are to be found under the \f(CW\*(C`POD2::\*(C'
namespace. See \s-1POD2::IT\s0 (or \s-1POD2::FR\s0) to see how to create new
localized \f(CW\*(C`POD2::*\*(C' documentation packages and integrate them into
Pod::Perldoc.

- \fBPageName|ModuleName|ProgramName|URL
Item "PageName|ModuleName|ProgramName|URL"
The item you want to look up.  Nested modules (such as \f(CW\*(C`File::Basename\*(C')
are specified either as \f(CW\*(C`File::Basename\*(C' or \f(CW\*(C`File/Basename\*(C'.  You may also
give a descriptive name of a page, such as \f(CW\*(C`perlfunc\*(C'.  For URLs, \s-1HTTP\s0 and
\s-1HTTPS\s0 are the only kind currently supported.
.Sp
For simple names like 'foo', when the normal search fails to find
a matching page, a search with the \*(L"perl\*(R" prefix is tried as well.
So \*(L"perldoc intro\*(R" is enough to find/render \*(L"perlintro.pod\*(R".

- \fB-n \fIsome-formatter
Item "-n some-formatter"
Specify replacement for groff

- \fB-r
Item "-r"
Recursive search.

- \fB-i
Item "-i"
Ignore case.

- \fB-V
Item "-V"
Displays the version of perldoc you're running.

## SECURITY

Header "SECURITY"
Because **perldoc** does not run properly tainted, and is known to
have security issues, when run as the superuser it will attempt to
drop privileges by setting the effective and real IDs to nobody's
or nouser's account, or -2 if unavailable.  If it cannot relinquish
its privileges, it will not run.

See the \f(CW\*(C`-U\*(C' option if you do not want this behavior but **beware**
that there are significant security risks if you choose to use \f(CW\*(C`-U\*(C'.

Since 3.26, using \f(CW\*(C`-F\*(C' as the superuser also implies \f(CW\*(C`-U\*(C' as opening
most files and traversing directories requires privileges that are
above the nobody/nogroup level.

## ENVIRONMENT

Header "ENVIRONMENT"
Any switches in the \f(CW\*(C`PERLDOC\*(C' environment variable will be used before the
command line arguments.

Useful values for \f(CW\*(C`PERLDOC\*(C' include \f(CW\*(C`-oterm\*(C', \f(CW\*(C`-otext\*(C', \f(CW\*(C`-ortf\*(C',
\f(CW\*(C`-oxml\*(C', and so on, depending on what modules you have on hand; or
the formatter class may be specified exactly with \f(CW\*(C`-MPod::Perldoc::ToTerm\*(C'
or the like.

\f(CW\*(C`perldoc\*(C' also searches directories
specified by the \f(CW\*(C`PERL5LIB\*(C' (or \f(CW\*(C`PERLLIB\*(C' if \f(CW\*(C`PERL5LIB\*(C' is not
defined) and \f(CW\*(C`PATH\*(C' environment variables.
(The latter is so that embedded pods for executables, such as
\f(CW\*(C`perldoc\*(C' itself, are available.)

In directories where either \f(CW\*(C`Makefile.PL\*(C' or \f(CW\*(C`Build.PL\*(C' exist, \f(CW\*(C`perldoc\*(C'
will add \f(CW\*(C`.\*(C' and \f(CW\*(C`lib\*(C' first to its search path, and as long as you're not
the superuser will add \f(CW\*(C`blib\*(C' too.  This is really helpful if you're working
inside of a build directory and want to read through the docs even if you
have a version of a module previously installed.

\f(CW\*(C`perldoc\*(C' will use, in order of preference, the pager defined in
\f(CW\*(C`PERLDOC_PAGER\*(C', \f(CW\*(C`MANPAGER\*(C', or \f(CW\*(C`PAGER\*(C' before trying to find a pager
on its own. (\f(CW\*(C`MANPAGER\*(C' is not used if \f(CW\*(C`perldoc\*(C' was told to display
plain text or unformatted pod.)

When using perldoc in it's \f(CW\*(C`-m\*(C' mode (display module source code),
\f(CW\*(C`perldoc\*(C' will attempt to use the pager set in \f(CW\*(C`PERLDOC_SRC_PAGER\*(C'.
A useful setting for this command is your favorite editor as in
\f(CW\*(C`/usr/bin/nano\*(C'. (Don't judge me.)

One useful value for \f(CW\*(C`PERLDOC_PAGER\*(C' is \f(CW\*(C`less -+C -E\*(C'.

Having \s-1PERLDOCDEBUG\s0 set to a positive integer will make perldoc emit
even more descriptive output than the \f(CW\*(C`-D\*(C' switch does; the higher the
number, the more it emits.

## CHANGES

Header "CHANGES"
Up to 3.14_05, the switch **-v** was used to produce verbose
messages of **perldoc** operation, which is now enabled by **-D**.

## SEE ALSO

Header "SEE ALSO"
perlpod, Pod::Perldoc

## AUTHOR

Header "AUTHOR"
Current maintainer: Mark Allen \f(CW\*(C`<mallen@cpan.org>\*(C'

Past contributors are:
brian d foy \f(CW\*(C`<bdfoy@cpan.org>\*(C'
Adriano R. Ferreira \f(CW\*(C`<ferreira@cpan.org>\*(C',
Sean M. Burke \f(CW\*(C`<sburke@cpan.org>\*(C',
Kenneth Albanowski \f(CW\*(C`<kjahds@kjahds.com>\*(C',
Andy Dougherty  \f(CW\*(C`<doughera@lafcol.lafayette.edu>\*(C',
and many others.
