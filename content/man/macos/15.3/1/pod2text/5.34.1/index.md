+++
operating_system = "macos"
title = "pod2text(1)"
description = "pod2text is a front-end for Pod::Text and its subclasses.  It uses them to generate formatted s-1ASCIIs0 text from s-1PODs0 source.  It can optionally use either termcap sequences or s-1ANSIs0 color escape sequences to format the text. input is the..."
operating_system_version = "15.3"
detected_package_version = "5.34.1"
keywords = ["header", "see", "also", "pod", "text", "color", "overstrike", "termcap", "simple", "fbperlpod", "1", "the", "current", "version", "of", "this", "script", "is", "always", "available", "from", "its", "web", "site", "at", "https", "www", "eyrie", "org", "eagle", "software", "podlators", "it", "part", "perl", "core", "distribution", "as", "5", "6"]
manpage_section = "1"
author = "None Specified"
manpage_format = "troff"
manpage_name = "pod2text"
date = "2024-12-14"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "POD2TEXT 1"
POD2TEXT 1 "2024-12-14" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

pod2text - Convert POD data to formatted ASCII text

## SYNOPSIS

Header "SYNOPSIS"
pod2text [**-aclostu**] [**--code**] [**--errors**=*style*] [**-i** *indent*]
    [**-q**\ *quotes*] [**--nourls**] [**--stderr**] [**-w**\ *width*]
    [*input* [*output* ...]]

pod2text **-h**

## DESCRIPTION

Header "DESCRIPTION"
**pod2text** is a front-end for Pod::Text and its subclasses.  It uses them
to generate formatted \s-1ASCII\s0 text from \s-1POD\s0 source.  It can optionally use
either termcap sequences or \s-1ANSI\s0 color escape sequences to format the text.

*input* is the file to read for \s-1POD\s0 source (the \s-1POD\s0 can be embedded in
code).  If *input* isn't given, it defaults to \f(CW\*(C`STDIN\*(C'.  *output*, if
given, is the file to which to write the formatted output.  If *output*
isn't given, the formatted output is written to \f(CW\*(C`STDOUT\*(C'.  Several \s-1POD\s0
files can be processed in the same **pod2text** invocation (saving module
load and compile times) by providing multiple pairs of *input* and
*output* files on the command line.

## OPTIONS

Header "OPTIONS"

- \fB-a, \fB--alt
Item "-a, --alt"
Use an alternate output format that, among other things, uses a different
heading style and marks \f(CW\*(C`=item\*(C' entries with a colon in the left margin.

- \fB--code
Item "--code"
Include any non-POD text from the input file in the output as well.  Useful
for viewing code documented with \s-1POD\s0 blocks with the \s-1POD\s0 rendered and the
code left intact.

- \fB-c, \fB--color
Item "-c, --color"
Format the output with \s-1ANSI\s0 color escape sequences.  Using this option
requires that Term::ANSIColor be installed on your system.

- \fB--errors=\fIstyle
Item "--errors=style"
Set the error handling style.  \f(CW\*(C`die\*(C' says to throw an exception on any
\s-1POD\s0 formatting error.  \f(CW\*(C`stderr\*(C' says to report errors on standard error,
but not to throw an exception.  \f(CW\*(C`pod\*(C' says to include a \s-1POD ERRORS\s0
section in the resulting documentation summarizing the errors.  \f(CW\*(C`none\*(C'
ignores \s-1POD\s0 errors entirely, as much as possible.
.Sp
The default is \f(CW\*(C`die\*(C'.

- \fB-i \fIindent, \fB--indent=\fIindent
Item "-i indent, --indent=indent"
Set the number of spaces to indent regular text, and the default indentation
for \f(CW\*(C`=over\*(C' blocks.  Defaults to 4 spaces if this option isn't given.

- \fB-h, \fB--help
Item "-h, --help"
Print out usage information and exit.

- \fB-l, \fB--loose
Item "-l, --loose"
Print a blank line after a \f(CW\*(C`=head1\*(C' heading.  Normally, no blank line is
printed after \f(CW\*(C`=head1\*(C', although one is still printed after \f(CW\*(C`=head2\*(C',
because this is the expected formatting for manual pages; if you're
formatting arbitrary text documents, using this option is recommended.

- \fB-m \fIwidth, \fB--left-margin=\fIwidth, \fB--margin=\fIwidth
Item "-m width, --left-margin=width, --margin=width"
The width of the left margin in spaces.  Defaults to 0.  This is the margin
for all text, including headings, not the amount by which regular text is
indented; for the latter, see **-i** option.

- \fB--nourls
Item "--nourls"
Normally, L<> formatting codes with a \s-1URL\s0 but anchor text are formatted
to show both the anchor text and the \s-1URL.\s0  In other words:
.Sp
.Vb 1
    L<foo|http://example.com/>
.Ve
.Sp
is formatted as:
.Sp
.Vb 1
    foo <http://example.com/>
.Ve
.Sp
This flag, if given, suppresses the \s-1URL\s0 when anchor text is given, so this
example would be formatted as just \f(CW\*(C`foo\*(C'.  This can produce less
cluttered output in cases where the URLs are not particularly important.

- \fB-o, \fB--overstrike
Item "-o, --overstrike"
Format the output with overstrike printing.  Bold text is rendered as
character, backspace, character.  Italics and file names are rendered as
underscore, backspace, character.  Many pagers, such as **less**, know how
to convert this to bold or underlined text.

- \fB-q \fIquotes, \fB--quotes=\fIquotes
Item "-q quotes, --quotes=quotes"
Sets the quote marks used to surround C<> text to *quotes*.  If
*quotes* is a single character, it is used as both the left and right
quote.  Otherwise, it is split in half, and the first half of the string
is used as the left quote and the second is used as the right quote.
.Sp
*quotes* may also be set to the special value \f(CW\*(C`none\*(C', in which case no
quote marks are added around C<> text.

- \fB-s, \fB--sentence
Item "-s, --sentence"
Assume each sentence ends with two spaces and try to preserve that spacing.
Without this option, all consecutive whitespace in non-verbatim paragraphs
is compressed into a single space.

- \fB--stderr
Item "--stderr"
By default, **pod2text** dies if any errors are detected in the \s-1POD\s0 input.
If **--stderr** is given and no **--errors** flag is present, errors are
sent to standard error, but **pod2text** does not abort.  This is
equivalent to \f(CW\*(C`--errors=stderr\*(C' and is supported for backward
compatibility.

- \fB-t, \fB--termcap
Item "-t, --termcap"
Try to determine the width of the screen and the bold and underline
sequences for the terminal from termcap, and use that information in
formatting the output.  Output will be wrapped at two columns less than the
width of your terminal device.  Using this option requires that your system
have a termcap file somewhere where Term::Cap can find it and requires that
your system support termios.  With this option, the output of **pod2text**
will contain terminal control sequences for your current terminal type.

- \fB-u, \fB--utf8
Item "-u, --utf8"
By default, **pod2text** tries to use the same output encoding as its input
encoding (to be backward-compatible with older versions).  This option
says to instead force the output encoding to \s-1UTF-8.\s0
.Sp
Be aware that, when using this option, the input encoding of your \s-1POD\s0
source should be properly declared unless it's US-ASCII.  Pod::Simple
will attempt to guess the encoding and may be successful if it's
Latin-1 or \s-1UTF-8,\s0 but it will warn, which by default results in a
**pod2text** failure.  Use the \f(CW\*(C`=encoding\*(C' command to declare the
encoding.  See **perlpod**\|(1) for more information.

- \fB-w, \fB--width=\fIwidth, \fB-\fIwidth
Item "-w, --width=width, -width"
The column at which to wrap text on the right-hand side.  Defaults to 76,
unless **-t** is given, in which case it's two columns less than the width of
your terminal device.

## EXIT STATUS

Header "EXIT STATUS"
As long as all documents processed result in some output, even if that
output includes errata (a \f(CW\*(C`POD ERRORS\*(C' section generated with
\f(CW\*(C`--errors=pod\*(C'), **pod2text** will exit with status 0.  If any of the
documents being processed do not result in an output document, **pod2text**
will exit with status 1.  If there are syntax errors in a \s-1POD\s0 document
being processed and the error handling style is set to the default of
\f(CW\*(C`die\*(C', **pod2text** will abort immediately with exit status 255.

## DIAGNOSTICS

Header "DIAGNOSTICS"
If **pod2text** fails with errors, see Pod::Text and Pod::Simple for
information about what those errors might mean.  Internally, it can also
produce the following diagnostics:

- -c (--color) requires Term::ANSIColor be installed
Item "-c (--color) requires Term::ANSIColor be installed"
(F) **-c** or **--color** were given, but Term::ANSIColor could not be
loaded.
.ie n .IP "Unknown option: %s" 4
.el .IP "Unknown option: \f(CW%s" 4
Item "Unknown option: %s"
(F) An unknown command line option was given.

In addition, other Getopt::Long error messages may result from invalid
command-line options.

## ENVIRONMENT

Header "ENVIRONMENT"

- \s-1COLUMNS\s0
Item "COLUMNS"
If **-t** is given, **pod2text** will take the current width of your screen
from this environment variable, if available.  It overrides terminal width
information in \s-1TERMCAP.\s0

- \s-1TERMCAP\s0
Item "TERMCAP"
If **-t** is given, **pod2text** will use the contents of this environment
variable if available to determine the correct formatting sequences for your
current terminal device.

## AUTHOR

Header "AUTHOR"
Russ Allbery <rra@cpan.org>.

## COPYRIGHT AND LICENSE

Header "COPYRIGHT AND LICENSE"
Copyright 1999-2001, 2004, 2006, 2008, 2010, 2012-2019 Russ Allbery
<rra@cpan.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

## SEE ALSO

Header "SEE ALSO"
Pod::Text, Pod::Text::Color, Pod::Text::Overstrike,
Pod::Text::Termcap, Pod::Simple, **perlpod**\|(1)

The current version of this script is always available from its web site at
<https://www.eyrie.org/~eagle/software/podlators/>.  It is also part of the
Perl core distribution as of 5.6.0.
