+++
detected_package_version = "0.7.3"
date = "Sun Feb 16 04:48:25 2025"
manpage_format = "troff"
title = "bundle-add(1)"
operating_system_version = "15.3"
author = "None Specified"
manpage_section = "1"
description = "Adds the named gem to the Gemfile and run bundle install. bundle install can be avoided by using the flag --skip-install. Example: bundle add rails bundle add rails --version < 3.0, > 1.1 bundle add rails --version ~> 5.0.0 --source https://g..."
operating_system = "macos"
manpage_name = "bundle-add"
+++

.
"BUNDLE-ADD" "1" "November 2018" "" ""
.

## NAME

**bundle-add** - Add gem to the Gemfile and run bundle install
.

## SYNOPSIS

**bundle add** *GEM_NAME* [--group=GROUP] [--version=VERSION] [--source=SOURCE] [--skip-install] [--strict] [--optimistic]
.

## DESCRIPTION

Adds the named gem to the Gemfile and run **bundle install**\. **bundle install** can be avoided by using the flag **--skip-install**\.
.
.P
Example:
.
.P
bundle add rails
.
.P
bundle add rails --version "< 3\.0, > 1\.1"
.
.P
bundle add rails --version "~> 5\.0\.0" --source "https://gems\.example\.com" --group "development"
.
.P
bundle add rails --skip-install
.
.P
bundle add rails --group "development, test"
.

## OPTIONS

.

**--version**, **-v**
Specify version requirements(s) for the added gem\.
.

**--group**, **-g**
Specify the group(s) for the added gem\. Multiple groups should be separated by commas\.
.

**--source**, , **-s**
Specify the source for the added gem\.
.

**--skip-install**
Adds the gem to the Gemfile but does not install it\.
.

**--optimistic**
Adds optimistic declaration of version
.

**--strict**
Adds strict declaration of version

