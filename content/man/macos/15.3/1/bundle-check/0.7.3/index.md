+++
author = "None Specified"
title = "bundle-check(1)"
detected_package_version = "0.7.3"
date = "Sun Feb 16 04:48:21 2025"
description = "check searches the local machine for each of the gems requested in the Gemfile. If all gems are found, Bundler prints a success message and exits with a status of 0. If not, the first missing gem is listed and Bundler exits status 1. --dry-run Lock..."
manpage_section = "1"
manpage_format = "troff"
manpage_name = "bundle-check"
operating_system_version = "15.3"
operating_system = "macos"
+++

.
"BUNDLE-CHECK" "1" "November 2018" "" ""
.

## NAME

**bundle-check** - Verifies if dependencies are satisfied by installed gems
.

## SYNOPSIS

**bundle check** [--dry-run] [--gemfile=FILE] [--path=PATH]
.

## DESCRIPTION

**check** searches the local machine for each of the gems requested in the Gemfile\. If all gems are found, Bundler prints a success message and exits with a status of 0\.
.
.P
If not, the first missing gem is listed and Bundler exits status 1\.
.

## OPTIONS

.

**--dry-run**
Locks the [**Gemfile(5)**][Gemfile(5)] before running the command\.
.

**--gemfile**
Use the specified gemfile instead of the [**Gemfile(5)**][Gemfile(5)]\.
.

**--path**
Specify a different path than the system default (**$BUNDLE_PATH** or **$GEM_HOME**)\. Bundler will remember this value for future installs on this machine\.

