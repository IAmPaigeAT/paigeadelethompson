+++
keywords = ["header", "see", "also", "the", "fichanges", "file", "for", "an", "explanation", "of", "how", "to", "view", "exhaustive", "details", "on", "what", "changed", "fi", "s-1install", "s0", "build", "perl", "s-1readme", "general", "stuff", "fiartistic", "and", "ficopying", "files", "copyright", "information"]
author = "None Specified"
manpage_section = "1"
manpage_format = "troff"
operating_system = "macos"
date = "2022-02-19"
manpage_name = "perl5201delta"
title = "perl5201delta(1)"
operating_system_version = "15.3"
description = "This document describes differences between the 5.20.0 release and the 5.20.1 release. If you are upgrading from an earlier release such as 5.18.0, first read perl5200delta, which describes differences between 5.18.0 and 5.20.0. There are no chang..."
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERL5201DELTA 1"
PERL5201DELTA 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perl5201delta - what is new for perl v5.20.1

## DESCRIPTION

Header "DESCRIPTION"
This document describes differences between the 5.20.0 release and the 5.20.1
release.

If you are upgrading from an earlier release such as 5.18.0, first read
perl5200delta, which describes differences between 5.18.0 and 5.20.0.

## Incompatible Changes

Header "Incompatible Changes"
There are no changes intentionally incompatible with 5.20.0.  If any exist,
they are bugs, and we request that you submit a report.  See \*(L"Reporting Bugs\*(R"
below.

## Performance Enhancements

Header "Performance Enhancements"

- \(bu
An optimization to avoid problems with \s-1COW\s0 and deliberately overallocated PVs
has been disabled because it interfered with another, more important,
optimization, causing a slowdown on some platforms.
[\s-1GH\s0 #13878] <https://github.com/Perl/perl5/issues/13878>

- \(bu
Returning a string from a lexical variable could be slow in some cases.  This
has now been fixed.
[\s-1GH\s0 #13880] <https://github.com/Perl/perl5/issues/13880>

## Modules and Pragmata

Header "Modules and Pragmata"

### Updated Modules and Pragmata

Subsection "Updated Modules and Pragmata"

- \(bu
Config::Perl::V has been upgraded from version 0.20 to 0.22.
.Sp
The list of Perl versions covered has been updated and some flaws in the
parsing have been fixed.

- \(bu
Exporter has been upgraded from version 5.70 to 5.71.
.Sp
Illegal \s-1POD\s0 syntax in the documentation has been corrected.

- \(bu
ExtUtils::CBuilder has been upgraded from version 0.280216 to 0.280217.
.Sp
Android builds now link to both **-lperl** and \f(CW$Config::Config\{perllibs\}.

- \(bu
File::Copy has been upgraded from version 2.29 to 2.30.
.Sp
The documentation now notes that \f(CW\*(C`copy\*(C' will not overwrite read-only files.

- \(bu
Module::CoreList has been upgraded from version 3.11 to 5.020001.
.Sp
The list of Perl versions covered has been updated.

- \(bu
The PathTools module collection has been upgraded from version 3.47 to 3.48.
.Sp
Fallbacks are now in place when cross-compiling for Android and
\f(CW$Config::Config\{sh\} is not yet defined.
[\s-1GH\s0 #13872] <https://github.com/Perl/perl5/issues/13872>

- \(bu
PerlIO::via has been upgraded from version 0.14 to 0.15.
.Sp
A minor portability improvement has been made to the \s-1XS\s0 implementation.

- \(bu
Unicode::UCD has been upgraded from version 0.57 to 0.58.
.Sp
The documentation includes many clarifications and fixes.

- \(bu
utf8 has been upgraded from version 1.13 to 1.13_01.
.Sp
The documentation has some minor formatting improvements.

- \(bu
version has been upgraded from version 0.9908 to 0.9909.
.Sp
External libraries and Perl may have different ideas of what the locale is.
This is problematic when parsing version strings if the locale's numeric
separator has been changed.  Version parsing has been patched to ensure it
handles the locales correctly.
[\s-1GH\s0 #13863] <https://github.com/Perl/perl5/issues/13863>

## Documentation

Header "Documentation"

### Changes to Existing Documentation

Subsection "Changes to Existing Documentation"
*perlapi*
Subsection "perlapi"

- \(bu
\f(CW\*(C`av_len\*(C' - Emphasize that this returns the highest index in the array, not the
size of the array.
[\s-1GH\s0 #13377] <https://github.com/Perl/perl5/issues/13377>

- \(bu
Note that \f(CW\*(C`SvSetSV\*(C' doesn't do set magic.

- \(bu
\f(CW\*(C`sv_usepvn_flags\*(C' - Fix documentation to mention the use of \f(CW\*(C`NewX\*(C' instead of
\f(CW\*(C`malloc\*(C'.
[\s-1GH\s0 #13835] <https://github.com/Perl/perl5/issues/13835>

- \(bu
Clarify where \f(CW\*(C`NUL\*(C' may be embedded or is required to terminate a string.

*perlfunc*
Subsection "perlfunc"

- \(bu
Clarify the meaning of \f(CW\*(C`-B\*(C' and \f(CW\*(C`-T\*(C'.

- \(bu
\f(CW\*(C`-l\*(C' now notes that it will return false if symlinks aren't supported by the
file system.
[\s-1GH\s0 #13695] <https://github.com/Perl/perl5/issues/13695>

- \(bu
Note that \f(CW\*(C`each\*(C', \f(CW\*(C`keys\*(C' and \f(CW\*(C`values\*(C' may produce different orderings for
tied hashes compared to other perl hashes.
[\s-1GH\s0 #13650] <https://github.com/Perl/perl5/issues/13650>

- \(bu
Note that \f(CW\*(C`exec LIST\*(C' and \f(CW\*(C`system LIST\*(C' may fall back to the shell on Win32.
Only \f(CW\*(C`exec PROGRAM LIST\*(C' and \f(CW\*(C`system PROGRAM LIST\*(C' indirect object syntax
will reliably avoid using the shell.  This has also been noted in perlport.
[\s-1GH\s0 #13907] <https://github.com/Perl/perl5/issues/13907>

- \(bu
Clarify the meaning of \f(CW\*(C`our\*(C'.
[\s-1GH\s0 #13938] <https://github.com/Perl/perl5/issues/13938>

*perlguts*
Subsection "perlguts"

- \(bu
Explain various ways of modifying an existing \s-1SV\s0's buffer.
[\s-1GH\s0 #12813] <https://github.com/Perl/perl5/issues/12813>

*perlpolicy*
Subsection "perlpolicy"

- \(bu
We now have a code of conduct for the *p5p* mailing list, as documented in
\*(L"\s-1STANDARDS OF CONDUCT\*(R"\s0 in perlpolicy.

*perlre*
Subsection "perlre"

- \(bu
The \f(CW\*(C`/x\*(C' modifier has been clarified to note that comments cannot be continued
onto the next line by escaping them.

*perlsyn*
Subsection "perlsyn"

- \(bu
Mention the use of empty conditionals in \f(CW\*(C`for\*(C'/\f(CW\*(C`while\*(C' loops for infinite
loops.

*perlxs*
Subsection "perlxs"

- \(bu
Added a discussion of locale issues in \s-1XS\s0 code.

## Diagnostics

Header "Diagnostics"
The following additions or changes have been made to diagnostic output,
including warnings and fatal error messages.  For the complete list of
diagnostic messages, see perldiag.

### Changes to Existing Diagnostics

Subsection "Changes to Existing Diagnostics"

- \(bu
Variable length lookbehind not implemented in regex m/%s/
.Sp
Information about Unicode behaviour has been added.

## Configuration and Compilation

Header "Configuration and Compilation"

- \(bu
Building Perl no longer writes to the source tree when configured with
*Configure*'s **-Dmksymlinks** option.
[\s-1GH\s0 #13712] <https://github.com/Perl/perl5/issues/13712>

## Platform Support

Header "Platform Support"

### Platform-Specific Notes

Subsection "Platform-Specific Notes"

- Android
Item "Android"
Build support has been improved for cross-compiling in general and for Android
in particular.

- OpenBSD
Item "OpenBSD"
Corrected architectures and version numbers used in configuration hints when
building Perl.

- Solaris
Item "Solaris"
**c99** options have been cleaned up, hints look for **solstudio** as well as
**SUNWspro**, and support for native \f(CW\*(C`setenv\*(C' has been added.

- \s-1VMS\s0
Item "VMS"
An old bug in feature checking, mainly affecting pre-7.3 systems, has been
fixed.

- Windows
Item "Windows"
\f(CW%I64d is now being used instead of \f(CW%lld for MinGW.

## Internal Changes

Header "Internal Changes"

- \(bu
Added \*(L"sync_locale\*(R" in perlapi.
Changing the program's locale should be avoided by \s-1XS\s0 code.  Nevertheless,
certain non-Perl libraries called from \s-1XS,\s0 such as \f(CW\*(C`Gtk\*(C' do so.  When this
happens, Perl needs to be told that the locale has changed.  Use this function
to do so, before returning to Perl.

## Selected Bug Fixes

Header "Selected Bug Fixes"

- \(bu
A bug has been fixed where zero-length assertions and code blocks inside of a
regex could cause \f(CW\*(C`pos\*(C' to see an incorrect value.
[\s-1GH\s0 #14016] <https://github.com/Perl/perl5/issues/14016>

- \(bu
Using \f(CW\*(C`s///e\*(C' on tainted utf8 strings could issue bogus \*(L"Malformed \s-1UTF-8\s0
character (unexpected end of string)\*(R" warnings.  This has now been fixed.
[\s-1GH\s0 #13948] <https://github.com/Perl/perl5/issues/13948>

- \(bu
\f(CW\*(C`system\*(C' and friends should now work properly on more Android builds.
.Sp
Due to an oversight, the value specified through **-Dtargetsh** to *Configure*
would end up being ignored by some of the build process.  This caused perls
cross-compiled for Android to end up with defective versions of \f(CW\*(C`system\*(C',
\f(CW\*(C`exec\*(C' and backticks: the commands would end up looking for */bin/sh* instead
of */system/bin/sh*, and so would fail for the vast majority of devices,
leaving \f(CW$! as \f(CW\*(C`ENOENT\*(C'.

- \(bu
Many issues have been detected by Coverity <http://www.coverity.com/> and
fixed.

## Acknowledgements

Header "Acknowledgements"
Perl 5.20.1 represents approximately 4 months of development since Perl 5.20.0
and contains approximately 12,000 lines of changes across 170 files from 36
authors.

Excluding auto-generated files, documentation and release tools, there were
approximately 2,600 lines of changes to 110 .pm, .t, .c and .h files.

Perl continues to flourish into its third decade thanks to a vibrant community
of users and developers.  The following people are known to have contributed
the improvements that became Perl 5.20.1:

Aaron Crane, Abigail, Alberto Simo\*~es, Alexandr Ciornii, Alexandre (Midnite)
Jousset, Andrew Fresh, Andy Dougherty, Brian Fraser, Chris 'BinGOs' Williams,
Craig A. Berry, Daniel Dragan, David Golden, David Mitchell, H.Merijn Brand,
James E Keenan, Jan Dubois, Jarkko Hietaniemi, John Peacock, kafka, Karen
Etheridge, Karl Williamson, Lukas Mai, Matthew Horsfall, Michael Bunk, Peter
Martini, Rafael Garcia-Suarez, Reini Urban, Ricardo Signes, Shirakata Kentaro,
Smylers, Steve Hay, Thomas Sibley, Todd Rinaldo, Tony Cook, Vladimir Marek,
Yves Orton.

The list above is almost certainly incomplete as it is automatically generated
from version control history.  In particular, it does not include the names of
the (very much appreciated) contributors who reported issues to the Perl bug
tracker.

Many of the changes included in this version originated in the \s-1CPAN\s0 modules
included in Perl's core.  We're grateful to the entire \s-1CPAN\s0 community for
helping Perl to flourish.

For a more complete list of all of Perl's historical contributors, please see
the *\s-1AUTHORS\s0* file in the Perl source distribution.

## Reporting Bugs

Header "Reporting Bugs"
If you find what you think is a bug, you might check the articles recently
posted to the comp.lang.perl.misc newsgroup and the perl bug database at
https://rt.perl.org/ .  There may also be information at http://www.perl.org/ ,
the Perl Home Page.

If you believe you have an unreported bug, please run the perlbug program
included with your release.  Be sure to trim your bug down to a tiny but
sufficient test case.  Your bug report, along with the output of \f(CW\*(C`perl -V\*(C',
will be sent off to perlbug@perl.org to be analysed by the Perl porting team.

If the bug you are reporting has security implications, which make it
inappropriate to send to a publicly archived mailing list, then please send it
to perl5-security-report@perl.org.  This points to a closed subscription
unarchived mailing list, which includes all the core committers, who will be
able to help assess the impact of issues, figure out a resolution, and help
co-ordinate the release of patches to mitigate or fix the problem across all
platforms on which Perl is supported.  Please only use this address for
security issues in the Perl core, not for modules independently distributed on
\s-1CPAN.\s0

## SEE ALSO

Header "SEE ALSO"
The *Changes* file for an explanation of how to view exhaustive details on
what changed.

The *\s-1INSTALL\s0* file for how to build Perl.

The *\s-1README\s0* file for general stuff.

The *Artistic* and *Copying* files for copyright information.
