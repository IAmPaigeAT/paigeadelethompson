+++
detected_package_version = "5.34.1"
operating_system = "macos"
manpage_name = "perlko"
author = "None Specified"
manpage_section = "1"
manpage_format = "troff"
description = "PerlX s-1XXX XX XX XXXXXs0! PerlX s-1XXs0 Practical Extraction and Report Languages-1XX XXX XXXX XX XX XXX XX XXXs0 Pathologically Eclectic Rubbish Listers-1XX XXX XXX. XX XXX XX XX XXXs0 PerlX s-1XXXX X XXX XXXX XXX XX XX XXXX.s0 PerlX s-1XXX..."
date = "2022-02-19"
operating_system_version = "15.3"
title = "perlko(1)"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLKO 1"
PERLKO 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"
X \s-1XXX XX XXX XX XXX XXXXXX XXX XXXXXX.
X XXX PODX XX X XXX POD XX\s0(*pod/perlpod.pod* \s-1XXX
XXXXX\s0)XX \s-1XXXX XXXX.\s0

## NAME

perlko - XXX Perl XXX

## DESCRIPTION

Header "DESCRIPTION"
PerlX \s-1XXX XX XX XXXXX\s0!

PerlX \s-1XX\s0 **'Practical Extraction and Report Language'**\s-1XX XXX XXXX
XX XX XXX XX XXX\s0 **'Pathologically Eclectic Rubbish Lister'**\s-1XX
XXX XXX. XX XXX XX XX XXX\s0 PerlX \s-1XXXX X XXX
XXXX XXX XX XX XXXX.\s0 PerlX \s-1XXX\s0 LarryX X \s-1XX XXX
XX XXXX XX XXX XX XXX XXX XXXXX. XXX XXX\s0
**'Perl'**X \s-1XX XXXX XXXX. XX XXX XX XX XXX XXXX
XX XXX XXXX.\s0 LarryX X X X \s-1XXXXX.\s0

\s-1XX\s0 pX \s-1XXXX XXX\s0 **'perl'**X X \s-1XXXX. PX XXXX XX XX\s0
**'Perl'**X \s-1XXX XXX X XXX\s0 **'perl'**\s-1XX\s0 pX \s-1XXXX XXX XXXX
XXXXX XXXXX XX X XXXX XXXX XXX X XXXXX.\s0

## PerlX XXX

Header "PerlX XXX"
PerlX \s-1XX XXX XXX XX XXXXX XXX XXX XXX X XX,
XXXX XXXXX, GUI XX XX XXX XX XXXX XX XXXX
XX XXXXX XXXXX.\s0

X \s-1XXX XXXX\s0(\s-1XX XX, XXXX, XX XX\s0)XX
\s-1XXX\s0(\s-1XXXX XX, XXXXX, XXX XXX\s0)X \s-1XX XXXX XXXX.
XXXX XX, XXX XXXXXX XX XX XXXXXX XX XXXX,
XXX XXX XX XXX XXXX, XXXX XX XXXX X 3XX XX
XXXX XXX XXX XX\s0 PerlX \s-1XX XXX XXXXX.\s0

PerlX \s-1XXX XXX\s0 *pod/perlintro.pod* \s-1XXXX XXXXX.\s0

\s-1XX XXXXX XX XXX XXX\s0 *pod/perldelta.pod*\s-1XX XXXXX.\s0

\s-1XX XXX XXXX XXX XX\s0 Perl \s-1XX XXX XXX XXX XXXX.
XXX XXX\s0 *pod/perlbook.pod* \s-1XXX XXXXX.\s0

## XX

Header "XX"
\s-1XXXX XXX XXX XXXXX XXXX XX XX XXX\s0 PerlX
\s-1XXXXX XXXX XXX XX XXX XXXXX.\s0

.Vb 3
    ./Configure -des -Dprefix=$HOME/localperl
    make test
    make install
.Ve

\s-1XX XXX XXXX XXXX XX XXX XXXX XXXX XXX X,
XX XXXX XXXX, X XXXX XXX\s0 *localperl* \s-1XXXXX\s0 perlX
\s-1XXXXX.\s0

\s-1XXXX XXX XXX XX XXX XXX XX XX\s0 PerlX \s-1XXX XXX XXX
XX XXXX XXXX\s0 *\s-1INSTALL\s0* \s-1XX XX XXX XXX XXX XXX.
XXXXX XXXXX XX XXX XXXXX\s0 PerlX \s-1XXXX XXXX
XXX XX XXXX XXX XXXX XX XX\s0 *\s-1README\s0* \s-1XXX XXXX.\s0

\s-1XX\s0 PerlX \s-1XXXX XX\s0 \f(CW\*(C`perldoc\*(C' \s-1XXX XXX XXX XXX XXX
X XXXX. XXXX XXX XX XXX XXXXX.\s0

.Vb 1
    perldoc perl
.Ve

## XXX XXXX XXXX

Header "XXX XXXX XXXX"
PerlX \s-1XXXXX XX XX XXXX XX XXXX XXX X XX XX
XXX XXXXXX. XXXX XXXX XXXXX X XXX XX XX
XXX XXXX XXXX XXXX. XXX XX XXXXXX XXX
XXXXX\s0 \f(CW\*(C`perlbug\*(C' \s-1XXX XXXX XXXX XXX XXXXXX.\s0
\f(CW\*(C`perlbug\*(C'X \s-1XX X XXX XXX\s0 \f(CW\*(C`perldoc perlbug\*(C' \s-1XX\s0 \f(CW\*(C`perlbug\*(C'X
\s-1XXXXX XXXX XXX X XXXX.\s0

PerlX \s-1XX XXXX XXXX XXXX\s0 PerlX \s-1XXXX XXXX XXX
XXXX XXXX XXX XXXXX XXXX XXXXX XXXXX
XXX XXX XXX X X XX XXX XX X XXXX.\s0

\s-1XXXX XX XX XXX\s0 perlX \s-1CPAN\s0 (Comprehensive Perl Archive Network)
\s-1XXX\s0 <http://www.cpan.org/src/> \s-1XX XX X XXXX.\s0

perl \s-1XXX XXX XXX XXXX XXX\s0 *pod/perlhack.pod* \s-1XXX\s0
**\*(L"\s-1SUPER QUICK PATCH GUIDE\*(R"\s0**X \s-1XXXXX.\s0

\s-1XX XXXXX XXXXX.
XX XXXX XX XXX XXXX XX XXXX XXX XXXX.
XXX X XXXX\s0 **\*(L"\s-1XX\s0(Author)\*(R"**X \s-1XXXXX XXXXX.
XXX XXXX XXX XXX XXXX\s0 **\*(L"\s-1XX\s0(Authorship)\*(R"**X
\s-1XX XXX XXXX X XX XXXX. XXX XXXX XXXX\s0
PerlX \s-1XXXXXX XXX XXXX. :-\s0)

- **\*(L"\s-1XX\*(R"\s0**\s-1XXX.\s0

## XXX

Header "XXX"
PerlX 5.8.0XXX \s-1XXXX/ISO 10646X XX XXXXX XXXXX.
XXXX XXX XXXX XXXX XXX XX XXXX
XXXX XXX XX XXX XXX XX XXX XX XXX XXXX
XXXXX. XXXXX X XXXX XXX XX XXX XX
XX XX\s0(\s-1XXX XX XXX, XX XXX, XXX XXX, XXX XX XXXX
XXX XX XXXX, XX XX, XXX XX, XXXX XX, XXXX XX,
XXXX XX, XX XXXX XX XX X\s0)X \s-1XXXX XX XXX XX
XX XXX XXX XXX\s0  X \s-1XX X XX XXX XX XXX XXX
XX XXX XXXX X X XX XX XXX XXXX\s0  \s-1XX XX XXXX
XXXX XX XX XX XX XXX\s0  \s-1XXXX XXXX.\s0

PerlX \s-1XXXXX XXXXX XX XXX XX XXXXX.
XX XXXXX XXX\s0 Perl \s-1XXXX XXX\s0  \s-1UTF-8 XXXX X X XX,
XX XXX XXX\s0(\s-1XX XX, XXX,\s0 index, substr)X \s-1XXX XX
XX XXXX XX XXX XXXXX.
X XXX XX\s0 *pod/perlunicode.pod* \s-1XXX XXXXX.
XXXXX XX XXXX XX XX XXX XXX, XXX XX XXX XX
XX/X XXX XXXXX XXXX XX XX XXXXX X XXXX XXX
XXX XX XX XX\s0 Encode \s-1XXX XXX XXXX.
XXXX\s0 Encode \s-1XXX XXXX XXX XXX XXX XXX XX X X XXXX.\s0

### Encode \s-1XX\s0

Subsection "Encode XX"
*\s-1XX XXX\s0*
Subsection "XX XXX"

Encode \s-1XXX XXX XX XXX XXXX XXXXX.\s0

- \(bu
\f(CW\*(C`euc-kr\*(C'
.Sp
US-ASCIIX \s-1KS X 1001X XX XX XXXXX XXXXX XX
XXXXXX XX. KS X 2901X RFC 1557 XX.\s0

- \(bu
\f(CW\*(C`cp949\*(C'
.Sp
MS-Windows 9x/MEXX \s-1XXX XX XXX.\s0 euc-krX 8,822XX
\s-1XX XXX XX XX.\s0 aliasX uhc, windows-949, x-windows-949,
ks_c_5601-1987. X \s-1XXX XXX XXXX XX XXXXX,\s0 Microsoft
\s-1XXXX CP949X XXX XXX XX.\s0

- \(bu
\f(CW\*(C`johab\*(C'
.Sp
\s-1KS X 1001:1998 XX 3XX XXX XXX. XX XXXXX\s0 cp949X \s-1XXXXX\s0
US-ASCIIX  \s-1KS X 1001X 8,822XX XX XXX XX XXX XXX XXX XX XX.\s0

- \(bu
\f(CW\*(C`iso-2022-kr\*(C'
.Sp
\s-1RFC 1557XX XXX XXX XXX XX XXX XXXXX\s0 US-ASCIIX
\s-1KS X 1001X XXXXX XX XXX\s0 euc-krX \s-1XXX XXX XXX XX.
1997-8X XXX XXXX X XX XX XXX XXX XX.\s0

- \(bu
\f(CW\*(C`ksc5601-raw\*(C'
.Sp
\s-1KS X 1001\s0(\s-1KS C 5601\s0)X \s-1GL\s0(X, \s-1MSBX 0XX X XX\s0)X \s-1XXX XX XXX.\s0
US-ASCIIX \s-1XXXX XX XXXX XXX XX X11 XXX XX
XXX\s0(ksc5601.1987-0. '0'X \s-1GLX XXX\s0)XX \s-1XXX XX XXXXX
XX XX. KS C 5601X 1997X KS X 1001X XXX XXXX. 1998XXX X
XX\s0(\s-1XXX XXX XX XX XX\s0)X \s-1XXXX.\s0

*\s-1XX XX\s0*
Subsection "XX XX"

\s-1XX XX,\s0 euc-kr \s-1XXXXX X XXX UTF-8X XXXXX
XXXXX XXXX XXXXX.\s0

.Vb 1
    perl -Mencoding=euc-kr,STDOUT,utf8 -pe1 < file.euc-kr > file.utf8
.Ve

\s-1XXX XXX XX XXXX XXXXX.\s0

.Vb 1
    perl -Mencoding=utf8,STDOUT,euc-kr -pe1 < file.utf8 > file.euc-kr
.Ve

\s-1XX XXX XX XXXX X X XXX XXXX\s0 *piconv*X PerlX
\s-1XXXX XXXXXX. X XXXXX\s0 Encode \s-1XXX XXX XX\s0 Perl
\s-1XXXXX XXXX X X XXX\s0 UnixX \f(CW\*(C`iconv\*(C'X \s-1XXX X XXXX.
XXXX XXX XXXX.\s0

.Vb 2
   piconv -f euc-kr -t utf8 < file.euc-kr > file.utf8
   piconv -f utf8 -t euc-kr < file.utf8 > file.euc-kr
.Ve

*\s-1XX XX\s0*
Subsection "XX XX"

PerlX \s-1XXXXX XXXX UTF-8X XXXX\s0 Encode \s-1XXX XX
XXX XXXX XXXXX XX XX XXX XXXXX XXXX
XXX XXXX XXX X XX XXX XXXX XXX XX XXXXX.\s0

- \(bu
\s-1XX XXX XX UTF-8 XXXXX XX\s0

- \(bu
\s-1XX XX XXX\s0 \f(CW\*(C`use utf8;\*(C' \s-1XXXX XX\s0

- \(bu
\s-1XX XX, XXX, XXXX, XXX XXXX XXXX XX\s0

- \(bu
\s-1XXX XX XXX XXXX XXXX XX\s0

- \(bu
\s-1XX\s0(double) \s-1XXXX XX\s0

*\s-1XXXX X XXX XXX XX XX\s0*
Subsection "XXXX X XXX XXX XX XX"

- \(bu
perluniintro

- \(bu
perlunicode

- \(bu
Encode

- \(bu
Encode::KR

- \(bu
encoding

- \(bu
<https://www.unicode.org/>
.Sp
\s-1XXXX XXXX\s0

- \(bu
<https://std.dkuug.dk/JTC1/SC2/WG2>
.Sp
\s-1XXXXX\s0 UnicodeX \s-1XX ISO XXX\s0  \s-1ISO/IEC 10646 UCS\s0(Universal
Character Set)X \s-1XXX ISO/IEC JTC1/SC2/WG2X X XXX\s0

- \(bu
<https://www.cl.cam.ac.uk/~mgk25/unicode.html>
.Sp
\s-1XXX/XXX XXXX XX UTF-8 X XXXX XX FAQ\s0

- \(bu
<http://wiki.kldp.org/Translations/html/UTF8-Unicode-KLDP/UTF8-Unicode-KLDP.html>
.Sp
\s-1XXX/XXX XXXX XX UTF-8 X XXXX XX FAQX XXX XX\s0

## Perl XX XX

Header "Perl XX XX"
\s-1XXX XXXX\s0 Perl \s-1XX XXX XXXXX.\s0

- \(bu
<https://www.perl.org/>
.Sp
Perl \s-1XX XXXX\s0

- \(bu
<https://www.perl.com/>
.Sp
O'ReillyX Perl X \s-1XXX\s0

- \(bu
<https://www.cpan.org/>
.Sp
\s-1CPAN\s0 - Comprehensive Perl Archive Network, \s-1XXX\s0 Perl \s-1XX XX XXXX\s0

- \(bu
<https://metacpan.org>
.Sp
\s-1XX CPAN\s0

- \(bu
<https://lists.perl.org/>
.Sp
Perl \s-1XXX XXX\s0

- \(bu
<https://blogs.perl.org/>
.Sp
Perl \s-1XX XXX\s0

- \(bu
<https://www.perlmonks.org/>
.Sp
Perl \s-1XXXXX XX XXX\s0

- \(bu
<https://www.pm.org/groups/asia.html>
.Sp
\s-1XXX XX\s0 Perl \s-1XXX XX\s0

- \(bu
<http://www.perladvent.org/>
.Sp
Perl \s-1XXXXX XX\s0

\s-1XXX\s0 PerlX X \s-1XX XXXXX XXX X X XX XXX XX XXXXXX.\s0

- \(bu
<https://perl.kr/>
.Sp
\s-1XX\s0 Perl \s-1XXXX XX XX\s0

- \(bu
<https://doc.perl.kr/>
.Sp
Perl \s-1XX XXX XXXX\s0

- \(bu
<https://cafe.naver.com/perlstudy.cafe>
.Sp
\s-1XXX\s0 Perl \s-1XX\s0

- \(bu
<http://www.perl.or.kr/>
.Sp
\s-1XX\s0 Perl \s-1XXX XX\s0

- \(bu
<https://advent.perl.kr>
.Sp
Seoul.pm Perl \s-1XXXXX XX\s0 (2010 ~ 2012)

- \(bu
<http://gypark.pe.kr/wiki/Perl>
.Sp
\s-1GYPARK\s0(Geunyoung Park)X Perl \s-1XX XX XX XXX\s0

## XXXX

Header "XXXX"
*\s-1README\s0* \s-1XXX\s0 **'\s-1LICENSING\s0'** \s-1XXX XXXXX.\s0

## AUTHORS

Header "AUTHORS"

- \(bu
Jarkko Hietaniemi <jhi@iki.fi>

- \(bu
\s-1XXX\s0 <jshin@mailaps.org>

- \(bu
\s-1XXX\s0 <keedi@cpan.org>
