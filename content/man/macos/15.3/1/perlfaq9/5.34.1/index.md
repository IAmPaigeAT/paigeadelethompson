+++
description = "This section deals with questions related to running web sites, sending and receiving email as well as general networking. Yes. If you are building a web site with any level of interactivity (forms / users / databases), you will want to use a fram..."
detected_package_version = "5.34.1"
author = "None Specified"
operating_system_version = "15.3"
title = "perlfaq9(1)"
manpage_section = "1"
manpage_format = "troff"
manpage_name = "perlfaq9"
date = "2022-02-19"
operating_system = "macos"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLFAQ9 1"
PERLFAQ9 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlfaq9 - Web, Email and Networking

## VERSION

Header "VERSION"
version 5.20210411

## DESCRIPTION

Header "DESCRIPTION"
This section deals with questions related to running web sites,
sending and receiving email as well as general networking.

### Should I use a web framework?

Subsection "Should I use a web framework?"
Yes. If you are building a web site with any level of interactivity
(forms / users / databases), you
will want to use a framework to make handling requests
and responses easier.

If there is no interactivity then you may still want
to look at using something like Template Toolkit <https://metacpan.org/module/Template>
or Plack::Middleware::TemplateToolkit
so maintenance of your \s-1HTML\s0 files (and other assets) is easier.

### Which web framework should I use?

Xref "framework CGI.pm CGI Catalyst Dancer"
Subsection "Which web framework should I use?"
There is no simple answer to this question. Perl frameworks can run everything
from basic file servers and small scale intranets to massive multinational
multilingual websites that are the core to international businesses.

Below is a list of a few frameworks with comments which might help you in
making a decision, depending on your specific requirements. Start by reading
the docs, then ask questions on the relevant mailing list or \s-1IRC\s0 channel.

- Catalyst
Item "Catalyst"
Strongly object-oriented and fully-featured with a long development history and
a large community and addon ecosystem. It is excellent for large and complex
applications, where you have full control over the server.

- Dancer2
Item "Dancer2"
Free of legacy weight, providing a lightweight and easy to learn \s-1API.\s0
Has a growing addon ecosystem. It is best used for smaller projects and
very easy to learn for beginners.

- Mojolicious
Item "Mojolicious"
Self-contained and powerful for both small and larger projects,
with a focus on \s-1HTML5\s0 and real-time web technologies such as WebSockets.

- Web::Simple
Item "Web::Simple"
Strongly object-oriented and minimal, built for speed and intended
as a toolkit for building micro web apps, custom frameworks or for tieing
together existing Plack-compatible web applications with one central dispatcher.

All of these interact with or use Plack which is worth understanding
the basics of when building a website in Perl (there is a lot of useful
Plack::Middleware <https://metacpan.org/search?q=plack%3A%3Amiddleware>).

### What is Plack and \s-1PSGI\s0?

Subsection "What is Plack and PSGI?"
\s-1PSGI\s0 is the Perl Web Server Gateway Interface Specification, it is
a standard that many Perl web frameworks use, you should not need to
understand it to build a web site, the part you might want to use is Plack.

Plack is a set of tools for using the \s-1PSGI\s0 stack. It contains
middleware <https://metacpan.org/search?q=plack%3A%3Amiddleware>
components, a reference server and utilities for Web application frameworks.
Plack is like Ruby's Rack or Python's Paste for \s-1WSGI.\s0

You could build a web site using Plack and your own code,
but for anything other than a very basic web site, using a web framework
(that uses <https://plackperl.org>) is a better option.

### How do I remove \s-1HTML\s0 from a string?

Subsection "How do I remove HTML from a string?"
Use HTML::Strip, or HTML::FormatText which not only removes \s-1HTML\s0
but also attempts to do a little simple formatting of the resulting
plain text.

### How do I extract URLs?

Subsection "How do I extract URLs?"
HTML::SimpleLinkExtor will extract URLs from \s-1HTML,\s0 it handles anchors,
images, objects, frames, and many other tags that can contain a \s-1URL.\s0
If you need anything more complex, you can create your own subclass of
HTML::LinkExtor or HTML::Parser. You might even use
HTML::SimpleLinkExtor as an example for something specifically
suited to your needs.

You can use URI::Find or URL::Search to extract URLs from an
arbitrary text document.

### How do I fetch an \s-1HTML\s0 file?

Subsection "How do I fetch an HTML file?"
(contributed by brian d foy)

The core HTTP::Tiny module can fetch web resources and give their
content back to you as a string:

.Vb 1
    use HTTP::Tiny;

    my $ua = HTTP::Tiny->new;
    my $html = $ua->get( "http://www.example.com/index.html" )->\{content\};
.Ve

It can also store the resource directly in a file:

.Vb 1
    $ua->mirror( "http://www.example.com/index.html", "foo.html" );
.Ve

If you need to do something more complicated, the HTTP::Tiny object can
be customized by setting attributes, or you can use LWP::UserAgent from
the libwww-perl distribution or Mojo::UserAgent from the Mojolicious
distribution to make common tasks easier. If you want to simulate an
interactive web browser, you can use the WWW::Mechanize module.

### How do I automate an \s-1HTML\s0 form submission?

Subsection "How do I automate an HTML form submission?"
If you are doing something complex, such as moving through many pages
and forms or a web site, you can use WWW::Mechanize. See its
documentation for all the details.

If you're submitting values using the \s-1GET\s0 method, create a \s-1URL\s0 and encode
the form using the \f(CW\*(C`www_form_urlencode\*(C' method from HTTP::Tiny:

.Vb 1
    use HTTP::Tiny;

    my $ua = HTTP::Tiny->new;

    my $query = $ua->www_form_urlencode([ q => \*(AqDB_File\*(Aq, lucky => 1 ]);
    my $url = "https://metacpan.org/search?$query";
    my $content = $ua->get($url)->\{content\};
.Ve

If you're using the \s-1POST\s0 method, the \f(CW\*(C`post_form\*(C' method will encode the
content appropriately.

.Vb 1
    use HTTP::Tiny;

    my $ua = HTTP::Tiny->new;

    my $url = \*(Aqhttps://metacpan.org/search\*(Aq;
    my $form = [ q => \*(AqDB_File\*(Aq, lucky => 1 ];
    my $content = $ua->post_form($url, $form)->\{content\};
.Ve

### How do I decode or create those %-encodings on the web?

Xref "URI URI::Escape RFC 2396"
Subsection "How do I decode or create those %-encodings on the web?"
Most of the time you should not need to do this as
your web framework, or if you are making a request,
the \s-1LWP\s0 or other module would handle it for you.

To encode a string yourself, use the URI::Escape module. The \f(CW\*(C`uri_escape\*(C'
function returns the escaped string:

.Vb 1
    my $original = "Colon : Hash # Percent %";

    my $escaped = uri_escape( $original );

    print "$escaped\\n"; # \*(AqColon%20%3A%20Hash%20%23%20Percent%20%25\*(Aq
.Ve

To decode the string, use the \f(CW\*(C`uri_unescape\*(C' function:

.Vb 1
    my $unescaped = uri_unescape( $escaped );

    print $unescaped; # back to original
.Ve

Remember not to encode a full \s-1URI,\s0 you need to escape each
component separately and then join them together.

### How do I redirect to another page?

Subsection "How do I redirect to another page?"
Most Perl Web Frameworks will have a mechanism for doing this,
using the Catalyst framework it would be:

.Vb 2
    $c->res->redirect($url);
    $c->detach();
.Ve

If you are using Plack (which most frameworks do), then
Plack::Middleware::Rewrite is worth looking at if you
are migrating from Apache or have \s-1URL\s0's you want to always
redirect.

### How do I put a password on my web pages?

Subsection "How do I put a password on my web pages?"
See if the web framework you are using has an
authentication system and if that fits your needs.

Alternativly look at Plack::Middleware::Auth::Basic,
or one of the other Plack authentication <https://metacpan.org/search?q=plack+auth>
options.

### How do I make sure users cant enter values into a form that causes my \s-1CGI\s0 script to do bad things?

Subsection "How do I make sure users can't enter values into a form that causes my CGI script to do bad things?"
(contributed by brian d foy)

You can't prevent people from sending your script bad data. Even if
you add some client-side checks, people may disable them or bypass
them completely. For instance, someone might use a module such as
\s-1LWP\s0 to submit to your web site. If you want to prevent data that
try to use \s-1SQL\s0 injection or other sorts of attacks (and you should
want to), you have to not trust any data that enter your program.

The perlsec documentation has general advice about data security.
If you are using the \s-1DBI\s0 module, use placeholder to fill in data.
If you are running external programs with \f(CW\*(C`system\*(C' or \f(CW\*(C`exec\*(C', use
the list forms. There are many other precautions that you should take,
too many to list here, and most of them fall under the category of not
using any data that you don't intend to use. Trust no one.

### How do I parse a mail header?

Subsection "How do I parse a mail header?"
Use the Email::MIME module. It's well-tested and supports all the
craziness that you'll see in the real world (comment-folding whitespace,
encodings, comments, etc.).

.Vb 1
  use Email::MIME;

  my $message = Email::MIME->new($rfc2822);
  my $subject = $message->header(\*(AqSubject\*(Aq);
  my $from    = $message->header(\*(AqFrom\*(Aq);
.Ve

If you've already got some other kind of email object, consider passing
it to Email::Abstract and then using its cast method to get an
Email::MIME object:

.Vb 2
  my $abstract = Email::Abstract->new($mail_message_object);
  my $email_mime_object = $abstract->cast(\*(AqEmail::MIME\*(Aq);
.Ve

### How do I check a valid mail address?

Subsection "How do I check a valid mail address?"
(partly contributed by Aaron Sherman)

This isn't as simple a question as it sounds. There are two parts:

a) How do I verify that an email address is correctly formatted?

b) How do I verify that an email address targets a valid recipient?

Without sending mail to the address and seeing whether there's a human
on the other end to answer you, you cannot fully answer part *b*, but
the Email::Valid module will do both part *a* and part *b* as far
as you can in real-time.

Our best advice for verifying a person's mail address is to have them
enter their address twice, just as you normally do to change a
password. This usually weeds out typos. If both versions match, send
mail to that address with a personal message. If you get the message
back and they've followed your directions, you can be reasonably
assured that it's real.

A related strategy that's less open to forgery is to give them a \s-1PIN\s0
(personal \s-1ID\s0 number). Record the address and \s-1PIN\s0 (best that it be a
random one) for later processing. In the mail you send, include a link to
your site with the \s-1PIN\s0 included. If the mail bounces, you know it's not
valid. If they don't click on the link, either they forged the address or
(assuming they got the message) following through wasn't important so you
don't need to worry about it.

### How do I decode a \s-1MIME/BASE64\s0 string?

Subsection "How do I decode a MIME/BASE64 string?"
The MIME::Base64 package handles this as well as the \s-1MIME/QP\s0 encoding.
Decoding base 64 becomes as simple as:

.Vb 2
    use MIME::Base64;
    my $decoded = decode_base64($encoded);
.Ve

The Email::MIME module can decode base 64-encoded email message parts
transparently so the developer doesn't need to worry about it.

### How do I find the users mail address?

Subsection "How do I find the user's mail address?"
Ask them for it. There are so many email providers available that it's
unlikely the local system has any idea how to determine a user's email address.

The exception is for organization-specific email (e.g. foo@yourcompany.com)
where policy can be codified in your program. In that case, you could look at
\f(CW$ENV\{\s-1USER\s0\}, \f(CW$ENV\{\s-1LOGNAME\s0\}, and getpwuid($<) in scalar context, like so:

.Vb 1
  my $user_name = getpwuid($<)
.Ve

But you still cannot make assumptions about whether this is correct, unless
your policy says it is. You really are best off asking the user.

### How do I send email?

Subsection "How do I send email?"
Use the Email::Stuffer module, like so:

.Vb 5
  # first, create your message
  my $message = Email::Stuffer->from(\*(Aqyou@example.com\*(Aq)
                              ->to(\*(Aqfriend@example.com\*(Aq)
                              ->subject(\*(AqHappy birthday!\*(Aq)
                              ->text_body("Happy birthday to you!\\n");

  $message->send_or_die;
.Ve

By default, Email::Sender::Simple (the \f(CW\*(C`send\*(C' and \f(CW\*(C`send_or_die\*(C' methods
use this under the hood) will try \f(CW\*(C`sendmail\*(C' first, if it exists
in your \f(CW$PATH. This generally isn't the case. If there's a remote mail
server you use to send mail, consider investigating one of the Transport
classes. At time of writing, the available transports include:

- Email::Sender::Transport::Sendmail
Item "Email::Sender::Transport::Sendmail"
This is the default. If you can use the **mail**\|(1) or **mailx**\|(1)
program to send mail from the machine where your code runs, you should
be able to use this.

- Email::Sender::Transport::SMTP
Item "Email::Sender::Transport::SMTP"
This transport contacts a remote \s-1SMTP\s0 server over \s-1TCP.\s0 It optionally
uses \s-1TLS\s0 or \s-1SSL\s0 and can authenticate to the server via \s-1SASL.\s0

Telling Email::Stuffer to use your transport is straightforward.

.Vb 1
  $message->transport($email_sender_transport_object)->send_or_die;
.Ve

### How do I use \s-1MIME\s0 to make an attachment to a mail message?

Subsection "How do I use MIME to make an attachment to a mail message?"
Email::MIME directly supports multipart messages. Email::MIME
objects themselves are parts and can be attached to other Email::MIME
objects. Consult the Email::MIME documentation for more information,
including all of the supported methods and examples of their use.

Email::Stuffer uses Email::MIME under the hood to construct
messages, and wraps the most common attachment tasks with the simple
\f(CW\*(C`attach\*(C' and \f(CW\*(C`attach_file\*(C' methods.

.Vb 4
  Email::Stuffer->to(\*(Aqfriend@example.com\*(Aq)
                ->subject(\*(AqThe file\*(Aq)
                ->attach_file(\*(Aqstuff.csv\*(Aq)
                ->send_or_die;
.Ve

### How do I read email?

Subsection "How do I read email?"
Use the Email::Folder module, like so:

.Vb 1
  use Email::Folder;

  my $folder = Email::Folder->new(\*(Aq/path/to/email/folder\*(Aq);
  while(my $message = $folder->next_message) \{
    # next_message returns Email::Simple objects, but we want
    # Email::MIME objects as they\*(Aqre more robust
    my $mime = Email::MIME->new($message->as_string);
  \}
.Ve

There are different classes in the Email::Folder namespace for
supporting various mailbox types. Note that these modules are generally
rather limited and only support **reading** rather than writing.

### How do I find out my hostname, domainname, or \s-1IP\s0 address?

Xref "hostname, domainname, IP address, host, domain, hostfqdn, inet_ntoa, gethostbyname, Socket, Net::Domain, Sys::Hostname"
Subsection "How do I find out my hostname, domainname, or IP address?"
(contributed by brian d foy)

The Net::Domain module, which is part of the Standard Library starting
in Perl 5.7.3, can get you the fully qualified domain name (\s-1FQDN\s0), the host
name, or the domain name.

.Vb 1
    use Net::Domain qw(hostname hostfqdn hostdomain);

    my $host = hostfqdn();
.Ve

The Sys::Hostname module, part of the Standard Library, can also get the
hostname:

.Vb 1
    use Sys::Hostname;

    $host = hostname();
.Ve

The Sys::Hostname::Long module takes a different approach and tries
harder to return the fully qualified hostname:

.Vb 1
  use Sys::Hostname::Long \*(Aqhostname_long\*(Aq;

  my $hostname = hostname_long();
.Ve

To get the \s-1IP\s0 address, you can use the \f(CW\*(C`gethostbyname\*(C' built-in function
to turn the name into a number. To turn that number into the dotted octet
form (a.b.c.d) that most people expect, use the \f(CW\*(C`inet_ntoa\*(C' function
from the Socket module, which also comes with perl.

.Vb 1
    use Socket;

    my $address = inet_ntoa(
        scalar gethostbyname( $host || \*(Aqlocalhost\*(Aq )
    );
.Ve

### How do I fetch/put an (S)FTP file?

Subsection "How do I fetch/put an (S)FTP file?"
Net::FTP, and Net::SFTP allow you to interact with \s-1FTP\s0 and \s-1SFTP\s0 (Secure
\s-1FTP\s0) servers.

### How can I do \s-1RPC\s0 in Perl?

Subsection "How can I do RPC in Perl?"
Use one of the \s-1RPC\s0 modules( <https://metacpan.org/search?q=RPC> ).

## AUTHOR AND COPYRIGHT

Header "AUTHOR AND COPYRIGHT"
Copyright (c) 1997-2010 Tom Christiansen, Nathan Torkington, and
other authors as noted. All rights reserved.

This documentation is free; you can redistribute it and/or modify it
under the same terms as Perl itself.

Irrespective of its distribution, all code examples in this file
are hereby placed into the public domain. You are permitted and
encouraged to use this code in your own programs for fun
or for profit as you see fit. A simple comment in the code giving
credit would be courteous but is not required.
