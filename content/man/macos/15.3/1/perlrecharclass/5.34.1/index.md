+++
description = "The top level documentation about Perl regular expressions is found in perlre. This manual page discusses the syntax and use of character classes in Perl regular expressions. A character class is a way of denoting a set of characters in such a wa..."
title = "perlrecharclass(1)"
operating_system = "macos"
manpage_section = "1"
author = "None Specified"
manpage_format = "troff"
manpage_name = "perlrecharclass"
date = "2022-02-19"
operating_system_version = "15.3"
detected_package_version = "5.34.1"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "PERLRECHARCLASS 1"
PERLRECHARCLASS 1 "2022-02-19" "perl v5.34.1" "Perl Programmers Reference Guide"

## NAME

perlrecharclass - Perl Regular Expression Character Classes
Xref "character class"

## DESCRIPTION

Header "DESCRIPTION"
The top level documentation about Perl regular expressions
is found in perlre.

This manual page discusses the syntax and use of character
classes in Perl regular expressions.

A character class is a way of denoting a set of characters
in such a way that one character of the set is matched.
It's important to remember that: matching a character class
consumes exactly one character in the source string. (The source
string is the string the regular expression is matched against.)

There are three types of character classes in Perl regular
expressions: the dot, backslash sequences, and the form enclosed in square
brackets.  Keep in mind, though, that often the term \*(L"character class\*(R" is used
to mean just the bracketed form.  Certainly, most Perl documentation does that.

### The dot

Subsection "The dot"
The dot (or period), \f(CW\*(C`.\*(C' is probably the most used, and certainly
the most well-known character class. By default, a dot matches any
character, except for the newline. That default can be changed to
add matching the newline by using the *single line* modifier:
for the entire regular expression with the \f(CW\*(C`/s\*(C' modifier, or
locally with \f(CW\*(C`(?s)\*(C'  (and even globally within the scope of
\f(CW\*(C`use re \*(Aq/s\*(Aq\*(C').  (The \f(CW"\\N" backslash
sequence, described
below, matches any character except newline without regard to the
*single line* modifier.)

Here are some examples:

.Vb 7
 "a"  =~  /./       # Match
 "."  =~  /./       # Match
 ""   =~  /./       # No match (dot has to match a character)
 "\\n" =~  /./       # No match (dot does not match a newline)
 "\\n" =~  /./s      # Match (global \*(Aqsingle line\*(Aq modifier)
 "\\n" =~  /(?s:.)/  # Match (local \*(Aqsingle line\*(Aq modifier)
 "ab" =~  /^.$/     # No match (dot matches one character)
.Ve

### Backslash sequences

Xref "\w \W \s \S \d \D \p \P \N \v \V \h \H word whitespace"
Subsection "Backslash sequences"
A backslash sequence is a sequence of characters, the first one of which is a
backslash.  Perl ascribes special meaning to many such sequences, and some of
these are character classes.  That is, they match a single character each,
provided that the character belongs to the specific set of characters defined
by the sequence.

Here's a list of the backslash sequences that are character classes.  They
are discussed in more detail below.  (For the backslash sequences that aren't
character classes, see perlrebackslash.)

.Vb 10
 \\d             Match a decimal digit character.
 \\D             Match a non-decimal-digit character.
 \\w             Match a "word" character.
 \\W             Match a non-"word" character.
 \\s             Match a whitespace character.
 \\S             Match a non-whitespace character.
 \\h             Match a horizontal whitespace character.
 \\H             Match a character that isn\*(Aqt horizontal whitespace.
 \\v             Match a vertical whitespace character.
 \\V             Match a character that isn\*(Aqt vertical whitespace.
 \\N             Match a character that isn\*(Aqt a newline.
 \\pP, \\p\{Prop\}  Match a character that has the given Unicode property.
 \\PP, \\P\{Prop\}  Match a character that doesn\*(Aqt have the Unicode property
.Ve

*\\N*
Subsection "N"

\f(CW\*(C`\\N\*(C', available starting in v5.12, like the dot, matches any
character that is not a newline. The difference is that \f(CW\*(C`\\N\*(C' is not influenced
by the *single line* regular expression modifier (see \*(L"The dot\*(R" above).  Note
that the form \f(CW\*(C`\\N\{...\}\*(C' may mean something completely different.  When the
\f(CW\*(C`\{...\}\*(C' is a quantifier, it means to match a non-newline
character that many times.  For example, \f(CW\*(C`\\N\{3\}\*(C' means to match 3
non-newlines; \f(CW\*(C`\\N\{5,\}\*(C' means to match 5 or more non-newlines.  But if \f(CW\*(C`\{...\}\*(C'
is not a legal quantifier, it is presumed to be a named character.  See
charnames for those.  For example, none of \f(CW\*(C`\\N\{COLON\}\*(C', \f(CW\*(C`\\N\{4F\}\*(C', and
\f(CW\*(C`\\N\{F4\}\*(C' contain legal quantifiers, so Perl will try to find characters whose
names are respectively \f(CW\*(C`COLON\*(C', \f(CW\*(C`4F\*(C', and \f(CW\*(C`F4\*(C'.

*Digits*
Subsection "Digits"

\f(CW\*(C`\\d\*(C' matches a single character considered to be a decimal *digit*.
If the \f(CW\*(C`/a\*(C' regular expression modifier is in effect, it matches [0-9].
Otherwise, it
matches anything that is matched by \f(CW\*(C`\\p\{Digit\}\*(C', which includes [0-9].
(An unlikely possible exception is that under locale matching rules, the
current locale might not have \f(CW\*(C`[0-9]\*(C' matched by \f(CW\*(C`\\d\*(C', and/or might match
other characters whose code point is less than 256.  The only such locale
definitions that are legal would be to match \f(CW\*(C`[0-9]\*(C' plus another set of
10 consecutive digit characters;  anything else would be in violation of
the C language standard, but Perl doesn't currently assume anything in
regard to this.)

What this means is that unless the \f(CW\*(C`/a\*(C' modifier is in effect \f(CW\*(C`\\d\*(C' not
only matches the digits '0' - '9', but also Arabic, Devanagari, and
digits from other languages.  This may cause some confusion, and some
security issues.

Some digits that \f(CW\*(C`\\d\*(C' matches look like some of the [0-9] ones, but
have different values.  For example, \s-1BENGALI DIGIT FOUR\s0 (U+09EA) looks
very much like an \s-1ASCII DIGIT EIGHT\s0 (U+0038), and \s-1LEPCHA DIGIT SIX\s0
(U+1C46) looks very much like an \s-1ASCII DIGIT FIVE\s0 (U+0035).  An
application that
is expecting only the \s-1ASCII\s0 digits might be misled, or if the match is
\f(CW\*(C`\\d+\*(C', the matched string might contain a mixture of digits from
different writing systems that look like they signify a number different
than they actually do.  \*(L"**num()**\*(R" in Unicode::UCD can
be used to safely
calculate the value, returning \f(CW\*(C`undef\*(C' if the input string contains
such a mixture.  Otherwise, for example, a displayed price might be
deliberately different than it appears.

What \f(CW\*(C`\\p\{Digit\}\*(C' means (and hence \f(CW\*(C`\\d\*(C' except under the \f(CW\*(C`/a\*(C'
modifier) is \f(CW\*(C`\\p\{General_Category=Decimal_Number\}\*(C', or synonymously,
\f(CW\*(C`\\p\{General_Category=Digit\}\*(C'.  Starting with Unicode version 4.1, this
is the same set of characters matched by \f(CW\*(C`\\p\{Numeric_Type=Decimal\}\*(C'.
But Unicode also has a different property with a similar name,
\f(CW\*(C`\\p\{Numeric_Type=Digit\}\*(C', which matches a completely different set of
characters.  These characters are things such as \f(CW\*(C`CIRCLED DIGIT ONE\*(C'
or subscripts, or are from writing systems that lack all ten digits.

The design intent is for \f(CW\*(C`\\d\*(C' to exactly match the set of characters
that can safely be used with \*(L"normal\*(R" big-endian positional decimal
syntax, where, for example 123 means one 'hundred', plus two 'tens',
plus three 'ones'.  This positional notation does not necessarily apply
to characters that match the other type of \*(L"digit\*(R",
\f(CW\*(C`\\p\{Numeric_Type=Digit\}\*(C', and so \f(CW\*(C`\\d\*(C' doesn't match them.

The Tamil digits (U+0BE6 - U+0BEF) can also legally be
used in old-style Tamil numbers in which they would appear no more than
one in a row, separated by characters that mean \*(L"times 10\*(R", \*(L"times 100\*(R",
etc.  (See <https://www.unicode.org/notes/tn21>.)

Any character not matched by \f(CW\*(C`\\d\*(C' is matched by \f(CW\*(C`\\D\*(C'.

*Word characters*
Subsection "Word characters"

A \f(CW\*(C`\\w\*(C' matches a single alphanumeric character (an alphabetic character, or a
decimal digit); or a connecting punctuation character, such as an
underscore (\*(L"_\*(R"); or a \*(L"mark\*(R" character (like some sort of accent) that
attaches to one of those.  It does not match a whole word.  To match a
whole word, use \f(CW\*(C`\\w+\*(C'.  This isn't the same thing as matching an
English word, but in the \s-1ASCII\s0 range it is the same as a string of
Perl-identifier characters.
.ie n .IP "If the ""/a"" modifier is in effect ..." 4
.el .IP "If the \f(CW/a modifier is in effect ..." 4
Item "If the /a modifier is in effect ..."
\f(CW\*(C`\\w\*(C' matches the 63 characters [a-zA-Z0-9_].

- otherwise ...
Item "otherwise ..."

> 0

- For code points above 255 ...
Item "For code points above 255 ..."
.PD
\f(CW\*(C`\\w\*(C' matches the same as \f(CW\*(C`\\p\{Word\}\*(C' matches in this range.  That is,
it matches Thai letters, Greek letters, etc.  This includes connector
punctuation (like the underscore) which connect two words together, or
diacritics, such as a \f(CW\*(C`COMBINING TILDE\*(C' and the modifier letters, which
are generally used to add auxiliary markings to letters.

- For code points below 256 ...
Item "For code points below 256 ..."

> 0

- if locale rules are in effect ...
Item "if locale rules are in effect ..."
.PD
\f(CW\*(C`\\w\*(C' matches the platform's native underscore character plus whatever
the locale considers to be alphanumeric.

- if, instead, Unicode rules are in effect ...
Item "if, instead, Unicode rules are in effect ..."
\f(CW\*(C`\\w\*(C' matches exactly what \f(CW\*(C`\\p\{Word\}\*(C' matches.

- otherwise ...
Item "otherwise ..."
\f(CW\*(C`\\w\*(C' matches [a-zA-Z0-9_].



> 




> 


Which rules apply are determined as described in \*(L"Which character set modifier is in effect?\*(R" in perlre.

There are a number of security issues with the full Unicode list of word
characters.  See <http://unicode.org/reports/tr36>.

Also, for a somewhat finer-grained set of characters that are in programming
language identifiers beyond the \s-1ASCII\s0 range, you may wish to instead use the
more customized \*(L"Unicode Properties\*(R", \f(CW\*(C`\\p\{ID_Start\}\*(C',
\f(CW\*(C`\\p\{ID_Continue\}\*(C', \f(CW\*(C`\\p\{XID_Start\}\*(C', and \f(CW\*(C`\\p\{XID_Continue\}\*(C'.  See
<http://unicode.org/reports/tr31>.

Any character not matched by \f(CW\*(C`\\w\*(C' is matched by \f(CW\*(C`\\W\*(C'.

*Whitespace*
Subsection "Whitespace"

\f(CW\*(C`\\s\*(C' matches any single character considered whitespace.
.ie n .IP "If the ""/a"" modifier is in effect ..." 4
.el .IP "If the \f(CW/a modifier is in effect ..." 4
Item "If the /a modifier is in effect ..."
In all Perl versions, \f(CW\*(C`\\s\*(C' matches the 5 characters [\\t\\n\\f\\r ]; that
is, the horizontal tab,
the newline, the form feed, the carriage return, and the space.
Starting in Perl v5.18, it also matches the vertical tab, \f(CW\*(C`\\cK\*(C'.
See note \f(CW\*(C`[1]\*(C' below for a discussion of this.

- otherwise ...
Item "otherwise ..."

> 0

- For code points above 255 ...
Item "For code points above 255 ..."
.PD
\f(CW\*(C`\\s\*(C' matches exactly the code points above 255 shown with an \*(L"s\*(R" column
in the table below.

- For code points below 256 ...
Item "For code points below 256 ..."

> 0

- if locale rules are in effect ...
Item "if locale rules are in effect ..."
.PD
\f(CW\*(C`\\s\*(C' matches whatever the locale considers to be whitespace.

- if, instead, Unicode rules are in effect ...
Item "if, instead, Unicode rules are in effect ..."
\f(CW\*(C`\\s\*(C' matches exactly the characters shown with an \*(L"s\*(R" column in the
table below.

- otherwise ...
Item "otherwise ..."
\f(CW\*(C`\\s\*(C' matches [\\t\\n\\f\\r ] and, starting in Perl
v5.18, the vertical tab, \f(CW\*(C`\\cK\*(C'.
(See note \f(CW\*(C`[1]\*(C' below for a discussion of this.)
Note that this list doesn't include the non-breaking space.



> 




> 


Which rules apply are determined as described in \*(L"Which character set modifier is in effect?\*(R" in perlre.

Any character not matched by \f(CW\*(C`\\s\*(C' is matched by \f(CW\*(C`\\S\*(C'.

\f(CW\*(C`\\h\*(C' matches any character considered horizontal whitespace;
this includes the platform's space and tab characters and several others
listed in the table below.  \f(CW\*(C`\\H\*(C' matches any character
not considered horizontal whitespace.  They use the platform's native
character set, and do not consider any locale that may otherwise be in
use.

\f(CW\*(C`\\v\*(C' matches any character considered vertical whitespace;
this includes the platform's carriage return and line feed characters (newline)
plus several other characters, all listed in the table below.
\f(CW\*(C`\\V\*(C' matches any character not considered vertical whitespace.
They use the platform's native character set, and do not consider any
locale that may otherwise be in use.

\f(CW\*(C`\\R\*(C' matches anything that can be considered a newline under Unicode
rules. It can match a multi-character sequence. It cannot be used inside
a bracketed character class; use \f(CW\*(C`\\v\*(C' instead (vertical whitespace).
It uses the platform's
native character set, and does not consider any locale that may
otherwise be in use.
Details are discussed in perlrebackslash.

Note that unlike \f(CW\*(C`\\s\*(C' (and \f(CW\*(C`\\d\*(C' and \f(CW\*(C`\\w\*(C'), \f(CW\*(C`\\h\*(C' and \f(CW\*(C`\\v\*(C' always match
the same characters, without regard to other factors, such as the active
locale or whether the source string is in \s-1UTF-8\s0 format.

One might think that \f(CW\*(C`\\s\*(C' is equivalent to \f(CW\*(C`[\\h\\v]\*(C'. This is indeed true
starting in Perl v5.18, but prior to that, the sole difference was that the
vertical tab (\f(CW"\\cK") was not matched by \f(CW\*(C`\\s\*(C'.

The following table is a complete listing of characters matched by
\f(CW\*(C`\\s\*(C', \f(CW\*(C`\\h\*(C' and \f(CW\*(C`\\v\*(C' as of Unicode 6.3.

The first column gives the Unicode code point of the character (in hex format),
the second column gives the (Unicode) name. The third column indicates
by which class(es) the character is matched (assuming no locale is in
effect that changes the \f(CW\*(C`\\s\*(C' matching).

.Vb 10
 0x0009        CHARACTER TABULATION   h s
 0x000a              LINE FEED (LF)    vs
 0x000b             LINE TABULATION    vs  [1]
 0x000c              FORM FEED (FF)    vs
 0x000d        CARRIAGE RETURN (CR)    vs
 0x0020                       SPACE   h s
 0x0085             NEXT LINE (NEL)    vs  [2]
 0x00a0              NO-BREAK SPACE   h s  [2]
 0x1680            OGHAM SPACE MARK   h s
 0x2000                     EN QUAD   h s
 0x2001                     EM QUAD   h s
 0x2002                    EN SPACE   h s
 0x2003                    EM SPACE   h s
 0x2004          THREE-PER-EM SPACE   h s
 0x2005           FOUR-PER-EM SPACE   h s
 0x2006            SIX-PER-EM SPACE   h s
 0x2007                FIGURE SPACE   h s
 0x2008           PUNCTUATION SPACE   h s
 0x2009                  THIN SPACE   h s
 0x200a                  HAIR SPACE   h s
 0x2028              LINE SEPARATOR    vs
 0x2029         PARAGRAPH SEPARATOR    vs
 0x202f       NARROW NO-BREAK SPACE   h s
 0x205f   MEDIUM MATHEMATICAL SPACE   h s
 0x3000           IDEOGRAPHIC SPACE   h s
.Ve

- [1]
Item "[1]"
Prior to Perl v5.18, \f(CW\*(C`\\s\*(C' did not match the vertical tab.
\f(CW\*(C`[^\\S\\cK]\*(C' (obscurely) matches what \f(CW\*(C`\\s\*(C' traditionally did.

- [2]
Item "[2]"
\s-1NEXT LINE\s0 and NO-BREAK \s-1SPACE\s0 may or may not match \f(CW\*(C`\\s\*(C' depending
on the rules in effect.  See
the beginning of this section.

*Unicode Properties*
Subsection "Unicode Properties"

\f(CW\*(C`\\pP\*(C' and \f(CW\*(C`\\p\{Prop\}\*(C' are character classes to match characters that fit given
Unicode properties.  One letter property names can be used in the \f(CW\*(C`\\pP\*(C' form,
with the property name following the \f(CW\*(C`\\p\*(C', otherwise, braces are required.
When using braces, there is a single form, which is just the property name
enclosed in the braces, and a compound form which looks like \f(CW\*(C`\\p\{name=value\}\*(C',
which means to match if the property \*(L"name\*(R" for the character has that particular
\*(L"value\*(R".
For instance, a match for a number can be written as \f(CW\*(C`/\\pN/\*(C' or as
\f(CW\*(C`/\\p\{Number\}/\*(C', or as \f(CW\*(C`/\\p\{Number=True\}/\*(C'.
Lowercase letters are matched by the property *Lowercase_Letter* which
has the short form *Ll*. They need the braces, so are written as \f(CW\*(C`/\\p\{Ll\}/\*(C' or
\f(CW\*(C`/\\p\{Lowercase_Letter\}/\*(C', or \f(CW\*(C`/\\p\{General_Category=Lowercase_Letter\}/\*(C'
(the underscores are optional).
\f(CW\*(C`/\\pLl/\*(C' is valid, but means something different.
It matches a two character string: a letter (Unicode property \f(CW\*(C`\\pL\*(C'),
followed by a lowercase \f(CW\*(C`l\*(C'.

What a Unicode property matches is never subject to locale rules, and
if locale rules are not otherwise in effect, the use of a Unicode
property will force the regular expression into using Unicode rules, if
it isn't already.

Note that almost all properties are immune to case-insensitive matching.
That is, adding a \f(CW\*(C`/i\*(C' regular expression modifier does not change what
they match.  But there are two sets that are affected.  The first set is
\f(CW\*(C`Uppercase_Letter\*(C',
\f(CW\*(C`Lowercase_Letter\*(C',
and \f(CW\*(C`Titlecase_Letter\*(C',
all of which match \f(CW\*(C`Cased_Letter\*(C' under \f(CW\*(C`/i\*(C' matching.
The second set is
\f(CW\*(C`Uppercase\*(C',
\f(CW\*(C`Lowercase\*(C',
and \f(CW\*(C`Titlecase\*(C',
all of which match \f(CW\*(C`Cased\*(C' under \f(CW\*(C`/i\*(C' matching.
(The difference between these sets is that some things, such as Roman
numerals, come in both upper and lower case, so they are \f(CW\*(C`Cased\*(C', but
aren't considered to be letters, so they aren't \f(CW\*(C`Cased_Letter\*(C's. They're
actually \f(CW\*(C`Letter_Number\*(C's.)
This set also includes its subsets \f(CW\*(C`PosixUpper\*(C' and \f(CW\*(C`PosixLower\*(C', both
of which under \f(CW\*(C`/i\*(C' match \f(CW\*(C`PosixAlpha\*(C'.

For more details on Unicode properties, see \*(L"Unicode
Character Properties\*(R" in perlunicode; for a
complete list of possible properties, see
\*(L"Properties accessible through \\p\{\} and \\P\{\}\*(R" in perluniprops,
which notes all forms that have \f(CW\*(C`/i\*(C' differences.
It is also possible to define your own properties. This is discussed in
\*(L"User-Defined Character Properties\*(R" in perlunicode.

Unicode properties are defined (surprise!) only on Unicode code points.
Starting in v5.20, when matching against \f(CW\*(C`\\p\*(C' and \f(CW\*(C`\\P\*(C', Perl treats
non-Unicode code points (those above the legal Unicode maximum of
0x10FFFF) as if they were typical unassigned Unicode code points.

Prior to v5.20, Perl raised a warning and made all matches fail on
non-Unicode code points.  This could be somewhat surprising:

.Vb 3
 chr(0x110000) =~ \\p\{ASCII_Hex_Digit=True\}     # Fails on Perls < v5.20.
 chr(0x110000) =~ \\p\{ASCII_Hex_Digit=False\}    # Also fails on Perls
                                               # < v5.20
.Ve

Even though these two matches might be thought of as complements, until
v5.20 they were so only on Unicode code points.

Starting in perl v5.30, wildcards are allowed in Unicode property
values.  See \*(L"Wildcards in Property Values\*(R" in perlunicode.

Examples
Subsection "Examples"

.Vb 8
 "a"  =~  /\\w/      # Match, "a" is a \*(Aqword\*(Aq character.
 "7"  =~  /\\w/      # Match, "7" is a \*(Aqword\*(Aq character as well.
 "a"  =~  /\\d/      # No match, "a" isn\*(Aqt a digit.
 "7"  =~  /\\d/      # Match, "7" is a digit.
 " "  =~  /\\s/      # Match, a space is whitespace.
 "a"  =~  /\\D/      # Match, "a" is a non-digit.
 "7"  =~  /\\D/      # No match, "7" is not a non-digit.
 " "  =~  /\\S/      # No match, a space is not non-whitespace.

 " "  =~  /\\h/      # Match, space is horizontal whitespace.
 " "  =~  /\\v/      # No match, space is not vertical whitespace.
 "\\r" =~  /\\v/      # Match, a return is vertical whitespace.

 "a"  =~  /\\pL/     # Match, "a" is a letter.
 "a"  =~  /\\p\{Lu\}/  # No match, /\\p\{Lu\}/ matches upper case letters.

 "\\x\{0e0b\}" =~ /\\p\{Thai\}/  # Match, \\x\{0e0b\} is the character
                           # \*(AqTHAI CHARACTER SO SO\*(Aq, and that\*(Aqs in
                           # Thai Unicode class.
 "a"  =~  /\\P\{Lao\}/ # Match, as "a" is not a Laotian character.
.Ve

It is worth emphasizing that \f(CW\*(C`\\d\*(C', \f(CW\*(C`\\w\*(C', etc, match single characters, not
complete numbers or words. To match a number (that consists of digits),
use \f(CW\*(C`\\d+\*(C'; to match a word, use \f(CW\*(C`\\w+\*(C'.  But be aware of the security
considerations in doing so, as mentioned above.

### Bracketed Character Classes

Subsection "Bracketed Character Classes"
The third form of character class you can use in Perl regular expressions
is the bracketed character class.  In its simplest form, it lists the characters
that may be matched, surrounded by square brackets, like this: \f(CW\*(C`[aeiou]\*(C'.
This matches one of \f(CW\*(C`a\*(C', \f(CW\*(C`e\*(C', \f(CW\*(C`i\*(C', \f(CW\*(C`o\*(C' or \f(CW\*(C`u\*(C'.  Like the other
character classes, exactly one character is matched.* To match
a longer string consisting of characters mentioned in the character
class, follow the character class with a quantifier.  For
instance, \f(CW\*(C`[aeiou]+\*(C' matches one or more lowercase English vowels.

Repeating a character in a character class has no
effect; it's considered to be in the set only once.

Examples:

.Vb 5
 "e"  =~  /[aeiou]/        # Match, as "e" is listed in the class.
 "p"  =~  /[aeiou]/        # No match, "p" is not listed in the class.
 "ae" =~  /^[aeiou]$/      # No match, a character class only matches
                           # a single character.
 "ae" =~  /^[aeiou]+$/     # Match, due to the quantifier.

 -------
.Ve

* There are two exceptions to a bracketed character class matching a
single character only.  Each requires special handling by Perl to make
things work:

- \(bu
When the class is to match caselessly under \f(CW\*(C`/i\*(C' matching rules, and a
character that is explicitly mentioned inside the class matches a
multiple-character sequence caselessly under Unicode rules, the class
will also match that sequence.  For example, Unicode says that the
letter \f(CW\*(C`LATIN SMALL LETTER SHARP S\*(C' should match the sequence \f(CW\*(C`ss\*(C'
under \f(CW\*(C`/i\*(C' rules.  Thus,
.Sp
.Vb 2
 \*(Aqss\*(Aq =~ /\\A\\N\{LATIN SMALL LETTER SHARP S\}\\z/i             # Matches
 \*(Aqss\*(Aq =~ /\\A[aeioust\\N\{LATIN SMALL LETTER SHARP S\}]\\z/i    # Matches
.Ve
.Sp
For this to happen, the class must not be inverted (see \*(L"Negation\*(R")
and the character must be explicitly specified, and not be part of a
multi-character range (not even as one of its endpoints).  (\*(L"Character
Ranges\*(R" will be explained shortly.) Therefore,
.Sp
.Vb 6
 \*(Aqss\*(Aq =~ /\\A[\\0-\\x\{ff\}]\\z/ui       # Doesn\*(Aqt match
 \*(Aqss\*(Aq =~ /\\A[\\0-\\N\{LATIN SMALL LETTER SHARP S\}]\\z/ui   # No match
 \*(Aqss\*(Aq =~ /\\A[\\xDF-\\xDF]\\z/ui   # Matches on ASCII platforms, since
                               # \\xDF is LATIN SMALL LETTER SHARP S,
                               # and the range is just a single
                               # element
.Ve
.Sp
Note that it isn't a good idea to specify these types of ranges anyway.

- \(bu
Some names known to \f(CW\*(C`\\N\{...\}\*(C' refer to a sequence of multiple characters,
instead of the usual single character.  When one of these is included in
the class, the entire sequence is matched.  For example,
.Sp
.Vb 2
  "\\N\{TAMIL LETTER KA\}\\N\{TAMIL VOWEL SIGN AU\}"
                              =~ / ^ [\\N\{TAMIL SYLLABLE KAU\}]  $ /x;
.Ve
.Sp
matches, because \f(CW\*(C`\\N\{TAMIL SYLLABLE KAU\}\*(C' is a named sequence
consisting of the two characters matched against.  Like the other
instance where a bracketed class can match multiple characters, and for
similar reasons, the class must not be inverted, and the named sequence
may not appear in a range, even one where it is both endpoints.  If
these happen, it is a fatal error if the character class is within the
scope of \f(CW\*(C`use re \*(Aqstrict\*(C', or within an extended
\f(CW\*(C`(?[...])\*(C' class; otherwise
only the first code point is used (with a \f(CW\*(C`regexp\*(C'-type warning
raised).

*Special Characters Inside a Bracketed Character Class*
Subsection "Special Characters Inside a Bracketed Character Class"

Most characters that are meta characters in regular expressions (that
is, characters that carry a special meaning like \f(CW\*(C`.\*(C', \f(CW\*(C`*\*(C', or \f(CW\*(C`(\*(C') lose
their special meaning and can be used inside a character class without
the need to escape them. For instance, \f(CW\*(C`[()]\*(C' matches either an opening
parenthesis, or a closing parenthesis, and the parens inside the character
class don't group or capture.  Be aware that, unless the pattern is
evaluated in single-quotish context, variable interpolation will take
place before the bracketed class is parsed:

.Vb 6
 $, = "\\t| ";
 $a =~ m\*(Aq[$,]\*(Aq;        # single-quotish: matches \*(Aq$\*(Aq or \*(Aq,\*(Aq
 $a =~ q\{[$,]\}\*(Aq        # same
 $a =~ m/[$,]/;        # double-quotish: Because we made an
                       #   assignment to $, above, this now
                       #   matches "\\t", "|", or " "
.Ve

Characters that may carry a special meaning inside a character class are:
\f(CW\*(C`\\\*(C', \f(CW\*(C`^\*(C', \f(CW\*(C`-\*(C', \f(CW\*(C`[\*(C' and \f(CW\*(C`]\*(C', and are discussed below. They can be
escaped with a backslash, although this is sometimes not needed, in which
case the backslash may be omitted.

The sequence \f(CW\*(C`\\b\*(C' is special inside a bracketed character class. While
outside the character class, \f(CW\*(C`\\b\*(C' is an assertion indicating a point
that does not have either two word characters or two non-word characters
on either side, inside a bracketed character class, \f(CW\*(C`\\b\*(C' matches a
backspace character.

The sequences
\f(CW\*(C`\\a\*(C',
\f(CW\*(C`\\c\*(C',
\f(CW\*(C`\\e\*(C',
\f(CW\*(C`\\f\*(C',
\f(CW\*(C`\\n\*(C',
\f(CW\*(C`\\N\{\f(CINAME\f(CW\}\*(C',
\f(CW\*(C`\\N\{U+\f(CIhex char\f(CW\}\*(C',
\f(CW\*(C`\\r\*(C',
\f(CW\*(C`\\t\*(C',
and
\f(CW\*(C`\\x\*(C'
are also special and have the same meanings as they do outside a
bracketed character class.

Also, a backslash followed by two or three octal digits is considered an octal
number.

A \f(CW\*(C`[\*(C' is not special inside a character class, unless it's the start of a
\s-1POSIX\s0 character class (see \*(L"\s-1POSIX\s0 Character Classes\*(R" below). It normally does
not need escaping.

A \f(CW\*(C`]\*(C' is normally either the end of a \s-1POSIX\s0 character class (see
\*(L"\s-1POSIX\s0 Character Classes\*(R" below), or it signals the end of the bracketed
character class.  If you want to include a \f(CW\*(C`]\*(C' in the set of characters, you
must generally escape it.

However, if the \f(CW\*(C`]\*(C' is the *first* (or the second if the first
character is a caret) character of a bracketed character class, it
does not denote the end of the class (as you cannot have an empty class)
and is considered part of the set of characters that can be matched without
escaping.

Examples:

.Vb 8
 "+"   =~ /[+?*]/     #  Match, "+" in a character class is not special.
 "\\cH" =~ /[\\b]/      #  Match, \\b inside in a character class
                      #  is equivalent to a backspace.
 "]"   =~ /[][]/      #  Match, as the character class contains
                      #  both [ and ].
 "[]"  =~ /[[]]/      #  Match, the pattern contains a character class
                      #  containing just [, and the character class is
                      #  followed by a ].
.Ve

*Bracketed Character Classes and the \f(CI\*(C`/xx\*(C'\fI pattern modifier*
Subsection "Bracketed Character Classes and the /xx pattern modifier"

Normally \s-1SPACE\s0 and \s-1TAB\s0 characters have no special meaning inside a
bracketed character class; they are just added to the list of characters
matched by the class.  But if the \f(CW\*(C`/xx\*(C'
pattern modifier is in effect, they are generally ignored and can be
added to improve readability.  They can't be added in the middle of a
single construct:

.Vb 1
 / [ \\x\{10 FFFF\} ] /xx  # WRONG!
.Ve

The \s-1SPACE\s0 in the middle of the hex constant is illegal.

To specify a literal \s-1SPACE\s0 character, you can escape it with a
backslash, like:

.Vb 1
 /[ a e i o u \\  ]/xx
.Ve

This matches the English vowels plus the \s-1SPACE\s0 character.

For clarity, you should already have been using \f(CW\*(C`\\t\*(C' to specify a
literal tab, and \f(CW\*(C`\\t\*(C' is unaffected by \f(CW\*(C`/xx\*(C'.

*Character Ranges*
Subsection "Character Ranges"

It is not uncommon to want to match a range of characters. Luckily, instead
of listing all characters in the range, one may use the hyphen (\f(CW\*(C`-\*(C').
If inside a bracketed character class you have two characters separated
by a hyphen, it's treated as if all characters between the two were in
the class. For instance, \f(CW\*(C`[0-9]\*(C' matches any \s-1ASCII\s0 digit, and \f(CW\*(C`[a-m]\*(C'
matches any lowercase letter from the first half of the \s-1ASCII\s0 alphabet.

Note that the two characters on either side of the hyphen are not
necessarily both letters or both digits. Any character is possible,
although not advisable.  \f(CW\*(C`[\*(Aq-?]\*(C' contains a range of characters, but
most people will not know which characters that means.  Furthermore,
such ranges may lead to portability problems if the code has to run on
a platform that uses a different character set, such as \s-1EBCDIC.\s0

If a hyphen in a character class cannot syntactically be part of a range, for
instance because it is the first or the last character of the character class,
or if it immediately follows a range, the hyphen isn't special, and so is
considered a character to be matched literally.  If you want a hyphen in
your set of characters to be matched and its position in the class is such
that it could be considered part of a range, you must escape that hyphen
with a backslash.

Examples:

.Vb 12
 [a-z]       #  Matches a character that is a lower case ASCII letter.
 [a-fz]      #  Matches any letter between \*(Aqa\*(Aq and \*(Aqf\*(Aq (inclusive) or
             #  the letter \*(Aqz\*(Aq.
 [-z]        #  Matches either a hyphen (\*(Aq-\*(Aq) or the letter \*(Aqz\*(Aq.
 [a-f-m]     #  Matches any letter between \*(Aqa\*(Aq and \*(Aqf\*(Aq (inclusive), the
             #  hyphen (\*(Aq-\*(Aq), or the letter \*(Aqm\*(Aq.
 [\*(Aq-?]       #  Matches any of the characters  \*(Aq()*+,-./0123456789:;<=>?
             #  (But not on an EBCDIC platform).
 [\\N\{APOSTROPHE\}-\\N\{QUESTION MARK\}]
             #  Matches any of the characters  \*(Aq()*+,-./0123456789:;<=>?
             #  even on an EBCDIC platform.
 [\\N\{U+27\}-\\N\{U+3F\}] # Same. (U+27 is "\*(Aq", and U+3F is "?")
.Ve

As the final two examples above show, you can achieve portability to
non-ASCII platforms by using the \f(CW\*(C`\\N\{...\}\*(C' form for the range
endpoints.  These indicate that the specified range is to be interpreted
using Unicode values, so \f(CW\*(C`[\\N\{U+27\}-\\N\{U+3F\}]\*(C' means to match
\f(CW\*(C`\\N\{U+27\}\*(C', \f(CW\*(C`\\N\{U+28\}\*(C', \f(CW\*(C`\\N\{U+29\}\*(C', ..., \f(CW\*(C`\\N\{U+3D\}\*(C', \f(CW\*(C`\\N\{U+3E\}\*(C',
and \f(CW\*(C`\\N\{U+3F\}\*(C', whatever the native code point versions for those are.
These are called \*(L"Unicode\*(R" ranges.  If either end is of the \f(CW\*(C`\\N\{...\}\*(C'
form, the range is considered Unicode.  A \f(CW\*(C`regexp\*(C' warning is raised
under \f(CW"use\ re\ \*(Aqstrict\*(Aq" if the other endpoint is specified
non-portably:

.Vb 2
 [\\N\{U+00\}-\\x09]    # Warning under re \*(Aqstrict\*(Aq; \\x09 is non-portable
 [\\N\{U+00\}-\\t]      # No warning;
.Ve

Both of the above match the characters \f(CW\*(C`\\N\{U+00\}\*(C' \f(CW\*(C`\\N\{U+01\}\*(C', ...
\f(CW\*(C`\\N\{U+08\}\*(C', \f(CW\*(C`\\N\{U+09\}\*(C', but the \f(CW\*(C`\\x09\*(C' looks like it could be a
mistake so the warning is raised (under \f(CW\*(C`re \*(Aqstrict\*(Aq\*(C') for it.

Perl also guarantees that the ranges \f(CW\*(C`A-Z\*(C', \f(CW\*(C`a-z\*(C', \f(CW\*(C`0-9\*(C', and any
subranges of these match what an English-only speaker would expect them
to match on any platform.  That is, \f(CW\*(C`[A-Z]\*(C' matches the 26 \s-1ASCII\s0
uppercase letters;
\f(CW\*(C`[a-z]\*(C' matches the 26 lowercase letters; and \f(CW\*(C`[0-9]\*(C' matches the 10
digits.  Subranges, like \f(CW\*(C`[h-k]\*(C', match correspondingly, in this case
just the four letters \f(CW"h", \f(CW"i", \f(CW"j", and \f(CW"k".  This is the
natural behavior on \s-1ASCII\s0 platforms where the code points (ordinal
values) for \f(CW"h" through \f(CW"k" are consecutive integers (0x68 through
0x6B).  But special handling to achieve this may be needed on platforms
with a non-ASCII native character set.  For example, on \s-1EBCDIC\s0
platforms, the code point for \f(CW"h" is 0x88, \f(CW"i" is 0x89, \f(CW"j" is
0x91, and \f(CW"k" is 0x92.   Perl specially treats \f(CW\*(C`[h-k]\*(C' to exclude the
seven code points in the gap: 0x8A through 0x90.  This special handling is
only invoked when the range is a subrange of one of the \s-1ASCII\s0 uppercase,
lowercase, and digit ranges, \s-1AND\s0 each end of the range is expressed
either as a literal, like \f(CW"A", or as a named character (\f(CW\*(C`\\N\{...\}\*(C',
including the \f(CW\*(C`\\N\{U+...\*(C' form).

\s-1EBCDIC\s0 Examples:

.Vb 10
 [i-j]               #  Matches either "i" or "j"
 [i-\\N\{LATIN SMALL LETTER J\}]  # Same
 [i-\\N\{U+6A\}]        #  Same
 [\\N\{U+69\}-\\N\{U+6A\}] #  Same
 [\\x\{89\}-\\x\{91\}]     #  Matches 0x89 ("i"), 0x8A .. 0x90, 0x91 ("j")
 [i-\\x\{91\}]          #  Same
 [\\x\{89\}-j]          #  Same
 [i-J]               #  Matches, 0x89 ("i") .. 0xC1 ("J"); special
                     #  handling doesn\*(Aqt apply because range is mixed
                     #  case
.Ve

*Negation*
Subsection "Negation"

It is also possible to instead list the characters you do not want to
match. You can do so by using a caret (\f(CW\*(C`^\*(C') as the first character in the
character class. For instance, \f(CW\*(C`[^a-z]\*(C' matches any character that is not a
lowercase \s-1ASCII\s0 letter, which therefore includes more than a million
Unicode code points.  The class is said to be \*(L"negated\*(R" or \*(L"inverted\*(R".

This syntax make the caret a special character inside a bracketed character
class, but only if it is the first character of the class. So if you want
the caret as one of the characters to match, either escape the caret or
else don't list it first.

In inverted bracketed character classes, Perl ignores the Unicode rules
that normally say that named sequence, and certain characters should
match a sequence of multiple characters use under caseless \f(CW\*(C`/i\*(C'
matching.  Following those rules could lead to highly confusing
situations:

.Vb 1
 "ss" =~ /^[^\\xDF]+$/ui;   # Matches!
.Ve

This should match any sequences of characters that aren't \f(CW\*(C`\\xDF\*(C' nor
what \f(CW\*(C`\\xDF\*(C' matches under \f(CW\*(C`/i\*(C'.  \f(CW"s" isn't \f(CW\*(C`\\xDF\*(C', but Unicode
says that \f(CW"ss" is what \f(CW\*(C`\\xDF\*(C' matches under \f(CW\*(C`/i\*(C'.  So which one
\*(L"wins\*(R"? Do you fail the match because the string has \f(CW\*(C`ss\*(C' or accept it
because it has an \f(CW\*(C`s\*(C' followed by another \f(CW\*(C`s\*(C'?  Perl has chosen the
latter.  (See note in \*(L"Bracketed Character Classes\*(R" above.)

Examples:

.Vb 4
 "e"  =~  /[^aeiou]/   #  No match, the \*(Aqe\*(Aq is listed.
 "x"  =~  /[^aeiou]/   #  Match, as \*(Aqx\*(Aq isn\*(Aqt a lowercase vowel.
 "^"  =~  /[^^]/       #  No match, matches anything that isn\*(Aqt a caret.
 "^"  =~  /[x^]/       #  Match, caret is not special here.
.Ve

*Backslash Sequences*
Subsection "Backslash Sequences"

You can put any backslash sequence character class (with the exception of
\f(CW\*(C`\\N\*(C' and \f(CW\*(C`\\R\*(C') inside a bracketed character class, and it will act just
as if you had put all characters matched by the backslash sequence inside the
character class. For instance, \f(CW\*(C`[a-f\\d]\*(C' matches any decimal digit, or any
of the lowercase letters between 'a' and 'f' inclusive.

\f(CW\*(C`\\N\*(C' within a bracketed character class must be of the forms \f(CW\*(C`\\N\{\f(CIname\f(CW\}\*(C'
or \f(CW\*(C`\\N\{U+\f(CIhex char\f(CW\}\*(C', and \s-1NOT\s0 be the form that matches non-newlines,
for the same reason that a dot \f(CW\*(C`.\*(C' inside a bracketed character class loses
its special meaning: it matches nearly anything, which generally isn't what you
want to happen.

Examples:

.Vb 4
 /[\\p\{Thai\}\\d]/     # Matches a character that is either a Thai
                    # character, or a digit.
 /[^\\p\{Arabic\}()]/  # Matches a character that is neither an Arabic
                    # character, nor a parenthesis.
.Ve

Backslash sequence character classes cannot form one of the endpoints
of a range.  Thus, you can't say:

.Vb 1
 /[\\p\{Thai\}-\\d]/     # Wrong!
.Ve

*\s-1POSIX\s0 Character Classes*
Xref "character class \p \p\{\} alpha alnum ascii blank cntrl digit graph lower print punct space upper word xdigit"
Subsection "POSIX Character Classes"

\s-1POSIX\s0 character classes have the form \f(CW\*(C`[:class:]\*(C', where *class* is the
name, and the \f(CW\*(C`[:\*(C' and \f(CW\*(C`:]\*(C' delimiters. \s-1POSIX\s0 character classes only appear
*inside* bracketed character classes, and are a convenient and descriptive
way of listing a group of characters.

Be careful about the syntax,

.Vb 2
 # Correct:
 $string =~ /[[:alpha:]]/

 # Incorrect (will warn):
 $string =~ /[:alpha:]/
.Ve

The latter pattern would be a character class consisting of a colon,
and the letters \f(CW\*(C`a\*(C', \f(CW\*(C`l\*(C', \f(CW\*(C`p\*(C' and \f(CW\*(C`h\*(C'.

\s-1POSIX\s0 character classes can be part of a larger bracketed character class.
For example,

.Vb 1
 [01[:alpha:]%]
.Ve

is valid and matches '0', '1', any alphabetic character, and the percent sign.

Perl recognizes the following \s-1POSIX\s0 character classes:

.Vb 10
 alpha  Any alphabetical character (e.g., [A-Za-z]).
 alnum  Any alphanumeric character (e.g., [A-Za-z0-9]).
 ascii  Any character in the ASCII character set.
 blank  A GNU extension, equal to a space or a horizontal tab ("\\t").
 cntrl  Any control character.  See Note [2] below.
 digit  Any decimal digit (e.g., [0-9]), equivalent to "\\d".
 graph  Any printable character, excluding a space.  See Note [3] below.
 lower  Any lowercase character (e.g., [a-z]).
 print  Any printable character, including a space.  See Note [4] below.
 punct  Any graphical character excluding "word" characters.  Note [5].
 space  Any whitespace character. "\\s" including the vertical tab
        ("\\cK").
 upper  Any uppercase character (e.g., [A-Z]).
 word   A Perl extension (e.g., [A-Za-z0-9_]), equivalent to "\\w".
 xdigit Any hexadecimal digit (e.g., [0-9a-fA-F]).  Note [7].
.Ve

Like the Unicode properties, most of the \s-1POSIX\s0
properties match the same regardless of whether case-insensitive (\f(CW\*(C`/i\*(C')
matching is in effect or not.  The two exceptions are \f(CW\*(C`[:upper:]\*(C' and
\f(CW\*(C`[:lower:]\*(C'.  Under \f(CW\*(C`/i\*(C', they each match the union of \f(CW\*(C`[:upper:]\*(C' and
\f(CW\*(C`[:lower:]\*(C'.

Most \s-1POSIX\s0 character classes have two Unicode-style \f(CW\*(C`\\p\*(C' property
counterparts.  (They are not official Unicode properties, but Perl extensions
derived from official Unicode properties.)  The table below shows the relation
between \s-1POSIX\s0 character classes and these counterparts.

One counterpart, in the column labelled \*(L"ASCII-range Unicode\*(R" in
the table, matches only characters in the \s-1ASCII\s0 character set.

The other counterpart, in the column labelled \*(L"Full-range Unicode\*(R", matches any
appropriate characters in the full Unicode character set.  For example,
\f(CW\*(C`\\p\{Alpha\}\*(C' matches not just the \s-1ASCII\s0 alphabetic characters, but any
character in the entire Unicode character set considered alphabetic.
An entry in the column labelled \*(L"backslash sequence\*(R" is a (short)
equivalent.

.Vb 10
 [[:...:]]      ASCII-range          Full-range  backslash  Note
                 Unicode              Unicode     sequence
 -----------------------------------------------------
   alpha      \\p\{PosixAlpha\}       \\p\{XPosixAlpha\}
   alnum      \\p\{PosixAlnum\}       \\p\{XPosixAlnum\}
   ascii      \\p\{ASCII\}
   blank      \\p\{PosixBlank\}       \\p\{XPosixBlank\}  \\h      [1]
                                   or \\p\{HorizSpace\}        [1]
   cntrl      \\p\{PosixCntrl\}       \\p\{XPosixCntrl\}          [2]
   digit      \\p\{PosixDigit\}       \\p\{XPosixDigit\}  \\d
   graph      \\p\{PosixGraph\}       \\p\{XPosixGraph\}          [3]
   lower      \\p\{PosixLower\}       \\p\{XPosixLower\}
   print      \\p\{PosixPrint\}       \\p\{XPosixPrint\}          [4]
   punct      \\p\{PosixPunct\}       \\p\{XPosixPunct\}          [5]
              \\p\{PerlSpace\}        \\p\{XPerlSpace\}   \\s      [6]
   space      \\p\{PosixSpace\}       \\p\{XPosixSpace\}          [6]
   upper      \\p\{PosixUpper\}       \\p\{XPosixUpper\}
   word       \\p\{PosixWord\}        \\p\{XPosixWord\}   \\w
   xdigit     \\p\{PosixXDigit\}      \\p\{XPosixXDigit\}         [7]
.Ve

- [1]
Item "[1]"
\f(CW\*(C`\\p\{Blank\}\*(C' and \f(CW\*(C`\\p\{HorizSpace\}\*(C' are synonyms.

- [2]
Item "[2]"
Control characters don't produce output as such, but instead usually control
the terminal somehow: for example, newline and backspace are control characters.
On \s-1ASCII\s0 platforms, in the \s-1ASCII\s0 range, characters whose code points are
between 0 and 31 inclusive, plus 127 (\f(CW\*(C`DEL\*(C') are control characters; on
\s-1EBCDIC\s0 platforms, their counterparts are control characters.

- [3]
Item "[3]"
Any character that is *graphical*, that is, visible. This class consists
of all alphanumeric characters and all punctuation characters.

- [4]
Item "[4]"
All printable characters, which is the set of all graphical characters
plus those whitespace characters which are not also controls.

- [5]
Item "[5]"
\f(CW\*(C`\\p\{PosixPunct\}\*(C' and \f(CW\*(C`[[:punct:]]\*(C' in the \s-1ASCII\s0 range match all
non-controls, non-alphanumeric, non-space characters:
\f(CW\*(C`[-!"#$%&\*(Aq()*+,./:;<=>?@[\\\\\\]^_\`\{|\}~]\*(C' (although if a locale is in effect,
it could alter the behavior of \f(CW\*(C`[[:punct:]]\*(C').
.Sp
The similarly named property, \f(CW\*(C`\\p\{Punct\}\*(C', matches a somewhat different
set in the \s-1ASCII\s0 range, namely
\f(CW\*(C`[-!"#%&\*(Aq()*,./:;?@[\\\\\\]_\{\}]\*(C'.  That is, it is missing the nine
characters \f(CW\*(C`[$+<=>^\`|~]\*(C'.
This is because Unicode splits what \s-1POSIX\s0 considers to be punctuation into two
categories, Punctuation and Symbols.
.Sp
\f(CW\*(C`\\p\{XPosixPunct\}\*(C' and (under Unicode rules) \f(CW\*(C`[[:punct:]]\*(C', match what
\f(CW\*(C`\\p\{PosixPunct\}\*(C' matches in the \s-1ASCII\s0 range, plus what \f(CW\*(C`\\p\{Punct\}\*(C'
matches.  This is different than strictly matching according to
\f(CW\*(C`\\p\{Punct\}\*(C'.  Another way to say it is that
if Unicode rules are in effect, \f(CW\*(C`[[:punct:]]\*(C' matches all characters
that Unicode considers punctuation, plus all ASCII-range characters that
Unicode considers symbols.

- [6]
Item "[6]"
\f(CW\*(C`\\p\{XPerlSpace\}\*(C' and \f(CW\*(C`\\p\{Space\}\*(C' match identically starting with Perl
v5.18.  In earlier versions, these differ only in that in non-locale
matching, \f(CW\*(C`\\p\{XPerlSpace\}\*(C' did not match the vertical tab, \f(CW\*(C`\\cK\*(C'.
Same for the two ASCII-only range forms.

- [7]
Item "[7]"
Unlike \f(CW\*(C`[[:digit:]]\*(C' which matches digits in many writing systems, such
as Thai and Devanagari, there are currently only two sets of hexadecimal
digits, and it is unlikely that more will be added.  This is because you
not only need the ten digits, but also the six \f(CW\*(C`[A-F]\*(C' (and \f(CW\*(C`[a-f]\*(C')
to correspond.  That means only the Latin script is suitable for these,
and Unicode has only two sets of these, the familiar \s-1ASCII\s0 set, and the
fullwidth forms starting at U+FF10 (\s-1FULLWIDTH DIGIT ZERO\s0).

There are various other synonyms that can be used besides the names
listed in the table.  For example, \f(CW\*(C`\\p\{XPosixAlpha\}\*(C' can be written as
\f(CW\*(C`\\p\{Alpha\}\*(C'.  All are listed in
\*(L"Properties accessible through \\p\{\} and \\P\{\}\*(R" in perluniprops.

Both the \f(CW\*(C`\\p\*(C' counterparts always assume Unicode rules are in effect.
On \s-1ASCII\s0 platforms, this means they assume that the code points from 128
to 255 are Latin-1, and that means that using them under locale rules is
unwise unless the locale is guaranteed to be Latin-1 or \s-1UTF-8.\s0  In contrast, the
\s-1POSIX\s0 character classes are useful under locale rules.  They are
affected by the actual rules in effect, as follows:
.ie n .IP "If the ""/a"" modifier, is in effect ..." 4
.el .IP "If the \f(CW/a modifier, is in effect ..." 4
Item "If the /a modifier, is in effect ..."
Each of the \s-1POSIX\s0 classes matches exactly the same as their ASCII-range
counterparts.

- otherwise ...
Item "otherwise ..."

> 0

- For code points above 255 ...
Item "For code points above 255 ..."
.PD
The \s-1POSIX\s0 class matches the same as its Full-range counterpart.

- For code points below 256 ...
Item "For code points below 256 ..."

> 0

- if locale rules are in effect ...
Item "if locale rules are in effect ..."
.PD
The \s-1POSIX\s0 class matches according to the locale, except:

> .ie n .IP """word""" 4
.el .IP "\f(CWword" 4
Item "word"
also includes the platform's native underscore character, no matter what
the locale is.
.ie n .IP """ascii""" 4
.el .IP "\f(CWascii" 4
Item "ascii"
on platforms that don't have the \s-1POSIX\s0 \f(CW\*(C`ascii\*(C' extension, this matches
just the platform's native ASCII-range characters.
.ie n .IP """blank""" 4
.el .IP "\f(CWblank" 4
Item "blank"
on platforms that don't have the \s-1POSIX\s0 \f(CW\*(C`blank\*(C' extension, this matches
just the platform's native tab and space characters.



> 


- if, instead, Unicode rules are in effect ...
Item "if, instead, Unicode rules are in effect ..."
The \s-1POSIX\s0 class matches the same as the Full-range counterpart.

- otherwise ...
Item "otherwise ..."
The \s-1POSIX\s0 class matches the same as the \s-1ASCII\s0 range counterpart.



> 




> 


Which rules apply are determined as described in
\*(L"Which character set modifier is in effect?\*(R" in perlre.

Negation of \s-1POSIX\s0 character classes
Xref "character class, negation"
Subsection "Negation of POSIX character classes"

A Perl extension to the \s-1POSIX\s0 character class is the ability to
negate it. This is done by prefixing the class name with a caret (\f(CW\*(C`^\*(C').
Some examples:

.Vb 7
     POSIX         ASCII-range     Full-range  backslash
                    Unicode         Unicode    sequence
 -----------------------------------------------------
 [[:^digit:]]   \\P\{PosixDigit\}  \\P\{XPosixDigit\}   \\D
 [[:^space:]]   \\P\{PosixSpace\}  \\P\{XPosixSpace\}
                \\P\{PerlSpace\}   \\P\{XPerlSpace\}    \\S
 [[:^word:]]    \\P\{PerlWord\}    \\P\{XPosixWord\}    \\W
.Ve

The backslash sequence can mean either \s-1ASCII-\s0 or Full-range Unicode,
depending on various factors as described in \*(L"Which character set modifier is in effect?\*(R" in perlre.

[= =] and [. .]
Subsection "[= =] and [. .]"

Perl recognizes the \s-1POSIX\s0 character classes \f(CW\*(C`[=class=]\*(C' and
\f(CW\*(C`[.class.]\*(C', but does not (yet?) support them.  Any attempt to use
either construct raises an exception.

Examples
Subsection "Examples"

.Vb 12
 /[[:digit:]]/            # Matches a character that is a digit.
 /[01[:lower:]]/          # Matches a character that is either a
                          # lowercase letter, or \*(Aq0\*(Aq or \*(Aq1\*(Aq.
 /[[:digit:][:^xdigit:]]/ # Matches a character that can be anything
                          # except the letters \*(Aqa\*(Aq to \*(Aqf\*(Aq and \*(AqA\*(Aq to
                          # \*(AqF\*(Aq.  This is because the main character
                          # class is composed of two POSIX character
                          # classes that are ORed together, one that
                          # matches any digit, and the other that
                          # matches anything that isn\*(Aqt a hex digit.
                          # The OR adds the digits, leaving only the
                          # letters \*(Aqa\*(Aq to \*(Aqf\*(Aq and \*(AqA\*(Aq to \*(AqF\*(Aq excluded.
.Ve

*Extended Bracketed Character Classes*
Xref "character class set operations"
Subsection "Extended Bracketed Character Classes"

This is a fancy bracketed character class that can be used for more
readable and less error-prone classes, and to perform set operations,
such as intersection. An example is

.Vb 1
 /(?[ \\p\{Thai\} & \\p\{Digit\} ])/
.Ve

This will match all the digit characters that are in the Thai script.

This is an experimental feature available starting in 5.18, and is
subject to change as we gain field experience with it.  Any attempt to
use it will raise a warning, unless disabled via

.Vb 1
 no warnings "experimental::regex_sets";
.Ve

Comments on this feature are welcome; send email to
\f(CW\*(C`perl5-porters@perl.org\*(C'.

The rules used by \f(CW\*(C`use re \*(Aqstrict\*(C' apply to this
construct.

We can extend the example above:

.Vb 1
 /(?[ ( \\p\{Thai\} + \\p\{Lao\} ) & \\p\{Digit\} ])/
.Ve

This matches digits that are in either the Thai or Laotian scripts.

Notice the white space in these examples.  This construct always has
the \f(CW\*(C`/xx\*(C' modifier turned on within it.

The available binary operators are:

.Vb 10
 &    intersection
 +    union
 |    another name for \*(Aq+\*(Aq, hence means union
 -    subtraction (the result matches the set consisting of those
      code points matched by the first operand, excluding any that
      are also matched by the second operand)
 ^    symmetric difference (the union minus the intersection).  This
      is like an exclusive or, in that the result is the set of code
      points that are matched by either, but not both, of the
      operands.
.Ve

There is one unary operator:

.Vb 1
 !    complement
.Ve

All the binary operators left associate; \f(CW"&" is higher precedence
than the others, which all have equal precedence.  The unary operator
right associates, and has highest precedence.  Thus this follows the
normal Perl precedence rules for logical operators.  Use parentheses to
override the default precedence and associativity.

The main restriction is that everything is a metacharacter.  Thus,
you cannot refer to single characters by doing something like this:

.Vb 1
 /(?[ a + b ])/ # Syntax error!
.Ve

The easiest way to specify an individual typable character is to enclose
it in brackets:

.Vb 1
 /(?[ [a] + [b] ])/
.Ve

(This is the same thing as \f(CW\*(C`[ab]\*(C'.)  You could also have said the
equivalent:

.Vb 1
 /(?[[ a b ]])/
.Ve

(You can, of course, specify single characters by using, \f(CW\*(C`\\x\{...\}\*(C',
\f(CW\*(C`\\N\{...\}\*(C', etc.)

This last example shows the use of this construct to specify an ordinary
bracketed character class without additional set operations.  Note the
white space within it.  This is allowed because \f(CW\*(C`/xx\*(C' is
automatically turned on within this construct.

All the other escapes accepted by normal bracketed character classes are
accepted here as well.

Because this construct compiles under
\f(CW\*(C`use re \*(Aqstrict\*(C',  unrecognized escapes that
generate warnings in normal classes are fatal errors here, as well as
all other warnings from these class elements, as well as some
practices that don't currently warn outside \f(CW\*(C`re \*(Aqstrict\*(Aq\*(C'.  For example
you cannot say

.Vb 1
 /(?[ [ \\xF ] ])/     # Syntax error!
.Ve

You have to have two hex digits after a braceless \f(CW\*(C`\\x\*(C' (use a leading
zero to make two).  These restrictions are to lower the incidence of
typos causing the class to not match what you thought it would.

If a regular bracketed character class contains a \f(CW\*(C`\\p\{\}\*(C' or \f(CW\*(C`\\P\{\}\*(C' and
is matched against a non-Unicode code point, a warning may be
raised, as the result is not Unicode-defined.  No such warning will come
when using this extended form.

The final difference between regular bracketed character classes and
these, is that it is not possible to get these to match a
multi-character fold.  Thus,

.Vb 1
 /(?[ [\\xDF] ])/iu
.Ve

does not match the string \f(CW\*(C`ss\*(C'.

You don't have to enclose \s-1POSIX\s0 class names inside double brackets,
hence both of the following work:

.Vb 2
 /(?[ [:word:] - [:lower:] ])/
 /(?[ [[:word:]] - [[:lower:]] ])/
.Ve

Any contained \s-1POSIX\s0 character classes, including things like \f(CW\*(C`\\w\*(C' and \f(CW\*(C`\\D\*(C'
respect the \f(CW\*(C`/a\*(C' (and \f(CW\*(C`/aa\*(C') modifiers.

Note that \f(CW\*(C`(?[ ])\*(C' is a regex-compile-time construct.  Any attempt
to use something which isn't knowable at the time the containing regular
expression is compiled is a fatal error.  In practice, this means
just three limitations:

- 1.
When compiled within the scope of \f(CW\*(C`use locale\*(C' (or the \f(CW\*(C`/l\*(C' regex
modifier), this construct assumes that the execution-time locale will be
a \s-1UTF-8\s0 one, and the generated pattern always uses Unicode rules.  What
gets matched or not thus isn't dependent on the actual runtime locale, so
tainting is not enabled.  But a \f(CW\*(C`locale\*(C' category warning is raised
if the runtime locale turns out to not be \s-1UTF-8.\s0

- 2.
Any
user-defined property
used must be already defined by the time the regular expression is
compiled (but note that this construct can be used instead of such
properties).

- 3.
A regular expression that otherwise would compile
using \f(CW\*(C`/d\*(C' rules, and which uses this construct will instead
use \f(CW\*(C`/u\*(C'.  Thus this construct tells Perl that you don't want
\f(CW\*(C`/d\*(C' rules for the entire regular expression containing it.

Note that skipping white space applies only to the interior of this
construct.  There must not be any space between any of the characters
that form the initial \f(CW\*(C`(?[\*(C'.  Nor may there be space between the
closing \f(CW\*(C`])\*(C' characters.

Just as in all regular expressions, the pattern can be built up by
including variables that are interpolated at regex compilation time.
But its best to compile each sub-component.

.Vb 2
 my $thai_or_lao = qr/(?[ \\p\{Thai\} + \\p\{Lao\} ])/;
 my $lower = qr/(?[ \\p\{Lower\} + \\p\{Digit\} ])/;
.Ve

When these are embedded in another pattern, what they match does not
change, regardless of parenthesization or what modifiers are in effect
in that outer pattern.  If you fail to compile the subcomponents, you
can get some nasty surprises.  For example:

.Vb 3
 my $thai_or_lao = \*(Aq\\p\{Thai\} + \\p\{Lao\}\*(Aq;
 ...
 qr/(?[ \\p\{Digit\} & $thai_or_lao ])/;
.Ve

compiles to

.Vb 1
 qr/(?[ \\p\{Digit\} & \\p\{Thai\} + \\p\{Lao\} ])/;
.Ve

But this does not have the effect that someone reading the source code
would likely expect, as the intersection applies just to \f(CW\*(C`\\p\{Thai\}\*(C',
excluding the Laotian.  Its best to compile the subcomponents, but you
could also parenthesize the component pieces:

.Vb 1
 my $thai_or_lao = \*(Aq( \\p\{Thai\} + \\p\{Lao\} )\*(Aq;
.Ve

But any modifiers will still apply to all the components:

.Vb 2
 my $lower = \*(Aq\\p\{Lower\} + \\p\{Digit\}\*(Aq;
 qr/(?[ \\p\{Greek\} & $lower ])/i;
.Ve

matches upper case things.  So just, compile the subcomponents, as
illustrated above.

Due to the way that Perl parses things, your parentheses and brackets
may need to be balanced, even including comments.  If you run into any
examples, please submit them to <https://github.com/Perl/perl5/issues>,
so that we can have a concrete example for this man page.

We may change it so that things that remain legal uses in normal bracketed
character classes might become illegal within this experimental
construct.  One proposal, for example, is to forbid adjacent uses of the
same character, as in \f(CW\*(C`(?[ [aa] ])\*(C'.  The motivation for such a change
is that this usage is likely a typo, as the second \*(L"a\*(R" adds nothing.
