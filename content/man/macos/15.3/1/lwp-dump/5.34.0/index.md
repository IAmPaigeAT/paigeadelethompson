+++
author = "None Specified"
manpage_name = "lwp-dump"
date = "2020-04-14"
title = "lwp-dump(1)"
description = "The lwp-dump program will get the resource identified by the s-1URLs0 and then dump the response object to s-1STDOUT.s0  This will display the headers returned and the initial part of the content, escaped so that its safe to display even binary co..."
manpage_section = "1"
operating_system_version = "15.3"
keywords = ["header", "see", "also", "lwp-request", "s-1lwp", "s0", "l", "dump", "r", "in", "http", "message"]
operating_system = "macos"
detected_package_version = "5.34.0"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
Title "LWP-DUMP 1"
LWP-DUMP 1 "2020-04-14" "perl v5.34.0" "User Contributed Perl Documentation"

## NAME

lwp-dump - See what headers and content is returned for a URL

## SYNOPSIS

Header "SYNOPSIS"
**lwp-dump** [ *options* ] *\s-1URL\s0*

## DESCRIPTION

Header "DESCRIPTION"
The **lwp-dump** program will get the resource identified by the \s-1URL\s0 and then
dump the response object to \s-1STDOUT.\s0  This will display the headers returned and
the initial part of the content, escaped so that it's safe to display even
binary content.  The escapes syntax used is the same as for Perl's double
quoted strings.  If there is no content the string \*(L"(no content)\*(R" is shown in
its place.

The following options are recognized:

- \fB--agent \fIstring
Item "--agent string"
Override the user agent string passed to the server.

- \fB--keep-client-headers
Item "--keep-client-headers"
\s-1LWP\s0 internally generate various \f(CW\*(C`Client-*\*(C' headers that are stripped by
**lwp-dump** in order to show the headers exactly as the server provided them.
This option will suppress this.

- \fB--max-length \fIn
Item "--max-length n"
How much of the content to show.  The default is 512.  Set this
to 0 for unlimited.
.Sp
If the content is longer then the string is chopped at the
limit and the string \*(L"...\\n(### more bytes not shown)\*(R"
appended.

- \fB--method \fIstring
Item "--method string"
Use the given method for the request instead of the default \*(L"\s-1GET\*(R".\s0

- \fB--parse-head
Item "--parse-head"
By default **lwp-dump** will not try to initialize headers by looking at the
head section of \s-1HTML\s0 documents.  This option enables this.  This corresponds to
\*(L"parse_head\*(R" in LWP::UserAgent.

- \fB--request
Item "--request"
Also dump the request sent.

## SEE ALSO

Header "SEE ALSO"
lwp-request, \s-1LWP\s0, \*(L"dump\*(R" in HTTP::Message
