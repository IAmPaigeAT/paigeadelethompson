+++
description = "is used to regenerate indices based upon the current contents of a database. It opens the given database determined by the database number or suffix and updates the indices for all values of all attributes of all entries. If a list of specific att..."
operating_system_version = "15.3"
title = "slapindex(8C)"
manpage_name = "slapindex"
manpage_format = "troff"
author = "The OpenLDAP Foundation All Rights Reserved."
keywords = ["ldap", "3", "ldif", "5", "slapadd", "8", "ldapadd", "1", "slapd", "openldap", "administrator", "s", "guide", "http", "www", "org", "doc", "admin", "acknowledgements", "shared", "project", "acknowledgement", "text", "software", "is", "developed", "and", "maintained", "by", "the", "derived", "from", "university", "of", "michigan", "release"]
operating_system = "macos"
detected_package_version = "2.4.28"
manpage_section = "8C"
date = "2011-01-01"
+++

SLAPINDEX 8C "2011/11/24" "OpenLDAP 2.4.28"

## NAME

slapindex - Reindex entries in a SLAPD database

## SYNOPSIS

/usr/sbin/slapindex
[\c
-b \ suffix]
[\c
-c ]
[\c
-d \ debug-level]
[\c
-f \ slapd.conf]
[\c
-F \ confdir]
[\c
-g ]
[\c
-n \ dbnum]
[\c
-o \ option[ = value]]
[\c
-q ]
[\c
-t ]
[\c
-v ]
[\c
attr [ ... ]]
.B


## DESCRIPTION


Slapindex
is used to regenerate
slapd (8)
indices based upon the current contents of a database.
It opens the given database determined by the database number or
suffix and updates the indices for all values of all attributes
of all entries. If a list of specific attributes is provided
on the command line, only the indices for those attributes will
be regenerated.
Databases configured as
subordinate
of this one are also re-indexed, unless **-g** is specified.

All files eventually created by
slapindex
will belong to the identity
slapindex
is run as, so make sure you either run
slapindex
with the same identity
slapd (8)
will be run as (see option
-u
in
slapd (8)),
or change file ownership before running
slapd (8).

## OPTIONS


-b \ suffix
Use the specified *suffix* to determine which database to
generate output for.  The **-b** cannot be used in conjunction
with the
-n
option.

-c
enable continue (ignore errors) mode.

-d \ debug-level
enable debugging messages as defined by the specified
debug-level ;
see
slapd (8)
for details.

-f \ slapd.conf
specify an alternative
slapd.conf (5)
file.

-F \ confdir
specify a config directory.
If both
-f
and
-F
are specified, the config file will be read and converted to
config directory format and written to the specified directory.
If neither option is specified, an attempt to read the
default config directory will be made before trying to use the default
config file. If a valid config directory exists then the
default config file is ignored.

-g
disable subordinate gluing.  Only the specified database will be
processed, and not its glued subordinates (if any).

-n \ dbnum
Generate output for the *dbnum*-th database listed in the
configuration file. The config database
slapd-config (5),
is always the first database, so use
-n 0

The
-n
cannot be used in conjunction with the
-b
option.

-o \ option[ = value]
Specify an
option
with a(n optional)
value .
Possible generic options/values are:


```
              syslog=<subsystems>  (see `\-s' in slapd(8))
              syslog\-level=<level> (see `\-S' in slapd(8))
              syslog\-user=<user>   (see `\-l' in slapd(8))

```


-q
enable quick (fewer integrity checks) mode. Performs no consistency checks
when writing the database. Improves indexing time,
however
the database will most likely be unusable if any errors or
interruptions occur.

-t
enable truncate mode. Truncates (empties) an index database before indexing
any entries. May only be used with Quick mode.

-v
enable verbose mode.

## LIMITATIONS

Your
slapd (8)
should not be running (at least, not in read-write
mode) when you do this to ensure consistency of the database.

This command provides ample opportunity for the user to obtain
and drink their favorite beverage.

## EXAMPLES

To reindex your SLAPD database, give the command:


```
.ft tt
	/usr/sbin/slapindex
.ft
```

To regenerate the index for only a specific attribute, e.g. "uid",
give the command:


```
.ft tt
	/usr/sbin/slapindex uid
.ft
```


## SEE ALSO

ldap (3),
ldif (5),
slapadd (8),
ldapadd (1),
slapd (8)

"OpenLDAP Administrator's Guide" (http://www.OpenLDAP.org/doc/admin/)

## ACKNOWLEDGEMENTS

"OpenLDAP Software"
is developed and maintained by The OpenLDAP Project <http://www.openldap.org/>.
"OpenLDAP Software"
is derived from University of Michigan LDAP 3.3 Release.
