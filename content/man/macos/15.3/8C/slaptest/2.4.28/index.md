+++
description = "is used to check the conformance of the configuration. It opens the configuration file or the  backend, and parses it according to the general and the backend-specific  rules, checking its sanity. enable debugging messages as defined by the spec..."
manpage_format = "troff"
operating_system_version = "15.3"
keywords = ["ldap", "3", "slapd", "8", "slapdn", "openldap", "administrator", "s", "guide", "http", "www", "org", "doc", "admin", "acknowledgements", "shared", "project", "acknowledgement", "text", "software", "is", "developed", "and", "maintained", "by", "the", "derived", "from", "university", "of", "michigan", "release"]
operating_system = "macos"
date = "2011-01-01"
detected_package_version = "2.4.28"
manpage_name = "slaptest"
title = "slaptest(8C)"
manpage_section = "8C"
author = "The OpenLDAP Foundation All Rights Reserved."
+++

SLAPTEST 8C "2011/11/24" "OpenLDAP 2.4.28"

## NAME

slaptest - Check the suitability of the OpenLDAP slapd configuration

## SYNOPSIS

/usr/sbin/slaptest
[\c
-d \ debug-level]
[\c
-f \ slapd.conf]
[\c
-F \ confdir]
[\c
-n dbnum]
[\c
-o \ option[ = value]]
[\c
-Q ]
[\c
-u ]
[\c
-v ]


## DESCRIPTION


Slaptest
is used to check the conformance of the
slapd (8)
configuration.
It opens the
slapd.conf (5)
configuration file or the
slapd-config (5)
backend, and parses it according to the general and the backend-specific
rules, checking its sanity.


## OPTIONS


-d \ debug-level
enable debugging messages as defined by the specified
debug-level ;
see
slapd (8)
for details.

-f \ slapd.conf
specify an alternative
slapd.conf (5)
file.

-F \ confdir
specify a config directory.
If both
-f
and
-F
are specified, the config file will be read and converted to
config directory format and written to the specified directory.
If neither option is specified, slaptest will attempt to read the
default config directory before trying to use the default
config file. If a valid config directory exists then the
default config file is ignored. If dry-run mode is also specified,
no conversion will occur.

-n \ dbnum
Just open and test the *dbnum*-th database listed in the
configuration file.
To only test the config database
slapd-config (5),
use
-n 0
as it is always the first database.

-o \ option[ = value]
Specify an
option
with a(n optional)
value .
Possible generic options/values are:


```
              syslog=<subsystems>  (see `\-s' in slapd(8))
              syslog\-level=<level> (see `\-S' in slapd(8))
              syslog\-user=<user>   (see `\-l' in slapd(8))

```


-Q
Be extremely quiet: only the exit code indicates success (0) or not
(any other value).

-u
enable dry-run mode (i.e. don't fail if databases cannot be opened,
but config is fine).

-v
enable verbose mode.

## EXAMPLES

To check a
slapd.conf (5)
give the command:


```
.ft tt
	/usr/sbin/slaptest \-f //etc/openldap/slapd.conf \-v
.ft
```


## SEE ALSO

ldap (3),
slapd (8),
slapdn (8)

"OpenLDAP Administrator's Guide" (http://www.OpenLDAP.org/doc/admin/)

## ACKNOWLEDGEMENTS

"OpenLDAP Software"
is developed and maintained by The OpenLDAP Project <http://www.openldap.org/>.
"OpenLDAP Software"
is derived from University of Michigan LDAP 3.3 Release.
