+++
date = "2009-01-01"
author = "Info-ZIP. All rights reserved."
keywords = ["fiegrep", "fiunzip", "fizip", "fifunzip", "fizipcloak", "fizipinfo", "fizipnote", "fizipsplit", "url", "the", "info-zip", "home", "page", "is", "currently", "at", "fchttp", "www", "org", "pub", "infozip", "or", "fcftp", "ftp", "authors", "fizipgrep", "was", "written", "by", "jean-loup", "gailly"]
operating_system = "macos"
manpage_name = "zipgrep"
title = "zipgrep(1L)"
description = "zipgrep will search files within a ZIP archive for lines matching the given string or pattern.  zipgrep is a shell script and requires egrep(1) and unzip(1L) to function.  Its output is identical to that of egrep(1). The pattern to be located with..."
detected_package_version = "1990-2009"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_section = "1L"
+++

ZIPGREP 1L "20 April 2009" "Info-ZIP"

## NAME

zipgrep - search files in a ZIP archive for lines matching a pattern
.PD

## SYNOPSIS

**zipgrep** [**egrep_options**] *pattern*
*file*[*.zip*] [*file(s)*\ .\|.\|.]
[**-x**\ *xfile(s)*\ .\|.\|.]
.PD

## DESCRIPTION

*zipgrep* will search files within a ZIP archive for lines matching
the given string or pattern.  *zipgrep* is a shell script and requires
*egrep*(1) and *unzip*(1L) to function.  Its output is identical to
that of *egrep*(1).
.PD

## ARGUMENTS


*pattern*
The pattern to be located within a ZIP archive.  Any string or regular
expression accepted by *egrep*(1) may be used.
file [ .zip ]
Path of the ZIP archive.  (Wildcard expressions for the ZIP archive name are
not supported.)  If the literal filename is not found, the suffix \fC.zip
is appended.  Note that self-extracting ZIP files are supported, as with any
other ZIP archive; just specify the \fC.exe suffix (if any) explicitly.
[*file(s)*]
An optional list of archive members to be processed, separated by spaces.
If no member files are specified, all members of the ZIP archive are searched.
Regular expressions (wildcards) may be used to match multiple members:

> *
matches a sequence of 0 or more characters
?
matches exactly 1 character
[.\|.\|.]
matches any single character found inside the brackets; ranges are specified
by a beginning character, a hyphen, and an ending character.  If an exclamation
point or a caret (`!' or `^') follows the left bracket, then the range of
characters within the brackets is complemented (that is, anything *except*
the characters inside the brackets is considered a match).


.IP
(Be sure to quote any character that might otherwise be interpreted or
modified by the operating system.)

- [\fB-x\
An optional list of archive members to be excluded from processing.
Since wildcard characters match directory separators (`/'), this option
may be used to exclude any files that are in subdirectories.  For
example, ``\fCzipgrep grumpy foo *.[ch] -x */*'' would search for the
string ``grumpy'' in all C source files in the main directory of the ``foo''
archive, but none in any subdirectories.  Without the **-x**
option, all C source files in all directories within the zipfile would be
searched.

## OPTIONS

All options prior to the ZIP archive filename are passed to *egrep*(1).
.PD

## SEE ALSO

*egrep*(1), *unzip*(1L), *zip*(1L), *funzip*(1L),
*zipcloak*(1L), *zipinfo*(1L), *zipnote*(1L), *zipsplit*(1L)
.PD

## URL

The Info-ZIP home page is currently at
.EX
\fChttp://www.info-zip.org/pub/infozip/
.EE
or
.EX
\fCftp://ftp.info-zip.org/pub/infozip/ .
.EE
.PD

## AUTHORS

*zipgrep* was written by Jean-loup Gailly.
.PD
