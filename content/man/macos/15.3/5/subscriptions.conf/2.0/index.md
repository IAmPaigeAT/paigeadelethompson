+++
manpage_section = "5"
description = "The subscriptions.conf file defines the local event notification subscriptions that are active. It is normally located in the /etc/cups directory and is maintained by the program. This file is not intended to be edited or managed manually. The nam..."
operating_system_version = "15.3"
operating_system = "macos"
title = "subscriptions.conf(5)"
date = "Sun Feb 16 04:48:11 2025"
detected_package_version = "2.0"
keywords = ["classes", "conf", "5", "cups-files", "cupsd", "8", "mime", "convs", "types", "printers", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
manpage_format = "troff"
manpage_name = "subscriptions.conf"
author = "None Specified"
+++

subscriptions.conf 5 "CUPS" "26 April 2019" "Apple Inc."

## NAME

subscriptions.conf - subscription configuration file for cups

## DESCRIPTION

The **subscriptions.conf** file defines the local event notification subscriptions that are active.
It is normally located in the */etc/cups* directory and is maintained by the
cupsd (8)
program.
This file is not intended to be edited or managed manually.

## NOTES

The name, location, and format of this file are an implementation detail that will change in future releases of CUPS.

## SEE ALSO

classes.conf (5),
cups-files.conf (5),
cupsd (8),
cupsd.conf (5),
mime.convs (5),
mime.types (5),
printers.conf (5),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
