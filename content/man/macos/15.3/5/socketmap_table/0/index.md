+++
operating_system = "macos"
date = "Sun Feb 16 04:48:11 2025"
detected_package_version = "0"
description = "The Postfix mail system uses optional tables for address rewriting. mail routing or policy lookup."
operating_system_version = "15.3"
author = "None Specified"
title = "socketmap_table(5)"
manpage_format = "troff"
keywords = ["na", "http", "cr", "yp", "to", "proto", "netstrings", "txt", "netstring", "definition", "postconf", "postfix", "supported", "lookup", "tables", "postmap", "table", "manager", "regexp_table", "format", "of", "regular", "expression", "pcre_table", "pcre", "cidr_table", "cidr", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "locate", "this", "information", "database_readme", "overview", "bugs", "the", "protocol", "limits", "are", "not", "yet", "configurable", "license", "secure", "mailer", "must", "be", "distributed", "with", "software", "history", "socketmap", "support", "was", "introduced", "version", "2", "10", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
manpage_name = "socketmap_table"
manpage_section = "5"
+++

SOCKETMAP_TABLE 5


## NAME

socketmap_table
-
Postfix socketmap table lookup client

## SYNOPSIS

.na

```
postmap \-q "string" socketmap:inet:host:port:name
.br
postmap \-q "string" socketmap:unix:pathname:name

postmap \-q \- socketmap:inet:host:port:name <inputfile
.br
postmap \-q \- socketmap:unix:pathname:name <inputfile
.SH DESCRIPTION
.ad
```

The Postfix mail system uses optional tables for address
rewriting. mail routing or policy lookup.

The Postfix socketmap client expects TCP endpoint names of
the form **inet:**host**:**port**:**name, or
UNIX-domain endpoints of the form **unix:**pathname**:**name.
In both cases, *name* specifies the name field in a
socketmap client request (see "REQUEST FORMAT" below).

## PROTOCOL

.na

```
.ad
```

Socketmaps use a simple protocol: the client sends one
request, and the server sends one reply.  Each request and
reply are sent as one netstring object.

## REQUEST FORMAT

.na

```
.ad
```

The socketmap protocol supports only the lookup request.
The request has the following form:


- \fB\fIname\fB <space>
Search the named socketmap for the specified key.

Postfix will not generate partial search keys such as domain
names without one or more subdomains, network addresses
without one or more least-significant octets, or email
addresses without the localpart, address extension or domain
portion. This behavior is also found with cidr:, pcre:, and
regexp: tables.

## REPLY FORMAT

.na

```
.ad
```

The Postfix socketmap client requires that replies are not
longer than 100000 characters (not including the netstring
encapsulation). Replies must have the following form:

- \fBOK <space>
The requested data was found.

- \fBNOTFOUND
The requested data was not found.

- \fBTEMP <space>

- \fBTIMEOUT <space>

- \fBPERM <space>
The request failed. The reason, if non-empty, is descriptive
text.

## SECURITY

.na

```
This map cannot be used for security\-sensitive information,
because neither the connection nor the server are authenticated.
.SH "SEE ALSO"
.na

```
http://cr.yp.to/proto/netstrings.txt, netstring definition
postconf(1), Postfix supported lookup tables
postmap(1), Postfix lookup table manager
regexp_table(5), format of regular expression tables
pcre_table(5), format of PCRE tables
cidr_table(5), format of CIDR tables
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
DATABASE_README, Postfix lookup table overview
.SH BUGS
.ad
```

The protocol limits are not yet configurable.

## LICENSE

.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## HISTORY


Socketmap support was introduced with Postfix version 2.10.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
