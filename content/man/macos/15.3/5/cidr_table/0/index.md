+++
manpage_name = "cidr_table"
operating_system = "macos"
detected_package_version = "0"
author = "None Specified"
description = "The Postfix mail system uses optional lookup tables. These tables are usually in dbm or db format. Alternatively, lookup tables can be specified in CIDR (Classless Inter-Domain Routing) form. In this case, each input is compared against a list of ..."
date = "Sun Feb 16 04:48:11 2025"
title = "cidr_table(5)"
manpage_format = "troff"
operating_system_version = "15.3"
manpage_section = "5"
keywords = ["na", "postmap", "postfix", "lookup", "table", "manager", "regexp_table", "format", "of", "regular", "expression", "tables", "pcre_table", "pcre", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "database_readme", "overview", "history", "cidr", "support", "was", "introduced", "with", "version", "2", "1", "author", "the", "code", "originally", "written", "by", "jozsef", "kadlecsik", "kfki", "research", "institute", "for", "particle", "and", "nuclear", "physics", "pob", "49", "1525", "budapest", "hungary", "adopted", "adapted", "wietse", "venema", "ibm", "t", "j", "watson", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
+++

CIDR_TABLE 5


## NAME

cidr_table
-
format of Postfix CIDR tables

## SYNOPSIS

.na

```
postmap \-q "string" cidr:/etc/postfix/filename

postmap \-q \- cidr:/etc/postfix/filename <inputfile
.SH DESCRIPTION
.ad
```

The Postfix mail system uses optional lookup tables.
These tables are usually in **dbm** or **db** format.
Alternatively, lookup tables can be specified in CIDR
(Classless Inter-Domain Routing) form. In this case, each
input is compared against a list of patterns. When a match
is found, the corresponding result is returned and the search
is terminated.

To find out what types of lookup tables your Postfix system
supports use the "**postconf -m**" command.

To test lookup tables, use the "**postmap -q**" command as
described in the SYNOPSIS above.

## TABLE FORMAT

.na

```
.ad
```

The general form of a Postfix CIDR table is:

- \fIpattern    
When a search string matches the specified *pattern*, use
the corresponding *result* value. The *pattern* must be
in *network/prefix* or *network_address* form (see
ADDRESS PATTERN SYNTAX below).

- \fB!\fIpattern    
When a search string does not match the specified *pattern*,
use the specified *result* value. The *pattern* must
be in *network/prefix* or *network_address* form (see
ADDRESS PATTERN SYNTAX below).

This feature is available in Postfix 3.2 and later.

- \fBif
"**endif**"
When a search string matches the specified *pattern*, match
that search string against the patterns between **if** and
**endif**.  The *pattern* must be in *network/prefix* or
*network_address* form (see ADDRESS PATTERN SYNTAX below). The
**if**..**endif** can nest.

Note: do not prepend whitespace to text between
**if**..**endif**.

This feature is available in Postfix 3.2 and later.

- \fBif
"**endif**"
When a search string does not match the specified *pattern*,
match that search string against the patterns between **if** and
**endif**. The *pattern* must be in *network/prefix* or
*network_address* form (see ADDRESS PATTERN SYNTAX below). The
**if**..**endif** can nest.

Note: do not prepend whitespace to text between
**if**..**endif**.

This feature is available in Postfix 3.2 and later.

- blank lines and
Empty lines and whitespace-only lines are ignored, as
are lines whose first non-whitespace character is a `#'.

- multi-line
A logical line starts with non-whitespace text. A line that
starts with whitespace continues a logical line.

## TABLE SEARCH ORDER

.na

```
.ad
```

Patterns are applied in the order as specified in the table, until a
pattern is found that matches the search string.

## ADDRESS PATTERN SYNTAX

.na

```
.ad
```

Postfix CIDR tables are pattern-based. A pattern is either
a *network_address* which requires an exact match, or a
*network_address/prefix_length* where the *prefix_length*
part specifies the length of the *network_address* prefix
that must be matched (the other bits in the *network_address*
part must be zero).

An IPv4 network address is a sequence of four decimal octets
separated by ".", and an IPv6 network address is a sequence
of three to eight hexadecimal octet pairs separated by ":"
or "::", where the latter is short-hand for a sequence of
one or more all-zero octet pairs. The pattern 0.0.0.0/0
matches every IPv4 address, and ::/0 matches every IPv6
address.  IPv6 support is available in Postfix 2.2 and
later.

Before comparisons are made, lookup keys and table entries
are converted from string to binary. Therefore, IPv6 patterns
will be matched regardless of leading zeros (a leading zero in
an IPv4 address octet indicates octal notation).

Note: address information may be enclosed inside "[]" but
this form is not required.

## EXAMPLE SMTPD ACCESS MAP

.na

```

```
/etc/postfix/main.cf:
    smtpd_client_restrictions = ... cidr:/etc/postfix/client.cidr ...

/etc/postfix/client.cidr:
    # Rule order matters. Put more specific whitelist entries
    # before more general blacklist entries.
    192.168.1.1             OK
    192.168.0.0/16          REJECT
    2001:db8::1             OK
    2001:db8::/32           REJECT
```


## SEE ALSO

.na

```
postmap(1), Postfix lookup table manager
regexp_table(5), format of regular expression tables
pcre_table(5), format of PCRE tables
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
DATABASE_README, Postfix lookup table overview
.SH HISTORY
.ad
```

CIDR table support was introduced with Postfix version 2.1.

## AUTHOR(S)

.na

```
The CIDR table lookup code was originally written by:
Jozsef Kadlecsik
KFKI Research Institute for Particle and Nuclear Physics
POB. 49
1525 Budapest, Hungary

Adopted and adapted by:
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
