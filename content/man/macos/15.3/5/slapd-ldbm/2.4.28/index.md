+++
detected_package_version = "2.4.28"
title = "slapd-ldbm(5)"
author = "The OpenLDAP Foundation All Rights Reserved."
date = "2011-01-01"
operating_system = "macos"
manpage_name = "slapd-ldbm"
operating_system_version = "15.3"
manpage_format = "troff"
keywords = ["slapd", "8", "slapd-bdb", "5", "backends", "acknowledgements", "shared", "project", "acknowledgement", "text", "openldap", "software", "is", "developed", "and", "maintained", "by", "the", "http", "www", "org", "derived", "from", "university", "of", "michigan", "ldap", "3", "release"]
manpage_section = "5"
description = "LDBM was the original database backend to and was supported up to OpenLDAP 2.3. It has been superseded by the more robust BDB and HDB backends."
+++

SLAPD-LDBM 5 "2011/11/24" "OpenLDAP 2.4.28"

## NAME

slapd-ldbm - Discontinued LDBM backend to slapd

## SYNOPSIS

/etc/openldap/slapd.conf

## DESCRIPTION

LDBM was the original database backend to
slapd (8),
and was supported up to OpenLDAP 2.3.
It has been superseded by the more robust BDB and HDB backends.


## SEE ALSO

slapd (8),
slapd-bdb (5),
slapd.backends (5).

## ACKNOWLEDGEMENTS

"OpenLDAP Software"
is developed and maintained by The OpenLDAP Project <http://www.openldap.org/>.
"OpenLDAP Software"
is derived from University of Michigan LDAP 3.3 Release.
