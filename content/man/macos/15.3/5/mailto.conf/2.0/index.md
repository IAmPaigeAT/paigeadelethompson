+++
manpage_format = "troff"
keywords = ["cupsd", "8", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
manpage_section = "5"
operating_system_version = "15.3"
operating_system = "macos"
detected_package_version = "2.0"
author = "None Specified"
date = "Sun Feb 16 04:48:10 2025"
title = "mailto.conf(5)"
description = "The mailto.conf file defines the local mail server and email notification preferences for CUPS. Each line in the file can be a configuration directive, a blank line, or a comment. Configuration directives typically consist of a name and zero or more..."
manpage_name = "mailto.conf"
+++

mailto.conf 5 "CUPS" "26 April 2019" "Apple Inc."

## NAME

mailto.conf - configuration file for cups email notifier

## DESCRIPTION

The **mailto.conf** file defines the local mail server and email notification preferences for CUPS.

Each line in the file can be a configuration directive, a blank line, or a comment.
Configuration directives typically consist of a name and zero or more values separated by whitespace.
The configuration directive name and values are case-insensitive.
Comment lines start with the # character.

### DIRECTIVES


**Cc **cc-address@domain.com
Specifies an additional recipient for all email notifications.

**From **from-address@domain.com
Specifies the sender of email notifications.

**Sendmail **sendmail command and options
Specifies the sendmail command to use when sending email notifications.
Only one *Sendmail* or *SMTPServer* line may be present in the **mailto.conf** file.
If multiple lines are present, only the last one is used.

**SMTPServer **servername
Specifies a SMTP server to send email notifications to.
Only one *Sendmail* or *SMTPServer* line may be present in the **mailto.conf** file.
If multiple lines are present, only the last one is used.

**Subject **subject-prefix
Specifies a prefix string for the subject line of an email notification.

## SEE ALSO

cupsd (8),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
