+++
author = "The OpenLDAP Foundation All Rights Reserved."
manpage_name = "slapo-valsort"
operating_system = "macos"
description = "The Value Sorting overlay can be used with a backend database to sort the values of specific multi-valued attributes within a subtree. The sorting occurs whenever the attributes are returned in a search response. Sorting can be specified in ascendi..."
detected_package_version = "2.4.28"
title = "slapo-valsort(5)"
manpage_format = "troff"
manpage_section = "5"
operating_system_version = "15.3"
keywords = ["slapd", "conf", "5", "slapd-config", "acknowledgements", "this", "module", "was", "written", "in", "2005", "by", "howard", "chu", "of", "symas", "corporation", "the", "work", "sponsored", "stanford", "university"]
date = "2011-01-01"
+++

SLAPO-VALSORT 5 "2011/11/24" "OpenLDAP 2.4.28"

## NAME

slapo-valsort - Value Sorting overlay to slapd

## SYNOPSIS

/etc/openldap/slapd.conf

## DESCRIPTION

The Value Sorting overlay can be used with a backend database to sort the
values of specific multi-valued attributes within a subtree. The sorting
occurs whenever the attributes are returned in a search response.

Sorting can be specified in ascending or descending order, using either
numeric or alphanumeric sort methods. Additionally, a "weighted" sort can
be specified, which uses a numeric weight prepended to the attribute values.
The weighted sort is always performed in ascending order, but may be combined
with the other methods for values that all have equal weights. The weight
is specified by prepending an integer weight \{<*weight*>\}
in front of each value of the attribute for which weighted sorting is
desired. This weighting factor is stripped off and never returned in
search results.


## CONFIGURATION

These
slapd.conf
options apply to the Value Sorting overlay.
They should appear after the
overlay
directive.

valsort-attr <*attribute*> <*baseDN*> (<*sort-method*> | weighted [<*sort-method*>])
Configure a sorting method for the specified
attribute
in the subtree rooted at
baseDN .
The
sort-method
may be one of
alpha-ascend ,
alpha-descend ,
numeric-ascend ,
or
numeric-descend .
If the special
weighted
method is specified, a secondary
sort-method
may also be specified. It is an
error to specify an alphanumeric
sort-method
for an attribute with Integer
or NumericString syntax, and it is an error to specify a numeric
sort-method
for an attribute with a syntax other than Integer or NumericString.

## EXAMPLES



```
	database bdb
	suffix dc=example,dc=com
	...
	overlay valsort
	valsort\-attr member ou=groups,dc=example,dc=com alpha\-ascend
```



## FILES


*/etc/openldap/slapd.conf*
default **slapd** configuration file

## SEE ALSO

slapd.conf (5),
slapd-config (5).

## ACKNOWLEDGEMENTS

.P
This module was written in 2005 by Howard Chu of Symas Corporation. The
work was sponsored by Stanford University.
