+++
operating_system_version = "15.3"
manpage_section = "5"
manpage_name = "classes.conf"
operating_system = "macos"
detected_package_version = "2.0"
date = "Sun Feb 16 04:48:11 2025"
description = "The classes.conf file defines the local printer classes that are available. It is normally located in the /etc/cups directory and is maintained by the program. This file is not intended to be edited or managed manually. The name, location, and for..."
manpage_format = "troff"
title = "classes.conf(5)"
author = "None Specified"
keywords = ["cupsd", "8", "conf", "5", "mime", "convs", "types", "printers", "subscriptions", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
+++

classes.conf 5 "CUPS" "26 April 2019" "Apple Inc."

## NAME

classes.conf - class configuration file for cups

## DESCRIPTION

The **classes.conf** file defines the local printer classes that are available.
It is normally located in the */etc/cups* directory and is maintained by the
cupsd (8)
program.
This file is not intended to be edited or managed manually.

## NOTES

The name, location, and format of this file are an implementation detail that will change in future releases of CUPS.

## SEE ALSO

cupsd (8),
cupsd.conf (5),
mime.convs (5),
mime.types (5),
printers.conf (5),
subscriptions.conf (5),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
