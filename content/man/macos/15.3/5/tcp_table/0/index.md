+++
description = "The Postfix mail system uses optional tables for address rewriting or mail routing. These tables are usually in dbm or db format. Alternatively, table lookups can be directed to a TCP server."
operating_system = "macos"
author = "None Specified"
keywords = ["na", "postmap", "postfix", "lookup", "table", "manager", "regexp_table", "format", "of", "regular", "expression", "tables", "pcre_table", "pcre", "cidr_table", "cidr", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "database_readme", "overview", "license", "the", "secure", "mailer", "must", "be", "distributed", "with", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
manpage_name = "tcp_table"
date = "Sun Feb 16 04:48:10 2025"
detected_package_version = "0"
title = "tcp_table(5)"
manpage_section = "5"
operating_system_version = "15.3"
manpage_format = "troff"
+++

TCP_TABLE 5


## NAME

tcp_table
-
Postfix client/server table lookup protocol

## SYNOPSIS

.na

```
postmap \-q "string" tcp:host:port

postmap \-q \- tcp:host:port <inputfile
.SH DESCRIPTION
.ad
```

The Postfix mail system uses optional tables for address
rewriting or mail routing. These tables are usually in
**dbm** or **db** format. Alternatively, table lookups
can be directed to a TCP server.

To find out what types of lookup tables your Postfix system
supports use the "**postconf -m**" command.

To test lookup tables, use the "**postmap -q**" command as
described in the SYNOPSIS above.

## PROTOCOL DESCRIPTION

.na

```
.ad
```

The TCP map class implements a very simple protocol: the client
sends a request, and the server sends one reply. Requests and
replies are sent as one line of ASCII text, terminated by the
ASCII newline character. Request and reply parameters (see below)
are separated by whitespace.

Send and receive operations must complete in 100 seconds.

## REQUEST FORMAT

.na

```
.ad
```

Each request specifies a command, a lookup key, and possibly a
lookup result.

- \fBget SPACE \fIkey
Look up data under the specified key.

- \fBput SPACE \fIkey SPACE \fIvalue
This request is currently not implemented.

## REPLY FORMAT

.na

```
.ad
```

Each reply specifies a status code and text. Replies must be no
longer than 4096 characters including the newline terminator.

- \fB500 SPACE \fItext
In case of a lookup request, the requested data does not exist.
In case of an update request, the request was rejected.
The text describes the nature of the problem.

- \fB400 SPACE \fItext
This indicates an error condition. The text describes the nature of
the problem. The client should retry the request later.

- \fB200 SPACE \fItext
The request was successful. In the case of a lookup request,
the text contains an encoded version of the requested data.

## ENCODING

.na

```
.ad
```

In request and reply parameters, the character %, each non-printing
character, and each whitespace character must be replaced by %XX,
where XX is the corresponding ASCII hexadecimal character value. The
hexadecimal codes can be specified in any case (upper, lower, mixed).

The Postfix client always encodes a request.
The server may omit the encoding as long as the reply
is guaranteed to not contain the % or NEWLINE character.

## SECURITY

.na

```
.ad
```

Do not use TCP lookup tables for security critical purposes.
The client-server connection is not protected and the server
is not authenticated.

## BUGS


Only the lookup method is currently implemented.

The client does not hang up when the connection is idle for
a long time.

## SEE ALSO

.na

```
postmap(1), Postfix lookup table manager
regexp_table(5), format of regular expression tables
pcre_table(5), format of PCRE tables
cidr_table(5), format of CIDR tables
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
DATABASE_README, Postfix lookup table overview
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
