+++
operating_system = "macos"
date = "Sun Feb 16 04:48:10 2025"
title = "pcre_table(5)"
operating_system_version = "15.3"
manpage_section = "5"
manpage_format = "troff"
description = "The Postfix mail system uses optional tables for address rewriting, mail routing, or access control. These tables are usually in dbm or db format."
manpage_name = "pcre_table"
detected_package_version = "0"
author = "None Specified"
keywords = ["na", "postmap", "postfix", "lookup", "table", "manager", "postconf", "configuration", "parameters", "regexp_table", "format", "of", "posix", "regular", "expression", "tables", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "database_readme", "overview", "author", "the", "pcre", "code", "was", "originally", "written", "by", "andrew", "mcnamara", "andrewm", "connect", "com", "au", "pty", "ltd", "level", "3", "213", "miller", "st", "north", "sydney", "nsw", "australia", "adopted", "and", "adapted", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
+++

PCRE_TABLE 5


## NAME

pcre_table
-
format of Postfix PCRE tables

## SYNOPSIS

.na

```
postmap \-q "string" pcre:/etc/postfix/filename

postmap \-q \- pcre:/etc/postfix/filename <inputfile

postmap \-hmq \- pcre:/etc/postfix/filename <inputfile

postmap \-bmq \- pcre:/etc/postfix/filename <inputfile
.SH DESCRIPTION
.ad
```

The Postfix mail system uses optional tables for address
rewriting, mail routing, or access control. These tables
are usually in **dbm** or **db** format.

Alternatively, lookup tables can be specified in Perl Compatible
Regular Expression form. In this case, each input is compared
against a list of patterns. When a match is found, the
corresponding result is returned and the search is terminated.

To find out what types of lookup tables your Postfix system
supports use the "**postconf -m**" command.

To test lookup tables, use the "**postmap -q**" command
as described in the SYNOPSIS above. Use "\fBpostmap -hmq
- <*file*" for header_checks(5) patterns, and
"**postmap -bmq -** <*file*" for body_checks(5)
(Postfix 2.6 and later).

## COMPATIBILITY

.na

```
.ad
```

With Postfix version 2.2 and earlier specify "\fBpostmap
-fq" to query a table that contains case sensitive
patterns. Patterns are case insensitive by default.

## TABLE FORMAT

.na

```
.ad
```

The general form of a PCRE table is:

- \fB/\fIpattern\fB/\fIflags
When *pattern* matches the input string, use
the corresponding *result* value.

- \fB!/\fIpattern\fB/\fIflags
When *pattern* does **not** match the input string, use
the corresponding *result* value.

- \fBif
"**endif**"
If the input string matches /*pattern*/, then match that
input string against the patterns between **if** and
**endif**.  The **if**..**endif** can nest.

Note: do not prepend whitespace to patterns inside
**if**..**endif**.

This feature is available in Postfix 2.1 and later.

- \fBif
"**endif**"
If the input string does not match /*pattern*/, then
match that input string against the patterns between **if**
and **endif**. The **if**..**endif** can nest.

Note: do not prepend whitespace to patterns inside
**if**..**endif**.

This feature is available in Postfix 2.1 and later.

- blank lines and
Empty lines and whitespace-only lines are ignored, as
are lines whose first non-whitespace character is a `#'.

- multi-line
A logical line starts with non-whitespace text. A line that
starts with whitespace continues a logical line.

Each pattern is a perl-like regular expression. The expression
delimiter can be any non-alphanumerical character, except
whitespace or characters
that have special meaning (traditionally the forward slash is used).
The regular expression can contain whitespace.

By default, matching is case-insensitive, and newlines are not
treated as special characters. The behavior is controlled by flags,
which are toggled by appending one or more of the following
characters after the pattern:

- \fBi (default:
Toggles the case sensitivity flag. By default, matching is case
insensitive.

- \fBm (default:
Toggles the PCRE_MULTILINE flag. When this flag is on, the **^**
and **$** metacharacters match immediately after and immediately
before a newline character, respectively, in addition to
matching at the start and end of the subject string.

- \fBs (default:
Toggles the PCRE_DOTALL flag. When this flag is on, the **.**
metacharacter matches the newline character. With
Postfix versions prior to 2.0, the flag is off by
default, which is inconvenient for multi-line message header
matching.

- \fBx (default:
Toggles the pcre extended flag. When this flag is on, whitespace
characters in the pattern (other than in a character class)
are ignored.  To include a whitespace character as part of
the pattern, escape it with backslash.

Note: do not use **#**comment after patterns.

- \fBA (default:
Toggles the PCRE_ANCHORED flag.  When this flag is on,
the pattern is forced to be "anchored", that is, it is
constrained to match only at the start of the string which
is being searched (the "subject string"). This effect can
also be achieved by appropriate constructs in the pattern
itself.

- \fBE (default:
Toggles the PCRE_DOLLAR_ENDONLY flag. When this flag is on,
a **$** metacharacter in the pattern matches only at the
end of the subject string. Without this flag, a dollar also
matches immediately before the final character if it is a
newline character (but not before any other newline
characters). This flag is ignored if PCRE_MULTILINE
flag is set.

- \fBU (default:
Toggles the ungreedy matching flag.  When this flag is on,
the pattern matching engine inverts the "greediness" of
the quantifiers so that they are not greedy by default,
but become greedy if followed by "?".  This flag can also
set by a (?U) modifier within the pattern.

- \fBX (default:
Toggles the PCRE_EXTRA flag.
When this flag is on, any backslash in a pattern that is
followed by a letter that has no special meaning causes an
error, thus reserving these combinations for future expansion.

## SEARCH ORDER

.na

```
.ad
```

Patterns are applied in the order as specified in the table, until a
pattern is found that matches the input string.

Each pattern is applied to the entire input string.
Depending on the application, that string is an entire client
hostname, an entire client IP address, or an entire mail address.
Thus, no parent domain or parent network search is done, and
*user@domain* mail addresses are not broken up into their
*user* and *domain* constituent parts, nor is *user+foo*
broken up into *user* and *foo*.

## TEXT SUBSTITUTION

.na

```
.ad
```

Substitution of substrings (text that matches patterns
inside "()") from the matched expression into the result
string is requested with $1, $2, etc.; specify $$ to produce
a $ character as output.
The macros in the result string may need to be written as
$\{n\} or $(n) if they aren't followed by whitespace.

Note: since negated patterns (those preceded by **!**) return a
result when the expression does not match, substitutions are not
available for negated patterns.

## EXAMPLE SMTPD ACCESS MAP

.na

```
# Protect your outgoing majordomo exploders
/^(?!owner\-)(.*)\-outgoing@(.*)/ 550 Use ${1}@${2} instead

# Bounce friend@whatever, except when whatever is our domain (you would
# be better just bouncing all friend@ mail \- this is just an example).
/^(friend@(?!my\\.domain$).*)$/  550 Stick this in your pipe $1

# A multi\-line entry. The text is sent as one line.
#
/^noddy@my\\.domain$/
\ 550 This user is a funny one. You really don't want to send mail to
\ them as it only makes their head spin.
.SH "EXAMPLE HEADER FILTER MAP"
.na

```
/^Subject: make money fast/     REJECT
/^To: friend@public\\.com/       REJECT
.SH "EXAMPLE BODY FILTER MAP"
.na

```
# First skip over base 64 encoded text to save CPU cycles.
# Requires PCRE version 3.
~^[[:alnum:]+/]{60,}$~          OK

# Put your own body patterns here.
.SH "SEE ALSO"
.na

```
postmap(1), Postfix lookup table manager
postconf(5), configuration parameters
regexp_table(5), format of POSIX regular expression tables
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
DATABASE_README, Postfix lookup table overview
.SH "AUTHOR(S)"
.na

```
The PCRE table lookup code was originally written by:
Andrew McNamara
andrewm@connect.com.au
connect.com.au Pty. Ltd.
Level 3, 213 Miller St
North Sydney, NSW, Australia

Adopted and adapted by:
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
