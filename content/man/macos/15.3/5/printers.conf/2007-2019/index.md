+++
title = "printers.conf(5)"
manpage_format = "troff"
manpage_section = "5"
operating_system = "macos"
operating_system_version = "15.3"
keywords = ["classes", "conf", "5", "cups-files", "cupsd", "8", "mime", "convs", "types", "subscriptions", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
manpage_name = "printers.conf"
date = "Sun Feb 16 04:48:10 2025"
description = "The printers.conf file defines the local printers that are available. It is normally located in the /etc/cups directory and is maintained by the program. This file is not intended to be edited or managed manually. The name, location, and format of t..."
author = "Easy Software Products."
detected_package_version = "2007-2019"
+++

printers.conf 5 "CUPS" "26 April 2019" "Apple Inc."

## NAME

printers.conf - printer configuration file for cups

## DESCRIPTION

The **printers.conf** file defines the local printers that are available. It is normally located in the */etc/cups* directory and is maintained by the
cupsd (8)
program. This file is not intended to be edited or managed manually.

## NOTES

The name, location, and format of this file are an implementation detail that will change in future releases of CUPS.

## SEE ALSO

classes.conf (5),
cups-files.conf (5),
cupsd (8),
cupsd.conf (5),
mime.convs (5),
mime.types (5),
subscriptions.conf (5),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
