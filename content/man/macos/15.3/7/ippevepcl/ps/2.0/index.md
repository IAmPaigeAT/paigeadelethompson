+++
keywords = ["ippeveprinter", "8", "copyright", "co", "2019", "by", "apple", "inc"]
date = "Sun Feb 16 04:48:27 2025"
detected_package_version = "2.0"
manpage_section = "7"
description = "and are print commands for As with all print commands, these commands read either the filename specified on the command-line or from the standard input. Output is sent to the standard output. Status and progress messages are sent to the standard e..."
operating_system = "macos"
operating_system_version = "15.3"
manpage_format = "troff"
author = "None Specified"
title = "ippevepcl/ps(7)"
manpage_name = "ippevepcl/ps"
+++

ippevepcl/ps 7 "CUPS" "24 April 2019" "Apple Inc."

## NAME

ippevepcl/ps - pcl and postscript print commands for ippeveprinter

## SYNOPSIS

ippevepcl
[
filename
]
.br
ippeveps
[
filename
]

## DESCRIPTION

ippevepcl
and
ippeveps
are print commands for
ippeveprinter (1).
As with all print commands, these commands read either the filename specified on the command-line or from the standard input.
Output is sent to the standard output.
Status and progress messages are sent to the standard error.

ippevepcl
prints to B&W HP PCL laser printers and supports printing of HP PCL (application/vnd.hp-pcl), PWG Raster (image/pwg-raster), and Apple Raster (image/urf) print files.

ippeveps
print to Adobe PostScript printers and supports printing of PDF (application/pdf), PostScript (application/postscript), JPEG (image/jpeg), PWG Raster (image/pwg-raster), and Apple Raster (image/urf) print files.
Printer-specific commands are read from a supplied PPD file.
If no PPD file is specified, generic commands suitable for any Level 2 or Level 3 PostScript printer are used instead to specify duplex printing and media size.

## EXIT STATUS

These programs return 1 on error and 0 on success.

## ENVIRONMENT

These program inherit the environment provided by the
ippeveprinter
program.

## SEE ALSO

ippeveprinter (8)

## COPYRIGHT

Copyright \[co] 2019 by Apple Inc.
