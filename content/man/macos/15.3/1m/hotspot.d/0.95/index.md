+++
date = "2005-05-14"
keywords = ["dtrace"]
manpage_format = "troff"
author = "None Specified"
title = "hotspot.d(1m)"
operating_system = "macos"
manpage_name = "hotspot.d"
manpage_section = "1m"
description = "hotspot.d is a simple DTrace script to determine if disk activity is  occuring in the one place - a hotspot. This helps us understand the  systems usage of a disk, it does not imply that the existance or not  of a hotspot is good or bad (often m..."
operating_system_version = "15.3"
detected_package_version = "0.95"
+++

hotspot.d 1m  "May 14, 2005" "version 0.95" "USER COMMANDS"

## NAME

hotspot.d - print disk event by location. Uses DTrace.

## SYNOPSIS

hotspot.d

## DESCRIPTION

hotspot.d is a simple DTrace script to determine if disk activity is
occuring in the one place - a "hotspot". This helps us understand the
system's usage of a disk, it does not imply that the existance or not
of a hotspot is good or bad (often may be good, less seeking).

Since this uses DTrace, only users with root privileges can run this command.

## EXAMPLES


Sample until Ctrl-C is hit then print report,
#
hotspot.d


## FIELDS


Disk
disk instance name

Major
driver major number

Minor
driver minor number

value
location of disk event, megabytes

count
number of events


## DOCUMENTATION

See the DTraceToolkit for further documentation under the
Docs directory. The DTraceToolkit docs may include full worked
examples with verbose descriptions explaining the output.

## EXIT

hotspot.d will sample until Ctrl-C is hit.

## AUTHOR

Brendan Gregg
[Sydney, Australia]

## SEE ALSO

dtrace(1M)
