+++
date = "2005-01-27"
author = "None Specified"
keywords = ["uptime", "dtrace"]
manpage_format = "troff"
description = "The dispatcher queue length is an indication of CPU saturation. It is not an indicatior of utilisation - the CPUs may or may not be utilised when the dispatcher queue reports a length of zero."
manpage_name = "dispqlen.d"
detected_package_version = "0.80"
title = "dispqlen.d(1m)"
operating_system = "macos"
operating_system_version = "15.3"
manpage_section = "1m"
+++

dispqlen.d 1m  "Jun 27, 2005" "version 0.80" "USER COMMANDS"

## NAME

dispqlen.d - dispatcher queue length by CPU. Uses DTrace.

## SYNOPSIS

dispqlen.d

## DESCRIPTION

The dispatcher queue length is an indication of CPU saturation.
It is not an indicatior of utilisation - the CPUs may or may not be
utilised when the dispatcher queue reports a length of zero.

This script measures this activity by sampling at 1000 Hertz per CPU.

Since this uses DTrace, only users with root privileges can run this command.

## EXAMPLES


Print dispatcher queue length by CPU.
#
dispqlen.d


## DOCUMENTATION

See the DTraceToolkit for further documentation under the
Docs directory. The DTraceToolkit docs may include full worked
examples with verbose descriptions explaining the output.

## EXIT

dispqlen.d will sample until Ctrl-C is hit.

## AUTHOR

Brendan Gregg
[Sydney, Australia]

## SEE ALSO

uptime(1), dtrace(1M)

