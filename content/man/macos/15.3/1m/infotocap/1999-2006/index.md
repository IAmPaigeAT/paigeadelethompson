+++
manpage_name = "infotocap"
manpage_section = "1M"
detected_package_version = "1999-2006"
keywords = ["fbcurses", "fbtic", "fbinfocmp", "fbterminfo", "n", "this", "describes", "fbncurses", "version", "5", "7", "patch", "20081102", "author", "eric", "s", "raymond", "esr", "snark", "thyrsus", "com", "and", "br", "thomas", "e", "dickey", "invisible-island", "net"]
title = "infotocap(1M)"
date = "Sun Feb 16 04:48:18 2025"
operating_system_version = "15.3"
description = "infotocap looks in each given text file for terminfo descriptions. For each terminfo description found, an equivalent termcap description is written to standard output. Terminfo use capabilities are translated directly to termcap tc capabilities...."
operating_system = "macos"
manpage_format = "troff"
author = ",2010 Free Software Foundation, Inc. *"
+++

'\" t
infotocap 1M ""
.ds n 5
.ds d /usr/share/terminfo

## NAME

**infotocap** - convert a *terminfo* description into a *termcap* description

## SYNOPSIS

**infotocap** [**-v***n* *width*]  [**-V**] [**-1**] [**-w** *width*] *file* . . .

## DESCRIPTION

**infotocap** looks in each given text
*file* for **terminfo** descriptions.
For each terminfo description found,
an equivalent **termcap** description is written to standard output.
Terminfo **use** capabilities are translated directly to termcap
**tc** capabilities.

**-v**
print out tracing information on standard error as the program runs.

**-V**
print out the version of the program in use on standard error and exit.

**-1**
cause the fields to print out one to a line.
Otherwise, the fields
will be printed several to a line to a maximum width of 60 characters.

**-w**
change the output to *width* characters.

## FILES


\*d
Compiled terminal description database.

## NOTES

This utility is actually a link to *tic*, running in *-C* mode.
You can use other *tic* options such as **-f** and  **-x**.

## SEE ALSO

**curses**(3X),
**tic**(1M),
**infocmp**(1M),
**terminfo**(\*n)

This describes **ncurses**
version 5.7 (patch 20081102).

## AUTHOR

Eric S. Raymond <esr@snark.thyrsus.com>
and
.br
Thomas E. Dickey <dickey@invisible-island.net>
