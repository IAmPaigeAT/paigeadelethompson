+++
detected_package_version = "0.70"
manpage_section = "1m"
date = "2005-01-08"
description = "This prints the last few system calls for processes matching the given name, when they exit. This makes use of a ring buffer so that the impact on the system is minimised."
title = "lastwords(1m)"
keywords = ["dtruss", "dtrace"]
author = "None Specified"
manpage_format = "troff"
operating_system = "macos"
manpage_name = "lastwords"
operating_system_version = "15.3"
+++

lastwords 1m  "Jun 08, 2005" "version 0.70" "USER COMMANDS"

## NAME

lastwords - print syscalls before exit. Uses DTrace.

## SYNOPSIS

lastwords command

## DESCRIPTION

This prints the last few system calls for processes matching
the given name, when they exit. This makes use of a ring buffer
so that the impact on the system is minimised.

Since this uses DTrace, only users with root privileges can run this command.

## EXAMPLES


Catch last few syscalls for dying netscape processes,
#
lastwords netscape


## FIELDS


TIME
time of syscall return, ns

PID
process ID

EXEC
process name (execname)

SYSCALL
system call

RETURN
return value for the system call

ERR
errno for the system call


## BASED ON

/usr/demo/dtrace/ring.d


## DOCUMENTATION

DTrace Guide "Buffers and Buffering" chapter (docs.sun.com)

See the DTraceToolkit for further documentation under the
Docs directory. The DTraceToolkit docs may include full worked
examples with verbose descriptions explaining the output.

## EXIT

lastwords will sample until a command with that name exits.

## SEE ALSO

dtruss(1M), dtrace(1M)

