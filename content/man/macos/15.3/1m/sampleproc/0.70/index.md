+++
manpage_name = "sampleproc"
manpage_format = "troff"
operating_system = "macos"
operating_system_version = "15.3"
author = "None Specified"
description = "This program samples which process is on each CPU, at a particular configurable rate. This can be used as an estimate for which process is consuming the most CPU time."
manpage_section = "1m"
keywords = ["dtrace"]
title = "sampleproc(1m)"
date = "2005-01-09"
detected_package_version = "0.70"
+++

sampleproc 1m  "Jun 09, 2005" "version 0.70" "USER COMMANDS"

## NAME

sampleproc - sample processes on the CPUs. Uses DTrace.

## SYNOPSIS

sampleproc [hertz]

## DESCRIPTION

This program samples which process is on each CPU, at a particular
configurable rate. This can be used as an estimate for which process
is consuming the most CPU time.

Since this uses DTrace, only users with root privileges can run this command.

## EXAMPLES


Sample at 100 hertz,
#
sampleproc

Sample at 400 hertz,
#
sampleproc
400


## FIELDS


PID
process ID

COMMAND
command name

COUNT
number of samples

PERCENT
percent of CPU usage


## BASED ON

/usr/demo/dtrace/prof.d


## DOCUMENTATION

DTrace Guide "profile Provider" chapter (docs.sun.com)

See the DTraceToolkit for further documentation under the
Docs directory. The DTraceToolkit docs may include full worked
examples with verbose descriptions explaining the output.

## EXIT

sampleproc will sample until Ctrl-C is hit.

## SEE ALSO

dtrace(1M)

