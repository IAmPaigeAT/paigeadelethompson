+++
author = "None Specified"
detected_package_version = "0.80"
date = "2005-01-09"
description = "This script prints the number of new processes created per second."
manpage_name = "pidpersec.d"
operating_system_version = "15.3"
keywords = ["execsnoop", "dtrace"]
manpage_section = "1m"
title = "pidpersec.d(1m)"
manpage_format = "troff"
operating_system = "macos"
+++

pidpersec.d 1m  "Jun 09, 2005" "version 0.80" "USER COMMANDS"

## NAME

pidpersec.d - print new PIDs per sec. Uses DTrace.

## SYNOPSIS

pidpersec.d

## DESCRIPTION

This script prints the number of new processes created per second.

Since this uses DTrace, only users with root privileges can run this command.

## EXAMPLES


Print PID statistics per second,
#
pidpersec.d


## FIELDS


TIME
time, as a string

LASTPID
last PID created

PID/s
Number of processes created per second


## DOCUMENTATION

See the DTraceToolkit for further documentation under the
Docs directory. The DTraceToolkit docs may include full worked
examples with verbose descriptions explaining the output.

## EXIT

pidpersec.d will run until Ctrl-C is hit.

## AUTHOR

Brendan Gregg
[Sydney, Australia]

## SEE ALSO

execsnoop(1M), dtrace(1M)
