+++
title = "cpuwalk.d(1m)"
date = "2005-01-22"
operating_system = "macos"
manpage_section = "1m"
description = "This program is for multi-CPU servers, and can help identify if a process is running on multiple CPUs concurrently or not."
manpage_format = "troff"
author = "None Specified"
keywords = ["threaded", "d", "dtrace"]
operating_system_version = "15.3"
manpage_name = "cpuwalk.d"
detected_package_version = "0.90"
+++

cpuwalk.d 1m  "Sep 22, 2005" "version 0.90" "USER COMMANDS"

## NAME

cpuwalk.d - Measure which CPUs a process runs on. Uses DTrace.

## SYNOPSIS

cpuwalk.d [duration]

## DESCRIPTION

This program is for multi-CPU servers, and can help identify if a process
is running on multiple CPUs concurrently or not.

A duration may be specified in seconds.

Since this uses DTrace, only users with root privileges can run this command.

## EXAMPLES


this runs until Ctrl-C is hit,
#
cpuwalk.d


run for 5 seconds,
#
cpuwalk.d
5


## FIELDS


PID
process ID

CMD
process name

value
CPU id

count
number of samples (sample at 100 hz)


## DOCUMENTATION

See the DTraceToolkit for further documentation under the
Docs directory. The DTraceToolkit docs may include full worked
examples with verbose descriptions explaining the output.

## EXIT

cpuwalk.d will run until Ctrl-C is hit, or the duration specified
is reached.

## SEE ALSO

threaded.d(1M), dtrace(1M)

