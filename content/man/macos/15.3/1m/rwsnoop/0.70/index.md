+++
manpage_section = "1m"
description = "This is measuring reads and writes at the application level. This matches the syscalls read, write, pread and pwrite."
operating_system = "macos"
keywords = ["rwtop", "dtrace"]
manpage_format = "troff"
detected_package_version = "0.70"
title = "rwsnoop(1m)"
operating_system_version = "15.3"
date = "2005-01-24"
author = "None Specified"
manpage_name = "rwsnoop"
+++

rwsnoop 1m  "Jul 24, 2005" "version 0.70" "USER COMMANDS"

## NAME

rwsnoop - snoop read/write events. Uses DTrace.

## SYNOPSIS

rwsnoop
[-jPtvZ] [-n name] [-p PID]

## DESCRIPTION

This is measuring reads and writes at the application level. This
matches the syscalls read, write, pread and pwrite.

Since this uses DTrace, only users with root privileges can run this command.

## OPTIONS


-j
print project ID

-P
print parent process ID

-t
print timestamp, us

-v
print time, string

-Z
print zone ID

-n name
process name to track

-p PID
PID to track


## EXAMPLES


Default output,
#
rwsnoop

Print zone ID,
#
rwsnoop
-\Z

Monitor processes named "bash",
#
rwsnoop
-n bash


## FIELDS


TIME
timestamp, us

TIMESTR
time, string

ZONE
zone ID

PROJ
project ID

UID
user ID

PID
process ID

PPID
parent process ID

CMD
command name for the process

D
direction, Read or Write

BYTES
total bytes during sample

FILE
filename, if file based.
Reads and writes that are not file based, for example with sockets, will
print "<unknown>" as the filename.


## DOCUMENTATION

See the DTraceToolkit for further documentation under the
Docs directory. The DTraceToolkit docs may include full worked
examples with verbose descriptions explaining the output.

## EXIT

rwsnoop will run forever until Ctrl-C is hit.

## AUTHOR

Brendan Gregg
[Sydney, Australia]

## SEE ALSO

rwtop(1M), dtrace(1M)

