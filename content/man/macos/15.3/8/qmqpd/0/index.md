+++
operating_system_version = "15.3"
description = "The Postfix QMQP server receives one message per connection. Each message is piped through the cleanup(8) daemon, and is placed into the incoming queue as one single queue file.  The program expects to be run from the master(8) process manager."
title = "qmqpd(8)"
operating_system = "macos"
author = "None Specified"
manpage_format = "troff"
manpage_section = "8"
keywords = ["na", "http", "cr", "yp", "to", "proto", "qmqp", "html", "protocol", "cleanup", "message", "canonicalization", "master", "process", "manager", "syslogd", "system", "logging", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "locate", "this", "information", "qmqp_readme", "postfix", "ezmlm-idx", "howto", "license", "the", "secure", "mailer", "must", "be", "distributed", "with", "software", "history", "qmqpd", "service", "was", "introduced", "version", "1", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
date = "Sun Feb 16 04:48:12 2025"
detected_package_version = "0"
manpage_name = "qmqpd"
+++

QMQPD 8


## NAME

qmqpd
-
Postfix QMQP server

## SYNOPSIS

.na

```
qmqpd [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The Postfix QMQP server receives one message per connection.
Each message is piped through the **cleanup**(8)
daemon, and is placed into the **incoming** queue as one
single queue file.  The program expects to be run from the
**master**(8) process manager.

The QMQP server implements one access policy: only explicitly
authorized client hosts are allowed to use the service.

## SECURITY

.na

```
.ad
```

The QMQP server is moderately security-sensitive. It talks to QMQP
clients and to DNS servers on the network. The QMQP server can be
run chrooted at fixed low privilege.

## DIAGNOSTICS


Problems and transactions are logged to **syslogd**(8).

## BUGS


The QMQP protocol provides only one server reply per message
delivery. It is therefore not possible to reject individual
recipients.

The QMQP protocol requires the server to receive the entire
message before replying. If a message is malformed, or if any
netstring component is longer than acceptable, Postfix replies
immediately and closes the connection. It is left up to the
client to handle the situation.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are picked up automatically, as **qmqpd**(8)
processes run for only a limited amount of time. Use the command
"**postfix reload**" to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

## CONTENT INSPECTION CONTROLS

.na

```
.ad
```


- \fBcontent_filter
After the message is queued, send the entire message to the
specified *transport:destination*.

- \fBreceive_override_options
Enable or disable recipient validation, built-in content
filtering, or address mapping.

## SMTPUTF8 CONTROLS

.na

```
.ad
```

Preliminary SMTPUTF8 support is introduced with Postfix 3.0.

- \fBsmtputf8_enable
Enable preliminary SMTPUTF8 support for the protocols described
in RFC 6531..6533.

- \fBsmtputf8_autodetect_classes (sendmail,
Detect that a message requires SMTPUTF8 support for the specified
mail origin classes.

Available in Postfix version 3.2 and later:

- \fBenable_idna2003_compatibility
Enable 'transitional' compatibility between IDNA2003 and IDNA2008,
when converting UTF-8 domain names to/from the ASCII form that is
used for DNS lookups.

## RESOURCE AND RATE CONTROLS

.na

```
.ad
```


- \fBline_length_limit
Upon input, long lines are chopped up into pieces of at most
this length; upon delivery, long lines are reconstructed.

- \fBhopcount_limit
The maximal number of Received:  message headers that is allowed
in the primary message headers.

- \fBmessage_size_limit
The maximal size in bytes of a message, including envelope information.

- \fBqmqpd_timeout
The time limit for sending or receiving information over the network.

## TROUBLE SHOOTING CONTROLS

.na

```
.ad
```


- \fBdebug_peer_level
The increment in verbose logging level when a remote client or
server matches a pattern in the debug_peer_list parameter.

- \fBdebug_peer_list
Optional list of remote client or server hostname or network
address patterns that cause the verbose logging level to increase
by the amount specified in $debug_peer_level.

- \fBsoft_bounce
Safety net to keep mail queued that would otherwise be returned to
the sender.

## TARPIT CONTROLS

.na

```
.ad
```


- \fBqmqpd_error_delay
How long the Postfix QMQP server will pause before sending a negative
reply to the remote QMQP client.

## MISCELLANEOUS CONTROLS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqmqpd_authorized_clients
What remote QMQP clients are allowed to connect to the Postfix QMQP
server port.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

- \fBverp_delimiter_filter
The characters Postfix accepts as VERP delimiter characters on the
Postfix **sendmail**(1) command line and in SMTP commands.

Available in Postfix version 2.5 and later:

- \fBqmqpd_client_port_logging
Enable logging of the remote QMQP client port in addition to
the hostname and IP address.

## SEE ALSO

.na

```
http://cr.yp.to/proto/qmqp.html, QMQP protocol
cleanup(8), message canonicalization
master(8), process manager
syslogd(8), system logging
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
QMQP_README, Postfix ezmlm\-idx howto.
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## HISTORY



The qmqpd service was introduced with Postfix version 1.1.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
