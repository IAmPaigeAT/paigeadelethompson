+++
detected_package_version = "0.7.3"
operating_system_version = "15.3"
manpage_name = "lsappinfo"
date = "Sun Feb 16 04:48:13 2025"
title = "lsappinfo(8)"
description = "None Specified"
operating_system = "macos"
manpage_section = "8"
author = "None Specified"
manpage_format = "troff"
+++

.
"LSAPPINFO" "8" "04/01/2013" "" ""
.

## NAME

**lsappinfo** - Control and query CoreApplicationServices about the app state on the system
.

## SYNOPSIS

**lsappinfo** **[options]** [ command [command **options**] ] \.\.\.
.

## COMMON COMMANDS

.

- \(bu
**front** Show the front application\.
.

- \(bu
**find** [ *key*=*value* ]+
.
.br
Show the ASN of all applications which have the given *key*/*value* present in their application information\. For *key* the actual CFString value for the key can be used, or any of the aliases described below under Key Strings\. For *value*, see the rules below under Key Values\.
.

- \(bu
**info** [**-only** *information-item-key*] [-app *app-specifier*] [**-long**][*app-specifier*]
.
.br
Show the information for the application app-specifier
.

- \(bu
**list**
.
.br
Show the application list and information about each running application
.

- \(bu
**listen** [+**notificationcode**]* [-**notificationcode**]* [**-addasn** *asn*] [**-removeasn** *asn*] [ **-id** *#* ] *duration* [--]
.
.br
Listen for the given notifications ( those with \'+\', excluding those with \'-\' ) and display each one and its payload\. Notifications are displayed when they receive when this tool is executing a **wait** or **forever** command\.
.

- \(bu
**launch** [[**launch-modifier**=*value*]+ [**launch-option**=*value*]+ [**-arg** *argument*] [*path-to-bundle*] [--]
.
.br
Launch an application with CoreApplicationServices in LaunchServices\. At the minimum, the execpath must be included as one of the **launch-options** or -poseas and a *path-to-bundle*\. This is a fairly low level operation and does not handle a number of conditions that the higher level functions do\.
.

- \(bu
**metainfo**
.
.br
Show the meta information, which is the session-wide information which CoreApplicationServices maintains for each login session\.
.

- \(bu
**processList**
.
.br
Show the application list, in ascending ASN order\.
.

- \(bu
**restart**
.
.br
Ask the launchservicesd to restart\. The requestor must be privileged\.
.

- \(bu
**sharedmemory**
.
.br
Show the shared memory information page for this session\.
.

- \(bu
**unlisten** [ **-id** *ID* ] [ **-all** ]
.
.br
Unlisten to all notifications on notification *ID*\.
.

- \(bu
**visibleProcessList**
.
.br
Show the visible ( front-to-back ) application list\.
.
"" 0
.

## UNCOMMON COMMANDS

.

- \(bu
**allocateASN** Ask launchservicesd to allocate an ASN, and print it out\.
.

- \(bu
**createFile** *PATH* Create a file at the given path
.

- \(bu
**disconnect** disconnect from launchservicesd

- \(bu
**file** *path* Open the file at *path* and read lines, treating each one as if it were passed to **lsappinfo** on the command line\.
.

- \(bu
**forever** Wait forever before executing the next command
.

- \(bu
**log** [ **-d** | **-i** | **-n** | **-w** | **-e** | **-c** | **-a** ] [ **-B** ] [ **-sender *processname*** ] [ *string* \.\.\. **--** ] If an option is given, dump any LaunchServices logging information on the system until the process is terminated with control-C\. If a string is provided, log that string to syslog\.
.

- \(bu
**removeFile** *PATH* Remove the file at the given path
.

- \(bu
**server** [ **-xpcservicename** *ARG* ] [ **-local** ] [ **-duration *DURATION*** ] [ **-file *FILEPATH*** ] [ **-gone** *FILEGONEPATH* ] [ **-forever** ] Start up the launchservicesd server in process, with the optional given xpc service name or if **-local** then processing xpc requests from future commands for this same process\. Terminate the server after the given *DURATION* seconds, or when the file at *FILEPATH* exists, or the file at path *FILEPATHFONE* is deleted, or never if **-forever**\.
.

- \(bu
**setinfo** [**-app** *app-specifier*] [**app-info-item**=*value*]+ [**--**] Set the values for the given application information items in the specified application\.
.

- \(bu
**setmetainfo** [**meta-info-item**=*value*]+ [--]
.

- \(bu
**wait** [ **-duration** *duration* ] [ **-file** *FILEPATH* ] [ **-gone** *FILEPATHGONE* ] *duration* Wait for duration seconds before executing the next command, or if *FILEPATH* is given until that file exists, or if *FILEPATHGONE* is given until that file no longer exists\.
.

- \(bu
**writePIDToFile** *PATH* Write the current processes pid to a file at *PATH*\.
.
"" 0
.

## OPTIONS

.

- \(bu
**-v** | **--verbose** Be more verbose about many operations
.

- \(bu
**-q** | **--quiet** Be less verbose about many operations
.

- \(bu
**-defaultSession** Use kLSDefaultSessionID as the sessionID passed to all calls (the default)
.

- \(bu
**-currentSession** Use kLSCurrentSessionID as the sessionID passed to all calls
.

- \(bu
**-debug** | **-info** | **-notice** | **-warning** | **-err** | **-critical** | **-alert** | **-emergency** Set the log level for this process to the given level
.
"" 0
.

## APPLICATION SPECIFIERS

There are different ways to indicate what application the commands operate on, collectively called the app-specifier\. This may be one of the following\.
.

- \(bu
"ASN:0x*AAAA*:0x*BBBB*:" where *AAAA* and *BBBB* are the values for an application ASN\.
.

- \(bu
"0x*BBBB*" where *BBBB* are the values from the lower part of an application ASN for which the upper part of the ASN is 0x0
.

- \(bu
"*#*" where *#* is a decimal value above 10, representing the application with the pid *#*
.

- \(bu
"*name*" where *name* is the display name of a running application
.

- \(bu
"*bundleid*" where *bundleid* is the bundle id of a running application
.

- \(bu
"me" the asn of the **lsappinfo** tool
.
"" 0
.

## KEY STRINGS

Any string from this set will map to the corresponding constant from the LaunchServices header files\.
.
"" 4
.

```

kCFBundleNameKey
kLSASNKey
kLSASNToBringForwardAtNextApplicationExitKey
kLSAllowedToBecomeFrontmostKey
kLSApplicationBackgroundOnlyTypeKey
kLSApplicationBackgroundPriorityKey
kLSApplicationCountKey
kLSApplicationDesiresAttentionKey,
kLSApplicationForegroundPriorityKey
kLSApplicationForegroundTypeKey
kLSApplicationHasRegisteredKey
kLSApplicationHasSignalledItIsReadyKey
kLSApplicationInStoppedStateKey
kLSApplicationInThrottledStateAfterLaunchKey
kLSApplicationInformationSeedKey
kLSApplicationIsHiddenKey
kLSApplicationListSeedKey
kLSApplicationReadyToBeFrontableKey
kLSApplicationTypeKey
kLSApplicationTypeToRestoreKey
kLSApplicationUIElementTypeKey
kLSApplicationVersionKey
kLSApplicationWasTerminatedByTALKey
kLSApplicationWouldBeTerminatedByTALKey
kLSArchitectureKey
kLSBundleIdentifierLowerCaseKey
kLSBundlePathDeviceIDKey
kLSBundlePathINodeKey
kLSBundlePathKey
kLSCheckInTimeKey
kLSDebugLevelKey
kLSDisplayNameKey
kLSExecutableFormatCFMKey
kLSExecutableFormatKey
kLSExecutableFormatMachOKey
kLSExecutableFormatPoundBangKey
kLSExecutablePathDeviceIDKey
kLSExecutablePathINodeKey
kLSExecutablePathKey
kLSExitStatusKey
kLSFileCreatorKey
kLSFileTypeKey
kLSFlavorKey
kLSFrontApplicationSeedKey
kLSHiddenApplicationCountKey
kLSLaunchTimeKey
kLSLaunchedByLaunchServicesKey
kLSLaunchedByLaunchServicesThruForkExecKey
kLSLaunchedByLaunchServicesThruLaunchDKey
kLSLaunchedByLaunchServicesThruSessionLauncherKey
kLSLaunchedInQuarantineKey
kLSMenuBarOwnerApplicationSeedKey
kLSModifierLaunchedForPersistenceKey
kLSModifierRefConKey
kLSNotifyBecameFrontmostAnotherLaunchKey
kLSNotifyBecameFrontmostFirstActivationKey
kLSNotifyLaunchRequestLaunchModifiersKey
kLSOriginalExecutablePathDeviceIDKey
kLSOriginalExecutablePathINodeKey
kLSOriginalExecutablePathKey
kLSOriginalPIDKey
kLSPIDKey
kLSParentASNKey
kLSParentASNWasInferredKey
kLSPersistenceSuppressRelaunchAtLoginKey
kLSPreviousASNKey
kLSPreviousPresentationModeKey
kLSPreviousValueKey
kLSRecordingAppleEventsKey
kLSRequiresCarbonKey
kLSSessionIDKey
kLSShellExecutablePathKey
kLSUIDsInSessionKey
kLSUIPresentationModeAllHiddenValue
kLSUIPresentationModeAllSuppressedValue
kLSUIPresentationModeContentHiddenValue
kLSUIPresentationModeContentSuppressedValue
kLSUIPresentationModeKey
kLSUIPresentationModeNormalValue
kLSUIPresentationOptionsKey
kLSUnhiddenApplicationCountKey
kLSVisibleApplicationCountKey
kLSVisibleApplicationListSeedKey
kLSWantsToComeForwardAtRegistrationTimeKey
launchedThrottled
.
```

.
"" 0
.
.P
Likewise, these short strings also make to the corresponding constants\.
.
"" 4
.

```

allowedtobecomefrontmost
applicationTypeToRestore
applicationWasTerminatedByTAL
applicationtype
arch
asn
bundleid
bundlelastcomponent
bundlename
bundlenamelc
bundlepath
changecount
creator
debuglevel
displayname
execpath
executablepath
filecreator
filename
filetype
hidden
isconnectedtowindowserver
isready
isregistered
isstopped
isthrottled
launchedForPersistence
launchedinquarantine
name
parentasn
pid
presentationmode
presentationoptions
psn
recordingAppleEvents
session
shellpath
supressRelaunch
version
.
```

.
"" 0
.

## KEY VALUES

In numerous places a key can be set to a value\. The format of value can be any of the following
.

- \(bu
"*string*" A string, surrounded by double quotes\.
.

- \(bu
*numeric-digits* | -*numeric-digits* | *numeric-digits*\.*numeric-digits*[E]*numeric-digits* A numeric value, either an integer type or a double floating point type\.
.

- \(bu
$*hex-digits* A numeric value given by the hex value hex-digits\.
.

- \(bu
"ASN:0x*AAAA*:0x*BBBB*:" An ASN, where *AAAA* and *BBBB* are the values for an application ASN\.
.

- \(bu
App:*str* An ASN, where *str* matches one of the application-specifier formats\.
.

- \(bu
( [[*str*,] *str*] ) A CFArrayRef, where each *str* is converted as if it were a key value\.
.

- \(bu
true The kCFBooleanTrue value\.
.

- \(bu
false The kCFBooleanFalse value\.
.

- \(bu
null The kCFNull value\.
.

- \(bu
Any of the application information item, or launch modifier strings The equivalent, exported LaunchServices CFStringRef key for the item or launch modifier\.
.
"" 0
.

## APPLICATION INFORMATION ITEM KEYS

.

- \(bu
**asn** An application ASN, which is unique identifier assigned to each application when the application is launched and persists until the application exits, and likely is unique for the entire time a user is logged in\. When displayed, an ASN looks like "ASN:0x0-0x1f01f:"\.
.

- \(bu
**parentasn** The ASN of the application which launched this application\.
.

- \(bu
**bundlename** The bundle name, if one exists, for the application\.
.

- \(bu
**bundlenamelc** The bundle name, if one exists, for the application, but with every upper case character converted into the equivalent lower case character\.
.

- \(bu
**bundlepath** The bundle path, if the application is bundled
.

- \(bu
**executablepath** The executable path of the application
.

- \(bu
**filetype** The file type of the application, if it has one\.
.

- \(bu
**filecreator** The creator type of the application, if it has one\.
.

- \(bu
**pid** The pid of the application\.
.

- \(bu
**filename** The filename of the executable (the last component of the executable path), converted into a lowercase string
.

- \(bu
**bundlelastcomponent** The last component of the bundle path, converted into a lowercase string\.
.

- \(bu
**displayname** | **name** The display name of this application
.

- \(bu
**bundleid** The bundle identifier of the application, if one exists\.
.

- \(bu
**applicationtype** The type of the application (generally "Foreground", "Background", or "UIElement")
.

- \(bu
**allowedtobecomefrontmost** The application is allowed to be frontmost\.
.

- \(bu
**version** The version string for the application, if it has one
.

- \(bu
**presentationmode** The UIPresentationMode for this application (only for foreground applications), generally one of "Normal", "ContentSupressed", "ContentHidden", "Suppressed", "AllHidden"
.

- \(bu
**presentationoptions**
.

- \(bu
**session** A number indicating which audit session this application is running in\.
.

- \(bu
**hidden** If this application is a foreground application, then if it is hidden, "true", or "false" if it is not hidden
.

- \(bu
**changecount** A number which changes whenever any items in the application\'s information dictionary is changed\.
.

- \(bu
**debuglevel**
.

- \(bu
**isregistered** If this application has registered, then "true", otherwise "false"\.
.

- \(bu
**isready** If this application has entered its main runloop and is able to respond to requests to hide or show itself, "true", otherwise "false"\.
.

- \(bu
**isstopped** If this application was launched stopped, and if it has not been started yet, then "true", otherwise "false" or not present\.
.

- \(bu
**launchedinquarantine** If this application was launched in a quarantined state, then "true", otherwise "false" or not present\.
.

- \(bu
**arch** The architecture of the code running this application, generally "x86_64" or "i386"\.
.

- \(bu
**recordingAppleEvents** If this application is recording AppleEvents, then "true", otherwise "false" or not present\.
.

- \(bu
**supressRelaunch** If this application should not be re-launched after a logout and login, then "true", otherwise "false" or not present\.
.

- \(bu
**applicationTypeToRestore**
.

- \(bu
**applicationWasTerminatedByTAL**
.

- \(bu
**isthrottled** If this application was launched in the throttled state, and if it has not been unthrottled, then "true", otherwise false or not present\.
.

- \(bu
**applicationWouldBeTerminatedByTALKey**
.

- \(bu
**launchedhidden** If the application was launched hidden, then "true", otherwise "false" or not present\. This is not whether the application is currently hidden, just whether at the time it was launched the request was to have it hide itself\.
.

- \(bu
**launchandhideothers** If the application was launched and asked to hide all other application, then "true", otherwise "false" or not present\. This is not whether the application is currently hidden, just whether at the time it was launched the request was to have it hide all other applications\.
.

- \(bu
**launchForPersistence** If the application was launched with launchForPersistence=true, then "true", otherwise "false" or not present\.
.
"" 0
.

## LAUNCHMODIFIER KEYS

.

- \(bu
**async**=[true|false] Launch asynchronously
.

- \(bu
**refcon**=[*#*] Launch with the given numeric refcon\.
.

- \(bu
**nofront**=[true|false] If true, do not bring the application to the front when it finishes launching
.

- \(bu
**stopped**=[true|false] Launch the process but do not start it\.
.

- \(bu
**launchandhide**=[true|false] Launch the process and cause it to hide itself when it finishes launching
.

- \(bu
\\launchandhideothers`=[true|false] Launch the process and couse it to hide all other applications when it finishes launching
.

- \(bu
**launchForPersistence**=[true|false]
.

- \(bu
**launchWithASLRDisabled**=[true|false]
.
"" 0
.

## NOTIFICATION CODES

Notifications are sent out by LaunchServices when various conditions arrive\. Each notification has a type, called the notification-code, a dictionary of data items which are specific to the notification, a time the notification was sent, and an optional affected ASN\.
.

- \(bu
**launch**
.
"" 0
.
.P
Sent when an application is launched
.

- \(bu
**creation**
.
"" 0
.
.P
Sent when an entry for an application is created on the system and associated with an ASN\.
.

- \(bu
**birth**
.
"" 0
.
.P
Sent when an
.

- \(bu
**death**
.
"" 0
.
.P
Sent when an application exits\.
.

- \(bu
**abnormaldeath**
.
"" 0
.
.P
Sent when an application exits with a non-zero exit status\.
.

- \(bu
**childDeath**
.
"" 0
.
.P
Sent when an application exits, with affected ASN set to the parent ASN of the application which exited\.
.

- \(bu
**abnormalChildDeath**
.
"" 0
.
.P
Sent when an application exits with a non-zero exit status, with affected ASN set to the parent ASN of the application which exited\.
.

- \(bu
**launchFailure**
.
"" 0
.
.P
Sent when an application launch fails, after a launch notification has been sent out\.
.

- \(bu
**appCreation**
.
"" 0
.
.P
Sent when an application is "created", which happens immediately after the application is created and certain items are added into the application information dictionary\.
.

- \(bu
**childAppCreation**
.
"" 0
.
.P
Sent when an application is "created", which happens immediately after the application is created and certain items are added into the application information dictionary, with affected ASN set to the asn of the parent ASN of this application\.
.

- \(bu
**appReady**
.
"" 0
.
.P
Sent when an applications signals to LaunchServices that it is ready to accept hide/show events, generally when it has entered its main runloop\.
.

- \(bu
**childAppReady**
.
"" 0
.
.P
Sent when an applications signals to LaunchServices that it is ready to accept hide/show events, generally when it has entered its main runloop, with affected ASN set to the parent ASN of the application which signalled ready\.
.

- \(bu
**readyToAcceptAppleEvents**
.
"" 0
.
.P
Sent when an application signals that it is ready to accept AppleEvents\.
.

- \(bu
**launchTimedOut**
.

- \(bu
**launchFinished**
.

- \(bu
**allTALAppsRegistered**
.
"" 0
.
.P
Sent when talagentd decides that all applications which were launched for persistence have registered\.
.

- \(bu
**becameFrontmost**
.
"" 0
.
.P
Sent when an application is made into the front application\.
.

- \(bu
**lostFrontmost**
.
"" 0
.
.P
Sent when an application which previously was the front application is no longer the front application\.
.

- \(bu
**orderChanged**
.
"" 0
.
.P
Sent when the front-to-back order of the application list changes\.
.

- \(bu
**bringForwardRequest**
.
"" 0
.
.P
Someone has requested that the application with affected ASN make itself frontmost\.
.

- \(bu
**menuBarAcquired**
.
"" 0
.
.P
Sent when the application which is responsible for drawing the menu bar (generally the frontmost foreground application) changes
.

- \(bu
**menuBarLost**
.
"" 0
.
.P
Sent when the application which was responsible for drawing the menu bar (generally the frontmost foreground application) is no longer responsible
.

- \(bu
**hidden**
.
"" 0
.
.P
Sent when the application is hidden
.

- \(bu
**shown**
.
"" 0
.
.P
Sent when the application is shown
.

- \(bu
**showRequest**
.
"" 0
.
.P
Someone has requested that the application with the affected application asn should show (un-hide) itself\.
.

- \(bu
**hideRequest**
.
"" 0
.
.P
Someone has requested that the application with the affected application asn should hide itself\.
.

- \(bu
**pullwindowsforward**
.
"" 0
.
.P
Someone has requested that the application with the affected application asn should show itself and pull all of its windows forward\.
.

- \(bu
**appInfoChanged**
.
"" 0
.
.P
Sent when the information for the application is changed\.
.

- \(bu
**appInfoKeyAdded**
.
"" 0
.
.P
Sent when a key is added to the information for the application\. The data for the notification will include the key being added and its value\.
.

- \(bu
**appInfoKeyChanged**
.
"" 0
.
.P
Sent when a value for an item in the application information is changed\. The data for the notification will include the key being changes and its new and old value\.
.

- \(bu
**appInfoKeyRemoved**
.
"" 0
.
.P
Sent when the value for an item in the application information is removed\. The data for the notification will include the key being removed and its value\.
.

- \(bu
**appTypeChanged**
.
"" 0
.
.P
Sent when the "ApplicationType" key in the application information is changed\.
.

- \(bu
**appNameChanged**
.
"" 0
.
.P
Sent when the application name in the application information is changed\.
.

- \(bu
**wantsAttentionChanged**
.
"" 0
.
.P
Sent when the LSWantsAttention key in the application information is changed\.
.

- \(bu
**presentationModeChanged**
.
"" 0
.
.P
Sent when an application changes its presentation mode\.
.

- \(bu
**pidChanged**
.
"" 0
.
.P
Sent when an application changes its pid\. In practice this can never happen, except when LaunchServices launches a process which itself forks or spawns a new process, and then checks-in from that new pid\.
.

- \(bu
**frontPresentationModeChanged**
.
"" 0
.
.P
Sent when the presentation mode of the system changes, generally when the foreground application changes its own presentation mode or when the front application changes and the old and new applications have different presentation modes\.
.

- \(bu
**presentationModeChangedBecauseFrontApplicationChanged**
.
"" 0
.
.P
Sent when the presentation mode of the system changes only because the front application changed and the old and new applications have different presentation modes\.
.

- \(bu
**launchrequest**
.

- \(bu
**started**
.
"" 0
.
.P
Sent when a formally stopped application is started\.
.

- \(bu
**sessionLauncherRegister**
.
"" 0
.
.P
Sent when the ASN of the session launcher application registers with LaunchServices\.
.

- \(bu
**sessionLauncherUnregistered**
.
"" 0
.
.P
Sent when the application registered as the session launcher unregisters or exits\.
.

- \(bu
**nextAppToBringForwardAtQuitRegistered**
.
"" 0
.
.P
Sent when the meta-information item for the next application to bring forward ASN is changed
.

- \(bu
**nextAppToBringForwardAtQuitUnregistered**
.

- \(bu
**systemProcessRegistered**
.
"" 0
.
.P
Sent when the system process (generally loginwindow) registers with LaunchServices\.
.

- \(bu
**systemProcessUnregistered**
.
"" 0
.
.P
Sent when the system process (generally loginwindow) unregisters with LaunchServices\.
.

- \(bu
**frontReservationCreated**
.
"" 0
.
.P
Sent when a front-reservation is created\.
.

- \(bu
**frontReservationDestroyed**
.
"" 0
.
.P
Sent when a front reservation is destroyed\.
.

- \(bu
**permittedFrontASNsChanged**
.
"" 0
.
.P
Sent when the array of permitted-front-applications changes\.
.

- \(bu
**suppressRelaunch**
.
"" 0
.
.P
Sent when an application changes its "LSSupressRelaunch" key\.
.

- \(bu
**terminatedByTALChanged**
.
"" 0
.
.P
Sent when an application changes its "TerminatedByTAL" key\.
.

- \(bu
**launchedThrottledChanged**
.
"" 0
.
.P
Sent when an application changes * **applicationWouldBeTerminatedByTALChanged** * **applicationProgressValueChanged** * **applicationVisualNotification** * **wakeup**
.
.P
Request that the application with affected ASN resume running its main runloop\.
.

- \(bu
**sessionCreated**
.
"" 0
.
.P
Sent when a session is created, generally when the first application registers inside the session\. Affected ASN is always NULL, since this does not refer to any particular application\.
.

- \(bu
**sessionDestroyed**
.
"" 0
.
.P
Sent when a session is destroyed\. Affected ASN is always NULL, since this does not refer to any particular application\.
.

- \(bu
**invalid**
.
"" 0
.
.P
This represents an invalid notification code, and is never sent\.
.

- \(bu
**all**
.
"" 0
.
.P
This represents all notification codes, and is never sent, but gets used when specifying which notifications to listen for\.
.

## EXAMPLES

.

- \(bu
List all of the running applications
.
"" 0
.
.P
**lsappinfo** list
.

- \(bu
Show all the notifications which are being sent out
.
"" 0
.
.P
**lsappinfo** listen +all forever
.

- \(bu
Show the notifications sent out whenever the front application is changed, for the next 60 seconds
.
"" 0
.
.P
**lsappinfo** listen +becameFrontmost wait 60
.

- \(bu
Launch TextEdit\.app, asyncronously, and don\'t bring it to the front
.
"" 0
.
.P
**lsappinfo** launch nofront=true async=true /Applications/TextEdit\.app/
.

- \(bu
Find the ASN for the running application "TextEdit", by bundle id
.
"" 0
.
.P
**lsappinfo** find bundleid=com\.apple\.TextEdit
.

- \(bu
Find the ASN for the running application "TextEdit", by name
.
"" 0
.
.P
**lsappinfo** find name="TextEdit"
.

- \(bu
Show the information for the running application "TextEdit"
.
"" 0
.
.P
**lsappinfo** info "TextEdit"
