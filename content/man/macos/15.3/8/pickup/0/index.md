+++
manpage_format = "troff"
operating_system_version = "15.3"
author = "None Specified"
manpage_section = "8"
date = "Sun Feb 16 04:48:16 2025"
description = "The pickup(8) daemon waits for hints that new mail has been dropped into the maildrop directory, and feeds it into the cleanup(8) daemon. Ill-formatted files are deleted without notifying the originator. This program expects to be run from the mas..."
title = "pickup(8)"
manpage_name = "pickup"
detected_package_version = "0"
keywords = ["na", "cleanup", "message", "canonicalization", "sendmail", "sendmail-compatible", "interface", "postdrop", "mail", "posting", "agent", "postconf", "configuration", "parameters", "master", "generic", "daemon", "options", "process", "manager", "syslogd", "system", "logging", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
operating_system = "macos"
+++

PICKUP 8


## NAME

pickup
-
Postfix local mail pickup

## SYNOPSIS

.na

```
pickup [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The **pickup**(8) daemon waits for hints that new mail has been
dropped into the **maildrop** directory, and feeds it into the
**cleanup**(8) daemon.
Ill-formatted files are deleted without notifying the originator.
This program expects to be run from the **master**(8) process
manager.

## STANDARDS

.na

```
.ad
```

None. The **pickup**(8) daemon does not interact with
the outside world.

## SECURITY

.na

```
.ad
```

The **pickup**(8) daemon is moderately security sensitive. It runs
with fixed low privilege and can run in a chrooted environment.
However, the program reads files from potentially hostile users.
The **pickup**(8) daemon opens no files for writing, is careful about
what files it opens for reading, and does not actually touch any data
that is sent to its public service endpoint.

## DIAGNOSTICS


Problems and transactions are logged to **syslogd**(8).

## BUGS


The **pickup**(8) daemon copies mail from file to the **cleanup**(8)
daemon.  It could avoid message copying overhead by sending a file
descriptor instead of file data, but then the already complex
**cleanup**(8) daemon would have to deal with unfiltered user data.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

As the **pickup**(8) daemon is a relatively long-running process, up
to an hour may pass before a **main.cf** change takes effect.
Use the command "**postfix reload**" command to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

## CONTENT INSPECTION CONTROLS

.na

```
.ad
```


- \fBcontent_filter
After the message is queued, send the entire message to the
specified *transport:destination*.

- \fBreceive_override_options
Enable or disable recipient validation, built-in content
filtering, or address mapping.

## MISCELLANEOUS CONTROLS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBline_length_limit
Upon input, long lines are chopped up into pieces of at most
this length; upon delivery, long lines are reconstructed.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## SEE ALSO

.na

```
cleanup(8), message canonicalization
sendmail(1), Sendmail\-compatible interface
postdrop(1), mail posting agent
postconf(5), configuration parameters
master(5), generic daemon options
master(8), process manager
syslogd(8), system logging
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
