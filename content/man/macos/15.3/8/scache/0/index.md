+++
detected_package_version = "0"
title = "scache(8)"
operating_system_version = "15.3"
keywords = ["na", "smtp", "client", "postconf", "configuration", "parameters", "master", "process", "manager", "syslogd", "system", "logging", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "connection_cache_readme", "postfix", "connection", "cache", "license", "the", "secure", "mailer", "must", "be", "distributed", "with", "software", "history", "service", "was", "introduced", "version", "2", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
manpage_section = "8"
operating_system = "macos"
date = "Sun Feb 16 04:48:16 2025"
author = "None Specified"
manpage_format = "troff"
description = "The scache(8) server maintains a shared multi-connection cache. This information can be used by, for example, Postfix SMTP clients or other Postfix delivery agents."
manpage_name = "scache"
+++

SCACHE 8


## NAME

scache
-
Postfix shared connection cache server

## SYNOPSIS

.na

```
scache [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The **scache**(8) server maintains a shared multi-connection
cache. This information can be used by, for example, Postfix
SMTP clients or other Postfix delivery agents.

The connection cache is organized into logical destination
names, physical endpoint names, and connections.

As a specific example, logical SMTP destinations specify
(transport, domain, port), and physical SMTP endpoints
specify (transport, IP address, port).  An SMTP connection
may be saved after a successful mail transaction.

In the general case, one logical destination may refer to
zero or more physical endpoints, one physical endpoint may
be referenced by zero or more logical destinations, and
one endpoint may refer to zero or more connections.

The exact syntax of a logical destination or endpoint name
is application dependent; the **scache**(8) server does
not care.  A connection is stored as a file descriptor together
with application-dependent information that is needed to
re-activate a connection object. Again, the **scache**(8)
server is completely unaware of the details of that
information.

All information is stored with a finite time to live (ttl).
The connection cache daemon terminates when no client is
connected for **max_idle** time units.

This server implements the following requests:

- \fBsave_endp\fI ttl endpoint endpoint_properties
Save the specified file descriptor and connection property data
under the specified endpoint name. The endpoint properties
are used by the client to re-activate a passivated connection
object.

- \fBfind_endp\fI
Look up cached properties and a cached file descriptor for the
specified endpoint.

- \fBsave_dest\fI ttl destination destination_properties
Save the binding between a logical destination and an
endpoint under the destination name, together with destination
specific connection properties. The destination properties
are used by the client to re-activate a passivated connection
object.

- \fBfind_dest\fI
Look up cached destination properties, cached endpoint properties,
and a cached file descriptor for the specified logical destination.

## SECURITY

.na

```
.ad
```

The **scache**(8) server is not security-sensitive. It does not
talk to the network, and it does not talk to local users.
The **scache**(8) server can run chrooted at fixed low privilege.

The **scache**(8) server is not a trusted process. It must
not be used to store information that is security sensitive.

## DIAGNOSTICS


Problems and transactions are logged to **syslogd**(8).

## BUGS


The session cache cannot be shared among multiple machines.

When a connection expires from the cache, it is closed without
the appropriate protocol specific handshake.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are picked up automatically as **scache**(8)
processes run for only a limited amount of time. Use the command
"**postfix reload**" to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

## RESOURCE CONTROLS

.na

```
.ad
```


- \fBconnection_cache_ttl_limit
The maximal time-to-live value that the **scache**(8) connection
cache server
allows.

- \fBconnection_cache_status_update_time
How frequently the **scache**(8) server logs usage statistics with
connection cache hit and miss rates for logical destinations and for
physical endpoints.

## MISCELLANEOUS CONTROLS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## SEE ALSO

.na

```
smtp(8), SMTP client
postconf(5), configuration parameters
master(8), process manager
syslogd(8), system logging
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
CONNECTION_CACHE_README, Postfix connection cache
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## HISTORY


This service was introduced with Postfix version 2.2.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
