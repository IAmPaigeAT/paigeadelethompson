+++
operating_system_version = "15.3"
date = "Sun Feb 16 04:48:16 2025"
author = "None Specified"
title = "dnsblog(8)"
detected_package_version = "0"
manpage_section = "8"
keywords = ["na", "smtpd", "postfix", "smtp", "server", "postconf", "configuration", "parameters", "syslogd", "system", "logging", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "history", "service", "was", "introduced", "version", "2", "8", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
description = "The dnsblog(8) server implements an ad-hoc DNS white/blacklist lookup service. This may eventually be replaced by an UDP client that is built directly into the postscreen(8) server. With each connection, the dnsblog(8) server receives a DNS white..."
manpage_format = "troff"
manpage_name = "dnsblog"
operating_system = "macos"
+++

DNSBLOG 8


## NAME

dnsblog
-
Postfix DNS white/blacklist logger

## SYNOPSIS

.na

```
dnsblog [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The **dnsblog**(8) server implements an ad-hoc DNS
white/blacklist lookup service. This may eventually be
replaced by an UDP client that is built directly into the
**postscreen**(8) server.

## PROTOCOL

.na

```
.ad
```

With each connection, the **dnsblog**(8) server receives
a DNS white/blacklist domain name, an IP address, and an ID.
If the IP address is listed under the DNS white/blacklist, the
**dnsblog**(8) server logs the match and replies with the
query arguments plus an address list with the resulting IP
addresses, separated by whitespace, and the reply TTL.
Otherwise it replies with the query arguments plus an empty
address list and the reply TTL; the reply TTL is -1 if there
is no reply, or a negative reply that contains no SOA record.
Finally, the **dnsblog**(8) server closes the connection.

## DIAGNOSTICS


Problems and transactions are logged to **syslogd**(8).

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are picked up automatically, as
**dnsblog**(8) processes run for only a limited amount
of time. Use the command "**postfix reload**" to speed
up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBpostscreen_dnsbl_sites
Optional list of DNS white/blacklist domains, filters and weight
factors.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## SEE ALSO

.na

```
smtpd(8), Postfix SMTP server
postconf(5), configuration parameters
syslogd(5), system logging
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## HISTORY



This service was introduced with Postfix version 2.8.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
