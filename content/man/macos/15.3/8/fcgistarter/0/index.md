+++
manpage_section = "8"
date = "2020-02-08"
operating_system_version = "15.3"
operating_system = "macos"
author = "None Specified"
manpage_format = "troff"
title = "fcgistarter(8)"
description = "None Specified"
detected_package_version = "0"
manpage_name = "fcgistarter"
+++

"FCGISTARTER" 8 "2020-02-08" "Apache HTTP Server" "fcgistarter"


## NAME

fcgistarter - Start a FastCGI program


## SYNOPSIS



****fcgistarter** -**c** **command -**p** *port* [ -**i** *interface* ] -**N** *num* 



## SUMMARY







## NOTE



Currently only works on Unix systems.


## OPTIONS




**-c **command
Absolute path of the FastCGI program

**-p **port
Port which the program will listen on

**-i **interface
Interface which the program will listen on

**-N **num
Number of instances of the program

