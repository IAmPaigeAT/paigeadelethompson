+++
manpage_section = "8"
description = "The cleanup(8) daemon processes inbound mail, inserts it into the incoming mail queue, and informs the queue manager of its arrival."
manpage_format = "troff"
operating_system = "macos"
title = "cleanup(8)"
date = "Sun Feb 16 04:48:12 2025"
author = "None Specified"
operating_system_version = "15.3"
detected_package_version = "0"
keywords = ["na", "trivial-rewrite", "address", "rewriting", "qmgr", "queue", "manager", "header_checks", "message", "header", "content", "inspection", "body_checks", "body", "parts", "canonical", "lookup", "table", "format", "virtual", "alias", "postconf", "configuration", "parameters", "master", "generic", "daemon", "options", "process", "syslogd", "system", "logging", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "address_rewriting_readme", "postfix", "manipulation", "content_inspection_readme", "license", "the", "secure", "mailer", "must", "be", "distributed", "with", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
manpage_name = "cleanup"
+++

CLEANUP 8


## NAME

cleanup
-
canonicalize and enqueue Postfix message

## SYNOPSIS

.na

```
cleanup [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The **cleanup**(8) daemon processes inbound mail, inserts it
into the **incoming** mail queue, and informs the queue
manager of its arrival.

The **cleanup**(8) daemon always performs the following transformations:
\(bu
Insert missing message headers: (**Resent-**) **From:**,
**To:**, **Message-Id:**, and **Date:**.
\(bu
Transform envelope and header addresses to the standard
*user@fully-qualified-domain* form that is expected by other
Postfix programs.
This task is delegated to the **trivial-rewrite**(8) daemon.
\(bu
Eliminate duplicate envelope recipient addresses.
\(bu
Remove message headers: **Bcc**, **Content-Length**,
**Resent-Bcc**, **Return-Path**.

The following address transformations are optional:
\(bu
Optionally, rewrite all envelope and header addresses according
to the mappings specified in the **canonical**(5) lookup tables.
\(bu
Optionally, masquerade envelope sender addresses and message
header addresses (i.e. strip host or domain information below
all domains listed in the **masquerade_domains** parameter,
except for user names listed in **masquerade_exceptions**).
By default, address masquerading does not affect envelope recipients.
\(bu
Optionally, expand envelope recipients according to information
found in the **virtual**(5) lookup tables.

The **cleanup**(8) daemon performs sanity checks on the content of
each message. When it finds a problem, by default it returns a
diagnostic status to the client, and leaves it up to the client
to deal with the problem. Alternatively, the client can request
the **cleanup**(8) daemon to bounce the message back to the sender
in case of trouble.

## STANDARDS

.na

```
RFC 822 (ARPA Internet Text Messages)
RFC 2045 (MIME: Format of Internet Message Bodies)
RFC 2046 (MIME: Media Types)
RFC 2822 (Internet Message Format)
RFC 3463 (Enhanced Status Codes)
RFC 3464 (Delivery status notifications)
RFC 5322 (Internet Message Format)
.SH DIAGNOSTICS
.ad
```

Problems and transactions are logged to **syslogd**(8).

## BUGS


Table-driven rewriting rules make it hard to express \fBif then
else and other logical relationships.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are picked up automatically, as
**cleanup**(8)
processes run for only a limited amount of time. Use the command
"**postfix reload**" to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

## COMPATIBILITY CONTROLS

.na

```
.ad
```


- \fBundisclosed_recipients_header (see 'postconf -d'
Message header that the Postfix **cleanup**(8) server inserts when a
message contains no To: or Cc: message header.

Available in Postfix version 2.1 only:

- \fBenable_errors_to
Report mail delivery errors to the address specified with the
non-standard Errors-To: message header, instead of the envelope
sender address (this feature is removed with Postfix version 2.2, is
turned off by default with Postfix version 2.1, and is always turned on
with older Postfix versions).

Available in Postfix version 2.6 and later:

- \fBalways_add_missing_headers
Always add (Resent-) From:, To:, Date: or Message-ID: headers
when not present.

Available in Postfix version 2.9 and later:

- \fBenable_long_queue_ids
Enable long, non-repeating, queue IDs (queue file names).

Available in Postfix version 3.0 and later:

- \fBmessage_drop_headers (bcc, content-length, resent-bcc,
Names of message headers that the **cleanup**(8) daemon will remove
after applying **header_checks**(5) and before invoking Milter applications.

## BUILT-IN CONTENT FILTERING CONTROLS

.na

```
.ad
```

Postfix built-in content filtering is meant to stop a flood of
worms or viruses. It is not a general content filter.

- \fBbody_checks
Optional lookup tables for content inspection as specified in
the **body_checks**(5) manual page.

- \fBheader_checks
Optional lookup tables for content inspection of primary non-MIME
message headers, as specified in the **header_checks**(5) manual page.

Available in Postfix version 2.0 and later:

- \fBbody_checks_size_limit
How much text in a message body segment (or attachment, if you
prefer to use that term) is subjected to body_checks inspection.

- \fBmime_header_checks
Optional lookup tables for content inspection of MIME related
message headers, as described in the **header_checks**(5) manual page.

- \fBnested_header_checks
Optional lookup tables for content inspection of non-MIME message
headers in attached messages, as described in the **header_checks**(5)
manual page.

Available in Postfix version 2.3 and later:

- \fBmessage_reject_characters
The set of characters that Postfix will reject in message
content.

- \fBmessage_strip_characters
The set of characters that Postfix will remove from message
content.

## BEFORE QUEUE MILTER CONTROLS

.na

```
.ad
```

As of version 2.3, Postfix supports the Sendmail version 8
Milter (mail filter) protocol. When mail is not received via
the smtpd(8) server, the cleanup(8) server will simulate
SMTP events to the extent that this is possible. For details
see the MILTER_README document.

- \fBnon_smtpd_milters
A list of Milter (mail filter) applications for new mail that
does not arrive via the Postfix **smtpd**(8) server.

- \fBmilter_protocol
The mail filter protocol version and optional protocol extensions
for communication with a Milter application; prior to Postfix 2.6
the default protocol is 2.

- \fBmilter_default_action
The default action when a Milter (mail filter) application is
unavailable or mis-configured.

- \fBmilter_macro_daemon_name
The \{daemon_name\} macro value for Milter (mail filter) applications.

- \fBmilter_macro_v ($mail_name
The \{v\} macro value for Milter (mail filter) applications.

- \fBmilter_connect_timeout
The time limit for connecting to a Milter (mail filter)
application, and for negotiating protocol options.

- \fBmilter_command_timeout
The time limit for sending an SMTP command to a Milter (mail
filter) application, and for receiving the response.

- \fBmilter_content_timeout
The time limit for sending message content to a Milter (mail
filter) application, and for receiving the response.

- \fBmilter_connect_macros (see 'postconf -d'
The macros that are sent to Milter (mail filter) applications
after completion of an SMTP connection.

- \fBmilter_helo_macros (see 'postconf -d'
The macros that are sent to Milter (mail filter) applications
after the SMTP HELO or EHLO command.

- \fBmilter_mail_macros (see 'postconf -d'
The macros that are sent to Milter (mail filter) applications
after the SMTP MAIL FROM command.

- \fBmilter_rcpt_macros (see 'postconf -d'
The macros that are sent to Milter (mail filter) applications
after the SMTP RCPT TO command.

- \fBmilter_data_macros (see 'postconf -d'
The macros that are sent to version 4 or higher Milter (mail
filter) applications after the SMTP DATA command.

- \fBmilter_unknown_command_macros (see 'postconf -d'
The macros that are sent to version 3 or higher Milter (mail
filter) applications after an unknown SMTP command.

- \fBmilter_end_of_data_macros (see 'postconf -d'
The macros that are sent to Milter (mail filter) applications
after the message end-of-data.

Available in Postfix version 2.5 and later:

- \fBmilter_end_of_header_macros (see 'postconf -d'
The macros that are sent to Milter (mail filter) applications
after the end of the message header.

Available in Postfix version 2.7 and later:

- \fBmilter_header_checks
Optional lookup tables for content inspection of message headers
that are produced by Milter applications.

Available in Postfix version 3.1 and later:

- \fBmilter_macro_defaults
Optional list of *name=value* pairs that specify default
values for arbitrary macros that Postfix may send to Milter
applications.

## MIME PROCESSING CONTROLS

.na

```
.ad
```

Available in Postfix version 2.0 and later:

- \fBdisable_mime_input_processing
Turn off MIME processing while receiving mail.

- \fBmime_boundary_length_limit
The maximal length of MIME multipart boundary strings.

- \fBmime_nesting_limit
The maximal recursion level that the MIME processor will handle.

- \fBstrict_8bitmime
Enable both strict_7bit_headers and strict_8bitmime_body.

- \fBstrict_7bit_headers
Reject mail with 8-bit text in message headers.

- \fBstrict_8bitmime_body
Reject 8-bit message body text without 8-bit MIME content encoding
information.

- \fBstrict_mime_encoding_domain
Reject mail with invalid Content-Transfer-Encoding: information
for the message/* or multipart/* MIME content types.

Available in Postfix version 2.5 and later:

- \fBdetect_8bit_encoding_header
Automatically detect 8BITMIME body content by looking at
Content-Transfer-Encoding: message headers; historically, this
behavior was hard-coded to be "always on".

## AUTOMATIC BCC RECIPIENT CONTROLS

.na

```
.ad
```

Postfix can automatically add BCC (blind carbon copy)
when mail enters the mail system:

- \fBalways_bcc
Optional address that receives a "blind carbon copy" of each message
that is received by the Postfix mail system.

Available in Postfix version 2.1 and later:

- \fBsender_bcc_maps
Optional BCC (blind carbon-copy) address lookup tables, indexed
by sender address.

- \fBrecipient_bcc_maps
Optional BCC (blind carbon-copy) address lookup tables, indexed by
recipient address.

## ADDRESS TRANSFORMATION CONTROLS

.na

```
.ad
```

Address rewriting is delegated to the **trivial-rewrite**(8) daemon.
The **cleanup**(8) server implements table driven address mapping.

- \fBempty_address_recipient
The recipient of mail addressed to the null address.

- \fBcanonical_maps
Optional address mapping lookup tables for message headers and
envelopes.

- \fBrecipient_canonical_maps
Optional address mapping lookup tables for envelope and header
recipient addresses.

- \fBsender_canonical_maps
Optional address mapping lookup tables for envelope and header
sender addresses.

- \fBmasquerade_classes (envelope_sender, header_sender,
What addresses are subject to address masquerading.

- \fBmasquerade_domains
Optional list of domains whose subdomain structure will be stripped
off in email addresses.

- \fBmasquerade_exceptions
Optional list of user names that are not subjected to address
masquerading, even when their address matches $masquerade_domains.

- \fBpropagate_unmatched_extensions (canonical,
What address lookup tables copy an address extension from the lookup
key to the lookup result.

Available before Postfix version 2.0:

- \fBvirtual_maps
Optional lookup tables with a) names of domains for which all
addresses are aliased to addresses in other local or remote domains,
and b) addresses that are aliased to addresses in other local or
remote domains.

Available in Postfix version 2.0 and later:

- \fBvirtual_alias_maps
Optional lookup tables that alias specific mail addresses or domains
to other local or remote address.

Available in Postfix version 2.2 and later:

- \fBcanonical_classes (envelope_sender, envelope_recipient, header_sender,
What addresses are subject to canonical_maps address mapping.

- \fBrecipient_canonical_classes (envelope_recipient,
What addresses are subject to recipient_canonical_maps address
mapping.

- \fBsender_canonical_classes (envelope_sender,
What addresses are subject to sender_canonical_maps address
mapping.

- \fBremote_header_rewrite_domain
Don't rewrite message headers from remote clients at all when
this parameter is empty; otherwise, rewrite message headers and
append the specified domain name to incomplete addresses.

## RESOURCE AND RATE CONTROLS

.na

```
.ad
```


- \fBduplicate_filter_limit
The maximal number of addresses remembered by the address
duplicate filter for **aliases**(5) or **virtual**(5) alias expansion, or
for **showq**(8) queue displays.

- \fBheader_size_limit
The maximal amount of memory in bytes for storing a message header.

- \fBhopcount_limit
The maximal number of Received:  message headers that is allowed
in the primary message headers.

- \fBin_flow_delay
Time to pause before accepting a new message, when the message
arrival rate exceeds the message delivery rate.

- \fBmessage_size_limit
The maximal size in bytes of a message, including envelope information.

Available in Postfix version 2.0 and later:

- \fBheader_address_token_limit
The maximal number of address tokens are allowed in an address
message header.

- \fBmime_boundary_length_limit
The maximal length of MIME multipart boundary strings.

- \fBmime_nesting_limit
The maximal recursion level that the MIME processor will handle.

- \fBqueue_file_attribute_count_limit
The maximal number of (name=value) attributes that may be stored
in a Postfix queue file.

Available in Postfix version 2.1 and later:

- \fBvirtual_alias_expansion_limit
The maximal number of addresses that virtual alias expansion produces
from each original recipient.

- \fBvirtual_alias_recursion_limit
The maximal nesting depth of virtual alias expansion.

Available in Postfix version 3.0 and later:

- \fBvirtual_alias_address_length_limit
The maximal length of an email address after virtual alias expansion.

## SMTPUTF8 CONTROLS

.na

```
.ad
```

Preliminary SMTPUTF8 support is introduced with Postfix 3.0.

- \fBsmtputf8_enable
Enable preliminary SMTPUTF8 support for the protocols described
in RFC 6531..6533.

- \fBsmtputf8_autodetect_classes (sendmail,
Detect that a message requires SMTPUTF8 support for the specified
mail origin classes.

Available in Postfix version 3.2 and later:

- \fBenable_idna2003_compatibility
Enable 'transitional' compatibility between IDNA2003 and IDNA2008,
when converting UTF-8 domain names to/from the ASCII form that is
used for DNS lookups.

## MISCELLANEOUS CONTROLS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBdelay_logging_resolution_limit
The maximal number of digits after the decimal point when logging
sub-second delay values.

- \fBdelay_warning_time
The time after which the sender receives a copy of the message
headers of mail that is still queued.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBmyhostname (see 'postconf -d'
The internet hostname of this mail system.

- \fBmyorigin
The domain name that locally-posted mail appears to come
from, and that locally posted mail is delivered to.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsoft_bounce
Safety net to keep mail queued that would otherwise be returned to
the sender.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

Available in Postfix version 2.1 and later:

- \fBenable_original_recipient
Enable support for the X-Original-To message header.

## FILES

.na

```
/etc/postfix/canonical*, canonical mapping table
/etc/postfix/virtual*, virtual mapping table
.SH "SEE ALSO"
.na

```
trivial\-rewrite(8), address rewriting
qmgr(8), queue manager
header_checks(5), message header content inspection
body_checks(5), body parts content inspection
canonical(5), canonical address lookup table format
virtual(5), virtual alias lookup table format
postconf(5), configuration parameters
master(5), generic daemon options
master(8), process manager
syslogd(8), system logging
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
ADDRESS_REWRITING_README Postfix address manipulation
CONTENT_INSPECTION_README content inspection
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
