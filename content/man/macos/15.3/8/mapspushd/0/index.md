+++
author = "None Specified"
manpage_section = "8"
description = "mapspushd handles some auxiliary tasks on behalf of the Maps application and related system services. For example, it may: display notifications relevant to the user, including local announcements and Report an Issue status updates; handle, coordina..."
operating_system_version = "15.3"
operating_system = "macos"
date = "Sun Feb 16 04:48:16 2025"
title = "mapspushd(8)"
manpage_name = "mapspushd"
manpage_format = "troff"
detected_package_version = "0"
+++

"MAPSPUSHD" "8" "August 2017" "" ""
.

## NAME

**mapspushd** - Maps application services daemon
.

## SYNOPSIS

**mapspushd**
.

## DESCRIPTION

**mapspushd** handles some auxiliary tasks on behalf of the Maps application and related system services\. For example, it may:
.

- \(bu
display notifications relevant to the user, including local announcements and Report an Issue status updates;
.

- \(bu
handle, coordinate and secure access to some user data related to the Maps application, such as preferences, recent items, or favorites information\.
.
"" 0
.
.P
**mapspushd** is a system daemon and should not be invoked directly\.
