+++
author = "None Specified"
date = "2018-07-06"
title = "suexec(8)"
operating_system = "macos"
manpage_name = "suexec"
manpage_section = "8"
detected_package_version = "0"
description = "None Specified"
operating_system_version = "15.3"
manpage_format = "troff"
+++

"SUEXEC" 8 "2018-07-06" "Apache HTTP Server" "suexec"


## NAME

suexec - Switch user before executing external programs


## SYNOPSIS



****suexec** -**V****



## SUMMARY



**suexec** is used by the Apache HTTP Server to switch to another user before executing CGI programs. In order to achieve this, it must run as **root**. Since the HTTP daemon normally doesn't run as **root**, the **suexec** executable needs the setuid bit set and must be owned by **root**. It should never be writable for any other person than **root**.


For further information about the concepts and the security model of suexec please refer to the suexec documentation (http://httpd.apache.org/docs/2.4/suexec.html).



## OPTIONS




**-V**
If you are **root**, this option displays the compile options of **suexec**. For security reasons all configuration options are changeable only at compile time.

