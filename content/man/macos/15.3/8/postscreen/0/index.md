+++
author = "None Specified"
date = "Sun Feb 16 04:48:17 2025"
manpage_section = "8"
keywords = ["na", "smtpd", "postfix", "smtp", "server", "tlsproxy", "tls", "proxy", "dnsblog", "dns", "black", "whitelist", "logger", "syslogd", "system", "logging", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "postscreen_readme", "postscreen", "howto", "license", "the", "secure", "mailer", "must", "be", "distributed", "with", "software", "history", "service", "was", "introduced", "version", "2", "8", "many", "ideas", "in", "fbpostscreen", "were", "explored", "earlier", "work", "by", "michael", "tokarev", "openbsd", "spamd", "and", "mailchannels", "traffic", "control", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
description = "The Postfix postscreen(8) server provides additional protection against mail server overload. One postscreen(8) process handles multiple inbound SMTP connections, and decides which clients may talk to a Postfix SMTP server process. By keeping spam..."
title = "postscreen(8)"
operating_system = "macos"
detected_package_version = "0"
manpage_name = "postscreen"
operating_system_version = "15.3"
manpage_format = "troff"
+++

POSTSCREEN 8


## NAME

postscreen
-
Postfix zombie blocker

## SYNOPSIS

.na

```
postscreen [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The Postfix **postscreen**(8) server provides additional
protection against mail server overload. One **postscreen**(8)
process handles multiple inbound SMTP connections, and decides
which clients may talk to a Postfix SMTP server process.
By keeping spambots away, **postscreen**(8) leaves more
SMTP server processes available for legitimate clients, and
delays the onset of server overload conditions.

This program should not be used on SMTP ports that receive
mail from end-user clients (MUAs). In a typical deployment,
**postscreen**(8) handles the MX service on TCP port 25, and
**smtpd**(8) receives mail from MUAs on the **submission**
service (TCP port 587) which requires client authentication.
Alternatively, a site could set up a dedicated, non-postscreen,
"port 25" server that provides **submission** service and
client authentication, but no MX service.

**postscreen**(8) maintains a temporary whitelist for
clients that have passed a number of tests.  When an SMTP
client IP address is whitelisted, **postscreen**(8) hands
off the connection immediately to a Postfix SMTP server
process. This minimizes the overhead for legitimate mail.

By default, **postscreen**(8) logs statistics and hands
off each connection to a Postfix SMTP server process, while
excluding clients in mynetworks from all tests (primarily,
to avoid problems with non-standard SMTP implementations
in network appliances).  This default mode blocks no clients,
and is useful for non-destructive testing.

In a typical production setting, **postscreen**(8) is
configured to reject mail from clients that fail one or
more tests. **postscreen**(8) logs rejected mail with the
client address, helo, sender and recipient information.

**postscreen**(8) is not an SMTP proxy; this is intentional.
The purpose is to keep spambots away from Postfix SMTP
server processes, while minimizing overhead for legitimate
traffic.

## SECURITY

.na

```
.ad
```

The **postscreen**(8) server is moderately security-sensitive.
It talks to untrusted clients on the network. The process
can be run chrooted at fixed low privilege.

## STANDARDS

.na

```
RFC 821 (SMTP protocol)
RFC 1123 (Host requirements)
RFC 1652 (8bit\-MIME transport)
RFC 1869 (SMTP service extensions)
RFC 1870 (Message Size Declaration)
RFC 1985 (ETRN command)
RFC 2034 (SMTP Enhanced Status Codes)
RFC 2821 (SMTP protocol)
Not: RFC 2920 (SMTP Pipelining)
RFC 3207 (STARTTLS command)
RFC 3461 (SMTP DSN Extension)
RFC 3463 (Enhanced Status Codes)
RFC 5321 (SMTP protocol, including multi\-line 220 banners)
.SH DIAGNOSTICS
.ad
```

Problems and transactions are logged to **syslogd**(8).

## BUGS


The **postscreen**(8) built-in SMTP protocol engine
currently does not announce support for AUTH, XCLIENT or
XFORWARD.
If you need to make these services available
on port 25, then do not enable the optional "after 220
server greeting" tests.

The optional "after 220 server greeting" tests may result in
unexpected delivery delays from senders that retry email delivery
from a different IP address.  Reason: after passing these tests a
new client must disconnect, and reconnect from the same IP
address before it can deliver mail. See POSTSCREEN_README, section
"Tests after the 220 SMTP server greeting", for a discussion.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to main.cf are not picked up automatically, as
**postscreen**(8) processes may run for several hours.
Use the command "postfix reload" after a configuration
change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

NOTE: Some **postscreen**(8) parameters implement
stress-dependent behavior.  This is supported only when the
default parameter value is stress-dependent (that is, it
looks like $\{stress?\{X\}:\{Y\}\}, or it is the $*name*
of an smtpd parameter with a stress-dependent default).
Other parameters always evaluate as if the **stress**
parameter value is the empty string.

## COMPATIBILITY CONTROLS

.na

```
.ad
```


- \fBpostscreen_command_filter
A mechanism to transform commands from remote SMTP clients.

- \fBpostscreen_discard_ehlo_keyword_address_maps
Lookup tables, indexed by the remote SMTP client address, with
case insensitive lists of EHLO keywords (pipelining, starttls, auth,
etc.) that the **postscreen**(8) server will not send in the EHLO response
to a remote SMTP client.

- \fBpostscreen_discard_ehlo_keywords
A case insensitive list of EHLO keywords (pipelining, starttls,
auth, etc.) that the **postscreen**(8) server will not send in the EHLO
response to a remote SMTP client.

Available in Postfix version 3.1 and later:

- \fBdns_ncache_ttl_fix_enable
Enable a workaround for future libc incompatibility.

## TROUBLE SHOOTING CONTROLS

.na

```
.ad
```


- \fBpostscreen_expansion_filter (see 'postconf -d'
List of characters that are permitted in postscreen_reject_footer
attribute expansions.

- \fBpostscreen_reject_footer
Optional information that is appended after a 4XX or 5XX
**postscreen**(8) server
response.

- \fBsoft_bounce
Safety net to keep mail queued that would otherwise be returned to
the sender.

## BEFORE-POSTSCREEN PROXY AGENT

.na

```
.ad
```

Available in Postfix version 2.10 and later:

- \fBpostscreen_upstream_proxy_protocol
The name of the proxy protocol used by an optional before-postscreen
proxy agent.

- \fBpostscreen_upstream_proxy_timeout
The time limit for the proxy protocol specified with the
postscreen_upstream_proxy_protocol parameter.

## PERMANENT WHITE/BLACKLIST TEST

.na

```
.ad
```

This test is executed immediately after a remote SMTP client
connects. If a client is permanently whitelisted, the client
will be handed off immediately to a Postfix SMTP server
process.

- \fBpostscreen_access_list
Permanent white/blacklist for remote SMTP client IP addresses.

- \fBpostscreen_blacklist_action
The action that **postscreen**(8) takes when a remote SMTP client is
permanently blacklisted with the postscreen_access_list parameter.

## MAIL EXCHANGER POLICY TESTS

.na

```
.ad
```

When **postscreen**(8) is configured to monitor all primary
and backup MX addresses, it can refuse to whitelist clients
that connect to a backup MX address only. For small sites,
this requires configuring primary and backup MX addresses
on the same MTA. Larger sites would have to share the
**postscreen**(8) cache between primary and backup MTAs,
which would introduce a common point of failure.

- \fBpostscreen_whitelist_interfaces
A list of local **postscreen**(8) server IP addresses where a
non-whitelisted remote SMTP client can obtain **postscreen**(8)'s temporary
whitelist status.

## BEFORE 220 GREETING TESTS

.na

```
.ad
```

These tests are executed before the remote SMTP client
receives the "220 servername" greeting. If no tests remain
after the successful completion of this phase, the client
will be handed off immediately to a Postfix SMTP server
process.

- \fBdnsblog_service_name
The name of the **dnsblog**(8) service entry in master.cf.

- \fBpostscreen_dnsbl_action
The action that **postscreen**(8) takes when a remote SMTP client's combined
DNSBL score is equal to or greater than a threshold (as defined
with the postscreen_dnsbl_sites and postscreen_dnsbl_threshold
parameters).

- \fBpostscreen_dnsbl_reply_map
A mapping from actual DNSBL domain name which includes a secret
password, to the DNSBL domain name that postscreen will reply with
when it rejects mail.

- \fBpostscreen_dnsbl_sites
Optional list of DNS white/blacklist domains, filters and weight
factors.

- \fBpostscreen_dnsbl_threshold
The inclusive lower bound for blocking a remote SMTP client, based on
its combined DNSBL score as defined with the postscreen_dnsbl_sites
parameter.

- \fBpostscreen_greet_action
The action that **postscreen**(8) takes when a remote SMTP client speaks
before its turn within the time specified with the postscreen_greet_wait
parameter.

- \fBpostscreen_greet_banner
The *text* in the optional "220-*text*..." server
response that
**postscreen**(8) sends ahead of the real Postfix SMTP server's "220
text..." response, in an attempt to confuse bad SMTP clients so
that they speak before their turn (pre-greet).

- \fBpostscreen_greet_wait (normal: 6s, overload:
The amount of time that **postscreen**(8) will wait for an SMTP
client to send a command before its turn, and for DNS blocklist
lookup results to arrive (default: up to 2 seconds under stress,
up to 6 seconds otherwise).

- \fBsmtpd_service_name
The internal service that **postscreen**(8) hands off allowed
connections to.

Available in Postfix version 2.11 and later:

- \fBpostscreen_dnsbl_whitelist_threshold
Allow a remote SMTP client to skip "before" and "after 220
greeting" protocol tests, based on its combined DNSBL score as
defined with the postscreen_dnsbl_sites parameter.

Available in Postfix version 3.0 and later:

- \fBpostscreen_dnsbl_timeout
The time limit for DNSBL or DNSWL lookups.

## AFTER 220 GREETING TESTS

.na

```
.ad
```

These tests are executed after the remote SMTP client
receives the "220 servername" greeting. If a client passes
all tests during this phase, it will receive a 4XX response
to all RCPT TO commands. After the client reconnects, it
will be allowed to talk directly to a Postfix SMTP server
process.

- \fBpostscreen_bare_newline_action
The action that **postscreen**(8) takes when a remote SMTP client sends
a bare newline character, that is, a newline not preceded by carriage
return.

- \fBpostscreen_bare_newline_enable
Enable "bare newline" SMTP protocol tests in the **postscreen**(8)
server.

- \fBpostscreen_disable_vrfy_command
Disable the SMTP VRFY command in the **postscreen**(8) daemon.

- \fBpostscreen_forbidden_commands
List of commands that the **postscreen**(8) server considers in
violation of the SMTP protocol.

- \fBpostscreen_helo_required
Require that a remote SMTP client sends HELO or EHLO before
commencing a MAIL transaction.

- \fBpostscreen_non_smtp_command_action
The action that **postscreen**(8) takes when a remote SMTP client sends
non-SMTP commands as specified with the postscreen_forbidden_commands
parameter.

- \fBpostscreen_non_smtp_command_enable
Enable "non-SMTP command" tests in the **postscreen**(8) server.

- \fBpostscreen_pipelining_action
The action that **postscreen**(8) takes when a remote SMTP client
sends
multiple commands instead of sending one command and waiting for
the server to respond.

- \fBpostscreen_pipelining_enable
Enable "pipelining" SMTP protocol tests in the **postscreen**(8)
server.

## CACHE CONTROLS

.na

```
.ad
```


- \fBpostscreen_cache_cleanup_interval
The amount of time between **postscreen**(8) cache cleanup runs.

- \fBpostscreen_cache_map
Persistent storage for the **postscreen**(8) server decisions.

- \fBpostscreen_cache_retention_time
The amount of time that **postscreen**(8) will cache an expired
temporary whitelist entry before it is removed.

- \fBpostscreen_bare_newline_ttl
The amount of time that **postscreen**(8) will use the result from
a successful "bare newline" SMTP protocol test.

- \fBpostscreen_dnsbl_max_ttl
The maximum amount of time that **postscreen**(8) will use the
result from a successful DNS-based reputation test before a
client IP address is required to pass that test again.

- \fBpostscreen_dnsbl_min_ttl
The minimum amount of time that **postscreen**(8) will use the
result from a successful DNS-based reputation test before a
client IP address is required to pass that test again.

- \fBpostscreen_greet_ttl
The amount of time that **postscreen**(8) will use the result from
a successful PREGREET test.

- \fBpostscreen_non_smtp_command_ttl
The amount of time that **postscreen**(8) will use the result from
a successful "non_smtp_command" SMTP protocol test.

- \fBpostscreen_pipelining_ttl
The amount of time that **postscreen**(8) will use the result from
a successful "pipelining" SMTP protocol test.

## RESOURCE CONTROLS

.na

```
.ad
```


- \fBline_length_limit
Upon input, long lines are chopped up into pieces of at most
this length; upon delivery, long lines are reconstructed.

- \fBpostscreen_client_connection_count_limit
How many simultaneous connections any remote SMTP client is
allowed to have
with the **postscreen**(8) daemon.

- \fBpostscreen_command_count_limit
The limit on the total number of commands per SMTP session for
**postscreen**(8)'s built-in SMTP protocol engine.

- \fBpostscreen_command_time_limit (normal: 300s, overload:
The time limit to read an entire command line with **postscreen**(8)'s
built-in SMTP protocol engine.

- \fBpostscreen_post_queue_limit
The number of clients that can be waiting for service from a
real Postfix SMTP server process.

- \fBpostscreen_pre_queue_limit
The number of non-whitelisted clients that can be waiting for
a decision whether they will receive service from a real Postfix
SMTP server
process.

- \fBpostscreen_watchdog_timeout
How much time a **postscreen**(8) process may take to respond to
a remote SMTP client command or to perform a cache operation before it
is terminated by a built-in watchdog timer.

## STARTTLS CONTROLS

.na

```
.ad
```


- \fBpostscreen_tls_security_level
The SMTP TLS security level for the **postscreen**(8) server; when
a non-empty value is specified, this overrides the obsolete parameters
postscreen_use_tls and postscreen_enforce_tls.

- \fBtlsproxy_service_name
The name of the **tlsproxy**(8) service entry in master.cf.

## OBSOLETE STARTTLS SUPPORT CONTROLS

.na

```
.ad
```

These parameters are supported for compatibility with
**smtpd**(8) legacy parameters.

- \fBpostscreen_use_tls
Opportunistic TLS: announce STARTTLS support to remote SMTP clients,
but do not require that clients use TLS encryption.

- \fBpostscreen_enforce_tls
Mandatory TLS: announce STARTTLS support to remote SMTP clients, and
require that clients use TLS encryption.

## MISCELLANEOUS CONTROLS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdelay_logging_resolution_limit
The maximal number of digits after the decimal point when logging
sub-second delay values.

- \fBcommand_directory (see 'postconf -d'
The location of all postfix administrative commands.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## SEE ALSO

.na

```
smtpd(8), Postfix SMTP server
tlsproxy(8), Postfix TLS proxy server
dnsblog(8), DNS black/whitelist logger
syslogd(8), system logging
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or "\fBpostconf
html_directory" to locate this information.

```
.na
POSTSCREEN_README, Postfix Postscreen Howto
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## HISTORY



This service was introduced with Postfix version 2.8.

Many ideas in **postscreen**(8) were explored in earlier
work by Michael Tokarev, in OpenBSD spamd, and in MailChannels
Traffic Control.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
