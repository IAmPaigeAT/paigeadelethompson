+++
operating_system = "macos"
description = "f[B]kernelmanagerdf[R] is the kernel extension server. It runs as a standalone f[B]launchd(8)f[R] daemon to handle requests from the kernel, and from other user-space processes to load kernel extensions (kexts) if they are available in a loaded f[B..."
title = "kernelmanagerd(8)"
operating_system_version = "15.3"
author = "None Specified"
date = "Sun Feb 16 04:48:14 2025"
manpage_section = "8"
manpage_name = "kernelmanagerd"
manpage_format = "troff"
detected_package_version = "0"
+++

.ie "\f[CB]x\f[]"x" \{\
. ftr V B
. ftr VI BI
. ftr VB B
. ftr VBI BI
.\}
.el \{\
. ftr V CR
. ftr VI CI
. ftr VB CB
. ftr VBI CBI
.\}
"KERNELMANAGERD" "8" "" "2023-08-25" "KernelManagement daemon for managing kexts and collections"
.hy

## DESCRIPTION


\f[B]kernelmanagerd\f[R] is the kernel extension server.
It runs as a standalone \f[B]launchd(8)\f[R] daemon to handle requests
from the kernel, and from other user-space processes to load kernel
extensions (kexts) if they are available in a loaded \f[B]kext
collection\f[R] (see \f[B]kmutil(8)\f[R]).
If a kext is requested for load that is not yet available for loading,
\f[B]kernelmanagerd\f[R] will build the kext into the \f[B]auxiliary
kext collection\f[R] for use on the next reboot.
It is also responsible for launching \f[B]driver extensions\f[R] on the
system.

The KernelManagement utility \f[B]kmutil(8)\f[R] can interact with
\f[B]kernelmanagerd\f[R] for requesting loads and unloads of kernel
extensions, and for querying the daemon\[cq]s state.

\f[B]kernelmanagerd\f[R] watches the \f[B]/Library/Extensions\f[R]
directory for changes, to support installation and removal of kexts with
installers.

## PATHS


\f[B]/System/Library/KernelCollections\f[R]
is the standard system repository of kext collections.

\f[B]/System/Library/Extensions/\f[R]
is the standard system repository of kernel extensions.

\f[B]/Library/KernelCollections\f[R]
is the directory containing the \f[B]auxiliary kext collection\f[R].

\f[B]/Library/Extensions/\f[R]
is a writable repository of kernel extensions suitable for use as an
installer destination.

\f[B]/System/Library/Frameworks/KernelManagement.framework/Resources/com.apple.kernelmanagement.plist\f[R]
is a configuration file to modify the repositories and default paths
used by \f[B]kernelmanagerd\f[R] and \f[B]kmutil(8)\f[R].

\f[B]/System/Library/LaunchDaemons/com.apple.kernelmanagerd.plist\f[R]
is the path to the daemon\[cq]s \f[B]launchd.plist(5)\f[R].

### SEE ALSO


\f[B]kmutil(8)\f[R]
