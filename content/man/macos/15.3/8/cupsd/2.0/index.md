+++
keywords = ["backend", "7", "classes", "conf", "5", "cups", "1", "cups-files", "cups-lpd", "8", "cupsd", "cupsd-helper", "cupsd-logs", "filter", "launchd", "mime", "convs", "types", "printers", "systemd", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
author = "None Specified"
title = "cupsd(8)"
manpage_name = "cupsd"
operating_system_version = "15.3"
manpage_format = "troff"
detected_package_version = "2.0"
description = "is the scheduler for CUPS. It implements a printing system based upon the Internet Printing Protocol, version 2.1, and supports most of the requirements for IPP Everywhere. If no options are specified on the command-line then the default configuration..."
manpage_section = "8"
operating_system = "macos"
date = "Sun Feb 16 04:48:13 2025"
+++

cupsd 8 "CUPS" "26 April 2019" "Apple Inc."

## NAME

cupsd - cups scheduler

## SYNOPSIS

cupsd
[
-c
cupsd.conf
] [
-f
] [
-F
] [
-h
] [
-l
] [
-s
cups-files.conf
] [
-t
]

## DESCRIPTION

cupsd
is the scheduler for CUPS. It implements a printing system based upon the Internet Printing Protocol, version 2.1, and supports most of the requirements for IPP Everywhere. If no options are specified on the command-line then the default configuration file
/etc/cups/cupsd.conf
will be used.

## OPTIONS


-c \ cupsd.conf
Uses the named cupsd.conf configuration file.

-f
Run
cupsd
in the foreground; the default is to run in the background as a "daemon".

-F
Run
cupsd
in the foreground but detach the process from the controlling terminal and current directory. This is useful for running
cupsd
from
init (8).

-h
Shows the program usage.

-l
This option is passed to
cupsd
when it is run from
launchd (8)
or
systemd (8).

-s \ cups-files.conf
Uses the named cups-files.conf configuration file.

-t
Test the configuration file for syntax errors.

## FILES


```
.I /etc/cups/classes.conf
.I /etc/cups/cups-files.conf
.I /etc/cups/cupsd.conf
.I /usr/share/cups/mime/mime.convs
.I /usr/share/cups/mime/mime.types
.I /etc/cups/printers.conf
.I /etc/cups/subscriptions.conf
```


## CONFORMING TO

cupsd
implements all of the required IPP/2.1 attributes and operations. It also implements several CUPS-specific administrative operations.

## EXAMPLES

Run
cupsd
in the background with the default configuration file:

```

    cupsd

```

Test a configuration file called
test.conf :

```

    cupsd \-t \-c test.conf

```

Run
cupsd
in the foreground with a test configuration file called
test.conf :

```

    cupsd \-f \-c test.conf

```


## SEE ALSO

backend (7),
classes.conf (5),
cups (1),
cups-files.conf (5),
cups-lpd (8),
cupsd.conf (5),
cupsd-helper (8),
cupsd-logs (8),
filter (7),
launchd (8),
mime.convs (5),
mime.types (5),
printers.conf (5),
systemd (8),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
