+++
manpage_section = "8"
keywords = ["fbaeutil", "files", "fi", "system", "library", "coreservices", "appleeventsd", "launchdaemons", "com", "apple", "appleevents", "plist", "history", "fbappleeventsd", "first", "appeared", "in", "mac", "os", "x", "10", "8"]
title = "appleeventsd(8)"
author = "None Specified"
manpage_name = "appleeventsd"
detected_package_version = "0.7.3"
manpage_format = "troff"
operating_system_version = "15.3"
operating_system = "macos"
description = "appleeventsd acts as a system-wide daemon for most AppleEvents between applications on the system. It is loaded by launchd. OPTIONS --server Run as the system-wide server. --noStartupNotification Do not send out a notification to all processes allo..."
date = "Sun Feb 16 04:48:12 2025"
+++

.
"APPLEEVENTSD" "" "June 2012" "" ""
.

## NAME

**appleeventsd**
.
.P
appleeventsd(8) -- System-wide daemon which coordinates AppleEvents activity on the system
.

## DESCRIPTION

appleeventsd acts as a system-wide daemon for most AppleEvents between applications on the system\. It is loaded by launchd\.
.
.P
OPTIONS
.

- \(bu
**--server** Run as the system-wide server\.
.

- \(bu
**--noStartupNotification** Do not send out a notification to all processes allowing them to reconnect, sent after **appleeventsd** restarts\.
.
"" 0
.

## SEE ALSO

**aeutil**
.

## FILES

*/System/Library/CoreServices/appleeventsd*
.
.P
*/System/Library/LaunchDaemons/com\.apple\.coreservices\.appleevents\.plist*
.

## HISTORY

**appleeventsd** first appeared in Mac OS X 10\.8\.
