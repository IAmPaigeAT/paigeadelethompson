+++
description = "The Postfix discard(8) delivery agent processes delivery requests from the queue manager. Each request specifies a queue file, a sender address, a next-hop destination that is treated as the reason for discarding the mail, and recipient informatio..."
operating_system_version = "15.3"
title = "discard(8)"
date = "Sun Feb 16 04:48:14 2025"
manpage_name = "discard"
manpage_section = "8"
keywords = ["na", "qmgr", "queue", "manager", "bounce", "delivery", "status", "reports", "error", "postfix", "agent", "postconf", "configuration", "parameters", "master", "generic", "daemon", "options", "process", "syslogd", "system", "logging", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "history", "service", "was", "introduced", "version", "2", "author", "victor", "duchovni", "morgan", "stanley", "based", "on", "code", "by", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
operating_system = "macos"
manpage_format = "troff"
detected_package_version = "0"
author = "None Specified"
+++

DISCARD 8


## NAME

discard
-
Postfix discard mail delivery agent

## SYNOPSIS

.na

```
discard [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The Postfix **discard**(8) delivery agent processes
delivery requests from
the queue manager. Each request specifies a queue file, a sender
address, a next-hop destination that is treated as the reason for
discarding the mail, and recipient information.
The reason may be prefixed with an RFC 3463-compatible detail code.
This program expects to be run from the **master**(8) process
manager.

The **discard**(8) delivery agent pretends to deliver all recipients
in the delivery request, logs the "next-hop" destination
as the reason for discarding the mail, updates the
queue file, and either marks recipients as finished or informs the
queue manager that delivery should be tried again at a later time.

Delivery status reports are sent to the **trace**(8)
daemon as appropriate.

## SECURITY

.na

```
.ad
```

The **discard**(8) mailer is not security-sensitive. It does not talk
to the network, and can be run chrooted at fixed low privilege.

## STANDARDS

.na

```
RFC 3463 (Enhanced Status Codes)
.SH DIAGNOSTICS
.ad
```

Problems and transactions are logged to **syslogd**(8).

Depending on the setting of the **notify_classes** parameter,
the postmaster is notified of bounces and of other trouble.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are picked up automatically as **discard**(8)
processes run for only a limited amount of time. Use the command
"**postfix reload**" to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBdelay_logging_resolution_limit
The maximal number of digits after the decimal point when logging
sub-second delay values.

- \fBdouble_bounce_sender
The sender address of postmaster notifications that are generated
by the mail system.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## SEE ALSO

.na

```
qmgr(8), queue manager
bounce(8), delivery status reports
error(8), Postfix error delivery agent
postconf(5), configuration parameters
master(5), generic daemon options
master(8), process manager
syslogd(8), system logging
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## HISTORY


This service was introduced with Postfix version 2.2.

## AUTHOR(S)

.na

```
Victor Duchovni
Morgan Stanley

Based on code by:
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
