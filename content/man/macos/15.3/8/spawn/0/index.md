+++
manpage_format = "troff"
manpage_name = "spawn"
operating_system_version = "15.3"
keywords = ["na", "postconf", "configuration", "parameters", "master", "process", "manager", "syslogd", "system", "logging", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
detected_package_version = "0"
manpage_section = "8"
title = "spawn(8)"
operating_system = "macos"
author = "None Specified"
date = "Sun Feb 16 04:48:13 2025"
description = "The spawn(8) daemon provides the Postfix equivalent of inetd. It listens on a port as specified in the Postfix master.cf file and spawns an external command whenever a connection is established. The connection can be made over local IPC (such as U..."
+++

SPAWN 8


## NAME

spawn
-
Postfix external command spawner

## SYNOPSIS

.na

```
spawn [generic Postfix daemon options] command_attributes...
.SH DESCRIPTION
.ad
```

The **spawn**(8) daemon provides the Postfix equivalent
of **inetd**.
It listens on a port as specified in the Postfix **master.cf** file
and spawns an external command whenever a connection is established.
The connection can be made over local IPC (such as UNIX-domain
sockets) or over non-local IPC (such as TCP sockets).
The command\'s standard input, output and error streams are connected
directly to the communication endpoint.

This daemon expects to be run from the **master**(8) process
manager.

## COMMAND ATTRIBUTE SYNTAX

.na

```
.ad
```

The external command attributes are given in the **master.cf**
file at the end of a service definition.  The syntax is as follows:

- \fBuser=\fIusername
"**user**=*username*:*groupname*"
The external command is executed with the rights of the
specified *username*.  The software refuses to execute
commands with root privileges, or with the privileges of the
mail system owner. If *groupname* is specified, the
corresponding group ID is used instead of the group ID
of *username*.

- \fBargv=\fIcommand...
The command to be executed. This must be specified as the
last command attribute.
The command is executed directly, i.e. without interpretation of
shell meta characters by a shell command interpreter.

## BUGS


In order to enforce standard Postfix process resource controls,
the **spawn**(8) daemon runs only one external command at a time.
As such, it presents a noticeable overhead by wasting precious
process resources. The **spawn**(8) daemon is expected to be
replaced by a more structural solution.

## DIAGNOSTICS


The **spawn**(8) daemon reports abnormal child exits.
Problems are logged to **syslogd**(8).

## SECURITY

.na

```
```

This program needs root privilege in order to execute external
commands as the specified user. It is therefore security sensitive.
However the **spawn**(8) daemon does not talk to the external command
and thus is not vulnerable to data-driven attacks.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are picked up automatically as **spawn**(8)
processes run for only a limited amount of time. Use the command
"**postfix reload**" to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

In the text below, *transport* is the first field of the entry
in the **master.cf** file.

## RESOURCE AND RATE CONTROL

.na

```
.ad
```


- \fItransport\fB_time_limit
The amount of time the command is allowed to run before it is
terminated.

Postfix 2.4 and later support a suffix that specifies the
time unit: s (seconds), m (minutes), h (hours), d (days),
w (weeks). The default time unit is seconds.

## MISCELLANEOUS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBexport_environment (see 'postconf -d'
The list of environment variables that a Postfix process will export
to non-Postfix processes.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBmail_owner
The UNIX system account that owns the Postfix queue and most Postfix
daemon processes.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## SEE ALSO

.na

```
postconf(5), configuration parameters
master(8), process manager
syslogd(8), system logging
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
