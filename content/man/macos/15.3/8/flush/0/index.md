+++
manpage_format = "troff"
manpage_section = "8"
detected_package_version = "0"
manpage_name = "flush"
date = "Sun Feb 16 04:48:14 2025"
author = "None Specified"
operating_system = "macos"
title = "flush(8)"
description = "The flush(8) server maintains a record of deferred mail by destination. This information is used to improve the performance of the SMTP ETRN request, and of its command-line equivalent, sendmail -qR or postqueue -f. This program expects to be..."
operating_system_version = "15.3"
keywords = ["na", "smtpd", "smtp", "server", "qmgr", "queue", "manager", "postconf", "configuration", "parameters", "master", "generic", "daemon", "options", "process", "syslogd", "system", "logging", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "etrn_readme", "postfix", "etrn", "howto", "license", "the", "secure", "mailer", "must", "be", "distributed", "with", "software", "history", "service", "was", "introduced", "version", "1", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
+++

FLUSH 8


## NAME

flush
-
Postfix fast flush server

## SYNOPSIS

.na

```
flush [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The **flush**(8) server maintains a record of deferred
mail by destination.
This information is used to improve the performance of the SMTP
**ETRN** request, and of its command-line equivalent,
"**sendmail -qR**" or "**postqueue -f**".
This program expects to be run from the **master**(8) process
manager.

The record is implemented as a per-destination logfile with
as contents the queue IDs of deferred mail. A logfile is
append-only, and is truncated when delivery is requested
for the corresponding destination. A destination is the
part on the right-hand side of the right-most **@** in
an email address.

Per-destination logfiles of deferred mail are maintained only for
eligible destinations. The list of eligible destinations is
specified with the **fast_flush_domains** configuration parameter,
which defaults to **$relay_domains**.

This server implements the following requests:

- \fBadd\fI sitename
Inform the **flush**(8) server that the message with the specified
queue ID is queued for the specified destination.

- \fBsend_site\fI
Request delivery of mail that is queued for the specified
destination.

- \fBsend_file\fI
Request delivery of the specified deferred message.
**refresh**
Refresh non-empty per-destination logfiles that were not read in
**$fast_flush_refresh_time** hours, by simulating
send requests (see above) for the corresponding destinations.

Delete empty per-destination logfiles that were not updated in
**$fast_flush_purge_time** days.

This request completes in the background.
**purge**
Do a **refresh** for all per-destination logfiles.

## SECURITY

.na

```
.ad
```

The **flush**(8) server is not security-sensitive. It does not
talk to the network, and it does not talk to local users.
The fast flush server can run chrooted at fixed low privilege.

## DIAGNOSTICS


Problems and transactions are logged to **syslogd**(8).

## BUGS


Fast flush logfiles are truncated only after a "send"
request, not when mail is actually delivered, and therefore can
accumulate outdated or redundant data. In order to maintain sanity,
"refresh" must be executed periodically. This can
be automated with a suitable wakeup timer setting in the
**master.cf** configuration file.

Upon receipt of a request to deliver mail for an eligible
destination, the **flush**(8) server requests delivery of all messages
that are listed in that destination's logfile, regardless of the
recipients of those messages. This is not an issue for mail
that is sent to a **relay_domains** destination because
such mail typically only has recipients in one domain.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are picked up automatically as **flush**(8)
processes run for only a limited amount of time. Use the command
"**postfix reload**" to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBfast_flush_domains
Optional list of destinations that are eligible for per-destination
logfiles with mail that is queued to those destinations.

- \fBfast_flush_refresh_time
The time after which a non-empty but unread per-destination "fast
flush" logfile needs to be refreshed.

- \fBfast_flush_purge_time
The time after which an empty per-destination "fast flush" logfile
is deleted.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBparent_domain_matches_subdomains (see 'postconf -d'
A list of Postfix features where the pattern "example.com" also
matches subdomains of example.com,
instead of requiring an explicit ".example.com" pattern.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## FILES

.na

```
/var/spool/postfix/flush, "fast flush" logfiles.
.SH "SEE ALSO"
.na

```
smtpd(8), SMTP server
qmgr(8), queue manager
postconf(5), configuration parameters
master(5), generic daemon options
master(8), process manager
syslogd(8), system logging
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
ETRN_README, Postfix ETRN howto
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## HISTORY


This service was introduced with Postfix version 1.0.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
