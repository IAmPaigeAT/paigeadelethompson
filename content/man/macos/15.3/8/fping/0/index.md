+++
author = "None Specified"
manpage_section = "8"
keywords = ["header", "see", "also", "f", "cwping"]
description = "fping is a program like ping which uses the Internet Control Message Protocol (s-1ICMPs0) echo request to determine if a target host is responding. fping differs from ping in that you can specify any number of targets on the command line, or specif..."
operating_system_version = "15.3"
manpage_name = "fping"
title = "fping(8)"
detected_package_version = "0"
date = "2023-09-08"
operating_system = "macos"
manpage_format = "troff"
+++

.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.    \" fudge factors for nroff and troff
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] 
.\}
.    ds #H ((1u-(\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ 
.    ds #] 
.\}
.    \" simple accents for nroff and troff
.    ds ' 
.    ds ` 
.    ds ^ 
.    ds , 
.    ds ~ ~
.    ds /
.\}
.    ds ' \k:\h'-(\n(.wu*8/10-\*(#H)'\'\h"|\n:u"
.    ds ` \k:\h'-(\n(.wu*8/10-\*(#H)'\`\h'|\n:u'
.    ds ^ \k:\h'-(\n(.wu*10/11-\*(#H)'^\h'|\n:u'
.    ds , \k:\h'-(\n(.wu*8/10)',\h'|\n:u'
.    ds ~ \k:\h'-(\n(.wu-\*(#H-.1m)'~\h'|\n:u'
.    ds / \k:\h'-(\n(.wu*8/10-\*(#H)'\z\(sl\h'|\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \k:\h'-(\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \k:\h'-(\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\v'.25m'\h'-\*(#H'
.ds D- D\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.    \" for low resolution devices (crt and lpr)
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
Title "FPING 8"
FPING 8 "2023-09-08" "fping" ""

## NAME

fping - send ICMP ECHO_REQUEST packets to network hosts

## SYNOPSIS

Header "SYNOPSIS"
**fping** [ *options* ] [ *systems...* ]

## DESCRIPTION

Header "DESCRIPTION"
**fping** is a program like **ping** which uses the Internet Control Message
Protocol (\s-1ICMP\s0) echo request to determine if a target host is responding.
**fping** differs from **ping** in that you can specify any number of targets on the
command line, or specify a file containing the lists of targets to ping.
Instead of sending to one target until it times out or replies, **fping** will
send out a ping packet and move on to the next target in a round-robin fashion.
In the default mode, if a target replies, it is noted and removed from the list
of targets to check; if a target does not respond within a certain time limit
and/or retry limit it is designated as unreachable. **fping** also supports
sending a specified number of pings to a target, or looping indefinitely (as in
**ping** ). Unlike **ping**, **fping** is meant to be used in scripts, so its
output is designed to be easy to parse.  Current statistics can be obtained without
termination of process with signal \s-1SIGQUIT\s0 (^\\ from the keyboard on most systems).

## OPTIONS

Header "OPTIONS"

- \fB-4, \fB--ipv4
Item "-4, --ipv4"
Restrict name resolution and IPs to IPv4 addresses.

- \fB-6, \fB--ipv6
Item "-6, --ipv6"
Restrict name resolution and IPs to IPv6 addresses.

- \fB-a, \fB--alive
Item "-a, --alive"
Show systems that are alive.

- \fB-A, \fB--addr
Item "-A, --addr"
Display targets by address rather than \s-1DNS\s0 name. Combined with -d, the output
will be both the ip and (if available) the hostname.

- \fB-b, \fB--size=\fI\s-1BYTES\s0
Item "-b, --size=BYTES"
Number of bytes of ping data to send.  The minimum size (normally 12) allows
room for the data that **fping** needs to do its work (sequence number,
timestamp).  The reported received data size includes the \s-1IP\s0 header (normally
20 bytes) and \s-1ICMP\s0 header (8 bytes), so the minimum total size is 40 bytes.
Default is 56, as in **ping**. Maximum is the theoretical maximum \s-1IP\s0 datagram
size (64K), though most systems limit this to a smaller, system-dependent
number.

- \fB-B, \fB--backoff=\fIN
Item "-B, --backoff=N"
Backoff factor. In the default mode, **fping** sends several requests to a
target before giving up, waiting longer for a reply on each successive request.
This parameter is the value by which the wait time (**-t**) is multiplied on each
successive request; it must be entered as a floating-point number (x.y). The
default is 1.5.

- \fB-c, \fB--count=\fIN
Item "-c, --count=N"
Number of request packets to send to each target.  In this mode, a line is
displayed for each received response (this can suppressed with **-q** or **-Q**).
Also, statistics about responses for each target are displayed when all
requests have been sent (or when interrupted).

- \fB-C, \fB--vcount=\fIN
Item "-C, --vcount=N"
Similar to **-c**, but the per-target statistics are displayed in a format
designed for automated response-time statistics gathering. For example:
.Sp
.Vb 2
 $ fping -C 5 -q somehost
 somehost : 91.7 37.0 29.2 - 36.8
.Ve
.Sp
shows the response time in milliseconds for each of the five requests, with the
\f(CW\*(C`-\*(C' indicating that no response was received to the fourth request.

- \fB-d, \fB--rdns
Item "-d, --rdns"
Use \s-1DNS\s0 to lookup address of ping target. This allows you to give fping
a list of \s-1IP\s0 addresses as input and print hostnames in the output. This is similar
to option **-n**/**--name**, but will force a reverse-DNS lookup even if you give
hostnames as target (\s-1NAME-\s0>\s-1IP-\s0>\s-1NAME\s0).

- \fB-D, \fB--timestamp
Item "-D, --timestamp"
Add Unix timestamps in front of output lines generated with in looping or counting
modes (**-l**, **-c**, or **-C**).

- \fB-e, \fB--elapsed
Item "-e, --elapsed"
Show elapsed (round-trip) time of packets.

- \fB-f, \fB--file
Item "-f, --file"
Read list of targets from a file.

- \fB-g, \fB--generate \fIaddr/mask
Item "-g, --generate addr/mask"
Generate a target list from a supplied \s-1IP\s0 netmask, or a starting and ending \s-1IP.\s0
Specify the netmask or start/end in the targets portion of the command line. If
a network with netmask is given, the network and broadcast addresses will be
excluded. ex. To ping the network 192.168.1.0/24, the specified command line
could look like either:
.Sp
.Vb 1
 $ fping -g 192.168.1.0/24
.Ve
.Sp
or
.Sp
.Vb 1
 $ fping -g 192.168.1.1 192.168.1.254
.Ve

- \fB-h, \fB--help
Item "-h, --help"
Print usage message.

- \fB-H, \fB--ttl=\fIN
Item "-H, --ttl=N"
Set the \s-1IP TTL\s0 field (time to live hops).

- \fB-i, \fB--interval=\fI\s-1MSEC\s0
Item "-i, --interval=MSEC"
The minimum amount of time (in milliseconds) between sending a ping packet
to any target (default is 10, minimum is 1).

- \fB-I, \fB--iface=\fI\s-1IFACE\s0
Item "-I, --iface=IFACE"
Set the interface (requires \s-1SO_BINDTODEVICE\s0 support).

- \fB-l, \fB--loop
Item "-l, --loop"
Loop sending packets to each target indefinitely. Can be interrupted with
Ctrl-C; statistics about responses for each target are then displayed.

- \fB-m, \fB--all
Item "-m, --all"
Send pings to each of a target host's multiple \s-1IP\s0 addresses (use of option '-A'
is recommended).

- \fB-M, \fB--dontfrag
Item "-M, --dontfrag"
Set the \*(L"Don't Fragment\*(R" bit in the \s-1IP\s0 header (used to determine/test the \s-1MTU\s0).

- \fB-n, \fB--name
Item "-n, --name"
If targets are specified as \s-1IP\s0 addresses, do a reverse-DNS lookup on them
to print hostnames in the output.

- \fB-N, \fB--netdata
Item "-N, --netdata"
Format output for netdata (-l -Q are required). See: <https://netdata.cloud/>

- \fB-o, \fB--outage
Item "-o, --outage"
Calculate \*(L"outage time\*(R" based on the number of lost pings and the interval used (useful for network convergence tests).

- \fB-O, \fB--tos=\fIN
Item "-O, --tos=N"
Set the typ of service flag (\s-1TOS\s0). *N* can be either decimal or hexadecimal
(0xh) format.

- \fB-p, \fB--period=\fI\s-1MSEC\s0
Item "-p, --period=MSEC"
In looping or counting modes (**-l**, **-c**, or **-C**), this parameter sets
the time in milliseconds that **fping** waits between successive packets to
an individual target. Default is 1000 and minimum is 10.

- \fB-q, \fB--quiet
Item "-q, --quiet"
Quiet. Don't show per-probe results, but only the final summary. Also don't
show \s-1ICMP\s0 error messages.

- \fB-Q, \fB--squiet=\fI\s-1SECS\s0
Item "-Q, --squiet=SECS"
Like **-q**, but additionally show interval summary results every *\s-1SECS\s0*
seconds.

- \fB-r, \fB--retry=\fIN
Item "-r, --retry=N"
Retry limit (default 3). This is the number of times an attempt at pinging
a target will be made, not including the first try.

- \fB-R, \fB--random
Item "-R, --random"
Instead of using all-zeros as the packet data, generate random bytes.
Use to defeat, e.g., link data compression.

- \fB-s, \fB--stats
Item "-s, --stats"
Print cumulative statistics upon exit.

- \fB-S, \fB--src=\fIaddr
Item "-S, --src=addr"
Set source address.

- \fB-t, \fB--timeout=\fI\s-1MSEC\s0
Item "-t, --timeout=MSEC"
Initial target timeout in milliseconds. In the default, non-loop mode, the
default timeout is 500ms, and it represents the amount of time that **fping**
waits for a response to its first request. Successive timeouts are multiplied
by the backoff factor specified with **-B**.
.Sp
In loop/count mode, the default timeout is automatically adjusted to match
the \*(L"period\*(R" value (but not more than 2000ms). You can still adjust the timeout
value with this option, if you wish to, but note that setting a value larger
than \*(L"period\*(R" produces inconsistent results, because the timeout value can
be respected only for the last ping.
.Sp
Also note that any received replies that are larger than the timeout value, will
be discarded.

- \fB-T \fIn
Item "-T n"
Ignored (for compatibility with fping 2.4).

- \fB-u, \fB--unreach
Item "-u, --unreach"
Show targets that are unreachable.

- \fB-v, \fB--version
Item "-v, --version"
Print **fping** version information.

- \fB-x, \fB--reachable=\fIN
Item "-x, --reachable=N"
Given a list of hosts, this mode checks if number of reachable hosts is >= N
and exits true in that case.

- \fB-X, \fB--fast-reachable=\fIN
Item "-X, --fast-reachable=N"
Given a list of hosts, this mode immediately exits true once N alive hosts
have been found.

## EXAMPLES

Header "EXAMPLES"
Generate 20 pings to two hosts in ca. 1 second (i.e. one ping every 50 ms to
each host), and report every ping \s-1RTT\s0 at the end:

.Vb 1
 $ fping --quiet --interval=1 --vcount=20 --period=50 127.0.0.1 127.0.0.2
.Ve

## AUTHORS

Header "AUTHORS"

- \(bu
Roland J. Schemers \s-1III,\s0 Stanford University, concept and versions 1.x

- \(bu
\s-1RL\s0 \*(L"Bob\*(R" Morgan, Stanford University, versions 2.x

- \(bu
David Papp, versions 2.3x and up

- \(bu
David Schweikert, versions 3.0 and up

**fping website: <http://www.fping.org>**

## DIAGNOSTICS

Header "DIAGNOSTICS"
Exit status is 0 if all the hosts are reachable, 1 if some hosts
were unreachable, 2 if any \s-1IP\s0 addresses were not found, 3 for invalid command
line arguments, and 4 for a system call failure.

## RESTRICTIONS

Header "RESTRICTIONS"
If fping was configured with \f(CW\*(C`--enable-safe-limits\*(C', the following values are
not allowed for non-root users:

- \(bu
**-i** *n*, where *n* < 1 msec

- \(bu
**-p** *n*, where *n* < 10 msec

## SEE ALSO

Header "SEE ALSO"
\f(CWping(8)
