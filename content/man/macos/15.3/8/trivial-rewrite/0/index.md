+++
manpage_name = "trivial-rewrite"
title = "trivial-rewrite(8)"
keywords = ["na", "postconf", "configuration", "parameters", "transport", "table", "format", "relocated", "of", "the", "user", "has", "moved", "master", "process", "manager", "syslogd", "system", "logging", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "address_class_readme", "postfix", "address", "classes", "howto", "address_verification_readme", "verification", "license", "secure", "mailer", "must", "be", "distributed", "with", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
author = "None Specified"
operating_system_version = "15.3"
operating_system = "macos"
detected_package_version = "0"
manpage_format = "troff"
date = "Sun Feb 16 04:48:12 2025"
description = "The trivial-rewrite(8) daemon processes three types of client service requests: Rewrite an address to standard form, according to the address rewriting context: Append the domain names specified with $myorigin or $mydomain to incomplete addresses..."
manpage_section = "8"
+++

TRIVIAL-REWRITE 8


## NAME

trivial-rewrite
-
Postfix address rewriting and resolving daemon

## SYNOPSIS

.na

```
trivial\-rewrite [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The **trivial-rewrite**(8) daemon processes three types of client
service requests:

- \fBrewrite \fIcontext
Rewrite an address to standard form, according to the
address rewriting context:

> **local**
Append the domain names specified with **$myorigin** or
**$mydomain** to incomplete addresses; do **swap_bangpath**
and **allow_percent_hack** processing as described below, and
strip source routed addresses (*@site,@site:user@domain*)
to *user@domain* form.
**remote**
Append the domain name specified with
**$remote_header_rewrite_domain** to incomplete
addresses. Otherwise the result is identical to that of
the **local** address rewriting context. This prevents
Postfix from appending the local domain to spam from poorly
written remote clients.



- \fBresolve \fIsender
Resolve the address to a (*transport*, *nexthop*,
*recipient*, *flags*) quadruple. The meaning of
the results is as follows:

> *transport*
The delivery agent to use. This is the first field of an entry
in the **master.cf** file.
*nexthop*
The host to send to and optional delivery method information.
*recipient*
The envelope recipient address that is passed on to *nexthop*.
*flags*
The address class, whether the address requires relaying,
whether the address has problems, and whether the request failed.



- \fBverify \fIsender
Resolve the address for address verification purposes.

## SERVER PROCESS MANAGEMENT

.na

```
.ad
```

The **trivial-rewrite**(8) servers run under control by
the Postfix master
server.  Each server can handle multiple simultaneous connections.
When all servers are busy while a client connects, the master
creates a new server process, provided that the trivial-rewrite
server process limit is not exceeded.
Each trivial-rewrite server terminates after
serving at least **$max_use** clients of after **$max_idle**
seconds of idle time.

## STANDARDS

.na

```
.ad
```

None. The command does not interact with the outside world.

## SECURITY

.na

```
.ad
```

The **trivial-rewrite**(8) daemon is not security sensitive.
By default, this daemon does not talk to remote or local users.
It can run at a fixed low privilege in a chrooted environment.

## DIAGNOSTICS


Problems and transactions are logged to **syslogd**(8).

## CONFIGURATION PARAMETERS

.na

```
.ad
```

On busy mail systems a long time may pass before a **main.cf**
change affecting **trivial-rewrite**(8) is picked up. Use the command
"**postfix reload**" to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

## COMPATIBILITY CONTROLS

.na

```
.ad
```


- \fBresolve_dequoted_address
Resolve a recipient address safely instead of correctly, by
looking inside quotes.

Available with Postfix version 2.1 and later:

- \fBresolve_null_domain
Resolve an address that ends in the "@" null domain as if the
local hostname were specified, instead of rejecting the address as
invalid.

Available with Postfix version 2.3 and later:

- \fBresolve_numeric_domain
Resolve "user@ipaddress" as "user@[ipaddress]", instead of
rejecting the address as invalid.

Available with Postfix version 2.5 and later:

- \fBallow_min_user
Allow a sender or recipient address to have `-' as the first
character.

## ADDRESS REWRITING CONTROLS

.na

```
.ad
```


- \fBmyorigin
The domain name that locally-posted mail appears to come
from, and that locally posted mail is delivered to.

- \fBallow_percent_hack
Enable the rewriting of the form "user%domain" to "user@domain".

- \fBappend_at_myorigin
With locally submitted mail, append the string "@$myorigin" to mail
addresses without domain information.

- \fBappend_dot_mydomain (Postfix >= 3.0: no, Postfix < 3.0:
With locally submitted mail, append the string ".$mydomain" to
addresses that have no ".domain" information.

- \fBrecipient_delimiter
The set of characters that can separate a user name from its
extension (example: user+foo), or a .forward file name from its
extension (example: .forward+foo).

- \fBswap_bangpath
Enable the rewriting of "site!user" into "user@site".

Available in Postfix 2.2 and later:

- \fBremote_header_rewrite_domain
Don't rewrite message headers from remote clients at all when
this parameter is empty; otherwise, rewrite message headers and
append the specified domain name to incomplete addresses.

## ROUTING CONTROLS

.na

```
.ad
```

The following is applicable to Postfix version 2.0 and later.
Earlier versions do not have support for: virtual_transport,
relay_transport, virtual_alias_domains, virtual_mailbox_domains
or proxy_interfaces.

- \fBlocal_transport
The default mail delivery transport and next-hop destination
for final delivery to domains listed with mydestination, and for
[ipaddress] destinations that match $inet_interfaces or $proxy_interfaces.

- \fBvirtual_transport
The default mail delivery transport and next-hop destination for
final delivery to domains listed with $virtual_mailbox_domains.

- \fBrelay_transport
The default mail delivery transport and next-hop destination for
remote delivery to domains listed with $relay_domains.

- \fBdefault_transport
The default mail delivery transport and next-hop destination for
destinations that do not match $mydestination, $inet_interfaces,
$proxy_interfaces, $virtual_alias_domains, $virtual_mailbox_domains,
or $relay_domains.

- \fBparent_domain_matches_subdomains (see 'postconf -d'
A list of Postfix features where the pattern "example.com" also
matches subdomains of example.com,
instead of requiring an explicit ".example.com" pattern.

- \fBrelayhost
The next-hop destination of non-local mail; overrides non-local
domains in recipient addresses.

- \fBtransport_maps
Optional lookup tables with mappings from recipient address to
(message delivery transport, next-hop destination).

Available in Postfix version 2.3 and later:

- \fBsender_dependent_relayhost_maps
A sender-dependent override for the global relayhost parameter
setting.

Available in Postfix version 2.5 and later:

- \fBempty_address_relayhost_maps_lookup_key
The sender_dependent_relayhost_maps search string that will be
used instead of the null sender address.

Available in Postfix version 2.7 and later:

- \fBempty_address_default_transport_maps_lookup_key
The sender_dependent_default_transport_maps search string that
will be used instead of the null sender address.

- \fBsender_dependent_default_transport_maps
A sender-dependent override for the global default_transport
parameter setting.

## ADDRESS VERIFICATION CONTROLS

.na

```
.ad
```

Postfix version 2.1 introduces sender and recipient address verification.
This feature is implemented by sending probe email messages that
are not actually delivered.
By default, address verification probes use the same route
as regular mail. To override specific aspects of message
routing for address verification probes, specify one or more
of the following:

- \fBaddress_verify_local_transport
Overrides the local_transport parameter setting for address
verification probes.

- \fBaddress_verify_virtual_transport
Overrides the virtual_transport parameter setting for address
verification probes.

- \fBaddress_verify_relay_transport
Overrides the relay_transport parameter setting for address
verification probes.

- \fBaddress_verify_default_transport
Overrides the default_transport parameter setting for address
verification probes.

- \fBaddress_verify_relayhost
Overrides the relayhost parameter setting for address verification
probes.

- \fBaddress_verify_transport_maps
Overrides the transport_maps parameter setting for address verification
probes.

Available in Postfix version 2.3 and later:

- \fBaddress_verify_sender_dependent_relayhost_maps
Overrides the sender_dependent_relayhost_maps parameter setting for address
verification probes.

Available in Postfix version 2.7 and later:

- \fBaddress_verify_sender_dependent_default_transport_maps
Overrides the sender_dependent_default_transport_maps parameter
setting for address verification probes.

## MISCELLANEOUS CONTROLS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBempty_address_recipient
The recipient of mail addressed to the null address.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBrelocated_maps
Optional lookup tables with new contact information for users or
domains that no longer exist.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBshow_user_unknown_table_name
Display the name of the recipient table in the "User unknown"
responses.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

Available in Postfix version 2.0 and later:

- \fBhelpful_warnings
Log warnings about problematic configuration settings, and provide
helpful suggestions.

## SEE ALSO

.na

```
postconf(5), configuration parameters
transport(5), transport table format
relocated(5), format of the "user has moved" table
master(8), process manager
syslogd(8), system logging
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
ADDRESS_CLASS_README, Postfix address classes howto
ADDRESS_VERIFICATION_README, Postfix address verification
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
