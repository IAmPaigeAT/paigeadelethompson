+++
manpage_name = "virtual"
description = "The virtual(8) delivery agent is designed for virtual mail hosting services. Originally based on the Postfix local(8) delivery agent, this agent looks up recipients with map lookups of their full recipient address, instead of using hard-coded unix..."
detected_package_version = "0"
title = "virtual(8)"
manpage_format = "troff"
author = "None Specified"
keywords = ["na", "qmgr", "queue", "manager", "bounce", "delivery", "status", "reports", "postconf", "configuration", "parameters", "syslogd", "system", "logging", "readme_files", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "virtual_readme", "domain", "hosting", "howto", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "software", "history", "agent", "was", "originally", "based", "on", "postfix", "local", "modifications", "mainly", "consisted", "of", "removing", "code", "that", "either", "not", "applicable", "safe", "in", "context", "aliases", "user", "forward", "files", "command", "file", "name", "fbdelivered-to", "message", "header", "appears", "fbqmail", "by", "daniel", "bernstein", "fbmaildir", "structure", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011", "andrew", "mcnamara", "andrewm", "connect", "com", "au", "pty", "ltd", "level", "3", "213", "miller", "st", "north", "sydney", "2060", "nsw", "australia"]
operating_system_version = "15.3"
manpage_section = "8"
operating_system = "macos"
date = "Sun Feb 16 04:48:13 2025"
+++

VIRTUAL 8


## NAME

virtual
-
Postfix virtual domain mail delivery agent

## SYNOPSIS

.na

```
virtual [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The **virtual**(8) delivery agent is designed for virtual mail
hosting services. Originally based on the Postfix **local**(8)
delivery
agent, this agent looks up recipients with map lookups of their
full recipient address, instead of using hard-coded unix password
file lookups of the address local part only.

This delivery agent only delivers mail.  Other features such as
mail forwarding, out-of-office notifications, etc., must be
configured via virtual_alias maps or via similar lookup mechanisms.

## MAILBOX LOCATION

.na

```
.ad
```

The mailbox location is controlled by the **virtual_mailbox_base**
and **virtual_mailbox_maps** configuration parameters (see below).
The **virtual_mailbox_maps** table is indexed by the recipient
address as described under TABLE SEARCH ORDER below.

The mailbox pathname is constructed as follows:


```
  $virtual_mailbox_base/$virtual_mailbox_maps(recipient)
```


where *recipient* is the full recipient address.

## UNIX MAILBOX FORMAT

.na

```
.ad
```

When the mailbox location does not end in **/**, the message
is delivered in UNIX mailbox format.   This format stores multiple
messages in one textfile.

The **virtual**(8) delivery agent prepends a "**From **sender
time_stamp" envelope header to each message, prepends a
**Delivered-To:** message header with the envelope recipient
address,
prepends an **X-Original-To:** header with the recipient address as
given to Postfix,
prepends a **Return-Path:** message header with the
envelope sender address, prepends a **>** character to lines
beginning with "**From **", and appends an empty line.

The mailbox is locked for exclusive access while delivery is in
progress. In case of problems, an attempt is made to truncate the
mailbox to its original length.

## QMAIL MAILDIR FORMAT

.na

```
.ad
```

When the mailbox location ends in **/**, the message is delivered
in qmail **maildir** format. This format stores one message per file.

The **virtual**(8) delivery agent prepends a **Delivered-To:**
message header with the final envelope recipient address,
prepends an **X-Original-To:** header with the recipient address as
given to Postfix, and prepends a
**Return-Path:** message header with the envelope sender address.

By definition, **maildir** format does not require application-level
file locking during mail delivery or retrieval.

## MAILBOX OWNERSHIP

.na

```
.ad
```

Mailbox ownership is controlled by the **virtual_uid_maps**
and **virtual_gid_maps** lookup tables, which are indexed
with the full recipient address. Each table provides
a string with the numerical user and group ID, respectively.

The **virtual_minimum_uid** parameter imposes a lower bound on
numerical user ID values that may be specified in any
**virtual_uid_maps**.

## CASE FOLDING

.na

```
.ad
```

All delivery decisions are made using the full recipient
address, folded to lower case. See also the next section
for a few exceptions with optional address extensions.

## TABLE SEARCH ORDER

.na

```
.ad
```

Normally, a lookup table is specified as a text file that
serves as input to the **postmap**(1) command. The result, an
indexed file in **dbm** or **db** format, is used for fast
searching by the mail system.

The search order is as follows. The search stops
upon the first successful lookup.
\(bu
When the recipient has an optional address extension the
*user+extension@domain.tld* address is looked up first.

With Postfix versions before 2.1, the optional address extension
is always ignored.
\(bu
The *user@domain.tld* address, without address extension,
is looked up next.
\(bu
Finally, the recipient *@domain* is looked up.

When the table is provided via other means such as NIS, LDAP
or SQL, the same lookups are done as for ordinary indexed files.

Alternatively, a table can be provided as a regular-expression
map where patterns are given as regular expressions. In that case,
only the full recipient address is given to the regular-expression
map.

## SECURITY

.na

```
.ad
```

The **virtual**(8) delivery agent is not security sensitive, provided
that the lookup tables with recipient user/group ID information are
adequately protected. This program is not designed to run chrooted.

The **virtual**(8) delivery agent disallows regular expression
substitution of $1 etc. in regular expression lookup tables,
because that would open a security hole.

The **virtual**(8) delivery agent will silently ignore requests
to use the **proxymap**(8) server. Instead it will open the
table directly. Before Postfix version 2.2, the virtual
delivery agent will terminate with a fatal error.

## STANDARDS

.na

```
RFC 822 (ARPA Internet Text Messages)
.SH DIAGNOSTICS
.ad
```

Mail bounces when the recipient has no mailbox or when the
recipient is over disk quota. In all other cases, mail for
an existing recipient is deferred and a warning is logged.

Problems and transactions are logged to **syslogd**(8).
Corrupted message files are marked so that the queue
manager can move them to the **corrupt** queue afterwards.

Depending on the setting of the **notify_classes** parameter,
the postmaster is notified of bounces and of other trouble.

## BUGS


This delivery agent supports address extensions in email
addresses and in lookup table keys, but does not propagate
address extension information to the result of table lookup.

Postfix should have lookup tables that can return multiple result
attributes. In order to avoid the inconvenience of maintaining
three tables, use an LDAP or MYSQL database.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are picked up automatically, as
**virtual**(8)
processes run for only a limited amount of time. Use the command
"**postfix reload**" to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

## MAILBOX DELIVERY CONTROLS

.na

```
.ad
```


- \fBvirtual_mailbox_base
A prefix that the **virtual**(8) delivery agent prepends to all pathname
results from $virtual_mailbox_maps table lookups.

- \fBvirtual_mailbox_maps
Optional lookup tables with all valid addresses in the domains that
match $virtual_mailbox_domains.

- \fBvirtual_minimum_uid
The minimum user ID value that the **virtual**(8) delivery agent accepts
as a result from $virtual_uid_maps table lookup.

- \fBvirtual_uid_maps
Lookup tables with the per-recipient user ID that the **virtual**(8)
delivery agent uses while writing to the recipient's mailbox.

- \fBvirtual_gid_maps
Lookup tables with the per-recipient group ID for **virtual**(8) mailbox
delivery.

Available in Postfix version 2.0 and later:

- \fBvirtual_mailbox_domains
Postfix is final destination for the specified list of domains;
mail is delivered via the $virtual_transport mail delivery transport.

- \fBvirtual_transport
The default mail delivery transport and next-hop destination for
final delivery to domains listed with $virtual_mailbox_domains.

Available in Postfix version 2.5.3 and later:

- \fBstrict_mailbox_ownership
Defer delivery when a mailbox file is not owned by its recipient.

## LOCKING CONTROLS

.na

```
.ad
```


- \fBvirtual_mailbox_lock (see 'postconf -d'
How to lock a UNIX-style **virtual**(8) mailbox before attempting
delivery.

- \fBdeliver_lock_attempts
The maximal number of attempts to acquire an exclusive lock on a
mailbox file or **bounce**(8) logfile.

- \fBdeliver_lock_delay
The time between attempts to acquire an exclusive lock on a mailbox
file or **bounce**(8) logfile.

- \fBstale_lock_time
The time after which a stale exclusive mailbox lockfile is removed.

## RESOURCE AND RATE CONTROLS

.na

```
.ad
```


- \fBvirtual_destination_concurrency_limit
The maximal number of parallel deliveries to the same destination
via the virtual message delivery transport.

- \fBvirtual_destination_recipient_limit
The maximal number of recipients per message for the virtual
message delivery transport.

- \fBvirtual_mailbox_limit
The maximal size in bytes of an individual **virtual**(8) mailbox or
maildir file, or zero (no limit).

## MISCELLANEOUS CONTROLS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBdelay_logging_resolution_limit
The maximal number of digits after the decimal point when logging
sub-second delay values.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

Available in Postfix version 3.0 and later:

- \fBvirtual_delivery_status_filter
Optional filter for the **virtual**(8) delivery agent to change the
delivery status code or explanatory text of successful or unsuccessful
deliveries.

## SEE ALSO

.na

```
qmgr(8), queue manager
bounce(8), delivery status reports
postconf(5), configuration parameters
syslogd(8), system logging
.SH "README_FILES"
.na

```
Use "postconf readme_directory" or
"postconf html_directory" to locate this information.
VIRTUAL_README, domain hosting howto
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## HISTORY



This delivery agent was originally based on the Postfix local delivery
agent. Modifications mainly consisted of removing code that either
was not applicable or that was not safe in this context: aliases,
~user/.forward files, delivery to "|command" or to /file/name.

The **Delivered-To:** message header appears in the **qmail**
system by Daniel Bernstein.

The **maildir** structure appears in the **qmail** system
by Daniel Bernstein.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA

Andrew McNamara
andrewm@connect.com.au
connect.com.au Pty. Ltd.
Level 3, 213 Miller St
North Sydney 2060, NSW, Australia
