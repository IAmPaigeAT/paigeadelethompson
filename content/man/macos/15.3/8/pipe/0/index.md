+++
manpage_section = "8"
manpage_format = "troff"
manpage_name = "pipe"
operating_system_version = "15.3"
title = "pipe(8)"
operating_system = "macos"
detected_package_version = "0"
author = "None Specified"
date = "Sun Feb 16 04:48:15 2025"
description = "The pipe(8) daemon processes requests from the Postfix queue manager to deliver messages to external commands. This program expects to be run from the master(8) process manager."
keywords = ["na", "qmgr", "queue", "manager", "bounce", "delivery", "status", "reports", "postconf", "configuration", "parameters", "master", "generic", "daemon", "options", "process", "syslogd", "system", "logging", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
+++

PIPE 8


## NAME

pipe
-
Postfix delivery to external command

## SYNOPSIS

.na

```
pipe [generic Postfix daemon options] command_attributes...
.SH DESCRIPTION
.ad
```

The **pipe**(8) daemon processes requests from the Postfix queue
manager to deliver messages to external commands.
This program expects to be run from the **master**(8) process
manager.

Message attributes such as sender address, recipient address and
next-hop host name can be specified as command-line macros that are
expanded before the external command is executed.

The **pipe**(8) daemon updates queue files and marks recipients
as finished, or it informs the queue manager that delivery should
be tried again at a later time. Delivery status reports are sent
to the **bounce**(8), **defer**(8) or **trace**(8) daemon as
appropriate.

## SINGLE-RECIPIENT DELIVERY

.na

```
.ad
```

Some destinations cannot handle more than one recipient per
delivery request. Examples are pagers or fax machines.
In addition, multi-recipient delivery is undesirable when
prepending a **Delivered-to:** or **X-Original-To:**
message header.

To prevent Postfix from sending multiple recipients per delivery
request, specify


```
    transport_destination_recipient_limit = 1
```


in the Postfix **main.cf** file, where *transport*
is the name in the first column of the Postfix **master.cf**
entry for the pipe-based delivery transport.

## COMMAND ATTRIBUTE SYNTAX

.na

```
.ad
```

The external command attributes are given in the **master.cf**
file at the end of a service definition.  The syntax is as follows:

- \fBchroot=\fIpathname
Change the process root directory and working directory to
the named directory. This happens before switching to the
privileges specified with the **user** attribute, and
before executing the optional **directory=**pathname
directive. Delivery is deferred in case of failure.

This feature is available as of Postfix 2.3.

- \fBdirectory=\fIpathname
Change to the named directory before executing the external command.
The directory must be accessible for the user specified with the
**user** attribute (see below).
The default working directory is **$queue_directory**.
Delivery is deferred in case of failure.

This feature is available as of Postfix 2.2.

- \fBeol=\fIstring (optional, default:
The output record delimiter. Typically one would use either
**\\r\\n** or **\\n**. The usual C-style backslash escape
sequences are recognized: \fB\\a \\b \\f \\n \\r \\t \\v
\\*ddd* (up to three octal digits) and **\\\\**.

- \fBflags=BDFORXhqu.>
Optional message processing flags. By default, a message is
copied unchanged.

> **B**
Append a blank line at the end of each message. This is required
by some mail user agents that recognize "**From **" lines only
when preceded by a blank line.
**D**
Prepend a "**Delivered-To: **recipient" message header with the
envelope recipient address. Note: for this to work, the
\fItransport**_destination_recipient_limit** must be 1
(see SINGLE-RECIPIENT DELIVERY above for details).

The **D** flag also enforces loop detection (Postfix 2.5 and later):
if a message already contains a **Delivered-To:** header
with the same recipient address, then the message is
returned as undeliverable. The address comparison is case
insensitive.

This feature is available as of Postfix 2.0.
**F**
Prepend a "**From **sender time_stamp" envelope header to
the message content.
This is expected by, for example, **UUCP** software.
**O**
Prepend an "**X-Original-To: **recipient" message header
with the recipient address as given to Postfix. Note: for this to
work, the \fItransport**_destination_recipient_limit** must be 1
(see SINGLE-RECIPIENT DELIVERY above for details).

This feature is available as of Postfix 2.0.
**R**
Prepend a **Return-Path:** message header with the envelope sender
address.
**X**
Indicate that the external command performs final delivery.
This flag affects the status reported in "success" DSN
(delivery status notification) messages, and changes it
from "relayed" into "delivered".

This feature is available as of Postfix 2.5.
**h**
Fold the command-line **$original_recipient** and
**$recipient** address domain part
(text to the right of the right-most **@** character) to
lower case; fold the entire command-line **$domain** and
**$nexthop** host or domain information to lower case.
This is recommended for delivery via **UUCP**.
**q**
Quote white space and other special characters in the command-line
**$sender**, **$original_recipient** and **$recipient**
address localparts (text to the
left of the right-most **@** character), according to an 8-bit
transparent version of RFC 822.
This is recommended for delivery via **UUCP** or **BSMTP**.

The result is compatible with the address parsing of command-line
recipients by the Postfix **sendmail**(1) mail submission command.

The **q** flag affects only entire addresses, not the partial
address information from the **$user**, **$extension** or
**$mailbox** command-line macros.
**u**
Fold the command-line **$original_recipient** and
**$recipient** address localpart (text to
the left of the right-most **@** character) to lower case.
This is recommended for delivery via **UUCP**.
**.**
Prepend "**.**" to lines starting with "**.**". This is needed
by, for example, **BSMTP** software.
**>**
Prepend "**>**" to lines starting with "**From **". This is expected
by, for example, **UUCP** software.



- \fBnull_sender=\fIreplacement (default:
Replace the null sender address (typically used for delivery
status notifications) with the specified text
when expanding the **$sender** command-line macro, and
when generating a From_ or Return-Path: message header.

If the null sender replacement text is a non-empty string
then it is affected by the **q** flag for address quoting
in command-line arguments.

The null sender replacement text may be empty; this form
is recommended for content filters that feed mail back into
Postfix. The empty sender address is not affected by the
**q** flag for address quoting in command-line arguments.

Caution: a null sender address is easily mis-parsed by
naive software. For example, when the **pipe**(8) daemon
executes a command such as:


```
    Wrong: command \-f$sender \-\- $recipient
```

.IP
the command will mis-parse the -f option value when the
sender address is a null string.  For correct parsing,
specify **$sender** as an argument by itself:


```
    Right: command \-f $sender \-\- $recipient
```

.IP
This feature is available as of Postfix 2.3.

- \fBsize=\fIsize_limit
Don't deliver messages that exceed this size limit (in
bytes); return them to the sender instead.

- \fBuser=\fIusername
"**user**=*username*:*groupname*"
Execute the external command with the user ID and group ID of the
specified *username*.  The software refuses to execute
commands with root privileges, or with the privileges of the
mail system owner. If *groupname* is specified, the
corresponding group ID is used instead of the group ID of
*username*.

- \fBargv=\fIcommand...
The command to be executed. This must be specified as the
last command attribute.
The command is executed directly, i.e. without interpretation of
shell meta characters by a shell command interpreter.

Specify "\{" and "\}" around command arguments that contain
whitespace (Postfix 3.0 and later). Whitespace
after "\{" and before "\}" is ignored.

In the command argument vector, the following macros are recognized
and replaced with corresponding information from the Postfix queue
manager delivery request.

In addition to the form $\{*name*\}, the forms $*name* and
the deprecated form $(*name*) are also recognized.
Specify **$$** where a single **$** is wanted.

> **$\{client_address\}**
This macro expands to the remote client network address.

This feature is available as of Postfix 2.2.
**$\{client_helo\}**
This macro expands to the remote client HELO command parameter.

This feature is available as of Postfix 2.2.
**$\{client_hostname\}**
This macro expands to the remote client hostname.

This feature is available as of Postfix 2.2.
**$\{client_port\}**
This macro expands to the remote client TCP port number.

This feature is available as of Postfix 2.5.
**$\{client_protocol\}**
This macro expands to the remote client protocol.

This feature is available as of Postfix 2.2.
**$\{domain\}**
This macro expands to the domain portion of the recipient
address.  For example, with an address *user+foo@domain*
the domain is *domain*.

This information is modified by the **h** flag for case folding.

This feature is available as of Postfix 2.5.
**$\{extension\}**
This macro expands to the extension part of a recipient address.
For example, with an address *user+foo@domain* the extension is
*foo*.

A command-line argument that contains **$\{extension\}** expands
into as many command-line arguments as there are recipients.

This information is modified by the **u** flag for case folding.
**$\{mailbox\}**
This macro expands to the complete local part of a recipient address.
For example, with an address *user+foo@domain* the mailbox is
*user+foo*.

A command-line argument that contains **$\{mailbox\}**
expands to as many command-line arguments as there are recipients.

This information is modified by the **u** flag for case folding.
**$\{nexthop\}**
This macro expands to the next-hop hostname.

This information is modified by the **h** flag for case folding.
**$\{original_recipient\}**
This macro expands to the complete recipient address before any
address rewriting or aliasing.

A command-line argument that contains
**$\{original_recipient\}** expands to as many
command-line arguments as there are recipients.

This information is modified by the **hqu** flags for quoting
and case folding.

This feature is available as of Postfix 2.5.
**$\{queue_id\}**
This macro expands to the queue id.

This feature is available as of Postfix 2.11.
**$\{recipient\}**
This macro expands to the complete recipient address.

A command-line argument that contains **$\{recipient\}**
expands to as many command-line arguments as there are recipients.

This information is modified by the **hqu** flags for quoting
and case folding.
**$\{sasl_method\}**
This macro expands to the name of the SASL authentication
mechanism in the AUTH command when the Postfix SMTP server
received the message.

This feature is available as of Postfix 2.2.
**$\{sasl_sender\}**
This macro expands to the SASL sender name (i.e. the original
submitter as per RFC 4954) in the MAIL FROM command when
the Postfix SMTP server received the message.

This feature is available as of Postfix 2.2.
**$\{sasl_username\}**
This macro expands to the SASL user name in the AUTH command
when the Postfix SMTP server received the message.

This feature is available as of Postfix 2.2.
**$\{sender\}**
This macro expands to the envelope sender address. By default,
the null sender address expands to MAILER-DAEMON; this can
be changed with the **null_sender** attribute, as described
above.

This information is modified by the **q** flag for quoting.
**$\{size\}**
This macro expands to Postfix's idea of the message size, which
is an approximation of the size of the message as delivered.
**$\{user\}**
This macro expands to the username part of a recipient address.
For example, with an address *user+foo@domain* the username
part is *user*.

A command-line argument that contains **$\{user\}** expands
into as many command-line arguments as there are recipients.

This information is modified by the **u** flag for case folding.



## STANDARDS

.na

```
RFC 3463 (Enhanced status codes)
.SH DIAGNOSTICS
.ad
```

Command exit status codes are expected to
follow the conventions defined in <**sysexits.h**>.
Exit status 0 means normal successful completion.

In the case of a non-zero exit status, a limited amount of
command output is logged, and reported in a delivery status
notification.  When the output begins with a 4.X.X or 5.X.X
enhanced status code, the status code takes precedence over
the non-zero exit status (Postfix version 2.3 and later).

After successful delivery (zero exit status) a limited
amount of command output is logged, and reported in "success"
delivery status notifications (Postfix 3.0 and later).
This command output is not examined for the presence of an
enhanced status code.

Problems and transactions are logged to **syslogd**(8).
Corrupted message files are marked so that the queue manager
can move them to the **corrupt** queue for further inspection.

## SECURITY

.na

```
```

This program needs a dual personality 1) to access the private
Postfix queue and IPC mechanisms, and 2) to execute external
commands as the specified user. It is therefore security sensitive.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are picked up automatically as **pipe**(8)
processes run for only a limited amount of time. Use the command
"**postfix reload**" to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

## RESOURCE AND RATE CONTROLS

.na

```
.ad
```

In the text below, *transport* is the first field in a
**master.cf** entry.

- \fItransport\fB_destination_concurrency_limit
Limit the number of parallel deliveries to the same destination,
for delivery via the named *transport*.
The limit is enforced by the Postfix queue manager.

- \fItransport\fB_destination_recipient_limit
Limit the number of recipients per message delivery, for delivery
via the named *transport*.
The limit is enforced by the Postfix queue manager.

- \fItransport\fB_time_limit
Limit the time for delivery to external command, for delivery via
the named *transport*.
The limit is enforced by the pipe delivery agent.

Postfix 2.4 and later support a suffix that specifies the
time unit: s (seconds), m (minutes), h (hours), d (days),
w (weeks). The default time unit is seconds.

## MISCELLANEOUS CONTROLS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBdelay_logging_resolution_limit
The maximal number of digits after the decimal point when logging
sub-second delay values.

- \fBexport_environment (see 'postconf -d'
The list of environment variables that a Postfix process will export
to non-Postfix processes.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBmail_owner
The UNIX system account that owns the Postfix queue and most Postfix
daemon processes.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBrecipient_delimiter
The set of characters that can separate a user name from its
extension (example: user+foo), or a .forward file name from its
extension (example: .forward+foo).

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

Available in Postfix version 3.0 and later:

- \fBpipe_delivery_status_filter
Optional filter for the **pipe**(8) delivery agent to change the
delivery status code or explanatory text of successful or unsuccessful
deliveries.

## SEE ALSO

.na

```
qmgr(8), queue manager
bounce(8), delivery status reports
postconf(5), configuration parameters
master(5), generic daemon options
master(8), process manager
syslogd(8), system logging
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
