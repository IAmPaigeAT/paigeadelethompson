+++
author = "None Specified"
keywords = ["cancel", "1", "cupsenable", "8", "lp", "lpadmin", "lpstat", "br", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
operating_system = "macos"
manpage_name = "cupsaccept"
detected_package_version = "2.0"
manpage_section = "8"
description = "The command instructs the printing system to accept print jobs to the specified destinations. The command instructs the printing system to reject print jobs to the specified destinations. The -r option sets the reason for rejecting print jobs. If..."
date = "Sun Feb 16 04:48:14 2025"
manpage_format = "troff"
title = "cupsaccept(8)"
operating_system_version = "15.3"
+++

cupsaccept 8 "CUPS" "26 April 2019" "Apple Inc."

## NAME

cupsaccept/cupsreject - accept/reject jobs sent to a destination

## SYNOPSIS

cupsaccept
[
-E
] [
-U
username
] [
-h
hostname[:port]
]
destination(s)
.br
cupsreject
[
-E
] [
-U
username
] [
-h
hostname[:port]
] [
-r
reason
]
destination(s)

## DESCRIPTION

The
cupsaccept
command instructs the printing system to accept print jobs to the specified destinations.

The
cupsreject
command instructs the printing system to reject print jobs to the
specified destinations.
The *-r* option sets the reason for rejecting print jobs. If not specified, the reason defaults to "Reason Unknown".

## OPTIONS

The following options are supported by both
cupsaccept
and
cupsreject :

-E
Forces encryption when connecting to the server.

**-U **username
Sets the username that is sent when connecting to the server.

**-h **hostname[:port]
Chooses an alternate server.

**-r **"*reason*"
Sets the reason string that is shown for a printer that is rejecting jobs.

## CONFORMING TO

The
cupsaccept
and
cupsreject
commands correspond to the System V printing system commands "accept" and "reject", respectively.
Unlike the System V printing system, CUPS allows printer names to contain any printable character except SPACE, TAB, "/", or "#".
Also, printer and class names are *not* case-sensitive.

Finally, the CUPS versions may ask the user for an access password depending on the printing system configuration.

## SEE ALSO

cancel (1),
cupsenable (8),
lp (1),
lpadmin (8),
lpstat (1),
.br
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
