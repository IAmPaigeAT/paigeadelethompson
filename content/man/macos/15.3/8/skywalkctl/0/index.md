+++
manpage_format = "troff"
title = "skywalkctl(8)"
manpage_section = "8"
date = "2018-01-24"
keywords = ["netstat", "lsof"]
manpage_name = "skywalkctl"
detected_package_version = "0"
operating_system = "macos"
description = "skywalkctl is a utility used to interact with the Skywalk subsystem, which provides the plumbing between various networking-related pieces of software and hardware. It should only be used in a test and debug context. Using it for any other purpose..."
operating_system_version = "15.3"
author = "None Specified"
+++

SKYWALKCTL 8 "Jul 24, 2018"

## NAME

skywalkctl - Interact with Skywalk subsystem

## SYNOPSIS

**skywalkctl** <command> [ <args> ]

## DESCRIPTION

**skywalkctl** is a utility used to interact with the Skywalk subsystem,
which provides the plumbing between various networking-related pieces of
software and hardware.
It should only be used in a test and debug context. Using it for any other
purpose is strongly discouraged.
It is composed of multiple commands list below. Use
skywalkctl *COMMAND* help to see
usage of each command.


## COMMANDS


\fCshow
Prints a brief overview of Skywalk runtime.

\fCflow
Display Skywalk flow.

\fCflow-adv
Display flow advisory information.

\fCflow-owner
Display flow owner information.

\fCflow-route
Display flow route information.

\fCchannel
Display channels with performance and error statistics.

\fCmemory
Display memory usage.

\fCprovider
Display Skywalk nexus providers and their child instances.

\fCnetns
Display information about port reservations.

\fClog
Display and manipulate Skywalk kernel logging verbosity.
\fCenable
Enable/disable Skywalk via boot-args.

## SEE ALSO

netstat(1), lsof(8)
