+++
keywords = ["cancel", "1", "cupsaccept", "8", "cupsenable", "lp", "lpadmin", "lpr", "lprm", "lpstat", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
operating_system = "macos"
detected_package_version = "2.0"
author = "None Specified"
description = "lpc provides limited control over printer and class queues provided by CUPS. It can also be used to query the state of queues. If no command is specified on the command-line, lpc displays a prompt and accepts commands from the standard input. The lp..."
title = "lpc(8)"
date = "Sun Feb 16 04:48:13 2025"
manpage_name = "lpc"
manpage_section = "8"
operating_system_version = "15.3"
manpage_format = "troff"
+++

lpc 8 "CUPS" "26 April 2019" "Apple Inc."

## NAME

lpc - line printer control program (deprecated)

## SYNOPSIS

lpc
[
command
[
parameter(s)
] ]

## DESCRIPTION

**lpc** provides limited control over printer and class queues provided by CUPS. It can also be used to query the state of queues.

If no command is specified on the command-line, **lpc** displays a prompt and accepts commands from the standard input.

### COMMANDS

The **lpc** program accepts a subset of commands accepted by the Berkeley **lpc** program of the same name:

exit
Exits the command interpreter.

**help **[*command*]

**? **[*command*]
Displays a short help message.

quit
Exits the command interpreter.

**status **[*queue*]
Displays the status of one or more printer or class queues.

## NOTES

This program is deprecated and will be removed in a future feature release of CUPS.

Since **lpc** is geared towards the Berkeley printing system, it is impossible to use **lpc** to configure printer or class queues provided by CUPS.
To configure printer or class queues you must use the
lpadmin (8)
command or another CUPS-compatible client with that functionality.

## SEE ALSO

cancel (1),
cupsaccept (8),
cupsenable (8),
lp (1),
lpadmin (8),
lpr (1),
lprm (1),
lpstat (1),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
