+++
description = "The  program is actually just a shell script which invokes the  daemon.   It is provided for backward compatibility. It causes  to call all systems for which there is work. Any option which may be given to may also be given to  For more detai..."
keywords = ["uucp", "uux", "uustat", "uucico", "uuxqt", "author", "ian", "lance", "taylor", "airs", "com", "text", "for", "this", "manpage", "comes", "from", "version", "1", "07", "info", "documentation"]
detected_package_version = "0"
author = "None Specified"
operating_system_version = "15.3"
date = "Sun Feb 16 04:48:15 2025"
manpage_name = "uusched"
title = "uusched(8)"
manpage_format = "troff"
operating_system = "macos"
manpage_section = "8"
+++

uusched 8 "Taylor UUCP 1.07"

## NAME

uusched - schedule the uucp file transport program

## SYNOPSIS

uusched
[ options ]

## DESCRIPTION

The
uusched
program is actually just a shell script which invokes
the
uucico
daemon.
It is provided for backward compatibility.
It causes
uucico
to call all systems for which there is work.
Any option which may be given to
uucico
may also be given to
uusched \.
For more details, see
uucico(8) \.

## SEE ALSO

uucp(1), uux(1), uustat(1), uucico(8), uuxqt(8)

## AUTHOR

Ian Lance Taylor
<ian@airs.com>.
Text for this Manpage comes from Taylor UUCP, version 1.07 Info documentation.

