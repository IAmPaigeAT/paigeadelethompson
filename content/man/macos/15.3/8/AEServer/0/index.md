+++
manpage_format = "troff"
manpage_section = "8"
detected_package_version = "0"
date = "Sun Feb 16 04:48:13 2025"
operating_system_version = "15.3"
manpage_name = "AEServer"
operating_system = "macos"
title = "AEServer(8)"
author = "None Specified"
description = "AEServer acts as a system-wide daemon when Remote AppleEvents is enabled, allowing other computers to send AppleEvents to applications on this machine. aeutil,  appeleventsd /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/AE..."
+++

.
"AEServer" "" "January 2017"
.

## NAME

**AEServer**
.
.P
AEServer(8) -- System-wide daemon which recevies incoming (remote) AppleEvents
.

## DESCRIPTION

AEServer acts as a system-wide daemon when Remote AppleEvents is enabled, allowing other computers to send AppleEvents to applications on this machine\.
"" 0
.

## SEE ALSO

**aeutil**,
**appeleventsd**
.

## FILES

*/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/AE.framework/Support/AEServer*
.
.P
*/System/Library/LaunchDaemons/com\.apple\.eppc\.appleevents\.plist*
