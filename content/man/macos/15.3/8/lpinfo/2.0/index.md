+++
operating_system = "macos"
manpage_format = "troff"
manpage_section = "8"
description = "lpinfo lists the available devices or drivers known to the CUPS server. The first form (-m) lists the available drivers, while the second form (-v) lists the available devices. lpinfo accepts the following options: Forces encryption when connecting..."
keywords = ["lpadmin", "8", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
date = "Sun Feb 16 04:48:16 2025"
operating_system_version = "15.3"
author = "None Specified"
detected_package_version = "2.0"
manpage_name = "lpinfo"
title = "lpinfo(8)"
+++

lpinfo 8 "CUPS" "26 April 2019" "Apple Inc."

## NAME

lpinfo - show available devices or drivers (deprecated)

## SYNOPSIS

lpinfo
[
-E
] [
**-h **server[**:**port]
] [
-l
] [
--device-id
device-id-string
] [
--exclude-schemes
scheme-list
] [
--include-schemes
scheme-list
] [
--language
locale
] [
--make-and-model
name
] [
--product
name
]
-m
.br
lpinfo
[
-E
] [
**-h **server[**:**port]
] [
-l
] [
--exclude-schemes
scheme-list
] [
--include-schemes
scheme-list
] [
--timeout
seconds
]
-v

## DESCRIPTION

**lpinfo** lists the available devices or drivers known to the CUPS server.
The first form (*-m*) lists the available drivers, while the second form (*-v*) lists the available devices.

## OPTIONS

**lpinfo** accepts the following options:

-E
Forces encryption when connecting to the server.

**-h **server[**:**port]
Selects an alternate server.

-l
Shows a "long" listing of devices or drivers.

**--device-id **device-id-string
Specifies the IEEE-1284 device ID to match when listing drivers with the *-m* option.

**--exclude-schemes **scheme-list
Specifies a comma-delimited list of device or PPD schemes that should be excluded from the results.
Static PPD files use the "file" scheme.

**--include-schemes **scheme-list
Specifies a comma-delimited list of device or PPD schemes that should be included in the results.
Static PPD files use the "file" scheme.

**--language **locale
Specifies the language to match when listing drivers with the *-m* option.

**--make-and-model **name
Specifies the make and model to match when listing drivers with the *-m* option.

**--product **name
Specifies the product to match when listing drivers with the *-m* option.

**--timeout **seconds
Specifies the timeout when listing devices with the *-v* option.

## CONFORMING TO

The *lpinfo* command is unique to CUPS.

## EXAMPLES

List all devices:

```

    lpinfo \-v

```

List all drivers:

```

    lpinfo \-m

```

List drivers matching "HP LaserJet":

```

    lpinfo \-\-make\-and\-model "HP LaserJet" \-m
```


## NOTES

CUPS printer drivers and backends are deprecated and will no longer be supported in a future feature release of CUPS.
Printers that do not support IPP can be supported using applications such as
ippeveprinter (1).

## SEE ALSO

lpadmin (8),
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
