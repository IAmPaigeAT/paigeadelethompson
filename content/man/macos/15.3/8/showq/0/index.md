+++
date = "Sun Feb 16 04:48:16 2025"
keywords = ["na", "pickup", "local", "mail", "service", "cleanup", "canonicalize", "and", "enqueue", "qmgr", "queue", "manager", "postconf", "configuration", "parameters", "master", "process", "syslogd", "system", "logging", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
manpage_section = "8"
operating_system = "macos"
operating_system_version = "15.3"
manpage_format = "troff"
detected_package_version = "0"
description = "The showq(8) daemon reports the Postfix mail queue status. The output is meant to be formatted by the postqueue(1) command, as it emulates the Sendmail `mailq command."
title = "showq(8)"
manpage_name = "showq"
author = "None Specified"
+++

SHOWQ 8


## NAME

showq
-
list the Postfix mail queue

## SYNOPSIS

.na

```
showq [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The **showq**(8) daemon reports the Postfix mail queue status.
The output is meant to be formatted by the postqueue(1) command,
as it emulates the Sendmail `mailq' command.

The **showq**(8) daemon can also be run in stand-alone mode
by the superuser. This mode of operation is used to emulate
the `mailq' command while the Postfix mail system is down.

## SECURITY

.na

```
.ad
```

The **showq**(8) daemon can run in a chroot jail at fixed low
privilege, and takes no input from the client. Its service port
is accessible to local untrusted users, so the service can be
susceptible to denial of service attacks.

## STANDARDS

.na

```
.ad
```

None. The **showq**(8) daemon does not interact with the
outside world.

## DIAGNOSTICS


Problems and transactions are logged to **syslogd**(8).

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are picked up automatically as **showq**(8)
processes run for only a limited amount of time. Use the command
"**postfix reload**" to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBduplicate_filter_limit
The maximal number of addresses remembered by the address
duplicate filter for **aliases**(5) or **virtual**(5) alias expansion, or
for **showq**(8) queue displays.

- \fBempty_address_recipient
The recipient of mail addressed to the null address.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

Available in Postfix version 2.9 and later:

- \fBenable_long_queue_ids
Enable long, non-repeating, queue IDs (queue file names).

## FILES

.na

```
/var/spool/postfix, queue directories
.SH "SEE ALSO"
.na

```
pickup(8), local mail pickup service
cleanup(8), canonicalize and enqueue mail
qmgr(8), queue manager
postconf(5), configuration parameters
master(8), process manager
syslogd(8), system logging
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
