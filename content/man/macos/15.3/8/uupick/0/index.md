+++
operating_system_version = "15.3"
detected_package_version = "0"
author = "None Specified"
keywords = ["uucp", "uuto", "author", "ian", "lance", "taylor", "airs", "com", "text", "for", "this", "manpage", "comes", "from", "version", "1", "07", "info", "documentation"]
manpage_section = "8"
operating_system = "macos"
title = "uupick(8)"
manpage_format = "troff"
manpage_name = "uupick"
date = "Sun Feb 16 04:48:17 2025"
description = "The  program is used to conveniently retrieve files transferred by the  program. For each file transferred by  will display the source system, the file name,  and whether the name refers to a regular file or a directory. It will then wait for th..."
+++

uupick 8 "Taylor UUCP 1.07"

## NAME

uupick - retrieve files transferred by uuto

## SYNOPSIS

uupick
[-s system] [--system system]

## DESCRIPTION

The
uupick
program is used to conveniently retrieve files transferred by the
uuto
program.

For each file transferred by
uuto,
uupick
will display the source system, the file name,
and whether the name refers to a regular file or a directory.
It will then wait for the user to specify an action to take.
One of the following commands must be entered:

`q'
     Quit out of `uupick'.

`RETURN'
     Skip the file.

`m [directory]'
     Move the file or directory to the specified directory.  If no
     directory is specified, the file is moved to the current directory.

`a [directory]'
     Move all files from this system to the specified directory.  If no
     directory is specified, the files are moved to the current
     directory.

`p'
     List the file on standard output.

`d'
     Delete the file.

`! [command]'
     Execute `command' as a shell escape.

## OPTIONS


-s system, --system system
Used to restrict `uupick' to
only present files transferred from a particular system.

Standard UUCP options:

-x type, --debug type
Turn on particular debugging types.  The following types are
recognized: abnormal, chat, handshake, uucp-proto, proto, port,
config, spooldir, execute, incoming, outgoing.

-I file, --config
Set configuration file to use.

-v, --version
Report version information and exit.

--help
Print a help message and exit.

## SEE ALSO

uucp(1), uuto(1)

## AUTHOR

Ian Lance Taylor
<ian@airs.com>.
Text for this Manpage comes from Taylor UUCP, version 1.07 Info documentation.

