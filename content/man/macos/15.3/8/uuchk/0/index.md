+++
manpage_name = "uuchk"
manpage_section = "8"
title = "uuchk(8)"
keywords = ["uucp", "author", "ian", "lance", "taylor", "airs", "com", "text", "for", "this", "manpage", "comes", "from", "version", "1", "07", "info", "documentation"]
date = "Sun Feb 16 04:48:16 2025"
detected_package_version = "0"
description = "The  program displays information read from the UUCP configuration files. It should be used to ensure that UUCP has been configured correctly. The  or options may be used to display the configuration for just the specified system,  rather than f..."
operating_system_version = "15.3"
author = "None Specified"
manpage_format = "troff"
operating_system = "macos"
+++

uuchk 8 "Taylor UUCP 1.07"

## NAME

uuchk - display information read from the UUCP configuration files.

## SYNOPSIS

uuchk
[ options ]

## DESCRIPTION

The
uuchk
program displays information read from the UUCP configuration files.
It should be used to ensure that UUCP has been configured correctly.

The
-s
or
--system
options may be used to display the configuration for just the specified system,
rather than for all systems.
The
uuchk
program also supports the standard UUCP program options.

## OPTIONS


-s system, --system system
Display the configuration for just the specified system,
rather than for all systems.

Standard UUCP options:

-I file, --config
Set configuration file to use.

-v, --version
Report version information and exit.

--help
Print a help message and exit.

## SEE ALSO

uucp(1)

## AUTHOR

Ian Lance Taylor
<ian@airs.com>.
Text for this Manpage comes from Taylor UUCP, version 1.07 Info documentation.

