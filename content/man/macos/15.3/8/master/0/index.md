+++
detected_package_version = "0"
operating_system_version = "15.3"
manpage_name = "master"
manpage_section = "8"
date = "Sun Feb 16 04:48:14 2025"
operating_system = "macos"
manpage_format = "troff"
description = "The master(8) daemon is the resident process that runs Postfix daemons on demand: daemons to send or receive messages via the network, daemons to deliver mail locally, etc.  These daemons are created on demand up to a configurable maximum number pe..."
author = "None Specified"
title = "master(8)"
keywords = ["na", "qmgr", "queue", "manager", "verify", "address", "verification", "master", "cf", "configuration", "file", "syntax", "postconf", "main", "syslogd", "system", "logging", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
+++

MASTER 8


## NAME

master
-
Postfix master process

## SYNOPSIS

.na

```
master [\-Ddtvw] [\-c config_dir] [\-e exit_time]
.SH DESCRIPTION
.ad
```

The **master**(8) daemon is the resident process that runs Postfix
daemons on demand: daemons to send or receive messages via the
network, daemons to deliver mail locally, etc.  These daemons are
created on demand up to a configurable maximum number per service.

Postfix daemons terminate voluntarily, either after being idle for
a configurable amount of time, or after having serviced a
configurable number of requests. Exceptions to this rule are the
resident queue manager, address verification server, and the TLS
session cache and pseudo-random number server.

The behavior of the **master**(8) daemon is controlled by the
**master.cf** configuration file, as described in **master**(5).

Options:

- \fB-c
Read the **main.cf** and **master.cf** configuration files in
the named directory instead of the default configuration directory.
This also overrides the configuration files for other Postfix
daemon processes.
**-D**
After initialization, run a debugger on the master process. The
debugging command is specified with the **debugger_command** in
the **main.cf** global configuration file.
**-d**
Do not redirect stdin, stdout or stderr to /dev/null, and
do not discard the controlling terminal. This must be used
for debugging only.

- \fB-e
Terminate the master process after *exit_time* seconds. Child
processes terminate at their convenience.
**-t**
Test mode. Return a zero exit status when the **master.pid** lock
file does not exist or when that file is not locked.  This is evidence
that the **master**(8) daemon is not running.
**-v**
Enable verbose logging for debugging purposes. This option
is passed on to child processes. Multiple **-v** options
make the software increasingly verbose.
**-w**
Wait in a dummy foreground process, while the real master
daemon initializes in a background process.  The dummy
foreground process returns a zero exit status only if the
master daemon initialization is successful, and if it
completes in a reasonable amount of time.

This feature is available in Postfix 2.10 and later.

Signals:
**SIGHUP**
Upon receipt of a **HUP** signal (e.g., after "**postfix reload**"),
the master process re-reads its configuration files. If a service has
been removed from the **master.cf** file, its running processes
are terminated immediately.
Otherwise, running processes are allowed to terminate as soon
as is convenient, so that changes in configuration settings
affect only new service requests.
**SIGTERM**
Upon receipt of a **TERM** signal (e.g., after "**postfix abort**"),
the master process passes the signal on to its child processes and
terminates.
This is useful for an emergency shutdown. Normally one would
terminate only the master ("**postfix stop**") and allow running
processes to finish what they are doing.

## DIAGNOSTICS


Problems are reported to **syslogd**(8). The exit status
is non-zero in case of problems, including problems while
initializing as a master daemon process in the background.

## ENVIRONMENT

.na

```
.ad
```

**MAIL_DEBUG**
After initialization, start a debugger as specified with the
**debugger_command** configuration parameter in the **main.cf**
configuration file.
**MAIL_CONFIG**
Directory with Postfix configuration files.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Unlike most Postfix daemon processes, the **master**(8) server does
not automatically pick up changes to **main.cf**. Changes
to **master.cf** are never picked up automatically.
Use the "**postfix reload**" command after a configuration change.

## RESOURCE AND RATE CONTROLS

.na

```
.ad
```


- \fBdefault_process_limit
The default maximal number of Postfix child processes that provide
a given service.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBservice_throttle_time
How long the Postfix **master**(8) waits before forking a server that
appears to be malfunctioning.

Available in Postfix version 2.6 and later:

- \fBmaster_service_disable
Selectively disable **master**(8) listener ports by service type
or by service name and type.

## MISCELLANEOUS CONTROLS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_directory (see 'postconf -d'
The directory with Postfix support programs and daemon programs.

- \fBdebugger_command
The external command to execute when a Postfix daemon program is
invoked with the -D option.

- \fBinet_interfaces
The network interface addresses that this mail system receives
mail on.

- \fBinet_protocols
The Internet protocols Postfix will attempt to use when making
or accepting connections.

- \fBimport_environment (see 'postconf -d'
The list of environment parameters that a Postfix process will
import from a non-Postfix parent process.

- \fBmail_owner
The UNIX system account that owns the Postfix queue and most Postfix
daemon processes.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## FILES

.na

```
.ad
```

To expand the directory names below into their actual values,
use the command "**postconf config_directory**" etc.
.na

```

$config_directory/main.cf, global configuration file.
$config_directory/master.cf, master server configuration file.
$queue_directory/pid/master.pid, master lock file.
$data_directory/master.lock, master lock file.
.SH "SEE ALSO"
.na

```
qmgr(8), queue manager
verify(8), address verification
master(5), master.cf configuration file syntax
postconf(5), main.cf configuration file syntax
syslogd(8), system logging
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
