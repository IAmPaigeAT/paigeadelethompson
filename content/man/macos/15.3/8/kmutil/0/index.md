+++
author = "None Specified"
manpage_section = "8"
operating_system = "macos"
manpage_name = "kmutil"
detected_package_version = "0"
date = "Sun Feb 16 04:48:13 2025"
manpage_format = "troff"
operating_system_version = "15.3"
title = "kmutil(8)"
description = "f[B]kmutilf[R] is a multipurpose tool for managing kernel extensions (kexts) and kext collections on disk. It takes a subcommand and a number of options, some of which are common to multiple commands. f[B]kmutilf[R] interacts with the KernelManage..."
+++

.ie "\f[CB]x\f[]"x" \{\
. ftr V B
. ftr VI BI
. ftr VB B
. ftr VBI BI
.\}
.el \{\
. ftr V CR
. ftr VI CI
. ftr VB CB
. ftr VBI CBI
.\}
"KMUTIL" "8" "" "2023-08-25" "KernelManagement utility for kext collections"
.hy

## SYNOPSIS


\f[B]kmutil\f[R] \f[B]<subcommand>\f[R]
0
.P
.PD
\f[B]kmutil\f[R] \f[B]<load|unload|showloaded>\f[R]
0
.P
.PD
\f[B]kmutil\f[R] \f[B]<find|libraries|print-diagnostics>\f[R]
0
.P
.PD
\f[B]kmutil\f[R] \f[B]<create|inspect|check|log|dumpstate>\f[R]
0
.P
.PD
\f[B]kmutil\f[R] \f[B]<clear-staging|trigger-panic-medic>\f[R]
0
.P
.PD
\f[B]kmutil\f[R] \f[B]-h\f[R]

## DESCRIPTION


\f[B]kmutil\f[R] is a multipurpose tool for managing kernel extensions
(kexts) and kext collections on disk.
It takes a subcommand and a number of options, some of which are common
to multiple commands.

\f[B]kmutil\f[R] interacts with the KernelManagement subsystem for
loading, unloading, and diagnosing kexts.
It can also be used for inspecting the contents of a kext collection,
interacting with the kernel to query load information, finding kexts and
kext dependencies on disk, creating new collections of kexts, and
displaying other diagnostic information.

## COLLECTIONS


Starting in \f[I]macOS 11\f[R], kernel extensions are found in 3
different artifacts on disk.
Each artifact is loaded exactly once at boot, and a kext must be linked
into one of the three artifacts before it can be used.

- \[bu]
The \f[B]boot kext collection\f[R] contains the kernel and all system
kexts necessary for starting and bootstrapping the operating system.
It is an immutable artifact in
\f[I]/System/Library/KernelCollections\f[R].
On Apple Silicon Macs, this artifact is kept exclusively in the Preboot
volume.

- \[bu]
The \f[B]system kext collection\f[R], if used, contains all remaining
system kexts required by the operating system, and is loaded after boot.
It is prelinked against the \f[B]boot kext collection\f[R], and is also
an immutable artifact in \f[I]/System/Library/KernelCollections\f[R].
Note that on Apple Silicon Macs, there is no system kext collection.

- \[bu]
The \f[B]auxiliary kext collection\f[R], if built, contains kexts placed
in \f[I]/Library/Extensions\f[R] and any other third-party kexts
installed on the system.
It is dynamically built by \f[B]kernelmanagerd(8)\f[R] and prelinked
against the \f[B]boot kext collection\f[R] and, if present, the
\f[B]system kext collection\f[R].
On Apple Silicon Macs, the \f[B]auxiliary kext collection\f[R] is
located in the Preboot volume.
For more information on installing third-party kexts into the
\f[B]auxiliary kext collection\f[R], see \f[B]INSTALLING\f[R].

## INSTALLING


As of macOS 11, a kext is only loadable once it has been built into the
\f[B]auxiliary kext collection\f[R] by \f[B]kernelmanagerd(8)\f[R], and
the system has rebooted.
At boot, \f[B]kernelmanagerd(8)\f[R] will load this collection into the
kernel, which allows all of the kexts in the collection to match and
load.
If \f[B]kmutil load\f[R], \f[B]kextload(8)\f[R], or any invocation of a
\f[B]KextManager\f[R] function attempts to load a kext that is not yet
loadable, \f[B]kernelmanagerd(8)\f[R] will stage the kext into a
protected location, validate it, and prompt the user to approve a
rebuild of the \f[B]auxiliary kext collection\f[R].
If the validation and rebuild are successful, the kext will be available
on the next boot.

## COMMANDS


Commands and their specific options are listed below.
For other options common to most commands, see \f[B]OPTIONS\f[R].

- \[bu]
\f[B]create\f[R]: Create a new kext collection according to the options
provided.
This command should only be used by developers investigating custom
kernels or replacing the contents of the \f[B]boot kext collection\f[R]
or \f[B]system kext collection\f[R].
As of \f[I]macOS 13.0\f[R], a KDK is required to create a new
\f[B]boot\f[R] or \f[B]system kext collection\f[R].
To load or unload kexts already in a collection see the \f[B]load\f[R]
and \f[B]unload\f[R] subcommands.

> 
\f[B]-n,\f[R] \f[B]--new <boot|sys|aux>\f[R]
Specify one or more of \f[B]boot\f[R], \f[B]sys\f[R], or \f[B]aux\f[R]
to build one or more collections at a time.

\f[B]-L,\f[R] \f[B]--no-system-collection\f[R]
If building an auxiliary collection, don\[cq]t look for or generate a
\f[B]system kext collection\f[R].

\f[B]-s,\f[R] \f[B]--strip\f[R]
Specify \f[B]none\f[R], \f[B]all\f[R], or \f[B]allkexts\f[R] (default:
\f[B]none\f[R]) to strip symbol information after a kext has been built
into a collection.

\f[B]-k,\f[R] \f[B]--kernel\f[R]
When building the \f[B]boot kext collection\f[R], specify the path to
the kernel.
If \f[B]-V\f[R] is specified, \f[B]kmutil\f[R] will append the variant
extension.

\f[B]-x,\f[R] \f[B]--explicit-only\f[R]
Only consider the bundle identifiers and paths explicitly specified,
along with their dependencies.

\f[B]--compress\f[R]
Compress results using the LZFSE algorithm.

\f[B]--img4-encode\f[R]
Encode the collection in an img4 payload.



- \[bu]
\f[B]inspect\f[R]: Inspect & display the contents of a kext collection
according to the options provided.

> 
\f[B]--show-mach-header\f[R]
Print the mach header(s) in the collection(s).
Use with --verbose to also display contents of inner fileset entries.

\f[B]--show-fileset-entries\f[R]
Only print mach header information present in fileset subentries.
This is useful for determining prelink addresses and other load
information about a kext in a collection.

\f[B]--show-kext-load-addresses\f[R]
When displaying the default output, include the load addresses of the
kexts inline.

\f[B]--show-kext-uuids\f[R]
Include the UUIDs of each kext in the output.

\f[B]--show-kernel-uuid\f[R]
Print the UUID (and version) of the kernel if present in the collection.
This will output nothing if a kernel is not found in the specified
collection(s).

\f[B]--show-kernel-uuid-only\f[R]
Print the UUID of the kernel if present in the collection, and suppress
default kext information.

\f[B]--show-prelink-info\f[R]
Dump the raw __PRELINK_INFO segment of the collection(s).

\f[B]--show-collection-metadata\f[R]
Print the metadata of the collection(s), such as their prelink uuids,
the uuids of collections they link against, and the build version that
produced the collection.

\f[B]--show-mach-boot-properties\f[R]
Print derived Mach-O boot properties of the collection(s).

\f[B]--json\f[R]
Output the section layout as JSON.



- \[bu]
\f[B]load\f[R]: Load the extension(s) specified with \f[B]-b\f[R] or
\f[B]-p\f[R].
If the extension is not already in the \f[B]auxiliary kext
collection\f[R], the collection will be dynamically rebuilt by
\f[B]kernelmanagerd(8)\f[R] for use on the next reboot.
For more information, see \f[B]INSTALLING\f[R].
For kexts already contained in the \f[B]boot\f[R], \f[B]system\f[R], or
\f[B]auxiliary\f[R] kext collection, the \f[B]load\f[R] subcommand will
start the kext if it has not already been started.

> 
For most kexts, the \f[B]load\f[R] subcommand must run as the superuser
(root).
Kexts installed under \f[B]/System/\f[R] with an
\f[B]OSBundleAllowUserLoad\f[R] property set to true may be loaded via
the \f[B]load\f[R] subcommand by non-root users.

macOS 10.6 introduced C functions for loading kexts,
\f[B]KextManagerLoadKextWithIdentifier()\f[R] and
\f[B]KextManagerLoadKextWithURL()\f[R], which are described in
Apple\[cq]s developer documentation.
These functions continue to be supported as of macOS 11.

\f[B]-P,\f[R] \f[B]--personality-name\f[R]
If this kext is already loaded, send the named personality to the
catalog.

\f[B]-e,\f[R] \f[B]--no-default-repositories\f[R]
Don\[cq]t use the default repositories for kexts.
If you use this option, you will have to explicitly specify all
dependencies of the kext being loaded, or otherwise worked on using the
\f[B]--repository\f[R] option.

\f[B]--load-style\f[R]
Control the load style of the request to load extension.
Valid options:

> 
- \[bu]
\f[B]start-and-match\f[R]: Start the kernel extension and also begin
matching on any accompanying personalities.
(default)

- \[bu]
\f[B]start-only\f[R]: Start any specified kernel extensions but do not
begin matching against any personalities provided by those extensions
(unless matching has already started for them).

- \[bu]
\f[B]match-only\f[R]: Do not explictly start any of the given kernel
extensions but do begin matching on IOKit personalities provided by
them.
This is useful to allow extensions that were previous loaded with
\f[B]start-only\f[R] to now begin matching.





- \[bu]
\f[B]unload\f[R]: Unload the extension(s) specified with \f[B]-b\f[R] or
\f[B]-p\f[R].
The extension must have been previously linked into a kext collection
and loaded by the \f[B]KernelManagement\f[R] system.
A successfull call to the \f[B]unload\f[R] subcommand will invoke the
kext\[cq]s \f[I]stop\f[R] function and end the kext\[cq]s IOKit
lifecycle, however the kext remains in kernel memory as part of the kext
collection from which it was loaded.
The extension will not be removed from any collection, including the
\f[B]auxiliary kext collection\f[R], and will still be available for
loading without requiring a reboot.

> 
If another loaded kext has a dependency on the kext being unloaded, the
unload will fail.
You can determine whether a kext has dependents using the
\f[B]showloaded\f[R] subcommand.

\f[B]-c,\f[R] \f[B]--class-name <class-name>\f[R]
Terminate all instances of the IOService class, but do not unload its
kext or unload its personalities.

\f[B]-P,\f[R] \f[B]--personalities-only\f[R]
Terminate services and remove personalities only; do not unload kexts.



- \[bu]
\f[B]libraries\f[R]: Search for library kexts in the \f[B]boot kext
collection\f[R] and the \f[B]system kext collection\f[R] (if available)
that define symbols needed for linking the specified kexts, printing
their bundle identifiers and versions.
Information on symbols not found are printed after the library kext
information for each architecture.

> 
A handy use of the \f[B]libraries\f[R] subcommand is to run it with just
the \f[B]--xml\f[R] flag and pipe the output to \f[B]pbcopy(1)\f[R].
If the exit status is zero (indicating no undefined or multiply-defined
symbols), you can open your kext\[cq]s \f[I]Info.plist\f[R] file in a
text editor and paste the library declarations over the
\f[I]OSBundleLibraries\f[R] property.

You can specify other collections with the \f[B]libraries\f[R]
subcommand to look for dependencies in other collections as well.

\f[B]--all-symbols\f[R]
List all symbols; found, not found, or found more than once.

\f[B]--onedef-symbols\f[R]
List all symbols found, with the library kext they were found in.

\f[B]--multdef-symbols\f[R]
List all symbols found more than once, with their library kexts.

\f[B]--undef-symbols\f[R]
List all symbols not found in any library.

\f[B]--unsupported\f[R]
Look in unsupported kexts for symbols.

\f[B]-c,\f[R] \f[B]--compatible-versions\f[R]
Use library kext compatible versions rather than current versions.

\f[B]--xml\f[R]
Print XML fragment suitable for pasting.



- \[bu]
\f[B]showloaded\f[R]: Display the status/information of loaded kernel
extensions on the system, according to the options provided.
By default, the following is shown for each kext:

> 
\f[B]Index\f[R]
The load index of the kext (used to track linkage references).
Gaps in the list indicate kexts that have been unloaded.

\f[B]Refs\f[R]
The number of references to this kext by others.
If nonzero, the kext cannot be unloaded.

\f[B]Address\f[R]
The address in kernel space where the kext has been loaded.

\f[B]Size\f[R]
The number of bytes of kernel memory that the kext occupies.
If this is zero, the kext is a built-in part of the kernel that has an
entry as a kext for resolving dependencies among kexts.

\f[B]Wired\f[R]
The number of wired bytes of kernel memory that the kext occupies.

\f[B]Architecture\f[R]
The architecture of the kext, displayed only if using the
\f[B]--arch-info\f[R] option.

\f[B]Name\f[R]
The \f[B]CFBundleIdentifier\f[R] of the kext.

\f[B]Version\f[R]
The \f[B]CFBundleVersion\f[R] of the kext.

\f[B]<Linked Against>\f[R]
The index numbers of all other kexts that this kext has a reference to.

The following options are available for the \f[B]showloaded\f[R]
command:

\f[B]--show-mach-headers\f[R]
Show the mach headers of the loaded extensions and/or kernel, if
\f[B]--show-kernel\f[R] is specified.

\f[B]--show <loaded|unloaded|all>\f[R]
Restrict output to a specific load state.

\f[B]--collection <boot|sys|aux|codeless>\f[R]
Restrict the load information to a particular kind.
Defaults to all non-codeless kexts if unspecified.
To display information about codeless kexts and dexts that the kernel
knows about, use \f[B]--collection codeless\f[R] \f[B]--show all\f[R].

\f[B]--sort\f[R]
Sort the output by load address of each extension, instead of by index.

\f[B]--list-only\f[R]
Print the list of extensions only, omitting the header on the first
line.

\f[B]--arch-info\f[R]
Include the architecture info in output.

\f[B]--no-kernel-components\f[R]
Do not show kernel components in output.

\f[B]--show-kernel\f[R]
Show load information about the kernel in the output.
Use with \f[B]--show-mach-headers\f[R] to view the kernel mach header.



- \[bu]
\f[B]dumpstate\f[R]: Display diagnostic information about the state of
\f[B]kernelmanagerd(8)\f[R].

- \[bu]
\f[B]find\f[R]: Locate and print paths of kexts (or kexts in
collections) matching the filter criteria.
For more information on filtering, see \f[B]FILTERING OPTIONS\f[R].
Searches are performed using the same kext management logic used
elsewhere in \f[B]kmutil\f[R], by which only kexts specified with the
repository or bundle options are eligible; this is specifically not an
exhaustive, recursive filesystem search.

- \[bu]
\f[B]check\f[R]: Check that load information and/or kext collections on
the system are consistent.

> 
\f[B]--collection-linkage\f[R]
Check to see that the collections on the system are properly linked
together by inspecting the UUID metadata in the prelink info section of
each collection on the system.

\f[B]--load-info\f[R]
Check to see that the load information in the kernel properly mirrors
the collections on disk.
This is the default action if no other options are specified.

\f[B]--kernel-only\f[R]
If checking load info, just check that the kernel matches, and no other
kexts.

\f[B]--collection <boot|sys|aux>\f[R]:
Restrict consistency check to one (or more) of the specified collection
types.
If unspecified, check all by default.



- \[bu]
\f[B]log\f[R]: Display logging information about the kext management
subsystem.
This is a wrapper around the system \f[B]log(1)\f[R] command with a
pre-defined predicate to show only logs from kernelmanagerd and kmutil.

- \[bu]
\f[B]print-diagnostics\f[R]: Perform all possible tests on one or more
kexts, and indicate whether or not the kext can be successfully built
into a collection.
If there are issues found with the kext, diagnostic information is
reported which can help to isolate and resolve the problem.
Note that some tests require root.
Note that custom collections, variants, and architectures can be
specified with the \f[I]GENERIC\f[R] and \f[I]COLLECTION\f[R] kmutil
options.

> 
\f[B]-p,\f[R] \f[B]--bundle-path\f[R]
Print diagnostics for the bundle specified at this path (can be
specified more than once).

\f[B]-Z\f[R] \f[B]--no-resolve-dependencies\f[R]
Don\[cq]t resolve kext dependencies

\f[B]-D\f[R] \f[B]--diagnose-dependencies\f[R]
Recursively diagnose all kext dependencies of each kext specified with
\f[B]-p\f[R].
Ignored when \f[B]-Z\f[R] is present.

\f[B]--plugins\f[R]
Diagnose each kext found in the PlugIns directory of kexts specified
with \f[B]-p\f[R].

\f[B]--do-staging\f[R]
Perform kext staging to the SIP protected location.
This test requires root privileges.



- \[bu]
\f[B]clear-staging\f[R]: Clear the staging directory managed by
\f[B]kernelmanagerd(8)\f[R] and \f[B]kmutil(8)\f[R].

- \[bu]
\f[B]migrate\f[R]: System subcommand used during a software update.

- \[bu]
\f[B]install\f[R]: System subcommand used to update the Boot and System
kext collections.

- \[bu]
\f[B]rebuild\f[R]: System subcommand used to attempt an Auxiliary kext
collection rebuild.
This command evaluates the current Auxiliary kext collection for
changes, which may add newly approved third-party kexts and remove kexts
that were previously installed and have since been deleted or moved from
their installed location.

> 
To uninstall a kext from the Auxiliary kext collection:

- 1.
Delete or move the kext bundle(s) to be uninstalled from their installed
location.

- 2.
Run \[lq]\f[B]kmutil rebuild\f[R]\[rq] from Terminal and confirm the
Auxiliary kext collection changes.

- 3.
Authorize the Auxiliary kext collection rebuild.

- 4.
Reboot the system for the changes to take effect.



## RECOVERY COMMANDS


The following commands can only be run in Recovery Mode.

- \[bu]
\f[B]trigger-panic-medic\f[R]: Remove the \f[B]auxiliary kext
collection\f[R] and remove all kext approvals on the next boot.
This subcommand can only be used in Recovery Mode.
This command can be used to recover the system from a kext that causes a
kernel panic.
After calling \f[B]trigger-panic-medic\f[R], all previously installed
kexts will prompt the user to re-approve them when they are loaded or
installed.

- \[bu]
\f[B]configure-boot\f[R]: Configure a custom boot object policy.
This command can be used to install a custom mach-o file from which the
system will boot.
In order to install custom boot objects, you must first enter
\f[I]Medium Security\f[R] by using the Startup Disk utility in Recovery
Mode.
Setting a custom boot object will further lower the system security to
\f[I]Permissive Security\f[R], and you will be prompted to confirm this
action.

> 
\f[B]-c,\f[R] \f[B]--custom-boot-object\f[R]
The Mach-O that the booter will load and start.
The file can be optionally compressed and wrapped in an img4.

\f[B]-C,\f[R] \f[B]--compress\f[R]
Compress the custom boot object

\f[B]-v,\f[R] \f[B]--volume\f[R]
Install the custom boot object for the specified volume

\f[B]--raw\f[R]
Treat custom boot object as a raw file to be installed.
The object will be installed with custom Mach-O boot properties derived
from \f[B]\[en]lowest-virtual-address\f[R] and
\f[B]\[en]entry-point\f[R]

\f[B]--lowest-virtual-address\f[R]
Lowest virtual memory address of the raw boot object.
(iBoot will map the raw boot object at this virtual address)

\f[B]--entry-point\f[R]
Virtual memory address of entry point into the raw boot object



## OPTIONS


### GLOBAL OPTIONS


The following options are global to most kmutil subcommands.

\f[B]-a,\f[R] \f[B]--arch\f[R]
Specify the architecture to use for the extensions or collections
specified.
Defaults to the current running architecture.

\f[B]-V,\f[R] \f[B]--variant-suffix\f[R]
Specify a variant, i.e., \f[B]development\f[R], \f[B]debug\f[R], or
\f[B]kasan\f[R], of extensions or collections to prefer instead of the
release defaults.

\f[B]-z,\f[R] \f[B]--no-authentication\f[R]
Disable staging and validation of extensions when performing an action.

\f[B]-v,\f[R] \f[B]--verbose\f[R]
Enable verbose output.

\f[B]-r,\f[R] \f[B]--repository\f[R]
Paths to directories containing extensions.
If \f[B]-R\f[R] is specified, the volume root will be automatically
prepended.

\f[B]-R,\f[R] \f[B]--volume-root\f[R]
Specify the target volume to operate on.
Defaults to /.

### FILTERING OPTIONS


The following options can be used in certain kmutil commands for
filtering its input or output.

\f[B]-p,\f[R] \f[B]--bundle-path\f[R]
Include the bundle specified at this path in the results.
Return an error if not found.

\f[B]-b,\f[R] \f[B]--bundle-identifier\f[R]
Search for, and/or include this identifier in the results.
Return an error if not found.

\f[B]--optional-identifier\f[R]
Search for, and/or include this identifier in the results, if possible.

\f[B]--elide-identifier\f[R]
Do not include this identifier in the results.

\f[B]-f,\f[R] \f[B]--filter\f[R]
Specify a filter, in predicate syntax, which must match against
properties of an extension to be included in the input or output.
This argument can be overridden by other arguments for specifying and
including extensions.

\f[B]-F,\f[R] \f[B]--filter-all\f[R]
Specify a filter, in predicate syntax, which must match against
properties of an extension to be included in the input or output.
This argument can \f[I]not\f[R] be overridden by other arguments for
specifying and including extensions.

\f[B]\[en]kdk\f[R]
The KDK path to use for discovering kexts when creating a new
\f[B]boot\f[R] or \f[B]sys\f[R] kext collection.

\f[B]\[en]build\f[R]
\f[B]Use with caution.\f[R] This specifies the build version number to
use when discovering kexts and building kext collections.
If no build version is specified, the current system build version
number is used.

For more information on predicate filter syntax, see the predicate
programming guide available in the Apple developer documentation.

### COLLECTION OPTIONS


The following options can be used to specify paths and options for
handling kext collections.
If left unspecified, collection paths will default to the default paths
for the system kext collections.

\f[B]-B,\f[R] \f[B]--boot-path\f[R]
The path to the boot kext collection.

\f[B]-S,\f[R] \f[B]--system-path\f[R]
The path to the system kext collection.

\f[B]-A,\f[R] \f[B]--aux-path\f[R]
The path to the auxiliary kext collection.

\f[B]-M,\f[R] \f[B]--allow-missing-collections\f[R]
Recover gracefully, where applicable, if a collection is missing.

## EXAMPLES


Inspect the contents of system kext collections:
.IP

```
\f[C]
$ kmutil inspect -v --show-mach-header -B /System/Library/KernelCollections/BootKernelExtensions.kc
$ kmutil inspect --show-fileset-entries --bundle-identifier com.apple.kernel
\f[R]
```


Load and unload kexts:
.IP

```
\f[C]
$ kmutil load -b com.apple.filesystems.apfs
$ kmutil load -p /Library/Extensions/foo.kext
$ kmutil unload -p /System/Library/Extensions/apfs.kext
\f[R]
```


Show load information about kexts:
.IP

```
\f[C]
$ kmutil showloaded --show-mach-headers --bundle-identifier com.example.foo
$ kmutil showloaded --show-kernel --collection boot
$ kmutil showloaded --show unloaded --filter \[dq]\[aq]CFBundleVersion\[aq] == \[aq]15.2.13\[aq]\[dq]
\f[R]
```


Find dependencies of kexts:
.IP

```
\f[C]
$ kmutil libraries -p /Library/Extensions/foo.kext --xml | pbcopy
\f[R]
```


Create custom kext collections:
.IP

```
\f[C]
$ kmutil -n boot -B myboot.kc -k mykernel --elide-identifier com.apple.filesystems.apfs

$ kmutil -n boot sys -B myboot.kc -S mysys.kc -V debug

$ kmutil -n boot -B myboot.kc -k mykernel
$ kmutil -n sys -B myboot.kc -S mysys.kc -F \[dq]\[aq]OSBundleRequired\[aq] == \[aq]Safe Boot\[aq]\[dq] -s stripkexts

$ kmutil -n aux -r /Library/Extensions -L
\f[R]
```


## DIAGNOSTICS


\f[B]kmutil\f[R] exits with a zero status on success.
On error, \f[B]kmutil\f[R] prints an error message and then exits with a
non-zero status.

Well known exit codes:

- \[bu]
3 : kmutil failed because the kext is missing when trying to unload it.

- \[bu]
27 : kmutil failed because user approval is required.

- \[bu]
28 : kmutil failed because a reboot is required.

## COMPLETIONS


For frequent users, \f[B]kmutil\f[R] can generate a shell completion
script by invoking:
.IP

```
\f[C]
$ kmutil --generate-completion-script <shell>
\f[R]
```


This option supports \f[B]zsh(1)\f[R], \f[B]bash(1)\f[R], and fish.
If no shell is specified, then a completion script will be generated for
the current running shell.

### SEE ALSO


\f[B]kernelmanagerd(8)\f[R]
