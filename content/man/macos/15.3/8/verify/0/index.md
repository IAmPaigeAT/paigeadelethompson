+++
title = "verify(8)"
date = "Sun Feb 16 04:48:17 2025"
operating_system_version = "15.3"
description = "The verify(8) address verification server maintains a record of what recipient addresses are known to be deliverable or undeliverable."
manpage_name = "verify"
manpage_format = "troff"
detected_package_version = "0"
operating_system = "macos"
author = "None Specified"
manpage_section = "8"
keywords = ["na", "smtpd", "postfix", "smtp", "server", "cleanup", "enqueue", "message", "postconf", "configuration", "parameters", "syslogd", "system", "logging", "readme", "files", "ad", "use", "fbpostconf", "readme_directory", "or", "html_directory", "to", "locate", "this", "information", "address_verification_readme", "address", "verification", "howto", "license", "the", "secure", "mailer", "must", "be", "distributed", "with", "software", "history", "service", "was", "introduced", "version", "2", "1", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
+++

VERIFY 8


## NAME

verify
-
Postfix address verification server

## SYNOPSIS

.na

```
verify [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The **verify**(8) address verification server maintains a record
of what recipient addresses are known to be deliverable or
undeliverable.

Addresses are verified by injecting probe messages into the
Postfix queue. Probe messages are run through all the routing
and rewriting machinery except for final delivery, and are
discarded rather than being deferred or bounced.

Address verification relies on the answer from the nearest
MTA for the specified address, and will therefore not detect
all undeliverable addresses.

The **verify**(8) server is designed to run under control
by the Postfix
master server. It maintains an optional persistent database.
To avoid being interrupted by "postfix stop" in the middle
of a database update, the process runs in a separate process
group.

The **verify**(8) server implements the following requests:

- \fBupdate\fI address status
Update the status and text of the specified address.

- \fBquery\fI
Look up the *status* and *text* for the specified
*address*.
If the status is unknown, a probe is sent and an "in progress"
status is returned.

## SECURITY

.na

```
.ad
```

The address verification server is not security-sensitive. It does
not talk to the network, and it does not talk to local users.
The verify server can run chrooted at fixed low privilege.

The address verification server can be coerced to store
unlimited amounts of garbage. Limiting the cache expiry
time
trades one problem (disk space exhaustion) for another
one (poor response time to client requests).

With Postfix version 2.5 and later, the **verify**(8)
server no longer uses root privileges when opening the
**address_verify_map** cache file. The file should now
be stored under the Postfix-owned **data_directory**.  As
a migration aid, an attempt to open a cache file under a
non-Postfix directory is redirected to the Postfix-owned
**data_directory**, and a warning is logged.

## DIAGNOSTICS


Problems and transactions are logged to **syslogd**(8).

## BUGS


Address verification probe messages add additional traffic
to the mail queue.
Recipient verification may cause an increased load on
down-stream servers in the case of a dictionary attack or
a flood of backscatter bounces.
Sender address verification may cause your site to be
blacklisted by some providers.

If the persistent database ever gets corrupted then the world
comes to an end and human intervention is needed. This violates
a basic Postfix principle.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are not picked up automatically,
as **verify**(8)
processes are long-lived. Use the command "**postfix reload**" after
a configuration change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

## PROBE MESSAGE CONTROLS

.na

```
.ad
```


- \fBaddress_verify_sender
The sender address to use in address verification probes; prior
to Postfix 2.5 the default was "postmaster".

Available with Postfix 2.9 and later:

- \fBaddress_verify_sender_ttl
The time between changes in the time-dependent portion of address
verification probe sender addresses.

## CACHE CONTROLS

.na

```
.ad
```


- \fBaddress_verify_map (see 'postconf -d'
Lookup table for persistent address verification status
storage.

- \fBaddress_verify_positive_expire_time
The time after which a successful probe expires from the address
verification cache.

- \fBaddress_verify_positive_refresh_time
The time after which a successful address verification probe needs
to be refreshed.

- \fBaddress_verify_negative_cache
Enable caching of failed address verification probe results.

- \fBaddress_verify_negative_expire_time
The time after which a failed probe expires from the address
verification cache.

- \fBaddress_verify_negative_refresh_time
The time after which a failed address verification probe needs to
be refreshed.

Available with Postfix 2.7 and later:

- \fBaddress_verify_cache_cleanup_interval
The amount of time between **verify**(8) address verification
database cleanup runs.

## PROBE MESSAGE ROUTING CONTROLS

.na

```
.ad
```

By default, probe messages are delivered via the same route
as regular messages.  The following parameters can be used to
override specific message routing mechanisms.

- \fBaddress_verify_relayhost
Overrides the relayhost parameter setting for address verification
probes.

- \fBaddress_verify_transport_maps
Overrides the transport_maps parameter setting for address verification
probes.

- \fBaddress_verify_local_transport
Overrides the local_transport parameter setting for address
verification probes.

- \fBaddress_verify_virtual_transport
Overrides the virtual_transport parameter setting for address
verification probes.

- \fBaddress_verify_relay_transport
Overrides the relay_transport parameter setting for address
verification probes.

- \fBaddress_verify_default_transport
Overrides the default_transport parameter setting for address
verification probes.

Available in Postfix 2.3 and later:

- \fBaddress_verify_sender_dependent_relayhost_maps
Overrides the sender_dependent_relayhost_maps parameter setting for address
verification probes.

Available in Postfix 2.7 and later:

- \fBaddress_verify_sender_dependent_default_transport_maps
Overrides the sender_dependent_default_transport_maps parameter
setting for address verification probes.

## SMTPUTF8 CONTROLS

.na

```
.ad
```

Preliminary SMTPUTF8 support is introduced with Postfix 3.0.

- \fBsmtputf8_autodetect_classes (sendmail,
Detect that a message requires SMTPUTF8 support for the specified
mail origin classes.

Available in Postfix version 3.2 and later:

- \fBenable_idna2003_compatibility
Enable 'transitional' compatibility between IDNA2003 and IDNA2008,
when converting UTF-8 domain names to/from the ASCII form that is
used for DNS lookups.

## MISCELLANEOUS CONTROLS

.na

```
.ad
```


- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## SEE ALSO

.na

```
smtpd(8), Postfix SMTP server
cleanup(8), enqueue Postfix message
postconf(5), configuration parameters
syslogd(5), system logging
.SH "README FILES"
.na

```
.ad
```

Use "**postconf readme_directory**" or
"**postconf html_directory**" to locate this information.
.na

```
ADDRESS_VERIFICATION_README, address verification howto
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## HISTORY



This service was introduced with Postfix version 2.1.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
