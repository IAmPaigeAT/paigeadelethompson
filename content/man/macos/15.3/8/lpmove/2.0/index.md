+++
date = "Sun Feb 16 04:48:16 2025"
manpage_format = "troff"
keywords = ["cancel", "1", "lp", "lpr", "lprm", "br", "cups", "online", "help", "http", "localhost", "631", "copyright", "co", "2007-2019", "by", "apple", "inc"]
operating_system = "macos"
description = "lpmove moves the specified job or all jobs from source to destination. job can be the job ID number or the old destination and job ID. The lpmove command supports the following options: Forces encryption when connecting to the server. -U username ..."
operating_system_version = "15.3"
manpage_section = "8"
manpage_name = "lpmove"
detected_package_version = "2.0"
author = "None Specified"
title = "lpmove(8)"
+++

lpmove 8 "CUPS" "26 April 2019" "Apple Inc."

## NAME

lpmove - move a job or all jobs to a new destination

## SYNOPSIS

lpmove
[
-E
] [
**-h **server[**:**port]
] [
-U
username
]
job
destination
.br
lpmove
[
-E
] [
**-h **server[**:**port]
] [
-U
username
]
source
destination

## DESCRIPTION

**lpmove** moves the specified *job* or all jobs from *source* to *destination*. *job* can be the job ID number or the old destination and job ID.

## OPTIONS

The **lpmove** command supports the following options:

-E
Forces encryption when connecting to the server.

**-U **username
Specifies an alternate username.

**-h **server[**:**port]
Specifies an alternate server.

## EXAMPLES

Move job 123 from "oldprinter" to "newprinter":

```

    lpmove 123 newprinter

            or

    lpmove oldprinter-123 newprinter

```

Move all jobs from "oldprinter" to "newprinter":

```

    lpmove oldprinter newprinter
```


## SEE ALSO

cancel (1),
lp (1),
lpr (1),
lprm (1),
.br
CUPS Online Help (http://localhost:631/help)

## COPYRIGHT

Copyright \[co] 2007-2019 by Apple Inc.
