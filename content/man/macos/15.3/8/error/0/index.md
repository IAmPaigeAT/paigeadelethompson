+++
title = "error(8)"
author = "None Specified"
manpage_section = "8"
date = "Sun Feb 16 04:48:12 2025"
keywords = ["na", "qmgr", "queue", "manager", "bounce", "delivery", "status", "reports", "discard", "postfix", "agent", "postconf", "configuration", "parameters", "master", "generic", "daemon", "options", "process", "syslogd", "system", "logging", "license", "ad", "the", "secure", "mailer", "must", "be", "distributed", "with", "this", "software", "author", "wietse", "venema", "ibm", "t", "j", "watson", "research", "p", "o", "box", "704", "yorktown", "heights", "ny", "10598", "usa", "google", "inc", "111", "8th", "avenue", "new", "york", "10011"]
detected_package_version = "0"
operating_system = "macos"
manpage_name = "error"
description = "The Postfix error(8) delivery agent processes delivery requests from the queue manager. Each request specifies a queue file, a sender address, the reason for non-delivery (specified as the next-hop destination), and recipient information. The rea..."
operating_system_version = "15.3"
manpage_format = "troff"
+++

ERROR 8


## NAME

error
-
Postfix error/retry mail delivery agent

## SYNOPSIS

.na

```
error [generic Postfix daemon options]
.SH DESCRIPTION
.ad
```

The Postfix **error**(8) delivery agent processes delivery
requests from
the queue manager. Each request specifies a queue file, a sender
address, the reason for non-delivery (specified as the
next-hop destination), and recipient information.
The reason may be prefixed with an RFC 3463-compatible detail code;
if none is specified a default 4.0.0 or 5.0.0 code is used instead.
This program expects to be run from the **master**(8) process
manager.

Depending on the service name in master.cf, **error**
or **retry**, the server bounces or defers all recipients
in the delivery request using the "next-hop" information
as the reason for non-delivery. The **retry** service name is
supported as of Postfix 2.4.

Delivery status reports are sent to the **bounce**(8),
**defer**(8) or **trace**(8) daemon as appropriate.

## SECURITY

.na

```
.ad
```

The **error**(8) mailer is not security-sensitive. It does not talk
to the network, and can be run chrooted at fixed low privilege.

## STANDARDS

.na

```
RFC 3463 (Enhanced Status Codes)
.SH DIAGNOSTICS
.ad
```

Problems and transactions are logged to **syslogd**(8).

Depending on the setting of the **notify_classes** parameter,
the postmaster is notified of bounces and of other trouble.

## CONFIGURATION PARAMETERS

.na

```
.ad
```

Changes to **main.cf** are picked up automatically as **error**(8)
processes run for only a limited amount of time. Use the command
"**postfix reload**" to speed up a change.

The text below provides only a parameter summary. See
**postconf**(5) for more details including examples.

- \fB2bounce_notice_recipient
The recipient of undeliverable mail that cannot be returned to
the sender.

- \fBbounce_notice_recipient
The recipient of postmaster notifications with the message headers
of mail that Postfix did not deliver and of SMTP conversation
transcripts of mail that Postfix did not receive.

- \fBconfig_directory (see 'postconf -d'
The default location of the Postfix main.cf and master.cf
configuration files.

- \fBdaemon_timeout
How much time a Postfix daemon process may take to handle a
request before it is terminated by a built-in watchdog timer.

- \fBdelay_logging_resolution_limit
The maximal number of digits after the decimal point when logging
sub-second delay values.

- \fBdouble_bounce_sender
The sender address of postmaster notifications that are generated
by the mail system.

- \fBipc_timeout
The time limit for sending or receiving information over an internal
communication channel.

- \fBmax_idle
The maximum amount of time that an idle Postfix daemon process waits
for an incoming connection before terminating voluntarily.

- \fBmax_use
The maximal number of incoming connections that a Postfix daemon
process will service before terminating voluntarily.

- \fBnotify_classes (resource,
The list of error classes that are reported to the postmaster.

- \fBprocess_id
The process ID of a Postfix command or daemon process.

- \fBprocess_name
The process name of a Postfix command or daemon process.

- \fBqueue_directory (see 'postconf -d'
The location of the Postfix top-level queue directory.

- \fBsyslog_facility
The syslog facility of Postfix logging.

- \fBsyslog_name (see 'postconf -d'
A prefix that is prepended to the process name in syslog
records, so that, for example, "smtpd" becomes "prefix/smtpd".

## SEE ALSO

.na

```
qmgr(8), queue manager
bounce(8), delivery status reports
discard(8), Postfix discard delivery agent
postconf(5), configuration parameters
master(5), generic daemon options
master(8), process manager
syslogd(8), system logging
.SH "LICENSE"
.na

```
.ad
```

The Secure Mailer license must be distributed with this software.

## AUTHOR(S)

.na

```
Wietse Venema
IBM T.J. Watson Research
P.O. Box 704
Yorktown Heights, NY 10598, USA

Wietse Venema
Google, Inc.
111 8th Avenue
New York, NY 10011, USA
