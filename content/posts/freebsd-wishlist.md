+++
Title = "Looking back in 2024"
Date = "2025-03-06T20:00:20-0800"
Author = "Paige"
Description = "FreeBSD feature wishlist"
cover = "img/og.png"
tags = ["Development", "FreeBSD", "GSoC ideas", "OS"]
+++

There's been a few hitches in making the permanent jump from linux to FreeBSD
but for the most part it has been a pretty happy experience. Over the past year
I started gathering some details on things that are left to be desired or that
could stand to be improved. 

The first thing and probably the most important for me is finding a way to get
containers working using jails. On FreeBSD we currently have Podman and 
[ocijail](https://github.com/dfr/ocijail). I have some context with the 
maintainer of ocijail. 

# ocijail problems with net.fibs > 0 

There's some issues that are not exactly the easiest thing to isolate even
with bootverbose, but I was able to and I'll get to that in a minute:

```
❯ sudo podman run --rm --os=linux alpine cat /etc/os-release | head -1
Resolving "alpine" using unqualified-search registries (/usr/local/etc/containers/registries.conf)
Trying to pull docker.io/library/alpine:latest...
Getting image source signatures
Copying blob 1f3e46996e29 done   |
Copying config b0c9d60fc5 done   |
Writing manifest to image destination
Error: OCI runtime error: ocijail: error calling jail_attach: Invalid argument

❯ podman --version
podman version 5.2.5
```

> I used Cursor to add some bootverbose logging to kern_jail.c and 
ultimately had to add some to kern_osd.c as well, though even that was 
not really enough to help me find this problem. 

> The logging I added gave me something that looks kinda like this:

```
osd: method 4 failed for slot 6 type 1 (error=22)
jail: module attachment preparation failed (error=22)
osd: calling method 5 for type 1
```

> After looking back at the code in kern.jail.c, in do_jail_attach:

```
error = osd_jail_call(pr, PR_METHOD_ATTACH, td);
if (error) {
if (bootverbose)
printf("jail: module attachment preparation failed (error=%d)\n", error);
prison_deref(pr, drflags);
return (error);
}
```

> I noticed that constant PR_METHOD_ATTACH as it relates to the method id, 
so I decided to see if I could narrow down what it could be talking 
about (assuming that PR_METHOD_ATTACH could only be used in so many 
places in the src tree):

```
➜  sys git:(release/14.2.0-p1) ✗ grep -r PR_METHOD_ATTACH
./sys/jail.h:#define	PR_METHOD_ATTACH	4
./kern/kern_jail.c:	error = osd_jail_call(pr, PR_METHOD_ATTACH, td);
./kern/kern_jail.c:	(void)osd_jail_call(td->td_ucred->cr_prison, PR_METHOD_ATTACH, td);
./net/route/route_tables.c:	    [PR_METHOD_ATTACH] =	rtables_check_proc_fib,
```

> And I remembered I had to enable > 0 fibs in order to be able to target 
different routing tables (VRF if you cisco/juni/linux or rdomain if you 
OpenBSD.) Yeah I’m definitely hoping that we can get that to work somehow. 
My biggest gripe about Docker is how shitty their default network drivers 
are, and not really wanting to write my own to make it work with VRF.

> FIBs with jails: https://forums.freebsd.org/threads/fib-setfib-question.44014/

So the TLDR; the `net.fibs=256` option that I added to my `/boot/loader.conf` 
causes some issues for ocijail. Moreover, Things like Docker and Podman don't 
really understand the concept of a VRF well. Docker uses NetNS with a veth 
pair and a bridge which looks kinda like this: 

```
Main NetNS                     Guest NetNS
(bridge, slave veth1) -------- (no bridge, veth0)
```

- On the Main NetNS, veth1 uses the primary routing table, whereas if it used
a VRF, the routing table would be separate. It is possible to facilitate such
a configuration with Docker but it requires a custom networking driver. 

- FreeBSD's equivalent to a VRF would be `fib`:

```
fib fib_number
             Specify interface FIB.  A FIB fib_number is assigned to all
             frames or packets received on that interface.  The FIB is not
             inherited, e.g., vlans or other sub-interfaces will use the
             default FIB (0) irrespective of the parent interface's FIB.  The
             kernel needs to be tuned to support more than the default FIB
             using the ROUTETABLES kernel configuration option, or the
             net.fibs tunable.

tunnelfib fib_number
             Specify tunnel FIB.  A FIB fib_number is assigned to all packets
             encapsulated by tunnel interface, e.g., gif(4), gre(4) and
             vxlan(4).
```

See also `setfib(1)` and `setsockopt(2)`.

Generally, in the context of a custom network driver for Docker, you would
just simply specify a `--driver-opt` to indicate to the driver which FIB
the guest. When you create a container with Podman on FreeBSD: 

```
cni-podman0: flags=1008843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST,LOWER_UP> metric 0 mtu 1500
	options=0
	ether 58:9c:fc:10:9b:00
	inet 10.88.0.1 netmask 0xffff0000 broadcast 10.88.255.255
	id 00:00:00:00:00:00 priority 32768 hellotime 2 fwddelay 15
	maxage 20 holdcnt 6 proto rstp maxaddr 2000 timeout 1200
	root id 00:00:00:00:00:00 priority 32768 ifcost 0 port 0
	member: vnet56589b17 flags=143<LEARNING,DISCOVER,AUTOEDGE,AUTOPTP>
	        ifmaxaddr 0 port 9 priority 128 path cost 2000
	groups: bridge
	nd6 options=9<PERFORMNUD,IFDISABLED>
vnet56589b17: flags=1008943<UP,BROADCAST,RUNNING,PROMISC,SIMPLEX,MULTICAST,LOWER_UP> metric 0 mtu 1500
	description: associated with jail: 21f94b992976896e60fe9f8b2d2e354b80333fd6f775f99b04995dfdad70513d as nic: eth0
	options=8<VLAN_MTU>
	ether 02:50:fd:a7:d4:0a
	groups: epair
	media: Ethernet 10Gbase-T (10Gbase-T <full-duplex>)
	status: active
	nd6 options=29<PERFORMNUD,IFDISABLED,AUTO_LINKLOCAL>
```

In the jail: 

```
sudo podman exec 21f94b992976 ip addr
7: lo: <LOOPBACK,MULTICAST,UP> mtu 16384 qdisc noqueue state UP ip: ioctl 0x8942 failed: Invalid argument

    link/ieee1394
    inet 127.0.0.1/8 scope global dynamic lo0
    inet6 ::1/128 scope host dynamic lo0
       valid_lft forever preferred_lft forever
    inet6 fe80::1/64 scope link dynamic lo0
       valid_lft forever preferred_lft forever
10: eth0: <BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state UP ip: ioctl 0x8942 failed: Invalid argument

    link/ether 02:50:fd:a7:d4:0b brd ff:ff:ff:ff:ff:ff
    inet 10.88.0.3/16 brd 10.88.255.255 scope global dynamic eth0

sudo podman exec 21f94b992976 ip route
0.0.0.0/0 via 10.88.0.1 dev if10
10.88.0.0/16 dev if10
10.88.0.3 dev if7
127.0.0.1 dev if7
```

So they do have something that is kind of like NetNS on Linux here, 
because the interfaces of the host don't appear in the jail. But On the host, 
it would still be nice to assign `cni-podman0` and `vnet56589b17` to a 
separate FIB in order to preserve the integrity of the host's routing table.
In this case, an additional `epair` can be created where one is assigned to
FIB 0 and the other epair peer assigned to the same FIB as `cni-podman0` and 
`vnet56589b17`:

```
FIB 0                              FIB 1
(host, veth1                       (container, veth0
 192.0.0.0/31) -------------------  192.0.0.1/31) 
```

You then provide a default gateway via `192.0.0.0/31`, and on the host provide
NAT from your internet interface to `veth1`. The idea here is that it gives you
a little bit more control over how networks can reach each other. You can
create guest networks which do not have to route through the host's routing
table at all to reach each other as well.

This concept as it is here is known as "VRF-lite" but VRF is part of a much 
bigger picture as it relates to MPLS: 
https://en.wikipedia.org/wiki/Virtual_routing_and_forwarding MPLS doesn't
matter too much here, but once you understand it you'll never look at networks
the same way again. I would consider this imparative to have, for building
secure networks using container environments. To be hoenst the Docker community
still hasn't figured this out: 

- https://forums.docker.com/t/how-to-implement-vrf-virtual-routing-and-forwarding-for-docker-containers/143054
- https://github.com/docker/for-linux/issues/698

The closest thing that I've seen, is Cumulus: 
- https://docs.nvidia.com/networking-ethernet-software/cumulus-linux-41/Network-Solutions/Docker-on-Cumulus-Linux/
- https://netdevconf.org/1.2/slides/oct7/01_ahern_microservice_net_vrf_on_host.pdf

# Secureboot
- https://github.com/paigeadelethompson/freebsd-secureboot

# ExFAT
- https://github.com/paigeadelethompson/exfat
