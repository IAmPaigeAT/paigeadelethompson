+++
Title = "Free CIDR/Subnet Calculator"
Date = "2023-07-08T05:58:20-0800"
Author = "Paige"
Description = "CIDR Network Calculator"
cover = "img/og.png"
tags = ["Tools", "CIDR", "Ipv4", "Ipv6"]
Keywords = ["CIDR", "ipv4", "ipv6", "calculator", "subnet", "ipv4", "ipv6"]
+++

<script src="/ip-cidr/ip-cidr.js"></script>

<div id="formio"></div>
<br />
<br />
<div id="cidr_form"></div>

<script type="application/javascript">

var formObj = undefined;
const regexIP = /\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/;

function toInt(ip) {
    if (!ip) {
        throw new Error('E_UNDEFINED_IP');
    }

    if (!regexIP.test(ip)) {
        throw new Error('E_INVALID_IP');
    }

    return ip.split('.').map((octet, index, array) => {
        return parseInt(octet) * Math.pow(256, (array.length - index - 1));
    }).reduce((prev, curr) => {
        return prev + curr;
    });

}

function toIP(value) {

    if (!value) {
        throw new Error('E_UNDEFINED_INTEGER');
    }

    const result = /\d+/.exec(value);

    if (!result) {
        throw new Error('E_INTEGER_NOT_FOUND');
    }

    value = result[0];

    return [
        (value>>24)&0xff,
        (value>>16)&0xff,
        (value>>8)&0xff,
        value&0xff
    ].join('.');
}

function validCidr(input) {
    if(IPCIDR.isValidCIDR(input.data.cidr)) {
        return true;
    }

    FormioUtils.getComponent(formObj.components, 'start_ip').setValue("");
    FormioUtils.getComponent(formObj.components, 'num_hosts').setValue("");
    FormioUtils.getComponent(formObj.components, 'end_ip').setValue("");

  	return false;
}

function toInt6(ipv6) {
    return ipv6.split(':').map(str => Number('0x'+str)).reduce(function(int, value) { return BigInt(int) * BigInt(65536) + BigInt(+value) })
}

function calculate(input) {
    if(validCidr(input)) {
        cidr = new IPCIDR(input.data.cidr);
        FormioUtils.getComponent(formObj.components, 'start_ip').setValue(cidr.start());
        
        FormioUtils.getComponent(formObj.components, 'num_hosts').setValue(cidr.address.v4 ?
                                                                           toInt(cidr.end()) - toInt(cidr.start()) + 1
                                                                           : toInt6(cidr.end()) - toInt6(cidr.start()) + BigInt(1));

        FormioUtils.getComponent(formObj.components, 'end_ip').setValue(cidr.end());
        return "";
    }
    return "Invalid CIDR";
}

document.addEventListener("DOMContentLoaded", function(event) {
        Formio.createForm(document.getElementById('cidr_form'), {
            components: [
                {
                    type: 'textfield',
                    label: 'CIDR',
                    placeholder: 'ex: 192.168.0.0/20 or fc00:f:f:f:f:f:f:f/128',
                    validate: {
                        required: true,
                        custom: calculate
                    },
                    key: 'cidr',
                    input: true,
                    inputType: 'text'
                },
                {
                    type: 'textfield',
                    label: 'Number of Hosts',
                    key: 'num_hosts',
                    input: true,
                    inputType: 'text'
                },
                {
                    type: 'textfield',
                    label: 'Start IP',
                    key: 'start_ip',
                    input: true,
                    inputType: 'text'
                },
                {
                    type: 'textfield',
                    label: 'End IP',
                    key: 'end_ip',
                    input: true,
                    inputType: 'text'
                }
            ]
        }).then(function(form) {
            formObj = form; 
        });
    });
</script>
