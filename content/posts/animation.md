
+++
Title = "Javascript ANSI/VT-like animation"
Date = "2023-02-16T07:27:20-0800"
Author = "Paige"
Description = "Some simple javascript text effect / animation I made as a demo using plain old JQuery (embedded from jsfiddle)"
cover = "img/og.png"
tags = ["Code", "Javascript", "Art"]
+++

<style>
    .post-inner {
        width:100%;
    }
</style>

Ideally I would rewrite this but just a a proof of concept, I was very pleased with the result given how easy it was so I decided to share it incase anybody has any interest in this sort of thing. I think at some point I'll use this as an example to create something
unique for my header / banner.

<iframe width="100%" height="300" src="//jsfiddle.net/erratic/v6wt51gj/9/embedded/result/dark/?fontColor=00ff00&bodyColor=343A40&accentColor=212529&menuColor=000" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>