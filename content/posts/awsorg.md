+++
Title = "Securing AWS with AWS Organizations"
Date = "2023-08-21T00:00:00-0800"
Author = "Paige"
Description = "This article describes how to setup AWS Organizations and setup a service control policy"
cover = "img/og.png"
keywords = ["OU", "SCP"]
tags = ["AWS", "Organizations", "Service Control Policy"]
+++

# Intro
I don't intend for this to be the best strategy for everybody, I reccomend reading through it first and making sure
it will work for you. 

# Setting up Organizations
If you already have an AWS account, create another AWS account to use as your management and billing account (effectively it will be the root account.) You need to do a couple of things for your current account (where your services
are located):

- Remove billing method
- Disable any billing services / insights, cost and usage reports, billing conductor
- Enable MFA if you haven't already

Configure your billing info for your new management account, and enable MFA. Now to setup Organizations:
- Navigate to the [Policies](https://us-east-1.console.aws.amazon.com/organizations/v2/home/policies) page, click
the [Service Control Policies](https://us-east-1.console.aws.amazon.com/organizations/v2/home/policies/service-control-policy) link, and enable service control policies.

- Go to the [Invitations](https://us-east-1.console.aws.amazon.com/organizations/v2/home/accounts/invitations) page under Account > Invitations and click the Add AWS Account button, invite your old AWS account (the one where all of your services are located.) Follow the rest of the instructions and once the organization is successfully joined, 
proceed to the next step. 

- Back on the [Organizations](https://us-east-1.console.aws.amazon.com/organizations/v2/home/accounts) main page, click the check box next to the Root OU, click the Actions menu, and under Organizational Unit click Create New. You can call it whatever you like. 

- Go to the [Service Control Policies](https://us-east-1.console.aws.amazon.com/organizations/v2/home/policies/service-control-policy) page, and create a policy with the following content: 

{{< highlight json >}}
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Statement6",
      "Effect": "Deny",
      "Action": [
        "ec2:RunInstances"
      ],
      "Resource": [
        "*"
      ],
      "Condition": {
        "StringNotEquals": {
          "ec2:InstanceType": [
            "t2.micro",
            "t3.micro"
          ]
        }
      }
    },
    {
      "Sid": "Statement8",
      "Effect": "Deny",
      "Action": [
        "ec2:AcceptTransitGatewayMulticastDomainAssociations",
        "ec2:AcceptTransitGatewayPeeringAttachment",
        "ec2:AcceptTransitGatewayVpcAttachment",
        "ec2:AssociateTransitGatewayMulticastDomain",
        "ec2:AssociateTransitGatewayPolicyTable",
        "ec2:AssociateTransitGatewayRouteTable",
        "ec2:CreateTransitGateway",
        "ec2:CreateTransitGatewayConnect",
        "ec2:CreateTransitGatewayConnectPeer",
        "ec2:CreateTransitGatewayPeeringAttachment",
        "ec2:CreateTransitGatewayPolicyTable",
        "ec2:CreateTransitGatewayPrefixListReference",
        "ec2:CreateTransitGatewayRoute",
        "ec2:CreateTransitGatewayRouteTable",
        "ec2:CreateTransitGatewayRouteTableAnnouncement",
        "ec2:CreateTransitGatewayVpcAttachment",
        "ec2:ApplySecurityGroupsToClientVpnTargetNetwork",
        "ec2:AssociateClientVpnTargetNetwork",
        "ec2:AttachVpnGateway",
        "ec2:AuthorizeClientVpnIngress",
        "ec2:CreateClientVpnEndpoint",
        "ec2:CreateClientVpnRoute",
        "ec2:CreateVpnConnection",
        "ec2:CreateVpnConnectionRoute",
        "ec2:CreateVpnGateway",
        "ec2:CreateCustomerGateway",
        "ec2:CreateNatGateway",
        "route53:CreateQueryLoggingConfig",
        "route53:CreateHealthCheck",
        "route53:CreateTrafficPolicy",
        "route53:CreateTrafficPolicyInstance",
        "route53:CreateTrafficPolicyVersion",
        "route53:UpdateTrafficPolicyComment",
        "route53:UpdateTrafficPolicyInstance",
        "ec2:AllocateHosts",
        "ec2:ModifyHosts",
        "ec2:PurchaseHostReservation",
        "ec2:PurchaseScheduledInstances",
        "ec2:RunScheduledInstances",
        "ec2:GetReservedInstancesExchangeQuote",
        "ec2:AcceptReservedInstancesExchangeQuote",
        "ec2:CancelReservedInstancesListing",
        "ec2:CreateReservedInstancesListing",
        "ec2:DeleteQueuedReservedInstances",
        "ec2:ModifyReservedInstances",
        "ec2:PurchaseReservedInstancesOffering",
        "ec2:CreateImage",
        "ec2:ExportImage",
        "ec2:RegisterImage",
        "ec2:CreateFpgaImage",
        "ec2:CopyFpgaImage",
        "ec2:CopyImage",
        "ec2:CreateRestoreImageTask",
        "ec2:CreateStoreImageTask"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Sid": "Statement7",
      "Effect": "Deny",
      "Action": [
        "s3:PutAccountPublicAccessBlock",
        "s3:ListBucket"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Sid": "Statement10",
      "Effect": "Deny",
      "Action": [
        "*"
      ],
      "Resource": [
        "*"
      ],
      "Condition": {
        "StringNotEquals": {
          "aws:RequestedRegion": [
            "us-east-1"
          ]
        }
      }
    },
    {
      "Sid": "Statement9",
      "Effect": "Allow",
      "Action": [
        "ec2:*",
        "lambda:*",
        "sns:*",
        "cloudfront:*",
        "cognito-identity:*",
        "cognito-sync:*",
        "cognito-idp:*",
        "apigateway:*",
        "s3:*",
        "elasticloadbalancing:*",
        "acm:*",
        "ses:*",
        "route53:*",
        "dynamodb:*",
        "storagegateway:*",
        "iam:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
{{< / highlight >}}

- Navigate back to the [Organizations](https://us-east-1.console.aws.amazon.com/organizations/v2/home/accounts) page, and click the new OU you just created. Click the policies tab, and attach the new policy you just created. 
- In the Applied policies list, click the radio button for the FullAWSAccess (not the inherited one, but the one that is attached directly) then click Detach. This didn't make sense to me at first, but inherited just means that if you attach it directly, then it will apply. If it's not attached directly, it will not apply.
- Navigate back to the [Organizations](https://us-east-1.console.aws.amazon.com/organizations/v2/home/accounts) page, click the check box next to the account you invited to your organization, click the Actions menu, and under AWS Account click Move. Move the account under the new OU you just created and applied a service control policy to. 
- Now go back again to the [Organizations](https://us-east-1.console.aws.amazon.com/organizations/v2/home/accounts) page, collapse your OU, and click the link for the account that you moved underneath it. 
- Click the policies tab, first make sure the policy that you created earlier is attached directly to it, then if FullAWSAccess is attached directly to it, detach it. 

Thats it, organizations are now set up and you're using the same policy that I am using. 

## About the Service Control Policy 

Essentially, no services that I can't imagine myself ever using are enabled. I haven't finished exploring it much
myself, but what's here seems to work fine and I'll probably release an update to this later. 

### Services
The services I've allowed are: 

- EC2 
- Lambda
- SNS
- cloudfront
- API Gateway
- S3
- Elastic Load Balancer
- ACM
- SES
- Route53
- DynamoDB
- Storage Gateway 
- IAM

### For EC2 and Route53
I disabled quite a few methods that I don't intend to use. Stuff like transit gateway and the likes that will only
cost money that I don't want to spend for example. 

- ec2:AcceptTransitGatewayMulticastDomainAssociations
- ec2:AcceptTransitGatewayPeeringAttachment
- ec2:AcceptTransitGatewayVpcAttachment
- ec2:AssociateTransitGatewayMulticastDomain
- ec2:AssociateTransitGatewayPolicyTable
- ec2:AssociateTransitGatewayRouteTable
- ec2:CreateTransitGateway
- ec2:CreateTransitGatewayConnect
- ec2:CreateTransitGatewayConnectPeer
- ec2:CreateTransitGatewayPeeringAttachment
- ec2:CreateTransitGatewayPolicyTable
- ec2:CreateTransitGatewayPrefixListReference
- ec2:CreateTransitGatewayRoute
- ec2:CreateTransitGatewayRouteTable
- ec2:CreateTransitGatewayRouteTableAnnouncement
- ec2:CreateTransitGatewayVpcAttachment
- ec2:ApplySecurityGroupsToClientVpnTargetNetwork
- ec2:AssociateClientVpnTargetNetwork
- ec2:AttachVpnGateway
- ec2:AuthorizeClientVpnIngress
- ec2:CreateClientVpnEndpoint
- ec2:CreateClientVpnRoute
- ec2:CreateVpnConnection
- ec2:CreateVpnConnectionRoute
- ec2:CreateVpnGateway
- ec2:CreateCustomerGateway
- ec2:CreateNatGateway
- route53:CreateQueryLoggingConfig
- route53:CreateHealthCheck
- route53:CreateTrafficPolicy
- route53:CreateTrafficPolicyInstance
- route53:CreateTrafficPolicyVersion
- route53:UpdateTrafficPolicyComment
- route53:UpdateTrafficPolicyInstance
- ec2:AllocateHosts
- ec2:ModifyHosts
- ec2:PurchaseHostReservation
- ec2:PurchaseScheduledInstances
- ec2:RunScheduledInstances
- ec2:GetReservedInstancesExchangeQuote
- ec2:AcceptReservedInstancesExchangeQuote
- ec2:CancelReservedInstancesListing
- ec2:CreateReservedInstancesListing
- ec2:DeleteQueuedReservedInstances
- ec2:ModifyReservedInstances
- ec2:PurchaseReservedInstancesOffering
- ec2:CreateImage
- ec2:ExportImage
- ec2:RegisterImage
- ec2:CreateFpgaImage
- ec2:CopyFpgaImage
- ec2:CopyImage
- ec2:CreateRestoreImageTask
- ec2:CreateStoreImageTask"

For EC2, you can only create instances of the following type: 

- t2.micro 
- t3.micro 

All other are denied. 

### S3 

The following methods are disabled: 

- s3:PutAccountPublicAccessBlock
- s3:ListBucket

One other thing I will probably add to this is something to enforce the use of a S3 VPC endpoint. I don't recall at the moment if the bucket policy can be used to restrict whether or not a bucket can only be accessed via a VPC endpoint but if it is then it is. I also recommend reading [Implementing service control policies and VPC endpoint Policies](https://docs.aws.amazon.com/awsconsolehelpdocs/latest/gsg/implementing-console-private-access-policies.html) 

### Regions
I've restricted access to only `us-east-1` for now. I don't really have a particular reason other than I just don't
use the account for anything and I just wanted to make a demo, but in general having other regions just makes things
more complicated if I don't need them, so I went with this. 

### Caveats 
There's a limit to how big your service control policy can get, and then you have to create another. Kinda sucks, but so far this hasn't proven to be that big of a deal. Other OUs can be created but there are limits to how many. There are also limits to how many policies can be directly attached. 
