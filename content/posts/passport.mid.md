+++
Title = "Embedding MIDI in modern browsers"
Date = "2023-06-05T09:00:00-0800"
Author = "Paige"
Description = "<embed> is gone, but there is a way"
cover = "img/og.png"
tags = ["MIDI", "Browsers", "JS-DOS", "EMScripten"]
+++

## passport.mid
Since `<embed>` isn't available anymore I came up with an alternative; use JS-DOS to run DOSMIDI and Windows 3.11 / Creative MIDI player to play passport.mid. I came up with this idea because I am really fond of the idea of using `asm.js` to [solve real world problems](https://webrtchacks.com/zoom-avoids-using-webrtc/). This works well but Safari doesn't allow sound to play automatically [by default](https://memrise.zendesk.com/hc/en-us/articles/360015889117-How-can-I-allow-audio-to-auto-play-on-Safari-).

*Note:* The work around requires you to click on the JS-DOS container in order to enjoy the MIDI song. If you would like to skip the DOSMIDI version, click to focus on the JS-DOS container then hit escape.
<link rel="stylesheet" href="/emulators-ui/emulators-ui.css">
<div id="dosbox-wrapper" style="width: 96%">
<div id="dosbox"></div>
</div>
<script src="/emulators/emulators.js"></script>
<script src="/emulators-ui/emulators-ui.js"></script>
<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    emulators.pathPrefix = "/emulators/";
    Dos(document.getElementById("dosbox")).run("/jsdos/bundle.zip");
  });
</script> 
