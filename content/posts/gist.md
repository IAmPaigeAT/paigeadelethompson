
+++
Title = "IPynb embed from GH gist test"
Date = "2023-02-16T07:27:20-0800"
Author = "Paige"
Description = "Testing to see what an embedded iPython Notebook looks like"
cover = "img/og.png"
tags = ["HTML/CSS Testing", "IPython", "scapy"]
+++

<style>
    .post-inner {
        width:100%;
    }
</style>    

I just wanted to see how this would look but decided it may as well be a boiler plate for future ipynb tests. Tested up to 
the packet generation, but not sr1.
<script src="https://gist.github.com/paigeadelethompson/338252045c4f060ea6e5b47ef34a8ef5.js"></script>

<style>
    .post-inner {
        width:100%;
    }
    .gist .gist-meta a {
        color: white;
        font-weight: unset;
        
    }
    .gist .gist-meta {
        background-color: #272822;
        font-family: unset;
        font-size: unset;
    }
    .gist {        
        background-color: #272822;
         box-shadow: 4px 4px 0 0 #212529;
    }
    .gist-file { 
        background-color: #272822;
    }
    .gist .gist-file {
        border: unset;
        font-family: unset;
        font-size: unset;
    }
    .gist .gist-data {
        background-color: #272822;
        border: unset;        
    }
</style>   