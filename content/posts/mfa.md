+++
Title = "MFA alternatives to Google/Microsoft Authenticator"
Date = "2023-06-07T10:27:20-0800"
Author = "Paige"
Description = "Better, reliable tools to get your MFA/TOTP needs on track"
cover = "img/og.png"
tags = ["Security", "MFA", "TOTP", "KeePass"]
+++

Initially I started using TOTP for everything fairly recently with the Microsoft Authenticator App because I thought that I would be able to back up my shared TOTP secrets for maintaining my own offline backups. I was wrong about that and ultimately had to reconfigure TOTP on all of the accounts that I had configured it for when I switched to [Keepassium](https://keepassium.com/) for iOS. It's a good, free app that offers a pro tier for cloud backups that you don't need if you'll be maintaining your own backups. 

## KeepassDX 
[KeepassDX](https://www.keepassdx.com) is the Android equivalent of what I use. Both use a database format that is common to KeePass and should therefore be interoperable. If not, I know that the desktop [KeePass](https://keepass.info/download.html) app is capable of opening Keepassium databases at least. KeePassDX is also free.

### Microsoft / Google Authenticator

The problem as I see it with Microsoft and Google Authenticator is that ~~[Google Authenticator isn't even available on iOS officially for one thing.](https://apps.apple.com/us/app/google-authenticator/id388497605)~~ Another is that neither of them allow you to export your shared secrets to another platform; you're locked in and you will have to reconfigure each one for every account that you have; currently I'm up to 44 shared secrets (44 different accounts that I have to enter a 6 digit pin to access upon logging into them.) Here's another problem I have with them; how do you get access to your Google authenticator if you need a pin from Google Authenticator? The short answer is you can't; they provide you with a series of recovery codes that they intend for you to print off and store away for safe keeping and you must use them in this scenario. 

This is fine, but personally I don't even want my shared secrets backed up on Google unless it's on Google Drive; in which case I've encrypted the backup with a PGP key that I also keep backed up on Google protected by a very long password. I also keep offline copies of these in the same format. There's really nothing inherently wrong with either of these options except for consistency and the inability to switch from one platform to another; of the two I would reccomend Microsoft Authenticator over Google for it's availability on both iOS and Android. 

### Why KeePassDX or Keepassium 
Not only do they support MFA/TOTP shared secrets (the ones that you setup with a QR code with your phone) but you can also use them to store files; a lot of sites provide you with a set of recovery codes when you activate MFA in the form of a text file, and it's nice to have the ability to store these securely; The KeePass database format is encrypted. Both Keepassium and KeePassDX can encrypt/decrypt the database using a password or even a security key such as a [YubiKey5Ci](https://www.yubico.com/product/yubikey-5ci/?gclid=CjwKCAjw1YCkBhAOEiwA5aN4Aca8kMyg8JVknSp0_l1mh01isjqSSoV6QpTeJC-DOxzBlMN9e4UGvxoCvWkQAvD_BwE). 

It's important to note that if you want to use a YubiKey for this purpose specifically, it's a good idea to keep an offline backup of the database that is *not* encypted with the YubiKey but rather a password because you won't be able to decrypt the database again if you lose your YubiKey. 

## Keeping offline backups 
To start I have always used [GnuPG](https://gnupg.org/download/) for personal encryption and I keep sensitve files encrypted with one of my PGP private keys. Unfortunately GnuPG is a worthy of a whole blog post on it's own but I reccomend starting with GPG4Win or GnuPG for Mac; both are avaiable on GnuPG's website. When setting up your GnuPG keychain you'll want to pick a relatively good password that you'll be able to recall as it will prompt you for it to unlock the keychain whenever you need to use it. 

I also export the keychain (the public and private key) to file and the file itself is encrypted using a password. You really don't need to do this, though since the KeePass database itself is encrypted; as mentioned before with a password, certificate or a security key. 

## Printing Recovery keys 
It's a good idea to do this if you have the capacity for it; store them in a safe place. Personally I only store printed copies of my most major accounts printed. In any case you should always keep all of your recovery keys printed if you're not using something like keepass to store them, but it's a good idea to store recovery keys for a few major accounts as printed on paper for good measure.

## FIDO/U2F/UAF
To be fair I still use the Microsoft Authenticator for one thing; logging into live.com. Live sends a notification to the Authenticator app on my phone to approve the login request. Google Search or the Youtube app for iOS also offers this as an alternative to entering a TOTP 6 digit pin if you have the search app installed. 

### Steam 
Steam is one of the few that doesn't have a modern TOTP option as of 06/07/2023, but they offer an authentication mechenism similar to FIDO/U2F/UAF. 

## Using Security keys to authenticate accounts
This is an option (for some) however TOTP is much more prevalent. I have not used security keys myself, but this is also not how I would prefer to use one either. They are however a good mechenism for securing a KeePass database; Microsoft and Google Authenticator both make use of the phone's secure/trusted storage which is a lost advantage in the KeePass database being stored on the user filesystem; having a physical security key that restricts access to the database would be sufficiently complimentary to the advantages of Microsoft and Google Authenticators in that regard. 

# Questions ? 
E-mail me, I'm glad to try to answer.