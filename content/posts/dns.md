+++
Title = "Free DNS Resolver"
Date = "2023-07-05T05:58:20-0800"
Author = "Paige"
Description = "Web based DNS resolver using DNS over HTTPS"
cover = "img/og.png"
tags = ["Tools", "DNS", "DoTLS", "DoH"]
Keywords = ["DNS", "DoTLS", "DoH", "resolver", "lookup"]
+++

<script src="/DNSoHTTPS/doh.min.js"></script>

<div id="formio"></div>
<br />
<br />
<div id="dns_form"></div>
<pre id="dns_output">Enter host and hit submit to perform dns lookup</pre>

<script type="application/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        function resolve(query) {
            document.getElementById("dns_output").innerHTML = "";
            const resolver = new doh.DohResolver('https://1.1.1.1/dns-query');
            if(query.data.a == true)
                resolver.query(query.data.question, 'A')
                .then(response => {
                    response.answers.forEach(ans => document.getElementById("dns_output").innerHTML += "A -> " + ans.data + "\n");
                })
                .catch(err => document.getElementById("dns_output").innerHTML += err + "\n");
            if(query.data.aaaa == true)
                resolver.query(query.data.question, 'AAAA')
                .then(response => {
                    response.answers.forEach(ans => document.getElementById("dns_output").innerHTML += "AAAA -> " + ans.data + "\n");
                })
                .catch(err => document.getElementById("dns_output").innerHTML += err + "\n");
            if(query.data.mx == true)
                resolver.query(query.data.question, 'MX')
                .then(response => {
                    response.answers.forEach(ans => document.getElementById("dns_output").innerHTML += "MX -> " + ans.data.preference + " " + ans.data.exchange + "\n");
                })
                .catch(err => document.getElementById("dns_output").innerHTML += err + "\n");
            if(query.data.srv == true)
                resolver.query(query.data.question, 'SRV')
                .then(response => {
                    response.answers.forEach(ans => document.getElementById("dns_output").innerHTML += "SRV -> " + ans.data.priority + " " + ans.data.target + " " + ans.data.port + "\n");
                })
                .catch(err => document.getElementById("dns_output").innerHTML += err + "\n");
            if(query.data.txt == true)
                resolver.query(query.data.question, 'TXT')
                .then(response => {
                    response.answers.forEach(ans => document.getElementById("dns_output").innerHTML += "TXT -> " + ans.data + "\n");
                })
                .catch(err => document.getElementById("dns_output").innerHTML += err + "\n");
            if(query.data.caa == true)
                resolver.query(query.data.question, 'CAA')
                .then(response => {
                    response.answers.forEach(ans => document.getElementById("dns_output").innerHTML += "CAA -> " + ans.data.flags + " " + ans.data.issuerCritical + " " + ans.data.tag + " " + ans.data.value + "\n");
                })
                .catch(err => document.getElementById("dns_output").innerHTML += err + "\n");
            if(query.data.sshfp == true)
                resolver.query(query.data.question, 'SSHFP')
                .then(response => {
                    response.answers.forEach(ans => document.getElementById("dns_output").innerHTML += "SSHFP -> " + ans.data + "\n");
                })
                .catch(err =>  document.getElementById("dns_output").innerHTML += err + "\n");
            if(query.data.ns == true)
                resolver.query(query.data.question, 'NS')
                .then(response => {
                    response.answers.forEach(ans => document.getElementById("dns_output").innerHTML += "NS -> " + ans.data + "\n");
                })
                .catch(err =>  document.getElementById("dns_output").innerHTML += err + "\n");
            if(query.data.soa == true)
                resolver.query(query.data.question, 'SOA')
                .then(response => {
                    response.answers.forEach(ans => document.getElementById("dns_output").innerHTML += "SOA -> " + ans.data.expire + " " + ans.data.minimum + " " + ans.data.mname + " " + ans.data.refresh + " " + ans.data.retry + " " + ans.data.rname + " " + ans.data.serial + "\n");
                })
                .catch(err =>  document.getElementById("dns_output").innerHTML += err + "\n");

        }

        Formio.createForm(document.getElementById('dns_form'), {
            components: [
                {
                    type: 'textfield',
                    label: 'Q',
                    placeholder: 'Question',
                    validate: {
                        required: true
                    },
                    key: 'question',
                    input: true,
                    inputType: 'text'
                },
                {
                    type: 'checkbox',
                    label: 'A',
                    placeholder: 'A',
                    key: 'a',
                    input: true,
                    inputType: 'checkbox'
                },
                {
                    type: 'checkbox',
                    label: 'AAAA',
                    placeholder: 'AAAA',
                    key: 'aaaa',
                    input: true,
                    inputType: 'checkbox'
                },
                {
                    type: 'checkbox',
                    label: 'MX',
                    placeholder: 'MX',
                    key: 'mx',
                    input: true,
                    inputType: 'checkbox'
                },
                {
                    type: 'checkbox',
                    label: 'SRV',
                    placeholder: 'SRV',
                    key: 'srv',
                    input: true,
                    inputType: 'checkbox'
                },
                {
                    type: 'checkbox',
                    label: 'TXT',
                    placeholder: 'TXT',
                    key: 'txt',
                    input: true,
                    inputType: 'checkbox'
                },
                {
                    type: 'checkbox',
                    label: 'CAA',
                    placeholder: 'CAA',
                    key: 'caa',
                    input: true,
                    inputType: 'checkbox'
                },
                {
                    type: 'checkbox',
                    label: 'SSHFP',
                    placeholder: 'SSHFP',
                    key: 'sshfp',
                    input: true,
                    inputType: 'checkbox'
                },
                {
                    type: 'checkbox',
                    label: 'NS',
                    placeholder: 'NS',
                    key: 'ns',
                    input: true,
                    inputType: 'checkbox'
                },
                {
                    type: 'checkbox',
                    label: 'SOA',
                    placeholder: 'SOA',
                    key: 'soa',
                    input: true,
                    inputType: 'checkbox'
                },
                {
                    type: 'button',
                    action: 'submit',
                    label: 'Submit',
                    theme: 'primary',
                    key: 'submit',
                    disableOnInvalid: true
                }
            ]
        }).then(function(form) {
            form.on('submit', function(submission) {
                console.log(submission);
                resolve(submission);
                return(true);
            });
        });
    });
</script>
