+++
Title = "Electronic Sets"
Date = "2023-01-26T09:58:20-0800"
Author = "Paige"
Description = "Nobody listens to techno"
cover = "img/og.png"
tags = ["Music"]
+++

I haven't put anything here that didn't speak to me, everything here is exceptionally good or it's not in this list.

## Channels

### Youtube
- [HÖR BERLIN](https://www.youtube.com/@hoer.berlin)
- [Boiler Room](https://www.youtube.com/@boilerroom)
- [MixMag](https://www.youtube.com/@Mixmag)

## Mixes
### Soundcloud
- [James Zabiela Rec Dec 2019](https://soundcloud.com/jameszabiela/james-zabiela-rec-dec-2019?utm_source=clipboard&utm_medium=text&utm_campaign=social_sharing)
- 
### Youtube
- [MODEL 1.4 - Ellen Allien B2B Héctor Oaks / January 22 / 5pm-6pm](https://www.youtube.com/live/Q3no26KDuh0?feature=share)
- [Noémie / July 30 / 9pm-10pm](https://www.youtube.com/watch?v=wDJvveMPfu4)
- [DJ HELL rare edits set in The Lab LDN](https://www.youtube.com/watch?v=d7ajh9C7IMY)
- [DJ Hell Boiler Room Berlin DJ Set](https://www.youtube.com/watch?v=QJSbISx8dCU)
- [James Zabiela Boiler Room London DJ Set](https://www.youtube.com/watch?v=nSSjnID4GJU)
- [Sama' Abdulhadi | Boiler Room Palestine](https://youtu.be/x9VYKrtziSg)
- [Neuromotor @ ReveillOZ 19/20 - Live Streaming BSTV](https://youtu.be/Ngc5MUQJcDM)
- [Astrix @ Universo Paralello 2019-2020 Full Set](https://youtu.be/8eYEW4q42y8)

### Stuff my friend recorded
from way back in the day 

- [Scott Bond - Ultra - Cybertribe - 6.22.01 Little Rock Ar](https://soundcloud.com/jasonhawkins-2/scott-bond-6-22-01-little-rock?utm_source=clipboard&utm_medium=text&utm_campaign=social_sharing)
