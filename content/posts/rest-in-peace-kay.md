+++
Title = "Looking back in 2024"
Date = "2024-12-16T20:00:20-0800"
Author = "Paige"
Description = "Catching up, it's been awhile"
cover = "img/og.png"
tags = ["Friends", "Sad"]
+++

Haven't written anything here in over a year. This year has been pretty awful since the start. My dad died last December, my partner Maxine 
crashed out in a motorcycle race (MotoAmerica qualifying lap), and a dear friend of mine died in a very tragic way. We had not spoken in years but I always held her in high regards and looked forward to the day that we might reconnect. I received This e-mail message from her father:

> Paige, I regret to inform you that Kay passed away last month.  She was with a friend, and took some powdered fentanyl.  She passed out. She was given CPR and 911 was called.  They managed to get her heart beating again and took her to Harborview.  She did not regain consciousness.  They did tests for about a week and said that her brain suffered irreparable damage because it had been deprived of oxygen for too long.  After a week, they removed the ventilator and she stopped breathing after about a day. Obviously this is devastating news for us.  So smart and talented; what a waste.  We knew she was having difficulties, but she didn't seem to want much contact with us.  We have been supported by family and close friends, who helped us clean out her apartment and storage, take loads to GoodWill, and continue to help out. I hope this email reaches you, let me know if it does and give me your current contact information.  I hope you are doing well.

> Regards,       
xxxxx xxxxxxxxx
xxxxxxxxx
Seattle WA xxxxx
(xxx) xxx-xxxx

I'd add that she was indeed incredibly talented as I'm recounting her corresondence with the `FreeBSD-hackers` mailing list dating back to 1998. She was a native to Seattle and worked for a number of local tech companies going back even to the first days of Amazon as well as Speakeasy. She was very accomplished and she is survived by her own contributions in a number of opensource projects. She was very tasteful and was fluent in the French language. She had her own beliefs and opinions and she wasn't afraid to own them which is such a rare quality of a person in my experience.

She was a huge inspiration and she gave me a sense of confidence that has driven me to find my own success in life and I am so greatful for it. She could also be very cruel, judgemental, a hypocrite, abusive but I never held it against her because I guess I could relate because she was apparently had the capacity for empathy, shame, and remorse and she was characteristically a real human being. I've never met another like her and I never once considered her to be unworthy of the relationship we shared. 

We sepearated in 2011 and cumulatively over the years since I have come to realize that I could have been a better friend myself. I really wanted to and I tried to reach out a few times but we could never seem to reconnect. I could tell since she graduated from obtaing her degree from the UW in 2016 she was struggling to make ends meet and keep a job and I wanted to do something. I tried to reach out to her one last time in April of this year. I asked my partner Maxine to reach out to her for me because I thought maybe it would help if she could see that I have such a great person such as her that would have anything to do with me let alone reach out to her for me. I wanted to give her the friends that I have and be a better one myself because she deserved it. She deserved to be happy and I wanted to restore her the confidence she once had in herself as she had once done for me at a very low point in my life. 

For as much confidence as she had in herself and despite her successes she could lose it all and this ultimately led her to self destructive choices. She found some reprise through 12 step programs and was even a substance abuse counselor herself. This was not something we particularly had in common nor could I relate to. She was characterstically better off around me because I would not encourage her when sought my approval but it is an addiction pattern that addicts seek called enablement. Consequently this led to fights which got out of hand. It caused me to get fired from two jobs, then she lost her own and she had to move back in with her parents. She went to rehab which she insisted on and bitterly I found a new job. I adopted a cat named Millie which once belonged to another friend of mine and things were good but I always missed Kay. I missed the companionship and mutual interest in technology, watching movies together, gaming, weekend getaways, she also taught me to love operas. We visited a few art exhibits, too though I can't think of the names it didn't matter and I was just happy to experience them with her.

