#!/usr/bin/env perl
use strict;
use warnings;
use feature 'state';
use File::Path qw(make_path);
use Getopt::Long;
use File::Basename;
use Time::Local;

# Command line arguments
my $os = '';
my $os_version = '';
my $output_root_dir = '';
my $input_file = '';
my $debug = 0;

# Parse command line arguments
GetOptions(
    "os=s" => \$os,
    "os-version=s" => \$os_version,
    "output-root-dir=s" => \$output_root_dir,
    "debug|d" => \$debug,
) or die "Error in command line arguments\n";

# Get input file from remaining argument
$input_file = shift @ARGV or die "Input file not specified\n";

# Man page format detection constants
use constant {
    FORMAT_UNKNOWN => 0,
    FORMAT_TROFF => 1
};

# Metadata storage
my %metadata = (
    title => '',
    date => scalar localtime,
    author => 'None Specified',
    tags => [],
    keywords => [],
    description => 'None Specified',
    manpage_name => '',
    manpage_section => '',
    operating_system => $os,
    operating_system_version => $os_version,
    detected_package_version => '0',
    manpage_format => '',
);

# Add debug helper function
sub debug {
    my ($msg) = @_;
    warn "DEBUG: $msg\n" if $debug;
}

# Helper function to parse dates
sub parse_date {
    my ($date_str) = @_;
    return undef unless $date_str;
    
    # Try various date formats
    if ($date_str =~ /(\d{4})-(\d{2})-(\d{2})/) {
        # YYYY-MM-DD
        return "$1-$2-$3";
    }
    elsif ($date_str =~ /(\w+)\s+(\d+),?\s+(\d{4})/) {
        # Month DD, YYYY
        my %months = (
            'January' => '01', 'February' => '02', 'March' => '03',
            'April' => '04', 'May' => '05', 'June' => '06',
            'July' => '07', 'August' => '08', 'September' => '09',
            'October' => '10', 'November' => '11', 'December' => '12'
        );
        my $month = $months{$1} || '01';
        my $day = sprintf("%02d", $2);
        return "$3-$month-$day";
    }
    elsif ($date_str =~ /(\d{4})/) {
        # Just year
        return "$1-01-01";
    }
    return undef;
}

# Helper function to format author
sub format_author {
    my ($author) = @_;
    return 'None Specified' unless $author;
    
    # Clean up the author string
    $author =~ s/^\s+|\s+$//g;
    
    # Try to extract email if present
    my $email = '';
    if ($author =~ s/<([^>]+)>//) {
        $email = $1;
    }
    
    # Clean up name
    $author =~ s/^\s+|\s+$//g;
    $author =~ s/\s+/ /g;
    
    # Remove common prefixes/suffixes
    $author =~ s/^(?:by\s+|written by\s+|copyright\s+)//i;
    $author =~ s/\s*(?:Inc\.|Corporation|Corp\.|Ltd\.|LLC)\.?\s*$//i;
    
    # If we have both name and email
    if ($email && $author) {
        return "$author <$email>";
    }
    
    return $author || 'None Specified';
}

# First pass: Detect format
sub detect_format {
    my ($file) = @_;
    debug("detect_format: Detecting format for file: $file");
    
    open(my $fh, '<', $file) or die "Cannot open $file: $!";
    
    my $format = FORMAT_UNKNOWN;
    my $line_num = 0;
    while (my $line = <$fh>) {
        $line_num++;
        debug("detect_format: Line $line_num: $line");
        
        # Check for mdoc format and reject
        if ($line =~ /^\.Dd\s+/ || $line =~ /^\.Dt\s+/) {
            die "mdoc format not supported";
        }
        # Detect troff format
        elsif ($line =~ /^\.TH\s+/ || $line =~ /^\.SH\s+/) {
            $format = FORMAT_TROFF;
            debug("detect_format: Detected format (line $line_num)");
            last;
        }
    }
    close($fh);
    
    debug("detect_format: Format detection result: " . 
          ($format == FORMAT_UNKNOWN ? "unknown" : "supported"));
           
    return $format;
}

# Second pass: Extract metadata
sub extract_metadata {
    my ($file, $format) = @_;
    debug("extract_metadata: Processing $file (format: " . 
          ($format == FORMAT_TROFF ? "troff" : "troff") . ")");
    
    return extract_metadata_internal($file);
}

sub extract_metadata_internal {
    my ($file) = @_;
    debug("extract_metadata: Processing $file");
    
    my %meta = (
        title => '',
        date => scalar localtime,
        author => 'None Specified',
        description => 'None Specified',
        manpage_name => '',
        manpage_section => '',
        operating_system => $os,
        operating_system_version => $os_version,
        detected_package_version => '0',
        manpage_format => 'troff',
        keywords => [],
    );
    
    # Get base name and section
    if ($file =~ m{/man([^/]+)/}) {
        $meta{manpage_section} = $1;
    }
    
    my $basename = basename($file);
    $basename =~ s/\.(gz|bz2)$//;
    $basename =~ s/\.[^.]+$//;
    $basename =~ s/\\//g;
    $basename =~ s/["']//g;
    $basename =~ s/\s+\d+$//;
    $basename =~ s/[^a-z0-9\-_]//gi;
    $meta{manpage_name} = $basename;
    
    # Read file for metadata
    open(my $fh, '<', $file) or die "Cannot open $file: $!";
    my $in_description = 0;
    my $description = '';
    my $in_see_also = 0;
    my @keywords;
    
    while (my $line = <$fh>) {
        # Look for .TH line with version and date
        if ($line =~ /^\.TH\s+"?([^"]+)"?\s+([^\s"]+)(?:\s+"([^"]*)")?(?:\s+"([^"]*)")?(?:\s+"([^"]*)")?/) {
            my ($name, $section, $date, $source, $manual) = ($1, $2, $3, $4, $5);
            debug("extract_metadata: Found .TH: name=" . ($name || '') . 
                  ", section=" . ($section || '') . 
                  ", date=" . ($date || 'none') . 
                  ", source=" . ($source || 'none'));
            
            $meta{manpage_name} = lc($name);
            $meta{manpage_section} = $section if $section =~ /^[a-z0-9]+$/i;
            
            # Parse date if provided
            if ($date) {
                my $parsed_date = parse_date($date);
                $meta{date} = $parsed_date if $parsed_date;
            }
            
            # Extract version from source field if it contains numbers
            if ($source && $source =~ /(\d+(?:\.\d+)+)/) {
                $meta{detected_package_version} = $1;
            }
        }
        # Look for copyright/author info with expanded patterns
        elsif ($line =~ /^\.\\\".*(?:Copyright|©)\s*(?:\(c\))?\s*(?:by)?\s*(\d{4}(?:-\d{4})?)\s*(.*?)$/i) {
            my ($year, $author) = ($1, $2);
            debug("extract_metadata: Found copyright: $author ($year)");
            
            # Format author
            my $formatted_author = format_author($author);
            $meta{author} = $formatted_author if $formatted_author ne 'None Specified';
            
            # Use year range for version if we don't have one
            if ($meta{detected_package_version} eq '0' && $year =~ /-/) {
                $meta{detected_package_version} = $year;
            }
            
            # Use year for date if we don't have one
            if ($year =~ /(\d{4})/) {
                $meta{date} ||= "$1-01-01";
            }
        }
        # Look for version info in text
        elsif ($line =~ /(?:Version|v)\s*(\d+(?:\.\d+)+)/) {
            debug("extract_metadata: Found version: $1");
            $meta{detected_package_version} = $1 if $meta{detected_package_version} eq '0';
        }
        # Handle description section
        elsif ($line =~ /^\.SH\s+"?DESCRIPTION"?/) {
            $in_description = 1;
            $description = '';
        }
        elsif ($in_description) {
            if ($line =~ /\S/ && $line !~ /^\s*\./) {
                # Strip font macros before cleaning
                $line =~ s/\\f[BI](.*?)(?:\\fR|\\fP|\\f[IRP])/$1/g;  # Remove paired font markers
                $line =~ s/\\f[BI].*$//;  # Remove unpaired font markers at end
                
                my $cleaned = clean_text($line);
                $cleaned =~ s/\\//g;  # Remove any remaining backslashes
                
                if ($description) {
                    $description .= " " . $cleaned;
                } else {
                    $description = $cleaned;
                }
                if (length($description) >= 251) {
                    $description = substr($description, 0, 251);
                    $description .= "...";
                    $in_description = 0;
                    $meta{description} = $description;
                }
            }
            elsif ($line =~ /^$/) {
                $description .= "..." unless $description =~ /\.$/;
                $in_description = 0;
                $meta{description} = $description if $description;
            }
        }
        # Collect keywords from SEE ALSO section
        elsif ($line =~ /^\.SH\s+"?SEE\s+ALSO"?/) {
            $in_see_also = 1;
        }
        elsif ($in_see_also && $line =~ /\S/) {
            my $cleaned = clean_text($line);
            # Extract command names, removing section numbers
            while ($cleaned =~ /\b([a-z0-9_-]+)(?:\([0-9a-z]+\))?[\s,.]*/gi) {
                push @keywords, lc($1);
            }
        }
        elsif ($in_see_also && $line =~ /^\.SH/) {
            $in_see_also = 0;
        }
    }
    close($fh);
    
    # Add unique keywords
    if (@keywords) {
        my %seen;
        @keywords = grep { !$seen{$_}++ } @keywords;
        $meta{keywords} = \@keywords;
    }
    
    # Clean up section and name if needed
    if ($meta{manpage_section} && length($meta{manpage_section}) > 1) {
        if ($meta{manpage_name} =~ /^(\S+)\s+([a-z])\s+.*$/i) {
            $meta{manpage_name} = $1;
            $meta{manpage_section} = $2;
        }
    }
    
    # Set title
    $meta{title} = "$meta{manpage_name}($meta{manpage_section})";
    
    debug("extract_metadata: Final metadata:");
    for my $key (sort keys %meta) {
        debug("    $key = $meta{$key}");
    }
    
    return %meta;
}

# Third pass: Convert to markdown
sub convert_to_markdown {
    my ($input_file, $format) = @_;
    debug("convert_to_markdown: Converting $input_file to markdown (format: " . 
          ($format == FORMAT_TROFF ? "troff" : "troff") . ")");
    
    my $markdown = '';
    state $last_macro_def_line = 0;  # Track the line number of the last '..'
    
    # First pass to find the last '..' line for troff files
    if ($format == FORMAT_TROFF) {
        debug("convert_to_markdown: Finding last macro definition");
        open(my $fh, '<', $input_file) or die "Cannot open $input_file: $!";
        my $line_num = 0;
        while (my $line = <$fh>) {
            $line_num++;
            if ($line =~ /^\.\./) {
                $last_macro_def_line = $line_num;
            }
        }
        close($fh);
        debug("convert_to_markdown: Last macro definition at line $last_macro_def_line");
    }
    
    # Add frontmatter
    debug("convert_to_markdown: Generating frontmatter");
    $markdown .= generate_frontmatter(%metadata);
    
    # Convert man page content
    debug("convert_to_markdown: Converting main content");
    open(my $fh, '<', $input_file) or die "Cannot open $input_file: $!";
    
    my $line_num = 0;
    while (my $line = <$fh>) {
        $line_num++;
        # Skip until after the last '..' line
        next if $line_num <= $last_macro_def_line;
        
        debug("convert_to_markdown: Processing line $line_num: $line") if $line =~ /^\.(?:SH|SS|TP|IP|nf|fi)/;
        $markdown .= convert_line($line);
    }
    
    close($fh);
    debug("convert_to_markdown: Finished converting content");
    return $markdown;
}

# Enhanced troff conversion with more macros and formatting support
sub convert_line {
    my ($line) = @_;
    our $in_list ||= 0;
    our $in_table ||= 0;
    our @table_data;
    our $in_license ||= 0;
    our $license_text = '';
    our $in_example ||= 0;
    our $example_text = '';
    
    # Handle example blocks first
    my $result = handle_example($line, \$in_example, \$example_text);
    return $result if defined $result;
    
    # Skip comments and formatting directives
    return "" if $line =~ /^\.\\\"/;  # Skip comments
    return "" if $line =~ /^\.(nh|if|ad|nr)/; # Skip formatting directives
    
    # Remove trailing whitespace
    $line =~ s/\s+$//;
    
    # Handle license/copyright blocks
    if ($line =~ /^\.(?:nf|PP)\s*$/ && $line =~ /Copyright|License|Permission|PROVIDED/i) {
        $in_license = 1;
        return "";
    }
    elsif ($in_license && $line =~ /^\.(?:fi|PP)/) {
        $in_license = 0;
        my $license = $license_text;
        $license_text = '';
        return "\n## License\n\n$license\n";
    }
    elsif ($in_license) {
        $license_text .= clean_text($line) . "\n";
        return "";
    }
    
    # Handle section headers
    if ($line =~ /^\.SH\s+"?([^"]+)"?/) {
        my $heading = clean_text($1);
        $heading =~ s/["']//g;
        return "\n## $heading\n\n";
    }
    elsif ($line =~ /^\.SS\s+"?([^"]+)"?/) {
        my $heading = clean_text($1);
        $heading =~ s/["']//g;
        return "\n### $heading\n\n";
    }
    
    # Handle paragraphs and spacing
    if ($line =~ /^\.PP|^\.sp|^\.LP/) {
        return "\n";
    }
    
    # Handle indented paragraphs and lists
    if ($line =~ /^\.TP\s+(\d+)?n?/) {
        $in_list = 1;
        return "\n";
    }
    elsif ($line =~ /^\.IP\s+"?([^"]+)"?\s+(\d+)?n?/) {
        return "\n- " . clean_text($1) . "\n";
    }
    
    # Handle font formatting
    if ($line =~ /\\f[BI]/) {
        # Handle nested font changes
        while ($line =~ /\\fB(.*?)(?:\\fR|\\fP|\\f[IRP])/g) {
            my $bold = $1;
            $line =~ s/\\fB\Q$bold\E(?:\\fR|\\fP|\\f[IRP])/**$bold**/g;
        }
        while ($line =~ /\\fI(.*?)(?:\\fR|\\fP|\\f[BRP])/g) {
            my $italic = $1;
            $line =~ s/\\fI\Q$italic\E(?:\\fR|\\fP|\\f[BRP])/*${italic}*/g;
        }
    }
    
    # Handle font size macros
    elsif ($line =~ /^\.SM\s+(.+)/) {
        debug("convert_line: Processing SM (small) macro");
        my $text = clean_text($1);
        return "<small>$text</small>";
    }
    elsif ($line =~ /^\.LG\s+(.+)/) {
        debug("convert_line: Processing LG (large) macro");
        my $text = clean_text($1);
        return "<big>$text</big>";
    }

    # Handle indentation and margins
    elsif ($line =~ /^\.RS\s*(\d+)?/) {
        debug("convert_line: Starting relative indent");
        return "\n> ";
    }
    elsif ($line =~ /^\.RE/) {
        debug("convert_line: Ending relative indent");
        return "\n\n";
    }

    # Handle definition lists
    elsif ($line =~ /^\.DT/) {
        debug("convert_line: Starting definition list");
        return "\n";
    }
    elsif ($line =~ /^\.DD\s+(.+)/) {
        debug("convert_line: Processing definition data");
        return ": " . clean_text($1) . "\n";
    }

    # Handle tables with box drawing
    elsif ($line =~ /^\.TB\s+(\d+)/) {
        debug("convert_line: Starting boxed table");
        return "\n";
    }

    # Handle manual page references
    elsif ($line =~ /^\.BR\s+(\S+)\s+(\d)/) {
        debug("convert_line: Processing BR (bold reference) macro");
        return "**$1**($2)";
    }
    elsif ($line =~ /^\.IR\s+(\S+)\s+(\d)/) {
        debug("convert_line: Processing IR (italic reference) macro");
        return "*$1*($2)";
    }

    # Handle URLs and links
    elsif ($line =~ /^\.URL\s+"([^"]+)"\s+"([^"]+)"/) {
        debug("convert_line: Processing URL macro");
        return "[$2]($1)";
    }

    # Handle command synopsis
    elsif ($line =~ /^\.SY\s+(\S+)/) {
        debug("convert_line: Processing SY (synopsis) macro");
        return "\n**$1**";
    }
    elsif ($line =~ /^\.YS/) {
        debug("convert_line: Ending synopsis");
        return "\n\n";
    }

    # Handle option lists
    elsif ($line =~ /^\.OP\s+(\S+)\s+(\S+)(?:\s+(\S+))?/) {
        debug("convert_line: Processing OP (option) macro");
        my ($flag, $arg, $default) = ($1, $2, $3);
        return $default ? 
            "\n- **$flag** *$arg* (default: $default)\n" :
            "\n- **$flag** *$arg*\n";
    }
    
    # Then clean the text
    $line = clean_text($line);
    
    # Return cleaned line with proper spacing
    return ($line =~ /\S/) ? "$line\n" : "\n";
}

# Helper function to clean and format text
sub clean_text {
    my ($text) = @_;
    debug("clean_text: Cleaning text: $text");
    
    # Escape curly braces
    $text =~ s/\{/\\{/g;
    $text =~ s/\}/\\}/g;
    
    # Remove lines that are just numbers followed by 'n'
    if ($text =~ /^\s*\d+n\s*$/) {
        debug("clean_text: Skipping number-n line");
        return "";
    }
    
    # Remove lines that start with a dot and contain just numbers followed by 'n'
    if ($text =~ /^\.\s*\d+n\s*$/) {
        debug("clean_text: Skipping .number-n line");
        return "";
    }
    
    # Remove specific formatting directives
    if ($text =~ /^\.(?:sp|RE|fi|nf)\s*\d*n?$/) {
        debug("clean_text: Skipping formatting directive");
        return "";
    }
    
    # Remove troff macros at start of line
    $text =~ s/^\.[A-Z]+\s+//;
    
    debug("clean_text: After macro removal: $text");
    
    $text =~ s/\\f[RP]//g;              # Remove remaining font changes
    $text =~ s/\\\((oq|cq)/'/g;         # Single quotes
    $text =~ s/\\\((lq|rq)/"/g;         # Double quotes
    
    debug("clean_text: After font handling: $text");
    
    # Handle special characters and escapes
    $text =~ s/\\e/\\\\\\\\/g;                  # Convert \e to backslash
    $text =~ s/\\\\/\\/g;                 # Convert double backslash to single
    $text =~ s/\\-/-/g;                   # Hyphens
    $text =~ s/\\\((em|--)/—/g;          # Em dash
    $text =~ s/\\\((oq|cq)/'/g;          # Single quotes
    $text =~ s/\\\((lq|rq)/"/g;          # Double quotes
    $text =~ s/\\&//g;                    # Zero-width space
    
    debug("clean_text: Final cleaned text: $text");
    return $text;
}

# Helper function to format tables - improved version
sub format_table {
    my ($data_ref) = @_;
    my @rows = @$data_ref;
    debug("format_table: Formatting table with " . scalar(@rows) . " rows");
    
    return "" unless @rows;
    
    # Clean up the data
    debug("format_table:     Original first row: $rows[0]");
    @rows = map { clean_text($_) } @rows;
    debug("format_table:     Cleaned first row: $rows[0]");
    
    my $table = "\n";
    my @columns = split(/\s{2,}/, $rows[0]);
    debug("format_table:     Found " . scalar(@columns) . " columns");
    
    # Create header
    $table .= "| " . join(" | ", @columns) . " |\n";
    $table .= "|" . join("|", map { "-" x 20 } @columns) . "|\n";
    
    # Add data rows
    for my $i (1..$#rows) {
        my @cols = split(/\s{2,}/, $rows[$i]);
        debug("format_table:     Row $i: " . join(", ", @cols));
        $table .= "| " . join(" | ", @cols) . " |\n";
    }
    
    return $table . "\n";
}

# Error handling wrapper for file operations
sub safe_open {
    my ($mode, $filename) = @_;
    debug("safe_open:     Safe opening file: $filename (mode: $mode)");
    my $fh;
    
    eval {
        open($fh, $mode, $filename) or die "Cannot open $filename: $!";
    };
    
    if ($@) {
        debug("safe_open:     Error opening file: $@");
        die "safe_open: Error processing file $filename: $@";
    }
    
    debug("safe_open:     File opened successfully");
    return $fh;
}

# Main execution
debug("main: Starting conversion of $input_file");
my $format = detect_format($input_file);
die "safe_open: Unknown man page format\n" if $format == FORMAT_UNKNOWN;

# Extract metadata
debug("main:     Extracting metadata");
%metadata = extract_metadata($input_file, $format);

# Validate required metadata
debug("main:     Validating metadata");
die "safe_open: Could not determine manpage section\n" unless $metadata{manpage_section};
die "safe_open: Could not determine manpage name\n" unless $metadata{manpage_name};

# Create output directory path
debug("main:     Creating output path");
my $output_path = sprintf("%s/%s/%s/%s/%s/%s",
    $output_root_dir,
    $metadata{operating_system},
    $metadata{operating_system_version},
    $metadata{manpage_section},
    $metadata{manpage_name},
    $metadata{detected_package_version}
);
debug("main:     Output path: $output_path");

# Create directory structure
debug("main:     Creating directory structure");
make_path($output_path);

# Convert and write markdown
debug("main:     Converting to markdown");
my $markdown = convert_to_markdown($input_file, $format);
my $output_file = "$output_path/index.md";
debug("main:     Writing output to: $output_file");
my $out_fh = safe_open('>', $output_file);
print $out_fh $markdown;
close($out_fh);

debug("main:     Conversion complete");
print "safe_open: Converted man page saved to: $output_file\n";

# Update frontmatter generation to clean up arrays and ensure ASCII
sub generate_frontmatter {
    my (%meta) = @_;
    debug("generate_frontmatter: Generating frontmatter");
    my $fm = "+++\n";
    
    while (my ($key, $value) = each %meta) {
        debug("generate_frontmatter: Processing key: $key");
        if (ref($value) eq 'ARRAY') {
            # Skip empty arrays or arrays with empty strings
            my @cleaned = grep { $_ && length($_) > 0 } @$value;
            if (@cleaned) {
                debug("generate_frontmatter:     Array value, " . scalar(@cleaned) . " items");
                @cleaned = map { 
                    my $v = $_;
                    $v =~ s/[^\x20-\x7E]//g;  # ASCII only
                    $v =~ s/["']//g;          # Remove quotes
                    "\"$v\""                  # Add consistent quotes
                } @cleaned;
                $fm .= "$key = [" . join(", ", @cleaned) . "]\n";
            }
        } else {
            # Clean scalar values
            debug("generate_frontmatter:     Scalar value: $value");
            my $cleaned = $value;
            $cleaned =~ s/[^\x20-\x7E]//g;  # ASCII only
            $cleaned =~ s/["']//g;          # Remove quotes
            $cleaned =~ s/\\-/-/g;          # Un-escape hyphens
            $fm .= "$key = \"$cleaned\"\n";  # Add consistent quotes
        }
    }
    
    $fm .= "+++\n\n";
    debug("generate_frontmatter: Frontmatter generated");
    return $fm;
}

# Handle troff example blocks
sub handle_example {
    my ($line, $in_example, $example_text) = @_;
    state $in_list = 0;  # Add state variables
    state $in_table = 0;
    state @table_data;
    state $in_license = 0;
    state $license_text = '';
    
    debug("handle_example:     $line");
    
    # Start of example block
    if ($line =~ /^\.nf/) {
        debug("handle_example: Starting example block");
        $$in_example = 1;
        return "\n```\n";
    }
    # End of example block
    elsif ($line =~ /^\.fi/ && $$in_example) {
        debug("handle_example:     Ending example block");
        $$in_example = 0;
        return "```\n\n";
    }
    # Inside example block
    elsif ($$in_example) {
        debug("handle_example:     Inside example block");
        # Skip standalone formatting lines
        if ($line =~ /^\.(?:sp|RE|RS|in|ti)\s*\d*n?\s*$/) {
            debug("handle_example:         Skipping formatting line: $line");
            return "";
        }
        if ($line =~ /^\s*\d+n\s*$/) {
            debug("handle_example:         Skipping number-n line: $line");
            return "";
        }
        
        debug("handle_example:         Before cleaning: $line");
        # Clean up the line but preserve content and backslashes
        $line =~ s/\\f[BIRP]//g;      # Remove font markers
        $line =~ s/\\\((oq|cq)/'/g;   # Then convert remaining quotes
        $line =~ s/\\\((lq|rq)/"/g;   # Then double quotes
        $line =~ s/\\\((em|--)/--/g;  # Finally em dashes
        debug("handle_example:         After cleaning: $line");
        
        return $line;
    }

    # In convert_line, add handling for these macros:
    if ($line =~ /^\.AP\s+(\S+)\s+(\S+)\s+(\S+)(?:\s+(\S+))?/) {
        debug("convert_line: Processing AP macro: $line");
        my ($type, $name, $inout, $indent) = ($1, $2, $3, $4);
        return "\n**$name** (*$type*) - $inout\n\n";
    }

    # Handle code blocks
    elsif ($line =~ /^\.CS/) {
        debug("convert_line: Starting code block");
        return "\n```\n";
    }
    elsif ($line =~ /^\.CE/) {
        debug("convert_line: Ending code block");
        return "```\n\n";
    }

    # Handle indented displays
    elsif ($line =~ /^\.DS/) {
        debug("convert_line: Starting indented display");
        return "\n> ";
    }

    elsif ($line =~ /^\.DE/) {
        debug("convert_line: Ending indented display");
        return "\n\n";
    }

    # Handle BS/BE box enclosures
    elsif ($line =~ /^\.BS(?:\s+(.+))?/) {
        debug("convert_line: Starting box section");
        my $title = $1 ? "\n### $1\n" : "";
        return "\n---$title\n";
    }
    elsif ($line =~ /^\.BE/) {
        debug("convert_line: Ending box section");
        return "\n---\n\n";
    }

    # Handle VS/VE vertical sidebars
    elsif ($line =~ /^\.VS(?:\s+(\S+))?/) {
        debug("convert_line: Starting vertical sidebar");
        my $version = $1 ? " (Added in version $1)" : "";
        return "\n> $version\n";
    }
    elsif ($line =~ /^\.VE/) {
        debug("convert_line: Ending vertical sidebar");
        return "\n\n";
    }

    # Handle SO/SE standard options
    elsif ($line =~ /^\.SO(?:\s+(\S+))?/) {
        debug("convert_line: Starting standard options section");
        my $options = $1 || 'options';
        return "\n### Standard $options\n\n";
    }
    elsif ($line =~ /^\.SE/) {
        debug("convert_line: Ending standard options section");
        return "\nSee the standard options manual entry for details.\n\n";
    }

    # Handle text formatting macros
    elsif ($line =~ /^\.UL\s+(\S+)(?:\s+(\S+))?/) {
        debug("convert_line: Processing UL (underline) macro");
        my $text = $1;
        my $after = $2 ? " $2" : "";
        return "_${text}_$after";
    }
    elsif ($line =~ /^\.QW\s+(\S+)(?:\s+(\S+))?/) {
        debug("convert_line: Processing QW (quote word) macro");
        my $text = $1;
        my $after = $2 ? " $2" : "";
        return "\"${text}\"$after";
    }
    elsif ($line =~ /^\.PQ\s+(\S+)(?:\s+(\S+))?(?:\s+(\S+))?/) {
        debug("convert_line: Processing PQ (parenthesized quote) macro");
        my $text = $1;
        my $after = $2 ? " $2" : "";
        my $final = $3 ? " $3" : "";
        return "(\"${text}\"$after)$final";
    }

    # Handle OP (option) macro
    elsif ($line =~ /^\.OP\s+(\S+)\s+(\S+)\s+(\S+)/) {
        debug("convert_line: Processing OP (option) macro");
        return "";
    }

    # Handle AS (argument size) macro
    elsif ($line =~ /^\.AS(?:\s+(\S+))?(?:\s+(\S+))?/) {
        debug("convert_line: Processing AS (argument size) macro");
        return "";  # This is just for formatting, we can skip it
    }

    # Handle TP (tagged paragraph) with better formatting
    elsif ($line =~ /^\.TP(?:\s+(\d+))?/) {
        debug("convert_line: Processing TP (tagged paragraph) macro");
        $in_list = 1;
        return "\n";  # Start a new line for the term
    }

    # Handle QR (quoted range) macro
    elsif ($line =~ /^\.QR\s+(\S+)\s+(\S+)(?:\s+(\S+))?/) {
        debug("convert_line: Processing QR (quoted range) macro");
        my ($start, $end, $after) = ($1, $2, $3);
        $after = $after ? " $after" : "";
        return "\"$start\"-\"$end\"$after";
    }

    # Handle MT (empty string) macro
    elsif ($line =~ /^\.MT/) {
        debug("convert_line: Processing MT (empty string) macro");
        return '""';
    }

    return undef;
} 