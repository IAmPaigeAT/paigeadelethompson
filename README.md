```                                                                                         
 ######                         ###                            
 #     #   ##   #  ####  ###### ###  ####                      
 #     #  #  #  # #    # #       #  #                          
 ######  #    # # #      #####  #    ####                      
 #       ###### # #  ### #               #                     
 #       #    # # #    # #          #    #                     
 #       #    # #  ####  ######      ####                      
                                                               
                                                               
       #    #  ####  #    # ###### #####    ##    ####  ###### 
       #    # #    # ##  ## #      #    #  #  #  #    # #      
       ###### #    # # ## # #####  #    # #    # #      #####  
       #    # #    # #    # #      #####  ###### #  ### #      
       #    # #    # #    # #      #      #    # #    # #      
       #    #  ####  #    # ###### #      #    #  ####  ###### 
                                                               
            ...if only I could figure out how to embed passport.mid
```
- Main Paige: [https://paige.bio/resume](https://paige.bio/resume)
- Systems Administrator/Software Developer with over 20 years of experience in the Greater Seattle Area

- Interested in new job opportunities (inquiries are always welcome)
- Link to my [Resume](https://github.com/paigeadelethompson/resume/releases/download/latest/cv.pdf)

![Stats](https://github-readme-stats.vercel.app/api?username=paigeadelethompson&show_icons=true)
